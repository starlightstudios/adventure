-- |[Encounter A]|
--Taking down the two no-good-nicks near the entrance!

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sResultName = LM_GetScriptArgument(0)

--Always clean up dialogue.
WD_SetProperty("Hide")

-- |[Action]|
if(sResultName == "Action") then
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iEncounterA", "N", 1.0)
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Sammy] (*Action* is my middle name![P] Agent Action Almond![P] It's not confusing at all!)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Sammy", 35.25, 33.50)
    fnCutsceneFace("Sammy", 1, 0)
    fnCutsceneBlocker()
    fnCutsceneFace("GolemA", -1, 0)
    fnCutsceneFace("GolemB", -1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Golem", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemB", "Neutral") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] Hey, you look knowledgeable.[P] I was looking for my friend.[P] Have you seen her?[B][C]") ]])
    fnCutscene([[ Append("Vicks:[E|Neutral] Happy to help -[P] but, uh, evilly![B][C]") ]])
    fnCutscene([[ Append("Vicks:[E|Neutral] What does your friend look like?[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] Let's see...[P] She's blue, has five fingers, is shaped like a ball, and is coming at your face at 100kph.[B][C]") ]])
    fnCutscene([[ Append("Wedge:[E|Neutral] Did you just describe a fist?[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Gun] HIIYYAAAHHHH!!!!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    
    --ACTION MOVIE!
    fnCutscene([[ AL_SetProperty("Mandated Music Intensity", 60.0) ]])
    fnCutsceneMoveFace("Sammy", 34.25, 33.50, 1, 0, 1.50)
    fnCutsceneMove("Sammy", 35.75, 33.50, 2.00)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
    fnCutsceneBlocker()
    fnCutsceneMoveFace("GolemA", 36.25, 32.50, 0, 1, 2.50)
    fnCutsceneMove("Sammy", 36.25, 33.50, 2.00)
    fnCutsceneBlocker()
    fnCutsceneSetFrame("GolemA", "Wounded")
    fnCutsceneBlocker()
    fnCutsceneWait(35)
    fnCutsceneBlocker()
    fnCutsceneMove("Sammy", 36.25, 34.50, 2.00)
    fnCutsceneFace("Sammy", 1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(35)
    fnCutsceneBlocker()
    fnCutsceneMove("Sammy", 37.25, 34.50, 2.00)
    fnCutsceneMoveFace("GolemB", 39.25, 34.50, -1, 0, 2.00)
    fnCutsceneBlocker()
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneMoveFace("GolemB", 37.75, 34.50, -1, 0, 2.00)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
    fnCutsceneMoveFace("Sammy", 35.25, 34.50, 1, 0, 2.00)
    fnCutsceneMoveFace("GolemB", 37.25, 34.50, -1, 0, 2.00)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneMoveFace("GolemB", 38.25, 34.50, -1, 0, 1.00)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneWait(35)
    fnCutsceneBlocker()
    fnCutsceneMoveFace("GolemB", 36.25, 34.50, -1, 0, 2.00)
    fnCutsceneBlocker()
    fnCutsceneMoveFace("Sammy", 35.25, 35.50, 0, -1, 2.00)
    fnCutsceneMoveFace("GolemB", 34.25, 34.50, -1, 0, 2.00)
    fnCutsceneBlocker()
    fnCutsceneMoveFace("Sammy", 35.25, 34.50, -1, 0, 2.00)
    fnCutsceneBlocker()
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneMoveFace("Sammy", 34.75, 34.50, -1, 0, 0.50)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
    fnCutscene([[ Append("Vicks:[E|Neutral] Ack![P] She's got me![B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] Knock knock![B][C]") ]])
    fnCutscene([[ Append("Vicks:[E|Neutral] Who's there?[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] Neck snap![B][C]") ]])
    fnCutscene([[ Append("Vicks:[E|Neutral] You can't neck snap a golem, lug nut.[P] You'll just - [P][CLEAR]") ]])
    fnCutscene([[ Append("Almond:[E|Gun] ACTION NECK SNAP!!!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Action.
    fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
    fnCutsceneSetFrame("GolemB", "Wounded")
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Sammy", 40.25, 35.50)
    fnCutsceneFace("Sammy", 0, 1)
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Mandated Music Intensity", 20.0) ]])
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] (OW OW OW I THINK I BROKE MY HAND PUNCHING THAT METAL GIRL!!![P] THE BONE IS PRACTICALLY POWDER!)[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] Ha ha![P] Your martial arts are weak even if your superior metal bodies are much stronger![B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] I barely even broke a sweat![P] Better luck next time, evildoers![B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] (There had better not be a next time or I'm going to strangle the director!)") ]])
    fnCutsceneBlocker()

-- |[Wit]|
elseif(sResultName == "Wit") then
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iEncounterA", "N", 2.0)
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Sammy] (The old Almond trademark wit.[P] This will give them something to think about -[P] literally and figuratively!)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Sammy", 35.25, 33.50)
    fnCutsceneFace("Sammy", 1, 0)
    fnCutsceneBlocker()
    fnCutsceneFace("GolemA", -1, 0)
    fnCutsceneFace("GolemB", -1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ AL_SetProperty("Mandated Music Intensity", 60.0) ]])
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Golem", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemB", "Neutral") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] Hey, you're evil robots, right?[P] Decided to work for Evil Corp and all that stuff?[B][C]") ]])
    fnCutscene([[ Append("Vicks:[E|Neutral] Sure are![P] Hey, are you looking to join?[B][C]") ]])
    fnCutscene([[ Append("Wedge:[E|Neutral] Benefits are great, and the pay isn't half bad despite being part-time.[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] I just wanted to ask what's so evil about following the rules and doing what you're told in exchange for money.[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] If anything, that seems like what good guys do.[B][C]") ]])
    fnCutscene([[ Append("Wedge:[E|Neutral] Well...[B][C]") ]])
    fnCutscene([[ Append("Vicks:[E|Neutral] You see...[B][C]") ]])
    fnCutscene([[ Append("Vicks:[E|Neutral] We may not be evil at each individual second of the day, but we're working for an evil cause.[B][C]") ]])
    fnCutscene([[ Append("Wedge:[E|Neutral] Yeah![P] If we didn't move these barrels and punch in on time, our boss couldn't do really evil stuff![B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] Have you considered maybe punching in but not working?[P] That'd be pretty evil.[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] Or complaining unnecessarily to your Lord Golem.[P] Or not converting rogue humans when you detect them.[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Sexy] (...[P] This script bites...)[B][C]") ]])
    fnCutscene([[ Append("Vicks:[E|Neutral] No way...[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] What I'm saying is, you two aren't evil.[P] Your boss is, but you aren't.[B][C]") ]])
    fnCutscene([[ Append("Vicks:[E|Neutral] ...[B][C]") ]])
    fnCutscene([[ Append("Wedge:[E|Neutral] She's right![P] Oh no, she's totally right![B][C]") ]])
    fnCutscene([[ Append("Wedge:[E|Neutral] We're not evil![P] We're just accessories to evil![P] Our job is a sham![B][C]") ]])
    fnCutscene([[ Append("Vicks:[E|Neutral] Don't say that![P] We're super-evil![B][C]") ]])
    fnCutscene([[ Append("Wedge:[E|Neutral] You pet bunnies in the Biolabs and hum showtunes when you're decontaminating your chassis![B][C]") ]])
    fnCutscene([[ Append("Wedge:[E|Neutral] I -[P] I love my tandem unit, always work hard and show up on time, and give my Lord Golem useful feedback on work conditions![B][C]") ]])
    fnCutscene([[ Append("Wedge:[E|Neutral] We're not evil![P] We're good guys![B][C]") ]])
    fnCutscene([[ Append("Vicks:[E|Neutral] But -[P] but...[B][C]") ]])
    fnCutscene([[ Append("Vicks:[E|Neutral] You're right.[P] We've been good guys this whole time.[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] You were just in denial about evil.[P] You wanted the sexy fun time evil...[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Sexy] Well take one look at this bod and tell me that good guys don't have sexy fun times too.[B][C]") ]])
    fnCutscene([[ Append("Vicks:[E|Neutral] She's right.[B][C]") ]])
    fnCutscene([[ Append("Wedge:[E|Neutral] Good guys wear swimsuits and have a pleasing colour scheme![B][C]") ]])
    fnCutscene([[ Append("Wedge:[E|Neutral] That's it![P] We're renouncing our bad guy status as of this moment.[B][C]") ]])
    fnCutscene([[ Append("Vicks:[E|Neutral] From now on, we're all about petting bunnies, following the rules, and working hard with little expectation of reward.[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] Welcome aboard.[P] And you know what the best part is?[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Gun] ...[P] The good guys always win![B][C]") ]])
    fnCutscene([[ Append("Wedge:[E|Neutral] Yeah![B][C]") ]])
    fnCutscene([[ Append("Vicks:[E|Neutral] We believe in you, Agent Almond![P] Go get em!") ]])
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Sammy", 35.25, 35.50)
    fnCutsceneBlocker()
    fnCutsceneFace("GolemA", 1, 0)
    fnCutsceneFace("GolemB", 0, -1)
    fnCutsceneMove("Sammy", 40.25, 35.50)
    fnCutsceneFace("Sammy", 0, 1)
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Mandated Music Intensity", 20.0) ]])
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] Agent Almond does it again.[P] It's a shame too, because this is getting too easy.[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] In this crazy-mixed up world we live in, there's just less and less of a place for wild action and explosions.[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] Talking things out and listening to one another is the way that us good guys solve our problems.[P] Violence never really solves anything.[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] ...[P] But listen to this old dinosaur rambling about the changing world.[P] I better find that evidence and prove my innocence!") ]])
    fnCutsceneBlocker()
    
-- |[Dance]|
elseif(sResultName == "Dance") then
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iEncounterA", "N", 3.0)
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Sammy] (All right![P] Let's show these metal girls how to boogie!)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Sammy", 34.25, 33.50)
    fnCutsceneFace("Sammy", 1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Golem", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemB", "Neutral") ]])
    fnCutscene([[ Append("Almond:[E|Gun] Hey, evil goons!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(35)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneFace("GolemA", -1, 0)
    fnCutsceneMove("GolemB", 36.25, 34.50)
    fnCutsceneFace("GolemB", -1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Golem", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemB", "Neutral") ]])
    fnCutscene([[ Append("Wedge:[E|Neutral] You think she's talking to us?[B][C]") ]])
    fnCutscene([[ Append("Vicks:[E|Neutral] Excuse me, but we are not goons.[B][C]") ]])
    fnCutscene([[ Append("Vicks:[E|Neutral] I have an advanced degree in hench-woman-ry, and wrote my thesis on cackling in time to your boss' evil laughter.[B][C]") ]])
    fnCutscene([[ Append("Wedge:[E|Neutral] Wow, really?[P] I just showed up the day they opened the secret facility.[B][C]") ]])
    fnCutscene([[ Append("Vicks:[E|Neutral] Our on-the-job evil-training program is top notch, so you are anything but some regular goon.[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] Just shut up and watch this!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dancing!
    local ciTicksPerFrame = 8
    local ciMoveIncrement37 = 1.00 / (3.00 * 7.00)
    local p = 0.0
    fnCutscene([[ AL_SetProperty("Mandated Music Intensity", 60.0) ]])
    for i = 1, 5, 1 do
        for i = 0, 7, 1 do
            fnCutsceneSetFrame("Sammy", "DanceA" .. i)
            fnCutsceneWait(ciTicksPerFrame)
            fnCutsceneBlocker()
        end
    end
    for i = 1, 3, 1 do
        for i = 0, 7, 1 do
            fnCutsceneTeleport("Sammy", 34.25 + p, 33.50)
            fnCutsceneSetFrame("Sammy", "DanceA" .. i)
            fnCutsceneWait(ciTicksPerFrame)
            fnCutsceneBlocker()
            p = p + ciMoveIncrement37
        end
    end
    fnCutsceneFace("GolemA", 0, 1)
    fnCutsceneFace("GolemB", 0, -1)
    
    for i = 1, 2, 1 do
        for i = 0, 7, 1 do
            fnCutsceneSetFrame("Sammy", "DanceB" .. i)
            fnCutsceneWait(ciTicksPerFrame)
            fnCutsceneBlocker()
        end
    end
    p = 0
    for i = 1, 3, 1 do
        for i = 0, 7, 1 do
            fnCutsceneTeleport("Sammy", 35.25 - p, 33.50)
            fnCutsceneSetFrame("Sammy", "DanceB" .. i)
            fnCutsceneWait(ciTicksPerFrame)
            fnCutsceneBlocker()
            p = p + ciMoveIncrement37
        end
    end
    fnCutsceneFace("GolemA", -1, 0)
    fnCutsceneFace("GolemB", -1, 0)
    for i = 1, 3, 1 do
        for i = 0, 7, 1 do
            fnCutsceneSetFrame("Sammy", "DanceA" .. i)
            fnCutsceneWait(ciTicksPerFrame)
            fnCutsceneBlocker()
        end
    end
    p = 0
    for i = 1, 3, 1 do
        for i = 0, 7, 1 do
            fnCutsceneTeleport("Sammy", 34.25 - p, 33.50)
            fnCutsceneSetFrame("Sammy", "DanceB" .. i)
            fnCutsceneWait(ciTicksPerFrame)
            fnCutsceneBlocker()
            p = p + ciMoveIncrement37
        end
    end
    fnCutsceneSetFrame("Sammy", "Null")
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Golem", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemB", "Neutral") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] Ooh aah ooh![P] What do you think?[B][C]") ]])
    fnCutscene([[ Append("Vicks:[E|Neutral] ...[B][C]") ]])
    fnCutscene([[ Append("Wedge:[E|Neutral] ...[B][C]") ]])
    fnCutscene([[ Append("Vicks:[E|Neutral] HQ?[P] Yeah -[P] yeah it's me.[P] We're out moving the barrels -[P] yeah, listen.[B][C]") ]])
    fnCutscene([[ Append("Vicks:[E|Neutral] There's a blue moth girl in a bikini dancing out here.[B][C]") ]])
    fnCutscene([[ Append("Vicks:[E|Neutral] ...[B][C]") ]])
    fnCutscene([[ Append("Vicks:[E|Neutral] Well yes, that's what I said.[P] Why did you say it back to me?[B][C]") ]])
    fnCutscene([[ Append("Vicks:[E|Neutral] ...[B][C]") ]])
    fnCutscene([[ Append("Vicks:[E|Neutral] Stop laughing![P] This is serious![P] She could be a spy![B][C]") ]])
    fnCutscene([[ Append("Vicks:[E|Neutral] HQ?[P] HQ![P] Hey, don't put me on speaker phone![P] Just -[P] damn it![P][CLEAR]") ]])
    fnCutscene([[ Append("Wedge:[E|Neutral] Uh, this might take a while.[P] You might just want to keep going.[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] Once again, the power of dance wins the day![B][C]") ]])
    fnCutscene([[ Append("Vicks:[E|Neutral] HQ?[P] HQ!?[P] AAARRRGGHH!!!") ]])
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Mandated Music Intensity", 20.0) ]])


--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
