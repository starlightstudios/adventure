-- |[Encounter B]|
--Evil with a computer!

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sResultName = LM_GetScriptArgument(0)

--Always clean up dialogue.
WD_SetProperty("Hide")

-- |[Action]|
if(sResultName == "Action") then
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iEncounterB", "N", 1.0)
    
    --Disable collision.
    TA_ChangeCollisionFlag("GolemC", false)
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Sammy] (Let's see...[P] Stealth is on the menu today!)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Sammy", 65.25, 30.50)
    fnCutsceneBlocker()
    fnCutsceneMove("Sammy", 65.25, 30.00, 0.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
    fnCutscene([[ Append("Almond:[E|Gun] Freeze, creep![B][C]") ]])
    fnCutscene([[ Append("Golem:[E|Neutral] Oh dear, it seems I have been captured.[B][C]") ]])
    fnCutscene([[ Append("Golem:[E|Neutral] Or have I?[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] You have![B][C]") ]])
    fnCutscene([[ Append("Golem:[E|Neutral] ...[B][C]") ]])
    fnCutscene([[ Append("Golem:[E|Neutral] Or have I![B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] The answer is still yes![B][C]") ]])
    fnCutscene([[ Append("Golem:[E|Neutral] (Okay someone missed her cue.[P] Blast it![P] Improvisation is part of the craft!)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutscene([[ AL_SetProperty("Mandated Music Intensity", 60.0) ]])
    fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
    fnCutsceneFace("GolemC", 0, 1)
    fnCutsceneMoveFace("Sammy", 65.25, 31.50, 0, -1, 2.00)
    fnCutsceneBlocker()
    fnCutsceneWait(35)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
    fnCutscene([[ Append("Almond:[E|Sexy] Ouch![B][C]") ]])
    fnCutscene([[ Append("Golem:[E|Neutral] Come, dear child, you face the way of the iron fist![B][C]") ]])
    fnCutscene([[ Append("Golem:[E|Neutral] Which is a real martial art![P] Really![B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] Yeah -[P] well -[P] *Action* is a martial art, too![P] Hyah![B][C]") ]])
    fnCutscene([[ Append("Golem:[E|Neutral] En garde!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMoveFace("Sammy", 63.25, 30.50, 1, 0, 2.00)
    fnCutsceneMoveFace("GolemC", 64.25, 30.50, -1, 0, 2.00)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneMoveFace("Sammy", 63.75, 30.50, 1, 0, 0.50)
    fnCutsceneMoveFace("GolemC", 64.75, 30.50, -1, 0, 0.50)
    fnCutsceneBlocker()
    fnCutsceneWait(35)
    fnCutsceneBlocker()
    fnCutsceneMoveFace("Sammy", 63.25, 30.50, 1, 0, 0.50)
    fnCutsceneMoveFace("GolemC", 64.25, 30.50, -1, 0, 0.50)
    fnCutsceneBlocker()
    fnCutsceneWait(35)
    fnCutsceneBlocker()
    fnCutsceneMoveFace("Sammy", 63.75, 30.50, 1, 0, 2.00)
    fnCutsceneBlocker()
    fnCutsceneMoveFace("Sammy", 64.25, 30.50, 1, 0, 2.00)
    fnCutsceneMoveFace("GolemC", 64.75, 30.50, -1, 0, 2.00)
    fnCutsceneBlocker()
    fnCutsceneMoveFace("Sammy", 65.25, 29.50, 1, 1, 2.00)
    fnCutsceneFace("GolemC", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneMoveFace("Sammy", 65.25, 29.00, 0, 1, 2.00)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutsceneMoveFace("Sammy", 64.25, 28.00, 0, 1, 0.50)
    fnCutsceneMoveFace("GolemC", 64.25, 30.50, 0, -1, 0.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneMoveFace("Sammy", 63.25, 28.00, 0, 1, 0.50)
    fnCutsceneMoveFace("GolemC", 63.25, 30.50, 0, -1, 0.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneMoveFace("Sammy", 64.25, 28.00, 0, 1, 0.50)
    fnCutsceneMoveFace("GolemC", 64.25, 30.50, 0, -1, 0.50)
    fnCutsceneBlocker()
    fnCutsceneWait(75)
    fnCutsceneBlocker()
    fnCutsceneMoveFace("GolemC", 64.25, 28.70, 0, -1, 2.50)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] Arrrghh![B][C]") ]])
    fnCutscene([[ Append("Golem:[E|Neutral] Now it is I that have you, do-gooder![B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] Well, it was a good fight...[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] But you made one mistake![B][C]") ]])
    fnCutscene([[ Append("Golem:[E|Neutral] Huh?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Action!
    fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
    fnCutsceneMoveFace("GolemC", 64.25, 29.00, 0, -1, 2.00)
    fnCutsceneBlocker()
    fnCutsceneMoveFace("Sammy", 64.25, 29.50, 0, 1, 2.00)
    fnCutsceneMoveFace("GolemC", 64.25, 30.50, 0, -1, 2.00)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] Some moths give up -[P] and some moths never do![P] Rawwwrrr![B][C]") ]])
    fnCutscene([[ Append("Golem:[E|Neutral] Oh I get it, that's like a play on the title of the videogr -[P] Waaahhh!!!!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Throw her!
    fnCutsceneFace("Sammy", 1, 1)
    fnCutsceneMoveFace("GolemC", 65.25, 29.50, -1, 0, 1.00)
    fnCutsceneBlocker()
    fnCutsceneFace("Sammy", 1, -1)
    fnCutsceneMoveFace("GolemC", 64.25, 28.50, 0, 1, 1.00)
    fnCutsceneBlocker()
    fnCutsceneFace("Sammy", -1, -1)
    fnCutsceneMoveFace("GolemC", 63.25, 29.50, 1, 0, 1.00)
    fnCutsceneBlocker()
    fnCutsceneFace("Sammy", -1, 1)
    fnCutsceneMoveFace("GolemC", 64.25, 30.50, 1, 0, 1.00)
    fnCutsceneBlocker()
    
    --Faster!
    fnCutsceneFace("Sammy", 1, 1)
    fnCutsceneMoveFace("GolemC", 65.25, 29.50, -1, 0, 1.50)
    fnCutsceneBlocker()
    fnCutsceneFace("Sammy", 1, -1)
    fnCutsceneMoveFace("GolemC", 64.25, 28.50, 0, 1, 1.50)
    fnCutsceneBlocker()
    fnCutsceneFace("Sammy", -1, -1)
    fnCutsceneMoveFace("GolemC", 63.25, 29.50, 1, 0, 1.50)
    fnCutsceneBlocker()
    fnCutsceneFace("Sammy", -1, 1)
    fnCutsceneMoveFace("GolemC", 64.25, 30.50, 1, 0, 1.50)
    fnCutsceneBlocker()
    
    --Faster!!!
    fnCutsceneFace("Sammy", 1, 1)
    fnCutsceneMoveFace("GolemC", 65.25, 29.50, -1, 0, 2.50)
    fnCutsceneBlocker()
    fnCutsceneFace("Sammy", 1, -1)
    fnCutsceneMoveFace("GolemC", 64.25, 28.50, 0, 1, 2.50)
    fnCutsceneBlocker()
    fnCutsceneFace("Sammy", -1, -1)
    fnCutsceneMoveFace("GolemC", 63.25, 29.50, 1, 0, 2.50)
    fnCutsceneBlocker()
    fnCutsceneFace("Sammy", -1, 1)
    fnCutsceneMoveFace("GolemC", 64.25, 30.50, 1, 0, 2.50)
    fnCutsceneBlocker()
    
    --THROW!
    fnCutsceneSetFrame("GolemC", "Wounded")
    fnCutsceneMoveFace("GolemC", 64.25, 34.50, 1, 0, 2.50)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
    fnCutsceneMoveFace("GolemC", 64.25, 36.50, 1, 0, 0.30)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Sammy walks over.
    fnCutsceneMove("Sammy", 64.25, 35.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ AL_SetProperty("Mandated Music Intensity", 20.0) ]])
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] *pant*[P] *pant*[P] Couldn't make it easy, could you?[B][C]") ]])
    fnCutscene([[ Append("Golem:[E|Neutral] Owww...[P] I am vanquished...[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] Where's the evidence that will exonerate me!?[P] Talk![B][C]") ]])
    fnCutscene([[ Append("Golem:[E|Neutral] I don't know, I swear![P] It's probably in the secret room in our secret base![B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] Secret room, huh?[B][C]") ]])
    fnCutscene([[ Append("Golem:[E|Neutral] I don't know how to get in, but I know it's in there.[P] Please, have mercy![B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Sexy] You're lucky I'm a good guy, and good guys don't finish off beaten enemies.[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] You're going to be arrested and stand trial for all your evil crimes.[B][C]") ]])
    fnCutscene([[ Append("Golem:[E|Neutral] Oh no![B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] Now, I had better go see about getting into that secret base!") ]])
    fnCutsceneBlocker()
    
    --Enable collision.
    fnCutscene([[ TA_ChangeCollisionFlag("GolemC", true) ]])
    fnCutsceneBlocker()

-- |[Wit]|
elseif(sResultName == "Wit") then
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iEncounterB", "N", 2.0)
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Sammy] (Maybe it's time to use Agent Almond's Agent Smarts!)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Sammy", 64.25, 29.50)
    fnCutsceneBlocker()
    fnCutsceneFace("Sammy", 1, 0)
    fnCutsceneFace("GolemC", -1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ AL_SetProperty("Mandated Music Intensity", 60.0) ]])
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] Hey buddy.[B][C]") ]])
    fnCutscene([[ Append("Golem:[E|Neutral] Hey b -[P] hey![P] You look like that agent that everyone is always talking about![B][C]") ]])
    fnCutscene([[ Append("Golem:[E|Neutral] Yeah![P] Agent Almond![P] She's a moth girl![B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] Moth girl?[P] Do I look like a moth to you?[B][C]") ]])
    fnCutscene([[ Append("Golem:[E|Neutral] Yes?[P] You do?[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] Sheesh, shows what you know about biology.[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] Moths are human-sized bipedal hopping mammals with digitigrade legs that carry their young in pouches on their tummies.[B][C]") ]])
    fnCutscene([[ Append("Golem:[E|Neutral] Really?[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] And do I look like I have digitigrade legs or carry my young in a pouch?[B][C]") ]])
    fnCutscene([[ Append("Golem:[E|Neutral] Erm, no.[B][C]") ]])
    fnCutscene([[ Append("Golem:[E|Neutral] But wait, if a moth is that -[P] then what are you?[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] Well obviously I'm a kangaroo girl, stupid.[P] Do they teach you nothing in evil school?[B][C]") ]])
    fnCutscene([[ Append("Golem:[E|Neutral] Oh no![P] I'm sorry![B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] Sheesh, you have no idea how offensive it is to be mislabelled like that.[B][C]") ]])
    fnCutscene([[ Append("Golem:[E|Neutral] No, no![P] What can I do to make it up to you?[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Sexy] Well...[B][C]") ]])
    fnCutscene([[ Append("Golem:[E|Neutral] I really didn't mean to offend you![B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] I was looking for some information, about Agent Almond, coincidentally.[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] Apparently us Evil Corp baddies had some but I really wanted to see it.[B][C]") ]])
    fnCutscene([[ Append("Golem:[E|Neutral] Huh.[P] I didn't see it myself, but it'd probably be in the secret room in our secret base over there.[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] There's a secret room?[B][C]") ]])
    fnCutscene([[ Append("Golem:[E|Neutral] Yep.[P] I don't know how you get in it, but I'm sure if you ask nicely someone will help you out.[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] Sorry for getting off on the wrong foot like that.[P] You seem like a nice and evil robot.[B][C]") ]])
    fnCutscene([[ Append("Golem:[E|Neutral] Yeah, you're a pretty cool and evil kangaroo, too.[P] See you around!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Sammy", 64.25, 34.50)
    fnCutsceneFace("GolemC", 0, -1)
    fnCutscene([[ AL_SetProperty("Mandated Music Intensity", 20.0) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] (That had no right working as well as it did...[P] What an idiot!)") ]])
    fnCutsceneBlocker()
    
-- |[Dance]|
elseif(sResultName == "Dance") then
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iEncounterB", "N", 3.0)
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Sammy] Cue the music![P] It's time for that famous Almond Dance!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutscene([[ AL_SetProperty("Mandated Music Intensity", 60.0) ]])
    fnCutsceneMove("Sammy", 63.25, 29.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dance.
    local ciTicksPerFrame = 8
    local ciMoveIncrement37 = 1.00 / (3.00 * 7.00)
    local p = 0.0
    for i = 1, 4, 1 do
        for i = 0, 7, 1 do
            fnCutsceneSetFrame("Sammy", "DanceA" .. i)
            fnCutsceneWait(ciTicksPerFrame)
            fnCutsceneBlocker()
        end
    end
    fnCutsceneFace("GolemC", -1, 0)
    for i = 1, 3, 1 do
        for i = 0, 7, 1 do
            fnCutsceneSetFrame("Sammy", "DanceA" .. i)
            fnCutsceneWait(ciTicksPerFrame)
            fnCutsceneBlocker()
        end
    end
    fnCutsceneSetFrame("Sammy", "Null")
    fnCutsceneFace("Sammy", 1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Golem:[VOICE|Golem] Excuse me, but what the hell are you doing?[B][C]") ]])
    fnCutscene([[ Append("Almond:[VOICE|Sammy] Dancing![P] Care to join me?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("GolemC", 0, 1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Golem:[VOICE|Golem] (What is this feeling within me?)[B][C]") ]])
    fnCutscene([[ Append("Golem:[VOICE|Golem] (I -[P] I must boogie![P] I must boogie down!)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("GolemC", 65.25, 30.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneMoveFace("GolemC", 65.75, 30.50, 0, 1, 0.50)
    fnCutsceneMoveFace("GolemC", 65.25, 30.50, 1, 0, 0.50)
    fnCutsceneMoveFace("GolemC", 64.75, 30.50, 0, -1, 0.50)
    fnCutsceneMoveFace("GolemC", 65.25, 30.50, -1, 0, 0.50)
    fnCutsceneMoveFace("GolemC", 65.75, 30.50, 0, 1, 0.50)
    fnCutsceneMoveFace("GolemC", 65.25, 30.50, 1, 0, 0.50)
    fnCutsceneMoveFace("GolemC", 64.75, 30.50, 0, -1, 0.50)
    fnCutsceneMoveFace("GolemC", 65.25, 30.50, -1, 0, 0.50)
    fnCutsceneMoveFace("GolemC", 65.75, 30.50, 0, 1, 0.50)
    fnCutsceneMoveFace("GolemC", 65.25, 30.50, 1, 0, 0.50)
    fnCutsceneMoveFace("GolemC", 64.75, 30.50, 0, -1, 0.50)
    fnCutsceneMoveFace("GolemC", 65.25, 30.50, -1, 0, 0.50)
    fnCutsceneMoveFace("GolemC", 65.75, 30.50, 0, 1, 0.50)
    fnCutsceneMoveFace("GolemC", 65.25, 30.50, 1, 0, 0.50)
    fnCutsceneMoveFace("GolemC", 64.75, 30.50, 0, -1, 0.50)
    fnCutsceneMoveFace("GolemC", 65.25, 30.50, -1, 0, 0.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Golem:[VOICE|Golem] I am dancing![P] I am dancing![P] I feel the power of dance![B][C]") ]])
    fnCutscene([[ Append("Almond:[VOICE|Sammy] Pretty good -[P] but try this move![P] You thrust your pelvis -[P] hwah!") ]])
    fnCutsceneBlocker()
    
    local h = 0
    local r = 0.20
    local o = 1
    local iDirX = 0
    local iDirY = 1
    for i = 1, 5, 1 do
        for i = 0, 7, 1 do
            fnCutsceneSetFrame("Sammy", "DanceB" .. i)
            fnCutsceneMoveFace("GolemC", 65.25 + h, 30.50, iDirX, iDirY, 0.50)
            fnCutsceneWait(ciTicksPerFrame)
            fnCutsceneBlocker()
            
            --Direction changer.
            o = o + 1
            if(o < 4) then
                iDirX = 0
                iDirY = 1
            elseif(o < 8) then
                iDirX = 1
                iDirY = 0
            elseif(o < 12) then
                iDirX = 0
                iDirY = -1
            elseif(o < 15) then
                iDirX = 1
                iDirY = 0
            else
                o = 0
            end
            
            --Position changer.
            h = h + r
            if(h >= 1.0) then
                r = r * -1.0
            elseif(h <= -1.0) then
                r = r * -1.0
            end
        end
    end
    for i = 1, 4, 1 do
        for i = 0, 7, 1 do
            fnCutsceneSetFrame("Sammy", "DanceA" .. i)
            fnCutsceneMoveFace("GolemC", 65.25 + h, 30.50, iDirX, iDirY, 0.50)
            fnCutsceneWait(ciTicksPerFrame)
            fnCutsceneBlocker()
            
            --Direction changer.
            o = o + 1
            if(o < 4) then
                iDirX = 0
                iDirY = 1
            elseif(o < 8) then
                iDirX = 1
                iDirY = 0
            elseif(o < 12) then
                iDirX = 0
                iDirY = -1
            elseif(o < 15) then
                iDirX = 1
                iDirY = 0
            else
                o = 0
            end
            
            --Position changer.
            h = h + r
            if(h >= 1.0) then
                r = r * -1.0
            elseif(h <= -1.0) then
                r = r * -1.0
            end
        end
    end
    fnCutsceneSetFrame("Sammy", "Null")
    fnCutsceneFace("Sammy", 1, 0)
    fnCutsceneFace("GolemC", -1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
    fnCutscene([[ Append("Almond:[E|Sexy] Phew![P] Nothing like a sexy dance to get rid of excess energy![B][C]") ]])
    fnCutscene([[ Append("Golem:[E|Neutral] Wow![P] My life has been so empty![B][C]") ]])
    fnCutscene([[ Append("Golem:[E|Neutral] I dedicated myself to evil deeds and maniacal laughter, but really I needed the thrusting of hips and jiggling of boobs![B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] Yeah, everyone says that.[P] I have that effect on people.[B][C]") ]])
    fnCutscene([[ Append("Golem:[E|Neutral] How can I repay you for this epiphany?[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] I happen to be looking for evidence on Agent Almond.[P] You know where it might be?[B][C]") ]])
    fnCutscene([[ Append("Golem:[E|Neutral] Hmm, probably in the secret base over there.[P] Evil Corp likes to keep secret stuff in secret places.[B][C]") ]])
    fnCutscene([[ Append("Golem:[E|Neutral] In fact, it's probably in the secret room in the secret base.[P] Secret secret, I've got a secret.[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] Do you know how I get into that secret room?[B][C]") ]])
    fnCutscene([[ Append("Golem:[E|Neutral] No, but I'm sure someone inside will.[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] Thank you very much-o missus robot-o,[P] for helping me escape to where I need to go.[B][C]") ]])
    fnCutscene([[ Append("Golem:[E|Neutral] No problem!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Sammy", 64.25, 34.50)
    fnCutsceneMove("GolemC", 65.25, 28.50)
    fnCutsceneFace("GolemC", 0, -1)
    fnCutscene([[ AL_SetProperty("Mandated Music Intensity", 20.0) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] (All right![P] Time to get in that secret base!)") ]])
    fnCutsceneBlocker()


--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
