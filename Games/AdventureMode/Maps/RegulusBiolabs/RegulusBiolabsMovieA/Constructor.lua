-- |[Constructor]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "RegulusBiolabsMovieA"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[Standard]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music. Uses layering.
    local iSawMovieIntro = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawMovieIntro", "N")
    if(iSawMovieIntro == 1.0) then
        AL_SetProperty("Music", "LAYER|SomeMothsDo")
        AL_SetProperty("Mandated Music Intensity", 20.0)
    else
        AL_SetProperty("Music", "Null")
    end
    
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation("RegulusBiolabsMovieA")
	
	-- |[Spawn NPCS Here]|
	--NPCs set to spawn here will be available for post-exec cutscenes.

	-- |[Lights]|
    --AL_SetProperty("Activate Lights")

-- |[Post-Exec]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	-- |[Spawn NPCs Here]|
	--If an entity needs to spawn after a post-exec cutscene, put them here.
    fnSpawnNPCPattern("Golem", "A", "J")
    
    --Give all golems the wounded frame.
	EM_PushEntity("GolemA")
        TA_SetProperty("Add Special Frame", "Wounded","Root/Images/Sprites/Special/GolemSlave|Wounded")
    DL_PopActiveObject()
	EM_PushEntity("GolemB")
        TA_SetProperty("Add Special Frame", "Wounded","Root/Images/Sprites/Special/GolemSlave|Wounded")
    DL_PopActiveObject()
	EM_PushEntity("GolemC")
        TA_SetProperty("Add Special Frame", "Wounded","Root/Images/Sprites/Special/GolemSlave|Wounded")
    DL_PopActiveObject()
	EM_PushEntity("GolemD")
        TA_SetProperty("Add Special Frame", "Wounded","Root/Images/Sprites/Special/GolemSlave|Wounded")
    DL_PopActiveObject()
	EM_PushEntity("GolemE")
        TA_SetProperty("Add Special Frame", "Wounded","Root/Images/Sprites/Special/GolemSlave|Wounded")
    DL_PopActiveObject()
	EM_PushEntity("GolemF")
        TA_SetProperty("Add Special Frame", "Wounded","Root/Images/Sprites/Special/GolemSlave|Wounded")
        for i = 0, 7, 1 do
            TA_SetProperty("Add Special Frame", "DanceG" .. i, "Root/Images/Sprites/Special/SammyDanceG|" .. i)
            TA_SetProperty("Add Special Frame", "DanceH" .. i, "Root/Images/Sprites/Special/SammyDanceH|" .. i)
        end
    DL_PopActiveObject()
	EM_PushEntity("GolemG")
        TA_SetProperty("Add Special Frame", "Wounded","Root/Images/Sprites/Special/GolemSlave|Wounded")
        for i = 0, 7, 1 do
            TA_SetProperty("Add Special Frame", "DanceG" .. i, "Root/Images/Sprites/Special/SammyDanceG|" .. i)
            TA_SetProperty("Add Special Frame", "DanceH" .. i, "Root/Images/Sprites/Special/SammyDanceH|" .. i)
        end
    DL_PopActiveObject()
	EM_PushEntity("GolemH")
        TA_SetProperty("Add Special Frame", "Wounded","Root/Images/Sprites/Special/GolemSlave|Wounded")
        for i = 0, 7, 1 do
            TA_SetProperty("Add Special Frame", "DanceG" .. i, "Root/Images/Sprites/Special/SammyDanceG|" .. i)
            TA_SetProperty("Add Special Frame", "DanceH" .. i, "Root/Images/Sprites/Special/SammyDanceH|" .. i)
        end
    DL_PopActiveObject()
	EM_PushEntity("GolemI")
        TA_SetProperty("Add Special Frame", "Wounded","Root/Images/Sprites/Special/GolemSlave|Wounded")
    DL_PopActiveObject()
	EM_PushEntity("GolemJ")
        TA_SetProperty("Add Special Frame", "Wounded","Root/Images/Sprites/Special/GolemSlave|Wounded")
    DL_PopActiveObject()
    
    -- |[Special NPCs]|
    --Spawns when repeating the movie.
    local iBiolabsMovieRepeat = VM_GetVar("Root/Variables/Chapter5/Scenes/iBiolabsMovieRepeat", "N")
    if(iBiolabsMovieRepeat == 1.0) then
        fnStandardNPCByPosition("SkipMovie")
    end
end
