-- |[Root]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then

    -- |[Name Resolve]|
    --Resolve the name of the calling entity.
    local sActorName = TA_GetProperty("Name")
	
    --Subdivide for all the entities sharing this script.
    if(sActorName == "GolemA") then
        
        --Variables.
        local iEncounterA = VM_GetVar("Root/Variables/Chapter5/Scenes/iEncounterA", "N")
        
        --Action:
        if(iEncounterA == 1.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Oof...[P] I am thoroughly defeated...") ]])
        
        --Wit:
        elseif(iEncounterA == 2.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] I'm going to work very hard and not complain![P] Being a good guy is great!") ]])
        
        --Dance:
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] HQ![P] No, this is real![P] Stop laughing and send backup![P] Arrrgghh!") ]])
        end
    
    elseif(sActorName == "GolemB") then
        
        --Variables.
        local iEncounterA = VM_GetVar("Root/Variables/Chapter5/Scenes/iEncounterA", "N")
        
        --Action:
        if(iEncounterA == 1.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] My neck was snapped![P] How can this be![P] Also I'm still alive because this is a family-friendly videograph.") ]])
        
        --Wit:
        elseif(iEncounterA == 2.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] You know what?[P] I'm going to tell my tandem unit I love her!") ]])
        
        --Dance:
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Hey, don't take this the wrong way, but when we say your dancing is unbelievable, well, it's a compliment.[P] Okay?") ]])
        
        end
    
    elseif(sActorName == "GolemC") then
        
        --Variables.
        local iEncounterB = VM_GetVar("Root/Variables/Chapter5/Scenes/iEncounterB", "N")
        
        --Action:
        if(iEncounterB == 1.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Oof...[P] I hope the justice system goes easy on a first-time evil offender...") ]])
        
        --Wit:
        elseif(iEncounterB == 2.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] I was just looking at some evil stuff, don't mind me.[P] You go ahead.") ]])
        
        --Dance:
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] I shall heretofore dedicate my synthetic life to the art of dance.[P] Dance![P] I love it!") ]])
        
        end
    
    elseif(sActorName == "GolemD") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] I am just so caught up in these barrels that I wouldn't even notice if a spy was right behind me.[P] Nope, not at all!") ]])
    
    
    elseif(sActorName == "GolemE") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] You came all the way over here just to talk to me?[P] Wow.[P] We may be on opposite sides, but that really matters to me.[B][C]") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] You -[P] you care?[P] About little old evil me?[P] I...[P] I don't know what to say...") ]])
    
    elseif(sActorName == "GolemF") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Ah![P] Ow![P] I got some rubber stuck in my silicon teeth, I tried to get it out but punched myself![B][C]") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] ...[P] I don't know why I told you that.[P] Okay.[P] Bye!") ]])
    
    elseif(sActorName == "GolemG") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] What a nice day it is today.[P] A good for evil!") ]])
    
    elseif(sActorName == "GolemH") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] My circuits are still a bit frazzled about those sandwiches but working together is awesome!") ]])
    
    elseif(sActorName == "GolemI") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] (The director said to stare at the fish, 'but evilly'.[P] How the hell do you do that?[P] Scowl?[P] Grrr![P] I am evil, fear me, fish!)") ]])
    
    elseif(sActorName == "GolemJ") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] You can just go right in...[P] Amphibian Research is happy to serve all visitors!") ]])
        
    --Rilmani that allows skipping the movie.
    elseif(sActorName == "SkipMovie") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Rilmani:[VOICE|Maram] Hello.[P] Would you like to exit out of the movie without completing it?[B]") ]])
        fnDecisionSet(LM_GetCallStack(0), {{"No", "Cancel"}, {"Yes", "ExitMovie"}})
    end

-- |[ ================================== Exit Movie Decisions ================================== ]|
--Close dialogue.
elseif(sTopicName == "Cancel") then
	WD_SetProperty("Hide")

--Exit the movie.
elseif(sTopicName == "ExitMovie") then
	WD_SetProperty("Hide")
    
    --Transition.
    fnCutscene([[ AL_BeginTransitionTo("RegulusBiolabsRaijuRanchC", "FORCEPOS:36.0x23.0x0") ]])
    fnCutsceneBlocker()
end
