-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
if(sObjectName == "ToMovieB") then
    
    --Variables.
    local iMovieHasDoorCode = VM_GetVar("Root/Variables/Chapter5/Scenes/iMovieHasDoorCode", "N")
    if(iMovieHasDoorCode == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("[SOUND|World|AutoDoorFail] Evil access denied.[P] Please input evil password.") ]])
        fnCutsceneBlocker()
    else
        AudioManager_PlaySound("World|AutoDoorOpen")
        AL_BeginTransitionTo("RegulusBiolabsMovieB", "FORCEPOS:13.5x10.0x0")
    end

-- |[Objects]|
elseif(sObjectName == "TerminalA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Sammy] (Become pure evil with this one weird trick![P] The Agency hates her!)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Sammy] ('I know, I know, but I did it for the pure evil of it.[P] Those kind of experiments don't come along every day.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Sammy] (There's a note written next to the terminal::[P] 'Wipe this terminal, we need it for the videograph we're shooting tomorrow.[P] Remove this note when you're done.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Sammy] (Wait...)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Sammy] ('Top 10 Barrel Rivalries' and 'Top 10 Barrel Betrayals'.[P] Really popular videographs on barreltube.net on the station network.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Sammy] (Let's see what's on this terminal...[P] oh, a pop-up advertisement.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Sammy] (Damn you, Evil Corp![P]  Will your villainy know no bounds?)") ]])
    fnCutsceneBlocker()
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
