-- |[PuzzleFile]|
--Contains the puzzle descriptions based on which answer rolled correct.

-- |[Arguments]|
--Argument Listing:
-- 0: sPasswordGuess - What the player typed.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sPasswordGuess = LM_GetScriptArgument(0)

-- |[Variables]|
local iAmphibianCorrectSpecies = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianCorrectSpecies", "N")

--Setup.
WD_SetProperty("Hide")
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    
-- |[Cancel]|
if(sObjectName == "Null") then
    fnCutscene([[ Append("Christine:[E|Neutral] Nevermind.[P] I think we should look for more clues first.") ]])
    return
end

-- |[Construct the Input String]|
local sString = "WD_SetProperty(\"Append\", \"Christine:[E|Neutral] Okay, let's try " .. sPasswordGuess .. "...[B][C]\")"
fnCutscene(sString)

--Comparison
local sLowercaseGuess = string.lower(sPasswordGuess)
local sCorrectAnswer = "dogs"

-- |[Axolotl]|
if(iAmphibianCorrectSpecies == 1.0) then
    sCorrectAnswer = "axolotl"

-- |[Olm]|
elseif(iAmphibianCorrectSpecies == 2.0) then
    sCorrectAnswer = "olm"
    
-- |[Blackbelly Salamander]|
elseif(iAmphibianCorrectSpecies == 3.0) then
    sCorrectAnswer = "blackbelly"
    
-- |[Red-Bellied Newt]|
elseif(iAmphibianCorrectSpecies == 4.0) then
    sCorrectAnswer = "redbelly"
    
-- |[Bufo bufo]|
elseif(iAmphibianCorrectSpecies == 10.0) then
    sCorrectAnswer = "bufo"

-- |[Lemur frog]|
elseif(iAmphibianCorrectSpecies == 11.0) then
    sCorrectAnswer = "lemur"

-- |[Spadefoot Toad]|
elseif(iAmphibianCorrectSpecies == 12.0) then
    sCorrectAnswer = "spadefoot"

-- |[Moor Frog]|
elseif(iAmphibianCorrectSpecies == 13.0) then
    sCorrectAnswer = "moor"

-- |[Banded Bullfrog]|
elseif(iAmphibianCorrectSpecies == 14.0) then
    sCorrectAnswer = "banded"

-- |[Iwokramae Caecilia]|
elseif(iAmphibianCorrectSpecies == 20.0) then
    sCorrectAnswer = "iwokramae"

-- |[Annalatus Caecilia]|
elseif(iAmphibianCorrectSpecies == 21.0) then
    sCorrectAnswer = "annulatus"
    
-- |[Rubber Eel]|
elseif(iAmphibianCorrectSpecies == 22.0) then
    sCorrectAnswer = "rubber"
end
--io.write("Correct answer is " .. sCorrectAnswer .. " you answered " .. sLowercaseGuess .. "\n")

-- |[Correct Answer]|
if(sCorrectAnswer == sLowercaseGuess) then
    VM_SetVar("Root/Variables/Chapter5/Scenes/iAmphibianGotAuthcode", "N", 1.0)
    fnCutscene([[ Append("Christine:[E|Happy] Got it![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] [SOUND|World|Keycard](Got the amphibian research facility access codes!)[B][C]") ]])
    fnCutscene([[ Append("55:[E|Smirk] Well done.[P] Let us proceed.") ]])

-- |[Debug Bypass]|
--Testers can input this string to bypass this section.
elseif(sLowercaseGuess == "debugbypass") then
    VM_SetVar("Root/Variables/Chapter5/Scenes/iAmphibianGotAuthcode", "N", 1.0)
    fnCutscene([[ Append("Christine:[E|Happy] Hey, I entered a debug passphrase and it worked![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] [SOUND|World|Keycard](Got the amphibian research facility access codes!)[B][C]") ]])
    fnCutscene([[ Append("55:[E|Smirk] Well done.[P] Let us proceed.") ]])

-- |[WRONG!]|
else

    --Increment the failure counter.
    local iAmphibianFailureCount = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianFailureCount", "N")
    VM_SetVar("Root/Variables/Chapter5/Scenes/iAmphibianFailureCount", "N", iAmphibianFailureCount + 1)

    --First failure.
    if(iAmphibianFailureCount == 0.0) then
        fnCutscene([[ Append("Terminal:[E|Neutral] [SPOOKY]WRONG ANSWER, DINGALING!!!![NOSPOOKY][B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Angry] Grrrr...[B][C]") ]])
    elseif(iAmphibianFailureCount == 1.0) then
        fnCutscene([[ Append("Terminal:[E|Neutral] [SPOOKY]STILL DIDN'T GET IT, DINGUS!!![NOSPOOKY][B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Angry] Blast it![B][C]") ]])
    elseif(iAmphibianFailureCount == 2.0) then
        fnCutscene([[ Append("Terminal:[E|Neutral] [SPOOKY]YOU SUCK.[NOSPOOKY][B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Angry] Arrggh![B][C]") ]])
    elseif(iAmphibianFailureCount == 3.0) then
        fnCutscene([[ Append("Terminal:[E|Neutral] [SPOOKY]YOU GUESS PASSWORDS LIKE YOU DRESS -[P] INSUFFICIENTLY![NOSPOOKY][B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Angry] Why you uncouth little - [P][CLEAR]") ]])
    elseif(iAmphibianFailureCount == 4.0) then
        fnCutscene([[ Append("Terminal:[E|Neutral] [SPOOKY]ARE YOU SCREWING UP JUST TO SEE THESE MESSAGES? BECAUSE YOU SUCK AT GUESSING PASSWORDS![NOSPOOKY][B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Angry] Stupid terminal![B][C]") ]])
    elseif(iAmphibianFailureCount == 5.0) then
        fnCutscene([[ Append("Terminal:[E|Neutral] [SPOOKY]HOW MANY TIMES HAVE YOU FAILED? I'M GETTING IMPRESSED.[NOSPOOKY][B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Angry] Grrragghh![B][C]") ]])
    else
        fnCutscene([[ Append("Terminal:[E|Neutral] [SPOOKY]PATHETIC![NOSPOOKY][B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Angry] Mgrrrmgrrmgrrr!![B][C]") ]])
    end
    fnCutscene([[ Append("55:[E|Neutral] The correct password will be the code associated with the species we need.[P] We should look for clues.") ]])


end
