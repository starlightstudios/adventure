-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Variables]|
local iListCommands = 0

-- |[Exits]|
if(sObjectName == "ToBetaD") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    local iAmphibianPipesReleased = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianPipesReleased", "N")
    if(iAmphibianPipesReleased == 0.0) then
        AL_BeginTransitionTo("RegulusBiolabsBetaD", "FORCEPOS:42.5x15.0x0")
    else
        AL_BeginTransitionTo("RegulusBiolabsBetaMeltedD", "FORCEPOS:42.5x15.0x0")
    end
    
elseif(sObjectName == "ToAmphibianBLft") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsAmphibianB", "FORCEPOS:26.5x17.0x0")
    
elseif(sObjectName == "ToAmphibianBRgt") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsAmphibianB", "FORCEPOS:33.0x15.0x0")
    
-- |[Objects]|
elseif(sObjectName == "TerminalA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (There's a list of visitor hours along with upcoming events.)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "TerminalB") then

    --Variables.
    local iAmphibianSaw55Joke   = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianSaw55Joke", "N")
    local iAmphibianGotAuthcode = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianGotAuthcode", "N")
    local iSX399JoinsParty      = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
    
    --First time starting the console:
    if(iAmphibianSaw55Joke == 0.0) then
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iAmphibianSaw55Joke", "N", 1.0)
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutscene([[ Append("Christine:[E|Neutral] Someone left this terminal logged in, 55![B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] The greatest vulnerability in any system is between the keyboard and the chair.[B][C]") ]])
        if(iAmphibianGotAuthcode == 0.0) then
            fnCutscene([[ Append("55:[E|Neutral] It was not an administrator account, but with some work we can figure out the access codes to the building covertly.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Happy] Is this what you do when you're hacking?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Yes.[P] Identify flaws in the architecture and exploit them.[P] A logged-in terminal that is unsupervised is one such flaw.[B][C]") ]])
            if(iSX399JoinsParty == 1.0) then
                fnCutscene([[ Append("SX-399:[E|Neutral] Messing with computers isn't really my thing.[P] Just ask if you need something.[B][C]") ]])
            end
        else
            fnCutscene([[ Append("55:[E|Neutral] While there is likely some information on this terminal, we already have the building's authcodes.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Happy] Right, but this is hacking isn't it?[P] Wheee![B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Very few units consider hacking to be enjoyable.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] I like to think of myself as one of a kind.[B][C]") ]])
            if(iSX399JoinsParty == 1.0) then
                fnCutscene([[ Append("SX-399:[E|Neutral] Messing with computers isn't really my thing.[P] You two enjoy it.[B][C]") ]])
            end
        end

    --Repeats.
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutscene([[ Append("Christine:[E|Neutral] (Let's see if there's anything useful on the local hard drive...)[B][C]") ]])
    end
    
    --Decision setup.
    iListCommands = 1

elseif(sObjectName == "Emails") then
	WD_SetProperty("Hide")
	
    --Variables.
    local iAmphibianGotAuthcode = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianGotAuthcode", "N")
    local iAmphibianDecryptAttachment = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianDecryptAttachment", "N")
    
    --Flags.
    if(iAmphibianDecryptAttachment < 1.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iAmphibianDecryptAttachment", "N", 1.0)
    end
    
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ Append("Christine:[E|Smirk] Let's see what's in her outbox...[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] This email is encrypted, which is unusual.[P] Fortunately, it is a common pattern.[P] I will decrypt it for you.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] If the correct password is input, the email will appear as though it was entirely corrupted and lost.[P] This deflects suspicion.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] She probably didn't want her Lord Golem reading her emails.[P] Which means it's probably useful for us.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] 'Hey Elmira, sorry about missing our get-together the other day.[P] My Lord Golem had me clean out the frog habitats.[P] You know, again.[P] Because she hates me.'[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] 'Let me make it up to you, okay?[P] We can have our next -[P] meeting -[P] in her office, ideally on her desk.'[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] 'I put the authcode to the labs in the attachment to this email.[P] The attachment password is the common name of your favourite amphibian species.[P] See you tomorrow after work!'[B][C]") ]])
    if(iAmphibianGotAuthcode == 0.0 and iAmphibianDecryptAttachment == 0.0) then
        fnCutscene([[ Append("Christine:[E|Smirk] So the authcodes are in that attachment.[P] Can you decode it, 55?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Not without additional information.[P] Though we could input the password.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Hmmm...[B][C]") ]])
    end
    
    --Decision setup.
    iListCommands = 1

elseif(sObjectName == "Photos") then
	WD_SetProperty("Hide")
    
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ Append("Christine:[E|Smirk] Two units posing in front of the lake in the Alpha Labs.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Adorable, but not relevant.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] At least we agree they're adorable![B][C]") ]])
    
    --Decision setup.
    iListCommands = 1

--Input password.
elseif(sObjectName == "Password") then
	WD_SetProperty("Hide")
    
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ Append("Christine:[E|Smirk] Okay, let's give the password a shot...") ]])
    local sScript = "\"" .. fnResolvePath() .. "/PasswordInput.lua" .. "\""
    fnCutscene(" WD_SetProperty(\"Activate String Entry\", " .. sScript .. ") ")
    fnCutsceneBlocker()

elseif(sObjectName == "SpeciesList") then
	WD_SetProperty("Hide")
    
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ Append("Christine:[E|Neutral] A list of the species in the public viewing area, and some information.[B][C]") ]])
    
    --Decision setup.
    iListCommands = 2

elseif(sObjectName == "Salamanders") then
	WD_SetProperty("Hide")
    
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ Append("Christine:[E|Smirk] Ooh, so many pretty pictures![P] 'Salamanders are lizard-like amphibians with distinct legs and a low body.'[B][C]") ]])
    
    --Decision setup.
    iListCommands = 3

elseif(sObjectName == "Frogs") then
	WD_SetProperty("Hide")
    
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ Append("Christine:[E|Neutral] Frogs and toads are so cute![P] 'Frogs and Toads are amphibians known for hopping, long tongues, and their ribbit calls.'[B][C]") ]])
    
    --Decision setup.
    iListCommands = 4

elseif(sObjectName == "Caecilians") then
	WD_SetProperty("Hide")
    
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ Append("Christine:[E|Neutral] Caecilians...[P] exotic and squirmy![P] 'The oft-forgotten third amphibian, caecilians are burrowers or swimmers that resemble snakes or worms.'[B][C]") ]])
    
    --Decision setup.
    iListCommands = 5

-- |[Species Descriptions]|
elseif(sObjectName == "Axolotl") then
	WD_SetProperty("Hide")
    
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ Append("Christine:[E|Neutral] The axolotl (Code AXOLOTL) is a species of salamander that is predominantly aquatic.[P] They do not have lungs, and retain their gills throughout life.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Sometimes called 'Walking Fish', axolotls are vital for our research here in Amphibian Genetics for their ability to regenerate lost limbs.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] When a limb or other non-essential body part is removed, the axolotl does not scar.[P] Instead, the lost part heals entirely.[P] It may even produce additional copies, leaving extra limbs.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] This allows researchers to hold genetic properties constant when performing invasive experiments.[P] With time, we hope to allow healing of all organic creatures using insights gained from the axolotls.[B][C]") ]])
    
    --Decision setup.
    iListCommands = 3
    
elseif(sObjectName == "Olm") then
	WD_SetProperty("Hide")
    
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ Append("Christine:[E|Neutral] The olm (Code OLM) is a species of salamander that live exclusively in underground caverns.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Living in caverns leaves the olm totally blind, but possessing an enhanced sensory suite in terms of smell and hearing.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Their bodies are very long with short limbs, and their skin is fleshy in appearance.[P] They have gained the affectionate nickname 'danger noodle' from our staff due to how similar they look to snakes.[P] Despite the name, they are totally harmless![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] If you wish to pet one of our olms, please be extremely gentle.[P] Like all organics, they are very fragile and sensitive.[B][C]") ]])
    
    --Decision setup.
    iListCommands = 3
    
elseif(sObjectName == "Blackbelly") then
	WD_SetProperty("Hide")
    
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ Append("Christine:[E|Neutral] The blackbelly salamander (Code BLACKBELLY) is a mostly aquatic salamander native to many terrestrial parts of Pandemonium.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Despite being an amphibian, the blackbelly rarely travels over land more than a few centimeters.[P] In fact, over 99pct of their lives are spent in the water, only leaving to forage for food.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Blackbelly salamander eggs require water to incubate correctly.[P] Attempts to create artificial incubators without water have failed, though the eggs enter a sort of stasis briefly until water returns, likely an adaptation for dry seasons.[B][C]") ]])
    
    --Decision setup.
    iListCommands = 3
    
elseif(sObjectName == "Redbelly") then
	WD_SetProperty("Hide")
    
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ Append("Christine:[E|Neutral] The red-bellied newt (Code REDBELLY) is a highly toxic amphibian noted for its black back scales and bright red tummy scales.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] If you wish to handle one, please wear gloves and wash your chassis -[P] they are toxic to organics![P] Please don't spread the toxins beyond the habitat.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] When threatened, the red-bellied newt deliberately exposes its belly to warn off predators.[P] Bright colors in the animal kingdom are an almost sure sign of poison -[P] predators, beware![B][C]") ]])
    
    --Decision setup.
    iListCommands = 3

elseif(sObjectName == "Bufo") then
	WD_SetProperty("Hide")
    
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ Append("Christine:[E|Neutral] The common Pandemonium toad (Code BUFO) are very common all across boreal biomes in eastern Pandemonium, specifically Argolis and Entreana.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Unlike other toxic species, the bufo toads are not brightly colored.[P] In fact, they are generally muddy browns.[P] Brief contact is not lethal, but extensive contact causes heart paralysis and death.[P] Handle with care, organic guests![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Even today, toads are often associate with witchcraft and magic.[P] The Arcane University would like to inform all visitors that toads possess no special arcane properties.[B][C]") ]])
    
    --Decision setup.
    iListCommands = 4

elseif(sObjectName == "Lemur") then
	WD_SetProperty("Hide")
    
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ Append("Christine:[E|Neutral] The lemur frog (Code LEMUR) is a small terrestrial amphibian found in tropical parts of Pandemonium.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Notably, their skin color is green during the day, but changes to brown at night -[P] the better to hide from predators.[P] They often spend the days hiding beneath leaves and emerge at night to hunt prey.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Lemur frogs are also extremely resistant to high temperatures.[P] Most amphibians lose moisture quickly, but the lemur frog's unique skin chemistry protects moisture loss and allows them to sun themselves for long periods.[B][C]") ]])
    
    --Decision setup.
    iListCommands = 4

elseif(sObjectName == "Spadefoot") then
	WD_SetProperty("Hide")
    
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ Append("Christine:[E|Neutral] The spadefoot toad (Code SPADEFOOT) is a small terrestrial amphibian of brownish coloration, native to boreal habitats.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] This is one of our 'burrowing' species, which prefers to dig into the soil in moist areas near water sources.[P] When the soil is sufficiently wet, they mate and lay eggs.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Interestingly, their tadpoles are known for cannibalism when their environments are stressed.[P] If the water threatens to dry or food is not sufficient, the tadpoles eat one another.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] This causes them to mature rapidly, allowing at least some individuals to survive despite the danger.[B][C]") ]])
    
    --Decision setup.
    iListCommands = 4

elseif(sObjectName == "Moor") then
	WD_SetProperty("Hide")
    
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ Append("Christine:[E|Neutral] The moor frog (Code MOOR) is a small brownish frog with an unspotted belly and typical frog characteristics, such as webbed feet and horizontal pupils.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Moor frogs have an enormous range, inhabiting dry, wet, warm, and cool areas.[P] They can even survive in rough tundra.[P] Their relatively high range but low locomotion means there are many subspecies which have adapted to local conditions extensively.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] They are also the only species we have here in the public area which like to form breeding choruses.[P] Many frogs will get together and sing to attract mates simultaneously.[P] We will provide a notification if you would like to see one in action![B][C]") ]])
    
    --Decision setup.
    iListCommands = 4

elseif(sObjectName == "Banded") then
	WD_SetProperty("Hide")
    
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ Append("Christine:[E|Neutral] The banded bullfrog (Code BANDED) is often associated with gluttony due to its large body but its tiny head and narrow mouth.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Their appetites are voracious -[P] they can devour hundreds of ants per day.[P] Like many predators they are opportunistic, though we cultivate ants for feeding here in the Biolabs.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] When threatened, they puff their bodies up and secrete a mild toxin.[P] Not dangerous to larger organics, the usual rules of protection apply. Wash your chassis![B][C]") ]])
    
    --Decision setup.
    iListCommands = 4

elseif(sObjectName == "Iwokramae") then
	WD_SetProperty("Hide")
    
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ Append("Christine:[E|Neutral] The iwokramae caecilia (Code IWOKRAMAE) is a caecilia species of amphibian which notably has no lungs.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Caecilians are often mistaken for worms or snakes, depending on their size, but are in fact amphibians.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Native to tropical biomes, they are unfamiliar to most local humans because they burrow into the soil and live their lives there.[P] Many humans have no interactions with caecilia despite living very close to them every day![B][C]") ]])
    
    --Decision setup.
    iListCommands = 5

elseif(sObjectName == "Annalatus") then
	WD_SetProperty("Hide")
    
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ Append("Christine:[E|Neutral] The annulatus caecilia (Code ANNULATUS) is a caecilia species of amphibian which is rare among its genus -[P] it secretes toxins![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Toxins are secreted from its rectum as it burrows.[P] They are currently a subject of research in order to determine how they use these toxins.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Please use care when handling them, and remember to wash your chassis to prevent cross-contamination with other species.[B][C]") ]])
    
    --Decision setup.
    iListCommands = 5

elseif(sObjectName == "Rubber") then
	WD_SetProperty("Hide")
    
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ Append("Christine:[E|Neutral] The rubber eel (Code RUBBER) is a caecilia species of amphibian that is entirely aquatic in habitat.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Despite being aquatic, they still must surface to breathe.[P] They are often mistaken for eels, hence the nickname 'rubber eel'.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] We would like to assure our visitors that the Drone Units at work here have no genetic similarity with these amphibians despite a similar skin sheen and dearth of intelligence.[P] That is purely coincidental.[B][C]") ]])
    
    --Decision setup.
    iListCommands = 5

elseif(sObjectName == "Reset") then
	WD_SetProperty("Hide")
    --Decision setup.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    iListCommands = 1
    
elseif(sObjectName == "Exit") then
	WD_SetProperty("Hide")
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end

-- |[List Commands]|
--Base list.
if(iListCommands == 1) then
    
    --Variables.
    local iAmphibianDecryptAttachment = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianDecryptAttachment", "N")
    local iAmphibianGotAuthcode       = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianGotAuthcode", "N")
    
    fnCutscene([[ Append("Terminal: Listing commands...[BLOCK]") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Sent Emails\", " .. sDecisionScript .. ", \"Emails\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Photos\",  " .. sDecisionScript .. ", \"Photos\") ")
    if(iAmphibianDecryptAttachment == 1.0 and iAmphibianGotAuthcode == 0.0) then
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Password\",  " .. sDecisionScript .. ", \"Password\") ")
    end
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Specimen List\",  " .. sDecisionScript .. ", \"SpeciesList\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Exit\",  " .. sDecisionScript .. ", \"Exit\") ")
    fnCutsceneBlocker()

--Amphibians list.
elseif(iListCommands == 2) then
    
    fnCutscene([[ Append("Terminal: Listing categories...[BLOCK]") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Salamanders\", " .. sDecisionScript .. ", \"Salamanders\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Frogs and Toads\",  " .. sDecisionScript .. ", \"Frogs\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Caecilians\",  " .. sDecisionScript .. ", \"Caecilians\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Back\",  " .. sDecisionScript .. ", \"Reset\") ")
    fnCutsceneBlocker()

--Salamanders
elseif(iListCommands == 3) then
    
    fnCutscene([[ Append("Terminal: Listing salamanders...[BLOCK]") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Axolotl\", " .. sDecisionScript .. ", \"Axolotl\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Olm\",  " .. sDecisionScript .. ", \"Olm\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Blackbelly Salamander\",  " .. sDecisionScript .. ", \"Blackbelly\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Red-bellied Bewt\",  " .. sDecisionScript .. ", \"Redbelly\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Back\",  " .. sDecisionScript .. ", \"SpeciesList\") ")
    fnCutsceneBlocker()

--Frogs and Toads
elseif(iListCommands == 4) then
    
    fnCutscene([[ Append("Terminal: Listing frogs and toads...[BLOCK]") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Bufo Bufo\", " .. sDecisionScript .. ", \"Bufo\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Lemur Frog\", " .. sDecisionScript .. ", \"Lemur\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Spadefoot Toad\", " .. sDecisionScript .. ", \"Spadefoot\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Moor Frog\", " .. sDecisionScript .. ", \"Moor\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Banded Bullfrog\", " .. sDecisionScript .. ", \"Banded\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Back\",  " .. sDecisionScript .. ", \"SpeciesList\") ")
    fnCutsceneBlocker()

--Caecilians
elseif(iListCommands == 5) then
    
    fnCutscene([[ Append("Terminal: Listing caecilians...[BLOCK]") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Iwokramae Caecilia\", " .. sDecisionScript .. ", \"Iwokramae\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Annulatus Caecilia\", " .. sDecisionScript .. ", \"Annalatus\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Rubber Eel\", " .. sDecisionScript .. ", \"Rubber\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Back\",  " .. sDecisionScript .. ", \"SpeciesList\") ")
    fnCutsceneBlocker()
end
