-- |[Golem Boat Script]|
--Parallel execution script. Handles animating the evil golem's boat. Basically has a ticks-per-frame
-- wait timer and changes the boat's animation state based on this.

--Script Usage:
--This script controls the "EnemyBoat" entity and changes its special frame instance.

--Boat state variables:
-- 0: Moving
-- 1: Being blown up
-- 2: Blown up

--Variables.
local fTPF = 3.5

--Increment the global timer.
local iGolemGlobalTimer = VM_GetVar("Root/Variables/Temporary/Movie/iGolemGlobalTimer", "N")
VM_SetVar("Root/Variables/Temporary/Movie/iGolemGlobalTimer", "N", iGolemGlobalTimer + 1)

--Offset the timer slightly.
iGolemGlobalTimer = iGolemGlobalTimer + (fTPF * 5.0)

--Manually specify the global timer.
iGolemGlobalTimer = iGolemGlobalTimer + 1

--Figure out how the player's boat is meant to animate.
local iGolemBoatState = VM_GetVar("Root/Variables/Temporary/Movie/iGolemBoatState", "N")

--Compute the frame.
local sFrameName = "Normal"
local iFrameNumber = math.floor(iGolemGlobalTimer / fTPF)

--Normal movement.
if(iGolemBoatState == 0.0) then
    sFrameName = "Normal"
    iFrameNumber = iFrameNumber % 9
    
--Being blown up.
elseif(iGolemBoatState == 1.0) then
    sFrameName = "Destroying"
    if(iFrameNumber >= 24) then
        VM_SetVar("Root/Variables/Temporary/Movie/iGolemBoatState", "N", 2.0)
        VM_SetVar("Root/Variables/Temporary/Movie/iGolemGlobalTimer", "N", 0.0)
        sFrameName = "Destroyed"
        iFrameNumber = 0
    end
    
    local iGolemPlayedSplash = VM_GetVar("Root/Variables/Temporary/Movie/iGolemPlayedSplash", "N")
    if(iFrameNumber == 11 and iGolemPlayedSplash == 0.0) then
        VM_SetVar("Root/Variables/Temporary/Movie/iGolemPlayedSplash", "N", 1.0)
        AudioManager_PlaySound("World|BigSplash")
    end

--Destroyed.
else
    sFrameName = "Destroyed"
    iFrameNumber = iFrameNumber % 8
end

--Set.
if(EM_Exists("EnemyBoat") == true) then
    EM_PushEntity("EnemyBoat")
        TA_SetProperty("Set Special Frame", sFrameName .. iFrameNumber)
    DL_PopActiveObject()
end
