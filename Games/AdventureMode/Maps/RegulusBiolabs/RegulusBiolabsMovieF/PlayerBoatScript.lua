-- |[Player Boat Script]|
--Parallel execution script. Handles animating the player's boat. Basically has a ticks-per-frame
-- wait timer and changes the boat's animation state based on this.

--Script Usage:
--This script controls the "PlayerBoat" entity and changes its special frame instance.

--Boat state variables:
-- 0: Moving
-- 1: Guns up
-- 2: Shooting

--Variables.
local fTPF = 3.0

--Increment the global timer.
local iSammyGlobalTimer = VM_GetVar("Root/Variables/Temporary/Movie/iSammyGlobalTimer", "N")
VM_SetVar("Root/Variables/Temporary/Movie/iSammyGlobalTimer", "N", iSammyGlobalTimer + 1)

--Manually specify the global timer.
iSammyGlobalTimer = iSammyGlobalTimer + 1

--Figure out how the player's boat is meant to animate.
local iSammyBoatState = VM_GetVar("Root/Variables/Temporary/Movie/iSammyBoatState", "N")

--Compute the frame. There are 9 frames for all animations.
local sFrameName = "Normal"
local iFrameNumber = (math.floor(iSammyGlobalTimer / fTPF) % 9)

--Normal movement.
if(iSammyBoatState == 0.0) then
    sFrameName = "Normal"
    
--Guns up.
elseif(iSammyBoatState == 1.0) then
    sFrameName = "Guns"

--Shooting.
else
    sFrameName = "Shoot"
end

--Set.
if(EM_Exists("PlayerBoat") == true) then
    EM_PushEntity("PlayerBoat")
        TA_SetProperty("Set Special Frame", sFrameName .. iFrameNumber)
    DL_PopActiveObject()
end
