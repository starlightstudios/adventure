-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
-- |[Objects]|
if(sObjectName == "ToDatacoreBRgt") then

    --Organic checker:
    local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
    if(sChristineForm == "Human" or sChristineForm == "Raiju") then
        
        --Variables:
        local iHasLatexForm         = VM_GetVar("Root/Variables/Global/Christine/iHasLatexForm", "N")
        local iHasDarkmatterForm    = VM_GetVar("Root/Variables/Global/Christine/iHasDarkmatterForm", "N")
        local iHasEldritchForm      = VM_GetVar("Root/Variables/Global/Christine/iHasEldritchForm", "N")
        local iHasElectrospriteForm = VM_GetVar("Root/Variables/Global/Christine/iHasElectrospriteForm", "N")
        local iHasSteamDroidForm    = VM_GetVar("Root/Variables/Global/Christine/iHasSteamDroidForm", "N")
        local iHasDollForm          = VM_GetVar("Root/Variables/Global/Christine/iHasDollForm", "N")
        local iSawRaijuIntro        = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawRaijuIntro", "N")
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        fnCutscene([[ Append("Door:[VOICE|Narrator] Warning:: Depressurized area ahead.[P] Secure organic subjects for transport before exiting.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] (Better change to an inorganic form before I go outside...)[BLOCK]") ]])

        --Decision script is this script. It must be surrounded by quotes.
        local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
        fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
        if(iHasDarkmatterForm == 1.0) then
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Darkmatter\",  " .. sDecisionScript .. ", \"TFToDarkmatter\") ")
        end
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Golem\", " .. sDecisionScript .. ", \"TFToGolem\") ")
        if(iHasLatexForm == 1.0) then
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Latex Drone\",  " .. sDecisionScript .. ", \"TFToLatex\") ")
        end
        if(iHasEldritchForm == 1.0 and iSawRaijuIntro == 1.0) then
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Dreamer\",  " .. sDecisionScript .. ", \"TFToEldritch\") ")
        end
        if(iHasElectrospriteForm == 1.0) then
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Electrosprite\",  " .. sDecisionScript .. ", \"TFToElectrosprite\") ")
        end
        if(iHasSteamDroidForm == 1.0) then
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Steam Droid\",  " .. sDecisionScript .. ", \"TFToSteam\") ")
        end
        if(iHasDollForm == 1.0) then
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Doll\",  " .. sDecisionScript .. ", \"TFToDoll\") ")
        end
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Cancel\", " .. sDecisionScript .. ", \"NoCancel\") ")
        fnCutsceneBlocker()
    
    --Normal case:
    else
        AudioManager_PlaySound("World|AutoDoorOpen")
        AL_BeginTransitionTo("RegulusBiolabsDatacoreB", "FORCEPOS:32.0x4.0x0")
    end
    
    
elseif(sObjectName == "TerminalA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Did you know?[P] Over seventy-five percent of workplace accidents are caused by this one weird software glitch!')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Even at a time like this, clickbait is omnipresent...)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (An email from an anonymous sender::[P] 'They're going to find you.[P] They know where you are.[P] It won't be long.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (There are logs from the Epsilon habitat...[P] but they're all garbled.[P] Must be classified, but I don't have time to decode it.)") ]])
    fnCutsceneBlocker()
    
-- |[Form Changers]|
elseif(sObjectName == "TFToGolem") then
	WD_SetProperty("Hide")
    
    --Run the cutscene.
    LM_ExecuteScript(gsRoot .. "Chapter 5/Scenes/100 Transform/Transform_ChristineToGolem/Scene_Begin.lua")
    fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutscene([[ AL_BeginTransitionTo("RegulusBiolabsDatacoreB", "FORCEPOS:32.0x4.0x0") ]])
    
elseif(sObjectName == "TFToLatex") then
	WD_SetProperty("Hide")
    
    --Run the cutscene.
    LM_ExecuteScript(gsRoot .. "Chapter 5/Scenes/100 Transform/Transform_ChristineToLatexDrone/Scene_Begin.lua")
    fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutscene([[ AL_BeginTransitionTo("RegulusBiolabsDatacoreB", "FORCEPOS:32.0x4.0x0") ]])
    
elseif(sObjectName == "TFToDarkmatter") then
	WD_SetProperty("Hide")
    
    --Run the cutscene.
    LM_ExecuteScript(gsRoot .. "Chapter 5/Scenes/100 Transform/Transform_ChristineToDarkmatter/Scene_Begin.lua")
    fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutscene([[ AL_BeginTransitionTo("RegulusBiolabsDatacoreB", "FORCEPOS:32.0x4.0x0") ]])
    
elseif(sObjectName == "TFToEldritch") then
	WD_SetProperty("Hide")
    
    --Run the cutscene.
    LM_ExecuteScript(gsRoot .. "Chapter 5/Scenes/100 Transform/Transform_ChristineToEldritch/Scene_Begin.lua")
    fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutscene([[ AL_BeginTransitionTo("RegulusBiolabsDatacoreB", "FORCEPOS:32.0x4.0x0") ]])
    
elseif(sObjectName == "TFToElectrosprite") then
	WD_SetProperty("Hide")
    
    --Run the cutscene.
    LM_ExecuteScript(gsRoot .. "Chapter 5/Scenes/100 Transform/Transform_ChristineToElectrosprite/Scene_Begin.lua")
    fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutscene([[ AL_BeginTransitionTo("RegulusBiolabsDatacoreB", "FORCEPOS:32.0x4.0x0") ]])
    
elseif(sObjectName == "TFToSteam") then
	WD_SetProperty("Hide")
    
    --Run the cutscene.
    LM_ExecuteScript(gsRoot .. "Chapter 5/Scenes/100 Transform/Transform_ChristineToSteamDroid/Scene_Begin.lua")
    fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutscene([[ AL_BeginTransitionTo("RegulusBiolabsDatacoreB", "FORCEPOS:32.0x4.0x0") ]])

elseif(sObjectName == "TFToDoll") then
	WD_SetProperty("Hide")
    
    --Run the cutscene.
    LM_ExecuteScript(gsRoot .. "Chapter 5/Scenes/100 Transform/Transform_ChristineToDoll/Scene_Begin.lua")
    fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutscene([[ AL_BeginTransitionTo("RegulusBiolabsDatacoreB", "FORCEPOS:32.0x4.0x0") ]])
    
elseif(sObjectName == "NoCancel") then
	WD_SetProperty("Hide")
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
