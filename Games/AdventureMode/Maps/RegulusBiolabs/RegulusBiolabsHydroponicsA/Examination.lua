-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
-- |[Objects]|
if(sObjectName == "ToAlphaC") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsAlphaC", "FORCEPOS:5.5x24.0x0")
elseif(sObjectName == "ToHydroponicsB") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsHydroponicsB", "FORCEPOS:21.5x26.0x0")
    
elseif(sObjectName == "ConsoleA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A list of personnel in the hydroponics division.[P] All of the names are listed as 'evacuated'.[P] That's a relief!)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "ConsoleB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A list of biological specimens, arrival dates, special instructions, and the like.[P] Not much of a discernable pattern.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ConsoleC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Looks like an experiment record, by an anonymous author.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Hypothesis::[P] Drone Units will receive an increase in motivator efficiency if specific colours are applied to their chassis.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Experiment::[P] Apply red dye to a Drone Unit, and order it to race the other Drone Units assigned to Hydroponics Research.[P] Do this when the Lord Units are otherwise engaged.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Results::[P] Unit dyed red came fifth out of six racers.[P] Curiously, all six Drone Units were convinced that the red racer had won the race, despite the obvious evidence otherwise.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Repeated trials yielded the same result.[P] The unit was not faster before or after being dyed.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Secondary Experiment Proposal::[P] Which chemical cleaner can most efficiently remove dye from a unit's chassis?)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ConsoleD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Looks like a log left by a researcher.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Now, I've looked at the personnel records.[P] Amazingly, nutrient requirements for the Raijus throughout Regulus City have tripled over the past three months.[P] Yet, Raiju Unit populations have increased by about ten percent.[P] Why the discrepancy?')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('I'm not trying to be subversive here, though I was verbally reprimanded by Command Unit 2856 when I asked if increased power production accounted for the difference.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('It's quite strange.[P] My hypothesis was that an increase in nutrient density might lead to higher voltages.[P] I was shouted at for even proposing it, and told not to waste time with the records.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('I'm also not stupid.[P] I know when I'm being told not to stick my olfactory sensors where they don't belong.[P] I dropped the issue.[P] I haven't heard anything since, which is the best possible news.')") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "ConsoleE") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (ATTENTION ALL UNITS.[P] EVACUATE HYDROPONICS LABS IMMEDIATELY.[P] PROCEED TO TRANSIT STATION OMICRON.[P] LEAVE PERSONAL POSSESSIONS BEHIND.[P] THANK YOU.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ConsoleF") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('I heard something big is going down at the new Epsilon Labs.[P] I want to ask my Lord Golem about it, maybe get some volunteer work done, but she was...[P] unusually candid.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('She looked straight at me and said not to ask about the Epsilon Labs.[P] She's never that serious, ever.[P] She told me to ignore all the rumours and just keep my head down.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('I know my Lord Golem cares about me very deeply.[P] It is good that she shows it by maintaining discipline over me.[P] But this was so different...')") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "ConsoleG") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Marcie got busted for her stupid drone experiment![P] What a socket-head!')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('She's lucky she got away with just a reprimand.[P] Honestly, why would putting red dye in the showers for the Drone Units do anything?[P] They're probably too dumb to even know what red is!')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ConsoleH") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Work orders for the unit assigned to this room.[P] Cleaning, watering plants, nothing interesting.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ConsoleI") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (DRONE UNIT LOG.[P] I AM WRITING A LOG.[P] MY LORD GOLEM TOLD ME I WROTE A GOOD LOG.[P] YAY!)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "ConsoleJ") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Finally got clearance from my Lord Unit to do my gravitation experiment, and then we get an evacuation order?[P] Why does fate hate me?')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ConsoleK") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A research paper attempting to tie the age of a dragon to their hoard size.[P] Interesting stuff, but statistically insignificant since not enough dragons can be located.[P] The researcher's requests for abductions to locate more dragons were all denied...)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ConsoleL") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The slave unit used her defragmentation terminal to record experimental results...)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Experiment EFE18::[P] Results indicate that playing music to the plants has not influenced growth patterns within statistical significance.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Experiment EFE19::[P] Recently asked to reconsider previous positions on playing music to plants.[P] Reorganized apparatus to use fewer automated inputs, 60pct of labour performed by slave units.[P] Growth rates now correlate with musical input.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Experiment EFE20::[P] Reorganized apparatus again.[P] Genre of music split between habitat subsets.[P] Classical and Synthwave produced notable growth increases.[P] Death Metal inhibits growth rates.[P] Investigate.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Experiment EFE21::[P] Reorganized staffing.[P] Determined Death Metal enthusiast slave unit was simply ill-equipped for caring for plants.[P] Reassigned unit to fabrication, growth rates levelled off.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ConsoleM") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('I managed to get my Lord Golem to approve the usage of Drone Units for trucking water through the hydroponics area in buckets, since the hoses are leaking.[P] Took long enough![P] Guess she was busy with something else.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Now I have no shortage of excuses to rub her and clean her off, every few minutes.[P] I just love how she feels...[P] So firm...[P] So taut...')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ConsoleO") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A log led by the Lord Golem assigned to hydroponics research...)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Our limited experimental evidence indicates that hydroponic cultivation, by itself, could feed the entire population of Pandemonium three times over, if we were to install a fusion reactor under the ice caps and convert the energy into light for photosynthesis.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('I suspect the harsh arctic environment will deter any sort of military assault by a hostile power, and the ice will provide hydrogen for fusion power.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('I recommended to the Head of Abductions this course of action, and she agreed.[P] She believes providing food to the mass of lower-class humans will win them to the Cause of Science quite handily.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('The challenge will be in maintaining soil fertility, as we are effectively exporting it around the planet.[P] I have requested resources to experiment with conversion of regolith into soil at an accelerated rate, but have yet to hear back from the Head of Research.[P] The Cause of Science will benefit greatly from my research, I am certain of it!')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ConsoleP") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Some emails on the Head of Hydroponics' terminal...)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('I'm very sorry to have to requisition your crop, but we need it for the latest cut.[P] We finally got it growing in the Epsilon Habitat![P] It's incredible!')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('You can come take a look whenever you're not busy, though I'm pretty sure you'll need to get clearance from the Head of Research.[P] She can be so stuffy sometimes.[P] I swear she's gotten angrier and angrier over the past few months.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Just be careful where you step, the stuff tends to self-organize around you.[P] I'm not sure if it has any practical applications, but who cares?[P] This stuff is just incredible!')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "OilmakerA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Some cheeky unit has written 'Crap' on all of the settings...[P] It doesn't smell that bad, does it?)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "OilmakerB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The Lord Golem here was putting organic plants in her oil for flavor?[P] Not a proper use of scientific resources, but it tastes like Earl Grey tea.[P] Can't fault her there!)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "NoteA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Whoever keeps vandalizing the oil maker::[P] We get the joke.[P] Stop it.[P] Our budget is already stretched as it is, we can't afford more flavours.[P] You aren't helping.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "NoteB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('ALL PERSONNEL MUST CLEAN ORGANIC CONTAMINANTS BEFORE RETURNING TO THEIR QUARTERS.[P] PLEASE DO NOT CROSS-CONTAMINATE SAMPLES IN HYDROPONICS WITH POLLEN FROM THE ALPHA LABS.[P] THANK YOU.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Window") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Ah, the surface of Regulus.[P] I can see the Datacore building in the distance...)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "RVD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (There's an evacuation order playing on the RVD.[P] Fortunately, it seems everyone got out okay.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Doll") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A Mini Ms. Seyton doll.[P] This Lord Golem has a refined taste!)") ]])
    fnCutsceneBlocker()
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
