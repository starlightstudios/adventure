-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
if(sObjectName == "Discussion") then
    
    --Repeat check.
    local iAmphibianUnlockDialogue = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianUnlockDialogue", "N")
    if(iAmphibianUnlockDialogue == 1.0) then return end
        
    --Flags.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iAmphibianUnlockDialogue", "N", 1.0)
    
    --Variables.
    local iSawMovieIntro   = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawMovieIntro", "N")
    local iSX399JoinsParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("Christine:[E|Neutral] Interesting.[P] A secret room?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Unlikely.[P] It is more likely that the public area was repurposed and this entrance was merely blocked off.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] As to why such an exotic trigger method was selected, rather than the simple use of an administrator's console, is not obvious.[B][C]") ]])
    if(iSawMovieIntro == 0.0) then
        fnCutscene([[ Append("Christine:[E|Neutral] I have no idea.[P] I just did random things when I heard a chirping noise.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Strange.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] Regardless, it seems there is equipment here for us to purloin.") ]])
    else
        fnCutscene([[ Append("Christine:[E|Blush] Uh yeah...[P] about that...[B][C]") ]])
        fnCutscene([[ Append("55:[E|Upset] Your body language suggests you are about to make a mistake.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Blush] Well...[P] Sophie and I may have watched a silly movie about a moth girl...[B][C]") ]])
        if(iSX399JoinsParty == 1.0) then
            fnCutscene([[ Append("55:[E|Neutral] This sounds very relevant.[B][C]") ]])
        else
            fnCutscene([[ Append("55:[E|Neutral] This sounds very relevant.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Smirk] Christine has good taste in videographs.[B][C]") ]])
        end
        fnCutscene([[ Append("Christine:[E|Blush] It was shot in the Biolabs.[P] In fact, some of it was recorded in this room.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] I see.[P] Excellent reconnaissance, Unit 771852.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Blush] You're not mad?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Upset] As I said, your body language suggested you were about to make a mistake.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Upset] That mistake is always assuming I will be upset by your...[P] lapses in judgement.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Uh, okay?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] I am not an authority figure, I am your partner.[P] I do not always agree with your choices, but you have a good instinct.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Please do not avoid telling me things because you are worried I will judge you harshly.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Wow![P] Okay, 55![P] Great![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] Wait a minute...[P] That sounds vaguely familiar...[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] ...[P] I borrowed it from episode 802 of 'All My Processors'.[B][C]") ]])
        if(iSX399JoinsParty == 1.0) then
            fnCutscene([[ Append("SX-399:[E|Neutral] All my whatnow?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] It's a show they record in Regulus City.[P] It has...[P] a lot of seasons...[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Smirk] And you watch it, 55?[B][C]") ]])
        else
            fnCutscene([[ Append("Christine:[E|Neutral] ...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] ...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Happy] You're a fan?[B][C]") ]])
        end
        fnCutscene([[ Append("55:[E|Neutral] I sometimes watch the episodes while waiting for replies during correspondence, yes.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Do not read any further into that than necessary.[B][C]") ]])
        if(iSX399JoinsParty == 1.0) then
            fnCutscene([[ Append("SX-399:[E|Flirt] Too late.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Happy] Reading way too far into it![B][C]") ]])
        else
            fnCutscene([[ Append("Christine:[E|Smirk] Suit yourself, 55.[B][C]") ]])
        end
        fnCutscene([[ Append("55:[E|Neutral] At any rate, there is equipment for us to purloin.[P] Let us proceed.") ]])
        
    end
    fnCutsceneBlocker()
end
