-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
-- |[Objects]|
if(sObjectName == "Terminal") then
    WD_SetProperty("Unlock Topic", "Biolabs_Poison", 1)
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('I understand the need to research defoliants, but why was such a large amount of dioxin produced?[P] What were you testing?')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('And now you dump it square in the middle of my research lab, in a bunch of steel barrels, and tell me not to touch them?[P] What am I supposed to do?[P] The organic wildlife doesn't listen to our orders, Unit 2856!')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Biological Services immediately reported loss of plant life as the barrels are leaching into the soil.[P] You're not considering using this on the people of Pandemonium, are you?[P] The Alraunes won't go anywhere near it!')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('It's fat soluble, which means it can get into nearly any organic's bloodstream and cause serious genetic damage.[P] I don't care how badly you want to destroy cropland in your hypothetical invasion scenario::[P] I won't stand for it.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('If I even hear rumours of further experiments with defoliants, I will be getting all the research units I know to sign pledges to refuse to work on them.[P] We don't care about reprimands.[P] We are scientists, not butchers.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "SignA") then
    WD_SetProperty("Unlock Topic", "Biolabs_Poison", 1)
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Defoliant Storage Site::[P] No organics without proper protective clothing.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "SignB") then
    WD_SetProperty("Unlock Topic", "Biolabs_Poison", 1)
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Report all container leaks and corrosion to a superior unit at once')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Boxes") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Boxes containing absorptive powders, for cleaning up spills.)") ]])
    fnCutsceneBlocker()
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
