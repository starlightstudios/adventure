-- |[Narissa's Dialogue]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Variables]|
local sChristineForm         = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
local iBiolabsMetNarissa     = VM_GetVar("Root/Variables/Chapter5/Scenes/iBiolabsMetNarissa", "N")
local iChristineLeadingParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iChristineLeadingParty", "N")

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
    
    --Setup.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Narissa", "Neutral") ]])
    
    --Christine variation:
    if(iChristineLeadingParty >= 1.0) then
    
        --First time meeting Narissa.
        if(iBiolabsMetNarissa == 0.0) then
            
            --Flag.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iBiolabsMetNarissa", "N", 1.0)
    
            --Human:
            if(sChristineForm == "Human" or sChristineForm == "Golem") then
                fnCutscene([[ Append("Raiju:[E|Neutral] You're unit 771852, aren't you.[B][C]") ]])
                fnCutscene([[ Append("Raiju:[E|Neutral] Don't bother denying it.[P] Everyone who knows what's up, knows what's up.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] I don't believe I've had the pleasure.[B][C]") ]])
                fnCutscene([[ Append("Narissa:[E|Neutral] Narissa.[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Smirk] Hello Narissa![P] I'm Unit 499323, but you can call me Sophie if you want.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] 771852, or Christine.[P] So, how do you know me?[B][C]") ]])
                fnCutscene([[ Append("Narissa:[E|Neutral] Relax, chuckles.[P] Your reputation gets around.[B][C]") ]])
                fnCutscene([[ Append("Narissa:[E|Neutral] Specifically, your pretty metal face should be all over the RVDs.[P] Am I right?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Are you referring to the recruitment videograph we made?[P] Are you a sympathizer?[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Smirk] You made a videograph?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] It's just 55 and I standing in front of a banner depicting a smashed killphrase plate.[P] We encourage everyone to support the rebels when they come.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] If our teams did their jobs, it'll be playing on every RVD in the city that has network access.[B][C]") ]])
                fnCutscene([[ Append("Narissa:[E|Neutral] Yeah, but I knew about it well before tonight.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] You didn't say you're a supporter.[B][C]") ]])
                fnCutscene([[ Append("Narissa:[E|Neutral] I'm not.[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Sad] You look on edge, Christine...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] Administration stooge, then, Narissa?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] No, you don't look the type.[B][C]") ]])
                fnCutscene([[ Append("Narissa:[E|Neutral] Is it the big dumb smile?[P] Sorry, I'm a Raiju.[P] Can't help it.[B][C]") ]])
                fnCutscene([[ Append("Narissa:[E|Neutral] No, 771852, I'm what we call a 'fixer'.[P] I know people who know people.[P] I can get things done, or acquired.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] Care to be specific?[B][C]") ]])
                fnCutscene([[ Append("Narissa:[E|Neutral] In my line of work?[P] Specific is called 'evidence'.[P] I don't do specific.[B][C]") ]])
                fnCutscene([[ Append("Narissa:[E|Neutral] There's a war brewing.[P] And when there's a war on, things become hard to find.[P] That's where I come in.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Oh, okay.[P] So you just want to sell things.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Play both sides, get rich.[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Neutral] And now you're smiling?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Well, why not?[P] We just found a smuggler![B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] But she's not a friend, Sophie.[B][C]") ]])
                fnCutscene([[ Append("Narissa:[E|Neutral] I don't pretend to be anything but what I am.[P] So, yes, I'm not your friend.[B][C]") ]])
                fnCutscene([[ Append("Narissa:[E|Neutral] I don't particularly care about your cause, or the Administration's.[P] But I do care about the color of your money.[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Neutral] ...[P] I don't get it?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Narissa here intends to sell her services to whichever side can afford to pay her.[P] Information, supplies, or maybe just bottles of booze.[P] Am I right?[B][C]") ]])
                fnCutscene([[ Append("Narissa:[E|Neutral] Bottles of booze, eh?[P] Good thinking.[P] With transport interrupted, organic citizens are going to need their fix.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] So happy to help.[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Neutral] But we're trying to free you.[P] Won't you help us?[B][C]") ]])
                fnCutscene([[ Append("Narissa:[E|Neutral] Girl, if I wanted off this rock, I could leave in an hour.[P] I know the right golems.[B][C]") ]])
                fnCutscene([[ Append("Narissa:[E|Neutral] I'm already free, and I prefer the administrators to not know that.[B][C]") ]])
                fnCutscene([[ Append("Narissa:[E|Neutral] So, during work hours, I'm just a big-titted dummy who screws everyone who gives her a tickle.[B][C]") ]])
                fnCutscene([[ Append("Narissa:[E|Neutral] But if you come in the off hours and say the right words, I can get you damn near anything.[B][C]") ]])
                fnCutscene([[ Append("Narissa:[E|Neutral] So, listen up.[P] You got spare work credits?[P] I can trade those for platina.[B][C]") ]])
                fnCutscene([[ Append("Narissa:[E|Neutral] Got platina?[P] I can sell you credits chips, or goodies I've 'found'.[P] You know.[P] Fell off the back of a tram.[B][C]") ]])
                fnCutscene([[ Append("Narissa:[E|Neutral] And if you've got spare junk, well, one robot's trash is another raiju's treasure.[P] Get me?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] So what have you got for us?[BLOCK]") ]])
            
            --Raiju:
            else
                fnCutscene([[ Append("Raiju:[E|Neutral] You're unit 771852, aren't you.[P] Good choice, becoming one of us.[B][C]") ]])
                fnCutscene([[ Append("Raiju:[E|Neutral] Though if half the stuff I've heard about you is true, that's a temporary feature.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] You seem to know a lot.[P] I don't believe I've had the pleasure.[B][C]") ]])
                fnCutscene([[ Append("Raiju:[E|Neutral] C'mon sweetie, act the part of the big-titted dumb raiju and smile![B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Blush] It is rather tempting, I admit...[B][C]") ]])
                fnCutscene([[ Append("Narissa:[E|Neutral] Name's Narissa, by the way.[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Smirk] Hello Narissa![P] I'm Unit 499323, but you can call me Sophie if you want.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] I prefer Christine, if we're being informal.[P] So, how do you know me?[B][C]") ]])
                fnCutscene([[ Append("Narissa:[E|Neutral] Relax, chuckles.[P] Your reputation gets around.[B][C]") ]])
                fnCutscene([[ Append("Narissa:[E|Neutral] Specifically, if you were metal right now, your face would be all over the RVDs.[P] Am I right?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Are you referring to the recruitment videograph we made?[P] Are you a sympathizer?[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Smirk] You made a videograph?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] It's just 55 and I standing in front of a banner depicting a smashed killphrase plate.[P] We encourage everyone to support the rebels when they come.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] If our teams did their jobs, it'll be playing on every RVD in the city that has network access.[B][C]") ]])
                fnCutscene([[ Append("Narissa:[E|Neutral] Yeah, but I knew about it well before tonight.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] You didn't say you're a supporter.[B][C]") ]])
                fnCutscene([[ Append("Narissa:[E|Neutral] I'm not.[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Sad] You look on edge, Christine...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] Administration stooge, then, Narissa?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] No, you don't look the type.[B][C]") ]])
                fnCutscene([[ Append("Narissa:[E|Neutral] Is it the big dumb smile?[P] Sorry, I'm a Raiju.[P] Can't help it.[B][C]") ]])
                fnCutscene([[ Append("Narissa:[E|Neutral] No, 771852, I'm what we call a 'fixer'.[P] I know people who know people.[P] I can get things done, or acquired.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] Care to be specific?[B][C]") ]])
                fnCutscene([[ Append("Narissa:[E|Neutral] In my line of work? Specific is called 'evidence'.[P] I don't do specific.[B][C]") ]])
                fnCutscene([[ Append("Narissa:[E|Neutral] There's a war brewing.[P] And when there's a war on, things become hard to find.[P] That's where I come in.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Oh, okay.[P] So you just want to sell things.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Play both sides, get rich.[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Neutral] And now you're smiling?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] We just met a smuggler who can help us![B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Blush] Plus I kind of want to rub all over her...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] But she's not a friend, Sophie.[B][C]") ]])
                fnCutscene([[ Append("Narissa:[E|Neutral] I don't pretend to be anything but what I am.[P] So, yes, I'm not your friend -[P] but if you want to cuddle...[B][C]") ]])
                fnCutscene([[ Append("Narissa:[E|Neutral] *ahem*[P] I don't particularly care about your cause, or the Administration's.[P] But I do care about the color of your money.[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Neutral] ...[P] I don't get it?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Narissa here intends to sell her services to whichever side can afford to pay her.[P] Information, supplies, or maybe just bottles of booze. Am I right?[B][C]") ]])
                fnCutscene([[ Append("Narissa:[E|Neutral] Bottles of booze, eh?[P] Good thinking.[P] With transport interrupted, organic citizens are going to need their fix.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] So happy to help.[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Neutral] But we're trying to free you.[P] Won't you help us?[B][C]") ]])
                fnCutscene([[ Append("Narissa:[E|Neutral] Girl, if I wanted off this rock, I could leave in an hour.[P] I know the right golems.[B][C]") ]])
                fnCutscene([[ Append("Narissa:[E|Neutral] I'm already free, and I prefer the administrators to not know that.[B][C]") ]])
                fnCutscene([[ Append("Narissa:[E|Neutral] So, during work hours, I'm just a big-titted dummy who screws everyone who gives her a tickle.[B][C]") ]])
                fnCutscene([[ Append("Narissa:[E|Neutral] But if you come in the off hours and say the right words, I can get you damn near anything.[B][C]") ]])
                fnCutscene([[ Append("Narissa:[E|Neutral] So, listen up.[P] You got spare work credits?[P] I can trade those for platina.[B][C]") ]])
                fnCutscene([[ Append("Narissa:[E|Neutral] Got platina? I can sell you credits chips, or goodies I've 'found'.[P] You know.[P] Fell off the back of a tram.[B][C]") ]])
                fnCutscene([[ Append("Narissa:[E|Neutral] And if you've got spare junk, well, one robot's trash is another raiju's treasure.[P] Get me?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] So what have you got for us?[BLOCK]") ]])
            end
        --Repeats.
        else
            fnCutscene([[ Append("Narissa:[E|Neutral] Need something?[BLOCK]") ]])
    
        end
    
    --55 in the lead.
    else
    
        --First time meeting Narissa.
        if(iBiolabsMetNarissa == 0.0) then
            
            --Flag.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iBiolabsMetNarissa", "N", 1.0)
            
            fnCutscene([[ Append("Raiju:[E|Neutral] Former Head of Security, Unit 2855.[P] Criminy, never thought I'd be happy to see you.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] You are almost as bad at greetings as I am.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Happy] I haven't seen a burn like that since the last time I pulled the trigger![B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] (I must have said something funny.)[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] [EMOTION|SX-399|Smirk]Why are you happy to see me, raiju?[B][C]") ]])
            fnCutscene([[ Append("Narissa:[E|Neutral] Call me Narissa, sweetie.[B][C]") ]])
            fnCutscene([[ Append("Narissa:[E|Neutral] I'm happy to see you because you're a rebel leader.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Why did you not previously think you'd be happy to see me?[B][C]") ]])
            fnCutscene([[ Append("Narissa:[E|Neutral] ...[P] Because you were a cop.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] I see.[P] So you are some sort of black marketeer.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] How'd you figure that out?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] She is a criminal of some description, hence fearing a 'cop' like me.[P] But she is not a rebel sympathizer, because I [P]*was*[P] a security unit before I was a rebel leader, so she was a criminal before the rebellion.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Therefore, of the list of criminal occupations, 'Black Marketeer' is the most encompassing.[P] She could be a mercenary, or a smuggler, but all fall under that umbrella.[B][C]") ]])
            fnCutscene([[ Append("Narissa:[E|Neutral] Sweet saltwater taffy, she's a sharp one.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Flirt] Isn't she?[B][C]") ]])
            fnCutscene([[ Append("Narissa:[E|Neutral] Yes, Unit 2855, I am a 'fixer'.[P] I move product, and that includes information.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Unfortunately, I am obligated not to execute you due to a promise made to Ms. Primrose.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Sad] Woah![P] That's a jump in tone![B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] This raiju intends to sell information about us to the administration, SX-399.[P] She intends to play both sides in the rebellion.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Unlike most neutrals, she is a belligerant, but for both factions at the same time.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] And she is therefore a spy to be executed.[B][C]") ]])
            fnCutscene([[ Append("Narissa:[E|Neutral] Five sentences in and she's already threatening to kill me.[P] Maybe I was wrong about being happy to see you.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] She does have a point.[P] Why should we trust you?[B][C]") ]])
            fnCutscene([[ Append("Narissa:[E|Neutral] For your own benefit?[P] Don't.[P] Trust my goods, I trust your money.[B][C]") ]])
            fnCutscene([[ Append("Narissa:[E|Neutral] But I suppose I'll have to act through an intermediary if my head's on a chopping block.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Down] No.[P] I apologize.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Down] Christine would not approve of executing a neutral.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Sad] She would not approve of an execution, period![B][C]") ]])
            fnCutscene([[ Append("55:[E|Down] ...[P] That is also correct as well.[B][C]") ]])
            fnCutscene([[ Append("Narissa:[E|Neutral] [EMOTION|SX-399|Neutral]In the future, I'll be dealing with Christine, then.[P] But for now?[B][C]") ]])
            fnCutscene([[ Append("Narissa:[E|Neutral] [EMOTION|Tiffany|Neutral]You got spare work credits?[P] I can trade those for platina.[B][C]") ]])
            fnCutscene([[ Append("Narissa:[E|Neutral] Got platina? I can sell you credits chips, or goodies I've 'found'.[P] You know.[P] Fell off the back of a tram.[B][C]") ]])
            fnCutscene([[ Append("Narissa:[E|Neutral] And if you've got spare junk, well, one robot's trash is another raiju's treasure.[P] Get me?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Very well.[BLOCK]") ]])
    
        --Repeats.
        else
            fnCutscene([[ Append("Narissa:[E|Neutral] Need something?[BLOCK]") ]])
        end
    
    end
    
    --Decisions.
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Buy Items\", " .. sDecisionScript .. ", \"Buy\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Buy Work Credits\", " .. sDecisionScript .. ", \"CreditsBuy\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Sell Work Credits\", " .. sDecisionScript .. ", \"CreditsSell\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Goodbye\", " .. sDecisionScript .. ", \"Goodbye\") ")
    fnCutsceneBlocker()

--Buying items.
elseif(sTopicName == "Buy") then
	WD_SetProperty("Hide")
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Narissa", "Neutral") ]])
	fnCutscene([[ Append("Narissa:[E|Neutral] Take a look, sweeties.") ]])
    fnCutsceneBlocker()
    
    --Setup.
    local sBuildPath = fnResolvePath() .. "Shop Setup.lua"
	fnCutscene(" AM_SetShopProperty(\"Show\", \"Narissa's Black Market\", \"" .. sBuildPath .. "\", \"Null\") ")
	fnCutsceneBlocker()

--Buying work credits.
elseif(sTopicName == "CreditsBuy") then

    --Platina count.
    local iPlatinaTotal = AdInv_GetProperty("Platina")

    --Dialogue.
	WD_SetProperty("Hide")
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Narissa", "Neutral") ]])
	fnCutscene([[ Append("Narissa:[E|Neutral] So here's the deal, sweetie.[P] I'll sell you a credits chip worth 30 work credits for 600 platina a pop.[B][C]") ]])
	fnCutscene([[ Append("Narissa:[E|Neutral] Just insert them in a work terminal and bam, instant wealth.[P] How many can I get you?[BLOCK]") ]])
    
    --Decisions.
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
    if(iPlatinaTotal >= 3000) then
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"5 Chips for 3000\", " .. sDecisionScript .. ", \"Buy5Chips\") ")
    end
    if(iPlatinaTotal >= 1800) then
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"3 Chips for 1800\", " .. sDecisionScript .. ", \"Buy3Chips\") ")
    end
    if(iPlatinaTotal >= 600) then
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"1 Chip for 600\", " .. sDecisionScript .. ", \"Buy1Chip\") ")
    end
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"No thanks\", " .. sDecisionScript .. ", \"NoThanks\") ")
    fnCutsceneBlocker()

-- |[Work Credits Purchasing]|
elseif(sTopicName == "Buy5Chips") then
	WD_SetProperty("Hide")
    
    --Subtract cash.
    AdInv_SetProperty("Remove Platina", 3000)
    for i = 1, 5, 1 do
        LM_ExecuteScript(gsItemListing, "Credits Chip")
    end
    
    --SFX.
    AudioManager_PlaySound("Menu|BuyOrSell")
    
    --Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Narissa", "Neutral") ]])
	fnCutscene([[ Append("Narissa:[E|Neutral] High roller?[P] I like that. Here you go.[B][C]") ]])
	fnCutscene([[ Append("Narissa:[E|Neutral] (Received 5 Credits Chips)[B][C]") ]])
	fnCutscene([[ Append("Narissa:[E|Neutral] Anything else?[BLOCK]") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Buy Items\", " .. sDecisionScript .. ", \"Buy\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Buy Work Credits\", " .. sDecisionScript .. ", \"CreditsBuy\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Sell Work Credits\", " .. sDecisionScript .. ", \"CreditsSell\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Goodbye\", " .. sDecisionScript .. ", \"Goodbye\") ")
    fnCutsceneBlocker()
    
    
elseif(sTopicName == "Buy3Chips") then
	WD_SetProperty("Hide")
    
    --Subtract cash.
    AdInv_SetProperty("Remove Platina", 1800)
    for i = 1, 3, 1 do
        LM_ExecuteScript(gsItemListing, "Credits Chip")
    end
    
    --SFX.
    AudioManager_PlaySound("Menu|BuyOrSell")
    
    --Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Narissa", "Neutral") ]])
	fnCutscene([[ Append("Narissa:[E|Neutral] Pleasure doing business with you.[B][C]") ]])
	fnCutscene([[ Append("Narissa:[E|Neutral] (Received 3 Credits Chips)[B][C]") ]])
	fnCutscene([[ Append("Narissa:[E|Neutral] Anything else?[BLOCK]") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Buy Items\", " .. sDecisionScript .. ", \"Buy\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Buy Work Credits\", " .. sDecisionScript .. ", \"CreditsBuy\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Sell Work Credits\", " .. sDecisionScript .. ", \"CreditsSell\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Goodbye\", " .. sDecisionScript .. ", \"Goodbye\") ")
    fnCutsceneBlocker()
    
elseif(sTopicName == "Buy1Chip") then
	WD_SetProperty("Hide")
    
    --Subtract cash.
    AdInv_SetProperty("Remove Platina", 600)
    LM_ExecuteScript(gsItemListing, "Credits Chip")
    
    --SFX.
    AudioManager_PlaySound("Menu|BuyOrSell")
    
    --Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Narissa", "Neutral") ]])
	fnCutscene([[ Append("Narissa:[E|Neutral] One credits chip, coming up.[B][C]") ]])
	fnCutscene([[ Append("Narissa:[E|Neutral] (Received Credits Chip)[B][C]") ]])
	fnCutscene([[ Append("Narissa:[E|Neutral] Anything else?[BLOCK]") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Buy Items\", " .. sDecisionScript .. ", \"Buy\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Buy Work Credits\", " .. sDecisionScript .. ", \"CreditsBuy\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Sell Work Credits\", " .. sDecisionScript .. ", \"CreditsSell\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Goodbye\", " .. sDecisionScript .. ", \"Goodbye\") ")
    fnCutsceneBlocker()

-- |[Selling Work Credits]|
elseif(sTopicName == "CreditsSell") then
	WD_SetProperty("Hide")

    --Credits count.
    local iWorkCreditsTotal = math.floor(VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N"))

    --Dialogue.
	WD_SetProperty("Hide")
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Narissa", "Neutral") ]])
	fnCutscene([[ Append("Narissa:[E|Neutral] The current going rate for a work credit is 20 platina per credit.[P] I'll just need your sign off.[B][C]") ]])
	fnCutscene(" WD_SetProperty(\"Append\", \"Narissa:[E|Neutral] How many would you like to sell? You have " .. iWorkCreditsTotal .. ".[BLOCK]\") ")
    
    --Decisions.
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
    if(iWorkCreditsTotal >= 200) then
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"200 Credits for 4000\", " .. sDecisionScript .. ", \"Sell200Creds\") ")
    end
    if(iWorkCreditsTotal >= 100) then
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"100 Credits for 2000\", " .. sDecisionScript .. ", \"Sell100Creds\") ")
    end
    if(iWorkCreditsTotal >= 50) then
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"50 Credits for 1000\", " .. sDecisionScript .. ", \"Sell50Creds\") ")
    end
    if(iWorkCreditsTotal >= 10) then
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"10 Credits for 200\", " .. sDecisionScript .. ", \"Sell10Creds\") ")
    end
    if(iWorkCreditsTotal >= 1) then
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Sell All Credits\", " .. sDecisionScript .. ", \"SellAllCredits\") ")
    end
    
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"No thanks\", " .. sDecisionScript .. ", \"NoThanks\") ")
    fnCutsceneBlocker()
    
elseif(sTopicName == "Sell200Creds") then
	WD_SetProperty("Hide")
    
    --Cash.
    AdInv_SetProperty("Add Platina", 4000)
    local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
    VM_SetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N", iWorkCreditsTotal - 200)
    
    --SFX.
    AudioManager_PlaySound("Menu|BuyOrSell")
    
    --Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Narissa", "Neutral") ]])
	fnCutscene([[ Append("Narissa:[E|Neutral] Here you go.[P] Trust me, we're both winning on this one.[B][C]") ]])
	fnCutscene([[ Append("Narissa:[E|Neutral] (Received 4000 Platina)[B][C]") ]])
	fnCutscene([[ Append("Narissa:[E|Neutral] Anything else?[BLOCK]") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Buy Items\", " .. sDecisionScript .. ", \"Buy\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Buy Work Credits\", " .. sDecisionScript .. ", \"CreditsBuy\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Sell Work Credits\", " .. sDecisionScript .. ", \"CreditsSell\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Goodbye\", " .. sDecisionScript .. ", \"Goodbye\") ")
    fnCutsceneBlocker()
    
elseif(sTopicName == "Sell100Creds") then
	WD_SetProperty("Hide")
    
    --Cash.
    AdInv_SetProperty("Add Platina", 2000)
    local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
    VM_SetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N", iWorkCreditsTotal - 100)
    
    --SFX.
    AudioManager_PlaySound("Menu|BuyOrSell")
    
    --Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Narissa", "Neutral") ]])
	fnCutscene([[ Append("Narissa:[E|Neutral] Another satisfied customer.[B][C]") ]])
	fnCutscene([[ Append("Narissa:[E|Neutral] (Received 2000 Platina)[B][C]") ]])
	fnCutscene([[ Append("Narissa:[E|Neutral] Anything else?[BLOCK]") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Buy Items\", " .. sDecisionScript .. ", \"Buy\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Buy Work Credits\", " .. sDecisionScript .. ", \"CreditsBuy\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Sell Work Credits\", " .. sDecisionScript .. ", \"CreditsSell\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Goodbye\", " .. sDecisionScript .. ", \"Goodbye\") ")
    fnCutsceneBlocker()
    
elseif(sTopicName == "Sell50Creds") then
	WD_SetProperty("Hide")
    
    --Cash.
    AdInv_SetProperty("Add Platina", 1000)
    local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
    VM_SetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N", iWorkCreditsTotal - 50)
    
    --SFX.
    AudioManager_PlaySound("Menu|BuyOrSell")
    
    --Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Narissa", "Neutral") ]])
	fnCutscene([[ Append("Narissa:[E|Neutral] Don't spend it all in one place, sweetie.[B][C]") ]])
	fnCutscene([[ Append("Narissa:[E|Neutral] (Received 1000 Platina)[B][C]") ]])
	fnCutscene([[ Append("Narissa:[E|Neutral] Anything else?[BLOCK]") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Buy Items\", " .. sDecisionScript .. ", \"Buy\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Buy Work Credits\", " .. sDecisionScript .. ", \"CreditsBuy\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Sell Work Credits\", " .. sDecisionScript .. ", \"CreditsSell\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Goodbye\", " .. sDecisionScript .. ", \"Goodbye\") ")
    fnCutsceneBlocker()
    
elseif(sTopicName == "Sell10Creds") then
	WD_SetProperty("Hide")
    
    --Cash.
    AdInv_SetProperty("Add Platina", 200)
    local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
    VM_SetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N", iWorkCreditsTotal - 10)
    
    --SFX.
    AudioManager_PlaySound("Menu|BuyOrSell")
    
    --Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Narissa", "Neutral") ]])
	fnCutscene([[ Append("Narissa:[E|Neutral] Here you go.[B][C]") ]])
	fnCutscene([[ Append("Narissa:[E|Neutral] (Received 200 Platina)[B][C]") ]])
	fnCutscene([[ Append("Narissa:[E|Neutral] Anything else?[BLOCK]") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Buy Items\", " .. sDecisionScript .. ", \"Buy\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Buy Work Credits\", " .. sDecisionScript .. ", \"CreditsBuy\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Sell Work Credits\", " .. sDecisionScript .. ", \"CreditsSell\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Goodbye\", " .. sDecisionScript .. ", \"Goodbye\") ")
    fnCutsceneBlocker()
    
elseif(sTopicName == "SellAllCredits") then
	WD_SetProperty("Hide")
    
    --Cash.
    local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
    AdInv_SetProperty("Add Platina", 20 * iWorkCreditsTotal)
    VM_SetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N", 0)
    
    --SFX.
    AudioManager_PlaySound("Menu|BuyOrSell")
    
    --Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Narissa", "Neutral") ]])
	fnCutscene([[ Append("Narissa:[E|Neutral] Closing the account, eh?[P] Here you go.[B][C]") ]])
	fnCutscene(" WD_SetProperty(\"Append\", \"Narissa:[E|Neutral] (Received " .. iWorkCreditsTotal * 20 .. " Platina)[B][C]\") ")
	fnCutscene([[ Append("Narissa:[E|Neutral] Anything else?[BLOCK]") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Buy Items\", " .. sDecisionScript .. ", \"Buy\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Buy Work Credits\", " .. sDecisionScript .. ", \"CreditsBuy\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Sell Work Credits\", " .. sDecisionScript .. ", \"CreditsSell\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Goodbye\", " .. sDecisionScript .. ", \"Goodbye\") ")
    fnCutsceneBlocker()

-- |[Cancel Out]|
elseif(sTopicName == "NoThanks") then
	WD_SetProperty("Hide")
    
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Narissa", "Neutral") ]])
	fnCutscene([[ Append("Narissa:[E|Neutral] Changed your mind?[P] No problem.[BLOCK]") ]])
    
    --Decisions.
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Buy Items\", " .. sDecisionScript .. ", \"Buy\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Buy Work Credits\", " .. sDecisionScript .. ", \"CreditsBuy\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Sell Work Credits\", " .. sDecisionScript .. ", \"CreditsSell\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Goodbye\", " .. sDecisionScript .. ", \"Goodbye\") ")
    fnCutsceneBlocker()

-- |[Exit Conversation]|
elseif(sTopicName == "Goodbye") then
	WD_SetProperty("Hide")
    
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Narissa", "Neutral") ]])
	fnCutscene([[ Append("Narissa:[E|Neutral] You know exactly where to find me, sweeties.[P] I'll be waiting.") ]])

end
