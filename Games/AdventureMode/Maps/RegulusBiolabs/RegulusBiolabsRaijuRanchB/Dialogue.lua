-- |[Root]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Variables]|
local iChristineLeadingParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iChristineLeadingParty", "N")

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    -- |[Drone Units]|
    if(sActorName == "SecurityA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Drone:[VOICE|LatexDrone] OUR COMMAND UNIT HAS APPROPRIATED THIS OFFICE AS HER HEADQUARTERS.[P] PLEASE WIPE YOUR FEET.") ]])
        
    elseif(sActorName == "SecurityB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Drone:[VOICE|LatexDrone] CURRENT SECURITY STATUS IS REGISTERED AS 'SPIFFY'.[P] AUTHORIZATION IS NEEDED TO UPGRADE IT TO 'SQUEAKY CLEAN'.[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Squeaky clean is the theoretically highest security status.[P] It's also asymptotically unobtainable, but a drone wouldn't know that.)") ]])
        
    -- |[2856]|
    elseif(sActorName == "2856") then
    
        --Variables
        local iGotBreachingTools = VM_GetVar("Root/Variables/Chapter5/Scenes/iGotBreachingTools", "N")
        
        --Christine is leading:
        if(iChristineLeadingParty >= 1.0) then
    
            --Don't have the tools yet.
            if(iGotBreachingTools == 0.0) then
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "2856", "Neutral") ]])
                fnCutscene([[ Append("2856:[E|Neutral] Have you obtained the breaching tools yet?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] No.[B][C]") ]])
                fnCutscene([[ Append("2856:[E|Neutral] I checked the inventory records.[P] They're in the northeastern storage sheds.[P] Are you on your way there?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] Yes.[B][C]") ]])
                fnCutscene([[ Append("2856:[E|Neutral] Good.[P] Have we anything further to discuss?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] No.[P] Goodbye.") ]])
                fnCutsceneBlocker()
        
            --Got the tools.
            else
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "2856", "Neutral") ]])
                fnCutscene([[ Append("2856:[E|Neutral] Have you obtained the breaching tools yet?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Yes.[B][C]") ]])
                fnCutscene([[ Append("2856:[E|Neutral] Good.[P] Your objective is in the Epsilon Laboratories.[P] Exit the ranch, go west, then north when you are in the Gamma Laboratories.[B][C]") ]])
                fnCutscene([[ Append("2856:[E|Neutral] The connection to the Beta Laboratories has a security station.[P] The entrance to Epsilon is on the eastern side of that connection.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] We're on our way.[P] Goodbye.") ]])
                fnCutsceneBlocker()
            end
    
        --55 is leading:
        else
    
            --Don't have the tools yet.
            if(iGotBreachingTools == 0.0) then
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Tiffany", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "SX-399", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "2856", "Neutral") ]])
                fnCutscene([[ Append("2856:[E|Neutral] Have you obtained the breaching tools yet?[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] No.[B][C]") ]])
                fnCutscene([[ Append("2856:[E|Neutral] I checked the inventory records.[P] They're in the northeastern storage sheds.[P] Are you on your way there?[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] Yes we are, thank you for the information.[B][C]") ]])
                fnCutscene([[ Append("2856:[E|Neutral] Good.[P] Have we anything further to discuss?[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] No.") ]])
                fnCutsceneBlocker()
        
            --Got the tools.
            else
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Tiffany", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "SX-399", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "2856", "Neutral") ]])
                fnCutscene([[ Append("2856:[E|Neutral] Have you obtained the breaching tools yet?[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] We have them.[B][C]") ]])
                fnCutscene([[ Append("2856:[E|Neutral] Good.[P] Your objective is in the Epsilon Laboratories.[P] Exit the ranch, go west, then north when you are in the Gamma Laboratories.[B][C]") ]])
                fnCutscene([[ Append("2856:[E|Neutral] The connection to the Beta Laboratories has a security station.[P] The entrance to Epsilon is on the eastern side of that connection.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] Moving out.") ]])
                fnCutsceneBlocker()
            end
        
        end
    
    -- |[Lord Golems]|
    elseif(sActorName == "GolemA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|GolemLord] To think I am contemplating taking up quarters in this domicile.[P] There are no defragmentation pods anywhere else for one of my stature.[B][C]") ]])
        fnCutscene([[ Append("Golem:[VOICE|GolemLord] Well, the principal's quarters.[P] But the Head of Research...[P] I'd rather not be in the same room with her.") ]])
        
    elseif(sActorName == "GolemB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|GolemLord] The humans run here, in a circle![P] It keeps their leg motivators strong, you know.[P] They have to use them constantly or they degrade.") ]])
        
    elseif(sActorName == "GolemC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|GolemLord] There's something to be said for an organic system that maintains itself when it is used.[P] Perhaps we should integrate that into our systems?") ]])
        
    elseif(sActorName == "GolemD") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|GolemLord] I am...[P] supervising...[P] the humans at the swimming pools.[P] My optical receptors are plenty sensitive from this distance.") ]])
    
    -- |[Raijus]|
    elseif(sActorName == "RaijuA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Raiju:[VOICE|HumanF0] I'm helping the security teams![P] Watch out for tangos, ha ha ha this is fun!") ]])
        
    elseif(sActorName == "RaijuB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Raiju:[VOICE|HumanF1] I wanna go swimming with everyone else![P] But they never let us because it'll probably kill them.[P] But I waaaannnaaaa!") ]])
        
    elseif(sActorName == "RaijuC") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Raiju:[VOICE|HumanF0] Don't worry sweetie.[P] If any of those nasties get close I can shock them for you![P] Zap zap!") ]])
    
    -- |[Slave Golems]|
    elseif(sActorName == "GolemSlaveA") then
        if(iChristineLeadingParty >= 1.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Honestly, looking after the humans is one of the best assignments I've had.[P] Sure, my Lord Golem is a typical Lord Golem, but the humans are all kind and sweet.[B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] What about the humans who make Lord Golem?[P] Do you think they'll stay nice?[B][C]") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] You know, I don't keep up with them afterwards.[P] Maybe the city will straighten itself out if we promote enough nice humans?[B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] (Something tells me the city will change them before they change the city...)") ]])
        
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Honestly, looking after the humans is one of the best assignments I've had.[P] Sure, my Lord Golem is a typical Lord Golem, but the humans are all kind and sweet.[B][C]") ]])
            fnCutscene([[ Append("55:[VOICE|Tiffany] And after they are converted?[B][C]") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] You know, I don't keep up with them afterwards.[P] Maybe the city will straighten itself out if we promote enough nice humans?[B][C]") ]])
            fnCutscene([[ Append("55:[VOICE|Tiffany] Structuralism suggests that is not the case.[B][C]") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Yeah, it does, doesn't it...") ]])
        end
        
    elseif(sActorName == "GolemSlaveB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Even with a freakin' crisis going on, oh we absolutely must trim the brush.[P] Of course, my Lord Golem![P] At once!") ]])
        
    elseif(sActorName == "GolemSlaveC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] I am supervising this human in fabrication training.[P] Fabricating pulse weaponry is no different than fabricating a table leg!") ]])
        
    elseif(sActorName == "GolemSlaveD") then
        if(iChristineLeadingParty >= 1.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] The humans, thus far, have managed to avoid accidents in the pool for 100 straight days.[P] Naturally my Lord Golem mandates that I watch them anyway, just in case.[B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] You don't sound like you mind.[B][C]") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Not really, it's easy work.[P] She also mandates I record the proceedings and upload them for her to view.[B][C]") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] She's extremely dedicated to the safety of these humans.[P] She dotes on them like they were her own children.[P] It's touching in a way.") ]])
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] The humans, thus far, have managed to avoid accidents in the pool for 100 straight days.[P] Naturally my Lord Golem mandates that I watch them anyway, just in case.[B][C]") ]])
            fnCutscene([[ Append("55:[VOICE|Tiffany] An impressive safety record for a human.[B][C]") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] We watch them very closely, and we record their work for the local administrator to review.[B][C]") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] She's extremely dedicated to the safety of these humans.[P] She dotes on them like they were her own children.[P] It's touching in a way.") ]])
    
        end
    
    -- |[Humans]|
    elseif(sActorName == "HumanA") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Human:[VOICE|HumanF0] Hmm, where does the magrail fit onto the slide?[P] Oh, right there, of course!") ]])
        
    elseif(sActorName == "HumanB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Human:[VOICE|HumanF1] We were all going to have a pool party when everything went wild.[P] But until we're told otherwise, we're not letting them stop us![P] Dive in, girls!") ]])
        
    elseif(sActorName == "HumanC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Human:[VOICE|HumanF0] Sometimes I like to just walk around in my swimwear and let all the male units gawk.[P] My Lord Golem even encourages me!") ]])
        
    elseif(sActorName == "HumanD") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Human:[VOICE|HumanF1] Sometimes I wish I hadn't volunteered for security duty, but I'll be damned if I'm letting any of these things get my fellow units!") ]])
        
    elseif(sActorName == "HumanE") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Human:[VOICE|HumanF0] If we work together, I know we can stop those bad guys.[P] I've got the power of lightning on my side!") ]])
        
    elseif(sActorName == "HumanF") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Human:[VOICE|HumanF1] I was going to go to bed early, but of course we had to have an emergency drill.[P] This is a drill, right?") ]])
        
    elseif(sActorName == "HumanG") then
        
        --Variables.
        local iFinished198 = VM_GetVar("Root/Variables/Chapter5/Scenes/iFinished198", "N")
        
        --Christine:
        if(iChristineLeadingParty >= 1.0) then
        
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            if(iFinished198 == 0.0) then
                fnCutscene([[ Append("Human:[VOICE|HumanF0] Hmm?[P] Oh, I was just thinking about something.[P] Don't mind me.") ]])
            else
                fnCutscene([[ Append("Human:[VOICE|HumanF0] I was told you might be coming this way, comrade.[P] I don't know about the others, but I'm with you.[B][C]") ]])
                fnCutscene([[ Append("Christine:[VOICE|Christine] Better keep that to yourself, for the time being.[P] The Head of Research is in the building across from you.[B][C]") ]])
                fnCutscene([[ Append("Human:[VOICE|HumanF0] I know.[P] I've been sending updates on her position out through the network.[B][C]") ]])
                fnCutscene([[ Append("Christine:[VOICE|Christine] But how?[P] Network access is spotty at best.[B][C]") ]])
                fnCutscene([[ Append("Human:[VOICE|HumanF0] Heh.[P] Unit 745110 sent a courier.[P] An...[P] electric...[P] courier.[P] You know what I mean.[B][C]") ]])
                fnCutscene([[ Append("Christine:[VOICE|Christine] Oh, I'm glad to hear that Amanda is doing all right.[P] Just be discreet.[B][C]") ]])
                fnCutscene([[ Append("Human:[VOICE|HumanF0] Of course.[P] To freedom.") ]])
            end
        
        --55:
        else
        
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            if(iFinished198 == 0.0) then
                fnCutscene([[ Append("Human:[VOICE|HumanF0] Hmm?[P] Oh, I was just thinking about something.[P] Don't mind me.") ]])
            else
                fnCutscene([[ Append("Human:[VOICE|HumanF0] Blue Leader![P] Can I help you with anything?[B][C]") ]])
                fnCutscene([[ Append("55:[VOICE|Tiffany] Do not blow your cover.[P] I have the situation under control.[B][C]") ]])
                fnCutscene([[ Append("Human:[VOICE|HumanF0] I know.[P] I've been trying to send updates out but the network is spotty.[B][C]") ]])
                fnCutscene([[ Append("Human:[VOICE|HumanF0] But...[P] Unit 745110 sent a courier.[P] An...[P] electric...[P] courier.[P] You know what I mean.[B][C]") ]])
                fnCutscene([[ Append("55:[VOICE|Tiffany] Excellent work.[P] These couriers can operate despite network outages.[P] Well done.[B][C]") ]])
                fnCutscene([[ Append("Human:[VOICE|HumanF0] Hey it was Unit 745110's idea.[P] Good luck out there, Blue Leader.") ]])
            end
        end
        
    elseif(sActorName == "HumanH") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Human:[VOICE|HumanM0] I can't wait to graduate so I can look as good as the girl units do...") ]])
        
    elseif(sActorName == "HumanI") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Human:[VOICE|HumanM1] If I get converted and assigned to a different department than my tandem unit, what then?[P] Will we never see each other again?[P] Oh my...") ]])
        
    elseif(sActorName == "HumanJ") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Human:[VOICE|HumanM0] I can't tell if Unit 754334 likes me or not.[P] Look at her, she's so good at fabricating.[P] I bet she'll make a great golem...") ]])
        
    -- |[Boob Girls]|
    --These have special reaction cases when spoken to by Christine. 55 does not notice.
    elseif(sActorName == "HumanK") then
        
        --Variables.
        local sChristineForm         = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
        local sSpokeToFirstBoobGirl  = VM_GetVar("Root/Variables/Chapter5/Scenes/sSpokeToFirstBoobGirl", "S")
        local sSpokeToSecondBoobGirl = VM_GetVar("Root/Variables/Chapter5/Scenes/sSpokeToSecondBoobGirl", "S")
        
        --Common.
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "HumanF0B", "Neutral") ]])
        
        --Christine:
        if(iChristineLeadingParty >= 1.0) then
            
            --Human
            if(sChristineForm == "Human") then
                
                --Hasn't spoken to the second boob girl yet:
                if(sSpokeToSecondBoobGirl == "Nobody") then
                    VM_SetVar("Root/Variables/Chapter5/Scenes/sSpokeToFirstBoobGirl", "S", "Nomatter")
                    VM_SetVar("Root/Variables/Chapter5/Scenes/sSpokeToSecondBoobGirl", "S", "Nomatter")
                    fnCutscene([[ Append("Human:[E|Neutral] Hello, lord unit![P] Are you here for an inspection?[B][C]") ]])
                    fnCutscene([[ Append("Sophie:[E|Neutral] Lord unit?[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Smirk] *You, Sophie!*[B][C]") ]])
                    fnCutscene([[ Append("Sophie:[E|Blush] Eep![P] Yes, I am Lord Unit 49 -[P] uh, just call me Sophie![B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] Hello, Sophie.[P] I've not seen your friend before.[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Smirk] Christine.[B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] Are you a recent transfer?[P] I didn't know we were getting a transfer![P] Hello![B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Smirk] I'm just visiting, actually.[B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] ...[P] Oh, so that means you must be wondering what these machines are![B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Smirk] That's it exactly![B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] These are the milking machines.[P] All our human cheese are made from milk pumped from these![B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] We're very proud of our work here.[P] We receive compliments from all across the city![B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Blush] Human cheese?[B][C]") ]])
                    fnCutscene([[ Append("Sophie:[E|Blush] Milking happens right here?[B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] It does![B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Blush] And your...[P] chest...[B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] A very common side effect of being milked is larger mammaries.[B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] Ms. Primrose takes a great interest in making sure we are milked as often as possible.[B][C]") ]])
                    fnCutscene([[ Append("Sophie:[E|Neutral] Does it hurt?[B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] It feels wonderous, lord unit![B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] In fact, permission is required to be milked.[P] It's a reward system to encourage good behavior.[B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] Well, males selected for reproduction also get milked, but that has its own machine.[B][C]") ]])
                    fnCutscene([[ Append("Sophie:[E|Blush] Males...[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Blush] Getting milked...[B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] Is there something upsetting you?[P] I wasn't aware this wasn't common knowledge.[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Smirk] This is both of our first times here.[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Blush] Say...[P] As a lord unit, would my friend here be able to give permission to a human to be milked?[B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] Of course![P] I can show you how to operate the machines, or give you a datafile.[B][C]") ]])
                    fnCutscene([[ Append("Sophie:[E|Neutral] *Christine?*[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Blush] *She says it feels really good, and gives you bigger boobs, Sophie!*[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Blush] *I would love it if you would play with my big jiggly boobs...*[B][C]") ]])
                    fnCutscene([[ Append("Sophie:[E|Blush] *Yes, but...[P] really?*[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Blush] *Really!*[B][C]") ]])
                    fnCutscene([[ Append("Sophie:[E|Happy] *Oh my goodness, yes![P] Yes yes yes!*[B][C]") ]])
                    fnCutscene([[ Append("Sophie:[E|Happy] Yippee![B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] Very good![P] Just check the machines if you'd like, I'll send the instruction file to your PDU.") ]])
                
                --Has spoken to two boob girls.
                elseif(sSpokeToSecondBoobGirl ~= "Nobody" and sSpokeToSecondBoobGirl ~= "Nomatter") then
                    VM_SetVar("Root/Variables/Chapter5/Scenes/sSpokeToFirstBoobGirl", "S", "Nomatter")
                    VM_SetVar("Root/Variables/Chapter5/Scenes/sSpokeToSecondBoobGirl", "S", "Nomatter")
                    fnCutscene([[ Append("Human:[E|Neutral] Hello, lord unit![P] Are you here for an inspection?[B][C]") ]])
                    fnCutscene([[ Append("Sophie:[E|Neutral] Lord unit?[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Smirk] *You, Sophie!*[B][C]") ]])
                    fnCutscene([[ Append("Sophie:[E|Blush] Eep![P] Yes, I am Lord Unit 49 -[P] uh, just call me Sophie![B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] Hello, Sophie.[P] I've not seen your friend before.[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Smirk] Christine.[B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] Are you a recent transfer?[P] I didn't know we were getting a transfer![P] Hello![B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Smirk] I'm just visiting, actually.[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Blush] So I've been told that these are milking machines?[B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] That's right![P] All our human cheese are made from milk pumped from these![B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] We're very proud of our work here.[P] We receive compliments from all across the city![B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Smirk] So if my lord unit friend were to authorize a certain human to use them...[B][C]") ]])
                    fnCutscene([[ Append("Sophie:[E|Neutral] *Christine?*[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Blush] *They said it feels really good, and gives you bigger boobs, Sophie!*[B][C]") ]])
                    fnCutscene([[ Append("Sophie:[E|Blush] *Yes, but...[P] really?*[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Blush] *Really!*[B][C]") ]])
                    fnCutscene([[ Append("Sophie:[E|Happy] *Oh my goodness, yes![P] Yes yes yes!*[B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] Of course, my lord![P] I'll send your PDU the instructions.[B][C]") ]])
                    fnCutscene([[ Append("Sophie:[E|Happy] Yippee![B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] Just inspect the machines and enjoy yourself![P] I was nervous my first time, but it feels *so good*...") ]])
                
                --Repeat cases.
                else
                    fnCutscene([[ Append("Human:[E|Neutral] Just take a look at the machines whenever you are ready.[P] Unless you enjoy talking to me?[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Blush] Yes...[P] We like being around...[P] you...[B][C]") ]])
                    fnCutscene([[ Append("Sophie:[E|Blush] They're just so big and bouncy...[B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] Tee hee![P] Thank you![B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] I hope to become a raiju after graduation![P] Are you aiming for raiju, or perhaps lord unit?[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Blush] Something along those lines...") ]])
                end
                fnCutsceneBlocker()
            
            --Raiju.
            elseif(sChristineForm == "Raiju") then
                
                --Hasn't spoken to the second boob girl yet:
                if(sSpokeToSecondBoobGirl == "Nobody") then
                    VM_SetVar("Root/Variables/Chapter5/Scenes/sSpokeToFirstBoobGirl", "S", "Nomatter")
                    VM_SetVar("Root/Variables/Chapter5/Scenes/sSpokeToSecondBoobGirl", "S", "Nomatter")
                    fnCutscene([[ Append("Human:[E|Neutral] Hello, lord unit![P] Are you here for an inspection?[B][C]") ]])
                    fnCutscene([[ Append("Sophie:[E|Neutral] Lord unit?[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Smirk] *You, Sophie!*[B][C]") ]])
                    fnCutscene([[ Append("Sophie:[E|Blush] Eep![P] Yes, I am Lord Unit 49 -[P] uh, just call me Sophie![B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] Hello, Sophie.[P] I've not seen your friend before.[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Smirk] Christine.[B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] Are you a recent convert?[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Smirk] Earlier today, actually.[B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] ...[P] Oh, so that means you must be wondering what these machines are![B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Smirk] That's it exactly![B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] These are the milking machines.[P] All our human cheese are made from milk pumped from these![B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] We're very proud of our work here.[P] We receive compliments from all across the city![B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Blush] Human cheese?[B][C]") ]])
                    fnCutscene([[ Append("Sophie:[E|Blush] Milking happens right here?[B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] It does![B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Blush] And your...[P] chest...[B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] A very common side effect of being milked is larger mammaries.[B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] Ms. Primrose takes a great interest in making sure we are milked as often as possible.[B][C]") ]])
                    fnCutscene([[ Append("Sophie:[E|Neutral] Does it hurt?[B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] It feels wonderous, lord unit![B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] In fact, permission is required to be milked.[P] It's a reward system to encourage good behavior.[B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] Well, males selected for reproduction also get milked, but that has its own machine.[B][C]") ]])
                    fnCutscene([[ Append("Sophie:[E|Blush] Males...[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Blush] Getting milked...[B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] Is there something upsetting you?[P] I wasn't aware this wasn't common knowledge.[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Smirk] This is both of our first times here.[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Blush] Say...[P] As a lord unit, would my friend here be able to give permission to a human to be milked?[B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] Of course![P] I can show you how to operate the machines, or give you a datafile.[B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] Though as a raiju, it's best if you don't.[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Neutral] Electrical problems?[B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] Yep.[P] Milking a raiju would probably blow a hole in the roof of the labs.[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Neutral] But if we brought a human friend of mine...[B][C]") ]])
                    fnCutscene([[ Append("Sophie:[E|Neutral] *Christine?*[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Blush] *She says it feels really good, and gives you bigger boobs, Sophie!*[B][C]") ]])
                    fnCutscene([[ Append("Sophie:[E|Blush] *Yes, but...[P] really?*[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Blush] *Really![P] Imagine sucking my big jiggly boobs...*[B][C]") ]])
                    fnCutscene([[ Append("Sophie:[E|Happy] *Oh my goodness, yes![P] Yes yes yes!*[B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] Are they nearby?[B][C]") ]])
                    fnCutscene([[ Append("Sophie:[E|Happy] I'll be back with her![P] Just wait here![B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] Very good![P] Just check the machines if you'd like, I'll send the instruction file to your PDU.") ]])
                
                --Has spoken to two boob girls.
                elseif(sSpokeToSecondBoobGirl ~= "Nobody" and sSpokeToSecondBoobGirl ~= "Nomatter") then
                    VM_SetVar("Root/Variables/Chapter5/Scenes/sSpokeToFirstBoobGirl", "S", "Nomatter")
                    VM_SetVar("Root/Variables/Chapter5/Scenes/sSpokeToSecondBoobGirl", "S", "Nomatter")
                    fnCutscene([[ Append("Human:[E|Neutral] Hello, lord unit![P] Are you here for an inspection?[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Smirk] Not an inspection.[P] Instead, I was wondering if we could make use of your facilities?[B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] Do you mean the milking machines?[P] Sorry, raijus can't be milked.[B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] I mean they can, but the resulting explosion of electricity would probably blow a hole in the roof.[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Smirk] No, I meant, if my lord unit friend were to authorize a certain human to use them?[B][C]") ]])
                    fnCutscene([[ Append("Sophie:[E|Neutral] *Christine?*[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Blush] *They said it feels really good, and gives you bigger boobs, Sophie!*[B][C]") ]])
                    fnCutscene([[ Append("Sophie:[E|Blush] *Yes, but...[P] really?*[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Blush] *Really!*[B][C]") ]])
                    fnCutscene([[ Append("Sophie:[E|Happy] *Oh my goodness, yes![P] Yes yes yes!*[B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] Did you have someone in mind? Any lord unit may approve a milking session![B][C]") ]])
                    fnCutscene([[ Append("Sophie:[E|Happy] I'll be back with her![P] Just wait here![B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] Very good![P] Just check the machines if you'd like, I'll send the instruction file to your PDU.") ]])
                
                --Repeat cases.
                else
                    fnCutscene([[ Append("Human:[E|Neutral] Just take a look at the machines whenever your human arrives.[P] Unless you enjoy talking to me?[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Blush] Yes...[P] We like being around...[P] you...[B][C]") ]])
                    fnCutscene([[ Append("Sophie:[E|Blush] They're just so big and bouncy...[B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] Tee hee![P] Thank you![B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] I hope to become a raiju after graduation![P] I'm sure we'll get to be great friends, then![B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Blush] Of course...") ]])
                end
                fnCutsceneBlocker()
            
            --Golem.
            else
                
                --Hasn't spoken to the second boob girl yet:
                if(sSpokeToSecondBoobGirl == "Nobody") then
                    VM_SetVar("Root/Variables/Chapter5/Scenes/sSpokeToFirstBoobGirl", "S", "Nomatter")
                    VM_SetVar("Root/Variables/Chapter5/Scenes/sSpokeToSecondBoobGirl", "S", "Nomatter")
                    fnCutscene([[ Append("Human:[E|Neutral] Hello, Lord Units![P] Are you here for an inspection?[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Smirk] Not an inspection, but, what is this place?[B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] These are the milking machines.[P] This is where we extract the milk used for the human cheeses you've no doubt had![B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] We're very proud of our work here.[P] We receive compliments from all across the city about our milk and cheese![B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Blush] Human cheese?[B][C]") ]])
                    fnCutscene([[ Append("Sophie:[E|Blush] Milking happens right here?[B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] It does![B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Blush] And your...[P] chest...[B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] A very common side effect of being milked is larger mammaries.[B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] Ms. Primrose takes a great interest in making sure we are milked as often as possible.[B][C]") ]])
                    fnCutscene([[ Append("Sophie:[E|Neutral] Does it hurt?[B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] It feels wonderous, lord unit![B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] In fact, permission is required to be milked.[P] It's a reward system to encourage good behavior.[B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] Well, males selected for reproduction also get milked, but that has its own machine.[B][C]") ]])
                    fnCutscene([[ Append("Sophie:[E|Blush] Males...[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Blush] Getting milked...[B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] Is there something upsetting you, my lords?[P] I wasn't aware this wasn't common knowledge.[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Smirk] This is my first year as a lord golem.[P] I've never been to the biolabs before.[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Blush] Say...[P] As a lord unit, would I be able to give permission to a human to be milked?[B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] Of course, lord unit.[P] I don't see why not.[P] I can show you how to operate the machines, or give you a datafile.[B][C]") ]])
                    fnCutscene([[ Append("Sophie:[E|Neutral] *Christine?*[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Blush] *She says it feels really good, and gives you bigger boobs, Sophie!*[B][C]") ]])
                    fnCutscene([[ Append("Sophie:[E|Blush] *Yes, but...[P] really?*[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Blush] *Really![P] Imagine suckling on my big jiggling boobies...*[B][C]") ]])
                    fnCutscene([[ Append("Sophie:[E|Happy] *Oh my goodness, yes![P] Yes yes yes!*[B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] Did you have someone in mind?[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Smirk] Yes.[P] I'll send her right along with my associate here.[B][C]") ]])
                    fnCutscene([[ Append("Sophie:[E|Happy] I'll be back with her![B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] Very good![P] Just check the machines if you'd like, I'll send the instruction file to your PDU.") ]])
                
                --Has spoken to two boob girls.
                elseif(sSpokeToSecondBoobGirl ~= "Nobody" and sSpokeToSecondBoobGirl ~= "Nomatter") then
                    VM_SetVar("Root/Variables/Chapter5/Scenes/sSpokeToFirstBoobGirl", "S", "Nomatter")
                    VM_SetVar("Root/Variables/Chapter5/Scenes/sSpokeToSecondBoobGirl", "S", "Nomatter")
                    fnCutscene([[ Append("Human:[E|Neutral] Hello, Lord Units![P] Are you here for an inspection?[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Smirk] Not an inspection.[P] Instead, I was wondering if we could make use of your facilities?[B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] Do you mean the milking machines?[P] I'm not sure they'd work on a golem.[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Smirk] No, I meant, if I were to authorize a human to use them?[B][C]") ]])
                    fnCutscene([[ Append("Sophie:[E|Neutral] *Christine?*[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Blush] *They said it feels really good, and gives you bigger boobs, Sophie!*[B][C]") ]])
                    fnCutscene([[ Append("Sophie:[E|Blush] *Yes, but...[P] really?*[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Blush] *Really!*[B][C]") ]])
                    fnCutscene([[ Append("Sophie:[E|Happy] *Oh my goodness, yes![P] Yes yes yes!*[B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] Did you have someone in mind?[P] Any lord unit may approve a milking session![B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Smirk] Yes.[P] I'll send her right along with my associate here.[B][C]") ]])
                    fnCutscene([[ Append("Sophie:[E|Happy] I'll be back with her![B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] Very good![P] Just check the machines if you'd like, I'll send the instruction file to your PDU.") ]])
                
                
                --Repeat cases.
                else
                    fnCutscene([[ Append("Human:[E|Neutral] Just take a look at the machines whenever your human arrives.[P] Unless you enjoy talking to me?[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Blush] Yes...[P] We like being around...[P] you...[B][C]") ]])
                    fnCutscene([[ Append("Sophie:[E|Blush] They're just so big and bouncy...[B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] Tee hee![P] Thank you, lords![B][C]") ]])
                    fnCutscene([[ Append("Human:[E|Neutral] I hope to become a raiju after graduation![P] Put in a good word for me with Ms. Primrose if you can![B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Blush] Of course...") ]])
                end
                fnCutsceneBlocker()

            end
            
        --55:
        else
            fnCutscene([[ Append("Human:[E|Neutral] Hello, Command Unit![P] And...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] This is SX-399.[P] She is an upgraded steam droid.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Smirk] Hi![B][C]") ]])
            fnCutscene([[ Append("Human:[E|Neutral] Steam droid?[P] Wow, I remember seeing those in history class.[P] I didn't think there were any left.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Smirk] There are![B][C]") ]])
            fnCutscene([[ Append("Human:[E|Neutral] Does the Administration intend to upgrade them, too?[P] Because, pardon me, but you look fabulous![B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Happy] Thanks![P] You too![B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Blush] I'm a little jealous...[P] You've got a very large...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Head?[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Blush] ...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Throat?[B][C]") ]])
            fnCutscene([[ Append("Human:[E|Neutral] ...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Eyes?[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Blush] Chest, 55.[P] She has big breasts.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] I do not think so.[P] They are within the tenth percentile among most humans, but the milking process in the breeding program places her in the fiftieth percentile here in the biolabs.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] If you are jealous, I'm sure Unit 499393 could synthesize new mammary plating for you.[B][C]") ]])
            fnCutscene([[ Append("Human:[E|Neutral] Wow.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Blush] Yeah, 55 is kind of an amazing unit, isn't she?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] (I seem to be in error.)[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Thank you for the compliment.[P] Please explain this to me further at your leisure, SX-399.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Blush] Oh I absolutely will, hun...") ]])
        
        end
        
    elseif(sActorName == "HumanL") then
        
        --Variables.
        local sSpokeToFirstBoobGirl  = VM_GetVar("Root/Variables/Chapter5/Scenes/sSpokeToFirstBoobGirl", "S")
        local sSpokeToSecondBoobGirl = VM_GetVar("Root/Variables/Chapter5/Scenes/sSpokeToSecondBoobGirl", "S")
        
        --Common.
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "HumanF1B", "Neutral") ]])
        
        --Christine:
        if(iChristineLeadingParty >= 1.0) then
                
            --Speaking to a first boob girl:
            if(sSpokeToFirstBoobGirl == "Nobody") then
                VM_SetVar("Root/Variables/Chapter5/Scenes/sSpokeToFirstBoobGirl", "S", "HumanL")
                fnCutscene([[ Append("Human:[E|Neutral] Isn't it just a lovely day out?[P] I mean, it always is because this is climate-controlled habitat, but you know what I meant.[B][C]") ]])
                fnCutscene([[ Append("Human:[E|Neutral] Erm, are you staring?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Blush] Oh...[P] no...[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Blush] Not staring...[B][C]") ]])
                fnCutscene([[ Append("Human:[E|Neutral] Oh.[P] All right.[P] Have a good one!") ]])
            
            --Second boob girl. Must not be the first one, and no second yet.
            elseif(sSpokeToFirstBoobGirl ~= "Nobody" and sSpokeToFirstBoobGirl ~= "HumanL" and sSpokeToSecondBoobGirl == "Nobody") then
                VM_SetVar("Root/Variables/Chapter5/Scenes/sSpokeToSecondBoobGirl", "S", "HumanL")
                fnCutscene([[ Append("Human:[E|Neutral] Isn't it just a lovely day out?[P] I mean, it always is because this is climate-controlled habitat, but you know what I meant.[B][C]") ]])
                fnCutscene([[ Append("Human:[E|Neutral] So, are you two enjoying the scenery?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Blush] The...[P] scenery?[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Blush] So jiggly...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Blush] Do all breeding humans have those?[B][C]") ]])
                fnCutscene([[ Append("Human:[E|Neutral] Only if you've been a good girl![B][C]") ]])
                fnCutscene([[ Append("Human:[E|Neutral] Repeated milking causes gland growth, and it feels so good, so you need to be rewarded for good behavior.[B][C]") ]])
                fnCutscene([[ Append("Human:[E|Neutral] I do volunteer cleaning so I can get milked.[P] I think it's worth it, but it's also addictive.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Blush] I think I'm addicted...[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Neutral] How is it addictive?[B][C]") ]])
                fnCutscene([[ Append("Human:[E|Neutral] Once you get used to getting milked, they start to get sore if they don't get milked.[B][C]") ]])
                fnCutscene([[ Append("Human:[E|Neutral] But I get my tandem unit to suck on them until they feel better...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Blush] Wow...[B][C]") ]])
                fnCutscene([[ Append("Human:[E|Neutral] You should go see the milking room in the northeast, there.[P] Maybe you'll even get to see someone being milked![B][C]") ]])
                fnCutscene([[ Append("Human:[E|Neutral] Have a good one!") ]])
            
            --All other repeat cases.
            else
                fnCutscene([[ Append("Human:[E|Neutral] Isn't it just a lovely day out?[P] I mean, it always is because this is climate-controlled habitat, but you know what I meant.") ]])
            end
            fnCutsceneBlocker()

        --55:
        else
            fnCutscene([[ Append("Human:[E|Neutral] Hello, Command Unit![P] It's a lovely day, isn't it?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] In strictest terms, it is nearly midnight in your biological cycling.[P] The labs merely have their sky emulation set to daytime.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Further, the quality of the day is mandated by the climate control apparatus.[P] The delta laboratories do not emulate rainfall or cloudiness.[B][C]") ]])
            fnCutscene([[ Append("Human:[E|Neutral] Affirmative, Command Unit![B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Why did you make that observation to me, then?[B][C]") ]])
            fnCutscene([[ Append("Human:[E|Neutral] I just wanted to be friendly.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Smirk] Oh.[P] Hello, friend.[B][C]") ]])
            fnCutscene([[ Append("Human:[E|Neutral] Don't be a stranger, Command Unit![P] I love meeting new people!") ]])
        end
    
    elseif(sActorName == "HumanM") then
    
        --Variables.
        local sSpokeToFirstBoobGirl  = VM_GetVar("Root/Variables/Chapter5/Scenes/sSpokeToFirstBoobGirl", "S")
        local sSpokeToSecondBoobGirl = VM_GetVar("Root/Variables/Chapter5/Scenes/sSpokeToSecondBoobGirl", "S")
        
        --Common.
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "HumanF0B", "Neutral") ]])
        
        --Christine:
        if(iChristineLeadingParty >= 1.0) then
                
            --Speaking to a first boob girl:
            if(sSpokeToFirstBoobGirl == "Nobody") then
                VM_SetVar("Root/Variables/Chapter5/Scenes/sSpokeToFirstBoobGirl", "S", "HumanM")
                fnCutscene([[ Append("Human:[E|Neutral] Do you ever spend time just looking up research papers?[P] I don't like reading fiction.[B][C]") ]])
                fnCutscene([[ Append("Human:[E|Neutral] Uh, my eyes are up here.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Blush] They... are?[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Blush] Eyes...?[B][C]") ]])
                fnCutscene([[ Append("Human:[E|Neutral] Heh. You like what you see?[B][C]") ]])
                fnCutscene([[ Append("Human:[E|Neutral] You must not come here often, then.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Blush] What do you mean? Jiggle jiggle...[B][C]") ]])
                fnCutscene([[ Append("Human:[E|Neutral] You'll find out if you ask around.") ]])
            
            --Second boob girl. Must not be the first one, and no second yet.
            elseif(sSpokeToFirstBoobGirl ~= "Nobody" and sSpokeToFirstBoobGirl ~= "HumanM" and sSpokeToSecondBoobGirl == "Nobody") then
                VM_SetVar("Root/Variables/Chapter5/Scenes/sSpokeToSecondBoobGirl", "S", "HumanM")
                fnCutscene([[ Append("Human:[E|Neutral] Do you ever spend time just looking up research papers?[P] I don't like reading fiction.[B][C]") ]])
                fnCutscene([[ Append("Human:[E|Neutral] Uh, my eyes are up here.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Blush] They... are?[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Blush] Eyes...?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Blush] Do all breeding humans have...[P] those?[B][C]") ]])
                fnCutscene([[ Append("Human:[E|Neutral] Only if you've been a good girl![B][C]") ]])
                fnCutscene([[ Append("Human:[E|Neutral] Repeated milking causes gland growth, and it feels so good, so you need to be rewarded for good behavior.[B][C]") ]])
                fnCutscene([[ Append("Human:[E|Neutral] My academic record is exemplary, so I'm both on track for lord unit, and I get milking permits often.[P] Bit of a double-edged sword, though.[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Neutral] Double-edged sword?[P] How?[B][C]") ]])
                fnCutscene([[ Append("Human:[E|Neutral] Once you get used to getting milked, they start to get sore if they don't get milked.[B][C]") ]])
                fnCutscene([[ Append("Human:[E|Neutral] They puff up and feel like they're going to pop, so you need to call your tandem unit to milk you with her mouth.[B][C]") ]])
                fnCutscene([[ Append("Human:[E|Neutral] And my tandem unit has the same problem, so we spend a lot of time together.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Blush] Wow...[B][C]") ]])
                fnCutscene([[ Append("Human:[E|Neutral] You should go see the milking room in the northeast, there.[P] Maybe you'll even get to see someone being milked![B][C]") ]])
                fnCutscene([[ Append("Human:[E|Neutral] Have a good one!") ]])
            
            --All other repeat cases.
            else
                fnCutscene([[ Append("Human:[E|Neutral] Do you ever spend time just looking up research papers?[P] I don't like reading fiction.") ]])
            end
            fnCutsceneBlocker()

        --55:
        else
            fnCutscene([[ Append("Human:[E|Neutral] Hello, Command Unit![P] I'm just reading some physics research.[P] May I help you with anything?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] No.[P] This is not an official visit.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Smirk] But I do enjoy hearing the lark song.[B][C]") ]])
            fnCutscene([[ Append("Human:[E|Neutral] ...[P] Blue leader?[P] How...?[B][C]") ]])
            fnCutscene([[ Append("Human:[E|Neutral] Nevermind.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Since I am here, may I have your report?[B][C]") ]])
            fnCutscene([[ Append("Human:[E|Neutral] Most of the breeding population is fairly neutral on the state of the city.[P] We aren't as negatively affected by the change in industrial priority.[B][C]") ]])
            fnCutscene([[ Append("Human:[E|Neutral] We're well taken care of and our immediate superiors are kind by lord unit standards.[P] I'm afraid we won't find many recruits here.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Except for those ideologically motivated, such as yourself.[B][C]") ]])
            fnCutscene([[ Append("Human:[E|Neutral] Freedom for some is freedom for none.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Flirt] I like you a lot.[B][C]") ]])
            fnCutscene([[ Append("Human:[E|Neutral] It's best if we're not seen together, so you should go.[P] To freedom, sisters.") ]])
        end
    
    elseif(sActorName == "HumanN") then
    
        --Variables.
        local sSpokeToFirstBoobGirl  = VM_GetVar("Root/Variables/Chapter5/Scenes/sSpokeToFirstBoobGirl", "S")
        local sSpokeToSecondBoobGirl = VM_GetVar("Root/Variables/Chapter5/Scenes/sSpokeToSecondBoobGirl", "S")
        
        --Common.
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "HumanF1B", "Neutral") ]])
        
        --Christine:
        if(iChristineLeadingParty >= 1.0) then
                
            --Speaking to a first boob girl:
            if(sSpokeToFirstBoobGirl == "Nobody") then
                VM_SetVar("Root/Variables/Chapter5/Scenes/sSpokeToFirstBoobGirl", "S", "HumanN")
                fnCutscene([[ Append("Human:[E|Neutral] What is up with this level?[P] So many switchbacks -[P] argh![P] I crashed again![B][C]") ]])
                fnCutscene([[ Append("Human:[E|Neutral] Oh, sorry.[P] I didn't see you there.[P] I am authorized to play video games on the education computers during off hours.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Blush] Huh...[P] Video games...?[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Blush] Video...[P] games..?[B][C]") ]])
                fnCutscene([[ Append("Human:[E|Neutral] You're looking at my -[P] oh.[B][C]") ]])
                fnCutscene([[ Append("Human:[E|Neutral] First time in the biolabs, ladies?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Blush] ...[P] Jiggle jiggle...[B][C]") ]])
                fnCutscene([[ Append("Human:[E|Neutral] Oh you two are going to have a grand time...") ]])
            
            --Second boob girl. Must not be the first one, and no second yet.
            elseif(sSpokeToFirstBoobGirl ~= "Nobody" and sSpokeToFirstBoobGirl ~= "HumanN" and sSpokeToSecondBoobGirl == "Nobody") then
                VM_SetVar("Root/Variables/Chapter5/Scenes/sSpokeToSecondBoobGirl", "S", "HumanN")
                fnCutscene([[ Append("Human:[E|Neutral] What is up with this level?[P] So many switchbacks -[P] argh![P] I crashed again![B][C]") ]])
                fnCutscene([[ Append("Human:[E|Neutral] Oh, sorry.[P] I didn't see you there.[P] I am authorized to play video games on the education computers during off hours.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Blush] Huh...[P] Video games...?[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Blush] Video...[P] games..?[B][C]") ]])
                fnCutscene([[ Append("Human:[E|Neutral] You're looking at my -[P] oh.[B][C]") ]])
                fnCutscene([[ Append("Human:[E|Neutral] First time in the biolabs, ladies?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Blush] ...[P] Jiggle jiggle...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Blush] Do all breeding humans have...[P] those?[B][C]") ]])
                fnCutscene([[ Append("Human:[E|Neutral] My breasts?[P] Well...[P] only if you've been a good girl.[B][C]") ]])
                fnCutscene([[ Append("Human:[E|Neutral] Repeated milking causes gland growth, and it feels so good, so you need to be rewarded for good behavior.[B][C]") ]])
                fnCutscene([[ Append("Human:[E|Neutral] I make video games in my spare time, and Ms. Primrose really likes them.[P] She gave me special milking permission.[P] I go once a day![B][C]") ]])
                fnCutscene([[ Append("Human:[E|Neutral] Because if I don't...[P] ouch.[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Neutral] Ouch?[B][C]") ]])
                fnCutscene([[ Append("Human:[E|Neutral] Once you get used to getting milked, they start to get sore if they don't get milked.[B][C]") ]])
                fnCutscene([[ Append("Human:[E|Neutral] They puff up and feel like they're going to pop, so you need to call your tandem unit to milk you with her mouth.[B][C]") ]])
                fnCutscene([[ Append("Human:[E|Neutral] And since I don't have a tandem unit, I make damn sure not to miss a milking session.[P] Otherwise I'll have to use my hands and moan so loud I wake up the other units.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Blush] Wow...[B][C]") ]])
                fnCutscene([[ Append("Human:[E|Neutral] You should go see the milking room in the northeast, there.[P] Maybe you'll even get to see someone being milked![B][C]") ]])
                fnCutscene([[ Append("Human:[E|Neutral] Have a good one!") ]])
            
            --All other repeat cases.
            else
                fnCutscene([[ Append("Human:[E|Neutral] What is up with this level?[P] So many switchbacks -[P] argh![P] I crashed again![B][C]") ]])
                fnCutscene([[ Append("Human:[E|Neutral] I let my friend design a track with the level editor I made and of course she made the most brutal course ever.") ]])
            end
            fnCutsceneBlocker()

        --55:
        else
            fnCutscene([[ Append("Human:[E|Neutral] Hello, Command Unit![P] Would you like to play the racing game I'm working on?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] That is all right.[P] I do not partake in video games.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Smirk] You're missing out, hun.[P] Video games are the best thing since steamed bread.[B][C]") ]])
            fnCutscene([[ Append("Human:[E|Neutral] Of course they are![P] And all the units who play video games are the coolest around![B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] I support your hobby.[P] I just do not partake myself.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Smirk] Please continue your creative development.[B][C]") ]])
            fnCutscene([[ Append("Human:[E|Neutral] Affirmative, Command Unit!") ]])
        end
        
    elseif(sActorName == "RaijuTrainA") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Raiju:[VOICE|HumanF1] I'd like to see those mutant freaks catch me![P] I'm limber and lightning fast!") ]])
    
    elseif(sActorName == "RaijuTrainB") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Raiju:[VOICE|HumanF0] Sweat![P] Sweat![P] Yeah!") ]])
    
    elseif(sActorName == "RaijuTrainC") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Raiju:[VOICE|HumanF0] *Pant pant*[P] I shouldn't have eaten right before a workout!") ]])
    
    elseif(sActorName == "RaijuTrainD") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Raiju:[VOICE|HumanF1] Ooh, yeah, ooh![P] I love this song!") ]])
    elseif(sActorName == "1969") then
        LM_ExecuteScript(fnResolvePath() .. "Dialogue 1969.lua", "Hello")
    end
end
