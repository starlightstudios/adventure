-- |[ =================================== Narissa Shop Setup =================================== ]|
--Normal shop, no gemcutting.

-- |[Item Stock Listing]|
--Adamantite
AM_SetShopProperty("Add Item", "Adamantite Powder", -1, -1)
AM_SetShopProperty("Add Item", "Adamantite Flakes", -1, -1)

--Equipment
AM_SetShopProperty("Add Item", "Neon Tanktop", -1, -1)
AM_SetShopProperty("Add Item", "Yttrium Electrospear", -1, -1)
AM_SetShopProperty("Add Item", "Mk VI Pulse Diffractor", -1, -1)
AM_SetShopProperty("Add Item", "J-12 Freeze Carbine", -1, -1)
AM_SetShopProperty("Add Item", "Armored Gothic Corset", -1, -1)
AM_SetShopProperty("Add Item", "Recoil Dampener", -1, -1)
AM_SetShopProperty("Add Item", "Jade Eye Ring", -1, -1)

--Items
AM_SetShopProperty("Add Item", "Nanite Injection", -1, -1)
AM_SetShopProperty("Add Item", "Regeneration Mist", -1, -1)
AM_SetShopProperty("Add Item", "Emergency Medkit", -1, -1)
