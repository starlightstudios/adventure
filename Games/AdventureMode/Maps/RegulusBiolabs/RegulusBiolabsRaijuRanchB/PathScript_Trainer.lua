-- |[ ==================================== Trainer/Trainees ==================================== ]|
--Handles the trainer, Kona, and three trainees.

-- |[ ========================== Setup =========================== ]|
-- |[Variables/Constants]|
local iTicksPerFrame = 20
local saNames = {"1969", "RaijuTrainA", "RaijuTrainB", "RaijuTrainC", "RaijuTrainD", "Boombox"}

-- |[Existence Check]|
--If the NPCs don't exist, delete this script.
if(EM_Exists(saNames[1]) == false) then
    io.write("Warning: Parallel cutscene deleted due to missing NPC. Internal Name: " .. Cutscene_GetProperty("Parallel Script Name") .. "\n")
    Cutscene_HandleParallel("DELETETHIS")
    return
end

-- |[First Run]|
--If the NPCs don't have their special frames yet, apply them.
local saPaths = {"Root/Images/Sprites/Special/1969|Workout", "Root/Images/Sprites/Special/Raiju|Kick", "Root/Images/Sprites/Special/Raiju|Kick", "Root/Images/Sprites/Special/Raiju|Kick", 
                 "Root/Images/Sprites/Special/Raiju|Dance", "Root/Images/Sprites/Special/Boombox|Song"}
for i = 1, #saNames, 1 do
    EM_PushEntity(saNames[i])
        if(TA_GetProperty("Has Special Frame", "Workout0") == false) then
            for p = 0, 7, 1 do
                TA_SetProperty("Add Special Frame", "Workout"..p, saPaths[i] .. p)
            end
        end
    DL_PopActiveObject()
end

-- |[ ====================== Cutscene Work ======================= ]|
--Pattern.
local iaPattern = {0, 1, 2, 3, 0, 1, 2, 3, 0, 1, 2, 3, 0, 1, 2, 3}

--Execution.
for i = 1, #iaPattern, 1 do

    --Order all entities to set this as their frame.
    for p = 1, #saNames, 1 do
        fnCutsceneSetFrame(saNames[p], "Workout"..iaPattern[i])
    end

    --Wait until next frame.
    fnCutsceneWait(iTicksPerFrame)
    fnCutsceneBlocker()
end
