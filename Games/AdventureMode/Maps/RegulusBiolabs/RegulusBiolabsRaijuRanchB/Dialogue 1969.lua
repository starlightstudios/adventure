-- |[ ================================= Dialogue Script - 1969 ================================= ]|
--Entry point for dialogue with Kona.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[Variables]|
    local iChristineLeadingParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iChristineLeadingParty", "N")
    local iMet1969               = VM_GetVar("Root/Variables/Chapter5/1969/iMet1969", "N")
    local iMet1969Biolabs        = VM_GetVar("Root/Variables/Chapter5/1969/iMet1969Biolabs", "N")
    local iMet196955Biolabs      = VM_GetVar("Root/Variables/Chapter5/1969/iMet196955Biolabs", "N")
    
    -- |[Christine Leading]|
    --Christine is leading:
    if(iChristineLeadingParty >= 1.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "1969", "Neutral") ]])
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/1969/iMet1969Biolabs", "N", 1.0)
        
        --First meeting in biolabs:
        if(iMet1969Biolabs == 0.0) then
        
            --Did not meet in Regulus City:
            if(iMet1969 == 0.0) then
                fnCutscene([[ Append("1969:[E|Smarmy] Hey, Unit 771852 and Unit 499323![P] It's nice to meet the enemy head on like this![B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] You're the fitness unit I heard about.[P] Guess I don't have any questions about your loyalties.[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Sad] When I saw you in Sector 96, you were always so nice, Command Unit 1969.[B][C]") ]])
                fnCutscene([[ Append("1969:[E|Neutral] Just my programming, makes units want to attend my classes.[P] You mavericks are scum amongst the dregs of this city, and we'd all be better off without you.[B][C]") ]])
                fnCutscene([[ Append("1969:[E|Neutral] Unit 2856 is just more open about her disdain for units like you.[P] At least these raijus are working hard for the future of the city, not to destroy it all.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] The Administration has led to the ruin of the city with its selfish behavior.[P] Do you think rebellions just *happen*?[B][C]") ]])
                fnCutscene([[ Append("1969:[E|Neutral] Equipment wears out and is discarded.[P] If the slave units are worn out and unhappy, discard them.[P] The future won't wait for them.[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Offended] M-[P]Mean![B][C]") ]])
                fnCutscene([[ Append("1969:[E|Smarmy] Silence.[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Surprised] Yes, command unit![B][C]") ]])
                fnCutscene([[ Append("1969:[E|Smarmy] Fortunately for you, Unit 2856 has ordered me to provide my services to you.[P] If your motivators need training, I can make that happen.[B][C]") ]])
                fnCutscene([[ Append("1969:[E|Booyeah] Since we're on the same side, let's make the most of it![P] I can retire you later, for now, we move, move, move!") ]])
                fnCutsceneBlocker()
                fnCutscene([[ AM_SetProperty("Open Trainer") ]])
        
            --Met in Regulus City:
            else
                fnCutscene([[ Append("1969:[E|Smarmy] Hey, Christine![P] Gee, if I had known you were a maverick, I wouldn't have been so cordial.[P] In fact I'd probably have melted you down myself.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] Nice to see you too, Unit 1969.[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Sad] You were so nice whenever you stopped by the sector.[P] What happened?[B][C]") ]])
                fnCutscene([[ Append("1969:[E|Neutral] Just my programming, makes units want to attend my classes.[P] You mavericks are scum amongst the dregs of this city, and we'd all be better off without you.[B][C]") ]])
                fnCutscene([[ Append("1969:[E|Neutral] Unit 2856 is just more open about her disdain for units like you.[P] At least these raijus are working hard for the future of the city, not to destroy it all.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] The Administration has led to the ruin of the city with its selfish behavior.[P] Do you think rebellions just *happen*?[B][C]") ]])
                fnCutscene([[ Append("1969:[E|Neutral] Equipment wears out and is discarded.[P] If the slave units are worn out and unhappy, discard them.[P] The future won't wait for them.[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Offended] M-[P]Mean![B][C]") ]])
                fnCutscene([[ Append("1969:[E|Smarmy] Silence.[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Surprised] Yes, command unit![B][C]") ]])
                fnCutscene([[ Append("1969:[E|Smarmy] Fortunately for you, Unit 2856 has ordered me to provide my services to you.[P] Same deal as before, I can train you and your team.[B][C]") ]])
                fnCutscene([[ Append("1969:[E|Booyeah] Once you're no longer useful, you're as good as dead.[P] Until then, move, move, move!") ]])
                fnCutsceneBlocker()
                fnCutscene([[ AM_SetProperty("Open Trainer") ]])
        
            end
        
        --Repeats.
        else
            fnCutscene([[ Append("1969:[E|Smarmy] What can I do for you, filthy traitors?") ]])
            fnCutsceneBlocker()
            fnCutscene([[ AM_SetProperty("Open Trainer") ]])
        end
        
    -- |[55 Leading]|
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Tiffany", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "SX-399", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "1969", "Neutral") ]])
    
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/1969/iMet196955Biolabs", "N", 1.0)
        
        --First meeting in biolabs:
        if(iMet196955Biolabs == 0.0) then
            fnCutscene([[ Append("1969:[E|Smarmy] Unit 2855![P] Pleased to meet the new you![P] Not that you and I got along, but at least I knew what you stood for.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] I assume my sister briefed you.[B][C]") ]])
            fnCutscene([[ Append("1969:[E|Smarmy] Lost your memories, apparently, and decided to turn your back on all of us.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Upset] I am not interested in litigating my political positions with you.[P] If you have a disagreement, I invite you to take it up with my pulse diffractor.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] It's weird that you seem...[P] nice.[B][C]") ]])
            fnCutscene([[ Append("1969:[E|Booyeah] The only reason I'm not trying to retire both of you is direct orders from Unit 2856![P] In fact, if you need motivator training, I am to provide it![B][C]") ]])
            fnCutscene([[ Append("1969:[E|Neutral] Nothing would make me happier than wiping that dumb smirk off your faceplates, but alas.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Happy] I'll kill you later![P] Our battle shall be legendary![B][C]") ]])
            fnCutscene([[ Append("1969:[E|Booyeah] Looking forward to it, steam droid![P] Now, about that training...") ]])
            fnCutsceneBlocker()
            fnCutscene([[ AM_SetProperty("Open Trainer") ]])

        --Repeats.
        else
            fnCutscene([[ Append("1969:[E|Smarmy] Hey there, defective maverick scum![P] Want to move those motivators while you're still of use to us?") ]])
            fnCutsceneBlocker()
            fnCutscene([[ AM_SetProperty("Open Trainer") ]])
        end
    end
end