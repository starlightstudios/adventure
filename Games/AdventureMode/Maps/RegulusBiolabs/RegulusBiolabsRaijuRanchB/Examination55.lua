-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
-- |[Objects]|
if(sObjectName == "TerminalA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
    fnCutscene([[ Append("55:[E|Neutral] Sleeping logs for the human who uses this room.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Smirk] Do you ever sleep, 55?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] No.[P] I defragment my drives, which is also when my auto-repair systems fix joint stressing and I recharge my power core's quantum-locked energy supply.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Smirk] So, sleeping.[P] But for Command Units.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Steam Droids still refer to it as sleeping?[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Smirk] Yeah, because our memory storage mediums were analog.[P] They never got fragmented the way yours did.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Neutral] That probably had its own problems.[P] I don't know how most of my systems work exactly, but nobody fully understood how Steam Droids work.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] A natural consequence of embracing technology too quickly.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Neutral] Yeah.[P] We later learned they were arcano-nanites, but that wasn't for decades.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Do you remember being a human?[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Neutral] A little, but I was...[P] ten?[P] When I was upgraded.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Neutral] I was pretty young, which probably caused a lot of complications.[P] Most of the kids younger than me just got expelled from Regulus.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] I see.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] I am sorry.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Neutral] For what?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Down] There is a non-zero probability I was involved.[P] Due to my low unit designation, I was likely a Steam Droid working for what would later become the Administration, and would later be upgraded.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Smirk] There's a zero-probability you were involved.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Neutral] Old 55?[P] Sure.[P] But she's dead.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Smirk] New 55 is a lot nicer.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Thank you, but I do not consider my old self 'dead'.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] I am her.[P] We are the same.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Neutral] I guess so, just try not to beat yourself up about it.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] It is difficult, but thank you.") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
    fnCutscene([[ Append("55:[E|Neutral] 'Pog24:: Why is it absolutely necessary to do all this variable-manifold calculus?[P] It's not like we're going to use it ever!'[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] 'LesKing:: Because if you don't, you'll get shearing duty.[P] I don't like it either.'[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] 'Pog24:: All this stuff is useless.[P] I'm probably going to get assigned as a labour unit anyway.'[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] 'LesKing:: I know, babe.'[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Unfortunate, but likely true.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Neutral] The breeding program humans, sentenced to a life of labour for not being smart enough...[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] It upsets me.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Neutral] Hm?[P] I always thought the revolution was Christine's idea.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] It was, however, I see myself in these units.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] I can make choices for myself now, but before, I was a mere pawn of a larger power.[P] This is the situation they find themselves in.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] And since I can make choices, I choose to liberate those who cannot liberate themselves.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Neutral] However you found your way to the path, I'm glad you're on it.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Smirk] Thank you.") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
    fnCutscene([[ Append("55:[E|Neutral] 'Tolky:: Hey, you hear about that petroleum girl thing?[P] You think it's true?'[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] 'TheBlat:: Of course not, that's so stupid.[P] You pour a slime into a drone unit's chassis and seal it up, then pretend you made something new?'[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] 'Tolky:: It can transform people.[P] That means it counts.'[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] 'TheBlat:: Into what?[P] More drone units?[P] They already do that.'[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] 'Tolky:: I heard the slime turned all black.'[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] 'TheBlat:: It probably absorbed some of the rubber chassis.[P] Stop being stupid.'[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] 'Tolky:: Come on, have some fun for once.'[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] 'TheBlat:: You keep this up, you'll become a drone unit come graduation.[P] Idiot.'[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] 'Tolky:: Open your mind to new experiences!'[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] 'TheBlat:: What?[P] I'm the one who's reading books and getting day passes![P] You spend all your time in the hot tubs!'[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] The conversation continues for some time, though synthesis is not reached.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Flirt] Wow, pointless arguments on the network?[P] Big surprise there!") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
    fnCutscene([[ Append("55:[E|Neutral] A homework assignment has been left on this terminal.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Smirk] See any mistakes?[P] Maybe we could help the kid out![B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Unfortunately I am not programmed for this particular task.[P] It is likely something a repair unit could help with.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Neutral] Mu7-i/o...[P] Yeah this is way beyond me.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Smirk] Now wedging a contact beam under a pulse-diffractor's barrel and sticking a gyrostabilizer on it so it doesn't affect the recoil?[P] That I can manage.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Smirk] There is a certain appeal to practical solutions...") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalE") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
    fnCutscene([[ Append("55:[E|Neutral] Virtual-pair asymmetry radiation.[P] Presumably the next class taught here will feature that.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Neutral] Yes, but, can you prove it exists?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Regulus City has deprioritized the research of voidcraft, and I doubt they would be foolish enough to try creating a black hole to test on within the city.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] So, that will have to wait.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Neutral] Shame.[P] Nothing keeps me up at night as much as knowing whether or not black holes will eventually run out of energy and vanish.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] The microwave background radiation would provide enough energy to override the effects of virtual-pair radiation.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Neutral] Bummer.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Smirk] But I'm sure we could get a Darkmatter girl to do something if a black hole was heading this way.") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalF") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
    fnCutscene([[ Append("55:[E|Neutral] The principal, Ms. Primrose, schedules one-on-one time with the students.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Neutral] She did say they were like her family.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Raising a human from infancy to adulthood would likely cause some sort of bond to form.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Smirk] The word 'likely' is doing a lot of work in that sentence, 55!") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalG") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
    fnCutscene([[ Append("SX-399:[E|Neutral] Primrose downloads philosophical tracts while defragmenting.[P] Interesting.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Neutral] The stories we always heard about the Golems was that they were only interested in things they could use to make more frivolous luxuries for their Lords.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Philosophy is a luxury, though it is not frivolous.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Neutral] Philosophy is a basic requirement of existence.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Neutral] Food and water -[P] or energy and steam.[P] Yes, those are basic.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Smirk] But people need something to look forward to.[P] They need art, expression, belief.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Smirk] Those are just as vital.[P] They are not luxuries.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] It seems that both you and Christine value them highly enough.[P] Perhaps I should read some of them myself.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Flirt] Just ask if you want to tandem-read them, babe.") ]])
    fnCutsceneBlocker()
        
elseif(sObjectName == "TerminalH") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
    fnCutscene([[ Append("55:[E|Neutral] 'Harpy Partirhuman Spread and Habits'.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] I would suppose the Harpy colonization method is meant to serve as an allegory for human abduction for conversion.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Neutral] That, or the teacher here just really likes girls with big talon feet.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] A foot fixation?[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Flirt] It's more common than you think.") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalI") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
    fnCutscene([[ Append("55:[E|Neutral] (The storage logs are out of date and incomplete.[P] Unsurprising considering the low amount of storage space.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalJ") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
    fnCutscene([[ Append("55:[E|Neutral] Athletics records.[P] There are separate records for Raijus and Non-Raijus in the swimming competition.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Neutral] Athletes probably get picked for labour units.[P] Muscular density apparently translates to motivator density during conversion.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] How do you know this?[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Neutral] Sophie mentioned it when she was upgrading me.[P] She said she could blueprint new parts using the old ones, but they took longer to fabricate due to the high density of motivators.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Fortunately you were not too strong, or Christine may not have survived your mother's wrath.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Blush] Jeepers did she ever get mad...[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Smirk] But it all worked out in the end, as though it was suspiciously timed by a greater intelligence.[P] What a funny, irrelevant thought.") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalK") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
    fnCutscene([[ Append("55:[E|Neutral] (A tower arcology is above, and this terminal lists unit designations for the dormitories.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BoxesA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("55:[VOICE|Tiffany] (Human eating utensils.[P] Forks, spoons, knives.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BoxesB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("55:[VOICE|Tiffany] (Artistic supplies.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BoxesC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("55:[VOICE|Tiffany] (Spare sports equipment.[P] It was not laundered before storage...)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BoxesD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("55:[VOICE|Tiffany] (Extra suction cups used in the milking machinery.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BoxesE") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("55:[VOICE|Tiffany] (Broken equipment.[P] It is of no interest.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BoxesF") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
    fnCutscene([[ Append("55:[E|Neutral] Portable computers.[P] The hard drives were wiped.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Neutral] So no snooping?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Smirk] It is called 'intelligence gathering', SX-399.") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "RVDA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
    fnCutscene([[ Append("55:[E|Neutral] Homework assignments for the students to do between classes.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Neutral] Better get used to a 12-hour workday, kids.[P] At least your golem bodies will be tough enough to survive it.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Smirk] We will fix this, SX-399.") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "RVDB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
    fnCutscene([[ Append("55:[E|Neutral] The previous class was teaching optical illusions and their properties.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Neutral] My optical receptors actually don't have the same illusions that afflict humans or golems.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Flirt] Apparently they're Command-Unit grade.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Smirk] Interesting.[P] Where did 499323 find them?[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Smirk] I like to imagine she secretly lures Command Units into the basement and dismembers them when they're beyond the security network.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Neutral] But they probably just have spares because all repair bays do.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] A Slave Unit would be very unlikely to be able to subdue a Command Unit, even with the advantage of surprise.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] In addition to lower motivator density and alacrity, Command Units have discharge defenses and [P][CLEAR]") ]])
    fnCutscene([[ Append("SX-399:[E|Smirk] It was a fantasy, babe.[P] Just a fun joke.[P] Moving on...") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "RVDC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
    fnCutscene([[ Append("55:[E|Neutral] The RVD shows a list of students and recommendations for their teachers.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Both male and female students have their photos emphasizing their chest areas.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Neutral] Are you a breast girl, 55?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Please rephrase the query.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Flirt] Boobs,[P] butts,[P] faces,[P] we all have something we like.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] ...[P] May I pass on the question, then?[P] I am undecided.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Smirk] Suit yourself.[P] But if there's anything you want me to do...[B][C]") ]])
    fnCutscene([[ Append("55:[E|Smirk] Thank you for the offer.[P] Let's move on.") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "PokerTable") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
    fnCutscene([[ Append("55:[E|Neutral] A game of cards.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Smirk] This is fairly obviously a game of Dog in the last stages.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Please elaborate.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Smirk] You gain one chip every round, but the minimum number of chips to stay in the game increases after a few rounds.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Smirk] So you have to get aggressive and try to play your strong hands to get more chips.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Smirk] And if you don't have enough chips to meet the minimum, you have to bark like a dog in order to get more chips.[P] Bark three times and you lose.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] But all of the players are machines and capable of calculating the probability of a given hand.[P] The exercise would be fruitless.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Smirk] The value of a card changes in relation to the cards played.[P] You can't predict if your hand is good until about halfway through the round, but you have to bet before then.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Interesting.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Smirk] I would like to play 'Dog' with you, Christine, and Sophie sometime.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Flirt] It's a date!") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "WatergirlA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The human seems to have fallen asleep.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "WatergirlB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Girl:[VOICE|HumanF0] The water is so warm...[P] mmmmm...") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "WatergirlC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Girl:[VOICE|HumanF1] Nothing like a good soak in the hot tubs after a hard day of running around...") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "WatergirlD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Those two girls are talking about something illicit.[P] Conveniently, there are no microphones in the pool.[P] Clever.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "FabricatorA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A fabricator bench with a number of scuffs and cut marks on the top.[P] Surface scans show no traces of blood, indicating the humans here are not particularly clumsy.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "FabricatorB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The fabricator has been used for assembling pulse weaponry only recently, judging by the age of the non-standard parts at the bench.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BackersTerminal") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
    fnCutscene([[ Append("55:[E|Neutral] A terminal listing events and important figures in the history of the Raiju Ranch.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] It seems very important.[P] Christine should have a look at her earliest convenience.") ]])

--Work Terminal:
elseif(sObjectName == "WorkTerminal") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (I could access the work terminal under Christine's account, but she might be upset with me when she finds out.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Milkers") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
    fnCutscene([[ Append("55:[E|Neutral] These are the milking machines used to extract lactates from the humans.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Smirk] Hey 55, why do machine girls have breasts, anyway?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Command Unit Pattern RR-T2, of which I am a member, stores extra capacitors in the mammary chassis.[P] They are optional.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] I could remove them if you like.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Sad] No way![B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Good.[P] I prefer to keep them.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] I am a machine, but they remind me that I am a woman, or was.[P] They, and my hair, are superfluous to combat, but not to identity.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] That would not have mattered to the old me.[P] Therefore, they are worth preserving, lest I become what I was.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Neutral] But you offered to remove them?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Smirk] I am willing to change any aspect of my physical self if it would please you.[P] Your comfort matters more to me than my own ego.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Blush] Wow![B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Smirk] But I like you the way you are, so it's okay.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Blush] (Maybe get her to wear a shirt with exposed navel?[P] Or underboob?[P] I love underboob!)[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Wouldn't it be foolish if Christine were to use these machines to milk herself?[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Smirk] Sounds like something she'd do.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Smirk] Indeed it does.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Moving on.") ]])
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
