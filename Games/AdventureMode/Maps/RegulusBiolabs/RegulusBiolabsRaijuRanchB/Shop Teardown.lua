-- |[Shop Teardown]|
--Once shopping is finished, tears down the menu and stores what items the shopkeeper has in their variable.
-- This is called right before the shop menu closes.
local sItemList = AM_GetShopInventory()

--Store.
VM_SetVar("Root/Variables/Chapter5/NarissaVendor/sShopInfo", "S", sItemList)
