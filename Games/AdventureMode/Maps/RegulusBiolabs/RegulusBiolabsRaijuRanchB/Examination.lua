-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[55 Investigation]|
--If 55 is investigating, then run a different script.
local iChristineLeadingParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iChristineLeadingParty", "N")
if(iChristineLeadingParty == 0.0) then
    LM_ExecuteScript(fnResolvePath() .. "Examination55.lua", sObjectName)
    return
end

-- |[Exits]|
-- |[Objects]|
if(sObjectName == "TerminalA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Defrag -[P] er, sleeping logs for the human in this room.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] I have a vague memory of having difficulty getting to sleep as a human.[P] That's about it, though.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Switching to defragmentation mode is much easier.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Laugh] I don't miss it in the slightest!") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] 'Pog24:: Why is it absolutely necessary to do all this variable-manifold calculus?[P] It's not like we're going to use it ever!'[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] 'LesKing:: Because if you don't, you'll get shearing duty.[P] I don't like it either.'[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] 'Pog24:: All this stuff is useless.[P] I'm probably going to get assigned as a labour unit anyway.'[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] 'LesKing:: I know, babe.'[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Manifold calculus isn't useless, but I guess kids wouldn't know that.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] It's a common complaint among students.[P] I heard it a lot, too.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] What was your counterargument?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] 'You need to know enough about every topic to defend yourself, otherwise you'll fall for confidence jobs.'[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Did it work?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Laugh] No, never![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Kids who grow up rich aren't any smarter than anyone else.[P] A fool and her money are soon parted.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Earth is just loaded down with wise sayings, isn't it?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] Too many are pertaining to war or cheating, though...") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] 'Tolky:: Hey, you hear about that petroleum girl thing?[P] You think it's true?'[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] 'TheBlat:: Of course not, that's so stupid.[P] You pour a slime into a drone unit's chassis and seal it up, then pretend you made something new?'[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] 'Tolky:: It can transform people.[P] That means it counts.'[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] 'TheBlat:: Into what?[P] More drone units?[P] They already do that.'[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] 'Tolky:: I heard the slime turned all black.'[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] 'TheBlat:: It probably absorbed some of the rubber chassis.[P] Stop being stupid.'[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] 'Tolky:: Come on, have some fun for once.'[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] 'TheBlat:: You keep this up, you'll become a drone unit come graduation.[P] Idiot.'[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] 'Tolky:: Open your mind to new experiences!'[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] 'TheBlat:: What?[P] I'm the one who's reading books and getting day passes![P] You spend all your time in the hot tubs!'[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] It goes on like this, Sophie.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] These humans get along so well.") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] The unit assigned to this room left her homework on the terminal.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Ooh, made a mistake here.[P] I'll just leave a little correction...[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] She made a mistake?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] It wouldn't be a mistake if this was an R-9 chipset, but this is a Mu7-i/o chipset.[P] You know, the ones they stopped making when they started all those fires?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Offended] Then the homework assignment should be [P]'How to throw out a Mu7-i/o board',[P] not how to fix one![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] The curriculum is laughably outdated?[P] Some things never change...") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalE") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] The class curriculum is here.[P] Seems they were going over...[P][EMOTION|Christine|Scared] Oh![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] What?[P] Something wrong?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Laugh] I just realized -[P] you call it something else here![P] Silly me![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] On Earth, we called it Hawking Radiation, after the physicist who first theorized it.[P] What a brilliant fellow he was -[P] and English, I must add.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Oh, you mean virtual pair asymmetry radiation?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] I suppose it'd be a little ridiculous to name scientific concepts after the unit who discovers it.[P] Unit 392001 Radiation doesn't have the same ring to it.") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalF") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Shall we make an appointment to see the principal, Sophie?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] I don't think that's necessary.[P] Anything interesting?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Hmm, seems the principal has lots of one-on-one time with each student.[P] There are regular schedules for each of them and frequent teacher appointments.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] It's hard to say if this is good without knowing the contents of the meetings.[P] I've found that the best results come from attention to the individual.[P] Class sizes over twenty mean the teacher just can't spend enough time with each student.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] I think the largest classes here are twelve, Christine.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Colour me pleasantly surprised, then.[P] Teachers need independence and resources to teach.[P] Give them those.") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalG") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Are we snooping around the defragmentation logs of the principal for a reason, Christine?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] I must make sure she is doing a good job.[P] The future of Regulus City depends on more than just its freedom -[P] it needs educated citizens to maintain that freedom.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Did you rehearse that?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Politics does not happen on its own, it is a culture, like a garden.[P] And like any garden, without proper care, it will die.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] It seems that the Lord Units here teach independent thought rather than blind obedience.[P] Look.[P] The principal set philosophical tracts to be downloaded during defragmentation.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Breeding program humans do generally become Lord Units.[P] But, what philosophies are being downloaded?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Hmmm...[P] I don't know these names, but they are predominantly concerned with metaphysics and not ethics.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Still, teaching how to think can only be good for the city.") ]])
    fnCutsceneBlocker()
        
elseif(sObjectName == "TerminalH") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Huh, look at this.[P] 'Harpy Partirhuman Spread and Habits'.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Learning about Pandemonium?[P] Let's see...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] 'Harpies, which have many names depending on the region, are easily the most widespread of the partirhuman species of Pandemonium.'[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] 'While only rough estimates of total population exist, they are expected to be in the top ten most populous species.'[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] 'The likely candidate for most populous is the common slime girl, but harpies make up for their lack of population with sheer range.'[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] 'Harpies can survive nearly anywhere, including deserts, ice caps, or frozen mountain ranges.[P] They typically have few competitors due to their ability to fly and locate food far from their nest colonies.'[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] 'The harpy colonization pattern is common to all of the subvarieties.[P] Once a colony has enough members, the youngest members are compelled to leave and start their own colonies.'[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] 'The colony is usually established in a location that can only be reached with flight, though plains harpies are known to construct simple wooden walls and obstacles to deter invaders.'[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] 'From there, the colony locates isolated humans and attacks them, dragging them to their nest colony for transformation.'[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] Oh![P] I understand![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] This is for abductions training![P] Harpies select humans based on their skillsets, look![P] Just like we do![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] We learned from the best.[P] But harpies don't breed their own humans, so we've superceded them there.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Laugh] Maybe we should form an alliance![P] Ha ha![P] Imagine that!") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalI") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Storage logs...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] This place is a mess, but we're not much better in Sector 96 are we?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] It's not our fault![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Offended] There's not much point in organizing our inventory if we're just shipping it back out tomorrow, anyway.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Meanwhile, I think the Raiju Ranch just doesn't have enough storage space.[P] You need space to categorize things.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] They could just build another storage shed though, right?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Maybe the administrator is just really messy and doesn't care.[P] I know the type.[P] There's no getting through to them.") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalJ") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Athletics records...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Laugh] Look![P] They allow everyone to compete, even the Raijus and Biological Services![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] But they don't have mixed competitions for swimming.[P] Not with the Raijus, anyway.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] Gee.[P] I wonder.[P] Why.[P] Hee hee!") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalK") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] The dorms are located in the arcology above us.[P] This terminals lists which room each individual is housed in.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Hmm, notice this?[P] Each year is housed on a given floor, and you move down a floor each year.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] That's because the creche is on the top floor, for the little baby humans.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] Baby...[P] humans...[P] something I haven't thought about in a long time.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Christine, when all this is over...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] ...[P] Yes?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Uh, nevermind.[P] Forget I said anything.") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BoxesA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Extra silverware.[P] Forks, knives, spoons.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Silverware?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Uh, yes.[P] Did I say something wrong?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] I just find it odd that you said silver.[P] They're not made of silver, they're made out of steel.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Laugh] Must be my Earth dialect creeping in![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] On Earth, people made utensils out of silver because it is chemically inert, so the food didn't taste different if you ate with it.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] Then again, only rich people could afford that.[P] Poorer people had to eat with their hands, or wooden implements...[P] It varied based on where you were and what time you lived in...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] The reason I know this is my family had silverware dating back six-hundred years, acquired by our ancestors.[P] Probably stolen from someone they killed.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Sad] I didn't mean to upset you.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Don't worry about it.[P] I shouldn't get upset about it, it doesn't mean anything.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Just a little way how our language shapes us, I suppose.") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BoxesB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Art supplies?[P] Here?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Huh.[P] I figured you'd just draw with your PDU.[P] You can get a tablet extension.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] There's something to be said for putting a real pencil on a real page, though.[P] Besides, there's lots of wood products here in the biolabs.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] And I don't know about you, but I wouldn't want all the constant problems that those tablet extensions cause.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] What do you mean?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] The constant driver malfunctions, stuck pixels, poor sensitivity, having to reinstall a terminal's OS because of a dumb update...[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Did someone send you a broken tablet for repairs?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] I spent two whole days futzing with it before I had to give up...[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Hee hee![P] Pencil and paper it is![P] Sometimes, simplest is best.") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BoxesC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Sports equipment?[P] It smells pretty bad...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] That's the smell of sweat.[P] Humans that are overheating secrete water that evaporates, cooling the skin.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] But isn't sweat just water, then?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Organics are also coated in micro-organisms that eat their dead skin cells or other waste.[P] It's their chemical processes that make sweat smell.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] That, and some excess toxin is secreted in sweat.[P] A human who eats a lot of garlic might smell like garlic when they sweat.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Interesting.[P] Could you please repeat that?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Offended] To whichever lug nut put this sports equipment in here without washing the sweat off?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Laugh] Ha ha![P] Of course![P] I'll have my PDU send a mail right away!") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BoxesD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Huh, suction cups.[P] I wonder what they use these for...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] (I think I know, but maybe I shouldn't tell Sophie just yet...)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BoxesE") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Oh look Christine, RJ-44 Pistons.[P] And they look broken down![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] We're not here to fix things, Sophie![P] Leave that to the repair units who work here![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] My goodness, do I get excited at the prospect of fixing things?[P] What's wrong with me?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] There's nothing wrong with loving your job, Sophie.") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BoxesF") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] There's a box full of portable computers here.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Hmm, maybe these are given out to visitors?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Sad] Crud, the hard drives were wiped.[P] No snooping for secrets.[P] Oh well.") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "RVDA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Homework assignments to do before next class are printed on the screen.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Why would you require the students to do work after class is over?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Sad] To accustom them to a life dominated by work, I'd say...") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "RVDB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] The last slides in use were documenting optical illusions.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Hmm, isn't that interesting?[P] Despite our optical sensors being hundreds of times more sensitive than a human eye, golems still see the same optical illusions.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] It's because, even when you're metal, you're still a person under there.[P] It's in your brain and the way you think, not in your eye.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Or it's a software bug we could fix.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Laugh] But why would you want to?[P] Optical illusions are so much fun to play with!") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "RVDC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Hmmm?[P] The RVD has photos of the students here displaying.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Along with a list of grades and recommendations from their teachers.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] I'd say the administrator here is doing due diligence.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Yes, but what part of the anatomy are the photos focused on?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] The...[P] chest?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] That's the middle of the human, isn't it?[P] What are you getting at?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] Oh, nothing.[P] Just pointing something out.") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "PokerTable") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Looks like the staff here were playing cards.[P] I'm not familiar with what kind of game, though.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] This is Dog.[P] It's a simple card game of who has the best hand, but if you lose the hand, you have to bark like a dog.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Pretty high-stakes stuff.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] And how do you know that?[P] There weren't any card games in my database when I last synchronized it.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Sometimes I'd play in the basement of the repair bay when everyone else got off work.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] That stopped when we got a new Lord Golem, but that's because I was busy doing...[P] other things...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] You should have asked me to play![P] I'd love to lose horribly and bark like a dog!") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "WatergirlA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The human seems to have fallen asleep.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "WatergirlB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Girl:[VOICE|HumanF0] The water is so warm... mmmmm...") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "WatergirlC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Girl:[VOICE|HumanF1] Nothing like a good soak in the hot tubs after a hard day of running around...") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "WatergirlD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Those two girls are talking about something illicit.[P] Conveniently, there are no microphones in the pool.[P] Clever.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "FabricatorA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A fabricator bench with a number of scuffs and cut marks on the top.[P] Fortunately, the humans in training have safety oversight and there's no blood.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "FabricatorB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The fabricator has several non-standard parts that are newer than the rest.[P] Those parts are used to assemble pulse weaponry.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BackersTerminal") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] A list of all the units who have made important contributions to the Raiju Ranch.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] None of this would have been possible without them! Great job![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Laying it on thick, Christine?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Gotta keep them happy, right?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] But they're not here, and they can't hear us.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Don't be so sure of that...[B][C]") ]])
	fnCutscene([[ WD_SetProperty("Activate Topics After Dialogue", "BackersRaiju", "Leave") ]])

--Work Terminal:
elseif(sObjectName == "WorkTerminal") then
    LM_ExecuteScript(gsRoot .. "Chapter 5/Scenes/300 Standards/Work Terminal/Execution.lua")

--Milking.
elseif(sObjectName == "Milkers") then

    --Variables.
    local sChristineForm         = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
    local sSpokeToFirstBoobGirl  = VM_GetVar("Root/Variables/Chapter5/Scenes/sSpokeToFirstBoobGirl", "S")
    local sSpokeToSecondBoobGirl = VM_GetVar("Root/Variables/Chapter5/Scenes/sSpokeToSecondBoobGirl", "S")
    local iBiolabsGotMilked      = VM_GetVar("Root/Variables/Chapter5/Scenes/iBiolabsGotMilked", "N")
    
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])

    --No idea what these are:
    if(sSpokeToSecondBoobGirl == "Nobody") then
        fnCutscene([[ Append("Christine:[E|Neutral] What a curious machine.[P] Any idea what they are?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] I'm sure somebody around here can tell us.") ]])
    
    --Oh ho!
    else
    
        --Golem:
        if(sChristineForm == "Golem") then
            fnCutscene([[ Append("Christine:[E|Neutral] A milking machine...[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Smirk] If you want to try it out, you'll need to come back in your squishy form.") ]])
            
        --Raiju:
        elseif(sChristineForm == "Raiju") then
            fnCutscene([[ Append("Christine:[E|Neutral] A milking machine...[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Smirk] But not if you are in your electrical form, Christine.[P] For you, it's just a comfy chair.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Smirk] I don't think Ms. Primrose would like you blowing up her ranch.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Blush] And damage such vital equipment?[P] Perish the thought!") ]])
        
        --Human:
        else
            fnCutscene([[ Append("Christine:[E|Neutral] A milking machine...[B][C]") ]])
            if(iBiolabsGotMilked == 0.0) then
                fnCutscene([[ Append("Sophie:[E|Smirk] Want to try it out, dearest?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Blush] W-[P]well...[BLOCK]") ]])
            else
                fnCutscene([[ Append("Sophie:[E|Smirk] Think you've got it in you for another go?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Blush] Somehow, I think I could survive it.[P] Should I?[BLOCK]") ]])
    
            end

            --Decision script is this script. It must be surrounded by quotes.
            local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
            fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"Yes\") ")
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"No\") ")
            fnCutsceneBlocker()
        end

    end

-- |[Milking Answers]|
elseif(sObjectName == "Yes") then
	WD_SetProperty("Hide")
    
    VM_SetVar("Root/Variables/Chapter5/Scenes/iBiolabsGotMilked", "N", 1.0)
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Fire it up, Sophie![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] Yippee!") ]])
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Run the cutscene.
    LM_ExecuteScript(fnResolvePath() .. "Milking Scene.lua")
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Blush] ...[P] Phew...[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] So smooth and creamy...[P] I want more...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Blush] I would love to, but give me a few minutes to recover.[P] This is more tiring than fighting for my life![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] Okay![P] Just come back if you want to go again!") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "No") then
	WD_SetProperty("Hide")
    
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Next time.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] I can't wait...") ]])

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
