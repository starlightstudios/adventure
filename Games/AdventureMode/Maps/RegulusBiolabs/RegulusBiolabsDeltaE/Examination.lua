-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
-- |[Objects]|
if(sObjectName == "TerminalA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('I'm formally contesting a reprimand I received for washing my hands in the fluid infiltration testing range.[P] What was I supposed to do?')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Yes, we do have water for washing filth off in the aviary.[P] Do you know where the only tap is located?[P] At the back of the chicken coop -[P] you know, the filthy place?[P] I get gunk on my hands just making my way out!')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('I'll install the extra piping myself if it means I don't have to walk around with chicken crap all over my chassis.[P] Just let me do something about it, the stuff is just nasty!')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('I got the evacuation notice, too.[P] I'm on my way to the Raiju Ranch.[P] It said to leave everything where it is.[P] Hope the alert is lifted before feeding time, because those chickens get ornery if they aren't fed.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Some idiot sent me an article asking about second-order transformations.[P] You know, making a partirhuman out of a partirhuman?[P] I think it's total nonsense.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Every single thing we know about the universe says it's a one-time, one-way thing.[P] Frankly I'd be a little creeped out if I, a noble machine, could be transformed into some quivering mass of slime.[P] Pathetic.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Sorry I had to knock off early today.[P] Noma wanted to get together with some of the golems from Sector 12.[P] Don't you know?[P] The sun's coming up in a few hours!')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalE") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (An inventory of the stocks in the aviary's storage building.[P] Diatomaceous earth is on back order.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Oilmaker") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The golems here have been flavouring the oil with eggs, because the chickens lay far too many to keep up with.[P] It smells...[P] like an acquired taste.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "RVD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Care and Feeding of Our Feathered Friends' is playing.[P] It's meant for visitors to the aviary, and shows how to not snap their spines.[P]  Organics sure are fragile!)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Door") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The door is locked, and I can hear far more clucking than I'd like in there.[P] I've never done well around chickens.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Note") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('My dearest Unit 399838::[P] If the smell of chicken feces disturbs you so, disable your olfactory sensors.[P] I'm not reassigning you.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Cage") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (An enclosed terrarium containing many types of grasshopper.[P] It seems the golems are experimenting using insect protein as animal feed.)") ]])
    fnCutsceneBlocker()
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
