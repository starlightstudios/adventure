-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
-- |[Objects]|
if(sObjectName == "Sign") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Hydroponics and Datacore:: West.[P]\\nGamma Labs:: North.)") ]])
    
elseif(sObjectName == "WaterTerminal") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Oceanic Aquatic Habitat Alpha::[P] Status, green.[P] Chemical checks normal.[P] Life form balance green.[P] Aquatic oxygen levels normal, next monitor pulse in 13 minutes.)") ]])
    
elseif(sObjectName == "RVD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] 'The Juni Swallow, so named for its regal character, is often portrayed in folklore as a fierce predatory bird.[P] In fact, it consumes primarily fruit which, with the exception of the durian, does not put up much of a fight.'[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Looks like a nature documentary is playing, recorded right here in the Biolabs!)") ]])
    
elseif(sObjectName == "TerminalA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Plain water solution, no fertilizer.[P] No pesticides, manual removal of ambient pests.[P] Growth rate 'baseline', used as reference for other plant specimens.)") ]])
    
elseif(sObjectName == "TerminalB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Nitrate rich water solution, no solid fertilizer, no pesticides, manual pest removal.[P] Growth rate above baseline, recommend further research into nitrate watering solutions.)") ]])
    
elseif(sObjectName == "TerminalC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Plain water solution, no fertilizer.[P] Use of pesticides rather than manual removal of pests.[P] Growth rates well below baseline, vastly reduced labor requirements.[P] Possible for mass production or use in times of labour shortages?[P] Investigate.)") ]])
    
elseif(sObjectName == "TerminalD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Plain water solution, nitrogen-rich soil fertilizer derived from animal droppings.[P] No pesticides manual removal of ambient pests.[P] Growth rates well above baseline, but use of externally derived fertilizers unsustainable barring extensive mining of potash or other fertilizers.[P] Reserve usage for possible food shortages recommended.)") ]])
    
elseif(sObjectName == "Wall") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Someone tore a section of the wall off of the power station.[P] The wiring is exposed but doesn't seem to be damaged.[P] A hacking attempt, maybe?)") ]])
    
elseif(sObjectName == "Boat") then

    --Variables.
    local iBoatShortcut = VM_GetVar("Root/Variables/Chapter5/Scenes/iBoatShortcut", "N")
    
    --Not open yet:
    if(iBoatShortcut == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (There's a boat here, but the sluice gates to the east are closed.[P] I can probably use this boat to get to the Delta labs if I can open them...)") ]])
        fnCutsceneBlocker()

    --Opened:
    else
        WD_SetProperty("Show")
        Append("Thought:[VOICE|Leader] (Use the boat to cross to the Delta Laboratories?)[BLOCK]")
        
        --Activate and set.
        local sDestScript = LM_GetCallStack(0)
        WD_SetProperty("Activate Decisions")
        WD_SetProperty("Add Decision", "Yes", sDestScript, "Yes")
        WD_SetProperty("Add Decision", "No",  sDestScript, "No")

    end

-- |[Crossing the Lake]|
elseif(sObjectName == "Yes") then
	WD_SetProperty("Hide")
    
    --Variables.
    local i55ComplainedBoat = VM_GetVar("Root/Variables/Chapter5/Scenes/i55ComplainedBoat", "N")
    if(i55ComplainedBoat == 0.0) then
    
        --Flag
        VM_SetVar("Root/Variables/Chapter5/Scenes/i55ComplainedBoat", "N", 1.0)
    
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        fnCutscene([[ Append("Christine:[E|Smirk] Okay everyone, grab an oar![B][C]") ]])
        fnCutscene([[ Append("55:[E|Upset] Is this absolutely necessary?[P] Why don't we simply use your teleportation ability?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] This is faster.[P] C'mon.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Upset] Are you absolutely sure?[P] What if the boat capsizes?[P] That crude wooden device looks poorly maintained.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Are you joking?[P] What rich girl wasn't part of the rowing team in school?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Okay a lot of them, but not this one![P] I'm a seasoned mariner.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] So long as I have your assurances...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] It'll be okay, I know what I'm doing.") ]])
        fnCutsceneBlocker()
    
        --Transition.
        fnCutscene([[ AL_BeginTransitionTo("RegulusBiolabsDeltaH", "FORCEPOS:39.5x13.0x0") ]])
    
    else
        AL_BeginTransitionTo("RegulusBiolabsDeltaH", "FORCEPOS:39.5x13.0x0")
    
    end
    
elseif(sObjectName == "No") then
	WD_SetProperty("Hide")
    
-- |[Skillbook]|
elseif(sObjectName == "Skillbook") then
    AL_SetProperty("Set Layer Disabled", "SkillbookIco", true)
	LM_ExecuteScript(gsSX399Skillbook, 0)

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
