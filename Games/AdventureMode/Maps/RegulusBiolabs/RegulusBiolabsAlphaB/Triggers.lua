-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
if(sObjectName == "ChangeMusic") then
    local iSawMusicComments = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawMusicComments", "N")
    if(iSawMusicComments == 0.0) then
        
        --Topics for 55.
        WD_SetProperty("Unlock Topic", "Biolabs_2856", 1)
        WD_SetProperty("Unlock Topic", "Biolabs_Biolabs", 1)
        WD_SetProperty("Unlock Topic", "Biolabs_Biology", 1)
        WD_SetProperty("Unlock Topic", "Biolabs_Magic", 1)
        WD_SetProperty("Unlock Topic", "Biolabs_Runestones", 1)
        
        --Topics for SX-399.
        WD_SetProperty("Unlock Topic", "Biolabs_Latex", 1)
        WD_SetProperty("Unlock Topic", "Biolabs_Oxygen", 1)
        
        --Topics for Sophie
        local iHasElectrospriteForm = VM_GetVar("Root/Variables/Global/Christine/iHasElectrospriteForm", "N")
        WD_SetProperty("Unlock Topic", "Biolabs_Runestone", 1)
        WD_SetProperty("Unlock Topic", "Biolabs_Tourism", 1)
        if(iHasElectrospriteForm == 1.0) then
            WD_SetProperty("Unlock Topic", "Biolabs_Electrosprites", 1)
        end
        
        --Flag. Allows warps.
        VM_SetVar("Root/Variables/Chapter5/WorldState/iNoWarpsAllowed", "N", 0.0)
        
        --Variables.
        local iSX399IsFollowing   = 0
        if(fnIsCharacterPresent("SX-399")) then iSX399IsFollowing = 1.0 end
        local iSpokeTo2856Biolabs = VM_GetVar("Root/Variables/Chapter5/Scenes/iSpokeTo2856Biolabs", "N")
        local sChristineForm      = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSawMusicComments", "N", 1.0)
        
        --Dialogue
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        
        --Didn't talk to 2856? Dialogue changes.
        if(iSpokeTo2856Biolabs == 0.0) then
            
            --Flag.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iSpokeTo2856Biolabs", "N", 1.0)
        
            --Dialogue.
            fnCutscene([[ Append("Christine:[E|PDU] Hmm?[P] I'm getting a call.[B][C]") ]])
            fnCutscene([[ Append("2856:[VOICE|2856] Oh, hello? Is anyone there?[P] I'm terrible sorry to interrupt...[B][C]") ]])
            fnCutscene([[ Append("2856:[VOICE|2856] BECAUSE YOU STUPID DONGLES DECIDED YOU DIDN'T NEED TO TALK TO ME BEFORE JUST WALTZING PAST ME YOU WORTHLESS - [P][CLEAR]") ]])
            fnCutscene([[ Append("Christine:[E|PDU] PDU, volume levels to five percent.[B][C]") ]])
            fnCutscene([[ Append("PDU: I'm sorry, Christine, volume levels were already at five percent.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|PDU] Well then, one percent?[B][C]") ]])
            fnCutscene([[ Append("2856:[VOICE|2856] WHY DON'T YOU JUST TURN ME OFF INSTEAD OF DAMPENING ME, HMM?[P] NOTHING IMPORTANT I COULD SAY TO YOU?[B][C]") ]])
            fnCutscene([[ Append("2856:[VOICE|2856] Since obstinacy is your one talent, I'll be as brief as I can.[B][C]") ]])
        
        --Otherwise:
        else
            fnCutscene([[ Append("Christine:[E|PDU] Hmm?[P] I'm getting a call.[B][C]") ]])
            fnCutscene([[ Append("2856:[VOICE|2856] Christine, there's something you should know before you go any further.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|PDU] You're sorry for all the terrible things you did?[B][C]") ]])
            fnCutscene([[ Append("2856:[VOICE|2856] Don't interrupt me you stupid - [P][CLEAR]") ]])
            fnCutscene([[ Append("Christine:[E|PDU] A girl can dream...[B][C]") ]])
            fnCutscene([[ Append("2856:[VOICE|2856] Since obstinacy is your one talent, I'll be as brief as I can.[B][C]") ]])
        end
        
        --Common dialogue.
        fnCutscene([[ Append("2856:[VOICE|2856] Network access in the Biolabs is spotty, for longstanding reasons.[P] Normally, we'd communicate with radio, but I'd prefer if you didn't.[B][C]") ]])
        fnCutscene([[ Append("2856:[VOICE|2856] We can't know for certain who is listening in.[P] Only contact me via network stations.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] Listening in?[P] Does she mean the rebels?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Unlikely.[P] She means the invaders, I would assume.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] 55's right.[P] Those creatures can metabolize radio waves.[P] Don't ask me how they do that, but they can do that.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] I'm more interested in how you know that...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] It just...[P] I just know it.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] (Because I am one...[P] I'm just still in denial about it...)[B][C]") ]])
        fnCutscene([[ Append("2856:[VOICE|2856][EMOTION|Christine|PDU] Whatever the reason, don't use radio waves for communication except at short ranges.[B][C]") ]])
        fnCutscene([[ Append("2856:[VOICE|2856] Your objective is the Raiju Ranch, north-east of your position.[P] You'll need to go through the Gamma habitat, then go east to the Delta habitat.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Understood.[P] Moving out.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|PDU] Actually...[P] PDU?[P] I don't want to listen to dour music all the time.[B][C]") ]])
        fnCutscene([[ Append("PDU:[VOICE|PDU] Affirmative, Christine.[P] Will this do?") ]])
        fnCutsceneBlocker()
        
        --Change the music.
        fnCutscene([[ AL_SetProperty("Music", "Biolabs") ]])
        fnCutsceneWait(425)
        fnCutsceneBlocker()
        
        --Dialogue
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Tiffany", "Neutral") ]])
        if(iSX399IsFollowing == 1.0) then
            fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "SX-399", "Neutral") ]])
        end
        
        --Dialogue.
        fnCutscene([[ Append("Christine:[E|Smirk] Thank you, PDU.[P] This will do nicely.[B][C]") ]])
        
        --Changes if SX-399 is in the party.
        if(iSX399IsFollowing == 1.0) then
            fnCutscene([[ Append("SX-399:[E|Smirk] Music?[P] You mean you listen to smooth beats while vaporizing critters?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] A fellow enthusiast?[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Smirk] Nope![P] RJ-45 said she'd box my ears if she caught me listening to music on a mission![B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] RJ-45 is practical.[P] You should follow her advice.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Smirk] Ha![P] I just never let her catch me![B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Flirt] This girl's got digital recording and playback.[P] Nobody but me can tell.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Happy] Christine likes synthwave?[P] Oooh![B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Smirk] Classical, myself, but I can respect your tastes.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Angry] During a combat situation, we should not distract ourselves with pointless diversions.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] You don't do music?[P] You're missing out, 55.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Besides, I think it actually enhances my combat performance.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Smirk] Kicking butt to a beat.[P] Seems like something you might want to research, cutie-five.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Offended] Cutie...[P] Five?[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Smirk] Oh, a reaction?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Smirk] That name...[P] will do.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Smirk] Very well, Christine.[P] I will research the effects of music on combat efficiency at a later date.[P] For now, proceed with caution.[B][C]") ]])
        
        --Default.
        else
            fnCutscene([[ Append("55:[E|Neutral] Music?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] I like to have music playing in my head while we're out in the field.[B][C]") ]])
            if(sChristineForm == "Human") then
                fnCutscene([[ Append("55:[E|Neutral] Organics cannot play music in their heads...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Laugh] Of course we can, silly![P] I just have to hear it and then I can memorize it![B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] (I'm glad she didn't ask why I was thinking of spooky music this whole time...)[B][C]") ]])
            elseif(sChristineForm == "Darkmatter") then
                fnCutscene([[ Append("Sophie:[E|Neutral] Can you do that with a head that's made of...[P] whatever you're made of?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Laugh] Of course I can, silly![P] There aren't many things I *can't* do![B][C]") ]])
            else
                fnCutscene([[ Append("55:[E|Upset] I object to the usage of music for entertainment while in a dangerous situation.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] It has worked out so far, hasn't it?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] Stop being a worrywart.[P] I switch it to combat music when we get attacked.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Angry] ...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] You're not going to get far by arguing the point.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Upset] Evidently.[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Happy] Synthwave...[P] She has good taste![B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Is there any genre more appropriate?[P] I think not![B][C]") ]])
            end
        end
        fnCutscene([[ Append("Christine:[E|Smirk] Okay then.[P] Let's move out!") ]])
        fnCutsceneBlocker()
    end
end
