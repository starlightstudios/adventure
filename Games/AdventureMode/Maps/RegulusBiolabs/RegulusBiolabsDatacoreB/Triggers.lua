-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
if(sObjectName == "SceneTriggerA") then
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iDatacoreNeedsTeleport", "N", 0.0)
    
    --Variables
    local iDatacoreSawScaredGolem = VM_GetVar("Root/Variables/Chapter5/Scenes/iDatacoreSawScaredGolem", "N")
    if(iDatacoreSawScaredGolem == 0.0) then
        
        --Variables.
        local bIsSX399Present = fnIsCharacterPresent("SX-399")
        local sChristineForm  = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iDatacoreSawScaredGolem", "N", 1.0)
        
        --Scene.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|GolemLord] ^Who's there?[P] Who are you?[P] Please don't hurt me!^") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Space out.
        fnCutsceneMove("Christine", 30.25, 24.50)
        fnCutsceneFace("Christine", 0, -1)
        fnCutsceneMove("Tiffany", 29.25, 24.50)
        fnCutsceneFace("Tiffany", 0, -1)
        fnCutsceneMove("Sophie", 31.25, 24.50)
        fnCutsceneFace("Sophie", 0, -1)
        if(bIsSX399Present == true) then
            fnCutsceneMove("SX-399", 32.25, 24.50)
            fnCutsceneFace("SX-399", 0, -1)
        end
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue setup.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
        
        --Varies by form.
        if(sChristineForm == "Golem") then
            fnCutscene([[ Append("Golem:[E|Neutral] ^Are you security units?[P] You must help!^[B][C]") ]])
            fnCutscene([[ Append("55:[E|Upset] ^We are under no obligations to help you.[P] We need to pass through the datacore's offices.[P] Give us your access codes.^[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] ^Ease off a tick, 55.^[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] ^I -[P] I can't give you my access codes![P] I don't have the keycard!^[B][C]") ]])
            
        elseif(sChristineForm == "LatexDrone") then
            fnCutscene([[ Append("Golem:[E|Neutral] ^Are you security units?[P] You must help!^[B][C]") ]])
            fnCutscene([[ Append("55:[E|Upset] ^We are under no obligations to help you.[P] We need to pass through the datacore's offices.[P] Give us your access codes.^[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] ^Ease off a tick, 55.^[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] ^Did that Drone Unit -[P] nevermind.[P] Not important.^[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] ^I can't give you my access codes![P] I don't have the keycard.^[B][C]") ]])
            
        elseif(sChristineForm == "Electrosprite") then
            fnCutscene([[ Append("Golem:[E|Neutral] ^A -[P] what are you?^[B][C]") ]])
            fnCutscene([[ Append("55:[E|Upset] ^Security units.[P] That is all you need to know.[P] We require datacore access codes.[P] Give them to us.^[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] ^Slow down there, 55.^[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] ^Are you -[P] no, no.[P] Trust my Command Unit's judgement.^[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] ^I'm sorry, I can't give you my access codes.[P] I don't have the keycard.^[B][C]") ]])
            
        elseif(sChristineForm == "SteamDroid") then
            fnCutscene([[ Append("Golem:[E|Neutral] ^Help -[P] you must help![P] Please.[P] I know our peoples are in conflict, but we have mutual enemies now![P] I can see to it you are rewarded!^[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] ^Straight to buying me off, eh?^[B][C]") ]])
            fnCutscene([[ Append("55:[E|Upset] ^These are my security squad.[P] Unorthodox, but effective.[P] That is all you need to know.[P] We require datacore access codes.[P] Give them to us.^[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] ^Are you -[P] no, no.[P] Trust my Command Unit's judgement.^[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] ^I'm sorry, I can't give you my access codes.[P] I don't have the keycard.^[B][C]") ]])
            
        elseif(sChristineForm == "Darkmatter") then
            fnCutscene([[ Append("Golem:[E|Neutral] ^Command Unit, please help![P] I -[P] is that Darkmatter following you?^[B][C]") ]])
            fnCutscene([[ Append("55:[E|Upset] ^Do not become so easily distracted.[P] We require datacore access codes.[P] Give them to us.^[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] ^I am a member of the security entourage of this Command Unit, for the time being, hardmatter.^[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] ^Is this -[P] I'm sorry, but the scientific leaps -[P] no no.[P] I must contain myself, and deal with the task at hand.^[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] ^I'm sorry, I can't give you my access codes.[P] I don't have the keycard.^[B][C]") ]])
        
        elseif(sChristineForm == "Doll") then
            fnCutscene([[ Append("Golem:[E|Neutral] ^Command units![P] Please help!^[B][C]") ]])
            fnCutscene([[ Append("55:[E|Upset] ^We are under no obligations to help you.[P] We need to pass through the datacore's offices.[P] Give us your access codes.^[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] ^Ease off a tick, 55.^[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] ^I -[P] I can't give you my access codes![P] I don't have the keycard!^[B][C]") ]])
        
        elseif(sChristineForm == "Secrebot") then
            fnCutscene([[ Append("Golem:[E|Neutral] ^Command unit![P] Please help!^[B][C]") ]])
            fnCutscene([[ Append("55:[E|Upset] ^We are under no obligations to help you.[P] We need to pass through the datacore's offices.[P] Give us your access codes.^[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] ^Ease off a tick, 55.^[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] ^You are -[P] do you -[P] command unit, are you taking orders from a secrebot?^[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] ^We're mavericks.[P] Equals.[P] She's my friend.^[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] ^I -[P] I can't give you my access codes![P] I don't have the keycard![P] Please don't hurt me!^[B][C]") ]])
        end

        fnCutscene([[ Append("55:[E|Upset] ^Then you are useless.[P] Stand aside.^[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] ^Ma'am, we have an important objective, more important than rescuing you.[P] The fate of the city rests on us.^[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] ^(Christine, why are you lying?[P] She's one of the bad robots, isn't she?)^[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] ^(She's a potential ally, or a prisoner.[P] Even if you hate them, you can't mistreat them.)^[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Sad] ^(But I don't hate her...[P] Even though I should, I just can't...)^[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] ^(We'll need to talk more about this later.[P] For now, this place is dangerous.[P] We need to keep moving.)^[B][C]") ]])
        fnCutscene([[ Append("Golem:[E|Neutral] ^The elevator broke when we were doing a routine upgrade on the server.[P] I checked the comms and there's an evacuation notice.^[B][C]") ]])
        fnCutscene([[ Append("Golem:[E|Neutral] ^Geneva -[P] I mean, Unit 702339, went to check if she could find an access ladder.[P] I heard a distress call on the radio and...[P] she didn't come back...^[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] ^I presume, then, that this Unit 702339 has the access card we need.^[B][C]") ]])
        fnCutscene([[ Append("Golem:[E|Neutral] ^Yes, we always keep a spare if we get locked out.^[B][C]") ]])
        fnCutscene([[ Append("Golem:[E|Neutral] ^But what should I do?[P] There are[P] - things -[P] all around![P] They could get me!^[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] ^Your offices are over there, right?[P] Go in, drop the door's deadbolts, and lock yourself off.[P] Wait for rescue.^[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] ^I'm sure you can find a copy of solitaire to entertain yourself with.^[B][C]") ]])
        fnCutscene([[ Append("Golem:[E|Neutral] ^C-[P]can't I come with you?^[B][C]") ]])
        if(bIsSX399Present == true) then
            fnCutscene([[ Append("SX-399:[E|Smirk] ^Sorry, toutse.[P] Can't have a civilian in my firing line.^[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] ^What my mercenary friend here means is, you are in greater danger with us than without.^[B][C]") ]])
        else
            fnCutscene([[ Append("Sophie:[E|Neutral] ^I think it's best if you...[P] uh...[P] don't.^[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] ^I was not speaking to you.^[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] ^My [P]- lieutenant? -[P] is correct.[P] You are in greater danger with us than without.^[B][C]") ]])
        end
        
        fnCutscene([[ Append("Christine:[E|Neutral] ^It is unlikely you will be able to evade capture.[P] Barricade yourself in place.^[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] ^If you do need to leave in a hurry, everyone is rallying at the Raiju Ranch.[P] Go there.^[B][C]") ]])
        fnCutscene([[ Append("Golem:[E|Neutral] ^And...[P] Geneva?^[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] ^Likely retired.^[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] ^55!^[B][C]") ]])
        fnCutscene([[ Append("Golem:[E|Neutral] ^No, no.[P] I must accept that. I will be delighted if she is not, but...[P] this is a crisis.[P] I cannot allow my feelings to make me another victim.^[B][C]") ]])
        fnCutscene([[ Append("Golem:[E|Neutral] ^Thank you, security units.[P] I'll lock myself in my office and wait for rescue.^") ]])
        fnCutsceneBlocker()
        
        --Fold party.
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnAutoFoldParty()
        fnCutsceneBlocker()
    end

--Depth teleporter.
elseif(sObjectName == "DepthTeleport") then

    local iDatacoreNeedsTeleport = VM_GetVar("Root/Variables/Chapter5/Scenes/iDatacoreNeedsTeleport", "N")
    if(iDatacoreNeedsTeleport == 1.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iDatacoreNeedsTeleport", "N", 0.0)
        Cutscene_CreateEvent("ActorEvent", "Actor")
            ActorEvent_SetProperty("Subject Name", "Christine")
            ActorEvent_SetProperty("Teleport To", 28.25 * gciSizePerTile, 29.50 * gciSizePerTile, 0)
        DL_PopActiveObject()
        fnAutoFoldParty()
    end
end
