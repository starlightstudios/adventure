-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
if(sObjectName == "ToGeneticsE") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsGeneticsE", "FORCEPOS:37.5x27.0x0")
    
-- |[Objects]|
elseif(sObjectName == "TerminalA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Population listings for the species in this habitat.[P] There are dolphins and seals and a lot of fish I don't recognize, but it seems they're in hiding since the shooting started.)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "TerminalB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Spending twelve hours a day underwater is simply ruining my hair.[P] Tell those clever engineers to come up with a cover that will keep my hair dry down here.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (I wonder if this brilliant researcher ever through of a diving helmet.)") ]])
    fnCutsceneBlocker()
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
