-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
-- |[Objects]|
if(sObjectName == "ToGammaA") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsGammaA", "FORCEPOS:29.0x45.0x0")
    
elseif(sObjectName == "TerminalA") then
    
    --Variables
    local iGammaPowerRestored = VM_GetVar("Root/Variables/Chapter5/Scenes/iGammaPowerRestored", "N")
    if(iGammaPowerRestored == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("PDU:[VOICE|Narrator] Scanning local drives...[P] done![B][C]") ]])
        fnCutscene([[ Append("PDU:[VOICE|Narrator] The local drive contains a list of units going to the Epsilon labs, and coming back.[P] A note is placed on the file of Unit 2856 indicating she is not required to check in beforehand.[P] Slave Units were informed to allow her free passage.") ]])
        fnCutsceneBlocker()
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (The local drive contains a list of units going to the Epsilon labs, and coming back.[P] A note is placed on the file of Unit 2856 indicating she is not required to check in beforehand.[P] Slave Units were informed to allow her free passage.)") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "TerminalB") then
    
    --Variables
    local iGammaPowerRestored = VM_GetVar("Root/Variables/Chapter5/Scenes/iGammaPowerRestored", "N")
    if(iGammaPowerRestored == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("PDU:[VOICE|Narrator] Scanning local drives...[P] done![B][C]") ]])
        fnCutscene([[ Append("PDU:[VOICE|Narrator] This terminal is for inputting credentials when a unit is being transferred to the Epsilon labs.[P] Beta labs access is not restricted.") ]])
        fnCutsceneBlocker()
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (This terminal is for inputting credentials when a unit is being transferred to the Epsilon labs.[P] Beta labs access is not restricted.)") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "Sign") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Sheep Sleep Pen.'[P] Looks like the shepherd isn't around.[P] Seems the sheep have been left alone by the creatures, though.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Shelf") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A broken pocket flash light.[P] Probably for the organics in the Biolabs, this one was probably dropped by their clumsy, clumsy hands.)") ]])
    fnCutsceneBlocker()
    
-- |[Skillbook]|
elseif(sObjectName == "Skillbook") then
    AL_SetProperty("Set Layer Disabled", "SkillbookIco", true)
	LM_ExecuteScript(gsSX399Skillbook, 2)
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
