-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
if(sObjectName == "SampleExit") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsGammaA", "FORCEPOS:29.0x45.0x0")
    
-- |[Objects]|
elseif(sObjectName == "Barrels") then

    --Variables
    local iRaibieFoundDrums = VM_GetVar("Root/Variables/Chapter5/Scenes/iRaibieFoundDrums", "N")
    local iRaibieQuest      = VM_GetVar("Root/Variables/Chapter5/Scenes/iRaibieQuest", "N")
    local iSX399JoinsParty  = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
    
    --Haven't found these yet:
    if(iRaibieFoundDrums == 0.0) then
    
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iRaibieFoundDrums", "N", 1.0)
        
        --Player is not far enough along the quest to know what this is:
        if(iRaibieQuest < 3.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
            fnCutscene([[ Append("Christine:[E|Neutral] Well hello, what do we have here?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Six barrels arranged in a rectangular formation on the western half of the Gamma Laboratories.[B][C]") ]])
            if(iSX399JoinsParty == 1.0) then
                fnCutscene([[ Append("SX-399:[E|Flirt] At some point, Christine is going to figure out she shouldn't ask dumb questions.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Honestly?[P] I'm too stubborn to not be an idiot.[B][C]") ]])
            end
            fnCutscene([[ Append("Christine:[E|Offended] No, what I mean is::[P] Why are these barrels here and why are they leaking?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] See the dead grass leading right into the water supply?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Organic contaminants allowed to leak into the groundwater may not be an accident.[P] In fact, there is an infiltration testing range in the Delta Laboratories.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] [SOUND|World|TakeItem]I'm going to collect a sample anyway.[P] It might come in handy later.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|PDU] Hmmm...[P] My PDU doesn't have the best sensor suite, but the material looks carcinogenic.[B][C]") ]])
            if(iSX399JoinsParty == 1.0) then
                fnCutscene([[ Append("SX-399:[E|Neutral] It's also colourless, odorless, and dissolves in water.[B][C]") ]])
                fnCutscene([[ Append("SX-399:[E|Neutral] Screams 'bioweapon' to me.[B][C]") ]])
            end
            fnCutscene([[ Append("55:[E|Neutral] A bioweapon?[P] Perhaps one of Unit 2856's countermeasures to the invaders?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Maybe, maybe not.[P] We'll have to look into it later.[P] Let's get going.") ]])
            fnCutsceneBlocker()
        
        --Far enough along the quest:
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
            fnCutscene([[ Append("Christine:[E|Neutral] Chemical storage barrels far away from a storage depot...[P] This is suspicious.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] 55, did Dr. Maisie brief you?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Yes, she informed me via mail about your plans.[P] Do you think these barrels are related to your investigation?[B][C]") ]])
            if(iSX399JoinsParty == 1.0) then
                fnCutscene([[ Append("SX-399:[E|Neutral] They do seem out of place.[B][C]") ]])
            end
            fnCutscene([[ Append("Christine:[E|Offended] They're clearly leaking into the water supply.[P] Look, you can see a line of dead grass.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Organic contaminants allowed to leak into the groundwater may not be an accident.[P] In fact, there is an infiltration testing range in the Delta Laboratories.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] True, but this is active in an area where the Raibies are present and would serve as a vector.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|PDU] My PDU says they're a candidate...[P] but they don't match perfectly...[B][C]") ]])
            if(iSX399JoinsParty == 1.0) then
                fnCutscene([[ Append("SX-399:[E|Neutral] It's also colourless, odorless, and dissolves in water.[B][C]") ]])
                fnCutscene([[ Append("SX-399:[E|Neutral] Screams 'bioweapon' to me.[B][C]") ]])
            end
            fnCutscene([[ Append("55:[E|Neutral] Perhaps one of Unit 2856's countermeasures to the invaders?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] I don't think so.[P] Let's get this back to Dr. Maisie and see what she has to say.") ]])
            fnCutsceneBlocker()
        
        
        end
    
    --Repeat:
    else
    
        --Not far enough along the quest:
        if(iRaibieQuest < 6.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Leader] (A colourless, odorless chemical that dissolves readily in water...[P] Obvious bioweapon candidate.[P] I've already got a sample of it.)") ]])
            fnCutsceneBlocker()
        
        --Time to take a hit!
        elseif(iRaibieQuest == 6.0) then
        
            --Christine has to be a Raiju:
            local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
        
            --Not in Raiju form:
            if(sChristineForm ~= "Raiju") then
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ Append("Thought:[VOICE|Leader] (This is the right place, but I need to be a Raiju in order to dose myself with it.)") ]])
                fnCutsceneBlocker()
        
            --In Raiju form:
            else
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
                fnCutscene([[ Append("Christine:[E|Neutral] Okay team, are we ready?[B][C]") ]])
                if(iSX399JoinsParty == 1.0) then
                    fnCutscene([[ Append("SX-399:[E|Neutral] My gun is ready.[B][C]") ]])
                    fnCutscene([[ Append("55:[E|Upset] Please do not melt Christine.[B][C]") ]])
                    fnCutscene([[ Append("SX-399:[E|Smirk] You kidding?[P] Her head's so thick, I probably wouldn't get past the first layer![B][C]") ]])
                    fnCutscene([[ Append("55:[E|Smirk] I will not attempt a counterargument.[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Offended] You two are my very best friends.[B][C]") ]])
                    fnCutscene([[ Append("55:[E|Neutral] We are ready.[P] The rest is up to you.[P] Are you ready?[B][C]") ]])
                else
                    fnCutscene([[ Append("55:[E|Neutral] I am as prepared as is possible.[B][C]") ]])
                    fnCutscene([[ Append("55:[E|Neutral] The rest is up to you.[P] Are you ready?[B][C]") ]])
                end
                
                --Dialogue.
                fnCutscene([[ Append("Christine:[E|Neutral] (Shall I take a drink of E-v89-r?[P] Better make sure I'm absolutely ready.)[BLOCK]") ]])

                --Decision script is this script. It must be surrounded by quotes.
                local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
                fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
                fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"Yes\") ")
                fnCutscene(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"No\") ")
                fnCutsceneBlocker()
            end
        
        --No dice!
        elseif(iRaibieQuest == 7.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Leader] (What a bust![P] This must not have been the vector.[P] We had best go back to Dr. Maisie.)") ]])
            fnCutsceneBlocker()
        
        --Post-sequence.
        elseif(iRaibieQuest == 8.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Leader] (This nasty stuff is the cause of the Raibies outbreak.[P] Hopefully Dr. Maisie can get it under control now.)") ]])
            fnCutsceneBlocker()
        end
    end

-- |[Decisions]|
elseif(sObjectName == "Yes") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")

    --Clear censor bars.
    WD_SetProperty("Clear Censor Bars")
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iRaibieDrank", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iRaibieQuest", "N", 7.0)
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("Christine:[E|Neutral] All right... *slurp*") ]])
    fnCutsceneBlocker()
    
    --Fade.
    fnCutsceneWait(25)
    fnCutscene([[ AL_SetProperty("Activate Fade", 25, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Scene.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Activate Scene") ]])
    fnCutscene([[ Append("Christine took a tiny bit of the substance in her hands and slurped it down.[P] It burned like whisky as it went, and she tensed.[B][C]") ]])
    fnCutscene([[ Append("She stopped, and listened.[P] The area was still.[P] 55 put her hand on Christine's shoulder.[P] She was shaking.[B][C]") ]])
    fnCutscene([[ Append("After a few moments, the shaking subsided.[P] Christine burped.[P] She smiled at 55, who frowned in response.[B][C]") ]])
    fnCutscene([[ Append("Unfortunately, it seemed this was not the cause of the Raibies outbreak.[P] 55's PDU, connected to the monitoring nanites, reported no effects.[B][C]") ]])
    fnCutscene([[ Append("Christine, while slightly upset, was more relieved.[P] She wasn't going to give up just yet, but the prospect of risking becoming a raging monster still was not her ideal outcome.[P] She grinned.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Unfade.
    fnCutsceneWait(25)
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("Christine:[E|Smirk] Well that wasn't so bad, was it?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Odd.[P] Dr. Maisie's mail seemed certain this was the vector, but I am reading no effects.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Let's go pay her a visit and figure out what do next.[P] And to get me something to wash the awful taste out of my mouth!") ]])
    fnCutsceneBlocker()
    
    --Despawn enemies.
    if(EM_Exists("EnemyAA") == true) then
        EM_PushEntity("EnemyAA")
            RE_SetDestruct(true)
        DL_PopActiveObject()
    end
    if(EM_Exists("EnemyAB") == true) then
        EM_PushEntity("EnemyAB")
            RE_SetDestruct(true)
        DL_PopActiveObject()
    end
    if(EM_Exists("EnemyBA") == true) then
        EM_PushEntity("EnemyBA")
            RE_SetDestruct(true)
        DL_PopActiveObject()
    end
    
elseif(sObjectName == "No") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("Christine:[E|Neutral] Actually, not quite yet.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] As you wish.") ]])
    fnCutsceneBlocker()

-- |[Errors]|
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
