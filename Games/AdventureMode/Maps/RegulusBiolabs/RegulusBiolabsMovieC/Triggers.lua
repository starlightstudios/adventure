-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
if(sObjectName == "Overhearing") then
    
    --Repeat check.
    local iMovieOverheard = VM_GetVar("Root/Variables/Chapter5/Scenes/iMovieOverheard", "N")
    if(iMovieOverheard == 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iMovieOverheard", "N", 1.0)
    
    --Movement.
    fnCutsceneMove("Sammy", 24.25, 12.50)
    fnCutsceneFace("Sammy", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Open the door.
    fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutscene([[ AL_SetProperty("Open Door", "MovieDoor") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Sammy", -1, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneMove("Sammy", 23.25, 12.50, 2.00)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Sammy] (Yikes![P] They almost saw me!)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Camera.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 2.0)
        CameraEvent_SetProperty("Focus Position", (19.25 * gciSizePerTile), (9.50 * gciSizePerTile))
    DL_PopActiveObject()
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Golem", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemB", "Neutral") ]])
    fnCutscene([[ Append("Lefty:[E|Neutral] Hey Righty, did you hear something?[B][C]") ]])
    fnCutscene([[ Append("Righty:[E|Neutral] Something like a door opening and someone running in a panic, and the door still being open?[B][C]") ]])
    fnCutscene([[ Append("Lefty:[E|Neutral] Yeah, like that.[B][C]") ]])
    fnCutscene([[ Append("Righty:[E|Neutral] Nope.[B][C]") ]])
    fnCutscene([[ Append("Righty:[E|Neutral] So anyway, you were telling me about the secret room?[B][C]") ]])
    fnCutscene([[ Append("Righty:[E|Neutral] Sorry I keep forgetting how to access it.[B][C]") ]])
    fnCutscene([[ Append("Lefty:[E|Neutral] Oh it's no problem.[P] In fact, everyone keeps forgetting how to access it, so I wrote it on one of the signs here.[B][C]") ]])
    fnCutscene([[ Append("Righty:[E|Neutral] Gee, that seems like a security flaw.[B][C]") ]])
    fnCutscene([[ Append("Lefty:[E|Neutral] Tch, as if![B][C]") ]])
    fnCutscene([[ Append("Lefty:[E|Neutral] An intruder would have to even know about our secret base to begin with...[B][C]") ]])
    fnCutscene([[ Append("Lefty:[E|Neutral] Slip past all our crack defense teams...[B][C]") ]])
    fnCutscene([[ Append("Lefty:[E|Neutral] Get the door access code without triggering a lockdown...[B][C]") ]])
    fnCutscene([[ Append("Lefty:[E|Neutral] Know about the secret room to begin with...[B][C]") ]])
    fnCutscene([[ Append("Lefty:[E|Neutral] *And* decipher my very tricky secret code.[P] Not likely![B][C]") ]])
    fnCutscene([[ Append("Righty:[E|Neutral] Yeah, I get you.[P] Only some kind of super agent could pull that off.[B][C]") ]])
    fnCutscene([[ Append("Lefty:[E|Neutral] Exactly![B][C]") ]])
    fnCutscene([[ Append("Righty:[E|Neutral] Well, we better get back to our evil tasks.[P] What did you get?[B][C]") ]])
    fnCutscene([[ Append("Lefty:[E|Neutral] I have to go clean the evil freezers.[P] You?[B][C]") ]])
    fnCutscene([[ Append("Righty:[E|Neutral] Evil mopping.[B][C]") ]])
    fnCutscene([[ Append("Lefty:[E|Neutral] Ha ha![P] Always evil mopping![P] Your Lord Golem must think you're good at it.[B][C]") ]])
    fnCutscene([[ Append("Righty:[E|Neutral] Come on, let's go.") ]])
    fnCutsceneBlocker()
    
    --Movement.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 1.0)
        CameraEvent_SetProperty("Focus Actor Name", "Sammy")
    DL_PopActiveObject()
    fnCutsceneMove("GolemA", 10.25, 9.50)
    fnCutsceneMove("GolemA", 10.25, 8.50)
    fnCutsceneMove("GolemB", 11.25, 9.50)
    fnCutsceneMove("GolemB", 11.25, 8.50)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutsceneTeleport("GolemA", -1.25, -1.50)
    fnCutsceneTeleport("GolemB", -1.25, -1.50)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] Guess those evil goons didn't count on me![P] All I have to do is decipher a secret code using my agent wits, and I'll be a free moth!") ]])
    fnCutsceneBlocker()

-- |[Secret Room Unlocking]|
elseif(sObjectName == "PoolRoom") then

    local iMovieUnlockSequence = VM_GetVar("Root/Variables/Chapter5/Scenes/iMovieUnlockSequence", "N")
    if(iMovieUnlockSequence == 0.0) then
        AudioManager_PlaySound("World|Chirp")
        VM_SetVar("Root/Variables/Chapter5/Scenes/iMovieUnlockSequence", "N", 1.0)
    elseif(iMovieUnlockSequence == 4.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iMovieUnlockSequence", "N", 5.0)
        
        --Three sounds.
        fnCutscene([[ AudioManager_PlaySound("World|Chirp") ]])
        fnCutsceneWait(15)
        fnCutsceneBlocker()
        fnCutscene([[ AudioManager_PlaySound("World|Chirp") ]])
        fnCutsceneWait(15)
        fnCutsceneBlocker()
        fnCutscene([[ AudioManager_PlaySound("World|Chirp") ]])
        fnCutsceneWait(15)
        fnCutsceneBlocker()
        
        --Open the door.
        Cutscene_CreateEvent("CameraEvent", "Camera")
            CameraEvent_SetProperty("Max Move Speed", 3.0)
            CameraEvent_SetProperty("Focus Position", (4.75 * gciSizePerTile), (11.50 * gciSizePerTile))
        DL_PopActiveObject()
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "SecretWalls2", false) ]])
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "SecretWallsHi2", false) ]])
        fnCutscene([[ AudioManager_PlaySound("World|RemoteDoor") ]])
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        Cutscene_CreateEvent("CameraEvent", "Camera")
            CameraEvent_SetProperty("Max Move Speed", 3.0)
            CameraEvent_SetProperty("Focus Actor Name", "Sammy")
        DL_PopActiveObject()
        fnCutsceneBlocker()
        fnCutsceneWait(15)
        fnCutsceneBlocker()
        
    end


elseif(sObjectName == "CoatRoom") then

    local iMovieUnlockSequence = VM_GetVar("Root/Variables/Chapter5/Scenes/iMovieUnlockSequence", "N")
    if(iMovieUnlockSequence == 1.0) then
        AudioManager_PlaySound("World|Chirp")
        VM_SetVar("Root/Variables/Chapter5/Scenes/iMovieUnlockSequence", "N", 2.0)
    end
    
elseif(sObjectName == "Office") then
    
    local iMovieUnlockSequence = VM_GetVar("Root/Variables/Chapter5/Scenes/iMovieUnlockSequence", "N")
    if(iMovieUnlockSequence == 3.0) then
        AudioManager_PlaySound("World|Chirp")
        VM_SetVar("Root/Variables/Chapter5/Scenes/iMovieUnlockSequence", "N", 4.0)
    end

end
