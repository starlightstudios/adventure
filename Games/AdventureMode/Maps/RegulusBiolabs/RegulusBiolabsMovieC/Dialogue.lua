-- |[Root]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    --Subdivide for all the entities sharing this script.
    if(sActorName == "GolemC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] I like working here![P] The toads are cute and -[P] hey why are you dressed like that?") ]])
    elseif(sActorName == "GolemD") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] I wish I could have been some kind of salamander girl.[P] I bet they get all the babes that way...") ]])
    elseif(sActorName == "GolemEnthusiast") then
        
        --Variables.
        local iAmphibianCorrectSpecies = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianCorrectSpecies", "N")
        
        --Common.
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Hey, you're a new face![P] I'm here all the time, you know.[P] I love amphibians![B][C]") ]])
        
        -- |[Axolotl]|
        if(iAmphibianCorrectSpecies == 1.0) then
            fnCutscene([[ Append("Golem:[VOICE|Golem] You know that some of them can regenerate lost limbs, right?[P] We have axolotls here and they're just the coolest!") ]])

        -- |[Olm]|
        elseif(iAmphibianCorrectSpecies == 2.0) then
            fnCutscene([[ Append("Golem:[VOICE|Golem] We have these perfectly white albino cave olms here![P] They're so cute I could just explode!") ]])

        -- |[Blackbelly Salamander]|
        elseif(iAmphibianCorrectSpecies == 3.0) then
            fnCutscene([[ Append("Golem:[VOICE|Golem] You know blackbelly salamanders are basically entirely aquatic, and rarely go more than a few centimeters from water?[P] I'd love to get one as a pet someday!") ]])
        
        -- |[Red-Bellied Newt]|
        elseif(iAmphibianCorrectSpecies == 4.0) then
            fnCutscene([[ Append("Golem:[VOICE|Golem] Careful around the red-bellied newts, they're toxic and probably dangerous to an organic like you.[P] Don't pet them!") ]])
        
        -- |[Bufo bufo]|
        elseif(iAmphibianCorrectSpecies == 10.0) then
            fnCutscene([[ Append("Golem:[VOICE|Golem] Don't touch the bufo-bufo frogs, by the way.[P] Your organic hands would probably get poisoned, they're toxic.") ]])

        -- |[Lemur frog]|
        elseif(iAmphibianCorrectSpecies == 11.0) then
            fnCutscene([[ Append("Golem:[VOICE|Golem] A lot of units think Lemur Frogs are cannibalistic, but that's a survival mechanism if the stream is drying up so they mature faster.[P] Fun nature facts!") ]])
        
        -- |[Spadefoot Toad]|
        elseif(iAmphibianCorrectSpecies == 12.0) then
            fnCutscene([[ Append("Golem:[VOICE|Golem] Spadefoot toads are the cutest, but they're really hard to spot because they like to blend in with the soil.[P] Look closely!") ]])

        -- |[Moor Frog]|
        elseif(iAmphibianCorrectSpecies == 13.0) then
            fnCutscene([[ Append("Golem:[VOICE|Golem] They're not doing it right now, but when mating season starts, Moor frogs like to form choruses to attract mates.[P] They get together and sing, it's so cute!") ]])

        -- |[Banded Bullfrog]|
        elseif(iAmphibianCorrectSpecies == 14.0) then
            fnCutscene([[ Append("Golem:[VOICE|Golem] My favourite are these Banded Bullfrog buddies.[P] I buy feed from the ranch and give it to them -[P] and they just devour it![P] They eat everything!") ]])

        -- |[Iwokramae Caecilia]|
        elseif(iAmphibianCorrectSpecies == 20.0) then
            fnCutscene([[ Append("Golem:[VOICE|Golem] These caecilians are called Iwokramaes, but they tend to be burrowed most of the time.[P] They actually breathe through their skin and don't have lungs, isn't that cool?") ]])
        
        -- |[Annalatus Caecilia]|
        elseif(iAmphibianCorrectSpecies == 21.0) then
            fnCutscene([[ Append("Golem:[VOICE|Golem] You know what a caecilian is?[P] Amphibians that burrow in the soil and if you want to see one, you have to dig.[P] But be careful, Annalatus caecilians are toxic![P] Your organic hands will need special gloves!") ]])

        -- |[Rubber Eel]|
        elseif(iAmphibianCorrectSpecies == 22.0) then
            fnCutscene([[ Append("Golem:[VOICE|Golem] See that little guy in the pond there?[P] That's called a rubber eel, but it's actually a caecilian![P] Isn't he a cutie?") ]])
        else
            fnCutscene([[ Append("Golem:[VOICE|Golem] We've got species from all around here![P] Check out the signs for more!") ]])
        end
    end
end
