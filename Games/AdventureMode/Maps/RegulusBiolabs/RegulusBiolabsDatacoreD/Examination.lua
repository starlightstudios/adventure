-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
-- |[Objects]|
if(sObjectName == "TerminalA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('You are wrong about everything.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (It's not an email, it's just what was on the terminal.[P] Pressing any key just reprints the message.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('It's cold where we are.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('It went inside her, and I watched it.[P] It writhed and squirmed and it came back out, and she wasn't there any more.[P] Now, it's inside me.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Body") then
    
    --Variables.
    local bIsSophiePresent = fnIsCharacterPresent("Sophie")
    local bIsSX399Present  = fnIsCharacterPresent("SX-399")
    local iBiolabsFoundRedKeycard = VM_GetVar("Root/Variables/Chapter5/Scenes/iBiolabsFoundRedKeycard", "N")
    local iHasDarkmatterForm      = VM_GetVar("Root/Variables/Global/Christine/iHasDarkmatterForm", "N")
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iBiolabsFoundRedKeycard", "N", 1.0)
    
    --First time.
    if(iBiolabsFoundRedKeycard == 0.0) then
        
        --Switch the last save point to this room. Otherwise if the player passes the savepoint and surrenders, they go to the Alpha Labs instead.
        AL_SetProperty("Last Save Point", "RegulusBiolabsDatacoreD")
        
        --Topics.
        WD_SetProperty("Unlock Topic", "Biolabs_Darkmatters", 1)
        
        --Spawn a Darkmatter.
        TA_Create("Darkmatter")
            TA_SetProperty("Position", -100, -100)
            TA_SetProperty("Facing", gci_Face_North)
            TA_SetProperty("Clipping Flag", false)
            fnSetCharacterGraphics("Root/Images/Sprites/DarkmatterGirl/", true)
        DL_PopActiveObject()
        
        --Spawn enemies.
        AL_SetProperty("Wipe Destroyed Enemies")
        fnStandardEnemyPulse()
        
        --Party moves over.
        fnCutsceneMove("Christine", 4.25, 6.50)
        fnCutsceneFace("Christine", 0, -1)
        fnCutsceneMove("Tiffany", 3.25, 5.50)
        fnCutsceneFace("Tiffany", 1, 0)
        if(bIsSophiePresent == true) then
            fnCutsceneMove("Sophie", 5.25, 5.50)
            fnCutsceneFace("Sophie", -1, 0)
        end
        if(bIsSX399Present == true) then
            fnCutsceneMove("SX-399", 4.25, 4.50)
            fnCutsceneFace("SX-399", 0, 1)
        end
        
        --Sophie and SX-399 are here.
        if(bIsSophiePresent == true and bIsSX399Present == true) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
            fnCutscene([[ Append("Christine:[E|Neutral] I guess that accounts for the missing unit...[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Smirk] Shame they didn't do more damage.[P] She's barely scratched and not melted at all.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] Please don't glorify violence.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] Just an observation, boss.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Sad] ...[P] No, no, no good.[P] She's long gone.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Sad] There's nothing I can do.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] I know.[P] Maybe if we had gotten here sooner...[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] Did you really intend to save her?[P] The enemy?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] She's not the enemy until she picks up a gun.[P] Until then, she's a civilian.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Sad] Are you kidding?[P] Do you have any idea what the Lords have done?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Upset] *Ahem*[B][C]") ]])
            fnCutscene([[ Append("55:[E|Upset] Please inform me as to what sort of things are to be forgiven, and what are not.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Upset] Whatever standard you set, I assure you, I do not meet it.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Sad] But...[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Sad] But you're different...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] How.[P] State how.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Sad] I...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] [EMOTION|Tiffany|Neutral]If 55 can change, then this Lord Unit could, too.[P] You can't treat her differently.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] Uhhh...[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Smirk] I know, I know.[P] We have to be evenhanded.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] Just, sometimes the things I saw out in the mines, or the things mother told me...[P] they stuck with me.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] But how do I know the difference between a good golem, and a bad one?[P] How many are Christines?[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Sad] There isn't a difference.[P] The good ones [P]*are*[P] the bad ones.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] The Lord Units don't see us any differently than you see a discarded pulse-rifle cell.[P] To be thrown away when it is not useful.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Offended] The truly cruel ones will get theirs in the end...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] ...[P] but to paint them all with the same brush is to make the same mistake they made about you.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Hate feeds on hate.[P] To end it, we must erase its source.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] All this because of a simple statement...[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Smirk] I get it.[P] Say, didn't we come here for a keycard?[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Flirt] [SOUND|World|Keycard]Like this one?[B][C]") ]])
            fnCutscene([[ Append("Narrator: [SOUND|World|Keycard](Received Red Keycard!)[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Great, back to the mission.") ]])
            fnCutsceneBlocker()
        
        --Just Sophie
        elseif(bIsSophiePresent == true and bIsSX399Present == false) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
            fnCutscene([[ Append("Christine:[E|Neutral] I guess that accounts for the missing unit...[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Sad] ...[P] No, no, no good.[P] She's long gone.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Sad] Even if I had her in the repair bay right now, there'd be nothing I could do.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Upset] Light damage, but inconsistent with the creatures we have been encountering.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] A trap?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Upset] Likely.[P] And not one set by Vivify and her minions.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] If it's a trap, stay behind me, Sophie.[P] Let's go.[B][C]") ]])
            fnCutscene([[ Append("Narrator: [SOUND|World|Keycard](Received Red Keycard!)") ]])
            fnCutsceneBlocker()
        
        --Just SX-399.
        elseif(bIsSophiePresent == false and bIsSX399Present == true) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
            fnCutscene([[ Append("Christine:[E|Neutral] I guess that accounts for the missing unit...[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Smirk] Shame they didn't do more damage.[P] She's barely scratched and not melted at all.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] Please don't glorify violence.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] Just an observation, boss.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] ...[P] No core beat, no fluid pumping.[P] She's gone.[P] Maybe if we had gotten here sooner...[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] Did you really intend to save her?[P] The enemy?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] She's not the enemy until she picks up a gun.[P] Until then, she's a civilian.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Sad] Are you kidding?[P] Do you have any idea what the Lords have done?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Upset] *Ahem*[B][C]") ]])
            fnCutscene([[ Append("55:[E|Upset] Please inform me as to what sort of things are to be forgiven, and what are not.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Upset] Whatever standard you set, I assure you, I do not meet it.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Sad] But...[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Sad] But you're different...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] How.[P] State how.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Sad] I...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] [EMOTION|Tiffany|Neutral]If 55 can change, then this Lord Unit could, too.[P] You can't treat her differently.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] Uhhh...[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Smirk] I know, I know.[P] We have to be evenhanded.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] Just, sometimes the things I saw out in the mines, or the things mother told me...[P] they stuck with me.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] But how do I know the difference between a good golem, and a bad one?[P] How many are Christines?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Some are, many are, most are the products of their environments.[P] Change where they are and you will change who they are.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Hate feeds on hate and makes everyone worse, but love feeds on love.[P] To change the cycle we must end it at its source.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] All this because of a simple statement...[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Smirk] I get it.[P] Say, didn't we come here for a keycard?[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Flirt] [SOUND|World|Keycard]Like this one?[B][C]") ]])
            fnCutscene([[ Append("Narrator: [SOUND|World|Keycard](Received Red Keycard!)[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Great, back to the mission.") ]])
            fnCutsceneBlocker()
        
        --Neither are present.
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
            fnCutscene([[ Append("Christine:[E|Neutral] I guess that accounts for the missing unit...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Upset] Light damage, but inconsistent with the creatures we have been encountering.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] A trap?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Upset] Likely.[P] And not one set by Vivify and her minions.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Guess we'll have to fight our way out in a few minutes, but at least we have the keycard.[B][C]") ]])
            fnCutscene([[ Append("Narrator: [SOUND|World|Keycard](Received Red Keycard!)") ]])
            fnCutsceneBlocker()
        
        end
        
        --Wait.
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Everyone looks south.
        fnCutsceneFace("Christine", 0, 1)
        fnCutsceneFace("Tiffany", 0, 1)
        if(bIsSophiePresent == true) then
            fnCutsceneFace("Sophie", 0, 1)
        end
        if(bIsSX399Present == true) then
            fnCutsceneFace("SX-399", 0, 1)
        end
        
        --Teleport the Darkmatter in place.
        fnCutsceneTeleport("Darkmatter", 5.25, 10.50)
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Darkmatter moves up.
        fnCutsceneMove("Darkmatter", 5.25, 8.50)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --If Christine has Darkmatter form:
        if(iHasDarkmatterForm == 1.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "DarkmatterGirl", "Neutral") ]])
            
            --SX-399 is present, Sophie is present:
            if(bIsSX399Present == true and bIsSophiePresent == true) then
                fnCutscene([[ Append("SX-399:[E|Happy] Far-out![P] It's a Darkmatter Girl![B][C]") ]])
                fnCutscene([[ Append("SX-399:[E|Smirk] Does that mean you're here to help?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] I can talk to her.[P] Don't worry, I'll translate.[B][C]") ]])
                fnCutscene([[ Append("Darkmatter:[E|Neutral] You will stop here.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] She says - [P][CLEAR]") ]])
                fnCutscene([[ Append("55:[E|Neutral] We are able to understand her.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] You are?[B][C]") ]])
                fnCutscene([[ Append("Darkmatter:[E|Neutral] You cannot leave this place.[P] We are sorry for this.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Hey, hold on.[P] What's going on?[P] I'm one of you -[P] just, ask - [P][CLEAR]") ]])
                fnCutscene([[ Append("Darkmatter:[E|Neutral] You must die.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] Now there's no need for this.[P] Don't you recognize me?[B][C]") ]])
                fnCutscene([[ Append("Darkmatter:[E|Neutral] Christine.[P] Bearer of the runestone.[P] You have helped us cleanse the taint of Serenity Crater.[B][C]") ]])
                fnCutscene([[ Append("Darkmatter:[E|Neutral] That taint nucleates within you.[P] It has followed you across the divide to this place.[B][C]") ]])
                fnCutscene([[ Append("Darkmatter:[E|Neutral] You have seen the future.[P] You have seen what you become.[B][C]") ]])
                fnCutscene([[ Append("SX-399:[E|Neutral] Is she talking about...[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Sad] B-[P]but she can control herself...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] The future isn't set.[P] We can avoid that fate.[P] Give me a chance.[B][C]") ]])
                fnCutscene([[ Append("Darkmatter:[E|Neutral] Please do not resist.[P] We owe you much.[P] It will not hurt.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] Not happening.[B][C]") ]])
                fnCutscene([[ Append("Darkmatter:[E|Neutral] We know.[P] We know that we fail.[P] You leave this place, and continue to where you will be.[B][C]") ]])
                fnCutscene([[ Append("Darkmatter:[E|Neutral] But we must try.[P] We are sorry.") ]])
            
            --Just SX-399.
            elseif(bIsSX399Present == true and bIsSophiePresent == false) then
                fnCutscene([[ Append("SX-399:[E|Happy] Far-out![P] It's a Darkmatter Girl![B][C]") ]])
                fnCutscene([[ Append("SX-399:[E|Smirk] Does that mean you're here to help?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] I can talk to her.[P] Don't worry, I'll translate.[B][C]") ]])
                fnCutscene([[ Append("Darkmatter:[E|Neutral] You will stop here.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] She says - [P][CLEAR]") ]])
                fnCutscene([[ Append("55:[E|Neutral] We are able to understand her.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] You are?[B][C]") ]])
                fnCutscene([[ Append("Darkmatter:[E|Neutral] You cannot leave this place.[P] We are sorry for this.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Hey, hold on.[P] What's going on?[P] I'm one of you -[P] just, ask - [P][CLEAR]") ]])
                fnCutscene([[ Append("Darkmatter:[E|Neutral] You must die.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] Now there's no need for this.[P] Don't you recognize me?[B][C]") ]])
                fnCutscene([[ Append("Darkmatter:[E|Neutral] Christine.[P] Bearer of the runestone.[P] You have helped us cleanse the taint of Serenity Crater.[B][C]") ]])
                fnCutscene([[ Append("Darkmatter:[E|Neutral] That taint nucleates within you.[P] It has followed you across the divide to this place.[B][C]") ]])
                fnCutscene([[ Append("Darkmatter:[E|Neutral] You have seen the future.[P] You have seen what you become.[B][C]") ]])
                fnCutscene([[ Append("SX-399:[E|Neutral] Is she talking about...[B][C]") ]])
                fnCutscene([[ Append("55:[E|Upset] We should have known better than to trust these creatures.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] Please 55.[P] Not now.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] The future isn't set.[P] We can avoid that fate.[P] Give me a chance.[B][C]") ]])
                fnCutscene([[ Append("Darkmatter:[E|Neutral] Please do not resist.[P] We owe you much.[P] It will not hurt.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] Not happening.[B][C]") ]])
                fnCutscene([[ Append("Darkmatter:[E|Neutral] We know.[P] We know that we fail.[P] You leave this place, and continue to where you will be.[B][C]") ]])
                fnCutscene([[ Append("Darkmatter:[E|Neutral] But we must try.[P] We are sorry.") ]])
            
            --Just Sophie.
            elseif(bIsSX399Present == false and bIsSophiePresent == true) then
                fnCutscene([[ Append("Sophie:[E|Happy] Hello night lady![P] Are you one of Christine's friends?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Of course![P] Don't worry, I'll translate, I can understand them.[B][C]") ]])
                fnCutscene([[ Append("Darkmatter:[E|Neutral] You will stop here.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] [EMOTION|Sophie|Neutral]She says - [P][CLEAR]") ]])
                fnCutscene([[ Append("55:[E|Neutral] We are able to understand her.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] You are?[B][C]") ]])
                fnCutscene([[ Append("Darkmatter:[E|Neutral] You cannot leave this place.[P] We are sorry for this.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Hey, hold on.[P] What's going on?[P] I'm one of you -[P] just, ask - [P][CLEAR]") ]])
                fnCutscene([[ Append("Darkmatter:[E|Neutral] You must die.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] Now there's no need for this.[P] Don't you recognize me?[B][C]") ]])
                fnCutscene([[ Append("Darkmatter:[E|Neutral] Christine.[P] Bearer of the runestone.[P] You have helped us cleanse the taint of Serenity Crater.[B][C]") ]])
                fnCutscene([[ Append("Darkmatter:[E|Neutral] That taint nucleates within you.[P] It has followed you across the divide to this place.[B][C]") ]])
                fnCutscene([[ Append("Darkmatter:[E|Neutral] You have seen the future.[P] You have seen what you become.[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Sad] B-[P]but she can control herself...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] The future isn't set.[P] We can avoid that fate.[P] Give me a chance.[B][C]") ]])
                fnCutscene([[ Append("Darkmatter:[E|Neutral] Please do not resist.[P] We owe you much.[P] It will not hurt.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] Not happening.[B][C]") ]])
                fnCutscene([[ Append("Darkmatter:[E|Neutral] We know.[P] We know that we fail.[P] You leave this place, and continue to where you will be.[B][C]") ]])
                fnCutscene([[ Append("Darkmatter:[E|Neutral] But we must try.[P] We are sorry.") ]])
            
            --Neither.
            else
                fnCutscene([[ Append("55:[E|Smirk] About time the darkmatters made themselves useful.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] See?[P] This is why you don't shoot them, 55![B][C]") ]])
                fnCutscene([[ Append("Darkmatter:[E|Neutral] You will stop here.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] She says - [P][CLEAR]") ]])
                fnCutscene([[ Append("55:[E|Neutral] I was able to understand her.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] You were?[B][C]") ]])
                fnCutscene([[ Append("Darkmatter:[E|Neutral] You cannot leave this place.[P] We are sorry for this.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Hey, hold on.[P] What's going on?[P] I'm one of you -[P] just, ask - [P][CLEAR]") ]])
                fnCutscene([[ Append("Darkmatter:[E|Neutral] You must die.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] Now there's no need for this.[P] Don't you recognize me?[B][C]") ]])
                fnCutscene([[ Append("Darkmatter:[E|Neutral] Christine.[P] Bearer of the runestone.[P] You have helped us cleanse the taint of Serenity Crater.[B][C]") ]])
                fnCutscene([[ Append("Darkmatter:[E|Neutral] That taint nucleates within you.[P] It has followed you across the divide to this place.[B][C]") ]])
                fnCutscene([[ Append("Darkmatter:[E|Neutral] You have seen the future.[P] You have seen what you become.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Upset] This course of action is idiotic.[P] Christine is your greatest ally.[P] You are making a mistake.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] 55's right.[P] We can avoid that fate.[P] Give me a chance.[B][C]") ]])
                fnCutscene([[ Append("Darkmatter:[E|Neutral] Please do not resist.[P] We owe you much.[P] It will not hurt.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] Not happening.[B][C]") ]])
                fnCutscene([[ Append("Darkmatter:[E|Neutral] We know.[P] We know that we fail.[P] You leave this place, and continue to where you will be.[B][C]") ]])
                fnCutscene([[ Append("Darkmatter:[E|Neutral] But we must try.[P] We are sorry.") ]])
                
            end
            
            fnCutsceneBlocker()
        
        --Doesn't have Darkmatter form.
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "DarkmatterGirl", "Neutral") ]])
            
            --SX-399 is present, Sophie is present:
            if(bIsSX399Present == true and bIsSophiePresent == true) then
                fnCutscene([[ Append("SX-399:[E|Happy] Far-out![P] It's a Darkmatter Girl![B][C]") ]])
                fnCutscene([[ Append("SX-399:[E|Smirk] I've seen them in the mines.[P] They aren't friends with the creepies, that's for sure.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Are you here to help?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Uh, you want my runestone?[P] Here, touch it...[B][C]") ]])
                fnCutscene([[ Append("Darkmatter:[E|Neutral] You...[P] stop...[P] now...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Me, stop, now?[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] I heard what she said.[P] It appears your runestone can translate.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Laugh] Well that's dead handy![B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Don't worry about us, darkmatter.[P] We can take care of ourselves.[P] In fact, could you - [P][CLEAR]") ]])
                fnCutscene([[ Append("Darkmatter:[E|Neutral] We...[P] stop...[P] you...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] Excuse me?[B][C]") ]])
                fnCutscene([[ Append("Darkmatter:[E|Neutral] You...[P] are...[P] them...[B][C]") ]])
                fnCutscene([[ Append("Darkmatter:[E|Neutral] Seen...[P] future...[P] you...[P] are...[P] them...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] I can control it.[P] The future is not set.[B][C]") ]])
                fnCutscene([[ Append("SX-399:[E|Neutral] What's she talking about?[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Sad] They've already turned Christine against us once...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Sad] I know, and I'm sorry.[P] I won't let it happen.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] But that means I need your help, darkmatter![B][C]") ]])
                fnCutscene([[ Append("Darkmatter:[E|Neutral] You...[P] die...[P] here...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Angry] You won't stop us![P] If we don't stop Vivify, what chance will you have?[P] You know I'm your best hope![B][C]") ]])
                fnCutscene([[ Append("Darkmatter:[E|Neutral] Talking...[P] over...[P] goodbye...") ]])
            
            --Just SX-399.
            elseif(bIsSX399Present == true and bIsSophiePresent == false) then
                fnCutscene([[ Append("SX-399:[E|Happy] Far-out![P] It's a Darkmatter Girl![B][C]") ]])
                fnCutscene([[ Append("SX-399:[E|Smirk] I've seen them in the mines.[P] They aren't friends with the creepies, that's for sure.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Are you here to help?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Uh, you want my runestone?[P] Here, touch it...[B][C]") ]])
                fnCutscene([[ Append("Darkmatter:[E|Neutral] You...[P] stop...[P] now...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Me, stop, now?[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] I heard what she said.[P] It appears your runestone can translate.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Laugh] Well that's dead handy![B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Don't worry about us, darkmatter.[P] We can take care of ourselves.[P] In fact, could you - [P][CLEAR]") ]])
                fnCutscene([[ Append("Darkmatter:[E|Neutral] We...[P] stop...[P] you...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] Excuse me?[B][C]") ]])
                fnCutscene([[ Append("Darkmatter:[E|Neutral] You...[P] are...[P] them...[B][C]") ]])
                fnCutscene([[ Append("Darkmatter:[E|Neutral] Seen...[P] future...[P] you...[P] are...[P] them...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] I can control it.[P] The future is not set.[B][C]") ]])
                fnCutscene([[ Append("SX-399:[E|Neutral] What's she talking about?[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] What happened at the gala.[P] Christine, despite her best efforts, is well-and-truly one of them.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] The darkmatter's assessment is incorrect, but understandable.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Sad] I'm sorry.[P] I won't let it happen if I can avoid it.[P] I didn't like being...[P] them...[P] either![B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] But that means I need your help, darkmatter![B][C]") ]])
                fnCutscene([[ Append("Darkmatter:[E|Neutral] You...[P] die...[P] here...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Angry] You won't stop us![P] If we don't stop Vivify, what chance will you have?[P] You know I'm your best hope![B][C]") ]])
                fnCutscene([[ Append("Darkmatter:[E|Neutral] Talking...[P] over...[P] goodbye...") ]])
            
            --Just Sophie.
            elseif(bIsSX399Present == false and bIsSophiePresent == true) then
                fnCutscene([[ Append("Sophie:[E|Happy] Christine, look![P] A darkmatter![B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Happy] Are they your friends?[P] Have you seen them on your adventures?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Friends, maybe.[P] They certainly aren't friends of Vivify or her ilk.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Uh, you want my runestone?[P] Here, touch it...[B][C]") ]])
                fnCutscene([[ Append("Darkmatter:[E|Neutral] You...[P] stop...[P] now...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Me, stop, now?[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] I heard what she said.[P] It appears your runestone can translate.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Laugh] Well that's dead handy![B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Don't worry about us, darkmatter.[P] We can take care of ourselves.[P] In fact, could you - [P][CLEAR]") ]])
                fnCutscene([[ Append("Darkmatter:[E|Neutral] We...[P] stop...[P] you...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] Excuse me?[B][C]") ]])
                fnCutscene([[ Append("Darkmatter:[E|Neutral] You...[P] are...[P] them...[B][C]") ]])
                fnCutscene([[ Append("Darkmatter:[E|Neutral] Seen...[P] future...[P] you...[P] are...[P] them...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] I can control it.[P] The future is not set.[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Sad] Christine won't let them take her over...[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] Christine, despite her best efforts, is well-and-truly one of them.[P] The enemy.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] The darkmatter's assessment is incorrect, but understandable.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Sad] I'm sorry.[P] I won't let it happen if I can avoid it.[P] I didn't like being...[P] them...[P] either![B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] But that means I need your help, darkmatter![B][C]") ]])
                fnCutscene([[ Append("Darkmatter:[E|Neutral] You...[P] die...[P] here...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Angry] You won't stop us![P] If we don't stop Vivify, what chance will you have?[P] You know I'm your best hope![B][C]") ]])
                fnCutscene([[ Append("Darkmatter:[E|Neutral] Talking...[P] over...[P] goodbye...") ]])
            
            --Neither.
            else
                fnCutscene([[ Append("55:[E|Neutral] A darkmatter.[P] This is unusual.[P] They do not often come this far into the city.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Maybe she wants to be friends?[P] They certainly aren't friends of Vivify or her ilk.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] They were there at the LRT facility.[P] Do you think they want to help us against Vivify?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Uh, you want my runestone?[P] Here, touch it...[B][C]") ]])
                fnCutscene([[ Append("Darkmatter:[E|Neutral] You...[P] stop...[P] now...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Me, stop, now?[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] I heard what she said.[P] It appears your runestone can translate.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Laugh] Well that's dead handy![B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Don't worry about us, darkmatter.[P] We can take care of ourselves.[P] In fact, could you - [P][CLEAR]") ]])
                fnCutscene([[ Append("Darkmatter:[E|Neutral] We...[P] stop...[P] you...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] Excuse me?[B][C]") ]])
                fnCutscene([[ Append("Darkmatter:[E|Neutral] You...[P] are...[P] them...[B][C]") ]])
                fnCutscene([[ Append("Darkmatter:[E|Neutral] Seen...[P] future...[P] you...[P] are...[P] them...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] I can control it.[P] The future is not set.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] Christine, despite your best efforts, you are well-and-truly one of them.[P] The enemy.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] The darkmatter's assessment is incorrect, but understandable.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Sad] I'm sorry.[P] I won't let it happen if I can avoid it.[P] I didn't like being...[P] them...[P] either![B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] But that means I need your help, darkmatter![B][C]") ]])
                fnCutscene([[ Append("Darkmatter:[E|Neutral] You...[P] die...[P] here...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Angry] You won't stop us![P] If we don't stop Vivify, what chance will you have?[P] You know I'm your best hope![B][C]") ]])
                fnCutscene([[ Append("Darkmatter:[E|Neutral] Talking...[P] over...[P] goodbye...") ]])
            end
            
            fnCutsceneBlocker()
        
        end
        
        --Wait a bit.
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Darkmatter walks away.
        fnCutsceneMoveFace("Darkmatter", 5.25, 10.50, 0, -1, 1.20)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneTeleport("Darkmatter", -100.25, -100.50)
        fnCutsceneBlocker()
        
        --Cutscene.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        fnCutscene([[ Append("Christine:[E|Angry] Exactly what we needed![P] More infighting![B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] We have acquired the Datacore's keycard.[P] We should continue.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] 55, don't you get it?[P] This wasn't an accident.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] It's an ambush.[P] They stuck the keycard here knowing we'd need it.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] It wasn't Vivify's goons that attacked this unit, it was the darkmatters.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] I understand that, obviously.[P] You are angry, try to think clearly.[P] This does not change our objectives.[P] We will need to fight our way out.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] But don't -[P] just, give them a bonk.[P] You know?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] I'm sure we can talk some sense into them later.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smug] Darkmatters are difficult to injure with physical weaponry.[P] I'm sure a pulse round to the face will be your 'bonk'.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] *sigh*[P] Why did it come to this?") ]])
        fnCutsceneBlocker()
        
        --Fold the party.
        fnAutoFoldParty()
        fnCutsceneBlocker()
        
        --Move camera.
        Cutscene_CreateEvent("CameraEvent", "Camera")
            CameraEvent_SetProperty("Max Move Speed", 1.0)
            CameraEvent_SetProperty("Focus Position", (11.25 * gciSizePerTile), (5.50 * gciSizePerTile))
        DL_PopActiveObject()
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] We could rest at that bench, just like a heating coil.[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] It won't allow us to warp, but at least we'll be at full health...") ]])
        fnCutsceneBlocker()
        
        --Move camera.
        Cutscene_CreateEvent("CameraEvent", "Camera")
            CameraEvent_SetProperty("Max Move Speed", 1.0)
            CameraEvent_SetProperty("Focus Actor Name", "Christine")
        DL_PopActiveObject()
        fnCutsceneBlocker()
        
    
    --Repeats.
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (The remains of Unit 689344.[P] She's beyond repair.[P] She was clutching a red keycard.)") ]])
        fnCutsceneBlocker()
    end

-- |[Skillbook]|
elseif(sObjectName == "Skillbook") then
    AL_SetProperty("Set Layer Disabled", "SkillbookIco", true)
	LM_ExecuteScript(gsTiffanySkillbook, 6)
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
