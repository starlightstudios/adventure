-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
-- |[Objects]|
if(sObjectName == "TerminalA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('This seems like an important question, yet I can't get a straight answer from any higher-ups.[P] Why is my testing range being used to test infiltration of specific chemical compounds?')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('I've been modelling water infiltration through various soil types for a decade now.[P] We've made some major improvements in agricultural technology.[P] Revolutionary, even.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Now, I'm being told to study dispersal patterns of lead acetate.[P] Why?[P] It's crucial that a scientist knows the details.[P] We can't do science in the dark!')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('I get the feeling this isn't simply to study pollution cleanup.[P] I have no allegiance to the organics planetside.[P] Just tell me already!')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('The soil itself acts as a receptacle for the noted materials, effectively filtering the water.[P] Given enough time, most pollutants would diffuse throughout an ecosystem, absorbed and used as catalysts by plant and bacterial species.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('This is to say nothing of the fixing tendency of mineral formation, which we do not model here.[P] Presumably, lead acetate would decay and form minerals like galena in a sedimentary deposit eventually, fully denaturing it.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Though if the purpose was to cause neurological damage, it would be enough to pour an excessive amount of lead acetate upstream and allow it to infiltrate into a river body.[P] The sugar dissolves well, even tasting sweet.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "SignA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Organic citizens are not to drink water in the infiltration testing range under any circumstances.[P] Please use approved filtered taps only.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "SignB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('South:: Biological Services barracks.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('East:: Aviary.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('West:: Raiju Ranch, Breeding Program facilities, Public Campsites.')") ]])
    fnCutsceneBlocker()
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
