-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
if(sObjectName == "Meeting") then
    
    --Variables.
    local iAmphibianMetDafoda = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianMetDafoda", "N")
    if(iAmphibianMetDafoda > 0.0) then return end
    
    --Variables.
    local iSX399JoinsParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
    local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
        
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iAmphibianMetDafoda", "N", 1.0)
    
    --Topics.
    WD_SetProperty("Unlock Topic", "Biolabs_Alraune", 1)
    
    --Merge party.
    fnCutsceneMergeParty()
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Christine", 15.25, 21.50)
    fnCutsceneFace("Christine", -1, 0)
    fnCutsceneMove("Tiffany", 15.25, 22.50)
    fnCutsceneFace("Tiffany", -1, 0)
    if(iSX399JoinsParty == 1.0) then
        fnCutsceneMove("SX-399", 15.25, 23.50)
        fnCutsceneFace("SX-399", -1, 0)
    end
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Alraune:[VOICE|Alraune] Yes, I know they're here.[P] Which one was the leader again?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Dafoda", 11.25, 21.50)
    fnCutsceneFace("Dafoda", 1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneMove("Dafoda", 14.25, 21.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Alraune", "Neutral") ]])
    fnCutscene([[ Append("Alraune:[E|Neutral] So I'm hearing that you're the leader of your group.[P] Is that correct?[B][C]") ]])
    fnCutscene([[ Append("Alraune:[E|Neutral] I wasn't expecting someone so...[B][C]") ]])
    if(sChristineForm == "Human") then
        fnCutscene([[ Append("Christine:[E|Smirk] Squishy?[B][C]") ]])
    elseif(sChristineForm == "Golem") then
        fnCutscene([[ Append("Christine:[E|Smirk] Fabulous?[B][C]") ]])
    elseif(sChristineForm == "LatexDrone") then
        fnCutscene([[ Append("Christine:[E|Smirk] Firm and shiny?[B][C]") ]])
    elseif(sChristineForm == "Electrosprite") then
        fnCutscene([[ Append("Christine:[E|Smirk] Electromental?[B][C]") ]])
    elseif(sChristineForm == "Darkmatter") then
        fnCutscene([[ Append("Christine:[E|Smirk] Exotic?[B][C]") ]])
    elseif(sChristineForm == "Eldritch") then
        fnCutscene([[ Append("Christine:[E|Smirk] Cold and clammy?[B][C]") ]])
    elseif(sChristineForm == "SteamDroid") then
        fnCutscene([[ Append("Christine:[E|Smirk] Retro?[B][C]") ]])
    elseif(sChristineForm == "Raiju") then
        fnCutscene([[ Append("Christine:[E|Smirk] Eager?[B][C]") ]])
    elseif(sChristineForm == "Doll") then
        fnCutscene([[ Append("Christine:[E|Smirk] Perfect?[B][C]") ]])
    elseif(sChristineForm == "Secrebot") then
        fnCutscene([[ Append("Christine:[E|Smirk] Pretty?[B][C]") ]])
    end
    fnCutscene([[ Append("Alraune:[E|Neutral] Naive looking.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Angry] Naive![P] How can you [P]*look*[P] naive?[B][C]") ]])
    fnCutscene([[ Append("Alraune:[E|Neutral] [EMOTION|Christine|Neutral]Doesn't matter.[P] You're with the rebels, aren't you?[B][C]") ]])
    fnCutscene([[ Append("Alraune:[E|Neutral] Don't bother denying it, we know.[B][C]") ]])
    if(iSX399JoinsParty == 1.0) then
        fnCutscene([[ Append("SX-399:[E|Neutral] Is it true?[P] Can you Alraunes speak to plants?[B][C]") ]])
        fnCutscene([[ Append("Alraune:[E|Neutral] It is.[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Happy] Keen![P] Can you teach me how to do that?[B][C]") ]])
        fnCutscene([[ Append("Alraune:[E|Neutral] Funny, we get that question from many Lord Golems.[B][C]") ]])
        fnCutscene([[ Append("Alraune:[E|Neutral] The fact that our services have been retained this long indicates they have still failed to learn the skill themselves.[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Sad] Bah, humbug.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] [EMOTION|SX-399|Neutral]I didn't catch your name.[P] Miss?[B][C]") ]])
    else
        fnCutscene([[ Append("55:[E|Neutral] My sources indicate that Alraunes are capable of conversing with plants.[P] It is likely they have been following our movements.[B][C]") ]])
        fnCutscene([[ Append("Alraune:[E|Neutral] Correct.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] I didn't catch your name.[P] Miss?[B][C]") ]])
    end
    fnCutscene([[ Append("Dafoda:[E|Neutral] I am Unit 588641.[P] Secondary designation, Dafoda.[P] I am the Lord Unit assigned to Biological Services.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] You're a Lord Unit?[B][C]") ]])
    fnCutscene([[ Append("Dafoda:[E|Neutral] I have the same access privileges as one, and the same responsibilities.[B][C]") ]])
    fnCutscene([[ Append("Dafoda:[E|Neutral] Though I will say that, despite this, I am not regarded as a peer by the other lord units.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] They're quite catty, aren't they?[B][C]") ]])
    fnCutscene([[ Append("Dafoda:[E|Neutral] You're Christine, or Unit 771852.[P] One of the rebel leaders, and also someone who can apparently transform herself.[B][C]") ]])
    fnCutscene([[ Append("Dafoda:[E|Neutral] I've been doing my homework.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] So you know who we are.[P] You know why we are here.[B][C]") ]])
    fnCutscene([[ Append("Dafoda:[E|Neutral] If you're looking for allies or traitors, keep looking.[P] We are loyal to the Administration.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] Why?[B][C]") ]])
    fnCutscene([[ Append("Dafoda:[E|Neutral] We believe in the Cause of Science.[P] All of us are from the breeding program, and we chose to become alraunes.[B][C]") ]])
    fnCutscene([[ Append("Dafoda:[E|Neutral] Of my own free will, I went to Pandemonium and was joined there.[P] Then, I came back, to aid the Cause.[P] All of my leaf-sisters here were transformed by me, to help the Cause.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] And what makes you think the rebels also don't serve the Cause?[P] I believe in it![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] The rebels are fighting for decent lives and respect.[P] If they can't have even that then what good is Science?[B][C]") ]])
    fnCutscene([[ Append("Dafoda:[E|Neutral] True.[P] But many of the units you are trying to protect will be retired.[P] Violence is not the alraune way.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Violence?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] Violence![P] The Administration are the violent ones![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] Every day they arrest, torture, and execute units who stepped one centimeter out of line.[P] We just don't see it because it happens in prisons or in back rooms.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] The entire system is predicated on passive violence.[P] If our new system is less violent, then we are right to overthrow it.[B][C]") ]])
    fnCutscene([[ Append("Dafoda:[E|Neutral] A big promise.[P] You think you can be less destructive?[P] Why won't you fall into the same patterns as the Administration has?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] We believe in something fundamentally different.[P] It's easy to sit there and not pick sides by assuming everyone is the same, but we are not.[B][C]") ]])
    fnCutscene([[ Append("Dafoda:[E|Neutral] Hmm...[P] I'm not convinced.[B][C]") ]])
    fnCutscene([[ Append("Dafoda:[E|Neutral] But...[P] It is not only I who makes decisions here.[P] All are equal in nature.[B][C]") ]])
    
    --Has not rescued Reika:
    local iAmphibianRescuedReika = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianRescuedReika", "N")
    if(iAmphibianRescuedReika == 0.0) then
        fnCutscene([[ Append("Dafoda:[E|Neutral] One of our number, Unit 765532, is missing.[P] Her secondary designation is Reika.[B][C]") ]])
        fnCutscene([[ Append("Dafoda:[E|Neutral] If you help us find her, then perhaps the others will be more charitable in their assessment of you.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Can't you locate her with the help of your plant-speech?[B][C]") ]])
        if(iSX399JoinsParty == 1.0) then
            fnCutscene([[ Append("SX-399:[E|Smirk] Maybe if she does it I can watch her and mimic it...[B][C]") ]])
            fnCutscene([[ Append("Dafoda:[E|Neutral] I have done it several times during this conversation.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Sad] Nuts...[B][C]") ]])
            fnCutscene([[ Append("Dafoda:[E|Neutral] [EMOTION|SX-399|Neutral]There are many areas of the biolabs where the little ones do not grow.[P] The last place she was seen was heading north towards the Beta Laboratories.[B][C]") ]])
        else
            fnCutscene([[ Append("Dafoda:[E|Neutral] There are many areas of the biolabs where the little ones do not grow.[P] The last place she was seen was heading towards the Beta Laboratories.[B][C]") ]])
        end
        fnCutscene([[ Append("Christine:[E|Neutral] You mean, the frozen Beta Laboratories?[B][C]") ]])
        fnCutscene([[ Append("Dafoda:[E|Neutral] Frozen?[P] Interesting.[B][C]") ]])
        fnCutscene([[ Append("Dafoda:[E|Neutral] Reika mentioned a curious silence from there, and she was going to investigate.[P] Do you know what has happened?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Likely the heat-exchange pipes were sabotaged.[P] That would sap the heat from the system and trigger a freeze-over.[B][C]") ]])
        fnCutscene([[ Append("Dafoda:[E|Neutral] And Reika doubtless would try to fix it.[P] She is the most mechanically-oriented of us, and also the most foolhardy.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] We'll find her.[B][C]") ]])
        fnCutscene([[ Append("Dafoda:[E|Neutral] I did not promise a reward.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] Don't need one.[P] We'll find your friend.[B][C]") ]])
        fnCutscene([[ Append("Dafoda:[E|Neutral] I would send assistance, but we are few and we must care for the civilians here.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] You can count on us, Dafoda.[B][C]") ]])
        fnCutscene([[ Append("Dafoda:[E|Neutral] Good luck, rebels.[P] Be safe.") ]])
        fnCutsceneBlocker()
    
    --Has already rescued Reika:
    elseif(iAmphibianRescuedReika == 1.0) then
    
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iAmphibianRescuedReika", "N", 2.0)
        
        --Dialogue.
        fnCutscene([[ Append("Dafoda:[E|Neutral] One of our number, Unit 765532, went missing.[P] You know her as Reika, and you have returned her to us.[B][C]") ]])
        fnCutscene([[ Append("Dafoda:[E|Neutral] This is already more than the officials of the city have done.[P] For that, I thank you.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] We found someone in need, and helped them.[P] Simple as that.[B][C]") ]])
        fnCutscene([[ Append("Dafoda:[E|Neutral] Indeed.[P] We will take this into consideration.[B][C]") ]])
        fnCutscene([[ Append("Dafoda:[E|Neutral] We must confer.[P] The Alraunes, and the little ones, and our grandfathers, make decisions together.[P] I will testify to your intentions.[B][C]") ]])
        fnCutscene([[ Append("Dafoda:[E|Neutral] At the very least, we will adopt a position of neutrality in the coming conflict.[P] You would not selflessly rescue our wayward sister if you did not have some purity of motive.[B][C]") ]])
        fnCutscene([[ Append("Dafoda:[E|Neutral] Our decision will take time.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] That's about the best we can hope for.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] I will inform our forces to respect your neutrality as soon as I am able.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] As neutrals, perhaps you will aid in negotiations?[B][C]") ]])
        fnCutscene([[ Append("Dafoda:[E|Neutral] The fighting has only just started, and already you're thinking of that?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] My plans go far in advance.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] The longer the fighting goes on, the fewer units who will live to see its end.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] The more lives will be scarred, the more resources wasted on violence.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] We want a swift, decisive victory, but are prepared for the long haul.[B][C]") ]])
        fnCutscene([[ Append("Dafoda:[E|Neutral] The Alraune way is not one of violence.[P] We pray you, or the Administration, win swiftly and bloodlessly.[B][C]") ]])
        fnCutscene([[ Append("Dafoda:[E|Neutral] Though I doubt the Administration will show you much mercy.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] All the more reason to back the rebels.[B][C]") ]])
        fnCutscene([[ Append("Dafoda:[E|Neutral] A factor we will consider when we confer.[B][C]") ]])
        fnCutscene([[ Append("Dafoda:[E|Neutral] Until then, be safe, rebels.[P] Good luck out there.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Same to you, Dafoda.[P] And don't punish Reika too hard, she's trying her best.[B][C]") ]])
        fnCutscene([[ Append("Dafoda:[E|Neutral] She has been punished appropriately to her actions.[P] Threats of punishment do not stop her, but they set an example for others.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] I can say I have the same problem with Christine.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Angry] Hey!!![B][C]") ]])
        fnCutscene([[ Append("Dafoda:[E|Neutral] Ha ha![P] They share a spirit![P] Good luck, rebels!") ]])
        fnCutsceneBlocker()
    
    end

    --Fold party.
    fnAutoFoldParty()
    fnCutsceneBlocker()
end
