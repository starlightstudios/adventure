-- |[Root]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)


-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Topics]|
--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    -- |[Alraunes]|
    if(sActorName == "AlrauneA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Alraune:[VOICE|Alraune] Keep your eyes open out there.[P] Those things could be hiding behind any branch.") ]])
        
    elseif(sActorName == "AlrauneB") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Alraune:[VOICE|Alraune] Dafoda called me a couch potato, can you believe that?[P] I can't help my starchy heritage!") ]])
        
    elseif(sActorName == "AlrauneC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        local iAmphibianRescuedReika = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianRescuedReika", "N")
        if(iAmphibianRescuedReika < 1.0) then
            fnCutscene([[ Append("Alraune:[VOICE|Alraune] Reika's not back yet...[P] And knowing her, she did something stupid but also really brave.") ]])
        else
            fnCutscene([[ Append("Alraune:[VOICE|Alraune] I don't care what Dafoda says, you are heroes to me.[P] Thanks for saving Reika.") ]])
        end
        
    elseif(sActorName == "AlrauneD") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Alraune:[VOICE|Alraune] We're keeping watch here.[P] We'll take in any civilians that come by, too.") ]])
        
    elseif(sActorName == "AlrauneE") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Alraune:[VOICE|Alraune] If this was any other day, I'd be sunning myself in the orchards.[P] I hope war doesn't come to the Biolabs...") ]])
        
    elseif(sActorName == "AlrauneF") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Alraune:[VOICE|Alraune] I shot one of those things.[P] Why won't my hands stop shaking?[P] Didn't I do the right thing?") ]])
        
    elseif(sActorName == "AlrauneG") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        local iAmphibianRescuedReika = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianRescuedReika", "N")
        if(iAmphibianRescuedReika < 1.0) then
            fnCutscene([[ Append("Alraune:[VOICE|Alraune] Please find our wayward leaf-sister.[P] She can be a handful, but she's our handful.") ]])
        else
            fnCutscene([[ Append("Alraune:[VOICE|Alraune] Trying to fix the pipes all by herself?[P] Reika is either brave or stupid.[P] Probably stupid.") ]])
        end
        
    elseif(sActorName == "Reika") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Reika:[VOICE|Alraune] Out of the freezer and into the frying pan.[P] Dafoda put me on probation for three weeks, *and* gave me a commendation![P] I don't know what to think!") ]])
        
    elseif(sActorName == "Dafoda") then
        TA_SetProperty("Face Character", "PlayerEntity")
        local iAmphibianRescuedReika = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianRescuedReika", "N")
        if(iAmphibianRescuedReika == 0.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Dafoda:[VOICE|Alraune] We will owe you a debt of gratitude if you rescue our missing unit.") ]])
        
        --Scene.
        elseif(iAmphibianRescuedReika == 1.0) then
        
            --Flag.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iAmphibianRescuedReika", "N", 2.0)
            
            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Alraune", "Neutral") ]])
            fnCutscene([[ Append("Dafoda:[E|Neutral] You return.[P] Reika arrived a few moments ago.[P] Alive.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] It'd be quite odd if she had arrived any other way.[B][C]") ]])
            fnCutscene([[ Append("Dafoda:[E|Neutral] The little ones have already told me what I need to know.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] So...[P] will you support our cause?[B][C]") ]])
            fnCutscene([[ Append("Dafoda:[E|Neutral] I told you, I cannot state that.[P] We must confer.[P] The Alraunes, and the little ones, and our grandfathers, make decisions together.[B][C]") ]])
            fnCutscene([[ Append("Dafoda:[E|Neutral] At the very least, we will adopt a position of neutrality in the coming conflict.[P] You would not selflessly rescue our wayward sister if you did not have some purity of motive.[B][C]") ]])
            fnCutscene([[ Append("Dafoda:[E|Neutral] I will testify on your behalf, but our decision will take time.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] That's about the best we can hope for.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] I will inform our forces to respect your neutrality as soon as I am able.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] As neutrals, perhaps you will aid in negotiations?[B][C]") ]])
            fnCutscene([[ Append("Dafoda:[E|Neutral] The fighting has only just started, and already you're thinking of that?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Smirk] My plans go far in advance.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] The longer the fighting goes on, the fewer units who will live to see its end.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] The more lives will be scarred, the more resources wasted on violence.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] We want a swift, decisive victory, but are prepared for the long haul.[B][C]") ]])
            fnCutscene([[ Append("Dafoda:[E|Neutral] The Alraune way is not one of violence.[P] We pray you, or the Administration, win swiftly and bloodlessly.[B][C]") ]])
            fnCutscene([[ Append("Dafoda:[E|Neutral] Though I doubt the Administration will show you much mercy.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] All the more reason to back the rebels.[B][C]") ]])
            fnCutscene([[ Append("Dafoda:[E|Neutral] A factor we will consider when we confer.[B][C]") ]])
            fnCutscene([[ Append("Dafoda:[E|Neutral] Until then, be safe, rebels.[P] Good luck out there.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Same to you, Dafoda.[P] And don't punish Reika too hard, she's trying her best.[B][C]") ]])
            fnCutscene([[ Append("Dafoda:[E|Neutral] She has been punished appropriately to her actions.[P] Threats of punishment do not stop her, but they set an example for others.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] I can say I have the same problem with Christine.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Angry] Hey!!![B][C]") ]])
            fnCutscene([[ Append("Dafoda:[E|Neutral] Ha ha![P] I see a resemblance![P] Good luck, rebels!") ]])
            fnCutsceneBlocker()
        
        elseif(iAmphibianRescuedReika == 2.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Dafoda:[VOICE|Alraune] I promise only that Reika will not be a problem for you in the future.[P] Come what may, you have our gratitude.") ]])
        
        
        end
        
    -- |[Civilians]|
    elseif(sActorName == "GolemA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] I hope my little chickadees are all right.[P] These Alraunes keep telling me they are, but I worry...") ]])
        
    elseif(sActorName == "GolemB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] My Lord Golem hasn't said a thing since the explosions.[P] I want to help her, but...") ]])
        
    elseif(sActorName == "GolemC") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|GolemLord] ........") ]])

    end
end
