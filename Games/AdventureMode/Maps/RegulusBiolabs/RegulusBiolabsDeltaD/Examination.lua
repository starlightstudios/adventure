-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
-- |[Objects]|
if(sObjectName == "Sign") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Biological Services barracks.[P] No visitors after 8pm -[P] organics are easily awoken by loud noises!')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Recall all your team members, Unit 443992.[P] That's a direct order, you are not authorized to engage the invading force.[P] Hunker down and await the security team response.[P] -2856')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Notices and emails for the Alraunes here.[P] Mostly just job orders and chit-chat.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Motilvac") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A motilvac, currently inactive.[P] This room is spotless, good job little buddy!)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "RVD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A documentary on Pandemonium is playing.[P] Continents, terrain features, species, history -[P] it's a broad overview.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Computer") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('You're the only ones I can raise on the network.[P] I think something is wrong with the modems, I can't get anyone outside the Biolabs consistently.[P] I heard explosions, and the little ones are telling me about these strange creatures -[P] of course everyone is panicking.[P] I'm trying to keep them calm.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookshelfA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Erotus and the Golden Mirror'.[P] Most of the books on this shelf are high-fantasy stuff.[P] Swords, sorcery, beautiful ladies on horses.[P] The usual.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookshelfB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Everything You Ever Wanted to Know About Plumbing (but were afraid to ask)'.[P] Exactly what it sounds like.[P] Pipes, pressure, how to clear plugged pipes...") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookshelfC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (There are some organic snacks on this shelf.[P] They smell like rotted flesh -[P] but I guess that's a snack for a plant girl.") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookshelfD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The books on this shelf are covered in dust.[P] There's a handheld video game console on the top shelf, which is less dusty.[P] Hmmm...)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookshelfE") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Dating Advice by Unit 40010'.[P] Someone is lonely![P] Don't worry, you'll find the right unit for you someday.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookshelfF") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Professor Killsalot and the Very Bad No Good Chemical Spill'.[P] Killsalot gets covered in goop and looks like a slime girl -[P] and has to avoid getting shot by security robots![P] Oh no!)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookshelfG") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Philosophy of Partirhuman Species'.[P] The chapter on Alraune philosophies has a bookmark in it.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookshelfH") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Pics of Dicks'.[P] Oh my, is the Alraune quartered here one of the rumoured heterosexual partirhumans?[P] Or maybe she just likes dicks.[P] Dicks are great in their own right!)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "PollenWater") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Water mixed with many types of pollens, sugars, and plant fibers.[P] Biological Services uses this to convert humans.[P] Not as efficient as a golem tube, but you can't argue with biology.)") ]])
    fnCutsceneBlocker()
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
