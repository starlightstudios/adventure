-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
if(sObjectName == "ToEpsilonA") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsEpsilonA", "FORCEPOS:7.5x4.0x0")
    
elseif(sObjectName == "ToEpsilonC") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsEpsilonC", "FORCEPOS:42.5x46.0x0")
    
-- |[Objects]|
elseif(sObjectName == "Hello") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Hello.)") ]])
    
elseif(sObjectName == "Wings") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (They are merely two wings of the same bird.[P] Disparate and connected.[P] All are of the whole.)") ]])
    
elseif(sObjectName == "Tubes") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A shell or rock of some sort is floating within this glass tube.[P] Pipes criss-cross through and around the shell.)") ]])
    
elseif(sObjectName == "Device") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (It looks like a warped version of a computer, complete with a screen, but it's totally inactive.[P] There's no electricity running through it.)") ]])
    
elseif(sObjectName == "Machine") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A machine of some sort, with pipes and plates.[P] I have no idea what it does, and it's totally inert.)") ]])
    
elseif(sObjectName == "Pump") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A pumping device, I think.[P] Tubes run from the floor to the ceiling and are filled with a black liquid.[P] Nothing is moving.)") ]])
    
elseif(sObjectName == "TerminalA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Most of the information we have managed to acquire has been due to leaving the sample unattended and allowing it to grow.[P] We would leave, come back the next day, and find modifications.[P] These 'terminals' were not acquired in that manner.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('I ran an electrical current through a conductive section of the substrate and into the terminals.[P] All readings indicated that the electricity conducted normally, and yet, this happened.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Whether or not these are still terminals is debatable.[P] The hardware is completely non-functional and it may as well be rock at this point.[P] But there is no physical precedent for this behavior whatsoever.')") ]])
    
elseif(sObjectName == "TerminalB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('The reappearance of the personalities despite not being present leads me to believe that these are some sort of non-living aware algorithm.[P] They do not have or require homeostasis, and are thus not alive.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('I believe this may cast some light on the mystery of 'the soul' as it is colloquially known.[P] While any arcanist can tell you the soul is important for the usage of magic, beyond that we know very little.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('I posit this instead::[P] That the soul is the unique identifier of a particular algorithm.[P] If that algorithm re-emerges later, even after an individual's death, then that soul is attached to it.[P] It may even be possible for this soul metadata to store information and retransmit it to the instance of the algorithm.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Obviously, testing this is not possible at the moment.[P] But it does suggest a possibility that it may be at some future date.')") ]])
    
elseif(sObjectName == "TerminalC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('An interesting note is that the cut requires a substrate of some description to grow, it cannot grow through mere air.[P] The substrate must be solid, or liquid, but not gaseous or plasmatic.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('I already have more than enough pending projects to follow up on, but establishing if the substrate can travel across the more exotic states of matter, such as quantum superfluids or photonic solids, is yet another future research project.')") ]])
    
elseif(sObjectName == "TerminalD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('During my examinations of these self-assembled murals, I discovered something of interest.[P] There is a particular 'personality' present within these murals, though that term might imply consciousness.[P] There is none.[P] But there is a personality.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('By my count, there are seventeen of these unique personalities.[P] More interestingly, when a 'cut' is taken and grown on a different substrate, even if that cut only contains one personality, the other sixteen will manifest sooner or later.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Precisely what this means has yet to be determined, but it does mean there is some sort of method to the self-assembly.')") ]])
    
elseif(sObjectName == "TerminalE") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('I would hypothesize that these are pumping machines, as that is their most obvious purpose.[P] They are similar to some of the hydraulic jacks in the heat-exchange pipes.[P] But why are they here?[P] Why did they grow here?')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('The fluid within them is water with carbon ash dissolved in it to give it a black appearance.[P] Why that material?[P] So many questions.')") ]])
    
elseif(sObjectName == "TerminalF") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('There are therefore two types of cloning::[P] Physical cloning, wherein the genetic component is copied and allowed to reproduce, and meta-cloning, which involves a duplication of the algorithmic metadata that backs the soul.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('If we assume that all algorithms are valid, even if they are not executed or are not executing right now, then it would be possible to create a new consciousness.[P] In fact, this is likely what we do when giving birth.[P] But, by sheer chance, we could also give birth to an existing consciousness.[P] There would thus either need to be an exclusionary mechanism to prevent meta-cloning in nature, or else such a large number of possible algorithms that it simply hasn't happened.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Given what we know about intelligence as it is, we living creatures cannot be described as simple.[P] If an algorithm has no finite upper maximum execution size, an infinite number of combinations exist.[P] But, for a simpler algorithm, meta-cloning may not only be possible, but common.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('This leads me to posit that the concept of elemental spirits may, in fact, be meta-cloning of a very simple algorithm.[P] But how to prove this?')") ]])
    
elseif(sObjectName == "TerminalG") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('I believe my position can most easily be described in the terms we most commonly use in conversation.[P] We are machines, after all.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('If we assert that the universe itself is data, then we are data within that universe.[P] Or rather, as living creatures, we are algorithms operating on that data.[P] Consciousness is merely a special class of algorithm that is aware it is an algorithm, but it is not particularly different otherwise.[P] A physical force, like gravity, is also an algorithm.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('The ability to store and manipulate data means an algorithm is capable of modifying itself.[P] It is not, however, capable of being something other than an algorithm.')") ]])
    
elseif(sObjectName == "TerminalH") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('I hate working here.[P] Does Unit 9203 even need any staff at this point?[P] She works tirelessly and all I do is just make sure she defragments.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('It's just the two of us in here, and that doesn't help at all.[P] I spend so much time alone, and when I go for a walk I'm afraid that she's going to pop out and babble at me.[P] Or maybe something else will.[P] This place is just...[P] eerie.')") ]])
    
elseif(sObjectName == "TerminalI") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('I believe I have a basic set of axioms from which I can make some derivations.[P] I'll be sure to send them out to the other research staff to see what they think.[P] This is very exciting!')") ]])
    
elseif(sObjectName == "TerminalJ") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('These devices, known as 'tubes' because there is truly no need for any other description, were not based off existing storage tube or conversion tubes.[P] They sprung fully formed on their own.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('The object within mimics how we use storage tubes, but is full of the same inert rock that the rest of the cut self-assembles into.[P] It is not alive and never was, but does mimic being alive.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('When I cut one apart, there were a series of concentric rings within it, which is common in accretion minerals like cassiterite.[P] Is the growth within the tube more similar to how a clam produces a pearl?[P] So much to investigate and so little time.')") ]])
    
elseif(sObjectName == "TerminalK") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('My assistant complained of a knocking sound coming from these self-assembled tubes.[P] While at first I thought it was paranoia, I set some spare PDUs aside and left their audio recording software on.[P] The knocking is quite real.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('When the tubes do not think anyone is near them, the object within grows suddenly and the tube shudders internally, displacing and reassembling its pseudo-machinery.[P] This produces the knocking sound.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Specifically, the tubes must not think anyone is near.[P] This is not an exaggeration.[P] When we enter the room, the knocking ceases.[P] Leaving unhidden recording PDUs nearby likewise causes the knocking to cease.[P] They must believe they are not being observed.[P] We do not know the mechanism for this perception yet.')") ]])
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
