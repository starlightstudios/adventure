-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
if(sObjectName == "ToMovieCLft") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsMovieC", "FORCEPOS:26.5x17.0x0")

elseif(sObjectName == "ToMovieCRgt") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsMovieC", "FORCEPOS:33.0x15.0x0")
    
elseif(sObjectName == "ToMovieA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Sammy] (I don't need to go outside, I need to find the evidence that will clear my name!)") ]])
    fnCutsceneBlocker()

-- |[Objects]|
elseif(sObjectName == "TerminalA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Sammy] ('I was asked to put something evil on this terminal.[P] I didn't know what to put, so I put some pictures of sheep eating grass.[P] Not evil, but probably baaaa-d!')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Sammy] (I don't know, that pun was pretty darn evil...)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "TerminalB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Sammy] ('You think they're going to cast any extras?[P] I want to be in a videograph![P] I've been practicing my acting all week![P] My Slave Units even tried to pay me to stop, that's how much they loved it!')") ]])
    fnCutsceneBlocker()
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
