-- |[Root]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    --Subdivide for all the entities sharing this script.
    if(sActorName == "GolemA") then
        
        
        local iMovieTalkedToReceptionist = VM_GetVar("Root/Variables/Chapter5/Scenes/iMovieTalkedToReceptionist", "N")
        
        --First time.
        if(iMovieTalkedToReceptionist == 0.0) then
            
            --Flag.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iMovieTalkedToReceptionist", "N", 1.0)
        
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] Welcome to the Amphibian Research Laboratories![P] Would you like me to take your coat?[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] ...[P] Sorry I have to say that even if you're not wearing a coat.[B][C]") ]])
            fnCutscene([[ Append("Almond:[E|Neutral] Nice name for a research lab -[P] [EMOTION|Sammy|Gun]or secret evil base for Evil Corp![B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] I don't care what you want to call it.[P] Just please stand here and talk to me.[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] It gets so lonely in here...[B][C]") ]])
            fnCutscene([[ Append("Almond:[E|Neutral] Mistreating their employees![P] Evil Corp is really evil![B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] *We're being recorded![P] Watch what you say!*[B][C]") ]])
            fnCutscene([[ Append("Almond:[E|Neutral] *I'm starring in a movie, dummy![P] Of course we're being recorded!*[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] *WHAT!?*[B][C]") ]])
            fnCutscene([[ Append("Almond:[E|Neutral] I'm sorry you hapless evil minion, but you won't be stopping me.[P] Stand aside![P] I must clear my name![B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] Uhhhh.[P] Well, the exhibits are to your left.[P] Please enjoy your stay.") ]])
            fnCutsceneBlocker()
        
        --Repeats.
        else
        
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] *So are you a videograph star?[P] Can I have your autograph?[P] You're really cute...*[B][C]") ]])
            fnCutscene([[ Append("Almond:[E|Sexy] *Come visit me after the shoot.[P] I love signing autographs for my fans!*[B][C]") ]])
            fnCutscene([[ Append("Almond:[E|Neutral] *Actually you're my first fan.[P] But I expect to have more once this videograph is actually released.*[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] Thank you for saving me from this evil corporation, great hero![P] I love you![B][C]") ]])
            fnCutscene([[ Append("Almond:[E|Neutral] *Try to keep a lid on the overacting, rookie...*") ]])
            fnCutsceneBlocker()
        
        end
    end
end
