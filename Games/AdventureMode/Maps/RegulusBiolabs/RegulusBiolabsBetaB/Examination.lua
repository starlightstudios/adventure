-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
-- |[Objects]|
if(sObjectName == "Wine") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Barrels of fermenting wine.[P] I'm sure these will be delicious in a few months!)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Bag") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The bag is full of plastic cards with descriptions of wine and beer flavours on them.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Looks like someone left a chat session open.[P] PJen27:: 'Got a note from Daisy today.[P] She said she wanted to talk about something really important, and that she'd be over at the Raiju Ranch, waiting for me to get off work.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Satch:: 'Woah, you think she's going to ask you go steady?')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (PJen27:: 'Dunno![P] Dunno!')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (PJen27:: 'Ah crud, my Lord Golem is telling me we need to go.[P] I'll try to slip away and go visit Daisy when she's not looking.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Satch:: 'Why are you going?[P] I just heard an explosion, are you okay?')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Satch:: 'Hello?')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Satch:: 'Aw geez.[P] Nobody is messaging me back all of the sudden.[P] Hope you're okay.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Attention all winery staff::[P] Evacuate immediately to transit station Omicron.[P] This is not a drill.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Shelf") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (An assortment of cheeses, for wine tastings.[P] Goat, sheep, cow...[P] human...)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Note") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Wine Fermentation Tour Scheduling...[P] List of available hours...)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Glass") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The glass here was shattered by an immense force...)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Barrel") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Barrels of alcohol, presumably for guests.[P] Alcohol doesn't have an inebriating effect on golems, but it can taste good with the right cheese!)") ]])
    fnCutsceneBlocker()
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
