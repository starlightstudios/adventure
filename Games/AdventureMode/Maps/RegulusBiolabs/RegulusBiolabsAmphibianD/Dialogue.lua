-- |[Root]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    --Rescue Reika!
    if(sActorName == "Reika") then
        
        --Variables.
        local iSawRaijuIntro         = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawRaijuIntro", "N")
        local iSX399JoinsParty       = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
        local iAmphibianMetDafoda    = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianMetDafoda", "N")
        local iAmphibianRescuedReika = VM_GetVar("Root/Variables/Chapter5/Scenes/iAmphibianRescuedReika", "N")
        
        --Achievement.
        AM_SetPropertyJournal("Unlock Achievement", "FinishReika")
        
        --First dialogue.
        if(iAmphibianRescuedReika == 0.0) then
        
            --Flag.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iAmphibianRescuedReika", "N", 1.0)
        
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Alraune", "Neutral") ]])
            fnCutscene([[ Append("Reika:[E|Neutral] Unnnnggghh....[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Hello, can you hear me?[B][C]") ]])
            if(iSX399JoinsParty == 1.0) then
                fnCutscene([[ Append("SX-399:[E|Happy] Time to shine, 399![P] I'll warm her right up![B][C]") ]])
                fnCutscene([[ Append("55:[E|Upset] Absolutely do not use your weapon![B][C]") ]])
                fnCutscene([[ Append("SX-399:[E|Happy] Nope![P] Steam Lord skills![P] C'mere, plant girl![B][C]") ]])
                fnCutscene([[ Append("SX-399:[E|Smirk] ...[B][C]") ]])
                fnCutscene([[ Append("SX-399:[E|Smirk] There you go![B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] Ah, you merely increased your power core's clock to heat your chassis.[B][C]") ]])
                fnCutscene([[ Append("SX-399:[E|Smirk] In retrospect, I may have been able to do that to the ice wall.[B][C]") ]])
                fnCutscene([[ Append("SX-399:[E|Smirk] Being clever is really hard.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Fantastic work, SX-399![P] She's coming around!") ]])
                fnCutsceneBlocker()
                fnCutsceneWait(45)
                fnCutsceneBlocker()
                
                --Change.
                fnCutsceneSetFrame("Reika", "Crouch")
                fnCutsceneWait(45)
                fnCutsceneBlocker()
                fnCutsceneSetFrame("Reika", "Null")
                fnCutsceneWait(25)
                fnCutsceneBlocker()
                
                --Dialogue.
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Alraune", "Neutral") ]])
                fnCutscene([[ Append("Reika:[E|Neutral] C-[P]cold...[B][C]") ]])
                fnCutscene([[ Append("SX-399:[E|Smirk] Cuddle with me.[P] I'll warm you up.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] ...[B][C]") ]])
                fnCutscene([[ Append("SX-399:[E|Smirk] Jealous?[P] You know, you could probably pull the same trick, cutie-5.[P] C'mon...[B][C]") ]])
                fnCutscene([[ Append("Reika:[E|Neutral] Who...[P] who are you?[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] Units who are rescuing you.[P] You do not need to know more.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] In fact, we have some questions for you.[P] Did you sabotage the heat transfer system?[B][C]") ]])
                fnCutscene([[ Append("Reika:[E|Neutral] No![P] No![P] I came here to fix it![B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] That's okay, it's what we figured.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] There is an authenticator chip in her leafy skirt.[P] This is Unit 765532, 'Reika'.[B][C]") ]])
                if(iAmphibianMetDafoda == 1.0) then
                    fnCutscene([[ Append("Christine:[E|Smirk] Dafoda sent us to rescue you, Reika.[P] Do you have any medical problems besides the hypothermia?[B][C]") ]])
                else
                    fnCutscene([[ Append("Christine:[E|Smirk] Nice to meet you, Reika.[P] Do you have any medical problems besides the hypothermia?[B][C]") ]])
                end
                fnCutscene([[ Append("Reika:[E|Neutral] 'Besides' is doing a lot of work in that sentence.[B][C]") ]])
                fnCutscene([[ Append("SX-399:[E|Smirk] We're not doctors, unfortunately.[P] You'll probably need proper treatment that we can't give.[B][C]") ]])
                if(iSawRaijuIntro == 0.0) then
                    fnCutscene([[ Append("Sophie:[E|Smirk] But at least you're not frozen![B][C]") ]])
                end
                fnCutscene([[ Append("Reika:[E|Neutral] Yeah, yeah...[P] I'm fine.[P] I'll be all right.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] So you were trying to fix the heat transfer pipes?[B][C]") ]])
                fnCutscene([[ Append("Reika:[E|Neutral] Affirmative.[P] There were some explosions and I was in the gamma laboratories at the time.[B][C]") ]])
                fnCutscene([[ Append("Reika:[E|Neutral] Then I heard the little ones calling out for help, so I went to investigate.[P] The whole beta habitat was rapidly freezing over.[B][C]") ]])
                fnCutscene([[ Append("Reika:[E|Neutral] The heat transfer pipes here pump excess heat out of the city and radiate it into space, then pump the cooled water back in.[P] If there's a major leak, it could freeze the entire biolabs out.[B][C]") ]])
                fnCutscene([[ Append("Reika:[E|Neutral] I was trying to get the backup transfer pipe in here open, but when I turned around, a steam leak had frosted over the door.[B][C]") ]])
                fnCutscene([[ Append("Reika:[E|Neutral] I thought I was done for...[B][C]") ]])
                fnCutscene([[ Append("SX-399:[E|Smirk] Not today![B][C]") ]])
                if(iSawRaijuIntro == 0.0) then
                    fnCutscene([[ Append("Sophie:[E|Happy] Yeah![P] Look at these heroic units I follow around![B][C]") ]])
                end
                fnCutscene([[ Append("Christine:[E|Smirk] I'm sure somebody would have found you...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Sad] Of course they would have...[B][C]") ]])
                fnCutscene([[ Append("Reika:[E|Neutral] Ungh, Dafoda is going to kill me when I get back.[B][C]") ]])
                fnCutscene([[ Append("Reika:[E|Neutral] I can make my way on my own.[P] We Alraunes can talk to plants, I won't even be seen on the way to Biological Services.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] You are certain you do not require an escort?[B][C]") ]])
                if(iSawRaijuIntro == 0.0) then
                    fnCutscene([[ Append("Reika:[E|Neutral] Positive.[P] You four would just slow me down.[P] I'm fine, really.[B][C]") ]])
                else
                    fnCutscene([[ Append("Reika:[E|Neutral] Positive.[P] You three would just slow me down.[P] I'm fine, really.[B][C]") ]])
                end
                fnCutscene([[ Append("Christine:[E|Smirk] If you insist.[P] We'll see you back at Biological Services HQ then.[B][C]") ]])
                fnCutscene([[ Append("Reika:[E|Neutral] Yeah, thanks.[P] Thanks a lot...") ]])
                fnCutsceneBlocker()
            
            --No SX-399.
            else
                fnCutscene([[ Append("55:[E|Angry] I believe I can warm her up with selective discharges...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] Wait - [P][CLEAR]") ]])
                fnCutscene([[ Append("Reika:[E|Neutral] [SOUND|World|SparksC]YEEEEEOOWWWWWWWWWW!!!!!") ]])
                fnCutsceneBlocker()
                fnCutsceneWait(25)
                fnCutsceneBlocker()
                
                --Change.
                fnCutsceneSetFrame("Reika", "Crouch")
                fnCutsceneWait(15)
                fnCutsceneBlocker()
                fnCutsceneSetFrame("Reika", "Null")
                fnCutsceneWait(25)
                fnCutsceneBlocker()
                
                --Dialogue.
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Alraune", "Neutral") ]])
                fnCutscene([[ Append("Reika:[E|Neutral] I'm up![P] I'm up![P] Yowch![B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Angry] We're trying to warm her up, not kill her![B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] Her skin is covered in a thin layer of water.[P] At a low voltage, the electricity discharges evenly across it, warming the skin.[B][C]") ]])
                if(iSawRaijuIntro == 0.0) then
                    fnCutscene([[ Append("Sophie:[E|Offended] You could at least have started with a lower voltage and worked your way up![B][C]") ]])
                    fnCutscene([[ Append("55:[E|Neutral] I did.[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Offended] Honestly I prefer my vegetables steamed, *not fried*.[B][C]") ]])
                else
                    fnCutscene([[ Append("Christine:[E|Offended] Oh yes, you like your vegetables nice and crispy.[B][C]") ]])
                end
                fnCutscene([[ Append("Reika:[E|Neutral] It -[P] it worked.[P] Owwww...[B][C]") ]])
                fnCutscene([[ Append("55:[E|Smirk] The efficacy of this approach was not in question.[B][C]") ]])
                fnCutscene([[ Append("Reika:[E|Neutral] I'm not trying to be rude, Command Unit, but who exactly are you?[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] Units who are rescuing you.[P] You do not need to know more.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] In fact, we have some questions for you.[P] Did you sabotage the heat transfer system?[B][C]") ]])
                fnCutscene([[ Append("Reika:[E|Neutral] No![P] No![P] I came here to fix it![B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] That's okay, it's what we figured.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] There is an authenticator chip in her leafy skirt.[P] This is Unit 765532, 'Reika'.[B][C]") ]])
                if(iAmphibianMetDafoda == 1.0) then
                    fnCutscene([[ Append("Christine:[E|Smirk] Dafoda sent us to rescue you, Reika.[P] Do you have any medical problems besides the hypothermia?[B][C]") ]])
                else
                    fnCutscene([[ Append("Christine:[E|Smirk] Nice to meet you, Reika.[P] Do you have any medical problems besides the hypothermia?[B][C]") ]])
                end
                fnCutscene([[ Append("Reika:[E|Neutral] 'Besides' is doing a lot of work in that sentence.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] You'll have to see a doctor as soon as possible, we're not in position to properly diagnose an organic such as yourself.[B][C]") ]])
                fnCutscene([[ Append("Reika:[E|Neutral] Yeah, yeah...[P] I'm fine.[P] I'll be all right.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] So you were trying to fix the heat transfer pipes?[B][C]") ]])
                fnCutscene([[ Append("Reika:[E|Neutral] Affirmative.[P] There were some explosions and I was in the gamma laboratories at the time.[B][C]") ]])
                fnCutscene([[ Append("Reika:[E|Neutral] Then I heard the little ones calling out for help, so I went to investigate.[P] The whole beta habitat was rapidly freezing over.[B][C]") ]])
                fnCutscene([[ Append("Reika:[E|Neutral] The heat transfer pipes here pump excess heat out of the city and radiate it into space, then pump the cooled water back in.[P] If there's a major leak, it could freeze the entire Biolabs out.[B][C]") ]])
                fnCutscene([[ Append("Reika:[E|Neutral] I was trying to get the backup transfer pipe in here open, but when I turned around, a steam leak had frosted over the door.[B][C]") ]])
                fnCutscene([[ Append("Reika:[E|Neutral] I thought I was done for...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] I'm sure somebody would have found you...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Sad] Of course they would have...[B][C]") ]])
                fnCutscene([[ Append("Reika:[E|Neutral] Ungh, Dafoda is going to kill me when I get back.[B][C]") ]])
                fnCutscene([[ Append("Reika:[E|Neutral] I can make my way on my own.[P] We Alraunes can talk to plants, I won't even be seen on the way to Biological Services.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] You are certain you do not require an escort?[B][C]") ]])
                if(iSawRaijuIntro == 0.0) then
                    fnCutscene([[ Append("Reika:[E|Neutral] Positive.[P] You three would just slow me down.[P] I'm fine, really.[B][C]") ]])
                else
                    fnCutscene([[ Append("Reika:[E|Neutral] Positive.[P] You two would just slow me down.[P] I'm fine, really.[B][C]") ]])
                end
                fnCutscene([[ Append("Christine:[E|Smirk] If you insist.[P] We'll see you back at Biological Services HQ then.[B][C]") ]])
                fnCutscene([[ Append("Reika:[E|Neutral] Yeah, thanks.[P] Thanks a lot...") ]])
                fnCutsceneBlocker()
                
            end
        
        --Repeats.
        else
        
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Alraune", "Neutral") ]])
            fnCutscene([[ Append("Reika:[E|Neutral] Dafoda is probably going to give me a thrashing when I get back...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Why?[P] You were trying to help.[B][C]") ]])
            fnCutscene([[ Append("Reika:[E|Neutral] I ignored the evacuation order and went alone.[P] She hates it when I do that.[B][C]") ]])
            fnCutscene([[ Append("Reika:[E|Neutral] But there wasn't anyone else around.[P] If I hadn't done what I did, the entire Biolabs would be a freezer by now.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] We'll vouch for you, then![B][C]") ]])
            fnCutscene([[ Append("Reika:[E|Neutral] Thank you for that, and saving my life.[P] You're a pretty good bunch.") ]])
        
        end
    end
end
