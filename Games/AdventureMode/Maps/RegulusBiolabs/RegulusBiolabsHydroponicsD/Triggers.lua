-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
if(sObjectName == "Announcement") then
    
    --Repeat check.
    local iSawAnnouncement = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawAnnouncement", "N")
    if(iSawAnnouncement == 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iSawAnnouncement", "N", 1.0)
    
    --Variables.
    local iKnowsTubeHoles = VM_GetVar("Root/Variables/Chapter5/Scenes/iKnowsTubeHoles", "N")
    local iSX399JoinsParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("Intercom:[VOICE|Golem] Attention personnel::[P] An evacuation order has been issued.[P] The Datacore building is locked down with security-red clearance required.[P] Please take an alternate path to Transit Station Omicron.[P] Thank you and have an obsequious day![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Red lockout?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] We will need a keycard to enter the datacore building to the north.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Smug] Unless you would like to blow the door open.[B][C]") ]])
    if(iKnowsTubeHoles == 0.0) then
        fnCutscene([[ Append("Christine:[E|Offended] Not today, 55.[P] That would depressurize the area and kill all the biological specimens here.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] I was aware of that.[P] I apologize.[P] My attempt at comedy failed.[B][C]") ]])
        if(iSX399JoinsParty == 1.0) then
            fnCutscene([[ Append("SX-399:[E|Smirk] Good try, hon.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Thank you.[B][C]") ]])
        end
    else
        fnCutscene([[ Append("Christine:[E|Offended] You're starting to sound like your sister.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] No, I apologize.[P] My attempt at comedy failed.[P] That was meant to be a joke.[B][C]") ]])
        if(iSX399JoinsParty == 1.0) then
            fnCutscene([[ Append("SX-399:[E|Smirk] Good try, hon.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Thank you.[B][C]") ]])
        end
        fnCutscene([[ Append("Christine:[E|Smirk] Oh, okay.[B][C]") ]])
    end
    fnCutscene([[ Append("Christine:[E|Neutral] Maybe we can find another way into the datacore building.[P] Let's try going over the surface.") ]])
    fnCutsceneBlocker()
end
