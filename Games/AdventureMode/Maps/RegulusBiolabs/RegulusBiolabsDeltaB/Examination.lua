-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
-- |[Objects]|
if(sObjectName == "SignA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Please extinguish campfires if you are not attending them.[P] Forest fires are something that can only be prevented by you.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Something about that slogan just isn't quite right...)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "SignB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('North:: Raiju Ranch and Breeding Program facilities.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('East:: Draining Research testing area, Biological Services headquarters, Aviary.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('South:: Defoliant storage site, Aquatic Genetics Labs, Waterflow Connection to Alpha Laboratories.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('West:: Duck Pond, Transit Station Omicron, Connection to Gamma Laboratories.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Terminal") then
    VM_SetVar("Root/Variables/Chapter5/WorldState/iNoWarpsAllowed", "N", 0.0)
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A list of campsites and which Lord Units have reserved each.[P] There's a backlog of several months.[P] This must be a popular area to make out.") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Pot") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Ooh, someone was cooking a tasty methylflourophosphate chowder.[P] Tasty for golems and command units alike!") ]])
    fnCutsceneBlocker()
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
