-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
if(sObjectName == "ToGammaB") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsGammaB", "FORCEPOS:44.0x4.0x0")
    
-- |[Objects]|
elseif(sObjectName == "OilMaker") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (An oilmaker.[P] The only flavour packets available are steel shavings.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Instructions for the team here to seal the door and proceed to the Raiju Ranch.[P] A direct order from Unit 2856.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (An inventory of equipment and units stationed here.[P] All but two are listed as relocated.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Sorry, I won't be able to attend our sunrise watching, Lauralie.[P] Just got an important order from the Head of Research to relocate my whole team to the Raiju Ranch.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('I tried to get confirmation but now the radios are out.[P] This message will send as soon as a network connection is established.[P] Hope you haven't left already![P] Love you.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (An unencrypted list of Slave Unit designations, marked for...[P] retirement.[P] Fortunately, only a few are listed as retired.[P] I hope the rest got away...)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalE") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Five Habits of Highly Successful Lord Units'.[P] I get this on my terminal from Command Unit 105322 every morning.[P] Not even security units are immune to the power of chain letters.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "RVDA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A test pattern is playing, indicating the network connection is offline.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "RVDB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A videograph named 'Proper Decontamination Procedures' is playing on loop.[P] It shows a researcher washing off her chassis and clothes thoroughly.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Oilmaker") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A note on the oilmaker says 'LORD UNITS ONLY'.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Rack") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A weapon rack with a pulse rifle and spare charge packs. Why is it here and not taken like the others?)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Computer") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A computer left here by the Lord Golem in charge of this facility.[P] The hard drive has been wiped.)") ]])
    fnCutsceneBlocker()

--Exit, has a cutscene. Breaching tools are needed.
elseif(sObjectName == "BreachDoor") then
    local iBreachedDoor      = VM_GetVar("Root/Variables/Chapter5/Scenes/iBreachedDoor", "N")
    local iGotBreachingTools = VM_GetVar("Root/Variables/Chapter5/Scenes/iGotBreachingTools", "N")
    
    --Door is already open, just move to the next room.
    if(iBreachedDoor == 1.0) then
        return
        AL_BeginTransitionTo("RegulusBiolabsEpsilonA", "FORCEPOS:7.5x40.0x0")
        
    --Door isn't open and we don't have the tools.
    elseif(iBreachedDoor == 0.0 and iGotBreachingTools == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (This door leads into the Epsilon labs, but the deadbolts are down and the edges were welded shut.[P] It's effectively a wall.)") ]])
        fnCutsceneBlocker()
        
    --Door isn't open and we have the tools.
    elseif(iBreachedDoor == 0.0 and iGotBreachingTools == 1.0) then
        
        --Variables.
        local bIsSX399Present = fnIsCharacterPresent("SX-399")
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iBreachedDoor", "N", 1.0)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iMainQuestBlock", "N", 52.0)
        
        --Unset collisions.
        AL_SetProperty("Set Collision", 45, 1, 0, 14)
        AL_SetProperty("Set Collision", 45, 2, 0, 14)
        AL_SetProperty("Set Collision", 46, 1, 0, 16)
        AL_SetProperty("Set Collision", 46, 2, 0, 16)
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        if(bIsSX399Present == true) then
            fnCutscene([[ Append("Christine:[E|Neutral] All right loves, stand back and let a repair unit get to work.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] We will keep you covered.") ]])
        else
            fnCutscene([[ Append("Christine:[E|Neutral] All right love, stand back and let a repair unit get to work.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] I will keep you covered.") ]])
        end
        fnCutsceneBlocker()
        
        --Fade out.
        fnCutsceneWait(25)
        fnCutscene([[ AL_SetProperty("Activate Fade", 25, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
        fnCutsceneBlocker()
        
        --Sounds.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] Come on you stupid -[P] woah![P] This welder has some kick to it![P] Woo![B][C]") ]])
        fnCutscene([[ Append("55:[VOICE|Tiffany] What happened?[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] Were you not watching?[P] I almost sawed my own arm off![B][C]") ]])
        fnCutscene([[ Append("55:[VOICE|Tiffany] I have other things I am attending to.[P] Specifically, protecting you.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Change layers.
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "Floor2Cut", false) ]])
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "Floor2Uncut", true) ]])
        
        --Reposition.
        fnCutsceneTeleport("Christine", 45.75, 3.50)
        fnCutsceneFace("Christine", 0, -1)
        fnCutsceneTeleport("Tiffany", 44.25, 6.50)
        fnCutsceneFace("Tiffany", 0, 1)
        if(bIsSX399Present == true) then
            fnCutsceneTeleport("SX-399", 47.25, 6.50)
            fnCutsceneFace("SX-399", -1, -1)
        end
        
        --Fade in.
        fnCutsceneWait(45)
        fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] Uhhhh, all done?") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    
        --Movement.
        fnCutsceneMoveFace("Christine", 45.75, 4.50, 0, -1)
        fnCutsceneMove("Tiffany", 44.75, 4.50)
        fnCutsceneFace("Tiffany", 0, -1)
        if(bIsSX399Present == true) then
            fnCutsceneMove("SX-399", 46.75, 4.50)
            fnCutsceneFace("SX-399", 0, -1)
        end
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        if(bIsSX399Present == false) then
            fnCutscene([[ Append("55:[E|Neutral] Excellent.[P] We can access the Epsilon Laboratories now.[P] Good work.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] Good work?[P] I wound up sawing through it![B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] This military-grade plasteel has absolutely no give.[P] If I had known that when I started...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] The nature of the cut does not matter, Christine.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] Maybe not to you.[P] Any repair unit would laugh at this.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Just please don't tell Sophie.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] If I agree not to tell your tandem unit, will you drop this pointless issue?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Deal.[P] Let's go.") ]])
            fnCutsceneBlocker()
            
            --Fold.
            fnCutsceneMove("Tiffany", 45.75, 4.50)
            fnCutsceneBlocker()
        else
            fnCutscene([[ Append("55:[E|Neutral] Excellent.[P] We can access the Epsilon Laboratories now.[P] Good work.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Flirt] Look at that![P] First try on military-grade plasteel![P] You're a pro![B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] Are you kidding?[P] I had to just saw through it![B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Smirk] Yes, that is the point.[P] That's why they made it the way they did.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Smirk] Though if you get lucky sometimes recrystallization exposes a plane of weakness.[P] But that's probably not the case for the new stuff.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Have you dealt with this material before?[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Smirk] All the time.[P] A lot of transport cars on the tram network are made out of repurposed plasteel.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Flirt] Breaking and entering is a hobby.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] I guess I did the best I could with the tools at hand.[P] That's all a repair unit can ask for.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] I assure you, complaining about it will increase the quality of the cut.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Upset] Can we continue?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] All right, all right.[P] Let's go.") ]])
            fnCutsceneBlocker()
            
            --Fold.
            fnCutsceneMove("Tiffany", 45.75, 4.50)
            fnCutsceneMove("SX-399", 45.75, 4.50)
            fnCutsceneBlocker()
        end
        
        --Fold.
        fnCutscene([[ AL_SetProperty("Fold Party") ]])
        fnCutsceneBlocker()
    
    end
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
