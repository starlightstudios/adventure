-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
if(sObjectName == "Ladder") then
    AudioManager_PlaySound("World|ClimbLadder")
    AL_BeginTransitionTo("RegulusBiolabsGammaWestC", "FORCEPOS:6.0x7.0x0")
    
-- |[Objects]|
elseif(sObjectName == "Boxes") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (These boxes contain assorted junk.[P] Nothing useful pertaining to my mission.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BigBox") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The only large storage container in the building.[P] It contains nothing useful.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "DistractionBox") then
    local iRaibieFoundNoisemaker = VM_GetVar("Root/Variables/Chapter5/Scenes/iRaibieFoundNoisemaker", "N")
    if(iRaibieFoundNoisemaker == 0.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iRaibieFoundNoisemaker", "N", 1.0)
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ([SOUND|World|TakeItem]There are noisemaking devices within the box, often used for frivolous parties.[P] This could be useful...)") ]])
        fnCutsceneBlocker()
    
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (I've already taken the noisemaker device.[P] Now to find a place to use it.)") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "Dummies") then
    local iRaibieFoundNoisemaker = VM_GetVar("Root/Variables/Chapter5/Scenes/iRaibieFoundNoisemaker", "N")
    local iRaibieFoundTranqGun   = VM_GetVar("Root/Variables/Chapter5/Scenes/iRaibieFoundTranqGun", "N")
    if(iRaibieFoundNoisemaker == 0.0 and iRaibieFoundTranqGun == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (The dummy softly says 'owie' when poked.[P] It is extremely lifelike, and could fool even a perfect machine from a distance.)") ]])
        fnCutsceneBlocker()
    
    elseif(iRaibieFoundNoisemaker == 1.0 and iRaibieFoundTranqGun == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (The dummy softly says 'owie' when poked.[P] It is extremely lifelike, and could fool even a perfect machine from a distance.)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (I could probably set off a noisemaker to lure Christine here, but I would have no way of subduing her while she was distracted...)") ]])
        fnCutsceneBlocker()
    
    
    elseif(iRaibieFoundNoisemaker == 0.0 and iRaibieFoundTranqGun == 1.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (The dummy softly says 'owie' when poked.[P] It is extremely lifelike, and could fool even a perfect machine from a distance.)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (If I could lure Christine here, these dummies would distract her long enough to tranquilize her.[P] But how to lure her here?)") ]])
        fnCutsceneBlocker()
    
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (The dummy softly says 'owie' when poked.[P] It is extremely lifelike, and could fool even a perfect machine from a distance.)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Should I set up a noisemaker here?)[BLOCK]") ]])

        --Decision script is this script. It must be surrounded by quotes.
        local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
        fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"SetNoisemaker\") ")
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Not Yet\",  " .. sDecisionScript .. ", \"NoCancel\") ")
        fnCutsceneBlocker()
   
    end
        
elseif(sObjectName == "RVD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (An evacuation notice is playing on the RVD.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Yeah, I had to do the training when I volunteered to work in abductions.[P] Of course they didn't pick me, but I did get a perfect score on all the levels.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalB") then
    local iRaibieFoundKeycode = VM_GetVar("Root/Variables/Chapter5/Scenes/iRaibieFoundKeycode", "N")
    if(iRaibieFoundKeycode == 0.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iRaibieFoundKeycode", "N", 1.0)
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('So the incinerator has been down for a while, and the garbage is starting to pile up.[P] Contamination protocols say we can't remove anything from the premises and have to hit our own chassis' with radiation to clean them.')[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('If you need to get to the storage shed, I included the passcode as an attachment.[P] Not like that's a security vulnerability, right?')[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ([SOUND|World|Keycard]I have acquired the keycode to the external storage shed.)") ]])
        fnCutsceneBlocker()
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('So the incinerator has been down for a while, and the garbage is starting to pile up.[P] Contamination protocols say we can't remove anything from the premises and have to hit our own chassis' with radiation to clean them.')[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('If you need to get to the storage shed, I included the passcode as an attachment.[P] Not like that's a security vulnerability, right?')") ]])
        fnCutsceneBlocker()
        
        
    end
    
elseif(sObjectName == "TerminalC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Whoever was using this terminal last left a copy of Mr. Needlemouse 2 running.[P] They are currently in the Chemical Facility zone.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Hey do you remember when Dr. Maisie brought in a tranquilized antelope and asked us to euthanize it due to it being a disease vector?[P] We still have the tranq gun in storage since we can't incinerate it.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('I thought she was going to tear my head off when I said she couldn't leave due to quarantine protocols.[P] Luckily, she understood and did a decontamination run.[P] Had to leave her clothes behind, though.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalE") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Always keep your laboratory clean when it is not in use!')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalF") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('So why are we setting up a firing range again?[P] This is a tissue testing lab, you know, for curing diseases.[P] Unless you count killing people as curing diseases, which is technically true...')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalG") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Due to the fabrication shortage, I've taken to some rather creative means.[P] Did you know that the electrical output of a those party-noise-bombs can stimulate tissue growth?')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('I purchased a number of them and put them in the white crate.[P] You know, the only white crate in the whole building.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalH") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The terminal has a list of videographs stored.[P] 'All My Processors' dominates the list.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalI") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The terminal has a list of videographs stored.[P] 'All My Processors' dominates the list.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalJ") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Self-assembling quasi-tissue from the Epsilon labs was a no-go.[P] No results.[P] Couldn't get it to take on any substrate, regardless of availability of energy or material.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalK") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Lab-grown muscle tissue as a use for dietary supplements?[P] Could be useful for the carnivorous partirhuman population of Pandemonium, if we're looking to make allies for some reason.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalL") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('I don't know, I thought the arc with the ninja magical girl showing up out of nowhere really caused it to drag.[P] Am I stuffy?[P] Is my taste bad?')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalM") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('The dummies we got are bizarrely authentic.[P] Was it necessary to install speech synthesizers in them?[P] All they do is go 'Owie' when you touch them.[P] Was that absolutely vital?')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalN") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('The cut we were provided with is not growing in an aquatic environment.[P] I would hypothesize that the fluid interrupts the self-assembly, but since we haven't got a cut growing anywhere yet, it might just be inert.')") ]])
    fnCutsceneBlocker()

-- |[Cutscene Case]|
elseif(sObjectName == "SetNoisemaker") then
	WD_SetProperty("Hide")
    
    --Scene.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("55:[VOICE|Tiffany] *Click* Best to be far away when this goes off...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Fade out.
    fnCutscene([[ AL_SetProperty("Activate Fade", 25, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    
    --Clone Christine. Teleport the real one off camera. That way the viewcone is hidden.
    TA_Create("CutsceneChristine")
        TA_SetProperty("Position", 10, 10)
        TA_SetProperty("Facing", gci_Face_South)
        TA_SetProperty("Clipping Flag", false)
        fnSetCharacterGraphics("Root/Images/Sprites/Christine_Raibie/", true)
    DL_PopActiveObject()
    fnCutsceneTeleport("Christine", -13.25, -26.10)
    
    --Reposition Christine, camera follows her.
    fnCutsceneTeleport("CutsceneChristine", 11.75, 15.30)
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Focus Actor Name", "CutsceneChristine")
    DL_PopActiveObject()
        
    --Sound.
    fnCutscene([[ AudioManager_PlaySound("World|Noisemaker") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Reposition.
    fnCutsceneTeleport("Tiffany", 13.25, 26.10)
    fnCutsceneSetFrame("Tiffany", "Stealth")
    fnCutsceneBlocker()
    
    --While fading in...
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneMove("CutsceneChristine", 10.75, 23.50, 2.50) 
    fnCutsceneMove("CutsceneChristine", 13.25, 23.50, 2.50) 
    fnCutsceneMove("CutsceneChristine", 13.25, 20.50, 2.50) 
    fnCutsceneMove("CutsceneChristine", 15.25, 18.50, 2.50) 
    fnCutsceneFace("CutsceneChristine", 0, -1)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("Combat|Impact_CrossSlash") ]])
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutsceneMove("CutsceneChristine", 17.25, 18.50, 2.50) 
    fnCutsceneFace("CutsceneChristine", 0, -1)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("Combat|Impact_CrossSlash") ]])
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Christine:[VOICE|Christine] RRRRAAAGGHHH!!!") ]])
    fnCutsceneBlocker()
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Focus Actor Name", "Tiffany")
    DL_PopActiveObject()
    fnCutsceneWait(55)
    fnCutsceneBlocker()
    
    --55 moves out.
    fnCutsceneSetFrame("Tiffany", "Null")
    fnCutsceneBlocker()
    fnCutsceneMove("Tiffany", 14.25, 26.10, 1.50)
    fnCutsceneMove("Tiffany", 14.25, 23.50, 1.50)
    fnCutsceneMove("Tiffany", 17.25, 23.50, 1.50)
    fnCutsceneFace("Tiffany", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|TranqShot") ]])
    fnCutsceneMove("CutsceneChristine", 17.25, 16.50, 0.30) 
    fnCutsceneFace("CutsceneChristine", 0, 1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Christine:[VOICE|Christine] Rrrraagh?[P] Urrggh...[B][C]") ]])
    fnCutscene([[ Append("55:[VOICE|Tiffany] 771852?[P] Can you understand me?[B][C]") ]])
    fnCutscene([[ Append("Christine:[VOICE|Christine] More![P] More!") ]])
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|TranqShot") ]])
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Dialogue.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Focus Actor Name", "CutsceneChristine")
    DL_PopActiveObject()
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Christine:[VOICE|Christine] Again...[P] Please...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Blackout.
    fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutscene([[ AudioManager_PlaySound("World|TranqShot") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Boot out.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iRaibieQuest", "N", 8.0)
    
    --Go to the Raiju Ranch.
    fnCutscene([[ AL_BeginTransitionTo("RegulusBiolabsRaijuRanchA", "FORCEPOS:46.0x12.0x0") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "NoCancel") then
	WD_SetProperty("Hide")

-- |[Errors]|
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
