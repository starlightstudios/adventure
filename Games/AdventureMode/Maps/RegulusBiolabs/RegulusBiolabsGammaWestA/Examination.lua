-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
if(sObjectName == "ToGammaWestCExterior") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusbiolabsGammaWestC", "FORCEPOS:4.0x13.0x0")
    
elseif(sObjectName == "ToGammaWestE") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusbiolabsGammaWestE", "FORCEPOS:5.0x7.0x0")
    
-- |[Objects]|
elseif(sObjectName == "SignA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (All shipping boxes are to be returned immediately.[P] Containment protocols dictate that no contaminated containers from the research labs are to be removed.[P] All waste is incinerated on site.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "SignB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Organic Tissue Research Laboratories.[P] No visitors - [P]deliveries -[P] leave by shipping doors to the south.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Entry") then
    local iRaibieDrank = VM_GetVar("Root/Variables/Chapter5/Scenes/iRaibieDrank", "N")
    if(iRaibieDrank >= 2.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Military-grade bulletproof glass, shattered from the outside.)") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "LockedDoor") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Door:[VOICE|Narrator] [SOUND|World|AutoDoorFail]Access denied.[P] Quarantine protocols in place.") ]])
    fnCutsceneBlocker()

-- |[Errors]|
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
