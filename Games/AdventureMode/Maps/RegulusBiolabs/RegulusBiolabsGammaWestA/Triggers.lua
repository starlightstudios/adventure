-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
if(sObjectName == "RaibiesScene") then
    
    --Variables.
    local iRaibieDrank = VM_GetVar("Root/Variables/Chapter5/Scenes/iRaibieDrank", "N")
    if(iRaibieDrank ~= 1.0) then return end
    
    --Set leader to Tiffany.
    WD_SetProperty("Set Leader Voice", "Tiffany")
        
    -- |[Achievement]|
    AM_SetPropertyJournal("Unlock Achievement", "BecomeRaibie")
    
    --Variables.
    local iSX399JoinsParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iRaibieDrank", "N", 2.0)
    
    --Movement.
    fnCutsceneCamera("Actor Name", 1000, "Christine")
    fnCutsceneMergeParty()
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (???)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Merge.
    fnCutsceneMergeParty()
    fnCutsceneBlocker()
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Focus Position", (40.25 * gciSizePerTile), (41.50 * gciSizePerTile))
        CameraEvent_SetProperty("Max Move Speed", 1.0)
    DL_PopActiveObject()
    
    --Movement.
    fnCutsceneMove("Christine", 42.25, 41.50)
    fnCutsceneFace("Christine", -1, 0)
    fnCutsceneMove("Tiffany", 41.25, 41.50)
    fnCutsceneFace("Tiffany", 1, 0)
    if(iSX399JoinsParty == 1.0) then
        fnCutsceneMove("SX-399", 41.25, 42.50)
        fnCutsceneFace("SX-399", 1, 0)
    end
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("55:[E|Neutral] Christine?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] ...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] Jeez, 55![B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] This is probably not the time for - [P][CLEAR]") ]])
    fnCutscene([[ Append("Christine:[E|Angry] Shut up![P] Why are you so mean to me all the time?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] I did not - [P][CLEAR]") ]])
    fnCutscene([[ Append("Christine:[E|Angry] And your voice - ohhh it makes me so mad![P] You're always harping on me and insulting me -[P] ARRRGGHH!!![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Sad] W-[P]wait, 55. I'm sorry![P] I would never![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Angry] But you need to stop insulting me![B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] I did not insult you.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Sad] J-[P]just...[P] need to cool down...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMoveFace("Christine", 45.25, 41.50, 1, 0, 0.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Christine", -1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("55:[E|Neutral] As expected, the PDU indicates a reaction...[B][C]") ]])
    if(iSX399JoinsParty == 1.0) then
        fnCutscene([[ Append("SX-399:[E|Neutral] Antibody production increasing threefold every eight seconds?[B][C]") ]])
    end
    fnCutscene([[ Append("55:[E|Neutral] Christine, are you feeling anything unusual?[P] Was this outburst related to your dosing?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMoveFace("Christine", 42.25, 41.50, -1, 0, 3.50)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("Combat|Impact_Strike_Crit") ]])
    fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
	fnCutscene([[ AL_SetProperty("Music", "Null") ]])
    
    --If SX-399 is present:
    if(iSX399JoinsParty == 1.0) then
        
        --Reposition.
        fnCutsceneTeleport("Christine", -100, -100)
        fnCutsceneTeleport("Tiffany", 33.25, 30.50)
        fnCutsceneSetFrame("Tiffany", "Downed")
        fnCutsceneTeleport("SX-399", 34.25, 30.50)
        fnCutsceneSetFrame("SX-399", "Wounded")
        
        --Glass layers.
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "BrokenGlass", false) ]])
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "NormalGlass", true) ]])
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "BrokenGlassHi", false) ]])
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "NormalGlassHi", true) ]])
    
        --Wait a bit.
        Cutscene_CreateEvent("CameraEvent", "Camera")
            CameraEvent_SetProperty("Focus Actor Name", "SX-399")
        DL_PopActiveObject()
        fnCutsceneBlocker()
        fnCutsceneWait(245)
        fnCutsceneBlocker()
        
        --Fade in.
        fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("SX-399:[VOICE|SX-399] (Gyroscope auto-stabilizer recalibrated.[P] Rebooting cognitive procedures.)") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneFace("SX-399", 0, 1)
        fnCutsceneSetFrame("SX-399", "Crouch")
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneSetFrame("SX-399", "Null")
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("SX-399:[VOICE|SX-399] (My cranial chassis got a few dents I'll need to buff out...[P] What happened?)") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneFace("SX-399", -1, 0)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("SX-399:[VOICE|SX-399] (55 is in system-standby...[P] But I know how to reset her...[P] Wait hold on, there's a lockout?)") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("55:[VOICE|Tiffany] Lockout bypassed.[P] Reactivating consciousness protocols...") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneSetFrame("Tiffany", "Crouch")
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneSetFrame("Tiffany", "Null")
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneFace("Tiffany", 1, 0)
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
        fnCutscene([[ Append("SX-399:[E|Smirk] Wakey-wakey, 55.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] My -[P] my chrono -[P] internal...[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Neutral] Let your boot finish before you talk.[P] Don't overdo it.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Down] ...[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] My internal chronometer reports thirteen minutes of unaccounted for time.[P] My CPU also reports a maintenance lockout.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Can you confirm the lost time?[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Neutral] Yep.[P] I just got up and removed the lockout.[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Neutral] Last thing I remember was Christine coming at you faster than a bullet.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] That is what my memory files indicate, as well.[P] I was unprepared and did not move in time.[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Neutral] And considering the military-grade glass which is shattered next to us, I'm thinking that stuff may have given Christine quite a kick.[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Neutral] But why didn't she say anything?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] I cannot say, other than that she clearly had a reason or a plan.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] She put me into maintenance lockout, presumably to have you remove it, and left us here.[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Neutral] Hold on, your PDU is flashing.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] ...[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] 'Am inside.[P] Not myself.[P] Stop me.[P] -C'[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Hmmm...[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Neutral] Maybe she couldn't concentrate enough to transform herself with the runestone?[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Smirk] But she still did her best.[P] There won't be anyone to harm in the Tissue Testing Laboratories.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] True, but considering her newfound strength, can we stop her?[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Neutral] You don't need to stop her, you goof.[P] You just need to neutralize her.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] We will need to be adaptable, now that our flimsy plans have fallen apart.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] I will enter the facility and attempt to stop Christine.[P] You remain here and see to it she doesn't get out.[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Smirk] You got it.[P] Try not to get taken down again.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] You did not fare any better than I.[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Neutral] Oh, I let her win.[P] I'm a good sport like that.[P] Now get to it!") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    
    --SX-399 is not present:
    else
        
        --Reposition.
        fnCutsceneTeleport("Christine", -100, -100)
        fnCutsceneTeleport("Tiffany", 33.25, 30.50)
        fnCutsceneSetFrame("Tiffany", "Downed")
        
        --Glass layers.
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "BrokenGlass", false) ]])
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "NormalGlass", true) ]])
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "BrokenGlassHi", false) ]])
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "NormalGlassHi", true) ]])
    
        --Wait a bit.
        Cutscene_CreateEvent("CameraEvent", "Camera")
            CameraEvent_SetProperty("Focus Actor Name", "Tiffany")
        DL_PopActiveObject()
        fnCutsceneBlocker()
        fnCutsceneWait(245)
        fnCutsceneBlocker()
        
        --Fade in.
        fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("55:[VOICE|Tiffany] Maintenance lockout reached timeout, rebooting...") ]])
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneSetFrame("Tiffany", "Crouch")
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneSetFrame("Tiffany", "Null")
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneFace("Tiffany", -1, 0)
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
        fnCutscene([[ Append("55:[E|Neutral] My -[P] recalibrating...[P] Finishing secondary table setup...[B][C]") ]])
        fnCutscene([[ Append("55:[E|Down] ...[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Internal chronometer reports thirteen minutes in maintenance lockout.[P] Christine?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] ...[P] She is not here, and the broken military-grade glass suggests her exit path.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Christine must have been unable to use her runestone, and disabled me...[P] Why?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] I'll need to recover her.[P] Luckily, she won't find any civilians inside the Tissue Testing Facility.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Down] Perhaps that was her plan.[P] Perhaps I have misjudged her.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] No matter.[P] With luck, I will be able to find some way of neutralizing Christine within the facility.[P] I will fix this.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    
    
    end
    
    --Remove party members so it's just 55.
    fnCutscene([[ AL_SetProperty("Unfollow Actor Name", "Tiffany") ]])
    fnCutscene([[ AL_SetProperty("Unfollow Actor Name", "SX-399") ]])
    EM_PushEntity("Tiffany")
        local i55ID = RO_GetID()
    DL_PopActiveObject()
    fnCutscene([[ AL_SetProperty("Player Actor ID", ]] .. i55ID .. [[) ]])
    AdvCombat_SetProperty("Clear Party")
    AdvCombat_SetProperty("Party Slot", 0, "Tiffany")
    
    --55 enters the building.
    fnCutsceneMove("Tiffany", 30.25, 30.50)
    fnCutsceneFace("Tiffany", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ AL_BeginTransitionTo("RegulusBiolabsGammaWestC", "FORCEPOS:12.0x20.0x0") ]])
    fnCutsceneBlocker()

-- |[Defeated by Raibie Christine]|
elseif(sObjectName == "RaibieDefeat") then

    --Black the screen out.
    fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
	AL_SetProperty("Music", "Null")
    
    --Variables.
    local iSX399JoinsParty       = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
    local iRaibieFoundNoisemaker = VM_GetVar("Root/Variables/Chapter5/Scenes/iRaibieFoundNoisemaker", "N")
    local iRaibieFoundKeycode    = VM_GetVar("Root/Variables/Chapter5/Scenes/iRaibieFoundKeycode", "N")
    local iRaibieFoundTranqGun   = VM_GetVar("Root/Variables/Chapter5/Scenes/iRaibieFoundTranqGun", "N")
    
    --Reposition.
    fnCutsceneTeleport("Tiffany", 30.25, 31.50)
    fnCutsceneSetFrame("Tiffany", "Wounded")
    fnCutsceneBlocker()
    
    --If SX-399 is present, spawn her.
    if(iSX399JoinsParty == 1.0) then
        fnSpecialCharacter("SX-399", -100, -100, gci_Face_South, false, nil)
        fnCutsceneTeleport("SX-399", 31.25, 31.50)
        fnCutsceneFace("SX-399", -1, 0)
    end

    --Fade in.
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    fnCutsceneSetFrame("Tiffany", "Crouch")
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    
    --Dialogue.
    if(iSX399JoinsParty == 1.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
        fnCutscene([[ Append("55:[E|Neutral] Damage... registered...[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Smirk] You're fine. Looks like Christine went easy on you.[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Neutral] Unfortunately the pavement didn't when you hit it. But a reinforced chassis can absorb that kind of it.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] What happened?[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Smirk] You came sailing through the window is what happened. I fixed you up.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] And Christine?[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Neutral] Still in there. She growled at me and ran off.[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Sad] I think she's trying her best to fight it...[B][C]") ]])
        fnCutscene([[ Append("55:[E|Down] I have failed her this time...[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Neutral] Find any useful clues in there? There's got to be a way to stop her.[B][C]") ]])
        if(iRaibieFoundNoisemaker == 0.0 and iRaibieFoundTranqGun == 0.0) then
            
            --Not found keycode.
            if(iRaibieFoundKeycode == 0.0) then
                fnCutscene([[ Append("55:[E|Neutral] I have been investigating but not found any useful clues yet.[B][C]") ]])
                fnCutscene([[ Append("SX-399:[E|Neutral] Did you check all the terminals and crates? Must be something useful.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] Not yet.[B][C]") ]])
                fnCutscene([[ Append("SX-399:[E|Flirt] Sounds like a plan. Good luck.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Smirk] I will not give up.") ]])
            
            --Found the keycode.
            else
                fnCutscene([[ Append("55:[E|Neutral] I have found a keycode that opens the airlock on the west side.[B][C]") ]])
                fnCutscene([[ Append("SX-399:[E|Neutral] Have you checked the storage shed out there?[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] Not yet, but doubtless something I will need is there.[B][C]") ]])
                fnCutscene([[ Append("SX-399:[E|Flirt] Sounds like a plan. Good luck.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Smirk] I will not give up.") ]])
            end
        
        --Found noisemaker but not the tranq gun:
        elseif(iRaibieFoundNoisemaker == 1.0 and iRaibieFoundTranqGun == 0.0) then
            fnCutscene([[ Append("55:[E|Neutral] I located a noisemaking device in a crate.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] You think Christine will respond to a loud bang?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Possibly. There is a shooting range in the basement with realistic dummies.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] However, if I lured her there, I do not have a way of subduing her.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Flirt] Yet.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Smirk] Correct. One must exist, and I will find it.") ]])
        
        --Found tranq gun but not noisemaker:
        elseif(iRaibieFoundNoisemaker == 0.0 and iRaibieFoundTranqGun == 1.0) then
            fnCutscene([[ Append("55:[E|Neutral] I located a tranquiliser gun in a storage shed. If it can break her skin, that will at least slow Christine down.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] But you didn't use it?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] She is faster than I am in her enraged form. I would need a way to distract her.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] There are some dummies downstairs used for target practice. But I would need to lure her to them first.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Flirt] So find a way to lure her to the dummies, then tranq her? Sounds like a plan![B][C]") ]])
            fnCutscene([[ Append("55:[E|Smirk] I will make another attempt. Resume your position here.") ]])
        
        --Found tranq and noisemaker:
        else
            fnCutscene([[ Append("55:[E|Neutral] I located a tranquiliser gun in a storage shed. If it can break her skin, that will at least slow Christine down.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] I have also located a noisemaking device, often used for Lord Golem parties.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] So... You're going to distract Christine and then shoot her?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Yes. However, I would need her to remain stationary.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] There are some dummies downstairs used for target practice. Perhaps if I detonate the noisemaker near them...[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Flirt] Sounds like a plan![B][C]") ]])
            fnCutscene([[ Append("55:[E|Smirk] I will make another attempt. Resume your position here.") ]])
        
        end
        fnCutsceneBlocker()
        
        --Stand and walk.
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneSetFrame("Tiffany", "Null")
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneFace("Tiffany", 0, -1)
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneMove("Tiffany", 30.25, 30.50)
        fnCutsceneFace("SX-399", -1, -1)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Transition.
        fnCutscene([[ AL_BeginTransitionTo("RegulusBiolabsGammaWestC", "FORCEPOS:12.0x20.0x0") ]])
        fnCutsceneBlocker()
        
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
        fnCutscene([[ Append("55:[E|Neutral] Damage...[P] registered...[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] My sensors indicate a cranial chassis impact...[P] Christine must have thrown me through the window...[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] But she has not followed. She must have enough control over herself for that.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] What can I do to improve my performance?[B][C]") ]])
        if(iRaibieFoundNoisemaker == 0.0 and iRaibieFoundTranqGun == 0.0) then
            
            --Not found keycode.
            if(iRaibieFoundKeycode == 0.0) then
                fnCutscene([[ Append("55:[E|Neutral] I have been investigating but not found any useful clues yet.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] I must continue to search the terminals and crates in the basement. There must be some useful clue...") ]])
            
            --Found the keycode.
            else
                fnCutscene([[ Append("55:[E|Neutral] I have found a keycode that opens the airlock on the west side.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] With luck, I can find something useful in the storage shed out there.") ]])
            end
        
        --Found noisemaker but not the tranq gun:
        elseif(iRaibieFoundNoisemaker == 1.0 and iRaibieFoundTranqGun == 0.0) then
            fnCutscene([[ Append("55:[E|Neutral] I located a noisemaking device in a crate.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] However, lacking a way of neutralizing Christine, I will only succeed in distracting her.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] If I do find a way to neutralize her, the target dummies will do nicely.") ]])
        
        --Found tranq gun but not noisemaker:
        elseif(iRaibieFoundNoisemaker == 0.0 and iRaibieFoundTranqGun == 1.0) then
            fnCutscene([[ Append("55:[E|Neutral] I located a tranquiliser gun in a storage shed. If it can break her skin, that will at least slow Christine down.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Unfortunately, she is considerably faster than I am in her enraged form. I would need a way to distract her.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] There are some dummies downstairs used for target practice. But I would need to lure her to them first.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Smirk] If I can find a device to lure her there...") ]])
        
        --Found tranq and noisemaker:
        else
            fnCutscene([[ Append("55:[E|Neutral] I located a tranquiliser gun in a storage shed. If it can break her skin, that will at least slow Christine down.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] I have also located a noisemaking device, often used for Lord Golem parties.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Perhaps the realistic dummies near the target range will distract her if I can lure her to them. I must at least try that.") ]])
        
        end
        fnCutsceneBlocker()
        
        --Stand and walk.
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneSetFrame("Tiffany", "Null")
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneFace("Tiffany", 0, -1)
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneMove("Tiffany", 30.25, 30.50)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Transition.
        fnCutscene([[ AL_BeginTransitionTo("RegulusBiolabsGammaWestC", "FORCEPOS:12.0x20.0x0") ]])
        fnCutsceneBlocker()
        
    end

-- |[No Music]|
--Disables the music in the outdoor area.
elseif(sObjectName == "NoMusic") then
	AL_SetProperty("Music", "Null")

end
