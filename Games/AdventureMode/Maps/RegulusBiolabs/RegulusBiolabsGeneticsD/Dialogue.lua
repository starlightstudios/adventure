-- |[Root]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Special: No need to go further if just backing out of dialogue.

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = "None"
if(sTopicName ~= "YesChristine" and sTopicName ~= "Yes55" and sTopicName ~= "NoCancel") then
    sActorName = TA_GetProperty("Name")
end

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    --Variables.
    local bIsSX399Present = fnIsCharacterPresent("SX-399")
    local iAquaticsMetLord = VM_GetVar("Root/Variables/Chapter5/Scenes/iAquaticsMetLord", "N")
    local iAquaticsBlankKeycard = VM_GetVar("Root/Variables/Chapter5/Scenes/iAquaticsBlankKeycard", "N")
    
    -- |[Wait What?]|
    if(iAquaticsMetLord == 0.0) then
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iAquaticsMetLord", "N", 1.0)
        
        --Dialogue.
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
        fnCutscene([[ Append("Lord:[E|Neutral] Oh no, the rebels are upon me![P] I surrender![B][C]") ]])
        fnCutscene([[ Append("Lord:[E|Neutral] I am unarmed and in no position to resist you.[P] There is no need to get violent.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Good.[P] Don't make any sudden moves.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] We accept your surrender.[B][C]") ]])
        fnCutscene([[ Append("Lord:[E|Neutral] Well, I suppose you can do whatever you want with me, then.[P] No need to make it quick, either.[B][C]") ]])
        if(bIsSX399Present == true) then
            fnCutscene([[ Append("SX-399:[E|Neutral] We don't scrap prisoners.[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] That is not what I meant.[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] You could do [P]*absolutely anything*[P] to me, and I would be unable to stop you.[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] All three of you at once, if necessary, or two at once while the other watches.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Sad] And we don't torture either.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Correct.[P] In addition to it being ineffective for any purpose but yielding forced confessions, it is morally abhorrent.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] So you have nothing to fear.[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] Not what -[P] Hmm.[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] Listen, I'd do absolutely anything you demanded of me.[P] *Anything*.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] Like quietly be escorted to a secured room?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Blush] (These two just do not get it do they?)[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Perhaps you would answer some questions voluntarily?[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] Questions?[P] While bent over and begging for release?[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] Oh no, we're not letting you go.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Blush] You two are so naive...[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] Huh?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] I do not follow your logic.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Look, miss.[P] Whatever you think is going to go on here, you're wrong.[P] We're all spoken for.[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] All of you?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] All of us.[P] Though perhaps some of the other rebels aren't, I wouldn't know.[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] There are more of you?[P] How many?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Enough.[P] We will not divulge sensitive information.[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] Perhaps a dozen or more, all seeking to take revenge on me for years of mistreatment?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Blush] And maybe they'd record and broadcast it...[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] As a warning to others...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Scared] Getting off track![B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Ma'am, we need to access the eastern section of the facility for our nefarious rebel purposes.[P] If you cooperate...[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] That area is under code-blue security lockdown.[P] I cannot bypass it myself.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] She's lying.[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] I said [P]*myself*[P], my dear.[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] I would require a keycard which I can imprint with my authcode.[P] That will allow you to bypass the lockdown.[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] I do not, however, have a blank keycard.[P] The lockdown was implemented without distributing keycards to the staff as per procedure.[B][C]") ]])
            
            --Don't have the blank card yet.
            if(iAquaticsBlankKeycard == 0.0) then
                fnCutscene([[ Append("Christine:[E|Smirk] So we need to find a blank keycard?[P] Shouldn't be a problem.[B][C]") ]])
                fnCutscene([[ Append("Lord:[E|Neutral] I understand they are kept in storage, but with everything being so chaotic as of late, I can't say where.[B][C]") ]])
                fnCutscene([[ Append("Lord:[E|Neutral] You might find something in the restricted labs north of here.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Stay here.[P] This place is dangerous.[B][C]") ]])
                fnCutscene([[ Append("Lord:[E|Neutral] Of course.[P] Oh how awful it'd be if someone were to happen upon poor, vulnerable me while trapped in this office...") ]])
            
            --Already have the keycard.
            else
                VM_SetVar("Root/Variables/Chapter5/Scenes/iAquaticsKeycard", "N", 1.0)
                fnCutscene([[ Append("Christine:[E|Neutral] A keycard like this one?[B][C]") ]])
                fnCutscene([[ Append("Lord:[E|Neutral] Precisely.[P] One moment...[B][C]") ]])
                fnCutscene([[ Append("Lord:[E|Neutral] [SOUND|World|Keycard]And there you have it.[P] One code-blue keycard.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] I didn't think this would be so easy...[B][C]") ]])
                fnCutscene([[ Append("Lord:[E|Neutral] I have kept my end of the...[P] shall we say, agreement.[P] Therefore, it is time for you to keep yours.[B][C]") ]])
                fnCutscene([[ Append("SX-399:[E|Neutral] By taking you prisoner?[B][C]") ]])
                fnCutscene([[ Append("Lord:[E|Neutral] Oh yes, and forcing me to do whatever it is you desire.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] We'll contact a rebel team to pick you up.[P] This area is still dangerous.[P] Stay in your office.[B][C]") ]])
                fnCutscene([[ Append("Lord:[E|Neutral] That's what I've been doing so far, and I am still functional.[B][C]") ]])
                fnCutscene([[ Append("Lord:[E|Neutral] Will your fellow rebels be far less charitable than you?[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] Likely.[B][C]") ]])
                fnCutscene([[ Append("Lord:[E|Neutral] Oh my...[P] the things I will be made to do...[P] so degrading for one of my stature...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Blush] They might make you lick their boots, or clean the floors where they walk...[B][C]") ]])
                fnCutscene([[ Append("Lord:[E|Neutral] Or lash me to a post, nude, and allow passersby to gawk at my exposed metallic skin...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Blush] (We had better get going before I ask to trade places with her!)") ]])
            end
        
        --SX-399 is not present.
        else
            fnCutscene([[ Append("55:[E|Neutral] Prisoners are not to be harmed so long as they cooperate with instructions.[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] That is not what I meant.[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] You could do [P]*absolutely anything*[P] to me, and I would be unable to stop you.[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] Both of you at once, if necessary, though if one merely wanted to watch...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Torture is inefficient and morally abominable.[P] We will not tolerate it.[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] Not what -[P] Hmm.[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] Listen, I'd do absolutely anything you demanded of me.[P] *Anything*.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] You will be taken to a secure room and interrogated.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Blush] (Oh, 55...)[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] If you answer questions voluntarily, we can provide certain amenities.[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] Questions?[P] While bent over and begging for release?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] You will not be released for the duration of the conflict.[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] Oh my goodness, yes...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Blush] You are so naive, 55...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] ..?[P] I do not follow your logic.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Look, miss.[P] Whatever you think is going to go on here, you're wrong.[P] I'm spoken for, and my friend...[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] Yes?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] There are other rebels who may be more...[P] accommodating.[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] There are more of you?[P] How many?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Enough.[P] We will not divulge sensitive information.[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] Perhaps a dozen or more, all seeking to take revenge on me for years of mistreatment?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Blush] And maybe they'd record and broadcast it...[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] As a warning to others...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Scared] Getting off track![B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Ma'am, we need to access the eastern section of the facility for our nefarious rebel purposes.[P] If you cooperate...[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] That area is under code-blue security lockdown.[P] I cannot bypass it myself.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Upset] You are an administrator.[P] You have access.[P] Do not lie to us.[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] I said [P]*myself*[P], my dear.[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] I would require a keycard which I can imprint with my authcode.[P] That will allow you to bypass the lockdown.[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] I do not, however, have a blank keycard.[P] The lockdown was implemented without distributing keycards to the staff as per procedure.[B][C]") ]])
            
            --Don't have the blank card yet.
            if(iAquaticsBlankKeycard == 0.0) then
                fnCutscene([[ Append("Christine:[E|Smirk] So we need to find a blank keycard?[P] Shouldn't be a problem.[B][C]") ]])
                fnCutscene([[ Append("Lord:[E|Neutral] I understand they are kept in storage, but with everything being so chaotic as of late, I can't say where.[B][C]") ]])
                fnCutscene([[ Append("Lord:[E|Neutral] You might find something in the restricted labs north of here.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Stay here.[P] This place is dangerous.[B][C]") ]])
                fnCutscene([[ Append("Lord:[E|Neutral] Of course.[P] Oh how awful it'd be if someone were to happen upon poor, vulnerable me while trapped in this office...") ]])
            
            --Already have the keycard.
            else
                VM_SetVar("Root/Variables/Chapter5/Scenes/iAquaticsKeycard", "N", 1.0)
                fnCutscene([[ Append("Christine:[E|Neutral] A keycard like this one?[B][C]") ]])
                fnCutscene([[ Append("Lord:[E|Neutral] Precisely.[P] One moment...[B][C]") ]])
                fnCutscene([[ Append("Lord:[E|Neutral] [SOUND|World|Keycard]And there you have it.[P] One code-blue keycard.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] I didn't think this would be so easy...[B][C]") ]])
                fnCutscene([[ Append("Lord:[E|Neutral] I have kept my end of the...[P] shall we say, agreement.[P] Therefore, it is time for you to keep yours.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] You wish to become a prisoner voluntarily?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Blush] (Maybe 55 and I need to have 'the talk'...)[B][C]") ]])
                fnCutscene([[ Append("Lord:[E|Neutral] Oh yes, and forcing me to do whatever it is you desire.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] We'll contact a rebel team to pick you up.[P] This area is still dangerous.[P] Stay in your office.[B][C]") ]])
                fnCutscene([[ Append("Lord:[E|Neutral] That's what I've been doing so far, and I am still functional.[B][C]") ]])
                fnCutscene([[ Append("Lord:[E|Neutral] Will your fellow rebels be far less charitable than you?[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] Likely.[B][C]") ]])
                fnCutscene([[ Append("Lord:[E|Neutral] Oh my...[P] the things I will be made to do...[P] so degrading for one of my stature...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Blush] They might make you lick their boots, or clean the floors where they walk...[B][C]") ]])
                fnCutscene([[ Append("Lord:[E|Neutral] Or lash me to a post, nude, and allow passersby to gawk at my exposed metallic skin...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Blush] (We had better get going before I ask to trade places with her!)") ]])
            end
            
            
            
        end
        fnCutsceneBlocker()
    
    --Repeat, no keycard.
    elseif(iAquaticsMetLord == 1.0 and iAquaticsBlankKeycard == 0.0) then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
        fnCutscene([[ Append("Lord:[E|Neutral] Do let me known when you have located a blanked keycard.[P] I will get it authorized for you post-haste.[B][C]") ]])
        fnCutscene([[ Append("Lord:[E|Neutral] You may be able to locate one on the northern end of the facility, in a storage container.") ]])
    
    --Repeat, got keycard.
    else
    
        --Don't have the blank card yet.
        local iAquaticsKeycard = VM_GetVar("Root/Variables/Chapter5/Scenes/iAquaticsKeycard", "N")
        if(iAquaticsKeycard == 0.0) then
            VM_SetVar("Root/Variables/Chapter5/Scenes/iAquaticsKeycard", "N", 1.0)
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] We've found a keycard.[P] Can you authorize it?[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] One moment...[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] [SOUND|World|Keycard]And there you have it.[P] One code-blue keycard.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] I didn't think this would be so easy...[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] I have kept my end of the...[P] shall we say, agreement.[P] Therefore, it is time for you to keep yours.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] You wish to become a prisoner voluntarily?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Blush] (Maybe 55 and I need to have 'the talk'...)[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] Oh yes, and forcing me to do whatever it is you desire.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] We'll contact a rebel team to pick you up.[P] This area is still dangerous.[P] Stay in your office.[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] That's what I've been doing so far, and I am still functional.[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] Will your fellow rebels be far less charitable than you?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Likely.[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] Oh my...[P] the things I will be made to do...[P] so degrading for one of my stature...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Blush] They might make you lick their boots, or clean the floors where they walk...[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] Or lash me to a post, nude, and allow passersby to gawk at my exposed metallic skin...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Blush] (We had better get going before I ask to trade places with her!)") ]])
            
        --Repeats.
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] I hope your rebel friends get here soon.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] They will provide protection in exchange for cooperation.[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] Oh, I intend to cooperate.[P] As hard as I can.") ]])
        
        
        end
    end
end
