-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
if(sObjectName == "Action") then
    
    --Movie!
    fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Camera handler.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 5.0) --Default is 5.0
        CameraEvent_SetProperty("Focus Position", (15.25 * gciSizePerTile), (13.50 * gciSizePerTile))
    DL_PopActiveObject()
    
    --Spawn player character.
    fnCutsceneTeleport("Christine", -100.25, -100.50)
    fnCutsceneTeleport("Sophie", -100.25, -100.50)
    fnSpecialCharacter("Sammy", 14, 12, gci_Face_South, false, nil)
    
    --Extras.
    TA_Create("AwardDoll")
        TA_SetProperty("Position", 15, 12)
        TA_SetProperty("Facing", gci_Face_South)
        TA_SetProperty("Clipping Flag", false)
        fnSetCharacterGraphics("Root/Images/Sprites/Doll/", false)
    DL_PopActiveObject()
    
    --Boss.
    TA_Create("BigBoss")
        TA_SetProperty("Position", 51, 13)
        TA_SetProperty("Facing", gci_Face_North)
        TA_SetProperty("Clipping Flag", false)
        fnSetCharacterGraphics("Root/Images/Sprites/GolemLordA/", false)
    DL_PopActiveObject()
    
    --Gossipers.
    TA_Create("GossipA")
        TA_SetProperty("Position", 50, 17)
        TA_SetProperty("Facing", gci_Face_East)
        TA_SetProperty("Clipping Flag", false)
        fnSetCharacterGraphics("Root/Images/Sprites/GolemSlave/", false)
    DL_PopActiveObject()
    TA_Create("GossipB")
        TA_SetProperty("Position", 51, 17)
        TA_SetProperty("Facing", gci_Face_West)
        TA_SetProperty("Clipping Flag", false)
        fnSetCharacterGraphics("Root/Images/Sprites/GolemSlave/", false)
    DL_PopActiveObject()
    
    --Coconut.
    TA_Create("Coconut")
        TA_SetProperty("Position", 88, 17)
        TA_SetProperty("Facing", gci_Face_South)
        TA_SetProperty("Clipping Flag", false)
        fnSetCharacterGraphics("Root/Images/Sprites/GenericF0/", false)
    DL_PopActiveObject()
    
    --Dialogue.
    fnCutscene([[ AL_SetProperty("Music", "Moth Catchup") ]])
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Narrator:[VOICE|Alraune] Last time, on Some Moths Do...[B][C]") ]])
    fnCutscene([[ Append("Narrator:[VOICE|Alraune] Knife-Edge City is wracked by the terrible machinations of Evil Corp.[P] The only thing standing between them and world domination is The Agency.[B][C]") ]])
    fnCutscene([[ Append("Narrator:[VOICE|Alraune] Famous for its Action and Brilliance, The Agency has stopped Evil Corp time and time again.[P] But their best agent, Agent Almond, has found herself in a situation that might be tougher than she is...[P] and she is plenty tough, missy!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Scene.
    fnCutsceneWait(25)
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Mayor:[VOICE|Doll] ...[P] And that is why, I, the mayor of Knife-Edge City, would like to present daring Agent Almond with our highest award![B][C]") ]])
    fnCutscene([[ Append("Mayor:[VOICE|Doll] Agent Almond, for your courage under fire, seductive prowess, and love of explosions, we the people of Knife-Edge City,[P] in association with Jack-Knife Town and Murderkill Unincorporated Municipal Area,[P] present you with the Order of the Blue Moth.[B][C]") ]])
    fnCutscene([[ Append("Mayor:[VOICE|Doll] Which we kind of made up to celebrate you, the greatest Agent![B][C]") ]])
    fnCutscene([[ Append("Crowd:[VOICE|Narrator] Three cheers for Agent Almond!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Fade out.
    fnCutsceneWait(25)
    fnCutscene([[ AL_SetProperty("Activate Fade", 25, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Teleport.
    fnCutsceneWait(65)
    fnCutsceneTeleport("Sammy", 55.25, 13.50)
    fnCutsceneFace("Sammy", -1, 0)
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 5.0) --Default is 5.0
        CameraEvent_SetProperty("Focus Position", (53.25 * gciSizePerTile), (16.50 * gciSizePerTile))
    DL_PopActiveObject()
    fnCutsceneBlocker()
    
    fnCutsceneWait(65)
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] But Big Boss, you know as well as I do that I'm close to cracking this case wide open![B][C]") ]])
    fnCutscene([[ Append("Lord:[E|Neutral] Is that so, Almond?[P] The press loves you, but internal affairs has uncovered evidence that you're working for Evil Corp.[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Sexy] Impossible![P] I would never side with them![B][C]") ]])
    fnCutscene([[ Append("Lord:[E|Neutral] It is undeniable.[P] In fact, I have orders from Bigger Boss to arrest you.[B][C]") ]])
    fnCutscene([[ Append("Lord:[E|Neutral] Almond, you and I go way back.[P] Back to the war in Pandatown.[B][C]") ]])
    fnCutscene([[ Append("Lord:[E|Neutral] So that's why I'm giving you a head start.[P] Get out of my sight.[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] ...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Fade out.
    fnCutsceneWait(25)
    fnCutscene([[ AL_SetProperty("Activate Fade", 25, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Teleport.
    fnCutsceneWait(65)
    fnCutsceneTeleport("Sammy", 94.25, 19.50)
    fnCutsceneTeleport("GossipA", 91.25, 23.50)
    fnCutsceneFace("GossipA", 1, 0)
    fnCutsceneTeleport("BigBoss", 92.25, 23.50)
    fnCutsceneFace("BigBoss", -1, 0)
    fnCutsceneFace("Sammy", -1, 0)
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 5.0) --Default is 5.0
        CameraEvent_SetProperty("Focus Position", (90.25 * gciSizePerTile), (20.50 * gciSizePerTile))
    DL_PopActiveObject()
    fnCutsceneBlocker()
    
    --Fade back in.
    fnCutsceneWait(45)
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Coconut:[VOICE|Coconut] Over here, Agent Almond.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Sammy", 88.25, 19.50)
    fnCutsceneMove("Sammy", 88.25, 18.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Coconut", "Neutral") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] Agent Coconut!?[P] Is that you?[B][C]") ]])
    fnCutscene([[ Append("Coconut:[E|Neutral] That's me![P] I'm here to help you, Agent Almond![B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Sexy] That's...[P] former Agent Almond...[P] I was disavowed.[B][C]") ]])
    fnCutscene([[ Append("Coconut:[E|Neutral] I know, but I also know you would never work for Evil Corp.[B][C]") ]])
    fnCutscene([[ Append("Coconut:[E|Neutral] I saw the video evidence they had, but it looked fishy to me.[P] Something has to be up.[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] Oh yeah, and just what are a former agent and a rookie going to do?[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] This isn't a training exercise, this is real life.[P] You might get shot, Coconut.[B][C]") ]])
    fnCutscene([[ Append("Coconut:[E|Neutral] I signed up because I wanted to be just like you, Almond.[P] And Agent Almond doesn't back down from a challenge![B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] Well...[P] I guess there's no helping it now.[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] So what's our next move?[P] What have you found out?[B][C]") ]])
    fnCutscene([[ Append("Coconut:[E|Neutral] Why, just the secret location of Evil Corp's secret base![P] I'm sure we can find the evidence to clear your name there![B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] As well as hundreds of heavily armed Evil Guards.[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Gun] ...[P] Let's go get some *Action*!!!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    fnCutscene([[ AL_SetProperty("Activate Fade", 5, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Alraune] And now, the adventure resumes...") ]])
    fnCutsceneBlocker()
    
    fnCutscene([[ AL_BeginTransitionTo("RegulusBiolabsMovieA", "FORCEPOS:7.0x29.0x0") ]])
    fnCutsceneBlocker()
    
end
