-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
-- |[Objects]|
if(sObjectName == "ToDatacoreF") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsDatacoreF", "FORCEPOS:18.0x4.0x0")
    
elseif(sObjectName == "ToDatacoreH") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsDatacoreH", "FORCEPOS:11.0x18.0x0")
    
elseif(sObjectName == "TerminalA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Experimental data from around the Biolabs.[P] Everything from the Epsilon habitat is classified, and stored in local drives there.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Meeting notes, production targets...[P] Nothing on here mentioned repair units. Honestly, do they even budget for the important things?)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Just got back from a date with my tandem unit.[P] We watched a videograph on the big screen in the Gamma Labs!')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('On the way back to my quarters, I got frisked and EM-checked by the security units near the Epsilon labs.[P] I wasn't even going in there, why am I getting searched?[P] Honestly, those security units are out of their minds.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Laptop") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The computer is locked, and probably doesn't contain anything worth hacking it for.)") ]])
    fnCutsceneBlocker()
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
