-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
if(sObjectName == "ToGeneticsA") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsGeneticsA", "FORCEPOS:26.5x15.0x0")
    
elseif(sObjectName == "ToGeneticsC") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsGeneticsC", "FORCEPOS:15.5x26.0x0")
    
elseif(sObjectName == "ToGeneticsD") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsGeneticsD", "FORCEPOS:5.5x4.0x0")
    
elseif(sObjectName == "ToGeneticsE") then
    
    --Variables.
    local iAquaticsKeycard = VM_GetVar("Root/Variables/Chapter5/Scenes/iAquaticsKeycard", "N")
    
    --No keycard.
    if(iAquaticsKeycard == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("[SOUND|World|AutoDoorFail] Access denied.[P] Blue authorization required.[P] Contact your administrator for access permissions.") ]])
        fnCutsceneBlocker()
    
    --First time opening the door.
    elseif(iAquaticsKeycard == 1.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iAquaticsKeycard", "N", 2.0)
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Access granted.") ]])
        fnCutsceneBlocker()
        fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
        fnCutscene([[ AL_BeginTransitionTo("RegulusBiolabsGeneticsE", "FORCEPOS:3.0x12.0x0") ]])
        fnCutsceneBlocker()
    
    --Repeats.
    else
        AudioManager_PlaySound("World|AutoDoorOpen")
        AL_BeginTransitionTo("RegulusBiolabsGeneticsE", "FORCEPOS:3.0x12.0x0")
    end
    
-- |[Objects]|
elseif(sObjectName == "TerminalA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Analysis of Aquatic Mammal Mating Habits'.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (...[P] 'Dolphins are one of a small group of mammals known to have sex for recreational purposes.[P] This is likely correlated with emotional intelligence and social structures, which have similarities to humans.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Since intelligence is correlated to sex drive, this explains why my tandem unit hasn't made a move on me in months.[P] I'm thirsty in the damn desert here, people.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (...[P] Geez...)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Yesterday, I ordered the production of several latex toys for use in my experiments.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('To my total dearth of surprise, I found some of them had not been delivered to my laboratory.[P] So, I did a quick search of the workers' quarters.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('I caught several units in the act.[P] I was unsure as to what punishment they deserved for misappropriation of research materials.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('I can, fortunately, report that no further discipline will be needed.[P] After what I did to their rear receiver ports, I doubt they will be doing that again.[P] At least not until a repair unit has looked at them first.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('In other news, the fabrication staff indicated they are eager to work on the next shipment of toys for my experiments.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Experimentation logs with the new 'Substance E', which they received from the Epsilon Labs.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Substance E, at first glance, is a non-reactive neutral fluid with a violet color, no odor, and a low boiling point.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('When applied to organic material, however, the fluid begins to self-assemble.[P] What is odd is that no material is pulled from the organic subject.[P] It seems to derive from the air itself.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('After resolving several technical issues, my next round of experiments will be attempting to determine the location from where extra matter is derived.[P] I expect a test in a suspended vacuum chamber will yield insight.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Experimentation logs with the new 'Substance E', which they received from the Epsilon Labs.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('The process, which slave units derisively call 'dunking', is simple.[P] The subject's body is immersed in Substance E dissolved in water.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Most interesting is the results.[P] There is no particular pattern by species, mass, or personality.[P] Some subjects are wholly unaffected, while others show an immediate increase in aggression.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('I would like to begin a genetically-controlled test next, using cloned aquatic species raised in isolation.[P] That should help reduce confounding variables.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalE") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Evacuation order is in effect?[P] Is this a drill?[P] In any case, I'll meet you at Transit Station Omicron.[P] See you soon!')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalF") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('I was defragmenting when my Lord Golem came running in screaming about results or somesuch.[P] She dragged half the domicile block out of the chambers and lined us all up.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('She pointed at each of us and marked us as 'not' and 'are'.[P] Then she went back down to the main floor and jumped into one of the aquariums.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('They've got her in the repair bay in Sector 112 trying to figure out what's wrong with her.[P] I don't usually say this, but I feel bad for my Lord Golem.[P] Maybe she got some of that Substance E stuff on her?')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalG") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('The seals are just too darn playful to use as experimentation subjects.[P] I'll put a motion in to see if we can't get them dedicated to entertainment purposes.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('After every show I get at least one Lord Unit asking about them.[P] Having to hack them up and pull their organs out when we're done with them...[P] I always lie about their fate...')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalH") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('If you want a good example of biological services being good at their jobs, have I got one for you.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('I noticed that some of the grass near the inlet was turning yellow, so I asked them about it.[P] Two minutes later, they say someone was dumping waste in.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('A quick check of the security cameras and the offending unit was identified and punished.[P] The grass began growing normally afterwards.[P] How do they do it?')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('I swear they can talk to plants.[P] Amazing!')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "NoteA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (There's an aquatics show that takes place in the next room.[P] There's a list of show times, but they've obviously been cancelled.)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "NoteB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Authorized scientific personnel only beyond this point.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "SignA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Please clean up any liquid spills, or contact a Slave Unit to do so.[P] We've had far too many slips this month!')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "SignB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Samples from the Epsilon Habitat must be cryogenically sealed before transport.[P] NO EXCEPTIONS.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Fabricator") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A fabrication bench with the tools still left out.[P] The staff here left in a hurry, as expected.)") ]])
    fnCutsceneBlocker()
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
