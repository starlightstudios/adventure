-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
if(sObjectName == "2856Call") then
    
    --Variables.
    local i2856YelledAboutWestward = VM_GetVar("Root/Variables/Chapter5/Scenes/i2856YelledAboutWestward", "N")
    local iSawGammaAirlockSequence = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawGammaAirlockSequence", "N")
    
    --Dialogue.
    if(i2856YelledAboutWestward == 0.0 and iSawGammaAirlockSequence == 0.0) then
    
        --Flag
        VM_SetVar("Root/Variables/Chapter5/Scenes/i2856YelledAboutWestward", "N", 1.0)

        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        fnCutscene([[ Append("Christine:[E|PDU] Wow, we went five whole minutes without 2856 calling us.[P] Whoopee.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|PDU] *Click*[P] What do you want?[B][C]") ]])
        fnCutscene([[ Append("2856:[VOICE|Tiffany] Hello?[P] Christine?[P] Are you all right?[P] Is there anything I can get you?[B][C]") ]])
        fnCutscene([[ Append("2856:[VOICE|Tiffany] Oh dear, I hope I'm not interrupting anything important.[P] I just saw you enter range of a network node and thought I'd check up on my dear sister.[B][C]") ]])
        fnCutscene([[ Append("2856:[VOICE|Tiffany] You know she means the world to me.[P] I hope your quest is going all right.[P] Are you having fun?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] What are you getting at?[B][C]") ]])
        fnCutscene([[ Append("2856:[VOICE|Tiffany] Well, just something I noticed...[P] You see, I told you to go north.[B][C]") ]])
        fnCutscene([[ Append("2856:[VOICE|Tiffany] And you're going west.[B][C]") ]])
        fnCutscene([[ Append("2856:[VOICE|Tiffany] BECAUSE YOU ARE SO STUPID YOU CAN'T TELL THE TWO APART, SO I'LL HELP YOU.[P] STOP GOING THAT WAY, GO BACK, GO NORTH.[B][C]") ]])
        fnCutscene([[ Append("2856:[VOICE|Tiffany] I tried being nice because it doesn't help to yell at stupid units, you see.[B][C]") ]])
        fnCutscene([[ Append("2856:[VOICE|Tiffany] BUT IT FEELS SO MUCH BETTER.[P] NOW GET BACK THERE AND GO NORTH, RIGHT NOW.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] Shut it.[P] We go where we want, when we want.[B][C]") ]])
        fnCutscene([[ Append("2856:[VOICE|Tiffany] YOU STUPID WORTHLESS PIECE OF - [P][CLEAR]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] I know exactly where I'm going, and we're going this way.[P] Christine, out.[B][C]") ]])
        fnCutscene([[ Append("2856:[VOICE|Tiffany] GET BACK THERE AND - [P]*click*[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Happy] Take me, right now.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Blush] Just a kiss for now, dearest.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Blush] *smooch*[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Blush] Y-[P]yeah.[P] Dangerous.[P] Have a job to do.[P] *But it's so hot when you stand up to her like that...*") ]])
    
    end
end
