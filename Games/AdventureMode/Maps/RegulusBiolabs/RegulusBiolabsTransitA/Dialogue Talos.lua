-- |[ ================================ Dialogue Script - Talos ================================= ]|
--Entry point for dialogue with Talos. Note: There are no power/initiative catalysts in this chapter.
-- In addition, all the health/skill catalysts are in the first half and can't be bought now.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[ =================================== fnSetTalosTopics() =================================== ]|
local function fnSetTalosTopics()
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
    
    --Variables.
    --local iTalosShowHealth     = VM_GetVar("Root/Variables/Chapter5/Talos/iTalosShow_Health", "N")
    --local iTalosShowPower      = VM_GetVar("Root/Variables/Chapter5/Talos/iTalosShow_Power", "N")
    --local iTalosShowInitiative = VM_GetVar("Root/Variables/Chapter5/Talos/iTalosShow_Initiative", "N")
    local iTalosShowAccuracy   = VM_GetVar("Root/Variables/Chapter5/Talos/iTalosShow_Accuracy", "N")
    local iTalosShowEvade      = VM_GetVar("Root/Variables/Chapter5/Talos/iTalosShow_Evasion", "N")
    --local iTalosShowSkills     = VM_GetVar("Root/Variables/Chapter5/Talos/iTalosShow_Skills", "N")
    
    --Always available.
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Fashion\",  " .. sDecisionScript .. ", \"Fashion\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Hackers\",  " .. sDecisionScript .. ", \"Hackers\") ")
    
    --Health Catalysts.
    --if(iTalosShowHealth == 0.0) then
    --    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Health Catalysts\",  " .. sDecisionScript .. ", \"QueryHealth\") ")
    --end
    --if(iTalosShowPower == 0.0) then
    --    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Power Catalysts\",  " .. sDecisionScript .. ", \"QueryPower\") ")
    --end
    --if(iTalosShowInitiative == 0.0) then
    --    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Initiative Catalysts\",  " .. sDecisionScript .. ", \"QueryInitiative\") ")
    --end
    if(iTalosShowAccuracy == 0.0) then
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Accuracy Catalysts\",  " .. sDecisionScript .. ", \"QueryAccuracy\") ")
    end
    if(iTalosShowEvade == 0.0) then
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Evasion Catalysts\",  " .. sDecisionScript .. ", \"QueryEvasion\") ")
    end
    --if(iTalosShowSkills == 0.0) then
    --    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Skills Catalysts\",  " .. sDecisionScript .. ", \"QuerySkills\") ")
    --end
    
    --Goodbye. Always available.
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Goodbye\",  " .. sDecisionScript .. ", \"Goodbye\") ")
    fnCutsceneBlocker()
end

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[Common Setup]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Talos", "Neutral") ]])
    
    -- |[Variables]|
    local iMetTalos        = VM_GetVar("Root/Variables/Chapter5/Talos/iMetTalos", "N")
    local iMetTalosBiolabs = VM_GetVar("Root/Variables/Chapter5/Talos/iMetTalosBiolabs", "N")
    local bIsSX399Present  = fnIsCharacterPresent("SX-399")
    
    -- |[First Meeting]|
    if(iMetTalos == 0.0) then
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Talos/iMetTalos", "N", 1.0)
        VM_SetVar("Root/Variables/Chapter5/Talos/iMetTalosBiolabs", "N", 1.0)
        
        --Variables.
        local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
    
        --Dialogue.
        fnCutscene([[ Append("Lord:[E|Neutral] Chief![P] What a surprise.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Upset] You aren't supposed to be here.[B][C]") ]])
        fnCutscene([[ Append("Lord:[E|Neutral] If you had given me an extra ten minutes I would have been on my way.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] I'm afraid I've not had the pleasure.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Meet the hacker operative known on the networks as 'Talos'.[P] It is best if I do not give out her real name or unit number, yet.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Upset] She was supposed to have left the biolabs hours ago and returned to sector 301.[B][C]") ]])
        fnCutscene([[ Append("Talos:[E|Unsure] Sorry chief, things don't always work out the way you want them to.[P] The explosions suggest that things have already started and yet all these lords being here...[B][C]") ]])
        if(bIsSX399Present == true) then
            fnCutscene([[ Append("SX-399:[E|Smirk] Guess we're not in a position to be lecturing about things not going to plan, eh?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Our issues are different.[B][C]") ]])
        else
            fnCutscene([[ Append("Christine:[E|Smirk] Sorry about that, but we're working on it.[P] Let's make the best of it.[B][C]") ]])
        end
        fnCutscene([[ Append("Talos:[E|Neutral] I assume the boss hasn't told you what I'm known for?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] She runs a hacker collective which has control of numerous cameras across the city, ones thought to be non-functional.[P] She knows a lot of useful information.[B][C]") ]])
        fnCutscene([[ Append("Talos:[E|Wise] I know all.[P] I see all.[B][C]") ]])
        fnCutscene([[ Append("Talos:[E|Neutral] Specifically I know which catalysts you've picked up and where they are.[P] Not that it's hard to know in the biolabs but...[B][C]") ]])
        fnCutscene([[ Append("Talos:[E|Wise] I can still help![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Wonderful![B][C]") ]])
        fnCutscene([[ Append("Talos:[E|Neutral] I think if I can score some platina I can get a smuggler contact of mine to get me out of here.[P] So sorry, but I may need to ask for a 'donation', chief.[B][C]") ]])
        fnCutscene([[ Append("Talos:[E|Neutral] Now, what can I help you with?[B]") ]])
        fnSetTalosTopics()

    -- |[Met Talos, but not in the Biolabs]|
    elseif(iMetTalosBiolabs == 0.0) then
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Talos/iMetTalosBiolabs", "N", 1.0)
    
        --Dialogue.
        fnCutscene([[ Append("Lord:[E|Neutral] Chief![P] What a surprise.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Upset] You aren't supposed to be here.[B][C]") ]])
        fnCutscene([[ Append("Lord:[E|Neutral] If you had given me an extra ten minutes I would have been on my way.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Nice to see you're in one piece, Talos.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Upset] She was supposed to have left the biolabs hours ago and returned to sector 301.[B][C]") ]])
        fnCutscene([[ Append("Talos:[E|Unsure] Sorry chief, things don't always work out the way you want them to.[P] The explosions suggest that things have already started and yet all these lords being here...[B][C]") ]])
        if(bIsSX399Present == true) then
            fnCutscene([[ Append("SX-399:[E|Smirk] Guess we're not in a position to be lecturing about things not going to plan, eh?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Our issues are different.[B][C]") ]])
        else
            fnCutscene([[ Append("Christine:[E|Smirk] Sorry about that, but we're working on it.[P] Let's make the best of it.[B][C]") ]])
        end
        fnCutscene([[ Append("Talos:[E|Wise] The good news is that I still have some intel I can share. For a price.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] For a price?[B][C]") ]])
        fnCutscene([[ Append("Talos:[E|Neutral] I have a smuggler contact near here that I think can get me to sector 301, but they don't take work credits.[P] Platina only.[B][C]") ]])
        fnCutscene([[ Append("Talos:[E|Unsure] So I need to ask you for a 'donation', chief.[P] Sorry about that.[B][C]") ]])
        fnCutscene([[ Append("Talos:[E|Neutral] Now, what can I help you with?[B]") ]])
        fnSetTalosTopics()

    -- |[Repeats]|
    else
        fnCutscene([[ Append("Talos:[E|Neutral] What can I help you with, comrades?[B]") ]])
        fnSetTalosTopics()
    end
    
-- |[ ==================================== Always-Available ==================================== ]|
elseif(sTopicName == "No") then
	WD_SetProperty("Hide")
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Talos", "Neutral") ]])
    fnCutscene([[ Append("Talos:[E|Unsure] Changed your mind?[P] That's chill.[B][C]") ]])
    fnCutscene([[ Append("Talos:[E|Neutral] Anything else I can do for you?[B]") ]])
    fnSetTalosTopics()

elseif(sTopicName == "Goodbye") then
	WD_SetProperty("Hide")
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Talos", "Neutral") ]])
    fnCutscene([[ Append("Talos:[E|Neutral] From the hills to Serenity, Regulus City will be free.") ]])

elseif(sTopicName == "Fashion") then
	WD_SetProperty("Hide")
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Talos", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Blush] How do I put this gently...[P] the way that you dress...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Blush] You look like a raiju ate a prism and vomited it back up.[B][C]") ]])
    fnCutscene([[ Append("Talos:[E|Unsure] You want it so bad, don't you?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Blush] Completely and totally, give me your fashion wisdom.[P] How can I be like you?[B][C]") ]])
    fnCutscene([[ Append("Talos:[E|Neutral] Beauty is in the eye of the beholder, my dear.[P] Be what you want and you will never dress wrong.[B][C]") ]])
    fnCutscene([[ Append("Talos:[E|Neutral] If you are comfortable and happy then everyone who complains is a naysayer.[B][C]") ]])
    fnCutscene([[ Append("Talos:[E|Neutral] Anything else you need from me?[B]") ]])
    fnSetTalosTopics()

elseif(sTopicName == "Hackers") then
	WD_SetProperty("Hide")
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Talos", "Neutral") ]])
    fnCutscene([[ Append("55:[E|Neutral] I have a few questions about your hacker collective.[B][C]") ]])
    fnCutscene([[ Append("Talos:[E|Unsure] Sorry chief, I'd still like to keep their identities private for a little longer.[P] If this doesn't work out...[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] That is fine, and I agree with your assessment.[P] I was actually intending to ask about your cover stories.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Smug] Not that lord golems need much of a reason to be aloof, in fact, I understand it is a sign of social strength to miss work.[B][C]") ]])
    fnCutscene([[ Append("Talos:[E|Neutral] You got that right.[P] I found a real useful business.[B][C]") ]])
    fnCutscene([[ Append("Talos:[E|Wise] Ever been looking over someone's shoulder, only for them to switch what's on their PDU to something 'work related'?[P] You know they're looking at porn.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Blush] Uh, um, uh...[B][C]") ]])
    fnCutscene([[ Append("Talos:[E|Wise] Eheheh, darkmatters kissing golems, so lewd![B][C]") ]])
    fnCutscene([[ Append("Talos:[E|Neutral] I basically made myself a 'cam service' that other lord golems pay me for.[P] They get to watch anonymized units making out, to get their thrills.[B][C]") ]])
    fnCutscene([[ Append("Talos:[E|Neutral] It's not actually illegal if it's not lord or command units, you know.[P] So my spy ring is totally legit.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Offended] Disgusting.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] Criminy...[B][C]") ]])
    fnCutscene([[ Append("Talos:[E|Neutral] I completely agree, that's why I joined the rebels, chief.[P] Anyway, the data I collect is 'known' by security, and they sometimes ask me for 'help'.[B][C]") ]])
    fnCutscene([[ Append("Talos:[E|Neutral] I have been walking an extremely fine line here.[P] Security would love to know who we are, but they also depend on us, so they can't press too hard.[B][C]") ]])
    fnCutscene([[ Append("Talos:[E|Neutral] I have to travel through the city to help with bugs and other tech support, kind of like you, Christine.[P] My helpers are in similar positions.[B][C]") ]])
    fnCutscene([[ Append("Talos:[E|Unsure] It's...[P] unsavory, but it has kept us out of the crosshairs.[P] Other mavericks aren't 'useful', so they get...[P] shot.[B][C]") ]])
    fnCutscene([[ Append("Talos:[E|Neutral] But all that changes tonight![P] Or rather, tomorrow.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Indeed.[P] Thank you.[B][C]") ]])
    fnCutscene([[ Append("Talos:[E|Neutral] Anyway, can we talk about something else?[B]") ]])
    fnSetTalosTopics()
    
-- |[ =================================== Querying Catalysts =================================== ]|
elseif(string.sub(sTopicName, 1, 5) == "Query") then
	WD_SetProperty("Hide")

    --Resolve the catalyst name.
    local sCatalystType = string.sub(sTopicName, 6)
    
    --Variables.
    local iPlatina = AdInv_GetProperty("Platina")
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    
    --Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Talos", "Neutral") ]])
    fnCutscene([[ Append("Talos:[E|Neutral] I will mark all the catalysts of that type on your map for a one-time fee of 500 platina.[P] Any you have already found won't appear, and they will be removed as you find them.[B][C]") ]])
    if(iPlatina < 500) then
        fnCutscene([[ Append("Talos:[E|Neutral] We got a deal?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] I'm a little strapped, actually.[B][C]") ]])
        fnCutscene([[ Append("Talos:[E|Neutral] No problem, get back to me when you have the readies.[P] Sorry to ask you for money but smuggling isn't free either.") ]])
    else
        fnCutscene([[ Append("Talos:[E|Neutral] We got a deal?[B]") ]])
        fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Sure\",      " .. sDecisionScript .. ", \"Yes" .. sCatalystType .. "\") ")
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"No Thanks\", " .. sDecisionScript .. ", \"No\") ")
    end
    
-- |[ ============================== Purchasing Catalyst Markers =============================== ]|
elseif(string.sub(sTopicName, 1, 3) == "Yes") then
	WD_SetProperty("Hide")

    --Resolve the catalyst name.
    local sCatalystType = string.sub(sTopicName, 4)
    
    --Assemble the variable path.
    local sPath = "Root/Variables/Chapter5/Talos/iTalosShow_" .. sCatalystType
    
    --Set it to 1
    VM_SetVar(sPath, "N", 1.0)
    
    --Subtract platina.
	AdInv_SetProperty("Remove Platina", 500)
    
    --Show pins.
    --local sLevelName = AL_GetProperty("Name")
    --GUIMap:fnRunPinHandlerOnAllMaps(sLevelName)
    
    --Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Talos", "Neutral") ]])
    fnCutscene([[ Append("Talos:[E|Neutral] [SOUND|Menu|BuyOrSell]There you go, I've updated your PDU.[P] I can practically feel the freedom oozing over me.[B][C]") ]])
    fnCutscene([[ Append("Talos:[E|Neutral] Anything else I can do for you?[B]") ]])
    fnSetTalosTopics()
    
    --Update map, must be done instead of the pin handler due to a different format for chapter 5 maps.
	fnResolveMapLocation("RegulusBiolabsTransitA")
    
end