-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
if(sObjectName == "Encounter") then

    --Variables.
    local bIsSX399Present = fnIsCharacterPresent("SX-399")
    local iSawTransitEncounter = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawTransitEncounter", "N")
    if(iSawTransitEncounter == 0.0) then
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSawTransitEncounter", "N",1.0)
        
        --Spawn NPCs.
        TA_Create("2856")
            TA_SetProperty("Position", 15, 14)
            TA_SetProperty("Facing", gci_Face_East)
            TA_SetProperty("Clipping Flag", false)
            fnSetCharacterGraphics("Root/Images/Sprites/56/", true)
        DL_PopActiveObject()
        TA_Create("DroneA")
            TA_SetProperty("Position", 14, 14)
            TA_SetProperty("Facing", gci_Face_East)
            TA_SetProperty("Clipping Flag", false)
            fnSetCharacterGraphics("Root/Images/Sprites/LatexDrone/", false)
        DL_PopActiveObject()
        TA_Create("DroneB")
            TA_SetProperty("Position", 13, 14)
            TA_SetProperty("Facing", gci_Face_East)
            TA_SetProperty("Clipping Flag", false)
            fnSetCharacterGraphics("Root/Images/Sprites/LatexDrone/", false)
        DL_PopActiveObject()
        
        --Reposition NPCs.
        fnCutsceneTeleport("GolemA", 16.25, 14.50)
        fnCutsceneFace("GolemA", -1, 0)
        fnCutsceneFace("GolemJ", 0, -1)
    
        --Party unfolds.
        fnCutsceneMove("Christine", 5.25, 13.50)
        fnCutsceneFace("Christine", 1, 0)
        fnCutsceneMove("Tiffany", 5.25, 14.50)
        fnCutsceneFace("Tiffany", 1, 0)
        fnCutsceneMove("Sophie", 4.25, 13.50)
        fnCutsceneFace("Sophie", 1, 0)
        if(bIsSX399Present == true) then
            fnCutsceneMove("SX-399", 4.25, 14.50)
            fnCutsceneFace("SX-399", 1, 0)
        end
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    
        --Camera moves.
        Cutscene_CreateEvent("CameraEvent", "Camera")
            CameraEvent_SetProperty("Max Move Speed", 3.0) --Default is 5.0
            CameraEvent_SetProperty("Focus Position", (16.25 * gciSizePerTile), (14.50 * gciSizePerTile))
        DL_PopActiveObject()
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "2856", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "LatexDrone", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "LatexDroneB", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
        fnCutscene([[ Append("Lord:[E|Neutral] But what will happen to us?[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Neutral] None of my concern.[B][C]") ]])
        fnCutscene([[ Append("Lord:[E|Neutral] The other Lord Golems are frightened and on the verge of panic.[P] Surely - [P][CLEAR]") ]])
        fnCutscene([[ Append("2856:[E|Angry] None.[P] Of.[P] My.[P] Concern.[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Neutral] My responsibilities are to the city, not a collection of its most worthless citizens.[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Angry] Solve your own problems.[P] Stand aside, Lord Unit.[B][C]") ]])
        fnCutscene([[ Append("Lord:[E|Neutral] Milady, please...[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Yelling] STAND ASIDE.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneMoveFace("GolemA", 16.25, 13.50, 0, 1)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneMove("2856", 26.25, 14.50)
        fnCutsceneMove("2856", 26.25, 15.50)
        fnCutsceneMove("DroneA", 26.25, 14.50)
        fnCutsceneMove("DroneB", 25.25, 14.50)
        fnCutsceneBlocker()
        fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
        fnCutscene([[ AL_SetProperty("Open Door", "DoorC") ]])
        fnCutsceneBlocker()
        fnCutsceneMove("2856", 26.25, 19.50)
        fnCutsceneMove("DroneA", 26.25, 14.50)
        fnCutsceneMove("DroneA", 26.25, 18.50)
        fnCutsceneMove("DroneB", 26.25, 14.50)
        fnCutsceneMove("DroneB", 26.25, 17.50)
        fnCutsceneBlocker()
        fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
        fnCutsceneTeleport("2856", -100, -100)
        fnCutsceneTeleport("DroneA", -100, -100)
        fnCutsceneTeleport("DroneB", -100, -100)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Camera moves back to the party.
        Cutscene_CreateEvent("CameraEvent", "Camera")
            CameraEvent_SetProperty("Max Move Speed", 3.0) --Default is 5.0
            CameraEvent_SetProperty("Focus Actor Name", "Tiffany")
        DL_PopActiveObject()
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutscene([[ Append("Sophie:[E|Neutral] She's quite a...[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Offended] A bad thing.[P] Bad word.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] We should do something.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Allow me.[B][C]") ]])
        if(bIsSX399Present == true) then
            fnCutscene([[ Append("SX-399:[E|Smirk] I don't think she meant 'shoot them'.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Not my intention.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] Huh.[P] Okay.[P] Work your magic.") ]])
        else
            fnCutscene([[ Append("Christine:[E|Neutral] Go right ahead.") ]])
        end
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneMove("Tiffany", 15.25, 14.50)
        fnCutsceneMove("Tiffany", 15.25, 13.50)
        fnCutsceneFace("Tiffany", 1, 0)
        fnCutsceneBlocker()
        fnCutsceneFace("GolemA", -1, 0)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
        fnCutscene([[ Append("55:[E|Neutral] What seems to be the problem here?[B][C]") ]])
        fnCutscene([[ Append("Lord:[E|Neutral] Command -[P] Unit?[P] Weren't you just here?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Incorrect.[P] That was my twin sister, Unit 2856.[B][C]") ]])
        fnCutscene([[ Append("Lord:[E|Neutral] Then that makes you...[B][C]") ]])
        fnCutscene([[ Append("Lord:[E|Neutral] Hello, Head of Security Unit 2855![B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Former Head of Security.[P] I have resigned my post.[B][C]") ]])
        fnCutscene([[ Append("Lord:[E|Neutral] You -[P] you can do that?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] I am here to assist you in whatever way I can.[P] What is the situation?[B][C]") ]])
        fnCutscene([[ Append("Lord:[E|Neutral] But -[P] I - [P][CLEAR]") ]])
        fnCutscene([[ Append("55:[E|Neutral] You are the Lord Golem in charge of this station.[B][C]") ]])
        fnCutscene([[ Append("Lord:[E|Neutral] No, I'm in charge of manufacturing in Sector 82.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] We are currently facing an emergency that is wholly unprecedented.[P] You are the golem who stepped forward.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] The others did not.[P] You are therefore the first choice for leader.[B][C]") ]])
        fnCutscene([[ Append("Lord:[E|Neutral] I'm no security unit.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] None of you are.[P] Therefore, you will need to improvise.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] I have important tasks elsewhere, but I can take a few minutes to give you some useful advice.[P] Will you listen?[B][C]") ]])
        fnCutscene([[ Append("Lord:[E|Neutral] Uh -[P] of course, Command Unit![P] Thank you![B][C]") ]])
        fnCutscene([[ Append("Lord:[E|Neutral] I'm not sure if I am up to the task...[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Your options are limited.[P] You will not be able to wait for rescue.[P] Take your fate into your own hands.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] I believe in you.[B][C]") ]])
        fnCutscene([[ Append("Lord:[E|Neutral] All right.[P] What -[P] what should I do first?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] First, organize the other units and get a headcount.[P] Take stock of your personnel and skillsets.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Judging by your outfits, I am assuming you were diverted to this transit station after the gala?[B][C]") ]])
        fnCutscene([[ Append("Lord:[E|Neutral] Yes.[P] I was here with my tandem unit, when we heard explosions.[P] Then, these other golems came from below.[B][C]") ]])
        fnCutscene([[ Append("Lord:[E|Neutral] They said the tunnels have collapsed and they were diverted here.[P] It seems we can't use the transit lines until they are cleared.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Yes, as I said, there is an emergency.[P] Invaders are in the facility, and security is busy elsewhere.[P] Do not allow your units to come into contact with them.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Select those that are most physically capable.[P] Organize them into teams of two.[P] Arm them with whatever you have.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Blunt weapons will work, sharp weapons are preferred.[P] You can snap the sports equipment here for makeshift weapons.[B][C]") ]])
        fnCutscene([[ Append("Lord:[E|Neutral] You want us to [P]*fight*[P] these creatures?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Only if absolutely necessary.[P] Rather, you will need to set scouts on each entrance.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] If you are attacked, fall back and drop the door's deadbolts.[P] Barricade yourselves in.[P] Use force only if the barricades are breached.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Have an escape plan.[P] The Raiju Ranches are safe, but there are likely hostiles between you and there.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Meanwhile, anyone with geological or transit skills should assess the tunnels below us, and put together a crew to attempt to clear them.[B][C]") ]])
        fnCutscene([[ Append("Lord:[E|Neutral] We are Lord Units.[P] We don't do...[P] work.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Upset] You do now.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Either you work or you wait here for the invaders to find you.[P] Make your choice.[B][C]") ]])
        fnCutscene([[ Append("Lord:[E|Neutral] But can't you - [P][CLEAR]") ]])
        fnCutscene([[ Append("55:[E|Upset] No help is coming, and it is unknown when the situation will be resolved.[P] You cannot make excuses.[P] Reality will not wait for you.[B][C]") ]])
        fnCutscene([[ Append("Lord:[E|Neutral] I see.[B][C]") ]])
        fnCutscene([[ Append("Lord:[E|Neutral] So that's it, then.[P] No help is coming.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] No.[B][C]") ]])
        fnCutscene([[ Append("Lord:[E|Neutral] At least I have an idea of what to do now.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Lastly, you must not allow the golems here to panic, or give them time to spread rumours.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Keep them occupied or they will gossip.[P] Gossip leads to paranoia, and that is a problem.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] If they will not work, find an activity for them.[P] Simple games or even presentations on research topics.[P] Anything so they will be diverted from negative feedback loops.[B][C]") ]])
        fnCutscene([[ Append("Lord:[E|Neutral] Hmmm...[B][C]") ]])
        fnCutscene([[ Append("Lord:[E|Neutral] I'm sure we have some researchers here.[P] And we can playback videographs on those computers.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Make sure they know the security forces are doing what they can, and they must merely hold out.[P] Give them hope.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] I know you can do this.[P] Don't let me down.[B][C]") ]])
        fnCutscene([[ Append("Lord:[E|Neutral] Yes, yes, this will be trivial.[P] I am certain we can weather this crisis.[B][C]") ]])
        fnCutscene([[ Append("Lord:[E|Neutral] Thank you, Command Unit.[P] I'll begin taking inventory and delegating tasks, just like I do in my department.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] Very good.[B][C]") ]])
        fnCutscene([[ Append("Lord:[E|Neutral] Uh, one other thing.[B][C]") ]])
        fnCutscene([[ Append("Lord:[E|Neutral] Are you the same Unit 2855 who I've heard so much about?[P] The -[P] you can't be the same one.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] ...[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] No I am not.[P] Not the same one at all.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneMove("GolemA", 22.25, 13.50)
        fnCutsceneBlocker()
        fnCutsceneFace("GolemI", -1, 0)
        fnCutsceneWait(45)
        fnCutsceneBlocker()
        fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
        fnCutscene([[ AL_SetProperty("Open Door", "DoorA") ]])
        fnCutsceneBlocker()
        fnCutsceneMove("GolemA", 22.25, 11.50)
        fnCutsceneMove("GolemA", 25.25, 11.50)
        fnCutsceneFace("GolemA", 0, -1)
        fnCutsceneMove("GolemI", 23.25, 14.50)
        fnCutsceneMove("GolemI", 20.25, 14.50)
        fnCutsceneMove("GolemI", 20.25, 15.50)
        fnCutsceneBlocker()
        fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
        fnCutscene([[ AL_SetProperty("Open Door", "DoorB") ]])
        fnCutsceneFace("GolemB", 0, 1)
        fnCutsceneFace("GolemC", 0, 1)
        fnCutsceneMove("GolemI", 20.25, 17.50)
        fnCutsceneMove("GolemI", 19.25, 17.50)
        fnCutsceneBlocker()
        fnCutsceneFace("GolemG", 1, 0)
        fnCutsceneBlocker()
        fnCutsceneWait(45)
        fnCutsceneBlocker()
        
        --55 moves.
        fnCutsceneMove("Tiffany", 5.25, 14.50)
        fnCutsceneFace("Tiffany", -1, 0)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        if(bIsSX399Present == true) then
            fnCutscene([[ Append("SX-399:[E|Flirt] Christine, watch out.[P] Someone's gunning for your leadership spot.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Smirk] Thank you, but I am not interested in leadership.[B][C]") ]])
        else
            fnCutscene([[ Append("Christine:[E|Smirk] Wow, 55![P] Things might just work out for these golems![B][C]") ]])
        end
        fnCutscene([[ Append("Sophie:[E|Neutral] But aren't these lords our enemies?[P] Will organizing them just make them harder to defeat later?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] That is a possibility, though a remote one.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] It would, however, be irresponsible to leave them in this state and simply hope that our enemies do not claim them.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] We do not need to feed Vivify's ranks.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] And who knows?[P] Maybe they'll think they owe 55 and even join us.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Excessively optimistic, Christine.[B][C]") ]])
        if(bIsSX399Present == true) then
            fnCutscene([[ Append("SX-399:[E|Smirk] After watching you do that, I'd say there's cause for optimism.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Smirk] You've come a long way since we first met.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Smirk] Th-[P]thank you.[B][C]") ]])
        end
        fnCutscene([[ Append("Sophie:[E|Neutral] But how'd you know what to say?[P] And when to say it?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smug] I merely asked myself what an overly emotional fool would say in the same situation.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Blush] She means me...[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smug] Yes.[P] I do.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] Now, proceed.[P] We cannot know how long until this position is compromised.[P] I suggest we do not allow this work to be for naught.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Move everyone onto the leader, and call the fold automatically.
        fnAutoFoldParty()
        fnCutsceneBlocker()
    
    end
end
