-- |[Root]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    --Subdivide for all the entities sharing this script.
    if(sActorName == "Talos") then
        LM_ExecuteScript(fnResolvePath() .. "Dialogue Talos.lua", "Hello")
    elseif(sActorName == "GolemA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|GolemLord] I've delegated out some assignments, Command Unit.[P] Leave the rest to us.") ]])
    
    elseif(sActorName == "GolemB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|GolemLordC] I suppose we must all chip in for the Cause, yes?") ]])
    
    elseif(sActorName == "GolemC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|GolemLordB] Scouting?[P] Hmm, that seems like an assignment for someone wearing...[P] green.[P] Am I right?[P] Fashion is such a challenge.") ]])
    
    elseif(sActorName == "GolemD") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|GolemLord] There go my plans to go rowing with my tandem unit...") ]])
    
    elseif(sActorName == "GolemE") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|GolemLordC] Everyone else is worried, but I dare say this has been the most exciting gala ever![P] Usually they are such bland affairs.") ]])
    
    elseif(sActorName == "GolemF") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|GolemLordB] I know a little of geological assessments...[P] Maybe I can help.") ]])
    
    elseif(sActorName == "GolemG") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|GolemLord] There are no Slave Units about.[P] Where have they gotten to?") ]])
    
    elseif(sActorName == "GolemH") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|GolemLord] Violence.[P] Pah.[P] I'm not designed for the thing, but I'll die fighting before I let those thugs sully these gorgeous outfits!") ]])
    
    elseif(sActorName == "GolemI") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|GolemLordC] I'll try to keep everyone focused and occupied until we figure something out.[P] I used to be in charge of morale in Sector 154, I've worked wonders with two tin cans and a string.[P] This?[P] This will be easy.") ]])
    
    elseif(sActorName == "GolemJ") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|GolemLord] I got a good look at the creatures before we had to run.[P] I saw what they did to that Command Unit.[P] Can that happen to us?") ]])
    
    elseif(sActorName == "GolemK") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|GolemFancyE] She's been staring at that screen, as if it's going to update and let us know the tunnels have been cleared.") ]])
    
    elseif(sActorName == "GolemL") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|GolemLordB] Everything will be okay, everything will be fine.[P] The tunnels will get cleared and it will all have been a silly mistake...") ]])
    end
end
