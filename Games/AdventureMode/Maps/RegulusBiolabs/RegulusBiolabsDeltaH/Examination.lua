-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
-- |[Objects]|
if(sObjectName == "TerminalA") then
    
    --Variables
    local iBoatShortcut = VM_GetVar("Root/Variables/Chapter5/Scenes/iBoatShortcut", "N")
    if(iBoatShortcut == 0.0) then
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iBoatShortcut", "N", 1.0)
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Hmm, seems I can open the sluice gates from this console...)") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Camera moves.
        Cutscene_CreateEvent("CameraEvent", "Camera")
            CameraEvent_SetProperty("Max Move Speed", 5.0) --Default is 5.0
            CameraEvent_SetProperty("Focus Position", (10.25 * gciSizePerTile), (28.50 * gciSizePerTile))
        DL_PopActiveObject()
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --SFX, open gate.
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "RemovableWallLayerA", true) ]])
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "RemovableWallLayerB", false) ]])
        fnCutscene([[ AudioManager_PlaySound("World|RemoteDoor") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Repeat.
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "RemovableWallLayerB", true) ]])
        fnCutscene([[ AudioManager_PlaySound("World|RemoteDoor") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        Cutscene_CreateEvent("CameraEvent", "Camera")
            CameraEvent_SetProperty("Max Move Speed", 5.0) --Default is 5.0
            CameraEvent_SetProperty("Focus Actor Name", "Christine")
        DL_PopActiveObject()
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (We should be able to get back to the Alpha Laboratories quickly now if we take that boat!)") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (The sluice gate leading to the Alpha Laboratories is already open.)") ]])
        fnCutsceneBlocker()
    
    end
    
elseif(sObjectName == "TerminalB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('I asked my Lord Golem what I'm supposed to do if the heat transfer pipes freeze over, or the pumps stop working.[P] She shrugged at me.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Yes, it's not our department.[P] But if those pipes freeze, an entire habitation dome could freeze.[P] All the experiments would be wrecked, not to mention all the animals in the domes would be at risk.[P] They're not built to go into hibernation at a moment's notice!')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Just got back from seeing that new bootleg Professor Killsalot videograph.[P] Editing was bad, acting was awful, but you can't keep a good story down!')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Shelf") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Discarded pH monitors and salinity checkers.[P] On their way to be recycled, eventually.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Boat") then

    --Variables.
    local iBoatShortcut = VM_GetVar("Root/Variables/Chapter5/Scenes/iBoatShortcut", "N")
    
    --Not open yet:
    if(iBoatShortcut == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (There's a boat here, but the sluice gates to the west are closed.[P] I can probably use this boat to get to the Alpha labs if I can open them...)") ]])
        fnCutsceneBlocker()

    --Opened:
    else
        WD_SetProperty("Show")
        Append("Thought:[VOICE|Leader] (Use the boat to cross to the Alpha Laboratories?)[BLOCK]")
        
        --Activate and set.
        local sDestScript = LM_GetCallStack(0)
        WD_SetProperty("Activate Decisions")
        WD_SetProperty("Add Decision", "Yes", sDestScript, "Yes")
        WD_SetProperty("Add Decision", "No",  sDestScript, "No")

    end

-- |[Crossing the Lake]|
elseif(sObjectName == "Yes") then
	WD_SetProperty("Hide")
    
    --Variables.
    local i55ComplainedBoat = VM_GetVar("Root/Variables/Chapter5/Scenes/i55ComplainedBoat", "N")
    if(i55ComplainedBoat == 0.0) then
    
        --Flag
        VM_SetVar("Root/Variables/Chapter5/Scenes/i55ComplainedBoat", "N", 1.0)
    
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        fnCutscene([[ Append("Christine:[E|Smirk] Okay everyone, grab an oar![B][C]") ]])
        fnCutscene([[ Append("55:[E|Upset] Is this absolutely necessary?[P] Why don't we simply use your teleportation ability?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] This is faster.[P] C'mon.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Upset] Are you absolutely sure?[P] What if the boat capsizes?[P] That crude wooden device looks poorly maintained.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Are you joking?[P] What rich girl wasn't part of the rowing team in school?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Okay a lot of them, but not this one![P] I'm a seasoned mariner.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] So long as I have your assurances...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] It'll be okay, I know what I'm doing.") ]])
        fnCutsceneBlocker()
    
        --Transition.
        fnCutscene([[ AL_BeginTransitionTo("RegulusBiolabsAlphaB", "FORCEPOS:53.5x24.0x0") ]])
    
    else
        AL_BeginTransitionTo("RegulusBiolabsAlphaB", "FORCEPOS:53.5x24.0x0")
    
    end
    
elseif(sObjectName == "No") then
	WD_SetProperty("Hide")
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
