-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[55 Investigation]|
--If 55 is investigating, then run a different script.
local iChristineLeadingParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iChristineLeadingParty", "N")
if(iChristineLeadingParty == 0.0) then
    LM_ExecuteScript(fnResolvePath() .. "Examination55.lua", sObjectName)
    return
end

-- |[Exits]|
-- |[Objects]|
if(sObjectName == "TerminalA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (This terminal controls the screen outside.[P] It is currently in intermission mode.)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "RVD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (This RVD plays the same videograph as the movie outside.[P] Is it so the staff can watch, too?)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Oilmaker") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Fizzy Pop! flavour for oilmakers'.[P] Anyone who drinks that will probably be blown clear through the roof.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Sign") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] There's a lot of different snacks.[P] But what are these symbols next to their names?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] That one looks like a human, that one looks like a golem...[P] and that yellow spiky one is a Raiju?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Oh, I get it![P] This is who the snacks are safe for![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Are you telling me, right now, that humans don't like eating shredded plastic?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] As hard as that is to believe...[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Offended] Now that's just sad.[P] Shredded plastic on top of powdered gypsum is my idea of heaven...") ]])
    
elseif(sObjectName == "Snacks") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "HumanF1", "Neutral") ]])
    fnCutscene([[ Append("Human:[E|Neutral] Can I get you any snacks?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Hmmm...[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] What is a chock-ho-lit?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] One chocolate chunk for my tandem unit, please![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] *Num*[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] Smooth, chewy, melty, delicious...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Chocolate is the perfect treat for organic and synthetic alike.[B][C]") ]])
    fnCutscene([[ Append("Human:[E|Neutral] We grow it here in the biolabs![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Christine, I hereby order you to overthrow the administration so we can all have chocolate.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] I'm now officially addicted.[P] I need more.[B][C]") ]])
    fnCutscene([[ Append("Human:[E|Neutral] Yeah, you and everyone else.[P] I love the stuff.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Blush] (What monster have I unleashed?[P] The world trembles before steps of a chocoholic...)") ]])
    
elseif(sObjectName == "Speaker") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (These speakers play the videograph's audio with a detachable receiver.[P] You can also wire a golem's auditory receptors to receive it directly.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Screen") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (An enormous digital display on a special platform.[P] The videograph is currently in intermission and will resume soon.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Bathroom") then

    --Variables.
    local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Wow, how long has it been since I've seen a bathroom.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] Don't miss them, nope.[P] Not one bit.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Bathroom...[P] Oh![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] Organics dispose of waste in these![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] You should not be smiling when you say that.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Huh?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Oh don't tell me you still have the essence fallacy in your central processor.[B][C]") ]])
    if(sChristineForm == "Human") then
        fnCutscene([[ Append("Sophie:[E|Neutral] I mean brain.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] It's a central processor, it's just wetware right now.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] It's quite reassuring to know that, once reprogrammed, a human brain is just as efficient as a golem processor.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] Except you still subscribe to the essence fallacy.[B][C]") ]])
    end
    fnCutscene([[ Append("Christine:[E|Neutral] Which one is that again?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] The belief that a material imparts an invisible essence on contact.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] If biological waste disgusts you, just wash your chassis.[P] There's nothing dirty after you're washed.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] But you seemed excited about the very concept.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Because this is how the world will look once the Cause of Science remakes it.[P] Indoor plumbing![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Those poor humans on Pandemonium live in squalor and disease.[P] We invented pressurized pipes.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Well as long as you're excited about the engineering...[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] What else is there to be excited about?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Uhhhh...[P] [EMOTION|Christine|Smirk]Converting all humans into perfect robots when we bring the Cause of Science to them?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Oh, I see what you were getting at.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Laugh] (Phew![P] Saved it!)") ]])
    fnCutsceneBlocker()
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
