-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
if(sObjectName == "PostAction") then
    
    --Clear parallel scripts.
    Cutscene_HandleParallel("ClearAll")
    
    --Upon firing, black the screen out.
    fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Spawn Christine and Sophie.
    fnSpecialCharacter("Christine", 31, 35, gci_Face_North, false, "Null")
    fnSpecialCharacter("Sophie",    32, 35, gci_Face_North, false, "Null")
    
    --Run Christine's scripts.
    local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
    if(sChristineForm == "Human") then
        LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Human.lua")
    elseif(sChristineForm == "Golem") then
        LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua")
    else
        LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Raiju.lua")
    end
    
    --Reposition.
    fnCutsceneTeleport("Christine", 31.75, 35.50)
    fnCutsceneFace("Christine", 0, -1)
    fnCutsceneTeleport("Sophie", 32.75, 35.50)
    fnCutsceneFace("Sophie", 0, -1)
    
    --Set Christine as the party lead.
    EM_PushEntity("Christine")
        local iLeaderID = RE_GetID()
    DL_PopActiveObject()
    AL_SetProperty("Player Actor ID", iLeaderID)

    --Get Sophie's uniqueID. 
    EM_PushEntity("Sophie")
        local iFollowerID = RE_GetID()
    DL_PopActiveObject()

    --Store Sophie's ID and tell her to follow.
    giFollowersTotal = 1
    gsaFollowerNames[1] = "Sophie"
    giaFollowerIDs[1] = iFollowerID
    AL_SetProperty("Follow Actor ID", iFollowerID)
    
    --Set camera to follow Christine.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Focus Actor Name", "Christine")
    DL_PopActiveObject()
    fnCutsceneBlocker()
    
    --Teleport Sammy away.
    fnCutsceneTeleport("Sammy", -1.25, -1.50)
    
    --Fade in.
    fnCutsceneWait(65)
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Blush] Well that certainly happened.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] What's going to happen next?[P] Do you think Agent Almond is going to stop the fake Almond?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Yes.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] You seem awfully confident about that.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] These movies all follow the same formula.[P] I've seen it a hundred times.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Hero struggles and overcomes the baddies, gets the girl, they make out, roll credits.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] ...[P] Yeah that's pretty much it.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] And yet I want to see what happens next![P] Oh my goodness![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Me too![P] But we also should probably get going before 55 comes looking and boxes my ears.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Just promise we'll watch the rest of it later.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] And if you see Sammy Davis out there in the Biolabs -[P] ask her for an autograph![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Does she live here?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Probably.[P] All the actors do, I would think.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Anyway, lead on, dearest.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Move.
    fnCutsceneMove("Sophie", 31.75, 35.50)
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Fold Party") ]])
    fnCutsceneBlocker()
    
end
