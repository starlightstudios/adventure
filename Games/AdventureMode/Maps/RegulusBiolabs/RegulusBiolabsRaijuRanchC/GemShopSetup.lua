-- |[ =================================== VN-19's Shop Setup =================================== ]|
--Second time around. Sells the same gems, now stocks flakes.

-- |[VN-19 Cuts Gems!]|
AM_SetShopProperty("Allow Gemcutting")

-- |[Item Stock Listing]|
--Adamantite
AM_SetShopProperty("Add Item", "Adamantite Powder", -1, -1)
AM_SetShopProperty("Add Item", "Adamantite Flakes", -1, -1)

--Gems
AM_SetShopProperty("Add Item", "Glintsteel Gem", -1, -1)
AM_SetShopProperty("Add Item", "Romite Gem",     -1, -1)
AM_SetShopProperty("Add Item", "Morite Gem",     -1, -1)
AM_SetShopProperty("Add Item", "Rubose Gem",     -1, -1)
AM_SetShopProperty("Add Item", "Iniorose Gem",   -1, -1)
AM_SetShopProperty("Add Item", "Mordreen Gem",   -1, -1)
AM_SetShopProperty("Add Item", "Thatophage Gem", -1, -1)
