-- |[Root]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Variables.
    local iMetJX101              = VM_GetVar("Root/Variables/Chapter5/Scenes/iMetJX101", "N")
    local iMetGemcutter          = VM_GetVar("Root/Variables/Chapter5/Scenes/iMetGemcutter", "N")
    local sChristineForm         = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
    local iChristineLeadingParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iChristineLeadingParty", "N")
    
    --Common.
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
        
    --Achievement.
    AM_SetPropertyJournal("Unlock Achievement", "Sickos")
	
	--Haven't met her yet.
	if(iMetGemcutter == 0.0) then
        
        --Set the flag to 2.0 to indicate we've met her in the biolabs.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iMetGemcutter", "N", 2.0)
        
        --Christine is leading the party.
        if(iChristineLeadingParty >= 1.0) then
            fnCutscene([[ Append("VN-19:[E|Neutral] What have we here but the battle-scarred leader of the resistance, perhaps on a reconnaissance mission?[B][C]") ]])
            fnCutscene([[ Append("VN-19:[E|Neutral] My name is VN-19, gemcutter extraordinaire.[P] You'll find no finer a cutter on this moon - or any other![B][C]") ]])
            if(iMetJX101 >= 1.0) then
                fnCutscene([[ Append("Christine:[E|Neutral] Hello, VN-19.[P] Are you perchance an associate of JX-101?[B][C]") ]])
                fnCutscene([[ Append("VN-19:[E|Neutral] Associate?[P] My friend, she is one of my biggest customers![B][C]") ]])
                fnCutscene([[ Append("VN-19:[E|Neutral] Unless you think an army can run without peerlessly cut gems?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Blush] ...[P] Probably?[B][C]") ]])
                fnCutscene([[ Append("VN-19:[E|Neutral] Yeah maybe.[B][C]") ]])
                fnCutscene([[ Append("VN-19:[E|Neutral] *cough*[B][C]") ]])
                fnCutscene([[ Append("VN-19:[E|Neutral] You a friend of 'ers, love?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Of course![B][C]") ]])
                fnCutscene([[ Append("VN-19:[E|Neutral] Stupendous![P] As I said, I am a gemcutter.[P] If you need a gem cut, then that is my raison d'etre![B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Great![P] Nice to meet you, VN-19![B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Smirk] I love meeting new robots.[B][C]") ]])
                fnCutscene([[ Append("VN-19:[E|Neutral] Shall we get down to business then?[BLOCK]") ]])
            else
                fnCutscene([[ Append("Christine:[E|Smirk] Hello, VN-19.[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Smirk] Pleased to meet you.[B][C]") ]])
                fnCutscene([[ Append("VN-19:[E|Neutral] Stupendously met![P] As I said, I am a gemcutter.[P] If you need a gem cut, then that is my raison d'etre![B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Smirk] We're repair units.[P] That's our -[P] whatever you said![B][C]") ]])
                fnCutscene([[ Append("VN-19:[E|Neutral] I 'unno love.[P] Just a thing I 'eard some lord unit say.[P] Dunno what it means, sounds fancy.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] It means 'reason for existing'.[P] It's French.[B][C]") ]])
                fnCutscene([[ Append("VN-19:[E|Neutral] *cough*[B][C]") ]])
                fnCutscene([[ Append("VN-19:[E|Neutral] So![P] Gems![P] Shall I cut one for you?[BLOCK]") ]])
            end
        
        --55 is leading the party.
        else
            fnCutscene([[ Append("55:[E|Neutral] Regulus is the only moon of Pandemonium and the only inhabited moon in the Bilarus system.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Your statement is needlessly elaborate.[B][C]") ]])
            fnCutscene([[ Append("VN-19:[E|Neutral] [P]*sniff*[P] Yeah that's called a flourish, love.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Smirk] Hey, VN-19.[P] How you been?[B][C]") ]])
            fnCutscene([[ Append("VN-19:[E|Neutral] My my.[P] SX-399?[P] I barely recognize you![B][C]") ]])
            fnCutscene([[ Append("VN-19:[E|Neutral] So the rumours were true![B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Flirt] I got me a custom-made body all thanks to Cutie-5 here.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] ...[P] Hello, friend of SX-399.[B][C]") ]])
            fnCutscene([[ Append("VN-19:[E|Neutral] Cutie-5?[P] Huh.[P] Coulda swore 'er name was summat else.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Upset] My designation is - [P][CLEAR]") ]])
            fnCutscene([[ Append("VN-19:[E|Neutral] So![P] My friends, are you in need of the services of a gemcutter?[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Smirk] Oh yeah![P] VN-19 here cuts all the gems for -[P] well, the Steam Droids.[P] I think she's the only gemcutter we have.[B][C]") ]])
            fnCutscene([[ Append("VN-19:[E|Neutral] It's a rare skill.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Smirk] Are we in need of any of her services?[BLOCK]") ]])
        end
    
    --Met her before in the basement of Regulus City:
    elseif(iMetGemcutter == 1.0) then
        
        --Set the flag to 2.0 to indicate we've met her in the biolabs.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iMetGemcutter", "N", 2.0)
        
        --Christine is leading the party.
        if(iChristineLeadingParty >= 1.0) then
            fnCutscene([[ Append("VN-19:[E|Neutral] Ha ha![P] Stupendous![P] The former maverick, now a revolutionary![B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Laugh] VN-19![P] Great to see you again![B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] You know each other?[B][C]") ]])
            fnCutscene([[ Append("VN-19:[E|Neutral] Ah.[P] Ah ah ah.[P] What a sight is bestowed upon me.[B][C]") ]])
            fnCutscene([[ Append("VN-19:[E|Neutral] Who is this enchanting machine?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] This is Unit 499323, Maintenance and Repair, Sector 96.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Smirk] Call me Sophie.[B][C]") ]])
            fnCutscene([[ Append("VN-19:[E|Neutral] A name as beautiful as her countenance.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Blush] Oh my![B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Don't be nervous, dearest![B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] *smooch*[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Blush] H-[P]hee hee![B][C]") ]])
            fnCutscene([[ Append("VN-19:[E|Neutral] So, Sophie.[P] Is she as fierce a lover as she is a warrior?[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Blush] .............[P] HEE HEE!!![B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Laugh] Okay, better dial it back, VN-19![B][C]") ]])
            fnCutscene([[ Append("VN-19:[E|Neutral] As you wish.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] I really didn't expect to see you here, though.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] Wait, how did you get into the Raiju Ranch?[B][C]") ]])
            fnCutscene([[ Append("VN-19:[E|Neutral] Well, I walked here.[P] And did a bit of climbing.[B][C]") ]])
            fnCutscene([[ Append("VN-19:[E|Neutral] If you know where you're going in the undercity, you can get nearly anywhere without crossing a security unit.[B][C]") ]])
            fnCutscene([[ Append("VN-19:[E|Neutral] Then all I had to do was crawl up a disposal pipe.[B][C]") ]])
            fnCutscene([[ Append("VN-19:[E|Neutral] [P]*sniff*[P] Why you lookin' at me like that?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Cry] No reason.[P][CLEAR]") ]])
            fnCutscene([[ Append("Sophie:[E|Surprised] No reason![P][CLEAR]") ]])
            fnCutscene([[ Append("VN-19:[E|Neutral] Smell comes right out, loves.[B][C]") ]])
            fnCutscene([[ Append("VN-19:[E|Neutral] Anyway, I did it all because I heard you were coming this way, and you need gems.[B][C]") ]])
            fnCutscene([[ Append("VN-19:[E|Neutral] So shall we get down to business?[BLOCK]") ]])
            
        --55 is leading the party.
        else
            fnCutscene([[ Append("VN-19:[E|Neutral] Ha ha![P] Stupendous![P] The former maverick, now a revolutionary![B][C]") ]])
            fnCutscene([[ Append("55:[E|Smirk] Greetings, VN-19.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Smirk] You've met?[B][C]") ]])
            fnCutscene([[ Append("VN-19:[E|Neutral] That voice...[B][C]") ]])
            fnCutscene([[ Append("VN-19:[E|Neutral] Is that the sweet song of SX-399?[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Flirt] The one and only![B][C]") ]])
            fnCutscene([[ Append("VN-19:[E|Neutral] Ha ha![P] Splendiferous![P] The rumours of your repair were true![B][C]") ]])
            fnCutscene([[ Append("VN-19:[E|Neutral] I've known her since she was only up to JX-101's knee, you know.[P] I am so glad to see you fixed and healthy![B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Smirk] It was all thanks to Cutie-5 here.[P] How did you meet each other?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Smirk] We have made use of her services in the past.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] I will note that I did not expect to meet you here, of all places.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] ...[P] Yeah how'd you get in here, anyway?[B][C]") ]])
            fnCutscene([[ Append("VN-19:[E|Neutral] Well, I walked here.[P] And did a bit of climbing.[B][C]") ]])
            fnCutscene([[ Append("VN-19:[E|Neutral] If you know where you're going in the undercity, you can get nearly anywhere without crossing a security unit.[B][C]") ]])
            fnCutscene([[ Append("VN-19:[E|Neutral] Then all I had to do was crawl up a disposal pipe.[B][C]") ]])
            fnCutscene([[ Append("VN-19:[E|Neutral] [P]*sniff*[P] Why you lookin' at me like that?[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Sad] Oh.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Sad] Oh my.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] That explains the smell.[P] You may wish to wash yourself before continuing.[B][C]") ]])
            fnCutscene([[ Append("VN-19:[E|Neutral] Smell comes right out, loves.[B][C]") ]])
            fnCutscene([[ Append("VN-19:[E|Neutral] Anyway, I did it all because I heard you were coming this way, and you need gems.[B][C]") ]])
            fnCutscene([[ Append("VN-19:[E|Neutral] So shall we get down to business?[BLOCK]") ]])
        end
    
    --Repeat, go to dialogue decisions quickly.
    else
        fnCutscene([[ Append("VN-19:[E|Neutral] Hello![P] Which of my talents are you in need of?[BLOCK]") ]])
    end
	
    --Decision setup.
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Let's get to the gems!\", " .. sDecisionScript .. ", \"Gemcutting\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"I'm good, thanks.\",  " .. sDecisionScript .. ", \"Bye\") ")
    fnCutsceneBlocker()

--Activate gemcutting mode.
elseif(sTopicName == "Gemcutting") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
    fnCutscene([[ Append("VN-19:[E|Neutral] Marvellous![P] Let's get started!") ]])
	fnCutsceneBlocker()
    
	--Run the shop.
    local sPath = fnResolvePath() .. "GemShopSetup.lua"
    local sString = "AM_SetShopProperty(\"Show\", \"VN-19's Gem Shop\", \"" .. sPath .. "\", \"Null\") "
	fnCutscene(sString)
	fnCutsceneBlocker()

--Talk.
elseif(sTopicName == "Talk") then

--Goodbye.
elseif(sTopicName == "Bye") then
	WD_SetProperty("Hide")
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
	fnCutscene([[ Append("VN-19:[E|Neutral] Until we meet again!") ]])
	
end
