-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
-- |[Objects]|
if(sObjectName == "TerminalA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (This terminal controls the screen outside.[P] It is currently in intermission mode.)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "RVD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (This RVD plays the same videograph as the movie outside.[P] Is it so the staff can watch, too?)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Oilmaker") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Fizzy Pop! flavour for oilmakers'.[P] Anyone who drinks that will probably be blown clear through the roof.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Sign") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
    fnCutscene([[ Append("55:[E|Neutral] The sign lists snacks available for purchase.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Neutral] Mostly stuff for organics.[P] What's the little yellow symbol?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Likely it indicates it is safe for Raijus to eat.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Neutral] I guess giving a Raiju some caffeine will probably cause an explosion.[P] Gotta be careful.") ]])
    
elseif(sObjectName == "Snacks") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "SX-399", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "HumanF1", "Neutral") ]])
    fnCutscene([[ Append("Human:[E|Neutral] May I get you anything, Command Unit?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] No thank you.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Happy] Oh, you should try some silcrete-toffee![B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Smirk] You see it in the mines every now and then, caused by subsurface pipes breaking and building up dissolved silcretes.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Smirk] Sprinkle some salt on it and it's the bee's knees![B][C]") ]])
    fnCutscene([[ Append("Human:[E|Neutral] I can't eat it because it's literally harder than my teeth, but the inorganic units say it's great![B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] I will pass.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Neutral] Please?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Will it make you happy?[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Happy] You bet![B][C]") ]])
    fnCutscene([[ Append("55:[E|Smirk] Very well...[B][C]") ]])
    fnCutscene([[ Append("55:[E|Smirk] *chomp*[B][C]") ]])
    fnCutscene([[ Append("Human:[E|Neutral] Do you like it?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Smirk] The interplay of hard and soft materials combined with the salt produces an enjoyable taste.[P] Thank you.[B][C]") ]])
    fnCutscene([[ Append("Human:[E|Neutral] Uhhh....[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Smirk] Trust me, buddy.[P] That's her way of saying she loves it.") ]])
    
elseif(sObjectName == "Speaker") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (These speakers play the videograph's audio with a detachable receiver.[P] You can also wire a golem's auditory receptors to receive it directly.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Screen") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (An enormous digital display on a special platform.[P] The videograph is currently in intermission and will resume soon.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Bathroom") then

    --Variables.
    local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")

    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
    fnCutscene([[ Append("55:[E|Neutral] Bathrooms, where organic units remove waste to be recycled.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Neutral] You know Steam Droids still do that, too?[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Neutral] Because of flaws in our lubrication, grease tends to build up inside the chassis.[P] It has to be flushed and dumped.[P] Smells nasty.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] You said 'our'.[P] Is that still the case for your new body?[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Smirk] I guess not, the auto-repair nanites do that work now.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Smirk] I suppose I never noticed I haven't had to go in so long.[P] You just forget about it.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Smirk] But I'm still a Steam Droid at heart, toutse.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Noted.") ]])
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
