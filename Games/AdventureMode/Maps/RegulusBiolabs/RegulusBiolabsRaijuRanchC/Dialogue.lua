-- |[Root]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Variables]|
local iChristineLeadingParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iChristineLeadingParty", "N")

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = "Null"
if(sTopicName == "Hello") then
    sActorName = TA_GetProperty("Name")
end

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    if(sActorName == "HumanA") then
        
    elseif(sActorName == "HumanB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Human:[VOICE|HumanM0] I hope I get assigned to the same sector as my tandem unit.[P] What if we don't?") ]])
        
    elseif(sActorName == "HumanC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Human:[VOICE|HumanM1] We've seen this movie a dozen times, but it never gets old.[P] It's like it's different every time I see it!") ]])
        
    elseif(sActorName == "HumanD") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Human:[VOICE|HumanF0] Having a rich girlfriend has a lot of perks![P] Let's go get some snacks before the videograph resumes!") ]])
        
    elseif(sActorName == "GolemA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|GolemLord] These videographs are always so silly, and where did they find these extras?[P] Their acting is abysmal.") ]])
        
    elseif(sActorName == "GolemB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|GolemLord] My tandem unit is two grades below me.[P] It's been very hard to find time to see her since I was converted, but I'm not leaving her behind me.[B][C]") ]])
        fnCutscene([[ Append("Golem:[VOICE|GolemLord] We'll have to get by until she graduates.[P] And then, we can be happy, forever.") ]])
        
    elseif(sActorName == "GolemSlaveA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] This is just the cushiest job ever, and I even get more work credits than most units.[P] All I have to do is pause and unpause the videographs!") ]])
        
    elseif(sActorName == "GolemSlaveB") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Do you think war will come to the Biolabs too?[P] It's so serene here, and all that seems so far away...") ]])
        
    elseif(sActorName == "GolemSlaveC") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Whatever happens, we'll make it through because we have each other.") ]])
        
    elseif(sActorName == "RaijuA") then
        
        --Christine:
        if(iChristineLeadingParty >= 1.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Raiju:[VOICE|GolemFancyD] Hello![P] Will you be joining us for the next section of the feature?[P] It'll start any moment now![BLOCK]") ]])

            --Decision script is this script. It must be surrounded by quotes.
            local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
            fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Sure!\", " .. sDecisionScript .. ", \"Yes\") ")
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"No Thanks\",  " .. sDecisionScript .. ", \"No\") ")
            fnCutsceneBlocker()
        
        --55:
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Raiju:[VOICE|GolemFancyD] Hello![P] Will you be joining us for the next section of the feature?[P] It'll start any moment now![B][C]") ]])
            fnCutscene([[ Append("55:[VOICE|Tiffany] We do not have time for such frivolities.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[VOICE|SX-399] Fifty platina says Christine would watch the movie.[B][C]") ]])
            fnCutscene([[ Append("55:[VOICE|Tiffany] ...[P] It is not a bet if the odds are one-hundred percent...") ]])
        
        
        end
        
    elseif(sActorName == "RaijuB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Raiju:[VOICE|GolemFancyC] Nothing gets me charged up quite like watching a moth girl in a bikini dance.[P] I could probably power this whole facility by myself when she gets going!") ]])
    end

-- |[ ==================================== Start The Movie ===================================== ]|
--Decision: Start the movie!
elseif(sTopicName == "Yes") then
	WD_SetProperty("Hide")

    --Variables.
    local iBiolabsFirstWatchMovie = VM_GetVar("Root/Variables/Chapter5/Scenes/iBiolabsFirstWatchMovie", "N")
    
    --Flags.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iBiolabsFirstWatchMovie", "N", 1.0)
    
    --Reset all movie variables.

    --Movement.
    fnCutsceneMove("Christine", 31.75, 35.50)
    fnCutsceneFace("Christine", 0, -1)
    fnCutsceneMove("Sophie", 32.75, 35.50)
    fnCutsceneFace("Sophie", 0, -1)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Have you ever watched a videograph outdoors like this, dearest?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] Oh, of course not![P] I could never afford a leisure pass if I worked a thousand years![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Offended] ...[P] And my robotic sixth-sense informs me they wouldn't sell me one even if I could...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] [EMOTION|Sophie|Neutral]Well we're watching one now![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Goodness, how long has it been since I was in one of those old drive-ins...[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Something from Earth?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] From an age gone by...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Oh![P] It's starting!") ]])
    fnCutsceneBlocker()
    
    --Reset movie variables.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iSawMovieIntro", "N", 0.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iEncounterA", "N", 0.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iEncounterB", "N", 0.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iEncounterC", "N", 0.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iMovieHasDoorCode", "N", 0.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iMovieHasSeenDoorScene", "N", 0.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iMovieTalkedToReceptionist", "N", 0.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iMovieOverheard", "N", 0.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iMovieUnlockSequence", "N", 0.0)
    
    fnCutsceneWait(65)
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    fnCutscene([[ AL_BeginTransitionTo("RegulusBiolabsMovieG", "FORCEPOS:15.0x12.0x0") ]])

--No movie.
elseif(sTopicName == "No") then
	WD_SetProperty("Hide")

end
