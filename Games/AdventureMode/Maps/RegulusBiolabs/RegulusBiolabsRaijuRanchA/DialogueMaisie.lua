-- |[Root]|
--Doctor Maisie's dialogue script.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then

    --Variables
    local iChristineLeadingParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iChristineLeadingParty", "N")
    local iRaibieQuest           = VM_GetVar("Root/Variables/Chapter5/Scenes/iRaibieQuest", "N")
    local sChristineForm         = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
    local iMaisieMetChristine    = VM_GetVar("Root/Variables/Chapter5/Scenes/iMaisieMetChristine", "N")
    local iMaisieMetTiffany      = VM_GetVar("Root/Variables/Chapter5/Scenes/iMaisieMetTiffany", "N")

    --Achievement.
    AM_SetPropertyJournal("Unlock Achievement", "MeetMosquito")

    --Common setup.
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Maisie", "Neutral") ]])

    -- |[ ===================================== Christine ====================================== ]|
    --Talking to Christine:
    if(iChristineLeadingParty ~= 0.0) then
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iMaisieMetChristine", "N", 1.0)
        
        --Display.
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
        
        --If the raibie quest is above 1.0 and Maisie has not met Christine, this plays instead.
        if(iRaibieQuest >= 1.0 and iMaisieMetChristine == 0.0) then
            fnCutscene([[ Append("Maisie:[E|Neutral] Ah, Christine.[P] Your associate mentioned you.[P] I am pleased to meet you.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] I know we have sings to discuss, but please let it be known your bravery is appreciated.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] You're taking a risk by doing the right thing, too, doc.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] Now to se matter at hand...[B][C]") ]])
        end
        
        --First time talking to the doc:
        if(iRaibieQuest == 0.0) then
            
            --Flag:
            VM_SetVar("Root/Variables/Chapter5/Scenes/iRaibieQuest", "N", 1.0)
            
            --Dialogue.
            fnCutscene([[ Append("Doctor:[E|Neutral] Ah ah, what is sis enchanting sing I see before me?[B][C]") ]])
            if(sChristineForm == "Human") then
                fnCutscene([[ Append("Doctor:[E|Neutral] Unit 771852 in her, shall we say, more delicious variation?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] I hope you don't talk to everyone you meet like this.[B][C]") ]])
                fnCutscene([[ Append("Doctor:[E|Neutral] Sis is merely my way of paying a compliment, my friend.[B][C]") ]])
                fnCutscene([[ Append("Doctor:[E|Neutral] You are doubtless wondering, as many do, whesser or not I must feast on se blood of se living to survive?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] I wasn't until you asked.[B][C]") ]])
                fnCutscene([[ Append("Doctor:[E|Neutral] Se answer is no.[B][C]") ]])
                fnCutscene([[ Append("Doctor:[E|Neutral] As wiss all of se subspecies I have examined, omnivorality is not lost during se partirhuman transformassion process.[B][C]") ]])
                fnCutscene([[ Append("Doctor:[E|Neutral] While stated preferences change, sese vary between individuals.[P] Se digestive capacity for meat and plant material is unchanged.[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Neutral] She's trying to say she doesn't want to suck your blood.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Oh, I picked that much up.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] You must deal with that accusation quite often.[B][C]") ]])
                fnCutscene([[ Append("Doctor:[E|Neutral] Oddly, more from golems san humans.[B][C]") ]])
            
            elseif(sChristineForm == "Raiju") then
                fnCutscene([[ Append("Doctor:[E|Neutral] Unit 771852 in her, shall we say, more electrical variation?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Flattering, sort of.[B][C]") ]])
                fnCutscene([[ Append("Doctor:[E|Neutral] You are doubtless wondering, as many do, whesser or not I must feast on se blood of se living to survive?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] I wasn't until you asked.[B][C]") ]])
                fnCutscene([[ Append("Doctor:[E|Neutral] Se answer is no.[B][C]") ]])
                fnCutscene([[ Append("Doctor:[E|Neutral] As wiss all of se subspecies I have examined, omnivorality is not lost during se partirhuman transformassion process.[B][C]") ]])
                fnCutscene([[ Append("Doctor:[E|Neutral] While stated preferences change, sese vary between individuals.[P] Se digestive capacity for meat and plant material is unchanged.[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Neutral] She's trying to say she doesn't want to suck your blood.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Oh, I picked that much up.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] You must deal with that accusation quite often.[B][C]") ]])
                fnCutscene([[ Append("Doctor:[E|Neutral] Oddly, more from golems san humans.[B][C]") ]])
            
            else
                fnCutscene([[ Append("Doctor:[E|Neutral] Unit 771852 in her mechanical variation?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] How are you aware of my unique abilities?[B][C]") ]])
                fnCutscene([[ Append("Doctor:[E|Neutral] I was paying you a compliment, my friend.[P] But if you must know...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] 2856?[B][C]") ]])
                fnCutscene([[ Append("Doctor:[E|Neutral] 2856.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] She talks a lot doesn't she?[B][C]") ]])
                fnCutscene([[ Append("Doctor:[E|Neutral] Endlessly and often loudly.[B][C]") ]])
            end
        
            --Resume.
            fnCutscene([[ Append("Doctor:[E|Neutral] But enough about that nonsense.[P] Introductions are in order, yes?[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] I am doctor Maisie de Havilland of An-Nador.[P] Yes, yes, if you must know, I [P]*did*[P] attend se University of Nador Nang.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] (I really should download a database on Pandemonium's geography one of these days.[P] For now, just smile and nod.)[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] I'm Unit 771852, and this is Unit 499323.[P] We go by Christine and Sophie.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Smirk] Hello, Dr. de Havilland.[B][C]") ]])
            if(sChristineForm == "Human") then
                fnCutscene([[ Append("Maisie:[E|Neutral] Oh, I am, [P]how do you say,[P] 'informed' about you.[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] In fact, se Head of Research herself has volunteered your servises.[B][C]") ]])
            else
                fnCutscene([[ Append("Maisie:[E|Neutral] So, to get to se business at hand:: Se Head of Research has volunteered me your services.[B][C]") ]])
            end
            fnCutscene([[ Append("Maisie:[E|Neutral] And since sis is not se first time she has done so, I will assume that she has not consulted you on sis volunteering.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] Not at all.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] Fursser, she did this only after multiple requests and with an undisguised display of disdain.[P] Serefore, I am to assume she did not mean it in se slightest.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] At least this means she's not being mean to only you, Christine.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] How reassuring.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] But, doctor, what do you need my help for?[P] I don't know anything about medicine.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] No, but you do know somesing about se art of fighting, yes?[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] We here in se Biolabs are currently dealing wiss a most curious passogen of unidentified providence, and it afflicts one partirhuman species alone.[P] Doubtless you have seen sem.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] The green Raijus?[P] We saw them on the way here.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] Yes.[P] Se staff here have taken to calling sem 'Raibies'.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] (That's so corny that only a real hack could have come up with it.)[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] I came up wiss se name, due to similarities between se frothing of a rabid mammal and se frothing on se Raijus, though I cannot be certain if sere is any genetic relation.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] So you need my help finding a cure?[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] Indeed.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] I'll help as best as I am able.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] Splendid.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Sad] Is it risky?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Don't worry, Sophie.[P] My inorganic forms will be totally immune from infection.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Sad] Will they be immune to getting clawed by a rabid Raiju?[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] My friend, nossing in sis life is wissout risk.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] ...[P] Just don't get hurt...[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] Now, Christine, what I require from you is rasser simple.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] Confront one of se Raibies specimens in se Gamma Laboraties and extract a tissue sample from her.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Will a tuft of hair do?[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] [SOUND|World|TakeItem]Sis sensor suite for your PDU will be able to perform se necessary work.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] Hair samples, skin samples, saliva samples...[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] Blood samples...[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] It should be no difficulty to acquire seese once you have subdued one of se specimens.[P] Bring sem to me as soon as you do.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] And you can make a cure?[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] No, I cannot guarantee sat.[P] At sis junction, I know literally nossing about se passogen.[P] Viral or bacterial, or a toxin? Attacking se brain, se kidneys, se toncils?[P] I know nossing.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] But I assure you sat I will do what I can.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] That's the best I can ask for.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Smirk] It was very nice to meet you, doctor.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] And miss Christine?[P] If you choose an organic form for sis task -[P] please do not add yourself to my list of patients, yes?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] You got it.") ]])
        
        --Repeats but hasn't gotten the samples
        elseif(iRaibieQuest == 1.0) then
        
            fnCutscene([[ Append("Maisie:[E|Neutral] Be aware sat what little we do know about se Raibies indicates increased strengss and speed.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] Doubtless seir electrical attacks will be even more potent san even se most excited Raiju.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] We'll be careful.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] Please return to me once you have se samples, sen.[P] I will make sure all my equipment is ready.") ]])
        
        --Got the samples.
        elseif(iRaibieQuest == 2.0) then
            
            --Flag:
            VM_SetVar("Root/Variables/Chapter5/Scenes/iRaibieQuest", "N", 3.0)
            
            --Dialogue:
            fnCutscene([[ Append("Maisie:[E|Neutral] Ah, se samples, excellent.[P] Your PDU already sent me a preview but se results were not conclusive.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] I will need a few moments...") ]])
            fnCutsceneBlocker()
        
            --Merge party.
            fnCutsceneWait(25)
            fnCutsceneMergeParty()
            fnCutsceneBlocker()
            
            --Movement.
            fnCutsceneMove("Christine", 56.25, 12.50)
            fnCutsceneFace("Christine", -1, 0)
            fnCutsceneMove("Sophie",    56.25, 13.50)
            fnCutsceneFace("Sophie",    -1, 0)
            fnCutsceneBlocker()
            
            --Door is not open:
            if(AL_GetProperty("Is Door Open", "MedicalDoor") == false) then
                fnCutsceneMove("DoctorMaisie", 55.25, 12.50)
                fnCutsceneFace("DoctorMaisie", 0, -1)
                fnCutsceneBlocker()
                fnCutsceneWait(15)
                fnCutsceneBlocker()
                fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
                fnCutscene([[ AL_SetProperty("Open Door", "MedicalDoor") ]])
                fnCutsceneBlocker()
                fnCutsceneWait(15)
                fnCutsceneBlocker()
            else
                fnCutsceneMove("DoctorMaisie", 55.25, 12.50)
            end
            fnCutsceneMove("DoctorMaisie", 55.25, 10.50)
            fnCutsceneMove("DoctorMaisie", 57.25, 10.50)
            fnCutsceneMove("DoctorMaisie", 57.25, 8.50)
            fnCutsceneMove("DoctorMaisie", 56.25, 8.50)
            fnCutsceneFace("DoctorMaisie", 0, -1)
            fnCutsceneBlocker()
            fnCutsceneWait(65)
            fnCutsceneBlocker()
            fnCutsceneMove("DoctorMaisie", 55.25, 8.50)
            fnCutsceneFace("DoctorMaisie", 0, -1)
            fnCutsceneBlocker()
            fnCutsceneWait(65)
            fnCutsceneBlocker()
            fnCutsceneMove("DoctorMaisie", 54.25, 8.50)
            fnCutsceneFace("DoctorMaisie", 0, -1)
            fnCutsceneBlocker()
            fnCutsceneWait(65)
            fnCutsceneBlocker()
            fnCutsceneMove("DoctorMaisie", 57.25, 8.50)
            fnCutsceneMove("DoctorMaisie", 57.25, 8.25)
            fnCutsceneFace("DoctorMaisie", 0, -1)
            fnCutsceneBlocker()
            fnCutsceneWait(65)
            fnCutsceneBlocker()
            fnCutsceneFace("DoctorMaisie", 0, 1)
            fnCutsceneWait(125)
            fnCutsceneBlocker()
            fnCutsceneMove("DoctorMaisie", 57.25, 10.50)
            fnCutsceneMove("DoctorMaisie", 55.25, 10.50)
            fnCutsceneMove("DoctorMaisie", 55.25, 12.50)
            fnCutsceneFace("DoctorMaisie", 1, 0)
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Maisie", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] Please forgive my medical jargon, but we are, [P]how you say,[P] 'boned'.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] That doesn't sound good.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] I have identified a likely passogen and already synssesized a likely cure.[P] At least, according to se usual principles of retroviral engineering.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] There's always a catch, isn't there?[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] I will try to keep sis as simple as possible.[P] Hmmm...[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] Se passogen is self-reactive.[P] It is capable of identifying sreats and adapting to it.[P] Sis is unlike anysing seen in nature.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] Indeed, such sings are impossible in nature, as se process of adaptation at sis scale requires evolutionary changes.[P] Yet, sis passogen is capable of it.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Haven't you seen the strange creatures in the labs?[P] I can tell you right now that they have a very different idea of possible.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] Yes, and it is no coincidence sat sey appeared at se same time as se Raibies became infected.[P] Still, proving se association was an important step.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] But can you cure the Raijus?[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] Sat is still possible, but I would need to know se original nature of se passogen.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] You see, sere were a dozen versions of it in se sample provided.[P] Sey had some common characteristics, but had already mutated semselves in response to se Raiju's immune system.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] I suspect sat sere is a 'prime' version of se passogen.[P] Finding sat would aid in se process of curing it immensely.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] But it's not a guarantee?[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Sad] Those poor Raijus...[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] Nosing is guaranteed in science.[P] We merely do what we can.[B][C]") ]])
            
            --Variables.
            local iRaibieFoundDrums = VM_GetVar("Root/Variables/Chapter5/Scenes/iRaibieFoundDrums", "N")
            if(iRaibieFoundDrums == 1.0) then
                VM_SetVar("Root/Variables/Chapter5/Scenes/iRaibieQuest", "N", 4.0)
                fnCutscene([[ Append("Christine:[E|Neutral] [EMOTION|Sophie|Neutral]Actually, Dr. de Havilland?[P] Could you take a look at this reading?[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] Substance E-v89-r.[P] Odorless, colourless, virtually untraceable.[P] Dissolves in water, half-life sree hours...[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] Where did you find sis?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] A raised section of the gamma laboratories, past the tissue testing center.[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] Yes, yes.[P] For, you see, sis material is not in se database.[P] At all.[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Offended] More top secret naughtiness from the Head of Research...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] I would guess so.[P] Doesn't surprise me she'd be researching something like this.[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] Hmmm...[P] Sere are similarities, to be sure.[P] Sis substance would likely cause se aggression we observe, but would have a short duration.[P] Meant to be used as a combat stimulant, I would suspect...[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] Furser, it would be metabolized quickly in any living species.[P] Why has it affected se Raijus alone?[P] Why has it become self-reactive?[P] Sese are serious questions.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Can we help answer them?[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Offended] We've got to help those poor Raijus![P] That big dumb meanie is using them as test subjects![B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] ...[P] Yes, yes we can help sem.[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] All I would need to do is observe an uninfected Raiju undergo infection, wiss my special monitoring nanites.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] Deliberately infect one of these Raijus?[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Offended] Doctor![B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] Oh no, of course not.[P] Not one of sem.[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] You, Christine.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Scared] Me![P] But I'm not - [P][CLEAR]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] ...[P] I get where you're coming from.[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Neutral] You want to turn her into a Raiju, and then have her infect herself?[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] Sat is correct, my friend.[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] I will be able to observe se infection as it evolves, and simultaneously extract se information from her immune system's respones.[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] I would hypossisize sat a large influx of antibodies produced in a cloning machine, injected at once, would overwhelm se passogen and eliminate it before it self-reacts.[P] But I would need se first-response antibodies from a fresh infection.[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] Even if I am incorrect, Christine can transform herself and immediately be cured.[P] So sere is little danger.[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Sad] ...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Sad] I know it sounds risky, Sophie...[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Sad] Christine, the transformation process involves...[B][C]") ]])
                if(sChristineForm == "Raiju") then
                    fnCutscene([[ Append("Christine:[E|Neutral] I think I'm aware, doc.[B][C]") ]])
                    fnCutscene([[ Append("Maisie:[E|Neutral] Se fact sat she is currently a raiju must be ignored for technical purposes.[P] Coding nonsense.[B][C]") ]])
                    fnCutscene([[ Append("Sophie:[E|Smirk] I was hoping we'd get to watch the scene again![B][C]") ]])
                    fnCutscene([[ Append("Maisie:[E|Neutral] Speak to me again when you are ready.[P] I will get my nanite cocktail ready.[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Blush] Sure thing, doc...") ]])
                else
                    fnCutscene([[ Append("Christine:[E|Neutral] Hmm?[B][C]") ]])
                    fnCutscene([[ Append("Sophie:[E|Sad] I'm okay with this on one condition.[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Smirk] Of course I'll be careful, Sophie.[P] 55 will - [P][CLEAR]") ]])
                    fnCutscene([[ Append("Sophie:[E|Blush] I get to watch and record it so I can watch it later![B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Blush] ...[P] Excuse me?[B][C]") ]])
                    fnCutscene([[ Append("Maisie:[E|Neutral] Se Raiju transformation process involves sex wiss a Raiju, 771852.[B][C]") ]])
                    fnCutscene([[ Append("Sophie:[E|Blush] Oh, oh![P] I'll need to get special discharge protectors -[P] and find a way to protect the cameras![B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Blush] Cameras?[P] Plural?[B][C]") ]])
                    fnCutscene([[ Append("Sophie:[E|Happy] I'll need to record it from different angles![P] Hee hee![B][C]") ]])
                    fnCutscene([[ Append("Maisie:[E|Neutral] It seems your tandem unit has made up her mind.[B][C]") ]])
                    fnCutscene([[ Append("Maisie:[E|Neutral] Speak to me again when you are ready.[P] I will get my nanite cocktail ready.[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Blush] Sure thing, doc...") ]])
                end
                fnCutsceneBlocker()
            
            else
                fnCutscene([[ Append("Christine:[E|Neutral] [EMOTION|Sophie|Neutral]Doctor, what if we could find the origin of the pathogen?[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] It's possible, if any of se source material is still present, sat I could work wiss it.[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] It would most likely be located somewhere in se gamma laboratories, since sat is where se Raibies are located.[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] I may be able to do somesing if you can find it, yes.[P] Assuming it exists...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] If it does, we'll find it.[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] Bring a scan from your PDU back for me if you do.[P] I will continue to work on sese samples...") ]])
                fnCutsceneBlocker()
            
            end
        
            --Movement.
            fnCutsceneMove("DoctorMaisie", 54.25, 12.50)
            fnCutsceneMove("Sophie", 56.25, 12.50)
            fnCutsceneBlocker()
            fnCutscene([[ AL_SetProperty("Fold Party") ]])
            fnCutsceneBlocker()
        
        --Got the samples, need to find the source of the pathogen.
        elseif(iRaibieQuest == 3.0) then
            fnCutscene([[ Append("Maisie:[E|Neutral] Any results on se source of se passogen?[B][C]") ]])
            
            --Variables.
            local iRaibieFoundDrums = VM_GetVar("Root/Variables/Chapter5/Scenes/iRaibieFoundDrums", "N")
            if(iRaibieFoundDrums == 1.0) then
                VM_SetVar("Root/Variables/Chapter5/Scenes/iRaibieQuest", "N", 4.0)
                fnCutscene([[ Append("Christine:[E|Neutral] Maybe.[P] Take a look at this stuff, doc.[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] Substance E-v89-r.[P] Odorless, colourless, virtually untraceable.[P] Dissolves in water, half-life sree hours...[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] Where did you find sis?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] A raised section of the gamma laboratories, past the tissue testing center.[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] Yes, yes.[P] For, you see, sis material is not in se database.[P] At all.[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Offended] More top secret naughtiness from the Head of Research...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] I would guess so.[P] Doesn't surprise me she'd be researching something like this.[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] Hmmm...[P] Sere are similarities, to be sure.[P] Sis substance would likely cause se aggression we observe, but would have a short duration.[P] Meant to be used as a combat stimulant, I would suspect...[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] Furser, it would be metabolized quickly in any living species.[P] Why has it affected se Raijus alone?[P] Why has it become self-reactive?[P] Sese are serious questions.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Can we help answer them?[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Offended] We've got to help those poor Raijus![P] That big dumb meanie is using them as guinea pigs![B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] ...[P] Yes, yes we can help sem.[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] All I would need to do is observe an uninfected Raiju undergo infection, wiss my special monitoring nanites.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] Deliberately infect one of these Raijus?[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Offended] Doctor![B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] Oh no, of course not.[P] Not one of sem.[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] You, Christine.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Scared] Me![P] But I'm not - [P][CLEAR]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] ...[P] I get where you're coming from.[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Neutral] You want to turn her into a Raiju, and then have her infect herself?[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] Sat is correct, my friend.[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] I will be able to observe se infection as it evolves, and simultaneously extract se information from her immune system's respones.[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] I would hypossisize sat a large influx of antibodies produced in a cloning machine, injected at once, would overwhelm se passogen and eliminate it before it self-reacts.[P] But I would need se first-response antibodies from a fresh infection.[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] Even if I am incorrect, Christine can transform herself and immediately be cured.[P] So sere is little danger.[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Sad] ...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Sad] I know it sounds risky, Sophie...[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Sad] Christine, the transformation process involves...[B][C]") ]])
                if(sChristineForm == "Raiju") then
                    fnCutscene([[ Append("Christine:[E|Neutral] I think I'm aware, doc.[B][C]") ]])
                    fnCutscene([[ Append("Maisie:[E|Neutral] Se fact sat she is currently a raiju must be ignored for technical purposes.[P] Coding nonsense.[B][C]") ]])
                    fnCutscene([[ Append("Sophie:[E|Smirk] I was hoping we'd get to watch the scene again![B][C]") ]])
                    fnCutscene([[ Append("Maisie:[E|Neutral] Speak to me again when you are ready.[P] I will get my nanite cocktail ready.[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Blush] Sure thing, doc...") ]])
                else
                    fnCutscene([[ Append("Christine:[E|Neutral] Hmm?[B][C]") ]])
                    fnCutscene([[ Append("Sophie:[E|Sad] I'm okay with this on one condition.[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Smirk] Of course I'll be careful, Sophie.[P] 55 will - [P][CLEAR]") ]])
                    fnCutscene([[ Append("Sophie:[E|Blush] I get to watch and record it so I can watch it later![B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Blush] ...[P] Excuse me?[B][C]") ]])
                    fnCutscene([[ Append("Maisie:[E|Neutral] Se Raiju transformation process involves sex wiss a Raiju, 771852.[B][C]") ]])
                    fnCutscene([[ Append("Sophie:[E|Blush] Oh, oh![P] I'll need to get special discharge protectors -[P] and find a way to protect the cameras![B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Blush] Cameras?[P] Plural?[B][C]") ]])
                    fnCutscene([[ Append("Sophie:[E|Happy] I'll need to record it from different angles![P] Hee hee![B][C]") ]])
                    fnCutscene([[ Append("Maisie:[E|Neutral] It seems your tandem unit has made up her mind.[B][C]") ]])
                    fnCutscene([[ Append("Maisie:[E|Neutral] Speak to me again when you are ready.[P] I will get my nanite cocktail ready.[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Blush] Sure thing, doc...") ]])
                end
                fnCutsceneBlocker()
            else
                fnCutscene([[ Append("Christine:[E|Neutral] Not yet, doc.[P] We're looking.[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] It will likely be somewhere in se gamma laboratories, likely in a storage medium.[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] Hmm...[P] Perhaps a leaking container?[P] Search carefully!") ]])
            end
            
        --Raiju TF time!
        elseif(iRaibieQuest == 4.0) then
            fnCutscene([[ Append("Maisie:[E|Neutral] Ready to become a Raiju for se Cause of Science, 771852?[BLOCK]") ]])

            --Decision script is this script. It must be surrounded by quotes.
            local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
            fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"RaijuTF\") ")
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Not Yet\",  " .. sDecisionScript .. ", \"No\") ")
            fnCutsceneBlocker()
            
        --Doctor Maisie tells you what to do.
        elseif(iRaibieQuest == 5.0) then
            VM_SetVar("Root/Variables/Chapter5/Scenes/iRaibieQuest", "N", 6.0)
            fnCutscene([[ Append("Christine:[E|Smirk] Okay doc, ready to dose me?[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] I have se mutator serum right here.[P] However, we do not have anywhere near enough E-v89-r.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] Which is good, because having you undergo se transformation in my office would likely destroy most of my furniture, and quite possibly se ranch itself.[P] Raibies exhibit vassly increased strengss and agility, and to pair sat wiss your combat skills...[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] [SOUND|World|TakeItem]So, take sis serum, but do not use it here.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] But don't you need to observe me?[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] Se serum also has a set of my observational nanites.[P] Sey will transmit se data to Unit 2855's PDU, who will relay it to me.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] She has already downloaded se necessary protocols.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] Also, she says you are foolish for undertaking sis risk.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] That's her![B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] You will need to return to whatever location you found se E-v89-r substance at, and dose yourself wis sat.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] So the western part of the Gamma Laboratories?[P] Shouldn't be a problem.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] Perhaps, perhaps not.[P] But, when se changes begin, you will need to transform yourself before you are overcome.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] And, unfortunately, we do not know se sreshold.[P] Do not take a risk.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] ...[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] ...[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] Are your instructions clear?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] As crystal.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] Christine, if you do sis, you will be saving se lives of se infected Raiju population here.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] For even trying, you are a hero.[P] Do not forget sat.[P] Be brave, not stupid.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] ...[P] Brave is what I do best, doc.") ]])
            
        --Doctor Maisie tells you what to do.
        elseif(iRaibieQuest == 6.0) then
            fnCutscene([[ Append("Maisie:[E|Neutral] Return to se location where you found se E-v89-r, and give yourself a good dose of sat material.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] Se data will be transmitted directly to me.[P] After sat, well, I must busy myself finding a cure.[P] Good luck, Christine.") ]])
        
        --After Raibies.
        else
            fnCutscene([[ Append("Maisie:[E|Neutral] Christine, what did you experience while infected?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] A total loss of self-control, and extreme anger.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] Every little thing 55 did ticked me off so much that I just had to crush her...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] I had to focus and think hard to do anything other than that.[P] If my attention wandered I would just start slashing at the walls.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] Interesting, very interesting.[P] I hope se oser Raijus in se labs will not attack us.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Them attacking you is them doing their best.[P] Try confusing them, that will buy you a few seconds.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] Oh, I will not be doing sis dangerous work![P] Se security units will.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] I would do it myself, but I have...[P] misplaced...[P] my tranquiliser gun.[P] If I had it I would be able to administer se serum easily.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] Oh well.[P] Beanbag rounds will suffice to stun sem, I hope.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Good luck, doctor.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Smirk] You've been very helpful.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] Stay safe, my friend.") ]])
        
        end
        
    -- |[ ========================================= 55 ========================================= ]|
    --Talking to 55:
    else
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iMaisieMetTiffany", "N", 1.0)
        
        --Display.
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Tiffany", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "SX-399", "Neutral") ]])
        
        --If the raibie quest is above 1.0 and Maisie has not met Christine, this plays instead.
        if(iRaibieQuest >= 1.0 and iMaisieMetTiffany == 0.0) then
            fnCutscene([[ Append("Maisie:[E|Neutral] Ah, Unit 2855.[P] Your associate mentioned you.[P] I am pleased to meet you.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] I know we have sings to discuss, but please let it be known your bravery is appreciated.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Thank you, doctor.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] Now to se matter at hand...[B][C]") ]])
        end
        
        --First time talking to the doc:
        if(iRaibieQuest == 0.0) then
            
            --Flag:
            VM_SetVar("Root/Variables/Chapter5/Scenes/iRaibieQuest", "N", 1.0)
            
            --Dialogue.
            fnCutscene([[ Append("Doctor:[E|Neutral] Oh, hello Unit 285 -[P] erm, 5. Unit 2855.[B][C]") ]])
            fnCutscene([[ Append("Doctor:[E|Neutral] Apologies, your sister was just here.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] There is no need to apologize.[P] Our appearances are similar.[P] I assure you, our personalities are not.[B][C]") ]])
            fnCutscene([[ Append("Doctor:[E|Neutral] Indeed![P] Already I see a crucial difference.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] What is that difference?[B][C]") ]])
            fnCutscene([[ Append("Doctor:[E|Neutral] It is a masser of decibels.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Smirk] I understand.[B][C]") ]])
            fnCutscene([[ Append("Doctor:[E|Neutral] Fursser, you are engaging wiss me and not simply barking orders, sen leaving.[B][C]") ]])
            fnCutscene([[ Append("Doctor:[E|Neutral] Perchance, is sis 'Christine' nearby?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Yes.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] She's covering the entrance.[B][C]") ]])
            fnCutscene([[ Append("Doctor:[E|Neutral] I see.[P] Your sister volunteered her services to me.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Without consulting her?[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] Not surprising at all.[B][C]") ]])
            fnCutscene([[ Append("Doctor:[E|Neutral] It is not see first time she has done sat.[P] Neverseless, I have need of your aid.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] I am doctor Maisie de Havilland of An-Nador.[P] University of Nador Nang, top of my class.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] So the golems abducted you to work as their doctor?[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] Ah ha![P] Abducted?[P] No, sey contracted me.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] Se medical advances I have made here will save countless lives planetside.[P] Se equipment is wissout peer, even if I do not understand how all of it works.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] Already I have definitively proved se hyposesized existence of microbes causing many afflictions and even begun to synsesize vaccines -[P] it has been an incredible few years![B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] But nevermind sat.[P] You and your team are capable fighters, and sat is what se Raiju Ranch is in need of.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Please inform us how we can help.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Flirt] We always help those in need![B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] [EMOTION|SX-399|Smirk]Sen let me lay it out as best as I can.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] We here in se biolabs are currently dealing wiss a most curious passogen of unidentified providence, and it afflicts one partirhuman species alone.[P] Doubtless you have seen sem.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] You are referring to the sickly green raijus.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] Yes.[P] Se staff here have taken to calling sem 'Raibies'.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Smirk] That seems like a name Christine would come up with.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] Actually, it was I who came up wiss it.[P] It is due to similarities between se frossing of a rabid mammal and se frossing on se raijus, sough I cannot be certain if sere is any genetic relation.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Do you need us to exterminate them?[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] No.[P] I am a doctor, not a butcher.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Sad] 55![B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Apologies, but what little I know of medical procedures involve quarantining dangerous specimens to prevent the pathogen's spread.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Wiping out the infected population would save the raijus here at the ranch.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] While grim, Unit 2855 is correct, my dear.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Sad] But can't we save them?[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] Sat is what I intend to do.[P] Only in se absolutely worst-case scenario would liquidation be called for.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Sad] ...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] I will only undertake that task if the doctor explicitly orders it, SX-399.[P] If we cannot save the infected, we must save the uninfected.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] I get you.[P] I hate how often you're right about these things.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Down] I do not take pleasure in it.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] [EMOTION|Tiffany|Neutral]So, we must find a cure.[P] And to find one, I require information.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] Sus far, I know nossing about se disease.[P] A passogen, a genetic trigger, viral or fungal?[P] Nossing.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] Confront one of se Raibies specimens in se Gamma Laboraties and extract a tissue sample from her.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] Tuft of fur?[P] Saliva?[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] [SOUND|World|TakeItem]Sis sensor suite for your PDU will be able to perform se necessary work.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] Hair samples, skin samples, saliva samples...[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] Blood samples...[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] It should be no difficulty to acquire seese once you have subdued one of se specimens.[P] Bring sem to me as soon as you do.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Smirk] And you can make a cure from that sample?[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] No, I cannot guarantee sat.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] But I assure you sat I will do what I can.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] We will do what we can as well.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Smirk] This will be a piece of cake with Dr. de Havilland on our team.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] There.[P] I have sent a mail to Christine's PDU informing her of your requirements.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] She is ultimately the one who decides what we do.[P] I leave the tactical choices to her.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] I cannot wait to meet her.[P] Now, I must prepare my equipment.[P] Good luck.") ]])
        
        --Repeats but hasn't gotten the samples
        elseif(iRaibieQuest == 1.0) then
        
            fnCutscene([[ Append("Maisie:[E|Neutral] Be aware sat what little we do know about se Raibies indicates increased strengss and speed.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] Doubtless seir electrical attacks will be even more potent san even se most excited Raiju.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] We will be ready for them.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] Please return to me once you have se samples, sen.[P] I will make sure all my equipment is ready.") ]])
        
        --Got the samples.
        elseif(iRaibieQuest == 2.0) then
            
            --Flag:
            VM_SetVar("Root/Variables/Chapter5/Scenes/iRaibieQuest", "N", 3.0)
            
            --Dialogue:
            fnCutscene([[ Append("Maisie:[E|Neutral] Ah, se samples, excellent.[P] Your PDU already sent me a preview but se results were not conclusive.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] I will need a few moments...") ]])
            fnCutsceneBlocker()
        
            --Merge party.
            fnCutsceneWait(25)
            fnCutsceneMergeParty()
            fnCutsceneBlocker()
            
            --Movement.
            fnCutsceneMove("Tiffany", 56.25, 12.50)
            fnCutsceneFace("Tiffany", -1, 0)
            fnCutsceneMove("SX-399",    56.25, 13.50)
            fnCutsceneFace("SX-399",    -1, 0)
            fnCutsceneBlocker()
            
            --Door is not open:
            if(AL_GetProperty("Is Door Open", "MedicalDoor") == false) then
                fnCutsceneMove("DoctorMaisie", 55.25, 12.50)
                fnCutsceneFace("DoctorMaisie", 0, -1)
                fnCutsceneBlocker()
                fnCutsceneWait(15)
                fnCutsceneBlocker()
                fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
                fnCutscene([[ AL_SetProperty("Open Door", "MedicalDoor") ]])
                fnCutsceneBlocker()
                fnCutsceneWait(15)
                fnCutsceneBlocker()
            else
                fnCutsceneMove("DoctorMaisie", 55.25, 12.50)
            end
            fnCutsceneMove("DoctorMaisie", 55.25, 10.50)
            fnCutsceneMove("DoctorMaisie", 57.25, 10.50)
            fnCutsceneMove("DoctorMaisie", 57.25, 8.50)
            fnCutsceneMove("DoctorMaisie", 56.25, 8.50)
            fnCutsceneFace("DoctorMaisie", 0, -1)
            fnCutsceneBlocker()
            fnCutsceneWait(65)
            fnCutsceneBlocker()
            fnCutsceneMove("DoctorMaisie", 55.25, 8.50)
            fnCutsceneFace("DoctorMaisie", 0, -1)
            fnCutsceneBlocker()
            fnCutsceneWait(65)
            fnCutsceneBlocker()
            fnCutsceneMove("DoctorMaisie", 54.25, 8.50)
            fnCutsceneFace("DoctorMaisie", 0, -1)
            fnCutsceneBlocker()
            fnCutsceneWait(65)
            fnCutsceneBlocker()
            fnCutsceneMove("DoctorMaisie", 57.25, 8.50)
            fnCutsceneMove("DoctorMaisie", 57.25, 8.25)
            fnCutsceneFace("DoctorMaisie", 0, -1)
            fnCutsceneBlocker()
            fnCutsceneWait(65)
            fnCutsceneBlocker()
            fnCutsceneFace("DoctorMaisie", 0, 1)
            fnCutsceneWait(125)
            fnCutsceneBlocker()
            fnCutsceneMove("DoctorMaisie", 57.25, 10.50)
            fnCutsceneMove("DoctorMaisie", 55.25, 10.50)
            fnCutsceneMove("DoctorMaisie", 55.25, 12.50)
            fnCutsceneFace("DoctorMaisie", 1, 0)
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Maisie", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Tiffany", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "SX-399", "Neutral") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] Please forgive my medical jargon, but we are, [P]how you say,[P] 'boned'.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] 'Boned' is probably not a synonym of 'good'.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] I have identified a likely passogen and already synssesized a likely cure.[P] At least, according to se usual principles of retroviral engineering.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Bear in mind we have no medical knowledge.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] I will try to keep sis as simple as possible.[P] Hmmm...[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] Se passogen is self-reactive.[P] It is capable of identifying sreats and adapting to sem.[P] Sis is unlike anysing seen in nature.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] Indeed, such sings are impossible in nature, as se process of adaptation at sis scale requires evolutionary changes.[P] Yet, sis passogen is capable of it.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] The creatures invading the labs possess many such impossibilities.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] Yes, and it is no coincidence sat sey appeared at se same time as se Raibies became infected.[P] Still, proving se association was an important step.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] So you made a cure, but it doesn't work?[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] It does.[P] Unfortunately, se passogen adapts and se cure becomes useless.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] Can you make a cure that, I dunno, bypasses that?[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] Sat is still possible, but I would need to know se original nature of se passogen.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] You see, sere were a dozen versions of it in se sample provided.[P] Sey had some common characteristics, but had already mutated semselves in response to se Raiju's immune system, and fursser in response to se cure.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] I suspect sat sere is a 'prime' version of se passogen.[P] Finding sat would aid in se process of curing it immensely.[B][C]") ]])
            
            --Variables.
            local iRaibieFoundDrums = VM_GetVar("Root/Variables/Chapter5/Scenes/iRaibieFoundDrums", "N")
            if(iRaibieFoundDrums == 1.0) then
                VM_SetVar("Root/Variables/Chapter5/Scenes/iRaibieQuest", "N", 4.0)
                fnCutscene([[ Append("55:[E|Neutral] I believe this substance we located in the Gamma Labs may be related, then.[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] Substance E-v89-r.[P] Odorless, colourless, virtually untraceable.[P] Dissolves in water, half-life sree hours...[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] Where did you find sis?[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] A raised section of the gamma laboratories, past the tissue testing center.[P] There were visible leaching marks in the nearby soil.[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] Yes, yes.[P] For, you see, sis material is not in se database.[P] At all.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] Or it is classified.[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] Likely.[P] If classified it would not appear on my database queries.[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] Hmmm...[P] Sere are similarities, to be sure.[P] Sis substance would likely cause se aggression we observe, but would have a short duration.[P] Meant to be used as a combat stimulant, I would suspect...[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] Furser, it would be metabolized quickly in any living species.[P] Why has it affected se raijus alone?[P] Why has it become self-reactive?[P] Sese are serious questions.[B][C]") ]])
                fnCutscene([[ Append("SX-399:[E|Neutral] Maybe they were using the raijus as test subjects.[P] Do you think we can cure them, still?[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] ...[P] Yes, yes we can help sem.[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] All I would need to do is observe an uninfected raiju undergo infection, wiss my special monitoring nanites.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] Would you like us to deliberately infect a raiju for observation?[B][C]") ]])
                fnCutscene([[ Append("SX-399:[E|Neutral] 55![B][C]") ]])
                fnCutscene([[ Append("SX-399:[E|Smirk] Wait, no.[P] You've got something up your sleeve, don't you.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Smirk] Correct.[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] Christine.[B][C]") ]])
                fnCutscene([[ Append("SX-399:[E|Flirt] She goes raiju, infects herself, and then goes golem.[P] Bam, she's an immune robot, you get your data.[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] Sat is correct, my friend.[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] I will be able to observe se infection as it evolves, and simultaneously extract se information from her immune system's respones.[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] I would hypossisize sat a large influx of antibodies produced in a cloning machine, injected at once, would overwhelm se passogen and eliminate it before it self-reacts.[P] But I would need se first-response antibodies from a fresh infection.[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] Even if I am incorrect, Christine can transform herself and immediately be cured.[P] So sere is little danger.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] There is attendant risk if she cannot control herself, though.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] It will be at her discretion to undertake that risk.[P] I will not speak for her.[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] Please send her a message and have her meet me here if she is willing.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] I have.[P] She has said she will discuss is with her tandem unit.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] ...[P] Which seems odd.[B][C]") ]])
                fnCutscene([[ Append("SX-399:[E|Flirt] Becoming a raiju means banging one.[B][C]") ]])
                fnCutscene([[ Append("SX-399:[E|Smirk] I can see why Sophie might object to that.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] Is fornication with another unit not allowed?[B][C]") ]])
                fnCutscene([[ Append("SX-399:[E|Smirk] ...[P] Implicitly yes.[P] If you ask and she says it's okay, you're okay.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] I see.[P] I was not aware.[B][C]") ]])
                fnCutscene([[ Append("SX-399:[E|Neutral] (Wow, she really means that...)[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] I will prepare se nanites used to observe se infection.[P] Good day, ladies.") ]])
                fnCutsceneBlocker()
            
            else
                fnCutscene([[ Append("55:[E|Neutral] And if we can find the source of the pathogen?[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] It's possible, if any of se source material is still present, sat I could work wiss it.[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] It would most likely be located somewhere in se gamma laboratories, since sat is where se raibies are located.[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] I may be able to do somesing if you can find it, yes.[P] Assuming it exists...[B][C]") ]])
                fnCutscene([[ Append("55:[E|Smirk] Then we will find it.[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] Bring a scan from your PDU back for me if you do.[P] I will continue to work on sese samples...") ]])
                fnCutsceneBlocker()
            
            end
        
            --Movement.
            fnCutsceneMove("DoctorMaisie", 54.25, 12.50)
            fnCutsceneMove("SX-399", 56.25, 12.50)
            fnCutsceneBlocker()
            fnCutscene([[ AL_SetProperty("Fold Party") ]])
            fnCutsceneBlocker()
        
        --Got the samples, need to find the source of the pathogen.
        elseif(iRaibieQuest == 3.0) then
            fnCutscene([[ Append("Maisie:[E|Neutral] Any results on se source of se passogen?[B][C]") ]])
            
            --Variables.
            local iRaibieFoundDrums = VM_GetVar("Root/Variables/Chapter5/Scenes/iRaibieFoundDrums", "N")
            if(iRaibieFoundDrums == 1.0) then
                VM_SetVar("Root/Variables/Chapter5/Scenes/iRaibieQuest", "N", 4.0)
                fnCutscene([[ Append("55:[E|Neutral] I believe this substance we located in the Gamma Labs may be related.[P] Please check it.[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] Substance E-v89-r.[P] Odorless, colourless, virtually untraceable.[P] Dissolves in water, half-life sree hours...[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] Where did you find sis?[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] A raised section of the gamma laboratories, past the tissue testing center.[P] There were visible leaching marks in the nearby soil.[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] Yes, yes.[P] For, you see, sis material is not in se database.[P] At all.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] Or it is classified.[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] Likely.[P] If classified it would not appear on my database queries.[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] Hmmm...[P] Sere are similarities, to be sure.[P] Sis substance would likely cause se aggression we observe, but would have a short duration.[P] Meant to be used as a combat stimulant, I would suspect...[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] Furser, it would be metabolized quickly in any living species.[P] Why has it affected se raijus alone?[P] Why has it become self-reactive?[P] Sese are serious questions.[B][C]") ]])
                fnCutscene([[ Append("SX-399:[E|Neutral] Maybe they were using the raijus as test subjects.[P] Do you think we can cure them, still?[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] ...[P] Yes, yes we can help sem.[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] All I would need to do is observe an uninfected raiju undergo infection, wiss my special monitoring nanites.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] Would you like us to deliberately infect a raiju for observation?[B][C]") ]])
                fnCutscene([[ Append("SX-399:[E|Neutral] 55![B][C]") ]])
                fnCutscene([[ Append("SX-399:[E|Smirk] Wait, no.[P] You've got something up your sleeve, don't you.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Smirk] Correct.[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] Christine.[B][C]") ]])
                fnCutscene([[ Append("SX-399:[E|Flirt] She goes raiju, infects herself, and then goes golem.[P] Bam, she's an immune robot, you get your data.[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] Sat is correct, my friend.[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] I will be able to observe se infection as it evolves, and simultaneously extract se information from her immune system's respones.[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] I would hypossisize sat a large influx of antibodies produced in a cloning machine, injected at once, would overwhelm se passogen and eliminate it before it self-reacts.[P] But I would need se first-response antibodies from a fresh infection.[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] Even if I am incorrect, Christine can transform herself and immediately be cured.[P] So sere is little danger.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] There is attendant risk if she cannot control herself, though.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] It will be at her discretion to undertake that risk.[P] I will not speak for her.[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] Please send her a message and have her meet me here if she is willing.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] I have.[P] She has said she will discuss is with her tandem unit.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] ...[P] Which seems odd.[B][C]") ]])
                fnCutscene([[ Append("SX-399:[E|Flirt] Becoming a raiju means banging one.[B][C]") ]])
                fnCutscene([[ Append("SX-399:[E|Smirk] I can see why Sophie might object to that.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] Is fornication with another unit not allowed?[B][C]") ]])
                fnCutscene([[ Append("SX-399:[E|Smirk] ...[P] Implicitly yes.[P] If you ask and she says it's okay, you're okay.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] I see.[P] I was not aware.[B][C]") ]])
                fnCutscene([[ Append("SX-399:[E|Neutral] (Wow, she really means that...)[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] I will prepare se nanites used to observe se infection.[P] Good day, ladies.") ]])
            else
                fnCutscene([[ Append("55:[E|Neutral] Not yet, doctor.[P] We're looking.[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] It will likely be somewhere in se gamma laboratories, likely in a storage medium.[B][C]") ]])
                fnCutscene([[ Append("Maisie:[E|Neutral] Hmm...[P] Perhaps a leaking container?[P] Search carefully!") ]])
            end
            
        --Raiju TF time!
        elseif(iRaibieQuest == 4.0) then
            fnCutscene([[ Append("Maisie:[E|Neutral] Please inform Unit 771852 to meet me here when she is ready to undergo se transformation.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] I have found a raiju volunteer for her.[P] Before you ask, no, it was not hard.") ]])
            
        --Doctor Maisie tells you what to do.
        elseif(iRaibieQuest == 5.0) then
            VM_SetVar("Root/Variables/Chapter5/Scenes/iRaibieQuest", "N", 6.0)
            fnCutscene([[ Append("55:[E|Neutral] What is the next step, doctor?[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] I have se mutator serum right here.[P] However, we do not have anywhere near enough E-v89-r.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] Which is good, because having Christine undergo se transformation in my office would likely destroy most of my furniture, and quite possibly se ranch itself.[P] Raibies exhibit vassly increased strengss and agility, and to pair sat wiss her combat skills...[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] [SOUND|World|TakeItem]So, take sis serum, but do not use it here.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Understood.[P] Will this compromise the observation?[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] Se serum also has a set of my observational nanites.[P] Sey will transmit se data to your PDU, who will relay it to me.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] Se network is spotty but someone repaired se power issues in se gamma labs, so we should be able to maintain a connection.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] Please download se protocols, Unit 2855.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Smirk] Already done, doctor.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] You will need to return to whatever location you found se E-v89-r substance at, and dose Christine wis sat.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Smirk] Not a problem.[P] It was in the gamma labs past the tissue research center.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] When se infection takes, she must transform herself when her immune system has reacted.[P] It is dangerous.[P] She must transform, quickly.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Yes.[P] We will make sure she knows.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] Unit 2855 and SX-399, sank you.[P] If you do sis, you will be saving se lives of se infected raiju population here.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] For even trying, you are a hero.[P] Do not forget sat.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Smirk] Being heroes is what we do.[P] Let's go, 55.") ]])
            
        --Doctor Maisie tells you what to do.
        elseif(iRaibieQuest == 6.0) then
            fnCutscene([[ Append("Maisie:[E|Neutral] Return to se location where you found se E-v89-r, and give Christine a good dose of sat material.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] Se data will be transmitted directly to me.[P] After sat, well, I must busy myself finding a cure.[P] Good luck, my friends.") ]])
        
        --After Raibies.
        else
            fnCutscene([[ Append("Maisie:[E|Neutral] Unit 2855, may I ask why you have saved se raijus here from sere fate?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] I did not.[P] I merely supported Christine and SX-399 while they did.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Smirk] She's so cute when she's humble.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Blush] Th-[P]thank you.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] From what I understand, sough, you would not have done sis before.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] What has changed you from se Head of Security to sis helpful, caring maverick?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Total memory loss.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] ...[P] Oh.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Smirk] I wiped my memories to prevent a suspected computer virus from spreading.[P] Ironic, isn't it?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] After that, I struggled to regain them.[P] When I saw who the old me was, I realized I did not want them back.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Christine has told me that I need something to believe in.[P] I have chosen to believe in the freedom from the systems that controlled me.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] The raibies were used as test subjects by an uncaring administration.[P] Tools to be used and discarded.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] In every way, that was both me, and those I once suppressed.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] Even as an enforcer of se administration, you were its victim?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Yes.[P] Not as much a victim as the others, but still one.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] I must come to terms with that.[P] I must make amends for what I did.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Smirk] To help others free themselves from that control is one way to do that.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] Sis has given me cause to sink.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] I suspect my role will be to return to Pandemonium and take my medical knowledge wiss me.[B][C]") ]])
            fnCutscene([[ Append("Maisie:[E|Neutral] I am now doubting se Administration cares what happens to se people of Pandemonium.[P] Has my research been used to create sis E-v89-r?[P] Just how innocent am I?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] What matters is what you do now that you know, not what you did while you were lied to.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] I believe in you.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] Some of those are things Christine has said before, 55.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Flirt] But not all of them.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] [P]I am learning.") ]])
        
        end

    end

--Raiju TF time!
elseif(sTopicName == "RaijuTF") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iRaibieQuest", "N", 5.0)
	
	--Reboot the dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Maisie", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
	fnCutscene([[ Append("Maisie:[E|Neutral] Very well.[P] I have already found several Raijus who will happily induct you into sere ranks.[B][C]") ]])
	fnCutscene([[ Append("Sophie:[E|Blush] Oh but I think a threesome would blow out the PDUs![B][C]") ]])
	fnCutscene([[ Append("Sophie:[E|Blush] (Speaking of, I'll need to get those set up with rubber blockers...)[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Blush] Using the PDU in-built cameras?[B][C]") ]])
	fnCutscene([[ Append("Sophie:[E|Blush] Definition won't be as high as I'd like, but where are we going to find high-quality videograph cameras at a time like this?[P] Hee hee![B][C]") ]])
	fnCutscene([[ Append("Maisie:[E|Neutral] Please, Christine.[P] Right sis way...") ]])
    fnCutsceneBlocker()
    
    --Raiju TF scene:
    LM_ExecuteScript(gsRoot .. "Chapter 5/Scenes/400 Volunteer/Raiju Volunteer/Scene_Begin.lua")

--No thanks.
elseif(sTopicName == "No") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")

    --Common setup.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Maisie", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Maisie:[E|Neutral] Very well. Speak to me again when you are prepared.") ]])
    fnCutsceneBlocker()

end
