-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
-- |[Objects]|
if(sObjectName == "Sign") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
    fnCutscene([[ Append("55:[E|Neutral] 'Swimming is prohibited for non-Raijus while the Raijus are being bathed.[P] No exceptions.'[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Happy] Ah hahahah![B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] What do you find humorous?[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Smirk] Imagine a Raiju.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Smirk] Now imagine a wet Raiju with a soaked droopy tail, shivering and rubbing her arms.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Sad.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Flirt] Adorable![B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Suit yourself.") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "SignDirections") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] 'North - Outdoor Videograph Theater'.[P] 'Some Moths Do!' is scribbled next to the message in a marker.[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] 'East - Clinic, Human Dormitories, Athletics Tracks, Pools, Classrooms'.[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] 'West - Raiju Dormitories and Voltaic Capture Chambers'.[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] 'South - Chow Hall, Exit'.") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
    fnCutscene([[ Append("55:[E|Neutral] There's a log of all the Raijus here, as well as a voltage number next to their name.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Neutral] The golems can suck the fun out of anything.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Neutral] Somehow, making love is now a chore and you're expected to compete with each other?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Is making love a fun activity?[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Flirt] ...[P] Oh, I have such things to show you...") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
    fnCutscene([[ Append("55:[E|Neutral] A rubber-insulated console.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Neutral] The better to protect its systems from sudden discharge.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] A rather crude method.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Neutral] Effective, though.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Would you rather encase me in rubber to protect me from electrical attacks?[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Blush] Well the first half of what you said...") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
    fnCutscene([[ Append("55:[E|Neutral] 'Today's special is tomato bisque, spinach dip and crackers, and berry-blast milkshakes.'[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Smirk] I could really go for some spinach![B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Can your power core accommodate organic foods now?[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Smirk] Totally![P] It's pretty killer.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Smirk] Whenever we captured organic rations, I got to keep whatever didn't go to the humans who live in Steamston.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] And?[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Neutral] I really like the cauliflowers and rhubarbs.[P] And celery.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Odd.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Neutral] You?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] I do not eat food to produce energy.[P] Accessing a recharging station is much more efficient.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Neutral] Somehow I knew the answer before I asked...") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
    fnCutscene([[ Append("55:[E|Neutral] Patient check-up listings.[P] This looks like the doctor's schedule.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Neutral] Any interesting patterns?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] There are periods where she leaves the ranch and is not officially accounted for.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Neutral] Those are called 'days off', 55.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Interesting.[P] Why not work constantly?[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Neutral] It might take a while to explain...") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalE") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
    fnCutscene([[ Append("55:[E|Neutral] The terminal has videographs of darkmatters doing things.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Smirk] Official research videos, of course.[B][C]") ]])
    
    --Variables
    local iHasDarkmatterForm = VM_GetVar("Root/Variables/Global/Christine/iHasDarkmatterForm", "N")
    local iDarkmattersBad    = VM_GetVar("Root/Variables/Chapter5/Scenes/iDarkmattersBad", "N")
    
    --Skipped the Darkmatter confrontation.
    if(iDarkmattersBad == 0.0) then
        fnCutscene([[ Append("55:[E|Neutral] Do you think the darkmatters could be allied with?[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Neutral] You see them in the mines sometimes.[P] They're tough and they certainly don't like the creepy crawlies.[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Neutral] Worst case scenario is they go back to being aloof and mysterious when we save the world.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] A good assessment.[P] Thank you.") ]])
    
    --Darkmatter confrontation happened.
    else
        fnCutscene([[ Append("55:[E|Neutral] Do you think Christine was incorrect in her assessment of the darkmatters?[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Neutral] Why are you asking me?[P] You've seen more of them than I have.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] They are emotional creatures.[P] You likely understand them better.[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Neutral] If that's the case, then...[P] I think they can be friends.[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Neutral] Maybe when we save the day, they'll see that they were wrong and apologize?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Optimistic.[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Smirk] Thanks, I try.") ]])
    end
    
elseif(sObjectName == "TerminalF") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
    fnCutscene([[ Append("55:[E|Neutral] 'I'm stuck in here because I scraped my knee.[P] Sorry, I can't go play tennis with you until Doc discharges me.'[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Neutral] Just pop it out and put a door-joint in.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Humans cannot replace their parts like that.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Neutral] Oh, they can, they're probably just real squeamish about it.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Smirk] All those fluids and bits sloshing around.[P] Keeping it all inside you must be a constant worry.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Correct.[P] Humans tend to like to keep their fluids inside them.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Smirk] They're very predictable in that way.") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalG") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
    fnCutscene([[ Append("55:[E|Neutral] A computer set to play music softly as the patient slept.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Neutral] Death metal or speed metal?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Classical.[P] Recall that the patients here are organic.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Neutral] Laaaaaaaame.") ]])
    
elseif(sObjectName == "TerminalH") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
    fnCutscene([[ Append("55:[E|Neutral] Patient records indicating the same unit, 749332, is three-fourths of the patients assigned to this room.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Neutral] Sounds like a security unit waiting to happen.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Down] ...[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Sad] Wait![P] I didn't mean that![B][C]") ]])
    fnCutscene([[ Append("55:[E|Down] Was I like this prior to my conversion?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] I reviewed my records but the early history of Regulus City is not accounted for.[P] I do not know what my organic self was like.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Neutral] Was anyone else around during that time?[P] Could you ask them?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Unit 2856.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Neutral] So no.[P] Nobody we could ask.") ]])
    
elseif(sObjectName == "TerminalI") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
    fnCutscene([[ Append("55:[E|Neutral] A log left by Doctor Maisie.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] [VOICE|Maisie]'Sis is yet anosser instance of my contract being knowingly violated by se administrators.[P] You are machines, yes?[P] Are you not aware of se stipulations?'[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] [VOICE|Maisie]'Se first violassion is the request, to deliberately infect sis boy vith a flu virus, and sen to transfuse his blood to his twin brosser.[P] Are you insane?'[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] [VOICE|Maisie]'I was also asked to amputate the limbs from one individual, and to attach prossetics.[P] While research into prossetics is a worssy goal, it cannot be done in such a gross violazion of medical essics.[P] Find an existing amputee, do not create sem!'[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] [VOICE|Maisie]'If you wish to discuzz sis wiss me, you may find me in my office.[P] Make an appointment wiss my assistant, I have real experiments to conduct.'[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Smirk] She seems like a pretty good doctor.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Smirk] Christine once told me:: Judge a unit by the quality of her enemies.") ]])
    
elseif(sObjectName == "BoxesA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
    fnCutscene([[ Append("55:[E|Neutral] A box full of toys.[P] Dolls, miniature fabrication benches, and a little PDU.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Neutral] Yeah, they hang onto those and give them to the next generation once the old one graduates.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Neutral] Sometimes we find discarded toys that got stuck in the trash disposal chutes.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] What do you do with them?[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Neutral] There are no children Steam Droids...[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Smirk] But we hang onto them anyway.[P] Because sometimes a breeding program human escapes.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Does that happen often?[P] It is not recorded in the official statistics.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Neutral] Not really, I think it's only happened once or twice.[P] But we hold out hope...") ]])
    
elseif(sObjectName == "BoxesB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
    fnCutscene([[ Append("55:[E|Neutral] A discarded copy of Mr. Needlemouse is in this box.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Neutral] The sidescrolling Needlemouse games were way better than the 3D ones.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Neutral] Some units insist that Needlemouse Adventure was good.[P] I am not one of those units.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] I have no opinion.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Flirt] You say that now, but once you see Needlemouse '06...") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BoxesC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Spare medicines and medical tools.[P] Disposable gloves, tubing, syringes.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookshelfA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
    fnCutscene([[ Append("55:[E|Neutral] 'Position Recommendations'...[P] A book dedicated to sexual positions for organics to use.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Blush] Wheeee...[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Did I upset you?[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Blush] Nope![P] Not upset at all![P] But you're very direct, aren't you?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] I am.[P] Shall we move on?") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookshelfB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
    fnCutscene([[ Append("55:[E|Neutral] 'Doggos and Pupperinos'.[P] A picture book with dogs and young dogs.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Smirk] They're called 'puppies' when they're young.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Incorrect.[P] They are called 'pupperinos'.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Smirk] (Don't correct her[P] don't correct her[P] don't correct her...)[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Flirt] Oh you're right.[P] My mistake.") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookshelfC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
    fnCutscene([[ Append("55:[E|Neutral] 'Arcane Superconductivity::[P] A Theory'[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Smirk] You're looking at a living example of arcane superconductivity![B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Smirk] Guess the book is a little out of date.[P] Which is what you'd expect from books.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] I assume so.[P] I am not educated on the higher sciences except where necessary to operate my equipment.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Smirk] In that case, you need to read up on arcane superconductivity, matter decompression, and subspace communication.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] For what purpose?[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Blush] ...[P] Operating my equipment...") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookshelfD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
    fnCutscene([[ Append("55:[E|Neutral] 'How To Get Her Right In The Soft Tissues, Including the Clitoris'[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Blush] ...[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] (SX-399 isn't saying anything.[P] I may have said something embarrassing.[P] Best to move on.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "CoffeeMaker") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (An oilmaker modified to grind coffee beans.[P] Tends to cause organics to act irreverently.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BarrelYellow") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (This barrel smells strongly of alcohol.[P] Has no effect on non-organic systems, though is a useful cleaner.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BarrelBlue") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (This barrel is full of water.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "RVD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The RVD is displaying the foods organics can order. There are no meat dishes.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Kitchen") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A full kitchen set, which is a real rarity on Regulus. It was likely made in the Raiju Ranch's fabricators.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Lunchbox") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A lunchbox with a thermos full of potato soup.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Computer") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Medical records.[P] The login is for the doctor's assistant.[P] Apparently, the doctor isn't very good with computers, and needs someone to keep her records in order.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Medicine") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (All kinds of organic medicines and disinfectants.[P] Common aspirin, pseudophedrine, penoxycyline, morphine, palimerfin...)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Cooler") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A portable cooler to keep drinks cold.[P] It appears the organics favour lemonade.)") ]])
    fnCutsceneBlocker()
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
