-- |[Root]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Variables]|
local iChristineLeadingParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iChristineLeadingParty", "N")

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = "None"
if(sTopicName ~= "YesChristine" and sTopicName ~= "Yes55" and sTopicName ~= "NoCancel") then
    sActorName = TA_GetProperty("Name")
end

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    -- |[Lord Golems]|
    if(sActorName == "GolemA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        if(iChristineLeadingParty >= 1.0) then
            fnCutscene([[ Append("Primrose:[VOICE|GolemLord] Don't cause any problems for me or my staff, and we'll get along just fine.") ]])
        else
            fnCutscene([[ Append("Primrose:[VOICE|GolemLord] I've got my optical receptors trained on you, Unit 2855.") ]])
        end
    
    elseif(sActorName == "GolemB") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|GolemLord] Did you see the creatures at the gala?[P] I've heard many rumours, but no facts...") ]])
    
    elseif(sActorName == "GolemC") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|GolemLord] There was an explosion, and I just -[P] I just ran.[P] Oh my goodness was I scared!") ]])
    
    elseif(sActorName == "GolemD") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|GolemLord] I haven't had occasion to visit the biolabs before.[P] I must say I appreciate the color scheme.") ]])
    
    elseif(sActorName == "GolemE") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|GolemLord] Entertainment passes to the labs?[P] Oh yes, they cost quite a few work credits.[P] Only the high ranking or frugal lords visit often.") ]])
    
    elseif(sActorName == "GolemF") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|GolemLord] What are we going to do?[P] Oh oh oh, it's all so hopeless...") ]])
    
    -- |[Humans]|
    elseif(sActorName == "HumanA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Human:[VOICE|HumanF0] Welcome to the Raiju Ranch, I suppose.[P] Our accommodations are limited, but we make the best of it.") ]])
        
    elseif(sActorName == "HumanB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Human:[VOICE|HumanF1] I'm keeping an eye on this area of the perimeter.[P] We don't have the numbers for any sort of scouting.") ]])
        
    elseif(sActorName == "HumanC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Human:[VOICE|HumanF1] Do you have any idea what happened at the gala?[P] A lot of frightened units have been showing up, and I don't know if we have space for all of them.") ]])
        
    elseif(sActorName == "Assistant") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        if(iChristineLeadingParty >= 1.0) then
            fnCutscene([[ Append("Human:[VOICE|HumanF0] I am doctor Maisie's assistant.[P] If you have any injuries, please let me know.[B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] She has an organic assistant?[B][C]") ]])
            fnCutscene([[ Append("Human:[VOICE|HumanF0] Of course![P] I hope to have her job someday![P] Best way to learn is by experience, you know.") ]])
        else
            fnCutscene([[ Append("Human:[VOICE|HumanF0] I am doctor Maisie's assistant.[P] If you have any injuries, please let me know.[B][C]") ]])
            fnCutscene([[ Append("55:[VOICE|Tiffany] It is unlikely that a medical worker could do much for an inorganic unit.[B][C]") ]])
            fnCutscene([[ Append("Human:[VOICE|HumanF0] Hey I've dabbled in repairs, too.[P] A lot is quite similar if you just be generous with the auto-repair nanites.") ]])
        end
    
    -- |[Raijus]|
    elseif(sActorName == "RaijuA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Raiju:[VOICE|HumanF0] Argh, my bed is a total mess![P] Every time I straighten it out, the static electricity causes it to curl!") ]])
        
    elseif(sActorName == "RaijuB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Raiju:[VOICE|HumanF1] Everyone is up and the lights are on despite the late hour.[P] I'm so tired, I just want to go to bed.[P] What's going on?") ]])
        
    elseif(sActorName == "RaijuC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Raiju:[VOICE|HumanF0] *pant*[P] *pant*[P] I'm just -[P] charging up for our next session.[P] Phew!") ]])
        
    elseif(sActorName == "RaijuD") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Raiju:[VOICE|HumanF1] Hey there, cuties![P] Want to join us?[P] We're just warming up for round six!") ]])
        
    elseif(sActorName == "RaijuE") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Raiju:[VOICE|HumanF0] They provide us reading material in case we need inspiration.[P] Honestly?[P] All I need is a hug and I'm good to go!") ]])
        
    elseif(sActorName == "RaijuF") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Raiju:[VOICE|HumanF1] I kinda set fire to a power conduit because I got too excited, so now I've been assigned to cleaning up dishes after chow time.") ]])
        
    elseif(sActorName == "RaijuG") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        WD_SetProperty("Unlock Topic", "Biolabs_Games", 1)
        if(iChristineLeadingParty >= 1.0) then
            fnCutscene([[ Append("Raiju:[VOICE|HumanF0] *tap tap*[P] Oh, sorry.[P] I'm playing a video game -[P] I just need special gloves or the console will get fried.[B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] What game are you playing?[B][C]") ]])
            fnCutscene([[ Append("Raiju:[VOICE|HumanF0] It's called 'Unit 2209:: Silent Infiltrator'.[P] You have to sneak into places, find high-ranking people, and convert them into golems without getting caught.[B][C]") ]])
            fnCutscene([[ Append("Raiju:[VOICE|HumanF0] It's way too easy, though.[P] They give you the spaghetti sauce right at the start of the game.[B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] The what now?[B][C]") ]])
            fnCutscene([[ Append("Raiju:[VOICE|HumanF0] *Spaghetti Punch!*[P] *Spaghetti Throw!*[P] See, so easy![P] Mission complete.") ]])
        else
            fnCutscene([[ Append("Raiju:[VOICE|HumanF0] *tap tap*[P] Oh, sorry.[P] I'm playing a video game -[P] I just need special gloves or the console will get fried.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[VOICE|SX-399] Oh, what game?[B][C]") ]])
            fnCutscene([[ Append("Raiju:[VOICE|HumanF0] It's called 'Unit 2209:: Silent Infiltrator'.[P] You have to sneak into places, find high-ranking people, and convert them into golems without getting caught.[B][C]") ]])
            fnCutscene([[ Append("55:[VOICE|Tiffany] A training program, likely.[B][C]") ]])
            fnCutscene([[ Append("Raiju:[VOICE|HumanF0] It's way too easy, though.[P] They give you the spaghetti sauce right at the start of the game.[B][C]") ]])
            fnCutscene([[ Append("Raiju:[VOICE|HumanF0] *Spaghetti Punch!*[P] *Spaghetti Throw!*[P] See, so easy![P] Mission complete.") ]])
        end
        
    elseif(sActorName == "RaijuH") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Raiju:[VOICE|HumanF1] A lot of really well dressed golems have been coming through here.[P] You think any of them want to snuggle?") ]])
        
    elseif(sActorName == "RaijuI") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Raiju:[VOICE|HumanF0] I heard something was wrong with the other Raijus.[P] Maybe they didn't get enough cuddles?[P] Can I help?[P] I'm good at cuddling!") ]])
    
    -- |[Slave Golems]|
    elseif(sActorName == "GolemSlaveA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] We normally allow units to watch the sunrise if they want to, but otherwise they'd be in bed by now.[P] What's going on out there?") ]])
        
    elseif(sActorName == "GolemSlaveB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] I've been working in this sector for a few years now, but I still don't understand 'injections'.[P] Why don't you just use the existing infiltration ports?") ]])
        
    elseif(sActorName == "GolemSlaveC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        if(iChristineLeadingParty >= 1.0) then
            fnCutscene([[ Append("Golem:[VOICE|Golem] We haven't had any serious injuries yet, but we haven't heard from Biological Services.[P] I hope they're okay.[B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] Are all the organics accounted for?[B][C]") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] The humans are, but there's a lot of Raijus missing, and we can't get a lock on their tracking collars...") ]])
        else
            fnCutscene([[ Append("Golem:[VOICE|Golem] We haven't had any serious injuries yet, but we haven't heard from Biological Services.[P] I hope they're okay.[B][C]") ]])
            fnCutscene([[ Append("55:[VOICE|Tiffany] Can you re-establish communications with their barracks?[B][C]") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] We'd have to run a line out there, and we don't have any spare security units.[P] So, until the network is fixed, no.[P] I hope they're okay...") ]])
        end
    elseif(sActorName == "GolemSlaveD") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        if(iChristineLeadingParty >= 1.0) then
            fnCutscene([[ Append("Golem:[VOICE|Golem] You know, 'breakfast' actually tastes pretty good, even by organic standards.[B][C]") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] We get fresh eggs, fruits, wheatflour -[P] and even ice cream![B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] Ice cream?[P] What do you milk?[B][C]") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] There are dromedaries in the dry habitats, and sheep in the gamma labs.[P] Plus, sometimes we have enough human milk if they're exceptionally productive. It's all great![B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] But not Raiju milk?[B][C]") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Oh, oh my goodness no.[P] The only thing that comes out when you squeeze them is lightning bolts!") ]])
        else
            fnCutscene([[ Append("Golem:[VOICE|Golem] You know, 'breakfast' actually tastes pretty good, even by organic standards.[B][C]") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] We get fresh eggs, fruits, wheatflour -[P] and even ice cream![B][C]") ]])
            fnCutscene([[ Append("55:[VOICE|Tiffany] Does your Lord Golem know you consume organic rations?[B][C]") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] What she doesn't know can't hurt her, right?") ]])
        end
        
    elseif(sActorName == "GolemSlaveE") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Steel?[P] Can't we make carbonweave cookware?[B][C]") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] I have to lubricate the pans or everything sticks![P] It's like being in the stone age!") ]])
        
    elseif(sActorName == "GolemSlaveF") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] I'm sorry, the kitchen isn't currently serving anything.[P] We can provide some pre-made snacks if you like.") ]])
        
    elseif(sActorName == "GolemSlaveG") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] They just go through this stuff like candy...") ]])
        
    elseif(sActorName == "GolemSlaveH") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] It'll be all right.[P] Everything will be okay.[P] We'll make it through this.") ]])
        
    
    -- |[Doctor Maisie]|
    elseif(sActorName == "DoctorMaisie") then
        LM_ExecuteScript(fnResolvePath() .. "DialogueMaisie.lua", "Hello")
    
    -- |[Party Members]|
    elseif(sActorName == "Tiffany") then
    
        --SX-399 is not a party member.
        local iChristineLeadingParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iChristineLeadingParty", "N")
        if(iChristineLeadingParty == 1.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
            fnCutscene([[ Append("55:[E|Neutral] I will accompany you when you exit the ranch.[P] Otherwise, enjoy yourselves.") ]])
            fnCutsceneBlocker()
            
        --SX-399 is a party member. Allow swapping.
        elseif(iChristineLeadingParty == 2.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "SX-399", "Neutral") ]])
            fnCutscene([[ Append("55:[E|Neutral] Want to switch investigators?[BLOCK]") ]])

            --Decision script is this script. It must be surrounded by quotes.
            local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
            fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"YesChristine\") ")
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"NoCancel\") ")
            fnCutsceneBlocker()
        end

    elseif(sActorName == "SX-399") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
        fnCutscene([[ Append("SX-399:[E|Smirk] I'll keep an eye on her, don't you worry.[P] I won't let her scamper off!") ]])
        fnCutsceneBlocker()
            
    elseif(sActorName == "Christine") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "SX-399", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Tiffany", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Sophie", "Neutral") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Want to switch investigators?[BLOCK]") ]])

        --Decision script is this script. It must be surrounded by quotes.
        local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
        fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"Yes55\") ")
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"NoCancel\") ")
        fnCutsceneBlocker()
        
    elseif(sActorName == "Sophie") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "SX-399", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Tiffany", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] Do behave, you two.[P] I'd hate to get kicked out of such a nice place because you melted something.[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Neutral] What do you mean, melted something?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] What do you mean, nice place?[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Happy] Aahhh![P] Good one![B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] (I must have accidentally made a joke...)[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] Thank you.") ]])
        fnCutsceneBlocker()
    
    end
    
-- |[Decision Handlers]|
--Switch to 55/SX-399 as investigators.
elseif(sTopicName == "YesChristine") then
	WD_SetProperty("Hide")
    
    --Disable collision flags for 55 and SX-399.
    EM_PushEntity("Tiffany")
        local i55ID = RE_GetID()
        TA_SetProperty("Clipping Flag", false)
        TA_SetProperty("Activation Script", "Null")
    DL_PopActiveObject()
    EM_PushEntity("SX-399")
        local iSX399ID = RE_GetID()
        TA_SetProperty("Clipping Flag", false)
        TA_SetProperty("Activation Script", "Null")
    DL_PopActiveObject()
    
    --Set follower values.
    AL_SetProperty("Unfollow Actor Name", "Sophie")
    AL_SetProperty("Player Actor ID", i55ID)
    AL_SetProperty("Follow Actor ID", iSX399ID)
    giFollowersTotal = 1
    gsaFollowerNames = {"SX-399"}
    giaFollowerIDs   = {iSX399ID}
    
    --Order Christine and Sophie to move over to the fence.
    fnCutsceneMove("Christine", 46.25, 35.50)
    fnCutsceneFace("Christine", 0, 1)
    fnCutsceneMove("Sophie", 47.25, 35.50)
    fnCutsceneFace("Sophie", 0, 1)
    fnCutsceneMove("Tiffany", 51.25, 35.50)
    fnCutsceneFace("Tiffany", 0, 1)
    fnCutsceneMove("SX-399", 51.25, 35.50)
    fnCutsceneFace("SX-399", 0, 1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Fold.
    fnAutoFoldParty()
    fnCutsceneBlocker()
            
    --Set collision flags and dialogue flags for the departing characters.
    local sString = "LM_ExecuteScript(\"" .. fnResolvePath() .. "SetChristineProperties.lua\")"
    fnCutscene(sString)
    sString = "LM_ExecuteScript(\"" .. fnResolvePath() .. "SetSophieProperties.lua\")"
    fnCutscene(sString)
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iChristineLeadingParty", "N", 0.0)
    
    --Set leader voice.
    gsPartyLeaderName = "Tiffany"
    WD_SetProperty("Set Leader Voice", "Tiffany")

--Switch to Christine/Sophie as investigators.
elseif(sTopicName == "Yes55") then
	WD_SetProperty("Hide")
    
    --Disable collision flags for Christine and Sophie.
    EM_PushEntity("Christine")
        local iChristineID = RE_GetID()
        TA_SetProperty("Clipping Flag", false)
        TA_SetProperty("Activation Script", "Null")
    DL_PopActiveObject()
    EM_PushEntity("Sophie")
        local iSophieID = RE_GetID()
        TA_SetProperty("Clipping Flag", false)
        TA_SetProperty("Activation Script", "Null")
    DL_PopActiveObject()
    
    --Set follower values.
    AL_SetProperty("Unfollow Actor Name", "SX-399")
    AL_SetProperty("Player Actor ID", iChristineID)
    AL_SetProperty("Follow Actor ID", iSophieID)
    giFollowersTotal = 1
    gsaFollowerNames = {"Sophie"}
    giaFollowerIDs   = {iSophieID}
    
    --Order 55 and SX399 to move over to the fence.
    fnCutsceneMove("Tiffany", 46.25, 35.50)
    fnCutsceneFace("Tiffany", 0, 1)
    fnCutsceneMove("SX-399", 47.25, 35.50)
    fnCutsceneFace("SX-399", 0, 1)
    fnCutsceneMove("Christine", 51.25, 35.50)
    fnCutsceneFace("Christine", 0, 1)
    fnCutsceneMove("Sophie", 51.25, 35.50)
    fnCutsceneFace("Sophie", 0, 1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Fold.
    fnAutoFoldParty()
    fnCutsceneBlocker()
            
    --Set collision flags and dialogue flags for the departing characters.
    local sString = "LM_ExecuteScript(\"" .. fnResolvePath() .. "Set55Properties.lua\")"
    fnCutscene(sString)
    sString = "LM_ExecuteScript(\"" .. fnResolvePath() .. "SetSX399Properties.lua\")"
    fnCutscene(sString)
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iChristineLeadingParty", "N", 2.0)
    
    --Set leader voice.
    gsPartyLeaderName = "Christine"
    WD_SetProperty("Set Leader Voice", "Christine")

--Backing out of dialogue.
elseif(sTopicName == "NoCancel") then
	WD_SetProperty("Hide")


end
