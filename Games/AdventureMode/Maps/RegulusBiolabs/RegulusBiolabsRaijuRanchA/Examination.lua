-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[55 Investigation]|
--If 55 is investigating, then run a different script.
local iChristineLeadingParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iChristineLeadingParty", "N")
if(iChristineLeadingParty == 0.0) then
    LM_ExecuteScript(fnResolvePath() .. "Examination55.lua", sObjectName)
    return
end

-- |[Exits]|
-- |[Objects]|
if(sObjectName == "Sign") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] 'Swimming is prohibited for non-raijus while the raijus are being bathed.[P] No exceptions.'[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] When you see a warning like that, you just know some lugnut was stupid enough to try it.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Swimming while a raiju is being bathed?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Sad] Oh no...[P] what kind of voltage would they be capable of?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] All I will note is that there are no fish or amphibians assigned to this body of water.[P] That's all the answer you need.") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "SignDirections") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] 'North - Outdoor Videograph Theater'.[P] 'Some Moths Do!' is scribbled next to the message in red marker.[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] 'East - Clinic, Human Dormitories, Athletics Tracks, Pools, Classrooms'.[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] 'West - Raiju Dormitories and Voltaic Capture Chambers'.[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] 'South - Chow Hall, Exit'.") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] There's a log of all the raijus here, as well as a voltage number next to their name.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] They must be tracking how much power they produce.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Sad] Can you imagine being under pressure to...[P] discharge...[P] as much as you can?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Sad] Wouldn't that take the fun out of it?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] I guess you just try not to think about it.[P] You'll do better if it comes naturally.") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] The console is covered in rubber insulation.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] I wonder if someone was dumb enough to hook it into the voltaic capturing line.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] The what now?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] There's a line that goes under this whole area that absorbs electromagnetic fields and converts them into electricity.[P] I don't know how it works, but I know it's there.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] The line comes closest to the surface under these buildings since, well, you can guess why.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Sad] And someone tried hooking the terminal's power supply to that?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] Not only would that blow the capacitor out, it'd probably blow a hole in the wall.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] But it'd be a lot of fun to watch -[P] from a safe distance![P] Hee hee!") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] 'Today's special is tomato bisque, spinach dip and crackers, and berry-blast milkshakes.'[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Gotta get that nutriment to keep productive, right?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] As long as an organic is well-fed, they won't complain.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Is that, uhh, a saying from Earth?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Laugh] It's lacking a certain something, I'd say![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] Oooh![P] 'An empty stomach is filled with complaints'![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Laugh] Top shelf, my dear![P] Well done!") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Should we be peeking on the good doctor's computer?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] I don't see why not.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Look, it's mostly patient check-ups.[P] A surgery here and there.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] You know, I always thought of us as being part of the city's industrial base.[P] But when you think about it, we're a lot closer to doctors than engineers.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] There's not as sharp a distinction on Regulus City.[P] But she's also a research doctor, isn't she?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Yeah, I don't think I have head for the life of a researcher.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] You're really smart, Christine![P] Don't sell yourself short![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Thanks, Sophie, but when all this is over, we'll just go back to the repair bay.[P] That's the best place for us.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] It's best to find joy in the little things, isn't it?") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalE") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] I guess this is the break room.[P] What's on the terminal?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] A videograph of a darkmatter playing with a string.[B][C]") ]])
    
    --Variables
    local iHasDarkmatterForm = VM_GetVar("Root/Variables/Global/Christine/iHasDarkmatterForm", "N")
    local iDarkmattersBad    = VM_GetVar("Root/Variables/Chapter5/Scenes/iDarkmattersBad", "N")
    
    --Skipped the Darkmatter confrontation.
    if(iDarkmattersBad == 0.0) then
        fnCutscene([[ Append("Sophie:[E|Neutral] The Darkmatters are friends, right?[B][C]") ]])
        
        --Christine does not have Darkmatter form.
        if(iHasDarkmatterForm == 0.0) then
            fnCutscene([[ Append("Christine:[E|Smirk] I've seen them up close.[P] They are not friends with Vivify and her ilk.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Maybe they just don't know how to help, or maybe they are just too busy.[P] Either way, I think we can trust them.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] I hope so, but nobody really knows.[P] They're everywhere, but we know nothing about them.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] They were at the LRT facility, watching.[P] Maybe they're just observing for now...") ]])
    
        --Has Darkmatter form.
        else
            fnCutscene([[ Append("Christine:[E|Smirk] They are.[P] You can trust them.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] It's hard for them to communicate normally, but they're doing what they can to help.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] Are they risking their lives?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Absolutely.[P] They look cute on the videographs, but they are capable warriors.[P] I have no fear if they are covering my back.") ]])
        end
    
    --Darkmatter confrontation happened.
    else
        fnCutscene([[ Append("Sophie:[E|Sad] I hope the Darkmatters can be reasoned with...[B][C]") ]])
        
        --Christine does not have Darkmatter form.
        if(iHasDarkmatterForm == 0.0) then
            fnCutscene([[ Append("Christine:[E|Smirk] They may not be our direct allies, but they still aren't friends of Vivify.[P] They're just being cautious.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] In the same circumstances, maybe I'd even agree with them...[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Offended] Don't say that.[P] You're -[P] you had better win![B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Offended] You go give that meanie a good whooping or you'll have me to deal with![B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Laugh] Okay, okay![P] I'll go save the universe![B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] But if anyone asks, it's only because you said so.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] (She jokes, but I'm still worried...)") ]])
    
        --Has Darkmatter form.
        else
            fnCutscene([[ Append("Christine:[E|Smirk] I know I'll be able to reason with them.[P] They're not so different from us.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] They just see it differently.[P] I'm a current ally, but a potential enemy.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Offended] Don't say that.[P] You don't get to lose un -[P] unless I say so![B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Offended] So if that big meanie does that singing thing again, you -[P] slap -[P] slap her![P] Yeah![B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Offended] (Slap her?[P] I am the biggest wuss in the galaxy...)[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Laugh] Woah, Sophie![P] Slapping her?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] But I get what you're saying.[P] I'll talk to the Darkmatters.[P] I know we can work together again.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] (But...[P] I'm still worried...)") ]])
        end
    
    
    end
    
elseif(sObjectName == "TerminalF") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] 'I'm stuck in here because I scraped my knee.[P] Sorry, I can't go play tennis with you until Doc discharges me.'[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] A scraped knee?[P] Humans are so delicate.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Hmm...[P] Is that all there is to it?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] There's another file here.[P] It's treatment recommendations.[P] Including blood sample extraction.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] Sixteen blood sample extractions...[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] Sixteen?[P] Are you implying that our good doctor is anything but the most professional medical expert?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] I'm not implying anything at all.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Though now that I think about it, I was once afraid of needles.[P] You know, those little things they use to inject fluids into organics?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] I think something in my conversion changed that.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] Can you reprogram someone not to be afraid of something?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Sad] I...[P] think so...[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] You don't look too happy about that.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Sad] If you can remove my fears with reprogramming, could you add them?[P] How else could you change me?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] I don't think it's something that would be permanent.[P] If a fear is due to a memory, removing that memory might help, but the fear remains.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] I think fears are part of who we are.[P] If you were scared of heights, it would re-emerge later even if it was reprogrammed out.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] I think you're just not scared of needles anymore because you're a golem.[P] Golems aren't scared of needles![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Yeah, that must be it.[P] We've already seen how some silly program couldn't change who I really am.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Thanks, Sophie.") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalG") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] This computer was set to play music as the patient slept.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] What kind?[P] Industrial?[P] Synthwave?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Classical.[P] Synthesized sheet music from Pandemonium.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] I'm not familiar with any of the composers planetside, though.[P] Where I'm from, we had Vivaldi and Beethoven and - [P][CLEAR]") ]])
    fnCutscene([[ Append("Christine:[E|Sad] ...[P] and I suddenly feel just a little homesick...[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Sad] I'm sorry...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] It's all right.[P] I've heard The Four Seasons so many times, I had it memorized.[P] I could easily recreate some of the sheet music.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] Such a travesty that the people of this world haven't heard Earth's finest composers.[P] This must be rectified at once![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Please tell me the sole purpose of our struggle for liberty is your desire to spread the arts.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] If all else fails, yes.[P] Music belongs to everyone.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] That is how we know we are in the right.") ]])
    
elseif(sObjectName == "TerminalH") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Patient records...[P] Nothing too interesting here.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Nothing too interesting?[P] Excuse me, are you all right?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Look at who the most common patient in this room is.[P] Tell me if you notice anything.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Hmmm...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Unit 749332 has been in this room repeatedly.[P] She's three-quarters of the patient data.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Looks like she spends a day at a time here.[P] Scraps and scuffs.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] I bet she gets herself hurt doing athletics.[P] Humans need to exercise their muscles or they break down.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] Ahhh...[P] No, Sophie.[P] I don't think so.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] The girl who is always going to the medical room?[P] She's either sick constantly, which is probably not the case here...[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Or?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] She's the one who's always getting into fights.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] There's always one.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Goodness, what would humans have to fight over?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] They have everything they want here.[P] Why fight?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] I think we can rule out bad homelife or physical abuse.[P] Which means, dominance.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] Some people just have to be on top.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Offended] Gee, I think I know the type.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Laugh] I'm not trying to patronize you![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] It starts early.[P] I just hope she grows out of it.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Offended] No, she'll get picked for Command Unit.[P] Obviously.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] That is how it always goes, isn't it?") ]])
    
elseif(sObjectName == "TerminalI") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Hmm, looks like a log the good doctor left, with audio transcription.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] 'Sis is yet anosser instance of my contract being knowingly violated by se administrators.[P] You are machines, yes?[P] Are you not aware of se stipulations?'[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] 'Se first violasion is the request to deliberately infect sis boy vith a flu virus, and sen to transfuse his blood to his twin brosser.[P] Are you insane?'[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] 'I was also asked to amputate se limbs from one individual, and to attach prosssetics.[P] While research into prosssetics is a worssy goal, it cannot be done in such a gross violation of medical essics.[P] Find an existing amputee, do not create sem!'[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] 'If you vish to discuss sis wiss me, you may find me in my office.[P] Make an appointment wiss my assistant, I have real experiments to conduct.'[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] I think Doctor de Havilland is a good doctor.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Whatever differences we have, I can't argue that her heart isn't in the right place.[P] Good for her.") ]])
    
elseif(sObjectName == "BoxesA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] This box is full of children's toys.[P] Dolls, miniature fabrication benches, and a little PDU.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Huh.[P] But all the humans in the breeding program look to be teenagers.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] They probably store them here until the next wave of humans is born, then distribute them.[P] Who knows how many children have played with these dolls?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] I hadn't even considered that.[P] There must be more than one class of breeding humans at a time, right?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Probably several groups harbored in the other labs.[P] I guess it's not considered worth it to transfer toys between the habitat areas.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Looking at these dolls...[P] I get a feeling of lost familiarity...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Maybe you played with dolls as a child, too?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Maybe.[P] I hope so.[P] I like dolls, they're cute and I'd love to cuddle with one.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] You might get your wish sooner than you think...") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BoxesB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] A discarded copy of Mr. Needlemouse.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Someone threw away a copy of Mr. Needlemouse?[P] Why?[P] I heard it was good.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] No no, this is the sequel.[P] It just uses the same name as the first one.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Offended] Why would you make another game with the exact same name as the first one?[P] That just makes it confusing.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] Oh, the name is the least of its worries.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] I didn't know you play video games, Sophie.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] I -[P] I don't, really![P] Hee hee![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] But a lot of my friends do.[P] Should I get into them?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Certainly![P] There's all kinds of great picks for a beginner![P] We should play some together when we have time![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Uh, after the...[P] civil war...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Life goes on ever when the bullets start flying... [EMOTION|Christine|Happy]Ooh, I should show you Pandemoniumbound![P] It's quirky and irreverent and funny and lighthearted![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] It's a date!") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BoxesC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Spare medicines and medical tools.[P] Disposable gloves, tubing, syringes.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookshelfA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] 'Position Recommendations'...[P] Pretty simple title.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Blush] ...[P] Oh my.[P] Very to-the-point, isn't it?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Oh?[P] Let me see![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Surprised] What!?[P] Can an organic's spine bend like that?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Blush] Look at this one![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Surprised] Wouldn't the blood rush to their head when they're upside-down like that?[P] I heard that causes an organic to pass out.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Blush] I think that might be the point, Sophie.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Christine, I am totally okay with boring and conventional stuff.[P] Don't feel like you need to try these things just to impress me.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Blush] (What if I just want your face in my crotch with the help of a high-tension wire?[P] I think this book is giving me new fetishes...)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookshelfB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] 'Doggos and Pupperinos'.[P] It's a book full of cute dog pictures![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Adorable![P] But what's it doing here?[P] I thought this is where raijus...[P] you know...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] The section here says 'overcharge aids'.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Laugh] Maybe they look at puppies to calm down if they're getting too frisky?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Offended] If you wanted to get them out of the mood, just show them something gross.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Like tentacles?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] I'm sure somebody is into those, Christine.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Offended] Just not me.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Look![P] A husky![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] Awwwwwwww![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Maybe these are just here to cheer them up if something goes wrong.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Sad] Oh no...[P] do raijus have...[P] dysfunctions?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Sad] And you can't just swap out the defective parts...[P] Being organic must be so hard...") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookshelfC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] 'Arcane Superconductivity::[P] A Theory'[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Oh, this is a little above my pay grade.[P] I don't know much about superconductivity.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] I'm sure with the proper documentation we could fix...[P] whatever it is this book is describing.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] I think it's theorizing about how raijus can generate so much electricity without exploding.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Of course![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Maybe one of the raijus borrowed it to better understand herself?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] See![P] They're not all airheads![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] Great![P] I hope she learns so much that she becomes a raiju researcher![P] The first of her kind![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] But, maybe better if the raijus don't become repair units.[P] I can't see that ending well...") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookshelfD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] 'How To Get Her Right In The - '[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] ...[P] Is that the whole title?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Surprised] Y-[P]yep![P] Whole title, right there.[P] Putting this book down!") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "CoffeeMaker") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (An oilmaker modified to grind coffee beans.[P] Has very little effect on golems, but probably gives organics quite a kick.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BarrelYellow") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (This barrel smells strongly of alcohol.[P] It's been a while since I drank any, but I think this is a brandy of some sort.[P] Potent!)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BarrelBlue") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (This barrel is full of water.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "RVD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The RVD is displaying a list of all the delicious things organics can order here.[P] There's over forty different vegetable platters alone![P] Meat, however, is absent from the menu.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Kitchen") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A full kitchen set, which is a real rarity on Regulus.[P] Everything is steel, fiber, or non-stick.[P] None of that cast-iron nonsense you find planetside!)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Lunchbox") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A lunchbox with a thermos full of potato soup.[P] I guess someone's plans got cancelled.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Computer") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Medical records.[P] The login is for the doctor's assistant.[P] Apparently, the doctor isn't very good with computers, and needs someone to keep her records in order.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Medicine") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (All kinds of organic medicines and disinfectants.[P] Common aspirin, pseudophedrine, penoxycyline, morphine, palimerfin...)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Cooler") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A portable cooler to keep drinks cold.[P] There's several jars full of lemonade in here.)") ]])
    fnCutsceneBlocker()
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
