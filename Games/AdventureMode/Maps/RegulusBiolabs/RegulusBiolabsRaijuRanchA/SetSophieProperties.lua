-- |[Set Sophie's Properties]|
--A script that sets Sophie's properties for collision/activation.
EM_PushEntity("Sophie")
    TA_SetProperty("Clipping Flag", true)
    TA_SetProperty("Activation Script", fnResolvePath() .. "Dialogue.lua")
DL_PopActiveObject()
