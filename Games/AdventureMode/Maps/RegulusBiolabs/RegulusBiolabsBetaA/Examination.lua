-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
if(sObjectName == "ToBiolabsDatacoreH") then
    
    --Variables.
    local iBiolabsFoundRedKeycard = VM_GetVar("Root/Variables/Chapter5/Scenes/iBiolabsFoundRedKeycard", "N")
    if(iBiolabsFoundRedKeycard == 0.0) then
        AudioManager_PlaySound("World|AutoDoorFail")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        fnCutscene([[ Append("Christine:[E|Neutral] Locked.[P] Security-Red lockout in place.[P] Any idea where we can find a red keycard, 55?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] It looks like the lockout was put in place from inside the datacore.[P] It may be possible to access from hydroponics on the west side of the alpha labs.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] We would need to find an access point on the surface.[P] If it is critical to our mission, that is.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] I don't think it's completely vital, but I'll keep it in mind.") ]])
        fnCutsceneBlocker()
    
    else
        AudioManager_PlaySound("World|AutoDoorOpen")
        AL_BeginTransitionTo("RegulusBiolabsDatacoreH", "FORCEPOS:11.0x4.0x0")
    end
    
-- |[Objects]|
elseif(sObjectName == "Box") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Spare organic rations, possibly for a human interning here.[P] They're frozen solid.)") ]])
    
elseif(sObjectName == "Shelf") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A broken handheld gaming console.[P] Wait -[P] the battery is just dead![P] Sheesh, assign a repair unit already!)") ]])
    
elseif(sObjectName == "TerminalA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('All Units::[P] Evacuate to Transit Station Omicron.[P] Defragmentation permissions suspended.')") ]])
    
elseif(sObjectName == "TerminalB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The screen is frozen over, but I can make out some of the text underneath.[P] Looks like an apology email to someone.)") ]])
    
elseif(sObjectName == "TerminalC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (I can't see much of the screen underneath the ice, but it's clearly an evacuation order.)") ]])
    
elseif(sObjectName == "Glass") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The glass was shattered with an incredible amount of force...)") ]])
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
