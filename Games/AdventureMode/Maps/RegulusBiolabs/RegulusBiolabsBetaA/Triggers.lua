-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
if(sObjectName == "Cold") then
    
    --Repeat check.
    local iSawItsCold = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawItsCold", "N")
    if(iSawItsCold == 1.0) then return end
    
    --Variables.
    local iSawRaijuIntro   = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawRaijuIntro", "N") --If 1.0, Sophie is not present.
    local iSX399JoinsParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iSawItsCold", "N", 1.0)
    
    --Dialogue.
    if(iSX399JoinsParty == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        fnCutscene([[ Append("Christine:[E|Neutral] Sheesh it's cold in here.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] That is correct, Unit 771852.[P] It is indeed, cold, in here.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smug] Are there any other observations to make?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] And overexpose you to my genius?[P] Perish the thought![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] I just didn't know we had an arctic research area.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] We do not.[P] The Biolabs does not currently have such an area, though one has been planned for several years.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] I would hypothesize an act of sabotage.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Not unexpected, as we've been seeing a lot of that.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] But how?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] The Beta Laboratories are the location of one of the heat exchange pipes transfer nodes.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Regulus City produces an excess of heat and pipes heated water to external tanks, where it diffuses heat with the substrate.[P] Cold water is then piped back in to the city.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] If the pipes were blocked, the subzero water entering the city would not mix and would instead sap the heat from the area.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] How possible is it that it was unintentional?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] A very specific combination of pipes would need to be destroyed.[P] It is unlikely to be an accident.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] 20 is trying to slow us down...[B][C]") ]])
        if(iSawRaijuIntro == 0.0) then
            fnCutscene([[ Append("Sophie:[E|Offended] She's going to freeze all the plants out![P] What a mean-jerk-head![B][C]") ]])
        end
        fnCutscene([[ Append("55:[E|Neutral] Unfortunately, it will take a dedicated repair crew and heavy equipment to repair the damage.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] Beyond our capabilities, then.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] We'll just have to proceed to the eastern side and head to the Gamma Labs, then.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] All right, move out everyone.") ]])
        fnCutsceneBlocker()
    
    --SX-399 variation.
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        fnCutscene([[ Append("Christine:[E|Neutral] Sheesh it's cold in here.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] That is correct, Unit 771852.[P] It is indeed, cold, in here.[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Smirk] A little harsh there, 55.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] ...[P] Apologies.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Is this an arctic research area?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] While such an area has been planned for some time, it has not been constructed.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] I would hypothesize an act of sabotage is the reason this lab is frozen over.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] Not unexpected, as we've been seeing a lot of that.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] But how?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] The Beta Laboratories are the location of one of the heat exchange pipes transfer nodes.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Regulus City produces an excess of heat and pipes heated water to external tanks, where it diffuses heat with the substrate.[P] Cold water is then piped back in to the city.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] If the pipes were blocked, the subzero water entering the city would not mix and would instead sap the heat from the area.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] How possible is it that it was unintentional?[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Neutral] Zero chance, really.[P] We, uh, 'borrow' the hot water for Steam Droid settlements.[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Smirk] So long as we put back what we use, Regulus City doesn't notice.[P] So we know pretty darn well what it takes to maintain them.[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Neutral] If you made a mistake, the system would just jam up.[P] You'd have to blow the correct pipes to get a net heat loss like this.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] 20 is trying to slow us down...[B][C]") ]])
        if(iSawRaijuIntro == 0.0) then
            fnCutscene([[ Append("Sophie:[E|Offended] She's going to freeze all the plants out![P] What a mean-jerk-head![B][C]") ]])
        end
        fnCutscene([[ Append("55:[E|Neutral] Unfortunately, it will take a dedicated repair crew and heavy equipment to repair the damage.[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Neutral] And soon, too.[P] Because there's going to be bursting pipes all over the city if this isn't fixed.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] Beyond our capabilities, unfortunately.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] We'll just have to proceed to the eastern side and head to the Gamma Labs, then.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] All right, move out everyone.") ]])
        fnCutsceneBlocker()
    
    end
end
