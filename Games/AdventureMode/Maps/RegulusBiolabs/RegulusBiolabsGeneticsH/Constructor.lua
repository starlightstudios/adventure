-- |[Constructor]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "RegulusBiolabsGeneticsH"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[Standard]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music. Uses layering.
	AL_SetProperty("Music", "Null")
    
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation("RegulusBiolabsGeneticsD")
	
	-- |[Spawn NPCS Here]|
	--NPCs set to spawn here will be available for post-exec cutscenes.

-- |[Post-Exec]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	-- |[Spawn NPCs Here]|
	--If an entity needs to spawn after a post-exec cutscene, put them here.
    fnSpawnNPCPattern("Golem", "A", "C")
    
    --Give special frames to these NPCs.
    EM_PushEntity("GolemA")
        TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/GolemSlaveR|Wounded")
        TA_SetProperty("Add Special Frame", "Sitting", "Root/Images/Sprites/Special/GolemSlaveR|Sitting")
    DL_PopActiveObject()
    EM_PushEntity("GolemB")
        TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/GolemSlaveR|Wounded")
        TA_SetProperty("Add Special Frame", "Sitting", "Root/Images/Sprites/Special/GolemSlaveR|Sitting")
    DL_PopActiveObject()
    EM_PushEntity("GolemC")
        TA_SetProperty("Clipping Flag", false)
        TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/GolemSlaveR|Wounded")
        TA_SetProperty("Add Special Frame", "Sitting", "Root/Images/Sprites/Special/GolemSlaveR|Sitting")
    DL_PopActiveObject()
    
    --Flag the NPCs to use those frames.
    fnCutsceneSetFrame("GolemA", "Wounded")
    fnCutsceneSetFrame("GolemB", "Sitting")
    fnCutsceneSetFrame("GolemC", "Wounded")
    
    --Disable layers.
    AL_SetProperty("Set Layer Disabled", "Splash0", true)
    AL_SetProperty("Set Layer Disabled", "Splash1", true)
    AL_SetProperty("Set Layer Disabled", "Splash2", true)
    AL_SetProperty("Set Layer Disabled", "Splash3", true)
end
