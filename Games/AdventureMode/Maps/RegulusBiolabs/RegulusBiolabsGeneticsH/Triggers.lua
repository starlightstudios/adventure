-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
if(sObjectName == "Action") then
    
    --Variables.
    local iAquaticsActionScene = VM_GetVar("Root/Variables/Chapter5/Scenes/iAquaticsActionScene", "N")
    if(iAquaticsActionScene >= 1.0) then return end
    
    --Achievement.
    AM_SetPropertyJournal("Unlock Achievement", "FinishAquatic")
    
    --Topics.
    WD_SetProperty("Unlock Topic", "Biolabs_Heroics", 1)
    
    --Variables.
    local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iAquaticsActionScene", "N", 1.0)
    
    --Movement.
    fnCutsceneMove("Christine", 28.25, 18.50, 2.50)
    fnCutsceneFace("Christine", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "RebelGolem", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] ...[P] Let's see...[P] Yep, lead slugs.[P] Not going to penetrate by themselves but a hundred of them will do some damage...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Fortunately this is just impact shaking...[P] Oh that's not good...[B][C]") ]])
    fnCutscene([[ Append("Rebel:[E|Neutral] Huh?[P] Is someone there?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Scared] You're online?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Where were you hit?[B][C]") ]])
    fnCutscene([[ Append("Rebel:[E|Neutral] Internal sensors are wrecked.[P] I can't move my legs...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] PDU, get me -[P] oh, nevermind.[P] There's the problem.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Looks like your bus wire got clipped by a bullet.[P] I can fix that...[B][C]") ]])
    fnCutscene([[ Append("Rebel:[E|Neutral] Aaaahhhh![P] Ow![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Okay that's a patch job until you can get to a repair bay.[P] You won't be winning any sprinting contests, but you should be able to walk.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneSetFrame("GolemA", "Null")
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneMove("Christine", 29.25, 18.50)
    fnCutsceneMove("Christine", 29.25, 17.50)
    fnCutsceneFace("Christine", -1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "RebelGolem", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Everything functional?[B][C]") ]])
    fnCutscene([[ Append("Rebel:[E|Neutral] Th-[P]thanks.[P] Are you with the rebels?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] You could say that.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] I've got to go check up your friend, there.[P] Stay here.[B][C]") ]])
    fnCutscene([[ Append("Rebel:[E|Neutral] Wait![B][C]") ]])
    fnCutscene([[ Append("Rebel:[E|Neutral] Stay behind these boxes![P] We're in sight of a gun nest!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Camera moves.
    fnCutsceneFace("Christine", 1, 0)
    fnCutsceneFace("GolemA", 1, 0)
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 3.0) --Default is 5.0
        CameraEvent_SetProperty("Focus Position", (44.25 * gciSizePerTile), (8.50 * gciSizePerTile))
    DL_PopActiveObject()
    fnCutsceneBlocker()
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 3.0) --Default is 5.0
        CameraEvent_SetProperty("Focus Actor Name", "Christine")
    DL_PopActiveObject()
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "RebelGolem", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Offended] Peachy.[P] Stay here.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] I'll get your friend fixed up and we can get you out of here.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Christine", 29.25, 20.50, 0.75)
    fnCutsceneFace("Christine", 1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "RebelGolem", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Offended] Okay, keep your head down, buddy.[P] Oh, limb control motors blown out, that's bad...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Okay you won't be going anywhere without total limb replacement.[P] Can you speak?[B][C]") ]])
    fnCutscene([[ Append("Rebel:[E|Neutral] Y-[P]yes...[B][C]") ]])
    fnCutscene([[ Append("Rebel:[E|Neutral] It hurts...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] There you go, pain receptors zeroed.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] Absolutely don't move your arms or legs, though.[P] With your receptors off you'll probably just damage your core systems.[B][C]") ]])
    fnCutscene([[ Append("Rebel:[E|Neutral] W-[P]wow.[P] It doesn't hurt?[P] Thank you...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Christine.[P] Pleased to meet you.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] I can carry you back to safety.[P] Just try not to move.[P] In fact, enter system standby, all right?[B][C]") ]])
    fnCutscene([[ Append("Rebel:[E|Neutral] Affirmative.[P] Entering system standby...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("GolemA", 28.25, 20.50)
    fnCutsceneFace("GolemA", 1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneFace("Christine", -1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "RebelGolem", "Neutral") ]])
    fnCutscene([[ Append("Rebel:[E|Neutral] Christine?[P] There's one problem.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] What's that?[B][C]") ]])
    fnCutscene([[ Append("Rebel:[E|Neutral] Tam...[P] didn't get across the bridge.[P] She's still over there.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Camera moves.
    fnCutsceneFace("Christine", 1, 0)
    fnCutsceneFace("GolemA", 1, 0)
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Focus Actor ID", 0)
    DL_PopActiveObject()
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 3.0) --Default is 5.0
        CameraEvent_SetProperty("Focus Position", (52.25 * gciSizePerTile), (18.50 * gciSizePerTile))
    DL_PopActiveObject()
    fnCutsceneBlocker()
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 3.0) --Default is 5.0
        CameraEvent_SetProperty("Focus Actor Name", "Christine")
    DL_PopActiveObject()
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "RebelGolem", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Offended] Okay, that's really bad...[B][C]") ]])
    fnCutscene([[ Append("Rebel:[E|Neutral] She looks pretty hurt.[P] Do you think...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] Change of plans.[P] You carry this unit.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Drag her if you have to.[P] That elevator over there leads underwater, and I can escort you from there.[B][C]") ]])
    fnCutscene([[ Append("Rebel:[E|Neutral] No.[P] No please, we can just leave Tam behind.[P] She's probably retired already.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Her power core is still cycling.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] And -[P] The Dormers don't leave anyone behind.[B][C]") ]])
    fnCutscene([[ Append("Rebel:[E|Neutral] Dormers?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] This is stupid, and dumb, and 55 is going to scream at me for it...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Take this unit down the elevator and wait for me.[P] I'll be back.[B][C]") ]])
    fnCutscene([[ Append("Rebel:[E|Neutral] But - [P][CLEAR]") ]])
    fnCutscene([[ Append("Christine:[E|Angry] Get going, soldier![B][C]") ]])
    fnCutscene([[ Append("Rebel:[E|Neutral] Affirmative!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Christine", 29.25, 21.50)
    fnCutsceneMove("Christine", 32.25, 21.50)
    fnCutsceneBlocker()
    fnCutsceneMove("Christine", 35.25, 18.50)
    fnCutsceneFace("Christine", 1, 0)
    fnCutsceneMove("GolemA", 29.25, 20.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneMove("GolemA", 22.25, 20.50)
    fnCutsceneMove("GolemA", 22.25, 18.50)
    fnCutsceneMove("GolemA", 15.25, 18.50)
    fnCutsceneMove("GolemB", 22.25, 20.50)
    fnCutsceneMove("GolemB", 22.25, 18.50)
    fnCutsceneMove("GolemB", 16.25, 18.50)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Deep breath, Christine.[P] Uh, figuratively.)[B][C]") ]])
    if(sChristineForm == "Golem" or sChristineForm == "LatexDrone" or sChristineForm == "Doll") then
        fnCutscene([[ Append("Thought:[VOICE|Leader] (It's just an armor-penetrating automated turret emplacement.[P] You can probably withstand a hit or two, right?)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (It's not like the approximately eighty rounds it will fire at you during this sprint will do any real damage...[P] right?)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Of course it will.[P] But you're going to do it anyway because that unit over there needs you to.[P] So stop wasting time...)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Here we go...)") ]])
    else
        fnCutscene([[ Append("Thought:[VOICE|Leader] (It's just an armor-penetrating automated turret emplacement.[P] It will probably just phase right through your body, right?)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Maybe if you had spent time mastering the teleportive abilities of this body instead of spending time with Sophie or planning or -[P] stop it!)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Stop wasting time, that unit needs you.[P] Get going.[P] Go!)") ]])
    end
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --fnCutsceneTeleport("Christine", 35.25, 18.50)
    --fnCutsceneWait(25)
    --fnCutsceneBlocker()
    
    -- |[Spawn Impacts]|
    --These are bullet impacts that spawn near Christine as she runs.
    local iCurrentImpact = 1
    local iImpactsTotal = 30
    for q = 1, iImpactsTotal, 1 do
        TA_Create("Impact" .. q)
            TA_SetProperty("Position", -100, -100)
            TA_SetProperty("Rendering Depth", 0.000000)
            TA_SetProperty("Walk Ticks Per Frame", 3)
            TA_SetProperty("Auto Animates Fast", true)
            for i = 1, 8, 1 do
                for p = 1, 4, 1 do
                    TA_SetProperty("Move Frame", i-1, p-1, "Root/Images/Sprites/Impacts/Bullet" .. (p-1))
                end
            end
        DL_PopActiveObject()
    end
    
    --Christine's movement. 
    fnCutsceneMove("Christine", 49.25, 18.50, 2.50)
    
    --Start the machine-gun sound after a few ticks.
    Cutscene_CreateEvent("AudioEvent", "Audio")
        AudioEvent_SetProperty("Delay", 15)
        AudioEvent_SetProperty("Sound", "World|MachineGun")
    DL_PopActiveObject()
    
    --We also set bullets to spawn based on Christine's rough movement.
    local fXPos = 39.25
    local fYPos = 18.50
    local fStepRate = 0.25
    local iDelayTimer = 25
    while fXPos < 50.00 do
        
        --Spawn position.
        local fSpawnX = LM_GetRandomNumber(-1, 1) + fXPos
        local fSpawnY = LM_GetRandomNumber(-1, 1) + fYPos
        
        --Cause the impact to spawn and despawn when it finishes running.
        Cutscene_CreateEvent("ActorEvent", "Actor")
            ActorEvent_SetProperty("Negative Move Timer", iDelayTimer)
            ActorEvent_SetProperty("Subject Name", "Impact" .. iCurrentImpact)
        DL_PopActiveObject()
        fnCutsceneTeleport("Impact" .. iCurrentImpact, fSpawnX, fSpawnY)
        Cutscene_CreateEvent("ActorEvent", "Actor")
            ActorEvent_SetProperty("Subject Name", "Impact" .. iCurrentImpact)
            ActorEvent_SetProperty("Reset Move Timer")
        DL_PopActiveObject()
        Cutscene_CreateEvent("ActorEvent", "Actor")
            ActorEvent_SetProperty("Subject Name", "Impact" .. iCurrentImpact)
            ActorEvent_SetProperty("Auto Despawn")
        DL_PopActiveObject()
        Cutscene_CreateEvent("AudioEvent", "Audio")
            AudioEvent_SetProperty("Delay", iDelayTimer)
            AudioEvent_SetProperty("Sound", "World|BulletImpact" .. LM_GetRandomNumber(0, 3))
        DL_PopActiveObject()
        
        --Next impact.
        iDelayTimer = iDelayTimer + 2
        iCurrentImpact = iCurrentImpact + 1
        if(iCurrentImpact > iImpactsTotal) then break end
        
        --Advance.
        fXPos = fXPos + fStepRate
        if(fXPos >= 50.00) then
            --io.write("Last impact was " .. iCurrentImpact .. "\n")
        end
    end
    fnCutsceneBlocker()
    
    --Screen goes red.
    fnCutsceneMove("Christine", 50.25, 18.50, 2.50)
    fnCutsceneMove("Christine", 51.25, 18.50, 0.50)
    Cutscene_CreateEvent("AudioEvent", "Audio")
        AudioEvent_SetProperty("Sound", "World|BulletImpactImportant")
    DL_PopActiveObject()
    fnCutsceneSetFrame("Christine", "Wounded")
    fnCutscene([[ AL_SetProperty("Activate Fade", 2, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 1, 0, 0, 1) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(305)
    fnCutsceneFace("Christine", 0, 1)
    fnCutsceneBlocker()
    
    --Fade back in.
    fnCutscene([[ AL_SetProperty("Activate Fade", 185, gci_Fade_Under_GUI, false, 1, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(185)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    if(sChristineForm == "Golem" or sChristineForm == "LatexDrone") then
        fnCutscene([[ Append("Thought:[VOICE|Leader] (That one...[P] hit my cranial chassis...)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Systems report nominal.[P] Chassis report deflection.[P] Shake it off, Christine...)") ]])
    elseif(sChristineForm == "Doll") then
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Barely...[P] scuffed the outermost resin layer...)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (I much prefer the golem chassis' shielding even if it's more rigid.)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Okay, just a little rattled, systems nominal.)") ]])
    else
        fnCutscene([[ Append("Thought:[VOICE|Leader] (That one...[P] hit something important...)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (I'm leaking starlight...[P] but I'll live.[P] I've taken harder hits.[P] Ughhh....)") ]])
    end
    fnCutsceneBlocker()
    fnCutsceneSetFrame("Christine", "Crouch")
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneSetFrame("Christine", "Null")
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneFace("Christine", 1, 0)
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Sad] Can you hear me?[P] Hello?[P] Blast, she's in standby...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Sad] Looks like that saved her life.[P] She's lost almost all her coolant, and her system is about to go red.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Sad] I can't stabilize the core, I might just melt my hands...[P] But...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] Repair units fix problems.[P] Come on, Christine, think![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] I've got to cool her down before she goes critical![P] Think![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] ...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Another stupid idea that 55 is going to scream at me for trying.[P] Come on, Tam, here we go!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Christine", 51.75, 18.50, 0.50)
    fnCutsceneBlocker()
    fnCutsceneMove("Christine", 56.25, 18.50, 0.50)
    fnCutsceneMove("GolemC", 56.75, 18.50, 0.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Jump!
    local iTicks = 0
    local iCalibratedTicks = 20 --Calibrated based on Y speed.
    local bIsInWater = false
    fXPos = 56.25
    fYPos = 18.50
    local fXSpeed = 2.0 / iCalibratedTicks
    local fYSpeed = -5.0 / gciSizePerTile
    local fGravity = 0.325 / gciSizePerTile
    while(bIsInWater == false) do
        
        --Speed.
        fXPos = fXPos + fXSpeed
        fYPos = fYPos + fYSpeed
        
        --Gravity
        fYSpeed = fYSpeed + fGravity
        iTicks = iTicks + 1
        
        --Set positions.
        fnCutsceneTeleport("Christine", fXPos, fYPos)
        fnCutsceneTeleport("GolemC", fXPos + 0.50, fYPos)
        fnCutsceneBlocker()
        
        --Ending case.
        if(fYSpeed >= 0.0 and fYPos >= 18.50) then
            bIsInWater = true
        end
    end
    
    --Camera focus on this point.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 5.0) --Default is 5.0
        CameraEvent_SetProperty("Focus Position", (59.25 * gciSizePerTile), (18.50 * gciSizePerTile))
    DL_PopActiveObject()
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|BigSplash") ]])
    fnCutsceneTeleport("Christine", -100, -100)
    fnCutsceneTeleport("GolemC", -100, -100)
    fnCutsceneBlocker()
    
    --Splashing animation.
    fnCutscene([[ AL_SetProperty("Set Layer Disabled", "Splash0", false) ]])
    fnCutsceneWait(4)
    fnCutsceneBlocker()
    
    fnCutscene([[ AL_SetProperty("Set Layer Disabled", "Splash0", true) ]])
    fnCutscene([[ AL_SetProperty("Set Layer Disabled", "Splash1", false) ]])
    fnCutsceneWait(4)
    fnCutsceneBlocker()
    
    fnCutscene([[ AL_SetProperty("Set Layer Disabled", "Splash1", true) ]])
    fnCutscene([[ AL_SetProperty("Set Layer Disabled", "Splash2", false) ]])
    fnCutsceneWait(4)
    fnCutsceneBlocker()
    
    fnCutscene([[ AL_SetProperty("Set Layer Disabled", "Splash2", true) ]])
    fnCutscene([[ AL_SetProperty("Set Layer Disabled", "Splash3", false) ]])
    fnCutsceneWait(4)
    fnCutsceneBlocker()
    
    fnCutscene([[ AL_SetProperty("Set Layer Disabled", "Splash3", true) ]])
    fnCutsceneWait(4)
    fnCutsceneBlocker()
    
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Room transition.
    fnCutsceneWait(85)
    fnCutscene([[ AL_SetProperty("Activate Fade", 85, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    fnCutscene([[ AL_BeginTransitionTo("RegulusBiolabsGeneticsG", "FORCEPOS:53.0x23.0x0") ]])
    fnCutsceneBlocker()
    
end
