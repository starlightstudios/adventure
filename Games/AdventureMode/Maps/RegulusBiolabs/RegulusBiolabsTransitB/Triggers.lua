-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
if(sObjectName == "Stargazing") then
    
    --Variables.
    local bIsSX399Present = fnIsCharacterPresent("SX-399")
    local iSawStargazing  = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawStargazing", "N")
    if(iSawStargazing == 0.0) then
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSawStargazing", "N", 1.0)
    
        --Movement.
        fnCutsceneMove("Christine", 8.25, 13.50)
        fnCutsceneFace("Christine", 0, 1)
        fnCutsceneMove("Tiffany", 7.25, 13.50)
        fnCutsceneFace("Tiffany", 0, 1)
        fnCutsceneMove("Sophie", 9.25, 13.50)
        fnCutsceneFace("Sophie", 0, 1)
        if(bIsSX399Present == true) then
            fnCutsceneMove("SX-399", 8.25, 12.50)
            fnCutsceneFace("SX-399", 0, 1)
        end
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        fnCutscene([[ Append("Sophie:[E|Happy] Christine![P] A glass-ceiling garden so you can watch the stars![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] It's just beautiful...[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] Spending my time in the repair bay, I forgot that such wonders exist right here in our own city.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] Can we just spend a moment looking out?[B][C]") ]])
        if(bIsSX399Present == true) then
            fnCutscene([[ Append("SX-399:[E|Flirt] I just need to remind everyone that this whole operation, from start to finish, has been an excuse to arrange a double-date.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Upset] Has it, now?[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Smirk] Of course![B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Sad] Wait what are you saying?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] I am sorry, I did not mean to upset you.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] ...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] I am just unsure.[P] You need to take this situation seriously.[P] We are in extreme danger at all times.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] I get you.[P] Really, I do.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] I am glad you are here.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Smirk] Yeah.[P] I know.[P] But you're kind of a grump.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Laugh] She's working on it![P] Ha ha ha![B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Flirt] That's what really counts.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Smirk] ...[P] ya grumpalump.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Smirk] And you are a -[P] cog.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Smirk] Cog?[P] That's all you had?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] I will check the network for insults at a later time.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Smirk] And I will compose some of my own.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Flirt] Of course.[P] And from now on, I'll wear cog as a badge of honor.") ]])
        else
            fnCutscene([[ Append("55:[E|Upset] No.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] But...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Sorry Sophie, but she's right.[P] People are counting on us.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Smirk] I know, I know.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Smirk] But when I went to dance with you, and I got to just -[P] just dance and nothing else?[P] I lost myself in it.[P] For once I was just me, having a good time, without worrying.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Smirk] I hope there will be many more nights like that.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] There will.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] (She spoke up before I did!)[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] It will take a great deal of work.[P] You know quite well the requirement of delayed gratification.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Smirk] I can still look forward to it.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Me too![P] But for now, just save a recording and replay it later.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] The stars will be the same next you look, I promise.") ]])
            
        end
        fnCutsceneBlocker()
    
        --Autofold party.
        fnAutoFoldParty()
        fnCutsceneBlocker()
    
    end
end
