-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
if(sObjectName == "Action") then
    
    --Lock fade out.
    fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Move Sammy offscreen.
    fnCutsceneTeleport("Sammy", 21.25, -1.50)
    
    --Remove Sammy's shadow.
    EM_PushEntity("Sammy")
        TA_SetProperty("No Automatic Shadow", true)
    DL_PopActiveObject()
    
    --Spawn NPCs.
    TA_Create("Coconut")
        TA_SetProperty("Position", 18, 11)
        TA_SetProperty("Facing", gci_Face_North)
        TA_SetProperty("Clipping Flag", false)
        fnSetCharacterGraphics("Root/Images/Sprites/GenericF0/", false)
    DL_PopActiveObject()
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Focus Actor Name", "Coconut")
    DL_PopActiveObject()
    fnCutsceneSetFrame("Sammy", "SwimS")
    fnCutsceneBlocker()
    
    --Fade in.
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Coconut:[VOICE|Coconut] (Where is she?[P] She should have come out by now...)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Teleport.
    fnCutsceneTeleport("Sammy", 22.25, 0.50)
    fnCutsceneMove("Sammy", 22.25, 11.50)
    fnCutsceneBlocker()
    fnCutsceneSetFrame("Sammy", "SwimW")
    fnCutsceneBlocker()
    fnCutsceneFace("Coconut", 1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sammy", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Coconut", "Neutral") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] *Cough*[P] I hope you've done your job, because I sure did mine![B][C]") ]])
    fnCutscene([[ Append("Coconut:[E|Neutral] Almond![P] You're alive![B][C]") ]])
    fnCutscene([[ Append("Coconut:[E|Neutral] And you weren't kidding about the swimming.[B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] It's an agent's second most important skill.[P] Wits are in first place, statistically.[B][C]") ]])
    fnCutscene([[ Append("Coconut:[E|Neutral] Okay, enough chatter![P] I stole a speedboat from some evil robots![P] Let's get out of here![B][C]") ]])
    fnCutscene([[ Append("Almond:[E|Neutral] Awright!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Transition.
    fnCutscene([[ AL_BeginTransitionTo("RegulusBiolabsMovieF", "FORCEPOS:24.0x7.0x0") ]])
    fnCutsceneBlocker()
        
end
