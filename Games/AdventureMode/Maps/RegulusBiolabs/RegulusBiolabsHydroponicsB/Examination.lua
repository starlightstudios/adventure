-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
-- |[Objects]|
if(sObjectName == "ToHydroponicsA") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsHydroponicsA", "FORCEPOS:4.5x27.0x0")
    
elseif(sObjectName == "ToHydroponicsCLft") then
    giForceFacing = gci_Face_North
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsHydroponicsC", "FORCEPOS:4.5x8.0x0")
    
elseif(sObjectName == "ToHydroponicsCRgt") then
    giForceFacing = gci_Face_North
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsHydroponicsC", "FORCEPOS:38.5x8.0x0")
    
elseif(sObjectName == "ConsoleA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Genetic Strain 7-C.[P] High nutrient density, slow growth rate.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ConsoleB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Genetic Strain 14-RR-2.[P] Generates excessive fruit relative to seed density.[P] Good crop candidate.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ConsoleC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Genetic Strain SCCS-4.[P] Abnormally popular with avians in the Alpha labs, particularly crows.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ConsoleD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Genetic Strain SCCS-5.[P] Kernels pop when heated.[P] Apply salt and butter, they're delicious!')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ConsoleE") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Genetic Strain NORMA-R12.[P] We got the first 11 tries wrong, we'll get it this time.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ConsoleF") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Genetic Strain 44-ISAARI.[P] Photosynthesis rates 20pct higher than species average.[P] Further investigation recommended.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ConsoleG") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Genetic Strain MALVOL-104-L.[P] Does not react to fertilizers, but fares well in poor soil quality.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ConsoleH") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Genetic Strain JMM-19.[P] No special properties.[P] Recommend dumping seed stock.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ConsoleI") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Genetic Strain FFF-99.[P] Dubbed 'Naty' by the Slave Units, shows excellent resistance to exposure to cosmic radiation.[P] Recommended candidate for orbital hydroponics.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ConsoleJ") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Genetic Strain VORO-MNIS.[P] Tends to concentrate alkaline elements in its root system.[P] Investigate usages for soil replenishment.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ConsoleK") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Genetic Strain 89-MIR-05.[P] Functions well at low moisture.[P] Recommend research into strains 99-MIR-05 and 117-MIR-05.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ConsoleL") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Genetic Strain NaM-AP0.[P] Annelids thrive in soils with this strain.[P] Useful as a first-order reclaimer.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ScreenLft") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('LAST SANITATION CYCLE -[P] SIXTEEN HOURS.[P] NEW CYCLE -[P] NEGATIVE FOUR HOURS.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ScreenRgt") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('ALL UNITS EVACUATE HYDROPONICS.[P] PROCEED TO TRANSIT STATION OMICRON.[P] LEAVE PERSONAL POSSESSIONS.[P] THANK YOU.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Racks") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Hundreds of plant species, tagged with barcodes.[P] The tags also show their genetic strain and special instructions.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerrariumA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A terrarium, this one contains several small arthropod species.[P] There are sensors set up to collect data on them.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerrariumABig") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A terrarium, with amphibians.[P] Cameras and air filters monitor them and send data to the datacore north of here.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerrariumB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A terrarium, with little fields tilled and segregated.[P] A list of worm species is written on the side of the glass.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerrariumBBig") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A terrarium, with various fertilizer formulas written on the glass.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerrariumC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A terrarium, with snakes slithering around in it.[P] Looks like an experiment is being conducted on their breeding habits in various soils.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerrariumCBig") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A terrarium, with a lot of webs on the leaves.[P] There are notes on spider web construction written on the glass.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerrariumD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A terrarium, with no animal species.[P] The experiment is documenting how pollination occurs with no intermediaries.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerrariumDBig") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A list of personnel in the hydroponics division is written on the side of the glass, indicating who is responsible for which terrarium.)") ]])
    fnCutsceneBlocker()
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
