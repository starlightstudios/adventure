-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
if(sObjectName == "ToAmphibianB") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsAmphibianB", "FORCEPOS:10.5x8.0x0")
    
elseif(sObjectName == "ToAmphibianD") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsAmphibianD", "FORCEPOS:38.5x29.0x0")
    
-- |[Objects]|
elseif(sObjectName == "TerminalA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Lists of shipping and receiving orders.[P] This laboratory imports a lot of feedstock from the other parts of the lab, probably to feed its specimens.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The water here is used for tadpole breeding, but it's not in use.[P] Good thing, too, because it's far too cold right now.[P] Current temperature readout is 1C.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('An algorithm is valid even if it is not executed.[P] So then does validity actually mean anything?[P] What exactly would constitute an invalid algorithm?[P] Seems like a tautology.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (An evacuation notice is flashing.[P] All of the domiciles above are marked as unoccupied.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalE") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Did you notice anything out of place in your office this morning?[P] Some of the things on my desk were shifted around, but no cleaners were scheduled to be in here.[P] Odd.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ComputerA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Ha ha, Unit 9203 sent me the same thing.[P] Axioms, derivations.[P] Mostly pseudointellectual nonsense that is basically untestable.[P] You're a lot more charitable than I am, it seems.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ComputerB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Automated Notification::[P] Unable to contact Beta Laboratories, possible network outage.[P] Please contact a repair team.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ComputerC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Oh yeah, I totally read that story![P] Was there more than one chapter?[P] I loved the part where Alice tries to turn Erica into a doll!')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "ComputerD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Tandem reading a Professor Killsalot story?[P] Wouldn't you rather feed the ducks with me, my sweet?[P] And, uh, make out on the park bench?')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "RVD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Proper organic handling and decontamination protocols.[P] A videograph is playing showing a Slave Unit washing her chassis, step by step.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Steps 12 through 34 are about making sure the mammary chassis is sparkling clean.[P] Very educational.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Oilmaker") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (This oilmaker has been sweetened by Fizzy Pop! which is probably a lethal amount of stimulant.[P] Yum!)") ]])
    fnCutsceneBlocker()
    
-- |[Skillbook]|
elseif(sObjectName == "Skillbook") then
    AL_SetProperty("Set Layer Disabled", "SkillbookIco", true)
	LM_ExecuteScript(gsSX399Skillbook, 3)

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
