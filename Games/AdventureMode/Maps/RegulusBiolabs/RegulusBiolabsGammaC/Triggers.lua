-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
if(sObjectName == "Jumper") then
    
    --Variables.
    local iSawHydrophobe = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawHydrophobe", "N")
    
    --Repeat check.
    if(iSawHydrophobe == 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iSawHydrophobe", "N", 1.0)
        
    --Scene.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("55:[VOICE|Tiffany] Christine, there's a power station.") ]])
    fnCutsceneBlocker()
        
    --Move the camera.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 2.0)
        CameraEvent_SetProperty("Focus Position", (54.25 * gciSizePerTile), (12.50 * gciSizePerTile))
    DL_PopActiveObject()
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Move the camera back.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 2.0)
        CameraEvent_SetProperty("Focus Actor Name", "Christine")
    DL_PopActiveObject()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("Christine:[E|Offended] If there was any question that it was sabotage, I think we can write that off now.[P] We better go cut that wire before someone gets hurt.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] I agree.[P] What about the lights?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Probably blown out due to a power surge causing the diodes to explode.[P] You'd need to unset the power modulators.[P] I can explain more later.[P] Come on!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
elseif(sObjectName == "Hydrophobe") then
    
    --Variables.
    local bIsSX399Present     = fnIsCharacterPresent("SX-399")
    local iSawHydrophobe      = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawHydrophobe", "N")
    local iGammaPowerRestored = VM_GetVar("Root/Variables/Chapter5/Scenes/iGammaPowerRestored", "N")
    
    --Haven't seen the cutscene and didn't take the shortcut,
    if(iSawHydrophobe == 0.0 and iGammaPowerRestored == 0.0) then
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSawHydrophobe", "N", 1.0)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iKnowsTubeHoles", "N", 1.0)
        
        --Scene.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("55:[VOICE|Tiffany] Christine, there's a power station.") ]])
        fnCutsceneBlocker()
        
        --Move the party.
        fnCutsceneMove("Christine", 33.25, 3.50)
        fnCutsceneFace("Christine", 1, 1)
        fnCutsceneMove("Tiffany", 34.25, 3.50)
        fnCutsceneFace("Tiffany", 1, 1)
        fnCutsceneMove("Sophie", 35.25, 3.50)
        fnCutsceneFace("Sophie", 1, 1)
        if(bIsSX399Present == true) then
            fnCutsceneMove("SX-399", 36.25, 3.50)
            fnCutsceneFace("SX-399", 1, 1)
        end
        fnCutsceneBlocker()
        
        --Move the camera.
        Cutscene_CreateEvent("CameraEvent", "Camera")
            CameraEvent_SetProperty("Max Move Speed", 2.0)
            CameraEvent_SetProperty("Focus Position", (54.25 * gciSizePerTile), (12.50 * gciSizePerTile))
        DL_PopActiveObject()
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Move the camera back.
        Cutscene_CreateEvent("CameraEvent", "Camera")
            CameraEvent_SetProperty("Max Move Speed", 2.0)
            CameraEvent_SetProperty("Focus Actor Name", "Christine")
        DL_PopActiveObject()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        fnCutscene([[ Append("Christine:[E|Neutral] Sabotage would explain why the lights are blown out.[P] Come on, we can just jump over this gap.[P] Follow me.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Wait...") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneMove("Christine", 34.25, 3.50)
        fnCutsceneMove("Christine", 34.25, 3.50)
        fnCutsceneFace("Christine", 0, 1)
        fnCutsceneMoveFace("Tiffany", 34.25, 2.50, 0, 1)
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        fnCutscene([[ Append("Christine:[E|Neutral] You might need to back up a bit...[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] My predictions estimate you'll need a two-meter running start.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] Mmmm...[P] Oh, don't splash, I might get my dress wet.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Angry] This course of action is incorrect.[P] The probability of failure is too high.[P] Reconsider.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Facing.
        fnCutsceneFace("Christine", 0, -1)
        fnCutsceneFace("Sophie", -1, -1)
        if(bIsSX399Present == true) then
            fnCutsceneFace("SX-399", -1, -1)
        end
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --SX-399 is not present:
        if(bIsSX399Present == false) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
            fnCutscene([[ Append("Christine:[E|Neutral] It's not that far, 55.[P] Besides, the water's pretty shallow.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Upset] Unit 499323 risks soiling her garments.[P] You should not risk that by leaping this gap.[P] Find another path.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Smirk] It's just a little water, 55.[P] Besides, this dress is synthweave.[P] It washes out very easily.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Upset] You are not taking into account the penalties for failure.[P] You could sink.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Then I'll be the first to dive in and save her.[P] Besides, your own estimation matrices must point out that there's almost no chance for failure.[P] And, the water is barely waist-deep.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Upset] 99 percent is not 100 percent.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] I'm willing to take a 99 percent leap here, 55.[P] For crying out loud.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Upset] I am not.[P] Find another path.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] Maybe we should just go around, Christine.[P] It's not very far, right?[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Smirk] She's just trying to protect us.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] All right.[P] Follow me.[P] There's a bridge to the south there.") ]])
            fnCutsceneBlocker()
        
        --SX-399 is present.
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
            fnCutscene([[ Append("Christine:[E|Neutral] It's not that far, 55.[P] Besides, the water's pretty shallow.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Upset] Unit 499323 risks soiling her garments.[P] You should not risk that by leaping this gap.[P] Find another path.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Flirt] She's right.[P] Never risk a gorgeous outfit.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Scared] I would never![P] Please, come to your senses, SX![B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] But it is a little bit of an overreaction.[P] Isn't that dress made of synthweave?[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Smirk] Machine-wash, warm![P] Easy to launder.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Smirk] So a little puddle isn't a problem.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Upset] It is.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] Huh.[P] Are you sure?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] Why are you making such a big issue of this?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Upset] Find another path, 771852.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] Maybe we should just go around, Christine.[P] It's not very far, right?[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Smirk] She's just trying to protect us.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] All right.[P] Follow me.[P] There's a bridge to the south there.") ]])
            fnCutsceneBlocker()
        
        end
    
        --Fold the party.
        fnCutsceneMove("Christine", 34.25, 3.50)
        fnCutsceneMove("Tiffany", 34.25, 3.50)
        fnCutsceneMove("Sophie", 34.25, 3.50)
        if(bIsSX399Present == true) then
            fnCutsceneMove("SX-399", 34.25, 3.50)
        end
        fnCutsceneBlocker()
        fnCutscene([[ AL_SetProperty("Fold Party") ]])
        fnCutsceneBlocker()
    
    end
    
elseif(sObjectName == "GoBack") then
    
    --Variables.
    local bIsSX399Present = fnIsCharacterPresent("SX-399")
    local iRepoweredGamma = VM_GetVar("Root/Variables/Chapter5/Scenes/iRepoweredGamma", "N")
    local iSkippedDatacore = VM_GetVar("Root/Variables/Chapter5/Scenes/iSkippedDatacore", "N")
    if(iRepoweredGamma == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        fnCutscene([[ Append("Christine:[E|PDU] Oh joy.[P] It's you-know-who.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] Can you maybe not yell at her?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Sad] I don't like it when people are fighting...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] To make you happy?[P] Anything.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|PDU] [EMOTION|Sophie|Neutral]What is it, 2856?[B][C]") ]])
        fnCutscene([[ Append("2856:[VOICE|Tiffany] You just connected to the network node in the Gamma labs.[P] So I have to ask, why haven't you fixed the door yet?[B][C]") ]])
        fnCutscene([[ Append("2856:[VOICE|Tiffany] I need that door open to relocate, and your CURRENT SOLE USEFUL PURPOSE TO ME IS DOING THAT.[B][C]") ]])
        if(iSkippedDatacore == 1.0) then
            fnCutscene([[ Append("Christine:[E|PDU] I already fixed the door, and will be dealing with the power issue that caused it very soon.[P] You'll be notified when it's done.[B][C]") ]])
        else
            fnCutscene([[ Append("Christine:[E|PDU] We're on it.[P] Stand by.[P] You should receive a notification when it's fixed.[B][C]") ]])
        end
        fnCutscene([[ Append("2856:[VOICE|Tiffany] ...[P] Thank you, Unit 771852.[P] Continue.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Well that worked![P] Let's go fix the power issue.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] So I can tell her to get stuffed in person.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Blush] (I don't like when they argue, but she's so hot when she does this...[P] Am I a bad unit?)") ]])
        fnCutsceneBlocker()
    
        --Fold the party.
        fnCutsceneMove("Christine", 60.25, 20.50)
        fnCutsceneMove("Tiffany", 60.25, 20.50)
        fnCutsceneMove("Sophie", 60.25, 20.50)
        if(bIsSX399Present == true) then
            fnCutsceneMove("SX-399", 60.25, 20.50)
        end
        fnCutsceneBlocker()
        fnCutscene([[ AL_SetProperty("Fold Party") ]])
        fnCutsceneBlocker()
        
    end
end
