-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
-- |[Objects]|
if(sObjectName == "ToAlphaD") then
    
    --Variables.
    local iRepoweredGamma  = VM_GetVar("Root/Variables/Chapter5/Scenes/iRepoweredGamma", "N")
    local iSkippedDatacore = VM_GetVar("Root/Variables/Chapter5/Scenes/iSkippedDatacore", "N")

    --Door is not repowered.
    if(iRepoweredGamma == 0.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSawGammaAirlockSequence", "N", 1.0)
        if(iSkippedDatacore == 0.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Leader] (The deadbolts on the door are down, and it's not receiving power.[P] We can't even pry it open.[P] Maybe if we can repower this area?)") ]])
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Leader] (We should fix gamma's power problems so 2856 can get through.[P] We can go back afterwards.)") ]])
        end
    
    --Door is repowered.
    else
    
        --Christine organic form checker:
        local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
        if(sChristineForm == "Human" or sChristineForm == "Raiju") then
    
            --Variables:
            local iHasLatexForm         = VM_GetVar("Root/Variables/Global/Christine/iHasLatexForm", "N")
            local iHasDarkmatterForm    = VM_GetVar("Root/Variables/Global/Christine/iHasDarkmatterForm", "N")
            local iHasEldritchForm      = VM_GetVar("Root/Variables/Global/Christine/iHasEldritchForm", "N")
            local iHasElectrospriteForm = VM_GetVar("Root/Variables/Global/Christine/iHasElectrospriteForm", "N")
            local iHasSteamDroidForm    = VM_GetVar("Root/Variables/Global/Christine/iHasSteamDroidForm", "N")
            local iHasDollForm          = VM_GetVar("Root/Variables/Global/Christine/iHasDollForm", "N")
            local iSawRaijuIntro        = VM_GetVar("Root/Variables/Global/Christine/iSawRaijuIntro", "N")
            
            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
            fnCutscene([[ Append("Christine:[E|Neutral] (The area beyond this door is depressurized.[P] Better take on an inorganic form before I go out there.)[BLOCK]") ]])

            --Decision script is this script. It must be surrounded by quotes.
            local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
            fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
            if(iHasDarkmatterForm == 1.0) then
                fnCutscene(" WD_SetProperty(\"Add Decision\", \"Darkmatter\",  " .. sDecisionScript .. ", \"TFToDarkmatter\") ")
            end
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Golem\", " .. sDecisionScript .. ", \"TFToGolem\") ")
            if(iHasLatexForm == 1.0) then
                fnCutscene(" WD_SetProperty(\"Add Decision\", \"Latex Drone\",  " .. sDecisionScript .. ", \"TFToLatex\") ")
            end
            if(iHasEldritchForm == 1.0 and iSawRaijuIntro == 1.0) then
                fnCutscene(" WD_SetProperty(\"Add Decision\", \"Dreamer\",  " .. sDecisionScript .. ", \"TFToEldritch\") ")
            end
            if(iHasElectrospriteForm == 1.0) then
                fnCutscene(" WD_SetProperty(\"Add Decision\", \"Electrosprite\",  " .. sDecisionScript .. ", \"TFToElectrosprite\") ")
            end
            if(iHasSteamDroidForm == 1.0) then
                fnCutscene(" WD_SetProperty(\"Add Decision\", \"Steam Droid\",  " .. sDecisionScript .. ", \"TFToSteam\") ")
            end
            if(iHasDollForm == 1.0) then
                fnCutscene(" WD_SetProperty(\"Add Decision\", \"Doll\",  " .. sDecisionScript .. ", \"TFToDoll\") ")
            end
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Cancel\", " .. sDecisionScript .. ", \"NoCancel\") ")
            fnCutsceneBlocker()
    
        --Inorganic:
        else
            AudioManager_PlaySound("World|AutoDoorOpen")
            AL_BeginTransitionTo("RegulusBiolabsAlphaD", "FORCEPOS:11.0x8.0x0")
        end
    end
    
elseif(sObjectName == "TerminalA") then
    
    --Variables.
    local iRepoweredGamma = VM_GetVar("Root/Variables/Chapter5/Scenes/iRepoweredGamma", "N")
    if(iRepoweredGamma == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('WARNING:: POWER MODULATORS --'[P] The terminal keeps cutting out before it can print the rest of the statement.)") ]])
        fnCutsceneBlocker()
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('Power modulation system reinitialized.[P] Contact repair crew to replace damaged components.'[P] It then displays a very, very long list of damaged electrical components...)") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "TerminalB") then
    
    --Variables.
    local iRepoweredGamma = VM_GetVar("Root/Variables/Chapter5/Scenes/iRepoweredGamma", "N")
    if(iRepoweredGamma == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('Modulator system che --'[P] The terminal is unable to boot the modulator before it surges.)") ]])
        fnCutsceneBlocker()
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('Autogenerated Repair Unit Warning::[P] Unauthorized loopback tampering in Gamma Modulator 7-R.[P] Please contact a security unit.')") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "TerminalC") then
    
    --Variables.
    local iRepoweredGamma = VM_GetVar("Root/Variables/Chapter5/Scenes/iRepoweredGamma", "N")
    if(iRepoweredGamma == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (The terminal is offline...)") ]])
        fnCutsceneBlocker()
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('So we can hire partirhuman contractors from Pandemonium, but nobody has thought of hiring bee girls yet?[P] That'll solve our pollination problems and then some!')[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('I understand that it's not in our budget, but think of the benefits![P] Plus we can probably collect excess honey for sale to Lord Units.[P] Maybe that'd bring in some extra work credits.')") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "TerminalD") then
    
    --Variables.
    local iRepoweredGamma = VM_GetVar("Root/Variables/Chapter5/Scenes/iRepoweredGamma", "N")
    if(iRepoweredGamma == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (The terminal is offline...)") ]])
        fnCutsceneBlocker()
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('I had thought this experiment was meant to establish nutrient density's relation to genetics, but I was told to increase productivity.[P] Productivity?[P] In an experiment?')[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('But I asked around and it turns out some genius in the Beta labs has been fermenting my berries with hops from Hydroponics, and distributing the booze.[P] Lovely.')[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('I'm afraid to terminate the project now, so I guess we'll just be researching quality of alcohol flavour in relation to genetics.[P] At least I won't run short of budget allocation...')") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "RVD") then
    
    --Variables.
    local iRepoweredGamma = VM_GetVar("Root/Variables/Chapter5/Scenes/iRepoweredGamma", "N")
    if(iRepoweredGamma == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (The RVD is offline...)") ]])
        fnCutsceneBlocker()
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('The diminuitive ant, so called in relation to its big-sister, the antgirl, is a eusocial insect species with a very wide range on Pandemonium.')[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('As of this recording, research habitats are being constructed that will allow the Biolabs to understand how these species construct their underground colonies.')[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('Central Administration believes that we can learn more from the habits of ants, particularly transport and storage techniques.[P] It is believed their techniques could be of use in the Regulus Secundus project.[P] Truly, there is much we can learn from the biological world.')") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "Oilmaker") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("Christine:[E|Angry] The oilmaker...[P] is empty![P] There's no oil, no flavours![P] Nothing![B][C]") ]])
    fnCutscene([[ Append("55:[E|Upset] An act of industrial sabotage, surely.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] What kind of monster would do that?[P] Surely one beyond redemption.") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Wire") then
    
    --Variables.
    local bIsSX399Present = fnIsCharacterPresent("SX-399")
    local iRepoweredGamma = VM_GetVar("Root/Variables/Chapter5/Scenes/iRepoweredGamma", "N")
    if(iRepoweredGamma == 0.0) then
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iRepoweredGamma", "N", 1.0)
        
        --Version where SX-399 is not in the party:
        if(bIsSX399Present == false) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
            fnCutscene([[ Append("Christine:[E|Neutral] Well let's see here.[P] I know I'm not much of a repair unit, but I'd say that's your problem right there.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Someone sabotaged the power modulators and stuck a main wire into the water.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] For those of us who are not repair units, please explain.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Are you actually asking about my job?[P] Without already knowing the answer?[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Smirk] And here I thought you knew everything, 55.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] I do not.[P] I research only what I deem to be of vital interest, and download secondary manuals when possible.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] I am not familiar with a power modulator unit.[P] Please elaborate.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] They're not that complex.[P] They're an adjustable analog computational unit that stops too much electricity going out to a given wire.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Smirk] They basically prevent power surges from frying equipment.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] If you hook several modulators in sequence, though, they can transmit back up the line to choke voltage at the source.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Normally, if you put a main wire into the water, the modulators would just stop the flow.[P] They were removed, so the power modulators didn't work and all the systems got fried.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] A deliberate act of sabotage, then.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] Not Vivify's modus operandi.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Sad] But the golem we saw at the gala...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] We are not dealing with a mindless destructive force anymore, Christine.[P] Regulus City's security forces have already been compromised and outmaneuvered.[P] We should assume ambushes and sabotage will become more commonplace, now.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] Her name was 20, right?[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Sad] Isn't she just like us?[P] A rebel?[P] Someone freed from their bondage, and taking revenge on their oppressors?[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Sad] Why is what she's doing different from what we're doing?[P] Shouldn't we be friends with her?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] No.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] We have the same enemy, but not the same goal.[P] 20 doesn't ask, she takes.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] She's not like us at all.[P] We don't...[P] change...[P] others.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Smirk] You seemed pretty excited to hear about Abductions from that Command Unit, Christine.[P] Were you, perhaps, not acting?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Laugh] W-[P]well, I guess you got me there![B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] It's a shame, but I guess I can't compromise my principles.[P] We'll make Abductions an all-volunteer program when we've liberated the city.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Smirk] You sound almost disappointed.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Maybe a little.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Anyway, I cut the wire and sent a reboot order.[P] The modulators should work now.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] The lights are blown out, but at least the doors should work now. [P][EMOTION|Christine|PDU]PDU, please notify Unit 2856 that she can now enter the Gamma Labs.[B][C]") ]])
            fnCutscene([[ Append("PDU:[VOICE|Narrator] Message sent.[P] Reply received.[P] A video call is coming...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] PDU, cancel video call.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Smirk] You enjoy tormenting my sister far too much.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] Just desserts.[P] It's a phrase from Earth.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|PDU] She says she's on her way to Transit Station Omicron.[P] That's east of here.[P] Let's get going.") ]])
            fnCutsceneBlocker()
        
        --SX-399 is in the party.
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
            fnCutscene([[ Append("Christine:[E|Neutral] Well let's see here.[P] I know I'm not much of a repair unit, but I'd say that's your problem right there.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Someone sabotaged the power modulators and stuck a main wire into the water.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] For those of us who are not repair units, please explain.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Are you actually asking about my job?[P] Without already knowing the answer?[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Smirk] And here I thought you knew everything, 55.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Flirt] It's her second best quality.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] And the first?[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Blush] That little smirk she gets when she's figured something out but you haven't...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] I do not 'Know Everything', Christine.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] I research only what I deem to be of vital interest, and download secondary manuals when possible.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] [EMOTION|SX-399|Neutral]I am not familiar with a power modulator unit.[P] Please elaborate.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] They're not that complex.[P] They're an adjustable analog computational unit that stops too much electricity going out to a given wire.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Smirk] They basically prevent power surges from frying equipment.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] If you hook several modulators in sequence, though, they can transmit back up the line to choke voltage at the source.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Normally, if you put a main wire into the water, the modulators would just stop the flow.[P] They were removed, so the power modulators didn't work and all the systems got fried.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] A deliberate act of sabotage, then.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] Not Vivify's modus operandi.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Sad] But the golem we saw at the gala...[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] 20.[P] And she was probably less than half golem.[P] Probably a quarter doll, and the other quarter...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] We are not dealing with a mindless destructive force anymore, Christine.[P] Regulus City's security forces have already been compromised and outmaneuvered.[P] We should assume ambushes and sabotage will become more commonplace, now.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Sad] Isn't she just like us?[P] A rebel?[P] Someone freed from their bondage, and taking revenge on their oppressors?[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Sad] Why is what she's doing different from what we're doing?[P] Shouldn't we be friends with her?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] No.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] We have the same enemy, but not the same goal.[P] 20 doesn't ask, she takes.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] She's not like us at all.[P] We don't...[P] change...[P] others.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Smirk] You seemed pretty excited to hear about Abductions from that Command Unit, Christine.[P] Were you, perhaps, not acting?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Laugh] W-[P]well, I guess you got me there![B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] It's a shame, but I guess I can't compromise my principles.[P] We'll make Abductions an all-volunteer program when we've liberated the city.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] Let me put it this way for you, Sophie.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] We Steam Droids?[P] Nobody's better than someone else.[P] Truly, because if my eyes work, then my ears probably don't.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] So I pair up with someone whose eyes don't work, but their ears do.[P] We gotta team up.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Smirk] Prior to your upgrade, you mean?[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Blush] I still team up with units smarter and cuter than me.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Smirk] Meanwhile, the golems?[P] Someone's on top, someone's on bottom.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] It's a big club, and you ain't in it.[P] So what did that palooka at the gala say?[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] She's mad, since Christine is on top, and she's not.[P] She's okay with there being a top and a bottom, she just wants to be on top.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] Vivify and her gang of weirdos are a lot closer to the golems than the steam droids, if you get me.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Smirk] When you put it like that...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] SX-399 understands intuitively what a rich girl like me had to be taught.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] We're all in it together.[P] You can't watch your own back.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Smirk] Install optical receptors in the rear of your cranial chassis.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Flirt] Not literally, babe.[P] How about::[P] We all gotta sleep sometime.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Traps, turrets, and mines around your defragmentation pod.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Yes, 55.[P] You can be independent, strong, and forced to live in a fortress surrounded by turrets and bombs for fear of having even one friend who can betray you.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] I - [P][CLEAR]") ]])
            fnCutscene([[ Append("55:[E|Down] ... [B][C]") ]])
            fnCutscene([[ Append("55:[E|Down] I see.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Anyway, I cut the wire and sent a reboot order.[P] The modulators should work now.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] The lights are blown out, but at least the doors should work now. [P][EMOTION|Christine|PDU]PDU, please notify Unit 2856 that she can now enter the Gamma Labs.[B][C]") ]])
            fnCutscene([[ Append("PDU:[VOICE|Narrator] Message sent.[P] Reply received.[P] A video call is coming...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] PDU, cancel video call.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Smirk] You enjoy tormenting my sister far too much.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] Just desserts.[P] It's a phrase from Earth.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|PDU] She says she's on her way to Transit Station Omicron.[P] That's east of here.[P] Let's get going.") ]])
            fnCutsceneBlocker()
        
        end
    end
    
-- |[Transformation]|
--Causes Christine to change forms before heading out an airlock.
elseif(sObjectName == "TFToDarkmatter") then
	WD_SetProperty("Hide")
    LM_ExecuteScript(gsRoot .. "Chapter 5/Scenes/100 Transform/Transform_ChristineToDarkmatter/Scene_Begin.lua")
    fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutscene([[ AL_BeginTransitionTo("RegulusBiolabsAlphaD", "FORCEPOS:11.0x8.0x0") ]])
    
elseif(sObjectName == "TFToGolem") then
	WD_SetProperty("Hide")
    LM_ExecuteScript(gsRoot .. "Chapter 5/Scenes/100 Transform/Transform_ChristineToGolem/Scene_Begin.lua")
    fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutscene([[ AL_BeginTransitionTo("RegulusBiolabsAlphaD", "FORCEPOS:11.0x8.0x0") ]])
    
elseif(sObjectName == "TFToLatex") then
	WD_SetProperty("Hide")
    LM_ExecuteScript(gsRoot .. "Chapter 5/Scenes/100 Transform/Transform_ChristineToLatexDrone/Scene_Begin.lua")
    fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutscene([[ AL_BeginTransitionTo("RegulusBiolabsAlphaD", "FORCEPOS:11.0x8.0x0") ]])
    
elseif(sObjectName == "TFToEldritch") then
	WD_SetProperty("Hide")
    LM_ExecuteScript(gsRoot .. "Chapter 5/Scenes/100 Transform/Transform_ChristineToEldritch/Scene_Begin.lua")
    fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutscene([[ AL_BeginTransitionTo("RegulusBiolabsAlphaD", "FORCEPOS:11.0x8.0x0") ]])
    
elseif(sObjectName == "TFToElectrosprite") then
	WD_SetProperty("Hide")
    LM_ExecuteScript(gsRoot .. "Chapter 5/Scenes/100 Transform/Transform_ChristineToElectrosprite/Scene_Begin.lua")
    fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutscene([[ AL_BeginTransitionTo("RegulusBiolabsAlphaD", "FORCEPOS:11.0x8.0x0") ]])

elseif(sObjectName == "TFToSteam") then
	WD_SetProperty("Hide")
    LM_ExecuteScript(gsRoot .. "Chapter 5/Scenes/100 Transform/Transform_ChristineToSteamDroid/Scene_Begin.lua")
    fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutscene([[ AL_BeginTransitionTo("RegulusBiolabsAlphaD", "FORCEPOS:11.0x8.0x0") ]])

elseif(sObjectName == "TFToDoll") then
	WD_SetProperty("Hide")
    LM_ExecuteScript(gsRoot .. "Chapter 5/Scenes/100 Transform/Transform_ChristineToDoll/Scene_Begin.lua")
    fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutscene([[ AL_BeginTransitionTo("RegulusBiolabsAlphaD", "FORCEPOS:11.0x8.0x0") ]])
    
elseif(sObjectName == "NoCancel") then
	WD_SetProperty("Hide")
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
