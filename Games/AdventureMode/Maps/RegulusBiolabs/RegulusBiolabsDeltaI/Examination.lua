-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
-- |[Objects]|
if(sObjectName == "ToGeneticsA") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsGeneticsA", "FORCEPOS:5.5x12.0x0")
    
elseif(sObjectName == "Sign") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Aquatic Genetics Laboratories::[P] Public Visiting Hours, Showtimes', followed by a list of aquatic shows.)") ]])
    
elseif(sObjectName == "Terminal") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (An ordering program.[P] You can input a refreshment and a quantity, and a staff member will deliver it to your table.)") ]])
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
