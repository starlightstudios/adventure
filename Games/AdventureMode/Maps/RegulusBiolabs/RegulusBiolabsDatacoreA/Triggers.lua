-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
if(sObjectName == "ActivateD10") then
    AL_SetProperty("Set Layer Disabled", "OverFloorForD10", false)
    AL_SetProperty("Set Layer Disabled", "OverFloorForD11", false)

elseif(sObjectName == "DeactivateD10") then
    AL_SetProperty("Set Layer Disabled", "OverFloorForD10", true)
    AL_SetProperty("Set Layer Disabled", "OverFloorForD11", true)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iDatacoreNeedsTeleport", "N", 0.0)

--Depth teleporter.
elseif(sObjectName == "DepthTeleport") then

    local iDatacoreNeedsTeleport = VM_GetVar("Root/Variables/Chapter5/Scenes/iDatacoreNeedsTeleport", "N")
    if(iDatacoreNeedsTeleport == 1.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iDatacoreNeedsTeleport", "N", 0.0)
        Cutscene_CreateEvent("ActorEvent", "Actor")
            ActorEvent_SetProperty("Subject Name", "Christine")
            ActorEvent_SetProperty("Teleport To", 38.25 * gciSizePerTile, 13.50 * gciSizePerTile, 1)
        DL_PopActiveObject()
        fnAutoFoldParty()
    end
end
