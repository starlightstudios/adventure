-- |[ ==================================== fnAddRemapping() ==================================== ]|
--Adds a remapping to a sublist.
function fnAddRemapping(psaList, psStartName, psRemapName)
	
    -- |[Argument Check]|
    if(psaList     == nil) then return end
    if(psStartName == nil) then return end
    if(psRemapName == nil) then return end
    
    -- |[Create Remapping]|
	--Baseline List. Used for debug. Makes it easier to diagnose a missing level.
    local zEntry = {}
	zEntry.sStartName = psStartName
	zEntry.sRemapName = psRemapName
    table.insert(psaList, zEntry)
    
    --Add to the master list.
    table.insert(gsaMasterList, zEntry)
end