-- |[ ================================= Build Map Path Remaps ================================== ]|
--Builds a static listing of map path remaps. The editor uses simplified names like "TrapBasementA" to indicate where exits go,
-- and cutscenes use the same logic. Because the maps can be stored in subdirectories, we need to remap the paths to go to 
-- specific subdirectories and do this without having to search for existence, which is slow.
--As such, we build a list of redirections here. These are also useful because Chapter 6 uses different versions of the same
-- maps at certain times to cause special effects, such as the changing seasons.
--IMPORTANT: Once the remappings are built, they should not be reset or modified during level loading. This will cause a crash.
-- Only modify them during an in-map script or before level loading occurs.

-- |[Master List]|
--All additions are added to this list. This is the one used for the C++ code.
gbModHandleRemapAdditions = false
gsaMasterList = {}

--Functions.
if(fnAddRemapping == nil or fnAddRemapsPattern == nil) then
    local sBasePath = fnResolvePath()
    LM_ExecuteScript(sBasePath .. "fnAddRemapping.lua")
    LM_ExecuteScript(sBasePath .. "fnAddRemapsPattern.lua")
end

-- |[ =================================== Chapter 1 Specific =================================== ]|
-- |[Bandit Forest]|
local saCh1BanditForestList = {}
fnAddRemapsPattern(saCh1BanditForestList, "BanditForest",      "Chapter 1/Bandit Forest/BanditForest", "A", "E")
fnAddRemapping(    saCh1BanditForestList, "BanditForestScene", "Chapter 1/Bandit Forest/BanditForestScene")

-- |[Starfield Mausoleum]|
local saCh1StarfieldMausoleumList = {}
fnAddRemapsPattern(saCh1StarfieldMausoleumList, "StarfieldMausoleum", "Chapter 1/Starfield Mausoleum/StarfieldMausoleum", "A", "E")
fnAddRemapsPattern(saCh1StarfieldMausoleumList, "TrannadarDescent",   "Chapter 1/Trannadar Descent/TrannadarDescent",     "A", "C")
fnAddRemapsPattern(saCh1StarfieldMausoleumList, "TrannadarReserve",   "Chapter 1/Trannadar Reserve/TrannadarReserve",     "A", "A")

-- |[ ==================================== Trannadar Region ==================================== ]|
-- |[Debug]|
local saDebugList = {}
fnAddRemapping(saDebugList, "Enemy Testing Range",     "AAA Debug/EnemyTestingRange")
fnAddRemapping(saDebugList, "Enemy Testing Range Ch2", "AAA Debug/EnemyTestingRangeCh2")
fnAddRemapping(saDebugList, "Enemy Testing Range Ch5", "AAA Debug/EnemyTestingRangeCh5")
fnAddRemapping(saDebugList, "NorthwoodsNCA",           "Trafal/Northwoods/NorthwoodsNCA")
fnAddRemapping(saDebugList, "TutorialA",               "AAA Debug/TutorialA")

-- |[Beehive and Dungeon]|
local saBeehiveList = {}
fnAddRemapsPattern(saBeehiveList, "BeehiveBasement",  "Beehive/BeehiveBasement", "A", "F")
fnAddRemapping(saBeehiveList, "BeehiveBasementScene", "Beehive/BeehiveBasementScene")
fnAddRemapping(saBeehiveList, "BeehiveInner",         "Beehive/BeehiveInner")
fnAddRemapping(saBeehiveList, "BeehiveOuter",         "Beehive/BeehiveOuter")

-- |[Trannadar and Evermoon]|
local saTrannadarList = {}
fnAddRemapping(saTrannadarList, "TrannadarTradingPost",  "Evermoon/TrannadarTradingPost")
fnAddRemapping(saTrannadarList, "AlrauneAndBeeScene",    "Evermoon/AlrauneAndBeeScene")
fnAddRemapping(saTrannadarList, "AlrauneChamber",        "Evermoon/AlrauneChamber")
fnAddRemapping(saTrannadarList, "BreannesPitStop",       "Evermoon/BreannesPitStop")
fnAddRemapping(saTrannadarList, "EvermoonCassandraA",    "Evermoon/EvermoonCassandraA")
fnAddRemapping(saTrannadarList, "EvermoonCassandraAMid", "Evermoon/EvermoonCassandraAMid")
fnAddRemapping(saTrannadarList, "EvermoonCassandraB",    "Evermoon/EvermoonCassandraB")
fnAddRemapping(saTrannadarList, "EvermoonCassandraCC",   "Evermoon/EvermoonCassandraCC")
fnAddRemapping(saTrannadarList, "EvermoonCassandraCE",   "Evermoon/EvermoonCassandraCE")
fnAddRemapping(saTrannadarList, "EvermoonCassandraCNE",  "Evermoon/EvermoonCassandraCNE")
fnAddRemapping(saTrannadarList, "EvermoonCassandraCNW",  "Evermoon/EvermoonCassandraCNW")
fnAddRemapping(saTrannadarList, "EvermoonE",             "Evermoon/EvermoonE")
fnAddRemapping(saTrannadarList, "EvermoonNE",            "Evermoon/EvermoonNE")
fnAddRemapping(saTrannadarList, "EvermoonNW",            "Evermoon/EvermoonNW")
fnAddRemapping(saTrannadarList, "EvermoonS",             "Evermoon/EvermoonS")
fnAddRemapping(saTrannadarList, "EvermoonSEA",           "Evermoon/EvermoonSEA")
fnAddRemapping(saTrannadarList, "EvermoonSEB",           "Evermoon/EvermoonSEB")
fnAddRemapping(saTrannadarList, "EvermoonSEC",           "Evermoon/EvermoonSEC")
fnAddRemapping(saTrannadarList, "EvermoonSlimeScene",    "Evermoon/EvermoonSlimeScene")
fnAddRemapping(saTrannadarList, "EvermoonSlimeVillage",  "Evermoon/EvermoonSlimeVillage")
fnAddRemapping(saTrannadarList, "EvermoonSW",            "Evermoon/EvermoonSW")
fnAddRemapping(saTrannadarList, "EvermoonW",             "Evermoon/EvermoonW")
fnAddRemapping(saTrannadarList, "PlainsC",               "Evermoon/PlainsC")
fnAddRemapping(saTrannadarList, "PlainsNW",              "Evermoon/PlainsNW")
fnAddRemapping(saTrannadarList, "SaltFlats",             "Evermoon/SaltFlats")
fnAddRemapping(saTrannadarList, "SaltFlatsCave",         "Evermoon/SaltFlatsCave")
fnAddRemapping(saTrannadarList, "WerecatScene",          "Evermoon/WerecatScene")
fnAddRemapping(saTrannadarList, "Island",                "Evermoon/Island")

-- |[Dimensional Trap Basement]|
local saTrapBasementList = {}
fnAddRemapsPattern(saTrapBasementList, "TrapBasement",                 "TrapBasement/TrapBasement", "A", "H")
fnAddRemapping    (saTrapBasementList, "TrapBasementSecret",           "TrapBasement/TrapBasementSecret")
fnAddRemapping    (saTrapBasementList, "DimensionalTrapBasementScene", "TrapBasement/DimensionalTrapBasementScene")

-- |[Dimensional Trap Dungeon]|
local saTrapDungeonList = {}
fnAddRemapsPattern(saTrapDungeonList, "TrapDungeon",      "TrapDungeon/TrapDungeon", "A", "F")
fnAddRemapping    (saTrapDungeonList, "TrapDungeonEntry", "TrapDungeon/TrapDungeonEntry")

-- |[Dimensional Trap Main Floor and Upper Floor]|
local saDimensionalTrapList = {}
fnAddRemapping(saTrapDungeonList, "TrapMainFloorCentral",   "DimensionalTrap/TrapMainFloorCentral")
fnAddRemapping(saTrapDungeonList, "TrapMainFloorEast",      "DimensionalTrap/TrapMainFloorEast")
fnAddRemapping(saTrapDungeonList, "TrapMainFloorExterior",  "DimensionalTrap/TrapMainFloorExterior")
fnAddRemapping(saTrapDungeonList, "TrapMainFloorExteriorN", "DimensionalTrap/TrapMainFloorExteriorN")
fnAddRemapping(saTrapDungeonList, "TrapMainFloorSouthHall", "DimensionalTrap/TrapMainFloorSouthHall")
fnAddRemapping(saTrapDungeonList, "TrapUpperFloorE",        "DimensionalTrap/TrapUpperFloorE")
fnAddRemapping(saTrapDungeonList, "TrapUpperFloorMain",     "DimensionalTrap/TrapUpperFloorMain")
fnAddRemapping(saTrapDungeonList, "TrapUpperFloorW",        "DimensionalTrap/TrapUpperFloorW")

-- |[Arbonne]|
local saArbonneList = {}
fnAddRemapsPattern(saArbonneList, "ArbonnePlains", "ArbonnePlains/ArbonnePlains", "A", "D")

-- |[Starfield Caves]|
local saStarfieldCaveList = {}
fnAddRemapsPattern(saStarfieldCaveList, "StarfieldCaves", "StarfieldCaves/StarfieldCaves", "A", "C")

-- |[Starfield Swamp]|
local saStarfieldList = {}
fnAddRemapsPattern(saStarfieldList, "StarfieldSwamp", "StarfieldSwamp/StarfieldSwamp", "A", "J")
fnAddRemapsPattern(saStarfieldList, "StarfieldCaves", "StarfieldCaves/StarfieldCaves", "A", "A")
fnAddRemapsPattern(saStarfieldList, "WisphagSwamp",   "StarfieldSwamp/WisphagSwamp",   "A", "C")

-- |[St. Fora's]|
local saStForasList = {}
fnAddRemapsPattern(saStForasList, "StForas", "StForas/StForas", "A", "I")

-- |[River Wilds]|
local saRiverWildsList = {}
fnAddRemapsPattern(saRiverWildsList, "RiverWilds",           "RiverWilds/RiverWilds", "A", "D")
fnAddRemapsPattern(saRiverWildsList, "WildsTower",           "RiverWilds/WildsTower", "A", "E")
fnAddRemapping(    saRiverWildsList, "DrinkingContest",      "RiverWilds/DrinkingContest")
fnAddRemapping(    saRiverWildsList, "FishingVillageRubber", "RiverWilds/FishingVillageRubber")

-- |[ ====================================== Void Region ======================================= ]|
-- |[Nix Nedar]|
local saNixNedarList = {}
fnAddRemapping(saNixNedarList, "NixNedarHouseEast",        "NixNedar/NixNedarHouseEast")
fnAddRemapping(saNixNedarList, "NixNedarHousePath",        "NixNedar/NixNedarHousePath")
fnAddRemapping(saNixNedarList, "NixNedarHouseSouthcenter", "NixNedar/NixNedarHouseSouthcenter")
fnAddRemapping(saNixNedarList, "NixNedarHouseSouthwest",   "NixNedar/NixNedarHouseSouthwest")
fnAddRemapping(saNixNedarList, "NixNedarMain",             "NixNedar/NixNedarMain")
fnAddRemapping(saNixNedarList, "NixNedarMaramHouse",       "NixNedar/NixNedarMaramHouse")
fnAddRemapping(saNixNedarList, "NixNedarSeptimaHouse",     "NixNedar/NixNedarSeptimaHouse")
fnAddRemapping(saNixNedarList, "NixNedarSouthPath",        "NixNedar/NixNedarSouthPath")
fnAddRemapping(saNixNedarList, "NixNedarSouthPathWander",  "NixNedar/NixNedarSouthPathWander")

-- |[ ===================================== Quantir Region ===================================== ]|
-- |[Quantir High Wastes]|
local saQuantirList = {}
fnAddRemapping(saQuantirList, "QuantirNW",     "Quantir/QuantirNW")
fnAddRemapping(saQuantirList, "QuantirNWCave", "Quantir/QuantirNWCave")

-- |[Quantir Mansion]|
local saQuantirManseList = {}
fnAddRemapping(saQuantirManseList, "QuantirManseBasementE",      "QuantirManse/QuantirManseBasementE")
fnAddRemapping(saQuantirManseList, "QuantirManseBasementW",      "QuantirManse/QuantirManseBasementW")
fnAddRemapping(saQuantirManseList, "QuantirManseCentralE",       "QuantirManse/QuantirManseCentralE")
fnAddRemapping(saQuantirManseList, "QuantirManseCentralW",       "QuantirManse/QuantirManseCentralW")
fnAddRemapping(saQuantirManseList, "QuantirManseEntrance",       "QuantirManse/QuantirManseEntrance")
fnAddRemapping(saQuantirManseList, "QuantirManseNEHall",         "QuantirManse/QuantirManseNEHall")
fnAddRemapping(saQuantirManseList, "QuantirManseNEHallFloor2",   "QuantirManse/QuantirManseNEHallFloor2")
fnAddRemapping(saQuantirManseList, "QuantirManseNWHall",         "QuantirManse/QuantirManseNWHall")
fnAddRemapping(saQuantirManseList, "QuantirManseSecretExit",     "QuantirManse/QuantirManseSecretExit")
fnAddRemapping(saQuantirManseList, "QuantirManseSEHall",         "QuantirManse/QuantirManseSEHall")
fnAddRemapping(saQuantirManseList, "QuantirManseSWYard",         "QuantirManse/QuantirManseSWYard")
fnAddRemapping(saQuantirManseList, "QuantirManseSWYardInterior", "QuantirManse/QuantirManseSWYardInterior")
fnAddRemapping(saQuantirManseList, "QuantirManseTruth",          "QuantirManse/QuantirManseTruth")
fnAddRemapping(saQuantirManseList, "SpookyExterior",             "QuantirManse/SpookyExterior")

-- |[ ===================================== Regulus Region ===================================== ]|
-- |[Regulus Cryogenics Facility]|
local saRegulusCryoList = {}
fnAddRemapsPattern(saRegulusCryoList, "RegulusCryo",              "RegulusCryo/RegulusCryo",              "A", "G")
fnAddRemapsPattern(saRegulusCryoList, "RegulusCryoLower",         "RegulusCryo/RegulusCryoLower",         "A", "D")
fnAddRemapsPattern(saRegulusCryoList, "RegulusCryoContainment",   "RegulusCryo/RegulusCryoContainment",   "A", "B")
fnAddRemapsPattern(saRegulusCryoList, "RegulusCryoToFabrication", "RegulusCryo/RegulusCryoToFabrication", "A", "B")
fnAddRemapsPattern(saRegulusCryoList, "RegulusCryoCommand",       "RegulusCryo/RegulusCryoCommand",       "A", "C")
fnAddRemapsPattern(saRegulusCryoList, "RegulusCryoSouth",         "RegulusCryo/RegulusCryoSouth",         "A", "G")
fnAddRemapsPattern(saRegulusCryoList, "RegulusCryoPowerCore",     "RegulusCryo/RegulusCryoPowerCore",     "A", "E")

-- |[Regulus Exterior]|
local saRegulusExteriorList = {}
fnAddRemapsPattern(saRegulusExteriorList, "RegulusExteriorE",        "RegulusExterior/RegulusExteriorE", "A", "F")
fnAddRemapsPattern(saRegulusExteriorList, "RegulusExteriorS",        "RegulusExterior/RegulusExteriorS", "A", "E")
fnAddRemapsPattern(saRegulusExteriorList, "RegulusExteriorW",        "RegulusExterior/RegulusExteriorW", "A", "C")
fnAddRemapping    (saRegulusExteriorList, "RegulusExteriorStationE", "RegulusExterior/RegulusExteriorStationE")
fnAddRemapping    (saRegulusExteriorList, "RegulusExteriorWAUp",     "RegulusExterior/RegulusExteriorWAUp")
fnAddRemapping    (saRegulusExteriorList, "RegulusExteriorTRNA",     "RegulusExterior/RegulusExteriorTRNA")
fnAddRemapping    (saRegulusExteriorList, "RegulusExteriorTRNB",     "RegulusExterior/RegulusExteriorTRNB")

-- |[Serenity Crater and Observatory]|
local saRegulusSerenityList = {}
fnAddRemapsPattern(saRegulusSerenityList, "SerenityCrater",      "RegulusSerenity/SerenityCrater",      "A", "H")
fnAddRemapsPattern(saRegulusSerenityList, "SerenityObservatory", "RegulusSerenity/SerenityObservatory", "A", "E")

-- |[Regulus City]|
local saRegulusCityList = {}
fnAddRemapsPattern(saRegulusCityList, "RegulusCity15",     "RegulusCity/RegulusCity15",    "A", "C")
fnAddRemapsPattern(saRegulusCityList, "RegulusCity119",    "RegulusCity/RegulusCity119",   "A", "B")
fnAddRemapsPattern(saRegulusCityList, "RegulusCity198",    "RegulusCity/RegulusCity198",   "A", "D")
fnAddRemapsPattern(saRegulusCityList, "RegulusCity",       "RegulusCity/RegulusCity",      "A", "G")
fnAddRemapsPattern(saRegulusCityList, "LowerRegulusCity",  "RegulusCity/LowerRegulusCity", "A", "D")
fnAddRemapping    (saRegulusCityList, "RegulusCityX",      "RegulusCity/RegulusCityX")
fnAddRemapping    (saRegulusCityList, "RegulusCityZ",      "RegulusCity/RegulusCityZ")
fnAddRemapping    (saRegulusCityList, "RegulusCityBAlt",   "RegulusCity/RegulusCityBAlt")

--Cannot be visited normally. Used for the dating montage.
fnAddRemapping(saRegulusCityList, "RegulusCityWholeCutscene", "RegulusCity/RegulusCityWholeCutscene")

-- |[Regulus Equinox Facility]|
local saRegulusEquinoxList = {}
fnAddRemapsPattern(saRegulusEquinoxList, "RegulusEquinox", "RegulusEquinox/RegulusEquinox", "A", "H")

-- |[Regulus Long-Range Telemetry Facility]|
local saRegulusLRTList = {}
fnAddRemapsPattern(saRegulusLRTList, "RegulusLRT",   "RegulusLRT/RegulusLRT", "A", "H")
fnAddRemapping    (saRegulusLRTList, "RegulusLRTEA", "RegulusLRT/RegulusLRTEA")
fnAddRemapping    (saRegulusLRTList, "RegulusLRTGX", "RegulusLRT/RegulusLRTGX")
fnAddRemapping    (saRegulusLRTList, "RegulusLRTGZ", "RegulusLRT/RegulusLRTGZ")
fnAddRemapsPattern(saRegulusLRTList, "RegulusLRTH",  "RegulusLRT/RegulusLRTH", "A", "F")
fnAddRemapsPattern(saRegulusLRTList, "RegulusLRTI",  "RegulusLRT/RegulusLRTI", "A", "G")
fnAddRemapping    (saRegulusLRTList, "RegulusLRTZA", "RegulusLRT/RegulusLRTZA")
fnAddRemapping    (saRegulusLRTList, "RegulusLRTZB", "RegulusLRT/RegulusLRTZB")

-- |[Regulus Manufactory]|
local saRegulusManufactoryList = {}
fnAddRemapsPattern(saRegulusManufactoryList, "RegulusManufactory", "RegulusManufactory/RegulusManufactory", "A", "J")

-- |[Regulus Mines]|
local saRegulusMinesList = {}
fnAddRemapsPattern(saRegulusMinesList, "TelluriumMines",     "RegulusMines/TelluriumMines", "A", "I")
fnAddRemapsPattern(saRegulusMinesList, "SprocketCity",       "RegulusMines/SprocketCity",   "A", "B")
fnAddRemapsPattern(saRegulusMinesList, "BlackSite",          "RegulusMines/BlackSite",      "A", "D")
fnAddRemapsPattern(saRegulusMinesList, "MinesRandom",        "RegulusMines/MinesRandom",    "A", "D")
fnAddRemapsPattern(saRegulusMinesList, "RegulusMinesCaves",  "Chapter 5/RegulusMinesCaves/RegulusMinesCaves",   "A", "I")
fnAddRemapsPattern(saRegulusMinesList, "RegulusMinesRunoff", "Chapter 5/RegulusMinesRunoff/RegulusMinesRunoff", "A", "I")

-- |[Sector 254]|
local saRegulusSector254List = {}
fnAddRemapsPattern(saRegulusSector254List, "Sector254", "Chapter 5/Sector254/Sector254", "A", "D")

-- |[Arcane Academy]|
local saArcaneAcademyList = {}
fnAddRemapsPattern(saArcaneAcademyList, "RegulusArcane",     "RegulusArcane/RegulusArcane", "A", "H")
fnAddRemapping    (saArcaneAcademyList, "RegulusArcaneCAlt", "RegulusArcane/RegulusArcaneCAlt")

-- |[Biolabs]|
local saBiolabsList = {}
fnAddRemapping(saBiolabsList, "RegulusBiolabsA", "RegulusBiolabs/RegulusBiolabsAlphaA")
fnAddRemapsPattern(saBiolabsList, "RegulusBiolabsAlpha",       "RegulusBiolabs/RegulusBiolabsAlpha",       "A", "D")
fnAddRemapsPattern(saBiolabsList, "RegulusBiolabsAmphibian",   "RegulusBiolabs/RegulusBiolabsAmphibian",   "A", "E")
fnAddRemapsPattern(saBiolabsList, "RegulusBiolabsBeta",        "RegulusBiolabs/RegulusBiolabsBeta",        "A", "E")
fnAddRemapsPattern(saBiolabsList, "RegulusBiolabsBetaMelted",  "RegulusBiolabs/RegulusBiolabsBetaMelted",  "A", "E")
fnAddRemapsPattern(saBiolabsList, "RegulusBiolabsDatacore",    "RegulusBiolabs/RegulusBiolabsDatacore",    "A", "H")
fnAddRemapsPattern(saBiolabsList, "RegulusBiolabsDelta",       "RegulusBiolabs/RegulusBiolabsDelta",       "A", "I")
fnAddRemapsPattern(saBiolabsList, "RegulusBiolabsEpsilon",     "RegulusBiolabs/RegulusBiolabsEpsilon",     "A", "E")
fnAddRemapsPattern(saBiolabsList, "RegulusBiolabsGamma",       "RegulusBiolabs/RegulusBiolabsGamma",       "A", "D")
fnAddRemapsPattern(saBiolabsList, "RegulusBiolabsGammaWest",   "RegulusBiolabs/RegulusBiolabsGammaWest",   "A", "E")
fnAddRemapsPattern(saBiolabsList, "RegulusBiolabsGenetics",    "RegulusBiolabs/RegulusBiolabsGenetics",    "A", "H")
fnAddRemapsPattern(saBiolabsList, "RegulusBiolabsHydroponics", "RegulusBiolabs/RegulusBiolabsHydroponics", "A", "D")
fnAddRemapsPattern(saBiolabsList, "RegulusBiolabsRaijuRanch",  "RegulusBiolabs/RegulusBiolabsRaijuRanch",  "A", "C")
fnAddRemapsPattern(saBiolabsList, "RegulusBiolabsTransit",     "RegulusBiolabs/RegulusBiolabsTransit",     "A", "B")
fnAddRemapsPattern(saBiolabsList, "RegulusBiolabsMovie",       "RegulusBiolabs/RegulusBiolabsMovie",       "A", "G")

-- |[Flashback and Finale]|
local saFlashbackList = {}
fnAddRemapsPattern(saFlashbackList, "RegulusFlashback", "RegulusFlashback/RegulusFlashback", "A", "L")
fnAddRemapsPattern(saFlashbackList, "RegulusFinale",    "RegulusFinale/RegulusFinale",       "A", "H")

-- |[ ===================================== Trafal Region ====================================== ]|
-- |[Trafal Glacier]|
local saTrafalList = {}
fnAddRemapping(saTrafalList, "TrafalNW", "Trafal/TrafalNW")

-- |[Shrine of the Hero]|
local saShrineList = {}
fnAddRemapsPattern(saShrineList, "LowerShrine", "Trafal/ShrineHero/LowerShrine", "A", "H")
fnAddRemapsPattern(saShrineList, "HeroShrine",  "Trafal/ShrineHero/HeroShrine",  "A", "E")
fnAddRemapsPattern(saShrineList, "HeroPlateau", "Trafal/ShrineHero/HeroPlateau", "A", "C")

-- |[Northwoods]|
local saNorthwoodsList = {}
fnAddRemapsPattern(saNorthwoodsList, "NorthwoodsNC", "Trafal/Northwoods/NorthwoodsNC", "A", "D")
fnAddRemapsPattern(saNorthwoodsList, "NorthwoodsNE", "Trafal/Northwoods/NorthwoodsNE", "A", "E")
fnAddRemapsPattern(saNorthwoodsList, "NorthwoodsNW", "Trafal/Northwoods/NorthwoodsNW", "A", "F")
fnAddRemapsPattern(saNorthwoodsList, "NorthwoodsSC", "Trafal/Northwoods/NorthwoodsSC", "A", "E")
fnAddRemapsPattern(saNorthwoodsList, "NorthwoodsSE", "Trafal/Northwoods/NorthwoodsSE", "A", "D")
fnAddRemapsPattern(saNorthwoodsList, "NorthwoodsSW", "Trafal/Northwoods/NorthwoodsSW", "A", "D")

-- |[Westwoods]|
local saWestwoodsList = {}
fnAddRemapsPattern(saWestwoodsList, "WestwoodsC",  "Trafal/Westwoods/WestwoodsC",  "A", "G")
fnAddRemapsPattern(saWestwoodsList, "WestwoodsE",  "Trafal/Westwoods/WestwoodsE",  "A", "G")
fnAddRemapsPattern(saWestwoodsList, "WestwoodsN",  "Trafal/Westwoods/WestwoodsN",  "A", "F")
fnAddRemapsPattern(saWestwoodsList, "WestwoodsSE", "Trafal/Westwoods/WestwoodsSE", "A", "C")
fnAddRemapsPattern(saWestwoodsList, "WestwoodsW",  "Trafal/Westwoods/WestwoodsW",  "A", "D")

-- |[Mt. Sarulente]|
local saSarulenteList = {}
fnAddRemapsPattern(saSarulenteList, "Sarulente",        "Trafal/MtSarulente/Sarulente",        "A", "G")
fnAddRemapsPattern(saSarulenteList, "FortSarulente",    "Trafal/MtSarulente/FortSarulente",    "A", "C")
fnAddRemapsPattern(saSarulenteList, "EmpressCave",      "Trafal/MtSarulente/EmpressCave",      "A", "D")
fnAddRemapsPattern(saSarulenteList, "SarulenteLibrary", "Trafal/MtSarulente/SarulenteLibrary", "A", "A")

-- |[Sanya's Cabin / Northwoods Mines]|
local saSanyaCabinList = {}
fnAddRemapsPattern(saSanyaCabinList, "SanyaCabin", "Trafal/NorthwoodsMines/SanyaCabin", "A", "B")
fnAddRemapsPattern(saSanyaCabinList, "SanyaMines", "Trafal/NorthwoodsMines/SanyaMines", "A", "G")

-- |[Granvire Pass]|
local saGranvirePassList = {}
fnAddRemapsPattern(saGranvirePassList, "GranvirePass", "Trafal/GranvirePass/GranvirePass", "A", "D")

-- |[Vuca Pass]|
local saVucaPassList = {}
fnAddRemapsPattern(saVucaPassList, "VucaPass",       "Trafal/VucaPass/VucaPass",       "A", "D")
fnAddRemapsPattern(saVucaPassList, "KitsuneVillage", "Trafal/VucaPass/KitsuneVillage", "A", "M")

-- |[Harpy Base]|
local saHarpyBaseList = {}
fnAddRemapsPattern(saHarpyBaseList, "HarpyBase", "Trafal/HarpyBase/HarpyBase", "A", "C")

-- |[Warrens]|
local saWarrensList = {}
fnAddRemapsPattern(saWarrensList, "Warrens",         "Trafal/Warrens/Warrens",     "A", "D")
fnAddRemapsPattern(saWarrensList, "WarrensCave",     "Trafal/Warrens/WarrensCave", "A", "B")
fnAddRemapping    (saWarrensList, "WarrensCaveAAlt", "Trafal/Warrens/WarrensCaveAAlt")

-- |[ ======================================= Mod Pieces ======================================= ]|
--If a mod folder is detected, execute its map path remaps script.
fnExecModScript("Maps/Map Path Remaps.lua")

-- |[ ================================== Finalize and Upload =================================== ]|
-- |[Finalize]|
--Using the master list, upload this to the C++ code.
AL_SetProperty("Remappings Total", #gsaMasterList)
for i = 1, #gsaMasterList, 1 do
	AL_SetProperty("Remapping", i-1, gsaMasterList[i].sStartName, gsaMasterList[i].sRemapName)
end

-- |[Clean]|
--Deallocate these functions.
fnAddRemapping = nil
fnAddRemapsPattern = nil

