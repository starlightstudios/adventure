-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Objects]|
--Cabinet.
if(sObjectName == "Cabinet") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Mei](The cabinet is full of glassware.[P] Some of it appears to have bite marks taken out of it...)") ]])
	
--Clothes.
elseif(sObjectName == "Clothes") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Mei](A wide range of grey, black, and white dresses.[P] I wonder how I'd look in these?)") ]])
	
--Bookshelves.
elseif(sObjectName == "Bookshelves") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Mei](Seems the entire bookshelf is dedicated to books about stars.[P] There are five universes worth of stars to memorize...)") ]])

-- |[Exits]|
--Back to the main town.
elseif(sObjectName == "Exit") then
	AudioManager_PlaySound("World|FlipSwitch")
	AL_BeginTransitionTo("NixNedarMain", "FORCEPOS:5x28x0")
	
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
