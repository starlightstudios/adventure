-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

--Clear censor bars.
WD_SetProperty("Clear Censor Bars")

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
    
    -- |[Name Resolve]|
    --Resolve the name of the calling entity.
    local sActorName = TA_GetProperty("Name")
	
    -- |[Septima]|
    if(sActorName == "Septima") then
	
        --Set facing.
        TA_SetProperty("Face Character", "PlayerEntity")
        
        --This is always a major dialogue.
        fnStandardMajorDialogue()
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Septima", "NeutralUp") ]])
        
        --First-time dialogue.
        local iTalkedSeptima = VM_GetVar("Root/Variables/Chapter1/Scenes/iTalkedSeptima", "N")
        if(iTalkedSeptima == 0.0) then
            VM_SetVar("Root/Variables/Chapter1/Scenes/iTalkedSeptima", "N", 1.0)
            fnCutscene([[ Append("Septima: We have much to discuss, Voidwalker.[P] Surely you are brimming with inquiry.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] I am -[P] but it's Mei, not Shirley.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] Let's start with the big one.[P] How do I get home?[B][C]") ]])
            fnCutscene([[ Append("Septima: You cannot.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Angry] WHAT!?[B][C]") ]])
            fnCutscene([[ Append("Septima: Please, calm yourself.[P] I do not place malice behind my words.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Offended] You've got to be kidding me![P] You brought me here - [P][CLEAR]") ]])
            fnCutscene([[ Append("Septima: Apologies, but we did no such thing.[P] The Rilmani have little power over The Still Plane.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Offended] But you clearly can teleport with mirrors and - [P][C]") ]])
            fnCutscene([[ Append("Septima: Yes, we move between dimensions.[P] But only between the nearest four.[P] The Still Plane is unlike the others.[B][C]") ]])
            fnCutscene([[ Append("Septima: Mei, you have been brought here for a purpose.[P] It is a great one, and it is also why you cannot return to Earth.[P] Now, or possibly ever.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Offended] I don't care about some riddles!") ]])
            fnCutsceneBlocker()
            
            --Stop the music, reboot the dialogue.
            fnCutscene([[ AL_SetProperty("Music", "Null") ]])
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Septima", "Neutral") ]])
            
            fnCutscene([[ Append("Septima: The universe will end if you return to Earth.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Surprise] Oh...[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] Okay, you have my attention.[B][C]") ]])
            fnCutscene([[ Append("Septima: There is a creature of unconscionable power, who predates the existence of reality itself.[P] It rivals the True God, or possibly is like the True God.[B][C]") ]])
            fnCutscene([[ Append("Septima: That creature is called Nemesis in our language, for we dare not utter his true name.[P] That creature's power is bound by your runestone.[B][C]") ]])
            fnCutscene([[ Append("Septima: If you were drawn to Pandemonium, it can only mean that he has finally found a way to regain the power locked away by your stone.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Offended] The runestone brought me here?[P] But can't it take me back, then?[B][C]") ]])
            fnCutscene([[ Append("Septima: The six sealing runes were sent to The Still Plane when the universe was split into five.[P] The Still Plane, where you are from, is isolated.[B][C]") ]])
            fnCutscene([[ Append("Septima: No creature can travel between the two, meaning Nemesis' power was safely locked away from him.[B][C]") ]])
            fnCutscene([[ Append("Septima: You exist for a reason, Mei.[P] Your reason is the rune's reason::[P] To keep Nemesis from breaking his seal.[P] If he returns, he will reduce the universe to entropic uniformity.[B][C]") ]])
            fnCutscene([[ Append("Septima: Your runestone exists to seal his power.[P] If he has regained it, you exist to restore the seal.[P] That is why you are here.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Sad] But I'm just a waitress...[B][C]") ]])
            fnCutscene([[ Append("Septima: Yes, you are.[P] You have been called upon to do the extraordinary.[B][C]") ]])
            fnCutscene([[ Append("Septima: You are not alone.[P] We, the Rilmani, will aid you to the best of our ability.[B][C]") ]])
            fnCutscene([[ Append("Septima: I have observed you in your daily life.[P] You are a good, kind, strong person.[P] You will succeed.[P] I have confidence in you.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Sad] Well that makes one of us...[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] This is all so much to take in.[P] I keep thinking this must be some terrible dream.[B][C]") ]])
            fnCutscene([[ Append("Septima: The return of Nemesis is beyond the most horrendous of scenarios.[P] To even look upon his form is to know depthless fear.[B][C]") ]])
            fnCutscene([[ Append("Septima: But you, Mei, will be *his* nightmare.[P] I will personally train you.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Sad] I just want to go home...[B][C]") ]])
            fnCutscene([[ Append("Septima: We will teach you to use the scrying mirror.[P] You will be able to see your family, though you will not be able to talk to them.[B][C]") ]])
            fnCutscene([[ Append("Septima: Truly, Mei, I am sorry that this is the situation you find yourself in.[P] Every effort will be made to comfort you.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Sad] Well, if you say there's no way back -[P] I can't really tell if you're lying.[P] I've no choice but to believe you.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] And -[P] there's only one way to go::[P] forward.[B][C]") ]])
            fnCutscene([[ Append("Septima: Very well.[P] Do you have any further questions?[P] Or, shall we begin?[BLOCK]") ]])

            --Decision script is this script. It must be surrounded by quotes.
            local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
            fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"I have some questions...\", " .. sDecisionScript .. ", \"Questions\") ")
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Let's do this.\",  " .. sDecisionScript .. ", \"Training\") ")
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"I want to look around.\",  " .. sDecisionScript .. ", \"Leave\") ")
            fnCutsceneBlocker()

        --Normal.
        else
            fnCutscene([[ Append("Septima: Hello again, Mei.[P] Did you have more questions, or are you ready to begin your training?[BLOCK]") ]])

            --Decision script is this script. It must be surrounded by quotes.
            local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
            fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"I have some questions...\", " .. sDecisionScript .. ", \"Questions\") ")
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Let's do this.\",  " .. sDecisionScript .. ", \"Training\") ")
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"I want to look around.\",  " .. sDecisionScript .. ", \"Leave\") ")
            fnCutsceneBlocker()
        end
    end
    
-- |[ ================================== Responses to Septima ================================== ]|
--Activates topics mode.
elseif(sTopicName == "Questions") then

	--Restart the music.
	fnCutscene([[ AL_SetProperty("Music", "NixNedar") ]])

	--Unlock Septima's topics.
	WD_SetProperty("Unlock Topic", "Name", 1)
	WD_SetProperty("Unlock Topic", "NewRilmani", 1)
	WD_SetProperty("Unlock Topic", "PrimaryWords", 1)
	WD_SetProperty("Unlock Topic", "Rilmani", 1)
	WD_SetProperty("Unlock Topic", "RilmaniLanguage", 1)
	WD_SetProperty("Unlock Topic", "Runestone", 1)
	WD_SetProperty("Unlock Topic", "StillPlane", 1)
	WD_SetProperty("Unlock Topic", "Voidwalker", 1)

	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Septima", "Neutral") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] There's a few things you could help clear up.[B][C]") ]])
	fnCutscene([[ Append("Septima: What would you like to know?[B][C]") ]])
	
	--Activate topics mode once the dialogue is complete.
	fnCutscene([[ WD_SetProperty("Activate Topics After Dialogue", "Septima") ]])

--End the chapter.
elseif(sTopicName == "Training") then

	--Restart the music.
	fnCutscene([[ AL_SetProperty("Music", "NixNedar") ]])
    
    --Chapter Complete!
    VM_SetVar("Root/Variables/Global/ChapterComplete/iChapter1", "N", 1.0)
    
    -- |[Achievement]|
    AM_SetPropertyJournal("Unlock Achievement", "FinishChapter1")

	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Septima", "Neutral") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Let's do this thing.[P] What do we do first?[B][C]") ]])
	fnCutscene([[ Append("Septima: First, we will teach you sword techniques, and the way of the void.[B][C]") ]])
	fnCutscene([[ Append("Septima: You have natural strength and grace.[P] With my teaching, you will master the Rilmani form of combat.[B][C]") ]])
	fnCutscene([[ Append("Septima: Your room is right over here.[P] Please, get comfortable.[P] I will see about getting you some fresh clothes.") ]])
	fnCutsceneBlocker()
	
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneBlocker()
	fnCutsceneWait(120)
	fnCutsceneBlocker()
	
	--Reboot the dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene") ]])
	fnCutscene([[ Append("And so, Mei, Voidwalker, bearer of courage, would train with Septima to learn the ways of the Rilmani.[B][C]") ]])
	fnCutscene([[ Append("Her story does not end here, but this chapter does.[B][C]") ]])
	fnCutscene([[ Append("What will happen to her?[P] And what of the other five?[P] Sanya, Jeanne, Lotta, Christine, Talia?[B][C]") ]])
	fnCutscene([[ Append("Chapters 2-5 unlocked...[P] Is what I'd say if they were complete.[P] In this version, Chapter 5 is playable, and Chapter 2 is partially completed (make a save!)") ]])
	fnCutsceneBlocker()
	
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
    --Execute the cleaner script.
	fnCutscene([[ LM_ExecuteScript(gsRoot .. "Chapter 1/Cleanup/000 Entry Point.lua") ]])
	fnCutsceneBlocker()
    
    --Return to the "Nowhere" map.
    fnCutscene([[ AL_BeginTransitionTo("Nowhere", "FORCEPOS:10.0x2.0x0") ]])
    fnCutsceneBlocker()

--Go wander a bit.
elseif(sTopicName == "Leave") then

	--Restart the music.
	fnCutscene([[ AL_SetProperty("Music", "NixNedar") ]])

	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Septima", "Neutral") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] I'm not done exploring yet.[P] Might as well get to know the place if I'm going to stay here.[B][C]") ]])
	fnCutscene([[ Append("Septima: Certainly.[P] If you've not done so, please speak to Maram.[P] She has been waiting to see you.") ]])
	fnCutsceneBlocker()
end
