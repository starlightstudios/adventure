-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
--House.
if(sObjectName == "PathHouseDoor") then
	
	--Variables.
	local iMetSeptima = VM_GetVar("Root/Variables/Chapter1/Scenes/iMetSeptima", "N")
	
	--Haven't talked to Septima yet.
	if(iMetSeptima == 0.0) then
		AudioManager_PlaySound("World|RemoteDoor")
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[VOICE|Mei](The door doesn't open.[P] Maybe I should check back later?)") ]])
	
	--Enter the house.
	else
		AudioManager_PlaySound("World|FlipSwitch")
		AL_BeginTransitionTo("NixNedarHousePath", "Null")
	end
	
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
