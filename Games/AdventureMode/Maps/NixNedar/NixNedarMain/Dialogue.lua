-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[Rilmani A]|
    if(sActorName == "RilmaniA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnStandardDialogue("Rilmani:[VOICE|Septima] The voidwalker...[P] It is an honor to meet you.")
        
    -- |[Rilmani B]|
    elseif(sActorName == "RilmaniB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnStandardDialogue("Rilmani:[VOICE|Septima] Greetings, Voidwalker.[P] I hope you are in good spirits.")
        
    -- |[Rilmani C]|
    elseif(sActorName == "RilmaniC") then
        fnStandardDialogue("Rilmani:[VOICE|Septima] *Oh, the voidwalker...[P] did she hear us?*")
    
    -- |[Rilmani D]|
    elseif(sActorName == "RilmaniD") then
        fnStandardDialogue("Rilmani:[VOICE|Septima] *I'm so embarrassed!*")
    
    -- |[Rilmani E]|
    elseif(sActorName == "RilmaniE") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnStandardDialogue("Rilmani:[VOICE|Septima] I sensed movement between the planes earlier, but could find no rifts.[P] Was it you, perhaps?")
    
    -- |[Rilmani F]|
    elseif(sActorName == "RilmaniF") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Rilmani:[VOICE|Septima] Look there, Voidwalker.[P] That star that shines across the cleft of realities is one from your home.[P] Sirius, I believe you call it.[P] Does that comfort you?[B][C]") ]])
        fnCutscene([[ Append("Mei:[VOICE|Mei] It...[P] does, oddly.[P] I don't feel as lonely, now.[B][C]") ]])
        fnCutscene([[ Append("Rilmani:[VOICE|Septima] You are among friends here.[P] If we can help you feel at ease, we will do what we can.") ]])
    
    -- |[Rilmani G]|
    elseif(sActorName == "RilmaniG") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnStandardDialogue("Rilmani:[VOICE|Septima] Objects sometimes fall between realities, and end up here.[P] For example, I recently found several contact lenses floating in the infinite nothing.")
    
    -- |[Rilmani Z]|
    elseif(sActorName == "RilmaniZ") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnStandardDialogue("Rilmani:[VOICE|Septima] Welcome to Nix Nedar.[P] Did you see the void gardens on your way here?[P] Are they not magnificent?")
    end
end
