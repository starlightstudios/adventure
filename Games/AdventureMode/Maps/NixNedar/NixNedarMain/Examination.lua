-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
--Septima's house.
if(sObjectName == "SeptimaDoor") then
	AudioManager_PlaySound("World|FlipSwitch")
	AL_BeginTransitionTo("NixNedarSeptimaHouse", "Null")

--Maram's house.
elseif(sObjectName == "MaramDoor") then
	AudioManager_PlaySound("World|FlipSwitch")
	AL_BeginTransitionTo("NixNedarMaramHouse", "Null")

--East-side cafeteria.
elseif(sObjectName == "EDoor") then
	AudioManager_PlaySound("World|FlipSwitch")
	AL_BeginTransitionTo("NixNedarHouseEast", "Null")

--South-center house.
elseif(sObjectName == "SCDoor") then
	AudioManager_PlaySound("World|FlipSwitch")
	AL_BeginTransitionTo("NixNedarHouseSouthcenter", "Null")

--South-west house.
elseif(sObjectName == "SWDoor") then
	AudioManager_PlaySound("World|FlipSwitch")
	AL_BeginTransitionTo("NixNedarHouseSouthwest", "Null")
	
--South-east house.
elseif(sObjectName == "SEDoor") then

	--Sound effect.
	AudioManager_PlaySound("World|Knock")
	fnCutsceneWait(35)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Rilmani:[VOICE|Septima] ..![P] Don't come in, I'm changing![B][C]Rilmani:[VOICE|Septima] *Oh, should I wear the frilly white outfit or the gothic black outfit?*") ]])

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
