-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Objects]|
--Corgi statue!
if(sObjectName == "Corgi") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Mei](A statue of a...[P] corgi...)") ]])
	
--Bookshelf
elseif(sObjectName == "Bookshelf") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Mei: [VOICE|Mei]('Monochrome Make-up Monthly:: Volume 0.'[P] Not a combat-oriented book, but apparently one book was not enough for wearing black and white make-up!)") ]])

-- |[Exits]|
--Back to the main town.
elseif(sObjectName == "Exit") then
	AudioManager_PlaySound("World|FlipSwitch")
	AL_BeginTransitionTo("NixNedarMain", "FORCEPOS:16x25x0")
	
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
