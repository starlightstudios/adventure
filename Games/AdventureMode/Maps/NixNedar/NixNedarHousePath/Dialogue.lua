-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[Rilmani]|
    if(sActorName == "Rilmani") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnStandardDialogue("Rilmani:[VOICE|Septima] Oh, was that you outside earlier?[P] Welcome to my home.[B][C]")
        fnStandardDialogue("Rilmani:[VOICE|Septima] I value my privacy, but my door is always open to the Voidwalker.")
    end
end
