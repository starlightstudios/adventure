-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[Maram]|
    if(sActorName == "Maram") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnStandardMajorDialogue()
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Maram", "Neutral") ]])
        fnCutscene([[ Append("Maram: Greetings, Mei.[P] When you have spoken with Septima, I am to train you on scrying with the mirror.[B][C]") ]])
        fnCutscene([[ Append("Maram: I am also to serve as your sparring partner and companion.[P] I relish the opportunity.") ]])
    end
end
