-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
--Meeting with Maram.
if(sObjectName == "MaramScene") then
	
	--Variables.
	local iMetMaram = VM_GetVar("Root/Variables/Chapter1/Scenes/iMetMaram", "N")
	if(iMetMaram == 0.0) then
		
		--Set flag.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iMetMaram", "N", 1.0)
		
		--Pause a bit.
		fnCutsceneWait(65)
		fnCutsceneBlocker()
		
		-- |[Dialogue]|
		--Setup.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Maram", "Neutral") ]])
		
		--Talking.
		fnCutscene([[ Append("Rilmani: ...[P][CLEAR]") ]])
		fnCutscene([[ Append("Rilmani: Where are you, Mei?[P] Please, be safe...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Are you talking to me?") ]])
		fnCutsceneBlocker()
		
		-- |[Movement]|
		--Wait a bit.
		fnCutsceneWait(30)
		fnCutsceneBlocker()
		
		--Maram looks south.
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Maram")
			ActorEvent_SetProperty("Face",  0, 1)
		DL_PopActiveObject()
		fnCutsceneBlocker()
		
		--Wait a bit.
		fnCutsceneWait(60)
		fnCutsceneBlocker()
		
		--Maram moves south a bit.
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Maram")
			ActorEvent_SetProperty("Move To", (8.25 * gciSizePerTile), (5.50 * gciSizePerTile), 0.25)
		DL_PopActiveObject()
		fnCutsceneBlocker()
		
		--Wait a bit.
		fnCutsceneWait(30)
		fnCutsceneBlocker()
		
		-- |[Dialogue]|
		--Setup.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Maram", "Neutral") ]])
		
		--Talking.
		fnCutscene([[ Append("Rilmani: Could it be?[P] Could you really be here?") ]])
		fnCutsceneBlocker()
		
		-- |[Movement]|
		--Wait a bit.
		fnCutsceneWait(30)
		fnCutsceneBlocker()
		
		--Maram runs up to Mei.
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Maram")
			ActorEvent_SetProperty("Move To", (8.25 * gciSizePerTile), (7.50 * gciSizePerTile), 2.5)
		DL_PopActiveObject()
		fnCutsceneBlocker()
		
		--Pause a bit.
		fnCutsceneWait(40)
		fnCutsceneBlocker()
		
		--Hug Mei.
		for i = 0, 10, 1 do
			Cutscene_CreateEvent("ActorEvent", "Actor")
				ActorEvent_SetProperty("Subject Name", "Maram")
				ActorEvent_SetProperty("Teleport To", (8.25 * gciSizePerTile), (7.50 * gciSizePerTile) + (i * gciSizePerTile * 0.05))
			DL_PopActiveObject()
			fnCutsceneWait(2)
			fnCutsceneBlocker()
		end
		
		-- |[Dialogue]|
		--Setup.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Maram", "Neutral") ]])
		
		--Talking.
		fnCutscene([[ Append("Mei:[E|Surprise] (I guess I'll just...[P] hug her back?)") ]])
		fnCutsceneBlocker()
		
		--Hug Mei.
		for i = 0, 5, 1 do
			Cutscene_CreateEvent("ActorEvent", "Actor")
				ActorEvent_SetProperty("Subject Name", "Maram")
				ActorEvent_SetProperty("Teleport To", (8.25 * gciSizePerTile), (8.00 * gciSizePerTile) - (i * gciSizePerTile * 0.10))
			DL_PopActiveObject()
			fnCutsceneWait(4)
			fnCutsceneBlocker()
		end
		fnCutsceneWait(30)
		fnCutsceneBlocker()
		
		-- |[Dialogue]|
		--Setup.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Maram", "Neutral") ]])
		
		--Talking.
		fnCutscene([[ Append("Mei:[E|Surprise] Erm, hello?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] Wait a minute...[P] I know you from somewhere...[B][C]") ]])
		fnCutscene([[ Append("Rilmani: Mei...[P] it is so good to finally meet you.[B][C]") ]])
		fnCutscene([[ Append("Maram: I am Maram.[P] I have been tasked with watching over you, as you are the one fated to arrive here.[B][C]") ]])
		fnCutscene([[ Append("Maram: I observed your birth, your childhood, your life...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] But I know you from somewhere.[P] How?[B][C]") ]])
		fnCutscene([[ Append("Maram: Perhaps you have gained a fleeting glimpse of me in the reflections of the world.[P] Perhaps your fourth-dimensional senses reversed the scrying effect of the mirror.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Happy] ...[P] That's it![P] You were my imaginary friend when I was a child![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] But you never went away, did you?[B][C]") ]])
		fnCutscene([[ Append("Maram: I have been here, watching you, making sure you developed the way you were meant to.[B][C]") ]])
		fnCutscene([[ Append("Maram: We have little influence over The Still Plane, but what little I could do to comfort you when you were sad,[P] I did.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] You've been there for me this whole time?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] I always felt at ease when I was in my room, all alone.[B][C]") ]])
		fnCutscene([[ Append("Maram: Where your desk mirror was located.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Surprise] ..![P] Does that mean you - [P][CLEAR]") ]])
		fnCutscene([[ Append("Maram: I stepped away from the mirror on those occasions, Mei.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Happy] Thank goodness![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] This is like meeting my best friend for the first time...[B][C]") ]])
		fnCutscene([[ Append("Maram: It is a joyous occasion for me, as well.[P] I am proud of the person you have become.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Laugh] Do you think you could smile when you say that?[B][C]") ]])
		fnCutscene([[ Append("Maram: I [P]*am*[P] smiling.[B][C]") ]])
		fnCutscene([[ Append("Maram: I nearly swallowed my heart when you disappeared.[P] I lost sight of you in that alley and thought something had happened.[B][C]") ]])
		fnCutscene([[ Append("Maram: I've been frantically searching for you since then.[P] I did not think the time had come so early.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Well, here I am.[B][C]") ]])
		fnCutscene([[ Append("Maram: Oh, you must speak with Septima.[P] I will not hold you here any longer.[P] Your purpose has yet to be fulfilled.[B][C]") ]])
		fnCutscene([[ Append("Maram: We will have much time to talk later.[P] I am to be an integral part of your training, you see.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Training?[B][C]") ]])
		fnCutscene([[ Append("Maram: Please discuss the matter with Septima.[B][C]") ]])
		fnCutscene([[ Append("Maram: Thank you for visiting me, Mei.[P] It is so good to meet you in person at last.[B][C]") ]])
		fnCutscene([[ Append("Maram: Now I must return to my scrying.[P] If you are here,[P] then I must attempt to locate the other bearers...") ]])
		fnCutsceneBlocker()
		
		-- |[Movement]|
		--Maram moves to the mirror.
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Maram")
			ActorEvent_SetProperty("Move To", (8.25 * gciSizePerTile), (4.50 * gciSizePerTile))
		DL_PopActiveObject()
		fnCutsceneBlocker()
		
		--Music starts up.
		fnCutscene([[ AL_SetProperty("Music", "NixNedar") ]])
		
	end
end
