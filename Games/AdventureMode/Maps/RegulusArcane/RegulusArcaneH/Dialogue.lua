-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    --Set facing.
    TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[2856]|
    if(sActorName == "2856") then
        local iSX399JoinsParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Tiffany", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        if(iSX399JoinsParty == 1.0) then
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "SX-399", "Neutral") ]])
        end
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "2856", "Neutral") ]])
        fnCutscene([[ Append("2856:[E|Punchable] Greetings, friends, what may I help you with?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] (Suddenly she's nice?)[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Neutral] Oh, oops, I had accidentally let my linguistic routines assume we were not facing an unprecedented crisis![P] [EMOTION|2856|Yelling]MY MISTAKE.[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Yelling] SHUT UP,[P] DON'T ASK QUESTIONS,[P] AND SEAL THE BREACHES IN THIS SECTOR,[P] BEFORE WE ALL DIE.[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Neutral] ...[P] Would it help if I said please?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] Yes?[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Neutral] Please.[P][EMOTION|2856|Yelling] GET GOING,[P] RIGHT NOW.") ]])
        
    -- |[SecurityA]|
    elseif(sActorName == "SecurityA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Drone:[VOICE|LatexDrone] COMMAND UNIT 2856 IS AN EXCELLENT LEADER.[P] THIS UNIT ENJOYS WORKING UNDER HER.") ]])
        fnCutsceneBlocker()
        
    -- |[SecurityB]|
    elseif(sActorName == "SecurityB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Drone:[VOICE|LatexDrone] WATCHING FOR TARGETS...") ]])
        fnCutsceneBlocker()
        
    -- |[SecurityC]|
    elseif(sActorName == "SecurityC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Drone:[VOICE|LatexDrone] AREA CLEAR, NO HOSTILES ACTIVELY MURDERING THIS UNIT.") ]])
        fnCutsceneBlocker()
        
    -- |[SecurityD]|
    elseif(sActorName == "SecurityD") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Drone:[VOICE|LatexDrone] THESE UNITS ARE BEYOND REPAIR.[P] PLEASE MOVE ON.") ]])
        fnCutsceneBlocker()
        
    -- |[SecurityE]|
    elseif(sActorName == "SecurityE") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Drone:[VOICE|LatexDrone] THIS UNIT CANNOT BE SALVAGED...") ]])
        fnCutsceneBlocker()
        
    -- |[SecurityF]|
    elseif(sActorName == "SecurityF") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Drone:[VOICE|LatexDrone] THIS UNIT'S SUPERIOR OFFICER HAS BEEN RETIRED.[P] THIS UNIT IS...[P] NOT SURE...[P] WHAT TO DO...[P] THIS UNIT...[P] IS CRYING...") ]])
        fnCutsceneBlocker()
    end
end
