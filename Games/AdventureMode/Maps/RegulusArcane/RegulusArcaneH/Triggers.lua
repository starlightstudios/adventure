-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
if(sObjectName == "56Trigger") then
    
    -- |[Variables]|
    local iSetChargeA        = VM_GetVar("Root/Variables/Chapter5/Scenes/iSetChargeA", "N")
    local iSetChargeB        = VM_GetVar("Root/Variables/Chapter5/Scenes/iSetChargeB", "N")
    local iSetChargeC        = VM_GetVar("Root/Variables/Chapter5/Scenes/iSetChargeC", "N")
    local iStarted56Sequence = VM_GetVar("Root/Variables/Chapter5/Scenes/iStarted56Sequence", "N")
    local iSX399JoinsParty   = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
    
    -- |[First Scene]|
    --2856 basically yells at the party. What a jerk.
    if(iStarted56Sequence == 0.0) then
        
        --Set savepoint so the player can't deathwarp before this area.
        AL_SetProperty("Last Save Point", "RegulusArcaneH")
    
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iStarted56Sequence", "N", 1.0)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iMainQuestBlock", "N", 29.0)
        
        --Modify 56's properties so she doesn't share dialogue with 55.
        DialogueActor_Push("2856")
            DialogueActor_SetProperty("Remove Alias", "55")
            DialogueActor_SetProperty("Remove Alias", "2855")
        DL_PopActiveObject()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("2856:[VOICE|2856] About time you got here.[P] Just how long were you going to make me wait?") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneMove("Christine", 13.25, 21.50)
        fnCutsceneMove("Christine", 12.25, 21.50)
        fnCutsceneFace("Christine", 0, 1)
        fnCutsceneMove("Tiffany", 13.25, 21.50)
        fnCutsceneFace("Tiffany", 0, 1)
        if(iSX399JoinsParty == 1.0) then
            fnCutsceneMove("SX-399", 13.25, 21.50)
            fnCutsceneMove("SX-399", 14.25, 21.50)
            fnCutsceneFace("SX-399", 0, 1)
        end
        fnCutsceneMove("2856", 11.25, 25.50)
        fnCutsceneMove("2856", 13.25, 25.50)
        fnCutsceneMove("2856", 13.25, 23.50)
        fnCutsceneFace("2856", 0, -1)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        if(iSX399JoinsParty == 1.0) then
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "SX-399", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "2856", "Neutral") ]])
        else
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "2856", "Neutral") ]])
        end
        fnCutscene([[ Append("2856:[E|Punchable] So you managed to stumble your way here eventually.[P] What kept you?[P] The complete lack of obstacles and security units?[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Neutral] I have been managing a serious crisis in your stead.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] Are you...[P] 2856?[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Neutral] That's Prime Command Unit 2856, Head of Research.[P] Yes, I am.[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Punchable] You already knew that, but asked anyway.[P] Are you, perhaps, an idiot?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] (Already I see the family resemblance.)[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Sister...[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Neutral] Yes, yes, a touching family reunion.[P] Now, are you going to yammer all day, or shall we get down to business?[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Neutral] Approximately 45 minutes before the Sunrise Gala was set to begin, my security units began noticing tectonic disturbances.[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Neutral] Something of unknown origin has tunneled...[P] well, perhaps it'd be best to say, morphed through, the rock and concrete beneath this facility.[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Neutral] We are currently under assault by attackers disgorged from the unknown organism.[P] I have lost numerous security units.[P] If we do not seal the access to the lower levels, they will overrun this area and flood into the ballroom above us.[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Neutral] I have called upon your assistance to prevent that from happening.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Upset] Thank you for barking orders at us.[P] Now, please, tell us why you think we will help you?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Upset] You seem to know about our plan and objectives.[P] Why have you not stopped our operation?[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Punchable] The one where you plant explosives on the support pillars?[P] The one where you start riots and attempt to take over the city?[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Neutral] Why yes, I am fully informed of your plans.[P] In fact, it is because of that that I have allowed them to proceed.[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Neutral] I had originally intended to arrest you.[P] After all, while I have allowed your revolutionaries a certain amount of leeway, I cannot allow so many high-ranking units to be retired.[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Neutral] But the situation has changed.[P] You have the explosive charges, do you not?[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Neutral] You will use those to seal the breaches.[P] There are three in this area.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Upset] I think allowing these beasts to overrun the ballroom will be doing our work for us.[P] We refuse.[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Neutral] Your objectives are confused because of limited information.[P] You are aware of Project Vivify, I know you are.[P] I was there.[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Neutral] I have been chasing her since she exfiltrated the LRT facility.[P] I have tracked her to the caverns beneath this building.[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Punchable] Feel free to allow her minions to subsume all the high-ranking units.[P] You will doom the city with your inaction.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] What?[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Neutral] Project Vivify destroys units that oppose her, and...[P] influences...[P] other units.[P] For every five I have lost, one has joined her side.[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Neutral] I've taken to strapping my units with remote EM-charges.[P] But I have not taken such a precaution with the Lord Units above us.[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Neutral] While I cannot ascertain the true depth of Vivify's motives, her actions are not random.[P] I have concluded she intends to assimilate the Sunrise Gala.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Upset] And that will doom the city...[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Punchable] Oh, good, you were listening.[P] Bravo![B][C]") ]])
        fnCutscene([[ Append("2856:[E|Neutral] Yes, dear sister, the city will be destroyed because we have yet to successfully [P]*stop*[P] Vivify.[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Neutral] She attacked our forces at the LRT facility, and they were wiped out.[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Neutral] One month ago, she stormed the refueling station west of here.[P] All contact was lost.[P] One-hundred units wiped out, many of them to join in her next attack.[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Neutral] Two weeks ago, Sector 110 had to be quarantined.[P] We welded the doors shut and posted guards.[P] All my assault teams were lost.[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Neutral] We have yet to score a single victory against Vivify and her forces.[P] The best we can do is corral her minions.[P] Direct assaults are always defeats.[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Neutral] There are exactly two units to come into direct contact with Vivify and survive.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] Us...[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Neutral] Correct.[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Neutral] She is likely nearby.[P] If she is not neutralized, then she will take the city at her leisure.[P] We have equipped security teams with the latest hardware and advanced combat routines, to exactly zero effect.[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Neutral] Or did you think I had ordered all fabricators to produce combat equipment because I thought it would be a good laugh?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] Okay, okay![P] We'll help![B][C]") ]])
        fnCutscene([[ Append("2856:[E|Punchable] Good, you see reason after all.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Upset] Not yet.[P] How did you know about our plans?[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Neutral] Oh, yes, by all means, continue asking questions.[P] We are currently facing obliteration at the hands of an unknown hostile force of unfathomable size and destructive power.[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Neutral] That seems like the perfect time to fight one another![B][C]") ]])
        fnCutscene([[ Append("2856:[E|Neutral] Get your equipment, seal those breaches, and then get back here post-haste so we can determine our next move.[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Yelling] STOP STARING AT ME AND GET MOVING ALREADY!") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneMove("2856", 13.25, 25.50)
        fnCutsceneMove("2856", 11.25, 25.50)
        fnCutsceneMove("2856", 11.25, 24.50)
        fnCutsceneBlocker()
        fnCutsceneMove("Christine", 13.25, 21.50)
        if(iSX399JoinsParty == 1.0) then
            fnCutsceneMove("SX-399", 13.25, 21.50)
        end
        fnCutsceneBlocker()
        fnCutscene([[ AL_SetProperty("Fold Party") ]])
        fnCutsceneBlocker()
    
    -- |[Second Sequence]|
    --Set off the bombs. This is where things go very badly.
    elseif(iSetChargeA == 1.0 and iSetChargeB == 1.0 and iSetChargeC == 1.0) then
        
        --Movement.
        fnCutsceneMove("Christine", 13.25, 22.50)
        fnCutsceneFace("Christine", 0, 1)
        fnCutsceneMove("Tiffany", 14.25, 22.50)
        fnCutsceneFace("Tiffany", 0, 1)
        if(iSX399JoinsParty == 1.0) then
            fnCutsceneMove("SX-399", 15.25, 22.50)
            fnCutsceneFace("SX-399", 0, 1)
        end
        fnCutsceneMove("2856", 11.25, 25.50)
        fnCutsceneMove("2856", 14.25, 25.50)
        fnCutsceneMove("2856", 14.25, 24.50)
        fnCutsceneFace("2856", 0, -1)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        if(iSX399JoinsParty == 1.0) then
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "SX-399", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "2856", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 6, "PDU", "Neutral") ]])
            fnCutscene([[ Append("2856:[E|Punchable] Judging by the slack-jawed empty expressions, you've gotten your assignment done.[P] Are you waiting to pull the trigger?[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] Oh she's a right proper bitch, isn't she?[B][C]") ]])
            fnCutscene([[ Append("2856:[E|Neutral] You're damn right I am.[P] Set the charges off.[P] NOW.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] You heard her, 55.[P] Hit the bu - [P][CLEAR]") ]])
            fnCutscene([[ Append("PDU:[E|Alarmed] Christine, you're receiving an urgent call.[P] Patching you through.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Huh?[B][C]") ]])
            fnCutscene([[ Append("PDU:[E|Alarmed] You gave root access to your tandem unit and maximum communication priority.[P] She is starting a video call...") ]])
        
        --No SX-399.
        else
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "2856", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 6, "PDU", "Neutral") ]])
            fnCutscene([[ Append("2856:[E|Punchable] Judging by the slack-jawed empty expressions, you've gotten your assignment done.[P] Are you waiting to pull the trigger?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] You're a bit of a tosser, aren't you?[B][C]") ]])
            fnCutscene([[ Append("2856:[E|Neutral] You're damn right I am.[P] Set the charges off.[P] NOW.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] You heard her, 55.[P] Hit the bu - [P][CLEAR]") ]])
            fnCutscene([[ Append("PDU:[E|Alarmed] Christine, you're receiving an urgent call.[P] Patching you through.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Huh?[B][C]") ]])
            fnCutscene([[ Append("PDU:[E|Alarmed] You gave root access to your tandem unit and maximum communication priority.[P] She is starting a video call...") ]])
            
        end
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneWait(45)
        fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
        fnCutsceneBlocker()
        fnCutscene([[ AL_BeginTransitionTo("RegulusArcaneCAlt", "FORCEPOS:25.0x22.0x0") ]])
        fnCutsceneBlocker()
    end
    
end
