-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
if(sObjectName == "SouthExit") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusArcaneF", "FORCEPOS:7.5x4.0x0")
    
elseif(sObjectName == "Ladder") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (This ladder leads to the lower floor.[P] Our current objective is the elevator terminal.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A list of units assigned to hauling.[P] Nothing about the security situation.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalB") then
    
    --Variables
    local iSX399JoinsParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
    
    --Flags.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iMainQuestBlock", "N", 28.0)
    
    --No SX-399:
    if(iSX399JoinsParty == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
        fnCutscene([[ Append("55:[E|Upset] (Unit 771852 has not attached the repeater yet...[P] as predicted...)[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] (She will do so in approximately 90 seconds, based on my models.)[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] (Note to self, increase all of Christine's timetables by 90 seconds in the future to prevent needless delays.)[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] (...[P] Why is there no security here?[P] Have we been compromised?[P] Hurry, Christine...)") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    
    
    --SX-399 joined the party:
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
        fnCutscene([[ Append("SX-399:[E|Happy] Surface access, here we come![B][C]") ]])
        fnCutscene([[ Append("55:[E|Upset] Don't touch the console.[P] Attempting to use it will likely lock down the elevator.[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Neutral] What do you take me for?[P] I know the plan.[P] We have to wait for Christine to install the repeater, right?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Upset] Just broadcast all the details so anyone lying in ambush can hear them.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Please, SX-399, this is an important mission...[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Neutral] Don't worry about my feelings, hun.[P] I'm tough.[P] I'm used to it.[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Neutral] Fighting creepers in the caves isn't quite the same as a stealth job.[P] I'll keep my mouth shut.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] If Christine is the unit I think she is, we won't have to wait long.[P] The console will inform us when the repeater is installed.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] We just need to wait.[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Neutral] So, do you want to chat?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] No.[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Smirk] So let me tell you a funny story about pulse weapon maintenance...[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] (...[P] Christine, please hurry...)") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    end
    
    --Map transition.
    fnCutsceneWait(45)
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    fnCutscene([[ AL_BeginTransitionTo("RegulusArcaneE", "FORCEPOS:42.5x6.0x0") ]])
    fnCutsceneBlocker()

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
