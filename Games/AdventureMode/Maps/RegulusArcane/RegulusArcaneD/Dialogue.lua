-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
    
    -- |[GolemGalaA]|
    if(sActorName == "GolemGalaA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] See the sun boiling beneath the horizon...") ]])
        fnCutsceneBlocker()
        
    -- |[GolemGalaB]|
    elseif(sActorName == "GolemGalaB") then
        
        --Variables.
        local iGalaMetPrimeC = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaMetPrimeC", "N")
        
        --First time.
        if(iGalaMetPrimeC == 0.0) then
            
            --Flag.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iGalaMetPrimeC", "N", 1.0)
            
            --If all three primes are identified, flag this.
            local iGalaMetPrimeA = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaMetPrimeA", "N")
            local iGalaMetPrimeB = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaMetPrimeB", "N")
            local iGalaMetPrimeC = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaMetPrimeC", "N")
            if(iGalaMetPrimeA == 1.0 and iGalaMetPrimeB == 1.0 and iGalaMetPrimeC == 1.0) then
                VM_SetVar("Root/Variables/Chapter5/Scenes/iGalaGoal1State", "N", 1.0)
                AL_SetProperty("Flag Objective True", "1: Identify Prime Command Units")
            end
            
            --Re-resolve the display name.
            local sObj1DisplayName = fnResolveCommandUnitObjectiveName()
            AL_SetProperty("Set Objective Display Name", "1: Identify Prime Command Units", sObj1DisplayName)
        
            --Dialogue.
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
            fnCutscene([[ Append("Command Unit:[E|Neutral] Oh my, it appears we have company.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] Apologies, Command Unit.[P] Is this room reserved?[B][C]") ]])
            fnCutscene([[ Append("Command Unit:[E|Neutral] No, no.[P] I just hadn't expected to see someone...[P] low ranking...[B][C]") ]])
            fnCutscene([[ Append("Command Unit:[E|Neutral] Shall I simply say 'politics' and leave it at that?[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] Seems like a fair assessment to me.[B][C]") ]])
            fnCutscene([[ Append("Command Unit:[E|Neutral] No matter.[B][C]") ]])
            fnCutscene([[ Append("Command Unit:[E|Neutral] Have you come to watch the sunrise in private?[P] It seems some golem somewhere set up some chairs for us.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Well...[B][C]") ]])
            fnCutscene([[ Append("Command Unit:[E|Neutral] No need to be shy.[P] I am enchanted by your lovely wardrobe, who composed it?[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Happy] It's a custom made piece.[P] I designed it myself![B][C]") ]])
            fnCutscene([[ Append("Command Unit:[E|Neutral] Oh, lovely![P] Might I ask you to compose mine next year, unit...[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Sad] Uhhh...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] I am Christine, and this is Andrea.[P] We're from Sector 96.[B][C]") ]])
            fnCutscene([[ Append("Briget:[E|Neutral] Briget.[P] Charmed.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Briget?[P] Prime Command Unit 5608, 'Bloody' Briget?[B][C]") ]])
            fnCutscene([[ Append("Briget:[E|Neutral] I see that nickname continues to follow me.[P] Yes, that is I.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] I'm not familiar with the name...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] According to the rumours...[B][C]") ]])
            fnCutscene([[ Append("Briget:[E|Neutral] The rumours are true.[P] I am the Prime Command Unit assigned to the maintenance of public order.[B][C]") ]])
            fnCutscene([[ Append("Briget:[E|Neutral] I regret that it became necessary.[P] Our operatives prefer to act quietly and behind the scenes.[P] Having to call security units is considered a point of failure.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] What happened?[B][C]") ]])
            fnCutscene([[ Append("Briget:[E|Neutral] A group of unruly Slave Units stormed the habitation domes and sealed the entrances.[P] They demanded an 11-hour maximum working day and repelled all attempts to enter.[B][C]") ]])
            fnCutscene([[ Append("Briget:[E|Neutral] The security units were unable to breach the domes without compromising their structural integrity, which was likely the objective of the insurgents.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Surprised] Oh my...[B][C]") ]])
            fnCutscene([[ Append("Briget:[E|Neutral] In the name of public order, I activated one of our reserve protocols.[P] We released a pheromone into the habitat, causing the Raijus great physical discomfort as it increased their electrical outputs well beyond normal levels.[B][C]") ]])
            fnCutscene([[ Append("Briget:[E|Neutral] The pain drove them to desperation, and the Raijus eventually began to discharge into the circuitry in the dome, and its occupants.[P] Without actually compromising the structural integrity of the dome, the insurgents were suppressed.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] But over 80 Raijus died, and another 156 were badly injured.[P] All the insurgents were fried, as well as several breeding program humans trapped in the dome.[B][C]") ]])
            fnCutscene([[ Append("Briget:[E|Neutral] For doing my job and maintaining public order, I was given the nickname 'Bloody' Briget.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Surprised] M-[P]my goodness![B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] *Careful, now, Sophie.[P] Play along.*[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] We weren't there, Andrea.[P] We can't have known the circumstances.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] I have complete faith that Prime Command Unit 5608 made the correct decision with the available information.[B][C]") ]])
            fnCutscene([[ Append("Briget:[E|Neutral] Finally![P] Someone who doesn't second-guess my choices![B][C]") ]])
            fnCutscene([[ Append("Briget:[E|Neutral] I have received immense criticism for my actions, but all of them from the perspective of the results.[P] Even Unit 2855, ever the stalwart, did not present an alternative approach.[B][C]") ]])
            fnCutscene([[ Append("Briget:[E|Neutral] Precisely what the other Primes would have done, they did not say.[P] Only that I was too brutal.[B][C]") ]])
            fnCutscene([[ Append("Briget:[E|Neutral] You, Christine, understand that what I did was necessary.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Sad] ...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Further, no insurgent will ever dare to do that again.[P] Public Order will be maintained now that they know the lengths we will go to to maintain it.[B][C]") ]])
            fnCutscene([[ Append("Briget:[E|Neutral] Another good point.[P] Thank you.[B][C]") ]])
            fnCutscene([[ Append("Briget:[E|Neutral] I did not think I would make a friend on a night like this.[P] Usually, I avoid public gatherings.[P] Most Lord Units try not to be associated with me.[B][C]") ]])
            fnCutscene([[ Append("Briget:[E|Neutral] Since you are here, I presume you are tandem units.[P] If you would like to watch the sunrise with us, you are more than welcome.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Sad] We...[P] wanted to go somewhere private...[B][C]") ]])
            fnCutscene([[ Append("Briget:[E|Neutral] I see.[P] Do not let me keep you.[P] Good evening, ladies.") ]])
            fnCutsceneBlocker()
        
        --Repeats.
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
            fnCutscene([[ Append("Briget:[E|Neutral] If you're looking to be alone, I think there must be a spare room around here for you.") ]])
        end
        
    -- |[GolemGalaE]|
    elseif(sActorName == "GolemGalaE") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Where has my tandem unit gotten to?[P] I should know better than to speak with my mouth full of shrimp!") ]])
        fnCutsceneBlocker()
        
    -- |[GolemGalaF]|
    elseif(sActorName == "GolemGalaF") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] I'm hiding from my tandem unit.[P] She ate the shrimp and it's now far too spicy to kiss her!") ]])
        fnCutsceneBlocker()
    end
end
