-- |[Victory!]|
--Variables.
local iSX399JoinsParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")

--The party defeats Eldritch Christine.
fnCutsceneTeleport("EntourageA", -100.25, -100.50)
fnCutsceneTeleport("EntourageB", -100.25, -100.50)
fnCutsceneTeleport("EntourageC", -100.25, -100.50)
fnCutsceneTeleport("Sophie", 40.25, 16.50)
fnCutsceneBlocker()
fnCutscene([[ AudioManager_PlayMusic("Null") ]])

--Christine collapses.
fnCutsceneSetFrame("Christine", "Crouch")
fnCutsceneBlocker()
fnCutsceneWait(45)
fnCutsceneBlocker()
fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
fnCutsceneSetFrame("Christine", "Wounded")
fnCutsceneBlocker()
fnCutsceneWait(45)
fnCutsceneBlocker()

--Sophie runs up.
fnCutsceneMove("Sophie", 33.25, 16.50, 1.70)
fnCutsceneMove("Sophie", 33.25,  5.50, 1.70)
fnCutsceneMove("Sophie", 26.25,  5.50, 1.70)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ Append("Sophie:[VOICE|Sophie] Christine![P] Christine, no, no, no!") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneSetFrame("Sophie", "Cry0")
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneSetFrame("Sophie", "Cry1")
fnCutsceneWait(125)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
if(iSX399JoinsParty == 1.0) then
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "SX-399", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Sad") ]])
else
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Sad") ]])
end
fnCutscene([[ Append("Sophie:[E|Sad] You -[P] you -[P] Christine![P] Wake up![B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Sad] Her organic life signs are at zero...[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] Unit 499323, this physical form of hers has always had zero life signs.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Sad] Didn't you hear her whispering?[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Sad] I was hiding in the side rooms there, but as soon as you started fighting her, I heard her.[P] She was telling me it'd be okay.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Sad] She told me not to worry about her and that I should come out.[P] Come join her.[P] Be with her.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] It was a lure.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Sad] But it was so convincing...[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Sad] Did you have to shoot her?[P] Can't you help her?[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] She will awaken momentarily.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Sad] How do you know?[B][C]") ]])
fnCutscene([[ Append("55:[E|Upset] Individuals like her prefer drama to efficiency.[P] While her life signs are at zero, she is fully awake and conscious, but is using this as a pretext.[B][C]") ]])
fnCutscene([[ Append("55:[E|Upset] She is going to stand up and tell you the power of love brought her back from the brink, or something equally trite.[B][C]") ]])
if(iSX399JoinsParty == 1.0) then
    fnCutscene([[ Append("SX-399:[E|Neutral] You think all of that is trite, 55?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Upset] Yes, when she is wasting my time with it.") ]])
else
    fnCutscene([[ Append("55:[E|Upset] Right, Christine?[P] ...[P] Right?") ]])
end
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(165)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
if(iSX399JoinsParty == 1.0) then
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "SX-399", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Sad") ]])
else
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Sad") ]])
end
fnCutscene([[ Append("Sophie:[E|Sad] Christine...[P] please...[B][C]") ]])
fnCutscene([[ Append("55:[E|Upset] Any second now...[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Sad] ...[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Sad] ...[B][C]") ]])
fnCutscene([[ Append("55:[E|Upset] I said, she will awaken and is using this as a pretext...[B][C]") ]])
fnCutscene([[ Append("55:[E|Offended] Unit 771852, stop testing my patience![P] Stand up, there is important work to do![B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Sad] Stop it, 55.[P] It's not going to work.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] Damn it, don't you dare be dead.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] As if a few pulse rounds to the temple would be enough to retire you![P] We've been through much worse![B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] ...[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] Please don't be dead, Christine...") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Music starts.
fnCutscene([[ AudioManager_PlayMusic("SophiesTheme") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneSetFrame("Christine", "Crouch")
fnCutsceneSetFrame("Sophie", "Null")
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneSetFrame("Christine", "Null")
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
if(iSX399JoinsParty == 1.0) then
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "SX-399", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Happy") ]])
else
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Happy") ]])
end
fnCutscene([[ Append("Christine:[E|Smirk] *Cough*[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Smirk] Thanks a ton, 55.[P] You almost ruined it![B][C]") ]])
fnCutscene([[ Append("55:[E|Smirk] ...[P] The outcome was never really in doubt.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Happy] Christine![P] *smooch*[P] Oh thank goodness![B][C]") ]])
fnCutscene([[ Append("Christine:[E|Smirk] Ouch, ouch, ooh.[P] I love the kisses, but remember that I have been shot.[P] A lot.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Smirk] But playing dead in this body is pretty easy, so I thought I'd try to get 55 to act nice for once.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Smirk] I didn't think it'd take as long as it did, but there we are.[P] Sorry about that, Sophie.[B][C]") ]])
if(iSX399JoinsParty == 1.0) then
    fnCutscene([[ Append("SX-399:[E|Neutral] Oh, I knew there was something soft beneath the shell.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] I was merely playing along to get Christine to stop acting.[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Smirk] Nobody believes you, hun.[B][C]") ]])
end
fnCutscene([[ Append("Sophie:[E|Happy] You gave me such a scare![B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Blush] (Ugh, these tentacles are so gross, but I can't help myself.[P] Gotta hold her tight!)[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Smirk] Okay, okay, fun's over.[P] We still have bad guys to deal with.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Neutral] Yeah, but, where did they go?[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] My sister has sent a message to my PDU.[P] She is waiting for us in the biolabs north of here.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Smirk] Sister?[P] Wow, 55, congratulations![B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] It has not been a tearful reunion.[P] Christine, we should go.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] 201890 was using you as a distraction to make her escape.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] No, no she wasn't.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] She has no fear of the security forces, or of you.[P] There is only one thing she fears, and that is...[B][C]") ]])
if(iSX399JoinsParty == 1.0) then
    fnCutscene([[ Append("SX-399:[E|Neutral] Vivify?[B][C]") ]])
else
    fnCutscene([[ Append("55:[E|Neutral] Project Vivify.[B][C]") ]])
end
fnCutscene([[ Append("Christine:[E|Neutral] Yes.[P] She is in the biolabs right now.[P] I can hear her song.[P] I know where she is.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] She called everyone to her, and she was very angry...[P] 20 was acting without permission...[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] She wants us to go there.[P] She wants to meet me in person again.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Offended] What?[P] No way, let's go home![B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Sad] ...[P] It's too dangerous.[P] What if you don't come back this time?[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] She threatens the entire city, Sophie.[P] We have to stop her, and I think I'm the only one who can.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] 201890 did something to me, turned me into this because I was vulnerable, but I think I can overcome it now.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] Once again, we are the only ones to survive a direct encounter with Vivify's influence.[P] We have to go.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Sad] But...[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] Chin up, Sophie.[P] Keep that smile going bright.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] As long as you smile, even in pitch darkness, I'll see it.[P] Your smile will guide me.[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Smirk] Gotta be strong...[P] Gotta be confident...[B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Smirk] But if you play a prank on me like that to prove a point again -[P] no sex for a month![B][C]") ]])
if(iSX399JoinsParty == 1.0) then
    fnCutscene([[ Append("SX-399:[E|Sad] Woah, woah, Sophie, think about what you're saying![B][C]") ]])
end
fnCutscene([[ Append("55:[E|Smirk] Even the administrators would never carry out such a punishment...[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Sad] Okay, okay![P] No more tricks![B][C]") ]])
fnCutscene([[ Append("Christine:[E|Smirk] We'd better go, 55, before she changes her mind![B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] Sophie...[P] Come with us.[P] We'll find a safe spot in the biolabs and hole you up there.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] The only other way out is with the security forces, and I don't trust them.[P] They might take you hostage.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] A good evaluation.[P] Despite the extreme danger, she will be safer with us.[B][C]") ]])
if(iSX399JoinsParty == 1.0) then
    fnCutscene([[ Append("SX-399:[E|Neutral] I'll keep you safe.[P] I think I owe you a bit of a debt, right?[B][C]") ]])
end
fnCutscene([[ Append("Sophie:[E|Blush] Oh my goodness![B][C]") ]])
fnCutscene([[ Append("Sophie:[E|Smirk] But I have to stay strong, no matter the danger...") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneWait(45)
fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneBlocker()

--Modify party setup. Christine is back in, and Sophie is now following.
AdvCombat_SetProperty("Party Slot", 0, "Christine")
AdvCombat_SetProperty("Party Slot", 1, "Tiffany")
if(iSX399JoinsParty == 1.0) then
    AdvCombat_SetProperty("Party Slot", 2, "SX-399")
end

--Flag Christine's form to be
AdvCombat_SetProperty("Push Party Member", "Christine")
    AdvCombatEntity_SetProperty("Clear Combat Start Job")
DL_PopActiveObject()

--IDs.
if(iSX399JoinsParty == 1.0) then
    EM_PushEntity("Tiffany")
        local i55ID = RE_GetID()
    DL_PopActiveObject()
    EM_PushEntity("SX-399")
        local iSX399ID = RE_GetID()
    DL_PopActiveObject()
    EM_PushEntity("Sophie")
        local iSophieID = RE_GetID()
    DL_PopActiveObject()

    --Store names and IDs.
    giFollowersTotal = 3
    gsaFollowerNames[1] = "Tiffany"
    gsaFollowerNames[2] = "SX-399"
    gsaFollowerNames[3] = "Sophie"
    giaFollowerIDs[0] = i55ID
    giaFollowerIDs[1] = iSX399ID
    giaFollowerIDs[2] = iSophieID

    --Tell everyone to follow.
    AL_SetProperty("Unfollow Actor Name", "Tiffany")
    AL_SetProperty("Unfollow Actor Name", "SX-399")
    AL_SetProperty("Unfollow Actor Name", "Sophie")
    AL_SetProperty("Follow Actor ID", i55ID)
    AL_SetProperty("Follow Actor ID", iSX399ID)
    AL_SetProperty("Follow Actor ID", iSophieID)
else
    EM_PushEntity("Tiffany")
        local i55ID = RE_GetID()
    DL_PopActiveObject()
    EM_PushEntity("Sophie")
        local iSophieID = RE_GetID()
    DL_PopActiveObject()

    --Store names and IDs.
    giFollowersTotal = 2
    gsaFollowerNames[1] = "Tiffany"
    gsaFollowerNames[2] = "Sophie"
    giaFollowerIDs[0] = i55ID
    giaFollowerIDs[1] = iSophieID

    --Tell everyone to follow.
    AL_SetProperty("Unfollow Actor Name", "Tiffany")
    AL_SetProperty("Unfollow Actor Name", "Sophie")
    AL_SetProperty("Follow Actor ID", i55ID)
    AL_SetProperty("Follow Actor ID", iSophieID)
end

--Scene transition.
fnCutscene([[ AL_BeginTransitionTo("RegulusBiolabsA", "FORCEPOS:15.0x31.0x0") ]])
fnCutsceneBlocker()
