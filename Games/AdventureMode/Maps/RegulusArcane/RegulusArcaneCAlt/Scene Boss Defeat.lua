-- |[Defeat]|
--Player's party is defeated.
local iSX399JoinsParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
fnCutsceneWait(55)
fnCutsceneBlocker()
fnCutsceneSetFrame("Tiffany", "Downed")
fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
fnCutsceneWait(55)
fnCutsceneBlocker()

--Quest flag.
VM_SetVar("Root/Variables/Chapter5/Scenes/iMainQuestBlock", "N", 30.0)

--Dialogue.
if(iSX399JoinsParty == 1.0) then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "SX-399", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Christine", "Neutral") ]])
    fnCutscene([[ Append("SX-399:[E|Sad] Heh...[P] Heh...[P] Gonna take more than...[P] that...[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Sad] Right, 55?[P] Right?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Sys...[P] tem...[P] fail...[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Sad] Hun?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(55)
    fnCutsceneBlocker()

    --Movement.
    fnCutsceneFace("SX-399", -1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "SX-399", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Christine", "Neutral") ]])
    fnCutscene([[ Append("SX-399:[E|Sad] 55, not the...[P] time to fool around...[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Sad] Hey, Christine, I need a breather,[P] is that okay?[B][C]") ]])
    fnCutscene([[ Append("SX-399:[E|Sad] I am...[P] not beaten...[P] but you know, be a good sport...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] (It is unconcerned.[P] It is not threatened.[P] It will find its other.[P] Where is other.)[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Bricks are liquid.[P] I'm pleased.[P] Not sideways.[P] Sideways.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

--No SX-399.
else
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Christine", "Neutral") ]])
    fnCutscene([[ Append("55:[E|Neutral] Ent-enter-stand-by...[P] Dam-dam-age crtttttttt------[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] ...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(55)
    fnCutsceneBlocker()
end

--Remove characters.
for i = 0, 19, 1 do
    local sName = "GenGolem"
    if(i < 10) then sName = sName .. "0" end
    sName = sName .. i
    fnCutsceneTeleport(sName, -100, -100)
end
fnCutsceneTeleport("2856", -100, -100)
fnCutsceneTeleport("Sophie", 44.25, 22.50)
fnCutsceneFace("Sophie", 0, 1)
fnCutsceneBlocker()
fnCutsceneSetFrame("Sophie", "Cry0")
fnCutsceneBlocker()

--Remove followers.
AL_SetProperty("Unfollow Actor Name", "Tiffany")
if(iSX399JoinsParty == 1.0) then
    AL_SetProperty("Unfollow Actor Name", "SX-399")
end
AL_SetProperty("Unfollow Actor Name", "Sophie")

--Collision.
AL_SetProperty("Set Collision", 35, 4, 0, 1)

--Give SX-399 a dialogue.
if(iSX399JoinsParty == 1.0) then
    EM_PushEntity("SX-399")
        TA_SetProperty("Clipping Flag", true)
        TA_SetProperty("Activation Script", fnResolvePath() .. "Dialogue.lua")
    DL_PopActiveObject()
end

--Give collisions to the entourage.
EM_PushEntity("Tiffany")
    TA_SetProperty("Clipping Flag", true)
DL_PopActiveObject()
EM_PushEntity("EntourageA")
    TA_SetProperty("Clipping Flag", true)
DL_PopActiveObject()
EM_PushEntity("EntourageB")
    TA_SetProperty("Clipping Flag", true)
DL_PopActiveObject()
EM_PushEntity("EntourageC")
    TA_SetProperty("Clipping Flag", true)
DL_PopActiveObject()
