-- |[ ====================================== Floor Burster ===================================== ]|
--In this cutscene, the party dramatically bursts from the floor and confronts 201890!

-- |[ ========================== Setup =========================== ]|
--Spawn Sophie.
TA_Create("Sophie")
    TA_SetProperty("Position", 25, 22)
    TA_SetProperty("Clipping Flag", false)
    fnSetCharacterGraphics("Root/Images/Sprites/SophieDress/", true)
    TA_SetProperty("Facing", gci_Face_North)
    TA_SetProperty("Wipe Special Frames")
    TA_SetProperty("Add Special Frame", "Cry0", "Root/Images/Sprites/Special/Sophie|Cry0")
    TA_SetProperty("Add Special Frame", "Cry1", "Root/Images/Sprites/Special/Sophie|Cry1")
DL_PopActiveObject()

--2856
TA_Create("2856")
    TA_SetProperty("Position", -10, -10)
    TA_SetProperty("Facing", gci_Face_North)
    TA_SetProperty("Clipping Flag", false)
    fnSetCharacterGraphics("Root/Images/Sprites/56/", true)
DL_PopActiveObject()

--Spawn 20 and her entourage.
TA_Create("20")
    TA_SetProperty("Position", 35, 4)
    TA_SetProperty("Clipping Flag", false)
    fnSetCharacterGraphics("Root/Images/Sprites/20/", false)
    TA_SetProperty("Facing", gci_Face_West)
DL_PopActiveObject()
TA_Create("EntourageA")
    TA_SetProperty("Position", 36, 4)
    TA_SetProperty("Clipping Flag", false)
    fnSetCharacterGraphics("Root/Images/Sprites/EldritchDream/", false)
    TA_SetProperty("Facing", gci_Face_West)
DL_PopActiveObject()
TA_Create("EntourageB")
    TA_SetProperty("Position", 37, 4)
    TA_SetProperty("Clipping Flag", false)
    fnSetCharacterGraphics("Root/Images/Sprites/EldritchDream/", false)
    TA_SetProperty("Facing", gci_Face_West)
DL_PopActiveObject()
TA_Create("EntourageC")
    TA_SetProperty("Position", 38, 4)
    TA_SetProperty("Clipping Flag", false)
    fnSetCharacterGraphics("Root/Images/Sprites/EldritchDream/", false)
    TA_SetProperty("Facing", gci_Face_West)
DL_PopActiveObject()

-- |[Disable Collisions]|
AL_SetProperty("Set Collision", 24, 16, 0, 0)
AL_SetProperty("Set Collision", 25, 16, 0, 0)
AL_SetProperty("Set Collision", 26, 16, 0, 0)

-- |[Variables]|
local iSX399JoinsParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")

-- |[ ======================== Execution ========================= ]|
--Normal cutscene execution. Change to 'false' to skip the scene for combat testing.
if(true) then

    --Black the screen out.
    fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()

    --Camera focuses on Sophie.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 5.0) --Default is 5.0
        CameraEvent_SetProperty("Focus Position", (25.25 * gciSizePerTile), (22.50 * gciSizePerTile))
    DL_PopActiveObject()

    --Move the rest of the party off.
    fnCutsceneTeleport("Christine", -100.25, -100.50)
    fnCutsceneTeleport("Tiffany", -100.25, -100.50)
    if(iSX399JoinsParty == 1.0) then
        fnCutsceneTeleport("SX-399", -100.25, -100.50)
    end
    fnCutsceneTeleport("Influenced", -100.25, -100.50)
    fnCutsceneBlocker()

    --Fade in.
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Sad] S-[P]Sophie? What's wrong?[P] Why are you still at the gala?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Sad] I tried to leave, but there was a unit who wouldn't let anyone through, and she looked really weird![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Sad] The security units are all acting really strange![B][C]") ]])
    fnCutscene([[ Append("55:[VOICE|Tiffany] Strange?[P] How strange?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Like they keep whispering to themselves and I think their eyes are bleeding lubricant or something![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] They punched a Lord Unit who tried to force her way out and I don't know what to do![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] 55![P] We need to go![P] Right now![B][C]") ]])
    fnCutscene([[ Append("55:[VOICE|Tiffany] Detonating charges...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Function
    local fnGolemsLookAround = function()
        for i = 0, 19, 1 do
            
            --Generate name.
            local sName = "GenGolem"
            if(i < 10) then sName = sName .. "0" end
            sName = sName .. i
            
            --NPC looks a random direction.
            local iRoll = LM_GetRandomNumber(1, 4)
            if(iRoll == 1) then
                fnCutsceneFace(sName, 0, 1)
            elseif(iRoll == 2) then
                fnCutsceneFace(sName, 0, -1)
            elseif(iRoll == 3) then
                fnCutsceneFace(sName, 1, 0)
            elseif(iRoll == 4) then
                fnCutsceneFace(sName, -1, 0)
            end
        end
    end

    --Explosions.
    fnCutscene([[ AudioManager_PlaySound("World|Explosion") ]])
    fnCutsceneBlocker()
    fnGolemsLookAround()

    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|Explosion") ]])
    fnCutsceneBlocker()
    fnGolemsLookAround()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|Explosion") ]])
    fnCutsceneBlocker()
    fnGolemsLookAround()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Sophie", -1, 1)
    fnCutsceneWait(25)
    fnGolemsLookAround()
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneFace("Sophie", 1, 0)
    fnCutsceneBlocker()
    fnGolemsLookAround()
    fnCutsceneWait(25)
    fnCutsceneFace("Sophie", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Sad] Sophie!?[P] Are you still there?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Sad] I-[P]I thought 55 was going to blow me up?[P] What's going on?[B][C]") ]])
    fnCutscene([[ Append("Voice:[VOICE|201890] Hello, hello, distinguished ladies![P] How are we this morning?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Camera moves. Music starts.
    fnCutscene([[ AL_SetProperty("Music", "EquinoxTheme") ]])
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 10.0) --Default is 5.0
        CameraEvent_SetProperty("Focus Position", (25.25 * gciSizePerTile), (6.50 * gciSizePerTile))
    DL_PopActiveObject()

    --All golems look towards this point.
    for i = 0, 19, 1 do
        
        --Generate name.
        local sName = "GenGolem"
        if(i < 10) then sName = sName .. "0" end
        sName = sName .. i
        fnCutsceneFace(sName, 0, -1)
    end

    --20 and her entourage move out.
    fnCutsceneMove("20", 25.25, 4.50)
    fnCutsceneMove("20", 25.25, 5.50)
    fnCutsceneFace("20", 0, 1)
    fnCutsceneMove("EntourageA", 23.25, 4.50)
    fnCutsceneFace("EntourageA", 0, 1)
    fnCutsceneMove("EntourageB", 25.25, 4.50)
    fnCutsceneFace("EntourageB", 0, 1)
    fnCutsceneMove("EntourageC", 27.25, 4.50)
    fnCutsceneFace("EntourageC", 0, 1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "201890", "Neutral") ]])
    fnCutscene([[ Append("Golem:[E|Neutral] Yes, yes, oh, and what a morning it is![P] For soon, everything about my beloved city will change.[B][C]") ]])
    fnCutscene([[ Append("Golem:[E|Neutral] But, oh, I'm sure you are wondering who I am, who my friends are, and where I'm going with this![B][C]") ]])
    fnCutscene([[ Append("Golem:[E|Neutral] Wonder no longer![P] For I am your gracious host and the organizer of this wonderful event![B][C]") ]])
    fnCutscene([[ Append("20:[E|Neutral] I go by the moniker of 201890, though of course my closest friends call me 20.[B][C]") ]])
    fnCutscene([[ Append("20:[E|Neutral] These are my friends![P] Go on, my friends, mingle![P] Mingle!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Movement.
    fnCutsceneMove("EntourageA", 23.25, 7.50)
    fnCutsceneMove("EntourageA", 20.25, 7.50)
    fnCutsceneFace("EntourageA", 0, 1)
    fnCutsceneMove("EntourageB", 25.25, 7.50)
    fnCutsceneMove("EntourageB", 28.25, 7.50)
    fnCutsceneFace("EntourageB", 0, 1)
    fnCutsceneMove("EntourageC", 27.25, 7.50)
    fnCutsceneMove("EntourageC", 30.25, 7.50)
    fnCutsceneFace("EntourageC", 0, 1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Golem:[VOICE|Golem] Get away from me, you freak![B][C]") ]])
    fnCutscene([[ Append("Golem:[VOICE|Golem] R-[P]run![P] Run!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "201890", "Neutral") ]])
    fnCutscene([[ Append("20:[E|Neutral] No, I believe you will be staying exactly where you are.[P] The exits have been blocked, and your precious security units are busy.[B][C]") ]])
    fnCutscene([[ Append("20:[E|Neutral] You see, those explosions you just heard?[P] That was them, being busy with my other friends in the basement.[P] They cannot protect you.[B][C]") ]])
    fnCutscene([[ Append("20:[E|Neutral] So you'll just remain calm, and remain seated, because I really don't want to have to snap any limbs.[B][C]") ]])
    fnCutscene([[ Append("20:[E|Neutral] We are, after all, dignified and well dressed units, are we not?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Movement.
    fnCutsceneMove("CommandGalaB", 32.25, 5.50)
    fnCutsceneFace("CommandGalaB", -1, 0)
    fnCutsceneBlocker()
    fnCutsceneFace("20", 1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Doll", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "201890", "Neutral") ]])
    fnCutscene([[ Append("Command Unit:[E|Neutral] That, [P]'friend'[P], is quite clearly enough.[B][C]") ]])
    fnCutscene([[ Append("Command Unit:[E|Neutral] Whoever you think you are, whatever you intend to do, we will not allow it.[B][C]") ]])
    fnCutscene([[ Append("20:[E|Neutral] Oh, you attend my party, and then act like you're above me?[B][C]") ]])
    fnCutscene([[ Append("Command Unit:[E|Neutral] *Your*[P] party, is it?[P] I can't wait to hear your explanation![B][C]") ]])
    fnCutscene([[ Append("20:[E|Neutral] Honored Command Unit, I did organize this party.[P] And I did what any good unit does -[P] delegate![B][C]") ]])
    fnCutscene([[ Append("20:[E|Neutral] It actually wasn't too difficult, you see.[P] By merely ordering that all communication was to be off the network, and sending a few handwritten notes, well, you did the rest![B][C]") ]])
    fnCutscene([[ Append("20:[E|Neutral] Yes, yes, I know.[P] You're welcome![B][C]") ]])
    fnCutscene([[ Append("Command Unit:[E|Neutral] Just who are you, anyway?[B][C]") ]])
    fnCutscene([[ Append("20:[E|Neutral] As I said, I am 201890.[P] That's my favoured name, though there are many.[B][C]") ]])
    fnCutscene([[ Append("Command Unit:[E|Neutral] I just checked my lookup table, and Unit 201890 is a Slave Unit currently assigned to welding in Sector 120.[P] You are not her.[B][C]") ]])
    fnCutscene([[ Append("20:[E|Neutral] Such a focus on names, on propriety.[P] Everything in its place, yes?[B][C]") ]])
    fnCutscene([[ Append("20:[E|Neutral] I am not the unit you refer to.[P] This name is mine.[P] I named myself.[P] A crucial first step in one's autonomy, I'd say.[B][C]") ]])
    fnCutscene([[ Append("20:[E|Neutral] Names are something someone else gives you, before they know who you are.[P] At birth or at conversion, the name we get is always going to be the wrong one.[P] So I made my own.[B][C]") ]])
    fnCutscene([[ Append("20:[E|Neutral] I made my own body, too.[P] Work in progress, sure, but still something to be proud of.[B][C]") ]])
    fnCutscene([[ Append("20:[E|Neutral] So, Command Unit, what do you think?[B][C]") ]])
    fnCutscene([[ Append("Command Unit:[E|Neutral] I think there is a place on the scrap heap for that husk of a body.[B][C]") ]])
    fnCutscene([[ Append("20:[E|Neutral] My friends thought the same thing at first.[P] Don't you want to be friends?[B][C]") ]])
    fnCutscene([[ Append("Command Unit:[E|Neutral] Die, scum!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Movement.
    fnCutsceneMove("CommandGalaB", 25.75, 5.50, 2.0)
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|Heartbeat") ]])
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    fnCutsceneTeleport("CommandGalaB", -100.25, -100.50)
    fnCutsceneBlocker()
    fnCutsceneWait(305)
    fnCutsceneBlocker()

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Golem:[VOICE|Golem] Where -[P] did she go?[P] What happened?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("20", 0, 1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("20:[VOICE|201890] Why, she's still here![P] Aren't you, my new friend?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Movement.
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|Heartbeat") ]])
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    fnCutsceneTeleport("Influenced", 26.25, 5.50)
    fnCutsceneFace("Influenced", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(125)
    fnCutsceneBlocker()

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "201890", "Neutral") ]])
    fnCutscene([[ Append("20:[VOICE|201890] Don't be shy![P] Go on, show everyone your pretty face!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    fnCutsceneFace("Influenced", 1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Movement.
    fnCutsceneFace("Influenced", 0, 1)
    fnCutsceneBlocker()
    fnCutsceneWait(125)
    fnCutsceneBlocker()

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "DollInfluenced", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "201890", "Neutral") ]])
    fnCutscene([[ Append("Command Unit:[E|Neutral] My.[P] Pretty.[P] Face.[B][C]") ]])
    fnCutscene([[ Append("20:[E|Neutral] You see?[P] Now don't you feel much better?[B][C]") ]])
    fnCutscene([[ Append("Command Unit:[E|Neutral] So.[P] Much.[P] My mind.[P] Infinity.[P] All thoughts.[P] Simultaneous.[P] Thank.[P] Thank.[P] It opens.[B][C]") ]])
    fnCutscene([[ Append("Command Unit:[E|Neutral] The vault opens and brains spill out![P] Eggs fester in flesh and a scream vibrates through eternity![B][C]") ]])
    fnCutscene([[ Append("Command Unit:[E|Neutral] We swirl around the light until we snuff it out, sucking through into the next self![P] Yes![P] Friends![B][C]") ]])
    fnCutscene([[ Append("20:[E|Neutral] Aww, aren't you just adorable?[P] Come![P] Let's make everyone here our friends, shall we?[B][C]") ]])
    fnCutscene([[ Append("Command Unit:[E|Neutral] Yes![P] Yes![P] Yes!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Move back to Sophie.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 5.0) --Default is 5.0
        CameraEvent_SetProperty("Focus Position", (25.25 * gciSizePerTile), (22.50 * gciSizePerTile))
    DL_PopActiveObject()
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Sophie:[E|Sad] Christine, help![P] Help![P] They're going to get me![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Sad] Sophie, find a place to hide![P] We're coming![P] We'll be right there!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(125)
    fnCutsceneBlocker()
        
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "2856", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] 55.[P] Give me that leftover blasting charge.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] We need to move to the west and head up the elevators to reach the main floor.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] No, that will take too long.[P] Give me the charge.[B][C]") ]])
    fnCutscene([[ Append("56:[E|Neutral] You're wasting time.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] I am thinking three-dimensionally.[P] Now, stand the hell back.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    fnCutsceneWait(45)

    --Remove the NPCs in question.
    fnCutsceneTeleport("20", 25.25, 4.50)
    fnCutsceneTeleport("GenGolem15", -100.25, -100.50)
    fnCutsceneTeleport("CommandGalaA", -100.25, -100.50)
    fnCutsceneTeleport("GenGolem01", -100.25, -100.50)
    fnCutsceneTeleport("GenGolem02", -100.25, -100.50)
    fnCutsceneTeleport("GenGolem03", -100.25, -100.50)
    fnCutsceneTeleport("GenGolem04", -100.25, -100.50)
    fnCutsceneTeleport("GenGolem05", -100.25, -100.50)
    fnCutsceneTeleport("GenGolem06", -100.25, -100.50)
    fnCutsceneTeleport("GenGolem07", -100.25, -100.50)
    fnCutsceneTeleport("GenGolem08", -100.25, -100.50)
    fnCutsceneTeleport("GenGolem09", -100.25, -100.50)
    fnCutsceneTeleport("GenGolem10", -100.25, -100.50)
    fnCutsceneTeleport("GenGolem11", -100.25, -100.50)
    fnCutsceneTeleport("GenGolem12", -100.25, -100.50)
    fnCutsceneTeleport("GenGolem13", -100.25, -100.50)
    fnCutsceneTeleport("Sophie", 45.25, 22.50)
    fnCutsceneFace("Sophie", 1, 0)
    fnCutsceneTeleport("EntourageA", 45.25, 25.50)
    fnCutsceneFace("EntourageA", -1, 0)
    fnCutsceneTeleport("EntourageB", 43.25, 25.50)
    fnCutsceneTeleport("EntourageC", 48.25, 23.50)
    fnCutsceneFace("EntourageC", 1, 0)
    fnCutsceneBlocker()

    --Fade back in.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 5.0) --Default is 5.0
        CameraEvent_SetProperty("Focus Position", (25.25 * gciSizePerTile), (16.50 * gciSizePerTile))
    DL_PopActiveObject()
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneWait(125)
    fnCutsceneBlocker()

    --Animate the explosion here.
    local iTPF = 6
    fnCutscene([[ AudioManager_PlaySound("World|BigExplosion") ]])
    for i = 1, 12, 1 do
        local sString = "AL_SetProperty(\"Set Layer Disabled\", \"FloorExpl\" .. " .. i .. ",  false)"
        fnCutscene(sString)
        if(i > 1) then
            sString = "AL_SetProperty(\"Set Layer Disabled\", \"FloorExpl\" .. " .. i-1 .. ",  true)"
            fnCutscene(sString)
        end
        fnCutsceneWait(iTPF)
        fnCutsceneBlocker()
    end

    --All golems look towards this point.
    for i = 0, 19, 1 do
        
        --Generate name.
        local sName = "GenGolem"
        if(i < 10) then sName = sName .. "0" end
        sName = sName .. i
        
        --Get current position.
        EM_PushEntity(sName)
            local fXPos, fYPos = TA_GetProperty("Position")
            fXPos = fXPos / gciSizePerTile
            fYPos = fYPos / gciSizePerTile
        DL_PopActiveObject()
        
        --Generate 'speeds'.
        local fXSpeed = 0
        local fYSpeed = 0
        if(fXPos < 25.25) then fXSpeed = 1.0 end
        if(fXPos > 25.25) then fXSpeed = -1.0 end
        
        
        Cutscene_CreateEvent("ActorEvent", "Actor")
            ActorEvent_SetProperty("Subject Name", sName)
            ActorEvent_SetProperty("Face", fXSpeed, fYSpeed)
        DL_PopActiveObject()
    end

    fnCutsceneWait(145)
    fnCutsceneBlocker()
    fnCutsceneFace("Christine", 0, -1)
    fnCutsceneFace("Tiffany", 0, -1)
    if(iSX399JoinsParty == 1.0) then
        fnCutsceneFace("SX-399", 0, -1)
    end
    fnCutsceneFace("2856", 0, -1)
    fnCutsceneTeleport("Christine", 25.25, 16.50)
    fnCutsceneTeleport("Tiffany", 26.25, 16.50)
    if(iSX399JoinsParty == 1.0) then
        fnCutsceneTeleport("SX-399", 24.25, 16.50)
    end
    fnCutsceneBlocker()
    fnCutsceneMove("Christine", 25.25, 14.50)
    fnCutsceneMove("Tiffany", 26.25, 15.50)
    if(iSX399JoinsParty == 1.0) then
        fnCutsceneMove("SX-399", 24.25, 15.50)
    end
    fnCutsceneBlocker()
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 2.0) --Default is 5.0
        CameraEvent_SetProperty("Focus Actor Name", "Christine")
    DL_PopActiveObject()
    fnCutsceneTeleport("2856", 25.25, 16.50)
    fnCutsceneBlocker()
    fnCutsceneMove("2856", 25.25, 15.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
        
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "2856", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Offended] Okay, ladies![P] Everyone out![P] Orderly fashion, and all that![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] Unit 2856, have your security units get everyone out through the tunnels.[P] We'll go distract this '20' character.[B][C]") ]])
    fnCutscene([[ Append("56:[E|Neutral] And become her next casualty?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] I think we can handle ourselves.[P] Worry about your assignment, not mine.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] (I don't see Sophie...[P] where is she?)[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] (No time -[P] gotta take down 201890 and hope the security units rescue Sophie!)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Move the party.
    fnCutsceneMove("Christine", 23.25, 15.50, 2.00)
    fnCutsceneBlocker()
    fnCutsceneMove("Tiffany", 25.25, 15.50, 2.00)
    if(iSX399JoinsParty == 1.0) then
        fnCutsceneMove("SX-399", 24.25, 15.50, 2.00)
    end
    fnCutsceneBlocker()
    fnCutsceneMove("Christine", 18.25, 15.50, 2.00)
    fnCutsceneMove("Christine", 18.25, 16.50, 2.00)
    fnCutsceneMove("Christine", 17.25, 16.50, 2.00)
    fnCutsceneMove("Christine", 17.25,  7.50, 2.00)
    fnCutsceneMove("Christine", 25.25,  7.50, 2.00)
    fnCutsceneFace("Christine", 0, -1)
    fnCutsceneMove("Tiffany", 18.25, 15.50, 2.00)
    fnCutsceneMove("Tiffany", 18.25, 16.50, 2.00)
    fnCutsceneMove("Tiffany", 17.25, 16.50, 2.00)
    fnCutsceneMove("Tiffany", 17.25,  7.50, 2.00)
    fnCutsceneMove("Tiffany", 26.25,  7.50, 2.00)
    fnCutsceneFace("Tiffany", 0, -1)
    if(iSX399JoinsParty == 1.0) then
        fnCutsceneMove("SX-399", 18.25, 15.50, 2.00)
        fnCutsceneMove("SX-399", 18.25, 16.50, 2.00)
        fnCutsceneMove("SX-399", 17.25, 16.50, 2.00)
        fnCutsceneMove("SX-399", 17.25,  7.50, 2.00)
        fnCutsceneMove("SX-399", 24.25,  7.50, 2.00)
        fnCutsceneFace("SX-399", 0, -1)
    end
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
        
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    if(iSX399JoinsParty == 1.0) then
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "SX-399", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "201890", "Neutral") ]])
        fnCutscene([[ Append("SX-399:[E|Neutral] Hey, robo-bimbo![P] Pick on someone in your own weight class![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Yeah, leave the small fry and come get the big fish.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] We are going to destroy you.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] (Real inventive, 55...)[B][C]") ]])
        fnCutscene([[ Append("20:[E|Neutral] Ah, finally.[P] The guest of honor.[P] The one who eeeeveryone won't shut up about.[B][C]") ]])
        fnCutscene([[ Append("20:[E|Neutral] Christine, what is it you think makes you so special?[P] Work ethic?[P] Birth?[P] No?[B][C]") ]])
        fnCutscene([[ Append("20:[E|Neutral] All I hear about you is the possibilities, but I never hear about anything you've actually done.[P] Just what you will do.") ]])
    else
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "201890", "Neutral") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Picking on poor defenseless units?[P] For shame![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Quit tormenting the small fry, come get the big fish.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] We are -[P] going to destroy you, interloper.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] (Real inventive, 55...)[B][C]") ]])
        fnCutscene([[ Append("20:[E|Neutral] Ah, finally.[P] The guest of honor.[P] The one who eeeeveryone won't shut up about.[B][C]") ]])
        fnCutscene([[ Append("20:[E|Neutral] Christine, what is it you think makes you so special?[P] Work ethic?[P] Birth?[P] No?[B][C]") ]])
        fnCutscene([[ Append("20:[E|Neutral] All I hear about you is the possibilities, but I never hear about anything you've actually done.[P] Just what you will do.") ]])
        
        
    end
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Movement.
    fnCutscene([[ AL_SetProperty("Music", "Null") ]])
    fnCutsceneMove("Christine", 25.25, 5.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
        
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "201890", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Who's been talking about me behind my back?[B][C]") ]])
    fnCutscene([[ Append("20:[E|Neutral] Behind your back?[P] No.[P] No.[P] Certainly not.[P] We've been talking to you this whole time, but you haven't been listening.[B][C]") ]])
    fnCutscene([[ Append("20:[E|Neutral] So self-absorbed, petty, and greedy to boot.[P] Taking things that aren't yours because you want them.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] What are you babbling about?[B][C]") ]])
    fnCutscene([[ Append("20:[E|Neutral] The spotlight![P] I was first![P] I was the favourite, but now you are![P] Because of all the hard work you haven't put in![B][C]") ]])
    fnCutscene([[ Append("20:[E|Neutral] Without trying you've been given everything that I would kill for![P] Do you not understand?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Nope.[P] You're just ranting.[B][C]") ]])
    fnCutscene([[ Append("20:[E|Neutral] Then stop listening to my words and listen to my meaning...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Scared] H-[P]huh?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutscene([[ AL_SetProperty("Activate Fade", 25, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()

    --Clear censor bars.
    WD_SetProperty("Clear Censor Bars")

    --Black the screen out.
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Music", "Vivify") ]])
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Activate Scene") ]])
    fnCutscene([[ Append("The thing known as 201890 stared at Christine, and Christine stared back.[P] She tensed, ready for an attack, but an attack did not come.[B][C]") ]])
    fnCutscene([[ Append("It was only then that she realized that time itself seemed to stand still.[P] She looked over her shoulder.[P] The room was still.[P] Golems were held in place, drops of liquid held in midair as they were spilled.[P] Nothing moved.[B][C]") ]])
    fnCutscene([[ Append("When she looked back to 201890, she realized that she was no longer there.[P] There was no longer a there, there.[B][C]") ]])
    fnCutscene([[ Append("She realized she was standing nowhere, in an empty blackness.[P] She could smell something, something burnt, fleshy, wrong...[B][C]") ]])
    fnCutscene([[ Append("She was still in the ballroom, but it had changed.[P] The floors were made of meat.[P] The walls were fleshy with tendrils poking out.[P] Blood poured from open wounds.[B][C]") ]])
    fnCutscene([[ Append("She could see outside, somehow.[P] The whole city, the whole moon, had become flesh.[P] Giant stalks of unknown origin towered over her.[P] Mouths gaped and bled, their teeth gnashing and chewing at the flesh they were attached to.[P] Everything was dead, somehow, but never stopped bleeding.[B][C]") ]])
    fnCutscene([[ Append("Worse still, this was no hallucination.[P] In the back of her mind, in the most basic and primitive place of her being, she knew that this was true.[P] This was the future.[P] It would happen.[P] It was certain.[P] It had already happened, and all things would lead to it.[P] Struggling would do nothing.[B][C]") ]])
    fnCutscene([[ Append("She looked down.[P] She was human, but still metal.[P] Her hands were covered in latex, her breasts had become melted starlight.[P] All her parts were many parts, flashing and melting into one another.[P] What was she?[P] Who was she?[P] She was nobody.[P] She was nothing.[P] A pustule on some enormous, dead creature.[B][C]") ]])
    fnCutscene([[ Append("Her form began to shimmer.[P] She was losing herself.[P] This was how she had gotten here, into the distant future.[P] She had stepped out of time, and stepped back in, now.[P] She needed to concentrate and return herself, or she would be lost.[B][C]") ]])
    fnCutscene([[ Append("Among the fleshy tendrils boiling and writhing across the mutated planet, there came a voice.[P] A quiet song, singing words she could not hear.[P] It called to her.[B][C]") ]])
    fnCutscene([[ Append("It was her.[P] 'Vivify'.[P] Not her name.[P] She had no name.[P] There were no names.[P] Everything was dead.[P] She was singing a sad song, mourning.[P] She had made mistakes.[P] She was not alive, nothing was.[P] Nothing would ever be.[P] Not what she had intended.[B][C]") ]])
    fnCutscene([[ Append("Christine understood.[P] She looked at herself.[P] Her flesh had become grey, dead.[P] She was dead in this world, but she kept moving around.[P] A sense of habit, of commitment, of purpose.[P] She was dead and could not die.[P] Vivify sung to her.[P] Her song animated this dead world.[B][C]") ]])
    fnCutscene([[ Append("She blinked.[P] The ballroom was back.[P] She was standing on the stage.[P] Everyone was staring at her.[P] 201890 was gone, where had she gone?[P] The song was here, around her, everywhere.[P] It was her.[P] She was animated by the song.[B][C]") ]])
    fnCutscene([[ Append("She looked at her hand again.[P] Dead.[P] Grey.[P] Incubating a corpse in an egg.[P] She saw 2855's face.[P] It was twisted into a rage.[P] She at once hated it.[P] She hated that face.[P] She was dead.[P] The song told her to hate.[P] She hated everything.[B][C]") ]])
    fnCutscene([[ Append("The song told her to destroy her former comrades.[P] She was dead, and dead things did what the song told them to.[P] She would destroy them...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Change Christine.
    local iLRTBossResult = VM_GetVar("Root/Variables/Chapter5/Scenes/iLRTBossResult", "N")
    local iHasEldritchForm = VM_GetVar("Root/Variables/Global/Christine/iHasEldritchForm", "N")
    fnCutscene([[LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Eldritch.lua") ]])

    --Reposition.
    fnCutsceneTeleport("Christine", 25.25, 4.50)
    fnCutsceneFace("Christine", 0, 1)
    fnCutsceneTeleport("Tiffany", 25.25, 6.50)
    if(iSX399JoinsParty == 1.0) then
        fnCutsceneTeleport("SX-399", 26.25, 6.50)
    end
    fnCutsceneTeleport("20", -100, -100)
    fnCutsceneTeleport("Influenced", -100, -100)
    fnCutsceneTeleport("2856", -100, -100)

    --Fade in.
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutsceneMove("Christine", 25.25, 5.50, 0.25)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Variations. Christine did not lose to Vivify in the LRT facility.
    if(iLRTBossResult == 1.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        if(iSX399JoinsParty == 1.0) then
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "SX-399", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Tiffany", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Christine", "Neutral") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] What happened to her?[P] 55?[P] What happened?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] She has fallen victim to the influence of 201890, as we were warned.[P] She is one of them, now.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] This is what happens to her.[P] We observed earlier that she can hear things we cannot.[P] When she was muttering to herself, she was conversing with them.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Sad] 'Them'?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] I do not know who, but I do know it is someone who exists.[P] Perhaps it is Vivify, perhaps something far greater.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] We've got to change her back![B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Then help me subdue her.") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
        else
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Christine", "Neutral") ]])
            fnCutscene([[ Append("55:[E|Neutral] Christine, the enemy has fled.[P] Respond.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Upset] Unit 771852, that is an order.[P] Respond, immediately.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] ...[P] EM scans indicate total brain death.[P] Can you hear me?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] You are...[P] one of them...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] The experiments I read about in the database suggest this can be corrected, but were cut short by the test subject escaping.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Christine...[P] Please be recoverable.[P] I am not sure what I will do without you...") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
        end

    --Christine did lose to Vivify in the LRT facility. We don't check for EDG form because that can be carried over from NC+.
    else

        --SX-399 is present.
        if(iSX399JoinsParty == 1.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "SX-399", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Tiffany", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Christine", "Neutral") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] What happened to her?[P] 55?[P] What happened?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] This has happened before.[P] In the LRT facility, Christine came into contact with Project Vivify.[P] It changed her.[P] She is one of them, now.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Her brain is currently in a zero-cycle state, like sleep.[P] According to the research collected before Vivify escaped, her body is dead.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Sad] Can't she transform again?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Yes, but to do so she would have to want to.[P] She is currently effectively brain-dead.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Whatever is animating her can be interrupted through the use of high-energy exposure and chemical reactions.[P] But she will need to be subdued first.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Sad] But - [P]but I don't want to fight her![B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Funny.[P] She does not appear to think the same thing.[P] Prepare yourself.") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
        
        --No SX-399.
        else
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Christine", "Neutral") ]])
            fnCutscene([[ Append("55:[E|Upset] Christine, you damned fool...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Why did you transform yourself?[P] Or were you even aware of what you were doing?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] You said this would not happen again, yet it has.[P] And this time I am unsure if I will be able to stop you.[P] Think of all the units you are failing.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Think of...[P] anything at all.[P] Just don't let it end like this...") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
        end
    end

--Debug version. Skips right to the fight.
else
    --Change Christine.
    local iHasEldritchForm = VM_GetVar("Root/Variables/Global/Christine/iHasEldritchForm", "N")
    fnCutscene([[LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Eldritch.lua") ]])
end

--Clear all party slots.
for i = 1, 4, 1 do
    AdvCombat_SetProperty("Party Slot", 0, "Null")
end

--Remove Christine from the party. It's now 55 and SX-399. Darkmatter also joins!
if(iSX399JoinsParty == 1.0) then
    AdvCombat_SetProperty("Party Slot", 0, "Tiffany")
    AdvCombat_SetProperty("Party Slot", 1, "SX-399")
else
    AdvCombat_SetProperty("Party Slot", 0, "Tiffany")
end

--Boss Paths.
local sVictoryPath = fnResolvePath() .. "Scene Boss Victory.lua"
local sDefeatPath  = fnResolvePath() .. "Scene Boss Defeat.lua"
        
--Battle.
fnCutscene([[ AdvCombat_SetProperty("Next Combat Music", "MotherTheme", 0.0000) ]])
fnCutscene([[ AdvCombat_SetProperty("World Pulse", true) ]])
fnCutscene([[ AdvCombat_SetProperty("Reinitialize") ]])
fnCutscene([[ AdvCombat_SetProperty("Activate") ]])
fnCutscene([[ AdvCombat_SetProperty("Unretreatable", true) ]])
fnCutscene([[ AdvCombat_SetProperty("Victory Script", "]] .. sVictoryPath .. [[") ]])
fnCutscene([[ AdvCombat_SetProperty("Defeat Script",  "]] .. sDefeatPath  .. [[") ]])
fnCutscene([[ LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 5/000 Scripted Enemies/Boss Christine.lua") ]])
fnCutsceneBlocker()

--Re-enable collisions here.
fnCutscene([[ AL_SetProperty("Set Collision", 24, 16, 0, 1) ]])
fnCutscene([[ AL_SetProperty("Set Collision", 25, 16, 0, 1) ]])
fnCutscene([[ AL_SetProperty("Set Collision", 26, 16, 0, 1) ]])