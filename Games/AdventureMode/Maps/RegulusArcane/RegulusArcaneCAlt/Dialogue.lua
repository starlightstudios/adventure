-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
    
    -- |[SX-399]|
    if(sActorName == "SX-399") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "SX-399", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Christine", "Neutral") ]])
        fnCutscene([[ Append("SX-399:[E|Neutral] 55, she left us alone...[P] Get up...[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Neutral] Come on...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] (Find other.[P] Find other.[P] Bring other.[P] Make other new.[P] New.[P] New.)") ]])
        fnCutsceneBlocker()
    end
end
