-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
--Volume handlers.
if(sObjectName == "SceneTrigger") then
    
    --Variables.
    local iStartInvasion = VM_GetVar("Root/Variables/Chapter5/Scenes/iStartInvasion", "N")
    if(iStartInvasion == 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iStartInvasion", "N", 1.0)
    
    --Call.
    LM_ExecuteScript(fnResolvePath() .. "Scene Floor Burst.lua")

--Go back.
elseif(sObjectName == "NoLeave") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (It goes the path.[P] Other is east.[P] It will go east.)") ]])
    fnCutsceneBlocker()
    fnCutsceneMove("Christine", 25.25, 23.50)

--Sophie's cutscene. Only plays if the party is defeated by Eldritch Christine.
elseif(sObjectName == "SophieScene") then

    --Variables.
    local iSX399JoinsParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
    local iSophieKnowsAboutRunestone = VM_GetVar("Root/Variables/Chapter5/Sophie/iSophieKnowsAboutRunestone", "N")

    --Movement.
    fnCutsceneMove("Christine", 45.25, 23.50)
    fnCutsceneMove("Christine", 45.25, 22.50)
    fnCutsceneFace("Christine", -1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Unset collision flag.
    if(iSX399JoinsParty == 1.0) then
        EM_PushEntity("SX-399")
            TA_SetProperty("Clipping Flag", false)
        DL_PopActiveObject()
    end
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sophie", "Sad") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Christine", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] (It has found the other.[P] It will take the other.[P] It will...)[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Sad] Please let her be safe, please let her be safe...[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Huh?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] Christine![P] You're okay![B][C]") ]])
    if(iSophieKnowsAboutRunestone == 0.0) then
        fnCutscene([[ Append("Sophie:[E|Happy] Oh, you don't have to explain anything.[P] I know you can transform yourself, 55 said - [P][CLEAR]") ]])
        fnCutscene([[ Append("Sophie:[E|Offended] Well it doesn't matter.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Offended] I don't like tentacles, though.[P] Didn't I mention that?[B][C]") ]])
    else
        fnCutscene([[ Append("Sophie:[E|Offended] D-[P]didn't I mention I don't like tentacles?[P] Ha ha...[P] You should transform right away![B][C]") ]])
    end
    fnCutscene([[ Append("Sophie:[E|Smirk] But I guess you must have had a reason.[P] You look pretty darn intimidating like that![P] Hee hee![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] ...[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] ...[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Christine?[P] Why aren't you saying anything?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] Oops![P] Hee hee![P] You know how I rant when I get nerv -[P] nerv...[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] You're starting to freak me out, Christine...[P] Hee...[P] Hee...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] I'm so...[P] Happy...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] There's only one side, and it twists and twists around us.[P] Do you see it?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] See what?[P] Do your weird eyes let you see gamma rays or something?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] It wraps around the center.[P] It emerges from the center.[P] One eye.[P] One brain.[P] A billion eyes![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Sad] Christine...[P] You're starting to scare me![P] Stop it![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] I will.[P] Show.[P] You.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Surprised] G-[P]get away from me![P] Get back![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Show.[P] You.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Show.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Surprised] D-[P]don't touch me![P] Help![P] Someone help![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] (It is upset.[P] Other was upset by it.[P] It...[P] It...)[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Not?[P] Show?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Surprised] Stop it Christine![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Surprised] *Slap*[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Surprised] Don't touch me![P] Don't![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Make you whole...[P] Make you empty...[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Sad] I want my Christine back![P] Give her back you dumb ugly thing![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Sad] *Sniff*[P] Give her back...[P] She was my everything...[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Sad] How could you take her away from me...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Empty...[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Sad] *Sniff*...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] It is...[P] It is...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Soooo -[P] feeeeeeee.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Sad] How could you...[P] I loved her...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] T-[P]touch it.[P] Touch it.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] (Hmm?[P] What did she just say?)[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Touch.[P] Touch.[P] Touchtouchtouchtouchtouch!!![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] T-[P]taff it![P] Taff it![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] *Smooch*[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] R-[P]right on your gross slimy lips -[P] Taff me you're so nasty![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] *Smooch*[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Again![P] Again![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] *Smooch*[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] Christine I don't know how much longer I can keep this up![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] [MUSIC|Null]How about,[P][P] for the rest of our synthetic lives?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Did it -[P] did it do something?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Christine?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] [MUSIC|SophiesThemeSlow]Sophie...[P] I am awake...[P] again...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] My mind is still cloudy, but, I think getting necked woke me up.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] Christine![P] Oh thank good -[P][E|Surprised]nope![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Surprised] Tentacles are gross![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] I'm sorry, I didn't mean to upset you.[P] But 201890 did this...[P] and...[P] I lost control...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Oh, I gave 55 a thrashing...[P] Hopefully she's as forgiving as you are.[P] Come on, I better go apologize!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Entourage leaves.
    fnCutsceneFace("Christine", 1, 1)
    fnCutsceneFace("Sophie", 1, 1)
    if(iSX399JoinsParty == 1.0) then
        fnCutsceneFace("SX-399", -1, 0)
    end
    fnCutsceneSetFrame("Sophie", "Null")
    fnCutsceneSetFrame("Tiffany", "Downed")
    fnCutsceneBlocker()
    fnCutsceneMove("EntourageA", 48.25, 25.50)
    fnCutsceneMove("EntourageA", 48.25, 16.50)
    fnCutsceneMove("EntourageB", 48.25, 25.50)
    fnCutsceneMove("EntourageB", 48.25, 16.50)
    fnCutsceneMove("EntourageC", 48.25, 16.50)
    fnCutsceneTeleport("EntourageA", -100.25, -100.50)
    fnCutsceneTeleport("EntourageB", -100.25, -100.50)
    fnCutsceneTeleport("EntourageC", -100.25, -100.50)
    fnCutsceneBlocker()
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sophie", "Sad") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Christine", "Neutral") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] [MUSIC|Null]Did those things just... leave?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Yes.[P] Because she just called.[P] She's calling.[P] Follow me.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Run.
    fnCutsceneMove("Christine", 44.25, 23.50, 1.70)
    fnCutsceneBlocker()
    fnCutsceneMove("Christine", 44.25, 25.50, 1.70)
    fnCutsceneMove("Christine", 48.25, 25.50, 1.70)
    fnCutsceneMove("Christine", 48.25, 17.50, 1.70)
    fnCutsceneMove("Christine", 34.25, 17.50, 1.70)
    fnCutsceneMove("Christine", 34.25,  7.50, 1.70)
    fnCutsceneMove("Christine", 28.25,  7.50, 1.70)
    fnCutsceneMove("Sophie", 44.25, 25.50, 1.70)
    fnCutsceneMove("Sophie", 48.25, 25.50, 1.70)
    fnCutsceneMove("Sophie", 48.25, 17.50, 1.70)
    fnCutsceneMove("Sophie", 34.25, 17.50, 1.70)
    fnCutsceneMove("Sophie", 34.25,  7.50, 1.70)
    fnCutsceneMove("Sophie", 29.25,  7.50, 1.70)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneSetFrame("Tiffany", "Null")
    fnCutsceneFace("SX-399", 1, 0)
    fnCutsceneFace("Tiffany", 1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --SX-399 is present.
    if(iSX399JoinsParty == 1.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "SX-399", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Tiffany", "Neutral") ]])
        fnCutscene([[ Append("SX-399:[E|Neutral] Sophie...[P] I'm hit...[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Neutral] G-[P]Get away from Christine![P] Quickly...[P] ow...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] No, no, it's okay.[P] I'm fine now.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] (Should I tell them that girl-on-girl lovin' saved me?[P] Would 55 believe it?)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Are you two all right?[P] Sorry for that...[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] I am injured...[P] Auto-repair is activated...[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Neutral] I'm all right, but I need to regenerate a bit.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] Oh good, the ChocoBromine nanites worked![P] That was something of an experiment.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] I should have followed up, but you never contacted me to say anything was wrong.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Chocolate Bromine?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] It's common practice to bake potions into cakes or sweets on the surface of Pandemonium.[P] Nobody knows why it amplifies the effect, but it does.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] So I mixed SX-399's auto-repair nanites with chocolate and bromine.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] It's worked before...[P] But she's not a milking machine, so I wasn't sure if it would scale up.[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Smirk] Thanks, love.[P] We should be all right.[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Neutral] Are you going to explain what happened, you big palooka?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] When I got close to 201890, she showed me a vision.[P] A vision of the future.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] And I don't know how I know, but it was real.[P] It was true.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] What...[P] In the future, what has happened?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] All of Regulus is...[P] part of some giant...[P] thing.[P] It's organic, I think.[P] It's made of meat, forever bleeding.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] It's covered in eggs, but they're all stillborn.[P] Fleshy tendrils the size of a skyscraper lay limp, and dead.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Dead like this body.[P] It's just like what we saw in the basement.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Then...[P] Do we fail?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] I don't know if we can change it, but...[P] When I came back here, I was...[P] this.[P] I was listening to her song.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Vivify is in the biolabs somewhere, she's singing.[P] She's telling us all to come back to her.[P] All of her followers.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] I can hear her voice echo off all the new ones she's making.[P] We -[P] we have to stop her...[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] I will message my sister.[P] I will tell her to meet up with us.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Okay.[P] Okay.[P] We can do this.[P] Follow me.") ]])
    
    --No SX-399.
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
        fnCutscene([[ Append("55:[E|Neutral] System...[P] reinitializing...[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Unit 499323, move away from the hostile entity...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] No, no, it's okay.[P] I'm fine now.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] (Should I tell them that girl-on-girl lovin' saved me?[P] Would she believe it?)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Are you all right?[P] I shouldn't have hurt you...[P] I'm sorry...[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Auto-repair is activated.[P] My systems will be functional again momentarily.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Unit 771852.[P] Explain.[P] Cause of behavior.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] When I got close to 201890, she showed me a vision.[P] A vision of the future.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] And I don't know how I know, but it was real.[P] It was true.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] The future?[P] What has happened in the future?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] All of Regulus is...[P] part of some giant...[P] thing.[P] It's organic, I think.[P] It's made of meat, forever bleeding.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] It's covered in eggs, but they're all stillborn.[P] Fleshy tendrils the size of a skyscraper lay limp, and dead.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Dead like this body.[P] It's just like what we saw in the basement.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Does that mean we fail?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] I don't know if we can change it, but...[P] When I came back here, I was...[P] this.[P] I was listening to her song.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Vivify is in the biolabs somewhere, she's singing.[P] She's telling us all to come back to her.[P] All of her followers.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] I can hear her voice echo off all the new ones she's making.[P] We -[P] we have to stop her...[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] I will message my sister.[P] I will tell her to meet up with us.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Okay.[P] Okay.[P] We can do this.[P] Follow me.") ]])
    
    
    end
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()

    --Modify party setup. Christine is back in, and Sophie is now following.
    if(iSX399JoinsParty == 1.0) then
        
        --Party lineup.
        AdvCombat_SetProperty("Clear Party")
        AdvCombat_SetProperty("Party Slot", 0, "Christine")
        AdvCombat_SetProperty("Party Slot", 1, "Tiffany")
        AdvCombat_SetProperty("Party Slot", 2, "SX-399")

        --IDs.
        EM_PushEntity("Tiffany")
            local i55ID = RE_GetID()
        DL_PopActiveObject()
        EM_PushEntity("SX-399")
            local iSX399ID = RE_GetID()
        DL_PopActiveObject()
        EM_PushEntity("Sophie")
            local iSophieID = RE_GetID()
        DL_PopActiveObject()

        --Store names and IDs.
        giFollowersTotal = 3
        gsaFollowerNames[1] = "Tiffany"
        gsaFollowerNames[2] = "SX-399"
        gsaFollowerNames[3] = "Sophie"
        giaFollowerIDs[0] = i55ID
        giaFollowerIDs[1] = iSX399ID
        giaFollowerIDs[2] = iSophieID

        --Tell everyone to follow.
        AL_SetProperty("Unfollow Actor Name", "Tiffany")
        AL_SetProperty("Unfollow Actor Name", "SX-399")
        AL_SetProperty("Unfollow Actor Name", "Sophie")
        AL_SetProperty("Follow Actor ID", i55ID)
        AL_SetProperty("Follow Actor ID", iSX399ID)
        AL_SetProperty("Follow Actor ID", iSophieID)
        
        --Remove SX-399's dialogue script.
        EM_PushEntity("SX-399")
            TA_SetProperty("Activation Script", "NULL")
        DL_PopActiveObject()
    
    --SX-399 not joining.
    else
        
        --Party lineup.
        AdvCombat_SetProperty("Clear Party")
        AdvCombat_SetProperty("Party Slot", 0, "Christine")
        AdvCombat_SetProperty("Party Slot", 1, "Tiffany")

        --IDs.
        EM_PushEntity("Tiffany")
            local i55ID = RE_GetID()
        DL_PopActiveObject()
        EM_PushEntity("Sophie")
            local iSophieID = RE_GetID()
        DL_PopActiveObject()

        --Store names and IDs.
        giFollowersTotal = 2
        gsaFollowerNames[1] = "Tiffany"
        gsaFollowerNames[2] = "Sophie"
        giaFollowerIDs[0] = i55ID
        giaFollowerIDs[1] = iSophieID

        --Tell everyone to follow.
        AL_SetProperty("Unfollow Actor Name", "Tiffany")
        AL_SetProperty("Unfollow Actor Name", "Sophie")
        AL_SetProperty("Follow Actor ID", i55ID)
        AL_SetProperty("Follow Actor ID", iSophieID)
    
    end

    --Scene transition.
    fnCutscene([[ AL_BeginTransitionTo("RegulusBiolabsA", "FORCEPOS:15.0x31.0x0") ]])
    fnCutsceneBlocker()

end
