-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
-- |[Objects]|
if(sObjectName == "ShoeRack") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Shoes, presumably left here by units who changed footwear at the party.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "AdminDesk") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The administrative desk, presumably for personnel transfers in the university.[P] It's empty right now.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "StandbyTerminals") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (These record-keeping terminals are all in standby.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BigTerminal") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The big screen terminal.[P] It's in standby right now.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "WorkTerminal") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A work terminal, used to distribute assignments for units transferred to the university.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "RecordsTerminal") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Looks like someone left this terminal running.[P] It shows hundreds of task assignments to the university, but I can't pick out a useful pattern.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Note") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] A handwritten note?[P] Here?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] 'Unidentified organic material located in basement 7 of block C.[P] Request for security team - approved.'[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] What could that mean?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] I don't think it's any of our concern.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] (I really, really hope nothing comes of this...)[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Let's just enjoy the party and let the security units handle it, dearest.") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "RecordsBooks") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Shelves of handwritten records, going back decades![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Bureaucrats sure do love their paperwork.[P] I guess nobody has thought to modernize the records?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] That, or nobody has allocated the labour time for it.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] There's a cut-off date where no more records appear.[P] I guess that's when they started using terminals to record everything.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] It looks like it was only about twelve years ago.[P] I wonder if there's a huge archive of hardcopy records going back a century...") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "PopMachine") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Fizzy Pop! is omnipresent.[P] I'd rather not risk spilling it on my dress.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (An empty list of work assignments for the cleanup crew.[P] Nothing useful.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Someone was playing solitaire here, but it looks like they tried to cheat.[P] What's the point of cheating at a one-player game?)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (I can't access the terminal, there are security units all over the place.[P] But, it looks like they've set up a temporary HQ here.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Stationary") then
    
    --Variables.
    local iBumpedIntoLatexDrone = VM_GetVar("Root/Variables/Chapter5/Scenes/iBumpedIntoLatexDrone", "N")
    local iGotStationary        = VM_GetVar("Root/Variables/Chapter5/Scenes/iGotStationary", "N")
    
    --Normal:
    if(iBumpedIntoLatexDrone == 0.0 or iGotStationary == 1.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Boxes full of stationery supplies.[P] Pens, paper, archaic stuff like that. Hasn't been used in some time.)") ]])
        fnCutsceneBlocker()
    
    --Get stationery items!
    else
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iGotStationary", "N", 1.0)
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Stationery supplies.[P][SOUND|World|TakeItem] A little dusty, but it should do the job.)") ]])
        fnCutsceneBlocker()
    
    end
elseif(sObjectName == "Tube") then
    
    --Variables.
    local iGalaMetContact   = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaMetContact", "N")
    local iGalaGotBug       = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaGotBug", "N")
    
    --Normal.
    if(iGalaMetContact == 0.0 or iGalaGotBug == 1.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (A spare defragmentation tube, not hooked to a power source.)") ]])
        fnCutsceneBlocker()
    
    --Get the bug.
    else
        VM_SetVar("Root/Variables/Chapter5/Scenes/iGalaGotBug", "N", 1.0)
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Hmmmmmm...[P] [SOUND|World|TakeItem]got it![P] There's a little sticky metal object here.[P] This is probably the audio looper bug.)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (I'll deliver it to our mole so we can speak freely.)") ]])
        fnCutsceneBlocker()
    
    end
    
elseif(sObjectName == "Oilmaker") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Unflavoured oil.[P] Clearly this break room is not for Lord Units.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "EastExit") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "LatexDrone", "Neutral") ]])
    fnCutscene([[ Append("Drone:[E|Neutral] FOR SECURITY REASONS, WE POLITELY REQUEST THAT YOU NOT EXIT THIS AREA, LORD UNIT.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Of course.[P] Excellent work, Drone Unit.") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "WestExitN") then
    
    --Variables.
    local iHasPermission   = VM_GetVar("Root/Variables/Chapter5/Scenes/iHasPermission", "N")
    local iSawExitDialogue = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawExitDialogue", "N")
    
    --No permissions.
    if(iHasPermission == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "LatexDrone", "Neutral") ]])
        fnCutscene([[ Append("Drone:[E|Neutral] FOR SECURITY REASONS, WE POLITELY REQUEST THAT YOU NOT EXIT THIS AREA, LORD UNIT.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Of course.[P] Excellent work, Drone Unit.") ]])
        fnCutsceneBlocker()
    
    --Has permission, needs to see dialogue.
    elseif(iHasPermission == 1.0 and iSawExitDialogue == 0.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSawExitDialogue", "N", 1.0)
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "LatexDrone", "Neutral") ]])
        fnCutscene([[ Append("Drone:[E|Neutral] FOR SECURITY REASONS, WE POLITELY REQUEST THAT YOU NOT EXIT THIS AREA, LORD UNIT.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] I'm looking to be alone with my tandem unit.[P] Check your database, drone.[B][C]") ]])
        fnCutscene([[ Append("Drone:[E|Neutral] SCANNI -[P] SCAN COMPLETE.[P] PERMISSIONS UPDATED.[B][C]") ]])
        fnCutscene([[ Append("Drone:[E|Neutral] PLEASE PROCEED, LORD UNITS.[P] HAVE A SPIFFY DAY.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Shall we?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] After you.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    
        --Transition.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iGalaGoal4State", "N", 1.0)
        fnCutscene([[ AudioManager_PlaySound("World|FlipSwitch") ]])
        fnCutscene([[ AL_BeginTransitionTo("RegulusArcaneD", "FORCEPOS:30.5x9.0x0") ]])
        fnCutsceneBlocker()
    
    --Repeats.
    else
        AudioManager_PlaySound("World|FlipSwitch")
        AL_BeginTransitionTo("RegulusArcaneD", "FORCEPOS:30.5x9.0x0")
    end
    
elseif(sObjectName == "WestExitS") then
    
    --Variables.
    local iHasPermission   = VM_GetVar("Root/Variables/Chapter5/Scenes/iHasPermission", "N")
    local iSawExitDialogue = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawExitDialogue", "N")
    
    --No permissions.
    if(iHasPermission == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "LatexDrone", "Neutral") ]])
        fnCutscene([[ Append("Drone:[E|Neutral] FOR SECURITY REASONS, WE POLITELY REQUEST THAT YOU NOT EXIT THIS AREA, LORD UNIT.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Of course.[P] Excellent work, Drone Unit.") ]])
        fnCutsceneBlocker()
    
    --Has permission, needs to see dialogue.
    elseif(iHasPermission == 1.0 and iSawExitDialogue == 0.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSawExitDialogue", "N", 1.0)
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "LatexDrone", "Neutral") ]])
        fnCutscene([[ Append("Drone:[E|Neutral] FOR SECURITY REASONS, WE POLITELY REQUEST THAT YOU NOT EXIT THIS AREA, LORD UNIT.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] I'm looking to be alone with my tandem unit.[P] Check your database, drone.[B][C]") ]])
        fnCutscene([[ Append("Drone:[E|Neutral] SCANNI -[P] SCAN COMPLETE.[P] PERMISSIONS UPDATED.[B][C]") ]])
        fnCutscene([[ Append("Drone:[E|Neutral] PLEASE PROCEED, LORD UNITS.[P] HAVE A SPIFFY DAY.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Shall we?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] After you.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    
        --Transition.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iGalaGoal4State", "N", 1.0)
        fnCutscene([[ AudioManager_PlaySound("World|FlipSwitch") ]])
        fnCutscene([[ AL_BeginTransitionTo("RegulusArcaneD", "FORCEPOS:30.5x15.0x0") ]])
        fnCutsceneBlocker()
    
    --Repeats.
    else
        AudioManager_PlaySound("World|FlipSwitch")
        AL_BeginTransitionTo("RegulusArcaneD", "FORCEPOS:30.5x15.0x0")
    end

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
