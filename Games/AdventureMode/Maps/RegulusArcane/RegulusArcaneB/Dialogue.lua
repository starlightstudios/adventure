-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
    
    -- |[GolemGalaA]|
    if(sActorName == "GolemGalaA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Due to the gala, our research projects are suspended for the evening.[B][C]") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] That makes sense from a security perspective, but they have the nerve to not even invite us to the Gala![B][C]") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] What are we supposed to do for the evening?[P] Research the effects of frustration on a Lord Unit?") ]])
        fnCutsceneBlocker()
        
    -- |[GolemGalaB]|
    elseif(sActorName == "GolemGalaB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] My tandem unit is rather cross this evening.[P] We'll have to sneak onto the roof to watch the sunrise.") ]])
        fnCutsceneBlocker()
        
    -- |[GolemGalaC]|
    elseif(sActorName == "GolemGalaC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Greetings, Lord Units.[P] I'm plainclothes security.[P] I assure you my fashion sense is superb.") ]])
        fnCutsceneBlocker()
        
    -- |[GolemGalaD]|
    elseif(sActorName == "GolemGalaD") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Most of our uniformed security units are Drone Units.[P] So of course, I got assigned to babysit them in the headquarters...[B][C]") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] They haven't set anything on fire yet, so that's a good thing.") ]])
        fnCutsceneBlocker()
        
    -- |[GolemGalaE]|
    elseif(sActorName == "GolemGalaE") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Drone:[VOICE|LatexDrone] GREETINGS, LORD UNIT.[P] SECURITY STATUS:: SPIFFY.") ]])
        fnCutsceneBlocker()
        
    -- |[GolemGalaF]|
    elseif(sActorName == "GolemGalaF") then
        
        --Variables.
        local iBumpedIntoLatexDrone = VM_GetVar("Root/Variables/Chapter5/Scenes/iBumpedIntoLatexDrone", "N")
        local iGotStationary        = VM_GetVar("Root/Variables/Chapter5/Scenes/iGotStationary", "N")
        
        --Normal:
        if(iBumpedIntoLatexDrone == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Drone:[VOICE|LatexDrone] PATROLLING HALLWAYS.[P] PATROLLING HALLWAYS.[P] PATROLLING HALLWAYS...") ]])
            fnCutsceneBlocker()
        
        --Repeat:
        elseif(iBumpedIntoLatexDrone == 1.0 and iGotStationary == 0.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "LatexDrone", "Neutral") ]])
            fnCutscene([[ Append("Christine:[E|Offended] You stay right here, drone.[P] I'll go find something to write my complaints on.[B][C]") ]])
            fnCutscene([[ Append("Drone:[E|Neutral] AFFIRMATIVE, LORD UNIT.") ]])
            fnCutsceneBlocker()
        
        --Got the stationary.
        else
            VM_SetVar("Root/Variables/Chapter5/Scenes/iSentComplaint", "N", 1.0)
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "LatexDrone", "Neutral") ]])
            fnCutscene([[ Append("Christine:[E|Offended] ...[P] Unit bumped my tandem unit...[P] very displeased...[P] critical damages...[P] there.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] Unit, deliver this complaint to your superior officer right now.[B][C]") ]])
            fnCutscene([[ Append("Drone:[E|Neutral] AFFIRMATIVE, LORD UNIT.[P] MOVING TO SUPERIOR OFFICER.[P] THANK YOU FOR YOUR COMPLAINT.") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Movement.
            fnCutsceneMove("Christine", 20.75, 13.50)
            fnCutsceneMove("Sophie", 21.75, 13.50)
            fnCutsceneFace("Christine", 0, -1)
            fnCutsceneFace("Sophie", 0, -1)
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            fnCutsceneMove("GolemGalaF", 19.75, 12.50)
            fnCutsceneMove("GolemGalaF", 19.75,  4.50)
            fnCutsceneBlocker()
            fnCutsceneMove("Sophie", 20.75, 13.50)
            fnCutsceneBlocker()
            fnCutscene([[ AL_SetProperty("Fold Party") ]])
            fnCutsceneBlocker()
        end
        
    -- |[GolemGalaG]|
    elseif(sActorName == "GolemGalaG") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Drone:[VOICE|LatexDrone] THE AREAS BEYOND HERE REQUIRE SECURITY PERMISSIONS TO ACCESS, LORD UNIT.") ]])
        fnCutsceneBlocker()
       
    -- |[GolemGalaH]| 
    elseif(sActorName == "GolemGalaH") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Drone:[VOICE|LatexDrone] PLEASE DO NOT WANDER OUTSIDE OF THE DESIGNATED ENJOYMENT AREA, LORD UNIT.") ]])
        fnCutsceneBlocker()
        
    -- |[GolemGalaI]|
    elseif(sActorName == "GolemGalaI") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Drone:[VOICE|LatexDrone] FUN IS CURRENTLY WITHIN ACCEPTABLE LEVELS.") ]])
        fnCutsceneBlocker()
        
    -- |[GolemGalaJ]|
    elseif(sActorName == "GolemGalaJ") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Drone:[VOICE|LatexDrone] THIS UNIT IS ENJOYING THE MUSIC, AND PROTECTING THIS AREA.[P] IN ORDER OF PRIORITY.") ]])
        fnCutsceneBlocker()
        
    -- |[GolemGalaK]|
    elseif(sActorName == "GolemGalaK") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Drone:[VOICE|LatexDrone] RECHARGING...[P] RECHARGING...[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (The drone has stuck its hand into the outlet powering the terminal...)") ]])
        fnCutsceneBlocker()
        
    -- |[GolemGalaL]|
    elseif(sActorName == "GolemGalaL") then
        
        --Standard.
        local iGalaMetPrimeA = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaMetPrimeA", "N")
        local iGotPrimeEarrings = VM_GetVar("Root/Variables/Chapter5/Scenes/iGotPrimeEarrings", "N")
        if(iGalaMetPrimeA == 0.0 or iGotPrimeEarrings == 1.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Drone:[VOICE|LatexDrone] THIS UNIT BUMPED INTO A COMMAND UNIT.[P] PUNISHMENT IS TO REORGANIZE THE STATIONERY SUPPLIES HERE.") ]])
            fnCutsceneBlocker()
        else
            VM_SetVar("Root/Variables/Chapter5/Scenes/iGotPrimeEarrings", "N", 1.0)
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Drone:[VOICE|LatexDrone] THIS UNIT BUMPED INTO A COMMAND UNIT.[P] PUNISHMENT IS TO REORGANIZE THE STATIONERY SUPPLIES HERE.[B][C]") ]])
            fnCutscene([[ Append("Drone:[VOICE|LatexDrone] SORTING SUPPLIES...[B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] (Wait a minute, this Drone is carrying some earrings...)[B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] Drone Unit, I am requisitioning those.[P] Give them to me.[B][C]") ]])
            fnCutscene([[ Append("Thought: [SOUND|World|TakeItem]Got Earrings![B][C]") ]])
            fnCutscene([[ Append("Drone:[VOICE|LatexDrone] THANK YOU, LORD UNIT.[P] THIS UNIT COULD NOT FIGURE OUT WHERE TO SORT THOSE ITEMS.[B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] Resume your sorting, Drone Unit.[B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] (I guess I should return these to Prime Command Unit 1007...)") ]])
            fnCutsceneBlocker()
        end
        
    -- |[GolemGalaO]|
    elseif(sActorName == "GolemGalaO") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] It's really hard to play cards when your processor is perfectly computing the statistics of the next draw...") ]])
        fnCutsceneBlocker()
        
    -- |[GolemGalaP]|
    elseif(sActorName == "GolemGalaP") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] We have to stay in here until all the Lord Units leave and we can begin cleanup.") ]])
        fnCutsceneBlocker()
        
    -- |[GolemGalaQ]|
    elseif(sActorName == "GolemGalaQ") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] I guess this is as close to a sunrise party as I'm going to get.[P] This assignment bites.") ]])
        fnCutsceneBlocker()
        
    -- |[GolemGalaM]|
    --GolemGalaM is the contact objective.
    elseif(sActorName == "GolemGalaM") then
    
        --Variables.
        TA_SetProperty("Face Character", "PlayerEntity")
        local iGalaMetContact      = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaMetContact", "N")
        local iGalaGotBug          = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaGotBug", "N")
        local iGalaInstalledBug    = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaInstalledBug", "N")
        local iSawPostDance        = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawPostDance", "N")
        local iKnowToGetPermission = VM_GetVar("Root/Variables/Chapter5/Scenes/iKnowToGetPermission", "N")
        if(iGalaMetContact == 0.0) then
            VM_SetVar("Root/Variables/Chapter5/Scenes/iGalaMetContact", "N", 1.0)
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] Lord Golem?[P] May I help you?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Possibly.[P] Have you noticed the odd electrical interference in this building?[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] (Oh, the code phrase?)[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] Yes, but you can always enjoy the hum. Right?[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] *We can't speak here, Pink Leader.*[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] *I set up an audio looper in the break room, but the security units kicked us out to use it as their field headquarters.*[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] *The looper is in the spare defragmentation tube.[P] Bring it to me.*[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Ah, yes.[P] I'll recalibrate my PDU.[P] Thank you.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] *We're on it.*") ]])
            fnCutsceneBlocker()
        
        --Met contact, don't have the bug yet.
        elseif(iGalaMetContact == 1.0 and iGalaGotBug == 0.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] *The bug is in the spare defragmentation tube behind the western break room.*[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] Enjoy your night at the Gala, Lord Unit.") ]])
            fnCutsceneBlocker()
        
        --Has the bug. Need to install it.
        elseif(iGalaMetContact == 1.0 and iGalaGotBug == 1.0 and iGalaInstalledBug == 0.0) then
            VM_SetVar("Root/Variables/Chapter5/Scenes/iGalaInstalledBug", "N", 1.0)
            VM_SetVar("Root/Variables/Chapter5/Scenes/iGalaGoal0State", "N", 1.0)
            AL_SetProperty("Flag Objective True", "0: Meet Contact")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] *Click, click, click.*[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] Sorry about that, Pink Leader.[P] The audio looper is in place.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Can we speak freely?[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] Yes.[P] The security units won't be able to use the microphones to listen to us, the looper will just play the audio from earlier today.[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] They'll notice eventually, but probably sometime tomorrow.[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] Unfortunately, they've been pretty good at keeping me from doing any scouting for you.[P] Sorry.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] We'll have to do the legwork, then.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] By the way, my lovely tandem unit here is Sophie.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Smirk] Pleased to meet you.[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] Oh, what an enchanting dress...[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] Now, Lord Unit, I haven't been able to figure out who the acting security lead is for the gala.[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] But I do have an idea::[P] They're using a lot of Drone Units.[P] And, Drone Units are pretty dumb.[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] Maybe you can trick one into revealing who the security lead is?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Good idea.[P] I'll see what I can do.") ]])
            fnCutsceneBlocker()
        
        --Bug installed. Repeat conversation.
        elseif(iGalaInstalledBug == 1.0 and iSawPostDance == 0.0) then
        
            local iIdentifiedSecurityLead = VM_GetVar("Root/Variables/Chapter5/Scenes/iIdentifiedSecurityLead", "N")
            if(iIdentifiedSecurityLead == 0.0) then
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
                fnCutscene([[ Append("Golem:[E|Neutral] Sorry, I can't do much else for you.[P] But you can ask me any questions you may have.[B][C]") ]])
                fnCutscene([[ Append("Golem:[E|Neutral] I would recommend trying to trick a Drone Unit into revealing who the security lead of the gala is.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Worth a shot.[P] I'll ask around.") ]])
            else
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
                fnCutscene([[ Append("Golem:[E|Neutral] Sorry, I can't do much else for you.[P] But you can ask me any questions you may have.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] I don't have any right now, but I'll keep that in mind.[P] Stay safe.") ]])
            end
        
        --Post dance.
        elseif(iSawPostDance == 1.0 and iKnowToGetPermission == 0.0) then
            VM_SetVar("Root/Variables/Chapter5/Scenes/iKnowToGetPermission", "N", 1.0)
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] We're still clear to speak?[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] We are.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] It's about time we make our exit from the party.[P] It's almost two hours to sunrise.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] I need to make my way to the physics research block.[P] Any advice?[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] The research block is to the west of here.[P] You can take an underground passage, and I assume the security will be fairly light.[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] I don't know if you saw it, but I saw a lot of security units getting redeployed.[P] They ran through the middle of the hall there.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Blush] (Maybe I shouldn't admit I was caught up dancing...)[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Didn't see it, but that might be a good thing.[P] Any idea where they got redeployed?[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] No, I haven't a clue.[P] I'm still confined to this room.[P] I just peeked out into the hallway.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Hopefully my path will be clear of security units...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] But there's still a guard posted on the west side.[P] She'll definitely see if I try to leave.[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] Oh, uh well...[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] Y'see, Lord Unit,[P] there are rooms reserved for...[P] tandem units...[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] It's not something we're supposed to talk about, but, y'know, cleanup duty...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] I happen to have a tandem unit right here![B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Smirk] Shame we don't have time to, actually...[P] you know...[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] You'd need to get special permission from someone.[P] Probably a security unit, though a Command Unit would have authorization.[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] If you know someone high up who owes you a favour, now would be a good time to make use of it.[B][C]") ]])
            
            --Variables.
            local iReturnedPrimeEarrings = VM_GetVar("Root/Variables/Chapter5/Scenes/iReturnedPrimeEarrings", "N")
            if(iReturnedPrimeEarrings == 0.0) then
                fnCutscene([[ Append("Christine:[E|Smirk] Oh, Sophie![P] Wasn't Prime Command Unit 1007 looking for something near the shoe rack?[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Happy] I think so![P] If we help her out, maybe she'll help us out![B][C]") ]])
                fnCutscene([[ Append("Golem:[E|Neutral] Good thinking.[P] Good luck, Lord Unit.[B][C]") ]])
            else
                fnCutscene([[ Append("Christine:[E|Smirk] Actually, Unit 1007 owes me one.[P] Let's go talk to her.[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Happy] She's probably still out front.[B][C]") ]])
                fnCutscene([[ Append("Golem:[E|Neutral] Good luck, Lord Unit.[B][C]") ]])
            end
            
            --Return.
            fnCutscene([[ Append("Christine:[E|Smirk] But what about you, friend?[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] Oh, yeah.[P] Do you know what's going to happen?[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] Just that I need to make sure my crew is out of the building at the moment of sunrise.[P] Don't worry, we have a plan.[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] I'm going to claim that my crew needs to get some cleaning supplies.[P] We'll leave on the tram.[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] Whatever is going to happen, well, we won't be here to see it.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Good.[P] I guess we won't see each other again.[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] Not until Regulus City is a free place.[P] Once again, good luck, free machine sister.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Smirk] Same to you, free machine sister.") ]])
            fnCutsceneBlocker()
            
        --Repeat.
        elseif(iKnowToGetPermission == 1.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] You should get a higher-up to give you permission to use the reserved rooms on the west side of the building.[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] We'll meet again when the sun shines over free Regulus, machine sisters.") ]])
            fnCutsceneBlocker()
        
        end
        
    -- |[DroneSpecial]|
    elseif(sActorName == "DroneSpecial") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Drone:[VOICE|LatexDrone] AWAITING PUNISHMENT FROM SUPERIOR OFFICER.") ]])
        fnCutsceneBlocker()
        
    -- |[CommandGalaA]|
    --CommandGalaA is one of the prime command units.
    elseif(sActorName == "CommandGalaA") then
    
        --Variables.
        TA_SetProperty("Face Character", "PlayerEntity")
        local iGalaMetPrimeA = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaMetPrimeA", "N")
        local iKnowToGetPermission = VM_GetVar("Root/Variables/Chapter5/Scenes/iKnowToGetPermission", "N")
        local iHasPermission = VM_GetVar("Root/Variables/Chapter5/Scenes/iHasPermission", "N")
        
        --First meeting.
        if(iGalaMetPrimeA == 0.0) then
        
            --Flag.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iGalaMetPrimeA", "N", 1.0)
            
            --If all three primes are identified, flag this.
            local iGalaMetPrimeA = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaMetPrimeA", "N")
            local iGalaMetPrimeB = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaMetPrimeB", "N")
            local iGalaMetPrimeC = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaMetPrimeC", "N")
            if(iGalaMetPrimeA == 1.0 and iGalaMetPrimeB == 1.0 and iGalaMetPrimeC == 1.0) then
                VM_SetVar("Root/Variables/Chapter5/Scenes/iGalaGoal1State", "N", 1.0)
                AL_SetProperty("Flag Objective True", "1: Identify Prime Command Units")
            end
            
            --Re-resolve the display name.
            local sObj1DisplayName = fnResolveCommandUnitObjectiveName()
            AL_SetProperty("Set Objective Display Name", "1: Identify Prime Command Units", sObj1DisplayName)
            
            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Good evening, Command Unit.[P] Are you admiring the shoe rack?[B][C]") ]])
            fnCutscene([[ Append("Command Unit:[E|Neutral] Nothing of the sort, Lord Unit.[B][C]") ]])
            fnCutscene([[ Append("Command Unit:[E|Neutral] I misplaced my earrings and have been unable to locate them.[P] If I don't find them soon, I might have to divert these Drone Units to help me.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Unfortunate, but understandable.[B][C]") ]])
            fnCutscene([[ Append("Command Unit:[E|Neutral] Have we met?[P] I am searching my archives...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] I don't believe so, Command Unit.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Sad] T-t-[P]this...[B][C]") ]])
            fnCutscene([[ Append("Command Unit:[E|Neutral] What is it, Lord Unit?[P] Out with it![B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Sad] F-first...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] This is our first year at the gala.[P] Andrea here is a little nervous.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Sad] Apologies, Command Unit.[B][C]") ]])
            fnCutscene([[ Append("Command Unit:[E|Neutral] Hmmm...[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] (I hope she doesn't look too closely!!!)[B][C]") ]])
            fnCutscene([[ Append("Command Unit:[E|Neutral] ...[B][C]") ]])
            fnCutscene([[ Append("Command Unit:[E|Neutral] There is something unusual about you, isn't there?[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Sad] ..?[B][C]") ]])
            fnCutscene([[ Append("Command Unit:[E|Neutral] Hah![P] I see it now![P] It's because I am a Command Unit![P] I assume I am the first you've seen?[B][C]") ]])
            fnCutscene([[ Append("Command Unit:[E|Neutral] We're not ogres like many of the lower Lords make us out to be, I assure you.[P] Yes, we are intolerant of failure, but we also seek to reward good performance.[B][C]") ]])
            fnCutscene([[ Append("Command Unit:[E|Neutral] Even I was nervous my first year, Andrea.[P] Do your best and you will overcome it.[B][C]") ]])
            fnCutscene([[ Append("Command Unit:[E|Neutral] Just remember, you are a Lord Unit.[P] You must set an example for others, even other Lord Units.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] And I am Christine, of Sector 96.[P] Pleased to meet you.[B][C]") ]])
            fnCutscene([[ Append("Command Unit:[E|Neutral] Hmmm, Sector 96?[P] It's at the top of my priority search lists.[B][C]") ]])
            fnCutscene([[ Append("Command Unit:[E|Neutral] You are the Lord Unit that has made those impressive gains in productivity.[P] Suiting that you'd receive an invitation.[B][C]") ]])
            fnCutscene([[ Append("1007:[E|Neutral] I am Unit 1007.[P] I am the Prime Command Unit of Transportation.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Blush] It![P] Is is...[P] It is an honor, Prime Command Unit![B][C]") ]])
            fnCutscene([[ Append("1007:[E|Neutral] Oh stop, you're going to embarrass me.[B][C]") ]])
            fnCutscene([[ Append("1007:[E|Neutral] Transportation is not glorious like Security or Research or even Abductions, but it is no less vital to the operation of the city.[B][C]") ]])
            fnCutscene([[ Append("1007:[E|Neutral] Though no amount of authority is going to bring back my wayward earrings...[B][C]") ]])
            fnCutscene([[ Append("1007:[E|Neutral] Is it all right if I rant at you, Christine and Andrea?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Blush] Go right ahead, Prime Command Unit 1007![B][C]") ]])
            fnCutscene([[ Append("1007:[E|Neutral] This Gala has been a pain in the rear-input port to me, if you take my meaning.[B][C]") ]])
            fnCutscene([[ Append("1007:[E|Neutral] Not allowed to transmit anything over the network, yet somehow supposed to arrange for transport of hundreds of units using a single track?[B][C]") ]])
            fnCutscene([[ Append("1007:[E|Neutral] And I've spoken with the other Prime Command Units.[P] Apparently none of them issued the order to not use the network.[B][C]") ]])
            fnCutscene([[ Append("1007:[E|Neutral] With Unit 2855 missing, our security is a real mess.[P] Despite our frequent differences...[P] well, she was a Prime Command Unit for a reason![B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] (Wait a minute, even Unit 1007 doesn't know who gave the network-silence order?)[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] (What does this mean?)[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Blush] Unit 2855 is missing?[B][C]") ]])
            fnCutscene([[ Append("1007:[E|Neutral] Mmm, forget I said that.[P] It's not public knowledge.[B][C]") ]])
            fnCutscene([[ Append("1007:[E|Neutral] But honestly, I don't think it's much of a secret anymore.[P] Any unit with two processor cycles can figure out something is wrong in the city.[B][C]") ]])
            fnCutscene([[ Append("1007:[E|Neutral] Then again, perhaps I have vented on you poor units long enough.[B][C]") ]])
            fnCutscene([[ Append("1007:[E|Neutral] Please enjoy the Sunrise Gala, and continue putting in excellent productivity.[P] Make sure I see you at the Sunset Gala, Christine and Andrea![B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Blush] Absolutely, Prime Command Unit.[P] We'll be there...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] (Okay, I've identified a Prime Command Unit.[P] Making great progress!)") ]])
        
        --Repeats.
        else
        
            --Variables.
            local iGotPrimeEarrings = VM_GetVar("Root/Variables/Chapter5/Scenes/iGotPrimeEarrings", "N")
            local iReturnedPrimeEarrings = VM_GetVar("Root/Variables/Chapter5/Scenes/iReturnedPrimeEarrings", "N")
        
            --No earrings:
            if(iGotPrimeEarrings == 0.0 and iReturnedPrimeEarrings == 0.0) then
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
                fnCutscene([[ Append("1007:[E|Neutral] Where are those blasted earrings?[P] My kingdom for my earrings![B][C]") ]])
                fnCutscene([[ Append("1007:[E|Neutral] Well not literally, but you know what I mean.") ]])
        
            --Return them.
            elseif(iGotPrimeEarrings == 1.0 and iReturnedPrimeEarrings == 0.0) then
                VM_SetVar("Root/Variables/Chapter5/Scenes/iReturnedPrimeEarrings", "N", 1.0)
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Prime Command Unit, are these yours, perchance?[B][C]") ]])
                fnCutscene([[ Append("1007:[E|Neutral] My earrings![P] Where did you find them?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] A Drone Unit had them and thought they were stationery pins.[B][C]") ]])
                fnCutscene([[ Append("1007:[E|Neutral] THAT BLASTED -[P] Grrrr...[B][C]") ]])
                fnCutscene([[ Append("1007:[E|Neutral] I can't blame a Drone Unit for being stupid, it's my fault for not noticing.[B][C]") ]])
                fnCutscene([[ Append("1007:[E|Neutral] Thank you for your help, Christine.[B][C]") ]])
                fnCutscene([[ Append("1007:[E|Neutral] I notice that Andrea is still with you.[P] Are you perhaps tandem units?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] We are.[B][C]") ]])
                fnCutscene([[ Append("1007:[E|Neutral] I see.[P] Andrea, you are very lucky.[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Happy] I know, Prime Command Unit![P] She's very special![B][C]") ]])
                fnCutscene([[ Append("1007:[E|Neutral] I suppose I owe you a favour, Christine.[P] Enjoy the evening.[B][C]") ]])
                
                --Get permission for fun!
                if(iKnowToGetPermission == 1.0) then
                    VM_SetVar("Root/Variables/Chapter5/Scenes/iHasPermission", "N", 1.0)
                    fnCutscene([[ Append("Christine:[E|Blush] Actually...[P] how do I put this...[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Blush] It's hard to find a place to be alone with my tandem unit...[B][C]") ]])
                    fnCutscene([[ Append("1007:[E|Neutral] Ah ha![P] Oh, how lucky you are![B][C]") ]])
                    fnCutscene([[ Append("Sophie:[E|Blush] You don't know the half of it...[B][C]") ]])
                    fnCutscene([[ Append("1007:[E|Neutral] There are some rooms on the west side reserved for those who want to watch the sunrise in more private accommodations.[B][C]") ]])
                    fnCutscene([[ Append("1007:[E|Neutral] Usually they're reserved for high-ranking units, but I think you may be able to make use of the supply room.[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Blush] Oooh...[P] that's actually better than a formal room...[B][C]") ]])
                    fnCutscene([[ Append("Sophie:[E|Blush] H-[P]hey![P] Mind out of the gutter, Christine![B][C]") ]])
                    fnCutscene([[ Append("1007:[E|Neutral] I'll send the notification with my PDU.[P] Just use the doors on the west side.[B][C]") ]])
                    fnCutscene([[ Append("1007:[E|Neutral] Good evening, Lord Units.[P] Don't be so occupied that you miss the sunrise! Wah ha ha!") ]])
                
                else
                    fnCutscene([[ Append("Christine:[E|Smirk] We will, Prime Command Unit.") ]])
                end
            
            --Repeats.
            elseif(iReturnedPrimeEarrings == 1.0) then
            
                if(iKnowToGetPermission == 1.0 and iHasPermission == 0.0) then
                    VM_SetVar("Root/Variables/Chapter5/Scenes/iHasPermission", "N", 1.0)
                    fnCutscene([[ WD_SetProperty("Show") ]])
                    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
                    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
                    fnCutscene([[ Append("Christine:[E|Smirk] Errr, Prime Command Unit, we have a favour to ask...[B][C]") ]])
                    fnCutscene([[ Append("1007:[E|Neutral] Mm?[P] You know how to play the game well, I see.[P] Get in good graces with a superior, and then use the favour for advancement?[P] You're ambitious![B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Blush] Well, it's not social climbing I seek...[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Blush] How do I put this...[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Blush] It's hard to find a place to be alone with my tandem unit...[B][C]") ]])
                    fnCutscene([[ Append("1007:[E|Neutral] Ah ha![P] Oh, how lucky you are![B][C]") ]])
                    fnCutscene([[ Append("1007:[E|Neutral] She's very pretty, and I hope she's an intellect to match![B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Blush] I have a hard time choosing which I like most...[B][C]") ]])
                    fnCutscene([[ Append("Sophie:[E|Blush] Me too, actually![B][C]") ]])
                    fnCutscene([[ Append("1007:[E|Neutral] There are some rooms on the west side reserved for those who want to watch the sunrise in more private accommodations.[B][C]") ]])
                    fnCutscene([[ Append("1007:[E|Neutral] Usually they're reserved for high-ranking units, but I think you may be able to make use of the supply room.[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Blush] Oooh...[P] that's actually better than a formal room...[B][C]") ]])
                    fnCutscene([[ Append("Sophie:[E|Blush] H-[P]hey![P] Mind out of the gutter, Christine![B][C]") ]])
                    fnCutscene([[ Append("1007:[E|Neutral] I'll send the notification with my PDU.[P] Just use the doors on the west side.[B][C]") ]])
                    fnCutscene([[ Append("1007:[E|Neutral] Good evening, Lord Units.[P] Don't be so occupied that you miss the sunrise![P] Wah ha ha!") ]])
            
                else
                
                    --Variables.
                    local iDancedWithSophie = VM_GetVar("Root/Variables/Chapter5/Scenes/iDancedWithSophie", "N")
                
                    fnCutscene([[ WD_SetProperty("Show") ]])
                    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
                    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
                    fnCutscene([[ Append("1007:[E|Neutral] I'm looking forward to seeing you two dance in the main hall.[P] I'm sure you'll be most graceful.[B][C]") ]])
                    if(iDancedWithSophie == 0.0) then
                        fnCutscene([[ Append("1007:[E|Neutral] We Prime Command Units have scarcely a moment to ourselves, and thus rarely have tandem units.[P] Enjoy one another's company in our stead.") ]])
                    else
                        fnCutscene([[ Append("Christine:[E|Neutral] I'm sorry, Prime Command Unit, but we just came from there.[B][C]") ]])
                        fnCutscene([[ Append("1007:[E|Neutral] Oh, did I miss it?[P] I'd miss my own funeral at this rate.[B][C]") ]])
                        fnCutscene([[ Append("Christine:[E|Offended] (Oh, I wouldn't say that...)[B][C]") ]])
                        fnCutscene([[ Append("Christine:[E|Neutral] I'll be attending to other things this evening.[P] Be seeing you.") ]])
                        
                    end
                end
            end
        end
    end
end
