-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
--Sample
if(sObjectName == "Silence") then
    AL_SetProperty("Mandated Music Intensity", 20.0)

elseif(sObjectName == "TriggerN") then
    
    --Silence.
    AL_SetProperty("Mandated Music Intensity", 20.0)
    
    --Don't double-play.
    local iReviewedObjectives = VM_GetVar("Root/Variables/Chapter5/Scenes/iReviewedObjectives", "N")
    if(iReviewedObjectives == 1.0) then return end
        
    --Achievement.
    AM_SetPropertyJournal("Unlock Achievement", "FinishShopping")
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iReviewedObjectives", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iMainQuestBlock", "N", 26.0)
    
    --Scene.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
	fnCutscene([[ Append("Christine:[E|PDU] All right, let's get down to business.[P] PDU, list objectives.[B][C]") ]])
	fnCutscene([[ Append("Sophie:[E|Neutral] Objectives?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Smirk] There are some things that I need to do at the party before sunrise.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Smirk] Rub elbows, confirm identities...[P] you know...[B][C]") ]])
	fnCutscene([[ Append("Sophie:[E|Blush] *Spy stuff?*[P] Oh this is making me hot![B][C]") ]])
	fnCutscene([[ Append("Sophie:[E|Smirk] Is there any way I can help?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Smirk] Follow my lead, play along, and act like nothing unusual is happening.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|PDU] Objective 0::[P] Link up with our mole.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] We've got a sympathizer who was assigned to cleaning duty.[P] She can probably help us accomplish the rest of the objectives.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|PDU] Objective 1::[P] Identify the three Prime Command Units.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] We know there are three of them here, but we don't know what they're in charge of.[P] This will be useful information to know.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|PDU] Objective 2::[P] Find out who is in charge of local security.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] It will be a plainclothes operative, most likely.[P] We'll need to figure out who it is without asking directly.[B][C]") ]])
	fnCutscene([[ Append("Sophie:[E|Smirk] I'm sure someone will let it slip.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|PDU] Objective 3::[P] Dance with Sophie.[B][C]") ]])
	fnCutscene([[ Append("Sophie:[E|Blush] I -[P] I don't know how to dance![B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Smirk] I said to follow my lead, didn't I?[P] It'll be easy, I promise.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|PDU] Objective 4::[P] Find a reason to leave the party at the correct time.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Smirk] The security units probably won't want us wandering around.[P] We'll need a good reason.[B][C]") ]])
	fnCutscene([[ Append("Sophie:[E|Neutral] Hmmm....[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Smirk] We need to complete those objectives.[P] If you forget one, just come down here and we can review them.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Smirk] Just check the terminal over there and I'll make it look like we're checking arrival times.") ]])
    
    --Display the objectives on screen.
    AL_SetProperty("Register Objective", "0: Meet Contact")
    AL_SetProperty("Register Objective", "1: Identify Prime Command Units")
    AL_SetProperty("Register Objective", "2: Identify Security Head")
    AL_SetProperty("Register Objective", "3: Dance with Sophie")
    AL_SetProperty("Register Objective", "4: Leave Party")
    
elseif(sObjectName == "TriggerS") then
    AL_SetProperty("Mandated Music Intensity", 30.0)
    
elseif(sObjectName == "NoExitTrigger") then
end
