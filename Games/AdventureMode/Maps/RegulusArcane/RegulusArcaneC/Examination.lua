-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
-- |[Objects]|
if(sObjectName == "Piano") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A classically designed piano, set with motivators to play music according to preset sheet music.[P] Far less efficient than a simple speaker, but so much more elegant!)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BuffetA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Oh my, organic shrimp?[P] I've never had one...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Are you sure about this?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] *Num*[P][P][EMOTION|Sophie|Surprised] S-s-s[P]-s-s-s[P]SPICY!!![P] HOT!!![P] HOT HOT HOTTTTT!!!![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Blush] Are you okay?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] Organic food is the best![P] Oh, I want another![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Blush] But you just said it was really hot...[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] Oh my oral intake is on fire, but mmm -[P] spicy![P] I love it![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] *Num*[P][P][EMOTION|Sophie|Surprised] YOWCH HOT HOT HOT![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] Another![P] Another![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] *Num*[P][P][EMOTION|Sophie|Surprised] I'M GOING TO MELT MY POWER CORE!!!![P] AIIEEEE!!![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Laugh] Okay I better stop you there before you liquify![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Here, try one?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] No thanks, I'm good![P] Let's go!") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BuffetB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Organic spinach leaves from the habitat, and chipmunk meat rolls on a rubber bun! Yum!)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BuffetC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Look, Christine![P] Human Milk Cheese![P] Aged two years![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Sad] They make cheese from human milk?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Oh yes.[P] Human females produce milk, but it usually gets allocated to rations or fertilizer.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] But [P]*Num*[P] Ohh, it's so creamy![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Sad] Something about this seems a little off...[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] You don't want it?[P] More for me![P] *Num*") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BuffetD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Synthoney drizzled on top of copper wire...[P] Mmmm...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Woah![P] Sweet![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] It [P]*is*[P] pure sugar.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] I remember reading in a manual somewhere that sweetness is one of the few things our oral intakes measure the same way a human's does.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] I don't mind![P] Mmm![P] *Crunch*[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] This is the best Sunrise Gala ever![P] *Num*[P] *Num*!") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BuffetE") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Salad with vinegar?[P] And what's this?[P] *Num*[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Cry] No, Sophie, you don't eat the pot plant![P] It's there for decoration![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] But it tastes so good![P] *Num*[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Cry] (I'm going to shutdown out of sheer embarrassment!)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BuffetF") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Oooh, tripe![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Do you like it?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] *Num*[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] ...[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Offended] ...[P] No I do not...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Laugh] Okay, on to the next table!") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BuffetG") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (This table is empty.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BuffetH") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (This table is empty.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "KitchenDoor") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (This leads to the kitchen, but it's off limits for the party.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Sky") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The unmistakable glow of the sun lurks just below the horizon.[P] It won't be long now.)") ]])
    fnCutsceneBlocker()

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
