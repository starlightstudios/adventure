-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
--Volume handlers.
if(sObjectName == "TriggerClose") then
    AL_SetProperty("Mandated Music Intensity", 100.0)
    
elseif(sObjectName == "TriggerMed") then
    AL_SetProperty("Mandated Music Intensity", 80.0)
    
elseif(sObjectName == "TriggerFar") then
    AL_SetProperty("Mandated Music Intensity", 60.0)
    
-- |[Event Triggers]|
elseif(sObjectName == "TriggerDance") then
    
    --Variables.
    local iGalaInstalledBug       = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaInstalledBug", "N")
    local iIdentifiedSecurityLead = VM_GetVar("Root/Variables/Chapter5/Scenes/iIdentifiedSecurityLead", "N")
    local iGalaMetPrimeA          = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaMetPrimeA", "N")
    local iGalaMetPrimeB          = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaMetPrimeB", "N")
    local iDancedWithSophie       = VM_GetVar("Root/Variables/Chapter5/Scenes/iDancedWithSophie", "N")
    if(iDancedWithSophie == 1.0) then return end
    if(iGalaInstalledBug == 1.0 and iIdentifiedSecurityLead == 1.0 and iGalaMetPrimeA == 1.0 and iGalaMetPrimeB == 1.0) then
    
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iDancedWithSophie", "N", 1.0)
    
        --Clear the parallel script which is still running.
        Cutscene_HandleParallel("ClearAll")
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
        fnCutscene([[ Append("Christine:[E|Happy] Sophie, c'mon, we should dance![B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] *But we haven't found the third Prime yet...*[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] The third prime is five, silly.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] *I take your meaning.[P] I'll be scanning the crowd while we dance, don't worry.*[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] I think we've done enough visiting.[P] We should dance while the night is young enough.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Blush] O-[P]okay...[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Blush] But I don't know how to dance...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Just set a timer and synchronize with my footstep pattern.[P] Follow your feet where mine go and move your hips where I direct them.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] I'm rather experienced.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Blush] Y-[P]you are?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Teaching rich girls?[P] Of course I had to teach them to dance![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] *But I had to demonstrate with Professor Heidigger.[P] She did not know how to dance, no matter how much practice she got...*[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] You'll enjoy it.[P] I promise.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Blush] Okay, lead...[P] lead on...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Oh, a new track is starting![P] Let's go!") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Stop the music.
        fnCutscene([[ AudioManager_StopMusic() ]])
        fnCutsceneBlocker()
        
        --Variables.
        local fDancerAX = 23.25
        local fDancerAY = 18.50
        local fDancerBX = 26.25
        local fDancerBY = 18.50
        local fDancerCX = 26.25
        local fDancerCY = 16.50
        local fDancerDX = 23.25
        local fDancerDY = 16.50
        local fDancerSpeed = 2.00
        
        --Move the other dancers to their start positions.
        fnCutsceneMove("Christine",     fDancerDX + 0.00, fDancerDY + 0.00, fDancerSpeed)
        fnCutsceneMove("Sophie",        fDancerDX + 1.00, fDancerDY + 0.00, fDancerSpeed)
        fnCutsceneFace("Christine", 1, 0)
        fnCutsceneFace("Sophie", -1, 0)
        fnCutsceneMoveFace("DancerAB",  fDancerAX + 1.00, fDancerAY + 0.00, -1, 0, fDancerSpeed)
        fnCutsceneMoveFace("DancerAA",  fDancerAX + 0.00, fDancerAY + 0.00,  1, 0, fDancerSpeed)
        fnCutsceneMoveFace("DancerBB",  fDancerBX + 1.00, fDancerBY + 0.00, -1, 0, fDancerSpeed)
        fnCutsceneMoveFace("DancerBA",  fDancerBX + 0.00, fDancerBY + 0.00,  1, 0, fDancerSpeed)
        fnCutsceneMoveFace("DancerCB",  fDancerCX + 1.00, fDancerCY + 0.00, -1, 0, fDancerSpeed)
        fnCutsceneMoveFace("DancerCA",  fDancerCX + 0.00, fDancerCY + 0.00,  1, 0, fDancerSpeed)
        fnCutsceneBlocker()
        fnCutsceneWait(65)
        fnCutsceneBlocker()
        
        --Dancing function.
        local fnDance = function(iXOffsetA, iYOffsetA, iXOffsetB, iYOffsetB, iFaceX, iFaceY, bIsDiagonal)
            
            --Variables.
            local fDancerAX = 23.25
            local fDancerAY = 18.50
            local fDancerBX = 26.25
            local fDancerBY = 18.50
            local fDancerCX = 26.25
            local fDancerCY = 16.50
            local fDancerDX = 23.25
            local fDancerDY = 16.50
            local fDancerSpeed = 1.00
            local iWaitTicks = 45
            if(bIsDiagonal == true) then fDancerSpeed = 1.414 end
            
            --Arg check.
            fnCutsceneMoveFace("DancerAA",  fDancerAX + iXOffsetA, fDancerAY + iYOffsetA, iFaceX *  1.0, iFaceY *  1.0, fDancerSpeed)
            fnCutsceneMoveFace("DancerAB",  fDancerAX + iXOffsetB, fDancerAY + iYOffsetB, iFaceX * -1.0, iFaceY * -1.0, fDancerSpeed)
            fnCutsceneMoveFace("DancerBA",  fDancerBX + iXOffsetA, fDancerBY + iYOffsetA, iFaceX *  1.0, iFaceY *  1.0, fDancerSpeed)
            fnCutsceneMoveFace("DancerBB",  fDancerBX + iXOffsetB, fDancerBY + iYOffsetB, iFaceX * -1.0, iFaceY * -1.0, fDancerSpeed)
            fnCutsceneMoveFace("DancerCA",  fDancerCX + iXOffsetA, fDancerCY + iYOffsetA, iFaceX *  1.0, iFaceY *  1.0, fDancerSpeed)
            fnCutsceneMoveFace("DancerCB",  fDancerCX + iXOffsetB, fDancerCY + iYOffsetB, iFaceX * -1.0, iFaceY * -1.0, fDancerSpeed)
            fnCutsceneMoveFace("Christine", fDancerDX + iXOffsetA, fDancerDY + iYOffsetA, iFaceX *  1.0, iFaceY *  1.0, fDancerSpeed)
            fnCutsceneMoveFace("Sophie",    fDancerDX + iXOffsetB, fDancerDY + iYOffsetB, iFaceX * -1.0, iFaceY * -1.0, fDancerSpeed)
            fnCutsceneBlocker()
            fnCutsceneWait(iWaitTicks)
            fnCutsceneBlocker()
            
        end
        
        --Start the music.
        fnCutscene([[ AudioManager_PlayMusic("Waltz") ]])
        fnCutsceneBlocker()
        
        --Back and forth.
        for i = 1, 2, 1 do
        fnDance( 1.00, 0.00,  2.00, 0.00, 1, 0, false)
        fnDance( 0.00, 0.00,  1.00, 0.00, 1, 0, false)
        fnDance(-1.00, 0.00,  0.00, 0.00, 1, 0, false)
        fnDance( 0.00, 0.00,  1.00, 0.00, 1, 0, false)
        end
        
        --Spin.
        fnDance( 0.00,  0.00,  0.00,  1.00,  0,  1, true)
        fnDance( 0.00,  0.00, -1.00,  0.00, -1,  0, true)
        fnDance( 0.00,  0.00,  0.00, -1.00,  0, -1, true)
        fnDance( 0.00,  0.00,  1.00,  0.00,  1,  0, true)
        
        --Back and forth x 2
        fnDance( 1.00, 0.00,  2.00, 0.00, 1, 0, false)
        fnDance( 0.00, 0.00,  1.00, 0.00, 1, 0, false)
        fnDance(-1.00, 0.00,  0.00, 0.00, 1, 0, false)
        fnDance( 0.00, 0.00,  1.00, 0.00, 1, 0, false)
        fnDance( 1.00, 0.00,  2.00, 0.00, 1, 0, false)
        fnDance( 0.00, 0.00,  1.00, 0.00, 1, 0, false)
        fnDance(-1.00, 0.00,  0.00, 0.00, 1, 0, false)
        fnDance( 0.00, 0.00,  1.00, 0.00, 1, 0, false)
        
        --Spin.
        fnDance( 0.00,  0.00,  0.00,  1.00,  0,  1, true)
        fnDance( 0.00,  0.00, -1.00,  0.00, -1,  0, true)
        fnDance( 0.00,  0.00,  0.00, -1.00,  0, -1, true)
        fnDance( 0.00,  0.00,  1.00,  0.00,  1,  0, true)
        
        --Reverse spin.
        fnDance( 1.00, -1.00,  1.00,  0.00,  0,  1, true)
        fnDance( 2.00,  0.00,  1.00,  0.00, -1,  0, true)
        fnDance( 1.00,  1.00,  1.00,  0.00,  0, -1, true)
        fnDance( 0.00,  0.00,  1.00,  0.00,  1,  0, true)
        
        --Back and forth x 2
        fnDance( 1.00, 0.00,  2.00, 0.00, 1, 0, false)
        fnDance( 0.00, 0.00,  1.00, 0.00, 1, 0, false)
        fnDance(-1.00, 0.00,  0.00, 0.00, 1, 0, false)
        fnDance( 0.00, 0.00,  1.00, 0.00, 1, 0, false)
        fnDance( 1.00, 0.00,  2.00, 0.00, 1, 0, false)
        fnDance( 0.00, 0.00,  1.00, 0.00, 1, 0, false)
        fnDance(-1.00, 0.00,  0.00, 0.00, 1, 0, false)
        fnDance( 0.00, 0.00,  1.00, 0.00, 1, 0, false)
        
        --Up and Down
        fnDance( 0.00,  0.00,  1.00, -1.00,  1,  0, false)
        fnDance( 0.00, -1.00,  1.00,  0.00,  1,  0, false)
        fnDance( 0.00,  0.00,  1.00,  1.00,  1,  0, false)
        fnDance( 0.00,  0.00,  1.00,  0.00,  1,  0, false)
        
        --Reverse spin.
        fnDance( 1.00, -1.00,  1.00,  0.00,  0,  1, true)
        fnDance( 2.00,  0.00,  1.00,  0.00, -1,  0, true)
        fnDance( 1.00,  1.00,  1.00,  0.00,  0, -1, true)
        fnDance( 0.00,  0.00,  1.00,  0.00,  1,  0, true)
        
        --Spin.
        fnDance( 0.00,  0.00,  0.00,  1.00,  0,  1, true)
        fnDance( 0.00,  0.00, -1.00,  0.00, -1,  0, true)
        fnDance( 0.00,  0.00,  0.00, -1.00,  0, -1, true)
        fnDance( 0.00,  0.00,  1.00,  0.00,  1,  0, true)
        
        --Back and forth x 2
        fnCutscene([[ AL_SetProperty("Activate Fade", 600, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
        fnDance( 1.00, 0.00,  2.00, 0.00, 1, 0, false)
        fnDance( 0.00, 0.00,  1.00, 0.00, 1, 0, false)
        fnDance(-1.00, 0.00,  0.00, 0.00, 1, 0, false)
        fnDance( 0.00, 0.00,  1.00, 0.00, 1, 0, false)
        fnDance( 1.00, 0.00,  2.00, 0.00, 1, 0, false)
        fnDance( 0.00, 0.00,  1.00, 0.00, 1, 0, false)
        fnDance(-1.00, 0.00,  0.00, 0.00, 1, 0, false)
        fnDance( 0.00, 0.00,  1.00, 0.00, 1, 0, false)
        
        --Spin.
        fnDance( 0.00,  0.00,  0.00,  1.00,  0,  1, true)
        fnDance( 0.00,  0.00, -1.00,  0.00, -1,  0, true)
        fnDance( 0.00,  0.00,  0.00, -1.00,  0, -1, true)
        fnDance( 0.00,  0.00,  1.00,  0.00,  1,  0, true)
        
        --Reverse spin.
        fnDance( 1.00, -1.00,  1.00,  0.00,  0,  1, true)
        fnDance( 2.00,  0.00,  1.00,  0.00, -1,  0, true)
        fnDance( 1.00,  1.00,  1.00,  0.00,  0, -1, true)
        fnDance( 0.00,  0.00,  1.00,  0.00,  1,  0, true)
        
        --Back and forth x 2
        fnDance( 1.00, 0.00,  2.00, 0.00, 1, 0, false)
        fnDance( 0.00, 0.00,  1.00, 0.00, 1, 0, false)
        fnDance(-1.00, 0.00,  0.00, 0.00, 1, 0, false)
        fnDance( 0.00, 0.00,  1.00, 0.00, 1, 0, false)
        fnDance( 1.00, 0.00,  2.00, 0.00, 1, 0, false)
        fnDance( 0.00, 0.00,  1.00, 0.00, 1, 0, false)
        fnDance(-1.00, 0.00,  0.00, 0.00, 1, 0, false)
        fnDance( 0.00, 0.00,  1.00, 0.00, 1, 0, false)
        
        --Transition back to the next room.
        fnCutscene([[ AL_BeginTransitionTo("RegulusArcaneB", "FORCEPOS:14.0x26.0x0") ]])
        fnCutsceneBlocker()
    end
end
