-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
    
    -- |[GolemGalaA]|
    if(sActorName == "GolemGalaA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Organic hors d'oeuvres![P] Mmm![P] Perfect!") ]])
        fnCutsceneBlocker()
        
    -- |[GolemGalaB]|
    elseif(sActorName == "GolemGalaB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Don't tell anyone, but these organic foods are basically Raiju rations that are seasoned.[P] I used to work in the biolabs, I saw it myself.") ]])
        fnCutsceneBlocker()
        
    -- |[GolemGalaC]|
    elseif(sActorName == "GolemGalaC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] There's a viewscreen behind the curtain that will show the sunset in a few hours.[P] I'm staking my spot now!") ]])
        fnCutsceneBlocker()
        
    -- |[GolemGalaD]|
    elseif(sActorName == "GolemGalaD") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] My goodness, who are you wearing?[P] You look fantabulous!") ]])
        fnCutsceneBlocker()
        
    -- |[GolemGalaE]|
    elseif(sActorName == "GolemGalaE") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] My tandem unit is embarrassed because we bought the same dress.[P] But it's so strange, I went into one store, and she went into the one across the hall.[P] How did we come out with the same outfit?") ]])
        fnCutsceneBlocker()
        
    -- |[GolemGalaF]|
    elseif(sActorName == "GolemGalaF") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] The organic sweets this year aren't as good as usual.[P] We've all seen chocolate and caviar, bring on the pizza, I say!") ]])
        fnCutsceneBlocker()
        
    -- |[GolemGalaG]|
    elseif(sActorName == "GolemGalaG") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] I suppose one advantage of being assigned to security is that I get to attend the Gala.[P] I've never received an invitation, but I always attend.") ]])
        fnCutsceneBlocker()
        
    -- |[GolemGalaH]|
    elseif(sActorName == "GolemGalaH") then
        
        --Variables.
        local iSentComplaint          = VM_GetVar("Root/Variables/Chapter5/Scenes/iSentComplaint", "N")
        local iIdentifiedSecurityLead = VM_GetVar("Root/Variables/Chapter5/Scenes/iIdentifiedSecurityLead", "N")
        
        --Normal.
        if(iSentComplaint == 0.0 or iIdentifiedSecurityLead == 1.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Did you need something, Lord Unit?[P] I'm busy.") ]])
            fnCutsceneBlocker()
        
        --Identify the security leader!
        else
            VM_SetVar("Root/Variables/Chapter5/Scenes/iIdentifiedSecurityLead", "N", 1.0)
            VM_SetVar("Root/Variables/Chapter5/Scenes/iGalaGoal2State", "N", 1.0)
            AL_SetProperty("Flag Objective True", "2: Identify Security Head")
            
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "LatexDrone", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] Goodness, what is it now?[P] What minor offense have you committed?[B][C]") ]])
            fnCutscene([[ Append("Drone:[E|Neutral] THIS COMPLAINT IS TO BE HAND DELIVERED TO MY SUPERIOR OFFICER.[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] ...[P] Bumped into a Lord Unit...[P] Very upset...[P] Oh dear...[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] Tch.[P] It's not my idea to use Drone Units, this is all I've been allocated.[P] Stupid drone![B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] I'll figure out your punishment later.[P] Stand there quietly while I update my PDU...[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] Oh, hello, Lord Unit.[P] Did you need something?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Oh, no.[P] Just visiting.[P] I thought this room might be open.[P] Who might you be?[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] Lord Unit 592311, Lord Golem of Engineering, Sector 200.[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] *Though at the moment, I am Lord Golem of Being Stressed Out by Idiot Subordinates, Sector 0.*[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] This room is a private room.[P] Not to be rude, but I'm rather busy at the moment.[P] Come back later.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] (Yeah that pretty much confirms that this is the local security chief here.)[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] (I assume her designation is correct, but she doesn't talk like an engineer.[P] Probably a cover story.)[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] (Objective completed.[P] On to the next one.)") ]])
            fnCutsceneBlocker()
        
        end
        
    -- |[GolemGalaI / J]|
    elseif(sActorName == "GolemGalaI" or sActorName == "GolemGalaJ") then
        
        --Variables.
        local iGotBullied = VM_GetVar("Root/Variables/Chapter5/Scenes/iGotBullied", "N")
        
        --First time.
        if(iGotBullied == 0.0) then
            
            --Flag.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iGotBullied", "N", 1.0)
            
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "GolemFancyE", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "GolemFancyF", "Neutral") ]])
            fnCutscene([[ Append("Pink:[E|Neutral] Who do we have here?[P] A pretender?[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Blush] Eep![B][C]") ]])
            fnCutscene([[ Append("White:[E|Neutral] Tch, at least this one knows her place.[P] You, apparently, don't.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] What are you two on about?[P] We haven't even said anything yet![B][C]") ]])
            fnCutscene([[ Append("Pink:[E|Neutral] Oh, you don't need to say a thing.[P] We can smell it on you.[B][C]") ]])
            fnCutscene([[ Append("Pink:[E|Neutral] You stink like a repair bay.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Blush] B-but...[B][C]") ]])
            fnCutscene([[ Append("White:[E|Neutral] Oh it's talking![P] Look at it go![B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] You leave my tandem unit alone.[P] You have a problem, you bring it to me.[B][C]") ]])
            fnCutscene([[ Append("White:[E|Neutral] I think I upset the butch one.[P] Ha ha![B][C]") ]])
            fnCutscene([[ Append("Pink:[E|Neutral] Bah.[P] Listen up, pup.[P] I'll give you some advice, since you clearly don't belong here.[B][C]") ]])
            fnCutscene([[ Append("Pink:[E|Neutral] Units like you who apparently work with their hands?[P] Those hands with microscopic traces of lubricant?[B][C]") ]])
            fnCutscene([[ Append("Pink:[E|Neutral] You give the rest of us a bad name.[B][C]") ]])
            fnCutscene([[ Append("White:[E|Neutral] I'm not sure I can keep this up.[P] The stink is going to get on my dress.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Angry] I'm a repair unit.[P] Your vocal synthesiser is going to need maintenance if you don't shut the hell up![B][C]") ]])
            fnCutscene([[ Append("White:[E|Neutral] Oh please, don't strike with me with your big man-hands![B][C]") ]])
            fnCutscene([[ Append("Pink:[E|Neutral] Wah ha ha![P] Excellent![B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Sad] Let's just go.[P] C'mon.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Angry] I'll rip your head off and shove it down your tandem unit's oral intake![P] I'll -[P] I'll!!![B][C]") ]])
            fnCutscene([[ Append("Pink:[E|Neutral] Picking a fight with two Lord Units who outrank you?[P] I wonder what security will think.[B][C]") ]])
            fnCutscene([[ Append("White:[E|Neutral] Be a shame to get kicked out of your first and soon to be only Gala, wouldn't it?[B][C]") ]])
            fnCutscene([[ Append("White:[E|Neutral] Do be a dear and kindly taff off.[P] Perhaps you can aid the cleaning units when your superiors are done enjoying themselves.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Sad] *It's not worth it.[P] C'mon, Christine.*[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] *Okay, okay.[P] Can't lose my cool now.*[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] We'll be taking our leave now, Lords.[P] It'd be uncouth to keep inconveniencing you so.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] Gosh, I remember my first fashion disaster.[P] I wish you two the best.[P] If you need help next year, I know a few units who are good with dressmaking.[B][C]") ]])
            fnCutscene([[ Append("White:[E|Neutral] Excuse me?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] I assume you're hiding in here because your outfits look like a Darkmatter's puke.[P] Aren't you?[B][C]") ]])
            fnCutscene([[ Append("Pink:[E|Neutral] You what, pigeon?[B][C]") ]])
            fnCutscene([[ Append("White:[E|Neutral] Impertinent little -[P] Grrrr![B][C]") ]])
            fnCutscene([[ Append("Pink:[E|Neutral] You taffin' what?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] (Okay, *now* is the time to leave!)") ]])
            fnCutsceneBlocker()
        
        --Repeats.
        else
            
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "GolemFancyE", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "GolemFancyF", "Neutral") ]])
            fnCutscene([[ Append("Pink:[E|Neutral] You gormless little trollop![B][C]") ]])
            fnCutscene([[ Append("White:[E|Neutral] Our dresses are peerless![P] You -[P] you have no fashion sense![B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] You seem upset.[P] I'll give you some space.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] (Just let 'em sputter and don't let them feel like they won.[P] There's no win scenario here, just walk away.)") ]])
            fnCutsceneBlocker()
        end
        
    -- |[GolemGalaK]|
    elseif(sActorName == "GolemGalaK") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] I prefer the night to the day.[P] The city is so wonderous to gaze upon in the dark...") ]])
        fnCutsceneBlocker()
        
    -- |[GolemGalaL]|
    elseif(sActorName == "GolemGalaL") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Some of these seats are reserved, but otherwise it's first come first serve.[P] How quaint.[P] Shouldn't the highest ranking units get preferred seating?") ]])
        fnCutsceneBlocker()
        
    -- |[GolemGalaM]|
    elseif(sActorName == "GolemGalaM") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] I'm not normally partial to organic cuisine, but this is some of the finest prepared food in the solar system.") ]])
        fnCutsceneBlocker()
        
    -- |[GolemGalaN]|
    elseif(sActorName == "GolemGalaN") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Isn't it lovely being surrounded by high class units?[P] I almost regret having to return to my slave units tomorrow.") ]])
        fnCutsceneBlocker()
        
    -- |[CommandGalaA]|
    elseif(sActorName == "CommandGalaA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
        fnCutscene([[ Append("Command Unit:[E|Neutral] Oh my, I've not seen you before.[P] Is this your first Sunrise Gala?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] It is, Command Unit.[B][C]") ]])
        fnCutscene([[ Append("Command Unit:[E|Neutral] Heh, don't be so formal.[P] I'm just here for my fans.[B][C]") ]])
        fnCutscene([[ Append("Command Unit:[E|Neutral] I'm Unit 762.[P] The song you're hearing on this auto-piano was composed by me.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Happy] THE Unit 762?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Happy] Didn't you also write the score for 'Regulus Needs Women' and 'A Steamwork Orange'?[B][C]") ]])
        fnCutscene([[ Append("Command Unit:[E|Neutral] Yes, I've scored several videographs.[P] I've accumulated quite the library over time.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Are you Prime Command Unit of Arts or something?[P] It's my first year after conversion, so I'm not familiar with your work yet.[B][C]") ]])
        fnCutscene([[ Append("Command Unit:[E|Neutral] A first year Lord Unit at the Sunrise Gala?[P] Impressive, that.[B][C]") ]])
        fnCutscene([[ Append("Command Unit:[E|Neutral] I'm afraid artistic works have no Prime Command Unit, my dear.[P] I was just blessed with creativity and, after years of service, freed to pursue my passions.[B][C]") ]])
        fnCutscene([[ Append("Command Unit:[E|Neutral] Long ago, I was in charge of construction of this very university.[P] It seems like a lifetime ago.[B][C]") ]])
        fnCutscene([[ Append("Command Unit:[E|Neutral] I composed in my spare time, and units who listen to music while doing repetitive tasks tend to increase output, so the Prime Command Units reassigned me to compose full time.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Happy] It's wonderful to meet you, Command Unit![P] I've been a fan for many years![B][C]") ]])
        fnCutscene([[ Append("Command Unit:[E|Neutral] Please enjoy the party, then![P] I want everyone to hear my music, and all you need to do to befriend me is listen![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] (Unfortunately, Unit 762 wasn't one of the Prime Command Units we're after...)") ]])
        fnCutsceneBlocker()
        
    -- |[CommandGalaB]|
    elseif(sActorName == "CommandGalaB") then
        
        --Variables.
        local iGalaMetPrimeB = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaMetPrimeB", "N")
        
        --First time.
        if(iGalaMetPrimeB == 0.0) then
            
            --Flag.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iGalaMetPrimeB", "N", 1.0)
            
            --If all three primes are identified, flag this.
            local iGalaMetPrimeA = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaMetPrimeA", "N")
            local iGalaMetPrimeB = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaMetPrimeB", "N")
            local iGalaMetPrimeC = VM_GetVar("Root/Variables/Chapter5/Scenes/iGalaMetPrimeC", "N")
            if(iGalaMetPrimeA == 1.0 and iGalaMetPrimeB == 1.0 and iGalaMetPrimeC == 1.0) then
                VM_SetVar("Root/Variables/Chapter5/Scenes/iGalaGoal1State", "N", 1.0)
                AL_SetProperty("Flag Objective True", "1: Identify Prime Command Units")
            end
            
            --Re-resolve the display name.
            local sObj1DisplayName = fnResolveCommandUnitObjectiveName()
            AL_SetProperty("Set Objective Display Name", "1: Identify Prime Command Units", sObj1DisplayName)
        
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
            fnCutscene([[ Append("Command Unit:[E|Neutral] Hrmph.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Is something the matter, Command Unit?[B][C]") ]])
            fnCutscene([[ Append("Command Unit:[E|Neutral] I dislike formal functions like this.[P] I prefer field work.[B][C]") ]])
            fnCutscene([[ Append("Command Unit:[E|Neutral] But I must keep up appearances for some foolish archaic reason.[P] I don't understand it.[B][C]") ]])
            fnCutscene([[ Append("Command Unit:[E|Neutral] Machines do not require social interaction.[P] These units are massaging their egos when there is work to be done.[B][C]") ]])
            fnCutscene([[ Append("Command Unit:[E|Neutral] Tell me, Lord Unit.[P] What is there that is special about the sun rising over the city?[P] It is something I see every day, you know.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] You do?[P] How?[B][C]") ]])
            fnCutscene([[ Append("Command Unit:[E|Neutral] I suppose I have failed to introduce myself.[P] I am Unit 3014, Prime Command Unit of Abductions.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] Oh my, we're sorry, Prime Command Unit.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] We meant no disrespect.[B][C]") ]])
            fnCutscene([[ Append("Command Unit:[E|Neutral] The formalities and petty grievances of the city are another reason I dislike it here.[B][C]") ]])
            fnCutscene([[ Append("Command Unit:[E|Neutral] Speak freely and openly, Lord Units.[P] I prefer it that way.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Abductions, though?[P] That must be exciting work.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Happy] Tracking down humans, capturing them, dragging them off for conversion...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Blush] Watching their flesh turn to metal, their bodies and minds perfected...[B][C]") ]])
            fnCutscene([[ Append("Command Unit:[E|Neutral] You remind me of me, in the old days.[B][C]") ]])
            fnCutscene([[ Append("Command Unit:[E|Neutral] Unfortunately, my work is largely administrative.[P] I operate our field headquarters on Pandemonium.[B][C]") ]])
            fnCutscene([[ Append("Command Unit:[E|Neutral] I do periodically get involved in frontline work, and let me tell you -[P] the thrill of conversion, there's nothing else like it.[B][C]") ]])
            fnCutscene([[ Append("Command Unit:[E|Neutral] But my job is more likely to be identifying and approving candidates for capture after surveillace.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] You don't just grab any human you find?[B][C]") ]])
            fnCutscene([[ Append("Command Unit:[E|Neutral] In the old days, we certainly did.[P] Sometimes, we still do, when we need more dumb labour units.[B][C]") ]])
            fnCutscene([[ Append("Command Unit:[E|Neutral] But humans with solid work ethic and docile personalities make the best labourers, easily worth three of their undisciplined counterparts.[P] That's one of the reasons we started the breeding program, after all.[B][C]") ]])
            fnCutscene([[ Append("Command Unit:[E|Neutral] Plus there's the work of provisioning, administration, moving the headquarters, ugh.[P] The list goes on.[B][C]") ]])
            fnCutscene([[ Append("Command Unit:[E|Neutral] But the smell of wild plant life, the shining sun, and the symphony of night creatures...[P] I love Pandemonium.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Regulus City has its charms...[B][C]") ]])
            fnCutscene([[ Append("Command Unit:[E|Neutral] Sterile, lifeless, mechanical.[B][C]") ]])
            fnCutscene([[ Append("Command Unit:[E|Neutral] Good traits, to be sure, but I'll still take a night watching bats chase moths in infrared.[B][C]") ]])
            fnCutscene([[ Append("Command Unit:[E|Neutral] Oh, but I suppose I've talked your aural receptors off.[P] Another habit, as intelligent conversation is a rare find, planetside.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Good evening then, Prime Command Unit.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] (Okay, making progress on my objectives.[P] Prime Command Unit, identified!)") ]])
            fnCutsceneBlocker()
        
        --Repeats.
        else
        
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
            fnCutscene([[ Append("Command Unit:[E|Neutral] If you desire a change of career, Lord Unit, I would be pleased to have you in Abductions.[B][C]") ]])
            fnCutscene([[ Append("Command Unit:[E|Neutral] That kind of enthusiasm is surprisingly rare.[P] Many units view conversion as a necessity at best.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Perhaps they didn't enjoy theirs as much as I did.[B][C]") ]])
            fnCutscene([[ Append("Command Unit:[E|Neutral] Perhaps, though the Golem Core is supposed to make the process as pleasurable as possible.[B][C]") ]])
            fnCutscene([[ Append("Command Unit:[E|Neutral] We've been discussing allowing units to maintain more of their human memories, but there are technical hurdles to be overcome.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] I'd argue less.[P] Perhaps reprogramming them to believe they were raised in the breeding program instead of captured would reduce trauma.[B][C]") ]])
            fnCutscene([[ Append("Command Unit:[E|Neutral] A novel position.[P] I'll present it to my officers at our next meeting.[P] Thank you, Lord Unit.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Smirk] You're really into converting humans, aren't you?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Laugh] What can I say?[P] I love being a Golem, and I want to share it with everyone!") ]])
            fnCutsceneBlocker()
        end
        
    -- |[CommandGalaC]|
    elseif(sActorName == "CommandGalaC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
        fnCutscene([[ Append("Command Unit:[E|Neutral] I don't get together very often with my friends, so I treasure every Gala that comes up.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Why don't you take time off?[P] Aren't you in charge of your own schedule?[B][C]") ]])
        fnCutscene([[ Append("Command Unit:[E|Neutral] No, the Prime Command Units are.[P] They're the ones who converse with Central Administration the most.[B][C]") ]])
        fnCutscene([[ Append("Command Unit:[E|Neutral] Obviously I have more discretion than, say, a Lord Unit.[P] But we are constantly evaluated by the Primes.[B][C]") ]])
        fnCutscene([[ Append("Command Unit:[E|Neutral] Taking time off for personal reasons is likely to get me demoted.[P] Personal pursuits are unbecoming of a perfect machine.[B][C]") ]])
        fnCutscene([[ Append("Command Unit:[E|Neutral] We do not age, tire, or become sick.[P] Therefore we are expected to maintain uptimes approaching 100 percent.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] (The way she's speaking, she can't be a Prime Command Unit.[P] No need to get her designation, then.)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] Being a Command Unit sounds like a difficult burden...[B][C]") ]])
        fnCutscene([[ Append("Command Unit:[E|Neutral] It is.[P] Maintaining order among petty Lord Units is probably the most demanding part of my job.[B][C]") ]])
        fnCutscene([[ Append("Command Unit:[E|Neutral] But I was chosen because I am able to do it, so I do it for the city, and for Central Administration.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] What about the Cause of Science?[B][C]") ]])
        fnCutscene([[ Append("Command Unit:[E|Neutral] As I see it, Central Administration is the living manifestation of the Cause.[P] Serve it, serve the Cause.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] I see.[P] Nobody has ever phrased it that way to me.[P] Good evening, Command Unit.") ]])
    
    -- |[Generic]|
    --Generic golem that does not talk to you.
    elseif(string.sub(sActorName, 1, 8) == "GenGolem") then
    
        --Random roll.
        local iRoll = LM_GetRandomNumber(1, 24)
    
        --Dialogues.
        if(iRoll == 1) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] (Blah blah...) So then I said 'Not in this department, Slave Unit!'[P] Ha ha ha![P] (Blah blah...)") ]])
            fnCutsceneBlocker()
            
        elseif(iRoll == 2) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] (Blah blah...) I don't think I noticed it.[P] Go on, tell me more![P] (Blah blah...)") ]])
            fnCutsceneBlocker()
            
        elseif(iRoll == 3) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] (Blah blah...) Not the right kind of assignment, if you catch my drift.[P] (Blah blah...)") ]])
            fnCutsceneBlocker()
            
        elseif(iRoll == 4) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] (Blah blah...) Really?[P]  Three hundred at once?[P] Impressive![P] (Blah blah...)") ]])
            fnCutsceneBlocker()
            
        elseif(iRoll == 5) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] (Blah blah...) But then how did she get the cow[P] *off*[P] the roof?[P] (Blah blah...)") ]])
            fnCutsceneBlocker()
            
        elseif(iRoll == 6) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] (Blah blah...) Ugh, sounds like a real headache for the cleanup crew.[P] (Blah blah...)") ]])
            fnCutsceneBlocker()
            
        elseif(iRoll == 7) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] (Blah blah...) Oh, where did you get your nails done?[P] They're marvellous![P] (Blah blah...)") ]])
            fnCutsceneBlocker()
            
        elseif(iRoll == 8) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] (Blah blah...) Yes yes, my budget got cut too...[P] (Blah blah...)") ]])
            fnCutsceneBlocker()
            
        elseif(iRoll == 9) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] (Blah blah...) Do we really need so many?[P] Oh, yes, yes, I see.[P] (Blah blah...)") ]])
            fnCutsceneBlocker()
            
        elseif(iRoll == 10) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] (Blah blah...) And then it turned out she was a Raiju in disguise the whole time![P] I was floored![P] (Blah blah...)") ]])
            fnCutsceneBlocker()
            
        elseif(iRoll == 11) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] (Blah blah...) But that's not the worst part...[P] (Blah blah...)") ]])
            fnCutsceneBlocker()
            
        elseif(iRoll == 12) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] (Blah blah...) Oh, I heard that about Unit 9923.[P] Tell me more.[P] (Blah blah...)") ]])
            fnCutsceneBlocker()
            
        elseif(iRoll == 13) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] (Blah blah...) Seriously, she thinks that's enough for a month?[P] (Blah blah...)") ]])
            fnCutsceneBlocker()
            
        elseif(iRoll == 14) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] (Blah blah...) Oh I had trouble walking, it was so good last night...[P] (Blah blah...)") ]])
            fnCutsceneBlocker()
            
        elseif(iRoll == 15) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] (Blah blah...) The repair units?[P] Pfft, unlikely![P] (Blah blah...)") ]])
            fnCutsceneBlocker()
            
        elseif(iRoll == 16) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] (Blah blah...) Huh, I had heard they were wiped out.[P] Good to hear.[P] (Blah blah...)") ]])
            fnCutsceneBlocker()
            
        elseif(iRoll == 17) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] (Blah blah...) But I had already bought that doll for her![P] She was so embarrassed![P] (Blah blah...)") ]])
            fnCutsceneBlocker()
            
        elseif(iRoll == 18) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] (Blah blah...) AND IT WAS SO SPICY OH MY GOSH!!![P] (Blah blah...)") ]])
            fnCutsceneBlocker()
            
        elseif(iRoll == 19) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] (Blah blah...) So this girl loses her memories and has to attend high school, but these two make her join a paranormal investigation club![P] Ha ha![P] (Blah blah...)") ]])
            fnCutsceneBlocker()
            
        elseif(iRoll == 20) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] (Blah blah...) And I said POW![P] Kane Kola Kancel![P] She was so mad![P] (Blah blah...)") ]])
            fnCutsceneBlocker()
            
        elseif(iRoll == 21) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] (Blah blah...) I was going over the numbers, and they are not good.[P] (Blah blah...)") ]])
            fnCutsceneBlocker()
            
        elseif(iRoll == 22) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] (Blah blah...) But she looked right at me and said 'That's my fetish'.[P] (Blah blah...)") ]])
            fnCutsceneBlocker()
            
        elseif(iRoll == 23) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] (Blah blah...) Oh sure, let's just say, thank goodness we can't get STIs.[P] (Blah blah...)") ]])
            fnCutsceneBlocker()
            
        elseif(iRoll == 24) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] (Blah blah...) Dignity was beneath her, typical![P] (Blah blah...)") ]])
            fnCutsceneBlocker()
        end
    end
end
