-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
if(sObjectName == "55Begin") then
    
    --Repeat check.
    local iStarted55Sequence = VM_GetVar("Root/Variables/Chapter5/Scenes/iStarted55Sequence", "N")
    if(iStarted55Sequence == 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iStarted55Sequence", "N", 1.0)
    
    --Black the screen out.
    fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Lock camera.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 5.0) --Default is 5.0
        CameraEvent_SetProperty("Focus Position", (37.25 * gciSizePerTile), (14.50 * gciSizePerTile))
    DL_PopActiveObject()
    
    --Spawn 55. If needed, spawn SX-399.
    fnSpecialCharacter("Tiffany", -100, -100, gci_Face_South, false, nil)
    local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
    if(iSXUpgradeQuest >= 3.0) then
        fnSpecialCharacter("SX-399", 10, 8, gci_Face_North, false, nil)
    end
    
    --Move Christine off the field. Sophie too, she might still be in the party.
    fnCutsceneTeleport("Christine", -100.25, -100.50)
    fnCutsceneTeleport("Sophie", -100.25, -100.50)
    fnCutsceneBlocker()
    
    --Adjust the party to be just 55 for now.
	gsPartyLeaderName = "Tiffany"
    AL_SetProperty("Unfollow Actor Name", "Sophie")
    WD_SetProperty("Set Leader Voice", "Tiffany")
	giFollowersTotal = 0
	gsaFollowerNames = {}
	giaFollowerIDs = {0}
    EM_PushEntity("Tiffany")
        local i55sID = RO_GetID()
    DL_PopActiveObject()
	AL_SetProperty("Player Actor ID", i55sID)
    
    --Wait a bit.
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Fade screen in.
    fnCutsceneWait(45)
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    
    --55 Appears.
    fnCutsceneTeleport("Tiffany", 37.25, 14.50)
    fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
    fnCutscene([[ Append("55:[E|Neutral] (No hostiles on motion tracker, scanning for security pulses...)[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] (All checks still green.[P] No network traffic.)[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] (Do not question good fortune.[P] Proceed with the mission.[P] Begin radio silence.)[B][C]") ]])
    fnCutscene([[ Append("55:[E|Smirk] (Here we go...)") ]])
    fnCutsceneBlocker()
    
    --Player light properties.
    fnCutscene([[ AL_SetProperty("Deactivate Player Light") ]])
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Activate Player Light", 3600, 3600) ]])
    
elseif(sObjectName == "WhereIsEveryone") then
    
    --Repeat check.
    local iSawWhereIsEveryone = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawWhereIsEveryone", "N")
    if(iSawWhereIsEveryone == 1.0) then return end
    
    --Variables.
    local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
    
    --SX-399 does not join the party:
    if(iSXUpgradeQuest < 3.0) then
    
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSawWhereIsEveryone", "N", 1.0)
        
        --Camera movement.
        fnCutsceneWait(15)
        fnCutsceneBlocker()
        Cutscene_CreateEvent("CameraEvent", "Camera")
            CameraEvent_SetProperty("Max Move Speed", 2.0)
            CameraEvent_SetProperty("Focus Position", (10.25 * gciSizePerTile), (8.50 * gciSizePerTile))
        DL_PopActiveObject()
        fnCutsceneBlocker()
        fnCutsceneWait(85)
        fnCutsceneBlocker()
        Cutscene_CreateEvent("CameraEvent", "Camera")
            CameraEvent_SetProperty("Max Move Speed", 2.0)
            CameraEvent_SetProperty("Focus Actor Name", "Tiffany")
        DL_PopActiveObject()
        fnCutsceneWait(45)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
        fnCutscene([[ Append("55:[E|Neutral] No movement...[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] No security detail, no barricades, no turrets, not even a remote sensing camera...[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] In this situation, Unit 771852 would say something dumb, like...[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] I am unable to construct a stupid enough sentence to do her justice.[P] Note to self, ask her for a list of stupid phrases later.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Proceeding with mission.") ]])
        fnCutsceneBlocker()
    
    --SX-399 joins the party:
    else
    
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSawWhereIsEveryone", "N", 1.0)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N", 1.0)
        
        --Achievement.
        AM_SetPropertyJournal("Unlock Achievement", "HeavySupport")
        
        --Camera movement.
        fnCutsceneWait(15)
        fnCutsceneBlocker()
        Cutscene_CreateEvent("CameraEvent", "Camera")
            CameraEvent_SetProperty("Max Move Speed", 2.0) --Default is 5.0
            CameraEvent_SetProperty("Focus Position", (10.25 * gciSizePerTile), (8.50 * gciSizePerTile))
        DL_PopActiveObject()
        fnCutsceneWait(45)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "SX-399", "Neutral") ]])
        fnCutscene([[ Append("SX-399:[E|Neutral] Gosh darn it, where the hay is everyone?[P] Don't tell me I'm too late...") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --55 walks up.
        fnCutsceneMove("Tiffany", 14.25, 11.50)
        fnCutsceneMove("Tiffany", 11.25, 8.50)
        fnCutsceneFace("Tiffany", -1, 0)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneFace("SX-399", 1, 0)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
        fnCutscene([[ Append("55:[E|Upset] SX-399...[P] why are you here?[P] You are in the process of compromising the most important operation - [P][CLEAR]") ]])
        fnCutscene([[ Append("SX-399:[E|Smirk] Oh there you are.[P] I thought maybe you'd already had some fun without me.[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Neutral] But then I noticed the total absence of scorch marks and plasma scoring, so I figured maybe you hadn't had any fun at all.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Upset] Why.[P] Are you.[P] Here?[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Smirk] Beeeecause...[P] you asked me to be?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Angry] I requested a Steam Droid heavy support detachment, in the event that I needed to destroy enemy vehicles or blockades.[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Flirt] Yep.[P] Reporting for duty.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Angry] Don't tell me you took the assignment![B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Smirk] Well, of course I did![P] I've been keen on it ever since PW-12 started looking for volunteers![B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Smirk] Everyone in my squad said I was perfect![P] I even got my mother to sign off on it![B][C]") ]])
        fnCutscene([[ Append("55:[E|Upset] This is not some social call![P] This is life and death![P] One mistake on this operation will cost hundreds of units![B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Neutral] And you're assuming that I've been spinning my gears since you left Sprocket City?[P] Hell no, sister![B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Smirk] I've spent months blasting creepy crawlies and wrestling with security units![P] I'm the best Sprocket City has to offer![B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Smirk] Yes, mother says I need more experience.[P] I'm not arguing that.[P] But when the chips are down, my shiny new body gives me faster reactions and prime accuracy.[P] I *am* your heavy support.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Upset] I will not allow you to be injured.[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Neutral] Well then.[P] You'd best cover me, hadn't you?[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Flirt] Because I'm here and I ain't leaving your patootie until the job is done.[P] Get me?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] ...[P] Arguing with you is unlikely to produce a favourable outcome.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] It seems I cannot find any companions who listen to reason, can I?[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Neutral] [P][P][P][P][P][EMOTION|SX-399|Flirt]Nope![B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] I will take point.[P] If we encounter heavy resistance and are unable to proceed, we *will* retreat.[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Neutral] Uh, maybe your optical scanners are shot.[P] Haven't you seen that there is no resistance?[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Neutral] I even polished my fission carbine but there's nobody to melt.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] That is indeed a problem.[P] I was expecting at least a security division to be posted here.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] The schematics show the access elevator is in the back room there.[P] They may yet be waiting in ambush.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Stay behind me, and move out, soldier.[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Neutral] You got it, boss bot![P] (Because behind you is right where I want to be...)[B][C]") ]])
        fnCutscene([[ Append("Narrator: (SX-399 has joined the party!)") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --SX-399 joins the party.
        fnCutsceneMove("SX-399", 11.25, 8.50)
        fnCutsceneBlocker()
        fnCutscene([[ AL_SetProperty("Fold Party") ]])
        fnCutsceneBlocker()
            
        --Lua globals.
        giFollowersTotal = 1
        gsaFollowerNames = {"SX-399"}
        giaFollowerIDs = {0}

        --Get her uniqueID. 
        EM_PushEntity("SX-399")
            local iCharacterID = RE_GetID()
        DL_PopActiveObject()

        --Store it and tell her to follow.
        giaFollowerIDs = {iCharacterID}
        AL_SetProperty("Follow Actor ID", iCharacterID)

        --Add SX-399 to the party lineup.
        AdvCombat_SetProperty("Party Slot", 2, "SX-399")

        --Normalize her EXP with the party leader's. She has 50% of the XP that Tiffany does. If she already has more EXP than that,
        -- due to NC+, do nothing.
        local iLeaderXP = 0
        local iLeaderJP = 0
        AdvCombat_SetProperty("Push Party Member", "Tiffany")
            iLeaderXP = AdvCombatEntity_GetProperty("Exp")
            iLeaderJP = AdvCombatEntity_GetProperty("Total JP")
        DL_PopActiveObject()

        --Set. SX-399 starts with no JP.
        AdvCombat_SetProperty("Push Party Member", "SX-399")
            local iCurrentXP = AdvCombatEntity_GetProperty("Exp")
            local fEXPRoll = LM_GetRandomNumber(45, 55) / 100.0
            local iAssignXP = math.floor(iLeaderXP * fEXPRoll)
            if(iCurrentXP < iAssignXP) then
                AdvCombatEntity_SetProperty("Current Exp", iAssignXP)
            end
        DL_PopActiveObject()
    end
end
