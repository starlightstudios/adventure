-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
if(sObjectName == "EntryScene") then
    
    --Repeat check.
    local iPhysicsEntry = VM_GetVar("Root/Variables/Chapter5/Scenes/iPhysicsEntry", "N")
    if(iPhysicsEntry == 1.0) then return end
    
    --Check if SX-399 is present.
    local bIsSX399Present = fnCharacterExists("SX-399")
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iPhysicsEntry", "N", 1.0)
    
    --Black the screen out.
    fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Lock camera.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 5.0) --Default is 5.0
        CameraEvent_SetProperty("Focus Position", (42.75 * gciSizePerTile), (6.50 * gciSizePerTile))
    DL_PopActiveObject()
    
    --Spawn Christine
	TA_Create("Christine")
		local iChristineID = RE_GetID()
		TA_SetProperty("Position", 42, 6)
		TA_SetProperty("Facing", gci_Face_South)
		TA_SetProperty("Clipping Flag", false)
        fnSetCharacterGraphics("Root/Images/Sprites/Christine_GolemDress/", true)
	DL_PopActiveObject()
    
    --Run Christine's costume handler to put her in the right costume.
    LM_ExecuteScript(gsCharacterAutoresolve, "Christine")
    
    --Reposition.
    fnCutsceneTeleport("Christine", 42.75, 6.50)
    fnCutsceneTeleport("Tiffany", -100.25, -100.50)
    if(bIsSX399Present) then
        fnCutsceneTeleport("SX-399", -100.25, -100.50)
    end
    fnCutsceneBlocker()
    
    --Adjust the party to be Christine and nobody else.
	gsPartyLeaderName = "Christine"
    if(bIsSX399Present) then
        AL_SetProperty("Unfollow Actor Name", "SX-399")
    end
    WD_SetProperty("Set Leader Voice", "Christine")
	giFollowersTotal = 0
	gsaFollowerNames = {}
	giaFollowerIDs = {0}
	AL_SetProperty("Player Actor ID", iChristineID)
    
    --Wait a bit.
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Fade screen in.
    fnCutsceneWait(45)
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] (I haven't seen a single security unit the whole way here...)[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Sad] (No alarms, not a lot of cameras to dodge...[P] Where is everyone?)[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] (Doesn't matter, I'm almost at the elevator.[P] This should be a piece of cake...)") ]])
    fnCutsceneBlocker()
    
    --Disable overlays.
    AL_SetProperty("Set Layer Disabled", "OverlayA", true)
    AL_SetProperty("Set Layer Disabled", "OverlayB", true)
    
elseif(sObjectName == "TerminalTrigger") then
    
    --Variables.
    local iSawElevatorScene = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawElevatorScene", "N")
    local iSX399JoinsParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
    if(iSawElevatorScene == 0.0) then
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSawElevatorScene", "N", 1.0)
        
        --Spawn the drone.
        TA_Create("Drone")
            TA_SetProperty("Position", -100, -100)
            TA_SetProperty("Facing", gci_Face_South)
            TA_SetProperty("Clipping Flag", false)
            fnSetCharacterGraphics("Root/Images/Sprites/LatexDrone/", false)
        DL_PopActiveObject()
        
        --Spawn 55 and SX399.
        fnSpecialCharacter("Tiffany", -100, -100, gci_Face_South, false, nil)
        if(iSX399JoinsParty == 1.0) then
            fnSpecialCharacter("SX-399", -100, -100, gci_Face_South, false, nil)
        end
        
        --Movement.
        fnCutsceneMove("Christine", 8.25, 27.50)
        fnCutsceneMove("Christine", 5.25, 27.50)
        fnCutsceneMove("Christine", 5.25, 24.50)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (This is definitely the terminal![P] All right!)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Just need to hook this repeater in and...[P] done!)") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Drone spawns!
        fnCutsceneTeleport("Drone", 14.25, 24.50)
        fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Turn to face.
        fnCutsceneFace("Christine", 1, 0)
        fnCutsceneBlocker()
        fnCutsceneWait(45)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (CRAPTANKEROUS CRAP, NOT RIGHT NOW!)") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneMove("Christine", 6.25, 26.50, 1.50)
        fnCutsceneBlocker()
        fnCutsceneFace("Christine", -1, 0)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Drone:[VOICE|LatexDrone] UNIT 771852.[P] A MESSAGE HAS BEEN RECORDED FOR YOU.[B][C]") ]])
        fnCutscene([[ Append("Drone:[VOICE|LatexDrone] ...[P] UNABLE TO LOCATE UNIT 771852 IN DESIGNATED AREA.[P] THIS UNIT IS LIKELY AT FAULT.[B][C]") ]])
        fnCutscene([[ Append("Drone:[VOICE|LatexDrone] ENGAGE SELF-FLAGELLATION ROUTINES...[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] (Sodbiscuits![P] How did they know I was here?[P] Did I miss a camera?)[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] (Or what if they got 55?[P] Or they got someone else, and made them fess up![P] Damn it, think, think!)") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Enable these layers.
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "OverlayA", false) ]])
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "OverlayB", false) ]])
        
        --Elevator appears.
        fnCutscene([[ AL_SetProperty("Add Dislocation", "DislocationA", "Floor1", 3, 1, 5, 4, 8 * gciSizePerTile, 29 * gciSizePerTile) ]])
        fnCutscene([[ AL_SetProperty("Add Dislocation", "DislocationB", "Floor2", 3, 1, 5, 4, 8 * gciSizePerTile, 29 * gciSizePerTile) ]])
        fnCutsceneBlocker()
        fnCutsceneTeleport("Tiffany", 9.25, 30.50)
        if(iSX399JoinsParty == 1.0) then
            fnCutsceneTeleport("SX-399", 10.25, 30.50)
        end
        fnCutsceneFace("Tiffany", 0, 1)
        if(iSX399JoinsParty == 1.0) then
            fnCutsceneFace("SX-399", 0, 1)
        end
        
        for y = 29.0, 24.0, -0.05 do
            fnCutsceneTeleport("Tiffany", 9.25, y + 1.50)
            if(iSX399JoinsParty == 1.0) then
                fnCutsceneTeleport("SX-399", 10.25, y + 1.50)
            end
            fnCutscene([[ AL_SetProperty("Modify Dislocation", "DislocationA", 8.00 * gciSizePerTile, ]] .. y .. [[ * gciSizePerTile) ]])
            fnCutscene([[ AL_SetProperty("Modify Dislocation", "DislocationB", 8.00 * gciSizePerTile, ]] .. y .. [[ * gciSizePerTile) ]])
            fnCutsceneWait(3)
            fnCutsceneBlocker()
        end
        fnCutscene([[ AudioManager_PlaySound("World|TramStop") ]])
        for y = 24.0, 23.0, -0.05 do
            fnCutsceneTeleport("Tiffany", 9.25, y + 1.50)
            if(iSX399JoinsParty == 1.0) then
                fnCutsceneTeleport("SX-399", 10.25, y + 1.50)
            end
            fnCutscene([[ AL_SetProperty("Modify Dislocation", "DislocationA", 8.00 * gciSizePerTile, ]] .. y .. [[ * gciSizePerTile) ]])
            fnCutscene([[ AL_SetProperty("Modify Dislocation", "DislocationB", 8.00 * gciSizePerTile, ]] .. y .. [[ * gciSizePerTile) ]])
            fnCutsceneWait(7)
            fnCutsceneBlocker()
        end
        fnCutsceneWait(45)
        fnCutsceneBlocker()
        fnCutscene([[ AudioManager_PlaySound("World|TramArrive") ]])
        
        --Movement.
        fnCutsceneMove("Drone", 14.25, 27.50)
        fnCutsceneFace("Drone", 0, -1)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        if(iSX399JoinsParty == 1.0) then
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "SX-399", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        else
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        end
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "LatexDrone", "Neutral") ]])
        fnCutscene([[ Append("Drone:[E|Neutral] ACCEPTABLE TARGETS LOCATED.[P] BEGINNING AUDIO PLAYBACK.[B][C]") ]])
        fnCutscene([[ Append("Drone:[E|Neutral][VOICE|Tiffany] 'Once you've finished wasting time with pointless espionage, report to the basement under the ballroom.'[B][C]") ]])
        fnCutscene([[ Append("Drone:[E|Neutral][VOICE|Tiffany] 'And don't even think of running off.[P] I need your help, right now.[P] Drone, end message.[P] Don't record this part.'[B][C]") ]])
        fnCutscene([[ Append("Drone:[E|Neutral] THANK YOU FOR YOUR COOPERATION, AND HAVE A SPIFFY DAY.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Disable these layers.
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "OverlayA", true) ]])
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "OverlayB", true) ]])
        
        --Drone walks off.
        fnCutsceneMove("Drone", 12.25, 27.50)
        fnCutsceneMove("Drone", 12.25, 30.50)
        fnCutsceneMove("Drone", 23.25, 30.50)
        fnCutsceneBlocker()
        fnCutsceneTeleport("Drone", -104.25, -204.50)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] (...[P] What?)") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Walk out.
        fnCutsceneMove("Christine", 6.25, 27.50)
        fnCutsceneMove("Christine", 8.25, 27.50)
        fnCutsceneMove("Tiffany", 9.25, 27.50)
        fnCutsceneFace("Tiffany", -1, 0)
        if(iSX399JoinsParty == 1.0) then
            fnCutsceneMove("SX-399", 10.25, 27.50)
            fnCutsceneFace("SX-399", -1, 0)
        end
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        if(iSX399JoinsParty == 1.0) then
            fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "SX-399", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Tiffany", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
            fnCutscene([[ Append("Christine:[E|Sad] Ummmm...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Upset] Explain.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] I don't know![P] As soon as I attached the repeater, that drone just popped out of nowhere![B][C]") ]])
            fnCutscene([[ Append("55:[E|Upset] You failed to secure the area.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] Give the girl a break.[P] Besides, it didn't pull the alarm, right?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] There was no security presence in the underground lift, and now we have received a cordial invitation to the location where we were already going.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] There wasn't much security on my way, either.[P] What's going on?[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] So what do we do?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Smirk] Proceed with the plan.[P] If this is an ambush, then we may get that 'action' you were asking for, SX-399.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Whoever sent that message, however, seemed familiar with our plans.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] It sounded a lot like your voice...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Obviously I did not record that message.[P] We must proceed with caution.") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
        else
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
            fnCutscene([[ Append("Christine:[E|Sad] Ummmm...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Upset] Explain.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] I don't know![P] As soon as I attached the repeater, that drone just popped out of nowhere![B][C]") ]])
            fnCutscene([[ Append("55:[E|Upset] You failed to secure the area.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Yet that drone did not pull the alarm or bring a combat escort...[P] Nor were there any security units near the underground lift.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Now, we have received a cordial invitation to the location where we were already going.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] There wasn't much security on my way, either.[P] What's going on?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Smirk] We have no choice but to proceed with the plan, despite the possibility of mission compromise.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Whoever sent that message seemed familiar with our plans.[P] Very familiar.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] It sounded a lot like your voice...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Obviously I did not record that message.[P] We must proceed with caution.") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
        end
        
        --Movement.
        fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
        fnCutsceneMove("Christine", 8.25, 30.50)
        fnCutsceneMove("Christine", 23.25, 30.50)
        fnCutsceneMove("Tiffany", 9.25, 30.50)
        fnCutsceneMove("Tiffany", 23.25, 30.50)
        if(iSX399JoinsParty == 1.0) then
            fnCutsceneMove("SX-399", 10.25, 30.50)
            fnCutsceneMove("SX-399", 23.25, 30.50)
        end
        fnCutsceneBlocker()
        fnCutscene([[ AL_BeginTransitionTo("RegulusArcaneH", "FORCEPOS:14.0x8.0x0") ]])
        
        --Set the party to include 55 and SX399.
        if(iSX399JoinsParty == 1.0) then
            giFollowersTotal = 2
            gsaFollowerNames = {"Tiffany", "SX-399"}
            giaFollowerIDs = {0, 0}

            --Get their uniqueIDs. 
            EM_PushEntity("Tiffany")
                local i55CharacterID = RE_GetID()
            DL_PopActiveObject()
            EM_PushEntity("SX-399")
                local iSXCharacterID = RE_GetID()
            DL_PopActiveObject()

            --Store it and tell her to follow.
            giaFollowerIDs = {i55CharacterID, iSXCharacterID}
            AL_SetProperty("Follow Actor ID", i55CharacterID)
            AL_SetProperty("Follow Actor ID", iSXCharacterID)
            
            --Flags.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 1.0)
            VM_SetVar("Root/Variables/Chapter5/Scenes/iSX399IsFollowing", "N", 1.0)
        
        --Party includes just 55.
        else
            giFollowersTotal = 1
            gsaFollowerNames = {"Tiffany"}
            giaFollowerIDs = {0}

            --Get their uniqueIDs. 
            EM_PushEntity("Tiffany")
                local i55CharacterID = RE_GetID()
            DL_PopActiveObject()

            --Store it and tell her to follow.
            giaFollowerIDs = {i55CharacterID}
            AL_SetProperty("Follow Actor ID", i55CharacterID)
            
            --Flags.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 1.0)
        end
    end
end
