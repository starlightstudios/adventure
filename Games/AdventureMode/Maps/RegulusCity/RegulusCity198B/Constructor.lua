-- |[Constructor]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "RegulusCity198B"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[Standard]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
	AL_SetProperty("Music", "RegulusCity")
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation("RegulusCity198B")
	
	-- |[Spawn NPCS Here]|
	--NPCs set to spawn here will be available for post-exec cutscenes.

-- |[Post-Exec]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	-- |[Spawn NPCs Here]|
	--If an entity needs to spawn after a post-exec cutscene, put them here.
    for i = string.byte("A", 1), string.byte("O", 1), 1 do
        fnStandardNPCByPosition("Golem198" .. string.char(i))
    end
    
    --Spawn Electrosprites if the player has completed the sector.
    local iFinished198 = VM_GetVar("Root/Variables/Chapter5/Scenes/iFinished198", "N")
    if(iFinished198 == 1.0) then
        for i = string.byte("A", 1), string.byte("E", 1), 1 do
            fnStandardNPCByPosition("Electrosprite" .. string.char(i))
        end
    end
    
    --Special entities.
	local iSaw198Intro = VM_GetVar("Root/Variables/Chapter5/Scenes/iSaw198Intro", "N")
    if(iSaw198Intro == 0.0) then
        fnStandardNPCByPosition("Theresa")
        fnStandardNPCByPosition("Lord Unit")
        EM_PushEntity("Theresa")
            TA_SetProperty("Position", 12, 8)
            TA_SetProperty("Facing", gci_Face_East)
        DL_PopActiveObject()
    
    --Saw the intro. Just spawn Theresa.
    else
        fnStandardNPCByPosition("Theresa")
        
    end
    
    --Always spawn Amanda here.
    fnStandardNPCByPosition("Amanda")
    fnCutsceneTeleport("Amanda", 15.75, 32.75)
    
    -- |[Tram Variable Reset]|
    VM_SetVar("Root/Variables/Chapter5/Scenes/iIsTramHere", "N", 0.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/sCurrentTramArea", "S", "Nowhere")

	-- |[Facing Override]|
    fnConstructorFacing()
    
end
