-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
--Opening scene.
if(sObjectName == "InitialTrigger") then
    
    --Variables.
	local iSaw198Intro = VM_GetVar("Root/Variables/Chapter5/Scenes/iSaw198Intro", "N")
    if(iSaw198Intro == 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iSaw198Intro", "N", 1.0)
    
    --Position Christine.
    fnCutsceneMove("Christine", 13.25, 6.50)
    fnCutsceneFace("Christine", 0, 1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Golem", "Pack") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLordB", "Neutral") ]])
    fnCutscene([[ Append("Slave:[E|Pack] ...[P] No, Lord Golem.[P] There hasn't been any improvement.[B][C]") ]])
    fnCutscene([[ Append("Lord:[E|Neutral] Pah, as expected.[P] If I hear you've been slacking, it'll be your processor on the line.[P] Are we clear?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Christine", 13.25, 7.50)
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Golem", "Pack") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLordB", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Excuse me for interrupting, Lord Unit, but is something the matter?[B][C]") ]])
    fnCutscene([[ Append("Lord:[E|Neutral] Hrmph.[P] Just reprimanding a subordinate.[P] Unfortunately I have things to attend to.[P] Unit, see to this Lord -[P] and don't screw it up![B][C]") ]])
    fnCutscene([[ Append("Slave:[E|Pack] Affirmative, Lord Unit.[P] I will not let you down.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneFace("Christine", 1, 1)
    fnCutsceneMove("Lord Unit", 22.75, 8.50)
    fnCutsceneMove("Lord Unit", 22.75, 7.50)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Remove her from the field.
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutsceneTeleport("Lord Unit", -100.25, -100.50)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Christine", 13.25, 8.50)
    fnCutsceneFace("Christine", -1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Pack") ]])
    fnCutscene([[ Append("Christine:[E|Offended] Sheesh, what'd you do that got her cables in a tangle?[B][C]") ]])
    fnCutscene([[ Append("Slave:[E|Pack] ...[P] Lord Unit?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Ah, forgive me.[P] I've not introduced myself.[P] I am Unit 771852, Lord Golem of Maintenance and Repair, Sector 96.[B][C]") ]])
    fnCutscene([[ Append("Slave:[E|Pack] Oh![P] Unit 771852![P] Sophie's friend![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] You know her?[B][C]") ]])
    fnCutscene([[ Append("Slave:[E|Pack] 499323 and I go way back.[P] We used to work on the same survey crew.[P] She mentions you a lot in her text messages.[B][C]") ]])
    fnCutscene([[ Append("Slave:[E|Pack] She says you're -[P] you know...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Uh...[B][C]") ]])
    fnCutscene([[ Append("Slave:[E|Pack] Not the same as other Lords...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Laugh] Oh, I see.[P][E|Smirk] I like to think of myself that way.[B][C]") ]])
    fnCutscene([[ Append("Slave:[E|Pack] She said you're actually productive...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Laugh] Ha ha![P] Indeed![P] I'm not the repair unit she is, but I manage.[B][C]") ]])
    fnCutscene([[ Append("Slave:[E|Pack] My name's Theresa, by the way.[P] Or Unit 516223, if you prefer.[B][C]") ]])
    fnCutscene([[ Append("Theresa:[E|Pack] Now, Christine...[P] May I call you Christine?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] What's the problem?[B][C]") ]])
    fnCutscene([[ Append("Theresa:[E|Pack] If you're here I'm assuming it's because of a work order.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Were you the one that filed it?[B][C]") ]])
    fnCutscene([[ Append("Theresa:[E|Pack] Me?[P] No.[P] It was Unit 745110.[P] She's in the storage bay at the south end of the center.[P] But, I think I know why she filed it.[B][C]") ]])
    fnCutscene([[ Append("Theresa:[E|Pack] So, this is a training facility.[P] We train the abduction teams before they get sent to the surface.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] You do vital work, then.[B][C]") ]])
    fnCutscene([[ Append("Theresa:[E|Pack] ...[P] Thanks.[B][C]") ]])
    fnCutscene([[ Append("Theresa:[E|Pack] Well, I'm sure 745110 can tell you more details, but we're in trouble.[P] Something has been going wrong with our training program lately, and none of us know why.[B][C]") ]])
    fnCutscene([[ Append("Theresa:[E|Pack] See, the program is really old.[P] As in, from the days back when Regulus City first sent out abduction teams.[B][C]") ]])
    fnCutscene([[ Append("Theresa:[E|Pack] It's really archaic, and the coders who made it are long gone.[P] The code got lost at some point, so it's hard to patch, and it's basically held together with duct tape and string at this juncture.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] I'm not sure if it's my help you need, then.[P] I'm a repair unit, not a coder.[B][C]") ]])
    fnCutscene([[ Append("Theresa:[E|Pack] Erm, well, you should still speak to 745110.[P] It's a political logjam, you see.[P] And I think you're well suited to help us with that.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Hmm, I'll see what I can do.[P] Anything for a friend of Sophie's.[B][C]") ]])
    fnCutscene([[ Append("Theresa:[E|Pack] 745110 is near the maintenance elevator at the south end.[P] Tell her I said hi.") ]])
    fnCutsceneBlocker()
    
--Scene with Amanda.
elseif(sObjectName == "AmandaTrigger") then

    --Variables.
    local iMetAmanda = VM_GetVar("Root/Variables/Chapter5/Scenes/iMetAmanda", "N")
    if(iMetAmanda == 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iMetAmanda", "N", 1.0)
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Voice:[VOICE|Amanda] Oh for crying out loud, come back here!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Move Christine to Amanda.
    fnCutsceneMove("Christine", 23.25, 32.70)
    fnCutsceneMove("Christine", 16.75, 32.70)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Amanda", 1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Amanda", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Serious] Attention human.[P] You will be converted.[P] Your cultural and physical uniqueness will be added to our workforce.[P] Do not attempt to resist, 'cause it's futile.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Serious] ...[P] I definitely botched half of that, but I've been wanting to say that for a while.[B][C]") ]])
    fnCutscene([[ Append("Human:[E|Neutral] Err, excuse me?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Serious] Don't be afraid.[P] See?[P] I'm not dangerous.[P] Just...[P] come here...[B][C]") ]])
    fnCutscene([[ Append("Human:[E|Neutral] Lord Golem, can you not see my -[P] oh.[P] Oops.[B][C]") ]])
    fnCutscene([[ Append("Human:[E|Neutral] My security badge is sitting on the terminal over there.[P] Serves me right for taking it off.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Serious] So you're not a rogue human?[P][E|Neutral] Crud.[B][C]") ]])
    fnCutscene([[ Append("Human:[E|Neutral] No.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Blush] But I wanted to convert you...[B][C]") ]])
    fnCutscene([[ Append("Human:[E|Neutral] Have you been spending a lot of time in the abductions training room?[B][C]") ]])
    fnCutscene([[ Append("Human:[E|Neutral] Ahem.[P] Unit 745110 reporting.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Ah![P] You put in the request for assistance.[P] I'm Unit 771852, ready for work.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] ...[P] How did a human get a programmer's assignment?[B][C]") ]])
    fnCutscene([[ Append("745110:[E|Neutral] This is my graduation assignment![P] I was selected for outstanding technical skill from my breeding group.[B][C]") ]])
    fnCutscene([[ Append("745110:[E|Neutral] If the Lord Golem gives me a recommendation, I'll become a Lord Unit when I get converted next month.[P] I've been working my buns off, let me tell you.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] (She'll become a Lord Golem if I help her get a recommendation?[P] Hmm, if she owes me a favour, that could be useful...)[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Well, you're a friend of Theresa, and Theresa is a friend of my tandem unit, Sophie.[P] So, what can I do for you?[B][C]") ]])
    fnCutscene([[ Append("745110:[E|Neutral] Oh, splendid, Christine![P] I mean, Lord Golem...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] I see Theresa told you all about me.[B][C]") ]])
    fnCutscene([[ Append("745110:[E|Neutral] So secondary designations are all right?[P] Mine's Amanda, then.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Okay, Amanda.[P] Theresa said you could give me some details, but she said there was politics involved.[P] What's the issue?[B][C]") ]])
    fnCutscene([[ Append("Amanda:[E|Neutral] How [P]*do*[P] I put this delicately...[B][C]") ]])
    fnCutscene([[ Append("Amanda:[E|Neutral] The Lord Unit in charge of this division is a waste of bolts, and I'm angling for her job if I can get it.[B][C]") ]])
    fnCutscene([[ Append("Amanda:[E|Neutral] She does little work, even for a Lord Unit -[P] no offense.[P] I mean, she doesn't manage personnel or approve requisition forms.[B][C]") ]])
    fnCutscene([[ Append("Amanda:[E|Neutral] There's been some problems as of late with the training program we use.[P] It doesn't usually act up like this, so I think something may be wrong with the servers.[B][C]") ]])
    fnCutscene([[ Append("Amanda:[E|Neutral] But, I -[P] I can't go to the server room to try to fix the issue.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Why not?[B][C]") ]])
    fnCutscene([[ Append("Amanda:[E|Neutral] It'd probably kill me.[P] The server room is located through this elevator, but it's also an airlock.[P] The server room is depressurized and has special quantum locks to handle cooling.[B][C]") ]])
    fnCutscene([[ Append("Amanda:[E|Neutral] Incredibly efficient, but for a fleshbag like me...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Can't you get a vac suit?[B][C]") ]])
    fnCutscene([[ Append("Amanda:[E|Neutral] The Lord Unit has denied my requisition forms, and told me 'If you can't fix it yourself, then you're not Lord Unit material.'[B][C]") ]])
    fnCutscene([[ Append("Amanda:[E|Neutral] She also forbade me from getting a Slave Unit to help, as they outrank me, so I'm not allowed to give them orders.[P] Officially, you're not even here to help me.[P] You're here to fix a pipe leak.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] Hm, is it because your Lord Unit is malicious?[P] Is she trying to stop you for some reason?[B][C]") ]])
    fnCutscene([[ Append("Amanda:[E|Neutral] I think she's just too lazy.[P] She spends most of her time in her quarters and makes us do the administrative work.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Angry] I've half a mind to go call her out for her behavior.[P] Doesn't she know abductions training is vital to the future of the city?[P] How are we going to make new golems without it?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] But...[P] Well, I think it's best if I just fix the issue.[B][C]") ]])
    fnCutscene([[ Append("Amanda:[E|Neutral] Oh, thank you very much![P] I knew we could count on you![P] Not just me, but all the other Units![B][C]") ]])
    fnCutscene([[ Append("Amanda:[E|Neutral] Now, I'll get you permissions to access the training program.[P] You should be able to run it from the server room.[B][C]") ]])
    fnCutscene([[ Append("Amanda:[E|Neutral] Poke around in there and see if you can find the problem.[P] One of the entities in the program runs across the river on the western end and says things it really isn't supposed to.[B][C]") ]])
    fnCutscene([[ Append("Amanda:[E|Neutral] I'll send your PDU a program to help trace the stack.[P] If you can solve the problem, I'll get you some work credits as a reward.[B][C]") ]])
    fnCutscene([[ Append("Amanda:[E|Neutral] ...[P] I happen to have my Lord Unit's credit account number.[P] She won't even notice.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] I think I like you, Amanda.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] All right, I'll take a look at the program from the server room.") ]])
    fnCutsceneBlocker()

-- |[Second Scene with Amanda]|
--Christine, you cad.
elseif(sObjectName == "AmandaTriggerRound2") then

    --Variables.
    local iSecondAmandaScene = VM_GetVar("Root/Variables/Chapter5/Scenes/iSecondAmandaScene", "N")
    if(iSecondAmandaScene == 1.0) then return end
    
    --Check if we should play this scene.
    local iBecameElectrosprite = VM_GetVar("Root/Variables/Chapter5/Scenes/iBecameElectrosprite", "N")
    if(iBecameElectrosprite == 0.0) then return end

    --Flags.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iSecondAmandaScene", "N", 1.0)

    --Christine stands there for several seconds.
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutsceneFace("Christine", 1, 1)
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutsceneFace("Christine", 1, 0)
    fnCutsceneFace("Amanda", -1, 0)
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Amanda", "Neutral") ]])
    fnCutscene([[ Append("Amanda:[E|Neutral] [P].[P].[P].[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] [P].[P].[P].[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Sad] I am such an idiot.[B][C]") ]])
    fnCutscene([[ Append("Amanda:[E|Neutral] Unit 771852?[P] Is that you?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Sad] Uhh, no, I'm...[B][C]") ]])
    fnCutscene([[ Append("Amanda:[E|Neutral] I recognize your chin and your accent.[P] You have a strong chin.[P] It inspires confidence.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Thank you?[B][C]") ]])
    fnCutscene([[ Append("Amanda:[E|Neutral] So are you going to explain what happened?[P] I have things to do, you know.[B][C]") ]])
    fnCutscene([[ Append("Amanda:[E|Neutral] Uh, not to be rude, of course.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Well, you see...[P] I can transform myself using my runestone here...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] And while I was looking into that bug of yours...[B][C]") ]])
    fnCutscene([[ Append("Amanda:[E|Neutral] You somehow transformed yourself into living electricity.[P] Interesting.[B][C]") ]])
    fnCutscene([[ Append("Amanda:[E|Neutral] I spent a lot of time hanging out with the Raijus in the biolabs.[P] I presume you are a construct of pure magic and physical discipline.[B][C]") ]])
    fnCutscene([[ Append("Amanda:[E|Neutral] Likely the metal insulators have conductive interiors to channel the soul's magic field.[P] Your limbs likely represent your self-image based on that magic field.[B][C]") ]])
    fnCutscene([[ Append("Amanda:[E|Neutral] I wonder if you spontaneously generate electricity via magic, like a mage, or if there's some sort of food source required.[B][C]") ]])
    fnCutscene([[ Append("Amanda:[E|Neutral] Are there electrical elementals on Pandemonium?[P] I've heard of wind and fire, but not electrical.[P] Maybe that's what you are?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] (This is certainly a lot better than her freaking out...)[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] I can transform myself back...[B][C]") ]])
    fnCutscene([[ Append("Amanda:[E|Neutral] Also very interesting, and possibly worthy of further investigation, but the real question is unanswered.[B][C]") ]])
    fnCutscene([[ Append("Amanda:[E|Neutral] Did you fix the bug?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Sort of?[B][C]") ]])
    fnCutscene([[ Append("Amanda:[E|Neutral] Phew![B][C]") ]])
    fnCutscene([[ Append("Amanda:[E|Neutral] Once again, not to be impolite about it, but I really want that recommendation.[P] I do not want to spend the rest of my days doing manual labour as a Slave Unit.[B][C]") ]])
    fnCutscene([[ Append("Amanda:[E|Neutral] It'd be a waste of my talents, would it not?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] Look, that's not the issue here.[P] I was transformed into this form by another.[P] She lived in the servers, and there are more like her.[P] She was causing the errors.[B][C]") ]])
    fnCutscene([[ Append("Amanda:[E|Neutral] Hmmm, do you think it has something to do with the quantum locking of the cooling system?[B][C]") ]])
    fnCutscene([[ Append("Amanda:[E|Neutral] Oh, I saw on the server logs that some of the software for this was written by a golem who later went on to work in the AI research labs.[P] Perhaps they were a machine-learning experiment, and it's taken this many years of continuous operation to achieve awareness?[B][C]") ]])
    fnCutscene([[ Append("Amanda:[E|Neutral] I'd say they're worthy of study.[P] Shame that you want to exterminate them.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] Absolutely not![B][C]") ]])
    fnCutscene([[ Append("Amanda:[E|Neutral] But you said they were causing the errors.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Angry] They are living, thinking creatures![P] Out of the question![B][C]") ]])
    fnCutscene([[ Append("Amanda:[E|Neutral] I want that recommendation.[P] I'll do whatever it takes to get it.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] Temper your ambition with kindness.[P] Being a Lord Unit means doing what's best for everyone, not just yourself.[B][C]") ]])
    fnCutscene([[ Append("Amanda:[E|Neutral] Well, that's not how it seems from talking to the other Lord Units...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] I think there is a way to let the Electrosprites live and get you the recommendation, but I'll need your cooperation.[B][C]") ]])
    fnCutscene([[ Append("Amanda:[E|Neutral] I was led to believe that being a leader meant making hard choices, and not being able to satisfy everyone.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] That much is still true, but the hard choices have more to do with befriending your enemies and forgiving those who wrong you.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] And, when they do come up and there is no third way, then the correct choice is the one that puts others ahead of you.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] The leader is the one who should suffer privations first.[P] To make this plan work, we will need to work harder than ever and take a personal risk.[P] If it works, everyone wins.[B][C]") ]])
    fnCutscene([[ Append("Amanda:[E|Neutral] [P].[P].[P].[B][C]") ]])
    fnCutscene([[ Append("Amanda:[E|Neutral] Tell me what to do, Lord Golem.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Just give me your PDU mail address and I'll send you the plan.[P] I need to talk to one other person first.[B][C]") ]])
    fnCutscene([[ Append("Amanda:[E|Neutral] I'll be waiting.[P] And, you should probably not go out there in electrical form.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Uh, yeah, right...") ]])
    fnCutsceneBlocker()
    
    --Transform Christine to Golem.
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Flashwhite, transform.
    Cutscene_CreateEvent("Flash Christine White", "Actor")
        ActorEvent_SetProperty("Subject Name", "Christine")
        ActorEvent_SetProperty("Flashwhite Quickly", "Null")
    DL_PopActiveObject()
    fnCutsceneBlocker()
    fnCutsceneWait(75)
    fnCutsceneBlocker()
    fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua") ]])
    fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Amanda", "Neutral") ]])
    fnCutscene([[ Append("Amanda:[E|Neutral] Impressive, Lord Golem.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] It's not.[P] Really.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Wait for my mail, I'll explain my plan when everything is in place.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] (Okay, better go talk to 55.[P] I'll need her procurement talents to pull this off.)") ]])
    fnCutsceneBlocker()

-- |[Final Trigger]|
--Last cutscene.
elseif(sObjectName == "FinalTrigger") then

    --Variables.
    local iRunFinalScene = VM_GetVar("Root/Variables/Chapter5/Scenes/iRunFinalScene", "N")
    if(iRunFinalScene == 0.0) then return end
    
    --Flags.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iRunFinalScene", "N", 0.0)
    
    --Spawn 55.
    fnSpecialCharacter("Tiffany", -100, -100, gci_Face_South, false, nil)
    
    --Spawn entities.
    TA_Create("FIFO")
        TA_SetProperty("Position", -10, -10)
        TA_SetProperty("Facing", gci_Face_South)
        TA_SetProperty("Clipping Flag", true)
        fnSetCharacterGraphics("Root/Images/Sprites/Electrosprite/", false)
    DL_PopActiveObject()
    TA_Create("FILO")
        TA_SetProperty("Position", -10, -10)
        TA_SetProperty("Facing", gci_Face_South)
        TA_SetProperty("Clipping Flag", true)
        fnSetCharacterGraphics("Root/Images/Sprites/Electrosprite/", false)
    DL_PopActiveObject()
    TA_Create("Bubble")
        TA_SetProperty("Position", -10, -10)
        TA_SetProperty("Facing", gci_Face_South)
        TA_SetProperty("Clipping Flag", true)
        fnSetCharacterGraphics("Root/Images/Sprites/Electrosprite/", false)
    DL_PopActiveObject()
    TA_Create("Quick")
        TA_SetProperty("Position", -10, -10)
        TA_SetProperty("Facing", gci_Face_South)
        TA_SetProperty("Clipping Flag", true)
        fnSetCharacterGraphics("Root/Images/Sprites/Electrosprite/", false)
    DL_PopActiveObject()
    TA_Create("Radix")
        TA_SetProperty("Position", -10, -10)
        TA_SetProperty("Facing", gci_Face_South)
        TA_SetProperty("Clipping Flag", true)
        fnSetCharacterGraphics("Root/Images/Sprites/Electrosprite/", false)
    DL_PopActiveObject()
    TA_Create("Insertion")
        TA_SetProperty("Position", -10, -10)
        TA_SetProperty("Facing", gci_Face_South)
        TA_SetProperty("Clipping Flag", true)
        fnSetCharacterGraphics("Root/Images/Sprites/Electrosprite/", false)
    DL_PopActiveObject()
    
    --Lord Golem
    TA_Create("Lord Unit")
        TA_SetProperty("Position", 23, 23)
        TA_SetProperty("Facing", gci_Face_South)
        TA_SetProperty("Clipping Flag", true)
        fnSetCharacterGraphics("Root/Images/Sprites/GolemLordB/", false)
    DL_PopActiveObject()
    
    --Attack lights to these entities.
    local ciLightPower = 1000
    AL_SetProperty("Register Radial Light", "FIFOLight", 0, 0, ciLightPower)
    AL_SetProperty("Attach Light To Entity", "FIFOLight", "FIFO")
    AL_SetProperty("Register Radial Light", "FILOLight", 0, 0, ciLightPower)
    AL_SetProperty("Attach Light To Entity", "FILOLight", "FILO")
    AL_SetProperty("Register Radial Light", "BubbleLight", 0, 0, ciLightPower)
    AL_SetProperty("Attach Light To Entity", "BubbleLight", "Bubble")
    AL_SetProperty("Register Radial Light", "QuickLight", 0, 0, ciLightPower)
    AL_SetProperty("Attach Light To Entity", "QuickLight", "Quick")
    AL_SetProperty("Register Radial Light", "RadixLight", 0, 0, ciLightPower)
    AL_SetProperty("Attach Light To Entity", "RadixLight", "Radix")
    AL_SetProperty("Register Radial Light", "InsertionLight", 0, 0, ciLightPower)
    AL_SetProperty("Attach Light To Entity", "InsertionLight", "Insertion")
    
    --Black the screen out.
    fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Position all the characters.
    fnCutsceneTeleport("Christine", 13.25, 8.50)
    fnCutsceneFace("Christine", 0, 1)
    fnCutsceneTeleport("Theresa",   13.25, 22.50)
    fnCutsceneTeleport("Amanda",    14.25, 22.50)
    fnCutsceneBlocker()
    
    --Fade back in.
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneMove("Theresa", 13.25, 9.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
    fnCutscene([[ Append("Theresa:[E|Neutral] Everything is in place L-[P] Christine.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Great.[P] Let me check in with everyone else...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|PDU] All right...[P] Psue...[P] Is everyone ready?[B][C]") ]])
    fnCutscene([[ Append("Psue(Call): :D[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|PDU] (Seems she's still doing that...)[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|PDU] 55, is operation White Tiger ready to go?[B][C]") ]])
    fnCutscene([[ Append("55(Call):[VOICE|Tiffany] If you mean me crawling around in the support structure is operation White Tiger, then yes.[P] I have your parts installed.[B][C]") ]])
    fnCutscene([[ Append("55(Call):[VOICE|Tiffany] If you're going to give these things names, you need to tell us what those names are.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|PDU] You don't think White Tiger is a good name?[B][C]") ]])
    fnCutscene([[ Append("55(Call):[VOICE|Tiffany] Yes I do, since it has nothing to do with anything.[P] This prevents an enemy's interception of the code name from having any significance.[B][C]") ]])
    fnCutscene([[ Append("55(Call):[VOICE|Tiffany] The problem is that you - [P][CLEAR]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] (I'll just let her rant for a while.)[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|PDU] Amanda?[B][C]") ]])
    fnCutscene([[ Append("Amanda(Call):[VOICE|Amanda] I've set things up.[P] I'll be right there.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Amanda", 14.25, 8.50)
    fnCutsceneFace("Amanda", -1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Golem", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Amanda", "Neutral") ]])
    fnCutscene([[ Append("Amanda:[E|Neutral] Shall we, Lord Golem?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] We shall.[P] You two stay here, I'll conduct the next session.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Christine", 6.75, 8.50)
    fnCutsceneMove("Christine", 6.75, 9.50)
    fnCutsceneFace("Theresa", -1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Door opens.
    fnCutscene([[ AL_SetProperty("Open Door", "DoorNW") ]])
    fnCutscene([[ AL_SetProperty("Open Door", "DoorA") ]])
    fnCutscene([[ AL_SetProperty("Open Door", "DoorB") ]])
    fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Christine", 6.75, 11.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Golem", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemB", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "GolemC", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Attention, Slave Units.[P] I will be supervising your training session today.[P] Please log in and begin a new session on level 2.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Oh, and there are some new adapters.[P] Please plug them into your lower input ports.[B][C]") ]])
    fnCutscene([[ Append("Golem A:[E|Neutral] Uh, Lord Golem...[P] why are we putting these adapters into our lower input ports?[B][C]") ]])
    fnCutscene([[ Append("Golem A:[E|Neutral] (Am I not allowed to call it a vagina now that I'm a robot?[P] Oh dear...)[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] It should make the experience more...[P] sensual...[P] and it will help your training a great deal.[B][C]") ]])
    fnCutscene([[ Append("Golem C:[E|Neutral] Lord Golem, I'm not cleared for level 2 yet![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] All of the levels should be unlocked already.[P] I had it specially set aside.[B][C]") ]])
    fnCutscene([[ Append("Golem C:[E|Neutral] It still doesn't work![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] Just...[P] log in...[P] have you logged in yet?[B][C]") ]])
    fnCutscene([[ Append("Golem C:[E|Neutral] Oh, nevermind...[P] it just worked...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Cry] (Oh no...)[B][C]") ]])
    fnCutscene([[ Append("Golem B:[E|Neutral] Lord Golem, why are having this session today?[P] Why not tomorrow?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Cry] (I'm having flashbacks to teaching the first-years at Onderdale...)[B][C]") ]])
    fnCutscene([[ Append("Golem A:[E|Neutral] Lord Golem, my power is low.[P] Can I go recharge?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Angry] Why didn't you recharge before you got here?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Angry] Just -[P] just -[P] begin the session![P] Is everyone in session yet?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] ...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] I will take your silence as a yes.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|PDU] (Okay Psue, it's all yours...)[B][C]") ]])
    fnCutscene([[ Append("Golem A:[E|Neutral] ...[P] Lord Golem, something weird is happening...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Wait a bit.
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Sounds.
    fnCutscene([[ AudioManager_PlaySound("World|SparksA") ]])
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Christine:[VOICE|Christine] (Seems those special plugs are doing their jobs...)") ]])
    fnCutsceneBlocker()
    
    --Sounds.
    fnCutscene([[ AudioManager_PlaySound("World|SparksB") ]])
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|SparksC") ]])
    fnCutsceneWait(85)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|SparksA") ]])
    fnCutsceneWait(85)
    fnCutsceneBlocker()
    
    --Wait a bit.
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    
    --Sounds.
    fnCutscene([[ AudioManager_PlaySound("World|SparksA") ]])
    fnCutsceneWait(35)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|SparksB") ]])
    fnCutsceneWait(35)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|SparksC") ]])
    fnCutsceneWait(35)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|SparksB") ]])
    fnCutsceneWait(35)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|SparksA") ]])
    fnCutsceneWait(35)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|SparksB") ]])
    fnCutsceneWait(35)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|SparksA") ]])
    fnCutsceneWait(35)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|SparksD") ]])
    fnCutsceneWait(5)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|SparksA") ]])
    fnCutsceneWait(5)
    fnCutsceneBlocker()
    
    --Power goes out.
    fnCutscene([[ AL_SetProperty("Activate Lights") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Christine:[VOICE|Christine] Remain calm everyone, it is a minor power outage.[P] I have things under control.[B][C]") ]])
    fnCutscene([[ Append("Golem:[VOICE|Golem] Lord Gol-[P] Arrgghh!") ]])
    fnCutsceneBlocker()
    
    --Move FIFO into position.
    fnCutsceneTeleport("FIFO", 9.25, 11.50)
    fnCutsceneBlocker()
    
    --Spin her.
    fnCutsceneFace("FIFO", -1, 0)
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneFace("FIFO", 0, -1)
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneFace("FIFO", 1, 0)
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneFace("FIFO", 0, 1)
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneFace("FIFO", -1, 0)
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneFace("FIFO", 0, -1)
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneFace("FIFO", 1, 0)
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneFace("FIFO", 0, 1)
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneFace("FIFO", -1, 0)
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneFace("FIFO", 0, -1)
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneFace("FIFO", 1, 0)
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneFace("FIFO", 0, 1)
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    
    --Walk her to the left.
    for i = 1, 10, 1 do
        fnCutsceneTeleport("FIFO", 9.25 - (i * 0.10), 11.50)
        fnCutsceneWait(3)
        fnCutsceneBlocker()
    end
    fnCutsceneFace("FIFO", 1, 0)
    fnCutsceneFace("Golem198D", -1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Electrosprite", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
    fnCutscene([[ Append("Sprite:[E|Neutral] Va-va-voom![P] Woah, here I am, in 3D space![P] I guess that means I owe Psue a jumpercola![B][C]") ]])
    fnCutscene([[ Append("Golem:[E|Neutral] My head -[P] my wiring -[P] arrgghh...[B][C]") ]])
    fnCutscene([[ Append("Sprite:[E|Neutral] Hey, don't look so down, buddy![P] Here, how about a kiss![B][C]") ]])
    fnCutscene([[ Append("Golem:[E|Neutral] You're...[P] FIFO...[P] My...[P] friend...?[B][C]") ]])
    fnCutscene([[ Append("Sprite:[E|Neutral] Oh wow, I guess we brainmerged for a bit there, didn't we?[B][C]") ]])
    fnCutscene([[ Append("Golem:[E|Neutral] Kiss me again...[B][C]") ]])
    fnCutscene([[ Append("Sprite:[E|Neutral] Sure thing, hon![B][C]") ]])
    fnCutscene([[ Append("Golem:[E|Neutral] (Oh my, it's like this electrical girl was made with all my kinks in mind...)[B][C]") ]])
    fnCutscene([[ Append("Golem:[E|Neutral] (Is it too good to be true?)[B][C]") ]])
    fnCutscene([[ Append("Sprite:[E|Neutral] Hey, I know what you're thinking![P] Is it because we shared minds?[B][C]") ]])
    fnCutscene([[ Append("Sprite:[E|Neutral] And if I know anything, it's that you're sizzling hot, Unit 699231![P] Wanna date?[B][C]") ]])
    fnCutscene([[ Append("Golem:[E|Neutral] Oh my gosh, yes.[P] Yes![B][C]") ]])
    fnCutscene([[ Append("Sprite:[E|Neutral] I'll be your digital waifu, and dress up in any costume you want![B][C]") ]])
    fnCutscene([[ Append("Sprite:[E|Neutral] And then we'll screw while I call you Bear-Pawed-Babe![P] Every day![B][C]") ]])
    fnCutscene([[ Append("Golem:[E|Neutral] All my fetishes![P] All of them![B][C]") ]])
    fnCutscene([[ Append("Sprite:[E|Neutral] *smooch*[P] Being 3D is great![B][C]") ]])
    fnCutscene([[ Append("Golem:[E|Neutral] *smooch*[P] Yes, it sure is!") ]])
    fnCutsceneBlocker()
    
    --Teleport in the other electrosprites.
    fnCutsceneTeleport("FILO", 4.25, 11.50)
    fnCutsceneTeleport("Bubble", 9.25, 13.50)
    fnCutsceneTeleport("Quick", 4.25, 13.50)
    fnCutsceneTeleport("Radix", 9.25, 15.50)
    fnCutsceneTeleport("Insertion", 4.25, 15.50)
    fnCutsceneWait(1)
    fnCutsceneBlocker()
    for i = 1, 10, 1 do
        fnCutsceneTeleport("FILO", 4.25 + (i * 0.10), 11.50)
        fnCutsceneTeleport("Bubble", 9.25 - (i * 0.10), 13.50)
        fnCutsceneTeleport("Quick", 4.25 + (i * 0.10), 13.50)
        fnCutsceneTeleport("Radix", 9.25 - (i * 0.10), 15.50)
        fnCutsceneTeleport("Insertion", 4.25 + (i * 0.10), 15.50)
        fnCutsceneWait(3)
        fnCutsceneBlocker()
    end
    fnCutsceneFace("FILO", -1, 0)
    fnCutsceneFace("Golem198A", 1, 0)
    fnCutsceneFace("Bubble", 1, 0)
    fnCutsceneFace("Golem198B", 1, 0)
    fnCutsceneFace("Quick", -1, 0)
    fnCutsceneFace("Golem198C", 1, 0)
    fnCutsceneFace("Radix", 1, 0)
    fnCutsceneFace("Golem198E", -1, 0)
    fnCutsceneFace("Insertion", -1, 0)
    fnCutsceneFace("Golem198F", -1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Power comes back on.
    fnCutscene([[ AL_SetProperty("Deactivate Lights") ]])
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Activate Lights") ]])
    fnCutsceneWait(35)
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Deactivate Lights") ]])
    fnCutsceneWait(35)
    fnCutsceneBlocker()

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|PDU] 55?[B][C]") ]])
    fnCutscene([[ Append("55(Call):[VOICE|Tiffany] I have the power back online.[P] I believe I can prevent the power outage on subsequent runs with a few extra surge protectors.[B][C]") ]])
    fnCutscene([[ Append("55(Call):[VOICE|Tiffany] Was the mission a success?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] ...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Happy] They're all making out![P] It worked![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] And -[P] Oh dear, it looks like all the cameras were fried in the power surge![P] What a shame![B][C]") ]])
    fnCutscene([[ Append("55(Call):[VOICE|Tiffany] You told me to attach them to the main circuit...[P] Why are you feigning surprise?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Live a little, 55.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Okay, come topside.[P] Let's get everyone debriefed.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Black out the screen.
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Reposition everyone.
    fnCutsceneTeleport("Christine", 14.25, 33.50)
    fnCutsceneTeleport("Tiffany", 14.25, 34.50)
    fnCutsceneTeleport("Theresa", 16.25, 33.50)
    fnCutsceneTeleport("Amanda", 16.25, 34.50)
    fnCutsceneFace("Christine", 1, 0)
    fnCutsceneFace("Tiffany", 1, 0)
    fnCutsceneFace("Theresa", -1, 0)
    fnCutsceneFace("Amanda", -1, 0)
    fnCutsceneBlocker()
    
    --Fade in.
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneWait(45)
    fnCutsceneBlocker()

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Amanda", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Golem", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] ...[P] And that's about the long and short of it.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] I think we can expect the golems to be discreet about this.[P] After all, they'll want to protect their new digital tandem units.[B][C]") ]])
    fnCutscene([[ Append("Amanda:[E|Neutral] Brilliant work, Lord Golem.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Never underestimate the power of love.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] I've personally checked every unit here.[P] Even the Lord Unit running Training Group 2 has sworn allegiance to these new Electrosprites.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] May I ask what you did to them?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] The adaptor I installed causes incredible physical stimulation...[P] you know...[P] down there...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] The result is that the Electrosprites and trainees experience an intense intimacy at the exact moment they share minds.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] The Electrosprites come into the real world, and the golems...[B][C]") ]])
    fnCutscene([[ Append("Theresa:[E|Neutral] Just plain come.[B][C]") ]])
    fnCutscene([[ Append("Amanda:[E|Neutral] That's one way of putting it.[B][C]") ]])
    fnCutscene([[ Append("Amanda:[E|Neutral] This should also result in a massive increase in training efficiency.[P] Already the golems are asking for extra sessions to visit their tandem units.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] And you're sure that you can keep this hidden from Central Administration?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] I hope so.[P] The Electrosprites are hackers with no equal.[P] I think they might even be better than you, 55![B][C]") ]])
    fnCutscene([[ Append("55:[E|Upset] ...[P] Possible...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] And...[P] I believe we'll have the Lord Golem here on our side...[B][C]") ]])
    fnCutscene([[ Append("Amanda:[E|Neutral] Uh -[P] oh![P] I forgot about Unit 107822!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Lord Unit", 23.25, 32.50)
    fnCutsceneMove("Lord Unit", 19.25, 32.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Amanda", 1, 0)
    fnCutsceneFace("Theresa", 1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Amanda", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLordB", "Neutral") ]])
    fnCutscene([[ Append("Lord:[E|Neutral] Unit 745110, I should have expected you to be hiding down here.[B][C]") ]])
    fnCutscene([[ Append("Lord:[E|Neutral] My PDU informs me there was a major power outage earlier![P] Is this your doing?[P] Report![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Excuse me, Lord Unit 107822.[B][C]") ]])
    fnCutscene([[ Append("Lord:[E|Neutral] Hrmph.[P] You again.[P] What are you doing here?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] The power outage was my fault.[P] I accidentally tripped a breaker while I was fixing an oxygen leak.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Unit 745110 was just scolding me in front of my superior officer.[B][C]") ]])
    fnCutscene([[ Append("Lord:[E|Neutral] I'm sorry, Command Unit.[P] I did not see you there.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Forget we were here.[P] Our business does not concern you.[B][C]") ]])
    fnCutscene([[ Append("Lord:[E|Neutral] Very well.[B][C]") ]])
    fnCutscene([[ Append("Lord:[E|Neutral] Unit 745110, I apologize for jumping to conclusions.[B][C]") ]])
    fnCutscene([[ Append("Amanda:[E|Neutral] It's no problem, Lord Unit.[B][C]") ]])
    fnCutscene([[ Append("Amanda:[E|Neutral] I am happy to report, though, that we have solved the problem in the training program.[P] Efficiency is up past expectations, and several units are now requesting extra training sessions.[B][C]") ]])
    fnCutscene([[ Append("Lord:[E|Neutral] Is that so?[B][C]") ]])
    fnCutscene([[ Append("Lord:[E|Neutral] ...[P] Thank goodness.[P] That means you really are cut out for this.[B][C]") ]])
    fnCutscene([[ Append("Lord:[E|Neutral] Unit 745110, may I call you by your secondary designation?[B][C]") ]])
    fnCutscene([[ Append("Amanda:[E|Neutral] Certainly.[B][C]") ]])
    fnCutscene([[ Append("Lord:[E|Neutral] Amanda, I apologize for my behavior.[P] You see...[B][C]") ]])
    fnCutscene([[ Append("Lord:[E|Neutral] I find computers to be the height of banality.[P] This job bores me.[P] I merely took it because no other Lord Unit would.[B][C]") ]])
    fnCutscene([[ Append("Lord:[E|Neutral] I am glad you will serve well as my successor.[P] I will put in your recommendation when I return to my quarters.[B][C]") ]])
    fnCutscene([[ Append("Amanda:[E|Neutral] L-[P]L-[P]Lord Unit?[P] Really?[B][C]") ]])
    fnCutscene([[ Append("Lord:[E|Neutral] I will be returning to the arcane university later today.[P] Your promotion is in effect as of this moment.[B][C]") ]])
    fnCutscene([[ Append("Lord:[E|Neutral] Congratulations, Unit 745110.[P] With your permission, I would like to attend your conversion ceremony next month.[B][C]") ]])
    fnCutscene([[ Append("Amanda:[E|Neutral] Certainly![P] Thank you![B][C]") ]])
    fnCutscene([[ Append("Lord:[E|Neutral] Very good.[P] It was a pleasure working with you.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Lord Unit", 23.25, 32.50)
    fnCutsceneMove("Lord Unit", 23.25, 24.50)
    fnCutsceneBlocker()
    fnCutsceneTeleport("Lord Unit", -100.25, -100.50)
    fnCutsceneBlocker()

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Amanda", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Golem", "Neutral") ]])
    fnCutscene([[ Append("Amanda:[E|Neutral] I'M GONNA BE A LORD UNIT YIPPEEEEEE![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Happy] Congratulations![B][C]") ]])
    fnCutscene([[ Append("Theresa:[E|Neutral] Great work, Amanda.[P] And to you, Christine and 2855.[B][C]") ]])
    fnCutscene([[ Append("Amanda:[E|Neutral] Christine, if you need our help in your revolution -[P] you have it.[B][C]") ]])
    fnCutscene([[ Append("Amanda:[E|Neutral] You were right.[P] About everything.[P] The Electrosprites will surely help you, as will all the trainees.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] And the Electrosprites still in the training program?[B][C]") ]])
    fnCutscene([[ Append("Theresa:[E|Neutral] We'll manifest them through the next set of trainees that come in.[B][C]") ]])
    fnCutscene([[ Append("Amanda:[E|Neutral] Yeah![P] I'm sure the trainees will side with us once they meet their true loves![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Happy] Capital![P] Simply capital![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Happy] Well, 55, we've still got work to do.[P] Shall we?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Smirk] If you insist.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Topics
    WD_SetProperty("Unlock Topic", "Electrosprites", 1)
    
    --Warp to the next area.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iRunPostScene", "N", 1.0)
    fnCutscene([[ AL_BeginTransitionTo("RegulusCity198A", "FORCEPOS:19.0x11.0x0") ]])
    fnCutsceneBlocker()

--Transformation trigger.
elseif(sObjectName == "TransformTrigger") then

	--Variables.
	local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
    local iFinished198   = VM_GetVar("Root/Variables/Chapter5/Scenes/iFinished198", "N")
	
	--Christine needs to change form.
	if(sChristineForm ~= "Golem") then
	
        --Hasn't finished 198:
        if(iFinished198 == 0.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("[VOICE|Christine](Better transform to a Lord Unit.[P] Wouldn't want to draw unnecessary attention.)") ]])
            fnCutsceneBlocker()
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("[VOICE|Christine](Better transform to a Lord Unit.[P] You never know who might be visiting the sector.)") ]])
            fnCutsceneBlocker()
        end
    
		--Flashwhite.
		Cutscene_CreateEvent("Flash Christine White", "Actor")
			ActorEvent_SetProperty("Subject Name", "Christine")
			ActorEvent_SetProperty("Flashwhite Quickly", "Null")
		DL_PopActiveObject()
		fnCutsceneBlocker()

		fnCutsceneWait(75)
		fnCutsceneBlocker()
		fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua") ]])
		fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
		fnCutsceneBlocker()
	end
    


end
