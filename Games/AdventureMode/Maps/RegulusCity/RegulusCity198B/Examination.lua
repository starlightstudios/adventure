-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Execution]|
--Exit
if(sObjectName == "ToCity198A") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusCity198A", "FORCEPOS:19.0x12.0x0")
	
--Exit
elseif(sObjectName == "ToCity198C") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusCity198C", "FORCEPOS:9.5x4.0x0")
	giForceFacing = gci_Face_South

-- |[Other Examinables]|
elseif(sObjectName == "ComputerA") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Leader](A directory of all the units in residence in this sector.[P] Only a handful of golems are assigned here permanently, the rest are temporary accommodations for training.)") ]])
	fnCutsceneBlocker()
    
elseif(sObjectName == "ComputerB") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Leader](A training console not in use.[P] This unit seemed to be struggling with implanting golem cores.[P] The logs indicate she kept trying to apply them to the legs...)") ]])
	fnCutsceneBlocker()
    
elseif(sObjectName == "ComputerC") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Leader](A training console not in use.[P] This unit did a great job converting targets, but wasn't very choosy.[P] It's important to pick high-quality human specimens or you wind up with a lot of lazy units.)") ]])
	fnCutsceneBlocker()
    
elseif(sObjectName == "ComputerD") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Leader](A training console not in use.[P] This unit excelled in the combat part of the exercise.[P] She typed furiously!)") ]])
	fnCutsceneBlocker()
    
elseif(sObjectName == "ComputerE") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Leader](A training console not in use.[P] There's no saved data on this one.)") ]])
	fnCutsceneBlocker()
    
elseif(sObjectName == "ComputerF") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Leader](A training console not in use.[P] The Lord Golem in charge of this room was using this console.[P] Her score was exemplary, as expected.)") ]])
	fnCutsceneBlocker()
    
elseif(sObjectName == "ComputerG") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Leader](A training console not in use.[P] This unit was multi-tasking, and playing three training simulations at once.[P] Nominally efficient, but her score was abysmal on all three...)") ]])
	fnCutsceneBlocker()
    
elseif(sObjectName == "ComputerH") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Leader](A training console not in use.[P] This unit didn't seem to understand the text prompt and was typing up a screed about poor-quality oil ingredients.)") ]])
	fnCutsceneBlocker()
    
elseif(sObjectName == "ComputerI") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Leader](A training console not in use.[P] The last unit did a great job and even got a commendation. Good work!)") ]])
	fnCutsceneBlocker()
    
elseif(sObjectName == "ComputerJ") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Leader](A training console not in use.[P] Hmm, this unit really enjoyed watching humans get converted.[P] I suppose I would too, it's quite exciting.)") ]])
	fnCutsceneBlocker()
    
elseif(sObjectName == "ComputerK") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Leader](This is Unit 107822's console.[P] She...[P] doesn't seem to use it very often...)") ]])
	fnCutsceneBlocker()
    
elseif(sObjectName == "ComputerL") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Leader](This console is networked specially to spy on the other consoles.[P] The logs indicate the Lord Unit would sometimes spawn extra enemies in a golem's simulation...)") ]])
	fnCutsceneBlocker()
    
elseif(sObjectName == "ComputerM") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Leader](This console is networked specially to spy on the other consoles.[P] There are logs of the scores and evaluations of previous graduates.[P] Nothing really stands out.)") ]])
	fnCutsceneBlocker()
    
elseif(sObjectName == "ComputerN") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Leader](Amanda's console.[P] It has a large number of requests and complaints, but she's been stuck on the strange entity behavior for a while now...)") ]])
	fnCutsceneBlocker()
    
elseif(sObjectName == "ComputerO") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Leader](A training console not in use.[P] This unit somehow acquired a beam sword in the simulation...[P] but there's no evidence of cheating.[P] Hmm...)") ]])
	fnCutsceneBlocker()
    
elseif(sObjectName == "Adaptors") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Leader](These shelves have a wide range of adaptors for converting inputs between socket types.[P] They're fairly old and worn out.)") ]])
	fnCutsceneBlocker()
    
elseif(sObjectName == "Converters") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Leader](These shelves have burnt out power converters for frequencies that aren't used anymore.)") ]])
	fnCutsceneBlocker()

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
