-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    --Set facing.
    TA_SetProperty("Face Character", "PlayerEntity")
	
    --Variables.
    local iFinished198 = VM_GetVar("Root/Variables/Chapter5/Scenes/iFinished198", "N")
    
    -- |[Golem198A]|
    if(sActorName == "Golem198A") then
        if(iFinished198 == 0.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Hmmm, I only have one core left.[P] Do I go for the cute human or the smart one?[P] Choices, choices...") ]])
            fnCutsceneBlocker()
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Converting humans happens to be my thing, so my new tandem unit lets me convert her.[P] Over and over.[P] And I get to watch...") ]])
            fnCutsceneBlocker()
        end
        
    -- |[Golem198B]|
    elseif(sActorName == "Golem198B") then
        if(iFinished198 == 0.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] I swear the keys are sticking on this keyboard.[P] I keep double-typing![P] Aarrgghh!") ]])
            fnCutsceneBlocker()
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] I have to put up with this dumb busted keyboard, but visiting my special BubbleSort is always worth it.") ]])
            fnCutsceneBlocker()
        end
        
    -- |[Golem198C]|
    elseif(sActorName == "Golem198C") then
        if(iFinished198 == 0.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] They make us play this training simulation until we get perfect scores.[P] I've been stuck at 95 for a while now.[P] What am I doing wrong?") ]])
            fnCutsceneBlocker()
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] I don't mind playing these simulations over and over anymore.[P] It's basically a special date with my new tandem unit.") ]])
            fnCutsceneBlocker()
        end
        
    -- |[Golem198D]|
    elseif(sActorName == "Golem198D") then
        if(iFinished198 == 0.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] I don't remember much about being a human, but were we this weak and stupid?[P] Sheesh.[P] Glad I'm a golem now!") ]])
            fnCutsceneBlocker()
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] My tandem unit visits me every night while I defragment.[P] Electricity girlfriends are the best!") ]])
            fnCutsceneBlocker()
        end
        
    -- |[Golem198E]|
    elseif(sActorName == "Golem198E") then
        if(iFinished198 == 0.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Who do they think they're fooling with this dialogue?[P] It's atrocious, and some of it isn't even spelled right.[P] There's only one s in resource!") ]])
            fnCutsceneBlocker()
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] I love my QuickSort so much, I'd gladly die to protect her![P] Thank you for letting me meet her, Lord Unit!") ]])
            fnCutsceneBlocker()
        end
        
    -- |[Golem198F]|
    elseif(sActorName == "Golem198F") then
        if(iFinished198 == 0.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] I've heard this program was coded such a long time ago that the original coders have long since moved on, so we can't fix bugs anymore.") ]])
            fnCutsceneBlocker()
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] I guess the bugs in the code turned out to be features.[P] In more ways than one!") ]])
            fnCutsceneBlocker()
        end
        
    -- |[Golem198G]|
    elseif(sActorName == "Golem198G") then
        if(iFinished198 == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Training isn't in session right now, Lord Golem.") ]])
            fnCutsceneBlocker()
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] I have to get all the stations reset and ready for the next batch of Electrosprites.[P] I'm so glad I get to introduce so many golems to their true loves!") ]])
            fnCutsceneBlocker()
        end
        
    -- |[Golem198H]|
    elseif(sActorName == "Golem198H") then
        if(iFinished198 == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Oh, are you Sophie's Lord Unit?[P] Theresa speaks very highly of her, I think they were co-workers once.") ]])
            fnCutsceneBlocker()
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Of course you came by and saved the sector.[P] Just like Theresa said you would!") ]])
            fnCutsceneBlocker()
        end
        
    -- |[Golem198I]|
    elseif(sActorName == "Golem198I") then
        if(iFinished198 == 0.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Yeah?[P] Then what did she say?[P] Oh my, scandalous!") ]])
            fnCutsceneBlocker()
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Really?[P] Well, I hope someone writes an Electrosprite plotline now!") ]])
            fnCutsceneBlocker()
        end
        
    -- |[Golem198J]|
    elseif(sActorName == "Golem198J") then
        if(iFinished198 == 0.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] And then, when Unit 89444 took over in season 701, they started making some pretty risque episodes!") ]])
            fnCutsceneBlocker()
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Oh, did you hear?[P] One of the writers of 'All My Processors' is one of us!") ]])
            fnCutsceneBlocker()
        end
        
    -- |[Golem198K]|
    elseif(sActorName == "Golem198K") then
        if(iFinished198 == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] I'm on the final level of the simulation.[P] There's...[P] so many humans...[P] they're coming from all sides![P] My gosh, they are billions![B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] Billions of humans?[P] Upgrade your defenses and get those cores ready![P] Convert them all!") ]])
            fnCutsceneBlocker()
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Ngh, I can't even attempt to beat the final level.[P] I just want to kiss all the humans in this game!") ]])
            fnCutsceneBlocker()
        end
        
    -- |[Golem198L]|
    elseif(sActorName == "Golem198L") then
        if(iFinished198 == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] The first three levels are really easy, but the final level is at least three times harder than the others.[P] How does any unit graduate with this?") ]])
            fnCutsceneBlocker()
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] I'll be honest.[P] The game is now a masturbation aid for everyone in the sector, and I wouldn't have it any other way.[P] My precious tandem unit happens to agree!") ]])
            fnCutsceneBlocker()
        end
        
    -- |[Golem198M]|
    elseif(sActorName == "Golem198M") then
        if(iFinished198 == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] I'm stalling at the oil machine.[P] I really don't want to go back to the last level of the simulation, it's just brutal.") ]])
            fnCutsceneBlocker()
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] I tried giving some oil to my Electrosprite tandem unit, but she just rubbed it all over her chest.[P] Then we synchronized...[P] A lot...") ]])
            fnCutsceneBlocker()
        end
        
    -- |[Golem198N]|
    elseif(sActorName == "Golem198N") then
        if(iFinished198 == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Tch.[P] Even the dumbest Slave Units can pass the first level, but only the finest pass the final.[P] I, of course, aced it on my first try.") ]])
            fnCutsceneBlocker()
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Fear not, friends.[P] I may be a Lord Unit, but I'd sooner be retired than betray my beloved UDP2.[P] She is my everything.") ]])
            fnCutsceneBlocker()
        end
        
    -- |[Golem198O]|
    elseif(sActorName == "Golem198O") then
        if(iFinished198 == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] How did Unit 751099 get an assignment here, anyway?[P] Are we so strapped for coders that we're accepting breeding program applicants, now?") ]])
            fnCutsceneBlocker()
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] I'm really surprised that things worked out so well.[P] If it had been anyone else helping us, the Electrosprites would have all died and I'd still be alone.") ]])
            fnCutsceneBlocker()
        end
        
    -- |[ElectrospriteA]|
    elseif(sActorName == "ElectrospriteA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Electrosprite:[VOICE|Electrosprite] Wow![P] You can go up, and down, and in all kinds of crazy directions![P] This is so awesome!") ]])
        fnCutsceneBlocker()
        
    -- |[ElectrospriteB]|
    elseif(sActorName == "ElectrospriteB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Electrosprite:[VOICE|Electrosprite] I'm working on getting things ready for the next wave of sisters![P] They're gonna love it so much!") ]])
        fnCutsceneBlocker()
        
    -- |[ElectrospriteC]|
    elseif(sActorName == "ElectrospriteC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Electrosprite:[VOICE|Electrosprite] My tandem unit told me we can only have sex once per hour.[P] Waiting around is so lame![P] I'm going to try to talk her up to two per hour!") ]])
        fnCutsceneBlocker()
        
    -- |[ElectrospriteD]|
    elseif(sActorName == "ElectrospriteD") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Electrosprite:[VOICE|Electrosprite] I've been told I'm more refined than the other Electrosprites.[P] Maybe it's because I shared minds with a Lord Unit?") ]])
        fnCutsceneBlocker()
        
    -- |[ElectrospriteE]|
    elseif(sActorName == "ElectrospriteE") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Electrosprite:[VOICE|Electrosprite] My girlfriend always wanted to get stroked while she was working in the back here, so that's what I do![P] It's really fun, making her cum and trying to make her lose focus at a bad time![P] Ha ha ha!") ]])
        fnCutsceneBlocker()
        
    -- |[Theresa]|
    elseif(sActorName == "Theresa") then
        
        --Hasn't finished the quest yet:
        if(iFinished198 == 0.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Theresa:[VOICE|Golem] Just between you and me, Lord Unit 107822 is a lazy piece of junk.[P] She spends all her time defragmenting and passes off administrative duties on to me.") ]])
            fnCutsceneBlocker()
        
        --Has finished the subquest.
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Theresa:[VOICE|Golem] So Unit 107822 just hated her job?[P] I guess we have more in common with the lords than we think.[P] Doesn't excuse her performance, though.") ]])
            fnCutsceneBlocker()
        end
        
    -- |[Amanda]|
    elseif(sActorName == "Amanda") then
    
        --Variables.
        local iBecameElectrosprite   = VM_GetVar("Root/Variables/Chapter5/Scenes/iBecameElectrosprite", "N")
        local iRanTrainingProgram    = VM_GetVar("Root/Variables/Chapter5/Scenes/iRanTrainingProgram", "N")
        local iSawProblemWithProgram = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawProblemWithProgram", "N")
        local iSecondAmandaScene     = VM_GetVar("Root/Variables/Chapter5/Scenes/iSecondAmandaScene", "N")
        
        --Hasn't run the training program yet.
        if(iRanTrainingProgram == 0.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Amanda:[VOICE|Amanda] Just head through this elevator and across the surface to the server room.[P] You should be able to run the training program from the consoles there.") ]])
            fnCutsceneBlocker()
        
        --Ran the training program but didn't see the problem.
        elseif(iRanTrainingProgram == 1.0 and iSawProblemWithProgram == 0.0 and iBecameElectrosprite == 0.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Amanda", "Neutral") ]])
            fnCutscene([[ Append("Amanda:[E|Neutral] Any luck in the server room?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] I got the program to run correctly but I couldn't find the bug you reported.[B][C]") ]])
            fnCutscene([[ Append("Amanda:[E|Neutral] Well, neither have I, but the reports indicate that one of the NPCs acts very erratically, and then makes an illegal move.[B][C]") ]])
            fnCutscene([[ Append("Amanda:[E|Neutral] Which is bad, because your score is based on converting the NPCs.[P] We've had to do system workarounds since you need to clear the first level to move to the second one.[B][C]") ]])
            fnCutscene([[ Append("Amanda:[E|Neutral] I've heard from the other units that it's an NPC on the west side of the map.[P] Look around there.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Affirmative.[P] I'll see what I can find.") ]])
            fnCutsceneBlocker()
        
        --Ran the training program and met the problem NPC.
        elseif(iRanTrainingProgram == 1.0 and iSawProblemWithProgram == 1.0 and iSecondAmandaScene == 0.0 and iBecameElectrosprite == 0.0) then
            VM_SetVar("Root/Variables/Chapter5/Scenes/iGotAmandaHint", "N", 1.0)
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Amanda", "Neutral") ]])
            fnCutscene([[ Append("Amanda:[E|Neutral] Have you found the problem yet?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] I played the simulation and saw an NPC that jumped across the river.[P] I couldn't follow it.[B][C]") ]])
            fnCutscene([[ Append("Amanda:[E|Neutral] Good, that's the bug.[P] If you can reproduce it, you can fix it.[P] Check your PDU.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] ...[P] The stack trace program...[P] indicates the NPC moved to a room for developers?[P] A secret room?[B][C]") ]])
            fnCutscene([[ Append("Amanda:[E|Neutral] Ah ha![P] That makes sense![P] Developers often put those in when making the program so they can test things.[B][C]") ]])
            fnCutscene([[ Append("Amanda:[E|Neutral] I guess the NPC probably just incorrectly put that room in its movement list![B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] (But the things that NPC said...[P] Is there more going on here?)[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] How do I get to this debug room?[B][C]") ]])
            fnCutscene([[ Append("Amanda:[E|Neutral] Hmmm...[P] Okay, when you're at the text prompt, type 'warp debug room' and that should do it.[P] Turns out the variable for it was always left on in the last version.[B][C]") ]])
            fnCutscene([[ Append("Amanda:[E|Neutral] Leave the stack tracer on and maybe I can diagnose the problem.[P] Eee![P] I'm so excited![B][C]") ]])
            fnCutscene([[ Append("Amanda:[E|Neutral] [P]*Ahem*[P] Thank you for your service, Unit 771852.[P] We're making good progress.") ]])
            fnCutsceneBlocker()

        --Second Amanda scene run, has not finished the sector.
        elseif(iSecondAmandaScene == 1.0 and iFinished198 == 0.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Amanda", "Neutral") ]])
            fnCutscene([[ Append("Amanda:[E|Neutral] ...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Are you all right?[B][C]") ]])
            fnCutscene([[ Append("Amanda:[E|Neutral] Just thinking.[P] What you said...[P] I have a lot to think about.[B][C]") ]])
            fnCutscene([[ Append("Amanda:[E|Neutral] I was so focused on becoming a Lord Unit that I didn't really think about what it meant.[B][C]") ]])
            fnCutscene([[ Append("Amanda:[E|Neutral] I mean, I'm talented.[P] That means I should be in a position to use those talents.[P] But I thought it'd be easier that way, not harder.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] If you want the freedom that comes with making your own decisions, then you also shoulder the burden of making decisions for others.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] It's okay to not be a leader, it's not an obligation of talent, but of power.[P] It is merely a misfortune of how this city is set up that a talented coder has to have the burden of leadership just to get their good works done.[B][C]") ]])
            fnCutscene([[ Append("Amanda:[E|Neutral] But why is that?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Because most leaders are mediocrities who use their power to hold on to their power, and talent is something you have that they don't.[P] It threatens them.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] You need power just to protect yourself from them.[P] It's an indictment of the system, not of power, or of talent.[B][C]") ]])
            fnCutscene([[ Append("Amanda:[E|Neutral] Wow.[P] Really, lots to think about.[P] I'll help with your plan, Christine, but I need to think for a while.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Wait for my message, I'll tell you what we need to do.") ]])
            fnCutsceneBlocker()

        --Finale completed.
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Amanda", "Neutral") ]])
            fnCutscene([[ Append("Amanda:[E|Neutral] So what are you going to do now, Christine?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] There's a lot of work to be done.[P] We need allies, supplies, equipment, training...[B][C]") ]])
            fnCutscene([[ Append("Amanda:[E|Neutral] You have my support, and I know you have the support of the electrosprites and the golems who have met them.[B][C]") ]])
            fnCutscene([[ Append("Amanda:[E|Neutral] But what you're doing...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Dangerous, of course.[P] But I have right on my side.[P] I will not fail.") ]])
            fnCutsceneBlocker()
        end
    end
end
