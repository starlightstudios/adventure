-- |[Post-Exec]|
--This is called after the game exits Text Adventure mode.
gsPostExec = nil

--Change any globals that got modified by the text adventure handlers.
gsDatafilesPath = gsRoot .. "Datafiles/"

--Black out the screen.
fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()
    
--Fade back in.
fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
fnCutsceneWait(15)
fnCutsceneBlocker()

--Dialogue execution is based on the variable states.
local iSawProblemWithProgram = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawProblemWithProgram", "N")
local iTalkedToElectrosprite = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToElectrosprite", "N")
local iBecameElectrosprite   = VM_GetVar("Root/Variables/Chapter5/Scenes/iBecameElectrosprite", "N")
local iFinished198           = VM_GetVar("Root/Variables/Chapter5/Scenes/iFinished198", "N")

--Nothing done yet:
if(iSawProblemWithProgram == 0.0 and iTalkedToElectrosprite == 0.0) then

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Christine:[VOICE|Leader] (I didn't really see anything unusual during that playthrough.[P] Maybe I should reset the program and try again?)") ]])
    fnCutsceneBlocker()

--Saw the problem but didn't fix it:
elseif(iSawProblemWithProgram == 1.0 and iTalkedToElectrosprite == 0.0) then

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Christine:[VOICE|Leader] (That NPC was definitely different from the others, but she warped or something.[P] I wonder what I should do?)[B][C]") ]])
    fnCutscene([[ Append("Christine:[VOICE|Leader] ^PDU, activate radio mode. Did you notice anything unusual?^[B][C]") ]])
    fnCutscene([[ Append("PDU:[VOICE|PDU] ^Yes, Lord Unit, but I was unable to track the odd behavior on the stack.[P] I will need you to interact with the NPC for a longer period.^[B][C]") ]])
    fnCutscene([[ Append("Christine:[VOICE|Leader] (Just like tracing a call...[P] but the NPC warped away.[P] I wonder if I can chase it somehow?)") ]])
    fnCutsceneBlocker()

--Talked to Susan but somehow *didn't* see the problem first, you hacker.
elseif(iSawProblemWithProgram == 0.0 and iTalkedToElectrosprite == 1.0 and iBecameElectrosprite == 0.0) then

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Christine:[VOICE|Leader] (Good thing I decided to randomly and for no reason at all type in that command.[P] Let's see what my PDU discovered.)[B][C]") ]])
    fnCutscene([[ Append("Christine:[VOICE|Leader] ^PDU, activate radio mode.[P] What have you discovered?^[B][C]") ]])
    fnCutscene([[ Append("PDU:[VOICE|PDU] ^Lord Unit, when you spoke to the NPC I detected unusual electrical interference moving through the server room.^[B][C]") ]])
    fnCutscene([[ Append("PDU:[VOICE|PDU] ^It jumps between server racks periodically.[P] I may be able to rewire a terminal to isolate it.^[B][C]") ]])
    fnCutscene([[ Append("Christine:[VOICE|Leader] ^There's a broken terminal over there.[P] Will that do?^[B][C]") ]])
    fnCutscene([[ Append("PDU:[VOICE|PDU] ^It may suffice.[P] I will look up circuit schematics for you.[P] Proceed to the damaged terminal.^") ]])
    fnCutsceneBlocker()

--Saw the problem and talked to the Electrosprite NPC:
elseif(iSawProblemWithProgram == 1.0 and iTalkedToElectrosprite == 1.0 and iBecameElectrosprite == 0.0) then

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Christine:[VOICE|Leader] (Well that was definitely weird...[P] Let's see what the PDU has to say.)[B][C]") ]])
    fnCutscene([[ Append("Christine:[VOICE|Leader] ^PDU, activate radio mode.[P] What have you discovered?^[B][C]") ]])
    fnCutscene([[ Append("PDU:[VOICE|PDU] ^Lord Unit, when you spoke to the NPC I detected unusual electrical interference moving through the server room.^[B][C]") ]])
    fnCutscene([[ Append("PDU:[VOICE|PDU] ^It jumps between server racks periodically.[P] I may be able to rewire a terminal to isolate it.^[B][C]") ]])
    fnCutscene([[ Append("Christine:[VOICE|Leader] ^There's a broken terminal over there.[P] Will that do?^[B][C]") ]])
    fnCutscene([[ Append("PDU:[VOICE|PDU] ^It may suffice.[P] I will look up circuit schematics for you.[P] Proceed to the damaged terminal.^") ]])
    fnCutsceneBlocker()

--Replays after becoming an electrosprite:
elseif(iSawProblemWithProgram == 1.0 and iTalkedToElectrosprite == 1.0 and iBecameElectrosprite == 1.0 and iFinished198 == 0.0) then

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Christine:[VOICE|Leader] (That was fun, but it doesn't look like Psue is in the program anymore.[P] I guess she's probably laying low to avoid attracting attention.)") ]])
    fnCutsceneBlocker()

--Replays after finishing the sector.
else

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Christine:[VOICE|Leader] (Converting humans is its own reward, and it's like I'm visiting old friends![P] Oh well, back to work.)") ]])
    fnCutsceneBlocker()

end
