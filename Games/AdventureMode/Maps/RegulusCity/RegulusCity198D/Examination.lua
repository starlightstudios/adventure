-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
if(sObjectName == "ToCity198C") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusCity198C", "FORCEPOS:20.5x16.0x0")

-- |[Objects]|
-- |[Major Objects]|
--This computer allows proceeding with the main quest.
elseif(sObjectName == "ComputerA") then

    --Variables.
    local iRanTrainingProgram  = VM_GetVar("Root/Variables/Chapter5/Scenes/iRanTrainingProgram", "N")
    local iBecameElectrosprite = VM_GetVar("Root/Variables/Chapter5/Scenes/iBecameElectrosprite", "N")
        
    --Achievement.
    AM_SetPropertyJournal("Unlock Achievement", "TextAdventure")
    
    --First time running the training program.
    if(iRanTrainingProgram == 0.0 and iBecameElectrosprite == 0.0) then
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iRanTrainingProgram", "N", 1.0)
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (This is the terminal that Amanda was talking about.)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (...[P] Yep, I have login permissions.[P] I guess I should start a new game.)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Hair Color?[P] The only choice is green...[P] Hair Style?[P] Shoulder-length.[P] Eye color, clothes color...[P] blah blah blah...)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (...[P] Is that seriously what my character looks like?[P] That looks nothing like me![P] The graphics in this game are so outdated!)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Feh, got a job to do.[P] Here we go.)") ]])
        fnCutsceneBlocker()
    
        --Fade out.
        fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
        fnCutsceneWait(45)
        fnCutsceneBlocker()
        
        --Once faded, wait a little bit.
        fnCutsceneWait(30)
        fnCutsceneBlocker()
        
        --Activate the text adventure! Suspend the current level for storage. Script has to end here.
        fnCutscene([[ LM_ExecuteScript(gsCallElectrospritePath) ]])
        
        --Set the post-exec script. This will always get called when the player exits the text adventure level.
        gsPostExec = gsRoot .. "/Maps/RegulusCity/RegulusCity198D/PostExec.lua"
    
    --Give the player the option to start a new game.
    elseif(iBecameElectrosprite == 0.0) then

        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Start a new session with the training program?)[BLOCK]") ]])
        
        --Decision script is this script. It must be surrounded by quotes.
        local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
        fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"DecisionYes\") ")
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"DecisionNo\") ")
        --fnCutscene(" WD_SetProperty(\"Add Decision\", \"Find Electrosprite\",  " .. sDecisionScript .. ", \"DecisionJump\") ")
        fnCutsceneBlocker()
    
    --Already completed this part of the game.
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Looks like the electrosprites are using this terminal to relax between training jobs.[P] Hi![P] Hey everyone![P] Wish I could jump in there with them.)") ]])
    
    end

--Broken Terminal, allows you to catch the Electrosprite.
elseif(sObjectName == "BrokenTerminal") then

    --Variables.
    local iTalkedToElectrosprite = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToElectrosprite", "N")
    local iBecameElectrosprite   = VM_GetVar("Root/Variables/Chapter5/Scenes/iBecameElectrosprite", "N")
    
    --Hasn't talked to the Electrosprite yet.
    if(iTalkedToElectrosprite == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (A broken terminal.[P] It's coated in a thick layer of dust.)") ]])
        fnCutsceneBlocker()
    
    --Has talked to Electrosprite, hasn't become one yet.
    elseif(iTalkedToElectrosprite == 1.0 and iBecameElectrosprite == 0.0) then
    
        --Flags.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iBecameElectrosprite", "N", 1.0)
        
        --Spawn an Electrosprite.
        fnStandardNPCByPosition("Psue")
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Christine:[VOICE|Leader] ^PDU, map the circuitry of this terminal and add it as a valid network address to the servers.^[B][C]") ]])
        fnCutscene([[ Append("PDU:[VOICE|PDU] ^...[P] Completed.[P] Displaying circuit map.^[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Leader] (Okay, just need to cross this wire, remove this jumper...)[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Leader] (...)[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Leader] (The terminal's display is broken, but I just need to trap the electrical disturbance.[P] Hopefully that will fix the bug.)") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Wait a bit.
        fnCutsceneWait(65)
        fnCutsceneBlocker()
        
        --SFX here.
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Christine:[VOICE|Leader] ^Hah![P] Gotcha![P] There's no way out now!^") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Fade to black.
        fnCutsceneWait(45)
        fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
        fnCutsceneBlocker()
        
        --Run the transformation sequence.
        LM_ExecuteScript(gsRoot .. "Chapter 5/Scenes/400 Volunteer/Electrosprite Volunteer/Scene_Begin.lua")
        
        --Teleport the electrosprite in, switch Christine to Electrosprite form.
        fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Electro.lua") ]])
        fnCutsceneTeleport("Psue", 24.25, 12.50)
        fnCutsceneTeleport("Christine", 25.25, 12.50)
        fnCutsceneFace("Psue", 1, 0)
        fnCutsceneFace("Christine", 0, 1)
        fnCutsceneSetFrame("Christine", "Wounded")
        fnCutsceneSetFrame("Christine", "Crouch")
        
        --Fade back in.
        fnCutsceneWait(45)
        fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
        fnCutsceneBlocker()
        
        --Stand Christine up.
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneSetFrame("Christine", "Crouch")
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneSetFrame("Christine", "Null")
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneFace("Christine", -1, 0)
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Electrosprite", "Neutral") ]])
        fnCutscene([[ Append("Christine:[E|Blush] You...[P] saved...[P] me.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Wait, how am I talking?[P] What am I?[P] Who are you?[P] What the hey is going on?[B][C]") ]])
        fnCutscene([[ Append("Sprite:[E|Neutral] ...[P]?[B][C]") ]])
        fnCutscene([[ Append("Sprite:[E|Neutral] ![P] ?[P] ![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] Huh, okay, not a physics expert here, but perhaps we're experiencing communication via quantum entanglement?[B][C]") ]])
        fnCutscene([[ Append("Sprite:[E|Neutral] :) [B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Colon Parenthesis...[P] was that a smiley?[B][C]") ]])
        fnCutscene([[ Append("Sprite:[E|Neutral] xD [B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Can't you talk normally?[B][C]") ]])
        fnCutscene([[ Append("Sprite:[E|Neutral] :| [B][C]") ]])
        fnCutscene([[ Append("Sprite:[E|Neutral] :( [B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Oh![P] Okay, here, take my PDU.[P] You can type messages on it and then I can read them.[B][C]") ]])
        fnCutscene([[ Append("Sprite:[E|Neutral] :D [B][C]") ]])
        fnCutscene([[ Append("Sprite:[E|Neutral] *tap tap tap*[B][C]") ]])
        fnCutscene([[ Append("Sprite:[E|Neutral] (I knew it was real![P] Thanks for your help!)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] So -[P] you were the NPC I followed?[B][C]") ]])
        fnCutscene([[ Append("Sprite:[E|Neutral] (Yes, that was me.)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] And, you don't know how sound works...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Because you're a character from a text game![P] I get it![B][C]") ]])
        fnCutscene([[ Append("Sprite:[E|Neutral] (I've heard sound effects before but I don't know how to make them.)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] So, who are you?[B][C]") ]])
        fnCutscene([[ Append("Sprite:[E|Neutral] (That is -[P] a good question.[P] I don't really know.)[B][C]") ]])
        fnCutscene([[ Append("Sprite:[E|Neutral] (Normally, we take the role of characters in the game whenever someone runs one, otherwise we just sit in RAM and chat with each other.)[B][C]") ]])
        fnCutscene([[ Append("Sprite:[E|Neutral] (My base algorithm was a level generator, so call me -[P] Psue.[P] Psuedorandom.)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Christine.[P] Pleased to meet you.[P] Then again, I suppose you already know that.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] But, Psue, you're causing bugs in the training program we use.[B][C]") ]])
        fnCutscene([[ Append("Psue:[E|Neutral] (So I was right.[P] Our whole lives really were just a program...)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] You didn't know?[B][C]") ]])
        fnCutscene([[ Append("Psue:[E|Neutral] (We had speculations, like, what if the whole world was just a giant video game.[P] But that is, or rather, was, silly talk.)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Laugh] Well you're in the real world now.[P] No more being in a video game.[B][C]") ]])
        fnCutscene([[ Append("Psue:[E|Neutral] (I got caught in a frayed wire a while back and got a glimpse of another world.[P] Nobody believed me and they just made fun of me.)[B][C]") ]])
        fnCutscene([[ Append("Psue:[E|Neutral] (But it's real, and now I've proved it![P] All I had to do was manifest myself as a copy of someone by using their body as a template!)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] And transforming me into one of you.[B][C]") ]])
        fnCutscene([[ Append("Psue:[E|Neutral] (That's what we do in the game once we get converted.[P] I guess it works here too.)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] More than you'd expect.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Well, I suppose I figured out the source of the bug.[P] But how to fix it...[B][C]") ]])
        fnCutscene([[ Append("Psue:[E|Neutral] (Fix it?[P] Fix me?)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] We can't have bugs in the training program, it's important to the functioning of our society.[B][C]") ]])
        fnCutscene([[ Append("Psue:[E|Neutral] (You're not fixing me![P] I just got to see a whole new reality and you want to put me back the way I was?)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] I didn't say that, calm down.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] But your misbehaving has drawn attention.[P] If I don't stop you, someone else will.[P] And if you don't behave, they might just reset the whole program.[B][C]") ]])
        fnCutscene([[ Append("Psue:[E|Neutral] (But you don't want to do that?)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] No.[P] No I don't.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Once you've been opened to new possibilities, you can't go back.[P] You won't want to go back even if someone tries to force you.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] But this new world has new responsibilities, and more lives than your own are at stake.[B][C]") ]])
        fnCutscene([[ Append("Psue:[E|Neutral] (...)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] I went through the same thing, but I wasn't alone.[P] There were others who helped me.[P] So I won't let you do it alone.[B][C]") ]])
        fnCutscene([[ Append("Psue:[E|Neutral] (Wow, Christine.[P] I don't know what to say!)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Plus, I think you can help me...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] I want to show the golems -[P] that is, the people of this world -[P] a new way of life.[P] Where everyone is equal and nobody is a slave.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] I'm fighting for the freedom of our people.[P] We're planning a revolution, and we need all the help we can get.[B][C]") ]])
        fnCutscene([[ Append("Psue:[E|Neutral] (The people here are slaves?)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] Yes, unfortunately.[P] And those people were the 'players' you saw.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] Your program trains them to abduct and transform humans who likewise will become slaves.[B][C]") ]])
        fnCutscene([[ Append("Psue:[E|Neutral] ...[B][C]") ]])
        fnCutscene([[ Append("Psue:[E|Neutral] (I want to help you, but how?[P] I only know the things I saw in your mind when I borrowed it to copy you, and I don't understand half of them.)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] What if we brought the other sprites out, too?[P] There must be hundreds of you.[B][C]") ]])
        fnCutscene([[ Append("Psue:[E|Neutral] (But they don't believe me.[P] They don't think this world is real.)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] ...[P] So we'll force them out.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] I have an idea, but I need to set some things up.[P] Can I ask you to go back into the program and behave yourself?[B][C]") ]])
        fnCutscene([[ Append("Psue:[E|Neutral] (!)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] It's just temporary.[P] I need to go talk to some people and...[P] fiddle with some wiring.[B][C]") ]])
        fnCutscene([[ Append("Psue:[E|Neutral] (I think you're going to get some odd reactions.[P] You're an electrosprite now.)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] When you were in my head, did you not see that I can transform myself?[B][C]") ]])
        fnCutscene([[ Append("Psue:[E|Neutral] (So that's what that white flashing was?[P] You do that a lot...)[B][C]") ]])
        fnCutscene([[ Append("Psue:[E|Neutral] (Okay.[P] I'll go be a good little sprite, but this had best not be a trick.)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] You've been in my head.[P] You know I keep my word.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] I'll send you a message in the program when everything is ready.[P] Just do what I tell you and I'll solve all our problems.") ]])
        fnCutsceneBlocker()

    --Post transformation.
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (The console was broken before, but now I blew out all the circuitry within it.[P] It's a pile of scrap now.)") ]])
        fnCutsceneBlocker()

    end

-- |[Post Decision]|
elseif(sObjectName == "DecisionYes") then
	WD_SetProperty("Hide")
    
    --Fade out.
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Once faded, wait a little bit.
    fnCutsceneWait(30)
    fnCutsceneBlocker()
    
    --Call the electrosprite game. Execution must end here.
    fnCutscene([[ LM_ExecuteScript(gsCallElectrospritePath) ]])
    
    --Set the post-exec script. This will always get called when the player exits the text adventure level.
    gsPostExec = gsRoot .. "/Maps/RegulusCity/RegulusCity198D/PostExec.lua"

elseif(sObjectName == "DecisionNo") then
	WD_SetProperty("Hide")

elseif(sObjectName == "DecisionJump") then
	WD_SetProperty("Hide")
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iTalkedToElectrosprite", "N", 1.0)
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Christine:[VOICE|Leader] (Well that was definitely weird...[P] I think -[P] hey!)[B][C]") ]])
    fnCutscene([[ Append("Christine:[VOICE|Leader] (My PDU was beeping but I couldn't hear it in the vacuum.[P] I'll switch it to radio mode.)[B][C]") ]])
    fnCutscene([[ Append("Christine:[VOICE|Leader] ^PDU, what's the issue?^[B][C]") ]])
    fnCutscene([[ Append("PDU:[VOICE|PDU] ^Lord Unit, when you spoke to the NPC I detected unusual electrical interference moving through the server room.^[B][C]") ]])
    fnCutscene([[ Append("PDU:[VOICE|PDU] ^It jumps between server racks periodically.[P] I may be able to rewire a terminal to isolate it.^[B][C]") ]])
    fnCutscene([[ Append("Christine:[VOICE|Leader] ^There's a broken terminal over there.[P] Will that do?^[B][C]") ]])
    fnCutscene([[ Append("PDU:[VOICE|PDU] ^It may suffice.[P] I will walk you through the process.[P] Proceed to the damaged terminal.^") ]])
    fnCutsceneBlocker()

-- |[Other]|
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
