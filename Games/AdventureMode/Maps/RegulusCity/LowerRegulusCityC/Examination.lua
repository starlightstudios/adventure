-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
if(sObjectName == "LadderD") then
    AudioManager_PlaySound("World|ClimbLadder")
    AL_BeginTransitionTo("LowerRegulusCityD", "FORCEPOS:34.0x21.0x0")
    
elseif(sObjectName == "DoorEN") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("LowerRegulusCityB", "FORCEPOS:22.0x28.0x0")
    
elseif(sObjectName == "DoorES") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("LowerRegulusCityB", "FORCEPOS:22.0x34.0x0")
    
elseif(sObjectName == "DoorN") then
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("LowerRegulusCityB", "FORCEPOS:11.0x20.0x0")

-- |[Examination]|
elseif(sObjectName == "StorageTerminal") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The manifests indicate that this room is storing a very large amount of nothing.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "PipeTerminal") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Pressure and temperature monitoring logs.[P] Nothing interesting.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "OtherTerminal") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Apparently there was once a geothermal drilling pipe here, and this terminal monitored it.[P] I guess it's been sitting here unused for years.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "RockCollection") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (An organized collection of rocks from the surface.[P] They're even labelled with pet names.)") ]])
    fnCutsceneBlocker()

-- |[Debug]|
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
