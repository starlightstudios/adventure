-- |[ ====================================== Examination ======================================= ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[ ========================================= Exits ========================================== ]|
--Exit
if(sObjectName == "ElevatorL" or sObjectName == "ElevatorR") then

    --Variables.
    local iMet55InLowerRegulus = VM_GetVar("Root/Variables/Chapter5/Scenes/iMet55InLowerRegulus", "N")

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Where shall I take the elevators to?)[BLOCK]") ]])

    --Decision script is this script. It must be surrounded by quotes.
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
    if(sObjectName == "ElevatorL") then
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Main Floor\", " .. sDecisionScript .. ", \"MainFloorL\") ")
        if(iMet55InLowerRegulus == 1.0) then fnCutscene(" WD_SetProperty(\"Add Decision\", \"Basement 2F\", " .. sDecisionScript .. ", \"Basement2L\") ") end
    else
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Main Floor\", " .. sDecisionScript .. ", \"MainFloorR\") ")
        if(iMet55InLowerRegulus == 1.0) then fnCutscene(" WD_SetProperty(\"Add Decision\", \"Basement 2F\", " .. sDecisionScript .. ", \"Basement2R\") ") end
    end
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Cancel\",  " .. sDecisionScript .. ", \"NoClose\") ")
    fnCutsceneBlocker()

--Broken airlock.
elseif(sObjectName == "BrokenAirlock") then

	--Variables.
	local iIs55Following = VM_GetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N")

	--If 55 is not present:
	if(iIs55Following == 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[SOUND|World|AutoDoorFail]Access denied.[P] Airlock internal door is malfunctioning.[P] Please contact the maintenance crew.") ]])
		fnCutsceneBlocker()
	
	--Otherwise:
	else

		--Variables.
		local i55ExplainedRegulusBrokenAirlock = VM_GetVar("Root/Variables/Chapter5/Scenes/i55ExplainedRegulusBrokenAirlock", "N")

		--If 55 has not explained the airlock yet, do that now.
		if(i55ExplainedRegulusBrokenAirlock == 0.0) then
			
			--Flag.
			VM_SetVar("Root/Variables/Chapter5/Scenes/i55ExplainedRegulusBrokenAirlock", "N", 1.0)

			--Dialogue.
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
			fnCutscene([[ Append("[SOUND|World|AutoDoorFail]Access denied.[P] Airlock internal door is malfunctioning.[P] Please contact the maintenance crew.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Seems we'll have to go around.[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] The door isn't actually broken, I've just reprogrammed it to behave like it is.[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] Thus I have a point of ingress into the city without any suspicion.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] Is this how you got in when you first converted me?[B][C]") ]])
			fnCutscene([[ Append("55:[E|Smirk] Correct.[P] I merely crossed the door circuits with the airlock above, and waited for you to open it.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] How clever of you.[P] I never suspected a thing.[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] Would you like the door opened to the exterior?[BLOCK]") ]])

			--Decision script is this script. It must be surrounded by quotes.
			local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
			fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
			fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"YesOpen\") ")
			fnCutscene(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"NoClose\") ")
			fnCutsceneBlocker()

		--Otherwise, skip right to her asking you to open the door.
		else
		
			--Dialogue.
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
			fnCutscene([[ Append("55:[E|Neutral] Would you like the door opened to the exterior?[BLOCK]") ]])

			--Decision script is this script. It must be surrounded by quotes.
			local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
			fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
			fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"YesOpen\") ")
			fnCutscene(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"NoClose\") ")
			fnCutsceneBlocker()

		end
	end
	
-- |[ ================================ Conventional Examinables ================================ ]|
--Damaged terminal.
elseif(sObjectName == "DamagedTerminal") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Christine](A wrecked terminal.[P] Looks like some unit was punching it.[P] Wonder why?)") ]])
	fnCutsceneBlocker()

--Assorted junk lying around.
elseif(sObjectName == "Junk") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Christine](Junk, just left lying around down here.)") ]])
	fnCutsceneBlocker()

--Box full of crap.
elseif(sObjectName == "Box") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Christine](This crate is full of miscellaneous bolts and parts.[P] It all looks to be in bad condition.)") ]])
	fnCutsceneBlocker()

--Big box full of crap.
elseif(sObjectName == "BoxBig") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Christine](Supply crates covered in dust.[P] Looks like they're set to be delivered somewhere but the delivery queue is really long...)") ]])
	fnCutsceneBlocker()

--Vents.
elseif(sObjectName == "Vent") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Christine](These must be the vents that 2855 is using to get around the sector...)") ]])
	fnCutsceneBlocker()

--Intercom.
elseif(sObjectName == "Intercom") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Christine](There's no reply from the intercom.[P] Looks like it's broken.)") ]])
	fnCutsceneBlocker()

--Fizzypop!
elseif(sObjectName == "FizzyDrinks") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Christine](Fizzy Pop![P] A great way to recharge![P] The machine is actually in good condition, but I'm not low on power right now.)") ]])
	fnCutsceneBlocker()

--Fabricator.
elseif(sObjectName == "Fabricator") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Christine](The fabrication machine is intact but not receiving power.)") ]])
	fnCutsceneBlocker()

-- |[ =============================== Rune Upgrade / TF Terminal =============================== ]|
elseif(sObjectName == "Terminal") then

	--Variables.
    local iIs55Following        = VM_GetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N")
	local iMet55InLowerRegulus  = VM_GetVar("Root/Variables/Chapter5/Scenes/iMet55InLowerRegulus", "N")
    local iRuneUpgradeTaken     = VM_GetVar("Root/Variables/Chapter5/Scenes/iRuneUpgradeTaken", "N")
    local iRuneUpgradeBriefing  = VM_GetVar("Root/Variables/Chapter5/Scenes/iRuneUpgradeBriefing", "N")
    local iRuneUpgradeEquinox   = VM_GetVar("Root/Variables/Chapter5/Scenes/iRuneUpgradeEquinox", "N")
    local iRuneUpgradeSerenity  = VM_GetVar("Root/Variables/Chapter5/Scenes/iRuneUpgradeSerenity", "N")
    local iRuneUpgradeTransit   = VM_GetVar("Root/Variables/Chapter5/Scenes/iRuneUpgradeTransit", "N")
    local iRuneUpgradeCompleted = VM_GetVar("Root/Variables/Chapter5/Scenes/iRuneUpgradeCompleted", "N")
	
    -- |[ ===================== Runestone Upgrade Case ===================== ]|
	--Already met 55.
	if(iMet55InLowerRegulus == 1.0) then
        
        -- |[Runestone Quest Briefing]|
        if(iRuneUpgradeTaken == 1.0 and iRuneUpgradeBriefing == 0.0 and iRuneUpgradeCompleted == 0.0) then
            
            --If 55 is not present.
            if(iIs55Following == 0.0) then
                    
                --Add her as a member.
                fnAddPartyMember("Tiffany", false)
                
                --Dialogue.
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ Append("[VOICE|Christine](This is the terminal I was ordered to look at, there's some encrypted data on it.[P] Which means...)") ]])
                fnCutsceneBlocker()
                fnCutsceneWait(25)
                fnCutsceneBlocker()
                
                --Movement.
                fnCutsceneTeleport("Tiffany", 43.25, 5.50)
                fnCutsceneWait(25)
                fnCutsceneBlocker()
                fnCutsceneMove("Tiffany", 43.25, 9.50)
                fnCutsceneMove("Tiffany", 38.25, 9.50)
                fnCutsceneBlocker()
                fnCutsceneFace("Christine", 0, 1)
                fnCutsceneMove("Tiffany", 38.25, 6.50)
                fnCutsceneBlocker()
                
                --Dialogue.
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
                fnCutscene([[ Append("55:[E|Smirk] I trust I did not keep you waiting long.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Your sense of timing is perfect.[P] So, what is it you wanted to show me with your 'work order'?[B][C]") ]])
            
            --55 is present.
            else
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] All right, I got your work order.[P] The terminal is functioning normally, so what did you want to show me?[B][C]") ]])
            end
            
            --Flags.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iRuneUpgradeBriefing", "N", 1.0)
            WD_SetProperty("Unlock Topic", "Academic Code", 1)
            fnCutscene([[ Append("55:[E|Smirk] Observant.[P] Good.[P] I installed a network backdoor for myself in the LRT datacore.[P] I decided to take a look at what information I could find concerning your runestone.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Ah, I see.[P] That looks like my runestone but the symbol is different.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Indeed.[P] The Administration has taken an interest in the runestones, there are presently at least two others that have been sighted on Pandemonium.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] This image was incorrectly unencrypted in an academic mail, but unfortunately, even with access to the core, I cannot decipher the rest of the data.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] The password protection on this is the highest grade, it often takes whole minutes to decrypt even for the academics with access.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Could we pay a trip to the university?[P] If we could get you into their consoles...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Too risky, plus, I believe I can piece together the decryption engine.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] According to the data at the LRT facility, I will need to visit the Equinox laboratories, Serenity Crater Observatory, and the transit station just north of Cryogenics.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] The hard drives in cryogenics are likely beyond use, but the transit station has a network bounce I can use.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] And if we do get these?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Smug] We will gain crucial information about your runestone.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] I cannot say exactly what information, but we may be able to improve it, or use a functionality you were unaware of.[P] The other runebearers may know more than we do.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Laugh] Nice to know I'm not alone![B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] All right.[P] Equinox, Serenity, and the transit station near cryogenics.[P] Shouldn't be a problem.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] When we have all three, return to this terminal, and I will decrypt the files.") ]])
            fnCutsceneBlocker()
            
            --Fold.
            fnAutoFoldParty()
            fnCutsceneBlocker()
            
            return
        
        -- |[Runestone Quest Reminder / Completion]|
        elseif(iRuneUpgradeTaken == 1.0 and iRuneUpgradeBriefing == 1.0 and iRuneUpgradeCompleted == 0.0) then
            
            -- |[Reminder]|
            --Remind the player, does not require 55 to be present.
            if(iRuneUpgradeEquinox == 0.0 or iRuneUpgradeSerenity == 0.0 or iRuneUpgradeTransit == 0.0) then
                
                if(iIs55Following == 0.0) then
                    fnCutscene([[ WD_SetProperty("Show") ]])
                    fnCutscene([[ Append("[VOICE|Christine](This terminal has the data 55 has collected, but it's still encrypted.[P] We need to get the decryption engine in Equinox, Serenity Observatory, and the transit station near cryogenics.)") ]])
                    fnCutsceneBlocker()
                    return
                end
                
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
                fnCutscene([[ Append("55:[E|Smirk] To reiterate, I need to examine the terminals in Equinox, Serenity, and the transit station north of Cryogenics.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] I should be able to resolve the decryption engine if I can get access to all three, and we may be able to make better use of your runestone.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] Unfortunately I cannot say precisely which console, if any, will contain the information I need.[P] We may need to search.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] All right.[P] Equinox, Serenity, and the transit station near cryogenics.[P] Shouldn't be a problem.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] When we have all three, return to this terminal, and I will decrypt the files.") ]])
                fnCutsceneBlocker()
            
            -- |[Complete Mission]|
            else
            
                --If 55 is not present.
                if(iIs55Following == 0.0) then
                    
                    --Add her as a member.
                    fnAddPartyMember("Tiffany", false)
                    
                    --Dialogue.
                    fnCutscene([[ WD_SetProperty("Show") ]])
                    fnCutscene([[ Append("[VOICE|Christine](Looks like 55 hasn't uploaded the encryption keys yet.[P] Let's see if my prediction is right...)") ]])
                    fnCutsceneBlocker()
                    fnCutsceneWait(25)
                    fnCutsceneBlocker()
                    
                    --Movement.
                    fnCutsceneTeleport("Tiffany", 43.25, 5.50)
                    fnCutsceneWait(25)
                    fnCutsceneBlocker()
                    fnCutsceneMove("Tiffany", 43.25, 9.50)
                    fnCutsceneMove("Tiffany", 38.25, 9.50)
                    fnCutsceneBlocker()
                    fnCutsceneFace("Christine", 0, 1)
                    fnCutsceneMove("Tiffany", 38.25, 6.50)
                    fnCutsceneBlocker()
                    
                    --Dialogue.
                    fnCutscene([[ WD_SetProperty("Show") ]])
                    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
                    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
                    fnCutscene([[ Append("55:[E|Smirk] Ah, you are already here.[P] I was about to call you.[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Happy] Are you kidding?[P] I can't wait! Let's find out more about this doohickey![B][C]") ]])
                
                --55 is present.
                else
                    fnCutscene([[ WD_SetProperty("Show") ]])
                    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
                    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
                    fnCutscene([[ Append("55:[E|Smirk] Excellent.[P] All three produce a decryption engine...[P] Good.[P] It will take a moment and we will know what the administrators know.[B][C]") ]])
                end
                
                VM_SetVar("Root/Variables/Chapter5/Scenes/iRuneUpgradeCompleted", "N", 1.0)
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
                fnCutscene([[ Append("55:[E|Smirk] Excellent.[P] All three produce a decryption engine...[P] Good.[P] It will take a moment and we will know what the administrators know.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] I've been looking at that runestone.[P] I think it means 'Quiet'.[P] I think.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] Interesting.[P] What do you base that on?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Sad] Instinct.[P] Sorry, it's just the word that comes to me.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] The linguistic database on 'Draconic' is limited, but that is fairly close.[P] However, it is not an exact match.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] The database has no matches similar to your runestone's marking in that language, though.[P] It may also be incorrect, the draconic language was only used by academics as a common language, and most of the documents were lost seven centuries ago.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Let's see here.[P] This runestone belongs to a subject named 'Jeanne'.[P] There are three other candidates, one confirmed, two hypothesized.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] Earth.[P] That is where you are from.[P] There is mention of it.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Laugh] Ha ha, look at this one![B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] 'The only visual reference found to this runebearer is a blurry photo taken by an undercover abductions unit.[P] She is punching an orc handkegger in the face.'[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] I have no idea who she is but I bet we'd get along famously.[P] What about the most recent one?[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] A runebearer found in the Quantir region, northern Arulenta, captured on videograph by a team filming an action movie.[P] Unfortunately, the team did not know about the runestone's true nature, and attempts to locate the bearer have proved fruitless.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] I like her, too.[P] I wonder if they want to be golems, too...[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] Academic jargon...[P] yes, here.[P] See?[P] The runestones are 'rusted'.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Upset] More waffling.[P] Professors waste too much time on unimportant material.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] Interesting.[P] The runestones should perform better after a 'cleaning' using common chemical agents.[P] In fact, there should be some here.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] This whole time, all it needed was a bit of scrubbing?[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] ...[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] There.[P] Tell me how it feels.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Blush] 55![P] Woah, it has quite a kick to it![P] It both repairs damage, and now seems to give me a powerful jolt.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Happy] Is there anything else we can do?[P] I feel like this is just the start![B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] Apologies, but this is all.[P] Research is extremely limited and based more off legend than analysis.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] With time and access to a lab, we could experiment on your runestone, but we lack both.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] ...[P] I thought so.[P] The city comes first, runestone second.[P] Still, thanks for doing all this for me.[P] I have a feeling it's going to come in handy.") ]])
                fnCutsceneBlocker()
            
                -- |[NG+ Check]|
                --If there is already a Mk II runestone, don't upgrade it.
                if(AdInv_GetProperty("Item Count", "Violet Runestone Mk II") > 0) then
                    return
                
                --If Christine already equipped it:
                else
                
                    --Push Christine.
                    AdvCombat_SetProperty("Push Party Member", "Christine")
                    
                        --Get slot contents.
                        local sItemInSlotA = AdvCombatEntity_GetProperty("Equipment In Slot S", "Item A")
                        local sItemInSlotB = AdvCombatEntity_GetProperty("Equipment In Slot S", "Item B")
                
                        if(sItemInSlotA == "Violet Runestone Mk II") then
                            DL_PopActiveObject()
                            return
                        elseif(sItemInSlotB == "Violet Runestone Mk II") then
                            DL_PopActiveObject()
                            return
                        end
                    DL_PopActiveObject()
                end
                
                -- |[Rune Upgrade]|
                --If Christine's runestone is in the inventory, remove it and replace it with the Mk II version.
                if(AdInv_GetProperty("Item Count", "Violet Runestone") > 0) then
                    AdInv_SetProperty("Remove Item", "Violet Runestone")
                    LM_ExecuteScript(gsItemListing, "Violet Runestone Mk II")
                
                --Otherwise, Christine must have it equipped. Figure out which slot it is in, and replace it.
                else
                    --Add the item.
                    LM_ExecuteScript(gsItemListing, "Violet Runestone Mk II")
                
                    --Swap the new runestone in.
                    AdvCombat_SetProperty("Push Party Member", "Christine")
                        local sItemInSlotA = AdvCombatEntity_GetProperty("Equipment In Slot S", "Item A")
                        local sItemInSlotB = AdvCombatEntity_GetProperty("Equipment In Slot S", "Item B")
                
                        if(sItemInSlotA == "Violet Runestone") then
                            AdvCombatEntity_SetProperty("Unequip Slot", "Item A")
                            AdvCombatEntity_SetProperty("Equip Item To Slot", "Item A", "Violet Runestone Mk II")
                
                        elseif(sItemInSlotB == "Violet Runestone") then
                            AdvCombatEntity_SetProperty("Unequip Slot", "Item B")
                            AdvCombatEntity_SetProperty("Equip Item To Slot", "Item B", "Violet Runestone Mk II")
                        
                        end
                    DL_PopActiveObject()
                    
                    --Remove the old runestone.
                    AdInv_SetProperty("Remove Item", "Violet Runestone")
                end
            
                --Fold.
                fnAutoFoldParty()
                fnCutsceneBlocker()
            
            end
            return
        end
        
        -- |[Normal Examination]|
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[VOICE|Christine](This terminal is active, but the PDU cut its network access.[P] I can't use it for anything fun.)") ]])
		fnCutsceneBlocker()
	
    -- |[ ===================== External Cutscene Case ===================== ]|
	--Scene.
	else
		VM_SetVar("Root/Variables/Chapter5/Scenes/iMet55InLowerRegulus", "N", 1.0)
		LM_ExecuteScript(fnResolvePath() .. "Meeting 55.lua")
	end

-- |[ ==================================== Dialogue Replies ==================================== ]|
--Open the door to lower Exterior SD.
elseif(sObjectName == "YesOpen") then
	WD_SetProperty("Hide")

    --Variables.
	local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
    
    --Christine is not in a vacuum-safe format.
	if(sChristineForm == "Human" or sChristineForm == "Raiju") then

        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Give me a moment to put my face on, and we'll be off.") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
	
		--Flashwhite.
		Cutscene_CreateEvent("Flash Christine White", "Actor")
			ActorEvent_SetProperty("Subject Name", "Christine")
			ActorEvent_SetProperty("Flashwhite Quickly", "Null")
		DL_PopActiveObject()
		fnCutsceneBlocker()

		fnCutsceneWait(75)
		fnCutsceneBlocker()
		fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua") ]])
		fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
		fnCutsceneBlocker()

    end

	fnCutscene([[ AL_BeginTransitionTo("RegulusExteriorSD", "FORCEPOS:14.5x4.0x0") ]])
	fnCutsceneBlocker()

--Close the dialogue.
elseif(sObjectName == "NoClose") then
	WD_SetProperty("Hide")
    
-- |[Main Floor]|
elseif(sObjectName == "MainFloorL" or sObjectName == "MainFloorR") then
	
	--Variables.
	local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
	local iIs55Following = VM_GetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N")
    local iSaw55sMemories = VM_GetVar("Root/Variables/Chapter5/Scenes/iSaw55sMemories", "N")
    local iSaw55PostLRT   = VM_GetVar("Root/Variables/Chapter5/Scenes/iSaw55PostLRT", "N")
	WD_SetProperty("Hide")
	
	--Christine needs to change form.
	if(sChristineForm ~= "Golem") then
	
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[VOICE|Christine](Better switch back to Golem.[P] Wouldn't want to draw unnecessary attention.)") ]])
		fnCutsceneBlocker()
	
		--Flashwhite.
		Cutscene_CreateEvent("Flash Christine White", "Actor")
			ActorEvent_SetProperty("Subject Name", "Christine")
			ActorEvent_SetProperty("Flashwhite Quickly", "Null")
		DL_PopActiveObject()
		fnCutsceneBlocker()

		fnCutsceneWait(75)
		fnCutsceneBlocker()
		fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua") ]])
		fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
		fnCutsceneBlocker()
	end
	
	--If 55 is following, she mentions she's leaving here.
	if(iIs55Following == 1.0) then
		
        --Post LRT scene:
        if(iSaw55sMemories == 1.0 and iSaw55PostLRT == 0.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
            fnCutscene([[ Append("55:[E|Neutral] Christine, please report to your normal work assignments.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] I downloaded what I could from the core and set up a back door into the network.[P] I will review what footage I can.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] Are you...[P] okay?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] ...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Report to your normal work assignments.[P] I will contact you when I have determined our next move.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] 55, come on...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Do not waste processor cycles worrying.[P] Focus on maintaining your cover.[P] Move out.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] ...") ]])
            fnCutsceneBlocker()
            
            --Remove 55 from the following group.
            giFollowersTotal = 0
            gsaFollowerNames = {}
            giaFollowerIDs = {0}
            --AdvCombat_SetProperty("Party Slot", 1, "Null")
            AL_SetProperty("Unfollow Actor Name", "Tiffany")

            --Flag to indicate she is not following Christine.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 0.0)
            
            --Flag for the next part of the cutscene.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iSaw55PostLRT", "N", 1.0)
            
            --Change map to Christine's quarters. This will fire the next part of the scenario.
            fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
            fnCutscene([[ AL_BeginTransitionTo("RegulusCityZ", "FORCEPOS:8.0x10.0x0") ]])
        
        --Normal:
        else
            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("55:[VOICE|Tiffany] It is best if I'm not seen on the main floor cameras.[P] I'll be in touch.") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(20)
            fnCutsceneBlocker()
            
            --Remove 55's sprite. She remains in the combat party for equipment reasons.
            giFollowersTotal = 0
            gsaFollowerNames = {}
            giaFollowerIDs = {0}
            --AdvCombat_SetProperty("Party Slot", 1, "Null")
            AL_SetProperty("Unfollow Actor Name", "Tiffany")

            --Flag to indicate she is not following Christine.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 0.0)
        end
	
	end
	
	--Force-facing flag. This will cause Christine to spawn with the correct facing.
	giForceFacing = gci_Face_South
	
	--Transition.
	fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
	if(sObjectName == "MainFloorL") then
		fnCutscene([[ AL_BeginTransitionTo("RegulusCityB", "FORCEPOS:6.5x14.0x0") ]])
	else
		fnCutscene([[ AL_BeginTransitionTo("RegulusCityB", "FORCEPOS:11.5x14.0x0") ]])
	end
	fnCutsceneBlocker()

-- |[Basement Floor 2]|
elseif(sObjectName == "Basement2L" or sObjectName == "Basement2R") then
	
	--Force-facing flag. This will cause Christine to spawn with the correct facing.
	giForceFacing = gci_Face_South
	WD_SetProperty("Hide")
	
	--Transition.
	fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
	if(sObjectName == "Basement2L") then
		fnCutscene([[ AL_BeginTransitionTo("LowerRegulusCityB", "FORCEPOS:27.5x13.0x0") ]])
	else
		fnCutscene([[ AL_BeginTransitionTo("LowerRegulusCityB", "FORCEPOS:31.5x13.0x0") ]])
	end
	fnCutsceneBlocker()

-- |[Debug]|
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
