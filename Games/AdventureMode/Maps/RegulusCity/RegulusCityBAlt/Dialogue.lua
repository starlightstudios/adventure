-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    --Set facing.
    TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[GolemLordA]|
    if(sActorName == "GolemLordA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Lord:[VOICE|GolemLord] Stay back, human![P] I -[P] I have superior strength and I'm not afraid to use it![B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] Yes you are.[P] You're shaking in your R-77 Integrated Footwear Modules![B][C]") ]])
        fnCutscene([[ Append("Lord:[VOICE|GolemLord] Don't belch on me again![P] Eeek!") ]])
        fnCutsceneBlocker()
        
    -- |[GolemLordB]|
    elseif(sActorName == "GolemLordB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Lord:[VOICE|GolemLord] Stop![P] I implore you![B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] Humans never, ever stop shedding![P] I produce over 30,000 skin cells every day, and of course that has to fall off![B][C]") ]])
        fnCutscene([[ Append("Lord:[VOICE|GolemLord] You'll choke the city in fallen skin-cell dust, you brute![P] Have you no shame?") ]])
        fnCutsceneBlocker()
        
    -- |[GolemLordC]|
    elseif(sActorName == "GolemLordC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Lord:[VOICE|GolemLord] You can't talk to me normally.") ]])
        fnCutsceneBlocker()
        
    -- |[GolemA]|
    elseif(sActorName == "GolemA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] You're not getting past me, human![P] And -[P] that's about it, actually.[P] Please leave me alone!") ]])
        
    -- |[GolemB]|
    elseif(sActorName == "GolemB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Eek![P] A human![P] Don't get your greasy skin on my chassis!") ]])
        
    -- |[GolemC]|
    elseif(sActorName == "GolemC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Hey, human, what's it like being mostly water?[P] Do you hear yourself sloshing when you move?") ]])
        
    -- |[GolemD]|
    elseif(sActorName == "GolemD") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] You won't get past us![P] Even your primitive ape-strength won't move 85 kilograms of metal woman!") ]])
        
    -- |[GolemE]|
    elseif(sActorName == "GolemE") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Don't make us hurt you, human.[P] Seriously.[P] We really don't want to fight.") ]])
        
    -- |[GolemF]|
    elseif(sActorName == "GolemF") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Well, stopping a rogue human sure beats fabricator duty.[P] You keep terrorizing, human.") ]])
        
    -- |[GolemG]|
    elseif(sActorName == "GolemG") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Oh my goodness, this is just like my video game:: Human Wave![P] Do you shoot slow-moving fireballs I have to dodge, too?") ]])
        
    -- |[GolemH]|
    elseif(sActorName == "GolemH") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] You probably can't harm us with your meat-fists, so I'll just stand here until you get tired and fall asleep.") ]])
        
    -- |[GolemI]|
    elseif(sActorName == "GolemI") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] I'm going to just stand here and solve math problems that your organic brain could never fathom.[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] Curses![P] The only advantage organic computation had over synthetic was the limits of digital computing![B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] Once quantum and analog computing became advanced enough, we organics were totally obsoleted![B][C]") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Uhhhh, yeah.[P] You're right.[P] So...[P] come quietly, please?") ]])
        
    -- |[GolemJ]|
    elseif(sActorName == "GolemJ") then
        
    -- |[GolemK]|
    elseif(sActorName == "GolemK") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Did you escape from the abductions teams or something?[P] How did you get here?[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] My organic memories are incomplete and easily misled![P] I can barely remember what I did ten minutes ago![B][C]") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Wow, too bad.[P] I can remember every moment of my life post-conversion with total accuracy.") ]])
    end
end
