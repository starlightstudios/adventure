-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    --Set facing.
    TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[Golem119BA]|
    if(sActorName == "Golem119BA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Shoes and dresses![P] Completely unique, no matter what anyone else says![P] Really, we mean it!") ]])
        fnCutsceneBlocker()
        
    -- |[Golem119BB]|
    elseif(sActorName == "Golem119BB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Shoes and dresses![P] Completely unique, no matter what anyone else says!") ]])
        fnCutsceneBlocker()
        
    -- |[Golem119BC]|
    elseif(sActorName == "Golem119BC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Flooring and special footwear here![P] Bring a special touch to your quarters or work area!") ]])
        fnCutsceneBlocker()
        
    -- |[Golem119BD]|
    elseif(sActorName == "Golem119BD") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Custom made dolls![P] Handwoven by the talented Slave Units in Sector 256's fabrication bays!") ]])
        fnCutsceneBlocker()
        
    -- |[Golem119BE]|
    elseif(sActorName == "Golem119BE") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Hardware, appliances here![P] Please let us get you the finest in convenience items!") ]])
        fnCutsceneBlocker()
        
    -- |[Golem119BF]|
    elseif(sActorName == "Golem119BF") then
        
        --Variables.
        local iSpokeToGolemStoreGolem= VM_GetVar("Root/Variables/Chapter5/Scenes/iSpokeToGolemStoreGolem", "N")
        if(iSpokeToGolemStoreGolem == 0.0) then
            
            --Flags.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iSpokeToGolemStoreGolem", "N", 1.0)
            
            --Dialogue.
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] Lord Unit?[P] May I -[P] help you?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Just browsing, thank you.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Though, is it my optical sensors malfunctioning, or is your inventory composed of items for Slave Units?[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] Of course it is, Lord Unit.[P] This is our shop![B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] Run by Slave Units, for Slave Units![B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] ...[P] My Lord Unit allows me to run the store because she thinks it's amusing.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Happy] But you get your own shop![P] That's wonderful![B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Happy] Do you get a lot of customers?[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] In the shop itself, no.[P] But we do fill orders sent to us from other sectors.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] But if your Lord Unit ordered the store shut down...[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] I'd shut it down, of course.[P] I would never disobey my Lord Unit.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] *This area is probably monitored more heavily than most, Sophie.[P] Be careful what you say.*[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Offended] *Always.*[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Happy] That's great![P] I'm sure your Lord Unit knows what's best.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] I think it's great.[P] Slave Units can spend their spare work credits and partake in the highest purpose in life![B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] Which...[P] is?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Laugh] Looking great, of course![B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] O-[P]of course, Lord Unit.[P] Were you interested in purchasing something for your subordinate?[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] *Or just taking our stock because we can't stop you...*[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Anything catch your eye, Unit 499323?[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] No thank you, Lord Unit.[P] Thank you for taking an interest, Lord Unit.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Oh, all right.[P] Thank you for your time, service unit.") ]])
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] Thank you for your kind words, Lord Unit.[P] If you would like to purchase anything for a subordinate, we do have an entry on the network.[B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] We usually ship orders within two minutes of receiving them, so don't be shy![B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] Wow, two full minutes?[P] How inefficient![B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] It's because Slave Units don't get shipping priority on the transit network, Lord Unit.[P] I apologize.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Ah, I see.[P] Very good, then.") ]])
        end
           
    -- |[Golem119BG]| 
    elseif(sActorName == "Golem119BG") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Standard loadouts and equipment here![P] We replace malformed S-15 apparel as well!") ]])
        fnCutsceneBlocker()
            
    -- |[Golem119BH]|
    elseif(sActorName == "Golem119BH") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Hair and nails![P] New styles![P] Look great for the upcoming gala!") ]])
        fnCutsceneBlocker()
            
    -- |[Golem119BI]|
    elseif(sActorName == "Golem119BI") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Designer shoes and boots here![P] Superb pedal module coverings![P] Unparalleled comfort!") ]])
        fnCutsceneBlocker()
            
    -- |[Golem119BA]|
    elseif(sActorName == "Golem119BJ") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Only the finest and fanciest here![P] Superior materials make superior clothing!") ]])
        fnCutsceneBlocker()
            
    -- |[Golem119BK]|
    elseif(sActorName == "Golem119BK") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] ChemFuel needs to be synthesized on the spot, it can't be stored because it eats through plasteel containers in under an hour.") ]])
        fnCutsceneBlocker()
            
    -- |[Golem119BL]|
    elseif(sActorName == "Golem119BL") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Unit 99182 entered defragmentation after 62 consecutive hours of service.[P] She's not being lazy, Lord Unit.") ]])
        fnCutsceneBlocker()
            
    -- |[Golem119BM]|
    elseif(sActorName == "Golem119BM") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Grrr, how is it I always seem to get the heaviest crates to deliver?[P] What's in this one, gold?[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] ...[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] Yes, this crate contains gold bricks.[P] Look at the shipping manifest.[P] It's flavouring for the ChemFuel shop.[B][C]") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Oh.[B][C]") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Well it's still really heavy![P] Grragh!") ]])
        fnCutsceneBlocker()
            
    -- |[Golem119BN]|
    elseif(sActorName == "Golem119BN") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] My Lord Unit brings me here all the time and just sits and talks with me.[P] I'm not sure what to make of it.") ]])
        fnCutsceneBlocker()
            
    -- |[Golem119BO]|
    elseif(sActorName == "Golem119BO") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] I apologize for being on the sales floor, Lord Unit.[P] I'm merely dressing these mannequins and will be off before you know it.[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] You don't have to apologize.[P] It's fine.[B][C]") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] We're not supposed to be seen on the sales floor without authorization, Lord Unit.[P] It's my fault.[B][C]") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] I should have had these mannequins prepared in the back earlier but I'm behind on my tasks.[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] Don't get your cables in a twist over it.[P] Your hard work is appreciated.") ]])
        fnCutsceneBlocker()
           
    -- |[Golem119ALA]| 
    elseif(sActorName == "Golem119ALA") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Lord:[VOICE|Golem] Oh, that videograph was positively scandalous![P] Can you imagine it -[P] a Slave Unit and Lord Unit -[P] in a tryst!") ]])
        fnCutsceneBlocker()
            
    -- |[Golem119ALB]|
    elseif(sActorName == "Golem119ALB") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Lord:[VOICE|Golem] My tandem unit simply can't appreciate the more risque types of erotica.[B][C]") ]])
        fnCutscene([[ Append("Lord:[VOICE|Golem] After all, it's mere escapism.[P] It's not real.[P] They're actors.[P] It's erotic precisely because it's forbidden!") ]])
        fnCutsceneBlocker()
            
    -- |[Golem119ALC]|
    elseif(sActorName == "Golem119ALC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Lord:[VOICE|Golem] That rotten Unit 208933 has been spying on my store and copying my techniques, I just know it!") ]])
        fnCutsceneBlocker()
            
    -- |[Golem119ALD]|
    elseif(sActorName == "Golem119ALD") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Lord:[VOICE|Golem] I'm convinced that Unit 339802 is copying my techniques by spying on my store![P] Don't you agree?") ]])
        fnCutsceneBlocker()
            
    -- |[Golem119ALE]|
    elseif(sActorName == "Golem119ALE") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Lord:[VOICE|Golem] Many Lord Units don't appreciate a fine rug and just how they tie the room together.") ]])
        fnCutsceneBlocker()
            
    -- |[Golem119ALF]|
    elseif(sActorName == "Golem119ALF") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Lord:[VOICE|Golem] Normally we're the busiest store in the sector, but due to the upcoming gala...[P] well, let's just say my Slave Units are appreciating the time off.") ]])
        fnCutsceneBlocker()
          
    -- |[Golem119ALG]|  
    elseif(sActorName == "Golem119ALG") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Lord:[VOICE|Golem] I can't decide if I should get the squid or the cat or the totally accurate doll -[P] so many choices and they're all great!") ]])
        fnCutsceneBlocker()
            
    -- |[Golem119ALH]|
    elseif(sActorName == "Golem119ALH") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Lord:[VOICE|Golem] Humans may be slow, boorish, and crude, but they are incredibly cute when in doll form.[P] So they have that going for them.") ]])
        fnCutsceneBlocker()
           
    -- |[Golem119ALI]| 
    elseif(sActorName == "Golem119ALI") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Lord:[VOICE|Golem] My tandem unit is stressing herself about which doll to get me for my conversion anniversary.[P] Honestly, I just want to be with her.[P] The doll doesn't matter.") ]])
        fnCutsceneBlocker()
            
    -- |[Golem119ALJ]|
    elseif(sActorName == "Golem119ALJ") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Lord:[VOICE|Golem] We have replacements for all standard-issue gear.[P] You'll look just like the day you were converted!") ]])
        fnCutsceneBlocker()
            
    -- |[Golem119ALK]|
    elseif(sActorName == "Golem119ALK") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
        fnCutscene([[ Append("Lord:[E|Neutral] My goodness -[P] your Slave Unit![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] She has my explicit permission to grow her hair out, thank you.[B][C]") ]])
        fnCutscene([[ Append("Lord:[E|Neutral] And what hair it is![P] The curl, the bangs, oh my![B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Sad] You're not upset?[B][C]") ]])
        fnCutscene([[ Append("Lord:[E|Neutral] Honey, if your Lord Unit wasn't literally holding your hand I'd ask you to be transferred to my department.[B][C]") ]])
        fnCutscene([[ Append("Lord:[E|Neutral] What product do you use?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] Uh, the molten metal shavings that fly off when I'm spot-welding a stress fracture?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] We're repair units.[B][C]") ]])
        fnCutscene([[ Append("Lord:[E|Neutral] If you ever want a change of career, just send me a message, darling.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Blush] Th-[P]thank you, Lord Unit![P] Thank you very much!") ]])
        fnCutsceneBlocker()
            
    -- |[Golem119ALL]|
    elseif(sActorName == "Golem119ALL") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Lord:[VOICE|Golem] I think I got the best assignment of my synthetic life.[P] Shoe salesrobot![B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] Why would you say that's the best assignment?[B][C]") ]])
        fnCutscene([[ Append("Lord:[VOICE|Golem] My raging foot fetish, of course.[B][C]") ]])
        fnCutscene([[ Append("Lord:[VOICE|Golem] ...[P] Oh don't look at me like that.[P] It's not weird.") ]])
        fnCutsceneBlocker()
            
    -- |[Golem119ALM]|
    elseif(sActorName == "Golem119ALM") then
        
        --Variables.
        local iStartedShoppingSequence = VM_GetVar("Root/Variables/Chapter5/Scenes/iStartedShoppingSequence", "N")
        if(iStartedShoppingSequence == 1.0) then
        
            --Flag.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iStartedShoppingSequence", "N", 2.0)
        
            --Dialogue.
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLordC", "Neutral") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] Slave Unit, what are you doing on the floor without permission?[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Sad] I'm...[P] I'm...[P] not without...[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Sad] I apologize, Lord Unit.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] What are you apologizing for?[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] I assume you are making a delivery.[P] Were you not aware of the rule that Slave Units are not to be on the sales floor without authorization?[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Sad] I was aware, Lord Unit.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Angry] Hey![B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] I am not ignoring you, Lord Unit.[P] I merely maintain strict discipline in my store.[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] I presume that you do the same in your department.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Sad] I am sorry, Lord Unit.[P] Please punish me however you like.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] What's gotten into you, Sophie?[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Sad] ...[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] Sophie?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] You didn't even ask her name or purpose before skipping right to the punishment?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] This Slave Unit is my -[P] my property.[P] She works in my department.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] She is here on my orders.[P] I will not tolerate you punishing my subordinates.[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] Ah.[P] I see.[P] I did not intend for that, Lord Unit.[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] It is very unusual to see a non-staff Slave Unit here except on deliveries, and they are to deliver to the back rooms only.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] She's here to -[P] carry my purchases.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] Obviously I would not deign to do such simple labour.[P] You understand.[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] I do.[P] But aren't you worried she may damage or stain your items, though?[P] Slave Units are very clumsy.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Certainly, but this one is less clumsy than most.[P] Plus, I am perfectly capable of punishing her mistakes myself.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] In addition, we are not here for pleasure, but business.[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] You are?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] I am Lord Golem of Maintenance and Repair, Sector 96.[P] Due to the fabricators in our sector being backlogged, it falls to us to do repairs on clothing.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] We've run out of fabrics and I can't requisition any, and I have a dozen dresses that need patches and alterations for the upcoming gala.[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] A-[P]A true tragedy![P] I myself have had difficulty accessing the fabricators.[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] How can I help you?[P] Perhaps some of my spare stock?[P] I believe I have some leftover fabric in the back...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Laugh] (Oh wow, she bought it!)[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] My Slave Unit here has a list of what we need on her PDU.[P] Give her anything she requests.[P] I have more important things to attend to.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Send the charges to my department and I'll give you whatever I can spare in my budget.[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] Oh no, I couldn't take work credits for this![B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] Can you imagine...[P] arriving to the gala with a hole in your ensemble?[P] It's -[P] too awful to countenance.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] I intend to make it an impossibility.[P] Thank you, Lord Unit.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] *Order anything you think you'll need with my PDU, Sophie.[P] Are you all right?*[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] *Yes, I am but -[P] I just -[P] I...*[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] *Let's talk about it later.[P] Just order what you need and let's get out of here as quick as we can.*[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] *...*[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] *...*[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] *Okay, that should do it.*[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] *Let's head back to Sector 96.[P] C'mon.*") ]])
            fnCutsceneBlocker()
        
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLordC", "Neutral") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] I just want to thank you for your vital repair services, Lord Unit.[B][C]") ]])
            fnCutscene([[ Append("Lord:[E|Neutral] It's truly a travesty that the administrators have prioritized firearms above fashion.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] We live in dark times indeed...") ]])
            fnCutsceneBlocker()
        
        end
            
    -- |[Golem119ALN]|
    elseif(sActorName == "Golem119ALN") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Lord:[VOICE|Golem] Sigh...[P] Another year, another gala, and again I go without a plus one...") ]])
        fnCutsceneBlocker()
            
    -- |[Golem119ALO]|
    elseif(sActorName == "Golem119ALO") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Lord:[VOICE|Golem] Have you ever heard of 'coffee'?[P] Apparently it's like ChemFuel, but even more addictive.[P] Not sure if I'm ready for that.") ]])
        fnCutsceneBlocker()
            
    -- |[Golem119ALP]|
    elseif(sActorName == "Golem119ALP") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Lord:[VOICE|Golem] My Slave Unit is the smartest and prettiest of the Slave Units...[P] Can you keep a secret, Lord Unit?[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] I believe so.[B][C]") ]])
        fnCutscene([[ Append("Lord:[VOICE|Golem] Sometimes I wish I had been converted to a Slave Unit all those years ago, just so I could be with her.[B][C]") ]])
        fnCutscene([[ Append("Lord:[VOICE|Golem] But this love is forbidden, and I would never tell her how I feel.[P] I don't want to hurt her...[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] Your secret is safe with me, but...[P] you should tell her.[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] You can be together in secret.[P] It can be done.[B][C]") ]])
        fnCutscene([[ Append("Lord:[VOICE|Golem] Do you think so?[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] I know so.[P] Follow your heart.[P] Or...[P] power core.") ]])
        fnCutsceneBlocker()
            
    -- |[Golem119ALQ]|
    elseif(sActorName == "Golem119ALQ") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Lord:[VOICE|Golem] These USBees are for sale as pets, but we don't have enough in the biolabs for our pollination efforts.[P] Maybe I should requisition some?") ]])
        fnCutsceneBlocker()
            
    -- |[Golem119ALR]|
    elseif(sActorName == "Golem119ALR") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Lord:[VOICE|Golem] Welcome to my store, the only pet shop on Regulus![P] And if you have an organic pet, we also do Raiju collaring and grooming!") ]])
        fnCutsceneBlocker()
            
    -- |[Golem119ALS]|
    elseif(sActorName == "Golem119ALS") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Lord:[VOICE|Golem] I don't know what to get...[P] My Slave Units will probably just trip over a motilvac, but they're so cute!") ]])
        fnCutsceneBlocker()
            
    -- |[Golem119ALT]|
    elseif(sActorName == "Golem119ALT") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Lord:[VOICE|Golem] I'd love to have a human as a pet, but apparently that's frowned upon.[P] The administrators would just order her converted and used for labour...") ]])
        fnCutsceneBlocker()
            
    -- |[CafeGolemA]|
    elseif(sActorName == "CafeGolemA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Lord:[VOICE|Golem] Welcome to Chez Fuel, may I take your order?[P] Piping hot ChemFuel in seconds, with hundreds of flavours available!") ]])
        fnCutsceneBlocker()
            
    -- |[CafeGolemB]|
    elseif(sActorName == "CafeGolemB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Lord:[VOICE|Golem] May I get you some chewable tires?[P] Or perhaps salted bolts?[P] Very crunchy!") ]])
        fnCutsceneBlocker()
    
    -- |[Friend Golem]|
    elseif(sActorName == "FriendGolem") then
    
        --Variables
        TA_SetProperty("Face Character", "PlayerEntity")
        local iMetFriendGolem = VM_GetVar("Root/Variables/Chapter5/Scenes/iMetFriendGolem", "N")
        if(iMetFriendGolem == 0.0) then
            VM_SetVar("Root/Variables/Chapter5/Scenes/iMetFriendGolem", "N", 1.0)
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] L-L-L[P]-L-L[P]-LORD GOLEM.[P] HELLO![B][C]") ]])
            fnCutscene([[ Append("Golem:[E|Neutral] H-[P]How are y-y-[P]you!?[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] Woah, calm down, Linda![B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] You know this unit?[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Smirk] Christine, meet Linda.[P] She works in the habitation domes.[B][C]") ]])
            fnCutscene([[ Append("Linda:[E|Neutral] E-[P]everything is fine, Lord Unit![B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Smirk] Linda, this is Christine.[P] You know, my *tandem unit*.[P] She's chill.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Outrageously chill?[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Smirk] Word![B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Smirk] (I don't know why Christine told me to say those things, but she smiles whenever I do...)[B][C]") ]])
            fnCutscene([[ Append("Linda:[E|Neutral] How chill is chill?[P] Because I need someone to be extremely - [P][CLEAR]") ]])
            fnCutscene([[ Append("Something: [SOUND|World|Chirp]*Chirp*[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Chirp?[B][C]") ]])
            fnCutscene([[ Append("Linda:[E|Neutral] Ha ha![P] What a funny noise![P] My -[P] linguistic routines just thought I should - [P][CLEAR]") ]])
            fnCutscene([[ Append("Something: [SOUND|World|Chirp]*Chirp*[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] We're maintenance robots, you know.[P] Do you need a checkup?[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Smirk] Linda, Christine is not going to punish you.[B][C]") ]])
            fnCutscene([[ Append("Linda:[E|Neutral] Sophie...[P] promise you won't be mad but...[P] Here...[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] A bird?[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Smirk] It's pretty cute, but are you allowed to have organic pets?[P] Especially here?[B][C]") ]])
            fnCutscene([[ Append("Linda:[E|Neutral] It's not a pet![P] Argh![P] I'm so doomed![B][C]") ]])
            fnCutscene([[ Append("Linda:[E|Neutral] Lord Unit Christine, I know what I did was wrong, but please listen to me?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] (I don't think she's even capable of acknowledging me properly...)[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] Slave Unit, explain your current activities.[B][C]") ]])
            fnCutscene([[ Append("Linda:[E|Neutral] Okay, okay, okay...[B][C]") ]])
            fnCutscene([[ Append("Linda:[E|Neutral] I was in the habitats, doing what I'm supposed to do.[P] I was very productive.[P] I was clearing away branches from the power conduction lines.[B][C]") ]])
            fnCutscene([[ Append("Linda:[E|Neutral] And I saw this little fella.[P] He's marked as Pest Control Vector #449231.[B][C]") ]])
            fnCutscene([[ Append("Linda:[E|Neutral] I think he flew into something because his wing was broken and he was just flopping around on the ground and I couldn't just *leave* him![B][C]") ]])
            fnCutscene([[ Append("Linda:[E|Neutral] So I kinda birdsnatched him and used some repair nanites to fix his wing...[P] And now I'm here...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Are any nanites appropriated for maintaining organic pest control vectors?[B][C]") ]])
            fnCutscene([[ Append("Linda:[E|Neutral] No![P] And if I put him back in the habitat my Lord Unit will find out and will probably order this little guy recycled![B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Okay, so I think I see the problem.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Sophie, time to work my Lord Unit magic.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|PDU] PDU, please send a message to Linda's Lord Unit.[B][C]") ]])
            fnCutscene([[ Append("Linda:[E|Neutral] *Oh no!*[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|PDU] I apprehended this Slave Unit attempting to smuggle contraband out of the habitat and have given her a severe reprimand.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|PDU] As punishment for her activities, she is to spend one day per week working in my repair facilities in Sector 96 until I so deem her debt to society repaid.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|PDU] I will provide a budgetary recompense if requested.[P] Our repair bay has spare work credits allocated to it.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|PDU] I will be happy to discuss the matter tomorrow in person if required.[P] This Slave Unit will be spending it in my repair bay.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|PDU] Your's cordially, Unit 771852.[P] PDU, send message.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Slave Unit, your assignment tomorrow will be to construct a proper habitat for this organic pest control vector.[P] We have some schematics for cages.[P] I'll also need to obtain some food supplies.[B][C]") ]])
            fnCutscene([[ Append("Linda:[E|Neutral] W-[P]what?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Look at his little wing.[P] The nanites fixed the tissue but he doesn't know how to use it yet.[P] See how he's fluttering?[B][C]") ]])
            fnCutscene([[ Append("Linda:[E|Neutral] Yeah, I thought maybe they didn't work...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] He'll need help training to use his wing again.[P] It may take some time.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] So you're going to make a habitat for him and come by once a week to visit.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Whenever he's ready, you're going to reintroduce him to the habitation domes.[P] I'll fudge the paperwork for you, it's not hard with the programs I have.[B][C]") ]])
            fnCutscene([[ Append("Linda:[E|Neutral] But what if...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] I'm sorry, Linda.[P] I wish there was a way I could do this without it being by fiat.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] You clearly know more about organic habitat management than I do, and I trust your judgement.[P] But our city has decided Lord Units like me count for more.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] I have to fix this now this way, but someday I won't have to.[P] Do you follow me?[B][C]") ]])
            fnCutscene([[ Append("Linda:[E|Neutral] ...[P] I think so?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Good.[P] Deliver this little guy to the Sector 96 Repair Bay in an hour and we'll find a spot for him.[P] Don't let anyone see him.[B][C]") ]])
            fnCutscene([[ Append("Something: [SOUND|World|Chirp]*Chirp*[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Laugh] Or hear him, in this case![B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Smirk] I told you she was the best.[B][C]") ]])
            fnCutscene([[ Append("Linda:[E|Neutral] But what if someone sees me...[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Smirk] The Lord Units in this sector never come to the back, right?[P] Just use the delivery trams.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Smirk] Put the little guy in a box and take the tram right to our repair bay.[P] You can use our basement![B][C]") ]])
            fnCutscene([[ Append("Linda:[E|Neutral] Okay.[P] Uh, thanks, Christine.[B][C]") ]])
            fnCutscene([[ Append("Linda:[E|Neutral] And I'm sure little Herbert here thanks you, too.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Sophie, please welcome our newest repair unit, Herbert![B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Smirk] Looking forward to working with both of you.") ]])
            fnCutsceneBlocker()
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
            fnCutscene([[ Append("Linda:[E|Neutral] I'll deliver little Herbert to the Sector 96 repair bay in an hour, like you said, Christine.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Smirk] See you tomorrow, Linda![P] Making a habitat for him will be so fun!") ]])
            fnCutsceneBlocker()
        end
    end
end
