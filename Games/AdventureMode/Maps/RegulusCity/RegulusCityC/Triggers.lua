-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
if(sObjectName == "WarpActivate") then
    fnWarpUnlockHandler("Root/Variables/Chapter5/Campfires/iRegulusCityC", 7.25, 11.50)
    
--Triggers a cutscene the first time Christine enters the maintenance bay.
elseif(sObjectName == "EnterBay") then

	--Variables.
	local iIsOnDate                 = VM_GetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N")
	local iTalkedToSophie           = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N")
	local iSophieWillSynchronizeNow = VM_GetVar("Root/Variables/Chapter5/Sophie/iSophieWillSynchronizeNow", "N")
	local iSophieImportantMeeting   = VM_GetVar("Root/Variables/Chapter5/Scenes/iSophieImportantMeeting", "N")
	local iSophieFirstDateState     = VM_GetVar("Root/Variables/Chapter5/Scenes/iSophieFirstDateState", "N")
    local iStartedShoppingSequence  = VM_GetVar("Root/Variables/Chapter5/Scenes/iStartedShoppingSequence", "N")
        
    --Achievement.
    AM_SetPropertyJournal("Unlock Achievement", "MeetSophie")
	
	--Important plot cutscene.
	if(iSophieImportantMeeting == 1.0) then
	
		LM_ExecuteScript(gsRoot .. "Chapter 5/Scenes/500 Normal/Post Tiffany Meeting/Scene_RepairBay.lua")
    
    --Post shopping sequence.
    elseif(iStartedShoppingSequence == 3.0) then
		
        --Flags.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iStartedShoppingSequence", "N", 4.0)
        VM_SetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N", 0.0)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iMainQuestBlock", "N", 23.0)
        
        --Set this flag so Herbert and Linda will spawn next time you enter.
        local iMetFriendGolem = VM_GetVar("Root/Variables/Chapter5/Scenes/iMetFriendGolem", "N")
        if(iMetFriendGolem == 1.0) then
            VM_SetVar("Root/Variables/Chapter5/Scenes/iCanSpawnBirdcage", "N", 1.0)
        end
        
        --Movement.
        fnCutsceneMove("Christine", 25.25, 13.50)
        fnCutsceneMove("Sophie", 25.25, 12.50)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneFace("Christine", 0, -1)
        fnCutsceneFace("Sophie", 0, 1)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Ah, home sweet repair bay.[P] How I missed the scent of grease and burnt metal.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Happy] So, with your permission, Lord Unit, I'll be getting to work on those new dresses as soon as the materials are delivered![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Dresses.[P] Plural.[P] You've decided to go?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Happy] I had a good idea.[P] You'll see.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] I'm not going to let some stuffy Lord Unit rattle me.[P] I'm going to march right into that gala like I own the place.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Happy] That's the spirit![B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Blush] B-[P]b-[P]b-[P]but I'll need you to hold my hand...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] As if I needed a reason.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|PDU] Oh, 55 sent me a message.[P] She's in the basement.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Best not keep her waiting.[P] She'll be short with me.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] Tell her I said hi![B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] Hmmm, going to need to borrow a binding gun...[P] Maybe Unit 763328 will let me borrow the one from her fabrication bench...") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneMove("Sophie", 22.25, 12.50)
        fnCutsceneFace("Sophie", 0, -1)
        fnCutsceneBlocker()
        
        --Activate Sophie's collision flag and whatnot.
        giFollowersTotal = 0
        gsaFollowerNames = {}
        giaFollowerIDs = {0}
        AL_SetProperty("Unfollow Actor Name", "Sophie")
        
        --Set Sophie's properties.
        EM_PushEntity("Sophie")
            TA_SetProperty("Clipping Flag", true)
            TA_SetProperty("Activation Script", fnResolvePath() .. "Dialogue.lua")
        DL_PopActiveObject()
    
    --Go talk to 55 first.
    elseif(iStartedShoppingSequence == 4.0) then
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] (I really shouldn't keep 55 waiting...)") ]])
        fnCutsceneBlocker()
        fnCutsceneMove("Christine", 25.25, 13.50)
	
	--Play the scene.
	elseif(iTalkedToSophie == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N", 1.0)
		
		--Run the rest of the scene.
		LM_ExecuteScript(gsRoot .. "Chapter 5/Scenes/500 Normal/Sophie Meeting/Scene_Begin.lua")
	
	--On a date with Sophie that is over.
	elseif(iIsOnDate == 2.0 and iSophieWillSynchronizeNow == 0.0) then
	
		--If this is the first date, don't play this scene. The player needs to go to Christine's quarters instead.
		if(iSophieFirstDateState == 3.0) then
	
			--Flags.
			VM_SetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N", 3.0)
		
			--Dialogue.
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
			fnCutscene([[ Append("Sophie:[E|Neutral] ...[P] I had a lot of fun today, Christine.[B][C]") ]])
			fnCutscene([[ Append("Sophie:[E|Neutral] I have a few things to finish up in here before I head back to my quarters.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] All right.[P] Don't stay up too late![B][C]") ]])
			fnCutscene([[ Append("Sophie:[E|Neutral] Good night, 771852.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Good night, 499323.") ]])
			fnCutsceneBlocker()
		
			--Move Sophie to the repair area.
			fnCutsceneMove("Sophie", 22.25, 12.50)
			fnCutsceneBlocker()
			fnCutsceneFace("Sophie", 0, -1)
			fnCutsceneBlocker()
			
			--Remove Sophie from the party.
			giFollowersTotal = 0
			gsaFollowerNames = {}
			giaFollowerIDs = {0}
			AL_SetProperty("Unfollow Actor Name", "Sophie")
			
			--Set Sophie's properties.
			EM_PushEntity("Sophie")
				TA_SetProperty("Clipping Flag", true)
                TA_SetProperty("Activation Script", fnResolvePath() .. "Dialogue.lua")
			DL_PopActiveObject()
		end
	end

--55 leaves the group if you warped in.
elseif(sObjectName == "55Leaves") then

	--Variables.
	local iIs55Following  = VM_GetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N")
    local iSaw55sMemories = VM_GetVar("Root/Variables/Chapter5/Scenes/iSaw55sMemories", "N")
    local iSaw55PostLRT   = VM_GetVar("Root/Variables/Chapter5/Scenes/iSaw55PostLRT", "N")
    
    --If Christine is in Eldritch form, Sophie looks north if she's in the area.
    local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
    if(sChristineForm == "Eldritch" and EM_Exists("Sophie") == true) then
        EM_PushEntity("Sophie")
            TA_SetProperty("Facing", gci_Face_North)
        DL_PopActiveObject()
    end
	
	--If 55 is following, she mentions she's leaving here.
	if(iIs55Following == 1.0) then
        
        --Post LRT scene:
        if(iSaw55sMemories == 1.0 and iSaw55PostLRT == 0.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
            fnCutscene([[ Append("55:[E|Neutral] Christine, please report to your normal work assignments.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] I downloaded what I could from the core and set up a back door into the network.[P] I will review what footage I can.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] Are you...[P] okay?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] ...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Report to your normal work assignments.[P] I will contact you when I have determined our next move.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] 55, come on...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Do not waste processor cycles worrying.[P] Focus on maintaining your cover.[P] Move out.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] ...") ]])
            fnCutsceneBlocker()
            
            --55 moves to the ladder and vanishes.
            fnCutsceneMove("Tiffany", 11.25, 14.50)
            fnCutsceneBlocker()
            fnCutsceneFace("Tiffany", 0, 1)
            fnCutsceneBlocker()
            fnCutsceneWait(20)
            fnCutsceneBlocker()
            fnCutsceneTeleport("Tiffany", -100.25, -100.50)
            fnCutsceneBlocker()
            
            --Remove 55 from the following group.
            giFollowersTotal = 0
            gsaFollowerNames = {}
            giaFollowerIDs = {0}
            --AdvCombat_SetProperty("Party Slot", 1, "Null")
            AL_SetProperty("Unfollow Actor Name", "Tiffany")

            --Flag to indicate she is not following Christine.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 0.0)
            
            --Flag for the next part of the cutscene.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iSaw55PostLRT", "N", 1.0)
            
            --Change map to Christine's quarters. This will fire the next part of the scenario.
            fnCutscene([[ AL_BeginTransitionTo("RegulusCityZ", "FORCEPOS:8.0x10.0x0") ]])
            VM_SetVar("Root/Variables/Chapter5/Scenes/iIsTramHere", "N", 0.0)
        
        --Normal:
        else
		
            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("55:[VOICE|Tiffany] If you're going in to the city, it's best we're not seen together.[P] I'll be in touch.") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(20)
            fnCutsceneBlocker()
            
            --55 moves to the ladder and vanishes.
            fnCutsceneMove("Tiffany", 11.25, 14.50)
            fnCutsceneBlocker()
            fnCutsceneFace("Tiffany", 0, 1)
            fnCutsceneBlocker()
            fnCutsceneWait(20)
            fnCutsceneBlocker()
            fnCutsceneTeleport("Tiffany", -100.25, -100.50)
            fnCutsceneBlocker()
            
            --Remove 55's sprite. She remains in the combat party for equipment reasons.
            giFollowersTotal = 0
            gsaFollowerNames = {}
            giaFollowerIDs = {0}
            --AdvCombat_SetProperty("Party Slot", 1, "Null")
            AL_SetProperty("Unfollow Actor Name", "Tiffany")

            --Flag to indicate she is not following Christine.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 0.0)
        end
	end

--Christine must maintain her cover as a lord golem, and/or 55 leaves the group.
elseif(sObjectName == "MaintainCover") then

	--Variables.
	local sChristineForm     = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
    local i254SophieLeading  = VM_GetVar("Root/Variables/Chapter5/Scenes/i254SophieLeading", "N")
    local i254FixedChristine = VM_GetVar("Root/Variables/Chapter5/Scenes/i254FixedChristine", "N")
	
    --Secrebot sequence.
    if(i254SophieLeading == 1.0 and i254FixedChristine == 0.0) then
		LM_ExecuteScript(fnResolvePath() .. "Secrebot Scene.lua")
        return
    end
        
	--Christine needs to change form.
	if(sChristineForm ~= "Golem") then
	
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[VOICE|Christine](Better transform to a Lord Unit.[P] Wouldn't want to draw unnecessary attention.)") ]])
		fnCutsceneBlocker()
	
		--Flashwhite.
		Cutscene_CreateEvent("Flash Christine White", "Actor")
			ActorEvent_SetProperty("Subject Name", "Christine")
			ActorEvent_SetProperty("Flashwhite Quickly", "Null")
		DL_PopActiveObject()
		fnCutsceneBlocker()

		fnCutsceneWait(75)
		fnCutsceneBlocker()
		fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua") ]])
		fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
		fnCutsceneBlocker()
	end

--Cutscene, part of the mines finale.
elseif(sObjectName == "SXCutsceneTrigger") then

	--Variables.
	local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
	if(iSXUpgradeQuest ~= 3.0) then return end

	--Execute.
	LM_ExecuteScript(gsRoot .. "Chapter 5/Scenes/500 Normal/Sprocket City Finale/Scene_A_RegulusCityC.lua")

--Cutscene, plays after the Human Special Date.
elseif(sObjectName == "PostHumanScene") then

    --Flags.
    local iHumanThirdScene = VM_GetVar("Root/Variables/Chapter5/Sophie/iHumanThirdScene", "N")
    local iHumanRepeat = VM_GetVar("Root/Variables/Chapter5/Sophie/iHumanRepeat", "N")
    if(iHumanThirdScene ~= 1.0) then return end
    
    --Set.
    VM_SetVar("Root/Variables/Chapter5/Sophie/iHumanThirdScene", "N", 2.0)
    
    --Christine switches to golem.
    LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua")
    
    --Black the screen out.
    fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Reposition.
    fnCutsceneTeleport("Christine", 19.25, 15.50)
    fnCutsceneFace("Christine", 0, 1)
    fnCutsceneTeleport("Sophie", 18.26, 16.50)
    fnCutsceneFace("Sophie", 0, -1)
    fnCutsceneBlocker()
    
    --Fade in.
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()

    -- |[First Time]|
    if(iHumanRepeat == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] All right, all the checksums are set.[P] You are back to normal.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Except for one thing, right?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] The new memory you requested?[P] I most surely uploaded it.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Which one was it, out of curiosity?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] It's not obvious?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] The programming makes it feel natural, but you have to search the memory for logical contradictions.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Blush] And I can't find a single contradiction in my love for you...[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] You have a way with words, Christine.[P] Were you a poet before you became a repair unit?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] An English teacher.[P][EMOTION|Christine|Laugh] Which is practically the same thing when you get right down to it![B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] Now I'm a little more worried about how we're going to make this little romp go away.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] It's a common practice in the security services to recycle unit designations.[P] When a unit gets 'disappeared', their designation gets a flag on it.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Our mutual friend has been using them to access things without rousing too much suspicion.[P] There are thousands.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] The unit you just converted?[P] My PDU has already specified she was assigned to the security services and sent to work on the South-Field Station.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] South-Field Station?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] It's the Regulus Secundus project being built on the other side of the moon.[P] They finished laying the track a month ago.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Units assigned to that project will be out of communication for at least two years, and record keeping is terrible.[P] Nobody will suspect a thing.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Blush] Doing all this just to get off on it...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] I know it's selfish of me...[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Blush] N-[P]no![P] I was talking about me![B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Blush] Forging documents, lying to units, pretending to be someone I'm not -[P] I'm like a secret agent![B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Blush] I don't know if you're bad for me, or if I'm bad for you![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Blush] The first one, absolutely.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] Well that is just more than enough excitement for one day, Christine.[P] So, I'll bid you good night.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Of course.[P] Until tomorrow, Sophie.") ]])
        fnCutsceneBlocker()
    
    -- |[Repeats]|
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] And then we stood here, and talked about how we were going to get away with it![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Oh yeah, this part I remember.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Blush] My favourite part was when you dressed me...[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Blush] Hee hee![B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Happy] We've been going over our memory files for hours now! That was a lot of fun![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Blush] Shall we consider this a night out, then?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Happy] We shall! I'll just get the files from earlier today sorted out, and then it's off to defrag for me.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] All right. Good night, Sophie.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] Good night, Christine!") ]])
    
    end
	
    --Flags.
    VM_SetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N", 3.0)

-- |[ ================================= End of Secrebot Quest ================================== ]|
elseif(sObjectName == "Secrebots") then

    -- |[ ======================== Setup ========================= ]|
    -- |[Flag]|
    VM_SetVar("Root/Variables/Chapter5/Scenes/i254Completed", "N", 1.0)
    
    -- |[Spawning]|
    --If Sophie didn't spawn, spawn her.
    if(EM_Exists("Sophie") == false) then
        fnSpecialCharacter("Sophie", 15, 12, gci_Face_West, true, fnResolvePath() .. "Dialogue.lua")
    else
        fnCutsceneTeleport("Sophie", 15.25, 12.50)
    end
    fnCutsceneFace("Sophie", -1, 1)
    
    --Night.
    TA_Create("Night")
        TA_SetProperty("Position", -1, -1)
        TA_SetProperty("Facing", gci_Face_South)
        TA_SetProperty("Clipping Flag", false)
        fnSetCharacterGraphics("Root/Images/Sprites/NightSecrebot/", false)
    DL_PopActiveObject()
    
    -- |[Position Party]|
    fnCutsceneTeleport("Christine", -1.25, -1.50)
    fnCutsceneTeleport("Tiffany", -1.25, -1.50)
    fnCutsceneFace("Christine", 0, -1)
    fnCutsceneFace("Tiffany", 0, -1)
    fnCutsceneFace("Night", 0, -1)
    fnCutsceneCamera("Position", gcfCamera_Cutscene_Speed, 13.25, 13.50)
    fnCutsceneFadeOut()
    fnCutsceneBlocker()

    -- |[ ====================== Execution ======================= ]|
    -- |[Fade In]|
    fnCutsceneFadeIn(45)
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutscenePlaySound("World|ClimbLadder")
    fnCutsceneTeleport("Tiffany", 11.25, 14.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneMove("Tiffany", 12.25, 14.50)
    fnCutsceneFace("Tiffany", -1, 0)
    fnCutsceneMove("Sophie", 11.25, 13.50, 2.50)
    fnCutsceneFace("Sophie", 0, 1)
    fnCutsceneBlocker()
    fnCutscenePlaySound("World|ClimbLadder")
    fnCutsceneTeleport("Night", 11.25, 14.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneMove("Night", 10.25, 14.50)
    fnCutsceneFace("Night", 1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscenePlaySound("World|ClimbLadder")
    fnCutsceneTeleport("Christine", 11.25, 14.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Sophie", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Night", "Secrebot") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] Christine![P] 55![P] You're okay![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] I just want everyone to know that I took that ladder like a champion.[P] You try climbing a ladder when your lower half is a wheel![B][C]") ]])
    fnCutscene([[ Append("CR-1-15:[E|Secrebot] It's not that hard.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] Hello![P] Are you a friend of Christine's?[P] What's your name?[B][C]") ]])
    fnCutscene([[ Append("Night:[E|SecrebotSad] It -[P] was -[P] Night.[P] Though CR-1-15 is okay if you prefer my designation.[B][C]") ]])
    fnCutscene([[ Append("Night:[E|SecrebotSad] I -[P] I'm sorry I got us into this, Christine.[P] She was trying to protect me from the other robots and we both got caught and converted into secrebots.[B][C]") ]])
    fnCutscene([[ Append("Night:[E|SecrebotSad] You can transform yourself but...[B][C]") ]])
    fnCutscene([[ Append("55:[E|Down] Please accept our apologies.[P] You do not deserve to be caught up in all of this.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Sad] It seems Abductions had her planned to be a slave golem, but Unit 8714 decided to use her as bait.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] The Administration uses people's lives as currency.[B][C]") ]])
    fnCutscene([[ Append("Night:[E|SecrebotHappy] I at least seem to be in control of myself now.[P] I guess I'm in with your revolution, aren't I?[P] You're mavericks, right?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] You are not obligated.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Yeah, but we'll take any help we can get.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] We can be free together, Night![P] If anyone can pull it off, it's Christine and 55![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Hey, don't celebrate just yet.[P] This mission didn't exactly go well.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Sad] The security forces are going to round up the secrebots, and I bet Vivify has all kinds of intelligence about the city now.[P] All we did was stem the bleeding.[B][C]") ]])
    fnCutscene([[ Append("Night:[E|SecrebotHappy] B-[P]but![P] I know where all the secrebots are![P] I was collating information for Odess![B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Very good.[P] Night, I will need your help.[P] We can inform the resistance of how to neutralize Vivify's influence.[P] The secrebots will need to go underground but...[B][C]") ]])
    fnCutscene([[ Append("Night:[E|SecrebotHappy] Of course I'll help my secrebot sisters![B][C]") ]])
    fnCutscene([[ Append("Night:[E|SecrebotSad] But...[P] what's going to become of me?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Oh, ha ha ha ha![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] What's so funny?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] You can stay with us in Sector 96, Night![P] I think I can fudge the records well enough that you can be a refurbished first-wave secrebot.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] Remember how all this started, Christine?[P] I asked you to get a secrebot for our department?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Happy] Well this isn't exactly what I had in mind![B][C]") ]])
    fnCutscene([[ Append("Night:[E|SecrebotHappy] I will do my best to serve you![B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] You two handle the paperwork.[P] Night, please follow me.[P] We need to contact the resistance cells quickly, before the security forces arrest the secrebots.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Christine, let me know when you want to move out again.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneMove("Christine", 11.25, 13.50)
    fnCutsceneFace("Christine", 0, 1)
    fnCutsceneMove("Sophie", 11.25, 12.50)
    fnCutsceneFace("Sophie", 0, 1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneMove("Tiffany", 11.25, 14.50)
    fnCutsceneFace("Tiffany", 0, 1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscenePlaySound("World|ClimbLadder")
    fnCutsceneTeleport("Tiffany", -1, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Sophie", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Night", "Secrebot") ]])
    fnCutscene([[ Append("Night:[E|SecrebotSad] I had better not keep her waiting.[P] And then I'll help you two as much as I can.[P] Because it looks like you need it.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Angry] What's that supposed to mean!?[B][C]") ]])
    fnCutscene([[ Append("Night:[E|SecrebotHappy] The caked on grease and dust in here are caked on top of other, greasier dust.[P] I'm going to need a mixing vat for all the bleach I'm going to need.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneMove("Night", 11.25, 14.50)
    fnCutsceneFace("Night", 0, 1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscenePlaySound("World|ClimbLadder")
    fnCutsceneTeleport("Night", -1, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] I happen to think we do a good job of cleaning up in here![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] We're at least 3 percent cleaner than your average repair bay![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] Christine![P] I'm so proud of you my big smart sexy cute spy![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Sad] Thanks Sophie, but...[P] if I had been faster or...[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] You did your best.[P] You can't win them all, but as long as you're okay, you're on the right track.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] And I guess I got a new form out of it.[P] I just...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] Vivify was using people, or at least her followers are.[P] Can't we just let people be?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] ...[P] I guess there's no sense getting worked up about it.[P] On to the next mission.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] So, uh, about all the stuff you said when you...[B][C]") ]])
    fnCutscene([[ Append("CR-1-16:[E|Serious] CR-1-16 online.[P] How may I serve you?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] D-[P]don't![B][C]") ]])
    fnCutscene([[ Append("CR-1-16:[E|Serious] I love you, Unit 499323.[P] Command me.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] O-[P]one kiss![B][C]") ]])
    fnCutscene([[ Append("CR-1-16:[E|Blush] *Smooch*[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] D-[P]d-[P]d-[P]do you like being my s-[P]s-[P]servant?![B][C]") ]])
    fnCutscene([[ Append("CR-1-16:[E|Blush] Of course.[P] I will do anything you command.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] T-[P]then...[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] We're going to scrub the floors in my quarters![P] Get them sparkling clean![P] A-[P]and then...[B][C]") ]])
    fnCutscene([[ Append("CR-1-16:[E|Blush] As you wish, superior unit.[P] I exist to serve you.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] O-[P]okay!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Next Scene]|
    fnCutscene([[ AL_BeginTransitionTo("RegulusCityB", "FORCEPOS:1.0x1.0x0") ]])
    fnCutsceneBlocker()
    
-- |[ ================================== Silly Bad End Thing =================================== ]|
elseif(sObjectName == "Secrebot Bad End") then

    -- |[Blackout]|
    fnCutsceneFadeOut()
    
    -- |[Spawning]|
    if(EM_Exists("Sophie") == false) then
        fnSpecialCharacter("Sophie", 15, 12, gci_Face_West, true, fnResolvePath() .. "Dialogue.lua")
    end
    
    -- |[Positioning]|
    fnCutsceneTeleport("Tiffany", -1.25, -1.50)
    fnCutsceneTeleport("Christine", 21.25, 10.50)
    fnCutsceneFace("Christine", 1, 0)
    fnCutsceneTeleport("Sophie", 22.25, 10.50)
    fnCutsceneFace("Sophie", -1, 0)
    
    -- |[Scene Execution]|
    fnCutsceneWait(135)
    fnCutsceneBlocker()
    fnCutsceneFadeIn(45)
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Serious") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] W-[P]wow...[P] that's so hot...[P] uh...[B][C]") ]])
    fnCutscene([[ Append("CR-1-16:[E|Serious] Thank you, superior unit.[P] What was your favourite part of my story?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] Um, the maid dresses.[P] I really liked those, you should...[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] B-[P]but next time, have 55 escape, okay?[P] I don't think she'd like it if you told that story to her![B][C]") ]])
    fnCutscene([[ Append("CR-1-16:[E|Serious] Criticism logged.[P] Would you like me to generate another story?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Jeez, where is 55 anyway?[P] I hope she didn't get stuck in a vent.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    -- |[Movement]|
    fnCutsceneTeleport("Tiffany", 11.25, 14.50)
    fnCutscenePlaySound("World|ClimbLadder")
    fnCutsceneBlocker()
    fnCutsceneMove("Tiffany", 19.25, 14.50, 2.50)
    fnCutsceneMove("Tiffany", 19.25, 10.50, 2.50)
    fnCutsceneFace("Tiffany", 1, 0)
    fnCutsceneBlocker()
    fnCutsceneFace("Christine", -1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Sophie", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Christine", "Serious") ]])
    fnCutscene([[ Append("55:[E|Upset] Report![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Surprised] C-[P]command unit I -[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Sad] C-[P]calm your processor Sophie.[P] It's fine.[P] It's 55.[P] She's a friend.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Narrator:[VOICE|Narrator] And so...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[To Sector 254]|
    fnCutscene([[ AL_BeginTransitionTo("Sector254B", "FORCEPOS:4.0x1.0x0") ]])
end
