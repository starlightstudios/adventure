-- |[ ================================= Secrebot Form Response ================================= ]|
--Called when talking to Sophie in secrebot form.
local iSophieKnowsAboutRunestone = VM_GetVar("Root/Variables/Chapter5/Sophie/iSophieKnowsAboutRunestone", "N")
local iSeenSecrebot = VM_GetVar("Root/Variables/Chapter5/Sophie/iSeenSecrebot", "N")

-- |[First Time Seeing RUnestone]|
--First time Sophie has seen non-golem Christine. Note: While it's not nominally possible to do this
-- on the first playthrough, in NC+ the player can get secrebot without doing the quest.
if(iSophieKnowsAboutRunestone == 0.0) then
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Sophie/iSophieKnowsAboutRunestone", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Sophie/iSeenSecrebot", "N", 1.0)
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Surprised") ]])
    fnCutscene([[ Append("Sophie:[E|Surprised] Christine![P] Did you get -[P] one hell of a makeover![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Happy] Of course![P] Secrebot CR-1-16, ready to serve![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Though in actuality, this is due to a little trick I have.[P] Magic runestone![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Offended] And just when were you going to tell me about this?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Blush] ...[P] Right now?[P] Do you like me like this?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] I'd have told you the moment we met but, um, it might be a good idea to keep this a secret.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Offended] I...[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Yeah, I think you're right.[P] I appreciate the trust you're showing me, but this does mean I might let something slip by accident.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] How does it work, or do you know?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Haven't the foggiest my dear, but let's just say.[P] Super magic.[P] The kind of magic they just don't make anymore.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] I had this runestone with me when I first arrived on Regulus, but how and why?[P] No idea.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] It's a tiny portable magic disguise kit![P] That's so neat, Christine![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] Your super duper secret is safe with me!") ]])
    fnCutsceneBlocker()

--First time as a Raiju.
elseif(iSeenSecrebot == 0.0) then
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Sophie/iSeenSecrebot", "N", 1.0)
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Sophie:[E|Offended] Christine![P] How -[P] right, runestone.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Secrebots, huh.[P] How interesting, I didn't think you could transform into one.[P] I suppose the parts might be compatible, apparently some excess golem parts were used to make them.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] I wouldn't know anything about that, that was before I came here.[P] But I have the form so let's not ask too many questions.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] And thank goodness you do, because this place needs some cleaning![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Secrebot, use a bleach wipe on that counter over there, let's get this place squeaky clean![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Serious] Affirmative, superior unit.[P] Secrebot CR-1-16 performing task.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] Hee hee![P] I -[P] uh, I like it when you do what I say![P] Hee hee hee hee![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] And -[P] you should kiss me![P] When you want to![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Serious] *Smooch*[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] S-[P]s-[P]secrebot CR-1-16 I order you to love me and obey me forever![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Serious] I already do, superior unit.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] Wheeeeee![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] Uh, secrebot, act like normal Christine now![P] Hee hee![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] The fun is over so soon?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] Over?[P] Not at all![P] But tonight, meet me in your quarters and we'll see just how far your programming goes!") ]])

--Repeats.
else

    --Special:
    local iSteamDroid100RepState = VM_GetVar("Root/Variables/Chapter5/Scenes/iSteamDroid100RepState", "N")
    if(iSteamDroid100RepState == 1.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSteamDroid100RepState", "N", 2.0)
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] Hey Christine, rocking the ol' secrebot chassis today?[P] What can I do for you?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Could you take a look at this with the micro-scanner?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] The -[P] piece of scrap you're holding?[P] Really?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] A Series-X20 transducer.[P] You can't tell?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Surprised] It's in awful shape![P] Where did you get that?[P] Who would send that piece of junk to us?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] JX-101, if you must know.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] Erm...[P] I'm searching my drives...[P] Where have I heard that name before?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Surprised] Wait, is she that Steam Droid that's on the shoot-on-sight notices?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Laugh] The same![P] I can attest that she's just as fierce in person.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Happy] Hee hee![P] I'll bet![B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] Well, Christine, I don't need to use the micro-scanner to tell you that transducer has seen its last days of service.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] [SOUND|World|TakeItem]So, why don't you take this one?[P] I'll just switch them and mark this one as scrapped.[P] Nobody will notice.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] I love the duplicity![B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] Hee hee![P] I hope this helps![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] If we can ally with the Steam Droids, we might have a real chance.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] My sexy maverick girlfriend...[P] Just be careful.[P] I've heard the caverns are dangerous.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] I will, dearest.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] Dearest?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Uhhhh...[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] No, it's okay.[P] I like it.[P] Good luck, dearest!") ]])
        return
    end

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Hello, Christine![P] Do you want to help clean the place up a bit, secrebot?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Blush] Oh yes, your lips are so filthy.[P] Here.[P] *smooch*[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Surprised] [SOUND|World|SparksA]N-[P]n-[P]not expected![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] Oh secrebot, I'm such a dirty girl![P] Thank you for 'cleaning' me right up!") ]])
    fnCutsceneBlocker()

end
