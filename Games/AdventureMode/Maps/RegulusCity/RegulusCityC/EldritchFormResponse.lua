-- |[Eldritch Dreamer Form Response]|
--Called when talking to Sophie in Eldritch form.
local iSeenEldritch = VM_GetVar("Root/Variables/Chapter5/Sophie/iSeenEldritch", "N")

--Variables.
local iSophieKnowsAboutRunestone = VM_GetVar("Root/Variables/Chapter5/Sophie/iSophieKnowsAboutRunestone", "N")
local iHasSteamDroidForm = VM_GetVar("Root/Variables/Global/Christine/iHasSteamDroidForm", "N")

--Flags.
VM_SetVar("Root/Variables/Chapter5/Sophie/iSophieKnowsAboutRunestone", "N", 1.0)

--First pass.
if(iSeenEldritch == 0.0) then

    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Sophie/iSeenEldritch", "N", 1.0)

    --Dialogue.
    fnCutsceneFace("Sophie", 0, -1)
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Just a second...[P] I'll be right with you...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Sophie...[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Sorry Christine, just a second...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] (No, no.[P] This is wrong...)[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] (I don't want her to see me like this...)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Flashwhite.
    Cutscene_CreateEvent("Flash Christine White", "Actor")
        ActorEvent_SetProperty("Subject Name", "Christine")
        ActorEvent_SetProperty("Flashwhite Quickly", "Null")
    DL_PopActiveObject()
    fnCutsceneBlocker()

    fnCutsceneWait(75)
    fnCutsceneBlocker()
    fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua") ]])
    fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()

    --Sophie faces the player.
    fnCutsceneFaceTarget("Sophie", "Christine")
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Ugh, it can wait.[P] What's going on?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] You look a little down.[P] Anything I can do to help?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Down?[P] Not a little![P] I'm great![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] But...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] No, it's all right.[P] There are things I really should talk to you about.[P] Yet.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Spy stuff?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Sort of.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Sophie, what if...[P] out there...[P] I got...[P] changed...[B][C]") ]])
    
    --If she doesn't know about the runestone, Christine has to show her.
    if(iSophieKnowsAboutRunestone == 0.0) then
        
        fnCutscene([[ Append("Sophie:[E|Neutral] Changed?[P] Like, shot up?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] No, Sophie.[P] I can transform myself using this runestone.[P] Watch.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()

        --Flashwhite.
        Cutscene_CreateEvent("Flash Christine White", "Actor")
            ActorEvent_SetProperty("Subject Name", "Christine")
            ActorEvent_SetProperty("Flashwhite Quickly", "Null")
        DL_PopActiveObject()
        fnCutsceneBlocker()

        fnCutsceneWait(75)
        fnCutsceneBlocker()
        fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Human.lua") ]])
        fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
        fnCutscene([[ Append("Sophie:[E|Happy] Wow![P] Into a human![P] You -[P] you went all bright and then poof![B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Happy] Amazing![B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] But how would getting changed into a human be a bad thing?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] *Because I could always convert you again to repair you if I had to...*[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] There are a lot of awful things out there, Sophie.[P] A lot.[P] And they can change me into them, as well.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] Uh, like a darkmatter?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Things you've never heard of.[P] Gross, disgusting, horrible things.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] But you could change back?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Yes.[P] The different bodies I have can be useful.[P] Some are stronger, or feel no pain, or are resistant to corrosion...[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Offended] As long as it doesn't have tentacles or one singular big eye...[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Offended] Cyclops-things are totally gross.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] (That's a strong no.)[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Happy] C-[P]can you do spider?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Ah, no, I'd have to have that form first.[P] I don't know of any spider-girls on Regulus.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Blush] Oh![P] Uh, too bad![P] How unfortunate![B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Blush] (Control yourself, 499323!)[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Happy] But you can transform, that means you're a super spy![P] Cool![B][C]") ]])
        
        --Sophie would already have a hint of it because of the Steam Droid TF.
        if(iHasSteamDroidForm == 1.0) then
            VM_SetVar("Root/Variables/Chapter5/Sophie/iSeenSteamDroid", "N", 1.0)
            fnCutscene([[ Append("Sophie:[E|Smirk] That's explain your plan with SX-399.[P] You were kinda sketchy on the details, I thought 55 was going to color your golem form to look like one.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Smirk] Did you turn into a Steam Droid?[P] Very clever![B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Laugh] Thanks![P] It was a proud moment![B][C]") ]])
        
        --No hint of it.
        else
            fnCutscene([[ Append("Christine:[E|Neutral] Yeah, that was our mutual friend's idea.[P] Doesn't always work out.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] But nobody is going to suspect it so long as we keep it a secret, so we can use it to break into places.[B][C]") ]])
        end
        fnCutscene([[ Append("Sophie:[E|Smirk] I'll keep your secret safe.[P] Don't you fret.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] Were you...[P] worried that I wouldn't like it?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Very.[P] I don't want to upset you.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] I'm sure I'd love it even if you were a six-eyed slavering demon.[P] I love you for you.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Blush] *But if you want to become a spider I'd love you for you even more...*[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] You're whispering to yourself.[P] My human ears aren't very sensitive you know.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Blush] N-[P]nothing![B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Blush] Got some work to do, my Lord Unit![P] I'd better get back to it!") ]])
        fnCutsceneBlocker()

    --Knows about the runestone.
    else
        
        fnCutscene([[ Append("Sophie:[E|Smirk] Are you referring to your runestone, Christine?[P] I think it's great![B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] What's there to be worried about?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] What if I were changed into something...[P] bad...[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] But you're still you, aren't you?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] What if I wasn't totally sure...[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] Let me say this, and then you decide what you want to do.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] I love you, Christine.[P] I trust you.[P] If you think it's for the best, I'll respect your decision.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] I know you're thinking of me whenever you choose to do something, and if for a second you think it'd hurt me, you wouldn't do it.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] So, is there something you wanted to show me?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] No.[P] No there isn't.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] But there might be, someday, when I'm more certain.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] See?[P] Wasn't so hard, was it?[P] This is what honesty really looks like.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] Follow your heart.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Thanks, I -[P] I feel a lot better now...") ]])
        fnCutsceneBlocker()


    end

--Sophie has run through the sequence before.
else

    --Dialogue.
    fnCutsceneFace("Sophie", 0, -1)
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] One moment, this will just -[P] not -[P] go in the hole![P] Come on![P] Who sanded this?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] (...[P] It's not the right time...)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Flashwhite.
    Cutscene_CreateEvent("Flash Christine White", "Actor")
        ActorEvent_SetProperty("Subject Name", "Christine")
        ActorEvent_SetProperty("Flashwhite Quickly", "Null")
    DL_PopActiveObject()
    fnCutsceneBlocker()

    fnCutsceneWait(75)
    fnCutsceneBlocker()
    fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua") ]])
    fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] [P]*Click*[P] There we go.") ]])
    fnCutsceneBlocker()

    --Dialogue.
    fnCutsceneFaceTarget("Sophie", "Christine")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] Oh, Christine![P] Sorry about that![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Need anything?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Just checking up on you.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] We're happy over here, thanks![P] Good luck out there!") ]])


end
