-- |[Darkmatter Form Response]|
--Called when talking to Sophie in Darkmatter form.
local iSophieKnowsAboutRunestone = VM_GetVar("Root/Variables/Chapter5/Sophie/iSophieKnowsAboutRunestone", "N")
local iSeenDarkmatter = VM_GetVar("Root/Variables/Chapter5/Sophie/iSeenDarkmatter", "N")

--First time Sophie has seen non-golem Christine.
if(iSophieKnowsAboutRunestone == 0.0) then
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Sophie/iSophieKnowsAboutRunestone", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Sophie/iSeenDarkmatter", "N", 1.0)
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Happy") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Hello, Christine.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Sophie?[P] You're not surprised?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] No, no I'm not.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Which is weird, but hear me out.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] You've been a Darkmatter for a long time, haven't you.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] In a time before time, when the world was still being forged in the core of distant stars.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] How did you know?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Because I saw it.[P] I was there.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] I had a dream while I was defragmenting.[P] But when I searched my archives, the signals came out of nowhere.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] They were just suddenly in my head, and I sorted them into my long-term memory.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] We were in front of some enormous black hole.[P] You were...[P] this...[P] and we were being as intimate as we've ever been.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] It wasn't a dream.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] I know.[P] Somehow, I know.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] And it means you can't break up with me no matter how weird I get, because our love is foretold.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] I wasn't going to, but you're right.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] It's nice not having to worry about you being shocked by this body.[P] Everyone else wants to study me.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] Oh, I want to study you![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Can my material hands caress you with as much care as when you're hard?[P] I should write a research paper.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Laugh] Let's serve the Cause of Science![P] Ha ha ha!") ]])
    fnCutsceneBlocker()

--First time as a Darkmatter. Basically the same, slight variations.
elseif(iSeenDarkmatter == 0.0) then
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Sophie/iSeenDarkmatter", "N", 1.0)
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Happy") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Hello, Christine.[P] I see you've become melted starlight.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] You are as you always were, and always were meant to be.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Sophie?[P] You're not surprised?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] No, no I'm not.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Which is weird, but hear me out.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] You've been a Darkmatter for a long time, haven't you.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] In a time before time, when the world was still being forged in the core of distant stars.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] How did you know?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Because I saw it.[P] I was there.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] I had a dream while I was defragmenting.[P] But when I searched my archives, the signals came out of nowhere.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] They were just suddenly in my head, and I sorted them into my long-term memory.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] We were in front of some enormous black hole.[P] You were...[P] this...[P] and we were being as intimate as we've ever been.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] It wasn't a dream.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] I know.[P] Somehow, I know.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] And it means you can't break up with me no matter how weird I get, because our love is foretold.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] I wasn't going to, but you're right.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] It's nice not having to worry about you being shocked by this body.[P] Everyone else wants to study me.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] Oh, I want to study you![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Can my material hands caress you with as much care as when you're hard?[P] I should write a research paper.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Laugh] Let's serve the Cause of Science![P] Ha ha ha!") ]])
    fnCutsceneBlocker()

--Repeats.
else

    --Special:
    local iSteamDroid100RepState = VM_GetVar("Root/Variables/Chapter5/Scenes/iSteamDroid100RepState", "N")
    if(iSteamDroid100RepState == 1.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSteamDroid100RepState", "N", 2.0)
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] Hey Christine, you're looking starry.[P] What can I do for you?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Could you take a look at this with the micro-scanner?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] The -[P] piece of scrap you're holding?[P] Really?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] A Series-X20 transducer.[P] You can't tell?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Surprised] It's in awful shape![P] Where did you get that?[P] Who would send that piece of junk to us?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] JX-101, if you must know.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] Erm...[P] I'm searching my drives...[P] Where have I heard that name before?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Surprised] Wait, is she that Steam Droid that's on the shoot-on-sight notices?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Laugh] The same![P] I can attest that she's just as fierce in person.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Happy] Hee hee![P] I'll bet![B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] Well, Christine, I don't need to use the micro-scanner to tell you that transducer has seen its last days of service.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] [SOUND|World|TakeItem]So, why don't you take this one?[P] I'll just switch them and mark this one as scrapped.[P] Nobody will notice.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] I love the duplicity![B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] Hee hee![P] I hope this helps![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] If we can ally with the Steam Droids, we might have a real chance.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] My sexy maverick girlfriend...[P] Just be careful.[P] I've heard the caverns are dangerous.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] I will, dearest.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] Dearest?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Uhhhh...[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] No, it's okay.[P] I like it.[P] Good luck, dearest!") ]])
        return
    end

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] I'm wondering what position and angle your body's stars are from?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Is that our galaxy there, or another?[P] Perhaps I could back-reference the starmaps...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] It's not this dimension, but another.[P] Some of the light from it reaches Regulus from gaps in the weave of reality.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] I don't want to go back there, ever, but this is a keepsake.[P] It's a place only I know of.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Fitting.[P] I wonder what I'd have if my body was starlight...") ]])
    fnCutsceneBlocker()

end
