-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Special: No need to go further if just backing out of dialogue.

-- |[ ========================================= "Hello" ======================================== ]|
if(sTopicName == "Hello") then
	
    -- |[Name Resolve]|
    --Resolve the name of the calling entity.
    local sActorName = TA_GetProperty("Name")

    -- |[ =================== Linda ==================== ]|
    if(sActorName == "Linda") then
        
        --Facing.
        TA_SetProperty("Face Character", "PlayerEntity")
        
        --Variables.
        local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
        
        --Handlers:
        if(sChristineForm == "Golem") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Linda:[VOICE|Golem] Hey Christine![P] Herbert is just settling in to the new cage I made him![P] Thanks for doing this...") ]])
            
        --Latex drone:
        elseif(sChristineForm == "LatexDrone") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Linda:[VOICE|Golem] Oh, hello, Drone Unit.[P] Um, if you need to deliver something, talk to the other golem over there?") ]])
        
        --Electrosprite:
        elseif(sChristineForm == "Electrosprite") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Linda:[VOICE|Golem] Woah![P] Hi, didn't know we have -[P] whatever you are -[P] as citizens in Regulus City.[P] Greetings!") ]])
        
        --Darkmatter:
        elseif(sChristineForm == "Darkmatter") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Linda:[VOICE|Golem] Uh oh, please don't knock over this cage.") ]])
        
        --SteamDroid:
        elseif(sChristineForm == "SteamDroid") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Linda:[VOICE|Golem] Christi-[P] no, couldn't be.[P] Hey buddy, you should stay out of sight.[P] The cameras here don't work but the ones in the rest of the sector might.") ]])
        
        --Eldritch:
        elseif(sChristineForm == "Eldritch") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Linda:[VOICE|Golem] I -[P] uh -[P] hello?[P] (Maybe just pretend like I'm not hallucinating...)") ]])
        
        --Raiju:
        elseif(sChristineForm == "Raiju") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Linda:[VOICE|Golem] Hi there![P] This is Herbert, my friends Christine and Sophie are helping to rehabilitate him.[P] He hurt his wing!") ]])
        
        --Doll:
        elseif(sChristineForm == "Doll") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Linda:[VOICE|Golem] C-[P]c-[P]command unit![P] I'm -[P] rehabilitating a damaged pest-control vector for use in the biolabs![P] I'm under orders![P] I promise!") ]])
        
        --Human:
        elseif(sChristineForm == "Human") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Linda:[VOICE|Golem] Hey![P] Look, you need to make sure you wear your day-pass on your shirt.[P] I don't want you to get in trouble but don't forget it, the other golems might get mad.") ]])
        
        --Secrebot:
        elseif(sChristineForm == "Secrebot") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Linda:[VOICE|Golem] Oh...[P] Night?[P] No.[P] You look different.[P] Are you a different secrebot than CR-1-15?[P] I can't believe our sector has two now!") ]])
        end
    
    -- |[ =================== Sophie =================== ]|
    elseif(sActorName == "Sophie") then

        --Facing.
        TA_SetProperty("Face Character", "PlayerEntity")
        
        --Christine's form.
        local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
        local iSophieKnowsAboutRunestone = VM_GetVar("Root/Variables/Chapter5/Sophie/iSophieKnowsAboutRunestone", "N")
        
        --If Christine is not a golem, these scenes play. You can't go on dates with Sophie! Well, not normal ones.
        if(sChristineForm ~= "Golem") then
            
            --Human handler:
            if(sChristineForm == "Human") then
                LM_ExecuteScript(fnResolvePath() .. "HumanFormResponse.lua", "No Special")
            
            --Latex drone:
            elseif(sChristineForm == "LatexDrone") then
                LM_ExecuteScript(fnResolvePath() .. "LatexFormResponse.lua", "No Special")
            
            --Electrosprite:
            elseif(sChristineForm == "Electrosprite") then
                LM_ExecuteScript(fnResolvePath() .. "ElectrospriteFormResponse.lua")
            
            --Darkmatter:
            elseif(sChristineForm == "Darkmatter") then
                LM_ExecuteScript(fnResolvePath() .. "DarkmatterFormResponse.lua")
            
            --SteamDroid:
            elseif(sChristineForm == "SteamDroid") then
                LM_ExecuteScript(fnResolvePath() .. "SteamDroidFormResponse.lua")
            
            --Eldritch:
            elseif(sChristineForm == "Eldritch") then
                TA_SetProperty("Facing", gci_Face_North)
                LM_ExecuteScript(fnResolvePath() .. "EldritchFormResponse.lua")
            
            --Raiju:
            elseif(sChristineForm == "Raiju") then
                LM_ExecuteScript(fnResolvePath() .. "RaijuFormResponse.lua")
            
            --Doll:
            elseif(sChristineForm == "Doll") then
                LM_ExecuteScript(fnResolvePath() .. "DollFormResponse.lua")
            
            --Secrebot:
            elseif(sChristineForm == "Secrebot") then
                LM_ExecuteScript(fnResolvePath() .. "SecrebotFormResponse.lua")
            end
            
            --Stop the normal response line.
            return
        end
        
        --Variables.
        local iReceivedFunction        = VM_GetVar("Root/Variables/Chapter5/Scenes/iReceivedFunction", "N")
        local iToldSophieAboutFunction = VM_GetVar("Root/Variables/Chapter5/Scenes/iToldSophieAboutFunction", "N")
        local iIsOnDate                = VM_GetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N")
        local iSawSpecialAnnouncement  = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawSpecialAnnouncement", "N")
        local iMet55InBasement         = VM_GetVar("Root/Variables/Chapter5/Scenes/iMet55InBasement", "N")
        local iSophieImportantMeeting  = VM_GetVar("Root/Variables/Chapter5/Scenes/iSophieImportantMeeting", "N")
        local iMet55InLowerRegulus     = VM_GetVar("Root/Variables/Chapter5/Scenes/iMet55InLowerRegulus", "N")
        local iLeftRepairBay           = VM_GetVar("Root/Variables/Chapter5/Scenes/iLeftRepairBay", "N")
        local iToldSophieAbout55       = VM_GetVar("Root/Variables/Chapter5/Scenes/iToldSophieAbout55", "N")
        local iStartedShoppingSequence = VM_GetVar("Root/Variables/Chapter5/Scenes/iStartedShoppingSequence", "N")
        local i254WorkOrder            = VM_GetVar("Root/Variables/Chapter5/Scenes/i254WorkOrder", "N")
        local i254TalkedToSophie       = VM_GetVar("Root/Variables/Chapter5/Scenes/i254TalkedToSophie", "N")
        
        --Hasn't received a function yet.
        if(iReceivedFunction == 0.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] Lord Golem?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] I keep telling you, call me Christine![B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] S-[P]Sorry.[P] Force of habit.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] Please check the work terminals in the hallway outside. Function assignments may take a few minutes to process.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Thank you very much.") ]])
        
        --Special: Christine just told Sophie she'd go look for 55. Dialogue changes for the duration.
        elseif(iSophieImportantMeeting == 2.0 and iMet55InLowerRegulus == 0.0 and iIsOnDate == 0.0) then
        
            --If the player has not left the maintenance bay yet:
            if(iLeftRepairBay == 0.0) then
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
                fnCutscene([[ Append("Sophie:[E|Neutral] Come back in a few minutes, 771852.[P] I'm going to doctor these reports a bit before I upload them.[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Smirk] Hey, that's weird.[P] You seem to have never had the encrypted memory to begin with.[B][C]") ]])
                fnCutscene([[ Append("*click*[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Smirk] In fact, there has never been any reason to take interest in Unit 771852.[P] How strange![B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] You called me the spymaster, but I'm starting to think I could learn a few things.[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Smirk] I'll keep you safe in whatever ways I can.[P] Nobody will take you away.[P] I promise.") ]])

            --Player has left and come back:
            else
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
                fnCutscene([[ Append("Sophie:[E|Neutral] Christine![P] Have you met up with 2855 yet?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Not yet.[P] I was looking for a meet-up with you, instead.[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Smirk] Even better.[P] Could I, perhaps, help you earn some work credits?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] You mean go on a date?[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Smirk] Hey, I can be evasive if I want to.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] (Take Sophie out on a date?)[BLOCK]") ]])
            
                --Decision stuff.
                local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
                fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
                fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"YesSpecial\") ")
                fnCutscene(" WD_SetProperty(\"Add Decision\", \"Not right now...\",  " .. sDecisionScript .. ", \"NoSpecial\") ")
                fnCutsceneBlocker()
            end
        
        --Has received a function. Not on a date, hasn't seen this before.
        elseif(iReceivedFunction == 1.0 and iToldSophieAboutFunction == 0.0) then
            VM_SetVar("Root/Variables/Chapter5/Scenes/iToldSophieAboutFunction", "N", 1.0)
            VM_SetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N", 1.0)
            VM_SetVar("Root/Variables/Chapter5/Scenes/iMainQuestBlock", "N", 13.0)
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "PDU", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] Lord G-[P] Christine![P] Was there a problem?[P] I've already uploaded your specifications to the network.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Unit 499323![P] I've got my function assignment![B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Smirk] Oh, that's nice.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] I appreciate you coming to tell me but you didn't have to.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Really?[P] Why don't you check your PDU?[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] All right...[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Surprised] No -[P] no way![B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Blush] A -[P] hello, Lor -[P] Christine![P] Lord Golem of Maintenance and Repair, Sector 96![P] Hee hee![B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Blush] (I can't believe it I can't believe it I can't believe it!)[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] I suppose I'm your boss now.[P] Though in reality, you're the real boss.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Blush] Whaaaaaaa?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] You're much more experienced.[P] I'm sure I'll learn as I go, I've already asked my PDU to forward some manuals to my defragmentation pod.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Oh, where is that, by the way?[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Blush] Well I - [B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Happy] Lord Golem![P] Would you like me to show you around?[B][C]") ]])
            fnCutscene([[ Append("PDU:[E|Happy][EMOTION|Christine|PDU] (Matchmaker protocols activated.[P] Sounds like Unit 499323 is asking you out.)[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|PDU] (She's offering to show me around, PDU.[P] Knock it off.)[B][C]") ]])
            fnCutscene([[ Append("PDU:[E|Happy] (Are you going to say no?)[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|PDU] (I'm going to say yes, and it's not a date![P] She's just being nice because I'm her boss!)[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] Is something wrong with your PDU?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] Yeah.[P] The little shit won't shut up.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] Oops![P] I'm sorry, I should watch my language.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] Hee hee![P] I don't mind, it's the other lord golems you need to watch out for.[P] They'll pounce on you if you show weakness.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Happy] All right![P] Let's go look around the sector![P] If you'd like to...[P] spend some time looking at something...[P] just examine it![B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Happy] Lead the way, lord unit!") ]])
            
            --[=[
            fnCutscene([[ Append("Sophie:[E|Smirk] Ah, lovely.[P] I just finished uploading your specs.[P] What function did you get?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] I've become the Lord Golem of Maintenance and Repair in this sector.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Surprised] Well that's -[P] what?[P] What!?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Happy] That's lovely news, isn't it?[P] We'll be performing our functions together![B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Surprised] But you -[P] huh?[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] Look at that, it's in the system.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Is that unusual?[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] Yes, it really is.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] Christine, Maintenance and Repair hasn't had a Lord Golem in -[P] well, since I've worked here.[P] I've kind of been running the department for a while.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Sad] But several departments are in need of a Lord Golem and got passed over.[P] Why would the administrators put you here?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Perhaps the algorithms determined we have a high compatibility?[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Smirk] Yeah, maybe.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] Sorry, I'm not ungrateful.[P] I just haven't had anyone looking over my shoulder in quite a while.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Hmm, I won't be looking over your shoulder...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] I'll be looking at...[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Blush] S-[P]stop![P] You'll get me all flustered![B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] You will, of course, need to teach me.[P] I only have the basics of fabrication and repair programmed in right now.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] Teach...[P] you?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Yes.[P] Don't you need my help fixing things?[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Smirk] Lord Golems don't usually work, Christine.[P] Their job is ordinarily to make Slave Golems work at optimum efficiency.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Well, I am no ordinary Lord Golem.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Happy] You sure aren't![B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Do you want to celebrate?[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] ...[P] What?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Come on, let's go do something fun![P] Let's celebrate being co-workers and tandem units![B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] You mean, you want to do something other than work, right now?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] If you've been working one-tenth as hard as it looks like you have, I think a break is in order.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] My optimization routines indicate that breaks improve physical efficiency and emotional well-being.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Blush] ...[P] And I really, really want to celebrate with you.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Happy] Yay![P] Yay![P] Yayyayayayayay![B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] ...[P] Ahem.[P] I mean, okay.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] Since you don't know your way around, I'll just suggest activities as we approach them.[P] Does that sound good?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] It does![P] Let's go![B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Smirk] All right 771852, lead the way!") ]])]=]
            fnCutsceneBlocker()
        
            --Set Lua globals.
            giFollowersTotal = 1
            gsaFollowerNames = {"Sophie"}
            giaFollowerIDs = {0}

            --Get Sophie's uniqueID. We also need to remove Sophie's dialogue flag and collisions.
            EM_PushEntity("Sophie")
                local iSophieID = RE_GetID()
                TA_SetProperty("Clipping Flag", false)
                TA_SetProperty("Activation Script", "Null")
            DL_PopActiveObject()

            --Store it and tell her to follow.
            giaFollowerIDs = {iSophieID}
            AL_SetProperty("Follow Actor ID", iSophieID)
        
            --Move Sophie onto Christine's position.
            EM_PushEntity("Christine")
                local fChristineX, fChristineY = TA_GetProperty("Position")
            DL_PopActiveObject()
            fnCutsceneMove("Sophie", fChristineX / gciSizePerTile, fChristineY / gciSizePerTile)
            fnCutsceneBlocker()
        
            --Fold the party.
            fnCutscene([[ AL_SetProperty("Fold Party") ]])
            fnCutsceneBlocker()
        
        --Sector 254 cutscene.
        elseif(i254WorkOrder == 1.0 and i254TalkedToSophie == 0.0) then
            LM_ExecuteScript(fnResolvePath() .. "Sector254Scene.lua")
        
        
        --Currently on a special assignment.
        elseif(iSawSpecialAnnouncement == 2.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] Hey Christine![P] I was just thinking about you...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Good things, I hope.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Smirk] Always![B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Smirk] Do you want to go out with me?[P] I could take a break right now...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Sorry, but I just got a special work order.[P] It said to meet a unit in the basement for further instructions.[P] It wasn't you, was it?[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] Uh, no.[P] And I haven't seen anyone in here other than you.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] The only way into the basement is that ladder or the hydraulic lift.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Sad] ...[P] Be on your guard...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Is something wrong?[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] N-[P]no.[P] Not at all.[P] Maybe they meant the terminal down there.[P] Yeah, that's got to be it.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] All right.[P] Thanks for your help.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Sad] ...[P] Please be careful...") ]])
            fnCutsceneBlocker()
        
        --Shopping!
        elseif(iStartedShoppingSequence == 1.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
            fnCutscene([[ Append("Sophie:[E|Happy] Christine![P] Ready to go shopping?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] (Should I go shopping with Sophie?)[BLOCK]") ]])
            
            --Special variables:
            local iManuTold55Plan = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuTold55Plan", "N")
            local iPerformedLatex = VM_GetVar("Root/Variables/Chapter5/Sophie/iPerformedLatex", "N")
            
            --Decision script is this script. It must be surrounded by quotes.
            local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
            fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Let's go!\", " .. sDecisionScript .. ", \"ShoppingTime\") ")
            if(iManuTold55Plan == 1.0 and iPerformedLatex == 0.0) then
                fnCutscene(" WD_SetProperty(\"Add Decision\", \"Drone Me!\", " .. sDecisionScript .. ", \"DroneMeGala\") ")
            end
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Not Quite Yet\",  " .. sDecisionScript .. ", \"NotThisSecond\") ")
            fnCutsceneBlocker()
            return
        
        --Have to talk to 55 first.
        elseif(iStartedShoppingSequence == 4.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] Hey, no peeking![P] Go bother Unit 2855![B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Hmm?[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Happy] I'm drawing up plans for your dress, silly![P] And I want it to be a surprise, so no peeking![B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] (Guess I better go talk to 55...)[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] (But I really want to see what my dress will look like...)") ]])
            fnCutsceneBlocker()
            return
        
        --Not on a date with Sophie.
        elseif(iReceivedFunction == 1.0 and iToldSophieAboutFunction == 1.0 and iIsOnDate == 0.0) then
        
            --Special:
            local iManuTold55Plan = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuTold55Plan", "N")
            local iPerformedLatex = VM_GetVar("Root/Variables/Chapter5/Sophie/iPerformedLatex", "N")
            if(iManuTold55Plan == 1.0 and iPerformedLatex == 0.0) then
                LM_ExecuteScript(LM_GetCallStack(0), "DroneMe")
                return
            end

            --Special:
            local iSteamDroid100RepState = VM_GetVar("Root/Variables/Chapter5/Scenes/iSteamDroid100RepState", "N")
            if(iSteamDroid100RepState == 1.0) then
                VM_SetVar("Root/Variables/Chapter5/Scenes/iSteamDroid100RepState", "N", 2.0)
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
                fnCutscene([[ Append("Sophie:[E|Neutral] Good morning, 771852.[P] Hey, what's that you've got there?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] A Series-X20 transducer.[P] You can't tell?[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Surprised] It's in awful shape![P] Where did you get that?[P] Who would send that piece of scrap to us?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] JX-101, if you must know.[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Neutral] Erm...[P] I'm searching my drives...[P] Where have I heard that name before?[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Surprised] Wait, is she that Steam Droid that's on the shoot-on-sight notices?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Laugh] The same![P] I can attest that she's just as fierce in person.[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Happy] Hee hee![P] I'll bet![B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Neutral] Well, Christine, I don't need to use the micro-scanner to tell you that transducer has seen its last days of service.[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Smirk] [SOUND|World|TakeItem]So, why don't you take this one?[P] I'll just switch them and mark this one as scrapped.[P] Nobody will notice.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] I love the duplicity![B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Smirk] Hee hee![P] I hope this helps![B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] If we can ally with the Steam Droids, we might have a real chance.[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Smirk] My sexy maverick girlfriend...[P] Just be careful.[P] I've heard the caverns are dangerous.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] I will, dearest.[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Neutral] Dearest?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Uhhhh...[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Smirk] No, it's okay.[P] I like it.[P] Good luck, dearest!") ]])
                return
            end
            
            --Told Sophie about 55.
            if(iToldSophieAbout55 == 0.0 and iMet55InLowerRegulus >= 1.0) then
                VM_SetVar("Root/Variables/Chapter5/Scenes/iToldSophieAbout55", "N", 1.0)
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
                fnCutscene([[ Append("Sophie:[E|Neutral] 771852![P] Did you -[P] did you meet with -[P] you know -[P] her?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] You mean Unit 2855?[P] There's nobody in earshot.[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Smirk] I know.[P] I even put up some dampeners outside.[P] But -[P] it's so much more fun to talk in code, right?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Oh![P] Yes, of course.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] I met my old friend.[P] We've decided that Operation Green Fox is a go.[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Neutral] Okay, I know what you're trying to do, but Operation Green Fox?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] Look.[P][P] Not everything that comes out of my vocal synthesizer is a treatise on Othello, okay?[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Happy] I'm kidding![P] You should see your face![B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Neutral] (What's Othello?[P] Hm, maybe I should just pretend like I know.)[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Now, it's probably best that I don't tell you *exactly* where we're going.[P] Or when.[P] For your safety.[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Neutral] I understand.[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Blush] ...[P] You are making me so hot right now.[P] I think I'm going to have a meltdown.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Blush] Please don't.[P] At least not until I've got you back to my quarters.[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Blush] Oh, calm yourself, Sophie...[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Smirk] Ahem.[P] So.[P] Christine.[B][C]") ]])
                fnCutscene([[ Append("Sophie:[E|Smirk] Are you ready to get to work for the day?[P] I'll understand if you have...[P] sexy...[P] dirty...[P] spy stuff to do...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] (Should I start my work for the day?)[BLOCK]") ]])
                
                --Decision stuff.
                local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
                fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
                fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"StartWork\") ")
                fnCutscene(" WD_SetProperty(\"Add Decision\", \"Not Yet...\",  " .. sDecisionScript .. ", \"NoWork\") ")
                fnCutsceneBlocker()
                return
            end
        
            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
            fnCutscene([[ Append("Sophie:[E|Smirk] Good morning, 771852.[P] Ready to get to work?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] (Should I start my work for the day?)[BLOCK]") ]])
        
            --Decision stuff.
            local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
            fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"StartWork\") ")
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Not Yet...\",  " .. sDecisionScript .. ", \"NoWork\") ")
            fnCutsceneBlocker()
        
        --Finished a date.
        elseif(iReceivedFunction == 1.0 and iToldSophieAboutFunction == 1.0 and iIsOnDate == 3.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
            fnCutscene([[ Append("Sophie:[E|Smirk] I just have a few things to finish up, 771852.[P] I won't stay up too late, I promise.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Smirk] I'll...[P] think of you when I'm defragging.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] I'll think of you, too.[P] Good night.") ]])
            fnCutsceneBlocker()
        end
    end

-- |[ ================================== Responses to Sophie =================================== ]|
--Start work.
elseif(sTopicName == "StartWork") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Variables.
	local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
	VM_SetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N", iWorkCreditsTotal + 5)
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] All right, let's get to it.[P] What's on our list for today?") ]])
	fnCutsceneBlocker()
	
	--Fade to black.
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	
	--Hold on black. This is where we'd put interstitial cutscenes.
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	
	--Reposition.
	fnCutsceneTeleport("Christine", 23.25, 12.50)
	fnCutsceneTeleport("Sophie", 22.25, 12.50)
	fnCutsceneFace("Christine", 0, -1)
	fnCutsceneFace("Sophie", 0, -1)
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Narrator: 10 hours later...[B][C]") ]])
	fnCutscene([[ Append("Narrator: (Received 5 work credits.)") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Fade in.
	fnCutsceneWait(25)
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
	fnCutscene([[ Append("Sophie:[E|Smirk] Hand me that screwdriver, won't you?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Smirk] Okay, finish that toaster and we're done for the day.[B][C]") ]])
	fnCutscene([[ Append("Sophie:[E|Smirk] Done, and done![P] Phew![B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Smirk] Job well done.[P] We're making some good progress on the backlog.[B][C]") ]])
	fnCutscene([[ Append("Sophie:[E|Smirk] Time really flies by when I have someone to talk to.[B][C]") ]])
	fnCutscene([[ Append("Sophie:[E|Happy] So, do you maybe want to keep the good times rolling...?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] (Sophie's pretty obviously asking me out.[P] Should I?)[BLOCK]") ]])
		
	--Decision stuff.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"Yes\") ")
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"Not tonight...\",  " .. sDecisionScript .. ", \"No\") ")
	fnCutsceneBlocker()
	
--No work yet.
elseif(sTopicName == "NoWork") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
	fnCutscene([[ Append("Christine:[E|Smirk] I have a few things I need to do first.[B][C]") ]])
	fnCutscene([[ Append("Sophie:[E|Blush] Oh, okay.[P] Uhm, could you maybe walk a bit slower on your way out?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Uhhh, why?[B][C]") ]])
	fnCutscene([[ Append("Sophie:[E|Blush] You have a very nice sway.[P] I like to...[P] oops...[P] I'm being creepy aren't I?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Laugh] No, not at all![P] I'll consider it a compliment.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Smirk] And, I'll come back when I have time.[P] Feel free to get started without me.[B][C]") ]])
	fnCutscene([[ Append("Sophie:[E|Smirk] Right away, Lord Unit!") ]])
	fnCutsceneBlocker()

--Go on a date with Sophie.
elseif(sTopicName == "Yes") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Flags.
	VM_SetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N", 1.0)
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
	fnCutscene([[ Append("Christine:[E|Happy] Of course![P] Come on, let's go do something fun![B][C]") ]])
	fnCutscene([[ Append("Sophie:[E|Happy] Lead the way, tandem unit.") ]])
	fnCutsceneBlocker()
	
	--Set Lua globals.
	giFollowersTotal = 1
	gsaFollowerNames = {"Sophie"}
	giaFollowerIDs = {0}

	--Get Sophie's uniqueID. We also need to remove Sophie's dialogue flag and collisions.
	EM_PushEntity("Sophie")
		local iSophieID = RE_GetID()
		TA_SetProperty("Clipping Flag", false)
		TA_SetProperty("Activation Script", "Null")
	DL_PopActiveObject()

	--Store it and tell her to follow.
	giaFollowerIDs = {iSophieID}
	AL_SetProperty("Follow Actor ID", iSophieID)

	--Move Sophie onto Christine's position.
	EM_PushEntity("Christine")
		local fChristineX, fChristineY = TA_GetProperty("Position")
	DL_PopActiveObject()
	fnCutsceneMove("Sophie", fChristineX / gciSizePerTile, fChristineY / gciSizePerTile)
	fnCutsceneBlocker()

	--Fold the party.
	fnCutscene([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()

--Go on a date with Sophie, special dialogue during the "Meet 55 in the basement" part of the story.
elseif(sTopicName == "YesSpecial") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Flags.
	VM_SetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N", 1.0)
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
	fnCutscene([[ Append("Christine:[E|Happy] Sure, let's go do something fun![B][C]") ]])
	fnCutscene([[ Append("Sophie:[E|Neutral] Oh, erm, maybe you should go meet up with Unit 2855 instead?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Smirk] She can wait a few hours.[P] It's not like she's going to spoil.[B][C]") ]])
	fnCutscene([[ Append("Sophie:[E|Smirk] Hee hee![P] I'm not going to try to talk you out of it![B][C]") ]])
	fnCutscene([[ Append("Sophie:[E|Happy] Lead the way, you sexy maverick!") ]])
	fnCutsceneBlocker()
	
	--Set Lua globals.
	giFollowersTotal = 1
	gsaFollowerNames = {"Sophie"}
	giaFollowerIDs = {0}

	--Get Sophie's uniqueID. We also need to remove Sophie's dialogue flag and collisions.
	EM_PushEntity("Sophie")
		local iSophieID = RE_GetID()
		TA_SetProperty("Clipping Flag", false)
		TA_SetProperty("Activation Script", "Null")
	DL_PopActiveObject()

	--Store it and tell her to follow.
	giaFollowerIDs = {iSophieID}
	AL_SetProperty("Follow Actor ID", iSophieID)

	--Move Sophie onto Christine's position.
	EM_PushEntity("Christine")
		local fChristineX, fChristineY = TA_GetProperty("Position")
	DL_PopActiveObject()
	fnCutsceneMove("Sophie", fChristineX / gciSizePerTile, fChristineY / gciSizePerTile)
	fnCutsceneBlocker()

	--Fold the party.
	fnCutscene([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()
	
--No date.
elseif(sTopicName == "No") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Flags.
	VM_SetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N", 3.0)
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
	fnCutscene([[ Append("Christine:[E|Smirk] I'm sorry Sophie, but I have a lot of things to do.[B][C]") ]])
	fnCutscene([[ Append("Sophie:[E|Smirk] Hee hee![P] Don't worry, I know how it is.[P] You probably have a lot of Lord Unit functions you have to do after hours.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Smirk] Sorry.[P] Maybe next time?[B][C]") ]])
	fnCutscene([[ Append("Sophie:[E|Smirk] I'm looking forward to it.[P] See you!") ]])
	fnCutsceneBlocker()

--No date, special dialogue during the "Meet 55" part of the story.
elseif(sTopicName == "NoSpecial") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Flags.
	VM_SetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N", 3.0)
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Not right now, no.[P] I've got a date -[P] with 2855![B][C]") ]])
	fnCutscene([[ Append("Sophie:[E|Offended] For 0.555866 seconds you made my CPU stutter.[P] Don't scare me like that![B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Sad] Sorry![P] I belong wholly to you![B][C]") ]])
	fnCutscene([[ Append("Sophie:[E|Happy] That's right![P] Now, go on.[P] My cute, sexy tandem unit is a rebel leader![P] This is so exciting![B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Smirk] I hope you don't talk like that when other units can hear you...[B][C]") ]])
	fnCutscene([[ Append("Sophie:[E|Happy] Hee hee![P] Nope![P] Nobody will suspect a thing!") ]])
	fnCutsceneBlocker()
    
--Time for some shopping.
elseif(sTopicName == "ShoppingTime") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Flags.
	VM_SetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/WorldState/iNoWarpsAllowed", "N", 1.0)
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
	fnCutscene([[ Append("Christine:[E|Happy] Yep, let's get going![B][C]") ]])
	fnCutscene([[ Append("Sophie:[E|Happy] Splendid![P] We can take the tram to Sector 119![P] Lead the way!") ]])
	fnCutsceneBlocker()
	
	--Set Lua globals.
	giFollowersTotal = 1
	gsaFollowerNames = {"Sophie"}
	giaFollowerIDs = {0}

	--Get Sophie's uniqueID. We also need to remove Sophie's dialogue flag and collisions.
	EM_PushEntity("Sophie")
		local iSophieID = RE_GetID()
		TA_SetProperty("Clipping Flag", false)
		TA_SetProperty("Activation Script", "Null")
	DL_PopActiveObject()

	--Store it and tell her to follow.
	giaFollowerIDs = {iSophieID}
	AL_SetProperty("Follow Actor ID", iSophieID)

	--Move Sophie onto Christine's position.
	EM_PushEntity("Christine")
		local fChristineX, fChristineY = TA_GetProperty("Position")
	DL_PopActiveObject()
	fnCutsceneMove("Sophie", fChristineX / gciSizePerTile, fChristineY / gciSizePerTile)
	fnCutsceneBlocker()

	--Fold the party.
	fnCutscene([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()

--Called when Sophie is asking to go shopping.
elseif(sTopicName == "DroneMeGala") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Smirk") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Actually...[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] You've got a twinkle in your eye.[P] Did you come up with a fun idea?[P] A joke?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Actually, I wanted you to transform me into a drone unit.[B][C]") ]])
    LM_ExecuteScript(LM_GetCallStack(0), "DroneMeLinkup")

--TF into a latex drone!
elseif(sTopicName == "DroneMe") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Smirk") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Good morning, 771852.[P] What -[P] what is that twinkle in your eye?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] You noticed.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] You're practically beaming![P] Did you just come up with a fun idea?[P] A joke?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Actually, I wanted you to transform me into a drone unit.[B][C]") ]])
    LM_ExecuteScript(LM_GetCallStack(0), "DroneMeLinkup")

--Common handler for drone sequence.
elseif(sTopicName == "DroneMeLinkup") then
                
    --Variables.
    local iSophieKnowsAboutRunestone = VM_GetVar("Root/Variables/Chapter5/Sophie/iSophieKnowsAboutRunestone", "N")
    
    --Flags.
    VM_SetVar("Root/Variables/Chapter5/Sophie/iSophieKnowsAboutRunestone", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Sophie/iSeenLatex", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Sophie/iPerformedLatex", "N", 1.0)
    
    fnCutscene([[ Append("Sophie:[E|Blush] Whaaaaaaaaaaaaaaaaaaaaaaat!?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] Tr-[P]transform you?[B][C]") ]])
    if(iSophieKnowsAboutRunestone == 0.0) then
        fnCutscene([[ Append("Christine:[E|Smirk] Well, I've had this little runestone in my family for generations.[P] Apparently, it lets me transform myself.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Blush] (She stands, helpless, in the tube.[P] I press the switch and the gel sprays...)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] I just focus for a second, think about how I was, and -[P] poof.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Blush] (Covered in crawling latex snaking its way up her nude body...)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] In fact, I can show you.[P] Do you want to see it?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Blush] (Her skin and muscles turn into actuators covered by tight latex that strains with each motion...)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] Sophie?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Blush] (Her face reshaped into a mask, her mind simplified.[P] She obeys my every command!)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] Sophie?[P] Are you all right?[P] Did I upset you?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Blush] Wh-[P]what?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Blush] What are we waiting for?[P] Transform into a human already![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Oh, all right.[P] I was just - [P][CLEAR]") ]])
        fnCutscene([[ Append("Sophie:[E|Blush] Hurry![P] Please![P] Hee hee!") ]])
        fnCutsceneBlocker()
    else
        fnCutscene([[ Append("Sophie:[E|Blush] Like, repurpose you?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] It's for a special assignment.[P] Nobody would suspect a simple-minded drone unit, right?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] I just transform myself into a human, you convert me...[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Blush] What are we waiting for?[P] Transform into a human already![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Oh, all right.[P] I was just - [P][CLEAR]") ]])
        fnCutscene([[ Append("Sophie:[E|Blush] Hurry![P] Please![P] Hee hee!") ]])
        fnCutsceneBlocker()
    end
    
    --Wait.
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Flashwhite.
    Cutscene_CreateEvent("Flash Christine White", "Actor")
        ActorEvent_SetProperty("Subject Name", "Christine")
        ActorEvent_SetProperty("Flashwhite Quickly", "Null")
    DL_PopActiveObject()
    fnCutsceneBlocker()

    fnCutsceneWait(75)
    fnCutsceneBlocker()
    fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Human.lua") ]])
    fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Smirk") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Blush") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Right, into the tube with you![P] We can use the one in the basement![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Okay![P] Be sure - [P][CLEAR]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Tube tube tube tube!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Call the TF cutscene here.
    fnCutsceneWait(25)
    fnCutscene([[ AL_SetProperty("Activate Fade", 25, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    LM_ExecuteScript(gsRoot .. "Chapter 5/Scenes/400 Volunteer/Latex Drone Volunteer/Scene_Begin.lua")
    
    --Move scene to RegulusCityG.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iManuPostLatexScene", "N", 1.0)
    fnCutscene([[ AL_BeginTransitionTo("RegulusCityG", "FORCEPOS:23.0x10.0x0") ]])
    fnCutsceneBlocker()
    
--Not time for shopping.
elseif(sTopicName == "NotThisSecond") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
	fnCutscene([[ Append("Christine:[E|Smirk] Still tying up a few loose ends.[B][C]") ]])
	fnCutscene([[ Append("Sophie:[E|Happy] I've been just daydreaming about the pretty dresses I'm going to make...[P] Oooh![P] Don't keep a girl waiting!") ]])
	fnCutsceneBlocker()
end
