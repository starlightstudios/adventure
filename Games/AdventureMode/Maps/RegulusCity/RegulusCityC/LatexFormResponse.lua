-- |[Latex Form Response]|
--Called when talking to Sophie in latex drone form.
local iSophieKnowsAboutRunestone = VM_GetVar("Root/Variables/Chapter5/Sophie/iSophieKnowsAboutRunestone", "N")
local iSeenLatex = VM_GetVar("Root/Variables/Chapter5/Sophie/iSeenLatex", "N")

--Argument handling.
if(not fnArgCheck(1)) then return end
local sTopicName = LM_GetScriptArgument(0)

-- |[Normal Path]|
--The topic "No Special" is used when first calling this from Sophie's dialogue.
if(sTopicName == "No Special") then

    --First time Sophie has seen non-golem Christine.
    if(iSophieKnowsAboutRunestone == 0.0) then
        
        --Flag.
         VM_SetVar("Root/Variables/Chapter5/Sophie/iSophieKnowsAboutRunestone", "N", 1.0)
         VM_SetVar("Root/Variables/Chapter5/Sophie/iSeenLatex", "N", 1.0)
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] Hello, drone unit.[P] I just need a second, I'll be right with you.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] Oooh![P] I like your hair![P] Too bad Christine isn't here, she'd probably get all jealous.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] (I don't think she recognizes me.[P] Should I mess with her?)[BLOCK]") ]])
        
        --Decision script is this script. It must be surrounded by quotes.
        local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
        fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"Yes\") ")
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"Yes\") ")
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"Yes\") ")
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"Yes\") ")
        fnCutsceneBlocker()

    --Hasn't seen the Latex form yet:
    elseif(iSeenLatex == 0.0 and iSophieKnowsAboutRunestone == 1.0) then
        
        --Flag.
         VM_SetVar("Root/Variables/Chapter5/Sophie/iSeenLatex", "N", 1.0)
         
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] GREETINGS, UNIT 499323.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] Drone Unit?[P] Do you need my attention for a delivery?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] THIS UNIT HAS BEEN REASSIGNED TO YOUR DEPARTMENT.[P] HOW MAY I SERVE YOU?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] (That accent...)[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] Drone Unit, who assigned you here?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] THIS UNIT WAS ASSIGNED BY UNIT 771852 TO SERVE UNIT 499323 WITH HER WHOLE BEING.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] Did Unit 771852 specify anything else?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] SHE ORDERED THIS UNIT TO LOVE AND PROTECT UNIT 499323.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] And?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] AND TO CHEER HER UP WHEN SHE'S DOWN, AND TO MAKE SURE NOBODY BULLIES HER.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] And?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] UHHH...[P] TO...[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] Oh my, Drone Unit.[P] It seems your mental faculties are not up to par.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] I have to give you an inspection.[P] That's what we do here in Maintenance and Repair, you know.[P] Always have tip-top equipment.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] AFFIRMATIVE.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] Drone Unit, stand at attention.[P] Prepare for inspection.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] Standard protocols apply.[P] Chest forward, arms at your side.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] AFFIRMATIVE, UNIT IS STANDING AT ATTENTION...") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Run the first-time cutscene.
        LM_ExecuteScript(fnResolvePath() .. "Inspection.lua", "First Time")

    --Repeats.
    else

        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] Hey Christine![P] How are you?[P] Need anything?[BLOCK]") ]])
        
        --Decision script is this script. It must be surrounded by quotes.
        local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
        fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Just checking up on you\", " .. sDecisionScript .. ", \"Checking\") ")
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"An inspection\", " .. sDecisionScript .. ", \"Inspection\") ")
        local iSteamDroid100RepState = VM_GetVar("Root/Variables/Chapter5/Scenes/iSteamDroid100RepState", "N")
        if(iSteamDroid100RepState == 1.0) then
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"This Transducer...\", " .. sDecisionScript .. ", \"Nothing\") ")
        else
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Nothing\", " .. sDecisionScript .. ", \"Nothing\") ")
        end
        fnCutsceneBlocker()

    end

--Second part of the latex scene, after choosing to mess with Sophie.
elseif(sTopicName == "Yes") then
        
    --Clean up remaining scene parts.
    WD_SetProperty("Hide")
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Sad] GREETINGS, SLAVE UNIT 499323.[P] THIS UNIT HAS UNFORTUNATE NEWS ABOUT 778152.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] H-[P]huh?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Sad] W-[P]what...[P] what kind of news...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Sad] (Look at that face...[P] but I know how to cheer her up!)[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] UNIT 771852 TASKED THIS UNIT WITH DELIVERING A MESSAGE.[P] SHE REPORTS SHE IS WHOLLY UNABLE TO COMPREHEND HOW MUCH SHE LOVES YOU.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Uhhh...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] UNIT 771852 IS UNABLE TO FUNCTION PROPERLY UNLESS SHE KNOWS YOU LOVE HER.[P] WOULD YOU LIKE ME TO RELAY A MESSAGE TO HER?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] Oh, of course![P] Tell her I love her twice as much as she loves me![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Angry] CRITICAL ERROR.[P] UNIT 771852 STATED HER LOVE WAS EQUAL TO UNIT 499323'S LOVE MULTIPLIED BY TWO.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Angry] INFINITE LOOP -[P] SHUT DOWN.[P] ERROR.[P] ERROR![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Surprised] Drone![P] Drone, execute IR-719![P] Abort command![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] COMMAND RECEIVED.[P] THIS UNIT WILL REPORT THAT UNIT 499323 DOES NOT LOVE UNIT 771852![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Offended] Arrggh, you stupid drone![P] You're...[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] You're absolutely doing this on purpose.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Happy] [P]*Ahem*[P] ...[P] Yep.[P] You got me.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Laugh] I just couldn't help myself![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] Hee hee![P] So is that a disguise?[P] I should have recognized your accent.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Do you use that to break into places?[P] Sneak around all stealthy?[P] Nobody even knows?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Might be a good idea to shave off the hair then, even if it is really pretty.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] This isn't a disguise.[P] I really am a Drone Unit.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Surprised] Did you get repurposed?[P] Oh no![P] We -[P] we can fix this, don't worry![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Laugh] Oh, don't panic.[P] Watch this!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Flashwhite.
    Cutscene_CreateEvent("Flash Christine White", "Actor")
        ActorEvent_SetProperty("Subject Name", "Christine")
        ActorEvent_SetProperty("Flashwhite Quickly", "Null")
    DL_PopActiveObject()
    fnCutsceneBlocker()

    fnCutsceneWait(75)
    fnCutsceneBlocker()
    fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua") ]])
    fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Ta-[P]da![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] Christine, this is amazing![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] No wonder you're a super-spy![P] You can change forms and disguise yourself and fight people and outsmart everyone![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] D-[P]do me![P][CLEAR]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] What was that?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] N-[P]nothing![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Hee hee![P] Got a little carried away.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Offended] And don't frighten me like that again.[P] I thought you got caught and repurposed for a second there.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Offended] I was terrified that a bunch of security units were going to storm in here and grab me right then...[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Offended] ...[P] And I can't imagine what I'd do if you did get repurposed...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] I did undergo the process.[P] It's actually quite pleasurable.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Blush] Synthskin is several times more sensitive than the normal sensory suite.[P] Every movement, even the air rushing over my body...[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Okay, okay.[P] Sheesh, Christine, you're a real emotional earthquake, aren't you?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] It's been working out so far...[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Sad] Just don't let it be the real thing someday...[P] I don't think I could handle you being taken away...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] You have my word.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

-- |[Checking up]|
--Checking up on Sophie! Awww.
elseif(sTopicName == "Checking") then
        
    --Clean up remaining scene parts.
    WD_SetProperty("Hide")
        
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Just checking up on you.[P] Drone Units are programmed to maximize the comfort and productivity of other units.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] Awww![P] I'm doing great, thanks for your concern.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] I know you can't talk about it, but I hope everything is going well for your activities.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] We're doing our best out there.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] See you soon, Sophie.") ]])
    
-- |[Inspection!]|
elseif(sTopicName == "Inspection") then
        
    --Clean up remaining scene parts.
    WD_SetProperty("Hide")
        
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Blush] THIS UNIT IS IN NEED OF AN INSPECTION...[P] If that's okay with you.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Drone Units do not speak unless spoken to.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Blush] AFFIRMATIVE.[P] STANDING BY.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] (This is actually really fun![P] Because my processor would melt if it was anyone but Christine!)[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Unit, stand at attention and prepare for inspection.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Blush] AFFIRMATIVE.[P] UNIT WILL STAND AT ATTENTION.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --First time?
    local iLatexInspectionScene = VM_GetVar("Root/Variables/Chapter5/Sophie/iLatexInspectionScene", "N")
    if(iLatexInspectionScene == 0.0) then
        LM_ExecuteScript(fnResolvePath() .. "Inspection.lua", "First Time")
    else
        LM_ExecuteScript(fnResolvePath() .. "Inspection.lua", "Repeat")
    end

-- |[Nothing Special]|
elseif(sTopicName == "Nothing") then
        
    --Clean up remaining scene parts.
    WD_SetProperty("Hide")
    
    --Special:
    local iSteamDroid100RepState = VM_GetVar("Root/Variables/Chapter5/Scenes/iSteamDroid100RepState", "N")
    if(iSteamDroid100RepState == 1.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSteamDroid100RepState", "N", 2.0)
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Could you take a look at this with the micro-scanner?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] Did some Lord Unit pass off a delivery job on you?[P] Hee hee![B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] Hmm, it seems it was a disposal job, actually.[P] What was this thing?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] It is Series-X20 transducer.[P] Or was, evidently.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Surprised] It's basically done for.[P] Who dumped this job on you?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] JX-101, if you must know.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] Erm...[P] I'm searching my drives...[P] Where have I heard that name before?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Surprised] Wait, is she that Steam Droid that's on the shoot-on-sight notices?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Laugh] The same![P] I can attest that she's just as fierce in person.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Happy] Hee hee![P] I'll bet![B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] Well, Christine, I don't need to use the micro-scanner to tell you that transducer has seen its last days of service.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] [SOUND|World|TakeItem]So, why don't you take this one?[P] I'll just switch them and mark this one as scrapped.[P] Nobody will notice.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] I love the duplicity![B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] Hee hee![P] I hope this helps![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] If we can ally with the Steam Droids, we might have a real chance.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] My sexy maverick girlfriend...[P] Just be careful.[P] I've heard the caverns are dangerous.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] I will, dearest.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] Dearest?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Uhhhh...[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] No, it's okay.[P] I like it.[P] Good luck, dearest!") ]])
        return
    end
        
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Nothing special, I was just in the area.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Well, I'll say you're looking rather pert.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Pert?[P] Oh![P] You think they're bigger in this form?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] No, but synthskin has a higher compressability and you're strapped down.[P] So they look...[P] big...[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] Very big...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Blush] Touch me.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] Yeah...[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Mmmmm, tight and firm...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Blush] My skin is about twelve times more sensitive than golem skin...[P] Oooooohohhhhooo!![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] What if I were to rub them clockwise?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Blush] NNnnnnngghhh![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Well then.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Blush] Why did you stop?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Gotta save something for later, right?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Blush] You tease![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] You know it.[P] Hee hee!") ]])
    fnCutsceneBlocker()

end
