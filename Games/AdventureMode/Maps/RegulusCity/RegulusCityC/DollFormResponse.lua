-- |[Doll Form Response]|
--Called when talking to Sophie in Doll form.
local iSophieKnowsAboutRunestone = VM_GetVar("Root/Variables/Chapter5/Sophie/iSophieKnowsAboutRunestone", "N")
local iSeenDoll = VM_GetVar("Root/Variables/Chapter5/Sophie/iSeenDoll", "N")

--First time Sophie has seen non-golem Christine.
if(iSophieKnowsAboutRunestone == 0.0) then
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Sophie/iSophieKnowsAboutRunestone", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Sophie/iSeenDoll", "N", 1.0)
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Surprised") ]])
    fnCutscene([[ Append("Sophie:[E|Surprised] Command unit![P] I wasn't expecting an inspection![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Surprised] L-[P]l-[P]l-[P]lord Unit 771852 isn't here right now, but if you'd just wait a moment I could call her![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Blush] Unit 499323, are you all right?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] Um, no, command unit![P] I must apologize again, but you do bear a striking resemblance to...[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] To...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Blush] To?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] My authenticator chip software must be experiencing a bug.[P] What is your unit number?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Blush] Command Unit 771852, reporting for duty.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] Christine...[P] how is this even possible?[P] Is that you?[P] What happened?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] I could try to explain, but it doesn't make a lot of sense to me, so I don't think it'd help.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Well, uh, I can use this runestone to transform into any form I've been in before.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Surprised] You were a command unit before we met!?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] No.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Yes.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Time is a very complicated thing.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Offended] You were right, this isn't helping at all.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] So you can transform yourself, and you have the power to turn into a command unit.[P] Does the administration know?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] No, and let's keep it secret from those jerks.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Offended] Yeah, bunch of meanies.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] Your secret is safe with me, command unit![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Please, don't be so formal.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Hee hee![P] It's a reflex.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] We have to be very careful how we act around command units.[P] Their word is law.[P] They can order you repurposed on a whim.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Umm, Christine?[P] May I ask you a favour?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Anything for you![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Could we, maybe, roleplay, later?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] N-[P]not-[P]not like that but...[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] I get so nervous just seeing your chassis.[P] Your joints.[P] Your optical receptors.[P] It scares me, because command units scare me.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] And I want to practice not being afraid.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Certainly![P] I'd love to help![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Blush] And did you know?[P] This body is amazingly flexible.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] Whoo, of course it is![P] Hee hee![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] All right, I'll let you get back to your infiltration missions or whatever sneaky stuff you're doing.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] And thank you so much, Christine.") ]])
    fnCutsceneBlocker()

--First time as a Doll.
elseif(iSeenDoll == 0.0) then
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Sophie/iSeenDoll", "N", 1.0)
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Sophie:[E|Surprised] Command unit![P] I wasn't expecting an inspection![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Surprised] L-[P]l-[P]l-[P]lord Unit 771852 isn't here right now, but if you'd just wait a moment I could call her![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Blush] Unit 499323, are you all right?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] Um, no, command unit![P] I must apologize again, but you do bear a striking resemblance to...[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] To...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Blush] To?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] My authenticator chip software must be experiencing a bug.[P] What is your unit number?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Blush] Command Unit 771852, reporting for duty.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] Christine...[P] how is this even possible?[P] Is that you?[P] What happened?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] I could try to explain, but it doesn't make a lot of sense to me, so I don't think it'd help.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Well, uh, remember my runestone?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] But, didn't you say it was forms you've been in before?[P] How is this possible?[P] Were you a command unit before we met?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] No.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Yes.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Time is a very complicated thing.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Offended] You were right, this isn't helping at all.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] But you're so pretty, command unit![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] You can just call me Christine, silly.[P] No need to be formal.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] Oops![P] Hee hee![P] Reflex![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] We have to be very careful how we act around command units.[P] Their word is law.[P] They can order you repurposed on a whim.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Umm, Christine?[P] May I ask you a favour?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Anything for you![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Could we, maybe, roleplay, later?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] N-[P]not-[P]not like that but...[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] I get so nervous just seeing your chassis.[P] Your joints.[P] Your optical receptors.[P] It scares me, because command units scare me.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] And I want to practice not being afraid.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Certainly![P] I'd love to help![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Blush] And did you know?[P] This body is amazingly flexible.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] Whoo, of course it is![P] Hee hee![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] All right, I'll let you get back to your infiltration missions or whatever sneaky stuff you're doing.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] And thank you so much, Christine.") ]])

--Repeats.
else

    --Special:
    local iSteamDroid100RepState = VM_GetVar("Root/Variables/Chapter5/Scenes/iSteamDroid100RepState", "N")
    if(iSteamDroid100RepState == 1.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSteamDroid100RepState", "N", 2.0)
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] Hey Christine, you're looking radiant as ever.[P] What can I do for you?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Could you take a look at this with the micro-scanner?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] The -[P] piece of scrap you're holding?[P] Really?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] A Series-X20 transducer.[P] You can't tell?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Surprised] It's in awful shape![P] Where did you get that?[P] Who would send that piece of junk to us?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] JX-101, if you must know.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] Erm...[P] I'm searching my drives...[P] Where have I heard that name before?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Surprised] Wait, is she that Steam Droid that's on the shoot-on-sight notices?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Laugh] The same![P] I can attest that she's just as fierce in person.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Happy] Hee hee![P] I'll bet![B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] Well, Christine, I don't need to use the micro-scanner to tell you that transducer has seen its last days of service.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] [SOUND|World|TakeItem]So, why don't you take this one?[P] I'll just switch them and mark this one as scrapped.[P] Nobody will notice.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] I love the duplicity![B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] Hee hee![P] I hope this helps![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] If we can ally with the Steam Droids, we might have a real chance.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] My sexy maverick girlfriend...[P] Just be careful.[P] I've heard the caverns are dangerous.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] I will, dearest.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] Dearest?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Uhhhh...[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] No, it's okay.[P] I like it.[P] Good luck, dearest!") ]])
        return
    end

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Hello, Christine![P] Have I complimented you on your absolute physical perfection?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Laugh] Sophie, please![P] All robots are perfect in their own ways![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] I'd even be willing to extend that moniker to some organics.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] But you take it to a whole new level![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Blush] Sophie, stop...[P] I don't deserve a girl like you...") ]])
    fnCutsceneBlocker()

end
