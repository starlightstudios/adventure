-- |[Human Form Response]|
--Called when talking to Sophie in human form.
local iSophieKnowsAboutRunestone = VM_GetVar("Root/Variables/Chapter5/Sophie/iSophieKnowsAboutRunestone", "N")
local iSeenHuman = VM_GetVar("Root/Variables/Chapter5/Sophie/iSeenHuman", "N")

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[ ================================ Normal Conversation Path ================================ ]|
if(sTopicName == "No Special") then

    --First time Sophie has seen non-golem Christine.
    if(iSophieKnowsAboutRunestone == 0.0) then
        
        --Flag.
         VM_SetVar("Root/Variables/Chapter5/Sophie/iSophieKnowsAboutRunestone", "N", 1.0)
         VM_SetVar("Root/Variables/Chapter5/Sophie/iSeenHuman", "N", 1.0)
         
         --Variables.
        local iMetAmanda = VM_GetVar("Root/Variables/Chapter5/Scenes/iMetAmanda", "N")
        local iFinished198 = VM_GetVar("Root/Variables/Chapter5/Scenes/iFinished198", "N")
        local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
        local iCompletedSerenity = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedSerenity", "N")
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
        fnCutscene([[ Append("Sophie:[E|Surprised] O-[P]oh my![P] A human![B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] Oh, erm.[P] Are you with the breeding program?[P] You're supposed to have your day pass on your shirt if you are.[B][C]") ]])
        
        --Doesn't know what a day pass is.
        if(iMetAmanda == 0.0) then
            fnCutscene([[ Append("Christine:[E|Neutral] Day pass?[P] What's that?[B][C]") ]])
        
        --Does.
        else
            fnCutscene([[ Append("Christine:[E|Neutral] Day pass?[P] Oh yes, like what Amanda had.[P] Otherwise you probably think I'm a rogue human, don't you?[B][C]") ]])
        end
        
        --Rejoin.
        fnCutscene([[ Append("Sophie:[E|Surprised] ...[P] Christine?[P] Wait, no.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] Sorry, human, but you look a lot like someone I know.[P] And you have the same accent.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Happy] Ooh, I should call 771852 and have her convert you![P] Are you perhaps related?[P] Wouldn't it be great to be converted by your sister?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Sophie, I [P]*am*[P] 771852.[P] Don't you recognize me?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Surprised] ...[P] What?[P] How is this possible?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Well, I have this runestone here, and I don't know how, but I can just focus on myself and poof.[P] I'm a human again.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Laugh] In fact I can transform to any form I've been in before.[P] 55 wants to use it to break into places we're not supposed to be.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] [P]This isn't an elaborate prank?[P] You're not Christine's sister or something?") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()

        --Flashwhite.
        Cutscene_CreateEvent("Flash Christine White", "Actor")
            ActorEvent_SetProperty("Subject Name", "Christine")
            ActorEvent_SetProperty("Flashwhite Quickly", "Null")
        DL_PopActiveObject()
        fnCutsceneBlocker()

        fnCutsceneWait(75)
        fnCutsceneBlocker()
        fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua") ]])
        fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Lua variables.
        local iCounter = 0
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Does that answer your question?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Happy] *Oh my gosh!*[P] You're -[P] this is incredible![B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Blush] .............[P] Christine, I am truly sorry, and I know this must be the most amazing thing ever for you...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] It's not really that amazing...[P][CLEAR]") ]])
        fnCutscene([[ Append("Sophie:[E|Blush] But all I am thinking about right now is naughty things.[P] I'm so sorry![B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Blush] You could become so soft, and supple, and -[P] oh![P] My core's going to skip a cycle![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Blush] Please calm down, Sophie.[P] I just wanted to show you, not give you a system lock.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Blush] Oh, my dear 771852 is such a special unit![B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Blush] But you best not go showing that off anywhere else.[P] The administrators would take you away...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Don't worry, I've been careful to conceal it.[P] Only you and 2855 know.[B][C]") ]])
        if(iFinished198 == 1.0) then
            iCounter = iCounter + 1
            fnCutscene([[ Append("Christine:[E|Smirk] And some of the golems at the training center...[B][C]") ]])
        end
        if(iCompletedSerenity == 1.0) then
            iCounter = iCounter + 1
            fnCutscene([[ Append("Christine:[E|Smirk] And everyone at Serenity Crater Observatory...[B][C]") ]])
        end
        if(iSXUpgradeQuest >= 3.0) then
            iCounter = iCounter + 1
            fnCutscene([[ Append("Christine:[E|Smirk] And the Steam Droids...[B][C]") ]])
        end
        
        if(iCounter >= 2.0) then
            fnCutscene([[ Append("Christine:[E|Neutral] And probably some other people I forgot.[P] Yikes.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Blush] I'll keep your secret, don't worry.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] At this point it's hardly a secret.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Blush] Well that's -[P] Oh -[P] naughty thoughts again.[P] You'd best go before I turn off my inhibitors![B][C]") ]])
        else
            fnCutscene([[ Append("Sophie:[E|Blush] I'll keep your secret, don't worry.[P] Oh -[P] naughty thoughts again.[P] You'd best go before I turn off my inhibitors!") ]])
        end
        
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()

    --Sophie knows about transformation but has not seen human form yet.
    elseif(iSeenHuman == 0.0) then

        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Sophie/iSeenHuman", "N", 1.0)
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
        fnCutscene([[ Append("Sophie:[E|Surprised] O-[P]oh my![P] A human![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Hello, Unit 449323.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] Are you with the breeding program?[P] Remember you're supposed to have your security badge on.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] Didn't anyone tell you we're under orders to convert unidentified humans on sight?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] Unit 771852 didn't mention we're getting an intern or anything...[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Happy] Oh, oh![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] I suppose you figured it out, didn't you?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] Considering everything that's been happening lately, getting a human intern to help with repairs could really happen.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] They wouldn't be very efficient, but I've been talking to the other units.[P] Everyone is short-staffed.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] Lots of units getting reassigned for security duty.[P] Having breeding program interns fill in for them would make sense.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Still seems too dangerous.[P] With all the sparks flying around in here, we should let them have a less dangerous job.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Blush] C-[P]Christine, I -[P] phew...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] What's the matter?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Surprised] My programming is telling me to convert you.[P] Phew, woah![B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Surprised] Every 1/6th of a second it bumps 'Convert Rogue Human' up my priority list.[P] I can't disable the algorithm, either![B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Surprised] Here, put this security pass on![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] (It's actually just a metal clip...)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Better?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] So much better.[P] Hee hee![B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] It's a potent reflex.[P] I'm sure abductions units are used to it but I'm not.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] Gosh, how long has it been since I last saw a human?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] ...[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] Archive scan complete.[P] Last human sighted was Unit 602332, assigned to deliver a broken sunlamp.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Broken sunlamp?[P] So, she brought it from the biolabs?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] Probably.[P] That was years ago, she's likely a golem by now.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] Quick question...[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] Is your whole body squishy?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Not my bones or cartilage.[P] Plus, I'm mostly water, so under pressure I'll firm up.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Or pop like a meat balloon.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Blush] Just... wondering...") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()

    --Repeats.
    else

        --Special:
        local iSteamDroid100RepState = VM_GetVar("Root/Variables/Chapter5/Scenes/iSteamDroid100RepState", "N")
        if(iSteamDroid100RepState == 1.0) then
            VM_SetVar("Root/Variables/Chapter5/Scenes/iSteamDroid100RepState", "N", 2.0)
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
            fnCutscene([[ Append("Sophie:[E|Surprised] A hum-[P] ack, I did it again, 771852.[P] Sorry.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] No problem.[P] Hey, can you take a look at this?[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] The -[P] piece of scrap you're holding?[P] What is it?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] A Series-X20 transducer.[P] You can't tell?[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Surprised] It's in awful shape![P] Where did you get that?[P] Who would send that piece of junk to us?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] JX-101, if you must know.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] Erm...[P] I'm searching my drives...[P] Where have I heard that name before?[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Surprised] Wait, is she that Steam Droid that's on the shoot-on-sight notices?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Laugh] The same![P] I can attest that she's just as fierce in person.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Happy] Hee hee![P] I'll bet![B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] Well, Christine, I don't need to use the micro-scanner to tell you that transducer has seen its last days of service.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Smirk] [SOUND|World|TakeItem]So, why don't you take this one?[P] I'll just switch them and mark this one as scrapped.[P] Nobody will notice.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] I love the duplicity![B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Smirk] Hee hee![P] I hope this helps![B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] If we can ally with the Steam Droids, we might have a real chance.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Smirk] My sexy maverick girlfriend...[P] Just be careful.[P] I've heard the caverns are dangerous.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] I will, dearest.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] Dearest?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Uhhhh...[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Smirk] No, it's okay.[P] I like it.[P] Good luck, dearest!") ]])
            return
        end
        
        --Variables.
        local iSpecialHumanDate = VM_GetVar("Root/Variables/Chapter5/Sophie/iSpecialHumanDate", "N")
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
        fnCutscene([[ Append("Sophie:[E|Surprised] A hum-[P] oh, sorry, 771852.[P] It's kind of a reflex.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] I know, I got the same programming.[P] It's difficult to suppress.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] Mmm, if we had spare nanofluid, I'd convert you, right here and now.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] Watching your body and mind get mechanized...[P] it's making my power core overclock...[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Blush] Hey, do you find me attractive, even though you're a human?[P] Or are you only attracted to other humans?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] *kiss*[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Blush] You're the most beautiful thing in the world to me.[P] Human, golem, no matter.[P] Love is not constrained by form.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Blush] Awwwww...[B][C]") ]])
        if(iSpecialHumanDate == 0.0) then
            fnCutscene([[ Append("Christine:[E|Smirk] Actually, Sophie...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Blush] I just had...[P] an idea...[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] That look on your face...[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Blush] Computing odds that I will not like this idea...[P] Done![P] Three in four![B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] Oh come on, you know that chance computation algorithm is filled with bugs and oversights.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Smirk] Well then, tell me what you have planned![BLOCK]") ]])

            --Decision script is this script. It must be surrounded by quotes.
            local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
            fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Special Date\", " .. sDecisionScript .. ", \"SpecialDate\") ")
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Not Right Now\",  " .. sDecisionScript .. ", \"NoDate\") ")
            fnCutsceneBlocker()
        
        else
            fnCutscene([[ Append("Christine:[E|Blush] Sophie, do you remember when you had me converted...[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Happy] A very special day I have marked in my memory drives. I will never, ever overwrite it.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Blush] Were you thinking of doing something so crazy again?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Blush] Let's just reminisce about it? In graphic detail.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Smirk] Oh, okay! Would you like to do that right now?[BLOCK]") ]])

            --Decision script is this script. It must be surrounded by quotes.
            local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
            fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Special Date\", " .. sDecisionScript .. ", \"SpecialDateRepeat\") ")
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Not Right Now\",  " .. sDecisionScript .. ", \"NoDateRepeat\") ")
            fnCutsceneBlocker()
        end

    end

-- |[ =================================== Decision Responses =================================== ]|
--Start the special date.
elseif(sTopicName == "SpecialDate") then
	WD_SetProperty("Hide")
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Sophie/iSpecialHumanDate", "N", 100.0)
    VM_SetVar("Root/Variables/Chapter5/Sophie/iHumanFirstScene", "N", 0.0)
    VM_SetVar("Root/Variables/Chapter5/Sophie/iHumanSecondScene", "N", 0.0)
    VM_SetVar("Root/Variables/Chapter5/Sophie/iHumanThirdScene", "N", 0.0)
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Smirk") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Smirk") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Okay, here's what we do...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] *whisper*[P] *whisper*[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Oh?[P] Oh?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] *whisper*[P] *whisper*[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Ohhhhhh.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] This is dangerous,[P] and stupid,[P] and a terrible idea that might get both of us repurposed for purely selfish reasons.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] When do we start?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Why not right now?[P] Get your part set up and message my PDU when you're all set.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneWait(75)
    fnCutscene([[ AL_SetProperty("Activate Fade", 75, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    fnCutscene([[ AL_BeginTransitionTo("RegulusCityBAlt", "FORCEPOS:27.0x25.0x0") ]])
    fnCutsceneBlocker()
    
--Start the special date, repeat.
elseif(sTopicName == "SpecialDateRepeat") then
	WD_SetProperty("Hide")
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Sophie/iSpecialHumanDate", "N", 100.0)
    VM_SetVar("Root/Variables/Chapter5/Sophie/iHumanFirstScene", "N", 0.0)
    VM_SetVar("Root/Variables/Chapter5/Sophie/iHumanSecondScene", "N", 0.0)
    VM_SetVar("Root/Variables/Chapter5/Sophie/iHumanThirdScene", "N", 0.0)
    VM_SetVar("Root/Variables/Chapter5/Sophie/iHumanRepeat", "N", 1.0)
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Smirk") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Smirk") ]])
    fnCutscene([[ Append("Christine:[E|Blush] Tell me every little detail.[P] Don't leave anything out.[P] Even the parts you didn't see.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Blush] This will be like an exact re-creation in my mind...[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] Well then listen closely!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneWait(75)
    fnCutscene([[ AL_SetProperty("Activate Fade", 75, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    fnCutscene([[ AL_BeginTransitionTo("RegulusCityBAlt", "FORCEPOS:27.0x25.0x0") ]])
    fnCutsceneBlocker()

--Not right now.
elseif(sTopicName == "NoDate") then
	WD_SetProperty("Hide")
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Smirk") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Smirk") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] I had better go set a few things up first.[P] I won't keep you waiting long.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Offended] You are such a dirty tease![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] You love it.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] I do![P] Don't keep my waiting, because if my imagination routines come up with something better I might just make you do that instead![P] Hee hee!") ]])
    fnCutsceneBlocker()

--Not right now.
elseif(sTopicName == "NoDateRepeat") then
	WD_SetProperty("Hide")
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Smirk") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Smirk") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] I really, really want to, but the mission takes priority.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] Oh, all right.[P] I was thinking maybe I'd upload my memories to a videograph so you could watch it![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] But keeping that around would be pretty risky, considering.[P] So it'll have to wait.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] That's a great idea, Sophie![P] Until next time!") ]])
    fnCutsceneBlocker()


end
