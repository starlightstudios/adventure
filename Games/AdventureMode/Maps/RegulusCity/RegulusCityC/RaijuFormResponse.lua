-- |[Raiju Form Response]|
--Called when talking to Sophie in human form.
local iSophieKnowsAboutRunestone = VM_GetVar("Root/Variables/Chapter5/Sophie/iSophieKnowsAboutRunestone", "N")
local iSeenRaiju = VM_GetVar("Root/Variables/Chapter5/Sophie/iSeenRaiju", "N")

--First time Sophie has seen non-golem Christine.
if(iSophieKnowsAboutRunestone == 0.0) then
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Sophie/iSophieKnowsAboutRunestone", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Sophie/iSeenRaiju", "N", 1.0)
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Surprised") ]])
    fnCutscene([[ Append("Sophie:[E|Surprised] Christine![P] What!?[P] How!?[P] What!?[P] How again!?[P] Really, how first, and then what!?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Sad] Calm down, Sophie![P] I know this might - [B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Be...[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Offended] Don't you dare![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Be...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Laugh] Shocking![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] We both knew it was going to happen, but we were powerless to stop it.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] But seriously, last I saw you, you weren't a raiju.[P] Is this a new program we're trying out?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] I'm not upset, just confused, really.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Okay, I'll try to explain.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] I have a runestone that I think is super-magic.[P] Like magic, but a bit beyond our academic concept of magic.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] It lets me transform into any form I've been in before, including human, so I can be transformed again.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] I know so little about magic that I am prepared to believe this uncritically.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Really?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] I practiced levitating a wrench once.[P] The best I could do was moving it a little without touching it.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Offended] Which is less than what I did with my arm, so checkmate, wizards.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Anyway, this is great news![P] Though I assume you need me to keep it secret.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Naturally.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] C-[P]c-[P]can I nuzzle your tail?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Surprised] [SOUND|World|SparksC]*Bzzzt*[P] Yipes![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Scared] Sophie, are you all right?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] Cancel the chemfuel, my systems are charged past maximum![P] Yawoo![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] Christine, I have to work off all this extra energy![P] Good luck out there BAZOO okaygottago!") ]])
    fnCutsceneBlocker()

--First time as a Raiju.
elseif(iSeenRaiju == 0.0) then
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Sophie/iSeenRaiju", "N", 1.0)
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Sophie:[E|Offended] Christine![P] Have you been cheating on me!?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Scared] No, Sophie![P] My heart is yours and yours alone![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Offended] You mean your power core is mine and mine alone.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] No, heart.[P] I'm an organic right now.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Offended] I'm not letting you off on a technicality![P] Answer my question![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Sad] Uh, I did, though.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Offended] Getting transformed into a raiju requires intercourse with a raiju![P] I know this because...[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] Because of some videographs I saw...[P] one time...[P] and I didn't enjoy them![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Sad] No, Sophie, believe me.[P] I'm telling the truth.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Sad] Okay well I did have intercourse with a Raiju, but it was your idea, and it also hasn't happened yet.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Offended] ...[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] ...[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] ...[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] ...[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] ...[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Hold on, what?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Blush] I did a bit of...[P] time travel, I guess?[P] Just, don't act different when we get there in this timeline because it might cause a paradox.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] You know, telling me this is because of time travel is so out there I'm primed to believe it.[P] Because, what the hell kind of lie is that?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Besides, you're not much of a liar.[P] And I wouldn't be mad at you if you slept with another robot, or...[P] raiju.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Sad] You wouldn't?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] No, because I didn't say you couldn't, after all.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Just, I don't know, ask my permission first.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] (And maybe I can watch, hee hee!)[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Oh, capital![P] I was worried that you wouldn't believe me.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] (I could hold her as the raiju licks her...[P] or just stand back...)[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Sophie?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] Christine, quickly, can I nuzzle your tail?[P] Please?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Surprised] [SOUND|World|SparksB]*Bzzzt*[P] Yipes![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Scared] Sophie, are you all right?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] Cancel the chemfuel, my systems are charged past maximum![P] Yawoo![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] Christine, I have to work off all this extra energy![P] Good luck out there BAZOO okaygottago!") ]])

--Repeats.
else

    --Special:
    local iSteamDroid100RepState = VM_GetVar("Root/Variables/Chapter5/Scenes/iSteamDroid100RepState", "N")
    if(iSteamDroid100RepState == 1.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSteamDroid100RepState", "N", 2.0)
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] Hey Christine, you're looking lovely as ever.[P] What can I do for you?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Could you take a look at this with the micro-scanner?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] The -[P] piece of scrap you're holding?[P] Really?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] A Series-X20 transducer.[P] You can't tell?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Surprised] It's in awful shape![P] Where did you get that?[P] Who would send that piece of junk to us?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] JX-101, if you must know.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] Erm...[P] I'm searching my drives...[P] Where have I heard that name before?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Surprised] Wait, is she that Steam Droid that's on the shoot-on-sight notices?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Laugh] The same![P] I can attest that she's just as fierce in person.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Happy] Hee hee![P] I'll bet![B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] Well, Christine, I don't need to use the micro-scanner to tell you that transducer has seen its last days of service.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] [SOUND|World|TakeItem]So, why don't you take this one?[P] I'll just switch them and mark this one as scrapped.[P] Nobody will notice.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] I love the duplicity![B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] Hee hee![P] I hope this helps![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] If we can ally with the Steam Droids, we might have a real chance.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] My sexy maverick girlfriend...[P] Just be careful.[P] I've heard the caverns are dangerous.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] I will, dearest.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] Dearest?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Uhhhh...[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] No, it's okay.[P] I like it.[P] Good luck, dearest!") ]])
        return
    end

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Hello, Christine![P] Could I perhaps trouble you for a pick-me-up?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Laugh] One tail nuzzle, coming up![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Surprised] [SOUND|World|SparksA]*Bzzzt*[P] Wahoo![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] Yeah yeah yeah yeahyeahyeah![P] I'm going to break every speed record![P] Wazimbo!") ]])
    fnCutsceneBlocker()

end
