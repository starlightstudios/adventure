-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Other Examinables]|
--Maintenance scanning terminal.
if(sObjectName == "ScanTerminal") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Christine](A scanning terminal.[P] This is where units upload their physical and mental data when being maintained.[P] Better not touch it, Sophie's the expert on these.)") ]])
	fnCutsceneBlocker()
	
--Crates.
elseif(sObjectName == "Crates") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Christine](Crates of various sizes.[P] Some of the labels are spare parts, while others are machines here to be repaired.)") ]])
	fnCutsceneBlocker()

-- |[Date Objects]|
--Objects the player can maintain.
elseif(sObjectName == "WorkObjects") then

	--Variables.
	local iIsOnDate             = VM_GetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N")
	local iExplainedMaintenance = VM_GetVar("Root/Variables/Chapter5/Sophie/iExplainedMaintenance", "N")
    local iIsGalaTime           = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N")
    local iStartedShoppingSequence = VM_GetVar("Root/Variables/Chapter5/Scenes/iStartedShoppingSequence", "N")

    --Gala.
    if(iIsGalaTime > 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutscene([[ Append("Sophie:[E|Smirk] Christine...[P] after all this...[P] will we come back here?[P] Will we work with these tools together again?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] We will.[P] We will, as free units.[P] I promise you.[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Smirk] I'll hold you to that...") ]])

	--Not on a date. Examine the stuff.
	elseif(iIsOnDate == 0.0 or iStartedShoppingSequence == 1.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[VOICE|Christine](Equipment in various states of disrepair, and tools to fix them with.)") ]])
		fnCutsceneBlocker()

	--Needs to explain maintenance.
	elseif(iIsOnDate == 1.0 and iExplainedMaintenance == 0.0) then
		VM_SetVar("Root/Variables/Chapter5/Sophie/iExplainedMaintenance", "N", 1.0)
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] So this is what you work on?[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Most of the time.[P] I get equipment brought in here and fix it.[P] Sometimes I work on site if the machines are too heavy.[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Do you want to spend some time working on them?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] You'd consider that a good time?[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] I really like my job, actually.[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Plus we can just let our subroutines do the work while we talk.[P] Get to know each other better.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Splendid![P] Shall we spend our evening here, then?[BLOCK]") ]])

		--Decision script is this script. It must be surrounded by quotes.
		local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
		fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
		fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"Work\") ")
		fnCutscene(" WD_SetProperty(\"Add Decision\", \"No Thanks\",  " .. sDecisionScript .. ", \"NoWork\") ")
		fnCutsceneBlocker()

	--Date option.
	elseif(iIsOnDate == 1.0 and iExplainedMaintenance == 1.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Shall we spend our evening performing our functions?[BLOCK]") ]])

		--Decision script is this script. It must be surrounded by quotes.
		local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
		fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
		fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"Work\") ")
		fnCutscene(" WD_SetProperty(\"Add Decision\", \"No Thanks\",  " .. sDecisionScript .. ", \"NoWork\") ")
		fnCutsceneBlocker()
	end

--Post-decision.
elseif(sObjectName == "Work") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Variables.
	local iSophieFirstDateState = VM_GetVar("Root/Variables/Chapter5/Scenes/iSophieFirstDateState", "N")
	local iSophieWillSynchronizeNow = VM_GetVar("Root/Variables/Chapter5/Sophie/iSophieWillSynchronizeNow", "N")

	--Fade.
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(85)
	fnCutsceneBlocker()

	--Execute the conversation routing case.
	LM_ExecuteScript(gsRoot .. "Chapter 5/Scenes/500 Normal/Sophie Conversation/Z Routing.lua")
	fnCutsceneBlocker()
	
	--Position characters if Sophie is not interested in synchronizing.
	if(iSophieWillSynchronizeNow == 0.0 or iSophieFirstDateState == 3.0) then
		fnCutsceneTeleport("Christine", 23.25, 10.10)
		fnCutsceneTeleport("Sophie", 23.25, 12.50)
		fnCutsceneFace("Christine", 0, 1)
		fnCutsceneFace("Sophie", 0, -1)
	end

	--Fade.
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
	fnCutsceneWait(85)
	fnCutsceneBlocker()
	
	--First date, changes dialogue and events.
	if(iSophieFirstDateState == 0.0) then
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSophieFirstDateState", "N", 1.0)
		VM_SetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N", 2.0)
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Wow, the day really flew by![P] That was a lot of fun![B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] A bit unorthodox for a first date but I enjoyed it plenty![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Ummm...[B][C]") ]])
        
		fnCutscene([[ Append("Christine:[E|Neutral] Date?[P] that wasn't a date, if I knew it was a date I would have done something nicer![B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Blush] Uh, uh![P] No, I was just showing you around the repair bay![P] Hee hee![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|PDU] Matchmaker -[P] shut up, PDU![B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Blush] Matchmaker?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Offended] It's my stupid PDU.[P] It said you were ogling me with your big pretty eyes and - [B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Offended] You keep flicking your hair a bit when you talk.[P] You keep looking at me.[P] I can't take my eyes off you.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Offended] What is your deal?[P] We're going to be working together so you may as well just tell me why you don't like me![B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Sad] Don't like you?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Offended] Yeah![P] Am I ugly, or weird, or something?[P] As a lord golem I order you to tell me![B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] ...[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Do you want to kiss me?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Offended] Of course![B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] *smooch*[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Blush] H-[P]huh?[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Happy] Want another one?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Blush] Yeah![B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] *smooch*[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Blush] My goodness, Sophie, Unit 499323, do you like me?[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Happy] Of course I do![P] I like you a lot![P] How do you not see that?[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Happy] I was holding your hand for half of that![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Blush] You were just -[P] guiding me while I got used to the tools![B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Offended] You're not fooling anyone.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] I tried, but...[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] Do you...[P] want to be my tandem unit?[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Happy] I thought you'd never ask![P] Of course![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] But I'm your lord golem...[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Blush] Yeah, but I don't care what anyone else thinks![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] What does that have to do with it?[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] We're different model variants.[P] Slave and lord, we're not supposed to date.[P] You didn't know?[P] Well, you are a fresh convert so...[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Yes, we'll be looked down on.[P] But I don't care.[P] I want you.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] I want you too![B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Offended] Darn it.[P] Now we can't have weeks of will-they-won't-they sexual tension...[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Sorry.[P] I can pretend not - [B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Offended] Oh no you don't![B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Happy] Phew![P] What a day it has been![P] I really need a recharge![P] Would you like to walk me back to my room?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] Of course.[P] The habitation block is just east of the surface exit, right?[P] Let's go!") ]])
        
        
        
		--[=[fnCutscene([[ Append("Sophie:[E|Neutral] Hee hee![P] You haven't even seen your quarters yet, have you?[P] Do you know where they are?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Vaguely.[P] I didn't download the directory when I got here.[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Well then allow me to show you.[P] We passed the elevators on the way here, they're on the east side of the sector.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Thanks, Sophie.[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] This does count as walking a lady back to her room.[P] We use the same elevators, of course.[P] All units in the sector use the same domicile block.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] All right.[P] Let's go.") ]])]=]
		fnCutsceneBlocker()
	
	--Successive dates: Sophie immediately returns to the maintenance bay if she doesn't want to synchronize.
	elseif(iSophieWillSynchronizeNow == 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] I never thought I'd have such a good time working with you![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] What's that supposed to mean?[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Hee hee![P] I was really nervous when you were assigned as my Lord Golem.[P] I guess I was just being silly.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Everything is going to be all right.[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Yeah, it is, isn't it?[P] Maybe things are going to work out.[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] You go ahead and defragment, I'll clean up here.[P] Good night, Christine.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Good night, Sophie.") ]])
		fnCutsceneBlocker()
		VM_SetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N", 3.0)
		
		--Remove Sophie from the party.
		giFollowersTotal = 0
		gsaFollowerNames = {}
		giaFollowerIDs = {0}
		AL_SetProperty("Unfollow Actor Name", "Sophie")
		
		--Set Sophie's properties.
		EM_PushEntity("Sophie")
			TA_SetProperty("Clipping Flag", true)
			TA_SetProperty("Activation Script", fnResolvePath() .. "Dialogue.lua")
		DL_PopActiveObject()
	
	--If she wants to synchronize, keep the flag at 2.0.
	else
		VM_SetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N", 2.0)
	
    end

elseif(sObjectName == "Birb") then
    local iCanSpawnBirdcage = VM_GetVar("Root/Variables/Chapter5/Scenes/iCanSpawnBirdcage", "N")
    if(iCanSpawnBirdcage == 1.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Herbert's living quarters.[P] He's fumbling around, pecking at things, and testing his wing.[P] He seems happy, for a bird.)") ]])
        fnCutsceneBlocker()
    end

-- |[Common Clearing]|
elseif(sObjectName == "NoWork") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
-- |[Exit Ladder]|
elseif(sObjectName == "Ladder") then
    
	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("RegulusCityG", "FORCEPOS:17.0x16.0x0")

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
