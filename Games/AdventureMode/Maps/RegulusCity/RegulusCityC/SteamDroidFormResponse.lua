-- |[ =============================== Steam Droid Form Response ================================ ]|
--Called when talking to Sophie in Steam Droid form.
local iSeenSteamDroid            = VM_GetVar("Root/Variables/Chapter5/Sophie/iSeenSteamDroid", "N")
local iCompletedSprocketCity     = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedSprocketCity", "N")
local iSophieKnowsAboutRunestone = VM_GetVar("Root/Variables/Chapter5/Sophie/iSophieKnowsAboutRunestone", "N")

-- |[ ====================================== No Runestone ====================================== ]|
--First time Sophie has seen non-golem Christine.
if(iSophieKnowsAboutRunestone == 0.0) then
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Sophie/iSophieKnowsAboutRunestone", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Sophie/iSeenSteamDroid", "N", 1.0)
    
    --Normal case:
    if(iCompletedSprocketCity == 1.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Happy") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] So that's what the plan was, eh?[P] Hee hee![P] You look good as a Steam Droid![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Thanks![P] I designed me...[P] myself![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] It doesn't have all the conveniences of a Golem, but it's a custom work.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] But I mean, no auto-stabilizers.[P] Primitive power system.[P] Corrosion problems.[P] No stasis locks.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] I have to rely on gyroscopes for dodging.[P] I have low optical fidelity.[P] The list goes on.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] But you're proud of how you turned out?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Happy] Absolutely![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Happy] I'd love to go back to the blueprints and give it another shot, but it'd be a hobby thing at this point.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Blush] Maybe I could work on you a bit...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] And make another steam lord?[P] No, I think we'll save the parts for other Steam Droids.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] Yes, good thinking.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] I haven't really improved the design too much, but I think I could streamline the upgrade process.[P] It's going to take a few iterations to get it down.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] Plus requisitioning the parts discreetly might be tricky.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] SX-399 was the only critical case.[P] I'm sure there's lot of Steam Droids who could use an upgrade, but we shouldn't compromise ourselves.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] 55 said you can transform yourself back, right?") ]])
        fnCutsceneBlocker()
    
    --NC+ only:
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Happy") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
        fnCutscene([[ Append("Sophie:[E|Surprised] Christine![P] You -[P] no, sorry.[P] You just look very much like my tandem unit.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] Not to be rude but you really can't be in here, if a lord unit sees you -[P] other than Christine -[P] they'll sic security after you.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] There's a drainpipe on the transit trackline you can use to get out.[P] Go![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Hello, Sophie.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Blush] Wh-[P]wh-[P]whaaaat?[P] Is that you?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Save your surprise for the next part, it's going to blow your processor.") ]])
        fnCutsceneBlocker()
    end

    --Flashwhite.
    Cutscene_CreateEvent("Flash Christine White", "Actor")
        ActorEvent_SetProperty("Subject Name", "Christine")
        ActorEvent_SetProperty("Flashwhite Quickly", "Null")
    DL_PopActiveObject()
    fnCutsceneBlocker()

    fnCutsceneWait(75)
    fnCutsceneBlocker()
    fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua") ]])
    fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Normal case:
    if(iCompletedSprocketCity == 1.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Happy") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] Keen![B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Blush] Did I just say that?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Laugh] SX-399 must have rubbed off on you![B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] That's what she said before she ran off.[P] How is she?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Everything went smoothly, more or less.[P] There was a close-call but love won out in the end.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Happy] It's so much fun doing this spy stuff -[P] if you need anything else from me, you let me know![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Gladly.") ]])
        fnCutsceneBlocker()
    
    --NC+.
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Happy") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
        fnCutscene([[ Append("Sophie:[E|Surprised] You weren't kidding![P] What did you do!?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Blush] T-[P]tell me everything![P] Gosh, this is so cool...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Happy] I'm so glad you like it![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] It's a thing I do with my runestone here.[P] I can change forms to any form I've been before, and...[P] some I haven't.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Offended] I said to tell me everything and you're being cryptic?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] No, Sophie, I'm really sorry, but I actually do mean that.[P] I never was a steam droid, but, I had...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] I guess it's like a dream.[P] Or a vision.[P] And I was a stream droid in it, and then...[P] I could transform.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Offended] So you really don't know?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] No.[P] Sorry, I'm being forthright.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Sad] I was...[P] just thinking that maybe it'd be possible...[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Sad] I have friends who got...[P] turned into drones and...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] It only works for me, Sophie.[P] I'm sorry.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Sad] I thought so...[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] Just don't let anyone see you, my cute sexy girlfriend spy.[P] You might have to beat them up with your cool martial arts![B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Happy] Yeah, master of disguise![P] Anyone could be Christine![P] Pow![P] Wham![P] Go get 'em!") ]])
        
    end

-- |[ =================================== Has Seen Runestone =================================== ]|
--First time as a Steam Droid. No transformation at the end.
elseif(iSeenSteamDroid == 0.0) then
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Sophie/iSeenSteamDroid", "N", 1.0)
    
    --Normal case:
    if(iCompletedSprocketCity == 1.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Happy") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
        fnCutscene([[ Append("Sophie:[E|Surprised] Huh!? -[P][EMOTION|Sophie|Smirk] oh, it's you, Christine.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] So that's our mutual friend's plan was, eh?[P] Hee hee![P] You look good as a Steam Droid![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Thanks![P] I designed me...[P] myself![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] It doesn't have all the conveniences of a Golem, but it's a custom work.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] But I mean, no auto-stabilizers.[P] Primitive power system.[P] Corrosion problems.[P] No stasis locks.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] I have to rely on gyroscopes for dodging.[P] I have low optical fidelity.[P] The list goes on.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] But you're proud of how you turned out?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Happy] Absolutely![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Happy] I'd love to go back to the blueprints and give it another shot, but it'd be a hobby thing at this point.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Blush] Maybe I could work on you a bit...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] And make another steam lord?[P] No, I think we'll save the parts for other Steam Droids.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] Yes, good thinking.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] I haven't really improved the design too much, but I think I could streamline the upgrade process.[P] It's going to take a few iterations to get it down.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] Plus requisitioning the parts discreetly might be tricky.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] SX-399 was the only critical case.[P] I'm sure there's lot of Steam Droids who could use an upgrade, but we shouldn't compromise ourselves.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] Oh, 55 swore me to secrecy but -[P] well.[P] I can't keep secrets from you.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] She said she was glad that you used your transformation ability so cleverly.[P] Hee hee![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Laugh] Of course she'd demand secrecy![P] Can't break the facade for even a second![B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] How is she?[P] And SX-399?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] They get along pretty well.[P] 55 is...[P] well, I'm working on getting her to open up.[P] It's tough.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Happy] It's so much fun doing this spy stuff -[P] if you need anything else from me, you let me know![B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Happy] And say hi for me to SX-399 if you see her again.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Gladly.") ]])
        fnCutsceneBlocker()
        
    --NC+ case:
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Happy") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
        fnCutscene([[ Append("Sophie:[E|Surprised] Huh!?[P] -[P] Christine, you -[P] you can't be that, you can't let anyone see you![B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] Though I love the design.[P] There's clearly some newer parts in you but you still have that retro look.[P] Well done.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Thanks![P] I designed me...[P] myself![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] It doesn't have all the conveniences of a Golem, but it's a custom work.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] But I mean, no auto-stabilizers.[P] Primitive power system.[P] Corrosion problems.[P] No stasis locks.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] I have to rely on gyroscopes for dodging.[P] I have low optical fidelity.[P] The list goes on.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] But you're proud of how you turned out?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Happy] Absolutely![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Happy] I'd love to go back to the blueprints and give it another shot, but it'd be a hobby thing at this point.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Blush] Maybe I could work on you a bit...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Blush] That'd be quite a date.[P] I'd love to have you inside me...[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Surprised] L-[P]language![P] Hee hee![B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Blush] I -[P] want to be inside you, too![P] Hee hee hee hee!") ]])
        fnCutsceneBlocker()
    end

--Repeats.
else

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Out of curiosity, did you need the goggles for something?[P] Aren't your eyes glare-proof?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Steam Droids don't have all the useful bits, unfortunately.[P] These come in handy in case of gas or steam leaks.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Practical and pretty -[P] that's my Christine!") ]])
    fnCutsceneBlocker()

end
