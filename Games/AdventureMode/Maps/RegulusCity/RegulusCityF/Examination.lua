-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Execution]|
--Exit
if(sObjectName == "ToCityE") then
	
	--If 55 is present, remove her.
	local iIs55Following = VM_GetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N")
    local iSaw55sMemories = VM_GetVar("Root/Variables/Chapter5/Scenes/iSaw55sMemories", "N")
    local iSaw55PostLRT   = VM_GetVar("Root/Variables/Chapter5/Scenes/iSaw55PostLRT", "N")
	if(iIs55Following == 1.0) then
		
        --Post LRT scene:
        if(iSaw55sMemories == 1.0 and iSaw55PostLRT == 0.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
            fnCutscene([[ Append("55:[E|Neutral] Christine, please report to your normal work assignments.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] I downloaded what I could from the core and set up a back door into the network.[P] I will review what footage I can.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] Are you...[P] okay?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] ...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Report to your normal work assignments.[P] I will contact you when I have determined our next move.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] 55, come on...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Do not waste processor cycles worrying.[P] Focus on maintaining your cover.[P] Move out.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] ...") ]])
            fnCutsceneBlocker()
            
            --Remove 55 from the following group.
            giFollowersTotal = 0
            gsaFollowerNames = {}
            giaFollowerIDs = {0}
            --AdvCombat_SetProperty("Party Slot", 1, "Null")
            AL_SetProperty("Unfollow Actor Name", "Tiffany")

            --Flag to indicate she is not following Christine.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 0.0)
            
            --Flag for the next part of the cutscene.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iSaw55PostLRT", "N", 1.0)
            
            --Change map to Christine's quarters. This will fire the next part of the scenario.
            fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
            fnCutscene([[ AL_BeginTransitionTo("RegulusCityZ", "FORCEPOS:8.0x10.0x0") ]])
            VM_SetVar("Root/Variables/Chapter5/Scenes/iIsTramHere", "N", 0.0)
            return
        
        --Normal:
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("55:[VOICE|Tiffany] It is best if I'm not seen on the main floor cameras.[P] I'll be in touch.") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(20)
            fnCutsceneBlocker()
            
            --Remove 55's sprite. She remains in the combat party for equipment reasons.
            giFollowersTotal = 0
            gsaFollowerNames = {}
            giaFollowerIDs = {0}
            AL_SetProperty("Unfollow Actor Name", "Tiffany")

            --Flag to indicate she is not following Christine.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 0.0)
        end
	end
	
	fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
	fnCutscene([[ AL_BeginTransitionTo("RegulusCityE", "FORCEPOS:16.0x4.0x0") ]])
	VM_SetVar("Root/Variables/Chapter5/Scenes/iIsTramHere", "N", 0.0)

--Terminal. Summons the train if it's not here.
elseif(sObjectName == "Terminal") then

	--Variables.
	local iIsTramHere              = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsTramHere", "N")
	local iMet55InLowerRegulus     = VM_GetVar("Root/Variables/Chapter5/Scenes/iMet55InLowerRegulus", "N")
	local iIsOnDate                = VM_GetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N")
    local iStartedShoppingSequence = VM_GetVar("Root/Variables/Chapter5/Scenes/iStartedShoppingSequence", "N")
    local b254SophieLeading        = fnIsSophieLeader()
	
    --Sophie can't use the tram.
    if(b254SophieLeading == true) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[VOICE|Sophie](No time for this nonsense, I had better get Christine fixed!)") ]])
		fnCutsceneBlocker()
    
	--Christine can't use the tram.
	elseif(iMet55InLowerRegulus == 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[VOICE|Christine](This console can summon the tram, but I don't have any reason to leave sector 96.)") ]])
		fnCutsceneBlocker()
	
	--Sophie can't go on the tram with Christine. Bypassed during the shopping sequence.
    elseif((iIsOnDate == 1.0 or iIsOnDate == 2.0) and iStartedShoppingSequence ~= 1.0) then
    
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Oh, do you want to take the tram somewhere?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Errr...[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] I'm afraid that might be a bad idea.[P] Unless we have an explicit assignment, Slave Units are not supposed to be using the tram network.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] But I could simply give you an order, and you'd have to follow it, right?[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Yes, but then [P]*you*[P] would be under scrutiny.[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] It is never a good idea to be under the administrator's scrutiny.[P] If you follow my meaning.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] I do.") ]])
		fnCutsceneBlocker()
	
	--Tram is already here.
	elseif(iIsTramHere == 1.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[VOICE|Christine](The tram is here already, no need to access this console.)") ]])
		fnCutsceneBlocker()

	--Tram is going to arrive soon.
	else
	
		--Set flags.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iIsTramHere", "N", 1.0)
		VM_SetVar("Root/Variables/Chapter5/Scenes/sCurrentTramArea", "S", "RegulusCityF")
	
		--Spawn the tram parts.
		TA_Create("TramA")
			TA_SetProperty("Position", 33, 6)
			TA_SetProperty("Move Frame", gci_Face_West, 0, "Root/Images/Sprites/Tram/West|0")
			TA_SetProperty("Move Frame", gci_Face_West, 1, "Root/Images/Sprites/Tram/West|0")
			TA_SetProperty("Move Frame", gci_Face_West, 2, "Root/Images/Sprites/Tram/West|0")
			TA_SetProperty("Move Frame", gci_Face_West, 3, "Root/Images/Sprites/Tram/West|0")
			TA_SetProperty("Facing", gci_Face_West)
			TA_SetProperty("Set Ignore Clips", true)
		DL_PopActiveObject()
		TA_Create("TramB")
			TA_SetProperty("Position", 34, 6)
			TA_SetProperty("Move Frame", gci_Face_West, 0, "Root/Images/Sprites/Tram/North|0")
			TA_SetProperty("Move Frame", gci_Face_West, 1, "Root/Images/Sprites/Tram/North|0")
			TA_SetProperty("Move Frame", gci_Face_West, 2, "Root/Images/Sprites/Tram/North|0")
			TA_SetProperty("Move Frame", gci_Face_West, 3, "Root/Images/Sprites/Tram/North|0")
			TA_SetProperty("Facing", gci_Face_West)
			TA_SetProperty("Set Ignore Clips", true)
		DL_PopActiveObject()
		TA_Create("TramC")
			TA_SetProperty("Position", 35, 6)
			TA_SetProperty("Move Frame", gci_Face_West, 0, "Root/Images/Sprites/Tram/South|0")
			TA_SetProperty("Move Frame", gci_Face_West, 1, "Root/Images/Sprites/Tram/South|0")
			TA_SetProperty("Move Frame", gci_Face_West, 2, "Root/Images/Sprites/Tram/South|0")
			TA_SetProperty("Move Frame", gci_Face_West, 3, "Root/Images/Sprites/Tram/South|0")
			TA_SetProperty("Move Frame", gci_Face_East, 0, "Root/Images/Sprites/Tram/South|1")
			TA_SetProperty("Move Frame", gci_Face_East, 1, "Root/Images/Sprites/Tram/South|1")
			TA_SetProperty("Move Frame", gci_Face_East, 2, "Root/Images/Sprites/Tram/South|1")
			TA_SetProperty("Move Frame", gci_Face_East, 3, "Root/Images/Sprites/Tram/South|1")
			TA_SetProperty("Facing", gci_Face_West)
			TA_SetProperty("Set Ignore Clips", true)
			TA_SetProperty("Activation Script", gsRoot .. "Chapter 5/Dialogue/Tram/Root.lua")
		DL_PopActiveObject()
		TA_Create("TramD")
			TA_SetProperty("Position", 36, 6)
			TA_SetProperty("Move Frame", gci_Face_West, 0, "Root/Images/Sprites/Tram/North|1")
			TA_SetProperty("Move Frame", gci_Face_West, 1, "Root/Images/Sprites/Tram/North|1")
			TA_SetProperty("Move Frame", gci_Face_West, 2, "Root/Images/Sprites/Tram/North|1")
			TA_SetProperty("Move Frame", gci_Face_West, 3, "Root/Images/Sprites/Tram/North|1")
			TA_SetProperty("Facing", gci_Face_West)
			TA_SetProperty("Set Ignore Clips", true)
		DL_PopActiveObject()
		TA_Create("TramE")
			TA_SetProperty("Position", 37, 6)
			TA_SetProperty("Move Frame", gci_Face_West, 0, "Root/Images/Sprites/Tram/North|2")
			TA_SetProperty("Move Frame", gci_Face_West, 1, "Root/Images/Sprites/Tram/North|2")
			TA_SetProperty("Move Frame", gci_Face_West, 2, "Root/Images/Sprites/Tram/North|2")
			TA_SetProperty("Move Frame", gci_Face_West, 3, "Root/Images/Sprites/Tram/North|2")
			TA_SetProperty("Facing", gci_Face_West)
			TA_SetProperty("Set Ignore Clips", true)
		DL_PopActiveObject()
		TA_Create("TramF")
			TA_SetProperty("Position", 38, 6)
			TA_SetProperty("Move Frame", gci_Face_West, 0, "Root/Images/Sprites/Tram/North|3")
			TA_SetProperty("Move Frame", gci_Face_West, 1, "Root/Images/Sprites/Tram/North|3")
			TA_SetProperty("Move Frame", gci_Face_West, 2, "Root/Images/Sprites/Tram/North|3")
			TA_SetProperty("Move Frame", gci_Face_West, 3, "Root/Images/Sprites/Tram/North|3")
			TA_SetProperty("Facing", gci_Face_West)
			TA_SetProperty("Set Ignore Clips", true)
		DL_PopActiveObject()
	
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[VOICE|Christine](Looks like the tram is set to arrive soon...)") ]])
		fnCutsceneBlocker()
		
		--Move the trams over. They all forcible face west while moving.
		fnCutsceneMoveFace("TramA", 17.25 + 6.0, 6.50, -1, 0, 2.50)
		fnCutsceneMoveFace("TramB", 18.25 + 6.0, 6.50, -1, 0, 2.50)
		fnCutsceneMoveFace("TramC", 19.25 + 6.0, 6.50, -1, 0, 2.50)
		fnCutsceneMoveFace("TramD", 20.25 + 6.0, 6.50, -1, 0, 2.50)
		fnCutsceneMoveFace("TramE", 21.25 + 6.0, 6.50, -1, 0, 2.50)
		fnCutsceneMoveFace("TramF", 22.25 + 6.0, 6.50, -1, 0, 2.50)
		fnCutsceneBlocker()
		fnCutscene([[ AudioManager_PlaySound("World|TramStop") ]])
		fnCutsceneMoveFace("TramA", 17.25 + 3.0, 6.50, -1, 0, 1.00)
		fnCutsceneMoveFace("TramB", 18.25 + 3.0, 6.50, -1, 0, 1.00)
		fnCutsceneMoveFace("TramC", 19.25 + 3.0, 6.50, -1, 0, 1.00)
		fnCutsceneMoveFace("TramD", 20.25 + 3.0, 6.50, -1, 0, 1.00)
		fnCutsceneMoveFace("TramE", 21.25 + 3.0, 6.50, -1, 0, 1.00)
		fnCutsceneMoveFace("TramF", 22.25 + 3.0, 6.50, -1, 0, 1.00)
		fnCutsceneBlocker()
		
		--Trams slow down.
		fnCutsceneMoveFace("TramA", 17.25 + 0.0, 6.50, -1, 0, 0.50)
		fnCutsceneMoveFace("TramB", 18.25 + 0.0, 6.50, -1, 0, 0.50)
		fnCutsceneMoveFace("TramC", 19.25 + 0.0, 6.50, -1, 0, 0.50)
		fnCutsceneMoveFace("TramD", 20.25 + 0.0, 6.50, -1, 0, 0.50)
		fnCutsceneMoveFace("TramE", 21.25 + 0.0, 6.50, -1, 0, 0.50)
		fnCutsceneMoveFace("TramF", 22.25 + 0.0, 6.50, -1, 0, 0.50)
		fnCutsceneBlocker()
		
		--Wait a bit.
		fnCutsceneWait(45)
		fnCutsceneBlocker()

		--Change the facing of part C. This causes the door to open.
		fnCutscene([[ AudioManager_PlaySound("World|TramArrive") ]])
		fnCutsceneBlocker()

	end

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
