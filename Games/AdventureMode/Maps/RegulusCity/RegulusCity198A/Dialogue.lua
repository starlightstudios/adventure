-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    --Set facing.
    TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[Tiffany]|
    if(sActorName == "Tiffany") then
    
        --Variables.
        local iRanTrainingProgram    = VM_GetVar("Root/Variables/Chapter5/Scenes/iRanTrainingProgram", "N")
        local iSawProblemWithProgram = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawProblemWithProgram", "N")
        local iSecondAmandaScene     = VM_GetVar("Root/Variables/Chapter5/Scenes/iSecondAmandaScene", "N")
        local iFinished198           = VM_GetVar("Root/Variables/Chapter5/Scenes/iFinished198", "N")
        
        --Normal:
        if(iSecondAmandaScene == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("55:[VOICE|Tiffany] I have access to the network from here.[P] If you wish to depart, board the tram.[P] Otherwise, I'm busy.") ]])
            fnCutsceneBlocker()

        --Begin sequence to end Sector 198.
        elseif(iSecondAmandaScene == 1.0 and iFinished198 == 0.0) then
            VM_SetVar("Root/Variables/Chapter5/Scenes/iRunFinalScene", "N", 1.0)
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
            fnCutscene([[ Append("55:[E|Neutral] If you are quite done in this sector, I'd like to move my operations to another terminal.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] In addition to being exposed in the event I am discovered, this one suffers from periodic power fluctuations.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] I - [P][CLEAR]") ]])
            fnCutscene([[ Append("55:[E|Neutral] The fluctuations have only begun recently, though it is unlikely that it is in relation to something I did.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] That said, the maintenance work in here is indeed shoddy.[P] You're a repair unit, what is your evaluation?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] Well - [P][CLEAR]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Actually, terminate request.[P] Having you repair this terminal does nothing for the security concerns.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] What was your purpose in this sector, anyway?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] If my directory memory has not decayed due to cosmic ray interference -[P] and it has not -[P] then I recall that a Unit who worked with Unit 499323 works in this sector.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Angry] 55 WILL YOU PLEASE LET ME SPEAK.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Smug] I was not attempting to restrict you.[P] Perhaps your social programs need recalibration?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] Sheesh...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] I need your help.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Is this in some way related to your presence in this sector, or a more general query?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Yes, Christine, I also need your help.[P] We can accomplish more together than we can alone.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] Oh for crying out -[P] wait...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] 55, do you get really chatty when you're lonely?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Offended] [P]Repeat.[P] Query.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Sorry for just leaving you in here all alone, but it's best if you're not seen on the cameras.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] If someone recognizes you, then a security team might be dispatched.[P] You know that.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Offended] [P]I am not.[P] Lonely.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Oh, my mistake.[P] I thought you might be.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] (She totally is.)[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] I need your help to acquire some specialized parts.[P] Take a look at my PDU, it has a list.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] ...[P] High-voltage capacitors, high-voltage cable, insulated ports, buffers...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] What is the purpose of these?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] I'll explain on the way.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] We'll have to spend a few hours tracking them down, of course.[P] Might even need to fabricate some ourselves.[P] Is that acceptable?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Upset] Of course it is.[P] While your insinuations are frustrating, your strategic capabilities are not in question.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Happy] Great![P] Let's go check Sector 19's warehouse and see what we can dig up!") ]])
            fnCutsceneBlocker()
            
            --Fade out.
            fnCutscene([[ AL_SetProperty("Activate Fade", 25, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Wait a bit.
            fnCutsceneWait(45)
            fnCutsceneBlocker()
            
            --Dialogue popup.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Narrator: Six hours later...") ]])
            fnCutsceneBlocker()
            
            --Wait a bit.
            fnCutsceneWait(45)
            fnCutsceneBlocker()
            
            --Transition to 198B. The rest of the cutscene plays out there.
            fnCutscene([[ AL_BeginTransitionTo("RegulusCity198B", "FORCEPOS:13.0x8.0x0") ]])
            fnCutsceneBlocker()

        --Sequence has been completed.
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("55:[VOICE|Tiffany] I suppose this terminal will do for my purposes.[P] The power fluctuations have, indeed, ended.[B][C]") ]])
            fnCutscene([[ Append("55:[VOICE|Tiffany] And, thank you for visiting me.[P] I assume you are checking my status for security reasons.[B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] Of course, 55.[P] Of course.") ]])
            fnCutsceneBlocker()
        end
    end
end
