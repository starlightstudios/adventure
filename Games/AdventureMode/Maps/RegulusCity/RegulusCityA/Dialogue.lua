-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Sophie Leader]|
--During the secrebot sequence, Sophie is in the party lead and uses a different file.
local b254SophieLeading = fnIsSophieLeader()
if(b254SophieLeading == true) then
    LM_ExecuteScript(fnResolvePath() .. "DialogueSophie.lua", sTopicName)
    return
end

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
    
    -- |[GolemAA]|
    if(sActorName == "GolemAA") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Huh, so you need to move the mouse while clicking.[P] What a silly bug.[P] What sort of programmer would miss that?") ]])
        fnCutsceneBlocker()
        
    -- |[GolemAB]|
    elseif(sActorName == "GolemAB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Raiju production rates in this sector are at optimal levels.[P] Maintain observation, update reports in six hours...") ]])
        fnCutsceneBlocker()
        
    -- |[GolemAC]|
    elseif(sActorName == "GolemAC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Greetings, Lord Golem.[P] This unit is currently in auto-repair, and will be back to work shortly.") ]])
        fnCutsceneBlocker()
        
    -- |[GolemAD]|
    elseif(sActorName == "GolemAD") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] I heard that something happened at the Cryogenics Research Facility, but my queries have been rebuffed.[P] Do you know anything, Lord Golem?") ]])
        fnCutsceneBlocker()
        
    -- |[GolemAE]|
    elseif(sActorName == "GolemAE") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Please be careful around the hydraulic lifting shafts, Lord Unit.") ]])
        fnCutsceneBlocker()
        
    -- |[GolemAF]|
    elseif(sActorName == "GolemAF") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Whenever I hit the lemon-lime button, an orange drink gets dispensed.[P] Perhaps my request got switched with another units?[P] Well, maybe not.[P] What kind of unit would prefer orange to lemon-lime?") ]])
        fnCutsceneBlocker()
        
    -- |[GolemAG]|
    elseif(sActorName == "GolemAG") then
	
        --Facing.
        TA_SetProperty("Face Character", "PlayerEntity")
        
        --Variables.
        local iTalkedToSophie = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N")
        
        --Hasn't talked to Sophie yet.
        if(iTalkedToSophie == 0.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Oh, hello, Lord Golem.[P] Please see the maintenance golem for your function assignment.[P] Thank you.") ]])
            fnCutsceneBlocker()

        --Has talked to Sophie.
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] I am being very productive, Lord Golem.[P] Thank you for taking an interest.") ]])
            fnCutsceneBlocker()
        end
        
    -- |[RaijuE]|
    elseif(sActorName == "RaijuE") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Raiju:[VOICE|HumanF0] Hello![P] If you want to proceed into the raiju habitat for snuggles you'll need special protective gear, lord unit.") ]])
        fnCutsceneBlocker()
        
    -- |[RaijuTrainA]|
    elseif(sActorName == "RaijuTrainA") then
        local iIsOnDate = VM_GetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N")
        if(iIsOnDate == 0.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Raiju:[VOICE|HumanF1] Stretch![P] Oh yeah, I'm really feeling it today!") ]])
            fnCutsceneBlocker()
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Hey I suddenly have a really bad idea for a date![B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Blush] Working out with the raijus?[P] Are you caught in a feedback loop?[P] My processor would fry out of embarassment!") ]])
            fnCutsceneBlocker()
        end
        
    -- |[RaijuTrainB]|
    elseif(sActorName == "RaijuTrainB") then
        local iIsOnDate = VM_GetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N")
        if(iIsOnDate == 0.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Raiju:[VOICE|HumanF0] You're quite safe without protective gear, we're surrounded by energy absorbers![P] Try not to stand right on top of one, though.") ]])
            fnCutsceneBlocker()
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Hey I suddenly have a really bad idea for a date![B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Blush] Working out with the raijus?[P] Are you caught in a feedback loop?[P] My processor would fry out of embarassment!") ]])
            fnCutsceneBlocker()
        end
        
    -- |[RaijuTrainC]|
    elseif(sActorName == "RaijuTrainC") then
        local iIsOnDate = VM_GetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N")
        if(iIsOnDate == 0.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Raiju:[VOICE|HumanF0] Sweat, sweat, for the cause of science![P] Hell yeah!") ]])
            fnCutsceneBlocker()
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Hey I suddenly have a really bad idea for a date![B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Blush] Working out with the raijus?[P] Are you caught in a feedback loop?[P] My processor would fry out of embarassment!") ]])
            fnCutsceneBlocker()
        end
        
    -- |[1969]|
    elseif(sActorName == "1969") then
        LM_ExecuteScript(fnResolvePath() .. "Dialogue 1969.lua", "Hello")
        
    -- |[NightA]|
    elseif(sActorName == "NightA") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Night:[VOICE|Night] CR-1-15 is -[P] oh, hi, Christine![P] How's it going?") ]])
        fnCutsceneBlocker()
    
    -- |[NightB]|
    elseif(sActorName == "NightB") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Night:[VOICE|Night] Phew, this sector is a mess.[P] Luckily you have me here!") ]])
        fnCutsceneBlocker()
    
    -- |[NightC]|
    elseif(sActorName == "NightC") then
        local iIsOnDate = VM_GetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N")
        if(iIsOnDate == 1.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Night", "Secrebot") ]])
            fnCutscene([[ Append("Night:[E|Secrebot] Can your circuits process it?[P] A raiju got too excited and blew the microphones in here out.[P] Don't close the door or you'll have to pry it open.[B][C]") ]])
            fnCutscene([[ Append("Night:[E|Secrebot] So what's up, you two?[P] Out for a stroll?[B][C]") ]])
            fnCutscene([[ WD_SetProperty("Activate Topics After Dialogue", "Night")  ]])
        
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Night:[VOICE|Night] Silly raijus, be more careful![P] You slop water everywhere!") ]])
            fnCutsceneBlocker()
        end
    
    -- |[NightD]|
    elseif(sActorName == "NightD") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Night:[VOICE|Night] Phew![P] Enjoying a hard-earned two minute break!") ]])
        fnCutsceneBlocker()
    end
end
