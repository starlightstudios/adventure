-- |[ ======================================= Examination ====================================== ]|
--Called if Sophie is the party leader during an examination.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[ ========================================= Exits ========================================== ]|
if(sObjectName == "ToExteriorSC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Sophie] (Taking Christine out onto the surface would get us both in major trouble!)") ]])
    fnCutsceneBlocker()

--Exit
elseif(sObjectName == "ToCityD") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusCityD", "FORCEPOS:12.0x16.0x0")

-- |[ ====================================== Examinables ======================================= ]|
--Intercom.
elseif(sObjectName == "Intercom") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Sophie] (I d-[P]don't want to bother the intercom operators with my problems!)") ]])
    fnCutsceneBlocker()

--Locked terminal.
elseif(sObjectName == "LockedTerminal") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Sophie] (Heh, maybe I can get Christine to play a video game and finally let me win!)") ]])
    fnCutsceneBlocker()

--Vending machine.
elseif(sObjectName == "FizzyDrink") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sophie:[VOICE|Sophie] CR-1-16![P] Do you like pink or blue Fizzy Pop! more?[B][C]") ]])
    fnCutscene([[ Append("CR-1-16:[VOICE|Christine] I prefer blue, though I believe they are totally identical chemically.") ]])
    fnCutsceneBlocker()

--Standard terminal, one of several on the station. Offloaded.
elseif(sObjectName == "WorkTerminal") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sophie:[VOICE|Sophie] CR-1-16, uh, transfer your account's work credits to my account![B][C]") ]])
    fnCutscene([[ Append("CR-1-16:[VOICE|Christine] Affirmative.[P] One moment.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[VOICE|Sophie] N-[P]no![P] That was a joke![P] Don't do that!") ]])
    fnCutsceneBlocker()

--Raiju habitat.
elseif(sObjectName == "Raijus") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sophie", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Christine", "Serious") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] CR-1-16![P] What's your opinion on raijus?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Serious] I greatly appreciate their fluffy tails and comfort in their own nudity.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Serious] My programming indicates that I may not interact with them without special equipment, else my circuits may be fried.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Offended] Um, I order you not to touch the raijus![P] Don't get hurt![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Serious] Order received.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Geez, I'm responsible now if you do get hurt.[P] I hate being in charge!") ]])
    fnCutsceneBlocker()

--Water Barrels.
elseif(sObjectName == "WaterBarrels") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("[VOICE|Sophie](I wonder if dumping a water barrel on a raiju would cause her to explode...)") ]])
    fnCutsceneBlocker()

--Raiju Terminal
elseif(sObjectName == "RaijuTerminal") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("[VOICE|Sophie](Sector 96's raijus are so productive![P] Great work, team!)") ]])
    fnCutsceneBlocker()

--Ladder
elseif(sObjectName == "Ladder") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("[VOICE|Sophie](I think it'd be mean to make Christine climb down there with a wheel for legs, not to mention the raijus might zap us.)") ]])
    fnCutsceneBlocker()

-- |[Skillbook]|
elseif(sObjectName == "Skillbook") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sophie", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Christine", "Serious") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] Hey, combat routines that look like they'd be right up your alley![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Serious] Combat is unnecessary at this juncture, superior unit.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] If -[P] if I ordered you to attack someone...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Serious] Designate target.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Surprised] No![P] Deactivate -[P] whatever attack order![P] Don't attack anyone![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Serious] Pacifism mode engaged.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Surprised] Darn it![P] Protect me and yourself if we get attacked but don't attack anyone unprovoked![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Serious] Affirmative.[P] Engaging context-based violence routines.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Offended] I am so glad my job is jamming my head inside engine blocks all day.[P] I'd never want to be a programmer...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Serious] Programmer golems are very smart and attractive.[P] They work very hard and should be appreciated.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] No argument here!") ]])
    fnCutsceneBlocker()

-- |[Error]|
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
