-- |[ =================================== Night the Secrebot =================================== ]|
--Handles Night working in the sector.

-- |[Setup]|
local iTicksPerFrame = 15
local iNightPosition = VM_GetVar("Root/Variables/Chapter5/Scenes/iNightPosition", "N")
local sEntityName = "Night"
local sFramePath = "Root/Images/Sprites/Special/NightSecrebot|"

-- |[Night A]|
--Dusting a terminal.
if(iNightPosition == 0) then
    sEntityName = "NightA"
    sFramePath = "Root/Images/Sprites/Special/NightSecrebot|Dust"

-- |[Night B]|
--Mopping.
elseif(iNightPosition == 1) then
    sEntityName = "NightB"
    sFramePath = "Root/Images/Sprites/Special/NightSecrebot|Mop"

-- |[Night C]|
--Mopping.
elseif(iNightPosition == 2) then
    sEntityName = "NightC"
    sFramePath = "Root/Images/Sprites/Special/NightSecrebot|Mop"

-- |[Night D]|
--On break. Not used.
else
end

-- |[ ========================== Setup =========================== ]|
-- |[Existence Check]|
--If the NPCs don't exist, delete this script.
if(EM_Exists(sEntityName) == false) then
    io.write("Warning: Parallel cutscene deleted due to missing NPC. Internal Name: " .. Cutscene_GetProperty("Parallel Script Name") .. "\n")
    Cutscene_HandleParallel("DELETETHIS")
    return
end

-- |[First Run]|
--If the NPCs don't have their special frames yet, apply them.
EM_PushEntity(sEntityName)
    if(TA_GetProperty("Has Special Frame", "Working0") == false) then
        for p = 0, 7, 1 do
            TA_SetProperty("Add Special Frame", "Working"..p, sFramePath .. p)
        end
    end
DL_PopActiveObject()

-- |[ ====================== Cutscene Work ======================= ]|
--Pattern.
local iaPattern = {0, 1, 2, 3, 0, 1, 2, 3}

--Execution.
for i = 1, #iaPattern, 1 do
    fnCutsceneSetFrame(sEntityName, "Working"..iaPattern[i])
    fnCutsceneWait(iTicksPerFrame)
    fnCutsceneBlocker()
end
