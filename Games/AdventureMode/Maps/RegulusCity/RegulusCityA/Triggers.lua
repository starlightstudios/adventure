-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
--Plays a message when the player first enters Regulus City.
if(sObjectName == "WelcomeToRegulusCity") then

	--Variables.
	local iSawWelcomeToRegulus = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawWelcomeToRegulus", "N")
	
	--Play the scene.
	if(iSawWelcomeToRegulus == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSawWelcomeToRegulus", "N", 1.0)
		
		--Wait a bit for the fading.
		fnCutsceneWait(25)
		fnCutsceneBlocker()

		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Serious") ]])
		fnCutscene([[ Append("Voice:[VOICE|Golem] Unidentified unit.[P] Please state designation.[B][C]") ]])
		fnCutscene([[ Append("771852:[E|Serious] Unit 771852 reporting for function assignment.[B][C]") ]])
		fnCutscene([[ Append("Voice:[VOICE|Golem] You are not currently registered in the database.[P] Please proceed down the hallway to your left to the maintenance room.[B][C]") ]])
		fnCutscene([[ Append("Voice:[VOICE|Golem] Airlock pressurized.[P] Welcome to Regulus City.[B][C]") ]])
		fnCutscene([[ Append("771852:[E|Serious] Affirmative.[P] Unit proceeding to maintenance room.") ]])
		fnCutsceneBlocker()
	end

--Forces the player to walk back a bit if they walk too far north without visiting the maintenance golem.
elseif(sObjectName == "NeedMaintenanceN") then

	--Variables.
	local iTalkedToSophie = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N")
	
	--Play the scene.
	if(iTalkedToSophie == 0.0) then
		
		--Get Christine's position.
		EM_PushEntity("Christine")
			local fPlayerX, fPlayerY = TA_GetProperty("Position")
		DL_PopActiveObject()
		
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("771852:[VOICE|Christine] My programming indicates that I should go to the maintenance room before I do anything else.") ]])
		fnCutsceneBlocker()
		
		fnCutsceneMove("Christine", fPlayerX / gciSizePerTile, 25.50)
		fnCutsceneBlocker()
	end

--Forces the player to walk back a bit if they walk too far east without visiting the maintenance golem.
elseif(sObjectName == "NeedMaintenanceE") then

	--Variables.
	local iTalkedToSophie = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N")
	
	--Play the scene.
	if(iTalkedToSophie == 0.0) then
		
		--Get Christine's position.
		EM_PushEntity("Christine")
			local fPlayerX, fPlayerY = TA_GetProperty("Position")
		DL_PopActiveObject()
		
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("771852:[VOICE|Christine] My programming indicates that I should go to the maintenance room before I do anything else.") ]])
		fnCutsceneBlocker()
		
		fnCutsceneMove("Christine", 21.25, fPlayerY / gciSizePerTile)
		fnCutsceneBlocker()
	end

--Waiting for an assignment.
elseif(sObjectName == "Patience") then

    --Repeat check.
	local iCheckedTerminal = VM_GetVar("Root/Variables/Chapter5/Scenes/iCheckedTerminal", "N")
    local iReceivedFunction = VM_GetVar("Root/Variables/Chapter5/Scenes/iReceivedFunction", "N")
    if(iCheckedTerminal ~= 1.0 or iReceivedFunction == 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iReceivedFunction", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iMainQuestBlock", "N", 12.0)
    
    --Movement.
    fnCutsceneMove("Christine", 25.25, 8.50)
    fnCutsceneMove("Christine", 25.25, 6.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "PDU", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] I wonder how long it usually takes to receive a function assignment.[B][C]") ]])
    fnCutscene([[ Append("PDU:[E|Neutral] Approximately 8 minutes.[P] The command unit in charge of this sector has been notified.[P] Lodging and credits requests are currently processing.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] I see.[P] What else goes into the process?[B][C]") ]])
    fnCutscene([[ Append("PDU:[E|Neutral] Other units may put in requests for assignment, or reassignment and you may be requested to fill their role.[P] This process typically takes 10 to 12 seconds.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Efficient![P] I presume an indisposed unit drags out the process, I can't see it taking more than a few milliseconds.[B][C]") ]])
    fnCutscene([[ Append("PDU:[E|Alarmed] Unfortunately, the process has already taken over the usual 12 second threshold.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Hm?[P] Why is that?[B][C]") ]])
    fnCutscene([[ Append("PDU:[E|Quiet] Querying.[P] Cross-referencing.[B][C]") ]])
    fnCutscene([[ Append("PDU:[E|Neutral] Reason -[P] radio communications are presently restricted for security reasons.[P] Short-range network access only.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Oh, how unfortunate.[P] Is this related to the...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Cry-[P] cryo...[P] odd.[P] I don't seem to have memory files concerning that, but I seem to know why the radio is restricted.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Probably nothing to worry about.[P] The city is quite hectic, and they need an organized machine like me to get things under control![B][C]") ]])
    fnCutscene([[ Append("PDU:[E|Neutral] Incoming email from Unit 11042, Command Unit of Sectors 90 through 99.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Dictate.[B][C]") ]])
    fnCutscene([[ Append("PDU:[E|Happy] 'Unit 771852, you are hereby assigned as Lord Golem of Maintenance and Repair, Sector 96.[P] Your lodgings will be in the habitation tower of Sector 96.'[B][C]") ]])
    fnCutscene([[ Append("PDU:[E|Happy] 'All permissions have been updated.[P] The work period for today is almost complete, but please introduce yourself to the staff.[P] I expect great things.[P] I will be watching.'[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Happy] Wow![P] I've been assigned to Sector 96![P] I suppose I'll be seeing more of Unit 499323![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] I guess it's not that surprising, they have been needing a lord golem apparently.[P] I should go let Unit 499323 know![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|PDU] PDU, please set repair manuals and specifications to be downloaded during my next defragmentation cycle.[P] I have a lot of reading to do!") ]])
    fnCutsceneBlocker()

    --Change the job name of Christine's Repair Unit class.
    AdvCombat_SetProperty("Push Party Member", "Christine")
        AdvCombatEntity_SetProperty("Push Job S", "Repair Unit")
            AdvCombatJob_SetProperty("Display Name", "Repair Unit")
        DL_PopActiveObject()
    DL_PopActiveObject()

--Music track fade handlers.
elseif(sObjectName == "MusicFadeE") then

    --Do nothing if the gala is active.
    local iIsGalaTime = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N")
    if(iIsGalaTime == 1.0) then return end

    --Get current music state.
    local iWorkoutTrack = VM_GetVar("Root/Variables/Chapter5/Scenes/iWorkoutTrack", "N")
    
    --In all cases, reset flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iWorkoutTrack", "N", 0.0)
    
    --If the starting value was 1, the workout track is playing. Fade it out and play Regulus City's track.
    if(iWorkoutTrack == 1.0) then
        AL_SetProperty("Music", "RegulusCity")
    end
    
elseif(sObjectName == "MusicFadeW") then

    --Do nothing if the gala is active.
    local iIsGalaTime = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N")
    if(iIsGalaTime == 1.0) then return end

    --Get current music state.
    local iWorkoutTrack = VM_GetVar("Root/Variables/Chapter5/Scenes/iWorkoutTrack", "N")
    
    --In all cases, reset flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iWorkoutTrack", "N", 1.0)
    
    --If the starting value was 0, the city track is playing. Fade it out and play workout track.
    if(iWorkoutTrack == 0.0) then
        AL_SetProperty("Music", "Workout")
    end
end
