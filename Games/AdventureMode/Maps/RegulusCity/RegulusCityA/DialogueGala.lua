-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    --Set facing.
    TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[GolemGalaA]|
    if(sActorName == "GolemGalaA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] We were planning to sneak out onto the surface to watch the sunrise this year.[P] You won't tell, right, Christine?[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] *There are microphones recording conversations in this sector, Marcie!*[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] I forbid it![P] Don't even think about it or I'll discipline you myself, unit![P] And address me as Lord Unit 771852![B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] *Use the exit door in the basement, it's not actually offline.*[B][C]") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] O-[P]of course, Lord Unit.[P] I apologize, Lord Unit.[B][C]") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] *Thanks, Christine![P] See you tomorrow!*") ]])
        fnCutsceneBlocker()
        
    -- |[GolemGalaB]|
    elseif(sActorName == "GolemGalaB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Your dress is so pretty, Christine![P] Enjoy the gala!") ]])
        
    -- |[GolemGalaC]|
    elseif(sActorName == "GolemGalaC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] The Raijus are all asleep in the bunks below.[P] I was getting sick of inputting their productivity statistics!") ]])
        
    -- |[GolemGalaD]|
    elseif(sActorName == "GolemGalaD") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] I've got to make sure this room is spotless before I -[P] y'know.[P] Take off.[P] For my...[P] gathering...") ]])
    end
end
