-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
    
    -- |[GolemAA]|
    if(sActorName == "GolemAA") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Hey, Sophie![P] Sorry, little busy with a bug here.[P] Are we still on for cards tomorrow?") ]])
        fnCutsceneBlocker()
        
    -- |[GolemAB]|
    elseif(sActorName == "GolemAB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Unit 499323![P] Wow, did you get one of the new secrebots?[P] She's cute![P] (Maybe she can take over my stupid job...)") ]])
        fnCutsceneBlocker()
        
    -- |[GolemAC]|
    elseif(sActorName == "GolemAC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] What a day![P] My arms are going to fall off at this rate.[P] Can I get transferred to Maintenance and Repair?") ]])
        fnCutsceneBlocker()
        
    -- |[GolemAD]|
    elseif(sActorName == "GolemAD") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Hey, Sophie![P] Is Christine coming to cards tomorrow?[P] Is she going to bring those rubber and coal tarts like last time?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[VOICE|Sophie] Um -[P] yeah![P] (I better get Christine fixed so we can get baking!)") ]])
        fnCutsceneBlocker()
        
    -- |[GolemAE]|
    elseif(sActorName == "GolemAE") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] A new secrebot?[P] Wow, Sector 96 is really moving up in the city.[P] I've heard the wait list is long!") ]])
        fnCutsceneBlocker()
        
    -- |[GolemAF]|
    elseif(sActorName == "GolemAF") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Oh, hi Chr-[P] oh, I'm sorry.[P] Your secrebot looks a lot like Lord Unit 771852.[P] Must be a coincidence.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[VOICE|Sophie] Y-[P]Yeah![P] Yep![P] After all, there's only so many faceplate designs, right?") ]])
        fnCutsceneBlocker()
        
    -- |[GolemAG]|
    elseif(sActorName == "GolemAG") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] No, no, I did -[P] it is not taking the credits chip.[P] Come on![B][C]") ]])
        fnCutscene([[ Append("Sophie:[VOICE|Sophie] There's one stuck in the slot.[P] Someone didn't take the used chip out.[B][C]") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Then -[P] oh wait, it was probably me.[P] Uh, Sophie, don't tell anyone please!") ]])
        fnCutsceneBlocker()
        
    -- |[RaijuE]|
    elseif(sActorName == "RaijuE") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Raiju:[VOICE|HumanF0] Hey, 499323![P] Any chance I can snuggle your new secrebot?[P] Plleeeaaase?") ]])
        fnCutsceneBlocker()
        
    -- |[RaijuTrainA]|
    elseif(sActorName == "RaijuTrainA") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Raiju:[VOICE|HumanF1] Get fit![P] GET FIT![P] Yeah!") ]])
        fnCutsceneBlocker()
        
    -- |[RaijuTrainB]|
    elseif(sActorName == "RaijuTrainB") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Raiju:[VOICE|HumanF0] Oh I'm gonna sleep like a brick tonight!") ]])
        fnCutsceneBlocker()
        
    -- |[RaijuTrainC]|
    elseif(sActorName == "RaijuTrainC") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Raiju:[VOICE|HumanF0] I'm gonna generate so much power, I'll blow a hole in the wall![P] Hiyah!") ]])
        fnCutsceneBlocker()
    
    -- |[1969]|
    elseif(sActorName == "1969") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Command Unit:[VOICE|1969] Nice secrebot you have there, but I'm afraid leg lifts aren't exactly going to help her![P] Ha ha ha!") ]])
        fnCutsceneBlocker()
    end
end
