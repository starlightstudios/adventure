-- |[ ================================= Dialogue Script - 1969 ================================= ]|
--Entry point for dialogue with Kona.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[Common Setup]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "1969", "Neutral") ]])
    
    -- |[Variables]|
    local iMet1969 = VM_GetVar("Root/Variables/Chapter5/1969/iMet1969", "N")
    
    -- |[First Meeting]|
    if(iMet1969 == 0.0) then
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/1969/iMet1969", "N", 1.0)
    
        --Dialogue.
        fnCutscene([[ Append("Command Unit:[E|Neutral] Hey hey![P] What's cookin'![P] Nice to see a new faceplate around here![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Hello, command unit.[P] I am Unit 771852, but my friends call me Christine.[B][C]") ]])
        fnCutscene([[ Append("1969:[E|Neutral] Command Unit 1969, Head of Exercise for Power Production Units![B][C]") ]])
        fnCutscene([[ Append("1969:[E|Booyeah] But I'm not just here to keep the organics fit and tough, oh no![P] Did you know repeated use of a golem's motivators increases efficiency as the auto-repair nanites reapply themselves?[B][C]") ]])
        fnCutscene([[ Append("1969:[E|Neutral] Exercise is good for every unit![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] I actually did not know that.[P] Are you located in Sector 96 or...[B][C]") ]])
        fnCutscene([[ Append("1969:[E|Neutral] Nope, I travel the city![P] My office is in the biolabs but I teach classes anywhere we have raijus.[P] Keeps them fit and happy![P] They love it![B][C]") ]])
        fnCutscene([[ Append("1969:[E|Neutral] See the lines on the ground?[P] If they discharge while I make them SWEAT, then all the better![P] You're relatively safe here but I still have protective rubber gear on.[B][C]") ]])
        fnCutscene([[ Append("1969:[E|Booyeah] Efficiency, morale, power production, and most importantly, gains![P] I've got it all! I can even help you if you're willing!") ]])
        fnCutsceneBlocker()
        fnCutscene([[ AM_SetProperty("Open Trainer") ]])
    
    -- |[Repeats]|
    else
        fnCutscene([[ Append("1969:[E|Booyeah] Yeah![P] Time to sweat grease![P] You ready to make your motivators move?") ]])
        fnCutsceneBlocker()
        fnCutscene([[ AM_SetProperty("Open Trainer") ]])
    end
end