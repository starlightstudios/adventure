-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Sophie Leader]|
--During the secrebot sequence, Sophie is in the party lead and uses a different file.
local b254SophieLeading = fnIsSophieLeader()
if(b254SophieLeading == true) then
    LM_ExecuteScript(fnResolvePath() .. "ExaminationSophie.lua", sObjectName)
    return
end

-- |[ ========================================= Exits ========================================== ]|
if(sObjectName == "ToExteriorSC") then
	
	--Christine cannot leave unless she has a reason. She gets one once 55 joins the party.
	local iReceivedFunction = VM_GetVar("Root/Variables/Chapter5/Scenes/iReceivedFunction", "N")
	local iMet55InLowerRegulus = VM_GetVar("Root/Variables/Chapter5/Scenes/iMet55InLowerRegulus", "N")
	local iIsOnDate = VM_GetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N")
    local iIsGalaTime = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N")
	if(iReceivedFunction == 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[VOICE|Christine](My programming indicates I must receive a function assignment before I go anywhere.)") ]])
		fnCutsceneBlocker()
	
	--Has a function assignment but hasn't met up with 55.
	elseif(iReceivedFunction == 1.0 and iMet55InLowerRegulus == 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[VOICE|Christine](I don't really have a reason to leave Regulus City right now.)") ]])
		fnCutsceneBlocker()
	
	--If Sophie is currently following.
	elseif(iIsOnDate == 1.0 or iIsOnDate == 2.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Christine, I'm not cleared to go outside.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] You aren't?[P] Can I clear you?[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] You could, but unless there's something on the work order list out there, it might be seen as odd by the administrators.[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] You're a Lord Unit, so they don't even really pay much attention to your movements.[P] Us Slave Units are under a lot more scrutiny.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Ah, I see.") ]])
		fnCutsceneBlocker()
	
    elseif(iIsGalaTime > 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Wrong way, Christine.[P] We need to go to the tram at the north end of the sector.[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Smirk] Hiking to the gala might get dust on your dress![P] Hee hee!") ]])
		fnCutsceneBlocker()
    
	--Exit.
	else
		AudioManager_PlaySound("World|AutoDoorOpen")
		AL_BeginTransitionTo("RegulusExteriorSC", "FORCEPOS:16.5x4.0x0")
	end
	
--Exit
elseif(sObjectName == "ToCityD") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusCityD", "FORCEPOS:12.0x16.0x0")

-- |[ ====================================== Examinables ======================================= ]|
--Intercom.
elseif(sObjectName == "Intercom") then
	
	--Dialogue changes based on where we are in the story.
	local iReceivedFunction = VM_GetVar("Root/Variables/Chapter5/Scenes/iReceivedFunction", "N")
    local iIsGalaTime = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N")
	if(iReceivedFunction == 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Intercom:[VOICE|Golem] Please proceed to the maintenance bay for your initial scan.[P] It is to your left past the airlock door.[B][C]") ]])
		fnCutscene([[ Append("771852:[VOICE|Christine] Affirmative.[P] Proceeding to maintenance bay.") ]])
		fnCutsceneBlocker()
	
    elseif(iIsGalaTime > 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Intercom:[VOICE|Golem] Lord Unit Christine?[P] May I assist you?[B][C]") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] Hey Jessica![P] Do you have intercom duty tonight?[B][C]") ]])
		fnCutscene([[ Append("Intercom:[VOICE|Golem] Yeah, probably gonna be a boring night.[P] I borrowed an RVD and I'm going to watch 'Planet of the Sharks' tonight.[B][C]") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] Have fun![P] I won't spoil the ending for you.[B][C]") ]])
		fnCutscene([[ Append("Intercom:[VOICE|Golem] Pah.[P] Everyone knows the ending.[P] You maniacs![P] You blew it up![B][C]") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] Awww.[P] See you later, Jessica.") ]])
		fnCutsceneBlocker()
    
	--Other cases.
	else
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Intercom:[VOICE|Golem] How may I assist you, Unit 771852?[B][C]") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] By being as efficient and helpful as you are currently being.[P] Good work![B][C]") ]])
		fnCutscene([[ Append("Intercom:[VOICE|Golem] Thank you for your praise, Lord Unit.[P] It will be logged on this unit's account.") ]])
		fnCutsceneBlocker()
	end

--Locked terminal.
elseif(sObjectName == "LockedTerminal") then

    --Variables.
    local iIsGalaTime = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N")
    
    if(iIsGalaTime == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("[VOICE|Christine](This terminal is password locked.[P] Another unit must have been using it and left it.[P] I should leave it for them.)") ]])
        fnCutsceneBlocker()
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("[VOICE|Christine](This terminal is in standby.)") ]])
        fnCutsceneBlocker()
    end

--Vending machine.
elseif(sObjectName == "FizzyDrink") then

    --Variables.
    local iIsGalaTime = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N")
    
    if(iIsGalaTime == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("[VOICE|Christine](Fizzy Pop!, a perfect way to boost your productive output![P] According to the cans, the energy-per-gram is five times that of trinitrotoluene.)") ]])
        fnCutsceneBlocker()
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("[VOICE|Christine](I really don't want to risk spilling any Fizzy Pop! on my dress...)") ]])
        fnCutsceneBlocker()

    end

--Water Barrels.
elseif(sObjectName == "WaterBarrels") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("[VOICE|Christine](Water barrels, they both help a raiju who has worked up a thirst, and if a golem overheats you can just dump it on them.)") ]])
    fnCutsceneBlocker()

--Raiju Terminal
elseif(sObjectName == "RaijuTerminal") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("[VOICE|Christine](A list of every raiju unit and which exercise classes they're scheduled for.)") ]])
    fnCutsceneBlocker()

--Ladder
elseif(sObjectName == "Ladder") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("[VOICE|Christine](Climbing into the raiju habitat is a one-way trip to the blown-out circuits scrap heap!)") ]])
    fnCutsceneBlocker()

-- |[Work Terminal]|
--Standard terminal, one of several on the station. Offloaded.
elseif(sObjectName == "WorkTerminal") then

    --Variables.
    local iIsGalaTime = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N")
    
    if(iIsGalaTime == 0.0) then
        LM_ExecuteScript(gsRoot .. "Chapter 5/Scenes/300 Standards/Work Terminal/Execution.lua")
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("[VOICE|Christine](No need to access the work terminal right now.[P] I've got a gala to attend.)") ]])
        fnCutsceneBlocker()
    end

-- |[Date: Watch the Raijus]|
--Watch the Raijus.
elseif(sObjectName == "Raijus") then

	--Variables.
	local iIsOnDate        = VM_GetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N")
	local iExplainedRaijus = VM_GetVar("Root/Variables/Chapter5/Sophie/iExplainedRaijus", "N")
    local iIsGalaTime      = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N")
    local iStartedShoppingSequence = VM_GetVar("Root/Variables/Chapter5/Scenes/iStartedShoppingSequence", "N")

    --Gala.
    if(iIsGalaTime > 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[VOICE|Christine](I guess the Raijus are all in the bunks down the ladder.[P] But I bet they're still cranking electricity out.)") ]])
		fnCutsceneBlocker()

	--Not on a date. Examine the stuff.
	elseif(iIsOnDate == 0.0 or iStartedShoppingSequence == 1.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[VOICE|Christine](One of several habitats for Raijus.[P] They generate electricity as they move or touch one another, and seem quite happy with their environment.)") ]])
		fnCutsceneBlocker()

	--Needs to explain maintenance.
	elseif(iIsOnDate == 1.0 and iExplainedRaijus == 0.0) then
		VM_SetVar("Root/Variables/Chapter5/Sophie/iExplainedRaijus", "N", 1.0)
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Aren't the Raijus fun to watch?[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Mmm, I do like to relax and just watch them play sometimes.[P] They seem so happy and carefree.[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Did you know that they generate about half of the power we use on Regulus?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Really?[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Power output increases when they have sex.[P] We used to pump in pheromones, but it turns out they just screw each other wildly regardless.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Can they hear us?[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Not sure if their organic hearing would work with glass this thick.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] So, should we spend the evening watching the Raijus?[BLOCK]") ]])

		--Decision script is this script. It must be surrounded by quotes.
		local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
		fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
		fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"WatchRaijus\") ")
		fnCutscene(" WD_SetProperty(\"Add Decision\", \"No Thanks\",  " .. sDecisionScript .. ", \"NoClose\") ")
		fnCutsceneBlocker()

	--Date option.
	elseif(iIsOnDate == 1.0 and iExplainedRaijus == 1.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Shall we spend our evening watching the Raijus?[BLOCK]") ]])

		--Decision script is this script. It must be surrounded by quotes.
		local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
		fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
		fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"WatchRaijus\") ")
		fnCutscene(" WD_SetProperty(\"Add Decision\", \"No Thanks\",  " .. sDecisionScript .. ", \"NoClose\") ")
		fnCutsceneBlocker()
	end

--Spend a date watching the Raijus.
elseif(sObjectName == "WatchRaijus") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")

	--Fade.
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(85)
	fnCutsceneBlocker()

	--Execute the conversation routing case.
	LM_ExecuteScript(gsRoot .. "Chapter 5/Scenes/500 Normal/Sophie Conversation/Z Routing.lua")
	fnCutsceneBlocker()

	--Fade.
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
	fnCutsceneWait(85)
	fnCutsceneBlocker()
	
	--Variables.
	local iSophieFirstDateState = VM_GetVar("Root/Variables/Chapter5/Scenes/iSophieFirstDateState", "N")
	local iSophieWillSynchronizeNow = VM_GetVar("Root/Variables/Chapter5/Sophie/iSophieWillSynchronizeNow", "N")
	
	--First date.
	if(iSophieFirstDateState == 0.0) then
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSophieFirstDateState", "N", 1.0)
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutscene([[ Append("Sophie:[E|Blush] Raijus are so cute and relaxing.[P] I can't think of a better idea for a first date.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] F-[P]f-[P]first... date...[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Blush] Uh, uh![P] No, I was just showing you around![P] Hee hee![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|PDU] Matchmaker -[P] shut up, PDU![B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Blush] Matchmaker?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Offended] It's my stupid PDU.[P] It said you were ogling me with your big pretty eyes and - [B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Offended] You keep flicking your hair a bit when you talk.[P] You keep looking at me.[P] I can't take my eyes off you.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Offended] What is your deal?[P] We're going to be working together so you may as well just tell me why you don't like me![B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Sad] Don't like you?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Offended] Yeah![P] Am I ugly, or weird, or something?[P] As a lord golem I order you to tell me![B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] ...[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Do you want to kiss me?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Offended] Of course![B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] *smooch*[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Blush] H-[P]huh?[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Happy] Want another one?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Blush] Yeah![B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] *smooch*[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Blush] My goodness, Sophie, Unit 499323, do you like me?[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Happy] Of course I do![P] I like you a lot![P] How do you not see that?[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Happy] I was holding your hand while we talked![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Blush] You -[P] you were just scared by the silly raijus![P] I was steadying you![B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Offended] Are you serious?[P] Come on![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] Okay that one was silly but - [B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] Do you...[P] want to be my tandem unit?[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Happy] I thought you'd never ask![P] Of course![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] But I'm your lord golem...[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Blush] Yeah, but I don't care what anyone else thinks![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] What does that have to do with it?[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] We're different model variants.[P] Slave and lord, we're not supposed to date.[P] You didn't know?[P] Well, you are a fresh convert so...[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Yes, we'll be looked down on.[P] But I don't care.[P] I want you.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] I want you too![B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Offended] Darn it.[P] Now we can't have weeks of will-they-won't-they sexual tension...[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Sorry.[P] I can pretend not - [B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Offended] Oh no you don't![B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Happy] Phew![P] What a day it has been![P] I really need a recharge![P] Would you like to walk me back to my room?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] Of course.[P] The habitation block is just east of the surface exit, right?[P] Let's go!") ]])
        
        --[=[
		fnCutscene([[ Append("Sophie:[E|Neutral] Oh my, look how late it is![P] I should really go defragment my drives.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Ummm...[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Hee hee![P] You haven't even seen your quarters yet, have you?[P] Do you know where they are?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Vaguely.[P] I didn't download the directory when I got here.[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Well then allow me to show you.[P] We need to take the elevators east of the airlock.[P] I'll point them out when we get close.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Thanks, Sophie.[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] This does count as walking a lady back to her room.[P] We use the same elevators, of course.[P] All units in the sector use the same domicile block.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] All right.[P] Let's go.") ]])]=]
		fnCutsceneBlocker()
	
	--On successive dates, Sophie requests to return to the maintenance bay unless she wants to synchronize.
	elseif(iSophieWillSynchronizeNow == 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Oh, look at that![P] I should be getting back to the maintenance bay.[P] If I don't have everything logged, my Lord Unit will -[P]  oh right.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Oh, you.[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Hee hee![P] This was fun, though.[P] We should do it again sometime.[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Would you care to escort your tandem unit?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] With pleasure.") ]])
		fnCutsceneBlocker()
	end
	
	--Variables.
	VM_SetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N", 2.0)
	
-- |[Multi-purpose Close]|
elseif(sObjectName == "NoClose") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")

-- |[Skillbook]|
elseif(sObjectName == "Skillbook") then
    AL_SetProperty("Set Layer Disabled", "SkillbookIco", true)
	LM_ExecuteScript(gsChristineSkillbook, 0)

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
