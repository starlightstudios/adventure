-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
--Plays a message when the player first enters Regulus City.
if(sObjectName == "Entrance") then

	--Variables.
	local iSophieFirstDateState    = VM_GetVar("Root/Variables/Chapter5/Scenes/iSophieFirstDateState", "N")
    local iSaw55PostLRT            = VM_GetVar("Root/Variables/Chapter5/Scenes/iSaw55PostLRT", "N")
    local iStartedShoppingSequence = VM_GetVar("Root/Variables/Chapter5/Scenes/iStartedShoppingSequence", "N")
    local iIsGalaTime              = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N")
	
	--Play the scene.
	if(iSophieFirstDateState < 3.0) then
		
		--Reset this flag.
		gbDontCancelMusic = false
		
		--Spawn Sophie.
		TA_Create("Sophie")
			TA_SetProperty("Position", -100, -100)
			TA_SetProperty("Clipping Flag", false)
			fnSetCharacterGraphics("Root/Images/Sprites/Sophie/", true)
			TA_SetProperty("Facing", gci_Face_North)
			TA_SetProperty("Wipe Special Frames")
			for i = 0, 7, 1 do
				TA_SetProperty("Add Special Frame", "Kiss" .. i, "Root/Images/Sprites/Special/ChristineSophie|Kiss" .. i)
			end
		DL_PopActiveObject()
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSophieFirstDateState", "N", 3.0)
		VM_SetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N", 0.0)
		VM_SetVar("Root/Variables/Chapter5/Sophie/iSophieWentToQuarters", "N", 0.0)
		
		--Fade to black immediately.
		fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
		fnCutsceneTeleport("Sophie", 9.25, 10.50)
		fnCutsceneTeleport("Christine", 9.25, 10.50)
		fnCutsceneBlocker()
		
		--Stop the music, fade in.
		fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
		fnCutsceneWait(45)
		fnCutsceneBlocker()

		--Movement.
		fnCutsceneMove("Sophie", 9.25, 8.50)
		fnCutsceneMove("Christine", 9.25, 9.50)
		fnCutsceneBlocker()
		fnCutsceneMove("Sophie", 9.25, 8.50, 0.50)
		fnCutsceneBlocker()
		fnCutsceneWait(45)
		fnCutsceneBlocker()
		fnCutsceneFace("Sophie", -1, -1)
		fnCutsceneWait(45)
		fnCutsceneBlocker()
		fnCutsceneFace("Sophie", 0, -1)
		fnCutsceneWait(45)
		fnCutsceneBlocker()
		fnCutsceneFace("Sophie", 1, -1)
		fnCutsceneWait(105)
		fnCutsceneBlocker()
		fnCutsceneFace("Sophie", 0, 1)
		fnCutsceneFace("Christine", 0, -1)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()

		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutscene([[ Append("Sophie:[E|Happy] Wooooooooow.[P] Lord Units have very spacious quarters...[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] Oh, I haven't shown you my quarters yet, have I?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] I've been meaning to change the colors around...[P][E|Laugh] maybe I could do with something violet?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Laugh] In case my favourite color wasn't obvious!") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Movement.
		fnCutsceneFace("Sophie", 0, 1)
		fnCutsceneMove("Christine", 8.25, 9.50)
		fnCutsceneBlocker()
		fnCutsceneFace("Sophie", -1, 1)
		fnCutsceneMove("Christine", 4.25, 9.50)
		fnCutsceneBlocker()
		fnCutsceneFace("Christine", 0, -1)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()

		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutscene([[ Append("Christine:[E|Happy] Ohmygosh![P] We'll get some potted plants to match your green hair, and they can go right here next to the oil machine![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Happy] Don't you think that'd tie the room together?[P] It'd be lovely![B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Smirk] Look at you, the interior decorator![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] I've a very refined taste, my dear.") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Movement.
		fnCutsceneMove("Christine", 6.25, 9.50)
		fnCutsceneBlocker()
		fnCutsceneMove("Christine", 6.25, 8.50)
		fnCutsceneBlocker()
		fnCutsceneFace("Sophie", -1, 0)
		fnCutsceneMove("Christine", 6.25, 4.50)
		fnCutsceneBlocker()
		fnCutsceneFace("Sophie", -1, -1)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()

		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] Sophie...[P] isn't the city beautiful?[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Th-[P] the city?") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Movement.
		fnCutsceneMove("Sophie", 7.25, 6.50)
		fnCutsceneMove("Sophie", 7.25, 4.50)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()

		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] You have a window in your quarters...[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Is this non-standard?[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] My quarters is a 1x2 block.[P] I have a filing cabinet and my defragmentation pod.[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Of course I don't really [P]*need*[P] any more space than that...[P] I spend most of my time in the maintenance bay...[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] But I just haven't seen the outside in a long time.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] How long?[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Smirk] A few years.[P] I used to work on the survey team.[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Once I got reassigned to Maintenance and Repair...[P] I just never had a reason to go outside.[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Sad] I mean it's dangerous out there for a unit without a security escort...[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] But you'd never forget the stars.[P] They're magnificent...[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Smirk] The night sky is breathtaking...[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Happy] ...[P] is what I'd say if we needed to breathe.[P] Hee hee!") ]])
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Wait a bit.
		fnCutsceneWait(185)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Sophie:[VOICE|Sophie] Well, I suppose I need to be getting back to my quarters...") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Cancel the music.
		fnCutscene([[ AL_SetProperty("Music", "NullSlow") ]])
		
		--Movement.
		fnCutsceneMove("Sophie", 7.25, 7.50, 0.70)
		fnCutsceneMove("Sophie", 9.25, 7.50, 0.70)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] Wait!") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Facing.
		fnCutsceneFace("Christine", 1, 1)
		fnCutsceneFace("Sophie", -1, -1)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()

		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Uh -[P] Are you sure you don't want to stay with me a little longer?[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Smirk] Oh, hun, I'm easy, but I'm not [P]*that*[P] easy![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] ...[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] ...[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Blush] That is only twenty-five percent of what I meant.[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Smirk] Hee hee hee![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] I just -[P] I'm not very good at this, but I don't want you to go.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Blush] S-[P]Sophie...[P] I - [B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Smirk] Is something wrong?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Scared] No![P] A-all parameters are within expected ranges![B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Smirk] Then what is it?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] I'm not sure how to put this, but...[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] I -[P] I uh -[P] I'm -[P] trying...[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Smirk] Oh my goodness.[P] Christine.[P] Are you no good with girls?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Scared] !!![B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Smirk] Hee hee![P] Oh, I see it written all over your face![B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Unit 771852, am I your first girlfriend?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Scared] N -[P] n -[P] !![B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Happy] Well now I'm just so flattered![B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Smirk] You're so pretty, and smart, and strong, and you have the cutest little accent...[P] I don't know...[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Smirk] If you hadn't wanted to be my tandem unit I'd...[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Smirk] Even if you're a Lord Unit...[P] You only live once, right?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Laugh] Oh thank goodness![P] I was so scared![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] When I first talked to you, I was smitten![P] But I thought you wouldn't like me as I was.[P] Nobody ever does.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] But then, your smile made my fear melt away.[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Smirk] I was thinking the same thing the whole time, but I guess I didn't know how to tell you.[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Smirk] I've had so much fun the past few weeks.[P] Every day just flew by...[P] The time before I met you,[P] doesn't even seem real...[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Smirk] I really like you, Christine.[P] I really do.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] I -[P] I -[P] I don't know what to say![B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Smirk] You don't need to say anything.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Blush] You're right...") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Start the music. Kiss happens 20.274 seconds after this.
		fnCutscene([[ AL_SetProperty("Music", "SophiesThemeSlow") ]])
		
		--Christine walks to Sophie.
		fnCutsceneMove("Christine", 6.25, 7.50, 0.50)
		fnCutsceneMove("Christine", 8.25, 7.50, 0.50)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Kiss. Teleport Christine offscreen and use Sophie's special frames.
		Cutscene_CreateEvent("CameraEvent", "Camera")
			CameraEvent_SetProperty("Focus Position", (8.25 * gciSizePerTile), (7.50 * gciSizePerTile))
		DL_PopActiveObject()
		fnCutsceneSetFrame("Sophie", "Kiss0")
		fnCutsceneTeleport("Sophie", 8.25, 7.50)
		fnCutsceneTeleport("Christine", -100.25, -100.50)
		fnCutsceneWait(250)
		fnCutsceneBlocker()
		fnCutsceneSetFrame("Sophie", "Kiss1")
		fnCutsceneWait(250)
		fnCutsceneBlocker()
		fnCutsceneSetFrame("Sophie", "Kiss2")
		fnCutsceneWait(250)
		fnCutsceneBlocker()
		
		--Wait. Total time elapsed is 90 ticks. Total is 1216. We need to wait while the music builds.
		-- There is a fudge rate for the walking, which is not strictly timed.
		fnCutsceneSetFrame("Sophie", "Kiss3")
		fnCutsceneWait(1216 - 750 - 175)
		fnCutsceneBlocker()
		
		--Start the kiss right as the music hits the start of its roll. Kiss lasts 10 seconds total.
		fnCutsceneSetFrame("Sophie", "Kiss4")
		fnCutsceneWait(15)
		fnCutsceneBlocker()
		fnCutsceneSetFrame("Sophie", "Kiss5")
		fnCutsceneWait(125)
		fnCutsceneBlocker()
		fnCutsceneSetFrame("Sophie", "Kiss6")
		fnCutsceneWait(600 - 140)
		fnCutsceneBlocker()
		
		--Switch to Kiss7 for the conversation.
		fnCutsceneSetFrame("Sophie", "Kiss7")
		fnCutsceneWait(200)
		fnCutsceneBlocker()
		fnCutsceneSetFrame("Sophie", "Kiss6")
		fnCutsceneWait(200)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] Sophie...[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Smirk] Christine...[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] I love you...[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Smirk] I love you too...") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(35)
		fnCutsceneBlocker()
		
		--Back to Kiss6, then 7.
		fnCutsceneSetFrame("Sophie", "Kiss6")
		fnCutsceneWait(400)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] But I still don't know what to say...[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Smirk] [P].[P].[P].[P][CLEAR]") ]])
		fnCutscene([[ Append("Sophie:[E|Smirk] 'See you tomorrow'?[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Smirk] And then the day after that, and the day after that, and the one after that?[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Smirk] We'll spend every day together.[P] Always.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Blush] Yes...[P] Every day like this...[P] together...[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Blush] We'll be together for the rest of our synthetic lives...[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Smirk] I'll look forward to tomorrow.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Blush] I can't wait for it to come, so I can spend it with you...") ]])
		fnCutsceneBlocker()
		
		--Fade to black.
		fnCutsceneWait(180)
		fnCutsceneBlocker()
		fnCutscene([[ AL_SetProperty("Activate Fade", 120, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
		fnCutsceneWait(360)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[VOICE|Christine](This is what it feels like to have something to look forward to...)") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(180)
		fnCutsceneBlocker()
		fnCutscene([[ AL_SetProperty("Music", "NullSlow") ]])
		fnCutsceneWait(300)
		fnCutsceneBlocker()
        fnCutscene([[ AM_SetProperty("Execute Rest") ]])
		
		--Teleport Sophie offscreen.
		fnCutsceneTeleport("Sophie", -100.25, -100.50)
		fnCutsceneTeleport("Christine", 14.30, 4.50)
		fnCutsceneFace("Christine", 0, 1)
		fnCutsceneBlocker()
		
		--Wait a bit.
		fnCutsceneWait(65)
		fnCutsceneBlocker()
		
		--Stop the music. Fade in.
		fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
		fnCutsceneWait(45)
		fnCutsceneBlocker()
		
		--Show Christine the special message.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSawSpecialAnnouncement", "N", 1.0)
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ Append("Christine: (Unit 771852 resuming cognitive functions.)[B][C]") ]])
		fnCutscene([[ Append("Christine: (Hmm?[P] I seem to have downloaded a notice while I was defragmenting.)[B][C]") ]])
		fnCutscene([[ Append("Christine: ('A special work order has been placed on your account.[P] Please consult a work terminal at your earliest convenience.')[B][C]") ]])
		fnCutscene([[ Append("Christine: (I guess I should go see what that's about.)") ]])
		fnCutsceneBlocker()
    
    --Second quarters scene with Christine and Sophie.
    elseif(iSaw55PostLRT == 1.0 and iStartedShoppingSequence == 0.0) then
		
        --Flags.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iMainQuestBlock", "N", 21.0)
        
        --Load assets.
        fnLoadDelayedBitmapsFromList("Chapter 5 Window", gciDelayedLoadLoadAtEndOfTick)
        
		--Spawn Sophie.
		TA_Create("Sophie")
			TA_SetProperty("Position", -100, -100)
			TA_SetProperty("Clipping Flag", false)
			fnSetCharacterGraphics("Root/Images/Sprites/Sophie/", true)
			TA_SetProperty("Facing", gci_Face_North)
			TA_SetProperty("Wipe Special Frames")
		DL_PopActiveObject()
		
		--Messenger Golem.
		TA_Create("Golem")
			TA_SetProperty("Position", -100, -100)
			TA_SetProperty("Clipping Flag", false)
			fnSetCharacterGraphics("Root/Images/Sprites/GolemSlaveP/", false)
			TA_SetProperty("Facing", gci_Face_North)
			TA_SetProperty("Wipe Special Frames")
		DL_PopActiveObject()
        
        --Christine needs to be a golem.
        LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua")
		
		--Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iStartedShoppingSequence", "N", 1.0)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSawBasementScene", "N", 0.0)
        
        --Variables.
        local iCompletedSerenity = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedSerenity", "N")
        local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
		
		--Fade to black immediately.
		fnCutscene([[ AL_SetProperty("Music", "Null") ]])
		fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
		fnCutsceneTeleport("Christine", 6.25, 4.50)
        fnCutsceneFace("Christine", 0, -1)
		fnCutsceneBlocker()
		fnCutsceneWait(165)
		fnCutsceneBlocker()
        
        --Narration:
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Narrator: Three weeks later...") ]])
        fnCutsceneBlocker()
		
		--Stop the music, fade in.
		fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
		fnCutsceneWait(165)
		fnCutsceneBlocker()
        
        --Sophie enters.
		fnCutsceneTeleport("Sophie", 8.75, 10.50)
        fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(45)
		fnCutsceneBlocker()
        
        --Sophie moves north a bit.
        fnCutsceneMove("Sophie", 7.25, 9.50)
        fnCutsceneFace("Sophie", 0, -1)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Sophie:[VOICE|Sophie] U-[P]Unit 771852?") ]])
        fnCutsceneBlocker()
        
        --Christine turns around.
        fnCutsceneFace("Christine", 0, 1)
		fnCutsceneWait(45)
		fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Hello, Sophie.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Sad] L-[P]Lord Unit, d-[P]did I do something?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Sad] If you're mad at me and don't want to be my tandem unit anymore, it's okay.[P] Really.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Sad] But I want to say I'm sorry so you don't hold it against me...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Scared] What?[P] No![P] Sophie![P] I love you, what are you talking about?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Sad] But you've been so distant at work.[P] You barely speak to me, and we haven't gone on a date since...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] The LRT Facility...[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Sad] Is that where you went three weeks ago?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] Sorry, you weren't supposed to know.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Though I guess there's no harm in it, now.[P] It seems nobody knows.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] The administration must be keeping a lid on it, because 55 would have notified me if they were publically looking for us.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Sad] So it's -[P][E|Happy] not something I did?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Blush] No dearest, it wasn't.[P] This is my fault.[B][C]") ]])
        if(iCompletedSerenity == 0.0) then
            fnCutscene([[ Append("Christine:[E|Blush] I'll love you until my core fuses its last.[P] Nothing could change that.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Smirk] Dearest...[P] I like the sound of that.[P] I think I'll be borrowing it, dearest.") ]])
        else
            fnCutscene([[ Append("Christine:[E|Blush] I'll love you until my core fuses its last.[P] Nothing could change that.") ]])
        end
		fnCutsceneBlocker()
        
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
        fnCutscene([[ WD_SetProperty("Set Scene Image", "Null") ]])
        fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/WindowReflection/Alone") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] It's my fault that you feel this way, and I'll take responsibility.[P] I've been preoccupied at work.[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] I -[P] I'm still not sure what to do.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[VOICE|Sophie] Do you want to talk about it?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[VOICE|Sophie] I know there are some things you can't tell me, to protect me, but...[P] Well, try to avoid those things.[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] ...[B][C]") ]])
        fnCutscene([[ Append("Sophie:[VOICE|Sophie] Please?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[VOICE|Sophie] You're suffering, and it pains me not to be able to help you.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[VOICE|Sophie] I wish I could be out there, on the front...[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] ...") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(45)
        fnCutsceneBlocker()
            
        --Christine turns around.
        fnCutsceneFace("Christine", 0, -1)
        fnCutsceneWait(85)
        fnCutsceneBlocker()
        
        --Sophie walks up to the window.
		fnCutsceneMove("Sophie", 7.25, 4.50)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Show the dawn shot.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
        fnCutscene([[ WD_SetProperty("Set Scene Image", "Null") ]])
        fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/WindowReflection/Together") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] The sun will show itself soon.[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] The city is beautiful at night, but during the day, will it be even more breathtaking?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[VOICE|Sophie] It's been so long since I saw the sun.[P] I'm sure the city will glitter as the rays strike it.[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] Half a year of dark, then half a year of light.[P] That happened in some places on Earth, too.[P] Far to the north and south, on the poles.[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] The people there had to adapt each time it changed.[P] Living in total darkness, I'm sure they looked forward to the day.[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] But after the day would come the night again.[P] It was inevitable.[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] Will our day too be followed by the night?[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] Sophie, I know it looks bad now, but I will liberate this city.[P] I will make Regulus City a paradise for all, I swear it.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[VOICE|Sophie] I believe you, and I'll be right there with you when you do.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[VOICE|Sophie] But the night isn't over yet.[P] A lot of golems won't see the dawn...[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] 55...[P] We went to the LRT facility because they keep long-term storage logs of all signals on Regulus there.[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] So 55 thought they might have logs of who she was, since she had otherwise been scrubbed from the network.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[VOICE|Sophie] And...[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] And...[P] we found those logs...[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] She -[P] was not the person who we had thought she was.[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] I watched as she...[P] retired them.[P] All of them.[P] Every single golem I found in the Cryogenics facility...[B][C]") ]])
        fnCutscene([[ Append("Sophie:[VOICE|Sophie] ...[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] And the worst part is that I wasn't surprised.[P] Not really.[P] Not deep down.[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] Oh, sure, I was surprised.[P] Shocked by the sheer brutality of it.[P] Shocked at the cold efficiency.[P] Shocked at the strewn casings and fragments and exposed wires.[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] But if you asked me if I thought 55 was capable of such a massacre, even before seeing it, I would not have said no.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[VOICE|Sophie] But that's not her![P] That's not 55![B][C]") ]])
        fnCutscene([[ Append("Sophie:[VOICE|Sophie] It's not the same person, she's better![P] You said she wanted to change![B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] And was that me saying it because I believed it, or because I wanted it to be true so badly that I believed it?[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] I admit, I have a tendency to fool myself.[P] I told myself everything was okay.[P] I know how to trick myself into being happy when I'm broken inside.[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] But then I became a robot.[P] I met you.[P] And I told myself that was all over.[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] And it wasn't![P] I was lying to myself and I lied to 55 along with me![B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] Can you imagine how betrayed she would feel -[P] if she even feels anything![B][C]") ]])
        fnCutscene([[ Append("Sophie:[VOICE|Sophie] Christine, stop this right now![B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] ..![B][C]") ]])
        fnCutscene([[ Append("Sophie:[VOICE|Sophie] Aren't you just doing it again, but in the other direction?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[VOICE|Sophie] You have so much to be hopeful for, and here you are beating yourself up![P] Well, I won't let you![B][C]") ]])
        fnCutscene([[ Append("Sophie:[VOICE|Sophie] 55 probably looks at you as her only true friend.[P] You stuck with her and believed in her, and that counts![B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] She hasn't returned any of my messages for the last three weeks...[P] She's ignoring me...[B][C]") ]])
        fnCutscene([[ Append("Sophie:[VOICE|Sophie] She's going through a lot, too.[P] You don't have to be there for her right now, but you do when she is ready for you.[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] Yeah...[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] I suppose she's learning what her challenges are.[P] She's finding out what she has to do to avoid becoming the same person as before.[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] And -[P] even if I'm not sure, I know she'll pull it off.[P] When she sets her mind to something, she doesn't give up.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[VOICE|Sophie] Christine, you are my light.[P] My bright spot.[P] You're why I drag myself out of the defragmentation pod every day.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[VOICE|Sophie] Be that for her.[P] She'll get tired or scared or demotivated, and want to stop.[P] Keep her going.[P] That's what you have to do.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(45)
        fnCutsceneBlocker()
    
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "PDU", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
        fnCutscene([[ Append("Christine:[E|Offended] She keeps insisting she doesn't feel anything.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Offended] And you believed her?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Offended] You should know, you claim to be the expert at lying to yourself.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Sad] Even if she thinks she doesn't, she does.[P] I bet her heart broke when she saw herself in those videographs.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] Yeah...[P] Because that's what I would do.[P] I'd tell myself I felt nothing rather than feel pain.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Do you know what it's like being...[P] different?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Angry] Being shouted at on the street because you walk differently than everyone else?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Angry] Being reminded that you're bad and wrong and don't fit someone else's expectations and that's all that matters?[P] That random other people can't make your day, but they can ruin it, so they do?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] It's unbearable, so you don't bear it.[P] You bury it.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] She must be doing that right now.[P] I -[P] PDU, send Unit 2855 an encrypted message please.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Tell her to meet me in the repair bay basement, and that it's extremely important.[B][C]") ]])
        fnCutscene([[ Append("PDU:[E|Neutral] Message sent.[B][C]") ]])
        fnCutscene([[ Append("PDU:[E|Question] Unit 771852, someone is at the door.[P] Would you like me to allow them in?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] Your door didn't stop me...[B][C]") ]])
        fnCutscene([[ Append("PDU:[E|Neutral] Unit 771852 has given Unit 499323 root access permissions to her quarters, and this PDU.[B][C]") ]])
        fnCutscene([[ Append("PDU:[E|Happy] She has also put maximum priority on messages from Unit 499323, including authorization to wake her from defragmentation if necessary.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Blush] Oh really?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Blush] Erm...[P] That's actually kind of a big step...[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] Oh, I know![P] PDU, let the visitor in.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Angry] Hey![B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Happy] Hee hee![P] You've made a big mistake, dearest![P] Now I'm never going to stop messaging you while you're sleeping![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Blush] I love you so much right now...[P] Tease me more...") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(65)
        fnCutsceneBlocker()
        
        --Unload assets.
        fnCutscene([[ fnUnloadBitmapsFromList("Chapter 5 Window") ]])
        
        --Golem enters.
		fnCutsceneTeleport("Golem", 8.25, 10.50)
        fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
        
        --They turn to face her.
        fnCutsceneFace("Christine", 0, 1)
        fnCutsceneFace("Sophie", 0, 1)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
        
        --Show the dawn shot.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
        fnCutscene([[ Append("Golem:[E|Neutral] My apologies, Lord Unit, but I have a message for you.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] A message?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] That can't be sent to my PDU via the network?[B][C]") ]])
        fnCutscene([[ Append("Golem:[E|Neutral] Special order, Lord Unit.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] No problem![P] Sorry if I gave you the wrong idea there.[P] We're just in the middle of something.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Are you all right?[P] You look pretty down.[B][C]") ]])
        fnCutscene([[ Append("Golem:[E|Neutral] No Lord Unit.[P] Everything is optimal.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] I'm a repair unit.[P] Your eyes are in low-power mode, and I can see micro-fractures building up on the outside of your joints.[P] A full scan would likely show overstressing.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] So, are you sure you're all right?[B][C]") ]])
        fnCutscene([[ Append("Golem:[E|Neutral] So the rumours were true about Lord Unit Christine...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Rumours?[B][C]") ]])
        fnCutscene([[ Append("Golem:[E|Neutral] ..![P] I-I-I mean, Lord Unit 771852![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Laugh] Ha ha![P] No need to be so formal![P] Christine is fine![B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] Some of the Slave Units seem to be taking a liking to you.[P] Maybe it's because you don't shout at them.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] I've been putting in a good word here and there too, of course.[B][C]") ]])
        fnCutscene([[ Append("Golem:[E|Neutral] I was talking to some of the other Slave Units on the tram here.[P] They say you're...[P] nice.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Thanks![P] I try.[P] Make sure to see a repair unit about that stress fractures and get a good defragmentation in as soon as you can.[B][C]") ]])
        fnCutscene([[ Append("Golem:[E|Neutral] ...[P] Thank you, Lord Unit.[B][C]") ]])
        fnCutscene([[ Append("Golem:[E|Neutral] Oh, the message![B][C]") ]])
        fnCutscene([[ Append("Golem:[E|Neutral] Here you go, it's a special note.[B][C]") ]])
        fnCutscene([[ Append("Golem:[E|Neutral] I'm not privy to the contents, of course, but it seems that the other Lord Units were happy to get theirs.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Aren't you curious what's in it?[B][C]") ]])
        fnCutscene([[ Append("Golem:[E|Neutral] It's -[P] not my place to know.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Balderdash.[P] Come, let's read it together.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Christine moves to the couch.
		fnCutsceneMove("Golem", 8.25, 6.50)
		fnCutsceneMove("Sophie", 7.25, 7.50)
		fnCutsceneMove("Christine", 7.25, 6.50)
        fnCutsceneFace("Golem", -1, 0)
        fnCutsceneFace("Christine", 1, 0)
        fnCutsceneFace("Sophie", 1, 0)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Show the dawn shot.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Now look at this, a handwritten note on embroidered plastic paper...[P] luxurious![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] 'Dear Lord Unit 771852.[P] You are cordially invited to the annual Sunrise Gala, to be hosted in the Arcane Academy's ballroom hall.'[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Blah blah blah, date, time...[P] And I can bring a plus one![P] Splendid![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] ...[P] What's the Sunrise Gala?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Happy] OH.[B][C]") ]])
        fnCutscene([[ Append("Golem:[E|Neutral] MY.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Happy] GOOOOOSH.[B][C]") ]])
        fnCutscene([[ Append("Golem:[E|Neutral] IT'S TIME FOR THE GALA I ALMOST FORGOT![B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Happy] SQUEEEE!!![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] (Yikes!)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] So what's this gala, then?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] Oh oh oh, yes, you're a new unit, this is your first year here.[P] Sorry.[P] But -[P] ohmygoshyougettogotothegala![B][C]") ]])
        fnCutscene([[ Append("Golem:[E|Neutral] So that's why I'm delivering handwritten letters![P] It makes sense now![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] Gala.[P] What is it already?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Happy] Only the biggest and fanciest event of the year![B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] Ah, well, one of the two.[P] The Sunset Gala is equally as big, as you might imagine.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] See -[P] and correct me if I misremember something, PDU -[P] this is an event that goes back to the founding of the Arcane Academy.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] Because it's six months of dark and six of light, the mages would celebrate the sunrise and sunset each year with a big party -[P] and probably get really drunk.[B][C]") ]])
        fnCutscene([[ Append("Golem:[E|Neutral] Humans do love their alcohol.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Happy] It's one of the few traditions that has carried over, and now, you get to go![P] Gah I'm so excited![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Happy] Sounds fun![P] Have you ever been?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] ...[P] No.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Sad] Obviously no.[P] No![P] Only the Lord Golems and Command Units get to go.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] Oh.[P] Yes.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] For a moment I forgot what kind of place we lived in.[B][C]") ]])
        fnCutscene([[ Append("Golem:[E|Neutral] Some of my friends have gotten clean-up duty afterwards, but we've never actually seen it in action.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Happy] But now, all the Lord Units are going to be getting the flashiest, shiniest, and prettiest dresses and parading them around![B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Happy] And more importantly, most of the Lord Units go to the gala so they're not supervising us.[P] So we usually have our own little parties, too.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Am I invited to those, too?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] Oh Christine, please.[P] I know what you're trying to do, but you really can't let me hold you from this.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Happy] Ever since I was first converted I've wanted to go.[P] I couldn't stop you.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Well then why don't you come with me?[B][C]") ]])
        fnCutscene([[ Append("Golem:[E|Neutral] Ha ha ha![P] I can see why the other units like you so much![B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Blush] Ahhhh -[P] no, she's serious, I think.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] No, Sophie.[P] I'm not going without my tandem unit.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] There's got to be a way.[B][C]") ]])
        fnCutscene([[ Append("Golem:[E|Neutral] Oh, I'm sorry![P] I'd best be delivering the rest of these.[B][C]") ]])
        fnCutscene([[ Append("Golem:[E|Neutral] But, Lord Unit Christine, if you want to come to one of our parties I'm sure I can convince everyone.[P] Just send me a message.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Certainly![P] Have a good day, and stop by the Sector 96 repair bay if you're ever in the area!") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Golem leaves.
        fnCutsceneFace("Christine", 0, 1)
        fnCutsceneFace("Sophie", 0, 1)
		fnCutsceneMove("Golem", 8.25, 10.50)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Golem enters.
		fnCutsceneTeleport("Golem", -100, -100)
        fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
        fnCutsceneFace("Sophie", 0, -1)
		fnCutsceneBlocker()
        
        --Show the dawn shot.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
        fnCutscene([[ Append("Christine:[E|PDU] ...[P] Hey, look.[P] 55 messaged me back.[P] She's on her way to the repair bay.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] But yes, Sophie.[P] If there's a way for you to attend the gala, I'm going to find it.[P] Don't you worry.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Sad] Attending the gala is what's making me worry, silly.[P] At the very least I'd get kicked out and reprimanded, and worse, they'd probably discipline you, too.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Hmm...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Well, I'll think of something.[P] Hey, have you met 55 yet?[B][C]") ]])
        if(iSXUpgradeQuest < 3.0) then
            fnCutscene([[ Append("Sophie:[E|Smirk] Heh -[P] when I chased her off after she shocked you.[P] Remember?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Not the best circumstances.[P] Let's give you a proper introduction, shall we?[B][C]") ]])
        else
            fnCutscene([[ Append("Sophie:[E|Smirk] Hee hee![P] Of course![B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Smirk] She carried SX-399 all the way to me so I could fix her, remember?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Oh, of course![B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Happy] By the way, how is that poor dear doing with the shiny upgrades I gave her?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] She's never been better, and I hope someday soon we can all hang out together without fear.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Now let's go tell 55 all about the gala, and cheer her up a bit, shall we?[B][C]") ]])
        end
        fnCutscene([[ Append("Sophie:[E|Smirk] Lead the way, dearest.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Transition to the next part of the scene.
        fnCutscene([[ AL_BeginTransitionTo("RegulusCityG", "FORCEPOS:14.0x8.0x0") ]])
        fnCutsceneBlocker()
    
    --Gala Dress Scene.
    elseif(iIsGalaTime == 1.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N", 2.0)
        LM_ExecuteScript(gsRoot .. "Chapter 5/Scenes/500 Normal/Gala Assault Briefing/DressTime.lua")
    
	end
end
