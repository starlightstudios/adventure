-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[GolemDA]|
    if(sActorName == "GolemDA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Ugh, I'm on the 'free unit' roster so I keep getting one-shift assignments all over the city.[B][C]") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] I don't smell bad, do I?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[VOICE|Sophie] Olfactory sensors detect a whiff of bleach.[B][C]") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] I got assigned to sewage treatment in the biolabs.[P] Had to go unplug a drain pipe.[P] Can I just say synthetic life is a clear improvement?") ]])
        fnCutsceneBlocker()
    
    -- |[GolemDB]|
    elseif(sActorName == "GolemDB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Things have been really crazy in the city lately, but Sector 96 is so calm.[P] Maybe it's because all my friends are here.") ]])
        fnCutsceneBlocker()
    
    -- |[GolemDC]|
    elseif(sActorName == "GolemDC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Sophie![P] I made a new video game, want to try it?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[VOICE|Sophie] 'The Chesty Robot Saves The World'.[P] She looks a lot like you.[B][C]") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] I -[P] it's a coincidence![B][C]") ]])
        fnCutscene([[ Append("Sophie:[VOICE|Sophie] These controls are really tight, and I am in love with the art style.[P] I might have to give this a full playthrough later!") ]])
        fnCutsceneBlocker()
    
    -- |[GolemDD]|
    elseif(sActorName == "GolemDD") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Yes, Lord Golem.[P] Whatever you say, Lord Golem.[P] Yes, Lord Golem.") ]])
        fnCutsceneBlocker()
    
    -- |[GolemDE]|
    elseif(sActorName == "GolemDE") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Unit 499323, shouldn't you be in your department?[P] Oh, a secrebot.[P] My apologies.[P] Go...[P] repair her, or whatever it is you do.") ]])
        fnCutsceneBlocker()
    
    -- |[GolemDF]|
    elseif(sActorName == "GolemDF") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] I've got some spare work credits these days, but what should I even spend them on?[P] Everything is on back order!") ]])
        fnCutsceneBlocker()
    
    -- |[GolemDG]|
    elseif(sActorName == "GolemDG") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] A secrebot![P] My, she looks like Unit 771852.[P] Then again -[P] ugh, that robot has such an ego about her![P] It figures that she'd have a secrebot made to flatter herself![B][C]") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] *grumble*[P] I want a secrebot that looks like me...") ]])
        fnCutsceneBlocker()
    end
end
