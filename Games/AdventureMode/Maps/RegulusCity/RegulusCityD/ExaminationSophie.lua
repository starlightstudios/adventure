-- |[ ======================================= Examination ====================================== ]|
--Called if Sophie is the party leader during an examination.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[ ========================================= Exits ========================================== ]|
--Exit
if(sObjectName == "ToCityA") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusCityA", "FORCEPOS:21.0x6.0x0")
	
--Exit
elseif(sObjectName == "ToCityE") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusCityE", "FORCEPOS:16.0x13.0x0")

-- |[ ====================================== Examinables ======================================= ]|
-- |[Other]|
elseif(sObjectName == "Butterbomber") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("[VOICE|Sophie]('Beware the Butterbomber!'[P] Bugs are gross but I guess it's a horror videograph, so it's supposed to be scary...)") ]])
    fnCutsceneBlocker()

-- |[Tandem Reading]|
--Open computer.
elseif(sObjectName == "Open") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("[VOICE|Sophie](Maybe I should plug Christine into one of these and make her write out something lewd![P] No, bad Sophie!)") ]])
    fnCutsceneBlocker()

-- |[Tetris]|
--Blockchain program.
elseif(sObjectName == "TetrisMachine") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("[VOICE|Sophie](I don't have time to solve some dumb blockchain program, I need to save my tandem unit!)") ]])
    fnCutsceneBlocker()

-- |[Space Invaders]|
--Yeah, it's not space invaders right?
elseif(sObjectName == "InvadersMachine") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("[VOICE|Sophie](I thought this was a video game for years until I found out it's actually a logic probe simulation.[P] Art imitates sythentic life, I suppose.)") ]])
    fnCutsceneBlocker()

-- |[Blue Sphere]|
--Yeah, it's not space invaders right?
elseif(sObjectName == "SphereMachine") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("[VOICE|Sophie](Oh no, what would happen if you put a secrebot on a rolling object.[P] Like a sphere or a log.[P] She'd be stuck in place, forever wheeling and going nowhere!)") ]])
    fnCutsceneBlocker()


--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
