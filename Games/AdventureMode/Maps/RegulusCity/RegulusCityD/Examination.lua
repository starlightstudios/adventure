-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Sophie Leader]|
--During the secrebot sequence, Sophie is in the party lead and uses a different file.
local b254SophieLeading = fnIsSophieLeader()
if(b254SophieLeading == true) then
    LM_ExecuteScript(fnResolvePath() .. "ExaminationSophie.lua", sObjectName)
    return
end

-- |[ ========================================= Exits ========================================== ]|
--Exit
if(sObjectName == "ToCityA") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusCityA", "FORCEPOS:21.0x6.0x0")
	
--Exit
elseif(sObjectName == "ToCityE") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusCityE", "FORCEPOS:16.0x13.0x0")

-- |[ ====================================== Examinables ======================================= ]|
-- |[Other]|
elseif(sObjectName == "Butterbomber") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("[VOICE|Leader]('Beware the Butterbomber!'[P] Looks like a new game coming soon.[P] Love the colors on that monster!)") ]])
    fnCutsceneBlocker()

-- |[Tandem Reading]|
--Open computer.
elseif(sObjectName == "Open") then

	--Variables.
	local iIsOnDate               = VM_GetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N")
	local iExplainedOpenComputers = VM_GetVar("Root/Variables/Chapter5/Sophie/iExplainedOpenComputers", "N")
    local iIsGalaTime             = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N")
    local iStartedShoppingSequence = VM_GetVar("Root/Variables/Chapter5/Scenes/iStartedShoppingSequence", "N")

    --Gala case.
    if(iIsGalaTime > 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[VOICE|Christine](The computers in here are all on standby.)") ]])
		fnCutsceneBlocker()

	--Not on a date. Examine the stuff.
	elseif(iIsOnDate == 0.0 or iStartedShoppingSequence == 1.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[VOICE|Christine](A computer, not currently set to do anything specific.)") ]])
		fnCutsceneBlocker()

	--Needs to explain maintenance.
	elseif(iIsOnDate == 1.0 and iExplainedOpenComputers == 0.0) then
		VM_SetVar("Root/Variables/Chapter5/Sophie/iExplainedOpenComputers", "N", 1.0)
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Looks like this computer is open.[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Oh, I'd just love to download some -[P] instructional manuals.[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] We could read them together![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Wouldn't downloading them instantly read them?[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Hah![P] You're such a new Golem it's almost funny![B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Downloading something is easy, you just store it on your hard drive.[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Reading it is different.[P] You have to run it through your processor and decide which parts of your core to modify.[P] Don't believe everything you read![B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] ...[P] Now that I think of it, organics do the same thing, don't they?[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Reading is a lot slower.[P] It's like the difference between hearing and understanding something.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Oh, all right![B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] And we could tandem read them.[P] You'd just hook into my datastream, and we'd be processing the same data.[P] Ooooh.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] That sounds like a lot of fun![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Shall we spend the evening tandem reading?[BLOCK]") ]])

		--Decision script is this script. It must be surrounded by quotes.
		local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
		fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
		fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"TandemRead\") ")
		fnCutscene(" WD_SetProperty(\"Add Decision\", \"No Thanks\",  " .. sDecisionScript .. ", \"NoClose\") ")
		fnCutsceneBlocker()

	--Date option.
	elseif(iIsOnDate == 1.0 and iExplainedOpenComputers == 1.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Shall we spend the evening tandem reading?[BLOCK]") ]])

		--Decision script is this script. It must be surrounded by quotes.
		local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
		fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
		fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"TandemRead\") ")
		fnCutscene(" WD_SetProperty(\"Add Decision\", \"No Thanks\",  " .. sDecisionScript .. ", \"NoClose\") ")
		fnCutsceneBlocker()
	end
	
--Spend a date reading in tandem.
elseif(sObjectName == "TandemRead") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] So what should we tandem read?[B][C]") ]])
	fnCutscene([[ Append("Sophie:[E|Neutral] I'll leave that up to you, tandem unit.[B][C]") ]])
	fnCutscene([[ Append("Sophie:[E|Neutral] I like all genres, as long as it's educational or funny or scary.[P] Whatever you like.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] All right...[BLOCK]") ]])

	--Decision script is this script. It must be surrounded by quotes.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"The Doll and the Bolts\", " .. sDecisionScript .. ", \"DollBolts\") ")
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"That\", " .. sDecisionScript .. ", \"That\") ")
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"The Drone and her Reflection\", " .. sDecisionScript .. ", \"DroneMirror\") ")
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"Forget It\",  " .. sDecisionScript .. ", \"NoClose\") ")
	fnCutsceneBlocker()

--All the novels do the same thing, but switch the routing function.
elseif(sObjectName == "DollBolts" or sObjectName == "That" or sObjectName == "DroneMirror") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")

	--Fade.
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(85)
	fnCutsceneBlocker()

	--Execute the conversation routing case.
	LM_ExecuteScript(gsRoot .. "Chapter 5/Scenes/500 Normal/Sophie Conversation/Z Reading Routing.lua", sObjectName)
	fnCutsceneBlocker()

	--Fade.
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
	fnCutsceneWait(85)
	fnCutsceneBlocker()
	
	--Variables.
	local iSophieFirstDateState = VM_GetVar("Root/Variables/Chapter5/Scenes/iSophieFirstDateState", "N")
	
	--On the first date, dialogue changes.
	if(iSophieFirstDateState == 0.0) then
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSophieFirstDateState", "N", 1.0)
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] That was fun![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] The writing wasn't the best...[P] But maybe I'm too picky.[P] I majored in literature.[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Majored?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] I was educated at Oxford.[P] But -[P] I don't remember it too well right now...[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] You were educated as an organic?[P] You must have been very rich, or very lucky![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] The first one, I'm afraid.[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Hee hee![P] No surprise there.[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] That was a lovely date, Christine, but I really need to defragment my drive. So...[B][C]") ]])
        
		fnCutscene([[ Append("Christine:[E|Neutral] Date?[P] That was a date?[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Blush] Uh, uh![P] No, I was just showing you around![P] Hee hee![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|PDU] Matchmaker -[P] shut up, PDU![B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Blush] Matchmaker?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Offended] It's my stupid PDU.[P] It said you were ogling me with your big pretty eyes and - [B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Offended] You keep flicking your hair a bit when you talk.[P] You keep looking at me.[P] I can't take my eyes off you.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Offended] What is your deal?[P] We're going to be working together so you may as well just tell me why you don't like me![B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Sad] Don't like you?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Offended] Yeah![P] Am I ugly, or weird, or something?[P] As a lord golem I order you to tell me![B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] ...[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Do you want to kiss me?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Offended] Of course![B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] *smooch*[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Blush] H-[P]huh?[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Happy] Want another one?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Blush] Yeah![B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] *smooch*[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Blush] My goodness, Sophie, Unit 499323, do you like me?[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Happy] Of course I do![P] I like you a lot![P] How do you not see that?[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Happy] We were holding hands while we tandem read, didn't you know?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Blush] I haven't been a golem for long, I thought you have to do that![P] I thought that's how it worked![B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Offended] You're not fooling anyone.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] I tried, but...[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] Do you...[P] want to be my tandem unit?[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Happy] I thought you'd never ask![P] Of course![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] But I'm your lord golem...[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Blush] Yeah, but I don't care what anyone else thinks![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] What does that have to do with it?[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] We're different model variants.[P] Slave and lord, we're not supposed to date.[P] You didn't know?[P] Well, you are a fresh convert so...[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Yes, we'll be looked down on.[P] But I don't care.[P] I want you.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] I want you too![B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Offended] Darn it.[P] Now we can't have weeks of will-they-won't-they sexual tension...[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Sorry.[P] I can pretend not - [B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Offended] Oh no you don't![B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Happy] Phew![P] What a day it has been![P] I really need a recharge![P] Would you like to walk me back to my room?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] Of course.[P] The habitation block is just east of the surface exit, right?[P] Let's go!") ]])
        
		--[=[fnCutscene([[ Append("Sophie:[E|Neutral] I'd love to keep reading with you, but it's late.[P] We should go defragment our drives.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Ummm...[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Hee hee![P] You haven't even seen your quarters yet, have you?[P] Do you know where they are?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Vaguely.[P] I didn't download the directory when I got here.[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Well then allow me to show you.[P] We need to take the elevators east of the airlock.[P] I'll point them out when we get close.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Thanks, Sophie.[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] This does count as walking a lady back to her room.[P] We use the same elevators, of course.[P] All units in the sector use the same domicile block.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] All right.[P] Let's go.") ]])]=]
		fnCutsceneBlocker()
	
	--After the cutscene is over, Sophie will request to go back to the maintenance bay.
	else
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Well, I suppose it's back to the maintenance bay for me.[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Tandem reading is a blast! We should do this again![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Count on it.[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Would you care to escort your tandem unit back to her function?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] I would like nothing better.") ]])
		fnCutsceneBlocker()
	end
	
	--Variables.
	VM_SetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N", 2.0)

-- |[Tetris]|
--Blockchain program.
elseif(sObjectName == "TetrisMachine") then

	--Variables.
	local iIsOnDate        = VM_GetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N")
	local iExplainedTetris = VM_GetVar("Root/Variables/Chapter5/Sophie/iExplainedTetris", "N")
    local iIsGalaTime      = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N")

    --Gala case.
    if(iIsGalaTime > 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[VOICE|Christine](The computers in here are all on standby.)") ]])
		fnCutsceneBlocker()

	--Not on a date. Examine the stuff.
	elseif(iIsOnDate == 0.0) then
		
		--Christine doesn't know that Tetris is.
		if(iExplainedTetris == 0.0) then
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ Append("[VOICE|Christine](This computer is set to some complex looking program.)") ]])
			fnCutsceneBlocker()
		
		--Christine knows what Tetris is.
		else
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ Append("[VOICE|Christine](This computer is set to a quantum-blockchain program.)") ]])
			fnCutsceneBlocker()
		end

	--Needs to explain maintenance.
	elseif(iIsOnDate == 1.0 and iExplainedTetris == 0.0) then
		VM_SetVar("Root/Variables/Chapter5/Sophie/iExplainedTetris", "N", 1.0)
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] What's this program, tandem unit?[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Seems some unit left a quantum blockchain compiler running.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Offended] (Quantum blockchain?[P] Looks oddly familiar...)[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Smirk] You basically have to solve these to add parts to the blockchain, and it can't be done without the intuition of our CPUs.[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] It's pretty dry stuff.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] Let's find something more fun!") ]])

	--Date option.
	elseif(iIsOnDate == 1.0 and iExplainedTetris == 1.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] (Quantum blockchain, huh? It still looks really familiar to me...)") ]])
	end
	
-- |[Space Invaders]|
--Yeah, it's not space invaders right?
elseif(sObjectName == "InvadersMachine") then

	--Variables.
	local iIsOnDate               = VM_GetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N")
	local iExplainedSpaceInvaders = VM_GetVar("Root/Variables/Chapter5/Sophie/iExplainedSpaceInvaders", "N")
    local iIsGalaTime             = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N")

    --Gala case.
    if(iIsGalaTime > 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[VOICE|Christine](The computers in here are all on standby.)") ]])
		fnCutsceneBlocker()
        
	--Not on a date. Examine the stuff.
	elseif(iIsOnDate == 0.0) then
		
		--Christine doesn't know that Space Invaders is.
		if(iExplainedSpaceInvaders == 0.0) then
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ Append("[VOICE|Christine](This computer is set to some complex looking program.)") ]])
			fnCutsceneBlocker()
		
		--Christine knows what Space Invaders is.
		else
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ Append("[VOICE|Christine](This computer is set to a white-hat simulation.)") ]])
			fnCutsceneBlocker()
		end

	--Needs to explain maintenance.
	elseif(iIsOnDate == 1.0 and iExplainedSpaceInvaders == 0.0) then
		VM_SetVar("Root/Variables/Chapter5/Sophie/iExplainedSpaceInvaders", "N", 1.0)
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] Hm, this program looks familiar...[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Smirk] This is a white-hat hacking simulator.[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Smirk] The security units use these to look for flaws in our system architecture.[P] They simulate vectors and you have to come up with fixes on the fly.[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Smirk] It's staggeringly complex.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] It doesn't look complex.[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Smirk] Well, you and I are very smart units, 771852.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] Well, I was in the mood for something a bit more fun.") ]])

	--Date option.
	elseif(iIsOnDate == 1.0 and iExplainedSpaceInvaders == 1.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] (Spending an evening looking for security flaws isn't my idea of exciting...)") ]])
	end
	
-- |[Blue Sphere]|
--Yeah, it's not space invaders right?
elseif(sObjectName == "SphereMachine") then

	--Variables.
	local iIsOnDate            = VM_GetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N")
	local iExplainedBlueSphere = VM_GetVar("Root/Variables/Chapter5/Sophie/iExplainedBlueSphere", "N")
    local iIsGalaTime          = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N")

    --Gala case.
    if(iIsGalaTime > 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[VOICE|Christine](The computers in here are all on standby.)") ]])
		fnCutsceneBlocker()

	--Not on a date. Examine the stuff.
	elseif(iIsOnDate == 0.0) then
		
		--Christine doesn't know that Blue Sphere is.
		if(iExplainedBlueSphere == 0.0) then
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ Append("[VOICE|Christine](This computer is set to some simple looking program.)") ]])
			fnCutsceneBlocker()
		
		--Christine knows what Blue Sphere is.
		else
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ Append("[VOICE|Christine](This computer is set to Blue Sphere.[P] From Mr. Needlemouse 3.)") ]])
			fnCutsceneBlocker()
		end

	--Needs to explain maintenance.
	elseif(iIsOnDate == 1.0 and iExplainedBlueSphere == 0.0) then
		VM_SetVar("Root/Variables/Chapter5/Sophie/iExplainedBlueSphere", "N", 1.0)
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Okay, now I know I've seen this before.[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Smirk] It's the Blue Sphere minigame from Mr. Needlemouse 3.[P] I guess some unit left it on by mistake.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] Is it fun?[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Smirk] Some units think so, others say it's the worst part of Mr. Needlemouse 3.[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Happy] Now Mr. Needlemouse 2's special stages?[P] Now you're talking.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] So, no to playing this, then.[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] No thanks...[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Happy] ...[P] But now I want to go play Needlemouse Hysteria again...") ]])

	--Date option.
	elseif(iIsOnDate == 1.0 and iExplainedBlueSphere == 1.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] By the way, Sophie. What's your opinion on video games in general?[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Hmmm...[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Smirk] Hard to say if they're a great invention, or the greatest invention...[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Happy] ChocoBromine synthshakes are fighting for the top spot...[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Laugh] Ha ha![P] I'll have to try one sometime!") ]])
	end

-- |[Multi-Purpose Close]|
elseif(sObjectName == "NoClose") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
