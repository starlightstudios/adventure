-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Sophie Leader]|
--During the secrebot sequence, Sophie is in the party lead and uses a different file.
local b254SophieLeading = fnIsSophieLeader()
if(b254SophieLeading == true) then
    LM_ExecuteScript(fnResolvePath() .. "DialogueSophie.lua", sTopicName)
    return
end

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[GolemDA]|
    if(sActorName == "GolemDA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] I am currently unassigned, Lord Golem.[P] I am thus reviewing engineering manuals to improve efficiency.[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] 'She placed her oral cavity against her tandem's receiver and began to thrust in with her tongue.'[P] What sort of manual is this?[B][C]") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Oh dear![P] How did that get on there?[P] I -[P] I - [B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] Carry on, unit.[P] And, please send a copy to my quarters so I might peruse this manual.[P] I want to improve my efficiency as well.[B][C]") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Right away, Lord Golem![P] And...[P] thank you...") ]])
        fnCutsceneBlocker()
    
    -- |[GolemDB]|
    elseif(sActorName == "GolemDB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] There are so many software bugs and not enough time to fix them all.[P] We need to stop spending abduction candidates on experiments and get more programmer Golems.") ]])
        fnCutsceneBlocker()
    
    -- |[GolemDC]|
    elseif(sActorName == "GolemDC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] I recently discovered that reactions and decision training are best done via the 'Video Game', but my Lord Golem continues to deny any merit to such programs.[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] Video Games are an excellent use of time. Carry on, unit.[B][C]") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Right away, Lord Golem![B][C]") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] (Sheesh, I wish all the Lord Golems were as smart as Unit 771852!)") ]])
        fnCutsceneBlocker()
    
    -- |[GolemDD]|
    elseif(sActorName == "GolemDD") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Yes, Lord Golem.[P] Whatever you say, Lord Golem.[P] Yes, Lord Golem.") ]])
        fnCutsceneBlocker()
    
    -- |[GolemDE]|
    elseif(sActorName == "GolemDE") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Are you being stupid intentionally, Unit 12125?[P] Record the outcomes as they appear![P] You're useless![P] I'd be better off with a toaster instead!") ]])
        fnCutsceneBlocker()
    
    -- |[GolemDF]|
    elseif(sActorName == "GolemDF") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] When I get some downtime, I like to let my CPU solve simple puzzles.[P] They've even started giving out work credits for it.[P] Maybe I'll be able to get a leg motivator upgrade someday.") ]])
        fnCutsceneBlocker()
    
    -- |[GolemDG]|
    elseif(sActorName == "GolemDG") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] My function assignment recently got changed to 'Roaming Support', so I've been here, earning work credits.[P] Honestly, can't Slave Units do this?[P] Aren't my talents better used elsewhere?") ]])
        fnCutsceneBlocker()
    end
end
