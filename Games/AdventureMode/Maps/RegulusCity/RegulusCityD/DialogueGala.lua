-- |[Root]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    -- |[GolemGalaA]|
    if(sActorName == "GolemGalaA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] It's so quiet in here with the computers off.") ]])
        
    -- |[GolemGalaB]|
    elseif(sActorName == "GolemGalaB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] I'm just going to spend the evening with my tandem unit.[P] I don't need a big party.") ]])
        
    -- |[GolemGalaC]|
    elseif(sActorName == "GolemGalaC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] It really sucks being single on a night like tonight, but I guess it's a chance to get out and meet some units right?[B][C]") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Say...[P] Lord Unit Christine...[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] Sorry, spoken for![B][C]") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Crud...") ]])
    end
end
