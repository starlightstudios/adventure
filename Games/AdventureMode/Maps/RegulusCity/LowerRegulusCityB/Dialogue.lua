-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[Name Resolve]|
    --Resolve the name of the calling entity.
    local sActorName = TA_GetProperty("Name")

    --Set facing.
    TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[VN-19]|
    if(sActorName == "VN-19") then
        
        --Variables.
        local iMetGemcutter = VM_GetVar("Root/Variables/Chapter5/Scenes/iMetGemcutter", "N")
        
        --Achievement.
        AM_SetPropertyJournal("Unlock Achievement", "Sickos")
        
        --Haven't met her yet.
        if(iMetGemcutter == 0.0) then
            
            VM_SetVar("Root/Variables/Chapter5/Scenes/iMetGemcutter", "N", 1.0)
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
            fnCutscene([[ Append("VN-19:[E|Neutral] Ah ha![P] Another daring adventurer seeks out the exquisite VN-19![P] Well met, comrade![B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Excuse me?[B][C]") ]])
            fnCutscene([[ Append("VN-19:[E|Neutral] You are, at current, skulking about in the shadows of Regulus City.[P] Darting between maverick robots, fighting when you cannot run.[B][C]") ]])
            fnCutscene([[ Append("VN-19:[E|Neutral] With daring and panache, you search for the rumoured gemcutter of note.[P] VN-19![P] There she is![P] We've found her![B][C]") ]])
            fnCutscene([[ Append("VN-19:[E|Neutral] Yes, yes,[P] so exciting,[P] I know![B][C]") ]])
            fnCutscene([[ Append("VN-19:[E|Neutral] Now, I care not for your allegiance.[P] Administrator, Maverick, Rebel...[P] There is only one reason you'd seek me out, and that is a desire to have the finest gems cut.[B][C]") ]])
            fnCutscene([[ Append("VN-19:[E|Neutral] ...[P] Right?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Uh, yes?[B][C]") ]])
            fnCutscene([[ Append("VN-19:[E|Neutral] Superb![P] Well, I assume you know how this works.[P] Shall we get to it then?[BLOCK]") ]])
        
        --Repeats.
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
            fnCutscene([[ Append("VN-19:[E|Neutral] You return![P] What services might this artisan provide?[BLOCK]") ]])
        end

        --Decision setup.
        local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
        fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Can you cut some gems?\", " .. sDecisionScript .. ", \"Gemcutting\") ")
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Tell me about yourself.\",  " .. sDecisionScript .. ", \"Talk\") ")
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"I'm good, thanks.\",  " .. sDecisionScript .. ", \"Bye\") ")
        fnCutsceneBlocker()
    end
    
-- |[ =================================== Responses to VN-19 =================================== ]|
--Activate gemcutting mode.
elseif(sTopicName == "Gemcutting") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
    fnCutscene([[ Append("VN-19:[E|Neutral] Splendiferous![P] Let's get started!") ]])
	fnCutsceneBlocker()

	--Run the shop.
	fnCutscene([[ AM_SetShopProperty("Show", "VN-19's Gem Shop", gsRoot .. "Maps/RegulusCity/LowerRegulusCityB/Shop Setup.lua", "Null") ]])
	fnCutsceneBlocker()

--Talk.
elseif(sTopicName == "Talk") then
	
	--Dialogue.
	WD_SetProperty("Hide")
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Could you tell me a bit more about yourself?[B][C]") ]])
	fnCutscene([[ Append("VN-19:[E|Neutral] Tell you...[P] about me?[B][C]") ]])
	fnCutscene([[ Append("VN-19:[E|Neutral] Whaddya wanna know about me fer?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Well...[P] I just...[P] want to know more. You seem nice enough.[B][C]") ]])
	fnCutscene([[ Append("VN-19:[E|Neutral] Ain't nobody gives a fig about me, toutse.[P] They want the gems.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Oh...[B][C]") ]])
	fnCutscene([[ Append("VN-19:[E|Neutral] And I am happy to provide![P] Ah ha![P] There is sparingly little to say about me, but so much to say about gems![B][C]") ]])
	fnCutscene([[ Append("VN-19:[E|Neutral] Would you like to hear about how I learned to cut them?[P] You see, my mentor, Ali- [P][CLEAR]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] But, gems aren't everything.[P] What are your hobbies?[B][C]") ]])
	fnCutscene([[ Append("VN-19:[E|Neutral] Cutting gems.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] ...[P] What did you want to be when you were growing up?[B][C]") ]])
	fnCutscene([[ Append("VN-19:[E|Neutral] A gemcutter.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] What's 2+2?[B][C]") ]])
	fnCutscene([[ Append("VN-19:[E|Neutral] Three Adamantite Flakes.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] I think her circuits may be fried.[P] Or, she is an obsessive.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] In any case, gems provide improvements to our combat potency.[P] The Administration overlooks their effects, as they cannot be mass-produced.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] If she wants to cut them for us, let her.[P] It is to both our advantages.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Okay, fine then...") ]])

--Goodbye.
elseif(sTopicName == "Bye") then
	WD_SetProperty("Hide")
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Steam Droid", "Neutral") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Next time.[P] See you later.[B][C]") ]])
	fnCutscene([[ Append("VN-19:[E|Neutral] The lure of my skills will draw you back here as it does to all others.[P] We will meet again!") ]])
	
end
