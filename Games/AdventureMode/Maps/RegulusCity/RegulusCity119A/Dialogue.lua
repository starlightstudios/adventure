-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    --Set facing.
    TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[Golem119AA]|
    if(sActorName == "Golem119AA") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Can I get you a ChemFuel, Lord Unit?[P] Just -[P] remember not to spill it and contact a cleanup crew if you do...") ]])
        fnCutsceneBlocker()
        
    -- |[Golem119AB]|
    elseif(sActorName == "Golem119AB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] You're not supposed to be able to reach me.") ]])
        fnCutsceneBlocker()
        
    -- |[Golem119AC]|
    elseif(sActorName == "Golem119AC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Lord:[VOICE|Golem] I've spent so much time chatting, my ChemFuel has grown cold![P] But, best not to throw it out, or it'll dissolve the disposal pipes.") ]])
        fnCutsceneBlocker()
        
    -- |[Golem119AD]|
    elseif(sActorName == "Golem119AD") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Lord:[VOICE|Golem] *Sigh*.[P] I didn't get invited to the gala this year.[P] Oh well.[P] There's always next year.[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] (I wouldn't be so sure of that...)") ]])
        fnCutsceneBlocker()
        
    -- |[Golem119AE]|
    elseif(sActorName == "Golem119AE") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Lord:[VOICE|Golem] I transferred from the security forces last year and got put in charge of fabric synthesis in Sector 188.[P] Turns out it's three times more stressful.[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] Why is that?[B][C]") ]])
        fnCutscene([[ Append("Lord:[VOICE|Golem] Errors in fabric synthesis mean errors in clothing.[P] Turns out most Lord Units care more about that than security breaches...") ]])
        fnCutsceneBlocker()
        
    -- |[Golem119AF]|
    elseif(sActorName == "Golem119AF") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Lord:[VOICE|Golem] When I'm feeling extra generous, I even allow my subordinates to have a cup of ChemFuel between work shifts.") ]])
        fnCutsceneBlocker()
        
    -- |[Golem119AG]|
    elseif(sActorName == "Golem119AG") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] My Lord Golem is so generous, she allows me to lick the spilled ChemFuel off her shirt.[B][C]") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] I told her I wasn't low on power but she insisted...") ]])
        fnCutsceneBlocker()
        
    -- |[Golem119AH]|
    elseif(sActorName == "Golem119AH") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Please enjoy your shopping experience, Lord Unit.[P] If a staff member can do anything for you, let us know immediately.") ]])
        fnCutsceneBlocker()
    
    end
end
