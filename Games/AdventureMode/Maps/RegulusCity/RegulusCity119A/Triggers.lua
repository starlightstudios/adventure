-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
if(sObjectName == "LeftExit") then
		
    --Get Christine's position.
    EM_PushEntity("Christine")
        local fPlayerX, fPlayerY = TA_GetProperty("Position")
    DL_PopActiveObject()
    
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (This is the foot path connecting to Sector 104.[P] We don't need to go that way.)") ]])
    fnCutsceneBlocker()
    
    fnCutsceneMove("Christine", 14.25, fPlayerY / gciSizePerTile)
    fnCutsceneMove("Sophie", 14.25, fPlayerY / gciSizePerTile)
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Fold Party") ]])
    fnCutsceneBlocker()
        
elseif(sObjectName == "RightExit") then
		
    --Get Christine's position.
    EM_PushEntity("Christine")
        local fPlayerX, fPlayerY = TA_GetProperty("Position")
    DL_PopActiveObject()
    
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (This is the foot path connecting to Sector 138.[P] We don't need to go that way.)") ]])
    fnCutsceneBlocker()
    
    fnCutsceneMove("Christine", 24.25, fPlayerY / gciSizePerTile)
    fnCutsceneMove("Sophie", 24.25, fPlayerY / gciSizePerTile)
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Fold Party") ]])
    fnCutsceneBlocker()
    
-- |[Cutscenes]|
elseif(sObjectName == "FirstTimeShopping") then

    --Repeat check.
    local iShowedGetOffTram = VM_GetVar("Root/Variables/Chapter5/Scenes/iShowedGetOffTram", "N")
    if(iShowedGetOffTram == 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iShowedGetOffTram", "N", 1.0)
    
    --Movement.
    fnCutsceneMove("Christine", 18.25, 16.50)
    fnCutsceneMove("Sophie", 19.25, 16.50)
    fnCutsceneFace("Christine", 0, 1)
    fnCutsceneFace("Sophie", 0, 1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] This place is really nice...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] You've never been to Sector 119 before?[P] But you sounded like you had.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] I have, actually.[P] I've delivered repaired appliances here before.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] But, we Slave Units have back access paths.[P] The Lord Units don't want us to be seen if possible when we're making deliveries.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] What about the employees?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] If you're assigned to a shop, you're expected to enter and exit via the back access paths.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] I have a friend who works in the Chez Fuel shop.[P] She's always getting her chassis corroded...[P] and she tells me about her job while I fix her![P] Hee hee![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] But I guess...[P] I'm out here now...[P] where everyone can see me...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Relax, silly![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] If anyone asks, you're here to -[P] uh -[P][EMOTION|Christine|Laugh] carry my purchases![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] But you don't actually have to...[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] Yeah, just, act synthetic.[P] I'm supposed to be here.[P] Nobody can punish me...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Happy] Just think of all the dresses and devices we'll get to see![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] Yeah![P] Can we visit the pet shop?[P] Oh -[P] and see the shoes?[P] Yeah, this'll be great![P] Let's go!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Fold the party.
    fnCutsceneMove("Sophie", 18.25, 16.50)
    fnCutsceneBlocker()
	fnCutscene([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()
    
end
