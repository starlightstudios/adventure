-- |[ ======================================= Examination ====================================== ]|
--Called if Sophie is the party leader during an examination.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[ ========================================= Exits ========================================== ]|
--Exit
if(sObjectName == "ToCityD") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusCityD", "FORCEPOS:12.0x4.0x0")
	
--Exit
elseif(sObjectName == "ToCityF") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusCityF", "FORCEPOS:19.0x12.0x0")

-- |[Examinables]|
elseif(sObjectName == "Explosion") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("[VOICE|Sophie](I love a good action videograph but this one is too much, it's just 50 minutes of explosions.)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "Arcee") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("[VOICE|Sophie](I love the vehicloids![P] This one turns into a motorcycle and beats up bad robots!)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "Miku") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("[VOICE|Sophie](I guess humans can be attractive but if you're a robot why have human skin and features?[P] Embrace your chrome, girl!)") ]])
    fnCutsceneBlocker()

-- |[Watching Videographs]|
--Movies! On a date!
elseif(sObjectName == "VideographL" or sObjectName == "VideographR") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("[VOICE|Sophie](I'd love to watch a videograph with Christine but I think 55 would kill me for it.[P] Better get her fixed.)") ]])
    fnCutsceneBlocker()

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
