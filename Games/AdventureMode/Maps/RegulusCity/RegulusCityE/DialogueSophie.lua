-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
    
    -- |[GolemEA]|
    if(sActorName == "GolemEA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Ah, Unit 499323.[P] Is 771852 still organizing that charity drive for homeless scraprats?") ]])
        fnCutsceneBlocker()
        
    -- |[GolemEB]|
    elseif(sActorName == "GolemEB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Hey Sophie.[P] Yep, in the theater again today.[P] Same as ever...") ]])
        fnCutsceneBlocker()

    -- |[GolemEC]|
    elseif(sActorName == "GolemEC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Remember that Darkmatter videograph idea you pitched earlier?[P] My lord golem shot it down, then not 48 hours later, pitches me a videograph about darkmatters with the exact same plot!") ]])
        fnCutsceneBlocker()
        
    -- |[GolemED]|
    elseif(sActorName == "GolemED") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Sophie![P] We're -[P] in here![B][C]") ]])
        fnCutscene([[ Append("Sophie:[VOICE|Sophie] You didn't even put a notice on the door![P] I swear you [P]*want*[P] units to walk in on you!") ]])
        fnCutsceneBlocker()
        
    -- |[GolemEE]|
    elseif(sActorName == "GolemEE") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Don't mind my tandem unit, we're just holding hands.") ]])
        fnCutsceneBlocker()
        
    -- |[GolemEF]|
    elseif(sActorName == "GolemEF") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] All I need is two replacement chips and I can get this room fixed, and I can't even get those.[P] Maybe I can go scrounging in the undercity for a spare.") ]])
        fnCutsceneBlocker()
    end
end
