-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Sophie Leader]|
--During the secrebot sequence, Sophie is in the party lead and uses a different file.
local b254SophieLeading = fnIsSophieLeader()
if(b254SophieLeading == true) then
    LM_ExecuteScript(fnResolvePath() .. "ExaminationSophie.lua", sObjectName)
    return
end

-- |[ ========================================= Exits ========================================== ]|
--Exit
if(sObjectName == "ToCityD") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusCityD", "FORCEPOS:12.0x4.0x0")
	
--Exit
elseif(sObjectName == "ToCityF") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusCityF", "FORCEPOS:19.0x12.0x0")

-- |[Examinables]|
elseif(sObjectName == "Explosion") then
    local iIsGalaTime = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N")
    if(iIsGalaTime == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("[VOICE|Christine](A videograph of things exploding.[P] There may or may not be characters involved, it's hard to say.)") ]])
        fnCutsceneBlocker()
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("[VOICE|Christine](Nothing is playing at the moment.)") ]])
        fnCutsceneBlocker()
	end
    
elseif(sObjectName == "Arcee") then
    local iIsGalaTime = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N")
    if(iIsGalaTime == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("[VOICE|Christine](An action videograph is playing.[P] Apparently this robot can turn into a motorcycle at will.[P] What a useful function!)") ]])
        fnCutsceneBlocker()
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("[VOICE|Christine](Nothing is playing at the moment.)") ]])
        fnCutsceneBlocker()
	end
	
elseif(sObjectName == "Miku") then
    local iIsGalaTime = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N")
    if(iIsGalaTime == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("[VOICE|Christine](A musical videograph is playing.[P] While she looks fairly human, this is actually a robot who sings and dances.[P] Is there nothing we don't do better than organics?)") ]])
        fnCutsceneBlocker()
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("[VOICE|Christine](Nothing is playing at the moment.)") ]])
        fnCutsceneBlocker()
	end

-- |[Watching Videographs]|
--Movies! On a date!
elseif(sObjectName == "VideographL" or sObjectName == "VideographR") then

	--Variables.
	local iIsOnDate            = VM_GetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N")
	local iExplainedVideograph = VM_GetVar("Root/Variables/Chapter5/Sophie/iExplainedVideograph", "N")
    local iIsGalaTime          = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N")
    local iStartedShoppingSequence = VM_GetVar("Root/Variables/Chapter5/Scenes/iStartedShoppingSequence", "N")
	
	--Convenience Flag.
	gsTemporaryStore = sObjectName

    --Gala time.
    if(iIsGalaTime > 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("[VOICE|Christine](We don't have time to watch a videograph at the moment.)") ]])
        fnCutsceneBlocker()

	--Not on a date. Examine the stuff.
	elseif(iIsOnDate == 0.0 or iStartedShoppingSequence == 1.0) then
		
		--Christine doesn't know what videographs are.
		if(iExplainedBlueSphere == 0.0) then
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ Append("[VOICE|Christine](This computer is hooked up to the display here.[P] Must be used for display videographs.)") ]])
			fnCutsceneBlocker()
		
		--Christine knows what videographs are.
		else
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ Append("[VOICE|Christine](This computer is hooked up the display here.[P] Maybe I should watch a videograph with Sophie later?)") ]])
			fnCutsceneBlocker()
		end

	--Needs to explain maintenance.
	elseif(iIsOnDate == 1.0 and iExplainedVideograph == 0.0) then
		VM_SetVar("Root/Variables/Chapter5/Sophie/iExplainedVideograph", "N", 1.0)
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Looks like this viewing room is open.[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Did you want to watch a videograph with me?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] What sort of videograph?[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] ...[P] Heh.[P] Obviously, a documentary or instructional videograph.[P] Of course.[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Now, if you know what commands to enter, there are other ones on the system.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Hmmmm?[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] If you take my meaning, 771852.[P] Not that I wouldn't classify them as 'instructional', of course.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Oh, I get what you're saying.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Does the central administration support such videographs?[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Central doesn't know about them.[P] And, I suspect any Lord Golem who finds out about them wouldn't want them removed.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] All right.[P] Shall we spend an evening watching some videographs?[BLOCK]") ]])

		--Decision script is this script. It must be surrounded by quotes.
		local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
		fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
		fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"VideoChoose\") ")
		fnCutscene(" WD_SetProperty(\"Add Decision\", \"No Thanks\",  " .. sDecisionScript .. ", \"NoClose\") ")
		fnCutsceneBlocker()

	--Date option.
	elseif(iIsOnDate == 1.0 and iExplainedVideograph == 1.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Shall we spend an evening watching some videographs?[BLOCK]") ]])

		--Decision script is this script. It must be surrounded by quotes.
		local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
		fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
		fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"VideoChoose\") ")
		fnCutscene(" WD_SetProperty(\"Add Decision\", \"No Thanks\",  " .. sDecisionScript .. ", \"NoClose\") ")
		fnCutsceneBlocker()
	end
	
--Spend a date watching a videograph.
elseif(sObjectName == "VideoChoose") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Which videograph should we watch?[B][C]") ]])
	fnCutscene([[ Append("Sophie:[E|Neutral] I'll let you pick.[P] There's a lot of good ones.[B][C]") ]])
	fnCutscene([[ Append("Sophie:[E|Neutral] Here.[P] Now you can unlock some of the -[P] hidden videographs.[B][C]") ]])
	fnCutscene([[ Append("Sophie:[E|Neutral] Hee hee![B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] All right...[BLOCK]") ]])

	--Decision script is this script. It must be surrounded by quotes.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"Roberta and Joliet\", " .. sDecisionScript .. ", \"VideoRoberta\") ")
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"A Talking Dark Matter Girl?!?\", " .. sDecisionScript .. ", \"VideoDMG\") ")
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"Slime Girls Are Easy\", " .. sDecisionScript .. ", \"VideoSlime\") ")
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"Dr. Strangelover\", " .. sDecisionScript .. ", \"VideoBomb\") ")
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"Forget It\",  " .. sDecisionScript .. ", \"NoClose\") ")
	fnCutsceneBlocker()

--All videgraphs use this for routing.
elseif(sObjectName == "VideoRoberta" or sObjectName == "VideoDMG" or sObjectName == "VideoSlime" or sObjectName == "VideoBomb") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Left screen.
	if(gsTemporaryStore == "VideographL") then
		fnCutscene([[ AL_SetProperty("Open Door", "DoorA") ]])
		fnCutsceneMove("Christine", 4.25, 8.50)
		fnCutsceneMove("Sophie",    4.25, 8.50)
		fnCutsceneMove("Christine", 4.25, 5.50)
		fnCutsceneMove("Sophie",    4.25, 5.50)
		fnCutsceneMove("Christine", 3.75, 5.50)
		fnCutsceneMove("Sophie",    4.75, 5.50)
		fnCutsceneFace("Christine", 0, -1)
		fnCutsceneFace("Sophie",    0, -1)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
	--Right screen.
	else
		fnCutscene([[ AL_SetProperty("Open Door", "DoorC") ]])
		fnCutsceneMove("Christine", 12.25, 8.50)
		fnCutsceneMove("Sophie",    12.25, 8.50)
		fnCutsceneMove("Christine", 12.25, 5.50)
		fnCutsceneMove("Sophie",    12.25, 5.50)
		fnCutsceneMove("Christine", 11.75, 5.50)
		fnCutsceneMove("Sophie",    12.75, 5.50)
		fnCutsceneFace("Christine", 0, -1)
		fnCutsceneFace("Sophie",    0, -1)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
	end

	--Fade.
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(85)
	fnCutsceneBlocker()

	--Execute the conversation routing case.
	LM_ExecuteScript(gsRoot .. "Chapter 5/Scenes/500 Normal/Sophie Conversation/Z Video Routing.lua", sObjectName)
	fnCutsceneBlocker()

	--Fade.
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
	fnCutsceneWait(85)
	fnCutsceneBlocker()
	
	--Variables.
	local iSophieFirstDateState = VM_GetVar("Root/Variables/Chapter5/Scenes/iSophieFirstDateState", "N")
	
	--First date.
	if(iSophieFirstDateState == 0.0) then
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSophieFirstDateState", "N", 1.0)
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Ah, videographs.[P] A classic first date.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] F-[P]f-[P]first... date...[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Blush] Uh, uh![P] No, I was just showing you around![P] Hee hee![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|PDU] Matchmaker -[P] shut up, PDU![B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Blush] Matchmaker?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Offended] It's my stupid PDU.[P] It said you were ogling me with your big pretty eyes and - [B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Offended] You keep flicking your hair a bit when you talk.[P] You keep looking at me.[P] I can't take my eyes off you.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Offended] What is your deal?[P] We're going to be working together so you may as well just tell me why you don't like me![B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Sad] Don't like you?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Offended] Yeah![P] Am I ugly, or weird, or something?[P] As a lord golem I order you to tell me![B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] ...[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Do you want to kiss me?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Offended] Of course![B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] *smooch*[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Blush] H-[P]huh?[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Happy] Want another one?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Blush] Yeah![B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] *smooch*[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Blush] My goodness, Sophie, Unit 499323, do you like me?[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Happy] Of course I do![P] I like you a lot![P] How do you not see that?[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Happy] I was holding your hand during the videograph![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Blush] T-[P]that was to balance my gyrostabilizers![B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Offended] Are you serious?[P] Come on![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] Okay that one was silly but - [B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] Do you...[P] want to be my tandem unit?[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Happy] I thought you'd never ask![P] Of course![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] But I'm your lord golem...[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Blush] Yeah, but I don't care what anyone else thinks![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] What does that have to do with it?[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] We're different model variants.[P] Slave and lord, we're not supposed to date.[P] You didn't know?[P] Well, you are a fresh convert so...[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Yes, we'll be looked down on.[P] But I don't care.[P] I want you.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] I want you too![B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Offended] Darn it.[P] Now we can't have weeks of will-they-won't-they sexual tension...[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Sorry.[P] I can pretend not - [B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Offended] Oh no you don't![B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Happy] Phew![P] What a day it has been![P] I really need a recharge![P] Would you like to walk me back to my room?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] Of course.[P] The habitation block is just east of the surface exit, right?[P] Let's go!") ]])
        
        --[=[
		fnCutscene([[ Append("Christine:[E|Neutral] Is that -[P] a good thing?[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] I love a good videograph![P] We should do this again![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] (Phew!)[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] So, what would you like to do now?[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] I hate to cut this short, but it's late and I had quite a morning.[P] I should really go to my quarters.[P] As should you![P] Don't stress your motivators![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Ummm...[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Hee hee![P] You haven't even seen your quarters yet, have you?[P] Do you know where they are?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Vaguely.[P] I didn't download the directory when I got here.[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Well then allow me to show you.[P] We need to take the elevators east of the airlock.[P] I'll point them out when we get close.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Thanks, Sophie.[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] This does count as walking a lady back to her room.[P] We use the same elevators, of course.[P] All units in the sector use the same domicile block.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] All right.[P] Let's go.") ]])]=]
		fnCutsceneBlocker()
	
	--After the cutscene is over, Sophie will request to go back to the maintenance bay.
	else
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Well, I suppose it's back to the maintenance bay for me.[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] I had a lot of fun today, Christine![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] No matter what, it's fun if you're there.[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Would you care to escort your tandem unit?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] With pleasure.") ]])
		fnCutsceneBlocker()
	end
	
	--Variables.
	VM_SetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N", 2.0)
	
	--Left screen, merge.
	if(gsTemporaryStore == "VideographL") then
		fnCutsceneMove("Christine", 4.25, 5.50)
		fnCutsceneMove("Sophie",    4.25, 5.50)
		fnCutsceneFace("Christine", 0, 1)
		fnCutsceneFace("Sophie",    0, 1)
		fnCutsceneBlocker()
		fnCutscene([[ AL_SetProperty("Fold Party") ]])
		fnCutsceneBlocker()
		
	--Right screen, edge.
	else
		fnCutsceneMove("Christine", 12.25, 5.50)
		fnCutsceneMove("Sophie",    12.25, 5.50)
		fnCutsceneFace("Christine", 0, 1)
		fnCutsceneFace("Sophie",    0, 1)
		fnCutsceneBlocker()
		fnCutscene([[ AL_SetProperty("Fold Party") ]])
		fnCutsceneBlocker()
	
	end

-- |[Multi-Purpose Close]|
elseif(sObjectName == "NoClose") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Clean up temporary flags.
	gsTemporaryStore = nil

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
