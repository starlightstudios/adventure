-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Sophie Leader]|
--During the secrebot sequence, Sophie is in the party lead and uses a different file.
local b254SophieLeading = fnIsSophieLeader()
if(b254SophieLeading == true) then
    LM_ExecuteScript(fnResolvePath() .. "DialogueSophie.lua", sTopicName)
    return
end

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
    
    -- |[GolemEA]|
    if(sActorName == "GolemEA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] When watching videographs, I find my Slave Units simply can't understand the subtleties.[P] They rarely seem to care about character development and merely want explosions to be on screen.[P] Typical.") ]])
        fnCutsceneBlocker()
        
    -- |[GolemEB]|
    elseif(sActorName == "GolemEB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] My Lord Golem keeps dragging me in here to see these bombastic actiongraphs.[P] I just keep telling her the explosions are nice, while I've been composing some poetry with my spare CPU time.[P] She won't even look at what I've written...[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] You should publish it to the station archives.[P] Just, use an alternate login, so your Lord doesn't know it was you.[B][C]") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] ...[P] That is a very good idea.[P] Thank you, Lord Golem.[P] It seems not all of you are -[P] well, best not say it.") ]])
        fnCutsceneBlocker()

    -- |[GolemEC]|
    elseif(sActorName == "GolemEC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] My function is to produce videographs.[P] I have a special processor unit to upload them to the screen directly from my CPU, but my Lord Golem gets all the credit for my ideas...") ]])
        fnCutsceneBlocker()
        
    -- |[GolemED]|
    elseif(sActorName == "GolemED") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] We are currently using this screening room, Lord Golem.[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] Are you tandem units?[B][C]") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Yes, Lord Golem.[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] Enjoy your date![P] I won't keep you![B][C]") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] It's not a date, we're working very hard![B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] Enjoy your date, that is a direct order.[B][C]") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Er -[P] Oh![P] Right away, Lord Golem![P] We will enjoy our date to its fullest!") ]])
        fnCutsceneBlocker()
        
    -- |[GolemEE]|
    elseif(sActorName == "GolemEE") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Videographs are such a great way to spend a few hours. I can just let my CPU rest and not think for a while...") ]])
        fnCutsceneBlocker()
        
    -- |[GolemEF]|
    elseif(sActorName == "GolemEF") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Apologies, Lord Golem, but this viewing room is under repair.[P] Since the fabricators are backed up, spare parts are in short supply.[P] Please use one of the other rooms.") ]])
        fnCutsceneBlocker()
    end
end
