-- |[Root]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    -- |[GolemGalaA]|
    if(sActorName == "GolemGalaA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] We were just going to watch a videograph for the evening, but now we can't agree on which one!") ]])
        
    -- |[GolemGalaB]|
    elseif(sActorName == "GolemGalaB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] I wanna watch that bootleg Professor Killsalot videograph![P] The one with Unit 771002!") ]])
        
    -- |[GolemGalaC]|
    elseif(sActorName == "GolemGalaC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Can we watch 'Prejudice and Prejudice and more Prejudice'?[P] I want to see all the Lord Units backstabbing each other!") ]])
    
    -- |[Night]|
    elseif(sActorName == "Night") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Night", "Secrebot") ]])
        fnCutscene([[ Append("Night:[E|Secrebot] So this is it, huh.[P] The big one.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Dawn of a new day.[B][C]") ]])
        fnCutscene([[ Append("Night:[E|Secrebot] I don't know if I'm ready.[P] I'm so nervous.[P] All I do is clean things, you know.[B][C]") ]])
        fnCutscene([[ Append("Night:[E|Secrebot] I can help manufacture things and -[P] I'm just worrying.[P] You don't need me to be worrying.[B][C]") ]])
        fnCutscene([[ Append("Night:[E|Secrebot] If I don't see you again...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] You will.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] It will be fine.[B][C]") ]])
        fnCutscene([[ Append("Night:[E|Secrebot] Yeah, of course.[P] The city will need someone to clean it after all this is over.[P] Enjoy the gala, the secrebots are with you all the way.") ]])
        fnCutsceneBlocker()
    end
end
