-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    --Set facing.
    local sActorName = TA_GetProperty("Name")
    TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[Tiffany]|
    if(sActorName == "Tiffany") then
    
        --Variables.
        local iStartedShoppingSequence = VM_GetVar("Root/Variables/Chapter5/Scenes/iStartedShoppingSequence", "N")
        
        --Normal:
        if(iStartedShoppingSequence < 4.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
            fnCutscene([[ Append("55:[E|Neutral] You're reading the last page first?[P] Shame on you![B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Go over there and let the cutscene play out as it's supposed to.") ]])
        
        --Shopping completed.
        elseif(iStartedShoppingSequence == 4.0) then
        
            --Flag.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iStartedShoppingSequence", "N", 5.0)
            VM_SetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N", 0.0)
            VM_SetVar("Root/Variables/Chapter5/Sophie/iSophieWentToQuarters", "N", 0.0)
            VM_SetVar("Root/Variables/Chapter5/Scenes/iMainQuestBlock", "N", 24.0)
        
            --Dialogue.
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
            fnCutscene([[ Append("55:[E|Neutral] Good.[P] I assume you've completed your dalliances.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Nice to see you too, 55.[P] I should have gotten you a cup of oil.[P] Maybe that'd brighten your mood.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] We really do need to talk about...[P] you.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] There is nothing to talk about.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] I am a machine, and machines do not care about your feelings.[P] We will proceed regardless.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] (She's backsliding...)[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Fine.[P] Then be a good robot and tell me why you called me here.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] I have performed my initial probings of the situation surrounding the upcoming gala.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] However, our preparatory work will take some time.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Just how many explosives do you need?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Smug] My existing supplies are beyond adequate.[P][EMOTION|Tiffany|Neutral] However, the civilian populace of Regulus City is in disarray.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] I have become a member of many of the small resistance cells that exist within the city.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] They...[P] already exist?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Yes.[P] There are hundreds, mostly existing of small groups of units.[P] Two or three per group, disgruntled and usually in direct verbal contact, often working in the same department.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Some of the larger groups attempt to coordinate the smaller groups via the network.[P] Arrests tend to follow when the groups are compromised.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] In addition to poor encryption protocols, there are infiltrators and agents provocateur from the security forces.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] I have done what I can to improve their communications.[P] Arrests are down by sixty percent.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] And I assume you want to arm and train these groups?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Correct.[P] If the Sunrise Gala is to be our focal point, they must be ready.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] So let's get to it, then![B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Your enthusiasm is most welcome, considering your frivolities.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Welcome sounds like there's an emotion behind it, perfect machine.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Smug] Noted.[P] I will reconfigure my linguistic routines to be as trite as possible, since you seem to impart sentience upon them.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] *Sigh*[B][C]") ]])
            
            --Variables.
            local iSawEquinoxMessage = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawEquinoxMessage", "N")
            local iCompletedEquinox  = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedEquinox", "N")
            local iCompletedSerenity = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedSerenity", "N")
            local iSXUpgradeQuest    = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
            local iFinished198       = VM_GetVar("Root/Variables/Chapter5/Scenes/iFinished198", "N")
            
            --If any of these are true:
            if(iSXUpgradeQuest < 3.0 or (iSawEquinoxMessage == 1.0 and iCompletedEquinox == 0.0) or iCompletedSerenity == 0.0 or iFinished198 == 0.0) then
                fnCutscene([[ Append("55:[E|Neutral] There are still other tasks we could undertake, as well.[B][C]") ]])
                
                --Handler:
                if(iSXUpgradeQuest < 3.0) then
                    fnCutscene([[ Append("55:[E|Neutral] We may attempt to seek an alliance with the Steam Droids.[P] Their firepower and organization would serve our cause well.[B][C]") ]])
                end
                if(iSawEquinoxMessage == 1.0 and iCompletedEquinox == 0.0) then
                    fnCutscene([[ Append("55:[E|Neutral] Investigating the Equinox Laboratories may yield equipment or valuable information.[B][C]") ]])
                end
                if(iCompletedSerenity == 0.0) then
                    fnCutscene([[ Append("55:[E|Neutral] The Serenity Crater Observatory would serve as an excellent strong point.[P] An alliance with them is desirable.[B][C]") ]])
                end
                if(iFinished198 == 0.0) then
                    fnCutscene([[ Append("55:[E|Neutral] The unresolved situation in Sector 198 may be turned to our advantage if handled carefully.[B][C]") ]])
                end
                fnCutscene([[ Append("55:[E|Neutral] Further, acquiring equipment for our own purposes will prove useful.[P] We should not discount that in favour of strategic objectives.[B][C]") ]])
            
            else
                fnCutscene([[ Append("55:[E|Neutral] While we have done an admirable job of obtaining allies, equipment, and information, there may yet be advantages to be had we do not know of.[B][C]") ]])
            
            end
        
            fnCutscene([[ Append("55:[E|Neutral] Therefore, I am leaving it to your discretion.[P] I will continue to assist you until you give the word.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Once you are satisfied with our preparations, I will be requiring all of your time in our task of arming the resistance groups.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] (If I were playing a video game, I'd take that as a warning -[P] no going back once we start.)[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] (Unfortunately, this isn't a video game.[P][EMOTION|Christine|Laugh] Otherwise I'd be sure to make a new save!)[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] I get you, 55.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Good.[P] I will remain here.[P] If you exit the city, I will accompany you as usual.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Speak to me here whenever you would like to begin the next phase.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Will I even have time for dates with Sophie?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] You will be maintaining your cover as a Lord Golem, however, I will be requiring your non-work time.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] I get you.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] There is one other thing.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] It is about my sister.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] Unit 2856...[P] I hope she's okay...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] Do you maybe want to find her?[P] Reunite with her?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] No.[P] I have already attempted to locate her, but was unable to.[P] She is 'off the grid', so to speak.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] If you come into contact with her, she is to be terminated.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Scared] !!![B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] She is the only witness who knows we were in the LRT facility.[P] Other than Project Vivify, that is.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] The Drone Units stationed there lack the long-term memory capabilities to remember us, and I wiped their memory backup banks when I had core access.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Scared] But she's your sister![B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] She is also the unit most capable of destroying me, and you.[P] She knows who you are, what you are, where you work...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] To protect yourself, our goals, and your tandem unit, she must be eliminated.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] ...[P] Affirmative.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Now, move out.[P] Speak to me when you are ready to begin the next phase of our preparations.") ]])
            VM_SetVar("Root/Variables/Chapter5/WorldState/iNoWarpsAllowed", "N", 0.0)
        
        --Asks about starting the next sequence.
        elseif(iStartedShoppingSequence == 5.0) then
        
            --Dialogue.
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
            fnCutscene([[ Append("55:[E|Neutral] Are you prepared to begin the next phase of the plan?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] This is your final opportunity to acquire arms and allies before we begin.[BLOCK]") ]])

            --Decision script is this script. It must be surrounded by quotes.
            local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
            fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Let's Go\", " .. sDecisionScript .. ", \"Yes\") ")
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Ideas\",    " .. sDecisionScript .. ", \"Ideas\") ")
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Not Yet\",  " .. sDecisionScript .. ", \"No\") ")
            fnCutsceneBlocker()
        end
    end
    
-- |[ ================================== Responses to Tiffany ================================== ]|
elseif(sTopicName == "Yes") then

	WD_SetProperty("Hide")
    fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
    fnCutscene([[ Append("55:[E|Neutral] Good.[P] Let us proceed.") ]])
    fnCutsceneBlocker()
    VM_SetVar("Root/Variables/Chapter5/WorldState/iNoWarpsAllowed", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iMainQuestBlock", "N", 25.0)
    
    fnCutsceneWait(45)
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    fnCutsceneWait(165)
    fnCutsceneBlocker()
    
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Narrator: And so, for the next two months, Christine and 55 sought out new allies.[B][C]") ]])
    fnCutscene([[ Append("Narrator: They provided what they could to the aspiring rebels. Arms, training, software, whatever they could spare.[B][C]") ]])
    fnCutscene([[ Append("Narrator: The gala drew near, and 2855 called a meeting to discuss their plans for the big night...") ]])
    fnCutsceneBlocker()

    --Next scene.
	fnCutscene([[ AL_BeginTransitionTo("RegulusCityX", "FORCEPOS:9.5x17.0x0") ]])
	fnCutsceneBlocker()

--Lists off possible sidequests.
elseif(sTopicName == "Ideas") then
	WD_SetProperty("Hide")
    fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Do you have any leads on getting more support?[B][C]") ]])
            
    --Variables.
    local iSawEquinoxMessage = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawEquinoxMessage", "N")
    local iCompletedEquinox  = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedEquinox", "N")
    local iCompletedSerenity = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedSerenity", "N")
    local iSXUpgradeQuest    = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
    local iFinished198       = VM_GetVar("Root/Variables/Chapter5/Scenes/iFinished198", "N")
    
    --If any of these are true:
    if(iSXUpgradeQuest < 3.0 or (iSawEquinoxMessage == 1.0 and iCompletedEquinox == 0.0) or iCompletedSerenity == 0.0 or iFinished198 == 0.0) then
        fnCutscene([[ Append("55:[E|Neutral] I believe so.[B][C]") ]])
        
        --Handler:
        if(iSXUpgradeQuest < 3.0) then
            fnCutscene([[ Append("55:[E|Neutral] We may attempt to seek an alliance with the Steam Droids.[P] Their firepower and organization would serve our cause well.[B][C]") ]])
        end
        if(iSawEquinoxMessage == 1.0 and iCompletedEquinox == 0.0) then
            fnCutscene([[ Append("55:[E|Neutral] Investigating the Equinox Laboratories may yield equipment or valuable information.[B][C]") ]])
        end
        if(iCompletedSerenity == 0.0) then
            fnCutscene([[ Append("55:[E|Neutral] The Serenity Crater Observatory would serve as an excellent strong point.[P] An alliance with them is desirable.[B][C]") ]])
        end
        if(iFinished198 == 0.0) then
            fnCutscene([[ Append("55:[E|Neutral] The unresolved situation in Sector 198 may be turned to our advantage if handled carefully.[B][C]") ]])
        end
        fnCutscene([[ Append("55:[E|Neutral] Further, acquiring equipment for our own purposes will prove useful.[P] We should not discount that in favour of strategic objectives.[B][C]") ]])
    
    else
        fnCutscene([[ Append("55:[E|Neutral] While we have done an admirable job of obtaining allies, equipment, and information, there may yet be advantages to be had we do not know of.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Better weaponry, armor, gems, or skills can never hurt.[P] As far as obtaining civilian support, I have no more leads.[B][C]") ]])
    end
    fnCutscene([[ Append("Christine:[E|Neutral] Gotcha.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] I leave the tactical decision to you.[P] Until we make our move, I will prepare as much as I can.") ]])

--Not quite yet.
elseif(sTopicName == "No") then
	WD_SetProperty("Hide")
    fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Not just yet, 55.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] If there was something else you needed to discuss, speak with me at a rest point in the field.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Affirmative.[P] Moving out.") ]])
end
