-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exit Ladder]|
if(sObjectName == "Ladder") then
	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("RegulusCityC", "FORCEPOS:11.0x14.0x0")

--Terminal with backers information.
elseif(sObjectName == "Terminal") then

	fnStandardMajorDialogue()
	fnCutscene([[ Append("Christine:[E|Neutral] Ah, this terminal has a list of all the commendations that have been awarded for the past few months.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] If it weren't for them, this city wouldn't be the way it is today.[P] We owe them quite a debt![B][C]") ]])
	if(fnIsCharacterPresent("Tiffany")) then
		fnCutscene([[ Append("55:[E|Neutral] Are you talking to yourself?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] I was talking to you.[P] Weren't you listening?[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] Ah.[P] It seemed you were talking to someone who isn't here but is still somehow hearing us speak.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Don't be silly.[B][C]") ]])
	end
		
	fnCutscene([[ WD_SetProperty("Activate Topics After Dialogue", "Backers5", "Leave") ]])
	
--Terminal with backers information.
elseif(sObjectName == "Registry") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (This terminal is a network backup terminal.[P] Let's see...)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Unit 771852 Assignment Request Log', interesting.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Two assignment requests logged.[P] First one, priority zero request with no sender specified.[P] There's an authcode of some sort on it but I don't know what it means.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Any available Maintenance and Repair position in the city, ideally with EVA access.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Second one.[P] 'Maintenance and Repair, Sector 96. Our department has been without a lord golem and efficiency would increase greatly if Unit 771852 were assigned here. Maximum priority, please! Thank you!'.[P] There's no authcode on that one...)") ]])
    fnCutsceneBlocker()

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
