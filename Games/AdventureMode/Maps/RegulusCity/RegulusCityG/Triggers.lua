-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
--if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
--55 Meeting Scene.
if(sObjectName == "Meet55") then

	--Variables.
    local iIsOnDate                = VM_GetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N")
	local iSawSpecialAnnouncement  = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawSpecialAnnouncement", "N")
    local iStartedShoppingSequence = VM_GetVar("Root/Variables/Chapter5/Scenes/iStartedShoppingSequence", "N")
    local iSawBasementScene        = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawBasementScene", "N")
    
    --Sequence does not play at all if Sophie is present.
    if(iIsOnDate == 1.0) then
        return
    end
	
	--If the value is 2.0, the scene is primed but hasn't played.
	if(iSawSpecialAnnouncement == 2.0) then
		
		--Spawn Sophie.
		TA_Create("Sophie")
			TA_SetProperty("Position", -100, -100)
			TA_SetProperty("Clipping Flag", true)
			fnSetCharacterGraphics("Root/Images/Sprites/Sophie/", true)
			TA_SetProperty("Facing", gci_Face_South)
		DL_PopActiveObject()
		
		--Variables.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iMet55InBasement", "N", 3.0)
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSawSpecialAnnouncement", "N", 3.0)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iMainQuestBlock", "N", 14.5)
		
		--Music fades out.
		AL_SetProperty("Music", "Null")
		
		--Move Christine.
		fnCutsceneMove("Christine", 14.25, 7.50)
		fnCutsceneBlocker()
	
		--Wait a bit.
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Christine looks around.
		fnCutsceneFace("Christine", -1, -1)
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		fnCutsceneFace("Christine", 0, -1)
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		fnCutsceneFace("Christine", 1, -1)
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] There's nobody here...[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Maybe the work terminal had a glitch?[B][C]") ]])
		fnCutscene([[ Append("2855:[VOICE|Tiffany] It was no glitch.") ]])
		fnCutsceneBlocker()
		
		--Wait a bit.
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Move 55 in.
		fnCutsceneMove("Tiffany", 17.25, 11.50)
		fnCutsceneBlocker()
		
		--Wait a bit.
		fnCutsceneWait(25)
		fnCutsceneBlocker()

		--55 Walks up. Christine turns around.
		fnCutsceneMove("Tiffany", 14.25, 8.50)
		fnCutsceneFace("Christine", 0, 1)
		fnCutsceneBlocker()
		fnCutsceneFace("Tiffany", 0, -1)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] Command Unit 2855![P] I'm glad you're all right![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] ...[P][E|Sad] Do I know you?[P] Something's wrong with my memory drives...[B][C]") ]])
		fnCutscene([[ Append("2855:[E|Neutral] Of course, your drives are out of phase.[P] Let me recalibrate them for you.") ]])
		fnCutsceneBlocker()
		
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Christine backs off.
		fnCutsceneMoveFace("Christine", 14.25, 6.50, 0, 1, 0.50)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
		fnCutscene([[ Append("Christine:[E|Sad] Something's wrong...[B][C]") ]])
		fnCutscene([[ Append("2855:[E|Upset] Stop mewling and come here.[P] We don't have time for this.") ]])
		fnCutsceneBlocker()
		
		--Advance.
		fnCutsceneMove("Tiffany", 14.25, 7.50)
		fnCutsceneBlocker()
		
		--Christine moves away again.
		fnCutsceneMoveFace("Christine", 15.25, 6.50, -1, 1, 2.50)
		fnCutsceneBlocker()
		fnCutsceneMove("Tiffany", 14.25, 6.50)
		fnCutsceneMoveFace("Christine", 16.25, 6.50, -1, 1, 2.50)
		fnCutsceneBlocker()
		fnCutsceneFace("Tiffany", 1, 0)
		fnCutsceneBlocker()
		fnCutsceneFace("Christine", -1, 0)
		fnCutsceneMove("Tiffany", 15.25, 6.50)
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
		fnCutscene([[ Append("Christine:[E|Offended] Stay away from me![B][C]") ]])
		fnCutscene([[ Append("2855:[E|Neutral] Cease your struggles, this will be over in just a moment.[B][C]") ]])
		fnCutscene([[ Append("[SOUND|World|SparksA]*ZAP*[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Scared] Aahhhhhh!!!") ]])
		fnCutsceneBlocker()
		
		--Christine spazzes out.
		fnCutsceneMoveFace("Christine", 16.25, 8.50,  1,  1, 2.00)
		fnCutsceneMoveFace("Christine", 16.25, 9.50,  1, -1, 2.00)
		fnCutsceneMoveFace("Christine", 16.25, 7.50, -1,  0, 2.00)
		fnCutsceneMoveFace("Christine", 16.25, 9.50,  1,  1, 2.00)
		fnCutsceneMoveFace("Christine", 16.25, 8.50,  0,  1, 2.00)
		fnCutsceneMoveFace("Christine", 15.25, 8.50,  1, -1, 2.00)
		fnCutsceneMoveFace("Christine", 17.25, 8.50,  1,  1, 2.00)
		fnCutsceneMoveFace("Christine", 15.25, 8.50,  0,  0, 2.00)
		fnCutsceneMoveFace("Christine", 17.25, 8.50,  1,  1, 2.00)
		fnCutsceneMoveFace("Christine", 17.25, 8.20,  0,  1, 2.00)
		fnCutsceneBlocker()
		fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Christine")
			ActorEvent_SetProperty("Special Frame", "Wounded")
		DL_PopActiveObject()
		fnCutsceneWait(65)
		fnCutsceneBlocker()
		fnCutsceneMove("Tiffany", 16.25, 8.50)
		fnCutsceneBlocker()
		fnCutsceneFace("Tiffany", 1, 0)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
		fnCutscene([[ Append("2855:[E|Neutral] Perhaps the voltage was too high?[P] No matter, your chassis could absorb that impact.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Cry] Get...[P] away from...[P] me...[B][C]") ]])
		fnCutscene([[ Append("2855:[E|Neutral] Stop complaining and let your drives format correctly.") ]])
		fnCutsceneBlocker()
		
		--Christine stands up.
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Christine")
			ActorEvent_SetProperty("Special Frame", "Crouch")
		DL_PopActiveObject()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Christine")
			ActorEvent_SetProperty("Special Frame", "Null")
		DL_PopActiveObject()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		fnCutsceneMove("Christine", 17.25, 8.50, 0.20)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		fnCutsceneFace("Christine", -1, 0)
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] I remember...[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Cryogenics...[P] the airlock...[P] the bodies...[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Offended] You -[P] you stuck me with a golem core![P] You did this to me![B][C]") ]])
		fnCutscene([[ Append("2855:[E|Upset] It was the only way to proceed.[P] I needed your authenticator chip to access the Regulus City airlock.[B][C]") ]])
		fnCutscene([[ Append("2855:[E|Upset] Your permission was not necessary.[P] Besides, you can - [P][CLEAR]") ]])
		fnCutscene([[ Append("Christine:[E|Happy] Thank you so much, 2855![B][C]") ]])
		fnCutscene([[ Append("*Hug*[B][C]") ]])
		fnCutscene([[ Append("2855:[E|Offended] ...[P] What are you doing?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Happy] I have you to thank for this![P] I've never been so happy in my life![B][C]") ]])
		fnCutscene([[ Append("2855:[E|Neutral] Truly?[P] You prefer being a golem?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Happy] Were it not for you, I would never have come to Regulus City.[P] I would never have gone to get scanned...[B][C]") ]])
		fnCutscene([[ Append("2855:[E|Neutral] (Oh dear...)[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] I didn't tell you what my life was like before we met, did I?[B][C]") ]])
		fnCutscene([[ Append("2855:[E|Upset] I didn't ask, and I don't care.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] You see, I've always felt -[P] I suppose the word would be uncomfortable.[P] I was never happy with who I was.[B][C]") ]])
		fnCutscene([[ Append("2855:[E|Neutral] We have business to attend to.[P] Can this wait?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] But now...[P] I have a place I belong.[P] Oh![P][E|Happy] Sophie![P] I must introduce you to Unit 499323![P] She'll be so excited to meet you![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] You made all of this possible.[P] From the bottom of my power core, thank you, 2855...[B][C]") ]])
		fnCutscene([[ Append("2855:[E|Neutral] Yes yes yes.[P] I've already fabricated a new authenticator chip.[P] You can transform back to a human now.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] .............................[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Sad] Assessing likelihood of deliberately false statements.[P] Likelihood is low.[P] Very low.[B][C]") ]])
		fnCutscene([[ Append("2855:[E|Neutral] I was intrigued by that runestone you were carrying.[P] I did some database searches on it.[P] Do you know what I found?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Sad] No no no no...[B][C]") ]])
		fnCutscene([[ Append("2855:[E|Smirk] It's a Rilmani artifact, and if the surface scans are accurate, it's a seventh-dimensional object.[P] Witness reports from the surface also suggest it allows the user to change their form.[B][C]") ]])
		fnCutscene([[ Append("2855:[E|Smug] But you already knew that, didn't you?[B][C]") ]])
		fnCutscene([[ Append("2855:[E|Neutral] I discovered numerous reports of others bearing runes similar to yours.[B][C]") ]])
		fnCutscene([[ Append("2855:[E|Neutral] These were highly classified reports, mind.[P] Not everyone knows this.[B][C]") ]])
		fnCutscene([[ Append("2855:[E|Neutral] Now stop wasting time and transform.[BLOCK]") ]])
		
		--Decision script is this script. It must be surrounded by quotes.
		local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
		fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
		fnCutscene(" WD_SetProperty(\"Add Decision\", \"Go Along With Her\", " .. sDecisionScript .. ", \"OptionA\") ")
		fnCutscene(" WD_SetProperty(\"Add Decision\", \"Refuse\",  " .. sDecisionScript .. ", \"OptionB\") ")
		fnCutsceneBlocker()
    
    --Basement cutscene leading to the shopping sequence.
    elseif(iStartedShoppingSequence == 1.0 and iSawBasementScene == 0.0) then    
    
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSawBasementScene", "N", 1.0)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iMainQuestBlock", "N", 22.0)
        
        --Variables.
        local iSXUpgradeQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iSXUpgradeQuest", "N")
        
        --Black out the screen.
        fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
        fnCutsceneBlocker()
        
        --Position characters.
		TA_Create("Sophie")
			TA_SetProperty("Position", -100, -100)
			TA_SetProperty("Clipping Flag", false)
			fnSetCharacterGraphics("Root/Images/Sprites/Sophie/", true)
			TA_SetProperty("Facing", gci_Face_North)
			TA_SetProperty("Wipe Special Frames")
		DL_PopActiveObject()
        fnSpecialCharacter("Tiffany", 13, 6, gci_Face_North, false, nil)
        
        --Position Christine.
        fnCutsceneTeleport("Christine", 17.25, 16.50)
        fnCutsceneFace("Christine", 0, 1)
        fnCutsceneBlocker()
        fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Move Christine spawn Sophie behind her.
        fnCutsceneMove("Christine", 16.25, 16.50)
        fnCutsceneBlocker()
        fnCutsceneFace("Christine", 1, 0)
        fnCutsceneWait(5)
        fnCutsceneBlocker()
        fnCutsceneTeleport("Sophie", 17.25, 16.50)
        fnCutsceneFace("Sophie", 0, 1)
        fnCutsceneWait(15)
        fnCutsceneBlocker()
        fnCutsceneFace("Sophie", -1, 0)
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Move.
        fnCutsceneMove("Christine", 13.25, 16.50)
        fnCutsceneMove("Christine", 13.25, 8.50)
        fnCutsceneMove("Sophie", 13.25, 16.50)
        fnCutsceneMove("Sophie", 13.25, 9.50)
        fnCutsceneMove("Sophie", 14.25, 9.50)
        fnCutsceneMove("Sophie", 14.25, 8.50)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneFace("Tiffany", 0, 1)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
        fnCutscene([[ Append("55:[E|Neutral] And just how long did you intend to make me wait?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Laugh] Oh hush, you can find ways to entertain yourself![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Speaking of, how have you been lately?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Upset] My time has value which you cannot appreciate.[P] Do not waste it carelessly.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] So as good as usual then.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Well, we have some good news for you.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Happy] Christine is going to the Sunrise Gala![B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Happy] Oh I bet it's because of all those efficiency gains we've been making -[P] most first-year Lord Units don't get invites, you know![B][C]") ]])
        fnCutscene([[ Append("55:[E|Upset] Hm.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] (Uh oh.[P] Hope I didn't tick her off.)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] 55, come on.[P] We're just trying to cheer you up.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Upset] It is to be expected that you did not realize the momentous nature of what you just told me.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] Errr...[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Happy] Are you going, too?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Sad] No, wait.[P] That'd be dumb...[B][C]") ]])
        if(iSXUpgradeQuest >= 3.0) then
            fnCutscene([[ Append("55:[E|Neutral] Do not denigrate yourself, Unit 499323.[P] Your skillset is admirable.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] I did not have time to thank you for your commendable performance with SX-399.[P] I would like to do that now.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] You're welcome![P] Give her my best the next time you see her![B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Please know that any and all scorn is exclusively reserved for Unit 771852.[P] Any directed at you is accidental.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] (The hay?)[B][C]") ]])
        end
        fnCutscene([[ Append("55:[E|Smirk] There is not, in fact, anything dumb about it, other than Christine's as-per-usual failure to think strategically.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] The needless put-downs are a firm indicator of a positive mood.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] Is that the dynamic?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] I have been given cause to be in good spirits.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] Christine, please use all of your processor cycles for the next question I posit.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] Why would I be happy about a very large collection of high-ranking enemy officials gathering in the same place?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] Wait, what?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] 55, no![P] We want to attend the Sunrise Gala, not blow it up![B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Unfortunate.[P] If that is the case, then I would suggest concluding your night before I set the charges off.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] ...[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Sad] You know, she's right, Christine.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Sad] It's a big opportunity.[P] There will be Lord Units and Command Units there in big numbers.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] A-[P]and security![P] It'll be packed to the gills with security units![B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] Correct.[P] You possess analytical skill after all.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] Fortunately, I have a unit with access codes right in front of me, and I know the time and location of the event.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] I figured you'd find that out with all your hacking...[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] And thus we come to the oddity.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] I have not seen any traffic related to this event on the network.[P] None.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Laugh] Maybe you're just slipping![B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Incorrect.[P] Such an event and its organization would trigger a great deal of planning amongst security forces, which I observe carefully.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Yet, I have seen nothing of unusual volume, not even encrypted communications.[P] This suggests non-networked communication protocols.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Then they know their network is compromised.[P] Consider what we've been up to...[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] I have reached the same conclusion.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Sad] Wait...[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Sad] What if this is a trap for you, Christine?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Sad] What if they know what you did in the LRT Facility?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Angry] She is not to know about our operations, Unit 771852.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] It kinda slipped out...[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] But while her assessment is not impossible, it is also unlikely.[P] At no point in the history of the city has arresting a Unit been anything other than a routine matter.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Were you targeted for interrogation, there would be dozens of security units swarming this area.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Though the possibility of a trap is definite, I would attribute the communications protocols to caution.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] This will be an operation to be undertaken with utmost secrecy and efficiency.[P] The risk is enormous.[P] At the same time, the benefits are obvious.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] Sophie...[P] This could be...[P] it...[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] It?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] The Administration's command structure would be dealt a massive blow by the loss of a large number of key personnel.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Further, the Arcane Academy is a historical bastion of strength.[P] To attack it directly would expose the weakness of the Administration.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] Even if we didn't intend it to be, other Golems might take this as a sign.[P] They might rise up on their own.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] We have to be prepared for that.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] You don't look too happy about it.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] It's going to involve a lot of retired units...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] Even if I know they're part of the problem...[P] I know there will be some innocents among them...[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smug] Hrmpf.[P] I see you have sympathy for the enemy.[P] Where is the firebrand maverick ready to risk her life?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] One of us has to be our conscience.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] Innocent units will be destroyed.[P] Historical artifacts reduced to rubble.[P] The Administration will scapegoat some poor Slave Unit who had nothing to do with it, and they'll suffer.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] I'm prepared to go to war, don't you dare think otherwise.[P] But I know I'm going to be damned for it.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Good.[P] That is all I require from you at the moment.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smug] You have preparations to see to, do you not?[P] Unless you intend to attend the gala in [P]that.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Angry] W-[P]what's that supposed to mean?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Happy] C-[P]Christine![P] I know exactly what you should wear![B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Happy] Let me make it for you![P] Please?[P] Please![B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Contact me when you are ready.[P] I will attempt to pull up schematics and prepare our plan.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Angry] Hey![P] My dress looks great, 55!") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneMove("Tiffany", 14.25, 6.50)
        fnCutsceneBlocker()
        fnCutsceneFace("Tiffany", 0, -1)
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneTeleport("Tiffany", -100.0, -100.0)
        fnCutsceneBlocker()
        fnCutsceneFace("Christine", 1, 0)
        fnCutsceneFace("Sophie", -1, 0)
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
        fnCutscene([[ Append("Christine:[E|Angry] That no good[P] driggle-draggle[P] crooked-nosed[P] fopdoodled[P] muckspouted[P] fusterlug![B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] You might be the world's easiest robot to tease.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Happy] But forget her![P] Let's make you a pretty dress to wear![B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Happy] *Oh maybe something low cut...*[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Low cut?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Blush] S-[P]said it too loud![P] H-h-h-hee hee![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] What about you?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] You have your heart set on this, I see.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] My feelings aren't hurt.[P] Really.[P] This isn't my first time not going to the Sunrise Gala, you know.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Sad] Can you imagine a Slave Unit walking in that door like she didn't care what anyone thought?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] And what if...[P] nobody knew you were a Slave Unit?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Surprised] ..![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Long dress reaching the ground, thick sleeves to cover the arms...[P] maybe a big hat?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Surprised] Do you not see the killphrase bolted to my forehead?[P] Or the headphones?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Oh I bet you look wonderful without your headphones on...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Besides, can't we just remove those?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Surprised] B-[P]but..![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] I think two prominent repair units can come up with a way to get those off, right?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Blush] We'd be breaking every single rule...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Yes, well,[P] I think we're well past that point.[P] Don't you?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Blush] Y-y-y-yeah...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] We can worry about that when the time comes, of course.[P] I'll run a few searches.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] I'm sure some unit somewhere has needed maintenance on their killphrase's hysterisis-switch, right?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Blush] W-[P]well, be that as it may, I still need some materials...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Happy] Then![P] Let's go shopping![B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Happy] Oh my gosh, Sector 119 will be abuzz![P] All the Lord Units will be out buying dresses![B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Happy] And all the shops will have their best on display and everyone will be gossiping and oh my gooooodness![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Happy] I'm sure I can give some reason why we need to...[P] requisition fabric...[P] right?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Happy] And we can get some ChemFuel and just chat about everything and see all the new videograph posters...[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Happy] Do you have anything to do first, or should we get going right now?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] (Do I have any other tasks to take care of before going shopping with Sophie?)[BLOCK]") ]])
		
		--Decision script is this script. It must be surrounded by quotes.
		local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
		fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
		fnCutscene(" WD_SetProperty(\"Add Decision\", \"Let's go shopping!\", " .. sDecisionScript .. ", \"ShoppingTime\") ")
		fnCutscene(" WD_SetProperty(\"Add Decision\", \"Give me a few minutes...\",  " .. sDecisionScript .. ", \"NotThisSecond\") ")
		fnCutsceneBlocker()
    
	end
	
elseif(sObjectName == "OptionA" or sObjectName == "OptionB") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
		
	--Acceptace.
	if(sObjectName == "OptionA") then
		fnCutscene([[ Append("Christine:[E|Neutral] (It's not worth the effort to argue with her...)[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Sad] (Wait...)[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Sad] (I've said that before.)[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Offended] (No.[P] No!)[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Offended] (Who does she think she is?[P] She can't just barge in here and tell me what to do!)[B][C]") ]])
	end
		
	--Common.
	fnCutscene([[ Append("Christine:[E|Offended] No...[B][C]") ]])
	fnCutscene([[ Append("2855:[E|Neutral] Your obstinancy serves no purpose.[P] We must finish the work we started at the Cryogenics Facility.[B][C]") ]])
	fnCutscene([[ Append("2855:[E|Neutral] Now, transform.[P] Central administration can track your authenticator chip as it stands, but I've disabled that feature on mine.[B][C]") ]])
	fnCutscene([[ Append("2855:[E|Neutral] If you become a human again, we'll be able to move undetected.[P] Which is for the best.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Offended] No![B][C]") ]])
	fnCutscene([[ Append("2855:[E|Neutral] It can't be that hard, if the other rune bearers could do it, you can.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Angry] No!!![B][C]") ]])
	fnCutscene([[ Append("2855:[E|Neutral] You just focus - [P][CLEAR]") ]])
	fnCutscene([[ Append("Christine:[E|Angry] I know how to do it, okay?[P] I've known since the day I was born.[P] It's always been there, but I didn't realize it until now.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Angry] I'm not going back.[P] Ever.[B][C]") ]])
	fnCutscene([[ Append("2855:[E|Upset] This is not optional, Unit 771852.[P] You need to transform so we can proceed.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Angry] Don't -[P] don't you dare touch me![B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Offended] Security Alert![P] Unit 771852 has encountered a maverick in the basement of the maintenance bay![B][C]") ]])
	fnCutscene([[ Append("2855:[E|Upset] You idiot, what are you doing?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Offended] You're a rogue unit![P] Leave me alone![B][C]") ]])
	fnCutscene([[ Append("2855:[E|Upset] Damn it...") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Teleport Sophie in.
	fnCutsceneTeleport("Sophie", 17.25, 16.50)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	fnCutsceneMove("Sophie", 13.25, 16.50, 3.0)
	fnCutsceneMove("Tiffany", 14.25, 8.50)
	fnCutsceneBlocker()
	fnCutsceneFace("Tiffany", 0, 1)
	fnCutsceneFace("Sophie", 0, -1)
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--55 Runs away.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sophie", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Upset") ]])
	fnCutscene([[ Append("Sophie:[E|Surprised] 771852?[P] What's going on?[P] I heard a distress call, are you all right?[B][C]") ]])
	fnCutscene([[ Append("2855:[E|Upset] Not good...") ]])
	fnCutsceneBlocker()
	
	--Zoop!
	fnCutsceneMove("Tiffany", 14.25, 6.50, 2.50)
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	fnCutsceneTeleport("Tiffany", -100.0, -100.0)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--55 Runs away.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ Append("Christine:[E|Sad] Over here, Sophie.[P] Over here...") ]])
	fnCutsceneBlocker()
	
	--Move up.
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	fnCutsceneMove("Christine", 16.25, 8.50, 0.30)
	fnCutsceneMove("Sophie", 13.25, 8.50, 3.0)
	fnCutsceneMove("Sophie", 15.25, 8.50, 3.0)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Sad") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
	fnCutscene([[ Append("Sophie:[E|Surprised] Christine![P] What happened?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Sad] A rogue unit ambushed me.[P] I think she escaped into the vents.[B][C]") ]])
	fnCutscene([[ Append("Sophie:[E|Surprised] Are you all right?[P] You don't look damaged...[B][C]") ]])
	fnCutscene([[ Append("Sophie:[E|Neutral] I'll do a full system scan![P] You'll be all right, I promise![B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Sad] No, I'm fine.[P] I'll be all right.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] ...[B][C]") ]])
	fnCutscene([[ Append("Sophie:[E|Sad] Are you sure?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Yeah.[P] I just need to go rest my drives for a bit.[P] I took a bit of a shock.[B][C]") ]])
	fnCutscene([[ Append("Sophie:[E|Neutral] Did you get a clear look at the rogue unit?[P] Or get her designation?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] ...[P] No.[P] Sorry.[P] It was dark and I didn't raise my ocular unit sensitivity in time.[B][C]") ]])
	fnCutscene([[ Append("Sophie:[E|Smirk] It's all right.[P] You're safe now.[B][C]") ]])
	fnCutscene([[ Append("Sophie:[E|Smirk] I'll walk you back to your quarters.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Yeah, yeah.[P] I'll just go recharge and be good as new tomorrow, okay?[B][C]") ]])
	fnCutscene([[ Append("Sophie:[E|Neutral] All right, all right.[P] Sorry for fussing.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Smirk] Thanks, Sophie.[P] Thanks...") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	
	--Fade to black.
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneBlocker()
	
	--Level transition.
	fnCutscene([[ AL_BeginTransitionTo("RegulusCityZ", gsRoot .. "Chapter 5/Scenes/500 Normal/Post Tiffany Meeting/Scene_Begin.lua") ]])
	fnCutsceneBlocker()

--Let's go SHOPPING!
elseif(sObjectName == "ShoppingTime") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Happy") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Happy") ]])
    fnCutscene([[ Append("Christine:[E|Happy] No time like the present![P] Come on![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] Fantastic![P] Let's head to the tram and take it to Sector 119![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] Lead the way, Lord Unit!") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
    
    --Move.
    fnCutsceneMove("Sophie", 13.25, 8.50)
	fnCutsceneBlocker()
    
    --Flags.
	VM_SetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter5/WorldState/iNoWarpsAllowed", "N", 1.0)
	EM_PushEntity("Sophie")
		local iSophieID = RE_GetID()
	DL_PopActiveObject()
    
    --Fold the party.
	giFollowersTotal = 1
	gsaFollowerNames = {"Sophie"}
	giaFollowerIDs = {0}
	giaFollowerIDs = {iSophieID}
	AL_SetProperty("Follow Actor ID", iSophieID)

	--Fold the party.
	fnCutscene([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()
    
--Not right now, sorry.
elseif(sObjectName == "NotThisSecond") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Happy") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Happy") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Oh, I want to very much, but I do have a few things I need to take care of.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Can you wait a little bit?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] Certainly![P] I'll try to get some work done in the meantime.[P] Just come upstairs when you're ready to go.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
    
    --Move.
    fnCutsceneMove("Sophie", 14.25, 11.50)
    fnCutsceneMove("Sophie", 13.25, 11.50)
    fnCutsceneMove("Sophie", 13.25, 16.50)
    fnCutsceneMove("Sophie", 17.25, 16.50)
	fnCutsceneFace("Sophie", 0, -1)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	fnCutsceneTeleport("Sophie", -100.0, -100.0)
	fnCutsceneBlocker()
    
--Post Latex-Drone TF scene.
elseif(sObjectName == "PostLatex") then

    --Repeat check.
    local iManuPostLatexScene = VM_GetVar("Root/Variables/Chapter5/Scenes/iManuPostLatexScene", "N")
    if(iManuPostLatexScene == 0.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iManuPostLatexScene", "N", 0.0)
    
    --If 55 is present, move her away.
    if(EM_Exists("Tiffany") == true) then
        fnCutsceneTeleport("Tiffany", -1.25, -1.50)
    end
    
    --Make sure Christine is a drone.
    LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Latex.lua")
    
    --Black the screen out.
    fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Spawn Sophie.
    TA_Create("Sophie")
        TA_SetProperty("Position", 22, 11)
        TA_SetProperty("Clipping Flag", false)
        fnSetCharacterGraphics("Root/Images/Sprites/Sophie/", true)
        TA_SetProperty("Facing", gci_Face_East)
    DL_PopActiveObject()
    
    --Position actors.
    fnCutsceneTeleport("Christine", 24.25, 10.50)
    fnCutsceneFace("Christine", -1, 0)
    fnCutsceneTeleport("Sophie", 22.25, 11.50)
    fnCutsceneFace("Sophie", 1, 0)
    fnCutsceneBlocker()
    
    --Fade in.
	fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Christine", 24.25, 11.50, 0.50)
    fnCutsceneMove("Christine", 23.25, 11.50, 0.50)
    fnCutsceneBlocker()
	fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Happy") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] CONVERSION COMPLETED.[P] INPUT FUNCTION ASSIGNMENT.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] You turned out wonderfully![P] You turn a standard unit template into a work of art![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] THANK YOU, SUPERIOR UNIT.[P] INPUT FUNCTION ASSIGNMENT.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] Hee hee![P] Function assignment?[P] Oooh, Sophie![P] Dirty thoughts, control yourself![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] INSTRUCTIONS UNCLEAR.[P] RE-INPUT FUNCTION ASSIGNMENT.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] Christine![P] You might be carrying this a bit too far![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] CARRY OBJECT.[P] DESTINATION UNCLEAR, OBJECT UNCLEAR.[P] RE-INPUT FUNCTION ASSIGNMENT.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Surprised] Wh-[P] Oh no![P] I left the inhibitor settings at normal![P] Turn around, drone unit!") ]])
    fnCutsceneBlocker()
	fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Movement.
    fnCutsceneFace("Christine", 0, 1)
	fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneFace("Christine", 1, 0)
	fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneMove("Sophie", 22.75, 11.50, 0.10)
    fnCutsceneBlocker()
	fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutsceneMoveFace("Sophie", 22.25, 11.50, 1, 0, 1.00)
    fnCutsceneBlocker()
	fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutsceneFace("Christine", -1, 0)
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] That should do it![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] SOPHIE![P] YOU FORGOT TO DISABLE THE INHIBITOR SETTINGS![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] Hee hee![P] Maybe I forgot about them![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] (Maybe I didn't![P] Hee hee hee hee!)[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] IT'S ALL RIGHT.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] MAYBE YOU CAN TURN IT BACK ON WHEN I DON'T HAVE A SECRET MISSION![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] I would love that![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] NOW, LET'S SEE HERE.[P] DO YOU KNOW HOW I RESET MY VOICE MODULATOR?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Software setting.[P] Check tools, voice, settings.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] IT'S NOT IN THERE.[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] It isn't?[P] But the manual says...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] NO, WAIT.[P] MY DRONE SOFTWARE IS SLIGHTLY OUT OF DATE, BECAUSE THIS CONVERSION TUBE IS A FEW MONTHS OLD.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] There we go![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Ah, good.[P] Be sure to update your software over the network, it'd be suspicious if you had outdated code.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Of course![P] Thanks for your help, Sophie![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] D-[P]do you need an inspection or anything before you go?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Blush] My goodness, yes...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Scared] N-[P]no![P] Important mission![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] I'll be upstairs at my workstation if you think your assets need to be inspected...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Blush] M-[P]maybe it can wait...[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Happy] Hee hee![P] Come see me if you like![P] But you've got spy sneaking to do![P] Go to it!") ]])
    fnCutsceneBlocker()
	fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Sophie", 21.25, 11.50)
    fnCutsceneMove("Sophie", 21.25, 16.50)
    fnCutsceneMove("Sophie", 17.25, 16.50)
    fnCutsceneFace("Sophie", 0, -1)
    fnCutsceneBlocker()
	fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneTeleport("Sophie", -1.25, -1.50)
    fnCutsceneBlocker()
	fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Blush") ]])
    fnCutscene([[ Append("Christine:[E|Blush] (What did I do to deserve a girl like her?)[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] (Well, the next move is for 55 and I to return to Sector 99 and get me into the sector undercover.)") ]])
    fnCutsceneBlocker()

end
