-- |[Constructor]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "RegulusCityB"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[Standard]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
    local iIsGalaTime = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N")
    if(iIsGalaTime == 0.0) then
        AL_SetProperty("Music", "RegulusCity")
    else
        AL_SetProperty("Music", "SophiesThemeSlow")
    end
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation("RegulusCityB")
	
	-- |[Spawn NPCS Here]|
	--NPCs set to spawn here will be available for post-exec cutscenes.

-- |[Post-Exec]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

    -- |[Facing Override]|
    fnConstructorFacing()

	-- |[Spawn NPCs Here]|
	--If an entity needs to spawn after a post-exec cutscene, put them here.
    local iIsGalaTime = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N")
    if(iIsGalaTime == 0.0) then
        fnStandardNPCByPosition("GolemBA")
        fnStandardNPCByPosition("GolemBB")
        fnStandardNPCByPosition("GolemBC")
        fnStandardNPCByPosition("GolemBD")
        fnStandardNPCByPosition("GolemBE")
        fnStandardNPCByPosition("GolemBF")
        fnStandardNPCByPosition("GolemBG")
        fnStandardNPCByPosition("GolemBH")
        fnStandardNPCByPosition("GolemBI")
        fnStandardNPCByPosition("GolemBJ")
        fnStandardNPCByPosition("GolemBK")

        -- |[Work Terminal]|
        --Determine if the work terminal should be red or green. Green means an assignment is available, red means no assignments available.
        LM_ExecuteScript(gsRoot .. "Chapter 5/Scenes/300 Standards/Work Terminal/QueryFunction.lua")

        --A work assignment is available, so show green work terminals.
        if(gbHasWorkAssignment == true) then
            AL_SetProperty("Set Layer Disabled", "WorkTerminalToggleA", true)
            AL_SetProperty("Set Layer Disabled", "WorkTerminalToggleB", true)
        end

        -- |[Night Working]|
        --Randomly roll a position for Night to be at.
        local sBasePath = fnResolvePath()
        local iIsOnDate = VM_GetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N")
        local i254Completed = VM_GetVar("Root/Variables/Chapter5/Scenes/i254Completed", "N")
        if(i254Completed == 1.0 and iIsOnDate == 0.0) then
            local iNightPosition = LM_GetRandomNumber(0, 1)
            VM_SetVar("Root/Variables/Chapter5/Scenes/iNightPosition", "N", iNightPosition)
            if(iNightPosition == 0) then
                fnStandardNPCByPosition("NightA")
                fnCreateStandardPathScript("Night Work", sBasePath .. "PathScript_NightA.lua", gcbHideParallelTiming, 0, 1760)
            else
                fnStandardNPCByPosition("NightB")
                fnCreateStandardPathScript("Night Work", sBasePath .. "PathScript_NightB.lua", gcbHideParallelTiming, 0, 226)
            end
        end
        
    --Gala time! NPC listing changes.
    else
        fnStandardNPCByPosition("GolemGalaA")
        fnStandardNPCByPosition("GolemGalaB")
        fnStandardNPCByPosition("GolemGalaC")
        fnStandardNPCByPosition("GolemGalaD")
        fnStandardNPCByPosition("GolemGalaE")
	end
end
