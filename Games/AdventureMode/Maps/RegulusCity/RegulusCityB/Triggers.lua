-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
--Cutscene where Sophie shows Christine the elevators.
if(sObjectName == "ElevatorTriggerL" or sObjectName == "ElevatorTriggerR") then

	--Variables.
	local iSophieFirstDateState = VM_GetVar("Root/Variables/Chapter5/Scenes/iSophieFirstDateState", "N")
	
	--Scene.
	if(iSophieFirstDateState == 1.0) then
		
		--Flags.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSophieFirstDateState", "N", 2.0)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iMainQuestBlock", "N", 14.0)
			
		--Remove Sophie from the party. She will have a different spawn in the next segment.
		giFollowersTotal = 0
		gsaFollowerNames = {}
		giaFollowerIDs = {0}
		AL_SetProperty("Unfollow Actor Name", "Sophie")
		
		--Movement for the left side.
		if(sObjectName == "ElevatorTriggerL") then
			fnCutsceneMove("Sophie", 7.25, 15.50)
			fnCutsceneMove("Christine", 6.25, 15.50)
			fnCutsceneBlocker()
			fnCutsceneFace("Christine", 0, -1)
			fnCutsceneFace("Sophie", 0, -1)
			fnCutsceneBlocker()
			fnCutsceneWait(45)
			fnCutsceneBlocker()
		
		--Movement for the right side.
		else
			fnCutsceneMove("Sophie", 11.25, 15.50)
			fnCutsceneMove("Christine", 12.25, 15.50)
			fnCutsceneBlocker()
			fnCutsceneFace("Christine", 0, -1)
			fnCutsceneFace("Sophie", 0, -1)
			fnCutsceneBlocker()
			fnCutsceneWait(45)
			fnCutsceneBlocker()
		end

		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] These elevators can take you to the apartment blocks above us.[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] They also go down to the basement levels, but we probably shouldn't go there.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Are there apartments in the basement as well?[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Hm?[P] Oh, that's not what I meant.[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] When Regulus City was being built, there wasn't really a plan.[P] As a result, there's a lot of now-unused corridors and access points.[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Sometimes things get in, so security doesn't like us going down there without a good reason.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Sad] *Things*?[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Oh, the wildlife of Regulus.[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] There are quite a few voidborne species of partirhuman on Regulus, and sometimes a broken robot will go maverick down there.[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Smirk] But you look like you'd be able to handle them...[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] Oh, thank you![P] I used to coach -[P] hmm...[P] my memory drives are a little fuzzy.[P] I think I used to teach sports...[P] maybe?[P] I think I taught literature, too.[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Smirk] Don't worry about it.[P] Your organic memories got repurposed when you were converted.[P] It causes much less distress than flat reprogramming.[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Smirk] Who you were before doesn't really matter that much.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] No.[P] No it doesn't.[P] What matters is now.[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Smirk] Hee hee![B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Well.[P] I'll get the other one to my quarters.[P] Good night.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Blush] Should we -[P] will you -[P] you'll be there tomorrow, right?[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Smirk] Of course I will, you silly spanner![P] Where else would I be?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Blush] Of course.[P] Of course.") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Sophie walks to the opposite elevator, left side:
		if(sObjectName == "ElevatorTriggerL") then
			fnCutsceneMove("Christine", 6.75, 15.50)
			fnCutsceneMove("Sophie", 11.25, 15.50)
			fnCutsceneMove("Sophie", 11.25, 14.50)
			fnCutsceneBlocker()
			fnCutsceneWait(45)
			fnCutsceneBlocker()
		
		--Right side.
		else
			fnCutsceneMove("Christine", 11.75, 15.50)
			fnCutsceneMove("Sophie", 7.25, 15.50)
			fnCutsceneMove("Sophie", 7.25, 14.50)
			fnCutsceneBlocker()
			fnCutsceneWait(45)
			fnCutsceneBlocker()
		end
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] Goodnight...") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Sophie turns to face Christine, left side:
		if(sObjectName == "ElevatorTriggerL") then
			fnCutsceneFace("Sophie", -1, 0)
			fnCutsceneWait(25)
			fnCutsceneBlocker()
		
		--Right side.
		else
			fnCutsceneFace("Sophie", 1, 0)
			fnCutsceneWait(25)
			fnCutsceneBlocker()
		end
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Sophie:[VOICE|Sophie] See you tomorrow!") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Common.
		fnCutsceneFace("Sophie", 0, -1)
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		fnCutsceneTeleport("Sophie", -100.25, -100.50)
		fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
		fnCutsceneWait(85)
		fnCutsceneBlocker()
		fnCutsceneFace("Christine", 0, -1)
		fnCutsceneWait(75)
		fnCutsceneBlocker()
		
		--Fade to black, stop the music.
		fnCutscene([[ AL_SetProperty("Activate Fade", 125, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
		fnCutsceneWait(120)
		fnCutsceneBlocker()
		fnCutscene([[ AL_SetProperty("Music", "NullSlow") ]])
		fnCutsceneWait(120)
		fnCutsceneBlocker()
        fnCutscene([[ AM_SetProperty("Execute Rest") ]])
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] (Please don't be a wonderful dream...)") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(85)
		fnCutsceneBlocker()
		
		--Begin the date montage scene.
        local sPath = fnResolvePath() .. "Scene Post Date Montage.lua"
		fnCutscene([[ AL_BeginTransitionTo("RegulusCityWholeCutscene", "]] .. sPath .. [[") ]])
		fnCutsceneBlocker()

    end

-- |[ ==================================== Secrebot Finale ===================================== ]|
elseif(sObjectName == "Secrebots") then

    -- |[Setup]|
    --Change Christine to Golem form.
    LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua")
    fnCutsceneTeleport("Christine", -1.25, -1.50)
    
    --Remove 55 from the party, warp her offscreen.
    giFollowersTotal = 0
    gsaFollowerNames = {}
    giaFollowerIDs = {0}
    AL_SetProperty("Unfollow Actor Name", "Tiffany")
    if(EM_Exists("Tiffany") == true) then
        fnCutsceneTeleport("Tiffany", -1.25, -1.50)
    end

    --Flag to indicate she is not following Christine.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 0.0)
    
    --Camera.
    fnCutsceneCamera("Position", gcfCamera_Cutscene_Speed, 6.75, 14.50)
    
    --Blackout.
    fnCutsceneFadeOut()
    fnCutsceneWait(45)
    fnCutsceneBlocker()

    -- |[Fade In]|
    fnCutsceneFadeIn(45)
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneTeleport("Christine", 6.75, 14.50)
    fnCutsceneFace("Christine", 0, 1)
    fnCutscenePlaySound("World|AutoDoorOpen")
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Christine:[VOICE|Christine] (Phew, what a night!)[B][C]") ]])
    fnCutscene([[ Append("Christine:[VOICE|Christine] (Now I just need to figure out if it's my secrebot programming that makes me horny to clean the floor, or if it's Sophie.[P] Either way!)") ]])
    fnCutsceneBlocker()
    fnCutsceneCamera("Actor Name", gcfCamera_Cutscene_Speed, "Christine")

end
