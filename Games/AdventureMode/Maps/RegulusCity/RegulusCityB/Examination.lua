-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--Global temporary variable.
sLastElevator = "ElevatorsL"

-- |[Sophie Leading]|
--During the secrebot sequence, Sophie is in the party lead and uses a different file.
local b254SophieLeading = fnIsSophieLeader()
if(b254SophieLeading == true) then
    LM_ExecuteScript(fnResolvePath() .. "ExaminationSophie.lua", sObjectName)
    return
end

-- |[ ========================================= Exits ========================================== ]|
-- |[ ====================================== Examinables ======================================= ]|
--Vending machine.
if(sObjectName == "FizzyDrinkBlue") then
    local iIsGalaTime = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N")
    if(iIsGalaTime == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("[VOICE|Christine](Fizzy drinks, a perfect way to boost your productive output![P] This is the blue variety, meant for manual labour units.[P] The chemical composition is the same as the pink variety, though.)") ]])
        fnCutsceneBlocker()
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("[VOICE|Christine](I really don't want to risk spilling any Fizzy Pop! on my dress...)") ]])
        fnCutsceneBlocker()
    end
    
--Vending machine.
elseif(sObjectName == "FizzyDrinkPink") then
    local iIsGalaTime = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N")
    if(iIsGalaTime == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("[VOICE|Christine](Fizzy drinks, a perfect way to boost your productive output![P] This is the pink variety, meant for Lord Units like me.[P] The chemical composition is the same as the blue variety, though.)") ]])
        fnCutsceneBlocker()
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("[VOICE|Christine](I really don't want to risk spilling any Fizzy Pop! on my dress...)") ]])
        fnCutsceneBlocker()
    end
	
--Booze.
elseif(sObjectName == "Booze") then
    local iIsGalaTime = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N")
    if(iIsGalaTime == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("[VOICE|Christine](The smell of alcohol is unmistakable.[P] This would have no useful digestive properties for a Golem, but would probably intoxicate an organic.)") ]])
        fnCutsceneBlocker()
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("[VOICE|Christine](I doubt there will be any organics at the gala, but if there were, they'd be drinking this.)") ]])
        fnCutsceneBlocker()
    end

--Broken cryogenics equipment.
elseif(sObjectName == "CryoEquipment") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Christine](A conversion cell and terminals.[P] Looks like it's here for servicing.)") ]])
	fnCutsceneBlocker()
	
--In-use console.
elseif(sObjectName == "Console") then
    local iIsGalaTime = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N")
    if(iIsGalaTime == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("[VOICE|Christine](This console is in use already.[P] Looks like a combat simulation program is running.)") ]])
        fnCutsceneBlocker()
	else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("[VOICE|Christine](Oddly, whoever plays the armored heroine character seems to win at this simulation...)") ]])
        fnCutsceneBlocker()
    end
    
--Game Screen.
elseif(sObjectName == "GameScreen") then
    local iIsGalaTime = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N")
    if(iIsGalaTime == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("[VOICE|Christine](A combat simulation program, paused at the moment.[P] The fighter on the left seems to be winning.)") ]])
        fnCutsceneBlocker()
	else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("[VOICE|Christine](The armored heroine character always seems to lose at first, but then she gets the missile launcher and it's all over.)") ]])
        fnCutsceneBlocker()
    end

-- |[Work Terminal]|
--Offsite dialogue script.
elseif(sObjectName == "WorkTerminal") then

    --Variables.
    local iIsGalaTime = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N")
    
    if(iIsGalaTime == 0.0) then
        LM_ExecuteScript(gsRoot .. "Chapter 5/Scenes/300 Standards/Work Terminal/Execution.lua")
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("[VOICE|Christine](No need to access the work terminal right now.[P] I've got a gala to attend.)") ]])
        fnCutsceneBlocker()
    end

--Apartment Terminal
elseif(sObjectName == "ApartmentTerminal") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Christine](This terminal shows address listings for the various units in the residential block above.)") ]])

-- |[Elevators]|
--Enter Christine's quarters.
elseif(sObjectName == "ElevatorsL" or sObjectName == "ElevatorsR") then

	--Variables.
	sLastElevator = sObjectName
	local iReceivedFunction        = VM_GetVar("Root/Variables/Chapter5/Scenes/iReceivedFunction", "N")
	local iToldSophieAboutFunction = VM_GetVar("Root/Variables/Chapter5/Scenes/iToldSophieAboutFunction", "N")
	local iSophieImportantMeeting  = VM_GetVar("Root/Variables/Chapter5/Scenes/iSophieImportantMeeting", "N")
	local iMet55InLowerRegulus     = VM_GetVar("Root/Variables/Chapter5/Scenes/iMet55InLowerRegulus", "N") 
	local iSophieFirstDateState    = VM_GetVar("Root/Variables/Chapter5/Scenes/iSophieFirstDateState", "N")
    local iIsGalaTime              = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N")
    local iStartedShoppingSequence = VM_GetVar("Root/Variables/Chapter5/Scenes/iStartedShoppingSequence", "N")
	local iIsOnDate                = VM_GetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N")

    --Gala.
    if(iIsGalaTime > 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[VOICE|Christine](Going into the basement and roughing up some maverick robots might scuff my dress.[P] Inconceivable!)") ]])
		fnCutsceneBlocker()

	--Player doesn't have a function assignment. Don't use the elevator.
	elseif(iReceivedFunction == 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[VOICE|Christine](This elevator can take me to my quarters, but my programming dictates I should get my function assignment first.)") ]])
		fnCutsceneBlocker()

	--Player has not told Sophie about her function yet. Don't use the elevator.
	elseif(iToldSophieAboutFunction == 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[VOICE|Christine](This elevator can take me to my quarters, but I should really go tell Sophie about my function assignment first!)") ]])
		fnCutsceneBlocker()
        
    --Shopping!
    elseif(iStartedShoppingSequence == 1.0 and iIsOnDate == 1.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[VOICE|Christine](I need to take Sophie to the tram, not my room.)") ]])
		fnCutsceneBlocker()
	
	--Player has not completed her first date with Sophie yet.
	elseif(iSophieFirstDateState < 2.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[VOICE|Christine](I should let Sophie show me around a bit before I head up to my quarters.)") ]])
		fnCutsceneBlocker()

	--Player has access to only their quarters:
	elseif(iSophieImportantMeeting == 0.0) then

		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[VOICE|Christine](Which floor should I go to?)[BLOCK]") ]])
		
		--Decision script is this script. It must be surrounded by quotes.
		local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
		fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
		fnCutscene(" WD_SetProperty(\"Add Decision\", \"My Quarters\", " .. sDecisionScript .. ", \"ElevatorToQuarters\") ")
		fnCutscene(" WD_SetProperty(\"Add Decision\", \"Cancel\",  " .. sDecisionScript .. ", \"NoClose\") ")
		fnCutsceneBlocker()

	--Important meeting with Sophie.
	elseif(iSophieImportantMeeting == 1.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[VOICE|Christine](Sophie said it would safe to talk while in the repair bay...)") ]])
		fnCutsceneBlocker()
		
	--Player has access to the lower floors, and reminds herself about 55.
	elseif(iSophieImportantMeeting == 2.0 and iMet55InLowerRegulus == 0.0) then

		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[VOICE|Christine](Which floor should I go to?[P] The PDU said 2855 was using terminals on the first basement floor...)[BLOCK]") ]])
		
		--Decision script is this script. It must be surrounded by quotes.
		local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
		fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
		fnCutscene(" WD_SetProperty(\"Add Decision\", \"My Quarters\", " .. sDecisionScript .. ", \"ElevatorToQuarters\") ")
		fnCutscene(" WD_SetProperty(\"Add Decision\", \"Basement 1F\", " .. sDecisionScript .. ", \"ElevatorToBasement1F\") ")
		fnCutscene(" WD_SetProperty(\"Add Decision\", \"Cancel\",  " .. sDecisionScript .. ", \"NoClose\") ")
		fnCutsceneBlocker()
	
	--Player has access to the lower floors. No reminder.
	else

		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[VOICE|Christine](Which floor should I go to?)[BLOCK]") ]])
		
		--Decision script is this script. It must be surrounded by quotes.
		local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
		fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
		fnCutscene(" WD_SetProperty(\"Add Decision\", \"My Quarters\", " .. sDecisionScript .. ", \"ElevatorToQuarters\") ")
		fnCutscene(" WD_SetProperty(\"Add Decision\", \"Basement 1F\", " .. sDecisionScript .. ", \"ElevatorToBasement1F\") ")
		fnCutscene(" WD_SetProperty(\"Add Decision\", \"Basement 2F\", " .. sDecisionScript .. ", \"ElevatorToBasement2F\") ")
		fnCutscene(" WD_SetProperty(\"Add Decision\", \"Cancel\",  " .. sDecisionScript .. ", \"NoClose\") ")
		fnCutsceneBlocker()
	
	end

--Elevator to Christine's Quarters.
elseif(sObjectName == "ElevatorToQuarters") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Change maps.
	fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
	fnCutscene([[ AL_BeginTransitionTo("RegulusCityZ", "FORCEPOS:8.5x10.0x0") ]])
	fnCutsceneBlocker()

--Elevator to Basement 1F.
elseif(sObjectName == "ElevatorToBasement1F") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--If Sophie is present:
	if(giFollowersTotal > 0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Erm, Christine?[P] I probably shouldn't go to the basement...[P] I'm not [P]*cleared*[P] like someone we know.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Cleared?[P] Oh![P] Of course, sorry.[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Did you finger slip?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Yep, that's it.") ]])
	
	--Nope.
	else
	
		--Change maps.
		fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
		if(sLastElevator == "ElevatorsL") then
			fnCutscene([[ AL_BeginTransitionTo("LowerRegulusCityA", "FORCEPOS:22.5x15.0x0") ]])
		else
			fnCutscene([[ AL_BeginTransitionTo("LowerRegulusCityA", "FORCEPOS:27.5x15.0x0") ]])
		end
		fnCutsceneBlocker()
	end

--Elevator to Basement 2F.
elseif(sObjectName == "ElevatorToBasement2F") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--If Sophie is present:
	if(giFollowersTotal > 0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Erm, Christine?[P] I probably shouldn't go to the basement...[P] I'm not [P]*cleared*[P] like someone we know.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Cleared?[P] Oh![P] Of course, sorry.[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Did you finger slip?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Yep, that's it.") ]])
	
	--Nope.
	else
	
		--Change maps.
		fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
		if(sLastElevator == "ElevatorsL") then
			fnCutscene([[ AL_BeginTransitionTo("LowerRegulusCityB", "FORCEPOS:27.5x13.0x0") ]])
		else
			fnCutscene([[ AL_BeginTransitionTo("LowerRegulusCityB", "FORCEPOS:31.5x13.0x0") ]])
		end
		fnCutsceneBlocker()
	end

-- |[Execution]|
--Fabricator that is unused.
elseif(sObjectName == "FabricatorDate") then

	--Variables.
	local iIsOnDate            = VM_GetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N")
	local iExplainedFabricator = VM_GetVar("Root/Variables/Chapter5/Sophie/iExplainedFabricator", "N")
    local iIsGalaTime          = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N")
    local iStartedShoppingSequence = VM_GetVar("Root/Variables/Chapter5/Scenes/iStartedShoppingSequence", "N")

    --Gala case.
    if(iIsGalaTime > 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[VOICE|Christine](The fabricators are quiet for the first time since I was converted...)") ]])
		fnCutsceneBlocker()

	--Not on a date. Examine the stuff.
	elseif(iIsOnDate == 0.0 or iStartedShoppingSequence == 1.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[VOICE|Christine](A fabrication machine.[P] This one is currently not in use.)") ]])
		fnCutsceneBlocker()

	--Needs to explain maintenance.
	elseif(iIsOnDate == 1.0 and iExplainedFabricator == 0.0) then
		VM_SetVar("Root/Variables/Chapter5/Sophie/iExplainedFabricator", "N", 1.0)
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] This fabricator is currently not in use.[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Would you like to spend an evening working one with me?[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Just let your subroutines do the handwork.[P] We could chat with one another.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] You'd find that to be a good time?[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] I want to know more about you.[P] Let's spend some time chatting.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Should we spend the evening working the fabricators, then?[BLOCK]") ]])

		--Decision script is this script. It must be surrounded by quotes.
		local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
		fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
		fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"WorkFabricators\") ")
		fnCutscene(" WD_SetProperty(\"Add Decision\", \"No Thanks\",  " .. sDecisionScript .. ", \"NoClose\") ")
		fnCutsceneBlocker()

	--Date option.
	elseif(iIsOnDate == 1.0 and iExplainedFabricator == 1.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Should we spend the evening working the fabricators?[BLOCK]") ]])

		--Decision script is this script. It must be surrounded by quotes.
		local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
		fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
		fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"WorkFabricators\") ")
		fnCutscene(" WD_SetProperty(\"Add Decision\", \"No Thanks\",  " .. sDecisionScript .. ", \"NoClose\") ")
		fnCutsceneBlocker()
	end

-- |[Post Decision]|
--Spend a date working the fabricators.
elseif(sObjectName == "WorkFabricators") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")

	--Fade.
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneWait(85)
	fnCutsceneBlocker()

	--Execute the conversation routing case.
	LM_ExecuteScript(gsRoot .. "Chapter 5/Scenes/500 Normal/Sophie Conversation/Z Routing.lua")
	fnCutsceneBlocker()

	--Fade.
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
	fnCutsceneWait(85)
	fnCutsceneBlocker()
	
	--Variables.
	local iSophieFirstDateState = VM_GetVar("Root/Variables/Chapter5/Scenes/iSophieFirstDateState", "N")
	local iSophieWillSynchronizeNow = VM_GetVar("Root/Variables/Chapter5/Sophie/iSophieWillSynchronizeNow", "N")
	
	--First date.
	if(iSophieFirstDateState == 0.0) then
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSophieFirstDateState", "N", 1.0)
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Who would have thought manual labour could be so much fun with you![B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] A bit unorthodox for a first date but I enjoyed it plenty![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Ummm...[B][C]") ]])
        
        
		fnCutscene([[ Append("Christine:[E|Neutral] Date?[P] that wasn't a date, if I knew it was a date I would have done something nicer![B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Blush] Uh, uh![P] No, I was just showing you around![P] Hee hee![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|PDU] Matchmaker -[P] shut up, PDU![B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Blush] Matchmaker?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Offended] It's my stupid PDU.[P] It said you were ogling me with your big pretty eyes and - [B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Offended] You keep flicking your hair a bit when you talk.[P] You keep looking at me.[P] I can't take my eyes off you.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Offended] What is your deal?[P] We're going to be working together so you may as well just tell me why you don't like me![B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Sad] Don't like you?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Offended] Yeah![P] Am I ugly, or weird, or something?[P] As a lord golem I order you to tell me![B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] ...[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Do you want to kiss me?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Offended] Of course![B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] *smooch*[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Blush] H-[P]huh?[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Happy] Want another one?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Blush] Yeah![B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] *smooch*[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Blush] My goodness, Sophie, Unit 499323, do you like me?[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Happy] Of course I do![P] I like you a lot![P] How do you not see that?[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Happy] I was holding your hand for half of that![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Blush] You were just -[P] guiding me while I got used to the tools![B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Offended] You're not fooling anyone.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] I tried, but...[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] Do you...[P] want to be my tandem unit?[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Happy] I thought you'd never ask![P] Of course![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] But I'm your lord golem...[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Blush] Yeah, but I don't care what anyone else thinks![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] What does that have to do with it?[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] We're different model variants.[P] Slave and lord, we're not supposed to date.[P] You didn't know?[P] Well, you are a fresh convert so...[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Yes, we'll be looked down on.[P] But I don't care.[P] I want you.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] I want you too![B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Offended] Darn it.[P] Now we can't have weeks of will-they-won't-they sexual tension...[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Sorry.[P] I can pretend not - [B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Offended] Oh no you don't![B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Happy] Phew![P] What a day it has been![P] I really need a recharge![P] Would you like to walk me back to my room?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] Of course.[P] The habitation block is just east of the surface exit, right?[P] Let's go!") ]])
        
        --[=[
		fnCutscene([[ Append("Sophie:[E|Neutral] But, I really should go defragment my drives.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Ummm...[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Hee hee![P] You haven't even seen your quarters yet, have you?[P] Do you know where they are?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Vaguely.[P] I didn't download the directory when I got here.[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Well then allow me to show you.[P] We passed the elevators on the way here, they're to the west.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Thanks, Sophie.[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] This does count as walking a lady back to her room.[P] We use the same elevators, of course.[P] All units in the sector use the same domicile block.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] All right.[P] Let's go.") ]])]=]
		fnCutsceneBlocker()
	
	--On successive dates, Sophie requests to return to the maintenance bay unless she wants to synchronize.
	elseif(iSophieWillSynchronizeNow == 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Oh, look at that![P] I should be getting back to the maintenance bay.[P] I better get everything logged before I defragment.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] All right, then.[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] I had a lot of fun today![P] We should do this again![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Me too, Sophie.[B][C]") ]])
		fnCutscene([[ Append("Sophie:[E|Neutral] Would you care to escort your tandem unit?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] With pleasure.") ]])
		fnCutsceneBlocker()
	end
	
	--Variables.
	VM_SetVar("Root/Variables/Chapter5/Sophie/iIsOnDate", "N", 2.0)
	
elseif(sObjectName == "NoClose") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
