-- |[ =================================== Night the Secrebot =================================== ]|
--Handles Night working in the sector.

-- |[Setup]|
local sEntityName = "NightB"
local sFramePath = "Root/Images/Sprites/Special/NightSecrebot|Dust"

-- |[ ========================== Setup =========================== ]|
-- |[Existence Check]|
--If the NPCs don't exist, delete this script.
if(EM_Exists(sEntityName) == false) then
    io.write("Warning: Parallel cutscene deleted due to missing NPC. Internal Name: " .. Cutscene_GetProperty("Parallel Script Name") .. "\n")
    Cutscene_HandleParallel("DELETETHIS")
    return
end

-- |[First Run]|
--If the NPCs don't have their special frames yet, apply them.
EM_PushEntity(sEntityName)
    if(TA_GetProperty("Has Special Frame", "Dust0") == false) then
        for p = 0, 7, 1 do
            TA_SetProperty("Add Special Frame", "Dust"..p, sFramePath .. p)
        end
    end
DL_PopActiveObject()

-- |[ ====================== Cutscene Work ======================= ]|
--Setup.
local iRepetitions = 4
local iTicksPerFrame = 15
local iaPattern = {0, 1, 2, 3}

--Work sequence.
for p = 1, iRepetitions, 1 do
    for i = 1, #iaPattern, 1 do
        fnCutsceneSetFrame(sEntityName, "Dust"..iaPattern[i])
        fnCutsceneWait(iTicksPerFrame)
        fnCutsceneBlocker()
    end
end