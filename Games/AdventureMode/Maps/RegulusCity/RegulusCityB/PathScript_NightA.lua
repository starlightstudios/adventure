-- |[ =================================== Night the Secrebot =================================== ]|
--Handles Night working in the sector.

-- |[Setup]|
local iNightPosition = VM_GetVar("Root/Variables/Chapter5/Scenes/iNightPosition", "N")
local sEntityName = "NightA"
local sFramePath = "Root/Images/Sprites/Special/NightSecrebot|Mop"

-- |[ ========================== Setup =========================== ]|
-- |[Existence Check]|
--If the NPCs don't exist, delete this script.
if(EM_Exists(sEntityName) == false) then
    io.write("Warning: Parallel cutscene deleted due to missing NPC. Internal Name: " .. Cutscene_GetProperty("Parallel Script Name") .. "\n")
    Cutscene_HandleParallel("DELETETHIS")
    return
end

-- |[First Run]|
--If the NPCs don't have their special frames yet, apply them.
TA_ChangeCollisionFlag(sEntityName, false)
EM_PushEntity(sEntityName)
    if(TA_GetProperty("Has Special Frame", "Mop0") == false) then
        for p = 0, 7, 1 do
            TA_SetProperty("Add Special Frame", "Mop"..p, sFramePath .. p)
        end
    end
DL_PopActiveObject()

-- |[ ====================== Cutscene Work ======================= ]|
--Setup.
local iRepetitions = 10
local iTicksPerFrame = 15
local iaPattern = {0, 1, 2, 3}

--Work sequence. Assumes the sequence starts at the left position.
for p = 1, iRepetitions, 1 do
    for i = 1, #iaPattern, 1 do
        fnCutsceneSetFrame(sEntityName, "Mop"..iaPattern[i])
        fnCutsceneWait(iTicksPerFrame)
        fnCutsceneBlocker()
    end
end
fnCutsceneSetFrame(sEntityName, "Null")

--Move to right position.
fnCutsceneMove("NightA", 32.25, 5.50, 0.75)
fnCutsceneBlocker()

--Work sequence.
for p = 1, iRepetitions, 1 do
    for i = 1, #iaPattern, 1 do
        fnCutsceneSetFrame(sEntityName, "Mop"..iaPattern[i])
        fnCutsceneWait(iTicksPerFrame)
        fnCutsceneBlocker()
    end
end
fnCutsceneSetFrame(sEntityName, "Null")

--Move to left position.
fnCutsceneMove("NightA", 17.25, 5.50, 0.75)
fnCutsceneBlocker()
