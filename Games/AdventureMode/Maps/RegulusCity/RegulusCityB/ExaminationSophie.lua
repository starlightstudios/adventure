-- |[ ======================================= Examination ====================================== ]|
--Called if Sophie is the party leader during an examination.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[ ========================================= Exits ========================================== ]|
-- |[ ====================================== Examinables ======================================= ]|
--Vending machine.
if(sObjectName == "FizzyDrinkBlue") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("[VOICE|Sophie](Maybe I could finally get a straight answer out of Christine.[P] Pink or blue flavour.[P] She seems to switch her preference whenever I ask!)") ]])
    fnCutsceneBlocker()
    
--Vending machine.
elseif(sObjectName == "FizzyDrinkPink") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("[VOICE|Sophie](I wonder if secrebots can even drink Fizzy Pop!, better not make a scene.)") ]])
	
--Booze.
elseif(sObjectName == "Booze") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("[VOICE|Sophie](Alcohol.[P] Do we still use this as a cleaner? Seems odd.)") ]])
    fnCutsceneBlocker()

--Broken cryogenics equipment.
elseif(sObjectName == "CryoEquipment") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Sophie](These have been in for servicing for a while.[P] They never asked us to fix it though.)") ]])
	fnCutsceneBlocker()
	
--In-use console.
elseif(sObjectName == "Console") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("[VOICE|Sophie](I wonder if Christine is any good at 'Alien Butt Kicker'.[P] I'm terrible at it.[P] But I absolutely dominate at 'Quantum Checkers'.)") ]])
    fnCutsceneBlocker()

--Game Screen.
elseif(sObjectName == "GameScreen") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("[VOICE|Sophie](I can never get past the first level of this dumb game.)") ]])
    fnCutsceneBlocker()

-- |[Work Terminal]|
--Offsite dialogue script.
elseif(sObjectName == "WorkTerminal") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("[VOICE|Sophie](Oh I could get into all kinds of trouble by making Christine log in for me...)") ]])
    fnCutsceneBlocker()

--Apartment Terminal
elseif(sObjectName == "ApartmentTerminal") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Sophie](Taking Christine back to her quarters and -[P] no![P] Bad robot![P] Take her to the repair bay, Sophie!)") ]])

-- |[Elevators]|
--Enter Christine's quarters.
elseif(sObjectName == "ElevatorsL" or sObjectName == "ElevatorsR") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("[VOICE|Sophie](Stop being horny![P] Stop thinking about taking a helpless secrebot Christine back to your quarters![P] Stop it Sophie!)") ]])
    fnCutsceneBlocker()

-- |[Fabricator]|
--Fabricator that is unused.
elseif(sObjectName == "FabricatorDate") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("[VOICE|Sophie](I really don't have time to use the fabricators.)") ]])
    fnCutsceneBlocker()

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
