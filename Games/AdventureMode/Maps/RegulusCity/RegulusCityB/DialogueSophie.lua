-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
    
    -- |[GolemBA]|
    if(sActorName == "GolemBA") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] 499323![P] Thanks for helping with the modulator, I owe you one.[P] I swear without you I'd never get this wiring fixed.") ]])
        fnCutsceneBlocker()
        
    -- |[GolemBB]|
    elseif(sActorName == "GolemBB") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Don't tell Christine but I think the other lord golems don't like you two.[P] I hear them making snarky remarks about her clothes.") ]])
        fnCutsceneBlocker()
    
    -- |[GolemBC]|
    elseif(sActorName == "GolemBC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Oh, Sophie, did you guys still need those backup isolinear chips?[P] My lord golem asked me to make a few extras.") ]])
        fnCutsceneBlocker()
    
    -- |[GolemBD]|
    elseif(sActorName == "GolemBD") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Guns, ammo, spare parts for guns.[P] I guess it's easy work but it sure is boring.") ]])
        fnCutsceneBlocker()
    
    -- |[GolemBE]|
    elseif(sActorName == "GolemBE") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Hey Sophie.[P] Nice secrebot.[P] Think she could take over for me?[P] Ha ha!") ]])
        fnCutsceneBlocker()
    
    -- |[GolemBF]|
    elseif(sActorName == "GolemBF") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Ugh, I defragged horribly last night.[P] Hey, do you think secrebots dream?") ]])
        fnCutsceneBlocker()
    
    -- |[GolemBG]|
    elseif(sActorName == "GolemBG") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Don't distract me, I'm practicing!") ]])
        fnCutsceneBlocker()
    
    -- |[GolemBH]|
    elseif(sActorName == "GolemBH") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Sophie", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Christine", "Serious") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
        fnCutscene([[ Append("Golem:[E|Neutral] 499323 how in the furnaces did you get a secrebot?[P] And why is she so...[P] hot?[B][C]") ]])
        fnCutscene([[ Append("CR-1-16:[E|Serious] Hello, Secrebot CR-1-16 reporting.[P] Thank you for the compliment.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Blush] Um![P] I'm -[P] repairing her![P] Yeah, so don't expect to see her around the sector very much![B][C]") ]])
        fnCutscene([[ Append("Golem:[E|Neutral] Ah, should have guessed.[P] My lord unit says she wants one really bad.[B][C]") ]])
        fnCutscene([[ Append("Golem:[E|Neutral] I guess you have to be pretty rich in work credits to bribe your way to the top of the waiting list.[P] You think if we pooled our credits we could get one?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] I, uh, erm, get the feeling that there won't be any more produced, really soon![B][C]") ]])
        fnCutscene([[ Append("Golem:[E|Neutral] ...[P] That sounds like something you say when you have a second meaning in mind.[P] What does that mean?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] Uh, um![B][C]") ]])
        fnCutscene([[ Append("Golem:[E|Neutral] You went all the way through the sentence without thinking it through, didn't you.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] Yep![P] I'm such a dumb bolt![B][C]") ]])
        fnCutscene([[ Append("Golem:[E|Neutral] Okay, Sophie.[P] Try to take it easy.") ]])
        fnCutsceneBlocker()
    
    -- |[GolemBI]|
    elseif(sActorName == "GolemBI") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Oh, a secrebot.[P] How elegant.") ]])
        fnCutsceneBlocker()
    
    -- |[GolemBJ]|
    elseif(sActorName == "GolemBJ") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Unit 499323, where is Unit 771852?[P] I came by your department but nobody was there![B][C]") ]])
        fnCutscene([[ Append("CR-1-16:[VOICE|Christine] We apologize, lord unit.[P] Unit 499323 had been called to pick me up from sector 254.[B][C]") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Oh, what a well-behaved secrebot![P] In for repairs, I assume.[P] Well, back to work then!") ]])
        fnCutsceneBlocker()
    
    -- |[GolemBK]|
    elseif(sActorName == "GolemBK") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Ah how I wish I had a secrebot, they're the talk of the town.[P] Maintenance and Repair has been reflecting well on our sector!") ]])
        fnCutsceneBlocker()
        
    end
end
