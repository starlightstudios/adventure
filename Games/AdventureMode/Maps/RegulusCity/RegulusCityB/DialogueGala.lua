-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    --Set facing.
    TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[GolemGalaA]|
    if(sActorName == "GolemGalaA") then
        
        --Variables.
        local iSpokeGalaLord96 = VM_GetVar("Root/Variables/Chapter5/Scenes/iSpokeGalaLord96", "N")
        if(iSpokeGalaLord96 == 0.0) then
            VM_SetVar("Root/Variables/Chapter5/Scenes/iSpokeGalaLord96", "N", 1.0)
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Lord Unit 649728.[P] You're looking well.[B][C]") ]])
            fnCutscene([[ Append("649728:[E|Neutral] Lord Unit 771852.[P] I suppose this was inevitable, wasn't it?[B][C]") ]])
            fnCutscene([[ Append("649728:[E|Neutral] Your department's productivity is the envy of the sector.[P] Suiting that you'd get an invitation to the Sunrise Gala.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] You didn't?[B][C]") ]])
            fnCutscene([[ Append("649728:[E|Neutral] Not this year, but the fabrication department has been on a bit of a downswing.[P] Oh well.[B][C]") ]])
            fnCutscene([[ Append("649728:[E|Neutral] But who is this enchanting lady I see before me?[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Blush] H-[P]hello![P] Lord Unit![B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] This is my tandem unit.[P] She's from Sector 88.[P] You don't know her.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] You may call her -[P] Andrea.[P] And you can call me Christine.[P] Unit 771852 is so formal![B][C]") ]])
            fnCutscene([[ Append("649728:[E|Neutral] Sector 88, hmm?[P] You look familiar, my dear.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Blush] D-[P]do I?[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Smirk] (Act synthetically, Sophie![P] You're going to blow this!)[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] You are likely recognizing my dress more than my face.[P] This is an original Unit 78.[B][C]") ]])
            fnCutscene([[ Append("649728:[E|Neutral] Unit 78 is still making dresses?[P] My stars![B][C]") ]])
            fnCutscene([[ Append("649728:[E|Neutral] You'll be the talk of the gala in something like that.[P] Good evening, Christine and Andrea.[P] Please give them Sector 96's best!") ]])
            fnCutsceneBlocker()
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sophie", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
            fnCutscene([[ Append("649728:[E|Neutral] I should probably be getting to my quarters.[P] My tandem unit will be cross if I'm late.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] A date?[B][C]") ]])
            fnCutscene([[ Append("649728:[E|Neutral] Indeed.[P] Oh I'm going to struggle to walk tomorrow...") ]])
            fnCutsceneBlocker()
        end
        
    -- |[GolemGalaB]|
    elseif(sActorName == "GolemGalaB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Since nobody is supervising, I'm going to practice this game until I can beat Unit 577422!") ]])
        
    -- |[GolemGalaC]|
    elseif(sActorName == "GolemGalaC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] My servos are shot.[P] I just want to go to my quarters and rest, but these two are set on having a party.") ]])
        
    -- |[GolemGalaD]|
    elseif(sActorName == "GolemGalaD") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] When the other bots see the balloon I got, they're gonna flip![B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] Balloon?[P] Singular?[B][C]") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Yeah![P] It'll be so festive!") ]])
        
    -- |[GolemGalaE]|
    elseif(sActorName == "GolemGalaE") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Remember not to drink too much punch at the gala, Christine.[P] Or whatever it is they serve there.") ]])
    end
end
