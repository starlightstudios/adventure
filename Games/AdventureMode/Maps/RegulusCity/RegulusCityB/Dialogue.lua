-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Sophie Leader]|
--During the secrebot sequence, Sophie is in the party lead and uses a different file.
local b254SophieLeading = fnIsSophieLeader()
if(b254SophieLeading == true) then
    LM_ExecuteScript(fnResolvePath() .. "DialogueSophie.lua", sTopicName)
    return
end

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
    
    -- |[GolemBA]|
    if(sActorName == "GolemBA") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Please stay back, Lord Golem.[P] I am repairing the wiring here and would not want you to be harmed by a discharge.") ]])
        fnCutsceneBlocker()
        
    -- |[GolemBB]|
    elseif(sActorName == "GolemBB") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] I am reinstalling software on this conversion tube.[P] If you need to use it right away, you'd have to manually upload memory overwrites.") ]])
        fnCutsceneBlocker()
    
    -- |[GolemBC]|
    elseif(sActorName == "GolemBC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] This fabricator is functioning as expected.[P] Thank you for making sure this Slave unit is being productive.") ]])
        fnCutsceneBlocker()
    
    -- |[GolemBD]|
    elseif(sActorName == "GolemBD") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] We have not fabricated anything for civilian use for some time.[P] Our priority schedule keeps getting updated with weapons and combat equipment.") ]])
        fnCutsceneBlocker()
    
    -- |[GolemBE]|
    elseif(sActorName == "GolemBE") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Our headsets are no longer functioning due to the security lockout.[P] We have to get fabrication instructions from the workstation there.[P] It's much less efficient.") ]])
        fnCutsceneBlocker()
    
    -- |[GolemBF]|
    elseif(sActorName == "GolemBF") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] An abduction team recently acquired these kegs of strong-smelling fluid.[P] Seems the Raijus really enjoy it.[P] Power production is up by eight percent.") ]])
        fnCutsceneBlocker()
    
    -- |[GolemBG]|
    elseif(sActorName == "GolemBG") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Hello, Lord Golem.[P] We are here to serve you.") ]])
        fnCutsceneBlocker()
    
    -- |[GolemBH]|
    elseif(sActorName == "GolemBH") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] If you are heading to the lower floors, be sure to bring personal protection items or a security escort.[P] Some of the service robots have gone maverick down there, and they hide in the corners and vents.") ]])
        fnCutsceneBlocker()
    
    -- |[GolemBI]|
    elseif(sActorName == "GolemBI") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] I take a great deal of pride in the service I provide to my Lord Golem.[P] She is truly an example for all of us.") ]])
        fnCutsceneBlocker()
    
    -- |[GolemBJ]|
    elseif(sActorName == "GolemBJ") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Unit 84211 is good for two things.[P] One of them is hauling, and the other is what she can do with her tongue.[P] No, I will not be loaning her to your service.") ]])
        fnCutsceneBlocker()
    
    -- |[GolemBK]|
    elseif(sActorName == "GolemBK") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] It was only recently that the administration started requiring Lord Units to pay work credits for consumer goods.[P] This makes us little different from the Slave Units, does it not?[P] Detestable.") ]])
        fnCutsceneBlocker()
        
    -- |[NightA]|
    elseif(sActorName == "NightA") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Night:[VOICE|Night] Busy, busy, I love being busy!") ]])
        fnCutsceneBlocker()
    
    -- |[NightB]|
    elseif(sActorName == "NightB") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Night:[VOICE|Night] There's tiny fragments of metal dust everywhere![P] I'll get this place ship-shape!") ]])
        fnCutsceneBlocker()
        
    end
end
