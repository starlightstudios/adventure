-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
--A distress call from the Equinox facility.
if(sObjectName == "Equinox") then
	
	--Variables.
	local iSawEquinoxMessage = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawEquinoxMessage", "N")
	
	--First time, play the cutscene.
	if(iSawEquinoxMessage == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSawEquinoxMessage", "N", 1.0)
		
		--Movement.
		fnCutscene([[ AL_SetProperty("Music", "NULL") ]])
		fnCutsceneMove("Christine", 23.25, 18.50)
		fnCutsceneFace("Christine", -1, 0)
		fnCutsceneMove("Tiffany", 24.25, 18.50)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Scene.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "PDU") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "PDU", "Neutral") ]])
		fnCutscene([[ Append("PDU:[E|Question] Unit 771852, I am picking up an erratic signal.[P] Would you like it played?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|PDU] Go ahead, PDU.") ]])
		fnCutsceneBlocker()
		
		--Scene, changes music.
		fnCutscene([[ AL_SetProperty("Music", "EquinoxTheme") ]])
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "PDU", "Neutral") ]])
		fnCutscene([[ Append("Golem:[VOICE|Golem] 'This is Unit 618285, transmitting across all frequencies![P] Something has happened at the Equinox facility![P] Everything is crazy -[P] they're all dead![P] Some unit, please send help![P] Plea - '[B][C]") ]])
		fnCutscene([[ Append("PDU:[E|Quiet] ...[P] It seems the transmission has ended there.[B][C]") ]])
		fnCutscene([[ Append("PDU:[E|Quiet] A jamming signal is currently blocking my radio receivers.[P] Its activation coincides with the transmission cutting out.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] Long-range radio usage is unauthorized at the moment.[P] The message boards have been complaining about it extensively.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] So you think the administrators blocked the signal because it was unauthorized?[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] No, I do not.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Offended] Good, because it'd be insane to block a distress call![B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] That is, then, why they blocked it.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Offended] You don't think - [P][CLEAR]") ]])
		fnCutscene([[ Append("55:[E|Neutral] I think it's suspicious, but am unconcerned.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Smirk] If what that unit said is correct, though, then the Equinox Laboratories will have a reduced security footprint.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Smirk] We may be able to acquire equipment there without scrutiny.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Offended] I'm sure the security teams are already on their way.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Smug] Oh?") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--55 Moves forward.
		fnCutsceneMove("Tiffany", 24.25, 19.50)
		fnCutsceneMove("Tiffany", 20.25, 19.50)
		fnCutsceneFace("Tiffany", 0, -1)
		fnCutsceneBlocker()
		fnCutsceneFace("GolemAA", 0, 1)
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Scene.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
		fnCutscene([[ Append("55:[E|Neutral] Unit.[P] Report.[B][C]") ]])
		fnCutscene([[ Append("Golem: (Eek![P] A command unit!)[B][C]") ]])
		fnCutscene([[ Append("Golem: All systems are nominal, Command Unit![P] Sector 15 is performing at peak efficiency![B][C]") ]])
		fnCutscene([[ Append("Golem: *You know, despite all the garbage laying around.*[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] Are you sure about that?[P] You haven't heard any distress signals?[B][C]") ]])
		fnCutscene([[ Append("Golem: Erm, I don't mean to have overheard, but I believe that Lord Unit's PDU mentioned something about one.[B][C]") ]])
		fnCutscene([[ Append("Golem: But my PDU is clear.[P] Are you experiencing a glitch, perhaps?") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--55 Moves forward.
		fnCutsceneMove("Tiffany", 21.25, 19.50)
		fnCutsceneMove("Tiffany", 21.25, 17.50)
		fnCutsceneBlocker()
		fnCutsceneFace("GolemAA", 1, 0)
		fnCutsceneWait(45)
		fnCutsceneBlocker()
		
		--Scene.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
		fnCutscene([[ Append("55:[E|Neutral] As expected, there is nothing in the security logs, either.[B][C]") ]])
		fnCutscene([[ Append("Golem: Erm, am I in trouble?[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] You are dismissed.[P] Back to work, unit.[B][C]") ]])
		fnCutscene([[ Append("Golem: Right away, Command Unit![P] (Phew!)") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Movement.
		fnCutsceneFace("GolemAA", -1, 0)
		fnCutsceneMove("Tiffany", 22.25, 18.50)
		fnCutsceneFace("Tiffany", 1, 0)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "PDU", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
		fnCutscene([[ Append("Christine:[E|Offended] I don't understand.[P] Why wouldn't there be anything in the logs?[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] Because the administrators do not desire the signal to be heard or appear in the logs.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] ...[P][E|Offended] We have to stop them.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Offended] I will not allow another Cryogenics...[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] Good.[P] Let us proceed.[B][C]") ]])
		fnCutscene([[ Append("PDU:[E|Alarmed][EMOTION|Christine|PDU] If you are travelling to the Equinox Laboratories, exit via the southern airlock in Sector 15 and travel southwest over the surface.[B][C]") ]])
		fnCutscene([[ Append("PDU:[E|Happy] Oh, and have a nice day.[P] My algorithms keep pestering me to say that.") ]])
		fnCutsceneBlocker()
		fnCutscene([[ AL_SetProperty("Music", "Null") ]])
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Fold party.
		fnCutsceneBlocker()
		fnCutsceneMove("Tiffany", 23.25, 18.50)
		fnCutsceneBlocker()
		fnCutscene([[ AL_SetProperty("Music", "RegulusCity") ]])
		fnCutscene([[ AL_SetProperty("Fold Party") ]])
		fnCutsceneBlocker()

	end
end
