-- |[Root]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Set facing.
	TA_SetProperty("Face Character", "PlayerEntity")
	
	--Flag.
	local iCassandraSpokenTo = VM_GetVar("Root/Variables/Chapter5/Scenes/iCassandraSpokenTo", "N")
	
	--First time speaking to Cassandra:
	if(iCassandraSpokenTo == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iCassandraSpokenTo", "N", 1.0)
	
		--Speak.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Golem") ]])
		fnCutscene([[ Append("Cassandra: Unit 771852![P] I am glad you are well![B][C]") ]])
		fnCutscene([[ Append("Christine: Greetings Unit 771853.[P] Have you received a function yet?[B][C]") ]])
		fnCutscene([[ Append("Cassandra: I have.[P] I have been assigned oversight of cleanup here in Sector 15, as well as general efficiency improvements.[B][C]") ]])
		fnCutscene([[ Append("Cassandra: It seems no Lord Unit was previously assigned.[P] I relish the challenge of the task.[B][C]") ]])
		fnCutscene([[ Append("Christine: Good for you![P] I'm sure you'll make things very productive around here.[B][C]") ]])
		fnCutscene([[ Append("Cassandra: What of you?[P] Why are you visiting this sector?[B][C]") ]])
		fnCutscene([[ Append("Christine: Just passing through.[B][C]") ]])
		fnCutscene([[ Append("Christine: Say, you haven't remembered anything about how you got to Regulus City, have you?[B][C]") ]])
		fnCutscene([[ Append("Cassandra: No.[P] What few organic memories I have were repurposed for my new function.[B][C]") ]])
		fnCutscene([[ Append("Cassandra: It is just as well.[P] As a human, I was purposeless.[P] Now, I will serve the Cause of Science.[B][C]") ]])
		fnCutscene([[ Append("Christine: As we all do.[P] Good luck, Unit 771853!") ]])
	
	--Repeats.
	else
	
		--Speak.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Golem") ]])
		fnCutscene([[ Append("Cassandra: Hmm.[P] I seem to have a vague memory of a white-haired -[P] not human.[P] She was a woman, but I'm not sure she was a human.[B][C]") ]])
		fnCutscene([[ Append("Christine: Old memories resurfacing?[B][C]") ]])
		fnCutscene([[ Append("Cassandra: They are not relevant.[P] I will ignore them.[B][C]") ]])
		fnCutscene([[ Append("Cassandra: I must serve the Cause of Science.[B][C]") ]])
		fnCutscene([[ Append("Christine: That is what we do.[P] See you later, Unit 771853.") ]])
	
	end
end
