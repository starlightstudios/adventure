-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end
AdvCombat_SetProperty("Party Slot", 1, "Tiffany")
AdvCombat_SetProperty("Party Slot", 2, "Null")
AdvCombat_SetProperty("Party Slot", 3, "Null")
AdvCombat_SetProperty("Party Slot", 4, "Null")
AdvCombat_SetProperty("Party Slot", 5, "Null")
AdvCombat_SetProperty("Party Slot", 6, "Null")

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Execution]|
--Exit
if(sObjectName == "ToRegulus15BWest" or sObjectName == "ToRegulus15BEast") then
	
	--Variables.
	local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
	local iIs55Following = VM_GetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N")
	
	--Christine needs to change form.
	if(sChristineForm ~= "Golem") then
	
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[VOICE|Christine](Better switch back to Golem.[P] Wouldn't want to draw unnecessary attention.)") ]])
		fnCutsceneBlocker()
	
		--Flashwhite.
		Cutscene_CreateEvent("Flash Christine White", "Actor")
			ActorEvent_SetProperty("Subject Name", "Christine")
			ActorEvent_SetProperty("Flashwhite Quickly", "Null")
		DL_PopActiveObject()
		fnCutsceneBlocker()

		fnCutsceneWait(75)
		fnCutsceneBlocker()
		fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua") ]])
		fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
		fnCutsceneBlocker()
	end
	
	--Force-facing flag. This will cause Christine to spawn with the correct facing.
	giForceFacing = gci_Face_South
	
	--Transition.
	fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
	if(sObjectName == "ToRegulus15BWest") then
		fnCutscene([[ AL_BeginTransitionTo("RegulusCity15B", "FORCEPOS:7.5x18.0x0") ]])
	else
		fnCutscene([[ AL_BeginTransitionTo("RegulusCity15B", "FORCEPOS:12.5x18.0x0") ]])
	end
	fnCutsceneBlocker()

-- |[Other Examinables]|
--Locked door.
elseif(sObjectName == "LockedDoor") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Christine](This door is locked.[P] I have no need to go this way.)") ]])
	fnCutsceneBlocker()

--Conversion Tube.
elseif(sObjectName == "ConversionTube") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Christine](A conversion tube, much like the one that brought me to mechanical perfection.[P] It is currently operational and awaiting a subject.)") ]])
	fnCutsceneBlocker()

--Terminal A.
elseif(sObjectName == "TerminalA") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Christine](A computer terminal.[P] Looks like someone has been accessing area schematics recently.)") ]])
	fnCutsceneBlocker()

--Terminal B.
elseif(sObjectName == "TerminalB") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Christine](A computer terminal.[P] The most recent logs are database queries on Regulus City and Golems.[P] Odd.)") ]])
	fnCutsceneBlocker()

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
