-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[ ======================================== Triggers ======================================== ]|
if(sObjectName == "WarpActivate") then
    fnWarpUnlockHandler("Root/Variables/Chapter5/Campfires/iRegulusCity15C", 32.25, 13.50)
    
--Cassandra flees from the party.
elseif(sObjectName == "CassandraNTrigger") then
	
	--Variables.
	local iTookCassandraQuest     = VM_GetVar("Root/Variables/Chapter5/Scenes/iTookCassandraQuest", "N")
	local iResolvedCassandraQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iResolvedCassandraQuest", "N")
	local iCassandraRanOff        = VM_GetVar("Root/Variables/Chapter5/Scenes/iCassandraRanOff", "N")
	local sChristineForm          = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
	
	--Play cutscene.
	if(iTookCassandraQuest == 1.0 and iResolvedCassandraQuest == 0.0 and iCassandraRanOff == 0.0) then
		
		--If Christine is currently a non-human...
		if(sChristineForm ~= "Human") then
			
			--Flag.
			VM_SetVar("Root/Variables/Chapter5/Scenes/iCassandraRanOff", "N", 1.0)
			
			--Close the door at the south.
			AL_SetProperty("Close Door", "DoorMiddleS")
			
			--Cassandra sees the party...
			fnCutsceneFace("Cassandra", 0, -1)
			fnCutsceneWait(25)
			fnCutsceneBlocker()
			
			--Dialogue.
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
            
            --Golem, Latex Drone, Steam Droid:
            if(sChristineForm == "Golem" or sChristineForm == "LatexDrone" or sChristineForm == "SteamDroid" or sChristineForm == "Secrebot") then
                fnCutscene([[ Append("Lady:[E|Neutral] Eek![P] More robots![B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Sad] Hey![P] Wait!") ]])
            elseif(sChristineForm == "Doll") then
                fnCutscene([[ Append("Lady:[E|Neutral] Eek![P] An incredibly attractive robot![B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Sad] Wait![P] Don't run![P] I want to repay the compliment!") ]])
            elseif(sChristineForm == "Raiju") then
                fnCutscene([[ Append("Lady:[E|Neutral] Eek![P] A zappy monster![B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Sad] Wait![P] I'm properly grounded!") ]])
            elseif(sChristineForm == "Eldritch") then
                fnCutscene([[ Append("Lady:[E|Neutral] Eek![P] An alien![B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Sad] Wait![P] No, actually you're right.[P] Really an alien, yep.") ]])
            else
                fnCutscene([[ Append("Lady:[E|Neutral] Eek![P] A monster![B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Sad] Hey![P] Wait!") ]])
            end
			fnCutsceneBlocker()
			fnCutsceneWait(25)
			fnCutsceneBlocker()
		
			--Cassandra runs off via the southern route.
			fnCutsceneFace("Christine", 0, 1)
			fnCutsceneFace("Tiffany", 0, 1)
			fnCutsceneMove("Cassandra", 17.25, 22.50, 2.50)
			fnCutsceneMove("Cassandra", 17.25, 23.50, 2.50)
			fnCutsceneBlocker()
			fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
			fnCutscene([[ AL_SetProperty("Open Door", "DoorMiddleS") ]])
			fnCutsceneWait(25)
			fnCutsceneBlocker()
			fnCutsceneMove("Cassandra", 17.25, 25.50, 2.50)
			fnCutsceneMove("Cassandra", 24.25, 25.50, 2.50)
			fnCutsceneMove("Cassandra", 24.25, 23.50, 2.50)
			fnCutsceneMove("Cassandra", 32.25, 23.50, 2.50)
			fnCutsceneBlocker()
			fnCutsceneTeleport("Cassandra", -100.25, -100.50)
			fnCutsceneBlocker()
			fnCutsceneWait(25)
			fnCutsceneBlocker()
			
			--Dialogue.
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
			fnCutscene([[ Append("Christine:[E|Sad] Wow, she's really fast![B][C]") ]])
            
            --Robots:
            if(sChristineForm == "Golem" or sChristineForm == "LatexDrone" or sChristineForm == "SteamDroid" or sChristineForm == "Secrebot") then
                fnCutscene([[ Append("Christine:[E|Neutral] What do you think she meant by 'more robots'?[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] We're robots, Christine.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] No, I mean, do you think some other units were sent down here to catch her?[B][C]") ]])
            elseif(sChristineForm == "Doll") then
                fnCutscene([[ Append("Christine:[E|Neutral] Why would she run?[P] Are we not amazingly cute?[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] I think that is beside the point.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] You think she had a bad experience growing up?[B][C]") ]])
            elseif(sChristineForm == "Raiju") then
                fnCutscene([[ Append("Christine:[E|Sad] I just want to cuddle...[B][C]") ]])
                fnCutscene([[ Append("55:[E|Upset] Diplomacy is not your forte.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Maybe she thinks we're here to catch her?[P] Why do you think she's so jumpy?[B][C]") ]])
            elseif(sChristineForm == "Eldritch") then
                fnCutscene([[ Append("Christine:[E|Sad] I had that one coming, didn't I.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] Do not blame yourself.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Can't blame her for being jumpy, either.[P] Do you think she's run into anyone else down here?[B][C]") ]])
            else
                fnCutscene([[ Append("Christine:[E|Neutral] I suppose it's my fault for looking like this, isn't it...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] She seemed pretty jumpy.[P] Do you think she's run into anyone else down here?[B][C]") ]])
            end
    
			fnCutscene([[ Append("55:[E|Neutral] Likely she encountered a maintenance unit, who probably put in the work order.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Hmm...[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] How are we going to get close to her?[P] She didn't even wait to hear us out.[B][C]") ]])
			fnCutscene([[ Append("55:[E|Smug] I imagine you'll think of something.") ]])
			fnCutsceneBlocker()
			fnCutsceneWait(25)
			fnCutsceneBlocker()
		
		--If Christine is currently a human...
		else
        
            --Achievement.
            AM_SetPropertyJournal("Unlock Achievement", "GolemCassandra")
			
			--Cassandra sees the party...
			fnCutsceneFace("Cassandra", 0, -1)
			fnCutsceneWait(25)
			fnCutsceneBlocker()
			
			--Dialogue.
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
			fnCutscene([[ Append("Lady:[E|Neutral] Eek![P] More robots![B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] I'm not a robot![P] Look![B][C]") ]])
			fnCutscene([[ Append("Lady:[E|Neutral] You...[P] aren't?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] May we come closer?[P] Please don't run away.") ]])
			fnCutsceneBlocker()
			fnCutsceneWait(25)
			fnCutsceneBlocker()
			
			--Movement.
			fnCutsceneMove("Christine", 16.25, 22.50)
			fnCutsceneMove("Tiffany", 17.25, 22.50)
			fnCutsceneBlocker()
			fnCutsceneFace("Cassandra", 1, 0)
			fnCutsceneFace("Christine", -1, 0)
			fnCutsceneFace("Tiffany", -1, 0)
			fnCutsceneBlocker()
			fnCutsceneWait(25)
			fnCutsceneBlocker()
			
			--Dialogue.
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
			fnCutscene([[ Append("Lady:[E|Neutral] Wait -[P] your friend...[P] Is she a robot?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Yes.[P] This is 2855.[P] She's a doll.[B][C]") ]])
			fnCutscene([[ Append("55:[E|Upset] Command unit...[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] But it's okay![P] She's my friend.[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] Call it what you will...[B][C]") ]])
			fnCutscene([[ Append("Lady:[E|Neutral] I didn't think I'd see another human down here...[B][C]") ]])
			fnCutscene([[ Append("Cassandra:[E|Neutral] My name is Cassandra.[P] Pleased to meet you...[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Happy] Christine Dormer![P] Charmed![B][C]") ]])
			fnCutscene([[ Append("Cassandra:[E|Neutral] I'm not really sure how I got here.[P] The last thing I remember, I was doing some reading at home, and then it all went black.[B][C]") ]])
			fnCutscene([[ Append("Cassandra:[E|Neutral] And now I'm in this creepy place, and there are robots coming after me...[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] You know, I can say much the same thing.[P] We're not sure how I got here either.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] (It seems like she trusts me now...[P] maybe I should convert her while her guard is down?)[BLOCK]") ]])

			--Decision script is this script. It must be surrounded by quotes.
			local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
			fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
			fnCutscene(" WD_SetProperty(\"Add Decision\", \"Convert Her\", " .. sDecisionScript .. ", \"Convert Her\") ")
			fnCutscene(" WD_SetProperty(\"Add Decision\", \"Lead Her to Safety\",  " .. sDecisionScript .. ", \"Lead Her to Safety\") ")
			fnCutsceneBlocker()
		
	
		end
	end
	
elseif(sObjectName == "CassandraSTrigger") then
	
	--Variables.
	local iTookCassandraQuest     = VM_GetVar("Root/Variables/Chapter5/Scenes/iTookCassandraQuest", "N")
	local iResolvedCassandraQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iResolvedCassandraQuest", "N")
	local iCassandraRanOff        = VM_GetVar("Root/Variables/Chapter5/Scenes/iCassandraRanOff", "N")
	local sChristineForm          = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
	
	--Play cutscene.
	if(iTookCassandraQuest == 1.0 and iResolvedCassandraQuest == 0.0 and iCassandraRanOff == 0.0) then
		
		--If Christine is currently a non-human...
		if(sChristineForm ~= "Human") then
			
			--Flag.
			VM_SetVar("Root/Variables/Chapter5/Scenes/iCassandraRanOff", "N", 1.0)
			
			--Close the door at the north.
			AL_SetProperty("Close Door", "DoorMiddleN")
			
			--Cassandra sees the party...
			fnCutsceneFace("Cassandra", 0, 1)
			fnCutsceneWait(25)
			fnCutsceneBlocker()
			
			--Dialogue.
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
            
            --Golem, Latex Drone, Steam Droid:
            if(sChristineForm == "Golem" or sChristineForm == "LatexDrone" or sChristineForm == "SteamDroid") then
                fnCutscene([[ Append("Lady:[E|Neutral] Eek![P] More robots![B][C]") ]])
            else
                fnCutscene([[ Append("Lady:[E|Neutral] Eek![P] A monster![B][C]") ]])
            end
			fnCutscene([[ Append("Christine:[E|Neutral] Hey![P] Wait!") ]])
			fnCutsceneBlocker()
			fnCutsceneWait(25)
			fnCutsceneBlocker()
		
			--Cassandra runs off via the southern route.
			fnCutsceneFace("Christine", 0, -1)
			fnCutsceneFace("Tiffany", 0, -1)
			fnCutsceneMove("Cassandra", 15.25, 21.50, 2.50)
			fnCutsceneMove("Cassandra", 17.25, 21.50, 2.50)
			fnCutsceneMove("Cassandra", 17.25, 20.50, 2.50)
			fnCutsceneBlocker()
			fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
			fnCutscene([[ AL_SetProperty("Open Door", "DoorMiddleN") ]])
			fnCutsceneWait(25)
			fnCutsceneBlocker()
			fnCutsceneMove("Cassandra", 17.25, 8.50, 2.50)
			fnCutsceneBlocker()
			fnCutsceneTeleport("Cassandra", -100.25, -100.50)
			fnCutsceneBlocker()
			fnCutsceneWait(25)
			fnCutsceneBlocker()
			
			--Dialogue.
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Wow, she's really fast![B][C]") ]])
            
            --Golem, Latex Drone, Steam Droid:
            if(sChristineForm == "Golem" or sChristineForm == "LatexDrone" or sChristineForm == "SteamDroid") then
                fnCutscene([[ Append("Christine:[E|Neutral] What do you think she meant by 'more robots'?[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] We're robots, Christine.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] No, I mean, do you think some other units were sent down here to catch her?[B][C]") ]])
            else
                fnCutscene([[ Append("Christine:[E|Neutral] I suppose it's my fault for looking like this, isn't it...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] She seemed pretty jumpy.[P] Do you think she's run into anyone else down here?[B][C]") ]])
            end
			fnCutscene([[ Append("55:[E|Neutral] Likely she encountered a maintenance unit, who probably put in the work order.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Hmm...[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] How are we going to get close to her?[P] She didn't even wait to hear us out.[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] I imagine you'll think of something.") ]])
			fnCutsceneBlocker()
			fnCutsceneWait(25)
			fnCutsceneBlocker()
		
		--If Christine is currently a human...
		else
        
            --Achievement.
            AM_SetPropertyJournal("Unlock Achievement", "GolemCassandra")
			
			--Cassandra sees the party...
			fnCutsceneFace("Cassandra", 0, 1)
			fnCutsceneWait(25)
			fnCutsceneBlocker()
			
			--Dialogue.
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
			fnCutscene([[ Append("Lady:[E|Neutral] Eek![P] More robots![B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] I'm not a robot![P] Look![B][C]") ]])
			fnCutscene([[ Append("Lady:[E|Neutral] You...[P] aren't?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] May we come closer?[P] Please don't run away.") ]])
			fnCutsceneBlocker()
			fnCutsceneWait(25)
			fnCutsceneBlocker()
			
			--Movement.
			fnCutsceneMove("Christine", 16.25, 22.50)
			fnCutsceneMove("Tiffany", 17.25, 22.50)
			fnCutsceneBlocker()
			fnCutsceneFace("Cassandra", 1, 0)
			fnCutsceneFace("Christine", -1, 0)
			fnCutsceneFace("Tiffany", -1, 0)
			fnCutsceneBlocker()
			fnCutsceneWait(25)
			fnCutsceneBlocker()
            
			--Dialogue.
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
			fnCutscene([[ Append("Lady:[E|Neutral] Wait -[P] your friend...[P] Is she a robot?[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] Yes.[P] This is 2855.[P] She's a doll.[B][C]") ]])
			fnCutscene([[ Append("55:[E|Upset] Command unit...[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] But it's okay![P] She's my friend.[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] Call it what you will...[B][C]") ]])
			fnCutscene([[ Append("Lady:[E|Neutral] I didn't think I'd see another human down here...[B][C]") ]])
			fnCutscene([[ Append("Cassandra:[E|Neutral] My name is Cassandra.[P] Pleased to meet you...[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Happy] Christine Dormer![P] Charmed![B][C]") ]])
			fnCutscene([[ Append("Cassandra:[E|Neutral] I'm not really sure how I got here.[P] The last thing I remember, I was doing some reading at home, and then it all went black.[B][C]") ]])
			fnCutscene([[ Append("Cassandra:[E|Neutral] And now I'm in this creepy place, and there are robots coming after me...[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] You know, I can say much the same thing.[P] We're not sure how I got here either.[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] (It seems like she trusts me now...[P] maybe I should convert her while her guard is down?)[BLOCK]") ]])

			--Decision script is this script. It must be surrounded by quotes.
			local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
			fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
			fnCutscene(" WD_SetProperty(\"Add Decision\", \"Convert Her\", " .. sDecisionScript .. ", \"Convert Her\") ")
			fnCutscene(" WD_SetProperty(\"Add Decision\", \"Lead Her to Safety\",  " .. sDecisionScript .. ", \"Lead Her to Safety\") ")
			fnCutsceneBlocker()
		
	
		end
	end

--Convert Cassandra.
elseif(sObjectName == "Convert Her") then
	
	--Flag.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iResolvedCassandraQuest", "N", 1.0)
	
	--Spawn a converted Cassandra. This will take her place for ease-of-use purposes.
	TA_Create("CassandraGolem")
		TA_SetProperty("Position", -100, -100)
		TA_SetProperty("Facing", gci_Face_South)
		TA_SetProperty("Clipping Flag", true)
		fnSetCharacterGraphics("Root/Images/Sprites/CassandraG/", false)
	DL_PopActiveObject()
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
	fnCutscene([[ Append("Cassandra:[E|Neutral] Do you think you can help me find my way home?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Smirk] Oh, easily![P] We'll make this your new home![B][C]") ]])
	fnCutscene([[ Append("Cassandra:[E|Neutral] H-[P]huh?") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Christine transforms.
	fnCutsceneWait(30)
	fnCutsceneBlocker()

	--Flash the active character to white. Immediately after, execute the transformation.
	Cutscene_CreateEvent("Flash Christine White", "Actor")
		ActorEvent_SetProperty("Subject Name", "Christine")
		ActorEvent_SetProperty("Flashwhite Quickly")
	DL_PopActiveObject()
	fnCutsceneWait(5)
	fnCutsceneBlocker()
	fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua") ]])
	fnCutsceneWait(gci_Flashwhite_Ticks_Total)
	fnCutsceneBlocker()

	--Now wait a little bit.
	fnCutsceneWait(30)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
	fnCutscene([[ Append("Cassandra:[E|Neutral] Y-[P]you were a robot in disguise![B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Smirk] Yes![P] And now, you will be too...[B][C]") ]])
	fnCutscene([[ Append("Cassandra:[E|Neutral] Eeeek![B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Offended] Stop fussing, you'll like it!") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Fade to black.
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneBlocker()
	
	--Scene.
	LM_ExecuteScript(gsRoot .. "Chapter 5/Scenes/500 Normal/Cassandra to Golem/Scene_Begin.lua")
	fnCutsceneBlocker()
	
	--Transport the party over to the golem tube.
	AL_SetProperty("Open Door", "ConversionDoor")
	fnCutsceneTeleport("Christine", 23.25, 13.50)
	fnCutsceneTeleport("Tiffany", 23.25, 14.50)
	fnCutsceneTeleport("CassandraGolem", 22.25, 13.50)
	fnCutsceneTeleport("Cassandra", -100.25, -100.50)
	fnCutsceneBlocker()
	fnCutsceneFace("Christine", -1, 0)
	fnCutsceneFace("Tiffany", -1, 0)
	fnCutsceneFace("CassandraGolem", 1, 0)
	fnCutsceneBlocker()
	
	--Unfade.
	fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
	fnCutsceneBlocker()
	fnCutsceneWait(105)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Happy") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
	fnCutscene([[ Append("Cassandra:[E|Golem] Proceeding to function assignment...[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Happy] Eee![P] You're so pretty as a golem![P] I'm so proud of you![B][C]") ]])
	fnCutscene([[ Append("Cassandra:[E|Golem] Proceeding to function assignment...") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Cassandra leaves.
	fnCutsceneMove("CassandraGolem", 22.25, 17.50)
	fnCutsceneMove("CassandraGolem", 8.25, 17.50)
	fnCutsceneFace("Christine", -1, 1)
	fnCutsceneFace("Tiffany", -1, 1)
	fnCutsceneBlocker()
	fnCutsceneTeleport("CassandraGolem", -100.25, -100.50)
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] She wasn't all that talkative, was she?[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] Initial function assignment takes priority over everything else.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Smirk] Unit 771852, I am legitimately impressed.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Oh?[B][C]") ]])
	fnCutscene([[ Append("55:[E|Smirk] You used the resources at hand to isolate and convert a rogue human.[P] You didn't allow emotion to cloud your judgement.[P] You did not hesitate.[P] Good work.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Smirk] Oh 55![P] I allowed emotions to guide me, and they helped![B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Blush] Can you just imagine it?[P] Thousands, millions of humans being converted...[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Blush] With their combined scientific output, we will plumb the mysterious depths of the universe...[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] What are you talking about?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Blush] Someday, we will return to Earth, where I am from, and we will bring mechanical perfection to everyone there.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] I didn't - [P][CLEAR]") ]])
	fnCutscene([[ Append("Christine:[E|Blush] Mmmmm, it's exciting to fantasize about...[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Smirk] But to bring about this beautiful future, we must focus on the present.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Smirk] Let's get going.[P] I can't wait to mechanize more humans![B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] (She's actually kind of off-putting when she's like this...)") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Fold the party.
	fnCutsceneMove("Tiffany", 23.25, 13.50)
	fnCutsceneBlocker()
	fnCutscene([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()

--Lead Cassandra to safety.
elseif(sObjectName == "Lead Her to Safety") then
	
    --Variables.
    local iMetJX101 = VM_GetVar("Root/Variables/Chapter5/Scenes/iMetJX101", "N")
    
	--Flag.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iResolvedCassandraQuest", "N", 2.0)
	
	--Close doors.
	AL_SetProperty("Close Door", "DoorMiddleN")
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "PDU", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
	fnCutscene([[ Append("Cassandra:[E|Neutral] Do you think you can help me find my way home?[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Smirk] Mmm, do you even know where home is?[B][C]") ]])
	fnCutscene([[ Append("Cassandra:[E|Neutral] ...[P] No...[P] I can't remember...[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] It's possible that she escaped from the biological research facility.[P] Or she was abducted and escaped from containment.[B][C]") ]])
	fnCutscene([[ Append("Cassandra:[E|Neutral] No, that's not right...[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] 55, where can we take her that will be safe until she remembers where she's from?[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] Tch, nowhere in Regulus City.[P] She will be hunted down by security units so long as she remains here.[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] I suppose the Steam Droids would probably take her in.[P] She is an agitator, and they like those.[B][C]") ]])
    if(iMetJX101 == 0.0) then
        fnCutscene([[ Append("Christine:[E|Neutral] The Steam Droids?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Out-of-date models that live in the Tellurium Mines.[P] They could provide sanctuary for Cassandra.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Plus it would likely ingratiate us with them.[P] They -[P] are not keen on Command Units like me, but if I was seen aiding their allies...[B][C]") ]])
    
    else
        fnCutscene([[ Append("Christine:[E|Smirk] Great idea![P] I'm sure JX-101 would love to have you![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Though you'll need to work, but I'm sure there's something you can do in Sprocket City.[B][C]") ]])
        fnCutscene([[ Append("Cassandra:[E|Neutral] Who's JX-101?[P] Another robot girl?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Yes, and another good friend of mine.[P] You can trust her.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Given the nature of the Tellurium Mines, I would recommend against sending her to Sprocket City.[P] There are other, smaller settlements that will be easier to direct her to.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] I have uploaded the data to your PDU.[P] There will be some risks involved.[B][C]") ]])
    end
	fnCutscene([[ Append("Christine:[E|PDU] Yeah, not a safe path to Sprocket City...[P][EMOTION|Christine|Smirk] This settlement here is closer...[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Smirk] How does that sound, Cassandra?[B][C]") ]])
	fnCutscene([[ Append("Cassandra:[E|Neutral] I suppose it's better than fleeing whenever a door opens...[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] PDU, please chart a course to the Tellurium Mines for Cassandra to follow.[P] Please avoid areas that are depressurized.[B][C]") ]])
	fnCutscene([[ Append("Cassandra:[E|Neutral] Chart a course..?[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] We will not be able to accompany you.[P] We will provide you a map to follow.[P] It should lead you to safety.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] Why can't we go with her?[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] The more of us there are, the more likely we will be detected.[P] It is best that she go alone.[B][C]") ]])
	fnCutscene([[ Append("Cassandra:[E|Neutral] Alone...[B][C]") ]])
	fnCutscene([[ Append("Cassandra:[E|Neutral] I'm sure I can make it.[P] I'll be fine.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Sad] I'm sorry, Cassandra...[B][C]") ]])
	fnCutscene([[ Append("Cassandra:[E|Neutral] It's all right.[P] Thank you for your help, Christine.[P] And yours, 55.[B][C]") ]])
	fnCutscene([[ Append("PDU:[E|Happy] Map printout complete.[P] Have a nice day![B][C]") ]])
	fnCutscene([[ Append("Cassandra:[E|Neutral] So I just need to crawl through those vents, and run along that unused section of track?[B][C]") ]])
	fnCutscene([[ Append("Cassandra:[E|Neutral] Please come visit me if you can, Christine.[P] I don't know how long until my memory returns, but if there's anything I can do for you, don't hesitate to ask.[B][C]") ]])
	fnCutscene([[ Append("Christine:[E|Smirk] Okay![P] Good luck, Cassandra!") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Cassandra walks off.
	fnCutsceneMove("Cassandra", 17.25, 22.50)
	fnCutsceneMove("Cassandra", 17.25, 20.50)
    if(AL_GetProperty("Is Door Open", "DoorMiddleN") == false) then
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutscene([[ AL_SetProperty("Open Door", "DoorMiddleN") ]])
        fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
        fnCutsceneBlocker()
    end
	fnCutsceneMove("Cassandra", 17.25, 8.50)
	fnCutsceneBlocker()
    fnCutsceneTeleport("Cassandra", -100.25, -100.50)
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
	fnCutscene([[ Append("Christine:[E|Neutral] I hope she'll be okay...[B][C]") ]])
	fnCutscene([[ Append("55:[E|Neutral] Most likely.[B][C]") ]])
    if(iMetJX101 == 0.0) then
        fnCutscene([[ Append("55:[E|Smirk] The Steam Droids will take her in, they detest Regulus City and its regime.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] This should make them more receptive to requests for assistance.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] We did this to help Cassandra.[P] This was not a calculated strategic move![B][C]") ]])
        fnCutscene([[ Append("55:[E|Smug] You may justify it any way you like.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] You're justifying doing the right thing as something cold and logical.[P] That's no different than my way of justifying it.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] That is true.[B][C]") ]])
    else
        fnCutscene([[ Append("55:[E|Smirk] Merely mentioning your name will likely guarantee her asylum.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] I was more worried about the creatures in the mines.[P] There's a lot of them, and if she runs into one...[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] For an organic, she runs quickly.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] Guess there's no point in worrying, is there?[B][C]") ]])
        
    end
	fnCutscene([[ Append("Christine:[E|Smirk] Well, come on.[P] Let's go lie on the work completion form.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
    fnCutsceneMove("Tiffany", 16.25, 22.50)
	fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Fold Party") ]])
    fnCutsceneBlocker()
	
end
