-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
if(sObjectName == "ToRegulusCryoSouthG") then
    
    --Check form for vacuum proofing.
	local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
	if(sChristineForm == "Human" or sChristineForm == "Raiju") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Oxygen levels are dangerously low beyond this point, as the air seals weren't finished.[P] I should transform into something that can handle a vacuum.)") ]])
    else
        AudioManager_PlaySound("World|AutoDoorOpen")
        AL_BeginTransitionTo("RegulusCryoSouthG", "FORCEPOS:32.5x10.0x0")
    end
    
-- |[Examinables]|
elseif(sObjectName == "Tube") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A defragmentation chamber, it is currently not receiving power.)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "Terminal") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The terminal was wired up but isn't connected to the defragmentation chamber and has no logs on it.)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "RVD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The RVD is playing its test pattern.[P] It is not receiving any broadcast data.)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "Scanner") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A radio-relay node to be attached to the lab's structures to boost network access.)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "Computer") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A portable computer.[P] There are no entries on it discussing the strange fleshy growth here, it must have been left here before that happened.)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "Boxes") then

    local iSCryoPowerABoxes = VM_GetVar("Root/Variables/Chapter5/Scenes/iSCryoPowerABoxes", "N")
    if(iSCryoPowerABoxes == 0.0) then
        
        --Flags.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSCryoPowerABoxes", "N", 1.0)
        LM_ExecuteScript(gsItemListing, "Assorted Parts")
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Mechanical parts to be used in constructing this laboratory room.[P] Mostly electrical wiring and socket connectors.)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (There are some parts we could use here...)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] [SOUND|World|TakeItem](Got Assorted Parts x 1)") ]])
        fnCutsceneBlocker()
    
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Mechanical parts to be used in constructing this laboratory room.[P] Mostly electrical wiring and socket connectors.)") ]])
        fnCutsceneBlocker()
    end

elseif(sObjectName == "Furniture") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Furniture meant to be placed once the sector's construction is complete.)") ]])
    fnCutsceneBlocker()
    
-- |[Other]|
-- |[Debug]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
