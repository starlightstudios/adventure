-- |[Constructor]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "RegulusCryoPowerCoreA"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[Standard]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
    local iSCryoPowerCoreShutOff = VM_GetVar("Root/Variables/Chapter5/Scenes/iSCryoPowerCoreShutOff", "N")
    if(iSCryoPowerCoreShutOff == 0.0) then
        AL_SetProperty("Music", "PowerCore")
    else
        AL_SetProperty("Music", "Null")
    end
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation("RegulusCryoSouth")
	
	-- |[Spawn NPCS Here]|
	--NPCs set to spawn here will be available for post-exec cutscenes.

	-- |[Lights]|
    AL_SetProperty("Activate Lights")

-- |[Post-Exec]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	-- |[Spawn NPCs Here]|
	--If an entity needs to spawn after a post-exec cutscene, put them here.
    
    -- |[Parallel Scripts]|
    --Run this script which causes some lights to flicker periodically.
    Cutscene_HandleParallel("Remove", "Surge North")
    Cutscene_HandleParallel("Remove", "Surge South")
    Cutscene_HandleParallel("Create", "Light Flicker A", gsRoot .. "Maps/RegulusCryo/RegulusCryoPowerCoreA/LightFlickerA.lua")
    Cutscene_HandleParallel("Create", "Light Flicker B", gsRoot .. "Maps/RegulusCryo/RegulusCryoPowerCoreA/LightFlickerB.lua")
    Cutscene_HandleParallel("Create", "Light Flicker D", gsRoot .. "Maps/RegulusCryo/RegulusCryoPowerCoreA/LightFlickerD.lua")
    
    -- |[Lighting]|
    --Change the light colors if the core is off.
    local iSCryoPowerCoreShutOff = VM_GetVar("Root/Variables/Chapter5/Scenes/iSCryoPowerCoreShutOff", "N")
    if(iSCryoPowerCoreShutOff == 1.0) then
        AL_SetProperty("Modify Light Color", "LampLightA", 1, 0, 0, 1)
        AL_SetProperty("Modify Light Color", "LampLightB", 1, 0, 0, 1)
        AL_SetProperty("Modify Light Color", "LampLightC", 1, 0, 0, 1)
        AL_SetProperty("Modify Light Color", "LampLightD", 1, 0, 0, 1)
    end
end
