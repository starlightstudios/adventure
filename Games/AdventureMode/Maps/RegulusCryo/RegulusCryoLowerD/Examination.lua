-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Execution]|
--Exit ladder.
if(sObjectName == "ToLowerCryoA") then
	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("RegulusCryoLowerA", "FORCEPOS:77.0x41.0x0")
	
--Unpowered door.
elseif(sObjectName == "ToLowerCryoE") then

	--Variables.
    local iMet55            = VM_GetVar("Root/Variables/Chapter5/Scenes/iMet55", "N")
	local iFixedLowerDDoor  = VM_GetVar("Root/Variables/Chapter5/Scenes/iFixedLowerDDoor", "N")
	local iReceivedFunction = VM_GetVar("Root/Variables/Chapter5/Scenes/iReceivedFunction", "N")
	
	--If the door is already fixed:
	if(iFixedLowerDDoor == 1.0) then
        AudioManager_PlaySound("World|AutoDoorOpen")
        AL_BeginTransitionTo("RegulusCryoPowerCoreE", "FORCEPOS:14.0x25.0x0")

	--Door isn't fixed yet.
	else
		
		--Doesn't have golem form:
		if(iMet55 == 0.0) then
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ Append("Chris: [VOICE|Leader](The door doesn't open or even respond...)[B][C]") ]])
			fnCutscene([[ Append("PDU: This door is not receiving power.[P] The transmission circuit was likely cut.[B][C]") ]])
			fnCutscene([[ Append("Chris: [VOICE|Leader]Can you fix it?[B][C]") ]])
			fnCutscene([[ Append("PDU: Unfortunately that would require more than a simple circuit bridging.[P] A repair unit would need to attend to the issue.[B][C]") ]])
			fnCutscene([[ Append("Chris: [VOICE|Leader](Guess I'm not going this way, then...)") ]])
		
		--Has golem form, has not been to Regulus City yet:
		elseif(iMet55 == 1.0 and iReceivedFunction == 0.0) then
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ Append("771852: [VOICE|Leader](This door doesn't appear on the local area map, and is not receiving power.)[B][C]") ]])
			fnCutscene([[ Append("771852: [VOICE|Leader](It is therefore not related to my programming.[P] I should proceed to Regulus City to receive a function assignment.)") ]])
		
		else
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ Append("Christine: [VOICE|Leader](This door isn't on the local area map, and isn't receiving power.)[B][C]") ]])
			fnCutscene([[ Append("Christine: [VOICE|Leader]PDU, short-range ultraviolet scan.[P] What's wrong with this door?[B][C]") ]])
			fnCutscene([[ Append("PDU: This door is not receiving power.[P] The transmission circuit was likely cut.[B][C]") ]])
			fnCutscene([[ Append("Christine: [VOICE|Leader]Show me a schematic of the circuits.[B][C]") ]])
			fnCutscene([[ Append("Christine: [VOICE|Leader]...[B][C]") ]])
			fnCutscene([[ Append("Christine: [VOICE|Leader]Crud, looks like I can't fix it from this side.[B][C]") ]])
			fnCutscene([[ Append("55: [VOICE|Tiffany]You don't have to repair everything you come across.[B][C]") ]])
			fnCutscene([[ Append("Christine: [VOICE|Leader]Yes I do, I'm a repair unit![B][C]") ]])
			fnCutscene([[ Append("Christine: [VOICE|Leader]But more importantly, there might be something in there we can use.[B][C]") ]])
			fnCutscene([[ Append("55: [VOICE|Tiffany]The access way isn't on the schematics.[P] It's probably disused and unimportant.[B][C]") ]])
			fnCutscene([[ Append("Christine: [VOICE|Leader]Telling me I can't have it just makes me want it more![P] Argh![B][C]") ]])
			fnCutscene([[ Append("Christine: [VOICE|Leader]We'll have to find a way around, and it likely won't be in the Cryogenics Facility...") ]])
		
		end
	end

--Retired doll.
elseif(sObjectName == "DollA") then

	--Variable.
	local iHasPDUCryoRedCard = VM_GetVar("Root/Variables/Chapter5/Scenes/iHasPDUCryoRedCard", "N")

	--Common.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("PDU: Scanning::[P] Command Unit #383099.[P] Status::[P] Retired.[B][C]") ]])
	fnCutscene([[ Append("PDU: Likely cause of retirement::[P] Power cycling unit ruptured leading to a slow loss of power.[P] Subject gradually lost function, presumably over the course of several hours, after receiving a penetrating blow in the torso.[B][C]") ]])

	--Player already has the keycard:
	if(iHasPDUCryoRedCard == 1.0) then
		fnCutscene([[ Append("PDU: Subject's components have suffered acidic damage due to the cycler rupturing.[P] Ocular and motivator units are degraded severely.[B][C]") ]])
		fnCutscene([[ Append("PDU: Subject's authenticator chip appears to have been forcibly removed after retirement.[P] An incision in the back of the head post-dates the cycling unit's rupture by several days.") ]])
	else
        
        --If the player skipped talking to the intercom and came straight here to get the red card:
        local iIntercomState = VM_GetVar("Root/Variables/Chapter5/Scenes/iIntercomState", "N")
        if(iIntercomState == 2.0) then
            VM_SetVar("Root/Variables/Chapter5/Scenes/iIntercomState", "N", 40.0)
        else
            VM_SetVar("Root/Variables/Chapter5/Scenes/iIntercomState", "N", 4.0)
        end
        
        --Common:
		VM_SetVar("Root/Variables/Chapter5/Scenes/iHasPDUCryoRedCard", "N", 1.0)
		fnCutscene([[ Append("PDU: Subject's components have suffered acidic damage due to the cycler rupturing.[P] Ocular and motivator units are degraded severely.[B][C]") ]])
		fnCutscene([[ Append("PDU: Subject's authenticator chip appears to have been forcibly removed after retirement.[P] An incision in the back of the head post-dates the cycling unit's rupture by several days.[B][C]") ]])
		fnCutscene([[ Append("PDU: [SOUND|World|Keycard]The command unit was clutching a security-red keycard.[P] It has been added to this PDU's database.") ]])
	end
	fnCutsceneBlocker()
		
		
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
