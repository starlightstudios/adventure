-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--Special
local bPrintSpecialResolve = false

-- |[Exits]|
if(sObjectName == "ToRegulusCryoSouthBE") then
    AudioManager_PlaySound("World|ClimbLadder")
    AL_BeginTransitionTo("RegulusCryoSouthB", "FORCEPOS:38.0x18.0x0")
    
elseif(sObjectName == "ToRegulusCryoSouthBW") then
    AudioManager_PlaySound("World|ClimbLadder")
    AL_BeginTransitionTo("RegulusCryoSouthB", "FORCEPOS:20.0x33.0x0")

-- |[Examinables]|
--I could probably cleverly use the letter from the examinable to resolve the variable, but that would make it hell to read for
-- a non-coder.
elseif(sObjectName == "BatteryA") then

    --Variables.
    local iSCryoBatteryA = VM_GetVar("Root/Variables/Chapter5/Scenes/iSCryoBatteryA", "N")
    if(iSCryoBatteryA == 0.0) then
        local iSCryoBatteryPickups = VM_GetVar("Root/Variables/Chapter5/Scenes/iSCryoBatteryPickups", "N")
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSCryoBatteryPickups", "N", iSCryoBatteryPickups + 1.0)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSCryoBatteryA", "N", 1.0)
        bPrintSpecialResolve = true
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (I've already drained the power from this one.)") ]])
        fnCutsceneBlocker()
    end
        
elseif(sObjectName == "BatteryB") then

    --Variables.
    local iSCryoBatteryB = VM_GetVar("Root/Variables/Chapter5/Scenes/iSCryoBatteryB", "N")
    if(iSCryoBatteryB == 0.0) then
        local iSCryoBatteryPickups = VM_GetVar("Root/Variables/Chapter5/Scenes/iSCryoBatteryPickups", "N")
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSCryoBatteryPickups", "N", iSCryoBatteryPickups + 1.0)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSCryoBatteryB", "N", 1.0)
        bPrintSpecialResolve = true
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (I've already drained the power from this one.)") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "BatteryC") then

    --Variables.
    local iSCryoBatteryC = VM_GetVar("Root/Variables/Chapter5/Scenes/iSCryoBatteryC", "N")
    if(iSCryoBatteryC == 0.0) then
        local iSCryoBatteryPickups = VM_GetVar("Root/Variables/Chapter5/Scenes/iSCryoBatteryPickups", "N")
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSCryoBatteryPickups", "N", iSCryoBatteryPickups + 1.0)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSCryoBatteryC", "N", 1.0)
        bPrintSpecialResolve = true
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (I've already drained the power from this one.)") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "BatteryD") then

    --Variables.
    local iSCryoBatteryD = VM_GetVar("Root/Variables/Chapter5/Scenes/iSCryoBatteryD", "N")
    if(iSCryoBatteryD == 0.0) then
        local iSCryoBatteryPickups = VM_GetVar("Root/Variables/Chapter5/Scenes/iSCryoBatteryPickups", "N")
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSCryoBatteryPickups", "N", iSCryoBatteryPickups + 1.0)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSCryoBatteryD", "N", 1.0)
        bPrintSpecialResolve = true
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (I've already drained the power from this one.)") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "Columns") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Silcrete columns waiting to be placed.[P] Maybe we could scavenge these later.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Under") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (It seems a power cable runs through this concrete foundation.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "WhiteCrates") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (UV-Activated Silcrete.[P] Much like concrete, but using silicon, it has no outgassing and instead sets when exposed to UV lights.[P] It requires several layerings to be placed properly, as UV light doesn't penetrate very far, but it's extremely strong when finished.)") ]])
    fnCutsceneBlocker()

-- |[Other]|
-- |[Debug]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end

-- |[Special]|
--Indicates how many charges are needed.
if(bPrintSpecialResolve == true) then
    
    local iSCryoBatteryPickups = VM_GetVar("Root/Variables/Chapter5/Scenes/iSCryoBatteryPickups", "N")
    if(iSCryoBatteryPickups == 1.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] [SOUND|World|TakeItem](Got it![P] I think I'll need four more power supplies before I can jump the door.)") ]])
        fnCutsceneBlocker()
    elseif(iSCryoBatteryPickups == 2.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] [SOUND|World|TakeItem](Two down, three to go.)") ]])
        fnCutsceneBlocker()
    elseif(iSCryoBatteryPickups == 3.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] [SOUND|World|TakeItem](That's three.[P] Two more power supplies needed.)") ]])
        fnCutsceneBlocker()
    elseif(iSCryoBatteryPickups == 4.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] [SOUND|World|TakeItem](That's four, but I don't see any more batteries around this site.[P] Maybe there's another construction site?)") ]])
        fnCutsceneBlocker()
    end
end
