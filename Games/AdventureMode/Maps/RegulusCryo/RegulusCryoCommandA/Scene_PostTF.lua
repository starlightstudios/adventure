-- |[Post TF Scene]|
--Brief scene that plays post-Golem-TF. Repositions Christine. Also replaces her equipment.

--Black the screen out.
AL_SetProperty("Music", "Null")
AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1)

--Reposition Christine.
fnCutsceneTeleport("Christine", 16.25, 4.50)
fnCutsceneFace("Christine", 0, 1)
fnCutsceneBlocker()

--Fade in.
fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
fnCutsceneBlocker()
fnCutsceneWait(45)
fnCutsceneBlocker()

--Christine takes her first true steps.
fnCutsceneMove("Christine", 16.25, 5.50, 0.30)
fnCutsceneBlocker()

--Set the leader voice to Christine's.
WD_SetProperty("Set Leader Voice", "Christine")

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutscene([[ Append("771852:[E|Serious] (Initialize cognition routines...[P] done.)[B][C]") ]])
fnCutscene([[ Append("771852:[E|Serious] (Physical calibrations complete.[P] Spatial estimation matrix online.[P] Setting logical tables.[P] Secondary boot complete.)[B][C]") ]])
fnCutscene([[ Append("771852:[E|Serious] (Assignment::[P] Move to Regulus City.[P] Receive further programming.[P] Carry assignment out::[P] Priority Zero.)[B][C]") ]])
fnCutscene([[ Append("771852:[E|Serious] (The airlock on the western edge of this floor should lead me there.[P] After that, I must follow the light posts to Regulus City.)") ]])
fnCutsceneBlocker()
	
--Music resumes.
fnCutscene([[ AL_SetProperty("Music", "RegulusTense") ]])

-- |[ ======================= Equip Change ======================= ]|
--Give Christine her Lord Unit equipment.
local iGotGolemEquipment = VM_GetVar("Root/Variables/Global/Christine/iGotGolemEquipment", "N")
if(iGotGolemEquipment == 0.0) then
    
    -- |[Flags]|
    --Repeat flag.
	VM_SetVar("Root/Variables/Global/Christine/iGotGolemEquipment", "N", 1.0)
    
    -- |[Item Creation]|
    --Create new items.
    LM_ExecuteScript(gsItemListing, "Carbonweave Electrospear")
    
    --On NG+, don't give the player a duplicate.
    local iLordGolemDressCount = AdInv_GetProperty("Item Count", "Lord Golem Dress")
    if(iLordGolemDressCount < 1) then
        LM_ExecuteScript(gsItemListing, "Lord Golem Dress")
    end
    
    -- |[Equipment Changing]|
    --Weapon switching does not occur if this is NC+. This is done by checking if the weapon was a tazer, as the tazer
    -- does not get re-added to the inventory when NC+ begins and the player cannot switch weapons in a normal game.
    AdvCombat_SetProperty("Push Party Member", "Christine")
    
        --Switch the weapon out.
        local sWeapon = AdvCombatEntity_GetProperty("Equipment In Slot S", "Weapon")
        if(sWeapon == "Tazer") then
            
            --Unequip.
            AdvCombatEntity_SetProperty("Unequip Slot", "Weapon")
            AdvCombatEntity_SetProperty("Unequip Slot", "Weapon Backup A")
            AdvCombatEntity_SetProperty("Unequip Slot", "Weapon Backup B")
        
            --Equip new items.
            AdvCombatEntity_SetProperty("Equip Item To Slot", "Weapon", "Carbonweave Electrospear")
        
        --Weapon was not the tazer, indicating this is an NC+ playthrough.
        else
            
            --Hide the "Lancer" job.
            AdvCombatEntity_SetProperty("Push Job S", "Lancer")
                AdvCombatJob_SetProperty("Appears on Skills UI", false)
            DL_PopActiveObject()
        
        end
    
        --Switch the armor out. The player can unequip the suit if they really want, so the removal from the inventory takes place later.
        local sArmor = AdvCombatEntity_GetProperty("Equipment In Slot S", "Body")
        if(sArmor == "Schoolmaster's Suit") then
    
            --Unequip.
            AdvCombatEntity_SetProperty("Unequip Slot", "Body")
            
            --Equip new items.
            AdvCombatEntity_SetProperty("Equip Item To Slot", "Body", "Lord Golem Dress")
        end
    
        --Remove the old equipment.
        AdInv_SetProperty("Remove Item", "Tazer")
        AdInv_SetProperty("Remove Item", "Schoolmaster's Suit")
        
        --Hide the "Teacher" job. This happens in all cases.
        AdvCombatEntity_SetProperty("Push Job S", "Teacher")
            AdvCombatJob_SetProperty("Appears on Skills UI", false)
        DL_PopActiveObject()
        
        AdvCombatEntity_SetProperty("Rebuild Job Skills UI")
	DL_PopActiveObject()
    
    --Set Christine to the "Serious" costume. This changes her combat portrait.
    VM_SetVar("Root/Variables/Costumes/Christine/sCostumeGolem", "S", "Serious")
    LM_ExecuteScript(gsCostumeAutoresolve, "Christine_Golem")
end
