-- |[ ====================================== Examination ======================================= ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
        
-- |[ ========================================= Exits ========================================== ]|
--Exit ladder.
if(sObjectName == "ToCryoG") then
	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("RegulusCryoG", "FORCEPOS:12.0x12.0x0")

--Exit door.
elseif(sObjectName == "ToCryoCommandC") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusCryoCommandC", "FORCEPOS:5.5x8.0x0")
	
--Exit door.
elseif(sObjectName == "ToCryoCommandB") then

	--Variables.
	local iIntercomState = VM_GetVar("Root/Variables/Chapter5/Scenes/iIntercomState", "N")

	--No access.
	if(iIntercomState < 7.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[SOUND|World|AutoDoorFail]Access denied.[P] Only command staff are authorized in the command center.") ]])

	--Access granted.
	else
		AudioManager_PlaySound("World|AutoDoorOpen")
		AL_BeginTransitionTo("RegulusCryoCommandB", "FORCEPOS:10.0x20.0x0")
	end
	
--Exit ladder.
elseif(sObjectName == "ToCryoFabB") then
	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("RegulusCryoToFabricationB", "FORCEPOS:4.0x21.0x0")
	
--Door, toggles the floor light behind it on.
elseif(sObjectName == "DoorToggleLight") then
	AL_SetProperty("Enable Light", "ToggleLight")

-- |[ ======================================== Intercom ======================================== ]|
elseif(sObjectName == "Intercom") then

	LM_ExecuteScript(gsRoot .. "Chapter 5/Dialogue/Intercom/Root.lua", "Hello")
	
-- |[ ===================================== Other Objects ====================================== ]|
--Conversion Tube. Has different examination post-golem TF.
elseif(sObjectName == "ConversionTube") then

	--Variables.
    local iMet55 = VM_GetVar("Root/Variables/Chapter5/Scenes/iMet55", "N")

	--Not transformed into a golem.
	if(iMet55 == 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[VOICE|ChrisMaleVoice](A pod of some sort.[P] Unlike most of the other ones like it, this one still hums with power.)") ]])

	--Have Golem form.
	else
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[VOICE|Christine](The device which brought me to mechanical perfection...[P] Seems it's out of nanofluid, though.[P] I was lucky to get the last of it.)") ]])
	end

--Damage door.
elseif(sObjectName == "DoorC") then

    --Variables.
    local iMet55 = VM_GetVar("Root/Variables/Chapter5/Scenes/iMet55", "N")
	local iFixedCryoDoor = VM_GetVar("Root/Variables/Chapter5/Scenes/iFixedCryoDoor", "N")

	--Door is already fixed.
	if(iFixedCryoDoor == 1.0) then
		AL_SetProperty("Open Door", "DoorC")
		AudioManager_PlaySound("World|AutoDoorOpen")
	
	--Door needs repair.
	else

		--Get Christine's position.
		EM_PushEntity("Christine")
			local fXPos, fYPos = TA_GetProperty("Position")
		DL_PopActiveObject()

		--Christine is to the south of the door.
		if(fYPos >= 12.5 * 16.0) then
            
            --Male:
            if(iMet55 == 0.0) then
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ Append("[SOUND|World|AutoDoorFail] Error.[P] Door motivator damaged. Please contact a repair crew.[B][C]") ]])
                fnCutscene([[ Append("Chris:[VOICE|ChrisMaleVoice] Um, PDU?[B][C]") ]])
                fnCutscene([[ Append("PDU: Yes, Chris?[B][C]") ]])
                fnCutscene([[ Append("Chris:[VOICE|ChrisMaleVoice] Can you do anything about this door?[B][C]") ]])
                fnCutscene([[ Append("PDU: One moment...[B][C]") ]])
                fnCutscene([[ Append("PDU: The door appears to be unmotivated.[B][C]") ]])
                fnCutscene([[ Append("Chris:[VOICE|ChrisMaleVoice] ...[B][C]") ]])
                fnCutscene([[ Append("PDU: Hey, you're the one who didn't want to disable humour protocols.[B][C]") ]])
                fnCutscene([[ Append("PDU: I can bypass the circuit for you, but I will need to access the panel on the other side of the door.[B][C]") ]])
                fnCutscene([[ Append("Chris:[VOICE|ChrisMaleVoice] Lovely.") ]])
                fnCutsceneBlocker()
                
            --All others:
            else
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ Append("[SOUND|World|AutoDoorFail] Error.[P] Door motivator damaged.[P] Please contact a repair crew.[B][C]") ]])
                fnCutscene([[ Append("Christine: PDU, diagnostics.[B][C]") ]])
                fnCutscene([[ Append("PDU: One moment...[B][C]") ]])
                fnCutscene([[ Append("PDU: I can bypass the circuit, but only from the other side of the door.[B][C]") ]])
                fnCutscene([[ Append("Christine: Affirmative.[P] Thank you, PDU.") ]])
            
            end
		
		else
            
            --Male:
            if(iMet55 == 0.0) then
                VM_SetVar("Root/Variables/Chapter5/Scenes/iFixedCryoDoor", "N", 1.0)
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ Append("[SOUND|World|AutoDoorFail] Error.[P] Door motivator damaged.[P] Please contact a repair crew.[B][C]") ]])
                fnCutscene([[ Append("Chris:[VOICE|ChrisMaleVoice] PDU?[B][C]") ]])
                fnCutscene([[ Append("PDU: Please open the panel next to the door and hold me up to it.[B][C]") ]])
                fnCutscene([[ Append("PDU: ...[B][C]") ]])
                fnCutscene([[ Append("PDU: There.[P] The damaged circuit has been bypassed.[P] The motivator should work now.[B][C]") ]])
                fnCutscene([[ Append("Chris:[VOICE|ChrisMaleVoice] Thank you, PDU.[B][C]") ]])
                fnCutscene([[ Append("PDU: Please redirect your praise to Unit 7018, who programmed my repair routines.[P] Have an exceedingly nice day!") ]])

            --All others:
            else
                VM_SetVar("Root/Variables/Chapter5/Scenes/iFixedCryoDoor", "N", 1.0)
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ Append("[SOUND|World|AutoDoorFail] Error.[P] Door motivator damaged.[P] Please contact a repair crew.[B][C]") ]])
                fnCutscene([[ Append("Christine: PDU, activate circuit bridge tool.[B][C]") ]])
                fnCutscene([[ Append("PDU: Affirmative.[P] Please open the panel next to the door and hold me up to it.[B][C]") ]])
                fnCutscene([[ Append("PDU: ...[B][C]") ]])
                fnCutscene([[ Append("PDU: There.[P] The damaged circuit has been bypassed.[P] The motivator should work now.[B][C]") ]])
                fnCutscene([[ Append("Christine: Good work, PDU.[B][C]") ]])
                fnCutscene([[ Append("PDU: Please redirect your praise to Unit 7018, who programmed my repair routines.[P] Have an exceedingly nice day!") ]])
            end
		end
	end
	
--Left computer.
elseif(sObjectName == "ComputerL") then

	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: Scanning hard drive...[P] done.[P] Playing most recent log entry.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: Research Log of Unit 609144, day 1.[P] I have been reassigned to the cryogenics research facility, and am unsure why.[P] Supposedly a very rare subject was acquired by the abductions department, but every request I have put in for details was denied.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: How am I supposed to do research on a subject when I don't even know what it is?[P] What sort of chicanery is this?[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: I have put in my staffing requests with central and settled in.[P] The subject is due to arrive tomorrow and I intend to be at the transit bay to greet it.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: I am very excited to be granted such a rare subject, despite the secrecy.[P] I hope I can learn a great deal from it.[P] Signing off.") ]])
	fnCutsceneBlocker()
	
--Right computer.
elseif(sObjectName == "ComputerR") then

	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: Scanning hard drive...[P] done.[P] Playing most recent log entry.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: Research Log of Unit 609144, day 2.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: A series of statistically improbable events has occurred concerning the sample.[P] The tracks bent as it was due to arrive, power fluctuations and interference in the radio network alarmed the slave units...[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: My PDU started behaving erratically and needed a restart, and worst of all, the oil machine in my quarters gave me decaff.[P] Why would you ever drink decaff?[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: Still, despite the numerous setbacks, the exhilaration was palpable.[P] I've already organized a dozen initial experiments to begin as soon as possible.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: The future is bright indeed.[P] Signing off.") ]])
	fnCutsceneBlocker()
	
--Retired golem.
elseif(sObjectName == "GolemA") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("PDU: Scanning::[P] Unit #40219. Status::[P] Retired.[B][C]") ]])
	fnCutscene([[ Append("PDU: Likely cause of retirement::[P] Concussive grenade caused rapid compression of the CPU casing, causing severe damage.[P] Unit went offline instantaneously.") ]])
	fnCutsceneBlocker()
	
--Retired golem.
elseif(sObjectName == "GolemB") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("PDU: Scanning::[P] Unit #752119. Status::[P] Retired.[B][C]") ]])
	fnCutscene([[ Append("PDU: Likely cause of retirement::[P] Multiple pulse impacts to main chassis.[P] This unit was caught by a hail of fire and did not seek cover.") ]])
	fnCutsceneBlocker()
	
--Retired golem.
elseif(sObjectName == "GolemC") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("PDU: Scanning::[P] Unit #602314. Status::[P] Retired.[B][C]") ]])
	fnCutscene([[ Append("PDU: Likely cause of retirement::[P] Multiple pulse impacts to main chassis.[P] Unit was attempting to drag another unit to cover when she was struck repeatedly by pulse impacts.") ]])
	fnCutsceneBlocker()
	
--Retired golem.
elseif(sObjectName == "GolemD") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("PDU: Scanning::[P] Unit #729804. Status::[P] Retired.[B][C]") ]])
	fnCutscene([[ Append("PDU: Likely cause of retirement::[P] Unit was hit by a pulse diffractor in the ocular unit at close range.[P] System offline was instantaneous.") ]])
	fnCutsceneBlocker()

--Rebel Banners
elseif(sObjectName == "Banner") then

	--Variables.
	local iTalkedToSophie = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N")
    local iMet55 = VM_GetVar("Root/Variables/Chapter5/Scenes/iMet55", "N")
    
    --Male Chris:
    if(iMet55 == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (A cloth banner depicting the faceplate of a robot, broken in half.[P] The banner has been torn up by projectiles.)") ]])
    
    --Golem Christine, in Programmed Mode
    elseif(iMet55 == 1.0 and iTalkedToSophie == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (A cloth banner depicting the faceplate of a golem, broken in half.[P] The banner has been torn up by projectiles.[P] It is not relevant to my programming.)") ]])
        
    --After Regulus City:
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (A cloth banner depicting the faceplate of a golem, broken in half.[P] The banner has been torn up by projectiles.[P] Your retirement will not be in vain, rebel sister...)") ]])
    
    end

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
