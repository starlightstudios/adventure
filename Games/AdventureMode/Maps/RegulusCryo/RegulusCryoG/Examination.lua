-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Execution]|
--Exit door.
if(sObjectName == "ToCryoC") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusCryoC", "FORCEPOS:16.0x3.0x0")
	
--Exit door.
elseif(sObjectName == "ToContainmentA") then

    --Flag.
    local iUnlockContainment = VM_GetVar("Root/Variables/Chapter5/Scenes/iUnlockContainment", "N")
    if(iUnlockContainment == 1.0) then
        AudioManager_PlaySound("World|AutoDoorOpen")
        AL_BeginTransitionTo("RegulusCryoContainmentA", "FORCEPOS:20.5x21.0x0")
    else
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[SOUND|World|AutoDoorFail] Access denied.") ]])
		fnCutsceneBlocker()
    end

--Ladder.
elseif(sObjectName == "ToFabricationA") then
	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("RegulusCryoToFabricationA", "FORCEPOS:8.0x30.0x0")

--Ladder.
elseif(sObjectName == "ToCommandA") then
	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("RegulusCryoCommandA", "FORCEPOS:10.0x14.0x0")

--Light source. Pick it up if we don't already have a light.
elseif(sObjectName == "Lantern") then

	--Variables.
	local iHasLantern = VM_GetVar("Root/Variables/Chapter5/Scenes/iHasLantern", "N")
	
	--Player does not have the lantern yet.
	if(iHasLantern == 0.0) then

		--Flag.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iHasLantern", "N", 1.0)

		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("*Picked up an electric lantern.*[B][C]") ]])
		fnCutscene([[ Append("The lantern provides a mobile light source in dark areas.[P] Be wary, as enemies have no problem seeing you in the dark.") ]])
		fnCutsceneBlocker()
		
		--Activate the light. Remove the world object.
		fnCutscene([[ AL_SetProperty("Activate Player Light", 3600, 3600) ]])
		fnCutscene([[ AL_SetProperty("Disable Light", "LanternLight") ]])
		fnCutsceneTeleport("LanternNPC", -100,-100)
		fnCutsceneBlocker()
	end

--Intercom.
elseif(sObjectName == "Intercom") then
	LM_ExecuteScript(gsRoot .. "Chapter 5/Dialogue/Intercom/Root.lua", "Hello")

--EmptyTube.
elseif(sObjectName == "EmptyTube") then

	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: This research containment unit is currently empty.[P] The hard drive has been wiped of any useful data.") ]])
	fnCutsceneBlocker()

--ComputerRB.
elseif(sObjectName == "ComputerRB") then

	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: Scanning hard drive...[P] done.[P] Playing most recent log entry.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: Research Log of Unit 609144, day 29.[P] I find myself at a loss.[P] I am not sure how to continue my research down this avenue at the present time.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: I have reviewed several days of videographs from the subject.[P] I wrote several programs to help myself make sense of the chaos I was seeing, and found that the programs were holding me back.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: There is a sort-of coherency to the presence of the shapes, but when compared using the program I wrote, it suggests a quantum randomness.[P] This cannot be correct.[P] I can personally predict the next shape with over seventy percent accuracy.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: Unless it is my own CPU that is the cause, or perhaps an error in my program, I am left to assume that this chaotic garbage is random but not random.[P] This is not possible.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: I have asked a slave unit to review the footage.[P] She refused.[P] I then ordered her to review the footage.[P] She refused no matter what level of punishment I doled out.[P] It seems I am alone.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: There is no course of action other than to continue to record videographs and hope I can discern a pattern.[P] Signing off.") ]])
	fnCutsceneBlocker()
	
--ComputerM.
elseif(sObjectName == "ComputerM") then

	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: Scanning hard drive...[P] done.[P] Playing most recent log entry.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: Research Log of Unit 609144, day 27.[P] Effectively immediately, all radio equipment is hereby contraband.[P] I am sick of these radio signals![B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: At first I thought the radio signals may be related to the subject, but that was incorrect.[P] The signals predate the extraction of the orbs, so a delinquent unit must be the cause.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: The sheer amount of random interference on the intercoms and slave headsets has proven aggravating.[P] All slave units have been ordered to disable their headsets.[P] They will have to receive verbal commands from now on.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: I have sent my recommendations to central administration in the hopes that they will dispatch personnel to help find the cause.[P] I am a research unit, damn it![P] My function is to research phenomena, not police the slave units![B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: There has been no response from central administration yet.[P] It is entirely possible they will ignore this request as they have the others.[P] There is nothing more I can do.[P] Signing off.") ]])
	fnCutsceneBlocker()
	
--ComputerLR.
elseif(sObjectName == "ComputerLR") then

	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: Scanning hard drive...[P] done.[P] Playing most recent log entry.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: Research Log of Unit 609144, day 36.[P] Three slave golems are unaccounted for.[P] They are not reporting in when summoned and have not been seen by the other units.[P] The security cameras are no help, as usual.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: We've been experiencing more power outages, as well.[P] Our maintenance unit is strained enough with the demands we put on our equipment, and now she has to fix the power grid.[P] Without more personnel, we will never catch up.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: On a positive note, I believe I am near a breakthrough in the videographs of the subject's dreams.[P] I found myself staring at a live stream of the dreams for a few hours and suddenly realized I had guessed fifteen shapes correctly in a row.[P] Astounding![B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: In spite of our mechanical failures, the cause of science marches on.[P] I am excited about the coming ramifications of my research.[P] Signing off.") ]])
	fnCutsceneBlocker()

--ComputerLL.
elseif(sObjectName == "ComputerLL") then

	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: Scanning hard drive...[P] done.[P] Playing most recent log entry.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: Research Log of Unit 609144, day 39.[P] It is not often I am humbled by something, but the day has come.[P] At no point was the subject dreaming of geometric shapes. This I am now certain of.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: What the subject was dreaming of was sixteen-dimensional math of a vibrating membrane.[P] Without proper calibrations, these would look like random quantum fluctuations on an electron, but after I reconfigured my CPU it all became clear.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: If this is correct, the subject's mental capability far outstrip that of even central administration.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: Even more interesting was that, as I have now reconfigured my CPU, the pace of the shapes appearing has increased by about three times its original rate.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: Due to the limited bandwidth on communications with central administration, I have withheld sending the videographs.[P] I am eager to compile my findings, but want to follow this trail to its end.[P] Signing off.") ]])
	fnCutsceneBlocker()

--ComputerBUR.
elseif(sObjectName == "ComputerBUR") then

	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: Scanning hard drive...[P] done.[P] Playing most recent log entry.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: Research Log of Unit 609144, day 41.[P] Han droemmer om sagn-omspundne [REDACTED] hvor de doede guder slumrer.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: Hun er hans taleroer, og hun er en gave til de droemmende masser.[P] Hun vil vaagne, han vil vaagne naar den trettende time ringer, og dukkerne vil blive hans marionetter, da de allerede spiller ind i hans mangfoldige haender.[P] Signing off.") ]])
	fnCutsceneBlocker()

--ComputerBLR.
elseif(sObjectName == "ComputerBLR") then

	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: Scanning hard drive...[P] done.[P] Playing most recent log entry.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: Research Log of Unit 609144, day 42.[P] While I have no recollection of recording the previous log entry, it has my authorization stamp on it.[P] Perhaps it is a slave unit playing a prank?[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: This garbage is infuriating.[P] It seems so close to language but is not any of the spoken languages on Pandemonium.[P] Yet, somehow, I can vaguely understand what it is saying.[P] My CPU has been working hard trying to decipher it.[P] I need a rest.[P] Signing off.") ]])
	fnCutsceneBlocker()
	
--ComputerBUL.
elseif(sObjectName == "ComputerBUL") then

	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: Scanning hard drive...[P] done.[P] Playing most recent log entry.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: Research Log of Unit 609144, day 44.[P] There is certainly some aesthetic value to the videographs.[P] While pseudo-random, they are quite beautiful to observe.[P] I have also realized I have not seen a slave unit for the past three days.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: I called out but received no response.[P] The subject was moved to the containment area without my authorization, and as I went there I thought I saw a slave unit moving in the shadows.[P] There was no one there.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: What the slave units do is none of my concern.[P] The facility is quiet.[P] I am now standing near the containment unit that holds the subject.[P] She is cryogenically frozen within the unit, yet I can tell she is still dreaming of shapes.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: The vitals monitor just spiked.[P] A sudden increase in brain activity.[P] The intercom spewed static so loud I could hear it from here.[P] The subject -[P][P][P] is waking up.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: ...[P] Apologies, that appears to be the end of the entry.[P] Unit 609144 did not sign off on it.") ]])
	fnCutsceneBlocker()

--ComputerBLL.
elseif(sObjectName == "ComputerBLL") then

	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: Scanning hard drive...[P] error.[P] The hard drive has been deliberately wiped of data.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: Scans suggest that the drive received a short-range electromagnetic pulse directed at it.[P] The rest of the terminal was not affected.[P] This was a carefully directed blast.") ]])
	fnCutsceneBlocker()

    --Debug tool. [REMOVECHAPTER2]
    VM_SetVar("Root/Variables/Chapter5/Scenes/iUnlockContainment", "N", 1.0)

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
