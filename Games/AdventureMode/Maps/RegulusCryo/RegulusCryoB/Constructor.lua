-- |[ ======================================= Constructor ====================================== ]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = fnResolveDirectory() --Map's name is the name of the folder it is in.
local sLevelMusic = "RegulusTense"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[ ======================================== Standard ======================================== ]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

    -- |[Construction]|
	--Music.
	AL_SetProperty("Music", sLevelMusic)
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()

	-- |[Lights]|
    AL_SetProperty("Activate Lights")
	
	--It's possible that the player has no map. If so, grey out the map.
	local iReceivedMap = VM_GetVar("Root/Variables/Chapter5/Scenes/iReceivedMap", "N")
	if(iReceivedMap == 1.0) then
        fnCryogenicsMap(1551, 1158, 1485, 1020)
	end
	
	-- |[Spawn NPCS Here]|
	--NPCs set to spawn here will be available for post-exec cutscenes.
	local iHasPDU = VM_GetVar("Root/Variables/Chapter5/Scenes/iHasPDU", "N")
	if(iHasPDU == 1.0) then
	
	--Spawn an entity to represent the PDU.
	else
		TA_Create("PDUNPC")
			TA_SetProperty("Position", 3, 11)
			TA_SetProperty("Clipping Flag", false)
			for p = 1, 4, 1 do
				TA_SetProperty("Move Frame", 0, p-1, "Root/Images/Sprites/Objects/PDU")
				TA_SetProperty("Move Frame", 1, p-1, "Root/Images/Sprites/Objects/PDU")
				TA_SetProperty("Move Frame", 2, p-1, "Root/Images/Sprites/Objects/PDU")
				TA_SetProperty("Move Frame", 3, p-1, "Root/Images/Sprites/Objects/PDU")
			end
			TA_SetProperty("Facing", 0)
			TA_SetProperty("Set Ignore Special Lights", true)
		DL_PopActiveObject()
	end
	
-- |[ ======================================== Post-Exec ======================================= ]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	-- |[Spawn NPCs Here]|
	--If an entity needs to spawn after a post-exec cutscene, put them here.
	local iHasPDU = VM_GetVar("Root/Variables/Chapter5/Scenes/iHasPDU", "N")
	if(iHasPDU == 0.0) then
		
		--Position.
		local fXPos =  4.20
		local fYPos = 11.70
		fnCutsceneTeleport("PDUNPC", fXPos, fYPos)
		
		--Put a radial light on the PDU.
		AL_SetProperty("Register Radial Light", "PDULight", (fXPos * 16.0) - 8.0, (fYPos * 16.0) - 12.0, 256.0)
	end

end
