-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
--Force the player to pick up the PDU.
if(sObjectName == "NeedPDUTrigger") then

	--Variables.
	local iHasPDU = VM_GetVar("Root/Variables/Chapter5/Scenes/iHasPDU", "N")
	
	--Cutscene moves Chris back a bit.
	if(iHasPDU == 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[VOICE|Leader](I should go pick up that PDU like the lady on the intercom said...)") ]])
		fnCutsceneBlocker()
		
		--Move back a bit.
		fnCutsceneMove("Christine", 2.25, 14.50)
		fnCutsceneBlocker()
	end
end
