-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Execution]|
--Exit door.
if(sObjectName == "ToCryoA") then

	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusCryoA", "FORCEPOS:4.5x2.0x0")

--PDU.
elseif(sObjectName == "PDU") then

	--If we already have the PDU, do nothing.
	local iHasPDU = VM_GetVar("Root/Variables/Chapter5/Scenes/iHasPDU", "N")
	if(iHasPDU == 1.0) then return end
	
	--Otherwise, get the PDU.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iHasPDU", "N", 1.0)
	VM_SetVar("Root/Variables/Chapter5/Scenes/iIntercomState", "N", 2.0)
	
	--Give Chris a Tazer, and equip it. If there is already a weapon in the weapon slot, don't do this (can happen on NC+).
    AdvCombat_SetProperty("Push Active Party Member", "Christine")
        local sWeapon = AdvCombatEntity_GetProperty("Equipment In Slot S", "Weapon")
	DL_PopActiveObject()
    
    --If the weapon is "Null", spawn a Tazer and equip it.
    if(sWeapon == "Null") then
        LM_ExecuteScript(gsItemListing, "Tazer")
        AdvCombat_SetProperty("Push Party Member", "Christine")
            AdvCombatEntity_SetProperty("Equip Item To Slot", "Weapon", "Tazer")
        DL_PopActiveObject()
    end

	--Dialogue stuff.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "PDU", "Neutral") ]])
	fnCutscene([[ Append("Chris:[VOICE|ChrisMaleVoice] (This must be what that lady was talking about.[P] She didn't mention all the bodies...)[B][C]") ]])
	fnCutscene([[ Append("Chris:[VOICE|ChrisMaleVoice] (Looks kind of like an old cell phone.[P] Let's see...)[B][C]") ]])
	fnCutscene([[ Append("*click*[B][C]") ]])
	fnCutscene([[ Append("PDU:[E|Neutral] Hello![P] I am the Portable Database Unit, revision 7.0![B][C]") ]])
	fnCutscene([[ Append("PDU:[E|Quiet] ...[B][C]") ]])
	fnCutscene([[ Append("PDU:[E|Question] Aren't you going to say hello?[B][C]") ]])
	fnCutscene([[ Append("Chris:[VOICE|ChrisMaleVoice] Hello?[B][C]") ]])
	fnCutscene([[ Append("PDU:[E|Alarmed] Thank you![P] Voice imprint acknowledged.[P] Setting facial recognition now.[B][C]") ]])
	fnCutscene([[ Append("*snap*[B][C]") ]])
	fnCutscene([[ Append("PDU:[E|Question] You are not in the existing database.[P] I will register a new user.[P] What is your name?[B][C]") ]])
	fnCutscene([[ Append("Chris:[VOICE|ChrisMaleVoice] Umm...[P] Chris.[B][C]") ]])
	fnCutscene([[ Append("PDU:[E|Neutral] Hello, Um... Chris![P] May I call you Chris for short?[B][C]") ]])
	fnCutscene([[ Append("Chris:[VOICE|ChrisMaleVoice] *sigh*[B][C]") ]])
	fnCutscene([[ Append("PDU:[E|Happy] Oh, I'm just kidding![P] Would you like me to disable humour protocols?[B][C]") ]])
	fnCutscene([[ Append("Chris:[VOICE|ChrisMaleVoice] No, it's fine.[P] I was just -[P] PDU, what happened here?[P] Who killed all these -[P] robot -[P] women?[B][C]") ]])
	fnCutscene([[ Append("PDU:[E|Neutral] Error [P]-[P] unable to access network.[P] Also, it is termed 'retired', and the units in this room are 'golems'.[P] Please update your dictionary software.[B][C]") ]])
	fnCutscene([[ Append("PDU:[E|Quiet] Considering the plasma scoring and damage to the room, I would venture that they retired one another.[B][C]") ]])
	fnCutscene([[ Append("Chris:[VOICE|ChrisMaleVoice] Why?[B][C]") ]])
	fnCutscene([[ Append("PDU:[E|Quiet] Because -[P] error[P], unable to access network.[P] Query cannot be completed.[B][C]") ]])
	fnCutscene([[ Append("Chris:[VOICE|ChrisMaleVoice] ...[B][C]") ]])
	fnCutscene([[ Append("PDU:[E|Neutral] Please contact your network administrator.[B][C]") ]])
	fnCutscene([[ Append("Chris:[VOICE|ChrisMaleVoice] Can you at least tell me where I am?[B][C]") ]])
	fnCutscene([[ Append("PDU:[E|Neutral] You are currently standing in the Extra-Vehicular Activity (EVA) room in the Cryogenics Research Facility on Regulus.[B][C]") ]])
	fnCutscene([[ Append("Chris:[VOICE|ChrisMaleVoice] Regulus?[B][C]") ]])
	fnCutscene([[ Append("PDU:[E|Neutral] Regulus is the moon of Pandemonium.[P] It completes one orbit every 45.15 days and is tidally locked to Pandemonium.[P] Its mass is - [P][CLEAR]") ]])
	fnCutscene([[ Append("Chris:[VOICE|ChrisMaleVoice] Thank you, that's enough.[B][C]") ]])
	fnCutscene([[ Append("Chris:[VOICE|ChrisMaleVoice] PDU, do you know how I got here?[B][C]") ]])
	fnCutscene([[ Append("PDU:[E|Quiet] I am sorry, Chris.[P] That information is not in my database.[B][C]") ]])
	fnCutscene([[ Append("PDU:[E|Neutral] Since I am unable to access the network, I would recommend moving to Regulus City and querying the central database.[B][C]") ]])
	fnCutscene([[ Append("PDU:[E|Question] Is there anything else I can do for you?[B][C]") ]])
	fnCutscene([[ Append("Chris:[VOICE|ChrisMaleVoice] That will be enough for now.[B][C]") ]])
	fnCutscene([[ Append("Chris:[VOICE|ChrisMaleVoice] (Hmm, there's something else on the table here.[P] Looks like a tazer, and a first-aid kit.)[B][C]") ]])
	fnCutscene([[ Append("[SOUND|World|TakeItem](*Received Tazer and Doctor Bag.*)[B][C]") ]])
	fnCutscene([[ Append("Chris:[VOICE|ChrisMaleVoice] (I hope I don't have to use it...)[B][C]") ]])
	fnCutscene([[ Append("PDU:[E|Neutral] Chris, I don't mean to interrupt, but it seems the area around us is rather dark.[B][C]") ]])
	fnCutscene([[ Append("PDU:[E|Happy] You can adjust the brightness via the options menu if you are finding it too difficult to navigate.[P] Thank you![B][C]") ]])
	fnCutscene([[ Append("PDU:[E|Neutral] One last thing.[P] I have a map of the local area.[P] You may access it via the pause menu.") ]])
	fnCutsceneBlocker()
    
    --Map flag.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iReceivedMap", "N", 1.0)
    
    --Add map.
    fnCryogenicsMap(1551, 1158, 1485, 1020)

	--Set the Doctor Bag charges.
    gbAutoSetDoctorBagCurrentValues = true
    LM_ExecuteScript(gsComputeDoctorBagTotalPath)
	
	--Remove the PDU NPC.
	fnCutsceneTeleport("PDUNPC", -100.0, -100.0)
	fnCutscene([[ AL_SetProperty("Disable Light", "PDULight") ]])
	fnCutsceneBlocker()

--Damaged Golem.
elseif(sObjectName == "GolemA") then

	--No PDU:
	local iHasPDU = VM_GetVar("Root/Variables/Chapter5/Scenes/iHasPDU", "N")
	if(iHasPDU == 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Chris:[VOICE|ChrisMaleVoice] (Some sort of mechanical woman.[P] She's not moving...)") ]])
		fnCutsceneBlocker()
	
	--PDU Scan:
	else
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("PDU: Scanning::[P]  Unit #48922.[P] Status:: Retired.[B][C]") ]])
		fnCutscene([[ Append("PDU: Likely cause of retirement::[P] Major plasma damage to central processing unit.[P] Subject absorbed multiple major hits at close range.[P] System offline was instantaneous.") ]])
		fnCutsceneBlocker()
	end

--Damaged Golem.
elseif(sObjectName == "GolemB") then

	--No PDU:
	local iHasPDU = VM_GetVar("Root/Variables/Chapter5/Scenes/iHasPDU", "N")
	if(iHasPDU == 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Chris:[VOICE|ChrisMaleVoice] (Some sort of mechanical woman.[P] She's not moving...)") ]])
		fnCutsceneBlocker()
	
	--PDU Scan:
	else
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("PDU: Scanning::[P]  Unit #51641. Status::[P] Retired.[B][C]") ]])
		fnCutscene([[ Append("PDU: Likely cause of retirement::[P] Blunt trauma to processing unit.[P] Subject suffered a heavy strike from behind, crushing the CPU's thermal regulator.[P] System offline was instantaneous.") ]])
		fnCutsceneBlocker()
	end

--Damaged Golem.
elseif(sObjectName == "GolemC") then

	--No PDU:
	local iHasPDU = VM_GetVar("Root/Variables/Chapter5/Scenes/iHasPDU", "N")
	if(iHasPDU == 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Chris:[VOICE|ChrisMaleVoice] (Some sort of mechanical woman.[P] She's not moving...)") ]])
		fnCutsceneBlocker()
	
	--PDU Scan:
	else
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("PDU: Scanning::[P]  Unit #10911. Status::[P] Retired.[B][C]") ]])
		fnCutscene([[ Append("PDU: Likely cause of retirement::[P] Plasma damage to power storage battery.[P] Subject absorbed a plasma diffractor blast at close range, then fell to the ground.[P] A second blast struck the storage battery, offlining the unit.") ]])
		fnCutsceneBlocker()
	end

--Damaged Golem.
elseif(sObjectName == "GolemD") then

	--No PDU:
	local iHasPDU = VM_GetVar("Root/Variables/Chapter5/Scenes/iHasPDU", "N")
	if(iHasPDU == 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Chris:[VOICE|ChrisMaleVoice] (Some sort of mechanical woman.[P] She's not moving...)") ]])
		fnCutsceneBlocker()
	
	--PDU Scan:
	else
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("PDU: Scanning::[P]  Unit #10912. Status::[P] Retired.[B][C]") ]])
		fnCutscene([[ Append("PDU: Likely cause of retirement::[P] Plasma damage to processing unit.[P] Subject was attempting to carry another, damaged, unit when it received a plasma diffractor blast to the cranial region.[P] System offline was instantaneous.") ]])
		fnCutsceneBlocker()
	end

--Damaged Golem.
elseif(sObjectName == "GolemE") then

	--No PDU:
	local iHasPDU = VM_GetVar("Root/Variables/Chapter5/Scenes/iHasPDU", "N")
	if(iHasPDU == 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Chris:[VOICE|ChrisMaleVoice] (Some sort of mechanical woman.[P] She's not moving...)") ]])
		fnCutsceneBlocker()
	
	--PDU Scan:
	else
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("PDU: Scanning::[P]  Unit #711820. Status::[P] Retired.[B][C]") ]])
		fnCutscene([[ Append("PDU: Likely cause of retirement::[P] Fragmentation explosive detonated on impact with central chassis.[P] All systems suffered catastrophic damage, system offline was instantaneous.") ]])
		fnCutsceneBlocker()
	end
    
--Window
elseif(sObjectName == "Window") then

    --Variables.
    local iHasGolemForm = VM_GetVar("Root/Variables/Global/Christine/iHasGolemForm", "N")
    if(iHasGolemForm == 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Chris:[VOICE|ChrisMaleVoice] (I can see grey rock outside, and I was in an airlock...[P] Am I on -[P] the moon?[P] How!?)") ]])
		fnCutsceneBlocker()
    else
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Thought:[VOICE|Leader] (Hard to imagine I used to be so scared of being on Regulus.[P] Even harder to imagine that became the best day of my life.)") ]])
		fnCutsceneBlocker()
    end

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
