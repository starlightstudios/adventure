-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
--Meeting 55 cutscene.
if(sObjectName == "Cutscene") then
	
	-- |[Setup]|
	--Variables.
	local iMet55 = VM_GetVar("Root/Variables/Chapter5/Scenes/iMet55", "N")
	if(iMet55 == 1.0) then return end
	
	--Set.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iMet55", "N", 1.0)
	VM_SetVar("Root/Variables/Chapter5/Scenes/iIsPostGolemScene", "N", 1.0)
	VM_SetVar("Root/Variables/Chapter5/Scenes/iIntercomState", "N", 8.0)
    VM_SetVar("Root/Variables/Chapter5/Scenes/iMainQuestBlock", "N", 10.0)
    
    --UI.
    AM_SetProperty("Set Form Icon",   "Root/Images/AdventureUI/CampfireMenuIcon/TransformChristine")
    AM_SetProperty("Set Relive Icon", "Root/Images/AdventureUI/CampfireMenuIcon/ReliveChristine")

    -- |[Item Name Change]|
    --Call this to reset all instances of item types and descriptions from Chris to Christine.
    fnSwitchItemTypesForChristine()
	
	-- |[Movement]|
	--Move Christine forward.
	fnCutsceneMove("Christine", 10.25, 10.50)
	fnCutsceneBlocker()
	
	--Camera focuses on 55.
	fnCutscene([[ AL_SetProperty("Music", "TiffanysTheme") ]])
	Cutscene_CreateEvent("CameraEvent", "Camera")
		CameraEvent_SetProperty("Max Move Speed", 1.0)
		CameraEvent_SetProperty("Focus Position", (10.25 * gciSizePerTile), (6.50 * gciSizePerTile))
	DL_PopActiveObject()
	
	--Wait a bit.
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	
	-- |[Dialogue]|
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ Append("Chris:[E|Neutral][VOICE|ChrisMaleVoice] Excuse me...[P] are you the lady who was on the intercom?[B][C]") ]])
	fnCutscene([[ Append("Girl:[VOICE|Tiffany] Yes.[P] I recognize your voice.[P] It is deeper than the interference made it out to be.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	
	-- |[Movement]|
	--55 turns partway around.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Tiffany")
		ActorEvent_SetProperty("Special Frame", "Chair1")
	DL_PopActiveObject()
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Tiffany")
		ActorEvent_SetProperty("Special Frame", "Chair2")
	DL_PopActiveObject()
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	
	-- |[Dialogue]|
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ Append("Chris:[E|Neutral][VOICE|ChrisMaleVoice] Y-[P]your legs...[B][C]") ]])
	fnCutscene([[ Append("Girl:[VOICE|Tiffany] I needed the parts you were collecting.[P] That should have been obvious.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	
	-- |[Movement]|
	--55 turns the rest of the way.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Tiffany")
		ActorEvent_SetProperty("Special Frame", "Chair3")
	DL_PopActiveObject()
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Tiffany")
		ActorEvent_SetProperty("Special Frame", "Chair4")
	DL_PopActiveObject()
	fnCutsceneWait(305)
	fnCutsceneBlocker()
	
	-- |[Fade]|
	--Start the fade so it takes a long time. Most players won't notice.
	fnCutscene([[ AL_SetProperty("Activate Fade", 1000, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	
	-- |[Dialogue]|
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Broken") ]])
	fnCutscene([[ Append("2855: I am unit 2855.[P] I - [P][CLEAR]") ]])
	fnCutscene([[ Append("2855: You -[P] (partial data retrieval failure) [P] you are a [P]human...[B][C]") ]])
	fnCutscene([[ Append("Chris:[E|Neutral][VOICE|ChrisMaleVoice] And you're one of those robot things![B][C]") ]])
	fnCutscene([[ Append("2855: Correct.[P] Your thermal output -[P] I see.[P] But the airlock -[P] Where did you achieve on the second airlock!?[B][C]") ]])
	fnCutscene([[ Append("Chris:[E|Neutral][VOICE|ChrisMaleVoice] ...E- Excuse me?[B][C]") ]])
	fnCutscene([[ Append("2855: Where did you achieve on the second airlock!?[B][C]") ]])
	fnCutscene([[ Append("2855: ...[B][C]") ]])
	fnCutscene([[ Append("2855: My query matrix must need recalibration.[P] I apologize.[B][C]") ]])
	fnCutscene([[ Append("2855: ...[P] There.[P] Now.[P] How did you get in the airlock when it was depressurized?[B][C]") ]])
	fnCutscene([[ Append("Chris:[E|Neutral][VOICE|ChrisMaleVoice] I don't know![P] Aren't you the one who brought me here? You should already know.[B][C]") ]])
	fnCutscene([[ Append("2855: Incorrect, but not surprising.[P] No, I only know what I've managed to recover from the logs in this facility.[B][C]") ]])
	fnCutscene([[ Append("Chris:[E|Neutral][VOICE|ChrisMaleVoice] What? What's not surprising?[B][C]") ]])
	fnCutscene([[ Append("2855: Presumably due to whatever calamity befell this installation, my memory of everything before the last three days has been purged.[B][C]") ]])
	fnCutscene([[ Append("2855: If you were collected before that happened, well, perhaps one of the servant units let you out.[P] For what purpose I cannot guess.[B][C]") ]])
	fnCutscene([[ Append("2855: What is your name,[P] human?[B][C]") ]])
	fnCutscene([[ Append("Chris:[E|Neutral][VOICE|ChrisMaleVoice] C-[P]...[P]Chris...[B][C]") ]])
	fnCutscene([[ Append("2855: Is there something about me that is upsetting to you, Chris?[P] Your heart rate is elevated.[B][C]") ]])
	fnCutscene([[ Append("Chris:[E|Neutral][VOICE|ChrisMaleVoice] You're...[B][C]") ]])
	fnCutscene([[ Append("2855: Disfigured, yes I know.[P] I will need to get more complete repairs at Regulus City, but the motivators and ocular unit you have collected will allow me to walk again and balance myself.[B][C]") ]])
	fnCutscene([[ Append("2855: You will not need to install them.[P] I will be able to do it myself.[B][C]") ]])
	fnCutscene([[ Append("Chris:[E|Neutral][VOICE|ChrisMaleVoice] You...[B][C]") ]])
	fnCutscene([[ Append("2855: Not that it matters to any degree.[P] Without the authenticator chip, we will be unable to enter the city.[P] We are stranded here.[B][C]") ]])
	fnCutscene([[ Append("Chris:[E|Neutral][VOICE|ChrisMaleVoice] Um... [P]What is this chip you keep talking about?[B][C]") ]])
	fnCutscene([[ Append("2855: All units are provided with an authenticator chip when they are created.[B][C]") ]])
	fnCutscene([[ Append("2855: The codes on the chip produce a four-dimensional formula that calculates a pre-determined primordial-class prime number to a pre-determined length.[P] When access is requested, random variables are provided by the requester.[B][C]") ]])
	fnCutscene([[ Append("2855: The chip computes the result and returns a four-kilobyte number block. The result is a unique code every time, guaranteeing the chip is the original one and the unit is who she says she is.[B][C]") ]])
	fnCutscene([[ Append("Chris:[E|Neutral][VOICE|ChrisMaleVoice] I don't think I follow what you mean...[B][C]") ]])
	fnCutscene([[ Append("2855: ...[P]Regardless, without such a chip, we will not able to enter Regulus City.[B][C]") ]])
	fnCutscene([[ Append("Chris:[E|Neutral][VOICE|ChrisMaleVoice] And you don't have one?[B][C]") ]])
	fnCutscene([[ Append("2855: Mine was...[P] removed.[P] As it was, it seems, with all the units you scanned.[B][C]") ]])
	fnCutscene([[ Append("Chris:[E|Neutral][VOICE|ChrisMaleVoice] So someone was removing them?[B][C]") ]])
	fnCutscene([[ Append("2855: The motion trackers have not indicated any additional intelligent presence in the building save us.[P] The scraprats are not programmed to remove the chips.[B][C]") ]])
	fnCutscene([[ Append("2855: Not that I could accommodate you.[P] As a human, you would not survive the journey to Regulus City.[P] The tram system tracks were destroyed and you will not be able to survive in a vacuum.[B][C]") ]])
	fnCutscene([[ Append("2855: There is no fate for you but to starve to death here, or die of dehydration.[P] I will make the process as comfortable as possible.[P] I owe it to you for helping repair me.[B][C]") ]])
	fnCutscene([[ Append("Chris:[E|Neutral][VOICE|ChrisMaleVoice] ...[P] Is this some kind of joke?[B][C]") ]])
	fnCutscene([[ Append("2855: No, it is not.[B][C]") ]])
	fnCutscene([[ Append("Chris:[E|Neutral][VOICE|ChrisMaleVoice] Is there no other -[P] well...[P] No.[P] Maybe I...[P][CLEAR]") ]])
	fnCutscene([[ Append("2855: If you prefer it, I can terminate you quickly and painlessly.[P] A pulse-diffractor round to the temple will suffice.[B][C]") ]])
	fnCutscene([[ Append("Chris:[E|Neutral][VOICE|ChrisMaleVoice] W-[P] Wha[P][P]t[P][P][P]?[B][C]") ]])
	fnCutscene([[ Append("2855: Would this not be a preferrable alternative to death by means of dehydration?[B][C]") ]])
	fnCutscene([[ Append("Chris:[E|Neutral][VOICE|ChrisMaleVoice] ...[P]that isn't, [P]... but[P]...[P] I-[P] I can't...[P][P] what[P]?[B][C]") ]])
	fnCutscene([[ Append("2855: Do you prefer self-termination?[B][C]") ]])
	fnCutscene([[ Append("Chris:[E|Neutral][VOICE|ChrisMaleVoice] ...[P]No! ...[P]i [P]...[P]WHAT is...[P] no[P]... you[P] ... [P][P]what[P]?[B][C]") ]])
	fnCutscene([[ Append("2855: Come, I will need to repair myself before we can do anything.[P] Give the parts to me...[B][C]") ]])
	fnCutscene([[ Append("Chris:[E|Neutral][VOICE|ChrisMaleVoice] But you... [P]Uh...[P]...OK...[B][C]") ]])
	fnCutscene([[ Append("2855: ...[B][C]") ]])
	fnCutscene([[ Append("2855: ...[B][C]") ]])
	fnCutscene([[ Append("2855: Excellent.[P] *Click*[P] Installing drivers...[P] These parts will do well.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(120)
	fnCutsceneBlocker()
	
	-- |[Dialogue Resumes]|
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[SOUND|World|Thump]*WHACK*[B][C]") ]])
	fnCutscene([[ Append("Chris:[VOICE|ChrisMaleVoice] Ugh, you...[B][C]") ]])
	fnCutscene([[ Append("2855:[VOICE|Tiffany] Perhaps you will prove useful to me again after all.[P] Now, I just place the core here...") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(60)
	fnCutsceneBlocker()
	
	--Golem Transformation.
	fnCutsceneBlocker()
	LM_ExecuteScript(gsRoot .. "Chapter 5/Scenes/500 Normal/Chris To Golem Cryogenics/Scene_Begin.lua")
	fnCutsceneBlocker()
	fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua") ]])
	fnCutsceneBlocker()
	
	fnCutscene([[ AL_BeginTransitionTo("RegulusCryoCommandA", gsRoot .. "Maps/RegulusCryo/RegulusCryoCommandA/Scene_PostTF.lua") ]])
	fnCutsceneBlocker()
end
