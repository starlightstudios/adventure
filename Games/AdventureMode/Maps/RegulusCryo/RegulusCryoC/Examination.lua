-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
    
-- |[Exits]|
--Exit door. Requires the red keycard.
if(sObjectName == "ToCryoG") then

	--Variables.
	local iHasPDUCryoRedCard = VM_GetVar("Root/Variables/Chapter5/Scenes/iHasPDUCryoRedCard", "N")
	local iSeenRedDoorWarning = VM_GetVar("Root/Variables/Chapter5/Scenes/iSeenRedDoorWarning", "N")
	if(iHasPDUCryoRedCard == 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[SOUND|World|AutoDoorFail] Access denied.[P] Please present security-red keycard.") ]])
		fnCutsceneBlocker()
	else
		
		--Warning text. Only plays once.
		if(iSeenRedDoorWarning == 0.0) then
			VM_SetVar("Root/Variables/Chapter5/Scenes/iSeenRedDoorWarning", "N", 1.0)
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ Append("Voice: Red access granted.[P] Warning:[P] A power failure has been detected in this sector.[P] Proceed with caution.") ]])
			fnCutsceneBlocker()
		end
		fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
		fnCutscene([[ AL_BeginTransitionTo("RegulusCryoG", "FORCEPOS:18.0x14.0x0") ]])
		fnCutsceneBlocker()
	end

--Exit ladder.
elseif(sObjectName == "ToLowerCryoA") then
	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("RegulusCryoLowerA", "FORCEPOS:3.0x19.0x0")

-- |[Terminals]|
elseif(sObjectName == "ComputerA") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: Scanning hard drive...[P] done.[P] Playing most recent log entry.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: Research Log of Unit 609144, day 3.[P] The subject has not become any calmer since it was relocated to this facility.[P] It seems to retain a degree of sentience, but refuses to speak to any of our units.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: Earlier, one of the restraints broke and the subject grabbed and throttled the nearest slave unit.[P] Six units were damaged in the ensuing chaos.[P] The subject was eventually subdued through physical force.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: Our first priority will be researching improved containment procedures.[P] Personnel damage is expensive and it will be better to have a failsafe rather than send units to maintenance.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: The restraint had been of premium quality.[P] It seems it broke due to the sheer strength of the subject, which would be at least fifteen times that of a normal human.[P] Such strength is incredible.[P] If we can harness it, we could greatly improve slave unit efficiency.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: The subject is asleep now.[P] It is unsettling, to say the least, that the subject sleeps with its eyes open.[P] Neurological scans show it is undergoing REM sleep despite not moving its eyes.[P] More research is required.[P] Unit 609144 signing out.") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "ComputerB") then
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: Scanning hard drive...[P] done.[P] Playing most recent log entry.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: Research Log of Unit 609144, day 4.[P] Subject has developed a series of orbs under her skin, at various locations without any discernable pattern.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: Extraction of one such orb did not appear to cause any biological harm to the subject.[P] The orb did not grow back, and I am unable to determine if it is a biological organ or perhaps a lymph node.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: The orb was placed in the containment area until we determine if it has any unusual properties.[P] If necessary, we can extract more orbs and use them to experiment on other subjects.[P] Signing out.") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "ComputerC") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: Scanning hard drive...[P] done.[P] Playing most recent log entry.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: Research Log of Unit 609144, day 12.[P] Central administration either has a great deal of faith in my abilities, or none.[P] I cannot say.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: Strange radio signals have been reported from the surface near the facility.[P] Recently, some emanations were discovered inside the facility.[P] Why they require me to find the source is beyond me.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: It's probably a slave unit with an unauthorized radio making short-wave transmissions for fun.[P] Increasing our security detail is thus not necessary.[P] I have instead reassigned the security units to aiding my research.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: The power exchange system is due to come online tomorrow, meaning we will no longer require a line from Regulus City.[P] I will thus also be forced to compute the power budget.[P] Why is all of this being dumped on a research unit?[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: No matter.[P] I will press on.[P] I've put in a request for more research units and will be looking for ways to improve my own efficiency.[P] Signing off.") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "ComputerD") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: Scanning hard drive...[P] done.[P] Playing most recent log entry.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: Research Log of Unit 609144, day 15.[P] A breakthrough![P] The subject has not awoken at all since it was transferred here![B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: Despite the eyes being open and 'aware', the subject is in a state of REM sleep at all times.[P] Even when it is observing units in the room with it, interacting with objects, or receiving stimulus, the subject is still asleep.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: While this causes me to ask a great deal of questions, answers are not forthcoming.[P] If this is what the subject is like when asleep, what would awakening it do?[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: I admit in my enthusiasm, I attempted several times to wake the subject up, with no success.[P] Incisions, burns, and high-frequency sound waves cause physical harm but have no impact on its sleeping patterns.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: In retrospect, we would need to quadruple our staff and provide combat routines to all of them just to contain it, should it escape.[P] I have reconsidered, and now I think it shall remain asleep for the time being.[P] Signing off.") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "ComputerE") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: Scanning hard drive...[P] done.[P] Playing most recent log entry.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: Research Log of Unit 609144, day 18.[P] Once again I am surrounded by incompetence.[P] Just where does central administration find these slave units?[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: The sample I extracted from the subject and placed in the containment area is gone.[P] I've pored over the cameras in that area but was unable to find a culprit.[P] Conveniently, there was a power outage for about thirty-five seconds two days ago.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: I do not have time to narrow down suspects and cross-reference alibis.[P] I am a research unit.[P] And yet, there is no trace of the lost sample.[P] Why would a unit steal it?[P] To what end?[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: No matter.[P] We will extract another sample and begin experiments on it.[P] I have ordered riot equipment for the slave units doing the extraction.[P] A repeat of last time would be undesirable.[P] Signing off.") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "ComputerF") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: Scanning hard drive...[P] done.[P] Playing most recent log entry.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: Research Log of Unit 609144, day 19. The mystery has been solved.[P] Punishments have been rescinded for all suspected slave units.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: After extraction of an orb sample from the subject's flesh, the very first experiment I subjected it to was simple thermal conductivity.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: When exposed to a sudden change in temperature, the material burst into a vapour.[P] Cold or hot, the outcome was the same.[P] This explains where the sample went.[P] Likely, the temperature of the containment unit spiked during the power outage.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: I was unable to detect any lingering vapour, so the experiment will need to proceed with better sensors in place.[P] Of interesting note is the fact that the nearby intercom unit emitted static when the orb burst.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: One of the slave units also complained that a great deal of interference was being experienced when we were performing the experiment.[P] I disciplined the unit, but it seems the orbs are capable of emitting and/or reflecting radio waves.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: Why a biological sample would produce or reflect radio waves is its own question, but I am glad we are making headway.[P] More experiments will follow.[P] Signing off.") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "ComputerG") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: Scanning hard drive...[P] done.[P] Playing most recent log entry.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: Research Log of Unit 609144, day 22.[P] Once again I am too clever for my own good.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: According to some recent findings by the units at Equinox, it is possible to map a living subject's REM cycle dreams into videographs.[P] Naturally I procured the mapping program they were using.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: And no sooner had I plugged the electrodes in and began recording data did a large amount of noise result.[P] I calibrated the software on a human subject, and the software is not malfunctioning.[P] This is, indeed, what the subject dreams of.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: The audio channels are largely unused.[P] The video channels are chaotic garbles.[P] Periodically, geometric shapes of single-digit sides can be seen flowing across the background.[P] The dreams do not change in response to external stimulus.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: I have set the program to record 72 hours of footage for later review while we consolidate our findings.[P] The slave units clearly need some time away from the subject, as they are behaving erratically.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: I have ordered a few other subjects removed from the containment area for routine cryogenics data gathering.[P] After a few days, we will resume work on the subject.[P] Signing off.") ]])
	fnCutsceneBlocker()

-- |[Golem Bodies]|
elseif(sObjectName == "GolemA") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("PDU: Scanning::[P]  Unit #990. Status::[P] Retired.[B][C]") ]])
	fnCutscene([[ Append("PDU: Likely cause of retirement::[P] Major damage to power core.[P] Subject chassis absorbed a compression wave, possibly due to an explosive or directed sonic weapon.[P] The power core then ruptured, leading to a system offline within a few minutes.") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "GolemB") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("PDU: Scanning::[P]  Unit #200817. Status::[P] Retired.[B][C]") ]])
	fnCutscene([[ Append("PDU: Likely cause of retirement::[P] Self-inflicted plasma bolt.[P] Subject appears to have fired her plasma diffractor into her own CPU from close range.[P] System offline was instantaneous.") ]])
	fnCutsceneBlocker()
		
elseif(sObjectName == "GolemC") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("PDU: Scanning::[P]  Unit #155981. Status::[P] Retired.[B][C]") ]])
	fnCutscene([[ Append("PDU: Likely cause of retirement::[P] CPU destruction.[P] Subject received two kinetic strikes to the rear chassis, then several pulse rounds into her cranial chassis.[P] The CPU and all related architecture is completely destroyed.") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "GolemD") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("PDU: Scanning::[P]  Unit #208179. Status::[P] Retired.[B][C]") ]])
	fnCutscene([[ Append("PDU: Likely cause of retirement::[P] Kill phrase.[P] Subject was severely damaged, including the loss of multiple limbs.[P] Unable to move, another unit stated its kill phrase twice.[P] System is permanently offline.") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "GolemE") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("PDU: Scanning::[P]  Unit #208179. Status::[P] Retired.[B][C]") ]])
	fnCutscene([[ Append("PDU: Likely cause of retirement::[P] CPU destruction.[P] The unit received several pulse impacts to its joints.[P] While immobilized, it received a pulse hit to its cranial chassis, destroying its CPU.") ]])
	fnCutsceneBlocker()

-- |[Other Objects]|
--Triggers dialogue with 55.
elseif(sObjectName == "Intercom") then
	LM_ExecuteScript(gsRoot .. "Chapter 5/Dialogue/Intercom/Root.lua", "Hello")

--Several cryogenics tubes. All are offline.
elseif(sObjectName == "CryoTube") then

	--Variables.
	local iHasGolemForm = VM_GetVar("Root/Variables/Global/Christine/iHasGolemForm", "N")
	
	--Doesn't have golem form:
	if(iHasGolemForm == 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("(Looks like some sort of research tube.[P] It's offline.)[B][C]") ]])
		fnCutscene([[ Append("Chris:[VOICE|ChrisMaleVoice] PDU, what is this thing?[B][C]") ]])
		fnCutscene([[ Append("PDU: This is a Cryogenic Containment Tube Mk VII.[P] This model has been outfitted with extra sensors, tubes, and drainage clamps.[B][C]") ]])
		fnCutscene([[ Append("PDU: Unlike a standard tube, this one can mix fluids, administer electricity or sound waves, and inject the subject with a wide array of chemicals.[B][C]") ]])
		fnCutscene([[ Append("PDU: These are very commonly used for scientific research on restrained organic subjects, as opposed to the long-term storage models.") ]])
		fnCutsceneBlocker()

	--Has golem form:
	else
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[VOICE|Leader](A Cryogenic Containment Tube Mk VII.[P] These are models used for research as opposed to long-term cryogenic storage of organic subjects.)[B][C]") ]])
		fnCutscene([[ Append("[VOICE|Leader](They are outfitted with extra sensors and can administer shocks, sound waves, and various fluid injections.[P] They serve the Cause of Science well.)") ]])
		fnCutsceneBlocker()

	end

--Defragmentation tube, closed or open.
elseif(sObjectName == "DefragTube") then

	--Variables.
	local iHasGolemForm = VM_GetVar("Root/Variables/Global/Christine/iHasGolemForm", "N")
	
	--Doesn't have golem form:
	if(iHasGolemForm == 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("(A glass tube with some computer hookups in it...)[B][C]") ]])
		fnCutscene([[ Append("Chris:[VOICE|ChrisMaleVoice] PDU, what is this thing?[B][C]") ]])
		fnCutscene([[ Append("PDU: An Autonomous Defragmentation Module, Mk IV. This model has been outfitted with a number of additional sensors.[B][C]") ]])
		fnCutscene([[ Append("PDU: These tubes are usually used for units to recharge their power cores and defragment their hard drives.[B][C]") ]])
		fnCutscene([[ Append("PDU: This one is used for performing cybernetic experiments on subjects, such as outfitting an organic's neurological systems with interface ports.[P] Most experiments thus far have been unsuccessful.") ]])
		fnCutsceneBlocker()

	--Has golem form:
	else
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[VOICE|Leader](An Autonomous Defragmentation Module, Mk IV.[P] These aren't used to defragment a unit's drives, though they could with a software update.)[B][C]") ]])
		fnCutscene([[ Append("[VOICE|Leader](Instead, these are used for cybernetics experiments, such as installing neuro-interfaces on organics.[P] They serve the Cause of Science well.)") ]])
		fnCutsceneBlocker()

	end

--Rebel Banners
elseif(sObjectName == "Banner") then

	--Variables.
	local iTalkedToSophie = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N")
	local iHasGolemForm = VM_GetVar("Root/Variables/Global/Christine/iHasGolemForm", "N")
    
    --Male Chris:
    if(iHasGolemForm == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (A cloth banner depicting the faceplate of a robot, broken in half.[P] The banner has been torn up by projectiles.)") ]])
    
    --Golem Christine, in Programmed Mode
    elseif(iHasGolemForm == 1.0 and iTalkedToSophie == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (A cloth banner depicting the faceplate of a golem, broken in half.[P] The banner has been torn up by projectiles.[P] It is not relevant to my programming.)") ]])
        
    --After Regulus City:
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (A cloth banner depicting the faceplate of a golem, broken in half.[P] The banner has been torn up by projectiles.[P] Your retirement will not be in vain, rebel sister...)") ]])
    
    end

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
