-- |[Warp Handler]|
--If the player warps here using the campfire, this script is called to reposition them as necessary.

--Constants. This is the important part that changes between each room.
local fTargetX = (11.25 * gciSizePerTile)
local fTargetY = ( 3.50 * gciSizePerTile)

--If 55 was not in the party when the warp started, add her.
local bWas55PresentAtStart = fnIsCharacterPresent("Tiffany")
if(bWas55PresentAtStart == false) then
    fnAddPartyMember("Tiffany")
end

--Execute.
LM_ExecuteScript(gsStandardWarpHandler, fTargetX, fTargetY)

--If 55 was not present before the warp started, and we have not seen this dialogue yet, show it.
local iSaidWarpDialogue = VM_GetVar("Root/Variables/Global/2855/iSaidWarpDialogue", "N")
local i55ReturnCryoScene = VM_GetVar("Root/Variables/Chapter5/Scenes/i55ReturnCryoScene", "N")
if(iSaidWarpDialogue == 0.0 and bWas55PresentAtStart == false) then
    
    --Flag.
    VM_SetVar("Root/Variables/Global/2855/iSaidWarpDialogue", "N", 1.0)
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
    fnCutscene([[ Append("55:[E|Neutral] !!![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] 55?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Interesting.[P] The reports *did* indicate your runestone allowed limited-scope teleportation of an unknown mechanism.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Then why are you surprised?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] I am not unacquainted with arcane teleportation spells.[P] However, I am, or was, unacquainted with being teleported while not near the caster.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Oh...[P] Yes, I suppose you weren't.[P] Maybe my rune's magic pulled you along because you're such a close friend?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] An unproveable supposition.[P] You are not the ideal candidate to advance the Cause of Science.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Gee, thanks, 55.[B][C]") ]])
    
    --Extra dialogue:
    if(i55ReturnCryoScene == 0.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/i55ReturnCryoScene", "N", 1.0)
        fnCutscene([[ Append("55:[E|Neutral] The magic on the runestone has proved convenient for our purposes.[P] Was the choice of the Cryogenics Facility deliberate?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] ...[P] Listen, I don't like wandering around this graveyard any more than you do, but...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] There may be supplies here we can use.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Indeed.[P] Supplies removed from Regulus City may be noticed by the administration, but those procured from Cryogenics Research will not.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Good thinking, 771852.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] (I guess she doesn't find this place unsettling...)") ]])
        
    --Normal:
    else
        fnCutscene([[ Append("55:[E|Neutral] The magic on the runestone has proved convenient for our purposes.[P] Let us continue.") ]])
    end
    
    
    
    fnCutsceneBlocker()

--Hasn't seen the return-to-cryo dialogue.
elseif(i55ReturnCryoScene == 0.0) then
		
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/i55ReturnCryoScene", "N", 1.0)
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
    fnCutscene([[ Append("55:[E|Neutral] So you've teleported us back here...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] ...[P] Listen, I don't like wandering around this graveyard any more than you do, but...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] There may be supplies here we can use.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Indeed.[P] Supplies removed from Regulus City may be noticed by the administration, but those procured from Cryogenics Research will not.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Good thinking, 771852.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] (I guess she doesn't find this place unsettling...)") ]])
    fnCutsceneBlocker()

end
