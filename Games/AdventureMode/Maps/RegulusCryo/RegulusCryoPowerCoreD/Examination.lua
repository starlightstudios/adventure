-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
-- |[Examinables]|
-- |[Other]|
if(sObjectName == "ToRegulusCryoPowerCoreC") then
    AudioManager_PlaySound("World|ClimbLadder")
    AL_BeginTransitionTo("RegulusCryoPowerCoreC", "FORCEPOS:17.0x18.0x0")

-- |[Debug]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
