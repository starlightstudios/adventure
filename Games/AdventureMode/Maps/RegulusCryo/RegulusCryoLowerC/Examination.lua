-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Execution]|
--Exit door.
if(sObjectName == "Nope") then
	--AL_BeginTransitionTo("RegulusCryoF", "FORCEPOS:13.0x14.0x0")
	
--Computer suspended above the darkness.
elseif(sObjectName == "ComputerMid") then

	--Variables.
	local iHasGolemForm = VM_GetVar("Root/Variables/Global/Christine/iHasGolemForm", "N")

	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: A notice has been left on this terminal.[P] Would you like me to read it?[B][C]") ]])
	if(iHasGolemForm == 1.0) then
		fnCutscene([[ Append("[VOICE|Christine]Christine:[VOICE|Christine] Go ahead.[B][C]") ]])
	else
		fnCutscene([[ Append("[VOICE|ChrisMaleVoice]Chris:[VOICE|ChrisMaleVoice] Go ahead.[B][C]") ]])
	end
	fnCutscene([[ Append("[VOICE|PDU]PDU: Security Alert to all units::[P] A large number of unauthorized radio signals have been recorded in the area around the cryogenics facility.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: If you see a unit in possession of illegal radio equipment, report them to your supervisor immediately.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|PDU]PDU: Remember::[P] Safety is everyone's job.[P] This means you.") ]])
	fnCutsceneBlocker()

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
