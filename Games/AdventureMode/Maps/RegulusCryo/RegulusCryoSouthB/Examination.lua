-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
if(sObjectName == "ToRegulusCryoSouthCE") then
    AudioManager_PlaySound("World|ClimbLadder")
    AL_BeginTransitionTo("RegulusCryoSouthC", "FORCEPOS:28.0x13.0x0")
    
elseif(sObjectName == "ToRegulusCryoSouthCW") then
    AudioManager_PlaySound("World|ClimbLadder")
    AL_BeginTransitionTo("RegulusCryoSouthC", "FORCEPOS:10.0x28.0x0")
    
elseif(sObjectName == "ToRegulusCryoSouthD") then
    AudioManager_PlaySound("World|ClimbLadder")
    AL_BeginTransitionTo("RegulusCryoSouthD", "FORCEPOS:25.0x9.0x0")

-- |[Examinables]|
elseif(sObjectName == "Sensor") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A radio-data recorder.[P] It does not have a power source and presumably was meant to be installed in the nearby construction site.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Note") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('ATTENTION::[P] ACTIVE CONSTRUCTION SITE.[P] PLEASE CONSULT THE LORD GOLEM FOR SAFETY INSTRUCTIONS IF YOU CANNOT ACCESS THE LOCAL NETWORK.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Probably a notice for anyone with damaged radio receivers.[P] Christine's safety tip?[P] Get those repaired ASAP!)") ]])
    fnCutsceneBlocker()

-- |[Skillbooks]|
elseif(sObjectName == "Skillbook") then
    AL_SetProperty("Set Layer Disabled", "SkillbookIco", true)
	LM_ExecuteScript(gsTiffanySkillbook, 1)

-- |[Other]|
elseif(sObjectName == "UnpoweredDoor") then

    --Variables.
    local iSCryoRepoweredDoor  = VM_GetVar("Root/Variables/Chapter5/Scenes/iSCryoRepoweredDoor", "N")
    local iSCryoBatteryPickups = VM_GetVar("Root/Variables/Chapter5/Scenes/iSCryoBatteryPickups", "N")
    
    --Fixed the door:
    if(iSCryoRepoweredDoor == 1.0) then
        AudioManager_PlaySound("World|AutoDoorOpen")
        AL_SetProperty("Open Door", "Door")

    --Door is unpowered.
    else
    
        --Varying battery states.
        if(iSCryoBatteryPickups == 0.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Leader] (I can get this door open safely if I can jump it.[P] I'll need to find five partially-charged batteries to do that.)") ]])
            fnCutsceneBlocker()
        elseif(iSCryoBatteryPickups == 1.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Leader] (I can get this door open safely if I can jump it.[P] I'll need to find four more partially-charged batteries to do that.)") ]])
            fnCutsceneBlocker()
        elseif(iSCryoBatteryPickups == 2.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Leader] (I can get this door open safely if I can jump it.[P] I'll need to find three more partially-charged batteries to do that.)") ]])
            fnCutsceneBlocker()
        elseif(iSCryoBatteryPickups == 3.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Leader] (I can get this door open safely if I can jump it.[P] I'll need to find two more partially-charged batteries to do that.)") ]])
            fnCutsceneBlocker()
        elseif(iSCryoBatteryPickups == 4.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Leader] (I can get this door open safely if I can jump it.[P] I'll need to find one more partially-charged batteries to do that.)[B][C]") ]])
            fnCutscene([[ Append("Thought:[VOICE|Leader] (There weren't enough batteries at the construction site, so another one must be somewhere nearby.") ]])
            fnCutsceneBlocker()
        
        --Fix the door scene:
        else
        
            --Flag.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iSCryoRepoweredDoor", "N", 1.0)
        
            --Achievement.
            AM_SetPropertyJournal("Unlock Achievement", "FindCrowbar")
            
            --Variables.
            local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
            
            --Merge.
            fnCutsceneMergeParty()
            fnCutsceneBlocker()
            
            --Spawn NPCs.
            TA_Create("Ravital")
                TA_SetProperty("Position", -10, -10)
                TA_SetProperty("Facing", gci_Face_South)
                TA_SetProperty("Clipping Flag", true)
                TA_SetProperty("Activation Script", fnResolvePath() .. "Dialogue.lua")
                fnSetCharacterGraphics("Root/Images/Sprites/GolemSlave/", false)
            DL_PopActiveObject()
            TA_Create("CrowbarChan")
                TA_SetProperty("Position", 33, 10)
                TA_SetProperty("Facing", gci_Face_South)
                TA_SetProperty("Clipping Flag", true)
                TA_SetProperty("Activation Script", fnResolvePath() .. "Dialogue.lua")
                for i = 1, 8, 1 do
                    TA_SetProperty("Move Frame", i-1, 0, "Root/Images/Sprites/CrowbarChan/0")
                    TA_SetProperty("Move Frame", i-1, 1, "Root/Images/Sprites/CrowbarChan/1")
                    TA_SetProperty("Move Frame", i-1, 2, "Root/Images/Sprites/CrowbarChan/2")
                    TA_SetProperty("Move Frame", i-1, 3, "Root/Images/Sprites/CrowbarChan/1")
                end
                TA_SetProperty("Auto Animates", true)
            DL_PopActiveObject()
            
            --Movement.
            fnCutsceneMove("Christine", 35.25, 15.50)
            fnCutsceneFace("Christine", 0, -1)
            fnCutsceneMove("Tiffany", 36.25, 15.50)
            fnCutsceneFace("Tiffany", -1, -1)
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Open the door.
            fnCutscene([[ AL_SetProperty("Open Door", "Door") ]])
            fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
            fnCutsceneLayerDisabled("SpecialBlackout", true)
            fnCutsceneBlocker()

            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutscene([[ Append("Christine:[E|Smirk] Got it![B][C]") ]])
            fnCutscene([[ Append("55:[E|Smirk] Well done, Unit 771852.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Thank you for your praise, Command Unit.[P] Now let's -") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            
            --Movement.
            fnCutsceneMove("CrowbarChan", 33.25, 11.50)
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()

            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "CrowbarChan", "Neutral") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] ...[P] Go get our eyes checked...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] My diagnostics are not detecting any errors.[B][C]") ]])
            fnCutscene([[ Append("CrowbarChan:[E|Neutral] Ravital![P] Ravital, exit system standby, we're saved!") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(125)
            fnCutsceneBlocker()
            
            --Movement.
            fnCutsceneLayerDisabled("GolemLower", true)
            fnCutsceneLayerDisabled("GolemUpper", true)
            fnCutsceneTeleport("Ravital", 35.25, 10.50)
            fnCutsceneFace("Ravital", 0, 1)
            fnCutsceneBlocker()
            fnCutsceneWait(45)
            fnCutsceneBlocker()
            fnCutsceneFace("Ravital", 1, 0)
            fnCutsceneWait(45)
            fnCutsceneBlocker()
            fnCutsceneFace("Ravital", -1, 0)
            fnCutsceneWait(45)
            fnCutsceneBlocker()
            fnCutsceneFace("Ravital", 0, 1)
            fnCutsceneWait(45)
            fnCutsceneBlocker()

            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Golem", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "CrowbarChan", "Neutral") ]])
            fnCutscene([[ Append("Ravital:[E|Neutral] Diagnostics...[P] Power core at forty percent...[P] CC, what happened?[B][C]") ]])
            fnCutscene([[ Append("CrowbarChan:[E|Neutral] Some very heroic friends are here to help![B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] That much is definitely true.[P] We were searching the area when I noticed your core cycle on a local area scan.[P] Are you all right?[B][C]") ]])
            fnCutscene([[ Append("Ravital:[E|Neutral] ...[P] Who are you?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Unit 771852, Maintenance and Repair, Sector 96.[B][C]") ]])
            if(sChristineForm == "Human" or sChristineForm == "Raiju") then
                fnCutscene([[ Append("Ravital:[E|Neutral] But we're standing in a vacuum.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] That'd be the result of a bug.[P] Report it to Salty and don't worry about it.[B][C]") ]])
            elseif(sChristineForm == "Golem") then
                fnCutscene([[ Append("Ravital:[E|Neutral] 771852?[P] You must be a new unit...[B][C]") ]])
                fnCutscene([[ Append("Ravital:[E|Neutral] Not that it matters.[B][C]") ]])
            elseif(sChristineForm == "LatexDrone") then
                fnCutscene([[ Append("Ravital:[E|Neutral] (Did something happen to her inhibitor chip?[P] Maybe Command Unit 2855 likes them that way?)[B][C]") ]])
            elseif(sChristineForm == "SteamDroid") then
                fnCutscene([[ Append("Ravital:[E|Neutral] Uhhhh...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] It's a long story.[B][C]") ]])
                fnCutscene([[ Append("Ravital:[E|Neutral] I'll bet it is.[B][C]") ]])
            elseif(sChristineForm == "Electrosprite") then
                fnCutscene([[ Append("Ravital:[E|Neutral] So what species are you, if I may ask?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] We're called electrosprites![P] We're -[P] electricity![P] I think![B][C]") ]])
                fnCutscene([[ Append("Ravital:[E|Neutral] Wow, you'd be pretty useful around a construction site...[B][C]") ]])
            elseif(sChristineForm == "Eldritch") then
                fnCutscene([[ Append("Ravital:[E|Neutral] So what species are you, if I may ask?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] A dreamer, is what I will say.[P] It's rather complicated.[B][C]") ]])
                fnCutscene([[ Append("Ravital:[E|Neutral] It looks that way, but given what I heard they were researching...[B][C]") ]])
            elseif(sChristineForm == "Secrebot") then
                fnCutscene([[ Append("Ravital:[E|Neutral] Are you a refurbished secrebot or something?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Second wave, with human-grade intelligence![B][C]") ]])
                fnCutscene([[ Append("Ravital:[E|Neutral] What else have we missed being stuck out here?[B][C]") ]])
            elseif(sChristineForm == "Doll") then
                fnCutscene([[ Append("Ravital:[E|Neutral] Uh huh.[P] A brand-spanking-new command unit, 771852, right at the top end of the designation stack.[B][C]") ]])
                fnCutscene([[ Append("Ravital:[E|Neutral] Freshly minted, and just so curiously out here to rescue us.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] It seems you've made up your mind, regardless of my explanation.[B][C]") ]])
                fnCutscene([[ Append("Ravital:[E|Neutral] I've found myself becoming considerably less gullible over the last stretch of time.[B][C]") ]])
            end
            fnCutscene([[ Append("55:[E|Neutral] And I am - [P][CLEAR]") ]])
            fnCutscene([[ Append("Ravital:[E|Neutral] I already know who you are.[P] So...[B][C]") ]])
            fnCutscene([[ Append("Ravital:[E|Neutral] Do I get any last words?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] After we went through the trouble of rescuing you?[P] We're not going to retire you![B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] That is correct.[B][C]") ]])
            fnCutscene([[ Append("Ravital:[E|Neutral] You have a change of heart, 2855?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] I wiped all of my memory drives, to be exact.[P] I do not have a heart.[B][C]") ]])
            fnCutscene([[ Append("Ravital:[E|Neutral] You can say that again -[P] wait, what?[B][C]") ]])
            fnCutscene([[ Append("Ravital:[E|Neutral] You wiped your memory drives?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Correct.[P] If we have met in the past, I do not remember it.[P] I harbor no grudges against you.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] We've gone maverick, actually.[B][C]") ]])
            fnCutscene([[ Append("CrowbarChan:[E|Neutral] She's not gonna retire us, nyuuuu![P] Yaaay![B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] And who are you, exactly?[B][C]") ]])
            fnCutscene([[ Append("Ravital:[E|Neutral] Oh, you've never heard of autonomous toolsets?[P] This is crowbar-chan.[B][C]") ]])
            fnCutscene([[ Append("CrowbarChan:[E|Neutral] Hi Christine![B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] I didn't tell you my secondary designation yet...[B][C]") ]])
            fnCutscene([[ Append("CrowbarChan:[E|Neutral] ...............[P] *Whoops!*[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] So you're a self-motile automated construction tool?[B][C]") ]])
            fnCutscene([[ Append("CrowbarChan:[E|Neutral] .[P].[P].[P] Yes?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] And therefore a robot?[B][C]") ]])
            fnCutscene([[ Append("CrowbarChan:[E|Neutral] .[P].[P].[P] Yes, absolutely![B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Capital![P] Sorry about the misunderstanding, but I'm a repair unit and your schematics aren't on file.[B][C]") ]])
            fnCutscene([[ Append("Ravital:[E|Neutral] She's a prototype.[P] We're testing her out.[P] Records tend to get lost, you know how it is.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Laugh] Oh, absolutely.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] So, Ravital, what exactly happened here?[B][C]") ]])
            fnCutscene([[ Append("Ravital:[E|Neutral] I'm not going to hide it.[P] There was a revolt.[P] The other workers made guns and took over the main building, demanding better treatment and a say in their working conditions.[B][C]") ]])
            fnCutscene([[ Append("Ravital:[E|Neutral] I'm not stupid, so I just kept doing my job out here.[P] I felt the breaching charges go off as they vibrated through the rock.[B][C]") ]])
            fnCutscene([[ Append("Ravital:[E|Neutral] Then the power got cut and I couldn't get the door open again.[P] I was trapped.[B][C]") ]])
            fnCutscene([[ Append("CrowbarChan:[E|Neutral] She went into system standby to conserve power until someone showed up to let us out.[P] We've been in here since then.[B][C]") ]])
            fnCutscene([[ Append("Ravital:[E|Neutral] How did you pass the time, Crowbar-chan?[B][C]") ]])
            fnCutscene([[ Append("CrowbarChan:[E|Neutral] I wrote some stories on my hard drive.[P] Want to read them?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Later.[P] I'm just glad there were other survivors of the...[P] Cryogenics Incident...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] In a way, we too are survivors like you.[B][C]") ]])
            fnCutscene([[ Append("Ravital:[E|Neutral] Hrmpf.[B][C]") ]])
            fnCutscene([[ Append("Ravital:[E|Neutral] So you say, but I don't trust you.[P] Command Unit 2855...[B][C]") ]])
            fnCutscene([[ Append("CrowbarChan:[E|Neutral] They just got done saving us![B][C]") ]])
            fnCutscene([[ Append("Ravital:[E|Neutral] It could be a trick.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Down] ...[B][C]") ]])
            fnCutscene([[ Append("Ravital:[E|Neutral] Listen, 771852, I appreciate it.[P] But from what you just said...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] Everyone else is retired...[B][C]") ]])
            fnCutscene([[ Append("Ravital:[E|Neutral] Yeah.[P] Yeah.[P] I'm going to head back to Regulus City.[B][C]") ]])
            fnCutscene([[ Append("Ravital:[E|Neutral] I know places we can hide out.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] I can help you.[P] I know of gaps in the security web.[B][C]") ]])
            fnCutscene([[ Append("Ravital:[E|Neutral] I don't need your help.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] But...[B][C]") ]])
            fnCutscene([[ Append("Ravital:[E|Neutral] If you're the real deal, 771852, maybe I'll contact you later.[P] If we're all mavericks now, we have to stick together.[B][C]") ]])
            fnCutscene([[ Append("Ravital:[E|Neutral] But I have a lot of thinking to do.[P] So please, leave us to it.[B][C]") ]])
            fnCutscene([[ Append("CrowbarChan:[E|Neutral] Nyuuuu...[P] Thanks, Christine...") ]])
            fnCutsceneBlocker()
            
            --Fold the party.
            fnAutoFoldParty()
            fnCutsceneBlocker()
            
        end
    end

-- |[Debug]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
