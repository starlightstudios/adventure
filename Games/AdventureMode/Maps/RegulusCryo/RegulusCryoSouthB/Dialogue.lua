-- |[Root]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    if(sActorName == "Ravital") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
        fnCutscene([[ Append("Ravital:[E|Neutral] I'll be heading to Regulus City in a minute.[P] I know some places I can hide, units I can talk to.[P] Don't worry about me.") ]])
	
    elseif(sActorName == "CrowbarChan") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "CrowbarChan", "Neutral") ]])
        fnCutscene([[ Append("CrowbarChan:[E|Neutral] Sorry about the chilly reception, but we've been through a lot.[P] Hey, Christine, what's it like getting your own chapter?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Chapter?[B][C]") ]])
        fnCutscene([[ Append("CrowbarChan:[E|Neutral] Some people on the forums have been asking for a CrowbarChan chapter or spin-off game![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] ...[P] The Regulus City message boards?[P] Is that what you meant?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] This unit's cognitive capacity is likely limited.[P] It is probably similar to a PDU.[B][C]") ]])
        fnCutscene([[ Append("CrowbarChan:[E|Neutral] Great idea![P] CrowbarChan and PDU team up to fight the bad guys![B][C]") ]])
        fnCutscene([[ Append("CrowbarChan:[E|Neutral] I'll be borrowing that idea, but I'll give you writing credit if you want~.") ]])
    end
end
