-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
--First time you enter the area.
if(sObjectName == "CheckBuilding") then

    --Repeat check.
    local iSCryoSawUnpoweredDoor = VM_GetVar("Root/Variables/Chapter5/Scenes/iSCryoSawUnpoweredDoor", "N")
    if(iSCryoSawUnpoweredDoor == 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iSCryoSawUnpoweredDoor", "N", 1.0)
    
    --Scene.
    fnCutsceneMergeParty()
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Christine", 35.25, 15.50)
    fnCutsceneFace("Christine", 0, -1)
    fnCutsceneMove("Tiffany", 36.25, 15.50)
    fnCutsceneFace("Tiffany", -1, -1)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Hmmm...[P] Door isn't receiving power, probably because of the damage to the facility.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|PDU] [ADDCHAR|PDU|4|Neutral]PDU, can you run a scan for me?[B][C]") ]])
    fnCutscene([[ Append("PDU:[E|Quiet] Beginning short-range scan...[B][C]") ]])
    fnCutscene([[ Append("PDU:[E|Alarmed] Attention::[P] An unidentified power cycler has been detected on the other side of this door.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Please inform a non-repair unit what that indicates.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] There's a survivor in this building, 55![P] She's probably in system standby...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] Damn...[P] The bolts are thrown, we're not wedging this door open.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Smirk] Could we shoot the door open?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] No, that'd risk damaging whoever is on the other side or bringing the building down.[P] This is still under construction, who knows what sort of damage it can survive.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Unfortunate.[P] Identify other options, Unit 771852.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Affirmative.[P] Other options...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] The deadbolts are down, but I could use a power supply to jump them...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneFace("Tiffany", -1, 0)
    fnCutsceneMove("Christine", 32.25, 15.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Battery disappears.
    fnCutscene([[ AudioManager_PlaySound("World|TakeItem") ]])
    fnCutsceneLayerDisabled("Battery", true)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Christine", 35.25, 15.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|PDU] This power supply wasn't full, or else it ran itself down when the main power was cut...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] There will be more like it.[P] I can siphon the power out and put it in this one.[P] If we can find enough portable batteries, I can jump the door's bolts and we can get it open safely.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Smirk] Excellent work, Unit 771852.[P] You are a superb repair unit.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Laugh] Thank you, Command Unit![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Laugh] The way to get you to smile is to be useful?[P] I'm really good at that![P] Let's get searching!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Finish up.
    fnCutsceneMove("Tiffany", 35.25, 15.50)
    fnCutsceneBlocker()
    fnAutoFoldParty()
    fnCutsceneBlocker()

--Prevent Christine from not checking the construction site first.
elseif(sObjectName == "MoveBack") then

    --Repeat check.
    local iSCryoBatteryPickups = VM_GetVar("Root/Variables/Chapter5/Scenes/iSCryoBatteryPickups", "N")
    if(iSCryoBatteryPickups >= 4.0) then return end
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
    fnCutscene([[ Append("55:[E|Neutral] Christine, if I may.[P] I sighted a backup cable heading south towards the foundation construction site.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] I recommend we check that area for spare power supplies.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Good thinking, 55.[P] Let's check that area first.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Christine", 19.25, 17.50)
    fnCutsceneMove("Tiffany", 19.25, 17.50)
    fnCutsceneBlocker()
    fnAutoFoldParty()
    fnCutsceneBlocker()

end
