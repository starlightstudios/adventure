-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
--Sample object.
if(sObjectName == "Trigger") then

    --Repeat check.
    local iSCryoPowerCoreScene = VM_GetVar("Root/Variables/Chapter5/Scenes/iSCryoPowerCoreScene", "N")
    if(iSCryoPowerCoreScene == 1.0) then return end
    
    --Achievement.
    AM_SetPropertyJournal("Unlock Achievement", "FinishCryo")
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iSCryoPowerCoreScene", "N", 1.0)
    
    --Variables.
	local iCutscene50       = VM_GetVar("Root/Variables/Chapter5/Scenes/iCutscene50", "N")
	local iSawFirstEDGScene = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawFirstEDGScene", "N")
    
    --Merge party for the scene.
    fnCutsceneMergeParty()
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Christine", 17.25, 13.50)
    fnCutsceneMove("Tiffany", 16.35, 13.50)
    fnCutsceneBlocker()
    fnCutsceneMove("Christine", 25.25, 13.50)
    fnCutsceneFace("Christine", 0, -1)
    fnCutsceneMove("Tiffany", 24.25, 13.50)
    fnCutsceneFace("Tiffany", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Camera moves.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 2.0)
        CameraEvent_SetProperty("Focus Position", (25.25 * gciSizePerTile), (8.50 * gciSizePerTile))
    DL_PopActiveObject()
    fnCutsceneBlocker()
    fnCutsceneWait(185)
    fnCutsceneBlocker()
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 2.0)
        CameraEvent_SetProperty("Focus Actor Name", "Christine")
    DL_PopActiveObject()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("Christine:[E|Sad] 55...[P] What is this?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] The geothermal exchange core recently activated to power the Cryogenics facility.[P] The core extracts heat from the subsurface of Regulus and conducts it here with a series of superconducting rods.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Heated air with an eighty-percent nitrogen content then cycles through a series of pico-filters which convert the heat energy into electrical charge.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] An unidentified organic material seems to be using the core's waste heat.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Sad] No, it's more than that.[P] The core is...[P] part of it, now.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Sad] The whole facility is now slowly becoming its blood vessels.[P] It's alive.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] It's -[P] alive![P] Wonderful![P] Spectacular![B][C]") ]])
    fnCutscene([[ Append("55:[E|Upset] Organic material of the type we have seen is not native to Regulus, Christine.[P] This should not be.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] But it is![P] Do you not understand the wonder of what you are seeing with your material eyes?[P] Peek beyond the veil![B][C]") ]])
    
    --Christine has not encountered Vivify or reached the end of the mines:
    if(iCutscene50 == 0.0 and iSawFirstEDGScene == 0.0) then
        fnCutscene([[ Append("55:[E|Upset] Explain your statement, right now.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] S-[P]sorry![P] I'm not really sure what it means, either.[P] I look at that thing and thoughts pop into my head...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Memories, memories of things that have yet to happen but do happen.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] Why do I have these memories?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Like a section of a memory drive with unidentified encrypted data?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] Yeah...[P] Why?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] It was mentioned on the logs stored in this facility that I found when I woke up.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Most of the drives had been purged but several audio files recorded this phenomena shortly before the battle broke out.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] I derived my plan to smuggle your memory files from that pattern.[P] I now have a better idea of its origin.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] But...[P] this is different, so different.[P] This is unlike what came before it.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] We -[P] we need to shut this core down and kill this thing![P] Right now![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] N-[P]no, wait![P] We can't do that![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] But it's dangerous![B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Are you arguing with me, or yourself?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] ...[P] 55, I don't think I can trust myself on this.[P] I simply can't.[P] I don't even know which thoughts are mine right now.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] Listen, whatever you decide to do, I'll respect it unconditionally.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] I appreciate the sentiment.[P] What if I decide to shut down the core?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] You have no right to murder this creature, whatever it is.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] It has a right to exist, same as we do.[P] Even if it's dangerous, you don't have that right.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] And if I leave the core as it is?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] ...[P] It will consume us all...[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] This does not seem like a difficult decision.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] Because you don't view killing as an abominable crime![P] But you should, you god damn monster![B][C]") ]])
        fnCutscene([[ Append("55:[E|Down] ...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Cry] N-[P]no![P] I'm sorry, 55, I didn't mean it![B][C]") ]])
        fnCutscene([[ Append("55:[E|Down] It is all right.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] I will make my decision in a moment.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] (I can shut down the core by checking the terminals just south of here.[P] It should be easy to override the security.)") ]])
        
    --Christine met Vivify, putting this into context:
    elseif(iSawFirstEDGScene == 1.0) then
        fnCutscene([[ Append("55:[E|Upset] Explain your statement, right now.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] S-[P]sorry![P] I'm not really sure what it means, either.[P] I look at that thing and thoughts pop into my head...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Memories, memories of things that have yet to happen but do happen.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Vivify...[P] It all has to do with Project Vivify...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] Did she put these here?[P] Did I absorb them?[P] Were they always there?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Like a section of a memory drive with unidentified encrypted data?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] Yeah, it kind of is like that. Why?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] It was mentioned on the logs stored in this facility that I found when I woke up.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Most of the drives had been purged but several audio files recorded this phenomena shortly before the battle broke out.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] I derived my plan to smuggle your memory files from that pattern.[P] I now have a better idea of its origin.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] But...[P] this is different.[P] I don't think Vivify even knows about this...[P] thing...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] We -[P] we need to shut this core down and kill this thing![P] Right now![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] N-[P]no, wait![P] We can't do that![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] But it's dangerous![B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Are you arguing with me, or yourself?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] ...[P] 55, I don't think I can trust myself on this.[P] I simply can't.[P] I don't even know which thoughts are mine right now.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] Listen, whatever you decide to do, I'll respect it unconditionally.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] I appreciate the sentiment.[P] What if I decide to shut down the core?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] You have no right to murder this creature, whatever it is.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] It has a right to exist, same as we do.[P] Even if it's dangerous, you don't have that right.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] And if I leave the core as it is?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] ...[P] It will consume us all...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] ...[P] And she will cry...[P] Does it even matter what we do?[P] Can I even change anything for her?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Who?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] My master, the one we call Project Vivify.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] This does not seem like a difficult decision.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] Because you don't view killing as an abominable crime![P] But you should, you god damn monster![B][C]") ]])
        fnCutscene([[ Append("55:[E|Down] ...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Cry] N-[P]no![P] I'm sorry, 55, I didn't mean it![B][C]") ]])
        fnCutscene([[ Append("55:[E|Down] It is all right.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] I will make my decision in a moment.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] (I can shut down the core by checking the terminals just south of here.[P] It should be easy to override the security.)") ]])
    
    --Christine reached the base of the mines but has not met Vivify:
    else
        fnCutscene([[ Append("55:[E|Upset] Explain your statement, right now.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] S-[P]sorry![P] I'm not really sure what it means, either.[P] I look at that thing and thoughts pop into my head...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Memories, memories of things that have yet to happen but do happen.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Just like in the Tellurium Mines...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] Did 609144 put these here?[P] Did I absorb them?[P] Were they always there?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Like a section of a memory drive with unidentified encrypted data?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] Yeah, it kind of is like that. Why?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] It was mentioned on the logs stored in this facility that I found when I woke up.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Most of the drives had been purged but several audio files recorded this phenomena shortly before the battle broke out.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] I derived my plan to smuggle your memory files from that pattern.[P] I now have a better idea of its origin.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] But...[P] it was dead before.[P] This -[P] this is alive...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] We -[P] we need to shut this core down and kill this thing![P] Right now![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] N-[P]no, wait![P] We can't do that![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] But it's dangerous![B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Are you arguing with me, or yourself?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] ...[P] 55, I don't think I can trust myself on this.[P] I simply can't.[P] I don't even know which thoughts are mine right now.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] Listen, whatever you decide to do, I'll respect it unconditionally.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] I appreciate the sentiment.[P] What if I decide to shut down the core?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] You have no right to murder this creature, whatever it is.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] It has a right to exist, same as we do.[P] Even if it's dangerous, you don't have that right.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] And if I leave the core as it is?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] ...[P] It will consume us all...[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] This does not seem like a difficult decision.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] Because you don't view killing as an abominable crime![P] But you should, you god damn monster![B][C]") ]])
        fnCutscene([[ Append("55:[E|Down] ...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Cry] N-[P]no![P] I'm sorry, 55, I didn't mean it![B][C]") ]])
        fnCutscene([[ Append("55:[E|Down] It is all right.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] I will make my decision in a moment.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] (I can shut down the core by checking the terminals just south of here.[P] It should be easy to override the security.)") ]])
    end
    fnCutsceneBlocker()
    fnAutoFoldParty()
    fnCutsceneBlocker()

end
