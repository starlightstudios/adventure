-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Execution]|
--Exit door.
if(sObjectName == "ToCryoCommandA") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusCryoCommandA", "FORCEPOS:4.5x9.0x0")
	
--Intercom.
elseif(sObjectName == "Intercom") then

	LM_ExecuteScript(gsRoot .. "Chapter 5/Dialogue/Intercom/Root.lua", "Hello")

--Exit.
elseif(sObjectName == "ToExteriorSA") then

	--Variables.
	local iMet55 = VM_GetVar("Root/Variables/Chapter5/Scenes/iMet55", "N")
	local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
	if(iMet55 == 0.0 or sChristineForm == "Human" or sChristineForm == "Raiju") then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[SOUND|World|AutoDoorFail]Access denied.[P] Subject would not survive in a vacuum.[P] Please secure the subject before transport.") ]])
		fnCutsceneBlocker()
	else
		AudioManager_PlaySound("World|AutoDoorOpen")
		AL_BeginTransitionTo("RegulusExteriorSA", "FORCEPOS:30.5x26.0x0")
	end

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
