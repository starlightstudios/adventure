-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
if(sObjectName == "ToRegulusCryoPowerCoreC") then
    AudioManager_PlaySound("World|ClimbLadder")
    AL_BeginTransitionTo("RegulusCryoPowerCoreC", "FORCEPOS:47.0x18.0x0")
    
elseif(sObjectName == "ToRegulusCryoLower") then
    
    --Variables.
	local iFixedLowerDDoor  = VM_GetVar("Root/Variables/Chapter5/Scenes/iFixedLowerDDoor", "N")
    
    --Door is fixed.
    if(iFixedLowerDDoor == 1.0) then
        AudioManager_PlaySound("World|AutoDoorOpen")
        AL_BeginTransitionTo("RegulusCryoLowerD", "FORCEPOS:29.0x28.0x0")

    --Error.
    else
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[VOICE|Leader](This section isn't receiving power because the bridge line was cut.[P] I should check the wires in this room.)") ]])
		fnCutsceneBlocker()
    end
    
-- |[Examinables]|
elseif(sObjectName == "WiresN") then
	local iFixedLowerDDoor  = VM_GetVar("Root/Variables/Chapter5/Scenes/iFixedLowerDDoor", "N")
    if(iFixedLowerDDoor == 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[VOICE|Leader](There doesn't seem to be a problem with the wiring here.[P] Maybe the other exposed wires?)") ]])
		fnCutsceneBlocker()
    end
    
elseif(sObjectName == "WiresS") then
	local iFixedLowerDDoor  = VM_GetVar("Root/Variables/Chapter5/Scenes/iFixedLowerDDoor", "N")
    if(iFixedLowerDDoor == 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[VOICE|Leader](Here's the problem.[P] Someone snipped the main bus wire to install a new set of converters, and didn't fix it when they were done.[P] I'll just fix this and the doors here should open normally.)") ]])
		fnCutsceneBlocker()
        VM_SetVar("Root/Variables/Chapter5/Scenes/iFixedLowerDDoor", "N", 1.0)
    end

elseif(sObjectName == "Shelf") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Nothing salvageable here...)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "Box") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Burnt computer chips, not worth recovering.)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "Battery") then
    local iSCryoBatteryE = VM_GetVar("Root/Variables/Chapter5/Scenes/iSCryoBatteryE", "N")
    if(iSCryoBatteryE == 0.0) then
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSCryoBatteryE", "N", 1.0)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSCryoBatteryPickups", "N", 5.0)
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] [SOUND|World|TakeItem](The last battery![P] My power supply should have enough to open that door we found outside!)") ]])
        fnCutsceneBlocker()
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (I already siphoned the power from this battery.)") ]])
        fnCutsceneBlocker()
    end

-- |[Other]|
-- |[Debug]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
