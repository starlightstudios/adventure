-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Execution]|
--Exit door.
if(sObjectName == "ToContainmentAR") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusCryoContainmentA", "FORCEPOS:18.0x3.0x0")
	
--Exit door.
elseif(sObjectName == "ToContainmentAL") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusCryoContainmentA", "FORCEPOS:4.0x4.0x0")

--Door locked due to a breach.
elseif(sObjectName == "LockedDoor") then

	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[SOUND|World|AutoDoorFail] Access denied.[P] Breach detected in chamber.[P] Radiation levels exceeding safe levels by 3000 percent.[P] Please contact a cleanup crew.") ]])
	fnCutsceneBlocker()

--Retired doll.
elseif(sObjectName == "GolemA") then

	--Variables.
	local iHasDollMotivatorB = VM_GetVar("Root/Variables/Chapter5/Scenes/iHasDollMotivatorB", "N")

	--Already has the motivator.
	if(iHasDollMotivatorB == 1.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("PDU: Scanning::[P] Unit #3144.[P] Status::[P] Retired.[B][C]") ]])
		fnCutscene([[ Append("PDU: Likely cause of retirement::[P] Massive blunt trauma.[P] Unit was thrown into the nearby wall at high velocity.[P] All systems suffered severe damage from the shock.[P] System offline was instantaneous.[B][C]") ]])
		fnCutscene([[ Append("PDU: All remaining subsystems are severely damaged.[P] The authenticator chip has been removed.[P] An incision in the back of the head is the likely removal vector.[P] The incision was made some time after system offline took place.") ]])
		fnCutsceneBlocker()
	
	--Pick up the motivator.
	else
		VM_SetVar("Root/Variables/Chapter5/Scenes/iHasDollMotivatorB", "N", 1.0)
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("PDU: Scanning::[P] Unit #3144.[P] Status::[P] Retired.[B][C]") ]])
		fnCutscene([[ Append("PDU: Likely cause of retirement::[P] Massive blunt trauma.[P] Unit was thrown into the nearby wall at high velocity.[P] All systems suffered severe damage from the shock.[P] System offline was instantaneous.[B][C]") ]])
		fnCutscene([[ Append("PDU: Subsystem scan completed.[P] Left primary leg motivator is of acceptable condition.[B][C]") ]])
		fnCutscene([[ Append("PDU: [SOUND|World|TakeItem]The left primary motivator has been extracted and added to your inventory.[B][C]") ]])
		fnCutscene([[ Append("PDU: All remaining subsystems are severely damaged.[P] The authenticator chip has been removed.[P] An incision in the back of the head is the likely removal vector.[P] The incision was made an unknown amount of time after system offline took place.") ]])
		fnCutsceneBlocker()
	end

--Retired doll.
elseif(sObjectName == "GolemB") then

	--Variables.
	local iHasDollOcular = VM_GetVar("Root/Variables/Chapter5/Scenes/iHasDollOcular", "N")

	--Already has the motivator.
	if(iHasDollOcular == 1.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("PDU: Scanning::[P] Unit #500932.[P] Status::[P] Retired.[B][C]") ]])
		fnCutscene([[ Append("PDU: Likely cause of retirement::[P] Severe sonic damage.[P] Unit was struck by an extremely high-frequency sound wave and went into system standby as a defense mechanism as subsystems began to vibrate apart.[P] The sound wave continued to be applied until the CPU shattered.[P] The unit is permanently offline.[B][C]") ]])
		fnCutscene([[ Append("PDU: The authenticator chip has been removed, likely by an incision in the back of the head.[P] Estimates indicate the incision was made shortly after the high-frequency waves damaged the other systems.") ]])
		fnCutsceneBlocker()
	
	--Pick up the motivator.
	else
		VM_SetVar("Root/Variables/Chapter5/Scenes/iHasDollOcular", "N", 1.0)
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("PDU: Scanning::[P] Unit #500932.[P] Status::[P] Retired.[B][C]") ]])
		fnCutscene([[ Append("PDU: Likely cause of retirement::[P] Severe sonic damage.[P] Unit was struck by an extremely high-frequency sound wave and went into system standby as a defense mechanism as subsystems began to vibrate apart.[P] The sound wave continued to be applied until the CPU shattered.[P] The unit is permanently offline.[B][C]") ]])
		fnCutscene([[ Append("PDU: Subsystem scan completed.[P] Right ocular unit did not suffer damage from the high-frequency wave, and is of acceptable condition.[B][C]") ]])
		fnCutscene([[ Append("PDU: [SOUND|World|TakeItem]The right ocular unit has been extracted and added to your inventory.[B][C]") ]])
		fnCutscene([[ Append("PDU: The authenticator chip has been removed, likely by an incision in the back of the head.[P] Estimates indicate the incision was made shortly after the high-frequency waves damaged the other systems.") ]])
		fnCutsceneBlocker()
	end

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
