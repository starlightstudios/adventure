-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
--Cutscene.
if(sObjectName == "WreckedFabricator") then
	
	-- |[Checks]|
	--Don't show this twice.
	local iSawRuinedFabricator = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawRuinedFabricator", "N")
	if(iSawRuinedFabricator == 1) then return end
	
	-- |[Flags]|
	--Set the flag to prevent playing this twice.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iSawRuinedFabricator", "N", 1.0)
	
	--Wait a bit.
	fnCutsceneWait(30)
	fnCutsceneBlocker()
	
	-- |[Look Around]|
	fnCutsceneFace("Christine", 1, 0)
	fnCutsceneWait(30)
	fnCutsceneBlocker()
	fnCutsceneFace("Christine", 1, -1)
	fnCutsceneWait(30)
	fnCutsceneBlocker()
	fnCutsceneFace("Christine", 0, -1)
	fnCutsceneWait(30)
	fnCutsceneBlocker()
	fnCutsceneFace("Christine", -1, -1)
	fnCutsceneWait(30)
	fnCutsceneBlocker()
	fnCutsceneFace("Christine", -1, 0)
	fnCutsceneWait(30)
	fnCutsceneBlocker()
	
	-- |[Dialogue]|
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
	fnCutscene([[ Append("Chris:[E|Neutral][VOICE|ChrisMaleVoice] This place is in worse shape than the rest of the building...[B][C]") ]])
	fnCutscene([[ Append("Chris:[E|Neutral][VOICE|ChrisMaleVoice] It looks like a bomb went off in here.[P] It's all scrap.[B][C]") ]])
	fnCutscene([[ Append("Chris:[E|Neutral][VOICE|ChrisMaleVoice] That lady on the intercom is going to be very cross with me.[P] What am I going to tell her?[B][C]") ]])
	fnCutscene([[ Append("Chris:[E|Neutral][VOICE|ChrisMaleVoice] Better look around.[P] There might be a shorter way back to the main area...") ]])
	fnCutsceneBlocker()

end
