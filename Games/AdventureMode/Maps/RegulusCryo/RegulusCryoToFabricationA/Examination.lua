-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Execution]|
--Exit ladder.
if(sObjectName == "ToCryoG") then
	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("RegulusCryoG", "FORCEPOS:58.0x17.0x0")

--Exit ladder.
elseif(sObjectName == "ToFabricationB") then
	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("RegulusCryoToFabricationB", "FORCEPOS:27.0x22.0x0")

--Door to the fabrication tunnels.
elseif(sObjectName == "DoorA") then
	
	--Variables.
	local iHasFabricationAAuth = VM_GetVar("Root/Variables/Chapter5/Scenes/iHasFabricationAAuth", "N")
	if(iHasFabricationAAuth == 1.0) then
		AL_SetProperty("Open Door", "DoorA")
		AudioManager_PlaySound("World|AutoDoorOpen")
		
	--First-time indicator.
	elseif(iHasFabricationAAuth == 2.0) then
		VM_SetVar("Root/Variables/Chapter5/Scenes/iHasFabricationAAuth", "N", 1.0)
		AL_SetProperty("Open Door", "DoorA")
		AudioManager_PlaySound("World|AutoDoorOpen")
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Access granted.[P] Have an exceedingly nice day.") ]])
		fnCutsceneBlocker()
	
	--Warning.
	else
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[SOUND|World|AutoDoorFail]Access denied.[P] Please contact your administrator for assistance.") ]])
		fnCutsceneBlocker()

	end

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
