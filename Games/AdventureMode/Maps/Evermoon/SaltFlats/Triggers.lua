-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
--Triggers the meeting with Adina if Mei needs it.
if(sObjectName == "MeetAdina") then

	--Variables.
	local sMeiForm             = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	local iHasMetAdina         = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iHasMetAdina", "N")
	local iHasAlrauneForm      = VM_GetVar("Root/Variables/Global/Mei/iHasAlrauneForm", "N")
	local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
	
	--Has not met Adina, fire a cutscene.
	if(iHasMetAdina == 0.0) then
		
		-- |[Flags]|
		--Prevent scene from firing twice.
		VM_SetVar("Root/Variables/Chapter1/SaltFlats/iHasMetAdina", "N", 1.0)
		
		--Store which form Mei first met Adina in.
		VM_SetVar("Root/Variables/Chapter1/Scenes/sAdinaFirstSceneForm", "S", sMeiForm)
		
		-- |[Music Change]|
		AL_SetProperty("Music", "AdinasTheme")
        
        -- |[Achievement]|
        AM_SetPropertyJournal("Unlock Achievement", "MeetAdina")
		
		-- |[Movement]|
		--Move Mei forward.
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Move To", (35.25 * gciSizePerTile), (15.50 * gciSizePerTile))
		DL_PopActiveObject()
		
		--If Florentina is here, move her as well.
		if(bIsFlorentinaPresent == true) then
			Cutscene_CreateEvent("ActorEvent", "Actor")
				ActorEvent_SetProperty("Subject Name", "Florentina")
				ActorEvent_SetProperty("Move To", (35.25 * gciSizePerTile), (14.50 * gciSizePerTile))
			DL_PopActiveObject()
		end
		
		--Adina faces west.
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Adina")
			ActorEvent_SetProperty("Face", -1, 0)
		DL_PopActiveObject()
		fnCutsceneBlocker()

		--Wait a bit.
		fnCutsceneWait(25)
		fnCutsceneBlocker()

		--Adina faces Mei.
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Adina")
			ActorEvent_SetProperty("Face", 0, -1)
		DL_PopActiveObject()
		fnCutsceneBlocker()

		--Wait a bit.
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		-- |[Dialogue]|
		--Setup.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])

		--Baseline: Mei is Human, Florentina is not present, and Mei has never been an Alraune.
		if(sMeiForm == "Human" and bIsFlorentinaPresent == false and iHasAlrauneForm == 0.0) then

			fnCutscene([[ Append("Alraune: Greetings, human.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Surprise] You're not going to attack me?[B][C]") ]])
			fnCutscene([[ Append("Alraune: I am rather tired. If it's all the same to you, I'd rather not.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Happy] What a relief![B][C]") ]])
			fnCutscene([[ Append("Alraune: You look tired as well. Would you like to rest for a moment?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Laugh] Well that'd -[P][CLEAR]") ]])
			fnCutscene([[ Append("Mei:[E|Offended] Oh, no![P] That's the oldest trick in the book![P] No way am I falling for that one.[B][C]") ]])
			fnCutscene([[ Append("Alraune: Interesting.[P] You do not trust me.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Did I make it obvious?[P][CLEAR]") ]])
			fnCutscene([[ Append("Alraune: What reason do you have to not trust me, human?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Huh?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Offended] You've got it backwards.[P] I need a pretty good reason to trust you, otherwise -[P] no way.[B][C]") ]])
			fnCutscene([[ Append("Alraune: Ah, I see.[P] Humans are paranoid by nature.[P] I apologize if I seemed too forward.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] You -[P] didn't know?[B][C]") ]])
			fnCutscene([[ Append("Alraune: We Alraunes trust one another, you see.[P] Perhaps if I introduced myself?[B][C]") ]])
			fnCutscene([[ Append("Adina: My name is Adina. I tend to these salted earths.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Mei.[B][C]") ]])
			fnCutscene([[ Append("Adina: A sweet name.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Happy] Maybe if we got to know one another, I could trust you.[P] Your cohorts haven't given me a reason to.[B][C]") ]])
			fnCutscene([[ Append("Adina: They are merely assuming that you are a danger.[P] You *are* openly brandishing a weapon.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] I'll have to give you that one...[B][C]") ]])
			fnCutscene([[ Append("Adina: Stay as long as you like, Mei.[P] I have made sure the creatures of the forest are kept away, they will not threaten you here.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] (She seems nice, but I bet something is up.[P] Better be careful.)") ]])
		
		--Mei is an Alraune, but Florentina is not present.
		elseif(sMeiForm == "Alraune" and bIsFlorentinaPresent == false) then

			fnCutscene([[ Append("Alraune: Greetings, leaf-sister.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] You are...[P] leaf-sister Adina?[B][C]") ]])
			fnCutscene([[ Append("Adina: Correct.[P] I see the little ones cannot help but gossip.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Happy] Has it ever been otherwise?[B][C]") ]])
			fnCutscene([[ Append("Adina: Have they told you the nature of my work here?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Some, but not the whole story.[P] Please.[P] Enlighten me.[B][C]") ]])
			fnCutscene([[ Append("Adina: Certainly.[B][C]") ]])
			fnCutscene([[ Append("Adina: Long ago, before the river wound this way, this was the site of a battle.[P] Two armies of humans clashed here.[B][C]") ]])
			fnCutscene([[ Append("Adina: Like all human conflicts, it was pointless and soon forgotten.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Of course.[B][C]") ]])
			fnCutscene([[ Append("Adina: However, to turn the tide of war, a mage brought forth molten stone from the sky.[P] It showered his enemies and scarred them horribly.[B][C]") ]])
			fnCutscene([[ Append("Adina: The battle was won for him, but the stone was rich in salt.[P] It scoured the ground, and no life grows here.[B][C]") ]])
			fnCutscene([[ Append("Adina: Even with periodic rains and the river there, it will be centuries before the salt has washed out.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] So you take it upon yourself to remove the salt?[P] How?[B][C]") ]])
			fnCutscene([[ Append("Adina: I have found several species of little ones that can survive in saline conditions.[P] They have agreed to help me, so I transplanted them here.[B][C]") ]])
			fnCutscene([[ Append("Adina: Their descendants are here, reclaiming the earth, but the work is hard.[P] They need constant attention.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Happy] But with your dedication, the soil is slowly recovering.[B][C]") ]])
			fnCutscene([[ Append("Adina: Do you consider me a fool for this errand?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Me?[P] Of course not, leaf-sister.[P] You're changing the world for the better![B][C]") ]])
			fnCutscene([[ Append("Adina: But the other leaf-sisters...[P][CLEAR]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] You're likely to succeed if you try![B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Offended] Don't let them get you down.[P] They'll be jealous when you have reclaimed the earth here.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Happy] Well, not jealous.[P] That's such a human emotion.[B][C]") ]])
			fnCutscene([[ Append("Adina: You are a recently joined leaf-sister, are you not?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] I am.[B][C]") ]])
			fnCutscene([[ Append("Adina: Your support is appreciated.[P] You are welcome to stay here as long as you like.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] I'm currently on a journey of my own, but perhaps a moment or two here would not be wasted.[B][C]") ]])
			fnCutscene([[ Append("Adina: If there's anything I can do to help, please ask.") ]])
	
		--Mei is a bee or slime, Florentina is not present:
		elseif((sMeiForm == "Bee" or sMeiForm == "Slime" or sMeiForm == "Werecat" or sMeiForm == "Ghost" or sMeiForm == "Gravemarker" or sMeiForm == "Wisphag" or sMeiForm == "Mannequin") and bIsFlorentinaPresent == false) then

			fnCutscene([[ Append("Alraune: Ah, it seems you have wandered across the wards.[P] You are a determined creature.[B][C]") ]])
			
			if(sMeiForm == "Bee") then
				fnCutscene([[ Append("Mei:[E|Surprise] Wards?[B][C]") ]])
				fnCutscene([[ Append("Alraune: And it speaks![P] Most intriguing![B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Neutral] Of course I can speak![P] Oh, oops.[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Happy] I sometimes forget I'm a bee.[P] It's so natural, I feel like I've been one my whole life![B][C]") ]])
				fnCutscene([[ Append("Alraune: What is your name, forest-creature?[B][C]") ]])
			elseif(sMeiForm == "Slime") then
				fnCutscene([[ Append("Mei:[E|Surprise] Wards?[B][C]") ]])
				fnCutscene([[ Append("Alraune: And it speaks![P] Most intriguing![B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Neutral] Of course I can speak![P] Oh, oops.[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Neutral] I guess the other slimes don't talk.[P] I forgot.[B][C]") ]])
				fnCutscene([[ Append("Alraune: What is your name, forest-creature?[B][C]") ]])
			elseif(sMeiForm == "Werecat") then
				fnCutscene([[ Append("Mei:[E|Smirk] You mean the minor tingling?[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Laugh] Those were meant to keep me out?[P] Ha ha![P] Hilarious![B][C]") ]])
				fnCutscene([[ Append("Alraune: What is your name, forest-creature?[B][C]") ]])
			elseif(sMeiForm == "Ghost") then
				fnCutscene([[ Append("Mei:[E|Smirk] I didn't notice anything.[P] Are you sure they're working?[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Neutral] I would like to assure you I come in peace.[B][C]") ]])
				fnCutscene([[ Append("Alraune: What is your name, wilted-one?[B][C]") ]])
			elseif(sMeiForm == "Gravemarker") then
				fnCutscene([[ Append("Mei:[E|Smirk] Your magic would have stung, but I have no skin to irritate.[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Neutral] I would like to assure you I come in peace.[B][C]") ]])
				fnCutscene([[ Append("Alraune: What is your name, angel?[B][C]") ]])
			elseif(sMeiForm == "Wisphag") then
				fnCutscene([[ Append("Mei:[E|Smirk] My skin's pretty thick, after all.[P] Don't worry, I'm not here for a fight.[B][C]") ]])
				fnCutscene([[ Append("Alraune: What is your name, crone?[B][C]") ]])
			elseif(sMeiForm == "Mannequin") then
				fnCutscene([[ Append("Mei:[E|Smirk] I don't think I'd be able to feel the sting.[B][C]") ]])
				fnCutscene([[ Append("Alraune: What is your name, thing?[B][C]") ]])
			end
			
			fnCutscene([[ Append("Mei:[E|Neutral] Mei.[P] You?[B][C]") ]])
			fnCutscene([[ Append("Adina: Adina.[P] I tend these salted earths.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] What was that about wards?[B][C]") ]])
			fnCutscene([[ Append("Adina: I have placed some minor magical wards to keep the forest creatures away.[P] My work is difficult, so I cannot entertain visitors easily.[B][C]") ]])
			if(sMeiForm == "Gravemarker") then
                fnCutscene([[ Append("Mei:[E|Neutral] I get the feeling it wouldn't do much to a being of stone.[B][C]") ]])
			elseif(sMeiForm == "Mannequin") then
                fnCutscene([[ Append("Mei:[E|Neutral] I get the feeling it wouldn't do much to my resin and wooden body.[B][C]") ]])
            else
                fnCutscene([[ Append("Mei:[E|Neutral] I felt a bit of a stinging feeling, but it wasn't much.[B][C]") ]])
            end
			fnCutscene([[ Append("Adina: Your willpower, it seems, exceeds that of your kind.[P] Since you are not aggressive, I see no reason to repulse you.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Happy] Thanks for that, I guess.[P] So what are you doing here?[B][C]") ]])
			fnCutscene([[ Append("Adina: The soil here was salted centuries ago in a battle between two human armies.[P] I am caring for these plants as they bravely extract the salt.[B][C]") ]])
			fnCutscene([[ Append("Adina: It is difficult work, but I know this land will be reclaimed in the end.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] You've got passion for your work.[P] I can respect that.[P] I hope you succeed.[B][C]") ]])
			if(sMeiForm == "Gravemarker") then
                fnCutscene([[ Append("Adina: Mm, might I convince you to stay?[P] A worker made of stone would be extremely capable.[B][C]") ]])
                fnCutscene([[ Append("Adina: I don't think I can offer you anything save companionship.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Laugh] Are you flirting with me?[B][C]") ]])
                fnCutscene([[ Append("Adina: A little.[P] You are, indeed, beautiful.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Blush] But what would the chorus think...[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Blush] Could I work here and have a dalliance with this lovely alraune?[B][C]") ]])
                fnCutscene([[ Append("Adina: Angels dislike other partirhumans.[P] I am surprised you do not know this.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] I'm not this way entirely by choice, you see.[P] Though I do respect the chorus.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] At any rate, even if I did want to stay, I still have a few obligations I must attend to, first.[B][C]") ]])
                fnCutscene([[ Append("Adina: I see.[P] I wish you well, whatever your errands.[P] And if you do come back, I will be here.") ]])
			elseif(sMeiForm == "Mannequin") then
                fnCutscene([[ Append("Adina: Mm, might I convince you to stay?[P] A worker made of resin and wood would be extremely capable.[B][C]") ]])
                fnCutscene([[ Append("Adina: I don't think I can offer you anything save companionship.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Are you flirting with me?[B][C]") ]])
                fnCutscene([[ Append("Adina: A little.[P] You are, indeed, beautiful. Then again, you are meant to be a thing of beauty, to entice others to purchase clothes.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Oh, stop.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Nice as you are, I have a lot of things I need to do before I make any sort of commitment.[B][C]") ]])
                fnCutscene([[ Append("Adina: I see.[P] I wish you well, whatever your errands.[P] And if you do come back, I will be here.") ]])
			elseif(sMeiForm == "Wisphag") then
                fnCutscene([[ Append("Adina: We are kindred spirits.[P] I have seen the wisphags of the swamps.[P] Your crone sisters guide the lost on to the next life.[B][C]") ]])
                fnCutscene([[ Append("Adina: We are both seeking to put right something that once went wrong.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] I can't really take credit for that, but...[P] do you have any idea why there are so many lost souls in Starfield Swamp?[B][C]") ]])
                fnCutscene([[ Append("Adina: I am well versed in this region's histories, and while there might be some army being massacred or drowning in the swamp...[P] no, it is not enough.[B][C]") ]])
                fnCutscene([[ Append("Adina: There must be some other thing beneath it that traps so many souls.[P] Yes, that is the only explanation.[P] Sorry, but that's as good a guess as I have.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Works for me.[P] Thanks for your insight.") ]])
            else
                fnCutscene([[ Append("Adina: Thank you.[P] Whatever your errands, I hope they meet with success.") ]])
            end
	
		--Mei is not an Alraune, and Florentina is not present, but Mei has access to Alraune form.
		elseif(sMeiForm ~= "Alraune" and bIsFlorentinaPresent == false and iHasAlrauneForm == 1.0) then
	
			--Flags.
			VM_SetVar("Root/Variables/Chapter1/SaltFlats/iHasMetAdina", "N", 10.0)
			VM_SetVar("Root/Variables/Chapter1/Scenes/sAdinaFirstSceneForm", "S", "Completed")
            VM_SetVar("Root/Variables/Chapter1/Scenes/iAdinaQuestVar", "N", 2.0)

			--Dialogue.
			fnCutscene([[ Append("Alraune: Greetings, leaf-sister Mei.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Surprise] ![B][C]") ]])
			fnCutscene([[ Append("Alraune: Do not look so surprised![P] The little ones mentioned you were coming this way.[B][C]") ]])
			fnCutscene([[ Append("Alraune: A leaf-sister who can change her form.[P] Most intriguing.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Happy] Seems they can't keep a secret.[B][C]") ]])
			fnCutscene([[ Append("Alraune: Not even for a moment![P] Nothing is hidden from them.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Well that's all right.[P] Honestly, the other Alraunes don't seem to recognize me.[P] We often...[P] come to blows...[B][C]") ]])
			
			--Form-sensitive. First, Human.
			if(sMeiForm == "Human") then
				fnCutscene([[ Append("Alraune: I admit, it is difficult to tell humans apart.[P] Perhaps they assume you are a mercenary?[B][C]") ]])
				fnCutscene([[ Append("Mei: That's very possible.[B][C]") ]])
			
			--Bee.
			elseif(sMeiForm == "Bee") then
				fnCutscene([[ Append("Alraune: Perhaps they assume you are after their nectar?[B][C]") ]])
				fnCutscene([[ Append("Mei: ...[P] Who says I'm not?[B][C]") ]])
				fnCutscene([[ Append("Alraune: Heh.[P] Amusing.[B][C]") ]])
			
			--Slime.
			elseif(sMeiForm == "Slime") then
				fnCutscene([[ Append("Alraune: I would not believe the little ones, given your appearance.[B][C]") ]])
				fnCutscene([[ Append("Mei: The slime is probably a turn-off.[B][C]") ]])
			
			--Werecat.
			elseif(sMeiForm == "Werecat") then
				fnCutscene([[ Append("Alraune: Admittedly, the werecats often attack us for their amusement.[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Neutral] I'll not contest the point.[P] You look like you'd be a good bit of sport.[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Laugh] Not that I would even think of harming you![B][C]") ]])
			
			--Ghost.
			elseif(sMeiForm == "Ghost") then
				fnCutscene([[ Append("Alraune: Surely you know that the undead are abhorrent to the daughters of the wild?[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Neutral] I do, but this form has advantages.[P] I need it, for the time being.[B][C]") ]])
				
			--Ghost.
			elseif(sMeiForm == "Gravemarker") then
				fnCutscene([[ Append("Alraune: The angels have an exceedingly poor reputation with other partirhumans.[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Smirk] Yeah, can't imagine why...[B][C]") ]])
				
			--Wisphag.
            elseif(sMeiForm == "Wisphag") then
				fnCutscene([[ Append("Alraune: It makes sense, they fear your visage.[P] Sometimes I see the wisphags slithering in the bog.[P] I keep my distance.[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Smirk] I get that a lot, but we're actually really nice![B][C]") ]])
            
			--Mannequin.
			elseif(sMeiForm == "Mannequin") then
				fnCutscene([[ Append("Alraune: Perhaps they think you are some unliving construct.[P] Such things are anathema to nature, you realize.[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Smirk] I see.[B][C]") ]])
			end
			
			--Continue.
			fnCutscene([[ Append("Alraune: So.[P] What brings you here?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] I was just exploring.[P] I'm trying to find my place in the great scheme of things.[B][C]") ]])
			fnCutscene([[ Append("Alraune: One who walks easily between bodies probably has a higher calling than tilling the earth.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Happy] That's one way of looking at it, but your work here has merit, too.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] You were the one who was reclaiming the salt flats.[P] Adina, right?[P] The little ones spoke of you when I could hear them.[B][C]") ]])
			fnCutscene([[ Append("Adina: Yes, I am her.[B][C]") ]])
			fnCutscene([[ Append("Adina: The work is hard, but I know I will be triumphant in the end.[B][C]") ]])
			fnCutscene([[ Append("Adina: The earth here is so coated in salt that it would take centuries to wash away normally.[P] The little ones require constant care to survive.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] I admit, your task seems daunting.[P] Nature will thank you in the end.[B][C]") ]])
			fnCutscene([[ Append("Adina: I appreciate your encouragement.[P] If you need a place to rest on your journey, look no further.") ]])
	
		--If Florentina is present, and Mei is not in Alraune form, this occurs:
		elseif(bIsFlorentinaPresent == true and sMeiForm ~= "Alraune") then
		
			--Flags.
			VM_SetVar("Root/Variables/Chapter1/Scenes/iNeedsToSeeSaltFlatsScene", "N", 1.0)
			VM_SetVar("Root/Variables/Chapter1/SaltFlats/iHasMetAdina", "N", 10.0)
			VM_SetVar("Root/Variables/Chapter1/Scenes/iFlorentinaMetAdina", "N", 1.0)
            VM_SetVar("Root/Variables/Chapter1/Scenes/iAdinaQuestVar", "N", 2.0)
		
			--Add Florentina to the lineup.
			fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Null") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])

			fnCutscene([[ Append("Alraune: Hello, Florentina.[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Confused] Adina.[B][C]") ]])
			fnCutscene([[ Append("Adina: You have brought company, I see.[B][C]") ]])
			
			--If Mei is a bee, this extra bit plays:
			if(sMeiForm == "Bee") then
				fnCutscene([[ Append("Mei:[E|Neutral] Nice to meet you![B][C]") ]])
				fnCutscene([[ Append("Adina: This bee speaks?[B][C]") ]])
				fnCutscene([[ Append("Florentina:[E|Happy] It's getting her to shut up that's hard.[B][C]") ]])
				fnCutscene([[ Append("Adina: That is not the attitude I would have in the company of such a bee.[B][C]") ]])
			
			--If Mei is a slime, this extra bit plays:
			elseif(sMeiForm == "Slime") then
				fnCutscene([[ Append("Mei:[E|Neutral] Nice to meet you![B][C]") ]])
				fnCutscene([[ Append("Adina: And you bring a slime that has learned to speak?[P] Truly, the world is filled with wonders.[B][C]") ]])
				fnCutscene([[ Append("Florentina:[E|Happy] Yeah, wonders.[P] Sure.[B][C]") ]])
			end
			
			--Dialogue. If Mei is a human, this segues naturally from the previous.
			fnCutscene([[ Append("Mei:[E|Neutral] Do you two know each other?[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Neutral] No, actually.[P] Never met.[P] But, the little ones talk about Adina all the time.[B][C]") ]])
			fnCutscene([[ Append("Adina: Likewise, they whisper of you.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] I'm just glad she's not trying to attack us.[B][C]") ]])
			fnCutscene([[ Append("Adina: Personally, I would rather not.[P] I am far too worn from my labours here.[B][C]") ]])
			fnCutscene([[ Append("Adina: If you intend to attack me, now is not a convenient time.[P] Later, perhaps?[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Happy] I think you can work us into your schedule - [P][CLEAR]") ]])
			fnCutscene([[ Append("Mei:[E|Offended] Florentina, no![B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Happy] Hello, Adina.[P] My name is Mei.[P] Pleased to meet you.[B][C]") ]])
			
			--Human:
			if(sMeiForm == "Human") then
				fnCutscene([[ Append("Adina: You are most cordial, for a human.[B][C]") ]])
			
			--Bee:
			elseif(sMeiForm == "Bee") then
				fnCutscene([[ Append("Adina: You are most cordial.[P] Would that all bees were like you.[B][C]") ]])
			
			--Slime:
			elseif(sMeiForm == "Slime") then
				fnCutscene([[ Append("Adina: You are most cordial.[P] Would that all slimes were like you.[B][C]") ]])
				
			--Werecat:
			elseif(sMeiForm == "Werecat") then
				fnCutscene([[ Append("Adina: You are most cordial, unlike your kin.[B][C]") ]])
			
			--Ghost:
			elseif(sMeiForm == "Ghost") then
				fnCutscene([[ Append("Adina: My dealings with the unliving are...[P] uncommon.[P] You are certainly the most polite.[B][C]") ]])
				
			--Gravemarker:
			elseif(sMeiForm == "Gravemarker") then
				fnCutscene([[ Append("Adina: I've not the faintest what sort of angelic creature you are, save that you are quite polite for one.[B][C]") ]])
				
			--Wisphag:
			elseif(sMeiForm == "Wisphag") then
				fnCutscene([[ Append("Adina: I've not known the wisphags to speak, much less be so polite.[B][C]") ]])
				
			--Mannequin:
			elseif(sMeiForm == "Mannequin") then
				fnCutscene([[ Append("Adina: Your face betrays nothing but my you are well-spoken.[B][C]") ]])
			end
			
			fnCutscene([[ Append("Mei:[E|Neutral] I just like making friends, not enemies.[P] Right, Florentina?[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Neutral] You're way too trusting, kid.[B][C]") ]])
			fnCutscene([[ Append("Adina: Most surprising.[P] Thank you for your tact.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] So...[P] what are you doing out here by yourself?[B][C]") ]])
			fnCutscene([[ Append("Adina: I am not alone, I am surrounded by the voices of the forest.[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Facepalm] Ugh.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Laugh][EMOTION|Florentina|Neutral] Well, what are you doing out here, surrounded by voices?[B][C]") ]])
			fnCutscene([[ Append("Adina:[EMOTION|Mei|Neutral] This earth has been scarred, and covered in salt, by some ancient conflict.[P] Nothing has grown here for centuries.[B][C]") ]])
			fnCutscene([[ Append("Adina: On my travels, I met a few species of little ones who agreed to help me cleanse it.[P] So, I care for them as they work the salt from the soil.[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Neutral] What a waste of time.[B][C]") ]])
			fnCutscene([[ Append("Adina: Pray tell, Florentina, what would you do that would be less wasteful?[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Neutral] It's not my problem.[P] I'd leave this place to stew in its salty juices.[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Neutral] Who cares if there's a few meters of earth not covered in grass?[B][C]") ]])
			fnCutscene([[ Append("Adina: Your attitude is common, even amongst Alraunes.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Well I think it's a great idea![P] Good for you![B][C]") ]])
			fnCutscene([[ Append("Adina: ...[B][C]") ]])
			fnCutscene([[ Append("Adina: Mei, thank you.[P] Truly.[P] I am often called a fool even by my own kin, though not for the reasons that Florentina espouses.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] You're likely to succeed if you try.[P] Try not to give up![B][C]") ]])
			fnCutscene([[ Append("Adina: Words to live by.[B][C]") ]])
			fnCutscene([[ Append("Adina: Mei, and yes, Florentina.[P] You are welcome here if you need a moment to rest.[P] Stay if you like.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Happy] Nice to meet you, Adina!") ]])
		
		--If Florentina is present, and Mei is in Alraune form:
		elseif(bIsFlorentinaPresent == true and sMeiForm == "Alraune") then
		
			--Flags.
			VM_SetVar("Root/Variables/Chapter1/SaltFlats/iHasMetAdina", "N", 10.0)
			VM_SetVar("Root/Variables/Chapter1/Scenes/iFlorentinaMetAdina", "N", 1.0)
            VM_SetVar("Root/Variables/Chapter1/Scenes/iAdinaQuestVar", "N", 2.0)
		
			--Add Florentina to the lineup.
			fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Null") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])

			--Dialogue.
			fnCutscene([[ Append("Alraune: Hello, Florentina.[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Confused] Adina.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Adina?[P] You're the one that tends the salt flats?[B][C]") ]])
			fnCutscene([[ Append("Adina: My reputation precedes me.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] I heard the little ones whispering of you.[P] I can see how monumental your task is.[B][C]") ]])
			fnCutscene([[ Append("Adina: It is.[P] Legend has it that a mage blasted his enemies with molten rock from the sky.[P] The rock was salt, and it stays here to this day.[B][C]") ]])
			fnCutscene([[ Append("Adina: I have found little ones who will aid me in cleansing it.[P] They are resistant to salt, but the work is hard.[P] I care for them as best I can.[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Neutral] You should give up.[B][C]") ]])
			fnCutscene([[ Append("Adina: And why would I do that?[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Neutral] Because not every millimeter of the world has to be covered in grass?[P] Because it's okay to just let things lie?[B][C]") ]])
			fnCutscene([[ Append("Adina: It goes deeper than that...[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] It's not part of the natural cycle, Florentina.[P] This is one of humanity's mistakes, and we correct for it.[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Surprise] Ah, cripes![P] Not you too, Mei![B][C]") ]])
			fnCutscene([[ Append("Adina: Mei, was it?[P] Thank you.[B][C]") ]])
			fnCutscene([[ Append("Adina: Most of the other Alraunes do not agree as you do.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] You're likely to succeed if you try.[P] Try not to give up![B][C]") ]])
			fnCutscene([[ Append("Adina: Wise words.[P] Where did you hear them?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] It's...[P] uh...[P] not important...[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Neutral] If you want to while away your time here, I'm not going to stop you.[B][C]") ]])
			fnCutscene([[ Append("Adina: I would not wish to impose on you, either.[P] Do what you will with yours, Florentina.[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Neutral] I intend to.[B][C]") ]])
			fnCutscene([[ Append("Adina: However, if you wish to stay here for a moment, you are welcome to.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Very nice to meet you, leaf-sister Adina.") ]])
		
		end
		
		--Common.
		fnCutsceneBlocker()
		
		--If Florentina is here, move her onto Mei and fold the party.
		if(bIsFlorentinaPresent == true) then
			
			--Move.
			Cutscene_CreateEvent("ActorEvent", "Actor")
				ActorEvent_SetProperty("Subject Name", "Florentina")
				ActorEvent_SetProperty("Move To", (35.25 * gciSizePerTile), (15.50 * gciSizePerTile))
			DL_PopActiveObject()
			fnCutsceneBlocker()

			--Fold the party positions up.
			fnCutscene([[ AL_SetProperty("Fold Party") ]])
			fnCutsceneBlocker()
		end

	end

--If Mei is currently mind-controlled, this zone keeps her from wandering off duty.
elseif(sObjectName == "Walkback") then

	--Variables.
	local iIsMeiControlled = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iIsMeiControlled", "N")
	if(iIsMeiControlled == 1.0) then
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Thrall:[VOICE|Mei] (Mistress has not dismissed me.)") ]])
		fnCutsceneBlocker()
		
		--Move Mei south.
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Move To", (35.75 * gciSizePerTile), (13.50 * gciSizePerTile))
		DL_PopActiveObject()
		fnCutsceneBlocker()
		
	end

--A short scene plays here after meeting Adina with Florentina present.
elseif(sObjectName == "PostAdinaFlorentinaScene") then

	--Variables. If flagged, play the scene.
	local iNeedsToSeeSaltFlatsScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iNeedsToSeeSaltFlatsScene", "N")
	if(iNeedsToSeeSaltFlatsScene == 1.0) then
		
		--Unflag so it doesn't play twice.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iNeedsToSeeSaltFlatsScene", "N", 0.0)

		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] Hey, Mei?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Yes?[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] Just what the hell is wrong with you?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Happy] Be more specific.[P] We could start with my childhood and go from there.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] Why are you all lovey-dovey with everyone you meet?[P] That attitude is going to get you killed someday.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] You mean Adina?[P] But she's not a bad person![B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] Someone's going to take advantage of you if you're too trusting.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] Hm.[P] I'm not sure I want to be paranoid all the time.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] If you assume everyone is out to get you, you'll never have any friends.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] Friends are better termed 'liabilities'.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] Is that how you see me?[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] ...[P] No.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] Because you're a goodie-[P]freaking-[P]two-shoes.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] All right, how's this?[P] As long as you're around, I'll be the nice one.[P] If you're not with me, I'll be a jerk.[P] Deal?[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] Deal.[P] Pleasure doing business with you.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Laugh] Likewise.") ]])
	end
	
--Disables the forest overlay.
elseif(sObjectName == "NoOverlay") then
    AL_SetProperty("Foreground Alpha", 0, 0.00, 15)
	
--Disables the forest overlay instantly.
elseif(sObjectName == "NoOverlayFast") then
    AL_SetProperty("Foreground Alpha", 0, 0.00, 0)
	
--Enables the forest overlay.
elseif(sObjectName == "YesOverlay") then
    AL_SetProperty("Foreground Alpha", 0, 0.50, 15)
	
end
