-- |[Constructor]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = fnResolveDirectory() --Map's name is the name of the folder it is in.

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[Standard]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music. If Adina has not been met, it's the ForestTheme
	local iHasMetAdina = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iHasMetAdina", "N")
	if(iHasMetAdina == 0.0) then
		AL_SetProperty("Music", "ForestTheme")
	
	--Otherwise, it's Adina's Theme.
	else
		AL_SetProperty("Music", "AdinasTheme")
	end
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/SaltFlats/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", "SaltFlats")

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
    
    -- |[Map Handling]|
    --Resolve the map's variable.
    fnStandardCh1Map(sLevelName)

	--Create Adina. She's in the grassy section.
    fnStandardNPCByPosition("Adina")
	
	--Create the plot characters. Their initial position is offscreen.
	for i = 0, 7, 1 do
		TA_Create("NPCPLOT" .. i)
			TA_SetProperty("Position", -100, -100)
			TA_SetProperty("Clipping Flag", false)
			fnSetToFarmGfx()
			TA_SetProperty("Facing", gci_FDir_Water)
			TA_SetProperty("Auto Animates", true)
		DL_PopActiveObject()
	end
	
	--Time of day. It's noon when we arrive unless otherwise specified.
	local iTimeOfDay = VM_GetVar("Root/Variables/Global/Time/iTimeOfDay", "N")
	local iTasksDone = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iTasksDone", "N")
	
	--Special: When returning from the cave, set to 100% complete so the right time is used.
	if(iTimeOfDay == 18) then iTasksDone = 4 end
	
	--Modify time of day instantly.
	giTmeOfDayModifyTicks = 1
	fnModifyTimeOfDay(iTimeOfDay, iTasksDone)
	giTmeOfDayModifyTicks = 45
	
	--Place NPCs to indicate tasks, if they exist.
	local iIsMeiControlled = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iIsMeiControlled", "N")
	local bRemoveEntities = false
	if(iIsMeiControlled == 0.0) then bRemoveEntities = true end
	fnPlaceProblemIndicators(bRemoveEntities)

-- |[Post-Exec]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then
	
	--Overlays.
    fnLightForest()

    --If the chapter has been completed before, spawn a rilmani.
    local iChapter1Complete = VM_GetVar("Root/Variables/Global/ChapterComplete/iChapter1", "N")
    if(iChapter1Complete == 1.0) then
        fnStandardNPCByPosition("Rilmani")
    end
end
