-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[Name Resolve]|
    --Resolve the name of the calling entity.
    local sActorName = TA_GetProperty("Name")

    -- |[NC+ Rilmani]|
    if(sActorName == "Rilmani") then
        fnStandardMajorDialogue()
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Maram", "Neutral") ]])
        fnCutscene([[ Append("Rilmani: Hello![P] I'm the NC+ Rilmani, here to help you.[B][C]") ]])
        fnCutscene([[ Append("Rilmani: I can 'reset' your alraune state and Adina's knowledge of you, allowing you to see the scenes that might not be possible in NC+.[B][C]") ]])
        fnCutscene([[ Append("Rilmani: Look, it's a lot easier than starting a new game and having to go through all that stuff, okay?[B][C]") ]])
        fnCutscene([[ Append("Rilmani: If you let me, I will set all the game variables such that Adina will have never met you, and will not identify you as an alraune if you had that form.[B][C]") ]])
        fnCutscene([[ Append("Rilmani: (Florentina will also leave but will rejoin you when you leave the area.)[B][C]") ]])
        fnCutscene([[ Append("Rilmani: You can then walk up to her and start the sequence anew.[P] So shall I?[B]") ]])
        
        --Decision script is this script. It must be surrounded by quotes.
        local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
        
        --Decision mode.
        fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Sure\", " .. sDecisionScript .. ", \"ResetAdina\") ")
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"No Thanks\",  " .. sDecisionScript .. ", \"NoSillyRilmani\") ")
        fnCutsceneBlocker()

    -- |[Adina]|
    elseif(sActorName == "Adina") then
        
        -- |[Variables]|
        local sMeiForm             = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
        local iHasMetAdina         = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iHasMetAdina", "N")
        local iIsMeiControlled     = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iIsMeiControlled", "N")
        local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
        
        --In all cases, mark the combat intro dialogues as complete.
        VM_SetVar("Root/Variables/Chapter1/Counts/sMeiSeenPartirhuman", "S", "Complete")
        
        --If Mei is a non-human, or Florentina is present, jump the iHasMetAdina value to 10. The Mind-control sequence is locked off.
        if(sMeiForm ~= "Human" or bIsFlorentinaPresent == true) then
            iHasMetAdina = 10.0
            VM_SetVar("Root/Variables/Chapter1/SaltFlats/iHasMetAdina", "N", 10.0)
            VM_SetVar("Root/Variables/Chapter1/Scenes/iAdinaQuestVar", "N", 2.0)
        end
        
        -- |[Flags]|
        --Set facing.
        TA_SetProperty("Face Character", "PlayerEntity")
        
        -- |[Dialogue Setup]|
        --This is always a major dialogue.
        fnStandardMajorDialogue()
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])
        
        -- |[Mei is Controlled]|
        --If this variable is 1, Mei is currently maintaining the farm.
        if(iIsMeiControlled == 1.0) then
            LM_ExecuteScript(fnResolvePath() .. "FarmHandler.lua", "Hello")

        -- |[First Dialogue]|
        --Adina explains what she's doing at the salt flats.
        elseif(iHasMetAdina == 1.0) then

            --Set this variable to 2. This prevents a total repeat.
            VM_SetVar("Root/Variables/Chapter1/SaltFlats/iHasMetAdina", "N", 2.0)

            --Dialogue.
            fnCutscene([[ Append("Adina: Hello, Mei.[P] I see you trust me enough to stand closer.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Laugh] Don't tease me.[B][C]") ]])
            fnCutscene([[ Append("Adina: You mentioned that if we knew one another better, you may relax your guard.[P] Would you like to know what I have tasked myself with?[BLOCK]") ]])
        
            --Decision script is this script. It must be surrounded by quotes.
            local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
            
            --Decision mode.
            fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Sure\", " .. sDecisionScript .. ", \"ListenToStory\") ")
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Not right now\",  " .. sDecisionScript .. ", \"NotRightNow\") ")
            fnCutsceneBlocker()
        
        -- |[First Dialogue Repeat]|
        --Shorter version of above if Mei blew Adina off.
        elseif(iHasMetAdina == 2.0) then

            --Dialogue.
            fnCutscene([[ Append("Adina: Hello, Mei.[P] Have you changed your mind?[BLOCK]") ]])
        
            --Decision script is this script. It must be surrounded by quotes.
            local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
            
            --Decision mode.
            fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Let's hear your story\", " .. sDecisionScript .. ", \"ListenToStory\") ")
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Not right now\",  " .. sDecisionScript .. ", \"NotRightNow\") ")
            fnCutsceneBlocker()
        
        -- |[Second Dialogue]|
        --Adina asks you for a trust test.
        elseif(iHasMetAdina == 5.0) then

            --Dialogue.
            fnCutscene([[ Append("Adina: Mei, perhaps I can assuage your fears.[P] Will you let me try?[BLOCK]") ]])
        
            --Decision script is this script. It must be surrounded by quotes.
            local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
            
            --Decision mode.
            fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Okay\", " .. sDecisionScript .. ", \"TrustTest\") ")
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Later\",  " .. sDecisionScript .. ", \"Later\") ")
            fnCutsceneBlocker()
        
        -- |[Second Dialogue]|
        --If you turned down hypnosis, this shows.
        elseif(iHasMetAdina == 7.0) then

            --Dialogue.
            fnCutscene([[ Append("Adina: Will you place yourself at my mercy?[P] I must demonstrate my faithfulness.[BLOCK]") ]])
        
            --Decision script is this script. It must be surrounded by quotes.
            local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
            
            --Decision mode.
            fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Okay\", " .. sDecisionScript .. ", \"Hypnosis\") ")
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"I don't know...\",  " .. sDecisionScript .. ", \"NoHypnosis\") ")
            fnCutsceneBlocker()
        
        -- |[Third Dialogue]|
        --Adina greets Mei cordially.
        elseif(iHasMetAdina == 10.0) then
        
            --Additional variables.
            local sMeiForm                 = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
            local iMeiLovesAdina           = VM_GetVar("Root/Variables/Chapter1/Scenes/iMeiLovesAdina", "N")
            local iFlorentinaMetAdina      = VM_GetVar("Root/Variables/Chapter1/Scenes/iFlorentinaMetAdina", "N")
            local sAdinaFirstSceneForm     = VM_GetVar("Root/Variables/Chapter1/Scenes/sAdinaFirstSceneForm", "S")
            local iFlorentinaKnowsMistress = VM_GetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsMistress", "N")
        
            --If the form is "Completed" then Adina knows Mei is a shapeshifter.
            if(sAdinaFirstSceneForm == "Completed" or sAdinaFirstSceneForm == sMeiForm) then
                
                --If Adina did not know Florentina is travelling with Mei:
                if(iFlorentinaMetAdina == 0.0 and bIsFlorentinaPresent == true) then
                    VM_SetVar("Root/Variables/Chapter1/Scenes/iFlorentinaMetAdina", "N", 1.0)
                    
                    --If Mei fell in love with Adina:
                    if(iMeiLovesAdina == 1.0) then
                        VM_SetVar("Root/Variables/Chapter1/Scenes/iAdinaExtendedMistress", "N", 1.0)
                        fnCutscene([[ Append("Mei:[E|Neutral] Mistress Adina![P] How goes the work with the flats?[B][C]") ]])
                        
                        --First time Florentina finds this out:
                        if(iFlorentinaKnowsMistress == 0.0) then
                            VM_SetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsMistress", "N", 1.0)
                            fnCutscene([[ Append("Florentina:[E|Surprise] Freakin' -[P] MISTRESS?[B][C]") ]])
                            fnCutscene([[ Append("Florentina:[E|Surprise] WHAT IN THE WASTES?[B][C]") ]])
                            fnCutscene([[ Append("Adina: Please calm yourself, Florentina.[P] Nothing is amiss.[B][C]") ]])
                            fnCutscene([[ Append("Florentina:[E|Confused] Mei, what's going on here?[B][C]") ]])
                            fnCutscene([[ Append("Mei:[E|Surprise] I'm sorry![P] I didn't think this would upset you![B][C]") ]])
                            fnCutscene([[ Append("Florentina:[E|Confused] Answer me![B][C]") ]])
                            fnCutscene([[ Append("Mei:[E|Neutral] I, uh, did some farm work for Adina.[P] That's all.[B][C]") ]])
                            fnCutscene([[ Append("Florentina:[E|Offended] You are an atrocious liar.[B][C]") ]])
                            fnCutscene([[ Append("Mei:[E|Sad] I'm sorry...[B][C]") ]])
                            fnCutscene([[ Append("Adina: Florentina, nothing illicit occurred.[P] Mei enjoyed her time here, but felt she had other obligations and set off to see to them.[B][C]") ]])
                            fnCutscene([[ Append("Adina: Nothing pleases me more than seeing her completing that task.[P] Doubtless you are involved, and I wish you the best of luck.[P] Help her achieve her goals.[B][C]") ]])
                            fnCutscene([[ Append("Florentina:[E|Offended] Is all this true, Mei?[P] Was it just 'farm work' and that's all?[B][C]") ]])
                            fnCutscene([[ Append("Mei:[E|Sad] I -[P] I...[B][C]") ]])
                            fnCutscene([[ Append("Mei:[E|Sad] I love her, Florentina.[P] She is my mistress, and I wish I could stay here with her.[B][C]") ]])
                            fnCutscene([[ Append("Mei:[E|Sad] But, I can't, because I still have other things I need to do first.[B][C]") ]])
                            fnCutscene([[ Append("Florentina:[E|Confused] Okay, okay.[B][C]") ]])
                            fnCutscene([[ Append("Florentina:[E|Confused] There's nothing wrong with that.[P] I just thought she may have done something to you.[B][C]") ]])
                            fnCutscene([[ Append("Mei:[E|Sad] Well...[B][C]") ]])
                            fnCutscene([[ Append("Florentina:[E|Confused] Love takes many forms.[P] If you meet someone you love that much...[B][C]") ]])
                            fnCutscene([[ Append("Florentina:[E|Confused] ...[B][C]") ]])
                            fnCutscene([[ Append("Florentina:[E|Confused] ...[B][C]") ]])
                            fnCutscene([[ Append("Florentina:[E|Confused] Never let them go.[B][C]") ]])
                            fnCutscene([[ Append("Mei:[E|Sad] You mean that?[B][C]") ]])
                            fnCutscene([[ Append("Florentina:[E|Neutral] Absolutely.[B][C]") ]])
                            fnCutscene([[ Append("Adina: Thank you for your understanding.[B][C]") ]])
                            fnCutscene([[ Append("Mei:[E|Neutral] Yeah, thanks, Florentina.[B][C]") ]])
                            fnCutscene([[ Append("Florentina:[E|Neutral] Yeah, yeah.[P] Whatever.[B][C]") ]])
                            fnCutscene([[ Append("Adina: Are you well, Mei?[B][C]") ]])
                            fnCutscene([[ Append("Mei:[E|Happy] I am.[P] Florentina is helping me out.[B][C]") ]])
                            fnCutscene([[ Append("Adina: I have no doubt you will be successful, then.[P] I can see she has your best interests at heart, no matter what she says.[P] Good luck.") ]])
                    
                        --Florentina already knows.
                        else
                            fnCutscene([[ Append("Florentina:[E|Surprise] Wow, you weren't kidding about the whole mistress thing.[B][C]") ]])
                            fnCutscene([[ Append("Adina: Ah.[P] My thrall told you of us.[B][C]") ]])
                            fnCutscene([[ Append("Florentina:[E|Confused] I didn't quite believe it, but I've seen it for myself.[B][C]") ]])
                            fnCutscene([[ Append("Mei:[E|Sad] I'm sorry![P] It's all right, really![B][C]") ]])
                            fnCutscene([[ Append("Florentina:[E|Neutral] Do you do this to anyone who passes by?[B][C]") ]])
                            fnCutscene([[ Append("Adina: Of course not.[P] Mei learned something about herself from what I thought was merely a demonstration of trust.[B][C]") ]])
                            fnCutscene([[ Append("Mei:[E|Smirk] If I wanted it to be any other way, it would be.[P] But I don't.[B][C]") ]])
                            fnCutscene([[ Append("Mei:[E|Smirk] I love my mistress, Florentina.[P] I would be with her always if I could.[B][C]") ]])
                            fnCutscene([[ Append("Florentina:[E|Confused] Okay, okay.[P] As long as you're not doing any enslaving.[P] I wouldn't abide that.[B][C]") ]])
                            fnCutscene([[ Append("Florentina:[E|Neutral] Not that I'd have to do anything about it, myself.[B][C]") ]])
                            fnCutscene([[ Append("Adina: It is abhorrent.[P] I would never.[B][C]") ]])
                            fnCutscene([[ Append("Florentina:[E|Neutral] Mei, if you really have found love here, then all the best.[B][C]") ]])
                            fnCutscene([[ Append("Mei:[E|Smirk] I just have to let everyone on Earth know I'm all right, and then I'll be by her side forever.[B][C]") ]])
                            fnCutscene([[ Append("Florentina:[E|Neutral] Yeah, I felt the same way myself way back when.[B][C]") ]])
                            fnCutscene([[ Append("Adina: Even Florentina's heart has its soft spots.[B][C]") ]])
                            fnCutscene([[ Append("Florentina:[E|Happy] By the way, Adina.[P] It's not just the flower petals, right?[P] You do some magic with spores?[B][C]") ]])
                            fnCutscene([[ Append("Florentina:[E|Happy] You wouldn't be willing to sell your secret, would you?[B][C]") ]])
                            fnCutscene([[ Append("Adina: You don't need any more advantages in your bartering, Florentina.[P] My secret is not for sale.[B][C]") ]])
                            fnCutscene([[ Append("Mei:[E|Laugh] Too bad, Florentina![B][C]") ]])
                            fnCutscene([[ Append("Florentina:[E|Neutral] No harm in trying, right?") ]])
                        end
                    
                    --Normal case:
                    else
                        fnCutscene([[ Append("Adina: Greetings, Mei.[P] And tidings to you, Florentina.[B][C]") ]])
                        fnCutscene([[ Append("Florentina:[E|Happy] So you're the nut who thinks the salt flats can be saved.[P] You've got quite a reputation.[B][C]") ]])
                        fnCutscene([[ Append("Mei:[E|Offended] Don't be so negative, Florentina![B][C]") ]])
                        fnCutscene([[ Append("Adina: I am accustomed to criticism.[P] Pay it no mind.[B][C]") ]])
                        fnCutscene([[ Append("Adina: Perhaps your influence will rub off on her, and she will see the world less darkly.[B][C]") ]])
                        fnCutscene([[ Append("Florentina:[E|Neutral] Don't count on it, buckaroo.[B][C]") ]])
                        fnCutscene([[ Append("Adina: It may take some time, yet...[B][C]") ]])
                        fnCutscene([[ Append("Mei:[E|Happy] I'll see what I can do.[P] Good luck with the salt flats!") ]])
                    end
                
                --Otherwise, normal:
                else
                
                    --Variables.
                    local iAdinaExtendedMistress = VM_GetVar("Root/Variables/Chapter1/Scenes/iAdinaExtendedMistress", "N")
                
                    --If Florentina is not around and Mei fell in love with Adina, and this is the first time:
                    if(iMeiLovesAdina == 1.0 and bIsFlorentinaPresent == false and iAdinaExtendedMistress == 0.0) then
                        VM_SetVar("Root/Variables/Chapter1/Scenes/iAdinaExtendedMistress", "N", 1.0)
                        fnCutscene([[ Append("Mei:[E|Happy] Hello, mistress![B][C]") ]])
                        fnCutscene([[ Append("Adina: Tidings, Mei.[P] You realize you need not call me mistress anymore?[B][C]") ]])
                        fnCutscene([[ Append("Mei:[E|Neutral] But you [P]*are*[P] my mistress![P] I would give anything to serve you mindlessly again![B][C]") ]])
                        fnCutscene([[ Append("Adina: Heh.[P] I would like to be near to you as well, but you are still called by a greater destiny.[B][C]") ]])
                        fnCutscene([[ Append("Adina: When you see to that, if you still wish it, come see me.[B][C]") ]])
                        fnCutscene([[ Append("Mei:[E|Neutral] Of course![B][C]") ]])
                        fnCutscene([[ Append("Adina: Was there anything else I could do for you?[BLOCK]") ]])
            
                        --Form.
                        local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
            
                        --Decision mode.
                        local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
                        fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
                        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Just saying Hi!\", " .. sDecisionScript .. ", \"JustHi\") ")
                        if(sMeiForm == "Human" or sMeiForm == "Alraune") then
                            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Enslave me again...\",  " .. sDecisionScript .. ", \"ReHypnosis\") ")
                        end
                    
                    --If Florentina is not around and Mei fell in love with Adina, and this is a repeat case:
                    elseif(iMeiLovesAdina == 1.0 and bIsFlorentinaPresent == false and iAdinaExtendedMistress == 1.0) then
                        fnCutscene([[ Append("Mei:[E|Happy] Hello, mistress![B][C]") ]])
                        fnCutscene([[ Append("Adina: Hello, Mei.[P] What brings you here?[BLOCK]") ]])
            
                        --Form.
                        local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
            
                        --Decision mode.
                        local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
                        fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
                        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Just saying Hi!\", " .. sDecisionScript .. ", \"JustHi\") ")
                        if(sMeiForm == "Human" or sMeiForm == "Alraune") then
                            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Enslave me again...\",  " .. sDecisionScript .. ", \"ReHypnosis\") ")
                        end
                    
                    --All other cases:
                    else
                        fnCutscene([[ Append("Adina: Greetings, Mei.[P] I wish you luck on your journey.") ]])
                    end
                end
        
            --If Mei is an Alraune and first met Adina as a human:
            elseif(sAdinaFirstSceneForm == "Human" and sMeiForm == "Alraune") then
                VM_SetVar("Root/Variables/Chapter1/Scenes/sAdinaFirstSceneForm", "S", "Alraune")
                fnCutscene([[ Append("Adina: Tidings, leaf-sister Mei.[P] So you have been joined?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Happy] Yes, I have.[P] I was wondering what you would think.[B][C]") ]])
                fnCutscene([[ Append("Adina: It is a joyous day.[P] The world is richer with you as one of us.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] It only impresses upon me further the difficulty of your position.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] I regret that I cannot stay and assist you with your work here.[B][C]") ]])
                fnCutscene([[ Append("Adina: Your destiny calls to you.[P] I understand.[P] I hope your journey goes well.") ]])
        
            --If Mei is a bee and first met Adina as a human:
            elseif(sAdinaFirstSceneForm == "Human" and sMeiForm == "Bee") then
                VM_SetVar("Root/Variables/Chapter1/Scenes/sAdinaFirstSceneForm", "S", "Bee")
                fnCutscene([[ Append("Adina: Tidings, Mei.[P] The little ones whisper that you are different from the other bees.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] I don't want to be.[B][C]") ]])
                fnCutscene([[ Append("Adina: Being unhappy with your position in life is what drives you to succeed.[B][C]") ]])
                fnCutscene([[ Append("Adina: Besides, no other bee has seen fit to speak with me.[P] It is enjoyable.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Happy] I guess being different isn't all bad.[B][C]") ]])
                fnCutscene([[ Append("Adina: Why do you wander so far from your hive?[P] I regret there is little nectar to be had here.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] My hive has given me an important mission.[P] I must find out why I am different from the other bees.[B][C]") ]])
                fnCutscene([[ Append("Adina: I see.[P] I can offer no insight, but I hope you find out.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Thanks, Adina.[P] I hope I found out, too.") ]])
        
            --If Mei is a slime and first met Adina as a human:
            elseif(sAdinaFirstSceneForm == "Human" and sMeiForm == "Slime") then
                VM_SetVar("Root/Variables/Chapter1/Scenes/sAdinaFirstSceneForm", "S", "Slime")
                fnCutscene([[ Append("Adina: I see you return here, Mei, despite your condition.[P] Are the rumours true?[P] Do you remember yourself?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Rumours?[B][C]") ]])
                fnCutscene([[ Append("Adina: So they are![P] The plants have long whispered rumours of slimes with wit.[P] I did not think it would be you.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] I like to surprise.[P] Have you been well?[B][C]") ]])
                fnCutscene([[ Append("Adina: My work is unchanging.[P] Your company is appreciated, though.[P] Stay as long as you like.") ]])
            
            --If this form is not "Completed" and not the same as the first form:
            elseif(sAdinaFirstSceneForm ~= sMeiForm) then
                VM_SetVar("Root/Variables/Chapter1/Scenes/sAdinaFirstSceneForm", "S", "Completed")
                fnCutscene([[ Append("Adina: So it seems the rumours were true.[P] You are one who walks between shapes.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] It's nothing special.[B][C]") ]])
                fnCutscene([[ Append("Adina: I disagree.[P] I consider it an honor that you would come and visit me.[P] You must have quite a destiny arrayed before you.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Maybe.[P] Have you ever met anyone else who can do this?[B][C]") ]])
                fnCutscene([[ Append("Adina: No, nor have I heard of any who could.[P] An anomaly as befits a destiny like yours.[B][C]") ]])
                fnCutscene([[ Append("Adina: I wish you best of luck on your journeys.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Happy] Thanks.[P] Good luck with the salt flats.") ]])
            end
            fnCutsceneBlocker()
        end
    end

-- |[ ============================== Rilmani NC+ Debug-ish Stuff =============================== ]|
elseif(sTopicName == "ResetAdina") then
	WD_SetProperty("Hide")
    
	--Reboot the dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Maram", "Neutral") ]])
	fnCutscene([[ Append("Rilmani: You got it![P] Enjoy!") ]])
	fnCutsceneBlocker()
    
    --Set Flags.
    VM_SetVar("Root/Variables/Chapter1/SaltFlats/iHasMetAdina", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter1/Scenes/sAdinaFirstSceneForm", "S", "Human")
    
    --Remove Florentina if she's present.
    local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
    local iReAddFlorentinaAfterLeaving = VM_GetVar("Root/Variables/Chapter1/Scenes/iReAddFlorentinaAfterLeaving", "N")
    if(bIsFlorentinaPresent == true and iReAddFlorentinaAfterLeaving == 0.0) then
        fnRemovePartyMember("Florentina", true)
        VM_SetVar("Root/Variables/Chapter1/Scenes/iReAddFlorentinaAfterLeaving", "N", 1.0)
    end

elseif(sTopicName == "NoSillyRilmani") then
	WD_SetProperty("Hide")
    
	--Reboot the dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Maram", "Neutral") ]])
	fnCutscene([[ Append("Rilmani: Suit yourself.") ]])
	fnCutsceneBlocker()
    
--"ListenToStory" explains the history of the salt flats and Adina's role in it.
elseif(sTopicName == "ListenToStory") then
	
	--Common. This is done to clear up any pending blocks from the previous part of the scene.
	WD_SetProperty("Hide")
	
	--Set this variable to 5.
	VM_SetVar("Root/Variables/Chapter1/SaltFlats/iHasMetAdina", "N", 5.0)
	
	--Reboot the dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Let's hear it.[B][C]") ]])
	fnCutscene([[ Append("Adina: Very well.[B][C]") ]])
	
	fnCutscene([[ Append("Adina: Several centuries ago, this place was the site of a battle between two armies of humans.[B][C]") ]])
	fnCutscene([[ Append("Adina: The river was not here then, and the forest further back, but the soil was the same.[B][C]") ]])
	fnCutscene([[ Append("Adina: The reason for the battle has been lost to time.[P] Like all humans conflicts, it was pointless and soon forgotten.[B][C]") ]])
	fnCutscene([[ Append("Adina: During the battle, it seemed that one army would win quite handily.[P] A mage on the other side decided to throw caution to the wind, and use a powerful new spell.[B][C]") ]])
	fnCutscene([[ Append("Adina: The sky opened up and spit molten fire on his enemies.[P] The fire was melted salt, scalding and scarring all those it touched.[B][C]") ]])
	fnCutscene([[ Append("Adina: The battle was swayed and the mage's side emerged victorious, but at a terrible cost.[B][C]") ]])
	fnCutscene([[ Append("Adina: The earth here is so salted that nothing has grown for centuries.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Surprise] That sounds awful...[B][C]") ]])
	fnCutscene([[ Append("Adina: Such is the folly of man.[P] Nature pays the price for his victories.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] But there's grass growing here, isn't there?[B][C]") ]])
	fnCutscene([[ Append("Adina: That is what I have tasked myself with.[P] I recently discovered several species of plants that can tolerate and even remove extreme saline concentrations.[B][C]") ]])
	fnCutscene([[ Append("Adina: I have sown these little ones on this field.[P] Slowly, life is returning, but it is a great deal of work to maintain.[B][C]") ]])
	fnCutscene([[ Append("Adina: Now you see what work I have taken up.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Your story seems to check out.[P] I can see little salt crystals all over the place.[B][C]") ]])
	fnCutscene([[ Append("Adina: When it is dry, new crystals form.[P] They wash down into the earth when it rains, but there is so much that it would take millennia to fully wash out.[B][C]") ]])
	fnCutscene([[ Append("Adina: Now that I have told you my story, perhaps you would tell me yours?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Oh, me?[P] I'm nothing special.[P] I'm...[P] trying to find my way back home.[P] I'm not from around here.[B][C]") ]])
	fnCutscene([[ Append("Adina: I see.[P] Your mannerisms and clothing are quite different from the other humans'.[B][C]") ]])
	fnCutscene([[ Append("Adina: Mei, I hope you see now that I bore you no ill will.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] I'm still not taking any chances.[B][C]") ]])
	fnCutscene([[ Append("Adina: Hm.[P] Perhaps another exercise...") ]])
	fnCutsceneBlocker()
        
-- |[ ==================================== Adina's Decisions =================================== ]|
--"NotRightNow" has Mei make a brief excuse.
elseif(sTopicName == "NotRightNow") then
	
	--Common. This is done to clear up any pending blocks from the previous part of the scene.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Sorry, but I'm looking for a way home right now.[P] Time might be of the essence.[B][C]") ]])
	fnCutscene([[ Append("Adina: I understand fully.[P] I will be here if you change your mind.") ]])
	fnCutsceneBlocker()


--"TrustTest" stars the brief trust-test scene.
elseif(sTopicName == "TrustTest") then
	
	--Common. This is done to clear up any pending blocks from the previous part of the scene.
	WD_SetProperty("Hide")
	
	--Variables.
	VM_SetVar("Root/Variables/Chapter1/SaltFlats/iHasMetAdina", "N", 7.0)
	
	--Reboot the dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] What did you have in mind?[B][C]") ]])
	fnCutscene([[ Append("Adina: Mei, draw your sword and hold it high.[B][C]") ]])
	
	fnCutscene([[ Append("Mei:[E|Neutral] All right...[B][C]") ]])
	fnCutscene([[ Append("Adina: I have my eyes closed, and my back turned.[P] Do as you will.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Surprise] ![B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] ...[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] ...[B][C]") ]])
	fnCutscene([[ Append("Adina: And here I stand.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] What are you trying to prove?[B][C]") ]])
	fnCutscene([[ Append("Adina: I was completely at your mercy.[P] I could have done nothing, should you have chosen to strike me down.[P] Yet you did not.[B][C]") ]])
	fnCutscene([[ Append("Adina: No matter what you may say now, I can see you are no cold-blooded killer.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] And if you had been wrong?[B][C]") ]])
	fnCutscene([[ Append("Adina: Then you would have killed me, or perhaps maimed me.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] Is that supposed to make me trust you?[B][C]") ]])
	fnCutscene([[ Append("Adina: No, but it serves as a demonstration.[P] I have placed full confidence in you.[B][C]") ]])
	fnCutscene([[ Append("Adina: Trust cannot be one-sided, it must always be shared.[P] Mei, for you to trust me you must place yourself at my mercy.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Hmmm.[B][C]") ]])
	fnCutscene([[ Append("Adina: Will you?[BLOCK]") ]])
	
	--Decision script is this script. It must be surrounded by quotes.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"Okay\", " .. sDecisionScript .. ", \"Hypnosis\") ")
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"Not Yet\",  " .. sDecisionScript .. ", \"NoHypnosis\") ")
	fnCutsceneBlocker()

--"Later" has Mei make an excuse to leave.
elseif(sTopicName == "Later") then
	
	--Common. This is done to clear up any pending blocks from the previous part of the scene.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] I'll think about it.[P] Maybe I should get going.[B][C]") ]])
	fnCutscene([[ Append("Adina: Take as long as you like.[P] My work requires I stay here.") ]])
	fnCutsceneBlocker()

--"Hypnosis" starts the next set of events.
elseif(sTopicName == "Hypnosis") then
	
	--Flip this variable.
	VM_SetVar("Root/Variables/Chapter1/SaltFlats/iIsMeiControlled", "N", 1.0)
	VM_SetVar("Root/Variables/Chapter1/SaltFlats/iHasMetAdina", "N", 10.0)
    VM_SetVar("Root/Variables/Chapter1/Scenes/iAdinaQuestVar", "N", 3.0)
	
	--Get Mei's position. The cutscene will need it later.
	local fMeiX, fMeiY
	EM_PushEntity("Mei")
		fMeiX, fMeiY = ("Position")
	DL_PopActiveObject()
	
	--Common. This is done to clear up any pending blocks from the previous part of the scene.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] This is going way against my better judgement...[B][C]") ]])
	fnCutscene([[ Append("Adina: All right, please follow me.[P] It is just over here.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(15)
	fnCutsceneBlocker()
	
	--Switch off Adina's collision flag.
	fnCutscene([[ 
		EM_PushEntity("Adina")
			TA_SetProperty("Clipping Flag", false)
		DL_PopActiveObject()
		]])
	fnCutsceneBlocker()
	
	--Adina walks south.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Adina")
		ActorEvent_SetProperty("Move To", (35.25 * gciSizePerTile), (19.50 * gciSizePerTile))
	DL_PopActiveObject()
	fnCutsceneBlocker()

	--Adina/Mei walks south.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Adina")
		ActorEvent_SetProperty("Move To", (35.25 * gciSizePerTile), (21.50 * gciSizePerTile))
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Move To", (36.25 * gciSizePerTile), (20.50 * gciSizePerTile))
	DL_PopActiveObject()
	fnCutsceneBlocker()

	--Adina faces east. Mei faces south. Should already be the case for Mei.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Adina")
		ActorEvent_SetProperty("Face", 1, 0)
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Face", 0, 1)
	DL_PopActiveObject()
	fnCutsceneBlocker()

	--Wait a bit.
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Switch on Adina's collision flag.
	fnCutscene([[ 
		EM_PushEntity("Adina")
			TA_SetProperty("Clipping Flag", true)
		DL_PopActiveObject()
		]])
	fnCutsceneBlocker()
	
	--Dialogue.
	fnStandardMajorDialogue()
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])
	fnCutscene([[ Append("Adina: ...[B][C]") ]])
	fnCutscene([[ Append("Adina: Here it is.[B][C]") ]])
	fnCutscene([[ Append("Adina: Mei, please smell this flower.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] It's not poisonous, is it?[B][C]") ]])
	fnCutscene([[ Append("Adina: I will say nothing.[P] You must trust me.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Sad] Oh geez...") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(15)
	fnCutsceneBlocker()
	
	--Switch Mei to the crouching pose.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Special Frame", "Crouch")
	DL_PopActiveObject()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Mei sniffs the flower. Doesn't take part as a major dialogue.
	fnCutscene([[ fnStandardDialogue("*sniff*") ]])
	fnCutscene([[ AL_SetProperty("Music", "Null") ]])
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Dialogue.
	fnStandardMajorDialogue()
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] ...[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Well that wasn't...[P] so...[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Blush] ...[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Blush] Oh I'm lightheaded... [B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Blush] What did...[P] you...[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Blush] So hard...[P] to think...[B][C]") ]])
	fnCutscene([[ Append("Adina: Please, smell it again.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Blush] Y-[P]yeah...[P] It smells...[P] so sweet.[B][C]") ]])
	fnCutscene([[ Append("*sniff*[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Blush] ...[B][C]") ]])
	fnCutscene([[ Append("Adina: How was it you said you feel?") ]])
	fnCutsceneBlocker()
	
	fnCutscene([[ fnSetMeiToWhiteout() ]])
    fnCutscene([[
    AdvCombat_SetProperty("Push Party Member", "Mei")
        AdvCombatEntity_SetProperty("Display Name", "Thrall")
    DL_PopActiveObject() ]])
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Special Frame", "CrouchMC")
	DL_PopActiveObject()
	fnCutscene([[ WD_SetProperty("FastShow") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "MC") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])
	fnCutscene([[ Append("Thrall:[E|MC] ...[B][C]") ]])
	fnCutscene([[ Append("Thrall:[E|MC] Obedient.[B][C]") ]])
	fnCutscene([[ Append("Thrall:[E|MC] I exist to serve you.[B][C]") ]])
	fnCutscene([[ Append("Thrall:[E|MC] Command me, my mistress.[B][C]") ]])
	fnCutscene([[ Append("Adina: What a response![P] I was not expecting this![B][C]") ]])
	fnCutscene([[ Append("Adina: Perhaps the vector amplified the potency?[P] No matter.[B][C]") ]])
	fnCutscene([[ Append("Adina: Thrall, you are charged with maintaining the plants on these salt flats.[B][C]") ]])
	fnCutscene([[ Append("Thrall:[E|MC] Yes mistress.[B][C]") ]])
	fnCutscene([[ Append("Adina: The tools you'll need are in my tool shed.[P] If you need pollen, report to me and I'll give you what you need.[B][C]") ]])
	fnCutscene([[ Append("Thrall:[E|MC] Yes mistress.[P] It will be done.") ]])
	fnCutsceneBlocker()
	
	--Wait a bit. Remove crouch forcing.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Special Frame", "Null")
	DL_PopActiveObject()
	fnCutscene([[ AL_SetProperty("Music", "AdinasTheme") ]])
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Generate some problems for the farm.
	fnCutscene([[ fnGenerateFarmProblems() ]])
	fnCutsceneBlocker()

--"NoHypnosis" has Mei make an excuse to leave.
elseif(sTopicName == "NoHypnosis") then
	
	--Common. This is done to clear up any pending blocks from the previous part of the scene.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] I'm not ready yet.[P] It's just too risky.[B][C]") ]])
	fnCutscene([[ Append("Adina: Rapprochement takes time.[P] I am patient.") ]])
	fnCutsceneBlocker()

--If Mei is "just saying hi" during a visit:
elseif(sTopicName == "JustHi") then
	
	--Common. This is done to clear up any pending blocks from the previous part of the scene.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] I just thought I'd come see you.[B][C]") ]])
	fnCutscene([[ Append("Adina: It pleases me to see you in such good spirits.[P] I hope you find you way in the world.") ]])
	fnCutsceneBlocker()

--If Mei wants to be re-hypnotised:
elseif(sTopicName == "ReHypnosis") then
	
	--Common. This is done to clear up any pending blocks from the previous part of the scene.
	WD_SetProperty("Hide")
	
	--Variables.
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	VM_SetVar("Root/Variables/Chapter1/SaltFlats/iIsMeiControlled", "N", 1.0)
	local iMeiRecontrolCount = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iMeiRecontrolCount", "N")
	VM_SetVar("Root/Variables/Chapter1/SaltFlats/iMeiRecontrolCount", "N", iMeiRecontrolCount + 1)
    VM_SetVar("Root/Variables/Chapter1/Scenes/iAdinaQuestVar", "N", 3.0)
	
	--Reboot the dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])
	
	--First time going for this:
	if(iMeiRecontrolCount == 0.0) then
		fnCutscene([[ Append("Mei:[E|Blush] Mistress...[P] could you...[B][C]") ]])
		fnCutscene([[ Append("Adina: You wish for me to enslave you again.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] Just for a little while...[P] I want to be with you...[B][C]") ]])
		fnCutscene([[ Append("Adina: Can your quest wait?[P] Are there others counting on you?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] A few days wouldn't hurt.[P] When the magic wears off, I'll leave.[P] I promise.[B][C]") ]])
		fnCutscene([[ Append("Adina: And if it doesn't?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] Then I will obey you until the end of time, mistress.[B][C]") ]])
		fnCutscene([[ Append("Adina: I don't think it will last that long![P] But, I can see you are sincere.[B][C]") ]])
		fnCutscene([[ Append("Adina: However, I did review my methods.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Did you learn something?[B][C]") ]])
		fnCutscene([[ Append("Adina: Yes.[P] The spell has scarcely an effect on an unwilling target.[B][C]") ]])
		fnCutscene([[ Append("Adina: I could hardly charm a fieldmouse.[P] I know, because I tried.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Surprise] So that means...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] You were always my mistress, before we even met...[B][C]") ]])
		fnCutscene([[ Append("Adina: Perhaps.[B][C]") ]])
		fnCutscene([[ Append("Adina: At the very least, you wanted it deeply, perhaps unconsciously.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] I want it now, so badly.[P] Please, mistress, I beg you![B][C]") ]])
		
		--Alraune:
		if(sMeiForm == "Alraune") then
			fnCutscene([[ Append("Adina: I fear I may not be able to.[P] As one of my kind, you may be resistant to the spell.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Blush] The spell is merely a formality.[P] I am already your slave.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Blush] Please, mistress.[P] Take your thrall's body.[P] Take it...[B][C]") ]])
			fnCutscene([[ Append("Adina: Again.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Blush] Take me...[P] Take...[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Blush] Please...[P] Enslave...[P] Please...[B][C]") ]])
			fnCutscene([[ Append("Adina: I'm sorry, I didn't hear you.") ]])
			fnCutsceneBlocker()
			
		--All other cases.
		else
			fnCutscene([[ Append("Adina: Oh, you do?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Blush] Yes, please![B][C]") ]])
			fnCutscene([[ Append("Adina: Again.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Blush] Please, enslave me.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Blush] En...[P]slave...[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Blush] Pl-[P]please...[B][C]") ]])
			fnCutscene([[ Append("Adina: I'm sorry, I didn't hear you.") ]])
			fnCutsceneBlocker()
		end
	
		fnCutscene([[ fnSetMeiToWhiteout() ]])
		fnCutscene([[
		AdvCombat_SetProperty("Push Party Member", "Mei")
			AdvCombatEntity_SetProperty("Display Name", "Thrall")
		DL_PopActiveObject() ]])
		fnCutscene([[ WD_SetProperty("FastShow") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "MC") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])
		fnCutscene([[ Append("Thrall:[E|MC] Pl...[P]ease...[B][C]") ]])
		fnCutscene([[ Append("Thrall:[E|MC] ...[B][C]") ]])
		fnCutscene([[ Append("Thrall:[E|MC] It is done.[P] I am yours, body and soul.[B][C]") ]])
		fnCutscene([[ Append("Thrall:[E|MC] Command me, mistress.[B][C]") ]])
		fnCutscene([[ Append("Adina: So you no longer even need a vector.[P] The magic was enough.[B][C]") ]])
		fnCutscene([[ Append("Thrall:[E|MC] Yes, mistress.[P] I am your thrall.[B][C]") ]])
		fnCutscene([[ Append("Thrall:[E|MC] I now see that, even when I am free willed, I am still your thrall.[P] Freedom is an illusion, a slave is what I am.[P] Forever.[B][C]") ]])
		fnCutscene([[ Append("Adina: You have come to this conclusion on your own?[B][C]") ]])
		fnCutscene([[ Append("Thrall:[E|MC] Yes, mistress.[P] If you order me to, I will forget it.[P] I will obey.[B][C]") ]])
		fnCutscene([[ Append("Adina: No, it is fine the way it is.[B][C]") ]])
		fnCutscene([[ Append("Adina: Now, enough chatter.[P] We must tend the little ones.[P] Thrall, go.[B][C]") ]])
		fnCutscene([[ Append("Thrall:[E|MC] Yes, mistress.[P] I obey.") ]])
	
	--Any other time:
	else
		local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
		fnCutscene([[ Append("Mei:[E|Blush] Mistress, my freedom weighs on me.[B][C]") ]])
		fnCutscene([[ Append("Adina: Would you like me to take it away?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] Would you?[P] I can't bear being away from you.[B][C]") ]])
		
		if(sMeiForm == "Alraune") then
			fnCutscene([[ Append("Adina: Then I will weave my spell...[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Blush] (Mistress' magic seems to have no effect on this leafy body...)[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Blush] (But...[P] I was always a slave.[P] Becoming a leaf-sister shouldn't change that...)[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Blush] (I...[P] I will not fail you, mistress.[P] I have no will.[P] I am mindless.[P] I am your thrall.)[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Blush] (I am a mindless thrall.[P] I am a mindless thrall...[P] I am a thrall...)[B][C]") ]])
			fnCutscene([[ Append("Adina: I cannot take from you what you have never had, slave.[B][C]") ]])
			fnCutscene([[ Append("Adina: You have always been my thrall, and always will be.") ]])
		else
			fnCutscene([[ Append("Adina: How can I take away what you never had?[B][C]") ]])
			fnCutscene([[ Append("Adina: You have always been my thrall, and always will be.") ]])
			
		end
		fnCutsceneBlocker()
	
		fnCutscene([[ fnSetMeiToWhiteout() ]])
		fnCutscene([[
		AdvCombat_SetProperty("Push Party Member", "Mei")
			AdvCombatEntity_SetProperty("Display Name", "Thrall")
		DL_PopActiveObject() ]])
		fnCutscene([[ WD_SetProperty("FastShow") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "MC") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])
		fnCutscene([[ Append("Adina: Isn't that right?[B][C]") ]])
		fnCutscene([[ Append("Thrall:[E|MC] Yes, mistress.[P] I was never free.[B][C]") ]])
		fnCutscene([[ Append("Adina: Now, tend to the fields.[B][C]") ]])
		fnCutscene([[ Append("Thrall:[E|MC] Yes, mistress.[P] My body exists to serve you.") ]])
	
	end
	fnCutsceneBlocker()
	
	--Generate some problems for the farm.
	fnCutscene([[ fnGenerateFarmProblems() ]])
	fnCutsceneBlocker()

end
