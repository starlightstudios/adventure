-- |[FarmHandler]|
--If Mei is currently working as a thrall on the salt flats farm, this is executed.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Base]|
--If this is the "Hello" then this is the first speech event.
if(sTopicName == "Hello") then

	-- |[Variable Checking]|
	local bIsAllFarmworkDone = true
	local bNeedsPollen = false
	for i = 0, 7, 1 do
		
		--Get the state.
		local iVariable = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iPlotState" .. i, "N")
		
		--At least one plot needs tending, so the farmwork is not done.
		if(iVariable ~= gci_SFF_NoTending) then
			bIsAllFarmworkDone = false
		end
		
		--If at least one plot needs pollen, Adina changes her dialogue.
		if(iVariable == gci_SFF_SpecialPollen) then
			bNeedsPollen = true
		end
		
	end
		
	-- |[Dialogue Setup]|
	--This is always a major dialogue.
	fnStandardMajorDialogue()
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "MC") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])

	-- |[Not All Farmwork Is Done]|
	if(bIsAllFarmworkDone == false) then

		-- |[Variables]|
		local iHasPollen  = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iHasPollen", "N")
		local iDaysPassed = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iDaysPassed", "N")
		local iMeiLovesAdina = VM_GetVar("Root/Variables/Chapter1/Scenes/iMeiLovesAdina", "N")

		-- |[Mei Needs Pollen]|
		--Adina gives Mei the pollen for the farm job.
		if(bNeedsPollen == true and iHasPollen == 0.0) then
			
			--Pollen get!
			VM_SetVar("Root/Variables/Chapter1/SaltFlats/iHasPollen", "N", 1.0)
			
			--Dialogue.
			fnCutscene([[ Append("Thrall: I require your pollen to complete my task, mistress.[B][C]") ]])
			fnCutscene([[ Append("Adina: Very well, take it. Breathe it in, let it soak into you.[B][C]") ]])
			fnCutscene([[ Append("Thrall: Yes mistress.[B][C]") ]])
			fnCutscene([[ Append("Adina: Now go, thrall.[B][C]") ]])
			fnCutscene([[ Append("Thrall: I serve.") ]])

		-- |[Mei Needs Pollen, and Has It]|
		--Adina tells Mei to get back to work.
		elseif(bNeedsPollen == true and iHasPollen == 1.0) then

			fnCutscene([[ Append("Adina: Thrall, your work is incomplete.[B][C]") ]])
			fnCutscene([[ Append("Thrall: Yes, mistress.[B][C]") ]])
			fnCutscene([[ Append("Adina: Complete it.[BLOCK]") ]])
	
			--Decision script is this script. It must be surrounded by quotes.
			local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
			fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
			fnCutscene(" WD_SetProperty(\"Add Decision\", \"Acquiesce\", " .. sDecisionScript .. ", \"YesMistress\") ")
			
			--Bonus option if Mei has been enslaved for 2+ days:
			if(iMeiLovesAdina == 1.0) then
				fnCutscene(" WD_SetProperty(\"Add Decision\", \"Thrall Wishes To Kiss You\",  " .. sDecisionScript .. ", \"KissHer\") ")
			end
			
			--Resistance Option.
			fnCutscene(" WD_SetProperty(\"Add Decision\", \"Resist\",  " .. sDecisionScript .. ", \"Resist\") ")
			fnCutsceneBlocker()
		
		-- |[Mei Needs No Pollen]|
		--Adina tells Mei to get back to work.
		else
			fnCutscene([[ Append("Adina: Thrall, your work is incomplete.[B][C]") ]])
			fnCutscene([[ Append("Thrall: Yes, mistress.[B][C]") ]])
			fnCutscene([[ Append("Adina: Complete it.[BLOCK]") ]])
	
			--Decision script is this script. It must be surrounded by quotes.
			local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
			fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
			fnCutscene(" WD_SetProperty(\"Add Decision\", \"Acquiesce\", " .. sDecisionScript .. ", \"YesMistress\") ")
			fnCutscene(" WD_SetProperty(\"Add Decision\", \"Resist\",  " .. sDecisionScript .. ", \"Resist\") ")
			fnCutsceneBlocker()
		end

	-- |[Farmwork Is Done]|
	else

		--Variables.
		local iTimeOfDay = VM_GetVar("Root/Variables/Global/Time/iTimeOfDay", "N")
		
		--If it's morning, time will have advanced to noon when the work is done:
		if(iTimeOfDay == 9.0) then
			
			--Dialogue.
			fnCutscene([[ Append("Thrall: My tasks are complete, mistress.[B][C]") ]])
			fnCutscene([[ Append("Adina: Very good, thrall.[P] Your work resumes after lunch.") ]])
			fnCutsceneBlocker()
		
			--Activate a fade to black.
			fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, gcf_Noon_R, gcf_Noon_G, gcf_Noon_B, gcf_Noon_A, 0, 0, 0, 1, true) ]])
			fnCutsceneWait(180)
			fnCutsceneBlocker()
			
			--Fade to noon, which is clear.
			fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 1, gcf_Noon_R, gcf_Noon_G, gcf_Noon_B, gcf_Noon_A, true) ]])
			fnCutsceneWait(60)
			fnCutsceneBlocker()
			
			--Dialogue.
			fnStandardMajorDialogue()
			fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "MC") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])
			fnCutscene([[ Append("Adina: Thrall.[P] Tend to the little ones.[B][C]") ]])
			fnCutscene([[ Append("Thrall: As you wish, mistress.") ]])
			fnCutsceneBlocker()
	
			--Generate some problems for the farm.
			fnCutscene([[ fnGenerateFarmProblems() ]])
			fnCutsceneBlocker()
			
			--Advance what time it is.
			VM_SetVar("Root/Variables/Global/Time/iTimeOfDay", "N", 12.0)
			VM_SetVar("Root/Variables/Chapter1/SaltFlats/iTasksDone", "N", 0)
		
		--If it's noon:
		elseif(iTimeOfDay == 12.0) then
			fnCutscene([[ Append("Thrall: My tasks are complete, mistress.[B][C]") ]])
			fnCutscene([[ Append("Adina: Very good, thrall.[P] There are more tasks for you.[P] Go do them.[B][C]") ]])
			fnCutscene([[ Append("Thrall: I obey.") ]])
			fnCutsceneBlocker()
	
			--Generate some problems for the farm.
			fnCutscene([[ fnGenerateFarmProblems() ]])
			fnCutsceneBlocker()
			
			--Advance what time it is.
			VM_SetVar("Root/Variables/Global/Time/iTimeOfDay", "N", 15.0)
			VM_SetVar("Root/Variables/Chapter1/SaltFlats/iTasksDone", "N", 0)
		
		--If it's 3 o clock, time will have advanced to sunset:
		elseif(iTimeOfDay == 15.0) then
			fnCutscene([[ Append("Thrall: I have completed my tasks, mistress.[B][C]") ]])
			fnCutscene([[ Append("Adina: Perhaps some food is in order.[P] Please, dine with me.[B][C]") ]])
			fnCutscene([[ Append("Thrall: Your will is my will, mistress.") ]])
			fnCutsceneBlocker()
		
			--Activate a fade to black.
			fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, gcf_Evening_R, gcf_Evening_G, gcf_Evening_B, gcf_Evening_A, 0, 0, 0, 1, true) ]])
			fnCutsceneWait(180)
			fnCutsceneBlocker()
			
			--Fade to night colors.
			fnCutscene([[ AL_SetProperty("Activate Fade", 135, gci_Fade_Under_GUI, true, 0, 0, 0, 1, gcf_Night_R, gcf_Night_G, gcf_Night_B, gcf_Night_A, true) ]])
			fnCutsceneWait(60)
			fnCutsceneBlocker()
			
			--Dialogue.
			fnStandardMajorDialogue()
			fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "MC") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])
			fnCutscene([[ Append("Adina: The sun has set. Thrall, put the tools away, it is time to rest.[B][C]") ]])
			fnCutscene([[ Append("Thrall: Yes, mistress.") ]])
			fnCutsceneBlocker()
			
			--Advance what time it is.
			VM_SetVar("Root/Variables/Global/Time/iTimeOfDay", "N", 18.0)
			VM_SetVar("Root/Variables/Chapter1/SaltFlats/iTasksDone", "N", 0)
			
			--Flags.
			VM_SetVar("Root/Variables/Chapter1/SaltFlats/iPutThingsAway", "N", 1)

		--If it's night, time to go to bed.
		elseif(iTimeOfDay == 18.0) then
		
			--Variables.
			local iHasWater = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iHasWater", "N")
			local iHasFertilizer = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iHasFertilizer", "N")
			
			--Needs to put the tools away.
			if(iHasWater > 0 or iHasFertilizer > 0) then
				fnStandardMajorDialogue()
				fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "MC") ]])
				fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])
				fnCutscene([[ Append("Adina: Thrall, put the tools away.[B][C]") ]])
				fnCutscene([[ Append("Thrall: Yes, mistress.") ]])
				fnCutsceneBlocker()
		
			--Ending case.
			else
				fnStandardMajorDialogue()
				fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "MC") ]])
				fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])
				fnCutscene([[ Append("Adina: Let us retire to my chambers, then.[P] Thrall, please go ahead.[B][C]") ]])
				fnCutscene([[ Append("Thrall: Yes, mistress.[P] I will await you.") ]])
				fnCutsceneBlocker()
				
				VM_SetVar("Root/Variables/Chapter1/SaltFlats/iPutThingsAway", "N", 0)
				VM_SetVar("Root/Variables/Chapter1/SaltFlats/iAwaitAdina", "N", 1)
                VM_SetVar("Root/Variables/Chapter1/Scenes/iAdinaQuestVar", "N", 4.0)
			end
		end
	end

-- |[YesMistress]|
--Dialogue ender.
elseif(sTopicName == "YesMistress") then

	--Common. This is done to clear up any pending blocks from the previous part of the scene.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "MC") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])
	fnCutscene([[ Append("Thrall: Yes, mistress.[P] I obey.") ]])
	fnCutsceneBlocker()

-- |[Resist]|
--This is how you break out of Adina's charms.
elseif(sTopicName == "Resist") then

	--Common. This is done to clear up any pending blocks from the previous part of the scene.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "MC") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])
	fnCutscene([[ Append("Thrall: I -[P] I -[P] will -[P][CLEAR]") ]])
	fnCutscene([[ Append("Adina: What is it, thrall?[BLOCK]") ]])
	
	--Decision script is this script. It must be surrounded by quotes.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"Acquiesce\", " .. sDecisionScript .. ", \"YesMistress\") ")
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"Resist\",  " .. sDecisionScript .. ", \"ResistAgain\") ")
	fnCutsceneBlocker()

-- |[ResistAgain]|
--Mei is too swole to control!
elseif(sTopicName == "ResistAgain") then

	--Variables.
	local iDaysPassed        = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iDaysPassed", "N")
	local iTasksDoneTotal    = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iTasksDoneTotal", "N")
	local iMeiRecontrolCount = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iMeiRecontrolCount", "N")

	--Common. This is done to clear up any pending blocks from the previous part of the scene.
	WD_SetProperty("Hide")
	
	--Set variables so Mei can leave.
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	VM_SetVar("Root/Variables/Chapter1/SaltFlats/iIsMeiControlled", "N", 0.0)
	
	--Reboot the dialogue.
	fnCutscene([[ WD_SetProperty("FastShow") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "MC") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])
	fnCutscene([[ Append("Thrall:[E|MC] I...[P] will...[P] ob...[P] ey...[B][C]") ]])
	fnCutscene([[ Append("Thrall:[E|MC] I...") ]])
	fnCutsceneBlocker()
	
	--Un-whiteout Mei.
	fnCutscene([[ fnClearWhiteout() ]])
    fnCutscene([[
    AdvCombat_SetProperty("Push Party Member", "Mei")
        AdvCombatEntity_SetProperty("Display Name", "DEFAULT")
    DL_PopActiveObject() ]])
	fnCutscene([[ WD_SetProperty("FastShow") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Offended") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])
	
	--If this is a re-hypnosis:
	if(iMeiRecontrolCount > 0) then
        VM_SetVar("Root/Variables/Chapter1/Scenes/iAdinaQuestVar", "N", 6.0)
		fnCutscene([[ Append("Mei:[E|MC] I -[P] am -[P][E|Blush] sorry mistress.[P] The spell has faded.[B][C]") ]])
		fnCutscene([[ Append("Adina: Hm.[P] The road calls to you again.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Yes.[P] I suppose I must return to my journey.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] I will miss you.[P] I will miss...[P] this...[B][C]") ]])
		fnCutscene([[ Append("Adina: Return to me any time.[P] I will be here, waiting for you.[P] Perhaps next time will be the last.") ]])
	
	--If no days have passed and Mei has done no tasks:
    elseif(iDaysPassed == 0.0 and iTasksDoneTotal == 0.0) then
        VM_SetVar("Root/Variables/Chapter1/Scenes/iAdinaQuestVar", "N", 5.0)
		fnCutscene([[ Append("Mei:[E|Offended] I -[P] will do no such thing![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Angry] What did you do to me!?[B][C]") ]])
		fnCutscene([[ Append("Adina: Ah, it seems you have overcome the effects of the spell.[P] I suppose this was inevitable.[B][C]") ]])
		fnCutscene([[ Append("Adina: It seems you are stronger willed than most, as it lasted mere moments.[P] Impressive.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Angry] You drug me and try to use me for manual labour?[P] I don't think so![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Angry] Prepare to -[P][CLEAR]") ]])
		fnCutscene([[ Append("Adina: Please understand, Mei.[P] I meant no harm to you.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Angry] No harm!?[B][C]") ]])
		fnCutscene([[ Append("Adina: Indeed, in these few moments you were under my control, I merely asked a bit of labour from you.[B][C]") ]])
		fnCutscene([[ Append("Adina: I could well have asked you to slit your own throat, but I did not.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] ...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] You used hypnotic magic to prove a point?[B][C]") ]])
		fnCutscene([[ Append("Adina: Yes, I did.[B][C]") ]])
		fnCutscene([[ Append("Adina: My understanding of humans is indeed limited.[P] I apologize if there was any discomfort.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] Well, there wasn't...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] I didn't think I needed to point out that we humans don't appreciate being controlled![B][C]") ]])
		fnCutscene([[ Append("Adina: Is there anything I can do to compensate you for it?[P] I truly am sorry.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] N-[P]no, I suppose no real harm was done.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] And it seems you made your point, if in the weirdest way possible.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Even at your total mercy, you didn't hurt me.[B][C]") ]])
		fnCutscene([[ Append("Adina: Indeed.[P] At least I have learned much from you, even if I have failed to earn your trust.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] No it's all right.[P] I think I understand you a bit better, too.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Just, ask next time you're going to do that.[B][C]") ]])
		fnCutscene([[ Append("Adina: I have much to think about.[B][C]") ]])
		fnCutscene([[ Append("Adina: Mei?[P] To be honest, I had truly assumed you would rent me in two when you had the opportunity.[B][C]") ]])
		fnCutscene([[ Append("Adina: Perhaps not all humans are truly dangerous.[P] Perhaps some can be reasoned with.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Maybe not all Alraunes are dangerous, either.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Happy] Misguided, but not dangerous.[B][C]") ]])
		fnCutscene([[ Append("Adina: Indeed.[P] Thank you, Mei.[B][C]") ]])
		fnCutscene([[ Append("Adina: If there is anything I can do to aid you on your journey, do not hesitate to ask.") ]])
	
	--If Mei has done at least one task but no days have passed:
	elseif(iDaysPassed == 0.0 and iTasksDoneTotal > 0.0) then
        VM_SetVar("Root/Variables/Chapter1/Scenes/iAdinaQuestVar", "N", 5.0)
		fnCutscene([[ Append("Mei:[E|Offended] I -[P] will do no such thing![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Angry] What did you do to me!?[B][C]") ]])
		fnCutscene([[ Append("Adina: Ah, it seems you have overcome the effects of the spell.[P] I suppose this was inevitable.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Angry] You drugged me![P] And had me do your dirty work![B][C]") ]])
		fnCutscene([[ Append("Adina: Drugged?[P] Hardly.[P] That was a bit of magic, the pollen merely acted as a conduit.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] Grrr...[B][C]") ]])
		fnCutscene([[ Append("Adina: I apologize, but I hope this means you can trust me now.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Angry] Do you think I'm stupid!?[B][C]") ]])
		fnCutscene([[ Append("Adina: ...[B][C]") ]])
		fnCutscene([[ Append("Adina: Mei, when you were under my influence, I could have done anything to you.[P] I could have ordered you to slash your own throat, and you would complied.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] ...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] And?[B][C]") ]])
		fnCutscene([[ Append("Adina: And I did no such thing, just as you did not strike me when my back was turned.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] But you were making me do farm work![B][C]") ]])
		fnCutscene([[ Append("Adina: If you would have preferred, I could have had you stand in the field until the magic subsided.[P] That would likely have been boring.[P] Obviously, I could not have asked you your preference![B][C]") ]])
		fnCutscene([[ Append("Adina: It was not difficult work, was it?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Well...[P] no...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Happy] In fact, I was kind of enjoying it.[P] It tingled whenever I obeyed...[B][C]") ]])
		fnCutscene([[ Append("Adina: Now we have both put ourselves at the mercy of the other and not been destroyed for it.[P] Clearly we are creatures of honor.[B][C]") ]])
		fnCutscene([[ Append("Adina: Honestly, Mei, I truly did expect you to strike me down when you had the chance.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] I guess in a way, you really didn't violate my trust.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] And it did feel good, even if I wasn't in control of myself...[B][C]") ]])
		fnCutscene([[ Append("Adina: There is no need for further deception, then.[P] The point is made.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Okay, Adina, you're on the level.[P] I guess.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] But you need to ask before you do something like that.[B][C]") ]])
		fnCutscene([[ Append("Adina: I understand.[P] I meant nothing by it.[B][C]") ]])
		fnCutscene([[ Append("Adina: You have given me much to think about, Mei.[P] Perhaps not all humans are as dangerous as I had thought.[B][C]") ]])
		fnCutscene([[ Append("Adina: I will...[P] meditate on this.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Could you maybe tell the other Alraunes?.[B][C]") ]])
		fnCutscene([[ Append("Adina: Already I have sent word through the little ones, but they are as obstinate as I was.[P] I doubt they will believe me.[B][C]") ]])
		fnCutscene([[ Append("Adina: Still, you have a friend here if you are in need.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] This has been a very bizarre friendship...") ]])
	
	--If less than two days have passed:
	elseif(iDaysPassed < 2.0) then
        VM_SetVar("Root/Variables/Chapter1/Scenes/iAdinaQuestVar", "N", 5.0)
		fnCutscene([[ Append("Mei:[E|Offended] I -[P] will do no such thing![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Angry] What did you do to me!?[B][C]") ]])
		fnCutscene([[ Append("Adina: Ah, it seems you have overcome the effects of the spell.[P] I suppose this was inevitable.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Angry] You -[P] you -[P][CLEAR]") ]])
		fnCutscene([[ Append("Mei:[E|Angry] You drugged me and violated me![B][C]") ]])
		fnCutscene([[ Append("Adina: There were no drugs present in the pollen.[P] I merely used it as a vector for a spell I had woven.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Angry] Don't try to hide behind technicalities![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Angry] You forced yourself on me![B][C]") ]])
		fnCutscene([[ Append("Adina: When you were under my control, you could well have said no.[P] I would even have let you sleep on my bed.[B][C]") ]])
		fnCutscene([[ Append("Adina: We alraunes are comfortable on a bed of grass, but none grows down there.[P] I would have slept outside.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Angry] B-[P]but-[P][CLEAR]") ]])
		fnCutscene([[ Append("Adina: Mei, when you had the opportunity to lay with me, you did.[P] Your inhibitions had been removed, and you chose to.[B][C]") ]])
		fnCutscene([[ Append("Adina: I am sorry if it seemed any other way.[P] I thought I was making you happy.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] ...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] You're right...[P] I'm not normally attracted to women, but you...[B][C]") ]])
		fnCutscene([[ Append("Adina: I apologize.[P] We alraunes do not think of sex the way you humans do.[P] I see that now.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] *sigh*[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] I am so confused right now.[B][C]") ]])
		fnCutscene([[ Append("Adina: Still, the point I had been trying to make with my spell was that I would not bring harm to you, even were you helpless before me.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] And...[P] you didn't...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Okay, okay.[P] I get what you were going for.[P] But -[P] asking is very important to humans.[P] We don't like being manipulated.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Happy] At least, not without our permission.[B][C]") ]])
		fnCutscene([[ Append("Adina: Perhaps by learning about each other, our races can coexist peacefully.[B][C]") ]])
		fnCutscene([[ Append("Adina: Mei, you have given me much to consider.[P] Please, if there is anything I can do to aid you on your journey, do not hesitate to ask.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Y-[P]yeah, sure.[P] You've given me a lot to think about, too.") ]])
	
	--If two or more days have passed:
	else
        VM_SetVar("Root/Variables/Chapter1/Scenes/iAdinaQuestVar", "N", 6.0)
		fnCutscene([[ Append("Mei:[E|Offended] I -[P] mistress!?[P][CLEAR]") ]])
		fnCutscene([[ Append("Mei:[E|Angry] Why have you returned my will?[P] I only wanted to serve you![B][C]") ]])
		fnCutscene([[ Append("Adina: I have done nothing to you.[P] You have broken the spell of your own accord.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] Huh...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] You're right, but...[P] I want to obey, and yet still be of my own mind...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Cry] I don't know any more![B][C]") ]])
		fnCutscene([[ Append("Adina: Please, do not cry.[P] You are confused right now.[P] Perhaps the spell's effect still lingers?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] No, it's not that.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] The spell is gone, but I am driven by something else.[P] I still have things I want to do with my life.[B][C]") ]])
		fnCutscene([[ Append("Adina: Your journey to find your home?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] I think so.[P] I can't stay here just yet.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] I am sorry, mistress, but I must leave.[P] Another path calls to me.[B][C]") ]])
		fnCutscene([[ Append("Adina: Mei, this is not at all what I had expected.[P] I had merely been trying to prove to you that I could be trusted not to harm you, if I had the chance.[B][C]") ]])
		fnCutscene([[ Append("Adina: But it seems you found something you had been looking for in me.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Happy] You are someone who loves me and will take care of me.[P] Few humans are lucky enough to find someone like you.[B][C]") ]])
		fnCutscene([[ Append("Adina: And you, in turn, care deeply for the little ones here.[P] I can see that in how you tend to them.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] I merely put my love for you into care for them.[B][C]") ]])
		fnCutscene([[ Append("Adina: Still, I had not expected this.[P] Perhaps not all humans are beyond redemption.[B][C]") ]])
		fnCutscene([[ Append("Adina: Perhaps by learning of one another, Alraune and Human can live in harmony...[B][C]") ]])
		fnCutscene([[ Append("Adina: I must meditate on this as I care for the little ones.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] And I need to find out what it is that drives me...[B][C]") ]])
		fnCutscene([[ Append("Adina: Truly, Mei, I wish you luck on your journey.[P] You are always welcome here should you find your greater place.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Yeah.[P] I might take you up on that later...") ]])
		
	end
	fnCutsceneBlocker()
	
	--Remove all farmwork indicators.
	fnCutscene([[ fnPlaceProblemIndicators(true) ]])
	fnCutsceneBlocker()

-- |[Kiss Her]|
--Mei wants to kiss her mistress.
elseif(sTopicName == "KissHer") then

	--Clean.
	WD_SetProperty("Hide")

    --Clear censor bars.
    WD_SetProperty("Clear Censor Bars")
	
	--Reboot the dialogue.
	fnCutscene([[ WD_SetProperty("FastShow") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "MC") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])
	fnCutscene([[ Append("Thrall: Mistress, your slave wishes to kiss you.[B][C]") ]])
	fnCutscene([[ Append("Adina: Mm, come closer.[B][C]") ]])
	fnCutscene([[ Append("Adina: *kiss*[B][C]") ]])
	fnCutscene([[ Append("Thrall: Mistress...[P] Lower...[B][C]") ]])
	fnCutscene([[ Append("Adina: Ah, well then...") ]])
	fnCutsceneBlocker()
	
	--Scene.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene") ]])
	fnCutscene([[ Append("There, on the salted earth, Mei knelt before her mistress.[P] A look of bemusement on her face, Adina drew towards her ensorcelled lover.[B][C]") ]])
	fnCutscene([[ Append("With no will of her own, Mei's lust grew.[P] Yet, she did not act on it, could not, until her mistress allowed her.[P] Each second built Mei's desire higher...[B][C]") ]])
	fnCutscene([[ Append("Her mistress lowered the green guards around her most delicate area, and Mei pressed her lips softly against her mistresses lips.[P] She kissed her in total submission.[B][C]") ]])
	fnCutscene([[ Append("Her task complete, Mei's empty mind stared at the vagina of her mistress.[P] She wished to kiss her again, but had not been told to.[P] Adina chuckled to herself.[B][C]") ]])
	fnCutscene([[ Append("'Again, Thrall'[P] she said.[P] Mei complied instantly and without question.[P] Each kiss brought her closer to pure mindless bliss.[P] Each kiss bonded her closer to her mistress.[B][C]") ]])
	fnCutscene([[ Append("She brought her hands up to steady her mistress' legs, and slipped her tongue into Adina's sex.[P] A small amount of nectar touched Mei's tongue, but she had not been told to taste it, so she did not.[P] She continued the deep kiss.[B][C]") ]])
	fnCutscene([[ Append("Mei kissed her mistress, scarcely aware of anything save pure ecstasy in her own, total, domination.[B][C]") ]])
	fnCutscene([[ Append("Her task complete, she mindlessly stood in front of her mistress as if nothing had happened.[P] It was time to resume her work.") ]])
	fnCutsceneBlocker()

end
