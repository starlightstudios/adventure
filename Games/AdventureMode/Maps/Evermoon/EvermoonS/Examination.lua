-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Examinations]|
--South-central sign.
if(sObjectName == "Sign SC") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Leader] (\"West:: Trannadar Trading Post.[P] South:: Salt Flats.[P] East:: Gemcutter's Cabin.[P] If you like my signs, let me know!\" -Nadia)") ]])
	fnCutsceneBlocker()
	
	--Variables.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iHasReadNadiaSign", "N", 1.0)
	WD_SetProperty("Unlock Topic", "Signs", 1)
    
elseif(sObjectName == "Sign East") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Leader] (\"West:: Trannadar Trading Post.[P] South:: Gemcutter's Cabin\" -Nadia)") ]])
	fnCutsceneBlocker()
	
	--Variables.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iHasReadNadiaSign", "N", 1.0)
	WD_SetProperty("Unlock Topic", "Signs", 1)

--Central sign.
elseif(sObjectName == "Sign Center") then

	--Variables.
	local iHasBeeForm = VM_GetVar("Root/Variables/Global/Mei/iHasBeeForm", "N")

	--Normal case:
	if(iHasBeeForm == 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Thought:[VOICE|Leader] (\"Caution:: Bee Girls abound![P] You probably don't want to get dragged off by them, I bet!\"[P] -Nadia.)[B][C]") ]])
		fnCutscene([[ Append("Thought:[VOICE|Leader] (That doesn't sound too encouraging...)") ]])
		fnCutsceneBlocker()
	
	--Mei has been form:
	else
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Thought:[VOICE|Leader] (\"Caution:: Bee Girls abound![P] You probably don't want to get dragged off by them, I bet!\"[P] -Nadia.)[B][C]") ]])
		fnCutscene([[ Append("Thought:[VOICE|Leader] (Best thing that's ever happened to me, but I guess some humans are wary of the bees.)") ]])
		fnCutsceneBlocker()
	end
	
	VM_SetVar("Root/Variables/Chapter1/Scenes/iHasReadNadiaSign", "N", 1.0)
	WD_SetProperty("Unlock Topic", "Signs", 1)

--Western sign.
elseif(sObjectName == "Sign West") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Leader] (\"You just proved sign advertising works![P] West:: Trannadar Trading Post.[P] East:: Gemcutter's Cabin.\"[P] -Nadia.)") ]])
	fnCutsceneBlocker()
	
	VM_SetVar("Root/Variables/Chapter1/Scenes/iHasReadNadiaSign", "N", 1.0)
	WD_SetProperty("Unlock Topic", "Signs", 1)

--Bed in the ruined cabin.
elseif(sObjectName == "Ruined Cabin") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Leader] (It doesn't look like anyone has used this place in a long time.)") ]])
	fnCutsceneBlocker()

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
