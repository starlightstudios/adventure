-- |[Constructor]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = fnResolveDirectory() --Map's name is the name of the folder it is in.

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[Standard]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
	AL_SetProperty("Music", "ForestTheme")
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/EvermoonS/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", "EvermoonS")

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
    
    -- |[Map Handling]|
    --Resolve the map's variable.
    fnStandardCh1Map(sLevelName)

-- |[Post-Exec]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	--Special: If this flag is tripped, spawn Florentina in the NW corner for the slime TF scene.
	local iSlimeMeetFlorentina = VM_GetVar("Root/Variables/Chapter1/Scenes/iSlimeMeetFlorentina", "N")
	if(iSlimeMeetFlorentina == 1.0) then
		
		--Unset the flag.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iSlimeMeetFlorentina", "N", 1.0)

		--Spawn and position Florentina.
		fnSpecialCharacter("Florentina", 16, 9, gci_Face_NorthEast, false, nil)
	end
	
    --If this flag is tripped, re-add Florentina after the Adina stuff. This is NC+ only.
    local iReAddFlorentinaAfterLeaving = VM_GetVar("Root/Variables/Chapter1/Scenes/iReAddFlorentinaAfterLeaving", "N")
    if(iReAddFlorentinaAfterLeaving == 1.0) then
        fnAddPartyMember("Florentina", false)
        VM_SetVar("Root/Variables/Chapter1/Scenes/iReAddFlorentinaAfterLeaving", "N", 0.0)
    end
    
	--Overlays.
    fnLightForest()

end
