-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ===================================== Warp Activation ==================================== ]|
if(sObjectName == "WarpActivate") then
    fnWarpUnlockHandler("Root/Variables/Chapter1/Campfires/iEvermoonS", 43.25, 18.50)

-- |[ ============================== Nadia News Delivery: Mycela =============================== ]|
--Nadia delivering news after the player warps.
elseif(sObjectName == "NadiaNewsN" or sObjectName == "NadiaNewsS" or sObjectName == "NadiaNewsE") then

    -- |[Activation Check]|
    --To activate this scene, the player must complete the Mycela quest in St. Fora's, then warp three or more
    -- times and then touch one of these triggers. Talking to Mycela at Breanne's disables the scene.
    local iWarpsSinceMycela  = VM_GetVar("Root/Variables/Chapter1/Mycela/iWarpsSinceMycela", "N")
    local iNadiaToldOfMycela = VM_GetVar("Root/Variables/Chapter1/Mycela/iNadiaToldOfMycela", "N")
    if(iNadiaToldOfMycela == 1.0 or iWarpsSinceMycela < 3.0) then return end
    
    -- |[Flag]|
    VM_SetVar("Root/Variables/Chapter1/Mycela/iNadiaToldOfMycela", "N", 1.0)
    
    -- |[Spawn Nadia]|
    TA_Create("Nadia")
        TA_SetProperty("Position", -1, -1)
        TA_SetProperty("Facing", gci_Face_South)
        TA_SetProperty("Clipping Flag", false)
        fnSetCharacterGraphics("Root/Images/Sprites/Nadia/", false)
    DL_PopActiveObject()

    -- |[Initial Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Voice:[VOICE|Nadia] Hey![P] Mei![P] Hold up a minute!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    -- |[Movement]|
    fnCutsceneTeleport("Nadia", 46.75, 33.50)
    fnCutsceneBlocker()
    fnCutsceneMove("Nadia", 46.75, 19.50)
    fnCutsceneMove("Mei", 46.25, 18.50)
    fnCutsceneFace("Mei", 0, 1)
    fnCutsceneMove("Florentina", 47.25, 19.50)
    fnCutsceneFace("Florentina", 0, 1)
    
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    -- |[Call Scene]|
    --This handles the dialogue.
    LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/Nadia_InformOfMycela/Scene_Begin.lua")

    -- |[Nadia's Exit]|
    fnCutsceneMove("Nadia", 46.75, 33.50)
    fnCutsceneTeleport("Nadia", -1.25, -1.50)
    
    -- |[Finish Up]|
    fnAutoFoldParty()
    fnCutsceneBlocker()

-- |[ ======================================= Rochea NC+ ======================================= ]|
elseif(sObjectName == "Alraunes") then

    -- |[Activation Check]|
    --To activate, the player must have alraune form without having met Rochea. This is only possible
    -- during NC+.
    local iHasAlrauneForm = VM_GetVar("Root/Variables/Global/Mei/iHasAlrauneForm", "N")
    local iMetRochea      = VM_GetVar("Root/Variables/Chapter1/Alraunes/iMetRochea", "N")
    if(iHasAlrauneForm ~= 1.0 or iMetRochea == 1.0) then return end
    
    -- |[Variables]|
    local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
    
    -- |[Flags]|
    VM_SetVar("Root/Variables/Chapter1/Alraunes/iMetRochea", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter1/Alraunes/iIsMeiForeigner", "N", 1.0)
    
    --Topics.
	WD_SetProperty("Unlock Topic", "Alraunes", 1)
	WD_SetProperty("Unlock Topic", "CleansingFungus", 1)
	WD_SetProperty("Unlock Topic", "Cultists", 1)
	WD_SetProperty("Unlock Topic", "Name", 1)
    
    -- |[Spawn NPCs]|
    TA_Create("Rochea")
        TA_SetProperty("Position", -1, -1)
        TA_SetProperty("Facing", gci_Face_South)
        TA_SetProperty("Clipping Flag", false)
        fnSetCharacterGraphics("Root/Images/Sprites/Rochea/", false)
    DL_PopActiveObject()
    TA_Create("Alraune")
        TA_SetProperty("Position", -1, -1)
        TA_SetProperty("Facing", gci_Face_South)
        TA_SetProperty("Clipping Flag", false)
        fnSetCharacterGraphics("Root/Images/Sprites/Alraune/", false)
    DL_PopActiveObject()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Voice:[VOICE|Rochea] Leaf-sister![P] Do you not know where you are?[B][C]") ]])
    fnCutscene([[ Append("Mei:[VOICE|Mei] Huh?[P] Who said that?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    -- |[Movement]|
    fnCutsceneMove("Mei", 46.25, 10.50)
    fnCutsceneFace("Mei", 0, 1)
    fnCutsceneTeleport("Rochea", 45.75, 19.50)
    fnCutsceneTeleport("Alraune", 46.75, 19.50)
    fnCutsceneBlocker()
    fnCutsceneMove("Rochea", 45.75, 12.50)
    fnCutsceneMove("Alraune", 46.75, 12.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Rochea", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Alraune", "Neutral") ]])
    fnCutscene([[ Append("Rochea:[E|Neutral] Greetings, I am Rochea.[P] This is Thistle.[B][C]") ]])
    
    -- |[Not in Alraune Form]|
    if(sMeiForm ~= "Alraune") then
        fnCutscene([[ Append("Rochea:[E|Neutral] You...[P] are not an alraune.[P] I apologize.[P] The little ones must be playing a trick on me.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Blush] Actually...") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(15)
        fnCutsceneBlocker()
    
        --Flash the active character to white. Immediately after, execute the transformation.
        Cutscene_CreateEvent("Flash Mei White", "Actor")
            ActorEvent_SetProperty("Subject Name", "Mei")
            ActorEvent_SetProperty("Flashwhite Quickly")
        DL_PopActiveObject()
        fnCutsceneWait(5)
        fnCutsceneBlocker()
        fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Alraune.lua") ]])
        fnCutsceneWait(gci_Flashwhite_Ticks_Total)
        fnCutsceneBlocker()

        --Now wait a little bit.
        fnCutsceneWait(30)
        fnCutsceneBlocker()
    
        --Resume dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Rochea", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Alraune", "Neutral") ]])
        fnCutscene([[ Append("Mei:[E|Happy] Ta-daa![B][C]") ]])
        fnCutscene([[ Append("Rochea:[E|Neutral] Very impressive.[B][C]") ]])
        fnCutscene([[ Append("Thistle:[E|Neutral] Some sort of disguise magic![P] Incredible![P] But you cannot hide your pure heart from the little ones.[B][C]") ]])
        fnCutscene([[ Append("Rochea:[E|Neutral] They told us a soul attuned with nature was here.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Smirk] It's nice to meet you, Rochea.[P] My name is Mei.[B][C]") ]])
        fnCutscene([[ Append("Rochea:[E|Neutral] But if you are an alraune, surely you know you ought to introduce yourself when you enter a new coven's territory?[B][C]") ]])
    
    -- |[In Alraune Form]|
    else
        fnCutscene([[ Append("Rochea:[E|Neutral] Surely you know you ought to introduce yourself when you enter a new coven's territory?[B][C]") ]])
    end
    fnCutscene([[ Append("Thistle:[E|Neutral] Just where are you from that you do not know the customs here?[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] Hong Kong.[P] It's...[P] really far away.[B][C]") ]])
    fnCutscene([[ Append("Rochea:[E|Neutral] Oh dear.[P] I have never heard of such a place.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Sad] I had a feeling you'd say that...[B][C]") ]])
    fnCutscene([[ Append("Rochea:[E|Neutral] Then I see no problem here.[P] The little ones will inform you of the local customs.[P] If you are just passing through, feel free to visit.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Sad] My apologies for being rude...[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Smirk] But it is very nice to see some friendly faces, indeed, some leaf-sisters I can relate to![B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] I'm looking for a way back to my home, so I may call upon you if you can help.[B][C]") ]])
    fnCutscene([[ Append("Thistle:[E|Neutral] Though I am loathe to admit it, the trading post not far from here may be the best place to ask.[P] They know more of the wider world there.[B][C]") ]])
    fnCutscene([[ Append("Rochea:[E|Neutral] Indeed.[P] Just don't ask Florentina for anything.[P] She's likely to take advantage of you.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Smirk] I was going that direction anyway.[P] Thank you for the advice.[B][C]") ]])
    fnCutscene([[ Append("Rochea:[E|Neutral] We will be in touch.[P] Good luck on your quest, leaf-sister.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(30)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneMove("Rochea", 45.75, 20.50)
    fnCutsceneMove("Alraune", 46.75, 20.50)
    fnCutsceneBlocker()
    fnCutsceneTeleport("Rochea", -1.75, -1.50)
    fnCutsceneTeleport("Alraune", -1.75, -1.50)
    fnCutsceneBlocker()
    
-- |[ ==================================== Exiting Slime TF ==================================== ]|
--This trigger is only fired after the slime TF scene, and only if Florentina was in the party.
elseif(sObjectName == "SlimeTrigger") then

	-- |[Flags]|
	--Check the variable.
	local iSlimeMeetFlorentina = VM_GetVar("Root/Variables/Chapter1/Scenes/iSlimeMeetFlorentina", "N")
	local iFlorentinaSawSmartSlimes = VM_GetVar("Root/Variables/Chapter1/Scenes/iFlorentinaSawSmartSlimes", "N")
	if(iSlimeMeetFlorentina ~= 1.0) then return end

	--Unset the flag.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iSlimeMeetFlorentina", "N", 0.0)
	
	-- |[Dialogue]|
	--Setup.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])

	--Talking.
	fnCutscene([[ Append("Florentina: Mei?[P] Meeeiii![P] Where are you, you damn idiot!") ]])
	fnCutsceneBlocker()
	
	-- |[Movement]|
	--Move Mei over to Florentina.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Move To", 15.25 * gciSizePerTile, 9.5 * gciSizePerTile)
	DL_PopActiveObject()
	fnCutsceneWait(2)
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	
	--Florentina turns to see Mei.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Face", 1.0, 0.0)
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Face", -1.0, 0.0)
	DL_PopActiveObject()
	fnCutsceneWait(15)
	fnCutsceneBlocker()
	
	-- |[Dialogue]|
	--Setup.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])

	--Talking.
	fnCutscene([[ Append("Florentina:[E|Confused] Are you *freaking* kidding me?[P] This is the last thing I need![B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Err, is this a bad time?[B][C]") ]])
	
	--Next bit of dialogue varies based on whether or not Florentina knows about Mei's runestone's powers.
	local iFlorentinaKnowsAboutRune = VM_GetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsAboutRune", "N")
	if(iFlorentinaKnowsAboutRune == 0.0) then
		VM_SetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsAboutRune", "N", 1.0)
		
		--Florentina knows about the smarty-slimes:
		if(iFlorentinaSawSmartSlimes == 1.0) then
			fnCutscene([[ Append("Florentina:[E|Neutral] You -[P] you can talk like those other slimes?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] I guess so.[P] My runestone did this.[P] It just started glowing, and now I'm smart again.[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Happy] Oh really?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Offended] Your smile is starting to unnerve me.[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Happy] The gears in my head are turning, is all.[P] This opens up so many options...[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Offended] Be specific.[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Happy] Slimes can slide under doors, you see.[P] No lock could stop you...[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Angry] For heaven's sake, Florentina![B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Neutral] You're the one who asked.[P] Blame yourself.[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Neutral] I assume you still want to find a way home, even now?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Yes, of course.[P] My runestone -[P] it will let me change back if I so choose.[P] I'm sure of it.[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Neutral] The surprises just keep coming, don't they?[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Happpy] Then let's not dally.[P] If the amazements keep coming, maybe the next one will finally get me rich.[P] Onward![B][C]") ]])
		
		--Florentina doesn't know about smarty-slimes:
		else
			fnCutscene([[ Append("Florentina:[E|Surprise] By the four holies, you can talk?[P] How!?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Search me.[P] My runestone just started glowing...[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Happy] Listen, Mei.[P] You may not realize this, but you're literally the only talking slime in...[P] ever.[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Happy] Do you have any idea what this means?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Happy] We can keep looking for a way back to Earth?[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Confused] What?[P] No![P][E|Happy] People will pay big to see you![P] Fortune and fame await![B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Offended] Florentina!![P] I'm not a circus freak show![B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Neutral] See this is why I'm never going to get rich...[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Neutral] So if you're a slime, does that mean you can change form at will?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Happy] I suppose so.[P] I quite like how I look right now...[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Neutral] Can you do something about that stupid look on your face?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Very funny...[P] Shall we go, then?[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Neutral] Yeah, yeah.[P] I'm fine, by the way.") ]])
		end
		fnCutsceneBlocker()
	
	else
		fnCutscene([[ Append("Florentina:[E|Neutral] At least you're in one piece...[P] sort of...[P] Are you okay?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Your concern is touching.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] Just keeping my good eye on my investment.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Happy] Naturally.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] I'm guessing this is the result of your little runestone, right?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Yep.[P] It let out a glow...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] I thought I saw something in the thicket.[P] That thing gets more intriguing by the minute...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Shall we be off, then?[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Blush] Hold on a moment...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] What are you looking at?[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Blush] If I don't miss my guess, you got [P]*bigger*[P] didn't you?[B][C]") ]])
		fnCutscene([[ Append("Mei[E|Blush]: Well, I can look however I want.[P] Do you like what you see?[P] Hehe...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] Can you do anything about the dumb look on your face?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] *Sigh*") ]])
		fnCutsceneBlocker()
	end
	
	-- |[Movement]|
	--Wait a moment.
	fnCutsceneWait(15)
	fnCutsceneBlocker()
	
	--Move Florentina onto Mei.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Move To", 15.25 * gciSizePerTile, 9.5 * gciSizePerTile)
	DL_PopActiveObject()
	fnCutsceneWait(2)
	fnCutsceneBlocker()
	
	-- |[Florentina Rejoins]|
	--Compose the execution string.
	local sString = [[fnAddPartyMember("Florentina")]]

	--Execute
	fnCutscene(sString)
	fnCutsceneBlocker()

	--Fold the party positions up.
	fnCutscene([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()
end
