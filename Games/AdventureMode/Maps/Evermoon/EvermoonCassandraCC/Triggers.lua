-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = LM_GetScriptArgument(1, "N")

-- |[ ======================================== Triggers ======================================== ]|
--Werecat cutscene.
if(sObjectName == "WerecatScene") then

	--Variables.
	local sCassandraLocation     = VM_GetVar("Root/Variables/Chapter1/Scenes/sCassandraLocation", "S")
	local iStartedCassandraEvent = VM_GetVar("Root/Variables/Chapter1/Scenes/iStartedCassandraEvent", "N")
	local iCassandraNotHereCC    = VM_GetVar("Root/Variables/Chapter1/Scenes/iCassandraNotHereCC", "N")
	
	--If the Cassandra event is active, and the event rolled this location, run the cutscene:
	if(iStartedCassandraEvent == 1.0 and sCassandraLocation == "EvermoonCassandraCC") then
		LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/EvermoonCassandraScenes/Scene_RescueCassandra.lua")
	
	--Mei guesses she's not here.
	elseif(iStartedCassandraEvent == 1.0 and sCassandraLocation ~= "EvermoonCassandraCC" and iCassandraNotHereCC == 0.0) then
		VM_SetVar("Root/Variables/Chapter1/Scenes/iCassandraNotHereCC", "N", 1.0)
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Mei:[VOICE|Mei] (A dead end.[P] Better keep looking...)") ]])
		fnCutsceneBlocker()
		
	end
end
