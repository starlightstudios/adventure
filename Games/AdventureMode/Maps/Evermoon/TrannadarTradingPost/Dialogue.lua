-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    --Common.
	TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[Denise]|
    if(sActorName == "Denise") then
        LM_ExecuteScript(fnResolvePath() .. "Dialogue Denise.lua", "Hello")
    
    -- |[Trainer]|
    elseif(sActorName == "Kona") then
        LM_ExecuteScript(fnResolvePath() .. "Dialogue Kona.lua", "Hello")
    
    -- |[Amulet Vendor]|
    elseif(sActorName == "AmuletVendor") then
	
        --Variables.
        local iHasAnnoyedVendor = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasAnnoyedVendor", "N")
        
        --First case:
        if(iHasAnnoyedVendor == 0.0) then
            VM_SetVar("Root/Variables/Chapter1/Scenes/iHasAnnoyedVendor", "N", 1.0)
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Lady:[VOICE|MercF] Well, hello there.[P] You look like a discerning individual.[B][C]") ]])
            fnCutscene([[ Append("Mei:[VOICE|Mei] What do you have for sale?[B][C]") ]])
            fnCutscene([[ Append("Lady:[VOICE|MercF] Me?[P] Oh, a bit of this, a bit of that.[P] But, just for you, I have something very special.[B][C]") ]])
            fnCutscene([[ Append("Mei:[VOICE|Mei] (Oh joy.[P] A ridiculously obvious scam.)[B][C]") ]])
            fnCutscene([[ Append("Mei:[VOICE|Mei] What is it?[B][C]") ]])
            fnCutscene([[ Append("Lady:[VOICE|MercF] This is an Amulet of Annihilation.[P] A very rare antique, normally worth millions of platina.[B][C]") ]])
            fnCutscene([[ Append("Lady:[VOICE|MercF] Since I like the look of you, it's yours for just ten thousand platina.[B][C]") ]])
            fnCutscene([[ Append("Mei:[VOICE|Mei] Oh, only ten thousand?[P] If I paid less than a hundred thousand I'd assume it's defective.[B][C]") ]])
            fnCutscene([[ Append("Lady:[VOICE|MercF] D-[P]defective?[B][C]") ]])
            fnCutscene([[ Append("Mei:[VOICE|Mei] Yeah.[P] If it's broken then I don't want it.[B][C]") ]])
            fnCutscene([[ Append("Lady:[VOICE|MercF] You really want this piece of c[P]r[P]a[P][P]ss jewelry?[B][C]") ]])
            fnCutscene([[ Append("Mei:[VOICE|Mei] Maybe.[P] I'd need to take it for a test run first, to make sure it's in working order.[B][C]") ]])
            fnCutscene([[ Append("Lady:[VOICE|MercF] Uh, I'm not sure I can do that.[B][C]") ]])
            fnCutscene([[ Append("Mei:[VOICE|Mei] Aren't you a reputable Amulet of Annihilation vendor?[P] Where I'm from, we have a registry for that.[P] What's your serial number?[B][C]") ]])
            fnCutscene([[ Append("Lady:[VOICE|MercF] Wait, sorry, I think I'm all sold out.[P] Yep, this one is reserved...[B][C]") ]])
            fnCutscene([[ Append("Mei:[VOICE|Mei] Oh, shame.") ]])
            fnCutsceneBlocker()
        
        --Repeat case:
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Mei:[VOICE|Mei] Got any spare amulets yet?[P] They're a steal at just ten thousand.[B][C]") ]])
            fnCutscene([[ Append("Lady:[VOICE|MercF] I -[P] I don't know what you're talking about.[B][C]") ]])
            fnCutscene([[ Append("Mei:[VOICE|Mei] (Hehe.)") ]])
            fnCutsceneBlocker()
        
        end
    
    -- |[Cassandra]|
    elseif(sActorName == "Cassandra") then
    
        --Flag.
        local iCassandraSpokenTo = VM_GetVar("Root/Variables/Chapter1/Scenes/iCassandraSpokenTo", "N")
        
        --First time speaking to Cassandra:
        if(iCassandraSpokenTo == 0.0) then
            
            --Flag.
            VM_SetVar("Root/Variables/Chapter1/Scenes/iCassandraSpokenTo", "N", 1.0)
        
            --Speak.
            fnStandardMajorDialogue()
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
            fnCutscene([[ Append("Cassandra: Mei![P] Good to see you![B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Happy] Likewise![P] Has your memory recovered?[B][C]") ]])
            fnCutscene([[ Append("Cassandra: Ugh, not at all.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] Maybe if you retraced your steps.[P] What can you remember?[B][C]") ]])
            fnCutscene([[ Append("Cassandra: I was at my home in Jeffespeir, reading a book on astronomy.[P] Then, I heard someone knock on my door.[P] Then, a blur -[P] and then I was in the forest.[B][C]") ]])
            fnCutscene([[ Append("Cassandra: My clothes are completely different and I don't know how I got here, but I was looking for someone very important.[P] I think.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] Who were you looking for?[B][C]") ]])
            fnCutscene([[ Append("Cassandra: That, I don't know.[P] I do remember it was extremely important that I find them.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] Hmmm...[B][C]") ]])
            fnCutscene([[ Append("Cassandra: I'm sure I'll figure it out in time.[P] This trading post is pretty busy.[P] Whoever I was looking for will probably recognize me when they come through here.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] Well, I wish you well.[P] Hopefully you find whoever you were looking for.[B][C]") ]])
            fnCutscene([[ Append("Cassandra: Thanks, Mei.[P] I hope you find what you were looking for as well.") ]])
        
        --Repeats.
        else
            fnStandardMajorDialogue()
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
            fnCutscene([[ Append("Cassandra: Hmmm...[P] I'm pretty sure I was looking for a white-haired woman, but that doesn't describe anyone here...") ]])
        
        end
    
    -- |[Fortune Teller]|
    elseif(sActorName == "Fortune") then

    -- |[Trainee A]|
    elseif(sActorName == "TraineeA") then
        WD_SetProperty("Show")
        Append("Merc:[VOICE|MercM] It's not enough to do cardio, you need to hate your body![P] Hate your cardiovascular system![P] And especially hate calories in all their forms!")

    -- |[Trainee B]|
    elseif(sActorName == "TraineeB") then
        WD_SetProperty("Show")
        Append("Merc:[VOICE|MercF] A few more reps and I can die happy!")

    -- |[Trainee C]|
    elseif(sActorName == "TraineeC") then
        WD_SetProperty("Show")
        Append("Merc:[VOICE|MercF] I'm sweating![P] Guh![P] Feel it!")
    
    -- |[Generic NPC A]|
    elseif(sActorName == "NPC A") then
        WD_SetProperty("Show")
        Append("Merc:[VOICE|MercF] Trannadar is a nice place.[P] I've been to the city, it smells awful there.")
    
    -- |[Generic NPC B]|
    elseif(sActorName == "NPC B") then
        WD_SetProperty("Show")
        Append("Trader:[VOICE|MercF] I'm waiting for the caravan to get going.[P] I heard there was a rockslide south of here, not looking forward to crossing that.")
    
    -- |[Generic NPC C]|
    elseif(sActorName == "NPC C") then
        WD_SetProperty("Show")
        Append("Trader:[VOICE|MercM] Hey buddy, want to buy a hit of Pixie Powder?[B][C]")
        Append("Trader:[VOICE|MercM] Don't give me that look, I'm a legitimate businessman!")
    
    -- |[Generic NPC D]|
    elseif(sActorName == "NPC D") then
        WD_SetProperty("Show")
        Append("Merc:[VOICE|MercF] Florentina's a real card shark.[P] I lost my shirt to that poker face![B][C]")
        Append("Merc:[VOICE|MercF] Luckily, I got it back on an installment plan.[P] Better not miss a payment...")
    
    -- |[Generic NPC E]|
    elseif(sActorName == "NPC E") then
        WD_SetProperty("Show")
        Append("Merc:[VOICE|MercM] Damn it, I lost my contact lens...")
    
    -- |[Generic NPC F]|
    elseif(sActorName == "NPC F") then
        WD_SetProperty("Show")
        Append("Trader:[VOICE|MercF] There's a much shorter route to Drimhaven to the west, but it goes through the Starfield Swamps.[B][C]")
        Append("Trader:[VOICE|MercF] The ground is awful for carts and pack-animals, so we usually go south through Trafal.[P] But the pass is blocked...")
    
    -- |[Generic NPC G]|
    elseif(sActorName == "NPC G") then
        
        --Variables.
        local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
        
        --Dialogue for non-Alraune:
        if(sMeiForm ~= "Alraune") then
            WD_SetProperty("Show")
            Append("Alraune:[VOICE|Alraune] Don't mind me, I mean you no harm.[P] I'm just tending to the trees around here.[B][C]")
            Append("Alraune:[VOICE|Alraune] The humans at this trading post use them for firewood.[P] I try to make sure they trim off dead branches and not live ones.")
        
        --Dialogue for Alraune:
        else
            WD_SetProperty("Show")
            Append("Alraune:[VOICE|Alraune] Hello, leaf-sister.[P] I'm making sure the humans only use dead branches for firewood.[B][C]")
            Append("Alraune:[VOICE|Alraune] Honestly, it can be so frustrating to deal with them sometimes...")
        end
    
    -- |[Generic NPC H]|
    elseif(sActorName == "NPC HA" or sActorName == "NPC HB") then
        WD_SetProperty("Show")
        Append("Merc:[VOICE|MercM] Welcome to Trannadar Trading Post.[P] I get paid to say that![P] This job is the best!")
    
    -- |[Generic NPC I]|
    elseif(sActorName == "NPC I") then
        WD_SetProperty("Show")
        Append("Lady:[VOICE|MercF] Hey, you're not here to pick up this statue, are you?[B][C]")
        Append("Lady:[VOICE|MercF] I'm supposed to deliver it but I'm a few days early.[P] Nobody has come to pick it up.[B][C]")
        Append("Lady:[VOICE|MercF] Who would commission a statue of a corgi, though?")

    -- |[Hypatia]|
    elseif(sActorName == "Hypatia") then
	
        --This is always a major dialogue.
        fnStandardMajorDialogue()
        
        --Hypatia takes slot 5.
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Hypatia", "Neutral") ]])
        
        --Variables.
        local iHasSeenHypatiaDiscountDialogue = VM_GetVar("Root/Variables/Chapter1/Hypatia/iHasSeenHypatiaDiscountDialogue", "N")
        if(iHasSeenHypatiaDiscountDialogue == 0.0) then
            VM_SetVar("Root/Variables/Chapter1/Hypatia/iHasSeenHypatiaDiscountDialogue", "N", 1.0)
            fnCutscene([[ Append("Hypatia: Hey boss, need some supplies?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] Do we get a discount?[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] No.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Offended] What?[P] But what if we need something important?[B][C]") ]])
            fnCutscene([[ Append("Florentina: Then you'll have no problem stumping up the cash.[P] I'm not in this for the charity.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Offended] *growl*[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Happy] How can I afford to pay Hypatia here a lavish salary if I'm giving goods away for free?[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] You're taking food out of her mouth here.[B][C]") ]])
            fnCutscene([[ Append("Hypatia: Yeah![P] I may not get paid much but it's more than nothing![B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] See?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] Fine then.") ]])
        
        else
            fnCutscene([[ Append("Hypatia: What can I get for you?") ]])
        end
        fnCutsceneBlocker()

        --Run the shop.
        local sBasePath = fnResolvePath()
        local sString = "AM_SetShopProperty(\"Show\", \"Trannadar Equipment\", \"" .. sBasePath .. "General Goods.lua\", \"Null\")"
        fnCutscene(sString)
        fnCutsceneBlocker()

    -- |[Equipment Vendor]|
    elseif(sActorName == "Equipment Vendor") then
        local sBasePath = fnResolvePath()
        local sString = "AM_SetShopProperty(\"Show\", \"Trannadar Equipment\", \"" .. sBasePath .. "Equipment Vendor.lua\", \"Null\")"
        fnCutscene(sString)
        fnCutsceneBlocker()

    -- |[Nadia]|
    elseif(sActorName == "Nadia") then
        fnStandardMajorDialogue()
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Nadia", "Neutral") ]])
        local iRoll = LM_GetRandomNumber(0, 100)
        if(iRoll < 25) then
            fnCutscene([[ Append("Nadia: Mei![P] Good to see ya, what's up?[B][C]") ]])
        elseif(iRoll < 50) then
            fnCutscene([[ Append("Nadia: Nice to see ya, Mei.[B][C]") ]])
        else
            fnCutscene([[ Append("Nadia: What can I do for ya?[B][C]") ]])
        end

        --Activate topics mode once the dialogue is complete.
        fnCutscene([[ WD_SetProperty("Activate Topics After Dialogue", "Nadia")  ]])

    -- |[Blythe]|
    elseif(sActorName == "Blythe") then
        WD_SetProperty("Unlock Topic", "Challenge", 1)
        fnStandardMajorDialogue()
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Blythe", "Neutral") ]])
        fnCutscene([[ Append("Blythe: May I help you with something?[B][C]") ]])
        fnCutscene([[ WD_SetProperty("Activate Topics After Dialogue", "Blythe") ]])
        
    -- |[Goat]|
    elseif(sActorName == "Goat") then
        --Play a goat sound.
        local iRandomNumber = LM_GetRandomNumber(0, 3)
        AudioManager_PlaySound("World|Goat" .. iRandomNumber)
        
        --Increment the bother counter.
        local iGoatBotherCount = VM_GetVar("Root/Variables/Global/Goat/iGoatBotherCount", "N")
        VM_SetVar("Root/Variables/Global/Goat/iGoatBotherCount", "N", iGoatBotherCount + 1.0)
        local iGoatBotherCountTotal = VM_GetVar("Root/Variables/Global/Goat/iGoatBotherCountTotal", "N")
        VM_SetVar("Root/Variables/Global/Goat/iGoatBotherCountTotal", "N", iGoatBotherCountTotal + 1.0)
    end
end
