-- |[Constructor]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = fnResolveDirectory() --Map's name is the name of the folder it is in.

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[Standard]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
	AL_SetProperty("Music", "TownTheme")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", "TrannadarTradingPost")
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/TrannadarTradingPost/")

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end
    
    -- |[Map Handling]|
    --Resolve the map's variable.
    fnStandardCh1Map(sLevelName)
    
    --Trading post never autosaves.
    AL_SetProperty("Block Autosave Once")

-- |[Post-Exec]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

    -- |[Approach Direction]|
	--Determine which side the player entered from.
	local bIsPlayerAtBottom = false
    local bIsPlayerAtTop = false
	EM_PushEntity("Adventure Party", 0)
		local iPlayerX, iPlayerY = TA_GetProperty("Position")
	DL_PopActiveObject()
	if(iPlayerY >= 37 * 16) then bIsPlayerAtBottom = true end
	if(iPlayerY <   7 * 16) then bIsPlayerAtTop = true end
	
    -- |[Variables]|
	--Variables for cutscenes:
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	local iHasSeenTrannadarFirstScene  = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFirstScene", "N")
	local iHasSeenTrannadarSecondScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarSecondScene", "N")
	local iHasSeenTrannadarThirdScene  = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarThirdScene", "N")
	local sTrannadarFirstSceneForm     = VM_GetVar("Root/Variables/Chapter1/Scenes/sTrannadarFirstSceneForm", "S")
	local sTrannadarSecondSceneForm    = VM_GetVar("Root/Variables/Chapter1/Scenes/sTrannadarSecondSceneForm", "S")
	
    -- |[Trap Dungeon Cutscene]|
	--If the player meets certain conditions, Nadia tells them about the trap dungeon.
	local iInformedOfDungeon   = VM_GetVar("Root/Variables/Chapter1/Scenes/iInformedOfDungeon", "N")
	local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
    local iIsRelivingScene     = VM_GetVar("Root/Variables/Chapter1/Scenes/iIsRelivingScene", "N")
	AdvCombat_SetProperty("Push Party Member", "Mei")
		local iMeiLevel = AdvCombatEntity_GetProperty("Level")
	DL_PopActiveObject()
	
	--In all cases, mark the combat intro dialogues as complete.
	VM_SetVar("Root/Variables/Chapter1/Counts/sMeiSeenPartirhuman", "S", "Complete")
	
    -- |[Nadia]|
	--Nadia always spawns on the side the player is on.
    fnStandardNPCByPosition("Nadia")
	EM_PushEntity("Nadia")
		if(bIsPlayerAtBottom) then
			TA_SetProperty("Position", 30, 37)
			TA_SetProperty("Facing", gci_Face_South)
        elseif(bIsPlayerAtTop) then
			TA_SetProperty("Position", 21, 7)
			TA_SetProperty("Facing", gci_Face_North)
		else
			TA_SetProperty("Position", 41, 26)
			TA_SetProperty("Facing", gci_Face_East)
		end
	DL_PopActiveObject()
    
    -- |[Blythe]|
	--Blythe spawns in his office 50% of the time, and 50% of the time will be near the watering hole.
    fnStandardNPCByPosition("Blythe")
	local iRoll = LM_GetRandomNumber(0, 99)
	TA_Create("Blythe")
		if(iRoll < 50) then
			TA_SetProperty("Position", 14, 27 - 0.10)
			TA_SetProperty("Facing", gci_Face_East)
			TA_SetProperty("Extended Activation Direction", gci_Face_East)
		else
			TA_SetProperty("Position", 20, 11)
			TA_SetProperty("Facing", gci_Face_South)
		end
	DL_PopActiveObject()
	
    -- |[Florentina]|
	--If Florentina is not in the party, she's in the back room of her shop.
	local bIsFlorentinaInParty = AL_GetProperty("Is Character Following", "Florentina")
	if(bIsFlorentinaInParty == false) then
		fnSpecialCharacter("Florentina", 32, 15, gci_Face_North, false, nil)
	end
	
    -- |[Goat]|
	--Has special handling.
    fnStandardNPCByPosition("Goat")
	EM_PushEntity("Goat")
		fnSetCharacterGraphics("Goat", false)
	DL_PopActiveObject()
	
    -- |[Cassandra]|
	--Spawns here if the party saved her.
	local iStartedCassandraEvent = VM_GetVar("Root/Variables/Chapter1/Scenes/iStartedCassandraEvent", "N")
	local iSavedCassandra = VM_GetVar("Root/Variables/Chapter1/Scenes/iSavedCassandra", "N")
	if(iStartedCassandraEvent == 2.0 and iSavedCassandra == 1.0) then
        fnStandardNPCByPosition("Cassandra")
	end
	
	-- |[Unconditional NPCs]|
    fnStandardNPCByPosition("AmuletVendor")
    fnStandardNPCByPosition("Equipment Vendor")
    fnStandardNPCByPosition("Denise")
    fnStandardNPCByPosition("Kona")
    fnStandardNPCByPosition("TraineeA")
    fnStandardNPCByPosition("TraineeB")
    fnStandardNPCByPosition("TraineeC")
    fnStandardNPCByPosition("Hypatia")
    fnStandardNPCByPosition("NPC A")
    fnStandardNPCByPosition("NPC B")
    fnStandardNPCByPosition("NPC C")
    fnStandardNPCByPosition("NPC D")
    fnStandardNPCByPosition("NPC E")
    fnStandardNPCByPosition("NPC F")
    fnStandardNPCByPosition("NPC G")
	if(bIsPlayerAtBottom) then
        fnStandardNPCByPosition("NPC HA")
	else
        fnStandardNPCByPosition("NPC HB")
	end
    fnStandardNPCByPosition("NPC I")
    
    -- |[Parallel Scripts]|
    local sBasePath = fnResolvePath()
    fnCreateStandardPathScript("Trainer Grouping", sBasePath .. "PathScript_Trainer.lua", gcbHideParallelTiming, 0, 290-1)
    fnCreateStandardPathScript("NPC A",            sBasePath .. "PathScript_NPCA.lua",    gcbHideParallelTiming, gciParallelScatterStdLo, gciParallelScatterStdHi)
    fnCreateStandardPathScript("NPC D",            sBasePath .. "PathScript_NPCD.lua",    gcbHideParallelTiming, gciParallelScatterStdLo, gciParallelScatterStdHi)
    fnCreateStandardPathScript("NPC E",            sBasePath .. "PathScript_NPCE.lua",    gcbHideParallelTiming, gciParallelScatterStdLo, gciParallelScatterStdHi)
    fnCreateStandardPathScript("NPC G",            sBasePath .. "PathScript_NPCG.lua",    gcbHideParallelTiming, gciParallelScatterStdLo, gciParallelScatterStdHi)
    
    -- |[First Entrance Cutscene]|
	--First entrance cutscene:
	if(iHasSeenTrannadarFirstScene == 0.0 and gbBypassTrannadarCutscene == false) then
	
		--Change the music.
		AL_SetProperty("Music", "NadiasTheme")
	
		--Flag.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFirstScene", "N", 1.0)
		VM_SetVar("Root/Variables/Chapter1/Scenes/sTrannadarFirstSceneForm", "S", sMeiForm)
	
		--Blythe will reposition right next to Nadia during this scene.
		EM_PushEntity("Blythe")
			if(bIsPlayerAtBottom) then
				TA_SetProperty("Position", 29, 37)
				TA_SetProperty("Facing", gci_Face_East)
            elseif(bIsPlayerAtTop) then
				TA_SetProperty("Position", 22, 7)
				TA_SetProperty("Facing", gci_Face_West)
			else
				TA_SetProperty("Position", 41, 27)
				TA_SetProperty("Facing", gci_Face_North)
			end
		DL_PopActiveObject()
		
		--Alraune
		if(sMeiForm == "Alraune") then
			LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/Trannadar_FirstSceneAlraune/Scene_Begin.lua")
		
		--Bee
		elseif(sMeiForm == "Bee") then
			LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/Trannadar_FirstSceneBee/Scene_Begin.lua")
		
		--Slime
		elseif(sMeiForm == "Slime") then
			LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/Trannadar_FirstSceneSlime/Scene_Begin.lua")
		
		--Ghost
		elseif(sMeiForm == "Ghost") then
			LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/Trannadar_FirstSceneGhost/Scene_Begin.lua")
		
		--Werecat
		elseif(sMeiForm == "Werecat") then
			LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/Trannadar_FirstSceneWerecat/Scene_Begin.lua")
        
        --Gravemarker.
        elseif(sMeiForm == "Gravemarker") then
			LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/Trannadar_FirstSceneGravemarker/Scene_Begin.lua")
        
        --Wisphag.
        elseif(sMeiForm == "Wisphag") then
			LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/Trannadar_FirstSceneWisphag/Scene_Begin.lua")
        
        --Mannequin.
        elseif(sMeiForm == "Mannequin") then
			LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/Trannadar_FirstSceneMannequin/Scene_Begin.lua")
        
		--Catch-all case defaults to human.
		else
			LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/Trannadar_FirstSceneHuman/Scene_Begin.lua")
		end
	
    -- |[Second Entrance Cutscene]|
	--Cutscene where species is different than first entrance species, and the first species was human.
	elseif(sTrannadarFirstSceneForm == "Human" and sMeiForm ~= "Human" and iHasSeenTrannadarSecondScene == 0.0 and iHasSeenTrannadarThirdScene == 0.0 and gbBypassTrannadarCutscene == false) then
	
		--Change the music.
		AL_SetProperty("Music", "NadiasTheme")
	
		--Flag.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarSecondScene", "N", 1.0)
		VM_SetVar("Root/Variables/Chapter1/Scenes/sTrannadarSecondSceneForm", "S", sMeiForm)
		
		--Alraune
		if(sMeiForm == "Alraune") then
			LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/Trannadar_SecondSceneAlraune/Scene_Begin.lua")
		
		--Bee
		elseif(sMeiForm == "Bee") then
			LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/Trannadar_SecondSceneBee/Scene_Begin.lua")
		
		--Slime
		elseif(sMeiForm == "Slime") then
			LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/Trannadar_SecondSceneSlime/Scene_Begin.lua")
		
		--Ghost.
		elseif(sMeiForm == "Ghost") then
			LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/Trannadar_SecondSceneGhost/Scene_Begin.lua")
		
		--Werecat.
		elseif(sMeiForm == "Werecat") then
			LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/Trannadar_SecondSceneWerecat/Scene_Begin.lua")
		
		--Gravemarker.
		elseif(sMeiForm == "Gravemarker") then
			LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/Trannadar_SecondSceneGravemarker/Scene_Begin.lua")
        
        --Wisphag.
        elseif(sMeiForm == "Wisphag") then
			LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/Trannadar_SecondSceneWisphag/Scene_Begin.lua")
        
        --Mannequin.
        elseif(sMeiForm == "Mannequin") then
			LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/Trannadar_SecondSceneMannequin/Scene_Begin.lua")
		end

	--Cutscene where species is different than the first entrance species, and the first species was not human.
	elseif(sTrannadarFirstSceneForm ~= "Human" and sMeiForm ~= sTrannadarFirstSceneForm and iHasSeenTrannadarSecondScene == 0.0 and iHasSeenTrannadarThirdScene == 0.0 and gbBypassTrannadarCutscene == false) then
	
		--Change the music.
		AL_SetProperty("Music", "NadiasTheme")
	
		--Flag.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarSecondScene", "N", 1.0)
		VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarThirdScene", "N", 1.0)
		
		--Execute.
		LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/Trannadar_ThirdScene/Scene_Begin.lua")

	--Cutscene where species is different from the second scene species.
	elseif(sMeiForm ~= sTrannadarSecondSceneForm and iHasSeenTrannadarSecondScene == 1.0 and iHasSeenTrannadarThirdScene == 0.0 and gbBypassTrannadarCutscene == false) then
	
		--Change the music.
		AL_SetProperty("Music", "NadiasTheme")
	
		--Flag.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarThirdScene", "N", 1.0)
		
		--Execute.
		LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/Trannadar_ThirdScene/Scene_Begin.lua")

    -- |[Trap Dungeon Notice]|
	--Nadia informs Mei of the Trap Dungeon events.
	elseif(iInformedOfDungeon == 0.0 and bIsFlorentinaPresent == true and iMeiLevel >= 12 and iIsRelivingScene == 0.0) then
		
		--Right-side spawn:
		if(bIsPlayerAtBottom == false and bIsPlayerAtTop == false) then
			fnCutsceneMove("Mei", 43.25, 28.50)
			fnCutsceneMove("Florentina", 44.25, 28.50)
			fnCutsceneBlocker()

			--Wait a bit.
			fnCutsceneWait(25)
			fnCutsceneBlocker()

			--Nadia faces right.
			fnCutsceneFace("Nadia", 1, 0)
			fnCutsceneWait(25)
			fnCutsceneBlocker()
        
        --Top spawn.
		elseif(bIsPlayerAtTop == true) then
			fnCutsceneMove("Mei", 22.25, 6.50)
			fnCutsceneMove("Florentina", 23.25, 6.50)
			fnCutsceneBlocker()
		
		--Bottom spawn:
		else
			fnCutsceneMove("Mei", 28.25, 39.50)
			fnCutsceneMove("Florentina", 29.25, 39.50)
			fnCutsceneBlocker()

			--Wait a bit.
			fnCutsceneWait(25)
			fnCutsceneBlocker()

			--Nadia faces north.
			fnCutsceneFace("Nadia", 0, 1)
			fnCutsceneWait(25)
			fnCutsceneBlocker()
		
		end
		
		--Run the common dialogue:
		LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/Nadia_InformOfDungeon/Scene_Begin.lua")
			
		--Right-side spawn:
		if(bIsPlayerAtBottom == false and bIsPlayerAtTop == false) then
			fnCutsceneMove("Florentina", 43.25, 28.50)
			fnCutsceneBlocker()
        
        --Top.
		elseif(bIsPlayerAtTop == true) then
			fnCutsceneMove("Florentina", 22.25, 6.50)
			fnCutsceneBlocker()
		
		--Bottom spawn:
		else
			fnCutsceneMove("Florentina", 28.25, 39.50)
			fnCutsceneBlocker()
		end
		
		--Fold the party.
		fnCutscene([[ AL_SetProperty("Fold Party") ]])
		fnCutsceneBlocker()
			
	--Normal, no cutscenes:
	else
	
	end
	
	--Overlays.
    fnLightForest()
    
    -- |[Skillbook]|
    local iSkillbook4 = VM_GetVar("Root/Variables/Global/Mei/iSkillbook4", "N")
    if(iSkillbook4 == 1.0) then
		AL_SetProperty("Set Layer Disabled", "SkillbookIco", true)
    end
end
