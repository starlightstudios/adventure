-- |[ ============================= Trannadar Equipment Shop Setup ============================= ]|
--Sells some basic equipment.

-- |[Item Stock Listing]|
--Adamantite
AM_SetShopProperty("Add Item", "Adamantite Powder",  -1, -1)
AM_SetShopProperty("Add Item", "Bronze Katana",      -1, -1)
AM_SetShopProperty("Add Item", "Troubadour's Robe",  -1, -1)
AM_SetShopProperty("Add Item", "Light Leather Vest", -1, -1)
AM_SetShopProperty("Add Item", "Healing Tincture",   -1, -1)
AM_SetShopProperty("Add Item", "Palliative",         -1, -1)
AM_SetShopProperty("Add Item", "Smelling Salts",     -1, -1)
AM_SetShopProperty("Add Item", "Glintsteel Gem",     -1, -1)
AM_SetShopProperty("Add Item", "Ardrion Gem",        -1, -1)
