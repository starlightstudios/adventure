-- |[ ========================================= NPC A ========================================== ]|
--NPCA, who is inspecting the garden.

-- |[ ========================== Setup =========================== ]|
-- |[Variables]|
local sEntityName = "NPC D"
local fWalkSpeed = 0.50
local saNodeList = {"B0", "B1", "B2", "B3"}

-- |[Existence Check]|
--If the NPC(s) don't exist, delete this script.
if(EM_Exists(sEntityName) == false) then
    io.write("Warning: Parallel cutscene deleted due to missing NPC. Internal Name: " .. Cutscene_GetProperty("Parallel Script Name") .. "\n")
    Cutscene_HandleParallel("DELETETHIS")
    return
end

-- |[ ====================== Cutscene Work ======================= ]|
-- |[Run Routine]|
fnNPCPathToRandomNode(sEntityName, fWalkSpeed, saNodeList)
