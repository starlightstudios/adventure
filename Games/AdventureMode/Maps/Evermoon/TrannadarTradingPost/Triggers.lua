-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
--Activates Florentina's cutscene if it hasn't been seen yet.
if(sObjectName == "FlorentinaSceneTrigger") then
	
	--Variables.
	local iHasSeenTrannadarFlorentinaScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFlorentinaScene", "N")
	
	--If we haven't seen the scene yet, launch it.
	if(iHasSeenTrannadarFlorentinaScene == 0.0) then
		LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/Trannadar_Florentina/Scene_Begin.lua")
	end
	
--Disables the forest overlay.
elseif(sObjectName == "NoOverlay") then
    AL_SetProperty("Foreground Alpha", 0, 0.00, 15)
	
--Disables the forest overlay instantly.
elseif(sObjectName == "NoOverlay") then
    AL_SetProperty("Foreground Alpha", 0, 0.00, 0)
	
--Enables the forest overlay.
elseif(sObjectName == "YesOverlay") then
    AL_SetProperty("Foreground Alpha", 0, 0.50, 15)
end
