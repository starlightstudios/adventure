-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Examination]|
--Watering hole.
if(sObjectName == "WateringHole") then
	fnStandardDialogue([[[VOICE|Mei](Seems to be a watering hole for pack animals.[P] It looks surprisingly clean.) ]])

--Empty object.
elseif(sObjectName == "Empty") then
	fnStandardDialogue([[[VOICE|Mei](Nothing of interest.) ]])

--Garden. Nadia is taking care of these guys!
elseif(sObjectName == "Garden") then

	--Normal dialogue:
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	if(sMeiForm ~= "Alraune") then
		fnStandardDialogue([[[VOICE|Mei](Not a weed in sight.[P] Someone is taking very good care of these plants.) ]])
	else
		fnStandardDialogue([[[VOICE|Mei](Hm? Oh, the little ones said that Nadia is taking care of them![P] She's a real sweetheart.) ]])
	end

--Barrel near the equipment shop.
elseif(sObjectName == "EquipmentBarrel") then
	fnStandardDialogue([[[VOICE|Mei](Empty, but there are traces of chalk on the edges of the barrel.[P] Might this be used for fluxing stone?) ]])

--Bag near Florentina's shop.
elseif(sObjectName == "FlorentinaBag") then
	fnStandardDialogue([[[VOICE|Mei](This bag is full of fragrant flowers.) ]])
	
--Bag in the barracks.
elseif(sObjectName == "BarracksBagA") then
	fnStandardDialogue([[[VOICE|Mei](There are corn kernels in this bag.[P] I really hope that's not a terrible joke.) ]])
	
--Bag in the barracks.
elseif(sObjectName == "BarracksBagB") then
	fnStandardDialogue([[[VOICE|Mei](This bag holds a number of whetstones.) ]])
	
--Yeah, not stealing that.
elseif(sObjectName == "Rummage") then
	fnStandardDialogue([[[VOICE|Mei](Probably not a good idea to go rummaging through someone's stuff while they're two feet away from you.) ]])

--Blythe's garden.
elseif(sObjectName == "GardenBlythe") then

	--Normal dialogue:
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	if(sMeiForm ~= "Alraune") then
		fnStandardDialogue([[[VOICE|Mei](A garden.[P] I'm no expert, but I can see there is far more passion than skill going into it.) ]])
	else
		fnStandardDialogue([[[VOICE|Mei](Blythe takes care of you?[P] I see his technique with his clippers is poor.[P] Tell Nadia and she'll give him some pointers.)[B][C][VOICE|Mei] (...) [B][C][VOICE|Mei](You're welcome!) ]])
	end

--Books explaining combat.
elseif(sObjectName == "CombatBooks") then

	fnStandardDialogue()
	fnCutscene([[ Append("[VOICE|Mei]('The Combat Primer. A step-by-step guide'.[P] Looks like a book with combat tips!)[BLOCK]") ]])
	fnCutscene([[ WD_SetProperty("Activate Topics After Dialogue", "CombatBooks", "Leave") ]])

--Backer listing.
elseif(sObjectName == "Backers") then

	fnStandardMajorDialogue()
	fnCutscene([[ Append("Mei: Looks like a list of all the merchants who back the trading post financially.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Happy] We would never have made it this far if it weren't for all of you![P] Thank you so much![B][C]") ]])
	if(fnIsCharacterPresent("Florentina")) then
		fnCutscene([[ Append("Florentina:[E|Happy] Laying it on a little thick, eh?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] I don't know what you're talking about![B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] Oh trust me, I love it when you talk-up merchants.[P] Keep at it.[B][C]") ]])
	end
		
	fnCutscene([[ WD_SetProperty("Activate Topics After Dialogue", "Backers", "Leave") ]])
	
--Corgi!
elseif(sObjectName == "Corgi") then

	--Pie Job:
	local iPaperCount  = AdInv_GetProperty("Item Count", "Booped Paper")
	local iTakenPieJob = VM_GetVar("Root/Variables/Chapter1/Breanne/iTakenPieJob", "N")
    local iMetPolaris  = VM_GetVar("Root/Variables/Chapter1/Polaris/iMetPolaris", "N")
	if(iTakenPieJob == 1.0 and iPaperCount < 1) then
		
		--Short scene.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Okay, give me a piece of paper...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Happy] Boop![B][C]") ]])
        if(iMetPolaris == 0.0) then
            fnCutscene([[ Append("Florentina:[E|Neutral] You're a special kind of moron, you know that?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] We need paper booped against a dog's nose.[P] This is a dog.[P] Does this not count?[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Happy] Actually I think it does.[P] Alchemy is weird like that.[B][C]") ]])
        else
            fnCutscene([[ Append("Florentina:[E|Neutral] Why don't we just go boop Dot's nose?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] This is a perfect replica of Dot, so it counts![B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Happy] Not arguing, just figured you'd go see Dot first.[B][C]") ]])
        end
		fnCutscene([[ Append("Mei:[E|Happy] Boop![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Laugh] Boop![B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] Okay, stop enjoying it.[B][C]") ]])
		fnCutscene([[ Append("[SOUND|World|TakeItem]*Got Booped Paper*") ]])
		
		--Add the item.
		LM_ExecuteScript(gsItemListing, "Booped Paper")
		
		--Reset flag.
		WD_SetProperty("Clear Topic Read", "Pepper Pie")
		
	--Normal case:
	else
		fnStandardDialogue([[[VOICE|Mei](Inexplicably, this is a finely crafted statue of a corgi.[P] It's palpitation-inducingly cute!)]])
	end

--The mirror in Florentina's shop:
elseif(sObjectName == "Mirror") then

	--Variables:
	local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
	
	--Mei is alone:
	if(bIsFlorentinaPresent == false) then
		fnStandardDialogue([[[VOICE|Mei](A mirror.[P] It provides a soothing aura...)]])
	
	--Florentina is here:
	else
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] ...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] Interested in that mirror?[P] The price is - [P][CLEAR]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Hmm, no, that's all right.[P] I just feel better when I'm looking at it.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Smirk] Don't you find it soothing to look into the mirror and just stop worrying?[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] And I thought [P]*I*[P] was a narcissist.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] Oh very funny.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Smirk] I feel a lot better now.[P] Everything is going to be all right.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] So you're not buying the mirror?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Laugh] You never let up, do you?") ]])

	end

--Sign near the goat. This is the most needlessly complicated script I have ever written. If you are reading this, I am so sorry.
elseif(sObjectName == "GoatSign") then

	--Get variables.
	local iGoatBotherState = VM_GetVar("Root/Variables/Global/Goat/iGoatBotherState", "N")
	local iGoatBotherCount = math.floor(VM_GetVar("Root/Variables/Global/Goat/iGoatBotherCount", "N"))
	
	--In all cases, a dialogue appears.
	fnStandardDialogue()

	--Zeroth: It begins...
	if(iGoatBotherState == 0) then
		VM_SetVar("Root/Variables/Global/Goat/iGoatBotherState", "N", 1.0)
		VM_SetVar("Root/Variables/Global/Goat/iGoatBotherCount", "N", 0.0)
		fnCutscene([[ Append("(\"Please do not bother the goat 10 times.\")") ]])
	
	--Oneth: Bother to 10.
	elseif(iGoatBotherState == 1) then

		--Zero bothers:
		if(iGoatBotherCount == 0.0) then
			fnCutscene([[ Append("(\"Please do not bother the goat 10 times.\")") ]])
		
		--One-to-eight bothers:
		elseif(iGoatBotherCount >= 1.0 and iGoatBotherCount <= 8.0) then
			local sString = "[CLEAR](Please do not bother the goat " .. (10 - iGoatBotherCount) .. " more times.)"
			fnCutscene(" WD_SetProperty(\"Append\", \"" .. sString .. "\") ")
		
		--Nine bothers:
		elseif(iGoatBotherCount == 9.0) then
			fnCutscene([[ Append("(\"Please do not bother the goat 1 more time.\")") ]])
	
		--Enough bothers.
		else
			fnCutscene([[ Append("(\"Hey, really? Please stop bothering the goat. There is no need to bother him 10 more times.\")") ]])
			VM_SetVar("Root/Variables/Global/Goat/iGoatBotherState", "N", 2.0)
			VM_SetVar("Root/Variables/Global/Goat/iGoatBotherCount", "N", 0.0)
		end
	
	--Second: Bother to 10, again.
	elseif(iGoatBotherState == 2) then

		--Zero bothers:
		if(iGoatBotherCount == 0.0) then
			fnCutscene([[ Append("(\"There is no need to bother the goat 10 more times.\")") ]])
		
		--One-to-eight bothers:
		elseif(iGoatBotherCount >= 1.0 and iGoatBotherCount <= 8.0) then
			local sString = "[CLEAR](There is no need to bother the goat " .. (10 - iGoatBotherCount) .. " more times.)"
			fnCutscene(" WD_SetProperty(\"Append\", \"" .. sString .. "\") ")
		
		--Nine bothers:
		elseif(iGoatBotherCount == 9.0) then
			fnCutscene([[ Append("(\"There is no need to bother the goat 1 more time.\")") ]])
	
		--Enough bothers.
		else
			fnCutscene([[ Append("(\"Why are you still bothering that poor goat? Knock it off!\")") ]])
			VM_SetVar("Root/Variables/Global/Goat/iGoatBotherState", "N", 3.0)
			VM_SetVar("Root/Variables/Global/Goat/iGoatBotherCount", "N", 0.0)
		end
	
	--Third: Bother to 10, again. No counter this time.
	elseif(iGoatBotherState == 3) then

		--Less than ten bothers.
		if(iGoatBotherCount < 10.0) then
			fnCutscene([[ Append("(\"Why are you still bothering that poor goat? Knock it off!\")") ]])
		else
			fnCutscene([[ Append("(\"I see what's going on here. You think something will happen if you keep bothering the goat. Well it won't. Stop it!\")") ]])
			VM_SetVar("Root/Variables/Global/Goat/iGoatBotherState", "N", 4.0)
			VM_SetVar("Root/Variables/Global/Goat/iGoatBotherCount", "N", 0.0)
		end
	
	--Fourth: Hey, cut it out!
	elseif(iGoatBotherState == 4) then

		--Less than ten bothers.
		if(iGoatBotherCount < 10.0) then
			fnCutscene([[ Append("(\"I see what's going on here. You think something will happen if you keep bothering the goat. Well it won't. Stop it!\")") ]])
		else
			fnCutscene([[ Append("(\"Hey, tell you what. You stop bothering this goat, and I'll give you ten platina. Sound good?\")[B][C]") ]])
			fnCutscene([[ Append("(10 Platina appeared in your pocket!)[B][C]") ]])
			fnCutscene([[ Append("(\"Do we have a deal?\")") ]])
			VM_SetVar("Root/Variables/Global/Goat/iGoatBotherState", "N", 5.0)
			VM_SetVar("Root/Variables/Global/Goat/iGoatBotherCount", "N", 0.0)
			LM_ExecuteScript(gsItemListing, "Platina x10")
		end
		
	--Fifth: Oh come on!
	elseif(iGoatBotherState == 5) then

		--Less than ten bothers.
		if(iGoatBotherCount < 10.0) then
			fnCutscene([[ Append("(\"Do we have a deal?\")") ]])
		else
			fnCutscene([[ Append("(\"So you took the money but kept bothering the goat? What is wrong with you!? Please! Stop bothering the goat!\")") ]])
			VM_SetVar("Root/Variables/Global/Goat/iGoatBotherState", "N", 6.0)
			VM_SetVar("Root/Variables/Global/Goat/iGoatBotherCount", "N", 0.0)
		end
		
	--Sixth: Someone needs to stop you.
	elseif(iGoatBotherState == 6) then

		--Less than ten bothers.
		if(iGoatBotherCount < 10.0) then
			fnCutscene([[ Append("(\"So you took the money but kept bothering the goat? What is wrong with you!? Please! Stop bothering the goat!\")") ]])
		else
			fnCutscene([[ Append("(\"You know what? Forget it. You're such a dink.\")") ]])
			VM_SetVar("Root/Variables/Global/Goat/iGoatBotherState", "N", 7.0)
			VM_SetVar("Root/Variables/Global/Goat/iGoatBotherCount", "N", 0.0)
		end
		
	--Seventh. Sequence ends here but the sign gets more vitriolic over time. You dink.
	elseif(iGoatBotherState == 7) then

		local iGoatBotherCountTotal = math.floor(VM_GetVar("Root/Variables/Global/Goat/iGoatBotherCountTotal", "N"))
		local sInsult = "BIG"
		if(iGoatBotherCountTotal >  20 + 70) then sInsult = "GIANT" end
		if(iGoatBotherCountTotal >  40 + 70) then sInsult = "HUGE" end
		if(iGoatBotherCountTotal >  60 + 70) then sInsult = "SUPER" end
		if(iGoatBotherCountTotal >  80 + 70) then sInsult = "ULTRA" end
		if(iGoatBotherCountTotal > 100 + 70) then sInsult = "TITANIC" end
	
		local sString = "[CLEAR][SPOOKY](YOU BOTHERED THE GOAT " .. iGoatBotherCountTotal .. " TIMES. YOUR MOTHER MUST BE PROUD OF YOU, YOU " .. sInsult .. " DINKBURGER.)[NOSPOOKY]"
		fnCutscene("WD_SetProperty(\"Append\", \"" .. sString .. "\") ")
	end
    
-- |[Skillbook]|
elseif(sObjectName == "Skillbook") then
    AL_SetProperty("Set Layer Disabled", "SkillbookIco", true)
	LM_ExecuteScript(gsMeiSkillbook, 4)
end
