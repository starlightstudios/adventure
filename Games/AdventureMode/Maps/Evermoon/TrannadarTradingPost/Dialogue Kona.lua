-- |[ ================================= Dialogue Script - Kona ================================= ]|
--Entry point for dialogue with Kona.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[Common Setup]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Kona", "Neutral") ]])
    
    -- |[Variables]|
    local iMetKona                         = VM_GetVar("Root/Variables/Chapter1/Kona/iMetKona", "N")
    local sMeiForm                         = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	local iHasSeenTrannadarFlorentinaScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFlorentinaScene", "N")
    
    -- |[First Meeting]|
    if(iMetKona == 0.0) then
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter1/Kona/iMetKona", "N", 1.0)
    
        --Dialogue.
        fnCutscene([[ Append("Kona:[E|Neutral] A thousand hellos to you![P] If you were looking for firmness, hardness, or flatness, then by gum you've come to the right alraune![P] Name's Kona![B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Smirk] Mei![P] Wow, can I just say?[P] You're looking great![B][C]") ]])
        if(sMeiForm == "Human") then
            fnCutscene([[ Append("Kona:[E|Happy] You're damn right![P] You're not too bad yourself![P] You do a lot of exercise for a living, right?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] I wait tables.[B][C]") ]])
            fnCutscene([[ Append("Kona:[E|Wink] Used to do that myself![P] You've got my respect, hun.[B][C]") ]])
        elseif(sMeiForm == "Alraune") then
            fnCutscene([[ Append("Kona:[E|Happy] You're damn right![P] But you can't relax, even though we mostly eat sunlight and air.[P] If you like being a soft succulent, that's on you.[B][C]") ]])
            fnCutscene([[ Append("Kona:[E|Happy] If you want to be a tough, firm, powerful plant?[P] Then that's going to require work![B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Happy] I want to be firm and tough![B][C]") ]])
        elseif(sMeiForm == "Bee") then
            fnCutscene([[ Append("Kona:[E|Happy] Right back at you![P] I can really respect someone who works on their wings, but you can't skip those legs![B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Happy] Yeah, but the abs![P] I have to tense myself up to fly right.[B][C]") ]])
            fnCutscene([[ Append("Kona:[E|Wink] That right there is a workout, hun![B][C]") ]])
        elseif(sMeiForm == "Ghost") then
            fnCutscene([[ Append("Kona:[E|Happy] So are you![P] I think![P] I can't really tell beneath that outfit but I think I can see some bicep![B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Happy] That's ectoplasm but I can reform myself into all sorts of muscles![B][C]") ]])
            fnCutscene([[ Append("Kona:[E|Wink] That's the spirit![P] Ha ha ha, oh, oh, I'm as bad as Nadia.[B][C]") ]])
        elseif(sMeiForm == "Gravemarker") then
            fnCutscene([[ Append("Kona:[E|Happy] You too![P] You're almost as rock hard as me![P] By the way, what are you?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Happy] I'm a -[P] well sort of, a gravemarker.[P] Yeah, a living statue.[B][C]") ]])
            fnCutscene([[ Append("Kona:[E|Wink] Then that means definition is just as challenging as building muscle, but don't worry, I believe in you.[B][C]") ]])
        elseif(sMeiForm == "Mannequin") then
            fnCutscene([[ Append("Kona:[E|Happy] You're looking great in whatever that ensemble is.[P] Hey, what are you, exactly?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] A mannequin.[P] It's from a curse.[B][C]") ]])
            fnCutscene([[ Append("Kona:[E|Neutral] Oh, that's unfortunate.[P] Can you work out?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] I think so?[B][C]") ]])
            fnCutscene([[ Append("Kona:[E|Happy] That's all I needed to hear![B][C]") ]])
        elseif(sMeiForm == "Slime") then
            fnCutscene([[ Append("Kona:[E|Happy] You're looking spot on, too![P] Honestly, you slimes can just openly cheat when it comes to muscle, I'm like, jealous![B][C]") ]])
        elseif(sMeiForm == "Werecat") then
            fnCutscene([[ Append("Kona:[E|Happy] You too, hun![P] Look at those muscles![P] Were you born swole?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] Lithe and flexible comes with the territory, purr...[B][C]") ]])
            fnCutscene([[ Append("Kona:[E|Wink] Sweet, but you gotta work to maintain that figure![B][C]") ]])
        elseif(sMeiForm == "Wisphag") then
            fnCutscene([[ Append("Kona:[E|Happy] You look half dead, but damn those arms are huge![P] I bet you can bench a thousand![B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] Maybe![P] I don't even know what they are, like clay or something?[P] But they're great![B][C]") ]])
        end
        
        if(iHasSeenTrannadarFlorentinaScene == 0.0) then
            fnCutscene([[ Append("Kona:[E|Neutral] I'm the physical trainer around here.[P] Blythe hired me to keep all the mercs in shape, and it keeps them out of trouble, too.[B][C]") ]])
            fnCutscene([[ Append("Kona:[E|Neutral] My program is so popular, even farmers and traders come to partake in the gains![B][C]") ]])
        else
            fnCutscene([[ Append("Florentina:[E|Neutral] Kona here is the physical trainer we keep around.[P] Keeps the mercs in shape.[B][C]") ]])
            fnCutscene([[ Append("Kona:[E|Neutral] Oh hey, if it isn't Flabentina.[P] Come out of your hidey-hole to take a gander at the sun?[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] I'll have you know that we plants are meant to be sedentary.[P] You're going against nature![B][C]") ]])
            fnCutscene([[ Append("Kona:[E|Wink] Yer damn right![P] Nothin' natural about these gains, hun.[P] Body's got a limit, and I push past it![B][C]") ]])
        end
        fnCutscene([[ Append("Kona:[E|Neutral] If you want, I can run you through my course.[P] 2 platina for 1 experience point, best rates you're going to find.[B][C]") ]])
        fnCutscene([[ Append("Kona:[E|Neutral] Remember, don't be afraid to sweat, baby, sweat!") ]])
    
    -- |[Repeats]|
    else
        fnCutscene([[ Append("Kona:[E|Happy] Hey, Mei![P] Ready to feel the burn?") ]])
        fnCutsceneBlocker()
        fnCutscene([[ AM_SetProperty("Open Trainer") ]])
    end
end