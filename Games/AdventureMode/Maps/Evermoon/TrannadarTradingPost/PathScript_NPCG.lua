-- |[ ========================================= NPC G ========================================== ]|
--NPC G is an alraune tending the plants.

-- |[ ========================== Setup =========================== ]|
-- |[Variables]|
local sEntityName = "NPC G"
local fWalkSpeed = 0.50
local saNodeList = {"D0", "D1", "D2", "D3"}

-- |[Existence Check]|
--If the NPC(s) don't exist, delete this script.
if(EM_Exists(sEntityName) == false) then
    io.write("Warning: Parallel cutscene deleted due to missing NPC. Internal Name: " .. Cutscene_GetProperty("Parallel Script Name") .. "\n")
    Cutscene_HandleParallel("DELETETHIS")
    return
end

-- |[ ====================== Cutscene Work ======================= ]|
-- |[Run Routine]|
fnNPCPathToRandomNode(sEntityName, fWalkSpeed, saNodeList)
