-- |[ ================================ Dialogue Script - Denise ================================ ]|
--Entry point for dialogue with Denise.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[ ================================== fnSetDeniseTopics() =================================== ]|
local function fnSetDeniseTopics()
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
    
    --Variables.
    local iDeniseShowHealth     = VM_GetVar("Root/Variables/Chapter1/Denise/iDeniseShow_Health", "N")
    local iDeniseShowPower      = VM_GetVar("Root/Variables/Chapter1/Denise/iDeniseShow_Power", "N")
    local iDeniseShowInitiative = VM_GetVar("Root/Variables/Chapter1/Denise/iDeniseShow_Initiative", "N")
    local iDeniseShowAccuracy   = VM_GetVar("Root/Variables/Chapter1/Denise/iDeniseShow_Accuracy", "N")
    local iDeniseShowEvade      = VM_GetVar("Root/Variables/Chapter1/Denise/iDeniseShow_Evasion", "N")
    local iDeniseShowSkills     = VM_GetVar("Root/Variables/Chapter1/Denise/iDeniseShow_Skills", "N")
    
    --Tomb of Red. Always available.
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Tomb Of Red\", " .. sDecisionScript .. ", \"Tomb\") ")
    
    --Health Catalysts.
    if(iDeniseShowHealth == 0.0) then
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Health Catalysts\",  " .. sDecisionScript .. ", \"QueryHealth\") ")
    end
    if(iDeniseShowPower == 0.0) then
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Power Catalysts\",  " .. sDecisionScript .. ", \"QueryPower\") ")
    end
    if(iDeniseShowInitiative == 0.0) then
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Initiative Catalysts\",  " .. sDecisionScript .. ", \"QueryInitiative\") ")
    end
    if(iDeniseShowAccuracy == 0.0) then
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Accuracy Catalysts\",  " .. sDecisionScript .. ", \"QueryAccuracy\") ")
    end
    if(iDeniseShowEvade == 0.0) then
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Evasion Catalysts\",  " .. sDecisionScript .. ", \"QueryEvasion\") ")
    end
    if(iDeniseShowSkills == 0.0) then
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Skills Catalysts\",  " .. sDecisionScript .. ", \"QuerySkills\") ")
    end
    
    --Goodbye. Always available.
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Goodbye\",  " .. sDecisionScript .. ", \"Goodbye\") ")
    fnCutsceneBlocker()
end

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[Common Setup]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Denise", "Neutral") ]])
    
    -- |[Variables]|
    local iMetDenise                       = VM_GetVar("Root/Variables/Chapter1/Denise/iMetDenise", "N")
	local iHasSeenTrannadarFlorentinaScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFlorentinaScene", "N")
    
    -- |[First Meeting]|
    if(iMetDenise == 0.0) then
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter1/Denise/iMetDenise", "N", 1.0)
    
        --Dialogue.
        fnCutscene([[ Append("Denise:[E|Neutral] Ah, hello.[P] My name is Denise.[P] It is nice to meet you.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Smirk] Hello![P] My name's Mei.[B][C]") ]])
        if(iHasSeenTrannadarFlorentinaScene == 0.0) then
            fnCutscene([[ Append("Denise:[E|Neutral] New in the area, are you?[P] I hope so.[P] Great.[B][C]") ]])
        else
            fnCutscene([[ Append("Florentina:[E|Neutral] Denise here is the local stark-raving maniac.[P] Don't waste your time with her.[B][C]") ]])
            fnCutscene([[ Append("Denise:[E|Smirk] I appreciate the vote of confidence.[B][C]") ]])
        end
        fnCutscene([[ Append("Mei:[E|Smirk] Uh, is there something wrong?[B][C]") ]])
        fnCutscene([[ Append("Denise:[E|Neutral] You're looking for something, right?[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Happy] I am![P] I - [B][C]") ]])
        fnCutscene([[ Append("Denise:[E|Insight] Don't tell me.[P] Fulfillment, companionship, and independence.[P] Someone you can believe in, want to believe in, without being made to believe in.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] Ah, no.[P] I'm looking for a way home.[B][C]") ]])
        fnCutscene([[ Append("Denise:[E|Neutral] Damn it![P] Damn it![P] Why are the auras always wrong![B][C]") ]])
        if(iHasSeenTrannadarFlorentinaScene == 1.0) then
            fnCutscene([[ Append("Florentina:[E|Neutral] Like I said.[P] Local maniac.[B][C]") ]])
            fnCutscene([[ Append("Denise:[E|Neutral] Shut up![P] You're the only one I was ever right about.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Happy] No you weren't.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] Right about?[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] Denise is a fortune teller, or something like it.[P] Never told a right fortune in her life but people keep paying her.[B][C]") ]])
        end
        fnCutscene([[ Append("Mei:[E|Neutral] I'll take it that you don't know a way back to Earth.[P] Oh well.[B][C]") ]])
        fnCutscene([[ Append("Denise:[E|Neutral] I'm not totally worthless, I promise.[P] Look, I can see the ache of a catalyst.[P] You like those, right?[B][C]") ]])
        fnCutscene([[ Append("Denise:[E|Neutral] Catalysts are objects that increase your power when collected.[P] I know where they are.[P] For a fee, I can tell you.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] Should I really pay for that, an unproven fortune teller?[P] Can I get a discount?[B][C]") ]])
        fnCutscene([[ Append("Denise:[E|Neutral] To prove myself, yes, I will tell you where to find one.[P] One I am sure you cannot get without my knowledge.[P] Will that do?[B][C]") ]])
        if(iHasSeenTrannadarFlorentinaScene == 1.0) then
            fnCutscene([[ Append("Florentina:[E|Happy] Ooh, free samples?[P] Who cares if the biscuits taste like ash if they're free![B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] That -[P] that's a reference to an old story, Mei.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] Didn't click with me at all.[B][C]") ]])
        end
        fnCutscene([[ Append("Denise:[E|Insight] Tomb of red, a place where the never living grumble and scratch at the door of life...[B][C]") ]])
        fnCutscene([[ Append("Denise:[E|Insight] Four caskets, one open a peek.[P] Southwest, Northeast, Southeast, then the third, sightless, seek.[P] Touch it and the way will open.[B][C]") ]])
        fnCutscene([[ Append("Denise:[E|Smirk] With my luck, that might just wind up making sense.[P] There.[P] Did it work?[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] If I ever -[P] in a tomb of red?[P] If it happens I'll let you know.[P] Thanks for the reading![B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] (Still, might be worth paying Denise if I can't find a catalyst for some reason.[P] What do I have to lose?[P] Other than this world's money, I guess.)") ]])
        fnCutsceneBlocker()
    
    -- |[Repeats]|
    else
        fnCutscene([[ Append("Denise:[E|Neutral] What sorts of service might I interest you in today?[B]") ]])
        fnSetDeniseTopics()
    end
    
-- |[ ==================================== Always-Available ==================================== ]|
elseif(sTopicName == "Tomb") then
	WD_SetProperty("Hide")
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Denise", "Neutral") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] Could you repeat that fortune you gave me earlier? The one with the Tomb of Red?[B][C]") ]])
    fnCutscene([[ Append("Denise:[E|Insight] Tomb of red, a place where the never living grumble and scratch at the door of life...[B][C]") ]])
    fnCutscene([[ Append("Denise:[E|Insight] Four caskets, one open a peek.[P] Southwest, Northeast, Southeast, then the third, sightless, seek.[P] Touch it and the way will open.[B][C]") ]])
    fnCutscene([[ Append("Denise:[E|Neutral] I hope that helps.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Smirk] Very possible![B]") ]])
    fnSetDeniseTopics()

elseif(sTopicName == "No") then
	WD_SetProperty("Hide")
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Denise", "Neutral") ]])
    fnCutscene([[ Append("Denise:[E|Insight] I sense you have changed your mind.[P] Very well.[B][C]") ]])
    fnCutscene([[ Append("Denise:[E|Neutral] Anything else I can do for you?[B]") ]])
    fnSetDeniseTopics()

elseif(sTopicName == "Goodbye") then
	WD_SetProperty("Hide")
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Denise", "Neutral") ]])
    fnCutscene([[ Append("Denise:[E|Smirk] You'll be back.") ]])
    
-- |[ =================================== Querying Catalysts =================================== ]|
elseif(string.sub(sTopicName, 1, 5) == "Query") then
	WD_SetProperty("Hide")

    --Resolve the catalyst name.
    local sCatalystType = string.sub(sTopicName, 6)
    
    --Variables.
    local iPlatina = AdInv_GetProperty("Platina")
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    
    --Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Denise", "Neutral") ]])
    fnCutscene([[ Append("Denise:[E|Neutral] I will mark all the catalysts of that type on your map for a one-time fee of 500 platina.[P] Any you have already found won't appear, and they will be removed as you find them.[B][C]") ]])
    if(iPlatina < 500) then
        fnCutscene([[ Append("Denise:[E|Neutral] So how about it?[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] I'd love to, but I'm a little short.[B][C]") ]])
        fnCutscene([[ Append("Denise:[E|Neutral] Join the club.[P] Very well, my fortunes can wait until you have your fortunes.") ]])
    else
        fnCutscene([[ Append("Denise:[E|Neutral] So how about it?[B]") ]])
        fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Sure\",      " .. sDecisionScript .. ", \"Yes" .. sCatalystType .. "\") ")
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"No Thanks\", " .. sDecisionScript .. ", \"No\") ")
    end
    
-- |[ ============================== Purchasing Catalyst Markers =============================== ]|
elseif(string.sub(sTopicName, 1, 3) == "Yes") then
	WD_SetProperty("Hide")

    --Resolve the catalyst name.
    local sCatalystType = string.sub(sTopicName, 4)
    
    --Assemble the variable path.
    local sPath = "Root/Variables/Chapter1/Denise/iDeniseShow_" .. sCatalystType
    
    --Set it to 1
    VM_SetVar(sPath, "N", 1.0)
    
    --Subtract platina.
	AdInv_SetProperty("Remove Platina", 500)
    
    --Show pins.
    local sLevelName = AL_GetProperty("Name")
    GUIMap:fnRunPinHandlerOnAllMaps(sLevelName)
    
    --Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Denise", "Neutral") ]])
    fnCutscene([[ Append("Denise:[E|Neutral] [SOUND|Menu|BuyOrSell]There you go.[P] Don't worry about the map pins coming back to me, I have my ways.[B][C]") ]])
    fnCutscene([[ Append("Denise:[E|Smirk] Anything else I can do for you?[B]") ]])
    fnSetDeniseTopics()
    
end