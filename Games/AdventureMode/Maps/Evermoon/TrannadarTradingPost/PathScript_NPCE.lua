-- |[ ========================================= NPC E ========================================== ]|
--NPC E dropped her contact lens.

-- |[ ========================== Setup =========================== ]|
-- |[Variables]|
local sEntityName = "NPC E"
local fWalkSpeed = 0.50
local saNodeList = {"C0", "C1", "C2", "C3", "C4"}

-- |[Existence Check]|
--If the NPC(s) don't exist, delete this script.
if(EM_Exists(sEntityName) == false) then
    io.write("Warning: Parallel cutscene deleted due to missing NPC. Internal Name: " .. Cutscene_GetProperty("Parallel Script Name") .. "\n")
    Cutscene_HandleParallel("DELETETHIS")
    return
end

-- |[ ====================== Cutscene Work ======================= ]|
-- |[Run Routine]|
fnNPCPathToRandomNode(sEntityName, fWalkSpeed, saNodeList)
