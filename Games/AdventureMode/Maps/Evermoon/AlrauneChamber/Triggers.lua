-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
--Florentina leaves the party temporarily. She stops following but doesn't leave the status display.
if(sObjectName == "FlorentinaLeave" and giFollowersTotal > 0) then

	--Variables
	local iCompletedTrapDungeon        = VM_GetVar("Root/Variables/Chapter1/Scenes/iCompletedTrapDungeon", "N")
	local iFlorentinaLeaveRochea       = VM_GetVar("Root/Variables/Chapter1/Scenes/iFlorentinaLeaveRochea", "N")
	local iFlorentinaSpecialAlraune    = VM_GetVar("Root/Variables/Chapter1/Scenes/iFlorentinaSpecialAlraune", "N")
	local iSeenAlrauneBattleIntroScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iSeenAlrauneBattleIntroScene", "N")
	
	--If Rochea is not present, then Florentina doesn't leave. This occurs if Rochea is currently in the trap dungeon.
	if(iCompletedTrapDungeon == 0.0 and iSeenAlrauneBattleIntroScene == 1.0) then
		VM_SetVar("Root/Variables/Chapter1/Scenes/iAlrauneMeetFlorentina", "N", 0.0)
	
	--Rochea is present, Florentina leaves.
	else
		--Set.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iAlrauneMeetFlorentina", "N", 100.0)
		
		--Haven't seen this scene before.
		if(iFlorentinaLeaveRochea == 0.0) then
			
			--Flag.
			VM_SetVar("Root/Variables/Chapter1/Scenes/iFlorentinaLeaveRochea", "N", 1.0)

			--Dialogue setup.
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina") ]])
		
			--If Mei has not spoken about Rochea before:
			if(iFlorentinaSpecialAlraune == 0.0) then
				fnCutscene([[ Append("Florentina:[E|Neutral] Mei...[P] you go on ahead.[B][C]") ]])
				fnCutscene([[ Append("Mei: Hmm?[B][C]") ]])
				fnCutscene([[ Append("Florentina:[E|Confused] Rochea and I have a bit of a history.[P] It'd be best if I wasn't around.[B][C]") ]])
				fnCutscene([[ Append("Florentina:[E|Neutral] I'll meet you outside.") ]])
			
			--If Mei has asked about Rochea before:
			else
				fnCutscene([[ Append("Florentina:[E|Neutral] Mei...[P] you go on ahead.[B][C]") ]])
				fnCutscene([[ Append("Mei: But didn't you say - [P][CLEAR]") ]])
				fnCutscene([[ Append("Florentina:[E|Confused] Not yet.[P] I'm not very good at apologies.[P] I need to think about it.[B][C]") ]])
				fnCutscene([[ Append("Florentina:[E|Neutral] I'll meet you outside.") ]])
			end
			
			--Common code.
			fnCutsceneBlocker()
			
		--Have seen it, make it shorter.
		else

			--Dialogue setup.
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina") ]])
		
			--If Mei has not spoken about Rochea before:
			if(iFlorentinaSpecialAlraune == 0.0) then
				fnCutscene([[ Append("Florentina:[E|Neutral] I'll be outside.") ]])
			
			--If Mei has asked about Rochea before:
			else
				fnCutscene([[ Append("Florentina:[E|Neutral] I'll be outside.[B][C]") ]])
				fnCutscene([[ Append("Mei: You still need time?[B][C]") ]])
				fnCutscene([[ Append("Florentina:[E|Confused] ...[P] I'll be outside...") ]])
			end
			fnCutsceneBlocker()

		end

		--Common code. Move Florentina to the stairs.
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Florentina")
			ActorEvent_SetProperty("Move To", (11.25 * gciSizePerTile), (9.50 * gciSizePerTile))
		DL_PopActiveObject()
		fnCutsceneBlocker()
		
		--Teleport Florentina off the map.
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Florentina")
			ActorEvent_SetProperty("Teleport To", (-100.25 * gciSizePerTile), (-100.50 * gciSizePerTile))
		DL_PopActiveObject()
		fnCutsceneBlocker()
		
		--Remove Florentina from the party.
		fnRemovePartyMember("Florentina", false)
	end
end
