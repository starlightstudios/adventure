-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    --Make the NPC face who is talking to them.
    TA_SetProperty("Face Character", "PlayerEntity")
        
    -- |[Rochea]|
    if(sActorName == "Rochea") then
        WD_SetProperty("Show")
        WD_SetProperty("Major Sequence", true)
        WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral")
        WD_SetProperty("Actor In Slot", 4, "Rochea", "Neutral")
        Append("Rochea: Greetings, leaf-sister Mei.[P] What may I help you with?[B][C]")
        WD_SetProperty("Activate Topics After Dialogue", "Rochea")
    
    -- |[Generic Alraune A]|
    elseif(sActorName == "AlrauneA") then
        fnStandardDialogue("Alraune:[VOICE|Alraune] Walk with nature, leaf-sister.")
    
    -- |[Generic Alraune B]|
    elseif(sActorName == "AlrauneB") then
        fnStandardDialogue("Alraune:[VOICE|Alraune] Welcome![P] We are always glad to join new sisters!")
    
    -- |[Generic Alraune C]|
    elseif(sActorName == "AlrauneC") then
        fnStandardDialogue("Alraune:[VOICE|Alraune] May you find clarity wherever you go.")
    
    end
end
