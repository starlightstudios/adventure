-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = LM_GetScriptArgument(1, "N")

-- |[ ======================================== Triggers ======================================== ]|
--The party hears Cassandra call for help!
if(sObjectName == "HelpMe") then

	-- |[Setup]|
	--Don't repeat the event:
	local iStartedCassandraEvent = VM_GetVar("Root/Variables/Chapter1/Scenes/iStartedCassandraEvent", "N")
	if(iStartedCassandraEvent > 0.0) then return end

	--Variables.
	local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
    
    --Mark the campfire as the last used one. This prevents warping outside the event.
    AL_SetProperty("Last Save Point", "EvermoonCassandraA")
	
	--Set flags.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iStartedCassandraEvent", "N", 1.0)
	
	--Roll where Cassandra will spawn.
	local iRoll = LM_GetRandomNumber(1, 4)
	iRoll = 1
	if(iRoll == 1) then
		VM_SetVar("Root/Variables/Chapter1/Scenes/sCassandraLocation", "S", "EvermoonCassandraCC")
	elseif(iRoll == 2) then
		VM_SetVar("Root/Variables/Chapter1/Scenes/sCassandraLocation", "S", "EvermoonCassandraCE")
	elseif(iRoll == 3) then
		VM_SetVar("Root/Variables/Chapter1/Scenes/sCassandraLocation", "S", "EvermoonCassandraCNW")
	else
		VM_SetVar("Root/Variables/Chapter1/Scenes/sCassandraLocation", "S", "EvermoonCassandraCNE")
	end
	
	-- |[Movement]|
	--Mei is alone:
	if(bIsFlorentinaPresent == false) then
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Move To", (21.25 * gciSizePerTile), (12.50 * gciSizePerTile))
		DL_PopActiveObject()
	
	--Florentina is here:
	else
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Move To", (20.75 * gciSizePerTile), (12.50 * gciSizePerTile))
		DL_PopActiveObject()
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Florentina")
			ActorEvent_SetProperty("Move To", (21.75 * gciSizePerTile), (12.50 * gciSizePerTile))
		DL_PopActiveObject()
	end
	fnCutsceneBlocker()
	
	--Timing.
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	-- |[Dialogue]|
	--Mei is alone:
	if(bIsFlorentinaPresent == false) then
		
		--Setup.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		
		--Talking.
		fnCutscene([[ Append("Mei:[E|Surprise] (..?[P] Blood?)[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] (Looks really fresh...)[B][C]") ]])
		fnCutscene([[ Append("Voice:[VOICE|MercF] Heeeelp![P] Eeeeeeeeeekkkk![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Surprise] (Oh crud![P] That's a yes![P] I have to do something!)[B][C]") ]])
		fnCutscene([[ Append("(Hey, you![P] The one at the keyboard![P] This is a timed sequence, but it's timed by battles, not seconds![P] Luckily, resting also doesn't consume time.)[B][C]") ]])
		fnCutscene([[ Append("(Every battle you fight wastes time -[P] you don't have much if you want to save that poor girl!)[B][C]") ]])
		fnCutscene([[ Append("(Stop mewling and go save her!)") ]])
	
	--Florentina is here to help:
	else
	
		--Setup.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
		
		--Talking.
		fnCutscene([[ Append("Mei:[E|Surprise] Blood?[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Surprise] The little ones are saying it belongs to a blonde girl.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] It's very fresh.[P] Do you think she's - [P][CLEAR]") ]])
		fnCutscene([[ Append("Voice:[VOICE|MercF] Heeeelp![P] Eeeeeeeeeekkkk![B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] Sounds like she got on the wrong side of the werecats.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] We -[P] we have to help![P] Come on![B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Surprise] We do?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] When somebody calls for help, you help, damn it![P] Let's go![B][C]") ]])
		fnCutscene([[ Append("(Hey, you![P] The one at the keyboard![P] This is a timed sequence, but it's timed by battles, not seconds![P] Luckily, resting also doesn't consume time.)[B][C]") ]])
		fnCutscene([[ Append("(Every battle you fight wastes time -[P] you don't have much if you want to save that poor girl!)[B][C]") ]])
		fnCutscene([[ Append("(Stop mewling and go save her!)") ]])
	end
	fnCutsceneBlocker()
	
	--Change the music.
	AL_SetProperty("Music", "TimeSensitive")
	fnCutsceneBlocker()
	
	--Move Florentina onto Mei.
	if(bIsFlorentinaPresent == true) then
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Florentina")
			ActorEvent_SetProperty("Move To", (20.75 * gciSizePerTile), (12.50 * gciSizePerTile))
		DL_PopActiveObject()
		fnCutsceneBlocker()
		fnCutscene([[ AL_SetProperty("Fold Party") ]])
		fnCutsceneBlocker()
	end
end
