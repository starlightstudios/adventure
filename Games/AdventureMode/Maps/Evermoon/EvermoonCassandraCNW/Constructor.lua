-- |[Constructor]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "EvermoonCassandraCNW"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[Standard]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Variables.
	local iStartedCassandraEvent = VM_GetVar("Root/Variables/Chapter1/Scenes/iStartedCassandraEvent", "N")

	--Music. If the Cassandra event is running, change the theme
	if(iStartedCassandraEvent == 1.0) then
		
		--Music setup.
		AL_SetProperty("Music", "TimeSensitive")
		AdvCombat_SetProperty("Next Combat Music", "TimeSensitive", -1.0)
		
	--All other cases:
	else
		AL_SetProperty("Music", "ForestTheme")
	end
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
    
    -- |[Map Handling]|
    --Resolve the map's variable.
    fnStandardCh1Map(sLevelName)
	
	-- |[Spawn NPCS Here]|
	--NPCs set to spawn here will be available for post-exec cutscenes.

-- |[Post-Exec]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then
	
	-- |[Set Time]|
	--Time of day needs to be set here during the Cassandra event.
	local iStartedCassandraEvent = VM_GetVar("Root/Variables/Chapter1/Scenes/iStartedCassandraEvent", "N")
	if(iStartedCassandraEvent == 1.0) then
		giTmeOfDayModifyTicks = 1
		fnSetCassandraTime()
		giTmeOfDayModifyTicks = 45
	end
	
	-- |[Werecat Spawning]|
	--If this location happens to be where Cassandra spawns, spawn her and the werecats.
	local sCassandraLocation = VM_GetVar("Root/Variables/Chapter1/Scenes/sCassandraLocation", "S")
	if(sCassandraLocation == "EvermoonCassandraCNW" and iStartedCassandraEvent == 1.0) then
		
		--Central set.
		TA_Create("Cassandra")
			TA_SetProperty("Position", 10, 7)
			TA_SetProperty("Facing", gci_Face_South)
			TA_SetProperty("Clipping Flag", false)
			fnSetCharacterGraphics("Root/Images/Sprites/CassandraH/", false)
			TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/CassandraH|Wounded")
			TA_SetProperty("Set Special Frame", "Wounded")
		DL_PopActiveObject()
		TA_Create("WerecatA")
			TA_SetProperty("Position", 9, 7)
			TA_SetProperty("Facing", gci_Face_East)
			TA_SetProperty("Clipping Flag", false)
			fnSetCharacterGraphics("Root/Images/Sprites/Werecat/", false)
			TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/Werecat|Wounded")
		DL_PopActiveObject()
		TA_Create("WerecatB")
			TA_SetProperty("Position", 11, 7)
			TA_SetProperty("Facing", gci_Face_West)
			TA_SetProperty("Clipping Flag", false)
			fnSetCharacterGraphics("Root/Images/Sprites/Werecat/", false)
			TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/Werecat|Wounded")
		DL_PopActiveObject()
		TA_Create("WerecatC")
			TA_SetProperty("Position", 10, 6)
			TA_SetProperty("Facing", gci_Face_South)
			TA_SetProperty("Clipping Flag", false)
			fnSetCharacterGraphics("Root/Images/Sprites/Werecat/", false)
			TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/Werecat|Wounded")
		DL_PopActiveObject()
		
		--Western set.
		TA_Create("WerecatD")
			TA_SetProperty("Position", 6, 7)
			TA_SetProperty("Facing", gci_Face_South)
			TA_SetProperty("Clipping Flag", false)
			fnSetCharacterGraphics("Root/Images/Sprites/Werecat/", false)
			TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/Werecat|Wounded")
			TA_SetProperty("Set Special Frame", "Wounded")
		DL_PopActiveObject()
		TA_Create("WerecatE")
			TA_SetProperty("Position", 6, 6)
			TA_SetProperty("Facing", gci_Face_South)
			TA_SetProperty("Clipping Flag", false)
			fnSetCharacterGraphics("Root/Images/Sprites/Werecat/", false)
			TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/Werecat|Wounded")
		DL_PopActiveObject()
		TA_Create("WerecatF")
			TA_SetProperty("Position", 7, 7)
			TA_SetProperty("Facing", gci_Face_West)
			TA_SetProperty("Clipping Flag", false)
			fnSetCharacterGraphics("Root/Images/Sprites/Werecat/", false)
			TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/Werecat|Wounded")
		DL_PopActiveObject()
		
		--Eastern set.
		TA_Create("WerecatG")
			TA_SetProperty("Position", 14, 6)
			TA_SetProperty("Facing", gci_Face_East)
			TA_SetProperty("Clipping Flag", false)
			fnSetCharacterGraphics("Root/Images/Sprites/Werecat/", false)
			TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/Werecat|Wounded")
		DL_PopActiveObject()
		TA_Create("WerecatH")
			TA_SetProperty("Position", 15, 6)
			TA_SetProperty("Facing", gci_Face_West)
			TA_SetProperty("Clipping Flag", false)
			fnSetCharacterGraphics("Root/Images/Sprites/Werecat/", false)
			TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/Werecat|Wounded")
		DL_PopActiveObject()
		
	end
	
	--Overlays.
    fnHeavyForest()

end
