-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Execution]|
--Sample object
if(sObjectName == "Sign East") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Leader] (It reads \"Need a rest or supplies?[P] Head east![P] We can't wait to meet you!\")") ]])
	fnCutsceneBlocker()

--Boat.
elseif(sObjectName == "Boat") then

	--If the player doesn't have the oars yet...
	local iHasOars = AdInv_GetProperty("Item Count", "Boat Oars")
	if(iHasOars < 1) then
		fnStandardDialogue("[VOICE|Leader](It's a boat, but I can't row it without a set of oars...)")
	else
		
		--Ask a question.
		fnStandardDialogue("[VOICE|Leader](Use the boat to cross the lake?)[BLOCK]")
	
		--Activate and set.
		local sDestScript = fnResolvePath() .. "CrossLake.lua"
		WD_SetProperty("Activate Decisions")
		WD_SetProperty("Add Decision", "Yes", sDestScript, "Yes")
		WD_SetProperty("Add Decision", "No",  sDestScript, "No")
	end

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
