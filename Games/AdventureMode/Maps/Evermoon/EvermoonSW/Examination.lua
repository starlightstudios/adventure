-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Examination]|
--Sign next to the rockslide.
if(sObjectName == "Rockslide Sign") then
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Leader] (\"Caution:: Rockslide.[P] Also, now-hiring:: People to clear rockslides.[P] Apply at Trannadar Trading Post.\" -Nadia.)") ]])
	fnCutsceneBlocker()
	
	--Variables.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iHasReadNadiaSign", "N", 1.0)
	WD_SetProperty("Unlock Topic", "Signs", 1)

--Sign on the west side.
elseif(sObjectName == "Sign West") then
	
	--Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Leader] (\"South:: Trafal Glacier Pass.[P] North and West:: Trannadar Trading Post.\" -Nadia.)") ]])
	fnCutsceneBlocker()
	
	--Variables.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iHasReadNadiaSign", "N", 1.0)
	WD_SetProperty("Unlock Topic", "Signs", 1)

--Barrel in the northwest.
elseif(sObjectName == "BarrelNW") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Leader] (Looks like a barrel that smells vaguely of gunpowder.[P] It's mostly empty, though.)") ]])
	fnCutsceneBlocker()

--Plants.
elseif(sObjectName == "Plants") then

	--Variables.
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")

	--Normal dialogue:
	if(sMeiForm ~= "Alraune") then
		fnStandardDialogue([[ [VOICE|Mei](These plants are very well kept.) ]])
	
	--Alraune.
	else
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Mei:[VOICE|Mei] (Clamoring for attention, are you?[P] Yes yes, I'm excited to meet you too.)[B][C]") ]])
		fnCutscene([[ Append("Mei:[VOICE|Mei] (Your vines are so well pruned![P] The leaf-sisters here are true experts.)[B][C]") ]])
		fnCutscene([[ Append("Mei:[VOICE|Mei] (Can't stay too long, though.[P] I have adventuring to do!)") ]])
	end

--Plants that slimes really seem to like.
elseif(sObjectName == "SlimePlant") then

	--Variables.
	local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	local iMeiTriedFruit = VM_GetVar("Root/Variables/Chapter1/Scenes/iMeiTriedFruit", "N")

	--Normal dialogue:
	if(sMeiForm ~= "Alraune" and sMeiForm ~= "Slime") then
		fnStandardDialogue([[[VOICE|Mei](The fruit from these plants is very pungent smelling!)]])
	
	--Slime:
	elseif(sMeiForm == "Slime") then
	
		--If Florentina is not present:
		if(bIsFlorentinaPresent == false) then
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ Append("Mei:[VOICE|Mei] (Hmm, these smell delicious!)[B][C]") ]])
			fnCutscene([[ Append("Mei:[VOICE|Mei] (Probably not a good idea to eat strange fruit, though.)") ]])
	
		--If Florentina is present:
		else
			fnStandardMajorDialogue()
			
			if(iMeiTriedFruit == 0.0) then
				VM_SetVar("Root/Variables/Chapter1/Scenes/iMeiTriedFruit", "N", 1.0)
				fnCutscene([[ Append("Mei:[E|Happy] Mmmmm, these fruits smell really, really good![B][C]") ]])
				fnCutscene([[ Append("Florentina:[E|Neutral] You can smell stuff with no nose?[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Neutral] I smell with my whole body.[P] The other slimes here must smell it too.[B][C]") ]])
				fnCutscene([[ Append("Florentina:[E|Neutral] Well that's because these are -[P][EMOTION|Florentina|Happy] oh, you should try one of the fruits.[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Offended] Hmm, eating strange fruit might be unsafe.[B][C]") ]])
				fnCutscene([[ Append("Florentina:[E|Neutral] I can talk to plants, dummy.[P] These are safe.[P] Besides, the other slimes have them in their bodies, see?[P] They're okay.[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Neutral] If you say so.[P][EMOTION|Mei|Offended] But if I get sick, I'll slime-smack you good![B][C]") ]])
				fnCutscene([[ Append("Florentina:[E|Neutral] Just eat one.[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Neutral] ...[P] I guess I can just shove this inside myself.[P] No need for a mouth.[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Neutral] *shlorp*[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Neutral] Oooh, that was so sweet![P] It tastes - [B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Blush] Uuuuhhhh...[P] Florentina?[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Blush] I'm feeling a little -[P] hey, were you always blue?[B][C]") ]])
				fnCutscene([[ Append("Florentina:[E|Happy] Haaaa ha ha ha![B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Blush] I'ma tell you a shecret right now.[P] Florentina -[P] I'm a shlime right now.[P] Shhhhh...[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Blush] I'ma shlime and I'm totally washted...[B][C]") ]])
				fnCutscene([[ Append("Florentina:[E|Happy] Why send away for dancers or performers when we have drunk slime Mei for entertainment?[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Blush] Thish fruit got me drunk?[P] Oooohh...[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Blush] *shlorp*[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Neutral] ...[P] Heh.[P] The effects dissipate pretty quickly when the fruit is gone.[P] Guess that's what happens when your whole body acts like a liver.[B][C]") ]])
				fnCutscene([[ Append("Florentina:[E|Happy] Aw come on, put it back in![P] I was having fun![B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Offended] We have a job to do![P] But -[P][EMOTION|Mei|Neutral] remember where these plants are so I can come back later...") ]])
			else
				fnCutscene([[ Append("Mei:[E|Neutral] (I don't have time to get hammered right now...[P] but...)[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Neutral] (As my alcoholic uncle Liang once said::[P] 'Any day not spent wasted, is'.)") ]])
			end
		end
	
	--Alraune.
	elseif(sMeiForm == "Alraune") then
		fnCutscene([[ WD_SetProperty("Show") ]])
			
		if(iMeiTriedFruit == 0.0) then
			fnCutscene([[ Append("Mei:[VOICE|Mei] (You're very popular with the slimes, it seems.)[B][C]") ]])
			fnCutscene([[ Append("Mei:[VOICE|Mei] (Oh come on, tell me![P] Why!?)[B][C]") ]])
			fnCutscene([[ Append("Mei:[VOICE|Mei] (...[P] Always with the pranks![P] One of these days I'm going to play a prank on you right back!)[B][C]") ]])
			fnCutscene([[ Append("Mei:[VOICE|Mei] (I don't actually mean that, little one.[P] Really![P] I'll be nice!)") ]])
		else
			fnCutscene([[ Append("Mei:[VOICE|Mei] (So the slimes eat your fruit to get drunk, and then spread the seeds around as they wander?)[B][C]") ]])
			fnCutscene([[ Append("Mei:[VOICE|Mei] (Well aren't you clever little ones!)") ]])
		end
	end

-- |[Doors]|
--Western cabin door. Requires a pry bar to open.
elseif(sObjectName == "StuckCabinDoorW") then

	--Variables.
	local iShowedDoorPry = VM_GetVar("Root/Variables/Chapter1/Scenes/iShowedDoorPry", "N")
	local iHasPryBar = AdInv_GetProperty("Item Count", "Pry Bar")
	
	--Has the pry bar.
	if(iHasPryBar >= 1) then
		
		--Hasn't shown the dialogue, so do that.
		if(iShowedDoorPry == 0.0) then
			VM_SetVar("Root/Variables/Chapter1/Scenes/iShowedDoorPry", "N", 1.0)
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ Append("Thought:[VOICE|Leader] (The door is stuck...)") ]])
			fnCutsceneBlocker()
			fnCutsceneWait(15)
			fnCutsceneBlocker()
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ Append("Thought:[VOICE|Leader] (Got it open with the pry bar!)") ]])
			fnCutscene([[ AL_SetProperty("Open Door", "CabinDoorW") ]])
			fnCutscene([[ AudioManager_PlaySound("World|FlipSwitch") ]])
			fnCutsceneBlocker()
		
		--Don't repeat the dialogue.
		else
			AL_SetProperty("Open Door", "CabinDoorW")
			AudioManager_PlaySound("World|FlipSwitch")
		end
	
	--Door is stuck.
	else
		AudioManager_PlaySound("World|RemoteDoor")
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Thought:[VOICE|Leader] (The door is stuck.[P] Maybe I can find something to open it with?)") ]])
		fnCutsceneBlocker()
	end

--Eastern cabin door. Requires a pry bar to open.
elseif(sObjectName == "StuckCabinDoorE") then

	--Variables.
	local iShowedDoorPry = VM_GetVar("Root/Variables/Chapter1/Scenes/iShowedDoorPry", "N")
	local iHasPryBar = AdInv_GetProperty("Item Count", "Pry Bar")
	
	--Has the pry bar.
	if(iHasPryBar >= 1) then
		
		--Hasn't shown the dialogue, so do that.
		if(iShowedDoorPry == 0.0) then
			VM_SetVar("Root/Variables/Chapter1/Scenes/iShowedDoorPry", "N", 1.0)
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ Append("Thought:[VOICE|Leader] (The door is stuck...)") ]])
			fnCutsceneBlocker()
			fnCutsceneWait(15)
			fnCutsceneBlocker()
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ Append("Thought:[VOICE|Leader] (Got it open with the pry bar!)") ]])
			fnCutscene([[ AL_SetProperty("Open Door", "CabinDoorE") ]])
			fnCutscene([[ AudioManager_PlaySound("World|FlipSwitch") ]])
			fnCutsceneBlocker()
		
		--Don't repeat the dialogue.
		else
			AL_SetProperty("Open Door", "CabinDoorE")
			AudioManager_PlaySound("World|FlipSwitch")
		end
	
	--Door is stuck.
	else
		AudioManager_PlaySound("World|RemoteDoor")
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Thought:[VOICE|Leader] (The door is stuck.[P] Maybe I can find something to open it with?)") ]])
		fnCutsceneBlocker()
	end

-- |[Skillbook]|
elseif(sObjectName == "Skillbook") then
    AL_SetProperty("Set Layer Disabled", "SkillbookIco", true)
	LM_ExecuteScript(gsMeiSkillbook, 2)

-- |[Debug]|
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
