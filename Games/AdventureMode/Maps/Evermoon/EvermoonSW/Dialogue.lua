-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then

    -- |[Name Resolve]|
    --Resolve the name of the calling entity.
    local sActorName = TA_GetProperty("Name")

    --Common.
    TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[All Drunk Slimes]|
    if(sActorName == "DrunkSlimeA" or sActorName == "DrunkSlimeB" or sActorName == "DrunkSlimeC") then

        --Variables.
        local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
        local iMeiTriedFruit = VM_GetVar("Root/Variables/Chapter1/Scenes/iMeiTriedFruit", "N")
        
        --If Mei has tried the fruit before:
        if(iMeiTriedFruit == 1.0) then
            
            --Setup.
            fnStandardMajorDialogue()
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Slime", "Neutral") ]])
            
            --There is a 1/100 chance of this happening:
            local iRoll = LM_GetRandomNumber(1, 100)
            if(iRoll == 79 - 10) then --Der's a dinger for ya.
                fnCutscene([[ Append("Mei:[E|Happy] Hey there, good lookin'.[P] I got a bucket a' chicken.[P] Wanna do it?[B][C]") ]])
            
            --Normal.
            else
                fnCutscene([[ Append("Mei:[E|Happy] Hey there, beautiful, want to bump membranes?[B][C]") ]])
            end
            
            --If Mei is a slime:
            if(sMeiForm == "Slime") then
                fnCutscene([[ Append("Mei:[E|Laugh] I don't even know if that makes sense![P] Just being around these fruits is making me tipsy...") ]])
            
            --If Mei is not a slime:
            else
                fnCutscene([[ Append("Florentina:[E|Surprise] Are you hitting on that drunk slime?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] You don't think her sheen is arousing?[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Surprise] But you're not a slime![P] At least, not right now...[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Blush] All I have to do is give her a kiss and I will be...") ]])
            end
        
        --Mei hasn't tried the fruit:
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("[VOICE|Mei](The slime gives you a dopey smile.[P] It looks...[P] unsteady...)") ]])
        
        end
    end
end
