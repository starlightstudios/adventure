-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[Name Resolve]|
    --Resolve the name of the calling entity.
    local sActorName = TA_GetProperty("Name")

    -- |[Slime Guide]|
    if(sActorName == "SlimeGuide") then
	
        --Variables:
        VM_SetVar("Root/Variables/Chapter1/Scenes/iMetSlimeGuide", "N", 0.0)
        VM_SetVar("Root/Variables/Chapter1/Scenes/iSawSlimevilleIntro", "N", 0.0)
        local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
        local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
        local iMetSlimeGuide = VM_GetVar("Root/Variables/Chapter1/Scenes/iMetSlimeGuide", "N")
        
        --If Florentina is present, set this flag to true:
        if(bIsFlorentinaPresent == true) then
            VM_SetVar("Root/Variables/Chapter1/Scenes/iFlorentinaSawSmartSlimes", "N", 1.0)
        end
        
        --Set facing.
        TA_SetProperty("Face Character", "PlayerEntity")
            
        --Common.
        fnStandardMajorDialogue()
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Slime", "Blue") ]])
        
        --First time meeting the slime:
        if(iMetSlimeGuide == 0.0) then
            
            --Mei is a human:
            if(sMeiForm == "Human") then
                fnCutscene([[ Append("Slime:[E|Blue] Hi hi hi there, human![B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Surprise] Oh my goodness![B][C]") ]])
                fnCutscene([[ Append("Slime:[E|Blue] Oh, you can put the sword away.[P] I'm not going to hurt you![B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Offended] That remains to be seen.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Offended] I didn't know slimes could talk...[B][C]") ]])
                fnCutscene([[ Append("Slime:[E|Blue] Hee hee![P] You're silly![P] Of course we can talk![B][C]") ]])
                fnCutscene([[ Append("Slime:[E|Blue] But you gotta be super duper smart to talk, and most slimes are real ditzes![P] Tee hee hee![B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Smirk] Well, if you're not going to attack me then I guess it's all right...[B][C]") ]])
                fnCutscene([[ Append("Slime:[E|Blue] Oooooohhhh![P] You know what'd be soooo super cool?[B][C]") ]])
                fnCutscene([[ Append("Slime:[E|Blue] Okay I don't want to get in trouble so you have to promise to keep a secret.[P] Can you keep a secret?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Smirk] Sure?[B][C]") ]])
                fnCutscene([[ Append("Slime:[E|Blue] Cool![P] Cool![P] You seem like a really trustworthy human since you put that sword away![B][C]") ]])
                
                if(bIsFlorentinaPresent == true) then
                    fnCutscene([[ Append("Slime:[E|Blue] We slimes have a triple-decker secret village where we all hang out and talk about stuff![P] Want to see it?[B][C]") ]])
                    fnCutscene([[ Append("Florentina:[E|Neutral] Yes.[P] Yes we do.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Surprise] *You don't think it's a trap?*[B][C]") ]])
                    fnCutscene([[ Append("Florentina:[E|Neutral] *Oh, probably.[P] But if it's real, I could make a bundle selling these slimes -[P] whatever it is that slimes buy.[P] Think about it!*[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Surprise] *But is it worth the risk?*[B][C]") ]])
                    fnCutscene([[ Append("Slime:[E|Blue] What are you silly-billies whispering about?[P] Do you want to see my secret slime town or what?[BLOCK]") ]])
                else
                    fnCutscene([[ Append("Slime:[E|Blue] We slimes have a triple-decker secret village where we all hang out and talk about stuff![P] Want to see it?[BLOCK]") ]])
                end

                --Decision script is this script. It must be surrounded by quotes.
                local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
                fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
                fnCutscene(" WD_SetProperty(\"Add Decision\", \"Sure\", " .. sDecisionScript .. ", \"Sure\") ")
                fnCutscene(" WD_SetProperty(\"Add Decision\", \"No Thanks\",  " .. sDecisionScript .. ", \"No Thanks\") ")
                fnCutsceneBlocker()
            
                --Set:
                VM_SetVar("Root/Variables/Chapter1/Scenes/iMetSlimeGuide", "N", 1.0)
            
            --Mei is a slime:
            elseif(sMeiForm == "Slime") then
                fnCutscene([[ Append("Slime:[E|Blue] Oooohh wow![P] I haven't seen you before![B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Surprise] Oh my goodness![P] You can talk!?[B][C]") ]])
                fnCutscene([[ Append("Slime:[E|Blue] Uh, yeah.[P] You can talk too, can't you?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Surprise] I thought I was the only one![P] Can we all talk?[B][C]") ]])
                fnCutscene([[ Append("Slime:[E|Blue] Nope, hee hee![P] Only really smart slimes can talk![P] You gotta eat a lot of smarty fruits to get that far![B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] What's a smarty fruit?[B][C]") ]])
                fnCutscene([[ Append("Slime:[E|Blue] Those are the big red ones that hang off of trees![P] You obviously ate a lot to be so eloquent![B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Happy] Erm...[P] Of course I did![P] Doi![P] I was just making sure you knew![B][C]") ]])
                
                if(bIsFlorentinaPresent == true) then
                    fnCutscene([[ Append("Mei:[E|Offended] *You didn't mention anything about smarty fruits to me!*[B][C]") ]])
                    fnCutscene([[ Append("Florentina:[E|Surprise] *What makes you think I knew about this?[P] I thought slimes were all dumb![P] Present company included!*[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Offended] *Thanks a bunch...*[B][C]") ]])
                    fnCutscene([[ Append("Slime:[E|Blue] What are you guys whispering about?[P] Can I whisper too?[P] Is it a secret?[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Offended] We were just discussing...[P] if you're super-cute or ultra-cute![B][C]") ]])
                end
                fnCutscene([[ Append("Slime:[E|Blue] You're, like, so nice![B][C]") ]])
                fnCutscene([[ Append("Slime:[E|Blue] Oh, does that mean you've never seen our village?[B][C]") ]])
                fnCutscene([[ Append("Slime:[E|Blue] Want to see it?[BLOCK]") ]])

                --Decision script is this script. It must be surrounded by quotes.
                local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
                fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
                fnCutscene(" WD_SetProperty(\"Add Decision\", \"Sure\", " .. sDecisionScript .. ", \"Sure Slime\") ")
                fnCutscene(" WD_SetProperty(\"Add Decision\", \"No Thanks\",  " .. sDecisionScript .. ", \"No Thanks Slime\") ")
                fnCutsceneBlocker()
            
                --Set:
                VM_SetVar("Root/Variables/Chapter1/Scenes/iMetSlimeGuide", "N", 1.0)
            
            --Mei is anything else:
            else
                fnCutscene([[ Append("Slime:[E|Blue] Heeyyyyy...[P] you're a human, aren't you?[P] You smell like one![B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Surprise] Oh my goodness![B][C]") ]])
                if(bIsFlorentinaPresent == true) then
                    fnCutscene([[ Append("Florentina:[E|Surprise] You can talk?[B][C]") ]])
                end
                fnCutscene([[ Append("Slime:[E|Blue] Oh don't look so surprised![P] Your costume is, like, super awesome.[P] I'm impressed![B][C]") ]])
                if(bIsFlorentinaPresent == true) then
                    fnCutscene([[ Append("Mei:[E|Smirk] *Costume?*[B][C]") ]])
                    fnCutscene([[ Append("Florentina:[E|Happy] *Just roll with it...*[B][C]") ]])
                end
                fnCutscene([[ Append("Slime:[E|Blue] But I don't think the other slimes will understand -[P] they're kinda dense, if you know what I mean![B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Smirk] So ignoring the fact that you can talk...[B][C]") ]])
                fnCutscene([[ Append("Slime:[E|Blue] Ooooh![P] I want to show you our village but I don't want to get in trouble![B][C]") ]])
                fnCutscene([[ Append("Slime:[E|Blue] You should take off your costume![P] Go on, I won't peek.[P] I promise!") ]])
            
                --Set:
                VM_SetVar("Root/Variables/Chapter1/Scenes/iMetSlimeGuide", "N", 1.0)
            
            end
            
        --Repeat visits:
        else
            
            --Mei is a human:
            if(sMeiForm == "Human") then
                fnCutscene([[ Append("Slime:[E|Blue] Hey![P] Want to visit the slime village?[BLOCK]") ]])

                --Decision script is this script. It must be surrounded by quotes.
                local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
                fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
                fnCutscene(" WD_SetProperty(\"Add Decision\", \"Sure\", " .. sDecisionScript .. ", \"Sure Repeat\") ")
                fnCutscene(" WD_SetProperty(\"Add Decision\", \"No Thanks\",  " .. sDecisionScript .. ", \"No Thanks Repeat\") ")
                fnCutsceneBlocker()
            
            --Mei is a slime:
            elseif(sMeiForm == "Slime") then
                fnCutscene([[ Append("Slime:[E|Blue] Hi hi hi![P] Want to visit the slime village?[BLOCK]") ]])

                --Decision script is this script. It must be surrounded by quotes.
                local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
                fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
                fnCutscene(" WD_SetProperty(\"Add Decision\", \"Sure\", " .. sDecisionScript .. ", \"Sure Repeat\") ")
                fnCutscene(" WD_SetProperty(\"Add Decision\", \"No Thanks\",  " .. sDecisionScript .. ", \"No Thanks Repeat\") ")
                fnCutsceneBlocker()
            
            --Mei is anything else:
            else
                fnCutscene([[ Append("Slime:[E|Blue] Sorry, but you'll need to take off your cool costume if you want to visit the slime village!") ]])
            end
        end
    end
    
-- |[ ================================= Slime Guide Responses ================================== ]|
elseif(sTopicName == "Sure") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnStandardMajorDialogue(true)
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Slime", "Blue") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Well, okay I guess.[P] You seem all right.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] But no sudden moves.[B][C]") ]])
	fnCutscene([[ Append("Slime:[E|Blue] Sudden moves?[P] Have you never met a slime before?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Smirk] Well, no, actually.[B][C]") ]])
	fnCutscene([[ Append("Slime:[E|Blue] Oh...[B][C]") ]])
	fnCutscene([[ Append("Slime:[E|Blue] Well you're gonna love our cool village![P] Follow me!") ]])
	fnCutsceneBlocker()
	
	--Transition.
	local iSawSlimevilleIntro = VM_GetVar("Root/Variables/Chapter1/Scenes/iSawSlimevilleIntro", "N")
	if(iSawSlimevilleIntro == 0.0) then
		VM_SetVar("Root/Variables/Chapter1/Scenes/iSawSlimevilleIntro", "N", 1.0)
		fnCutscene([[ AL_BeginTransitionTo("EvermoonSlimeVillage", gsRoot .. "Chapter 1/Scenes/Slimeville_Entry/Scene_Begin.lua") ]])
	else
		fnCutscene([[ AL_BeginTransitionTo("EvermoonSlimeVillage", "FORCEPOS:22.0x39.5x0") ]])
		fnCutsceneBlocker()
		
		--Wait.
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Mei faces north.
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Face",  0, -1)
		DL_PopActiveObject()
	end
	
elseif(sTopicName == "Sure Slime") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnStandardMajorDialogue(true)
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Slime", "Blue") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Okay![P] Lead the way![B][C]") ]])
	fnCutscene([[ Append("Slime:[E|Blue] This is so totally awesome![P] Follow me!") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Transition.
	local iSawSlimevilleIntro = VM_GetVar("Root/Variables/Chapter1/Scenes/iSawSlimevilleIntro", "N")
	if(iSawSlimevilleIntro == 0.0) then
		VM_SetVar("Root/Variables/Chapter1/Scenes/iSawSlimevilleIntro", "N", 1.0)
		fnCutscene([[ AL_BeginTransitionTo("EvermoonSlimeVillage", gsRoot .. "Chapter 1/Scenes/Slimeville_Entry/Scene_Begin.lua") ]])
	else
		fnCutscene([[ AL_BeginTransitionTo("EvermoonSlimeVillage", "FORCEPOS:22.0x39.5x0") ]])
		fnCutsceneBlocker()
		
		--Wait.
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Mei faces north.
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Face",  0, -1)
		DL_PopActiveObject()
	end
	
elseif(sTopicName == "Sure Repeat") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnStandardMajorDialogue(true)
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Slime", "Blue") ]])
	fnCutscene([[ Append("Slime:[E|Blue] Okay![P] Follow me!") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Transition.
	local iSawSlimevilleIntro = VM_GetVar("Root/Variables/Chapter1/Scenes/iSawSlimevilleIntro", "N")
	if(iSawSlimevilleIntro == 0.0) then
		VM_SetVar("Root/Variables/Chapter1/Scenes/iSawSlimevilleIntro", "N", 1.0)
		fnCutscene([[ AL_BeginTransitionTo("EvermoonSlimeVillage", gsRoot .. "Chapter 1/Scenes/Slimeville_Entry/Scene_Begin.lua") ]])
	else
		fnCutscene([[ AL_BeginTransitionTo("EvermoonSlimeVillage", "FORCEPOS:22.0x39.5x0") ]])
		fnCutsceneBlocker()
		
		--Wait.
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Mei faces north.
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Face",  0, -1)
		DL_PopActiveObject()
	end
	
elseif(sTopicName == "No Thanks") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnStandardMajorDialogue(true)
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Slime", "Blue") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] (I smell a trap...)[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Smirk] No thanks, Miss Slime.[P] Maybe some other time?[B][C]") ]])
	fnCutscene([[ Append("Slime:[E|Blue] Ohmygosh that rhymed![P] You're so cool![B][C]") ]])
	fnCutscene([[ Append("Slime:[E|Blue] If you want to see it, I'll be here![P] Just holler!") ]])
	fnCutsceneBlocker()
	
elseif(sTopicName == "No Thanks Slime") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnStandardMajorDialogue(true)
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Slime", "Blue") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Well I'm kinda busy at the moment.[B][C]") ]])
	fnCutscene([[ Append("Slime:[E|Blue] If you want to see it, I'll be here![P] Just holler!") ]])
	fnCutsceneBlocker()
	
elseif(sTopicName == "No Thanks Repeat") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnStandardMajorDialogue(true)
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Slime", "Blue") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Well I'm kinda busy at the moment.[B][C]") ]])
	fnCutscene([[ Append("Slime:[E|Blue] If you want to see it, I'll be here![P] Just holler!") ]])
	fnCutsceneBlocker()
end
