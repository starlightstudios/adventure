-- |[ ============================= Karina and Claudia's Dialogue ============================== ]|
--Dialogue subfile for Karina / Claudia, as theirs is a lot more complex than the standard NPCs.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
    
    -- |[Variables]|
    local sMeiForm                = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
    local iKarinaSawGravemarker   = VM_GetVar("Root/Variables/Chapter1/Scenes/iKarinaSawGravemarker", "N")
    local iClaudiaSawGravemarker  = VM_GetVar("Root/Variables/Chapter1/Scenes/iClaudiaSawGravemarker", "N")
    local iHasFoundOutlandAcolyte = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasFoundOutlandAcolyte", "N")
    local bIsFlorentinaPresent    = AL_GetProperty("Is Character Following", "Florentina")
    local iSavedClaudia           = VM_GetVar("Root/Variables/Chapter1/Scenes/iSavedClaudia", "N")
    local iHasGravemarkerForm     = VM_GetVar("Root/Variables/Global/Mei/iHasGravemarkerForm", "N")
    
    -- |[Dialogue Setup]|
    --This is always a major dialogue.
    fnStandardMajorDialogue()
    
    --Karina takes slot 4.
    if(iSavedClaudia == 0.0) then
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Karina", "Neutral") ]])
    
    --If Claudia was rescued, they move a bit.
    else
        fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Karina", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Claudia", "Neutral") ]])
    end
    
    -- |[ ================== Gravemarker Cases =================== ]|
    --If Mei has Gravemarker form, the dialogue changes a lot. This is only the case if Claudia is present.
    if(iHasGravemarkerForm == 1.0 and iSavedClaudia == 1.0) then
        
        -- |[First Meeting]|
        --If we've rescued Claudia but not met Karina before:
        if(iHasFoundOutlandAcolyte == 0.0) then
        
            --Flag.
            VM_SetVar("Root/Variables/Chapter1/Scenes/iHasFoundOutlandAcolyte", "N", 2.0)
        
            -- |[Solo Case]|
            --If Florentina is not present:
            if(bIsFlorentinaPresent == false) then
                fnCutscene([[ Append("Mei:[E|Neutral] Claudia?[P] Is that you?[B][C]") ]])
                
                --Claudia knows about Gravemarker Mei, and Mei is not in Gravemarker form:
                if(iClaudiaSawGravemarker == 1.0 and sMeiForm ~= "Gravemarker") then
                    VM_SetVar("Root/Variables/Chapter1/Scenes/iClaudiaSawGravemarker", "N", 1.0)
                    VM_SetVar("Root/Variables/Chapter1/Scenes/iKarinaSawGravemarker", "N", 1.0)
                    fnCutscene([[ Append("Claudia: Tool.[P] You have returned to me.[B][C]") ]])
                    fnCutscene([[ Append("Karina: Tool?[P] But...[B][C]") ]])
                    fnCutscene([[ Append("Claudia: This one has a divine runestone that allows her to change her shape.[P] Nonetheless, she is one of our tools.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] That is true.[B][C]") ]])
                    fnCutscene([[ Append("Karina: I see.[P] Is this the one you mentioned rescued you?[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Indeed.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Tool, you must behave normally and speak normally while in my presence.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: We do not want to bring any more attention than we already do.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] Yes, sister.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Good.[P] You are rewarded.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Blush] (Aaaaaaahhhhhhh... mmmmm...)[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Blush] Thank you, sister.[B][C]") ]])
                    fnCutscene([[ Append("Karina: I had my doubts and no longer do.[P] Truly a miracle is upon us![B][C]") ]])
                    fnCutscene([[ Append("Claudia: We have no further use for you, tool.[P] Return to your quest.[P] You are dimissed.[B][C]") ]])
                    fnCutscene([[ Append("Karina: Good luck, Mei![B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] Thank you, sisters.") ]])
        
                --Claudia knows about Gravemarker Mei, and Mei is in Gravemarker form:
                elseif(iClaudiaSawGravemarker == 1.0 and sMeiForm == "Gravemarker") then
                    VM_SetVar("Root/Variables/Chapter1/Scenes/iClaudiaSawGravemarker", "N", 1.0)
                    VM_SetVar("Root/Variables/Chapter1/Scenes/iKarinaSawGravemarker", "N", 1.0)
                    fnCutscene([[ Append("Claudia: Tool.[P] You have returned to me.[B][C]") ]])
                    fnCutscene([[ Append("Karina: Tool?[P] But...[B][C]") ]])
                    fnCutscene([[ Append("Claudia: This one has a divine runestone that allows her to change her shape.[P] Nonetheless, she is one of our tools.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] That is true.[B][C]") ]])
                    fnCutscene([[ Append("Karina: I see.[P] Is this the one you mentioned rescued you?[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Indeed.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Tool, you must behave normally and speak normally while in my presence.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: We do not want to bring any more attention than we already do.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] Yes, sister.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Good.[P] You are rewarded.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Blush] (Aaaaaaahhhhhhh... mmmmm...)[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Blush] Thank you, sister.[B][C]") ]])
                    fnCutscene([[ Append("Karina: I had my doubts and no longer do.[P] Truly a miracle is upon us![B][C]") ]])
                    fnCutscene([[ Append("Claudia: We have no further use for you, tool.[P] Return to your quest.[P] You are dimissed.[B][C]") ]])
                    fnCutscene([[ Append("Karina: Good luck, Mei![B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] Thank you, sisters.") ]])
                
                --Claudia has not met Gravemarker Mei, and Mei is in Gravemarker form:
                elseif(iClaudiaSawGravemarker == 0.0 and sMeiForm == "Gravemarker") then
                    VM_SetVar("Root/Variables/Chapter1/Scenes/iMeiHasSubmittedGravemarker", "N", 1.0)
                    VM_SetVar("Root/Variables/Chapter1/Scenes/iClaudiaSawGravemarker", "N", 1.0)
                    VM_SetVar("Root/Variables/Chapter1/Scenes/iKarinaSawGravemarker", "N", 1.0)
                    fnCutscene([[ Append("Claudia: Tool.[P] You have joined the light.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Offended] In body, not mind.[P] I'm not your tool, angel.[B][C]") ]])
                    fnCutscene([[ Append("Karina: Sister Claudia, she knows![B][C]") ]])
                    fnCutscene([[ Append("Claudia: Of course.[P] The light can sense the light.[B][C]") ]])
                    fnCutscene([[ Append("Karina: But how?[B][C]") ]])
                    fnCutscene([[ Append("Claudia: She is not of this world, and bears a runestone with a destiny on it.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: I see no need to force her to obey.[P] Her cause is already divine.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Offended] Maybe I ought to stay on Pandemonium just to spite you.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: I would tolerate independence, but not defiance.[P] Tool.[P] Submit.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Sad] Huh?[P] What?[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Sad] Voices...[P] the light?[P] No, no![P] Get away![B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Cry] Nnnnggghhhh...[P] Impure...[P] the runestone...[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Cry] I...[P] This one...[P] This one will...[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] Follow orders.[P] This one will follow orders, sister.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Her body and the light flowing through it are greater influences than the runestone, you see.[B][C]") ]])
                    fnCutscene([[ Append("Karina: Sister Claudia, is this right?[P] She has a divine artifact and a divine mission.[P] Bending her to your will...[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] This one must find a way to return to Earth.[P] That is its divine purpose.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] Sister Claudia is only reinforcing that purpose, not contradicting it.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: You see?[P] From her own mouth.[P] You will find a way to return to Earth, tool.[B][C]") ]])
                    fnCutscene([[ Append("Karina: I apologize for doubting you, sister.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] May this one render any further aid, sisters?[B][C]") ]])
                    fnCutscene([[ Append("Claudia: No.[P] You are dismissed.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] Yes, sister.") ]])
        
                --Claudia has not met Gravemarker Mei, and Mei is not in Gravemarker form:
                elseif(iClaudiaSawGravemarker == 0.0 and sMeiForm ~= "Gravemarker") then
                    fnCutscene([[ Append("Mei:[E|Neutral] Claudia?[P] Is that you?[B][C]") ]])
                    fnCutscene([[ Append("Claudia: It is not by chance that we again meet, my child.[P] The divine lays our paths to cross this way.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: This is Sister Karina.[P] She is my assistant.[B][C]") ]])
                    fnCutscene([[ Append("Karina: Pleasure to meet you![P] You must be the one who saved Sister Claudia![B][C]") ]])
                    fnCutscene([[ Append("Karina: If there's anything I can do for you, let me know![B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] I was just surprised to see you out here.[P] Is everything going well?[B][C]") ]])
                    fnCutscene([[ Append("Claudia: We begin the long path to redemption.[P] We must rebuild our convent and finish our holy task.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] You're pretty driven, I'm sure you'll be okay.[B][C]") ]])
                    fnCutscene([[ Append("Karina: The divine will watch over us, and you.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Indeed.[P] I wish you luck on your path.") ]])
                end
        
            -- |[Florentina Case]|
            --Florentina is around.
            else
                fnCutscene([[ Append("Mei:[E|Neutral] Claudia?[P] Is that you?[B][C]") ]])
                
                --Claudia knows about Gravemarker Mei, and Mei is not in Gravemarker form:
                if(iClaudiaSawGravemarker == 1.0) then
                    fnCutscene([[ Append("Claudia: (TOOL.[P] DO NOT REVEAL YOURSELF TO THE ALRAUNE.)[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] (Yes, sister.)[B][C]") ]])
                    fnCutscene([[ Append("Claudia: It is not by chance that we again meet, my child.[P] The divine lays our paths to cross this way.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: This is Sister Karina.[P] She is my assistant.[B][C]") ]])
                    fnCutscene([[ Append("Florentina:[E|Neutral] You know her, Mei?[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] In fact, I rescued her from the sewers.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: The divine sent her, and has brought her back to me.[B][C]") ]])
                    fnCutscene([[ Append("Florentina:[E|Offended] Yeah sure, buddy.[P] It's all part of the plan.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Smirk] Oh give it a rest, Florentina.[P] She's really sweet![B][C]") ]])
                    fnCutscene([[ Append("Florentina:[E|Neutral] That's not a solution, you just re-described the problem.[B][C]") ]])
                    fnCutscene([[ Append("Karina: Sister Claudia says we're to help you in any way we can!") ]])
                    
                --Claudia has not met Gravemarker Mei, and Mei is in Gravemarker form:
                elseif(iClaudiaSawGravemarker == 0.0 and sMeiForm == "Gravemarker") then
                    VM_SetVar("Root/Variables/Chapter1/Scenes/iMeiHasSubmittedGravemarker", "N", 1.0)
                    VM_SetVar("Root/Variables/Chapter1/Scenes/iClaudiaSawGravemarker", "N", 1.0)
                    VM_SetVar("Root/Variables/Chapter1/Scenes/iKarinaSawGravemarker", "N", 1.0)
                    fnCutscene([[ Append("Claudia: (TOOL. DO NOT REVEAL YOURSELF TO THE ALRAUNE.)[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] (What the -[P] how are you?)[P][CLEAR]") ]])
                    fnCutscene([[ Append("Claudia: (SILENCE.)[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Offended] (You can't order me around like the other gravemarkers![P] I'm not your tool!)[B][C]") ]])
                    fnCutscene([[ Append("Claudia: (LET THE LIGHT FLOW OVER YOU.)[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Sad] (Wh-[P]what?[P] No, not again![P] No!)[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Cry] (They're in my head![P] The voices![P] No![P] Leave me alone!)[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Cry] (...)[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Cry] (...)[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] (This one...[P] this one will follow orders.)[B][C]") ]])
                    fnCutscene([[ Append("Claudia: (TOOL.[P] DO NOT REVEAL YOURSELF TO THE ALRAUNE.)[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] (Yes, sister.)[B][C]") ]])
                    fnCutscene([[ Append("Florentina:[E|Neutral] Mei?[P] You okay?[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] Oh yeah, peachy.[P] Just got a little homesick for a second there.[B][C]") ]])
                    fnCutscene([[ Append("Florentina:[E|Confused] Is that so?[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Hello.[P] I am Claudia, I believe we've met.[P] Mei rescued me from a prison beneath the old manor east of here.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Smirk] She told me about the rilmani and her journal.[P] It just kind of hit me that I'm still stuck here.[B][C]") ]])
                    fnCutscene([[ Append("Florentina:[E|Confused] Sure.[P] I believe you.[B][C]") ]])
                    fnCutscene([[ Append("Florentina:[E|Neutral] We'd better make a priority of getting you far away from Claudia, then.[P] Back home, right?[B][C]") ]])
                    fnCutscene([[ Append("Claudia: If the Doves can do anything to help, please let us know.[B][C]") ]])
                    fnCutscene([[ Append("Karina: Yeah[P]! We always help those in need![B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Smirk] See?[P] They're on the up and up.[B][C]") ]])
                    fnCutscene([[ Append("Florentina:[E|Confused] (What am I going to do...)") ]])
                
                --Claudia has not met Gravemarker Mei, and Mei is not in Gravemarker form:
                elseif(iClaudiaSawGravemarker == 0.0 and sMeiForm ~= "Gravemarker") then
                    fnCutscene([[ Append("Mei:[E|Neutral] Claudia?[P] Is that you?[B][C]") ]])
                    fnCutscene([[ Append("Claudia: It is not by chance that we again meet, my child.[P] The divine lays our paths to cross this way.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: This is Sister Karina.[P] She is my assistant.[B][C]") ]])
                    fnCutscene([[ Append("Karina: Pleasure to meet you![P] You must be the one who saved Sister Claudia![B][C]") ]])
                    fnCutscene([[ Append("Karina: If there's anything I can do for you, let me know![B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] I was just surprised to see you out here.[P] Is everything going well?[B][C]") ]])
                    fnCutscene([[ Append("Claudia: We begin the long path to redemption.[P] We must rebuild our convent and finish our holy task.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] You're pretty driven, I'm sure you'll be okay.[B][C]") ]])
                    fnCutscene([[ Append("Karina: The divine will watch over us, and you.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Indeed.[P] I wish you luck on your path.") ]])
                end
            end
        
        -- |[Repeat Cases]|
        else
        
            -- |[Solo Case]|
            --If Florentina is not present:
            if(bIsFlorentinaPresent == false) then
                
                --Claudia knows about Gravemarker Mei, and Mei is not in Gravemarker form:
                if(iClaudiaSawGravemarker == 1.0) then
                    fnCutscene([[ Append("Claudia: Tool.[P] Do you require anything further?[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] May you bless this one with your wisdom regarding its mission.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Indeed.[P] You must seek out the rilmani.[P] Locate my journal in the Quantir manor, and then search for other rilmani symbols.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: My journal will help translating them.[P] After that, you will be on your own.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Blush] Thank you for your blessing, sister.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Tool, you are dismissed.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Blush] Yes, sister.") ]])
        
                --Claudia has not met Gravemarker Mei, and Mei is in Gravemarker form:
                elseif(iClaudiaSawGravemarker == 0.0 and sMeiForm == "Gravemarker") then
                    VM_SetVar("Root/Variables/Chapter1/Scenes/iMeiHasSubmittedGravemarker", "N", 1.0)
                    VM_SetVar("Root/Variables/Chapter1/Scenes/iClaudiaSawGravemarker", "N", 1.0)
                    VM_SetVar("Root/Variables/Chapter1/Scenes/iKarinaSawGravemarker", "N", 1.0)
                    fnCutscene([[ Append("Claudia: Tool.[P] You have joined the light.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Offended] In body, not mind.[P] I'm not your tool, angel.[B][C]") ]])
                    fnCutscene([[ Append("Karina: Sister Claudia, she knows![B][C]") ]])
                    fnCutscene([[ Append("Claudia: Of course.[P] The light can sense the light.[B][C]") ]])
                    fnCutscene([[ Append("Karina: But how?[B][C]") ]])
                    fnCutscene([[ Append("Claudia: She is not of this world, and bears a runestone with a destiny on it.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: I see no need to force her to obey.[P] Her cause is already divine.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Offended] Maybe I ought to stay on Pandemonium just to spite you.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: I would tolerate independence, but not defiance.[P] Tool.[P] Submit.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Sad] Huh?[P] What?[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Sad] Voices...[P] the light?[P] No, no![P] Get away![B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Cry] Nnnnggghhhh...[P] Impure...[P] the runestone...[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Cry] I...[P] This one...[P] This one will...[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] Follow orders.[P] This one will follow orders, sister.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Her body and the light flowing through it are greater influences than the runestone, you see.[B][C]") ]])
                    fnCutscene([[ Append("Karina: Sister Claudia, is this right?[P] She has a divine artifact and a divine mission.[P] Bending her to your will...[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] This one must find a way to return to Earth.[P] That is its divine purpose.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] Sister Claudia is only reinforcing that purpose, not contradicting it.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: You see?[P] From her own mouth.[P] You will find a way to return to Earth, tool.[B][C]") ]])
                    fnCutscene([[ Append("Karina: I apologize for doubting you, sister.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] May this one render any further aid, sisters?[B][C]") ]])
                    fnCutscene([[ Append("Claudia: No.[P] You are dismissed.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] Yes, sister.") ]])
        
                --Claudia has not met Gravemarker Mei, and Mei is not in Gravemarker form:
                elseif(iClaudiaSawGravemarker == 0.0 and sMeiForm ~= "Gravemarker") then
                    fnCutscene([[ Append("Claudia: Was there anything else I could do to help you, Mei?[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] Can you give me a rundown on what I should do next?[B][C]") ]])
                    fnCutscene([[ Append("Claudia: My journal has the writings you will need to translate rilmani runes.[P] Find it.[P] I left it in the manor.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Once you've found it, look for rilmani artifacts.[P] Mirrors, glass items, fine silverware.[P] That sort of thing.[B][C]") ]])
                    fnCutscene([[ Append("Claudia: The rilmani tend towards these things, and if you can find their runes written on such an item, that will surely aid you.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] Got it.[P] Thanks![B][C]") ]])
                    fnCutscene([[ Append("Claudia: The divine blesses you, Mei.[P] Good luck.[B][C]") ]])
                    fnCutscene([[ Append("Karina: And if you see a rilmani, write down what they look like![P] We have some...[P] theories...[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Indeed.[P] But do not feel obligated.[P] You have already done much.") ]])
                end
        
            -- |[Florentina Case]|
            --Florentina is around.
            else
                
                --Claudia knows about Gravemarker Mei, and Mei is in any form:
                if(iClaudiaSawGravemarker == 1.0) then
                    fnCutscene([[ Append("Claudia: (TOOL.[P] DO NOT REVEAL YOURSELF TO THE ALRAUNE.)[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] (Yes, sister.)[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Tidings, Mei.[P] How may we aid you?[B][C]") ]])
            
                    --Mei is in bee form:
                    if(sMeiForm == "Bee") then
                        fnCutscene([[ Append("Karina: Oh, could I trouble you to answer some questions about being a bee?[B][C]") ]])
                        fnCutscene([[ Append("Claudia: (TOOL.[P] ANSWER HER.)[B][C]") ]])
                        fnCutscene([[ Append("Mei:[E|Neutral] (Yes, sister.)[B][C]") ]])
                        fnCutscene([[ Append("Florentina:[E|Neutral] We're busy.[B][C]") ]])
                        fnCutscene([[ Append("Mei:[E|Neutral] It's all right.[P] It's just a few questions.[B][C]") ]])
                        WD_SetProperty("Unlock Topic", "Karina_Claudia", 1)
                        fnCutscene([[ WD_SetProperty("Activate Topics After Dialogue", "Karina") ]])
            
                    --All other forms.
                    else
                        fnCutscene([[ Append("Mei:[E|Neutral] Can you give me a rundown on what I should do next?[B][C]") ]])
                        fnCutscene([[ Append("Claudia: My journal has the writings you will need to translate rilmani runes.[P] Find it.[P] I left it in the manor.[B][C]") ]])
                        fnCutscene([[ Append("Claudia: Once you've found it, look for rilmani artifacts.[P] Mirrors, glass items, fine silverware.[P] That sort of thing.[B][C]") ]])
                        fnCutscene([[ Append("Claudia: The rilmani tend towards these things, and if you can find their runes written on such an item, that will surely aid you.[B][C]") ]])
                        fnCutscene([[ Append("Mei:[E|Neutral] Got it.[P] Thanks![B][C]") ]])
                        fnCutscene([[ Append("Claudia: The divine blesses you, Mei.[P] Good luck.[B][C]") ]])
                        fnCutscene([[ Append("Karina: And if you see a rilmani, write down what they look like![P] We have some...[P] theories...[B][C]") ]])
                        fnCutscene([[ Append("Claudia: Indeed.[P] But do not feel obligated.[P] You have already done much.[B][C]") ]])
                        fnCutscene([[ Append("Florentina:[E|Neutral] Let's get going.") ]])
                    end
            
                --Claudia does not know about Gravemarker Mei, and Mei is in Gravemarker form:
                elseif(iClaudiaSawGravemarker == 0.0 and sMeiForm == "Gravemarker") then
                    VM_SetVar("Root/Variables/Chapter1/Scenes/iMeiHasSubmittedGravemarker", "N", 1.0)
                    VM_SetVar("Root/Variables/Chapter1/Scenes/iClaudiaSawGravemarker", "N", 1.0)
                    VM_SetVar("Root/Variables/Chapter1/Scenes/iKarinaSawGravemarker", "N", 1.0)
                    fnCutscene([[ Append("Claudia: (TOOL.[P] DO NOT REVEAL YOURSELF TO THE ALRAUNE.)[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] (What the -[P] how are you?)[P][CLEAR]") ]])
                    fnCutscene([[ Append("Claudia: (SILENCE.)[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Offended] (You can't order me around like the other gravemarkers![P] I'm not your tool!)[B][C]") ]])
                    fnCutscene([[ Append("Claudia: (LET THE LIGHT FLOW OVER YOU.)[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Sad] (Wh-[P]what?[P] No, not again![P] No!)[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Cry] (They're in my head![P] The voices![P] No![P] Leave me alone!)[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Cry] (...)[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Cry] (...)[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] (This one...[P] this one will follow orders.)[B][C]") ]])
                    fnCutscene([[ Append("Claudia: (TOOL.[P] DO NOT REVEAL YOURSELF TO THE ALRAUNE.)[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] (Yes, sister.)[B][C]") ]])
                    fnCutscene([[ Append("Florentina:[E|Neutral] Mei?[P] You okay?[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] Oh yeah, peachy.[P] Just got a little homesick for a second there.[B][C]") ]])
                    fnCutscene([[ Append("Florentina:[E|Confused] Is that so?[B][C]") ]])
                    fnCutscene([[ Append("Claudia: Hello.[P] I am Claudia, I believe we've met.[P] Mei rescued me from a prison beneath the old manor east of here.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Smirk] She told me about the rilmani and her journal.[P] It just kind of hit me that I'm still stuck here.[B][C]") ]])
                    fnCutscene([[ Append("Florentina:[E|Confused] Sure.[P] I believe you.[B][C]") ]])
                    fnCutscene([[ Append("Florentina:[E|Neutral] We'd better make a priority of getting you far away from Claudia, then.[P] Back home, right?[B][C]") ]])
                    fnCutscene([[ Append("Claudia: If the Doves can do anything to help, please let us know.[B][C]") ]])
                    fnCutscene([[ Append("Karina: Yeah[P]! We always help those in need![B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Smirk] See?[P] They're on the up and up.[B][C]") ]])
                    fnCutscene([[ Append("Florentina:[E|Confused] (What am I going to do...)") ]])
                
                --Claudia does not know about Gravemarker Mei and Mei is in any other form:
                else
                    fnCutscene([[ Append("Claudia: Tidings, Mei.[P] How may we aid you?[B][C]") ]])
            
                    --Mei is in bee form:
                    if(sMeiForm == "Bee") then
                        fnCutscene([[ Append("Karina: Oh, could I trouble you to answer some questions about being a bee?[B][C]") ]])
                        fnCutscene([[ Append("Florentina:[E|Neutral] We're busy.[B][C]") ]])
                        fnCutscene([[ Append("Mei:[E|Neutral] Heh, are we?[P] I'm sure a question or two wouldn't be a problem.[B][C]") ]])
                        fnCutscene([[ Append("Florentina:[E|Confused] If you say so.[B][C]") ]])
                        WD_SetProperty("Unlock Topic", "Karina_Claudia", 1)
                        fnCutscene([[ WD_SetProperty("Activate Topics After Dialogue", "Karina") ]])
            
                    --All other forms.
                    else
                        fnCutscene([[ Append("Mei:[E|Neutral] Can you give me a rundown on what I should do next?[B][C]") ]])
                        fnCutscene([[ Append("Claudia: My journal has the writings you will need to translate rilmani runes.[P] Find it.[P] I left it in the manor.[B][C]") ]])
                        fnCutscene([[ Append("Claudia: Once you've found it, look for rilmani artifacts.[P] Mirrors, glass items, fine silverware.[P] That sort of thing.[B][C]") ]])
                        fnCutscene([[ Append("Claudia: The rilmani tend towards these things, and if you can find their runes written on such an item, that will surely aid you.[B][C]") ]])
                        fnCutscene([[ Append("Mei:[E|Neutral] Got it.[P] Thanks![B][C]") ]])
                        fnCutscene([[ Append("Claudia: The divine blesses you, Mei.[P] Good luck.[B][C]") ]])
                        fnCutscene([[ Append("Karina: And if you see a rilmani, write down what they look like![P] We have some...[P] theories...[B][C]") ]])
                        fnCutscene([[ Append("Claudia: Indeed.[P] But do not feel obligated.[P] You have already done much.[B][C]") ]])
                        fnCutscene([[ Append("Florentina:[E|Neutral] Let's get going.") ]])
                    end
                end
            end
        end
        
        --Stop here.
        return
        
    -- |[ ================ Non-Gravemarker Cases ================= ]|
    --If Mei either has not rescued Claudia, or does not have gravemarker form, display these. These are the 'normal' cases.
    else
        --If we've rescued Claudia but not met Karina before:
        if(iSavedClaudia == 1.0 and iHasFoundOutlandAcolyte == 0.0) then
            fnCutscene([[ Append("Mei:[E|Neutral] Claudia?[P] Is that you?[B][C]") ]])
            fnCutscene([[ Append("Claudia: It is not by chance that we again meet, my child.[P] The divine lays our paths to cross this way.[B][C]") ]])
            fnCutscene([[ Append("Claudia: This is Sister Karina.[P] She is my assistant.[B][C]") ]])
            fnCutscene([[ Append("Karina: Pleasure to meet you![P] You must be the one who saved Sister Claudia![B][C]") ]])
            fnCutscene([[ Append("Karina: If there's anything I can do for you, let me know![B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] I was just surprised to see you out here.[P] Is everything going well?[B][C]") ]])
            fnCutscene([[ Append("Claudia: We begin the long path to redemption.[P] We must rebuild our convent and finish our holy task.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] You're pretty driven, I'm sure you'll be okay.[B][C]") ]])
            fnCutscene([[ Append("Karina: The divine will watch over us, and you.[B][C]") ]])
            fnCutscene([[ Append("Claudia: Indeed.[P] I wish you luck on your path.") ]])
            return
        end

        --If Mei is a Gravemarker, and has not rescued Claudia:
        if(sMeiForm == "Gravemarker") then
            
            --Hasn't met Karina before:
            if(iHasFoundOutlandAcolyte == 0.0) then
                fnCutscene([[ Append("Karina: Oh my![P] Hello there![B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Hi![B][C]") ]])
                fnCutscene([[ Append("Karina: (She looks just like a sentinel...[P] but has a head?)[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Is something the matter?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Smirk] I get that not a lot of people have seen the kind of monstergirl I am.[B][C]") ]])
                fnCutscene([[ Append("Karina: (But is she?[P] What could have happened?)[B][C]") ]])
                fnCutscene([[ Append("Karina: Um, sorry.[P] It's impolite to stare.[B][C]") ]])
                fnCutscene([[ Append("Karina: Go-good luck.[P] Thanks.[P] Okay.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] (Well that was awkward.)") ]])

            --Has met Karina before:
            else
                fnCutscene([[ Append("Karina: Oh my![P] Hello there![B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Hi![B][C]") ]])
                fnCutscene([[ Append("Karina: (She looks just like a sentinel...[P] but has a head?[P] And haven't I seen her face before?)[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Is something the matter?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Smirk] I get that not a lot of people have seen the kind of monstergirl I am.[B][C]") ]])
                fnCutscene([[ Append("Karina: (But is she?[P] What could have happened?)[B][C]") ]])
                fnCutscene([[ Append("Karina: Um, sorry.[P] It's impolite to stare.[B][C]") ]])
                fnCutscene([[ Append("Karina: Go-[P]good luck.[P] Thanks.[P] Okay.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] (Well that was awkward.)") ]])
            end
            return
        end

        --If we've rescued Claudia and have met Karina before, but don't have any bee stuff:
        if(iSavedClaudia == 1.0 and iHasFoundOutlandAcolyte == 2.0) then
        
            --If Mei is not in Bee form:
            if(sMeiForm ~= "Bee") then
                fnCutscene([[ Append("Claudia: Hello again, my child.[P] We will pray for your success.[B][C]") ]])
                fnCutscene([[ Append("Karina: Yeah![P] Blessings be upon you!") ]])
            
            --Mei is in bee form:
            else
                fnCutscene([[ Append("Karina: Mei![P] Could you answer a few questions about being a bee?[B][C]") ]])
                fnCutscene([[ Append("Claudia: It would do much to aid our research.[P] I only lament that the Doves call upon you again to aid us...[B][C]") ]])
                WD_SetProperty("Unlock Topic", "Karina_Claudia", 1)
                fnCutscene([[ WD_SetProperty("Activate Topics After Dialogue", "Karina") ]])
            end
            
        --If we've rescued Claudia and Karina knows about Mei being a bee. Ostensibly. This is bugged, so a guard has been put in place. Mei must be a bee.
        elseif(iSavedClaudia == 1.0 and iHasFoundOutlandAcolyte == 1.0) then
        
            --If Mei is not in Bee form:
            if(sMeiForm ~= "Bee") then
                fnCutscene([[ Append("Claudia: Hello again, my child.[P] We will pray for your success.[B][C]") ]])
                fnCutscene([[ Append("Karina: Yeah![P] Blessings be upon you!") ]])
            
            --Mei is in bee form:
            else
                fnCutscene([[ Append("Karina: Mei![P] Could you answer a few questions about being a bee?[B][C]") ]])
                fnCutscene([[ Append("Claudia: It would do much to aid our research.[P] I only lament that the Doves call upon you again to aid us...[B][C]") ]])
                WD_SetProperty("Unlock Topic", "Karina_Claudia", 1)
                fnCutscene([[ WD_SetProperty("Activate Topics After Dialogue", "Karina") ]])
            end
        
        --If we haven't met Karina yet, and Florentina is not present.
        elseif(iHasFoundOutlandAcolyte == 0.0 and bIsFlorentinaPresent == false) then
            
            --Flag.
            VM_SetVar("Root/Variables/Chapter1/Scenes/iHasFoundOutlandAcolyte", "N", 2.0)
        
            --Scene.
            fnCutscene([[ Append("Mei:[E|Neutral] Excuse me, but what are you looking at?[B][C]") ]])
            fnCutscene([[ Append("Lady: Oh, hello![P] You must be new around here.[P] There's a bee hive across the river there, you can see the top of it from here.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Surprise] Are you worried about the bees?[B][C]") ]])
            fnCutscene([[ Append("Lady: Perish the thought![P] I'm actually researching them.[P] This spot is a pretty good vantage point.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] Don't let me stop you, then.[P] Good luck.[B][C]") ]])
            fnCutscene([[ Append("Lady: Thanks!") ]])

        --If we haven't met Karina yet, but have already spoken to her once, the dialogue changes slightly. Doesn't unlock topics mode.
        elseif(iHasFoundOutlandAcolyte == 2.0 and bIsFlorentinaPresent == false and sMeiForm ~= "Bee") then
        
            fnCutscene([[ Append("Lady: I've been watching those bees for a few weeks now.[P] They seem to be pretty agitated by something...") ]])

        --If Mei is a bee but we don't have Florentina present.
        elseif(iHasFoundOutlandAcolyte == 2.0 and bIsFlorentinaPresent == false and sMeiForm == "Bee") then
        
            fnCutscene([[ Append("Karina: Have you reconsidered my offer?[P] Want to give me an interview?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] The hive is currently considering it.[P] I will let you know we reach consensus.") ]])

        --If we haven't met Karina yet, and Florentina is present.
        elseif(iHasFoundOutlandAcolyte == 0.0 and bIsFlorentinaPresent == true) then
            
            --Flag.
            VM_SetVar("Root/Variables/Chapter1/Scenes/iHasFoundOutlandAcolyte", "N", 1.0)
        
            --Scene.
            fnCutscene([[ Append("Mei:[E|Neutral] Excuse me, but what are you looking at?[B][C]") ]])
            fnCutscene([[ Append("Lady: Oh, hello![P] You must be new around here.[P] There's a bee hive across the river there, you can see the top of it from here.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] Wait, Mei.[P] I think this is the one we're looking for.[B][C]") ]])
            fnCutscene([[ Append("Lady: Were you looking for me?[P] I assure you, my convent is completely peaceful.[P] We'll compensate you for any damages![B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Happy] Oh yeah, this is her.[P] Are you with Sister Claudia's gang?[B][C]") ]])
            fnCutscene([[ Append("Lady: Yes.[P] My name is Sister Karina.[P] I apologize for any - [B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] Actually we're just looking for Claudia.[P] We heard she came this way.[B][C]") ]])
            fnCutscene([[ Append("Karina: Please, bounty hunters are unnecessary![B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Confused] *Sheesh, these guys must get up to all sorts of things.*[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] Can you just tell us where Claudia is?[P] We need her help with getting Mei here back to Earth.[B][C]") ]])
            fnCutscene([[ Append("Karina: Earth?[P] Never heard of it, but Sister Claudia probably has.[P] She has knowledge granted by the divine, you see.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Confused] Yeah, sure she does.[B][C]") ]])
            fnCutscene([[ Append("Karina: Unfortunately she is not here.[P] She took most of the convent to Quantir in search of a rare species of partirhuman, you see.[B][C]") ]])
            fnCutscene([[ Append("Karina: She was to have returned by now, actually.[P] I hope she's all right.[P] She tends to get into trouble...[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] I suppose we're going out to Quantir, then.[P] Thanks for the help, Karina.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] That's east of here.[P] Lead the way.") ]])
            WD_SetProperty("Unlock Topic", "Partirhuman", 1)

            --Clear the flag on Next Move.
            WD_SetProperty("Clear Topic Read", "NextMove")

        --If we met Karina previously, and Florentina is now present:
        elseif(iHasFoundOutlandAcolyte == 2.0 and bIsFlorentinaPresent == true) then
            
            --Flag.
            VM_SetVar("Root/Variables/Chapter1/Scenes/iHasFoundOutlandAcolyte", "N", 1.0)
        
            --Scene.
            fnCutscene([[ Append("Florentina:[E|Neutral] I believe this is the one we're looking for.[B][C]") ]])
            fnCutscene([[ Append("Lady: Hello again![B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Happy] I see you two are acquainted.[P] Good.[P] Then you'll have no problem telling us where Sister Claudia is.[B][C]") ]])
            fnCutscene([[ Append("Lady: Straight to the point, are you?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Happy] You'll have to excuse her.[P] My name is Mei, and this is my friend Florentina.[P] We're currently looking for someone who might know about Earth, or what this runestone is.[B][C]") ]])
            fnCutscene([[ Append("Lady: Well, then![P] I am Sister Karina, and you've come to the right place![B][C]") ]])
            fnCutscene([[ Append("Karina: Unfortunately, Sister Claudia is not here.[P] She's gone over the hills to Quantir to look for a rare species of partirhuman.[B][C]") ]])
            fnCutscene([[ Append("Karina: I'm afraid she's been away longer than expected.[P] I haven't heard from the convent at all, actually.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] Great.[P] Let's go.[B][C]") ]])
            fnCutscene([[ Append("Karina: In a hurry, I see![P] Do be careful.[P] I earnestly hope they found something interesting, because the alternative is much worse.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Happy] We'll be on guard.[P] Thanks for your help.") ]])
            WD_SetProperty("Unlock Topic", "Partirhuman", 1)

            --Clear the flag on Next Move.
            WD_SetProperty("Clear Topic Read", "NextMove")

        --If we have met Karina and Florentina informed her of Claudia's goals, this plays:
        elseif(iHasFoundOutlandAcolyte == 1.0 and sMeiForm ~= "Bee") then
            fnCutscene([[ Append("Karina: Hello![P] Can I help you with something else?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Happy] You've already helped quite a bit.[B][C]") ]])
            fnCutscene([[ Append("Karina: If there's anything else the Heavenly Doves convent can do for you, please let us know.[B][C]") ]])
            fnCutscene([[ Append("Karina: Perhaps you would like a pamphlet...[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Surprise] No![P] That's quite all right![P] Come on, Mei!") ]])

        --If we have met Karina and Mei is currently a Bee:
        else
            fnCutscene([[ Append("Karina: Mei![P] Could you perhaps answer a few questions about what it's like being a bee?[B][C]") ]])
            fnCutscene([[ WD_SetProperty("Activate Topics After Dialogue", "Karina") ]])
        end
    end
end