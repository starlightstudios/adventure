-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    --Facing.
    TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[Guard E]|
    if(sActorName == "Guard E") then
        
        --Variables.
        local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
        
        --If Mei is in slime form:
        WD_SetProperty("Show")
        if(sMeiForm == "Slime") then
            Append("Merc:[VOICE|MercF] Be on your best behavior.")
        
        --Mei is in bee form:
        elseif(sMeiForm == "Bee") then
            Append("Merc:[VOICE|MercF] If you're a spy, you'll be dealt with.[P] Don't try anything.")
        
        --All other cases.
        else
            Append("Merc:[VOICE|MercF] I can't wait for this job to be over so I can go home and just relax.[P] I hate being paranoid.")
        end

    -- |[Guard S]|
    elseif(sActorName == "Guard S") then
        
        --Variables.
        local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
        
        --If Mei is in slime form:
        WD_SetProperty("Show")
        if(sMeiForm == "Slime") then
            Append("Merc:[VOICE|MercM] Talking slimes.[P] Weird, but I guess stranger things have happened.")
        
        --Mei is in bee form:
        elseif(sMeiForm == "Bee") then
            Append("Merc:[VOICE|MercM] Becoming a bee girl would be so great.[P] I wish I could just run away from it all...[B][C]")
            Append("Mei:[VOICE|Mei] You could.[P] We'd love to have you.[B][C]")
            Append("Merc:[VOICE|MercM] M-[P]maybe I should.[P] Maybe not.[P] I don't know...")
        
        --All other cases.
        else
            Append("Merc:[VOICE|MercM] Forest's a real dangerous place.[P] Be careful.")
        end

    -- |[Guard N]|
    elseif(sActorName == "Guard N") then
        
        --Variables.
        local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
        
        --If Mei is in slime form:
        WD_SetProperty("Show")
        if(sMeiForm == "Slime") then
            Append("Merc:[VOICE|MercF] We're watching you.[P] Don't try to slime anyone.")
        
        --Mei is in bee form:
        elseif(sMeiForm == "Bee") then
            Append("Merc:[VOICE|MercF] Just because you got the go-ahead doesn't mean I trust you.[P] No sudden moves.")
        
        --All other cases.
        else
            Append("Merc:[VOICE|MercF] The bees have been agitated lately.[P] Be careful if you're going across the river.")
        end

    -- |[NPC A]|
    elseif(sActorName == "NPC A") then
        WD_SetProperty("Show")
        Append("Farmer:[VOICE|MercF] The soil here is very fertile, but dry.[P] We truck the water over by hand.[P] Hard work, but worth it.")
        
    -- |[NPC B]|
    elseif(sActorName == "NPC B") then
        WD_SetProperty("Show")
        Append("Farmer:[VOICE|MercM] What am I going to do with all this junk?[B][C]")
        Append("Farmer:[VOICE|MercM] Hey, want to buy some worthless crap?[B][C]")
        Append("Farmer:[VOICE|MercM] ...[B][C]")
        Append("Farmer:[VOICE|MercM] No harm in asking, right?")
    
    -- |[NPC C]|
    elseif(sActorName == "NPC C") then
        WD_SetProperty("Show")
        Append("Man:[VOICE|MercM] I wonder if the trading post is hiring guards.[P] I used to be a soldier.[B][C]")
        Append("Man:[VOICE|MercM] I hear they have beds to sleep on over there, even if the pay isn't as good.[B][C]")
        Append("Man:[VOICE|MercM] My back is killing me...")
    
    -- |[NPC D]|
    elseif(sActorName == "NPC D") then
        WD_SetProperty("Show")
        Append("Farmer:[VOICE|MercF] There are a lot of bees across the river.[P] I bet they made a nest over there.[P] I'd hate to have to hire mercenaries to go flush them out...")
    
    -- |[NPC E]|
    elseif(sActorName == "NPC E") then
        WD_SetProperty("Show")
        Append("Merc:[VOICE|MercF] Bees give me the creeps.[P] At least they don't mess with us.[P] Just let them do their thing, they tend to buzz off sooner or later.")
        
    -- |[Karina and Claudia]|
    elseif(sActorName == "Karina" or sActorName == "Claudia") then
	
        --Route this to a subfile.
        LM_ExecuteScript(fnResolvePath() .. "KarinaClaudiaDialogue.lua", sTopicName)
    end
end
