-- |[Constructor]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = fnResolveDirectory() --Map's name is the name of the folder it is in.

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[Standard]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
	AL_SetProperty("Music", "HighlandsTheme")
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/PlainsNW/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", "PlainsNW")

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
    
    -- |[Map Handling]|
    --Resolve the map's variable.
    fnStandardCh1Map(sLevelName)

-- |[Post-Exec]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

    -- |[NPCs]|
	--Claudia's Acolyte, Karina. Studying bees.
    fnStandardNPCByPosition("Karina")
	
	--If Mei rescued Claudia, spawn her here:
	local iSavedClaudia = VM_GetVar("Root/Variables/Chapter1/Scenes/iSavedClaudia", "N")
	if(iSavedClaudia == 1.0) then
        fnStandardNPCByPosition("Claudia")
	end
	
	--Gate Guards.
    fnStandardNPCByPosition("GuardE")
    fnStandardNPCByPosition("GuardS")
    fnStandardNPCByPosition("GuardN")
	
	--Farmers. 
    fnSpawnNPCPattern("NPC ", "A", "E")
	
    -- |[Special Bee Scene]|
	--If Mei just got turned into a Bee and came down south, move NPCs around. Also spawn Florentina if that happens.
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	local iHasSeenOutlandBeeScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenOutlandBeeScene", "N")
	local iHasSeenTrannadarFlorentinaScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFlorentinaScene", "N")
	if(iHasSeenOutlandBeeScene == 0.0 and sMeiForm == "Bee") then
		
		--Karina changes position.
		EM_PushEntity("Karina")
			TA_SetProperty("Position", 15, 19)
			TA_SetProperty("Depth", 0)
			if(iHasSeenTrannadarFlorentinaScene == 1.0) then
				TA_SetProperty("Facing", gci_Face_East)
			else
				TA_SetProperty("Facing", gci_Face_North)
			end
		DL_PopActiveObject()
		
		--Claudia changes position as well.
		if(iSavedClaudia == 1.0) then
			EM_PushEntity("Claudia")
				TA_SetProperty("Position", 14, 19)
				TA_SetProperty("Depth", 0)
				TA_SetProperty("Facing", gci_Face_North)
			DL_PopActiveObject()
		end
		
		--Spawn Florentina.
		if(iHasSeenTrannadarFlorentinaScene == 1.0) then
			fnSpecialCharacter("Florentina", 16, 19, gci_Face_West, false, nil)
		end
	end
	
    -- |[Overlays]|
	--Overlays.
    fnLightForest()
    
    -- |[Skillbook]|
    local iSkillbook1 = VM_GetVar("Root/Variables/Global/Florentina/iSkillbook1", "N")
    if(iSkillbook1 == 1.0 or iHasSeenTrannadarFlorentinaScene == 0.0) then
		AL_SetProperty("Set Layer Disabled", "SkillbookIco", true)
    end
end
