-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
--Mei trips over... nothing?
if(sObjectName == "TripZone" and iIsWholeCollision == 1.0) then

	--Variables.
	local iSlimeHasSeenTrip = VM_GetVar("Root/Variables/Chapter1/Scenes/iSlimeHasSeenTrip", "N")

	--Activate if we haven't seen this scene yet.
	if(iSlimeHasSeenTrip == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iSlimeHasSeenTrip", "N", 1.0)
        
        --Load images.
        fnLoadDelayedBitmapsFromList("Chapter 1 Mei Slime TF", gciDelayedLoadLoadAtEndOfTick)
		
		-- |[Movement]|
		--Set to crouch, then wounded, to simulate a trip.
		Cutscene_CreateEvent("Event", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Special Frame", "Crouch")
		DL_PopActiveObject()
		fnCutsceneWait(5)
		fnCutsceneBlocker()
		fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
		Cutscene_CreateEvent("Event", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Special Frame", "Wounded")
		DL_PopActiveObject()
		fnCutsceneWait(2)
		fnCutsceneBlocker()
		
		--Wait a bit.
		fnCutsceneWait(45)
		fnCutsceneBlocker()
		
		-- |[Dialogue]|
		--Setup.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Cry") ]])

		--Actual talking.
		fnCutscene([[ Append("Her legs and lungs burning from exertion after fleeing the disastrous fight, Mei collapsed to the ground.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] N-no...[P] I can't stop here.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] But my legs feel so weak.[P] It feels like they're made of jelly.[P].[P].[P] How long have I been running?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] It doesn't matter.[P] I need to keep moving.") ]])
		fnCutsceneWait(15)
		fnCutsceneBlocker()
		
		-- |[Movement]|
		--Stand Mei back up.
		Cutscene_CreateEvent("Event", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Special Frame", "Crouch")
		DL_PopActiveObject()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		Cutscene_CreateEvent("Event", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Special Frame", "Null")
		DL_PopActiveObject()
		fnCutsceneWait(2)
		fnCutsceneBlocker()
		
	end

--Mei starts to feel funny.
elseif(sObjectName == "FunnyZone" and iIsWholeCollision == 1.0) then

	--Variables.
	local iSlimeHasSeenFeelFunny = VM_GetVar("Root/Variables/Chapter1/Scenes/iSlimeHasSeenFeelFunny", "N")

	--Activate if we haven't seen this scene yet.
	if(iSlimeHasSeenFeelFunny == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iSlimeHasSeenFeelFunny", "N", 1.0)

        --Clear censor bars.
        WD_SetProperty("Clear Censor Bars")
		
		-- |[Dialogue]|
		--Mei's standing pose.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Activate Scene") ]])
		fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Human") ]])
		fnCutscene([[ Append("The feeling of weakness spread from her legs into her body, prickling its way up into her chest.[P] She wobbled as her legs grew heavier, and nearly fell over again.[B][C]") ]])
		fnCutscene([[ Append("Mei:[VOICE|Mei] I feel...[P] Funny...[P] I guess I've ran far enough.[P] Maybe I should sit down for a bit.[B][C]") ]])
		fnCutscene([[ Append("The odd feelings in her legs continued to spread throughout her body, spreading out from her chest into her arms and neck.[P] Her legs grew weaker still, and she forced a small laugh as they began to feel like they were melting.") ]])
		fnCutsceneBlocker()
		
		--Switch to slime TF 1.
		fnCutscene([[ WD_SetProperty("FastShow") ]])
		fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
		fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Human") ]])
		fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/SlimeTF0") ]])
		fnCutscene([[ Append("Bending down to rub the aching muscles in her legs, Mei jerked her body upright with a start.[B][C]") ]])
		fnCutscene([[ Append("Mei:[VOICE|Mei][E|Surprise] Dear lord![P] What's happening to me?[B][C]") ]])
		fnCutscene([[ Append("The muscles in her legs seemed to quiver, and her skin shone in places as if covered by a thick green gel.[P] The muscles in her legs seemed to sag off the bones before undulating back into her legs.[P] She began to lift her hands to rub her eyes, but stopped short.[B][C]") ]])
		fnCutscene([[ Append("Her hands had begun to melt.[B][C]") ]])
		fnCutscene([[ Append("Mei:[VOICE|Mei] Gotta...[P] find...[P] help[P][P]...") ]])
		fnCutsceneBlocker()
		
		
	end

--Finish the transformation here.
elseif(sObjectName == "TFZone" and iIsWholeCollision == 1.0) then

	--Variables.
	local iSlimeHasFinishedTF = VM_GetVar("Root/Variables/Chapter1/Scenes/iSlimeHasFinishedTF", "N")

	--Activate if we haven't seen this scene yet.
	if(iSlimeHasFinishedTF == 0.0) then
	
        --Issue a loading order.
        fnLoadDelayedBitmapsFromList("Chapter 1 Mei Runestone", gciDelayedLoadLoadAtEndOfTick)
		
		-- |[Flags]|
		--Prevent repeat.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iSlimeHasFinishedTF", "N", 1.0)
		
		--If Florentina was in the party, set this flag.
		local iHasSeenTrannadarFlorentinaScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFlorentinaScene", "N")
		if(iHasSeenTrannadarFlorentinaScene == 1.0) then
			VM_SetVar("Root/Variables/Chapter1/Scenes/iSlimeMeetFlorentina", "N", 1.0)
		end
		
		-- |[Darken]|
		--Darken the screen during the TF.
		AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1)

        --Clear censor bars.
        WD_SetProperty("Clear Censor Bars")
		
		-- |[Scene]|
		--TF0.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Activate Scene") ]])
		fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/SlimeTF0") ]])
		fnCutscene([[ Append("Mei:[VOICE|Mei] Must..[P] find help...[P] before I...[P] turn into...") ]])
		fnCutsceneBlocker()
		
		--Switch to slime TF1.
		fnCutscene([[ WD_SetProperty("FastShow") ]])
		fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
		fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/SlimeTF0") ]])
		fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/SlimeTF1") ]])
		fnCutscene([[ Append("Mei:[VOICE|Mei] A sliiiime...[B][C]") ]])
		fnCutscene([[ Append("The sheen on her skin spread rapidly, absorbing into her flesh even as it grew.[P] Her body glistened beneath it, growing translucent, until she could see the very bones that held her form.[P] Bones that were slowly dissolving in the gel.[B][C]") ]])
		fnCutscene([[ Append("Soon her clothes began to dissolve and be absorbed into her body, and she could see that every part of herself had been covered with slime.[B][C]") ]])
		fnCutscene([[ Append("She shook her hands slowly as she felt the creeping tendrils of slime crawl up her neck and onto her head.[P] Her senses focused on the spreading touch, clawing at the slime in one final attempt to pull herself free.[B][C]") ]])
		fnCutscene([[ Append("At last it reached her brain, already numbed with fear, and the infectious tendrils dug into it.[P] Her fear of the slime faded, replaced with a fear of hunger, and as the last of the slime transformed her, her eyes began to grow vacant with the desire to feed and reproduce.") ]])
		fnCutsceneBlocker()
		
		--Finalized.
		fnCutscene([[ WD_SetProperty("FastShow") ]])
		fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
		fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/SlimeTF1") ]])
		fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/SlimeTF2") ]])
        fnCutscene([[ WD_SetProperty("Register Censor Bar", 605, 179, 112, 45) ]])
		fnCutscene([[ Append("Mei:[VOICE|Mei] I'm...[P] losing...[P] myself...[P][P].[P].[P].[B][C]") ]])
		fnCutscene([[ Append("With a final sigh, her last thought was for the journey she had been on, focusing on it as if she might pull herself back from the brink.[P][P] Soon, even this slipped away, and her instincts took hold of her.[P] They were more bestial,[P] primitive,[P] driven by the need for survival above all else.[B][C]") ]])
		fnCutscene([[ Append("Her journey had been forgotten.[P] Her life and home before her arrival on this strange world would not even live on as a dream.[P] Even her very identity had been lost to the all-consuming hunger of the slime.[B][C]") ]])
		fnCutscene([[ Append("She began to make her way back into the forest, seeking her first meal,[P] her first spawn[P], when a pale grey glow began to envelop her.") ]])
		fnCutsceneBlocker()
		
		--Animation.
		local sAllocationString = "WD_SetProperty(\"Allocate Animation\", gciMeiRuneFramesTotal, 1, -90)"
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
		fnCutscene([[ WD_SetProperty("Set Scene Image Fast", "Root/Images/Scenes/Mei/SlimeTF2") ]])
		fnCutscene(sAllocationString)
		for i = 0, gciMeiRuneFramesTotal-1, 1 do
			local iNumber = string.format("%02i", i)
			local sString = "WD_SetProperty(\"Set Animation Frame\", " .. i .. ", \"Root/Images/Scenes/Mei/RuneAnim" .. iNumber .. "\")"
			fnCutscene(sString)
		end
		fnCutscene([[ WD_SetProperty("Activate Flash", 0) ]])
		
		fnCutscene([[ Append("Deep within her chest, the runestone pulsed with light.[P] Memories came flooding back to her as the gentle glow engulfed her and soothed her bestial mind.[B][C]") ]])
		fnCutscene([[ Append("The memory of a quiet evening on a warm summer night.[P] It was a humid evening,[P] and the pages of the romance novel she was been reading clung together during a rather heated scene.[B][C]") ]])
		fnCutscene([[ Append("The many times where she would gather with friends in a cafe.[P] They sat at the windows and rate the boys who passed by, teasing each other when they disagreed.[B][C]") ]])
		fnCutscene([[ Append("She remembered a cute boy she met not long after starting her job.[P] She was at the train station and it was raining.[P] He shared his umbrella and offered his jacket to hide the rain-plastered uniform that clung to her body.[B][C]") ]])
		fnCutscene([[ Append("Memories of the first kiss they shared.[P] Her heart had felt ready to burst when she realized what he was about to do, and she had forgotten to close her eyes.[P] She had demanded a redo immediately after she caught her breath.[B][C]") ]])
		fnCutscene([[ Append("The memory of his arms as he held their bodies together.[P] The growing heat as he thrust into her,[P] his lips gliding over her neck,[P] threatened to push her over the edge into ecstasy.[B][C]") ]])
		fnCutscene([[ Append("The glow of the light reminded her of who she was,[P] of where she had come from.[P] She remembered where she was now,[P] and of all the desires she had wanted in her life.") ]])

		--Scene part 5. Animation fades to nothing.
		fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Slime.lua") ]])
		fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
		fnCutscene([[ WD_SetProperty("FastShow") ]])
		fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
		fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Slime") ]])
		fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Slime") ]])
		fnCutscene([[ Append("As quickly as it had come, the glow faded.[P] With her mind restored, Mei looked over her new body...") ]])
		fnCutsceneBlocker()
		
		-- |[Dialogue]|
		fnCutscene([[ WD_SetProperty("FastShow") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutscene([[ Append("Mei[E|Surprised]: I...[P] I...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] I'm so...[P] beautiful...[B][C]") ]])
		fnCutscene([[ Append("Mei ran her hands along her sides, her fingers brushing against the curves under her breasts and gliding down to wrap over her thighs as they melded into a single trunk.[B][C]") ]])
		fnCutscene([[ Append("Her eyes fluttered closed as her head fell back from the feel of the smooth, gliding touch.[P] Drawing her hands back up her body, each nerve seemed to be keenly aware that they were sensing the world in a new way, and were eager to trigger at the lightest touch.[B][C]") ]])
		fnCutscene([[ Append("Gliding her hands up her body, they dipped inward in the valley between her breasts before circling around to cup herself from beneath.[P] A shuddering breath escaped her as her gelatinous skin quivered, and she gave her breasts a squishing squeeze.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] Mmmmmmm.[B][C]") ]])
		fnCutscene([[ Append("Her hands slid over her new body, gliding from her breasts to her stomach, then wrapping around her hips and down to the cheeks of her voluminous ass.[P] She squeezed herself again, and a heated moan rolled out from her throat.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] And sensitive.[B][C]") ]])
		fnCutscene([[ Append("She released her ass with reluctance, her curiosity pulling her hands around the outside of her thighs, over them, then inward.[P] She slowed, then stopped the exploration of her body, and her hands trembled in anticipation.[B][C]") ]])
		fnCutscene([[ Append("The trembling of her hands sent small ripples through her thighs that collided at the soft folds that lead to her sex.[P] Her breath quickened.[P] She was already so sensitive, and her entire body seemed to have become a countless number of erogenous zones.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] Why was I even worried?[P] Th-this is fantastic![B][C]") ]])
		fnCutscene([[ Append("She began to move one of her hands again, following those ripples upward, inward, until it hovered above her burning core.[P] She grasped herself, cupping her sex, the slowly slid a single finger inside of herself.[P] Every nerve in her body seemed to come alive with electricity as the finger slide deeper, and moaned loudly.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] No, no, get it together.[P] I've gotta find my way back...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] .[P].[P].[P].[P].[P].[P] O-okay,[P] one more...[B][C]") ]])
		fnCutscene([[ Append("She slid a second finger after the first, then started to thrust into herself with slow, deep strokes.[P] She could feel herself being pushed to the edge of her cliff almost immediately.  [B][C]") ]])
		fnCutscene([[ Append("She pumped into herself at a leisurely pace, savoring the hypersensitivity that that her new body provided and sending slow ripples across her skin.[P] Everywhere that the ripples passed crackle with her building orgasm, and seemed to match the burning heat that surrounded her fingers.[B][C]") ]])
		fnCutscene([[ Append("She came with a shout, her voice calling its worship of her new body for the entire forest to hear as she toppled over the cliff of her orgasm. [B][C]") ]])
		fnCutscene([[ Append("A second moan escaped her as the orgasm coursed through her body, and the tenuous grasp she had on her physical form shattered alongside her orgasm.[P] She collapsed to the ground, her body a quivering puddle of gel and slime that pooled where she had stood a moment before. [B][C]") ]])
		fnCutscene([[ Append("Several minutes passed as she struggled to regain her senses, the orgasm still lingering in her mind and body.[P] Instinctively she began to pull herself back together, slowly restructuring her body to resemble what she remembered herself to look like, though now with a single stalk replacing her legs. [B][C]") ]])
		fnCutscene([[ Append("A new shudder coursed through her as she finished restoring herself, and she could feel her arousal surging again.[P] Smiling to herself, she pulled her body off the ground and glanced around.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] *Huff*[P] *Huff*[P] Okay,[P] *Puff*[P] okay.[P][P] Definitely looking forward to some sensitivity training.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Serious] Back to work...") ]])
		fnCutsceneWait(15)
		fnCutsceneBlocker()
            
        --Clean images.
        fnCutscene([[ fnUnloadBitmapsFromList("Chapter 1 Mei Slime TF") ]])
		
		--If this is a relived scene, go back to the last save point.
		local iIsRelivingScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iIsRelivingScene", "N")
		if(iIsRelivingScene == 1.0) then
	
			--Wait a bit.
			fnCutsceneWait(10)
			fnCutsceneBlocker()
			
			--Black the screen out.
			fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
			fnCutsceneWait(45)
			fnCutsceneBlocker()
			
			--Unset this flag.
			VM_SetVar("Root/Variables/Chapter1/Scenes/iSlimeMeetFlorentina", "N", 0.0)
			
			--Return to the last save point and execute the post-script..
			fnCutscene([[ AL_BeginTransitionTo("LASTSAVE", gsStandardReliveEnd) ]])
			fnCutsceneBlocker()
		end
		
	end
end
