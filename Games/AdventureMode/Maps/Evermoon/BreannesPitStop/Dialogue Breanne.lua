-- |[ ===================================== Dialogue Script ==================================== ]|
--Only used for Breanne.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
	-- |[Common]|
	--Set facing.
	TA_SetProperty("Face Character", "PlayerEntity")
	
	-- |[Variables]|
	local bIsFlorentinaPresent        = AL_GetProperty("Is Character Following", "Florentina")
	local sMeiForm                    = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	local sMeiFirstForm               = VM_GetVar("Root/Variables/Chapter1/Breanne/sMeiFirstForm", "S")
	local iHasSeenMeiNewForm          = VM_GetVar("Root/Variables/Chapter1/Breanne/iHasSeenMeiNewForm", "N")
	local iMetMeiWithFlorentina       = VM_GetVar("Root/Variables/Chapter1/Breanne/iMetMeiWithFlorentina", "N")
	local iHasSeenTrannadarThirdScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarThirdScene", "N")
	local iGotWerecatCollar           = VM_GetVar("Root/Variables/Chapter1/Breanne/iGotWerecatCollar", "N")
	
	--Variables that might cause Breanne to start the Flower Quest.
	local iTalkedJob     = VM_GetVar("Root/Variables/Chapter1/Breanne/iTalkedJob", "N")
	local iTakenPieJob   = VM_GetVar("Root/Variables/Chapter1/Breanne/iTakenPieJob", "N")
	local iTalkedParents = VM_GetVar("Root/Variables/Chapter1/Breanne/iTalkedParents", "N")
	AdvCombat_SetProperty("Push Party Member", "Mei")
		local sMeiLevel = AdvCombatEntity_GetProperty("Level")
	DL_PopActiveObject()
	
	-- |[Talking]|
	--This is always a major dialogue.
	fnStandardMajorDialogue()
	
	--Breanne takes slot 5.
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Breanne", "Neutral") ]])
	
	--If Mei is not in the first form Breanne saw her in:
	if(sMeiFirstForm ~= sMeiForm and iHasSeenMeiNewForm == 0.0) then
	
		--Flags.
		VM_SetVar("Root/Variables/Chapter1/Breanne/iHasSeenMeiNewForm", "N", 1.0)
	
		--If Nadia knows about Mei's shapeshifting properties...
		if(iHasSeenTrannadarThirdScene == 1.0) then
			fnCutscene([[ Append("Breanne: Well hello, Mei.[P] You're looking different than normal.[B][C]") ]])
			fnCutscene([[ Append("Mei: Wait, I can explain.[B][C]") ]])
			fnCutscene([[ Append("Breanne: No need for that, hun.[P] Nadia was here earlier, told me everything.[B][C]") ]])
			fnCutscene([[ Append("Breanne: Crazy magic shapeshifter, eh?[P] Quite a story, but the evidence is right in front of me.[B][C]") ]])
			fnCutscene([[ Append("Mei: Word travels fast around here, doesn't it?[B][C]") ]])
			fnCutscene([[ Append("Breanne: With Nadia, sure.[P] She's ten gossips in a five stone bag.[B][C]") ]])
			fnCutscene([[ Append("Breanne: Not that I mind![B][C]") ]])
			fnCutscene([[ Append("Mei: All right, then.[P] As long as you're okay with it.[B][C]") ]])
			
			if(sMeiForm == "Werecat" and iGotWerecatCollar == 0.0) then
				fnCutscene([[ Append("Breanne: Oh![P] Oh![P] Here, this would look great on you![B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Offended] A collar?[P] I am not a simple housecat, Breanne![B][C]") ]])
				fnCutscene([[ Append("Breanne: Are you sure?[P] Look closer![B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Surprise] [SOUND|World|TakeItem](Received Moonglow Collar)[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Surprise] This...[P] It glows like the moon![B][C]") ]])
				fnCutscene([[ Append("Breanne: I think they're called...[P] Moonglow Stones?[P] They're supposed to capture moonlight at night and release it during the day.[B][C]") ]])
				fnCutscene([[ Append("Breanne: I found it in the forest a while back.[P] I think it suits you![B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Happy] It's beautiful![P] Thank you, Breannepurrr![B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Blush] ... [P]Sorry about getting upset earlier.[B][C]") ]])
				fnCutscene([[ Append("Breanne: Aww, that's okay.[P] No harm done.[P] Take good care of that, now![B][C]") ]])
				VM_SetVar("Root/Variables/Chapter1/Breanne/iGotWerecatCollar", "N", 1.0)
				LM_ExecuteScript(gsItemListing, "Moonglow Collar")
			end
		
		--If Florentina is present:
		elseif(bIsFlorentinaPresent == true) then
			
			--Flag.
			VM_SetVar("Root/Variables/Chapter1/Breanne/iMetMeiWithFlorentina", "N", 1.0)
			
			--If Mei was originally a human:
			if(sMeiFirstForm == "Human") then
				
				--If Mei is a slime:
				if(sMeiForm == "Slime") then
					fnCutscene([[ Append("Breanne: Oh, no.[P] Mei...[P] I'm so sorry...[B][C]") ]])
					fnCutscene([[ Append("Mei: Breanne, I can explain...[B][C]") ]])
					fnCutscene([[ Append("Breanne: Oh, you can speak?[P] How?[B][C]") ]])
					fnCutscene([[ Append("Florentina: Don't look at me.[P] I didn't think she had vocal chords.[B][C]") ]])
					fnCutscene([[ Append("Breanne: Well golly, what a relief![P] I had thought I lost a friend.[P] Don't spook me like that again![B][C]") ]])
					fnCutscene([[ Append("Mei: It's my runestone.[P] It lets me stay, well, me.[P] I can even turn back into a human if I want to.[B][C]") ]])
					fnCutscene([[ Append("Breanne: Now that there is a special rock.[P] You hang on to that.[P] World needs more girls like you, and it certainly doesn't need more slimes.[B][C]") ]])
					fnCutscene([[ Append("Breanne: That said, you look pretty good for mass of goo.[B][C]") ]])
					fnCutscene([[ Append("Florentina: I think she likes you, Mei.[B][C]") ]])
					fnCutscene([[ Append("Breanne: Don't ya go readin' too far into that![B][C]") ]])
					fnCutscene([[ Append("Breanne: Now then, what can I do for you?") ]])
				
				--If Mei is an Alraune:
				elseif(sMeiForm == "Alraune") then
					fnCutscene([[ Append("Breanne: Mei![P] You're -[P] Florentina, what have you gone and done?[B][C]") ]])
					fnCutscene([[ Append("Florentina: Why do you always blame me first?[B][C]") ]])
					fnCutscene([[ Append("Breanne: You no good, dirty - [P][CLEAR]") ]])
					fnCutscene([[ Append("Mei: It's not her fault, honest![B][C]") ]])
					fnCutscene([[ Append("Mei: Besides, I don't mind this.[P] Not one bit.[P] It feels...[P] liberating...[B][C]") ]])
					fnCutscene([[ Append("Breanne: You promise it wasn't her?[P] She's a tricky one.[B][C]") ]])
					fnCutscene([[ Append("Florentina: Absolutely not![P] You ought to know better![P] If I've made one thing clear, it's that I don't associate with Rochea and her ilk.[B][C]") ]])
					fnCutscene([[ Append("Breanne: ...[P] All right, all right.[P] I'm sorry to have accused you.[B][C]") ]])
					fnCutscene([[ Append("Mei: Besides, it's not permanent.[P] This runestone lets me change form if I feel like it.[B][C]") ]])
					fnCutscene([[ Append("Breanne: It does?[P] That's awful handy.[P] Seems there's more to you than meets the eye.[B][C]") ]])
					fnCutscene([[ Append("Breanne: Does that mean you feel like being a plant girl?[B][C]") ]])
					fnCutscene([[ Append("Mei: I love it.[P] Could I possibly convince you to be joined?[P] The sensation is incomparable...[B][C]") ]])
					fnCutscene([[ Append("Breanne: I'm afraid that'd be a one-way trip for me, dearie, and I'll not be making it any time soon.[B][C]") ]])
					fnCutscene([[ Append("Breanne: Unless you think that stone of yours might work on me?[B][C]") ]])
					fnCutscene([[ Append("Mei: It won't.[P] I'm not sure how I know that, but I do.[B][C]") ]])
					fnCutscene([[ Append("Breanne: I'm quite happy as I am, but thank you for the offer.[P] Now what can I do for you?[B][C]") ]])
	
				--If Mei is a bee:
				elseif(sMeiForm == "Bee") then
					fnCutscene([[ Append("Breanne: Dearest me, Mei.[P] Did you pick a fight with the bees?[B][C]") ]])
					fnCutscene([[ Append("Breanne: What happened, Florentina?[B][C]") ]])
					fnCutscene([[ Append("Mei: It's not what it looks like.[B][C]") ]])
					fnCutscene([[ Append("Breanne: You can speak?[P] Are you still you?[B][C]") ]])
					fnCutscene([[ Append("Mei: In some ways.[P] In others, less-so.[B][C]") ]])
					fnCutscene([[ Append("Florentina: It's quite a trick, that.[P] I didn't believe it myself at first.[B][C]") ]])
					fnCutscene([[ Append("Mei: It has to do with my runestone, but I don't know why.[P] It lets me change form, and remember who I am.[P] For better or worse...[B][C]") ]])
					fnCutscene([[ Append("Breanne: I've had some friends go that way.[P] I reckon they'd want that runestone to work a treat on them too.[B][C]") ]])
					fnCutscene([[ Append("Mei: ...[P] Yes?[P] I see.[P] I'll tell her.[B][C]") ]])
					fnCutscene([[ Append("Florentina: She's talking to the hive.[P] She gets like that sometimes.[B][C]") ]])
					fnCutscene([[ Append("Mei: They said they remember you, Breanne.[P] They said not to worry, and that they're very happy.[B][C]") ]])
					fnCutscene([[ Append("Breanne: Oh, Joanie...[B][C]") ]])
					fnCutscene([[ Append("Mei: Joanie?[P] You mean -[P] well, we don't normally have names.[P] Yes, she's here.[B][C]") ]])
					fnCutscene([[ Append("Breanne: I'm about to cry.[P] Look away, please.[B][C]") ]])
					fnCutscene([[ Append("Mei: She's very happy as a bee.[P] You have nothing to worry about.[B][C]") ]])
					fnCutscene([[ Append("Breanne: *sniff*[P] You mean that?[B][C]") ]])
					fnCutscene([[ Append("Mei: We have no reason to lie.[B][C]") ]])
					fnCutscene([[ Append("Florentina: Isn't this a tearful reunion-by-proxy.[P] Mei, don't we have a job to do here?[B][C]") ]])
					fnCutscene([[ Append("Mei: Don't be so callous.[B][C]") ]])
					fnCutscene([[ Append("Breanne: Oh Joanie, I'm so glad you're okay.[B][C]") ]])
					fnCutscene([[ Append("Breanne: *sniff*[P] No no, gotta be strong.[B][C]") ]])
					fnCutscene([[ Append("Breanne: Let's talk about something else, okay?[P] But you tell Joanie that if she wants to come see me...[B][C]") ]])
					fnCutscene([[ Append("Mei: She already knows.[B][C]") ]])
					fnCutscene([[ Append("Breanne: Thanks so much, Mei.[P] Thank you.[P] Really...[B][C]") ]])
					
					--Flag.
					VM_SetVar("Root/Variables/Chapter1/Breanne/iCanSpawnJoanie", "N", 1.0)
	
				--If Mei is a werecat:
				elseif(sMeiForm == "Werecat") then
					fnCutscene([[ Append("Breanne: Oh my, Mei![P] What happened?[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Happy] What does it look like?[B][C]") ]])
					fnCutscene([[ Append("Breanne: Oh dear.[P] The cure is so easy to mix up, but I'm afraid it won't have any effect once the transformation is complete.[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Offended] Cure?[P] Hss![P] No thanks![B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Smirk] I've never felt better![P] The moon is my eternal companion![B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Blush] You should come hunt with me, Breanne.[P] You'd love the speed, the rush of the kill...[B][C]") ]])
					fnCutscene([[ Append("Breanne: Hah![P] Thanks for the offer, but I'm all right the way I am.[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Blush] I need strong pridemates.[P] If you change your mind...[B][C]") ]])
					fnCutscene([[ Append("Florentina:[E|Happy] I'm a pridemate, am I?[P] Yeah, Breanne, you should get your hammer and tag along![B][C]") ]])
					fnCutscene([[ Append("Breanne: Aww, you're really flattering me![B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Neutral] Worth a shot.[P] Hey, you wouldn't have any chicken in the pantry, would you?[P] I'm starved![B][C]") ]])
					fnCutscene([[ Append("Breanne: Didn't you just say you're a hunter?[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Neutral] We hunt mostly for sport, for food only when necessary.[P] But, I'm so busy looking for a way back to Earth...[B][C]") ]])
					fnCutscene([[ Append("Breanne: I'll give you some chicken if you're willing to curl up on my lap and purr.[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Offended] Don't patronize me![B][C]") ]])
					fnCutscene([[ Append("Breanne: Hmm... how about this?[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Surprise] [SOUND|World|TakeItem](Received Moonglow Collar)[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Surprise] This...[P] It glows like the moon![B][C]") ]])
					fnCutscene([[ Append("Breanne: I think they're called...[P] Moonglow Stones?[P] They're supposed to capture moonlight at night and release it during the day.[B][C]") ]])
					fnCutscene([[ Append("Breanne: I found it in the forest a while back.[P] I think it suits you![B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Happy] It's beautiful![P] Thank you, Breannepurrr![B][C]") ]])
					fnCutscene([[ Append("Breanne: Anything for a friend![P] Take good care of it, now!") ]])
					VM_SetVar("Root/Variables/Chapter1/Breanne/iGotWerecatCollar", "N", 1.0)
					LM_ExecuteScript(gsItemListing, "Moonglow Collar")
	
				--If Mei is a ghost:
				elseif(sMeiForm == "Ghost") then
					fnCutscene([[ Append("Breanne: ...[P] Mei?[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Happy] Hello, Breanne.[P] What have you been up to lately?[B][C]") ]])
					fnCutscene([[ Append("Breanne: Breathing![B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Surprise] Huh?[P] Oh...[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Neutral] It's not what it looks like.[B][C]") ]])
					fnCutscene([[ Append("Breanne: I'd say it is, considering you're transparent![B][C]") ]])
					fnCutscene([[ Append("Florentina: Incredible how you can see right through her, right?[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Smirk] I'm fine, really.[P] In fact, this is quite nice.[P] I'll never get hungry, or sick.[B][C]") ]])
					fnCutscene([[ Append("Breanne: I suppose so...[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Happy] There's always a good side, if you look for it.[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Neutral] It's quite cold, though.[B][C]") ]])
					fnCutscene([[ Append("Breanne: Could we perhaps talk about something else?[B][C]") ]])
                
                --If Mei is a gravemarker:
				elseif(sMeiForm == "Gravemarker") then
					fnCutscene([[ Append("Breanne: Well I'll be a mermaid's uncle, is that you Mei?[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Happy] Nice to see you, Breanne![B][C]") ]])
					fnCutscene([[ Append("Breanne: Is the whole 'angel made of rock' thing a faux pas?[P] Am I not supposed to mention it?[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Neutral] Oops.[P] Uhhh, it's -[P] it's not a big deal![B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Neutral] I had a run in with some statues and, well...[B][C]") ]])
					fnCutscene([[ Append("Florentina:[E|Happy] She didn't lose her head.[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Offended] Good one, *Nadia*.[B][C]") ]])
					fnCutscene([[ Append("Florentina:[E|Happy] I taught her everything she knows.[B][C]") ]])
					fnCutscene([[ Append("Breanne: And you're fine with this?[P] What's it like?[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Smirk] It has its upsides and its downsides.[P] Despite the extra weight, I can move normally.[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Neutral] Can't fly, obviously, and I'll sink like a brick, but the extra mass helps in a fight.[B][C]") ]])
					fnCutscene([[ Append("Breanne: What do you eat?[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Neutral] Nothing, unfortunately.[B][C]") ]])
					fnCutscene([[ Append("Florentina:[E|Neutral] Can't sample your cooking, eh Breanne?[B][C]") ]])
					fnCutscene([[ Append("Breanne: Shame![P] More for me, in that case.[B][C]") ]])
					fnCutscene([[ Append("Breanne: Can I get you anything else?[B][C]") ]])
                
                --If Mei is a wisphag:
                elseif(sMeiForm == "Wisphag") then
					fnCutscene([[ Append("Breanne: Now if I'm seeing this right, you're a wisphag who looks an awful lot like Mei.[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Happy] Hello, Breanne![P] My, your soul is big and strong![B][C]") ]])
					fnCutscene([[ Append("Breanne: You can see that?[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Neutral] Of course![P] The duty of a wisphag is to help a lost soul find their way.[P] It means we can see all sorts of souls.[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Neutral] Yours is exactly where it wants to be.[P] Big and happy.[B][C]") ]])
					fnCutscene([[ Append("Breanne: What about Florentina's?[B][C]") ]])
					fnCutscene([[ Append("Florentina:[E|Neutral] Don't ask that.[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Neutral] She'd never admit it but she's just as happy as you.[P] Except for...[B][C]") ]])
					fnCutscene([[ Append("Florentina:[E|Happy] Kindly shut up.[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Neutral] Are you not going to ask me how this happened?[P] Or how I can turn back?[B][C]") ]])
					fnCutscene([[ Append("Breanne: The first one, yes, and what's this about turning back?[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Sad] Darn it, I gave away the cool part.[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Happy] I was taken to meet some wisps in the swamp, and they made me a wisphag![P] I am apparently sensitive to lost souls.[P] Maybe.[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Happy] But I can change back with my runestone here.[B][C]") ]])
					fnCutscene([[ Append("Breanne: Hey, there's no lost souls around here, are there?[P] The Pit Stop is some kinda magic place.[P] Is it lost souls?[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Neutral] No, I can't see anything like that.[B][C]") ]])
					fnCutscene([[ Append("Breanne: I guess that's a good thing.[P] And good for you, Mei.[P] Help those fellas get to the next life, you're a real hero to those in need.[B][C]") ]])
					fnCutscene([[ Append("Florentina:[E|Neutral] Oh like it's hard.[B][C]") ]])
					fnCutscene([[ Append("Breanne: You stick around if you've got more stories![P] I'd love to hear them![B][C]") ]])
                
                --If Mei is a mannequin:
				elseif(sMeiForm == "Mannequin") then
					fnCutscene([[ Append("Breanne: Well isn't that unnerving.[P] Looks just like Mei.[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Neutral] Hello.[B][C]") ]])
					fnCutscene([[ Append("Breanne: Yipes![B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Neutral] Don't worry, Breanne.[P] It's just a curse.[B][C]") ]])
					fnCutscene([[ Append("Breanne: Well then, uncurse yourself already![B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Neutral] I can if I want to, but being cursed has its benefits.[B][C]") ]])
					fnCutscene([[ Append("Breanne: Like what?[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Neutral] However frightened you were just now, imagine you're an enemy in a fight with me.[B][C]") ]])
					fnCutscene([[ Append("Breanne: I'd have headed for the hills.[P] Something about that just ain't right.[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Neutral] Exactly.[P] So I'll let the curse sit a little longer.[B][C]") ]])
					fnCutscene([[ Append("Breanne: Goodness me.[P] Can't argue with that.[B][C]") ]])
				end
		
			--If Mei did not enter as a human.
			else
				fnCutscene([[ Append("Breanne: Mei?[P] Well, aren't you full of surprises.[B][C]") ]])
				fnCutscene([[ Append("Mei: Oh, dear.[P] I can explain.[B][C]") ]])
				fnCutscene([[ Append("Mei: Y'see, my runestone does this thing...[B][C]") ]])
				fnCutscene([[ Append("Breanne: Say no more.[P] There's magic afoot, is there?[B][C]") ]])
				fnCutscene([[ Append("Mei: I guess so.[P] It flashes white and then, poof.[B][C]") ]])
				fnCutscene([[ Append("Breanne: Very useful to have around.[B][C]") ]])
				fnCutscene([[ Append("Florentina: You don't seem surprised.[P] Have you ever seen someone like Mei before?[B][C]") ]])
				fnCutscene([[ Append("Breanne: Can't say that I have.[P] If there were any rules about Pandemonium, you just broke one of them.[B][C]") ]])
			
				if(sMeiForm == "Werecat" and iGotWerecatCollar == 0.0) then
					fnCutscene([[ Append("Breanne: Oh![P] Oh![P] Here, this would look great on you![B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Offended] A collar?[P] I am not a simple housecat, Breanne![B][C]") ]])
					fnCutscene([[ Append("Breanne: Are you sure?[P] Look closer![B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Surprise] [SOUND|World|TakeItem](Received Moonglow Collar)[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Surprise] This...[P] It glows like the moon![B][C]") ]])
					fnCutscene([[ Append("Breanne: I think they're called...[P] Moonglow Stones?[P] They're supposed to capture moonlight at night and release it during the day.[B][C]") ]])
					fnCutscene([[ Append("Breanne: I found it in the forest a while back.[P] I think it suits you![B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Happy] It's beautiful![P] Thank you, Breannepurrr![B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Blush] ... [P]Sorry about getting upset earlier.[B][C]") ]])
					fnCutscene([[ Append("Breanne: Aww, that's okay.[P] No harm done.[P] Take good care of that, now![B][C]") ]])
					VM_SetVar("Root/Variables/Chapter1/Breanne/iGotWerecatCollar", "N", 1.0)
					LM_ExecuteScript(gsItemListing, "Moonglow Collar")
				else
					fnCutscene([[ Append("Breanne: I'll be keeping an eye on you two, that's for sure.[P] If you've got any stories I'd love to hear them.[B][C]") ]])
				end
				
				--Flag.
				WD_SetProperty("Unlock Topic", "Pandemonium", 1)
	
			end
	
		--Otherwise, Breanne observes it herself.
		else
		
			--If Mei was originally a human:
			if(sMeiFirstForm == "Human") then
				
				--If Mei is a slime:
				if(sMeiForm == "Slime") then
					fnCutscene([[ Append("Breanne: Oh, no.[P] Mei...[P] I'm so sorry...[B][C]") ]])
					fnCutscene([[ Append("Mei: Breanne, I can explain...[B][C]") ]])
					fnCutscene([[ Append("Breanne: Oh, you can speak?[P] How?[B][C]") ]])
					fnCutscene([[ Append("Breanne: Well golly, what a relief![P] I had thought I lost a friend.[P] Don't spook me like that again![B][C]") ]])
					fnCutscene([[ Append("Mei: It's my runestone.[P] It lets me stay, well, me.[P] I can even turn back into a human if I want to.[B][C]") ]])
					fnCutscene([[ Append("Breanne: Now that there is a special rock.[P] You hang on to that.[P] World needs more girls like you, and it certainly doesn't need more slimes.[B][C]") ]])
					fnCutscene([[ Append("Breanne: That said, you look pretty good for mass of goo.[B][C]") ]])
					fnCutscene([[ Append("Mei: Oh, why thank you.[P] I can look however I want, but I like this the most...[B][C]") ]])
					fnCutscene([[ Append("Breanne: ...[P] Don't ya go readin' too far into that![B][C]") ]])
					fnCutscene([[ Append("Breanne: Now then, what can I do for you?[B][C]") ]])
				
				--If Mei is an Alraune:
				elseif(sMeiForm == "Alraune") then
					fnCutscene([[ Append("Breanne: Mei![P] You're -[P] What have you gone and done?[B][C]") ]])
					fnCutscene([[ Append("Mei: Is this a problem?[P] I rather like myself now...[B][C]") ]])
					fnCutscene([[ Append("Mei: Besides, it's not permanent.[P] This runestone lets me change form if I feel like it.[B][C]") ]])
					fnCutscene([[ Append("Breanne: It does?[P] That's awful handy.[P] Seems there's more to you than meets the eye.[B][C]") ]])
					fnCutscene([[ Append("Breanne: Does that mean you feel like being a plant girl?[B][C]") ]])
					fnCutscene([[ Append("Mei: I love it.[P] Could I possibly convince you to be joined?[P] The sensation is incomparable...[B][C]") ]])
					fnCutscene([[ Append("Breanne: I'm afraid that'd be a one-way trip for me, dearie, and I'll not be making it any time soon.[B][C]") ]])
					fnCutscene([[ Append("Breanne: Unless you think that stone of yours might work on me?[B][C]") ]])
					fnCutscene([[ Append("Mei: It won't.[P] I'm not sure how I know that, but I do.[B][C]") ]])
					fnCutscene([[ Append("Breanne: I'm quite happy as I am, but thank you for the offer.[P] Now what can I do for you?[B][C]") ]])
	
				--If Mei is a bee:
				elseif(sMeiForm == "Bee") then
					fnCutscene([[ Append("Breanne: Dearest me, Mei.[P] Did you pick a fight with the bees?[B][C]") ]])
					fnCutscene([[ Append("Mei: It's not what it looks like.[B][C]") ]])
					fnCutscene([[ Append("Breanne: You can speak?[P] Are you still you?[B][C]") ]])
					fnCutscene([[ Append("Mei: In some ways.[P] In others, less-so.[B][C]") ]])
					fnCutscene([[ Append("Mei: It has to do with my runestone, but I don't know why.[P] It lets me change form, and remember who I am.[P] For better or worse...[B][C]") ]])
					fnCutscene([[ Append("Breanne: I've had some friends go that way.[P] I reckon they'd want that runestone to work a treat on them too.[B][C]") ]])
					fnCutscene([[ Append("Mei: ...[P] Yes?[P] I see.[P] I'll tell her.[B][C]") ]])
					fnCutscene([[ Append("Breanne: Excuse me?[B][C]") ]])
					fnCutscene([[ Append("Mei: Apologies, I was talking to the hive.[P] We can hear one another, you see.[B][C]") ]])
					fnCutscene([[ Append("Mei: They said they remember you, Breanne.[P] They said not to worry, and that they're very happy.[B][C]") ]])
					fnCutscene([[ Append("Breanne: Oh, Joanie...[B][C]") ]])
					fnCutscene([[ Append("Mei: Joanie?[P] You mean - [P]well, we don't normally have names.[P] But, yes, she's here.[B][C]") ]])
					fnCutscene([[ Append("Breanne: I'm about to cry.[P] Look away, please.[B][C]") ]])
					fnCutscene([[ Append("Mei: She's very happy as a bee.[P] You have nothing to worry about.[B][C]") ]])
					fnCutscene([[ Append("Breanne: *sniff*[P] You mean that?[B][C]") ]])
					fnCutscene([[ Append("Mei: We have no reason to lie.[B][C]") ]])
					fnCutscene([[ Append("Breanne: Oh Joanie, I'm so glad you're okay.[B][C]") ]])
					fnCutscene([[ Append("Breanne: *sniff*[P] No no, gotta be strong.[B][C]") ]])
					fnCutscene([[ Append("Breanne: Let's talk about something else, okay?[P] But you tell Joanie that if she wants to come see me...[B][C]") ]])
					fnCutscene([[ Append("Mei: She already knows.[B][C]") ]])
					fnCutscene([[ Append("Breanne: Thanks so much, Mei.[P] Thank you.[P] Really...[B][C]") ]])
					
					--Flag.
					VM_SetVar("Root/Variables/Chapter1/Breanne/iCanSpawnJoanie", "N", 1.0)
	
				--If Mei is a werecat:
				elseif(sMeiForm == "Werecat") then
					fnCutscene([[ Append("Breanne: Oh my, Mei![P] What happened?[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Happy] What does it look like?[B][C]") ]])
					fnCutscene([[ Append("Breanne: Oh dear.[P] The cure is so easy to mix up, but I'm afraid it won't have any effect once the transformation is complete.[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Offended] Cure?[P] Hss![P] No thanks![B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Smirk] I've never felt better![P] The moon is my eternal companion![B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Blush] You should come hunt with me, Breanne.[P] You'd love the speed, the rush of the kill...[B][C]") ]])
					fnCutscene([[ Append("Breanne: Hah![P] Thanks for the offer, but I'm all right the way I am.[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Blush] I need strong pridemates.[P] If you change your mind...[B][C]") ]])
					fnCutscene([[ Append("Breanne: Aww, Mei, you're really flattering me![B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Neutral] Worth a shot.[P] Hey, you wouldn't have any chicken in the pantry, would you?[P] I'm starved![B][C]") ]])
					fnCutscene([[ Append("Breanne: Didn't you just say you're a hunter?[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Neutral] We hunt mostly for sport, for food only when necessary.[P] But, I'm so busy looking for a way back to Earth...[B][C]") ]])
					fnCutscene([[ Append("Breanne: I'll give you some chicken if you're willing to curl up on my lap and purr.[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Offended] Don't patronize me![B][C]") ]])
					fnCutscene([[ Append("Breanne: Oh, I'm sorry![P] Here, how about a gift?[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Surprise] [SOUND|World|TakeItem](Received Moonglow Collar)[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Surprise] This...[P] It glows like the moon![B][C]") ]])
					fnCutscene([[ Append("Breanne: I think they're called...[P] Moonglow Stones?[P] They're supposed to capture moonlight at night and release it during the day.[B][C]") ]])
					fnCutscene([[ Append("Breanne: I found it in the forest a while back.[P] I think it suits you![B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Happy] It's beautiful![P] Thank you, Breannepurrr![B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Blush] ... [P]Sorry about getting upset earlier.[B][C]") ]])
					fnCutscene([[ Append("Breanne: Aww, that's okay.[P] No harm done.[P] Take good care of that, now![B][C]") ]])
					VM_SetVar("Root/Variables/Chapter1/Breanne/iGotWerecatCollar", "N", 1.0)
					LM_ExecuteScript(gsItemListing, "Moonglow Collar")
	
				--If Mei is a ghost:
				elseif(sMeiForm == "Ghost") then
					fnCutscene([[ Append("Breanne: ...[P] Mei?[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Happy] Hello, Breanne.[P] What have you been up to lately?[B][C]") ]])
					fnCutscene([[ Append("Breanne: Breathing![B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Surprise] Huh?[P] Oh...[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Neutral] It's not what it looks like.[B][C]") ]])
					fnCutscene([[ Append("Breanne: I'd say it is, considering you're transparent![B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Smirk] I'm fine, really.[P] In fact, this is quite nice.[P] I'll never get hungry, or sick.[B][C]") ]])
					fnCutscene([[ Append("Breanne: I suppose so...[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Happy] There's always a good side, if you look for it.[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Neutral] It's quite cold, though.[B][C]") ]])
					fnCutscene([[ Append("Breanne: Could we perhaps talk about something else?[B][C]") ]])
	
                --If Mei is a gravemarker:
				elseif(sMeiForm == "Gravemarker") then
					fnCutscene([[ Append("Breanne: Well I'll be a mermaid's uncle, is that you Mei?[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Happy] Nice to see you, Breanne![B][C]") ]])
					fnCutscene([[ Append("Breanne: Is the whole 'angel made of rock' thing a faux pas?[P] Am I not supposed to mention it?[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Neutral] Oops.[P] Uhhh, it's -[P] it's not a big deal![B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Neutral] I had a run in with some statues and, well...[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Offended] I got away before they made some permanent changes.[B][C]") ]])
					fnCutscene([[ Append("Breanne: And you're fine with this?[P] What's it like?[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Smirk] It has its upsides and its downsides.[P] Despite the extra weight, I can move normally.[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Neutral] Can't fly, obviously, and I'll sink like a brick, but the extra mass helps in a fight.[B][C]") ]])
					fnCutscene([[ Append("Breanne: What do you eat?[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Neutral] Nothing, unfortunately.[B][C]") ]])
					fnCutscene([[ Append("Breanne: Guess I'll have to eat that roast by myself.[P] Shame![B][C]") ]])
					fnCutscene([[ Append("Breanne: Can I get you anything else?[B][C]") ]])
                
                --If Mei is a wisphag:
                elseif(sMeiForm == "Wisphag") then
					fnCutscene([[ Append("Breanne: Now if I'm seeing this right, you're a wisphag who looks an awful lot like Mei.[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Happy] Hello, Breanne![P] My, your soul is big and strong![B][C]") ]])
					fnCutscene([[ Append("Breanne: You can see that?[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Neutral] Of course![P] The duty of a wisphag is to help a lost soul find their way.[P] It means we can see all sorts of souls.[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Neutral] Yours is exactly where it wants to be.[P] Big and happy.[B][C]") ]])
					fnCutscene([[ Append("Breanne: That's quite a thing to say about someone.[P] You sure you're seeing it right?[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Smirk] Guiding souls is literally the entire purpose of a wisphag.[B][C]") ]])
					fnCutscene([[ Append("Breanne: What would you do if you ran out of souls to guide?[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Smirk] Maybe pick up Cricket.[P] I've always seen it on TV but never actually played it.[P] It looks fun.[B][C]") ]])
					fnCutscene([[ Append("Breanne: Is that a game where you're from?[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Happy] Hey![P] Maybe I can introduce Pandemonium to Earth sports![B][C]") ]])
					fnCutscene([[ Append("Breanne: That's the spirit, Mei![B][C]") ]])
                
                --If Mei is a mannequin:
				elseif(sMeiForm == "Mannequin") then
					fnCutscene([[ Append("Breanne: Well isn't that unnerving.[P] Looks just like Mei.[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Neutral] Hello.[B][C]") ]])
					fnCutscene([[ Append("Breanne: Yipes![B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Neutral] Don't worry, Breanne.[P] It's just a curse.[B][C]") ]])
					fnCutscene([[ Append("Breanne: Well then, uncurse yourself already![B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Neutral] I can if I want to, but being cursed has its benefits.[B][C]") ]])
					fnCutscene([[ Append("Breanne: Like what?[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Neutral] However frightened you were just now, imagine you're an enemy in a fight with me.[B][C]") ]])
					fnCutscene([[ Append("Breanne: I'd have headed for the hills.[P] Something about that just ain't right.[B][C]") ]])
					fnCutscene([[ Append("Mei:[E|Neutral] Exactly.[P] So I'll let the curse sit a little longer.[B][C]") ]])
					fnCutscene([[ Append("Breanne: Goodness me.[P] Can't argue with that.[B][C]") ]])
				end
		
			--If Mei did not enter as a human.
			else
				fnCutscene([[ Append("Breanne: Mei?[P] Well, aren't you full of surprises.[B][C]") ]])
				fnCutscene([[ Append("Mei: Oh, dear.[P] I can explain.[B][C]") ]])
				fnCutscene([[ Append("Mei: Y'see, my runestone does this thing...[B][C]") ]])
				fnCutscene([[ Append("Breanne: Say no more.[P] There's magic afoot, is there?[B][C]") ]])
				fnCutscene([[ Append("Mei: I guess so.[P] It flashes white and then, poof.[B][C]") ]])
				fnCutscene([[ Append("Breanne: Very useful to have around.[B][C]") ]])
				fnCutscene([[ Append("Mei: You don't seem surprised.[P] Have you met anyone else who can do this?[B][C]") ]])
				fnCutscene([[ Append("Breanne: Can't say that I have.[P] If there were any rules about Pandemonium, you just broke one of them.[B][C]") ]])
				fnCutscene([[ Append("Breanne: I'll be keeping an eye on you, that's for sure.[P] If you've got any stories I'd love to hear them.[B][C]") ]])
				
				--Flag.
				WD_SetProperty("Unlock Topic", "Pandemonium", 1)
			end
		end
	
	--If Mei arrived with Florentina when she was not in tow earlier.
	elseif(bIsFlorentinaPresent == true and iMetMeiWithFlorentina == 0.0) then
			
		--Flag.
		VM_SetVar("Root/Variables/Chapter1/Breanne/iMetMeiWithFlorentina", "N", 1.0)
        local iMetClaudia = VM_GetVar("Root/Variables/Chapter1/Scenes/iMetClaudia", "N")
		
		--Dialogue.
		fnCutscene([[ Append("Breanne: Is that Florentina?[P] You don't visit nearly enough![P] How are you?[B][C]") ]])
		fnCutscene([[ Append("Florentina: Breanne...[B][C]") ]])
		fnCutscene([[ Append("Breanne: Friendlier than the usual greeting.[P] You're improving.[B][C]") ]])
		fnCutscene([[ Append("Mei: Do you two have a history?[B][C]") ]])
		fnCutscene([[ Append("Breanne: She's a sometime business partner, and sometime rival.[P] You know, best friend material.[B][C]") ]])
		fnCutscene([[ Append("Florentina: Don't believe her lies.[B][C]") ]])
		fnCutscene([[ Append("Breanne: Aww, that's as close to a sign of affection as you'll get out of her.[B][C]") ]])
		fnCutscene([[ Append("Mei: I'll keep that in mind.[B][C]") ]])
		fnCutscene([[ Append("Breanne: Now what can I do for you two?[P] Did Florentina figure out your conundrum like I said?[B][C]") ]])
        if(iMetClaudia == 0.0) then
            fnCutscene([[ Append("Florentina: No, but I'd bet that Sister Claudia would.[P] Is she here?[B][C]") ]])
            fnCutscene([[ Append("Breanne: Claudia?[P] Claudia...[P] oh right, the monk.[P] No, she went northeast a few weeks ago with her convent.[B][C]") ]])
            fnCutscene([[ Append("Mei: Guess we're going northeast, then.[P] Thanks, Breanne.[B][C]") ]])
            fnCutscene([[ Append("Breanne: No trouble at all![P] Oh, I think one of her followers was at Outland Farm west of here.[P] You might want to ask them.[B][C]") ]])
        
            local iHasFoundOutlandAcolyte = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasFoundOutlandAcolyte", "N")
            if(iHasFoundOutlandAcolyte == 0.0) then
                fnCutscene([[ Append("Mei: Thanks![P] You're a huge help, you know that?[B][C]") ]])
                fnCutscene([[ Append("Breanne: Just doing my job.[B][C]") ]])
                fnCutscene([[ Append("Florentina: You two are so saccharine it makes me sick.[B][C]") ]])
                fnCutscene([[ Append("Breanne: Aww, flattery will get you everywhere.[B][C]") ]])
                fnCutscene([[ Append("Breanne: Can I help you with anything else?[B][C]") ]])
            else
                fnCutscene([[ Append("Mei: Thanks, but we just came from there.[P] She said the same thing you did.[B][C]") ]])
                fnCutscene([[ Append("Breanne: This still counts as me helping, right?[B][C]") ]])
                fnCutscene([[ Append("Mei: [E|Happy]It's the thought that counts![B][C]") ]])
                fnCutscene([[ Append("Florentina: You two are so saccharine it makes me sick.[B][C]") ]])
                fnCutscene([[ Append("Breanne: Aww, flattery will get you everywhere.[B][C]") ]])
                fnCutscene([[ Append("Breanne: Can I help you with anything else?[B][C]") ]])
            end
        else
            fnCutscene([[ Append("Mei:[E|Neutral] We're running down some leads, I think we're making real progress![B][C]") ]])
            fnCutscene([[ Append("Breanne: Shucks, I can't help but feel good for helping you out.[B][C]") ]])
            fnCutscene([[ Append("Florentina: Yeah you did a lot, staying here and fixing fences.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Happy] Ignore her, I'm very grateful.[P] Thanks again, Breanne.") ]])
        end
            
		--Flag.
		WD_SetProperty("Unlock Topic", "OutlandFarm", 1)
	
	--If for any reason Mei is a werecat and has not received the moonglow collar:
	elseif(iGotWerecatCollar == 0.0 and sMeiForm == "Werecat") then
	
		--Flags.
		VM_SetVar("Root/Variables/Chapter1/Breanne/iGotWerecatCollar", "N", 1.0)
		LM_ExecuteScript(gsItemListing, "Moonglow Collar")
		
		--Dialogue.
		fnCutscene([[ Append("Breanne: Oh, Mei, I was just thinking about you![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Surprise] ...[P] What is that glow in your pocket?[B][C]") ]])
		fnCutscene([[ Append("Breanne: Nothing gets past those eyes of yours, I see.[P] Here, take a look![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] This glows just like the moon...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] [SOUND|World|TakeItem](Received Moonglow Collar)[B][C]") ]])
		fnCutscene([[ Append("Breanne: I found this in the forest a while back.[P] It glows during the day if you leave it outside at night.[B][C]") ]])
		fnCutscene([[ Append("Breanne: I think it's called a Moonglow stone.[P] I think.[P] Not totally sure.[P] But I thought you'd like it![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Happy] It's beautiful, thank you![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] But I don't have a gift to give you in return...[B][C]") ]])
		fnCutscene([[ Append("Breanne: Just seein' you in such spirits is gift enough, darlin'.[B][C]") ]])
	
	--If none of the above cases trigger, check if Breanne can offer Mei the Pie quest. Mei cannot be a ghost or a slime.
	elseif(bIsFlorentinaPresent == true and iTalkedJob == 1.0 and iTalkedParents == 1.0 and iTakenPieJob == 0.0 and sMeiLevel >= 3 and sMeiForm ~= "Ghost" and sMeiForm ~= "Slime") then
	
		--Set flag.
		VM_SetVar("Root/Variables/Chapter1/Breanne/iTakenPieJob", "N", 1.0)
		
		--Unlock the pie topic with Florentina.
		WD_SetProperty("Unlock Topic", "Pepper Pie", 1)
	
		--Dialogue.
		fnCutscene([[ Append("Breanne: My stars, Mei![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Oh?[P] Did I do something wrong?[P] I'm sorry...[B][C]") ]])
		fnCutscene([[ Append("Breanne: Just the opposite, actually. I was just admiring your arms, there.[B][C]") ]])
		fnCutscene([[ Append("Breanne: You've been getting quite a lot of exercise, haven't you?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Happy] ..?[P] Oh, I suppose I have.[P] I guess constantly fighting for your life does that to you.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] This is Breanne's awful way of disguising admiration.[B][C]") ]])
		fnCutscene([[ Append("Breanne: Hush![P] I actually wanted to venture an idea.[B][C]") ]])
		fnCutscene([[ Append("Breanne: Mei, have you ever heard of Rondheim Pepper Pie?[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Surprise] !!![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] No, what's that?[B][C]") ]])
		fnCutscene([[ Append("Breanne: Well, I was thinking of ways to help you out on your quest, and - [P][CLEAR]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] We'll take it![B][C]") ]])
		fnCutscene([[ Append("Breanne: Well don't go jumping the gun, now.[P] I don't actually have all of the ingredients.[P] That's where you come in.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] Is anyone going to actually explain what this pie is?[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] It's a very powerful magic potion.[P] Very rare.[P] The ingredients are hard to track down, and you need to be an expert baker to get it right.[B][C]") ]])
		fnCutscene([[ Append("Breanne: Pshaw![P] It's not that hard![P] But, she's right about it being powerful magic.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] But didn't you say it was a pie?[B][C]") ]])
		fnCutscene([[ Append("Breanne: If you like drinking magic potions, then more power to you.[P] Personally it tastes like sweaty sock drippings to me.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] It's common practice to mix potions into baked goods to make them taste better.[P] Cakes, pies, donuts, you name it.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Happy] Oh![P] Okay![B][C]") ]])
		fnCutscene([[ Append("Breanne: I've got most of the ingredients on me, but there's a few more I need and I can't really leave the Pit Stop to get them.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] Let me see your list...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] Kokayanee, Gaardian Cave Moss, Decayed Nectar?[P] Yeah we can manage this stuff.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] We can?[P] I don't even know what half of these are![B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] Ask me at a rest stop and I'll give you a list of what we need and what I know about them.[P] There's quite a few.[B][C]") ]])
		fnCutscene([[ Append("Breanne: If you can get me all of these then I'll make you a nice Pepper Pie![P] Really lay the hurt on those baddies out there![B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] You better believe we're on the job.[P] C'mon, Mei.[B][C]") ]])
		fnCutscene([[ Append("Breanne: Good luck![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] *You really seem to want this Pepper Pie, Florentina.[P] Is it expensive?[P] Are you looking to sell it?*[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] *It'd fetch a hefty sum, sure, but I really just want to eat it.[P] It's better than sex.*[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] *You'll agree when you taste it, trust me.*[B][C]") ]])
	
	--If on the pie job and you have what you need:
	elseif(iTakenPieJob == 1.0) then
	
		--Item Variables.
		local iMossCount      = AdInv_GetProperty("Item Count", "Gaardian Cave Moss")
		local iPaperCount     = AdInv_GetProperty("Item Count", "Booped Paper")
		local iNectarCount    = AdInv_GetProperty("Item Count", "Decayed Bee Nectar")
		local iFlowerCount    = AdInv_GetProperty("Item Count", "Hypnotic Flower Petals")
		local iSalamiCount    = AdInv_GetProperty("Item Count", "Translucent Quantirian Salami")
		local iKokayaneeCount = AdInv_GetProperty("Item Count", "Kokayanee")
	
		--Party has everything:
		if(iMossCount > 0 and iPaperCount > 0 and iNectarCount > 0 and iFlowerCount > 0 and iSalamiCount > 0 and iKokayaneeCount > 0) then
            
            -- |[Achievement]|
            AM_SetPropertyJournal("Unlock Achievement", "FinishPie")
			
			--Dialogue.
			fnCutscene([[ Append("Florentina:[E|Happy] Look what we got![B][C]") ]])
			fnCutscene([[ Append("Breanne: You guys are fast![P] Hey, if the whole Earth thing doesn't work out, we could go into business together![P] We'd make a small fortune selling these![B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Happy] I might take you up on that.[P] Don't tempt me.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] So what do we do now?[B][C]") ]])
			fnCutscene([[ Append("Breanne: You patiently wait here while I do a bit of baking.[P] Make yourselves comfortable!") ]])
			fnCutsceneBlocker()
			
			--Wait a bit.
			fnCutsceneWait(45)
			fnCutsceneBlocker()
			
			--Fade out.
			fnCutscene([[ AL_SetProperty("Activate Fade", 90, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
			fnCutsceneBlocker()
			
			--Wait a while.
			fnCutsceneWait(240)
			fnCutsceneBlocker()
            
            --If Joanie is on the field, reposition her away.
            if(EM_Exists("Joanie") == true) then
                fnCutsceneTeleport("Joanie", 17.25, 3.50)
                fnCutsceneFace("Joanie", 1, 0)
            end
			
			--Reposition everyone.
			fnCutscene([[ AL_SetProperty("Open Door", "Kitchen Door") ]])
			Cutscene_CreateEvent("ActorEvent", "Actor")
				ActorEvent_SetProperty("Subject Name", "Mei")
				ActorEvent_SetProperty("Teleport To", (20.25 * gciSizePerTile), (15.50 * gciSizePerTile))
			DL_PopActiveObject()
			Cutscene_CreateEvent("ActorEvent", "Actor")
				ActorEvent_SetProperty("Subject Name", "Florentina")
				ActorEvent_SetProperty("Teleport To", (19.25 * gciSizePerTile), (15.50 * gciSizePerTile))
			DL_PopActiveObject()
			Cutscene_CreateEvent("ActorEvent", "Actor")
				ActorEvent_SetProperty("Subject Name", "Breanne")
				ActorEvent_SetProperty("Teleport To", (27.25 * gciSizePerTile), (17.50 * gciSizePerTile))
			DL_PopActiveObject()
			
			--Change facing.
			Cutscene_CreateEvent("ActorEvent", "Actor")
				ActorEvent_SetProperty("Subject Name", "Mei")
				ActorEvent_SetProperty("Face",  -1, 0)
			DL_PopActiveObject()
			Cutscene_CreateEvent("ActorEvent", "Actor")
				ActorEvent_SetProperty("Subject Name", "Florentina")
				ActorEvent_SetProperty("Face",  1, 0)
			DL_PopActiveObject()
			Cutscene_CreateEvent("ActorEvent", "Actor")
				ActorEvent_SetProperty("Subject Name", "Breanne")
				ActorEvent_SetProperty("Face",  1, 0)
			DL_PopActiveObject()
			fnCutsceneBlocker()
			
			--Fade back in.
			fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
			fnCutsceneBlocker()
			
			--Wait a bit.
			fnCutsceneWait(30)
			fnCutsceneBlocker()
			
			--Breanne dialogue:
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ Append("Breanne:[VOICE|Breanne] Finished![P] And it smells great!") ]])
			fnCutsceneBlocker()
			
			--Breanne walks out of the kitchen.
			Cutscene_CreateEvent("ActorEvent", "Actor")
				ActorEvent_SetProperty("Subject Name", "Mei")
				ActorEvent_SetProperty("Face",  1, 0)
			DL_PopActiveObject()
			Cutscene_CreateEvent("ActorEvent", "Actor")
				ActorEvent_SetProperty("Subject Name", "Breanne")
				ActorEvent_SetProperty("Move To", (24.25 * gciSizePerTile), (16.50 * gciSizePerTile))
			DL_PopActiveObject()
			Cutscene_CreateEvent("ActorEvent", "Actor")
				ActorEvent_SetProperty("Subject Name", "Breanne")
				ActorEvent_SetProperty("Move To", (22.25 * gciSizePerTile), (16.50 * gciSizePerTile))
			DL_PopActiveObject()
			Cutscene_CreateEvent("ActorEvent", "Actor")
				ActorEvent_SetProperty("Subject Name", "Breanne")
				ActorEvent_SetProperty("Move To", (21.25 * gciSizePerTile), (15.50 * gciSizePerTile))
			DL_PopActiveObject()
			fnCutsceneBlocker()
			
			--Wait a bit.
			fnCutsceneWait(30)
			fnCutsceneBlocker()
			
			Cutscene_CreateEvent("ActorEvent", "Actor")
				ActorEvent_SetProperty("Subject Name", "Florentina")
				ActorEvent_SetProperty("Move To", (20.25 * gciSizePerTile), (15.50 * gciSizePerTile))
			DL_PopActiveObject()
			fnCutsceneBlocker()
			fnCutscene([[ AL_SetProperty("Fold Party") ]])
			fnCutsceneBlocker()
			
			--Dialogue.
			fnStandardMajorDialogue()
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Breanne", "Neutral") ]])
			fnCutscene([[ Append("Breanne: Who wants the first taste?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Happy] Me, me![B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Neutral] Yeah, you go ahead, kid.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] *nom*[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] (...)[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Happy] (...)[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Laugh] (...)[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Laugh] I think I'm going to explode out of sheer pleasure![B][C]") ]])
			fnCutscene([[ Append("Breanne: Have another bite![B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] *nom*[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Laugh] Oof, now I might explode, and then explode again![B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Surprise] Can you do that?[P] Can you explode twice?[B][C]") ]])
			fnCutscene([[ Append("Breanne: Of course not![B][C]") ]])
			fnCutscene([[ Append("Breanne: Well the pie was a huge success, and it's all thanks to you.[P] Nice work, everyone![B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Happy] This stuff is a powerful potion, Mei.[P] It'll make those desperate battles just a little less desperate.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] As if I needed more reason to eat it![B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Thanks a ton, Breanne![P] This is great![B][C]") ]])
			fnCutscene([[ Append("Breanne: Just hoping it helps you along.[P] Good luck out there![B][C]") ]])
			fnCutscene([[ Append("Breanne: Now was there anything else I could do for ya?[B][C]") ]])
			
			--Gain the pie.
			LM_ExecuteScript(gsItemListing, "Pepper Pie")
			
			--Remove the other items.
			AdInv_SetProperty("Remove Item", "Gaardian Cave Moss")
			AdInv_SetProperty("Remove Item", "Booped Paper")
			AdInv_SetProperty("Remove Item", "Decayed Bee Nectar")
			AdInv_SetProperty("Remove Item", "Hypnotic Flower Petals")
			AdInv_SetProperty("Remove Item", "Translucent Quantirian Salami")
			AdInv_SetProperty("Remove Item", "Kokayanee")
			
			--Set variable.
			VM_SetVar("Root/Variables/Chapter1/Breanne/iTakenPieJob", "N", 2.0)
			
		--Default case:
		else
			fnCutscene([[ Append("Breanne: Howdy, Mei![P] What can I do for ya?[B][C]") ]])
		end
	
	--Default case, no special flags.
	else
		fnCutscene([[ Append("Breanne: Howdy, Mei![P] What can I do for ya?[B][C]") ]])
	end

	-- |[Topics]|
	--Activate topics mode once the dialogue is complete.
	fnCutscene([[ WD_SetProperty("Activate Topics After Dialogue", "Breanne") ]])
end
