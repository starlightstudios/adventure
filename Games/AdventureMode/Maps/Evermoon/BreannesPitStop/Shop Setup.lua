-- |[ ================================== Breanne's Shop Setup ================================== ]|
--Not fully set yet.

-- |[Item Stock Listing]|
--Adamantite
AM_SetShopProperty("Add Item", "Adamantite Powder",  -1, -1)
AM_SetShopProperty("Add Item", "Bronze Katana",      -1, -1)
AM_SetShopProperty("Add Item", "Troubadour's Robe",  -1, -1)
AM_SetShopProperty("Add Item", "Healing Tincture",   -1, -1)
AM_SetShopProperty("Add Item", "Palliative",         -1, -1)
AM_SetShopProperty("Add Item", "Piorose Gem",        -1, -1)
AM_SetShopProperty("Add Item", "Nockrion Gem",       -1, -1)
