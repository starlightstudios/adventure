-- |[ ===================================== Dialogue Script ==================================== ]|
--Used for Mycela.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
    
    -- |[Variables]|
    local iTalkedToMycela = VM_GetVar("Root/Variables/Chapter1/Mycela/iTalkedToMycela", "N")
    local iHasJob_Agarist = VM_GetVar("Root/Variables/Global/Florentina/iHasJob_Agarist", "N")
    
    -- |[Flags]|
    VM_SetVar("Root/Variables/Chapter1/Mycela/iNadiaToldOfMycela", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter1/Mycela/iTalkedToMycela", "N", 1.0)
    
    -- |[Introduction]|
    if(iTalkedToMycela == 0.0) then
        WD_SetProperty("Unlock Topic", "Future Plans", 1)
        fnStandardMajorDialogue()
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Mycela", "Demushed") ]])
        fnCutscene([[ Append("Mycela:[E|Demushed] Ah![P] Hello![P] Oh my goodness, it is so very nice to meet you properly.[B][C]") ]])
        fnCutscene([[ Append("Mycela:[E|Demushed] Before you say anything, I must make it absolutely clear that I apologize for everything I did.[P] I am attempting to make amends as best I can.[B][C]") ]])
        fnCutscene([[ Append("Mycela:[E|Demushed] If Rochea has not told you already, they gave me the name Mycela.[P] I think it suits me.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Smirk] Glad to see you're doing well.[B][C]") ]])
        fnCutscene([[ Append("Mycela:[E|Demushed] Rochea and her friends have told me all about the little ones.[P] I -[P] again, I find myself apologizing.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Smirk] It's okay, I forgive you.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Happy] I don't.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Offended] Come on, Florentina![B][C]") ]])
        fnCutscene([[ Append("Mycela:[E|Demushed] What must I do to earn your forgiveness for the trouble I caused?[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Happy] 2000 platina sound nice?[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Offended] No![P] Mycela, don't listen to her.[P] She forgives you.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Surprise] Yeah and I'd forgive her even harder for 1500 platina![B][C]") ]])
        fnCutscene([[ Append("Mycela:[E|Demushed] Ah yes, Nadia warned me about you.[P] I will find another way to earn your forgiveness, as I am short on money.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] Stupid bigmouth Nadia...[B][C]") ]])
        fnCutscene([[ Append("Mycela:[E|Demushed] My education in the ways of the wilds is still ongoing, but I would like to help you understand what happened beneath the monastery as best I can.[B][C]") ]])
        fnCutscene([[ Append("Mycela:[E|Demushed] I -[P] do not remember who I was before I woke up there.[P] I am sorry.[P] But I will do my best to answer all your questions.[B][C]") ]])
        if(iHasJob_Agarist == 0.0) then
            fnCutscene([[ Append("Mycela:[E|Demushed] Oh, Florentina.[P] A friend of mine had a business proposition for you.[P] I should have said that earlier.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Happy] Business proposition?[P] Now we're talking.[B][C]") ]])
            WD_SetProperty("Unlock Topic", "Business Offer", 1)
        end
    
    -- |[Repeats]|
    else
        fnStandardMajorDialogue()
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Mycela", "Demushed") ]])
        fnCutscene([[ Append("Mycela:[E|Demushed] Ask me anything, I will answer to the best of my abilities.[B][C]") ]])
    end
    
	-- |[Topics]|
	--Activate topics mode once the dialogue is complete.
	fnCutscene([[ WD_SetProperty("Activate Topics After Dialogue", "Mycela") ]])
end