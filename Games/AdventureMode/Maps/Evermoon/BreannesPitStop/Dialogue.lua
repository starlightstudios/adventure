-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

--Clear censor bars.
WD_SetProperty("Clear Censor Bars")

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[Name Resolve]|
    --Resolve the name of the calling entity.
    local sActorName = TA_GetProperty("Name")

    --Facing.
	TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[Werecat]|
    if(sActorName == "Werecat") then
        
        --Variables.
        local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
        
        --If Mei is not a werecat:
        if(sMeiForm ~= "Werecat") then
            fnStandardDialogue("Werecat:[VOICE|Werecat] Hss![P] Sod off!")
        
        --If Mei is a werecat:
        else
            fnStandardDialogue("Werecat:[VOICE|Werecat] Find your own fishing spot, kinfang.[P] This one is mine.")
        end
    
    -- |[Chair Man]|
    elseif(sActorName == "Chair Man") then
        fnStandardDialogue("Suitor:[VOICE|MercM] You just passing through?[P] Try some of Breanne's chowder before you go!")
    
    -- |[Suitor]|
    elseif(sActorName == "Suitor") then
        fnStandardDialogue("Suitor:[VOICE|MercM] Miss Breanne is awful pretty, but she don't seem to want nothin' to do with me...")
        
    -- |[Nadia]|
    elseif(sActorName == "Nadia") then
        fnStandardDialogue("Nadia:[VOICE|Nadia] I have to get back to the trading post.[P] Sorry I can't help with what Rochea needs you to do.")
    
    -- |[Mycela]|
    elseif(sActorName == "Mycela") then
        LM_ExecuteScript(fnResolvePath() .. "Dialogue Mycela.lua", sTopicName)
    
    -- |[Alraune]|
    elseif(sActorName == "Alraune") then
    
        --Variables.
        local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
        local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
        
        --If Mei is an Alraune:
        if(sMeiForm == "Alraune") then
        
            --Dialogue.
            fnStandardMajorDialogue()
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Alraune", "Neutral") ]])
            fnCutscene([[ Append("Alraune: Greetings, leaf-sister.[P] I have been looking for those willing to be joined, but have found nothing here.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] You trust the humans who come through here?[B][C]") ]])
            fnCutscene([[ Append("Alraune: I do not, nor need I.[P] There is a magical field which repulses those who would do harm.[B][C]") ]])
            fnCutscene([[ Append("Alraune: Ask the innkeeper of it, she will tell you.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] Have you offered to join Breanne?[P] I think she would be a fine addition.[B][C]") ]])
            fnCutscene([[ Append("Alraune: Mm.[P] She is not interested.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Blush] Perhaps when she takes a trip to the lakeside, we could ambush her.[B][C]") ]])
            
            --If Florentina is not present:
            if(bIsFlorentinaPresent == false) then
                fnCutscene([[ Append("Alraune: I will pass the idea to the others.[P] Do not underestimate her, she is a capable fighter.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Happy] So much the better that we join her, then.[P] She might serve to protect the vulnerable.[B][C]") ]])
                fnCutscene([[ Append("Alraune: Indeed.[P] It has been a pleasure, leaf-sister.") ]])
            
            --If Florentina is present:
            else
                fnCutscene([[ Append("Florentina:[E|Offended] Mei![P] What in the wastes is wrong with you?[B][C]") ]])
                fnCutscene([[ Append("Alraune: Florentina is not like us.[P] She may intervene.[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Confused] You're damn right I will![B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Sad] S-[P]sorry Florentina.[P] I just -[P] I think she'd make a great leaf-sister.[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Confused] Over my dead body![B][C]") ]])
                fnCutscene([[ Append("Alraune: If her joining would bring such discord, we must forego it.[P] I will attempt to convince her further.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] I hope you're successful.") ]])
            end
        
            --Topic unlock.
            WD_SetProperty("Unlock Topic", "PitStopMagicField", 1)
        
        --If Mei is a Bee:
        elseif(sMeiForm == "Bee") then
            fnStandardDialogue("Alraune:[VOICE|Alraune] Mm, little bee, I would love to nuzzle you, but I have tasks I must see to.[P] Perhaps later.")
        
        --If Mei is a ghost:
        elseif(sMeiForm == "Ghost") then
            fnStandardDialogue("Alraune:[VOICE|Alraune] An...[P] interesting one, you are.[P] Please, not too close...")
        
        --If Mei is a Slime:
        elseif(sMeiForm == "Slime") then
            fnStandardDialogue("Alraune:[VOICE|Alraune] Hello, little slime.[P] Aren't you cute?")
        
        --If Mei is a werecat:
        elseif(sMeiForm == "Werecat") then
            fnStandardDialogue("Alraune:[VOICE|Alraune] Ah, a hunter in the night![P] I hope your pursuits go well.")
        
        --If Mei is a gravemarker:
        elseif(sMeiForm == "Gravemarker") then
            fnStandardMajorDialogue()
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Alraune", "Neutral") ]])
            fnCutscene([[ Append("Alraune: What sort of creature are you, if I may ask?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] I am a gravemarker.[P] I stand watch over places where angels rest.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] Or I would, but I have things to do.[B][C]") ]])
            fnCutscene([[ Append("Alraune: I see.[P] Do you want to keep watch?[P] Is it a choice you made?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] I feel it within myself that I do, but I know it was a lie imprinted upon me.[P] It's complicated.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] If you see any others like me, they are a lot less conversational.[B][C]") ]])
            fnCutscene([[ Append("Alraune: I'll give them their space.") ]])
        
        --If Mei is a wisphag:
        elseif(sMeiForm == "Wisphag") then
            fnStandardDialogue("Alraune:[VOICE|Alraune] Oh, ones like you do not wander far from the swamps.[P] I have observed you from a distance.[P] Your hearts are pure, I wish you well.")
        
        --Mannequin.
        elseif(sMeiForm == "Mannequin") then
            fnStandardDialogue("Alraune:[VOICE|Alraune] What an uncanny thing.[P] Did someone put it here when I was daydreaming?")
        
        --Human.
        elseif(sMeiForm == "Human") then
        
            --Variables.
            local iHasAlrauneForm           = VM_GetVar("Root/Variables/Global/Mei/iHasAlrauneForm", "N")
            local iMeiVolunteeredToAlraune  = VM_GetVar("Root/Variables/Chapter1/Scenes/iMeiVolunteeredToAlraune", "N")
            local iFlorentinaKnowsAboutRune = VM_GetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsAboutRune", "N")
            local iMeiJoinedSuitor          = VM_GetVar("Root/Variables/Chapter1/Breanne/iMeiJoinedSuitor", "N")
        
            --Dialogue.
            fnStandardMajorDialogue()
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Alraune", "Neutral") ]])
            
            
            --Joined the suitor:
            if(iMeiJoinedSuitor == 1.0) then
            
                fnCutscene([[ Append("Alraune: Leaf-sister Mei, it is good to see you again.[B][C]") ]])
                fnCutscene([[ Append("Alraune: Our new sister is still being cleansed, but I intend to bring her here when she is ready.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Happy] To show everyone the benefits?[B][C]") ]])
                fnCutscene([[ Append("Alraune: Indeed.[P] When they see how joyous it is to be a plant, I'm sure they will reconsider.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Happy] I wish you luck!") ]])
            
            
            --Mei has Alraune form:
            elseif(iHasAlrauneForm == 1.0) then
                
                --Common:
                fnCutscene([[ Append("Alraune: Hello, human.[P] I have been seeking those who would walk with nature.[B][C]") ]])
                fnCutscene([[ Append("Alraune: Perhaps you would be willing?[P] I can make arrangements, we would not need to go far.[B][C]") ]])
                    
                --Set this flag.
                VM_SetVar("Root/Variables/Chapter1/Breanne/iMeiWillJoinSuitors", "N", 1.0)
                
                --Mei volunteered:
                if(iMeiVolunteeredToAlraune == 1.0) then
                    fnCutscene([[ Append("Mei:[E|Happy] You don't recognize me, leaf-sister?[B][C]") ]])
                    fnCutscene([[ Append("Alraune: L-[P]Leaf-sister Mei?[P] How is this possible!?[P] I was at your joining![B][C]") ]])
                    fnCutscene([[ Append("Alraune: ...[B][C]") ]])
                    fnCutscene([[ Append("Alraune: Ah, I see.[P] The little ones tell me you possess some sort of magic beyond my understanding.[B][C]") ]])
                
                --Involuntary:
                else
                    fnCutscene([[ Append("Mei:[E|Happy] My heart already beats in time with the forest.[B][C]") ]])
                    fnCutscene([[ Append("Alraune: Truly?[B][C]") ]])
                    fnCutscene([[ Append("Alraune: Ah, I see.[P] The little ones speak of you, leaf-sister Mei.[B][C]") ]])
                end
                fnCutscene([[ Append("Alraune: Perhaps we can use this to the advantage of the wilds?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] How do you mean?[B][C]") ]])
                fnCutscene([[ Append("Alraune: The humans here implicitly distrust me, but they may put faith in you.[P] Tell them the benefits of our path.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] I don't think they'll listen to me, but I could try.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Blush] But, if someone elicits to walk with us...[P] May I be the one to join them?[B][C]") ]])
                fnCutscene([[ Append("Alraune: I would not deny you the honor.[P] You would certainly have earned it.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] I am merely a servant.[B][C]") ]])
                fnCutscene([[ Append("Alraune: Good luck in your endeavours.") ]])
                fnCutsceneBlocker()
                
                --If Florentina is present, and she knows about the runestone.
                if(bIsFlorentinaPresent == true and iFlorentinaKnowsAboutRune == 1.0) then
                    fnCutscene([[ WD_SetProperty("Show") ]])
                    fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
                    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
                    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
                    fnCutscene([[ Append("Florentina:[E|Neutral] Hey, Mei?[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] Yes?[B][C]") ]])
                    fnCutscene([[ Append("Florentina:[E|Neutral] I normally wouldn't care about you tricking some fools into being joined.[P][EMOTION|Florentina|Happy] Hell, I might help if I thought it would be funny.[B][C]") ]])
                    fnCutscene([[ Append("Florentina:[E|Neutral] But don't you think it's kinda sick?[P] It's not something they can undo.[P] They don't have the rune that you do.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Blush] I love what I am -[P] or, what I will be, once this is all over.[P] This human body is so...[P] imperfect...[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] I know that, once they feel it, they would never want to go back.[P] I'd feel no guilt.[B][C]") ]])
                    fnCutscene([[ Append("Florentina:[E|Confused] So then how do you explain me, huh?[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] Do you want to become a human again?[B][C]") ]])
                    fnCutscene([[ Append("Florentina:[E|Neutral] ...[P] No, but...[P] you wouldn't understand why.[P] It is...[P][CLEAR]") ]])
                    fnCutscene([[ Append("Mei:[E|Sad] Painful?[B][C]") ]])
                    fnCutscene([[ Append("Florentina:[E|Confused] Just, shut up, Mei.[P] Let's go.") ]])
                
                --If Florentina is present, but doesn't know about the runestone.
                elseif(bIsFlorentinaPresent == true and iFlorentinaKnowsAboutRune == 0.0) then
                    fnCutscene([[ WD_SetProperty("Show") ]])
                    fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
                    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
                    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
                    fnCutscene([[ Append("Florentina:[E|Neutral] Mei, have you been sneaking hits off my pipe when I'm not looking?[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] The way you abuse that thing?[P] Just when is it not in your hands?[B][C]") ]])
                    fnCutscene([[ Append("Florentina:[E|Neutral] So if that's a no, what's this crap the little ones are saying?[P] Why are you acting all flowery?[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] Perhaps I'll show you when we reach a campfire...[B][C]") ]])
                    fnCutscene([[ Append("Florentina:[E|Neutral] You better.[P] I don't like being outside the loop.") ]])
                end
            
            --Mei does not have Alraune form:
            else
                
                --Common:
                fnCutscene([[ Append("Alraune: Hello, human.[P] I have been seeking those who would walk with nature.[B][C]") ]])
                fnCutscene([[ Append("Alraune: Perhaps you would be willing?[P] I can make arrangements, we would not need to go far.[B][C]") ]])
                fnCutscene([[ Append("Alraune: The joining process would be most pleasurable, I assure you.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Joining?[P] Is that what it's called?[B][C]") ]])
            
                --If Florentina is not present:
                if(bIsFlorentinaPresent == false) then
                    fnCutscene([[ Append("Alraune: Yes.[P] Our most skilled leaf-sister will immerse you in a special mixture of pollen, water, and our magic.[B][C]") ]])
                    fnCutscene([[ Append("Alraune: You will become one with the forest.[P] You will hear the whispers of the flowers.[B][C]") ]])
                    fnCutscene([[ Append("Alraune: Even now they call to you.[P] They wish to befriend you, for you to join their family.[B][C]") ]])
                
                --If Florentina is present:
                else
                    fnCutscene([[ Append("Florentina:[E|Offended] Yeah, no.[B][C]") ]])
                    fnCutscene([[ Append("Florentina:[E|Confused] Don't let her sweet-talk you, Mei.[P] It's not all sunshine and rainbows.[B][C]") ]])
                    fnCutscene([[ Append("Alraune: You, Florentina, have forsaken the gifts given to you.[B][C]") ]])
                    fnCutscene([[ Append("Florentina:[E|Confused] Skepticism is healthy.[P] Mei, c'mon.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Blush] I don't know...[BLOCK]") ]])
                end
            
                --Dialogue option.
                local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
                fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
                fnCutscene(" WD_SetProperty(\"Add Decision\", \"Be Joined\", " .. sDecisionScript .. ", \"BeJoined\") ")
                fnCutscene(" WD_SetProperty(\"Add Decision\", \"No Thanks\",  " .. sDecisionScript .. ", \"Nope\") ")
                fnCutsceneBlocker()
            end
        end
        
    -- |[Pliable Man]|
    elseif(sActorName == "Pliable Man") then
    
        --Variables.
        local sMeiForm              = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
        local iMeiWillJoinSuitors   = VM_GetVar("Root/Variables/Chapter1/Breanne/iMeiWillJoinSuitors", "N")
        local bIsFlorentinaPresent  = fnIsCharacterPresent("Florentina")
        local iMeiKnowsAboutSuitors = VM_GetVar("Root/Variables/Chapter1/Breanne/iMeiKnowsAboutSuitors", "N")
        
        --Normal case:
        if(iMeiWillJoinSuitors == 0.0 or iMeiKnowsAboutSuitors == 0.0) then
            fnStandardDialogue("Suitor:[VOICE|MercM] Hello there, beautiful...[P] *hic*")
        
        --Mei wants to join this guy:
        elseif(iMeiKnowsAboutSuitors == 1.0 and iMeiKnowsAboutSuitors == 1.0) then
        
            --Major dialogue:
            fnStandardMajorDialogue()
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "MercM", "Neutral") ]])
        
            --If Mei is something other than human:
            if(sMeiForm ~= "Human") then
                fnCutscene([[ Append("Suitor: Hello there, beautiful...[P] *hic*[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Well hello to you.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Out of curiosity, you wouldn't be...[P] here for Breanne, would you?[B][C]") ]])
                fnCutscene([[ Append("Suitor: 'Coursh.[P] Her folksh are loaded, and she's a mite pretty, too.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Smirk] Well you know...[B][C]") ]])
                fnCutscene([[ Append("Suitor: Shorry darlin *hic*, but I ain't into that.[P] I like my ladies to be, ladies, you know?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] (Dang it...)") ]])
                fnCutsceneBlocker()
        
            --If Mei is human:
            else
            
                fnCutscene([[ Append("Suitor: Hello there, beautiful...[P] *hic*[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Well hello to you.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Out of curiosity, you wouldn't be...[P] here for Breanne, would you?[B][C]") ]])
                fnCutscene([[ Append("Suitor: 'Coursh.[P] Her folksh are loaded, and she's a mite pretty, too.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Smirk] (This guy's clearly drunk.[P] Probably wouldn't be that hard to lure him away and join him...)[BLOCK]") ]])
                
                --Decision script is this script. It must be surrounded by quotes.
                local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
                fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
                fnCutscene(" WD_SetProperty(\"Add Decision\", \"Join Him\", " .. sDecisionScript .. ", \"Join\") ")
                fnCutscene(" WD_SetProperty(\"Add Decision\", \"Leave Him\",  " .. sDecisionScript .. ", \"Leave\") ")
                fnCutsceneBlocker()
            end
        end

    -- |[Joanie]|
    elseif(sActorName == "Joanie") then
    
        --This is always a major dialogue.
        fnStandardMajorDialogue()
        
        --Hypatia takes slot 5.
        fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Bee", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Breanne", "Neutral") ]])
        
        --Variables.
        local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
        
        --If Mei is a bee during this conversation:
        if(sMeiForm == "Bee") then
            fnCutscene([[ Append("Drone: [FOCUS|Bee](Sister Drone.)[B][C]") ]])
            fnCutscene([[ Append("Mei: (Sister Drone.)[B][C]") ]])
            fnCutscene([[ Append("Drone: [FOCUS|Bee](We enjoy spending time with Breanne.[P] We remember Breanne.)[B][C]") ]])
            fnCutscene([[ Append("Mei: (We enjoy Breanne's company.)[B][C]") ]])
            fnCutscene([[ Append("Drone: [FOCUS|Bee](We would like to convert Breanne.[P] We would enjoy her.[P] She would make a good drone.)[B][C]") ]])
            fnCutscene([[ Append("Mei: (We agree.[P] She would be a good drone.)[B][C]") ]])
            fnCutscene([[ Append("Mei: (We cannot convert Breanne.[P] We should not convert Breanne.[P] We must not convert Breanne.)[B][C]") ]])
            fnCutscene([[ Append("Drone: [FOCUS|Bee](We understand.[P] We will enjoy her company.[P] Thank you for reuniting us, Sister Drone.)[B][C]") ]])
            fnCutscene([[ Append("Mei: (We are happy to help.)[B][C]") ]])
            fnCutscene([[ Append("Breanne: Are you guys talking or something?[B][C]") ]])
            fnCutscene([[ Append("Mei: Yes.[P] When our antennae are moving like that, it means we're talking.[B][C]") ]])
            fnCutscene([[ Append("Breanne: Joanie won't talk to me verbally, but I guess I don't need her to.[P] It's just nice having her here.[B][C]") ]])
            fnCutscene([[ Append("Breanne: Joanie, you feel free to look for nectar in my garden.[B][C]") ]])
            fnCutscene([[ Append("Mei: I'll ask the hive to give her foraging preference for this area.[B][C]") ]])
            fnCutscene([[ Append("Breanne: Thanks a lot, Mei.[P] This means so much...") ]])
        
        else
            fnCutscene([[ Append("Drone: [FOCUS|Bee]Bzzz.[P] (Sister Drone.)[B][C]") ]])
            fnCutscene([[ Append("Mei: Zzz.[P] Bzzz.[P] (Hello Sister Drone. I lack antennae, but we may speak verbally.)[B][C]") ]])
            fnCutscene([[ Append("Breanne: Are you guys talking?[B][C]") ]])
            fnCutscene([[ Append("Mei: Yes.[P] I can...[P] I don't know how, but I understand her.[B][C]") ]])
            fnCutscene([[ Append("Drone: [FOCUS|Bee]Zzz.[P] Zzzz.[P] (The hive has difficulty recognizing you. We apologize for any attempts to re-add you.)[B][C]") ]])
            fnCutscene([[ Append("Mei: Bzzz.[P] Bzzz.[P] (I must remain like this for a while longer.)[B][C]") ]])
            fnCutscene([[ Append("Breanne: How do you do that?[P] Can you teach me?[B][C]") ]])
            fnCutscene([[ Append("Mei: I guess a part of me is still a bee.[P] Maybe it'll be that way forever.[P] I don't think I could teach you, it's not like learning a language.[B][C]") ]])
            fnCutscene([[ Append("Mei: We sort of understand each other's intentions.[P] I know what she's thinking because that's what I'd be thinking if I were her.[B][C]") ]])
            fnCutscene([[ Append("Mei: I can tell you she's very happy to see you.[B][C]") ]])
            fnCutscene([[ Append("Breanne: Joanie...[P] I missed you.[B][C]") ]])
            fnCutscene([[ Append("Mei: She'll come and see you whenever she can.[B][C]") ]])
            fnCutscene([[ Append("Breanne: Thanks a lot, Mei.[P] This means so much...") ]])
        end
        fnCutsceneBlocker()
    end

-- |[ ================================ Responses to the Alraune ================================ ]|
--Nope.
elseif(sTopicName == "Nope") then

	--Variables.
	local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")

	--Clean.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnCutscene([[ WD_SetProperty("FastShow") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	
	--Florentina is not present:
	if(bIsFlorentinaPresent == false) then
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Alraune", "Neutral") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] I appreciate the offer, but I'll have to decline.[B][C]") ]])
		fnCutscene([[ Append("Alraune: It is the response I am accustomed to.[P] Very well.[P] If you change your mind, seek out a daughter of the wild.") ]])
		fnCutsceneBlocker()

	--Florentina is present, and knows about the runestone.
	else
		fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Alraune", "Neutral") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] I appreciate the offer, but I'll have to decline.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] Good call.[B][C]") ]])
		fnCutscene([[ Append("Alraune: It is the response I am accustomed to.[P] Very well.[P] If you change your mind, seek out a daughter of the wild.") ]])
		fnCutsceneBlocker()
	end

--Mei decides to become an Alraune.
elseif(sTopicName == "BeJoined") then

	--Variables.
	local bIsFlorentinaPresent      = fnIsCharacterPresent("Florentina")
	local iFlorentinaKnowsAboutRune = VM_GetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsAboutRune", "N")
	
	--Set flags.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iMeiVolunteeredToAlraune", "N", 1.0)

	--Clean.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnCutscene([[ WD_SetProperty("FastShow") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	
	--Florentina is not present:
	if(bIsFlorentinaPresent == false) then
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Alraune", "Neutral") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] I...[P] want it...[P] I want to be happy...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] I didn't have a lot of friends back home.[P] Not real friends.[B][C]") ]])
		fnCutscene([[ Append("Alraune: Every blade of grass will be your friend when you are joined.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] I'm just so nervous...[B][C]") ]])
		fnCutscene([[ Append("Alraune: Whatever for?[P] You will be happy and secure.[P] You will not age or falter from disease.[P] You will be more than human.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] ...[P] Let's go.[P] Let's not waste any more time.[B][C]") ]])
		fnCutscene([[ Append("Alraune: Stupendous![P] I will send word ahead.[P] Please, follow me.") ]])
		fnCutsceneBlocker()

	--Florentina is present, and knows about the runestone.
	elseif(bIsFlorentinaPresent == true and iFlorentinaKnowsAboutRune == 1.0) then
		fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Alraune", "Neutral") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] I...[P] want it...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Surprise] Mei, have you lost your mind?[P] Think of what you're giving up![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] It'll be all right.[P] Runestone, remember?[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] ...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] Yeah, I can't argue with that.[P] It's almost like you're running a con.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] If I don't like it...[B][C]") ]])
		fnCutscene([[ Append("Alraune: You will love every moment of your life when you are joined.[P] I speak from experience.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] And you speak for yourself exclusively.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] All right, let's go.[B][C]") ]])
		fnCutscene([[ Append("Alraune: Wanderer Florentina...[P] you will not be present.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Surprise] ![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Surprise] Why not?[B][C]") ]])
		fnCutscene([[ Append("Alraune: She is not like us.[P] She is uncleansed.[P] She may attempt to sabotage the process.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Offended] You haughty scum![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] But...[P][CLEAR]") ]])
		fnCutscene([[ Append("Alraune: It is the only way forward.[P] I am sorry.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] Florentina, would you...[P][CLEAR]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] Yeah.[P] I'll wait outside when we get there.[P] As if I wanted to associate with your jerks anyway.") ]])
		fnCutsceneBlocker()

	--Florentina is present, and does not know about the runestone.
	elseif(bIsFlorentinaPresent == true and iFlorentinaKnowsAboutRune == 0.0) then
		fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Alraune", "Neutral") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] I...[P] want it...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Surprise] Mei, have you lost your mind?[P] Think of what you're giving up![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] ..?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] I'm not giving up anything.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Surprise] Your humanity is at stake![P] You won't be you anymore![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] No, I will.[P] I can feel it.[P] I -[P] my runestone...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] I can't explain it.[P] I'll show you.[P] Afterwards.[B][C]") ]])
		fnCutscene([[ Append("Alraune: Please, come.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Lead the way.[B][C]") ]])
		fnCutscene([[ Append("Alraune: Wanderer Florentina...[P] you will not be present.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Surprise] ![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Surprise] Why not?[B][C]") ]])
		fnCutscene([[ Append("Alraune: She is not like us.[P] She is uncleansed.[P] She may attempt to sabotage the process.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Offended] You haughty scum![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] But...[P][CLEAR]") ]])
		fnCutscene([[ Append("Alraune: It is the only way forward.[P] I am sorry.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] Florentina, would you...[P][CLEAR]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] Yeah.[P] I'll wait outside when we get there.[P] As if I wanted to associate with your jerks anyway.") ]])
		fnCutsceneBlocker()
	end
	
	--Fade to black.
	fnCutscene([[ AL_SetProperty("Activate Fade", 60, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneBlocker()
	fnCutsceneWait(90)
	fnCutsceneBlocker()
	
	--Mini dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Alraune", "Neutral") ]])
	fnCutscene([[ Append("Alraune: New sister.[P] Please, breathe my pollen.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Hmm?[B][C]") ]])
	fnCutscene([[ Append("Alraune: Your body must be pliable.[P] This pollen will help.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Blush] Ooooh... I feel so...[P] sleepy...") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(90)
	fnCutsceneBlocker()
	
	--Execute the Alraune cutscene.
	fnCutscene([[ LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/200 Defeat/Defeat_Alraune/Scene_Begin.lua") ]])
    
-- |[ ============================== Responses to the Pliable Man ============================== ]|
--Mei decides to join the idiot.
elseif(sTopicName == "Join") then

	--Variables.
	local bIsFlorentinaPresent  = fnIsCharacterPresent("Florentina")
		
    --Flag.
    VM_SetVar("Root/Variables/Chapter1/Breanne/iMeiJoinedSuitor", "N", 1.0)
	
	--Clean.
	WD_SetProperty("Hide")
	
	--Dialogue Setup.
	fnCutscene([[ WD_SetProperty("FastShow") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	if(bIsFlorentinaPresent == true) then
		fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
	else
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	end
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "MercM", "Neutral") ]])
	
	--Talking.
	fnCutscene([[ Append("Mei:[E|Neutral] And you don't think I'm a 'mite pretty'?[P] I'm crushed![B][C]") ]])
	fnCutscene([[ Append("Suitor: Now I didn't shay that, missy.[P] In fact...[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Blush] You like what you see?[P] Maybe if I lean over a bit?[B][C]") ]])
	
	--If Florentina is not present:
	if(bIsFlorentinaPresent == false) then
		fnCutscene([[ Append("Mei:[E|Smirk] (What a sucker![P] Being drunk probably isn't hurting...)[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] Oh yeah, just -[P] put your hand right there.[P] That's right.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] You don't need to think about any girl but me, right, handsome?[B][C]") ]])
		fnCutscene([[ Append("Suitor: Ooh my...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Smirk] But not here.[P] Meet me at my cabin, it's just south of here.[B][C]") ]])
		fnCutscene([[ Append("Suitor: Y-[P]yeah![P] I'll be there![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Smirk] Don't keep a girl waiting.[P] And...[P] bring a bit of wine for me.[B][C]") ]])
		fnCutscene([[ Append("Suitor: Sure thing, missy![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Happy] (What an idiot![P] This is too easy!)") ]])
		fnCutsceneBlocker()
		
	--If Florentina is present:
	else
		fnCutscene([[ Append("Florentina:[E|Neutral] *Mei, what are you doing?*[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] *What does it look like?*[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] *Something I'll be putting an end to!*[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] Hey, wino.[P] She's - [P][CLEAR]") ]])
		fnCutscene([[ Append("Suitor: Heh, no offense missy, but this lady over here has my attentions.[B][C]") ]])
		fnCutscene([[ Append("Suitor: When a man has options, he takes the best one.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] [P].[P].[P].[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] *You do your thing.[P] I'll be over here.*[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Oh yeah, just -[P] put your hand right there.[P] That's right.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] You don't need to think about any girl but me, right, handsome?[B][C]") ]])
		fnCutscene([[ Append("Suitor: Ooh my...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] But not here.[P] Meet me at my cabin, it's just south of here.[B][C]") ]])
		fnCutscene([[ Append("Suitor: Y-[P]yeah![P] I'll be there![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] Don't keep a girl waiting.[P] And...[P] bring a bit of wine for me.[B][C]") ]])
		fnCutscene([[ Append("Suitor: Shure thing, misshy!") ]])
		fnCutsceneBlocker()
		
		fnCutscene([[ WD_SetProperty("FastShow") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] Sheesh.[P] The least they could do is make it a challenge.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Happy] I'm a waitress.[P] I manipulate people for a living.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] Manipulate?[P] All you did was turn to the side and he was practically drooling.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] I happen to have a nice figure.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Now I better get to the cabin.[P] I've earned this joining.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] Yeah, about that.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] You don't feel bad at all?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Happy] He'll love being a daughter of the wilds.[P] I might even truly make love to my new leaf-sister...[P] she'll be so pretty...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] I'll just sit this one out if it's all the same to you.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Suit yourself.[P] I have a date!") ]])
		fnCutsceneBlocker()
	end
	
	--Fade to black.
	fnCutscene([[ AL_SetProperty("Activate Fade", 30, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneBlocker()
	fnCutscene([[ AL_SetProperty("Music", "Null") ]])
	fnCutsceneWait(180)
	fnCutsceneBlocker()
	
	--Remove the man actor.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Pliable Man")
		ActorEvent_SetProperty("Teleport To", (-100.25 * gciSizePerTile), (-100.50 * gciSizePerTile))
	DL_PopActiveObject()
	
	--Teleport Mei to the entrance of the Pit Stop.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Teleport To", (2.25 * gciSizePerTile), (14.50 * gciSizePerTile))
	DL_PopActiveObject()
	fnCutsceneBlocker()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Face", 1, 0)
	DL_PopActiveObject()
	fnCutsceneBlocker()
	
	--If Florentina is present, teleport her as well.
	if(bIsFlorentinaPresent == true) then
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Florentina")
			ActorEvent_SetProperty("Teleport To", (16.25 * gciSizePerTile), (14.50 * gciSizePerTile))
		DL_PopActiveObject()
		fnCutsceneBlocker()
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Florentina")
			ActorEvent_SetProperty("Face", -1, 0)
		DL_PopActiveObject()
		fnCutsceneBlocker()
	end
	
	--Transform Mei into an Alraune.
	fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Alraune.lua") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(15)
	fnCutsceneBlocker()
	
	--Next dialogue sequence.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Alraune", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Alraune2", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "MercM", "Neutral") ]])
	fnCutscene([[ Append("Mei:[E|Blush] Hey there, handsome.[P] Did you bring the wine?[B][C]") ]])
	fnCutscene([[ Append("Suitor: Heh, you brought shome friendsh along?[P] I like that.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Uhhhh...[B][C]") ]])
	fnCutscene([[ Append("Alraune: He does not seem to realize the situation.[B][C]") ]])
	fnCutscene([[ Append("Suitor: Well why don't you jusht take a sheat over here, pretty thing?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] This is a trap, stupid.[B][C]") ]])
	fnCutscene([[ Append("Suitor: What kind of trap?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Offended] ...[P] I shouldn't need to explain this.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Happy] In fact, I don't have to.[P] Sisters?") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(45)
	fnCutsceneBlocker()

	--Scene.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene") ]])
	fnCutscene([[ Append("The fool put up no resistance as the three wildflowers descended upon him.[P] As if by way of apology, Mei gave his crotch a brief stroke as their combined pollen put him to the edge of sleep.[B][C]") ]])
	fnCutscene([[ Append("They dragged him into the concealed basement where the other Alraunes waited patiently.[P] Her sisters placed the barely conscious man near the edge of the pool, but left the real work to Mei.[B][C]") ]])
	fnCutscene([[ Append("Mei looked over her catch.[P] As a human, she may have found him attractive, but no longer.[P] Her libido cared only for the tranquility and beauty that came with subsumption by nature.[B][C]") ]])
	fnCutscene([[ Append("Rochea, the most skilled leaf-sister at joining, waited now at the edge of the pool.[P] There was no need to speak.[P] Mei's instincts knew what to do.[P] She merely wished for Rochea to advise her and be near to her.[B][C]") ]])
	fnCutscene([[ Append("She lifted his body and carried it effortlessly into the pool.[P] Somehow, here in the earth, surrounded by the spirits of the forest, her strength had grown far beyond what she or any other human could have achieved.[B][C]") ]])
	fnCutscene([[ Append("With Rochea's hand steadying hers, she held the man in the pool, floating with him and keeping his head above the surface.[P] As she had before, she felt the warm, comforting embrace of the pool soak her.[B][C]") ]])
	fnCutscene([[ Append("She now understood what was happening.[P] The pollen of millions of plants from all over the forest had been collected here, mixed with the magic of her sisters and water provided by the earth.[P] The collective dreams of millions of plants soaked her and her human protege.[B][C]") ]])
	fnCutscene([[ Append("His skin had turned blue, and his body had become lithe, curvaceous, better than it was before.[P] She felt at his, [P]no,[P] her,[P] chest.[P] She was pleased with what her hands felt, as was her charge, who moaned softly.[B][C]") ]])
	fnCutscene([[ Append("Rochea now put one hand on the new sister's head.[P] Mei put hers over Rochea's, and they both pressed the young one down into the fluid.[B][C]") ]])
	fnCutscene([[ Append("They now had some time while the joining would complete without them.[P] Mei drew Rochea close to her and kissed her softly on the lips.[P] Rochea smiled at her and returned the favour.[P] Covered in Alraune magical fluid, the two joined their bodies as one.[B][C]") ]])
	fnCutscene([[ Append("From near them in the fluid, a form stirred.[P] A young, beautiful face emerged from the waters.[P] She blinked as she adapted to her new eyes, drawing them around the room to take in the new sights.[B][C]") ]])
	fnCutscene([[ Append("When her eyes drew across Mei's, she thrust forward.[P] Rochea swam away to allow Mei's new sister to kiss her.[P] Her hands pushed through the fluid to find Mei's hips and caress her legs...") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(45)
	fnCutsceneBlocker()

	--Scene.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene") ]])
	fnCutscene([[ Append("Rising from the pool together, Mei and her new leaf-sister continued to lock lips at each opportunity that presented itself.[P] She had never heard the man's name, and never cared to.[P] He was a distant memory, of no importance now.[B][C]") ]])
	fnCutscene([[ Append("Rochea emerged behind them and placed her hand on the new sister's back.[P] Her sister reached out a hand to grasp Mei's, and Mei held it gingerly.[P] Then, Rochea led the new sister away.") ]])
	fnCutsceneBlocker()
	fnCutsceneWait(45)
	fnCutsceneBlocker()
	
	--Next dialogue sequence.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Rochea", "Neutral") ]])
	fnCutscene([[ Append("Mei:[E|Blush] Ahhh....[B][C]") ]])
	fnCutscene([[ Append("Rochea: The cleansing has begun.[P] It will be three days before she is pure.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Blush] I'm so glad another walks the path with us.[B][C]") ]])
	fnCutscene([[ Append("Rochea: As are we all.[P] Mei, perhaps we were wrong about you.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] ..?[B][C]") ]])
	fnCutscene([[ Append("Rochea: When you refused to be cleansed, we had feared you were clinging to humanity.[P] It is clear that is not the case.[B][C]") ]])
	fnCutscene([[ Append("Rochea: While you are still not one of us, truly, we have no reason to doubt your motives.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] I serve the forest with my whole being.[B][C]") ]])
	fnCutscene([[ Append("Rochea: I apologize for questioning you.[B][C]") ]])
	fnCutscene([[ Append("Rochea: Have you thought of a name for your new sister?[P] It is customary that you be allowed to suggest one, as you brought her here.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] ...[P] Camellia.[B][C]") ]])
	fnCutscene([[ Append("Rochea: An interesting name.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] It's a species of tree from my home.[P] I played under one in the park after school.[B][C]") ]])
	fnCutscene([[ Append("Rochea: I am sorry.[P] It seems your home still calls to you.[P] I had hoped perhaps you would stay with us.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Whatever my destiny is, I hope it brings me back here.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] If not, then...[P] the least I could do is bring nature's blessing to Earth.[P] Perhaps join my own leaf-sisters there.[B][C]") ]])
	fnCutscene([[ Append("Rochea: If you cannot stay with us, we hope that you will find happiness.[P] Return to us any time.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Will leaf-sister Camellia remember me?[B][C]") ]])
	fnCutscene([[ Append("Rochea: She will remember everything after she opened her true eyes for the first time.[P] You were the first thing she saw.[B][C]") ]])
	fnCutscene([[ Append("Rochea: I will tell her of your role in her joining, if you like.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Thank you, Rochea.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Now, the road calls.[P] Until next time!") ]])
	fnCutsceneBlocker()
	
	fnCutsceneWait(180)
	fnCutsceneBlocker()
	
	--Overlay goes away.
	fnCutscene([[ AL_SetProperty("Activate Fade", 30, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 0) ]])
	fnCutsceneBlocker()
	fnCutsceneWait(30)
	fnCutsceneBlocker()
	
	--If Florentina was not in the party:
	if(bIsFlorentinaPresent == false) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Hmm, enough fun.[P] Back to work!") ]])
		fnCutsceneBlocker()
	
	--If Florentina was in the party.
	else
	
		--Florentina walks up.
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Florentina")
			ActorEvent_SetProperty("Move To", (4.25 * gciSizePerTile), (14.50 * gciSizePerTile))
		DL_PopActiveObject()
		fnCutsceneBlocker()
		fnCutsceneWait(30)
		fnCutsceneBlocker()
		
		--Setup.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
		
		--Dialogue.
		fnCutscene([[ Append("Mei:[E|Blush] Hmmm...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] Nice afterglow, kid.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] H-[P]huh?[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] So you're all done?[P] Done tricking innocent people so you can get off on it?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] Hey...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] Look, Mei.[P] I'm not really sure what I expected from you.[P] You seem all right most of the time.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] I haven't known you very long, but I didn't think you were this way.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused][P] You're a predator.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Surprise] ..![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] I am -[P] I'm not![B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] You waited until you found a drunk, stupid man.[P] You took him someplace and had your way with him.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] It's not like that...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] She'll be happier this way...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] You didn't even ask.[P] You just took it, from someone who wasn't even aware enough to say no.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] ...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Cry] You're right...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] Hmpf.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Cry] This body...[P][CLEAR]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] Don't you dare.[P] I've never seduced someone and joined them against their will.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] So what's your excuse?[P] Or are you just a rotten person?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] ...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] No.[P] No![P] What I did was right![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] You're taking the side of humans because you still are one, at heart.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] You live with them, you deal with them.[P] You trust them.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] But you can trust them, because you are one![P] Well I'm not![B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] You can be any time you want to.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Angry] Out of necessity and nothing else![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] I did what was right...[P][EMOTION|Mei|Sad] didn't I?[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] From one perspective.[P] I'm telling you to look at the other ones.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] The worst part is that you're probably right.[P] She will be happier, probably.[P] But it's for the wrong reasons.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] Why do the reasons matter?[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] Because if the ends justify the means, then there is no atrocity you won't do.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] There's a lot of people with body counts in the thousands for whom the end justified the means.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] We had them on Earth, too.[P] But they all had one common feature::[P] They were all stupid, violent, flawed...[P] human.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] There's just no getting through to you, is there?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] I -[P] I won't do it again.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] Oh?[P] Because I told you not to?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] ...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] Because...[P] Because you might be right.[P] I need to think about it.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] I'm being pulled in so many different directions.[P] I would never have done this back home.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] Is this world changing me, or was I always like this...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] Buck up, kid.[P] You're under a lot of stress.[P] You'll figure it out.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] And then maybe you'll atone for what you did.[P] Lord knows I have...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] ..?[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] Enough yakking.[P] Let's move out.") ]])
		fnCutsceneBlocker()
		
		--Florentina walks to Mei, fold the party.
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Florentina")
			ActorEvent_SetProperty("Move To", (2.25 * gciSizePerTile), (14.50 * gciSizePerTile))
		DL_PopActiveObject()
		fnCutsceneBlocker()
		
		--Start the music back up.
		fnCutscene([[ AL_SetProperty("Music", "BreannesTheme") ]])
		fnCutscene([[ AL_SetProperty("Fold Party") ]])
		fnCutsceneBlocker()
	end
	
--Mei decides to leave the guy alone.
elseif(sTopicName == "Leave") then
	WD_SetProperty("Hide")
end
