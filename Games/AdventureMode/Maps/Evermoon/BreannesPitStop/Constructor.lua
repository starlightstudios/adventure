-- |[Constructor]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = fnResolveDirectory() --Map's name is the name of the folder it is in.

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[Standard]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
	AL_SetProperty("Music", "BreannesTheme")
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/BreannesPitStop/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", "BreannesPitStop")

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end
    
    -- |[Map Handling]|
    --Resolve the map's variable.
    fnStandardCh1Map(sLevelName)

-- |[Post-Exec]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	--Variables.
	local fSpawnX = 0
	local fSpawnY = 0
	local iSpawnD = gci_Face_South
	local iHasMetMei = VM_GetVar("Root/Variables/Chapter1/Breanne/iHasMetMei", "N")
	local iCanSpawnJoanie = VM_GetVar("Root/Variables/Chapter1/Breanne/iCanSpawnJoanie", "N")

	--Case: Haven't met Breanne yet. Only she spawns. She spawns in the kitchen.
	if(iHasMetMei == 0.0) then
		fSpawnX = 25
		fSpawnY = 15
		iSpawnD = gci_Face_North

	--Mei has met Breanne.
	else

		-- |[Nadia]|
		--Special: If Mei has not been to the Dimensional Trap Dungeon, is level 5, has Florentina, and has not seen it already, spawn Nadia:
		local bSpawnedNadia        = false
		local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
		local iInformedOfDungeon   = VM_GetVar("Root/Variables/Chapter1/Scenes/iInformedOfDungeon", "N")
        local iIsRelivingScene     = VM_GetVar("Root/Variables/Chapter1/Scenes/iIsRelivingScene", "N")
		AdvCombat_SetProperty("Push Party Member", "Mei")
			local iMeiLevel = AdvCombatEntity_GetProperty("Level")
		DL_PopActiveObject()
		if(iInformedOfDungeon == 0.0 and bIsFlorentinaPresent == true and iMeiLevel >= 12 and iIsRelivingScene == 0.0) then
			
			--Flags:
			bSpawnedNadia = true
			iCanSpawnJoanie = 0.0
			
			--Spawn Nadia:
            fnStandardNPCByPosition("Nadia")

			-- |[Walking]|
			--Mei/Florentina walks in a bit.
			fnCutsceneMove("Mei", 6.25, 14.00)
			fnCutsceneMove("Florentina", 6.25, 15.00)
			fnCutsceneBlocker()

			--Wait a bit.
			fnCutsceneWait(25)
			fnCutsceneBlocker()

			--Nadia faces left.
			fnCutsceneFace("Nadia", -1, 0)
			fnCutsceneWait(25)
			fnCutsceneBlocker()
			
			--Play the cutscene.
			LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/Nadia_InformOfDungeon/Scene_Begin.lua")
			
			--Move Florentina onto Mei, fold the party.
			fnCutsceneMove("Florentina", 6.25, 14.00)
			fnCutsceneBlocker()
			fnCutscene([[ AL_SetProperty("Fold Party") ]])
			fnCutsceneBlocker()
			
		end
	
		-- |[Random NPCs]|
		--There is a 25% chance that a werecat will spawn on the south end of the map.
        fnStandardNPCByPosition("Werecat")
        fnStandardNPCByPosition("Chair Man")
        fnStandardNPCByPosition("Suitor")
        fnStandardNPCByPosition("Alraune")
		
		-- |[Specific NPC]|
		--Mei can convince this fellow to become an Alraune.
		local iMeiJoinedSuitor = VM_GetVar("Root/Variables/Chapter1/Breanne/iMeiJoinedSuitor", "N")
		if(iMeiJoinedSuitor == 0.0) then
            fnStandardNPCByPosition("Pliable Man")
		end

		-- |[Breanne]|
		--Breanne's spawn position will be in one of several locations.
		local iRoll = LM_GetRandomNumber(0, 99)
		
		--Nadia case:
		if(bSpawnedNadia == true) then
			fSpawnX = 9
			fSpawnY = 14
			iSpawnD = gci_Face_West
		
		--Normal case:
		elseif(iCanSpawnJoanie == 0.0) then
		
			--Outside the shed.
			if(iRoll < 10) then
				fSpawnX = 11
				fSpawnY = 11
				iSpawnD = gci_Face_South
			
			--On the chair outside of the building.
			elseif(iRoll < 20) then
				fSpawnX = 13
				fSpawnY = 17
				iSpawnD = gci_Face_West
			
			--Near the table inside the building.
			elseif(iRoll < 30) then
				fSpawnX = 19
				fSpawnY = 15
				iSpawnD = gci_Face_South
			
			--Kitchen, near the counter.
			elseif(iRoll < 40) then
				fSpawnX = 27
				fSpawnY = 17
				iSpawnD = gci_Face_East
			
			--Kitchen, shelves.
			elseif(iRoll < 50) then
				fSpawnX = 25
				fSpawnY = 15
				iSpawnD = gci_Face_North
			
			--Breanne's room.
			elseif(iRoll < 60) then
				fSpawnX = 26
				fSpawnY = 9
				iSpawnD = gci_Face_North
			
			--Main room.
			elseif(iRoll < 80) then
				fSpawnX = 21
				fSpawnY = 12
				iSpawnD = gci_Face_South
			
			--Near the entrance.
			else
				fSpawnX = 5
				fSpawnY = 11
				iSpawnD = gci_Face_North
			end
		
		--If Joanie can be spawned, Breanne always spawns near Joanie.
		else
			--Position.
			fSpawnX = 14
			fSpawnY = 8
			iSpawnD = gci_Face_East
			
			--Spawn the bee.
            fnStandardNPCByPosition("Joanie")
		
		end
		
	end

	--Spawn Breanne.
    fnStandardNPCByPosition("Breanne")
	EM_PushEntity("Breanne")
		TA_SetProperty("Position", fSpawnX, fSpawnY)
		TA_SetProperty("Facing", iSpawnD)
	DL_PopActiveObject()
    
    --Spawn Mycela.
    local iDefeatedMycela = VM_GetVar("Root/Variables/Chapter1/Sharelock/iDefeatedMycela", "N")
    if(iDefeatedMycela == 1.0) then
        fnStandardNPCByPosition("Mycela")
    end
    
    --If Mycela is not here, or Florentina has the Agarist job, don't show the mushroom.
    local iHasJob_Agarist = VM_GetVar("Root/Variables/Global/Florentina/iHasJob_Agarist", "N")
    if(iDefeatedMycela == 0.0 or iHasJob_Agarist == 1.0) then
        AL_SetProperty("Set Layer Disabled", "Mushroom", true)
    end
    
    -- |[Skillbook]|
    local iSkillbook3 = VM_GetVar("Root/Variables/Global/Mei/iSkillbook3", "N")
    if(iSkillbook3 == 1.0) then
		AL_SetProperty("Set Layer Disabled", "SkillbookIcoA", true)
    end
    local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
    local iSkillbook0 = VM_GetVar("Root/Variables/Global/Florentina/iSkillbook0", "N")
    if(iSkillbook0 == 1.0 or bIsFlorentinaPresent == false) then
		AL_SetProperty("Set Layer Disabled", "SkillbookIcoB", true)
    end

end
