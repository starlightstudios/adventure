-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Execution]|
--Sign next to Breanne's shed.
if(sObjectName == "Shed Sign") then
	fnStandardDialogue([[ [VOICE|Mei](It reads "Supply shed.[P] Keep out.") ]])

--Attempting to open the door.
elseif(sObjectName == "Shed Door") then

	--Variables.
	local iHasPryBar = AdInv_GetProperty("Item Count", "Pry Bar")
	
	--Player does not have anything to pry the door with.
	if(iHasPryBar == 0.0) then
		fnStandardDialogue([[[VOICE|Mei] (It's locked.) ]])
	
	--Pry it open.
	else
		fnStandardDialogue([[[VOICE|Mei] (It's locked.[P] Probably not a good idea to pry it open...) ]])
	end
	
--Sign next to the main Inn building.
elseif(sObjectName == "Inn Sign") then
	fnStandardDialogue([[ [VOICE|Mei](It reads "Breanne's Pit Stop. Come on in!")[B][C][VOICE|Mei](Written below that, in smaller letters, is "Unless my parents sent you.[P] In which case, taff off!") ]])
	WD_SetProperty("Unlock Topic", "BreanneSign", 1)
	
--Sign next to the watering hole.
elseif(sObjectName == "Water Sign") then
	fnStandardDialogue([[ [VOICE|Mei](It reads "This is the watering hole.[P] No fishing, please.")[B][C][VOICE|Mei]("To whoever keeps eating my pet fish -[P] Knock it off!") ]])

--Bookshelves.
elseif(sObjectName == "Bookshelf0") then
	fnStandardDialogue([[[VOICE|Mei] (Books that seem to have been left here by previous guests.[P] There's a lot of romance novels, but some on swordplay, astronomy, exploration, and marine life.) ]])
	
elseif(sObjectName == "Bookshelf1") then
	fnStandardDialogue([[[VOICE|Mei] (Books that seem to have been left here by previous guests.[P] All romance novels, every single one!) ]])

elseif(sObjectName == "Bookshelf2") then
	fnStandardDialogue([[[VOICE|Mei] (This is Breanne's bookshelf.[P] Lots of books on woodworking, cooking, and negotiating.)[B][C][VOICE|Mei](Oh, what's this?[P] Lesbian erotica?[P] Better not touch that, she might notice...) ]])

elseif(sObjectName == "KitchenShelves") then
	fnStandardDialogue([[[VOICE|Mei] (Vegetables, breads, and a few dried meats.[P] Despite not having any refrigeration, the food is in very good condition.) ]])

-- |[Skillbook]|
elseif(sObjectName == "SkillbookA") then
    AL_SetProperty("Set Layer Disabled", "SkillbookIcoA", true)
	LM_ExecuteScript(gsMeiSkillbook, 3)
    
elseif(sObjectName == "SkillbookB") then
    AL_SetProperty("Set Layer Disabled", "SkillbookIcoB", true)
	LM_ExecuteScript(gsFlorentinaSkillbook, 0)
    
-- |[Debug]|
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
