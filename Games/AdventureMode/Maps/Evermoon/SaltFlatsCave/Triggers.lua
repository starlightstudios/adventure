-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
--Triggers the meeting with Adina if Mei needs it.
if(sObjectName == "AdinaArrive") then

	--Variables.
	local iAwaitAdina = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iAwaitAdina", "N")
	
	--Waiting for Adina to arrive.
	if(iAwaitAdina == 1.0) then
		
		--Reset the flag so we don't double-activate.
		VM_SetVar("Root/Variables/Chapter1/SaltFlats/iAwaitAdina", "N", 0.0)
        VM_SetVar("Root/Variables/Chapter1/Scenes/iAdinaQuestVar", "N", 3.0)
		
		--Null the music.
		AL_SetProperty("Music", "Null")
		
		--This marks a day as having passed.
		local iDaysPassed = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iDaysPassed", "N")
		VM_SetVar("Root/Variables/Chapter1/SaltFlats/iDaysPassed", "N", iDaysPassed + 1)
		if(iDaysPassed + 1 >= 2) then
			VM_SetVar("Root/Variables/Chapter1/Scenes/iMeiLovesAdina", "N", 1.0)
		end
		
		--Run the scene externally.
		LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/Adina_Sex_Scene/Scene_Begin.lua")
	end
end
