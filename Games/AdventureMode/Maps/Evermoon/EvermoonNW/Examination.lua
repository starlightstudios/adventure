-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Execution]|
--Note, can't be decoded until later.
if(sObjectName == "CodedNote") then
	fnStandardDialogue([[ [VOICE|Mei](It reads "QXCCE OHTT 1NN".[P] Is this some sort of code?) ]])

--Barrels. Nothing of major interest.
elseif(sObjectName == "Barrels") then
	fnStandardDialogue([[ [VOICE|Mei](The barrels contain salted fish.[P] Not very appetizing...) ]])

--Bookshelves. Redirects to a topic set.
elseif(sObjectName == "Bookshelf") then
	fnStandardDialogue([[ [VOICE|Mei](There's quite a collection of books here.[P] Read one?)[BLOCK] ]])
	fnCutscene([[ WD_SetProperty("Activate Topics After Dialogue", "EvermoonNWBookshelf", "Leave") ]])

--Bed.
elseif(sObjectName == "Bed") then
	fnStandardDialogue([[ [VOICE|Mei](The bed hasn't been used in a while, probably at least a week, judging by the dust buildup.[P] It was very neatly made, though.) ]])

--Letter. Only appears if you gave the rubber sample to Polaris.
elseif(sObjectName == "Letter") then
    local iReturnedPhial = VM_GetVar("Root/Variables/Chapter1/Scenes/iReturnedPhial", "N")
    if(iReturnedPhial == 0.0) then return end
    
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Dear Galena. Sorry, I misplaced the cipher book in my move. It's probably in Dorothy's trunk at my other house. I hope it's okay to use plain text.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('A friend of mine says there's a bunch of rubbery people in a tower to the west, and they brought me a primal sample of the stuff. Might be a threat, "..
                         "but it might also be a big scientific discovery!')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('You're much more familiar with Trannadar's history than I am. If you could bring some history books over next time you come for tea, I'd appreciate it greatly. "..
                         "And if you're feeling up to it, I want to go see these rubber people in person. Let me know if you want to come, or if Aquillia is interested. After her arm heals, of course.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('And bring some of that salami you brought last time! Dot gobbled it right up, and she's pouting at me to get more. Thanks! See you soon! -Polaris')") ]])
    fnCutsceneBlocker()

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
