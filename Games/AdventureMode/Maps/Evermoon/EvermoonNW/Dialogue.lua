-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    --Set facing.
    TA_SetProperty("Face Character", "PlayerEntity")
        
    -- |[Aquillia]|
    if(sActorName == "Aquillia") then
	
        --This is always a major dialogue.
        fnStandardMajorDialogue()
        
        --Variables.
        local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
        local iHasMetAquillia = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasMetAquillia", "N")
        local iHasSeenTrannadarFlorentinaScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFlorentinaScene", "N")
        
        --Aquillia has not revealed herself yet.
        if(iHasMetAquillia == 0.0) then
        
            -- |[Setup]|
            --Constants.
            local ciRotationTicks = 5
            
            --Flag.
            VM_SetVar("Root/Variables/Chapter1/Scenes/iHasMetAquillia", "N", 1.0)
            if(iHasSeenTrannadarFlorentinaScene == 1.0) then
                VM_SetVar("Root/Variables/Chapter1/Scenes/iHasFlorentinaMetAquillia", "N", 1.0)
            end
        
            -- |[Dialogue]|
            --Aquillia takes slot 5, dressed as the prisoner.
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Aquillia", "Cultist") ]])
        
            --If Mei is an Alraune during this conversation:
            if(sMeiForm == "Alraune") then
                fnCutscene([[ Append("Cultist: Hey, you made it![B][C]") ]])
                fnCutscene([[ Append("Cultist: ...[P] and apparently had a bit of a run-in with the plant girls, too.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Er, do I know you?[B][C]") ]])
                fnCutscene([[ Append("Cultist: You don't recognize me?[B][C]") ]])
                fnCutscene([[ Append("Cultist: Oh right...") ]])
            
            --If Mei is a Bee during the conversation:
            elseif(sMeiForm == "Bee") then
                fnCutscene([[ Append("Cultist: Hey, you made it![B][C]") ]])
                fnCutscene([[ Append("Cultist: ...[P] and apparently had a bit of a run-in with the bee girls, too.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Happy] I don't mind.[P] Best thing that ever happened to me.[B][C]") ]])
                fnCutscene([[ Append("Cultist: And you can talk, too?[P] Oh, Hirundo is going to get a real kick out of this...[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Excuse me for being rude, but do I know you?[B][C]") ]])
                fnCutscene([[ Append("Cultist: You don't recognize me?[B][C]") ]])
                fnCutscene([[ Append("Cultist: Oh right...") ]])
            
            --If Mei is a Slime during the conversation:
            elseif(sMeiForm == "Slime") then
                fnCutscene([[ Append("Cultist: Hey, you made it![B][C]") ]])
                fnCutscene([[ Append("Cultist: Or maybe you just look like her...[B][C]") ]])
                fnCutscene([[ Append("Cultist: You -[P] you just keep your distance, slime...[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Don't be afraid, I'm not looking to eat you.[B][C]") ]])
                fnCutscene([[ Append("Cultist: You can talk?[P] That's actually really rad to the max.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Blush] (Rad to the max?[P] Who is this person?)[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Do I know you?[B][C]") ]])
                fnCutscene([[ Append("Cultist: Maybe, maybe not.[P] You look a lot like someone who helped me out of a bind.[B][C]") ]])
                fnCutscene([[ Append("Cultist: Perhaps this will jog your memory.") ]])
            
            --If Mei is a Ghost during the conversation:
            elseif(sMeiForm == "Ghost") then
                fnCutscene([[ Append("Cultist: Wow.[P] Guess those thugs got the better of you, huh?[B][C]") ]])
                fnCutscene([[ Append("Cultist: ...[P] And you became a maid in the interim.[B][C]") ]])
                fnCutscene([[ Append("Cultist: I get the feeling there's a lot going on here I am not privy to.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Offended] Look, being dead really isn't as bad as everyone makes it out to be.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Offended] You know how when you eat a bunch, you feel really tired?[P] I don't need to eat, *or* sleep.[P] Top that![B][C]") ]])
                fnCutscene([[ Append("Cultist: Okay I just got taken to school.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Offended] Just who the heck are you, anyway?[B][C]") ]])
                fnCutscene([[ Append("Cultist: Huh?[P] Oh yeah, the getup!") ]])
            
            --If Mei is a Gravemarker during the conversation:
            elseif(sMeiForm == "Gravemarker") then
                fnCutscene([[ Append("Cultist: Oh...[B][C]") ]])
                fnCutscene([[ Append("Cultist: You're the girl who helped me in that dungeon. And it turns out you're...[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] A petrified angel.[P] Hm.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] You seem awfully familiar.[P] I feel like I've known you since the dawn of time.[B][C]") ]])
                fnCutscene([[ Append("Cultist: Uhm, clearly it's because you recognize me through my bitchin' disguise.") ]])
        
            --Wisphag
            elseif(sMeiForm == "Wisphag") then
                fnCutscene([[ Append("Cultist: Great, another one of those things.[P] Beat it, hag![B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Offended] I'm not a hag![P] I'm more of a beldam![B][C]") ]])
                fnCutscene([[ Append("Cultist: Maybe a crone?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Yeah crone works but sounds like a food.[P] Nobody knows what a beldam is.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Offended] Hold on do I know you?[B][C]") ]])
                fnCutscene([[ Append("Cultist: You sound familiar.[P] Allow me to remind you...[B][C]") ]])
            
            --Mannequin
            elseif(sMeiForm == "Mannequin") then
                fnCutscene([[ Append("Cultist: Man, that's freaky.[P] A mannequin that looks just like that girl I saw...[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] I am no mere mannequin.[B][C]") ]])
                fnCutscene([[ Append("Cultist: Woah![P] You can talk?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] I am fully in control of myself.[P] This form is the result of a curse.[B][C]") ]])
                fnCutscene([[ Append("Cultist: Hold on.[P] Yeah, someone I know mentioned that once.[P] Did you - [B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] The person who cursed me has been dealt with.[P] You need not worry.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] You seem familiar.[P] Do I know you?[B][C]") ]])
                fnCutscene([[ Append("Cultist: Hey, you don't recognize me?[B][C]") ]])
                fnCutscene([[ Append("Cultist: Oh right...") ]])
            
            --Human/unhandled.
            else
                fnCutscene([[ Append("Cultist: Hey, you made it![B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Offended] A cultist?[P] Out here?[P] I thought I was done with you![B][C]") ]])
                fnCutscene([[ Append("Cultist: Hey, you don't recognize me?[B][C]") ]])
                fnCutscene([[ Append("Cultist: Oh right...") ]])
            end
            fnCutsceneBlocker()
        
            -- |[Movement]|
            --Wait a bit.
            fnCutsceneWait(30)
            fnCutsceneBlocker()
            
            --Aquillia spins around.
            Cutscene_CreateEvent("ActorEvent", "Actor")
                ActorEvent_SetProperty("Subject Name", "Aquillia")
                ActorEvent_SetProperty("Face", -1, 0)
            DL_PopActiveObject()
            fnCutsceneWait(ciRotationTicks * 2)
            fnCutsceneBlocker()
            Cutscene_CreateEvent("ActorEvent", "Actor")
                ActorEvent_SetProperty("Subject Name", "Aquillia")
                ActorEvent_SetProperty("Face", 0, -1)
            DL_PopActiveObject()
            fnCutsceneWait(ciRotationTicks * 2)
            fnCutsceneBlocker()
            Cutscene_CreateEvent("ActorEvent", "Actor")
                ActorEvent_SetProperty("Subject Name", "Aquillia")
                ActorEvent_SetProperty("Face", 1, 0)
            DL_PopActiveObject()
            fnCutsceneWait(ciRotationTicks * 2)
            fnCutsceneBlocker()
            Cutscene_CreateEvent("ActorEvent", "Actor")
                ActorEvent_SetProperty("Subject Name", "Aquillia")
                ActorEvent_SetProperty("Face", 0, 1)
            DL_PopActiveObject()
            fnCutsceneWait(ciRotationTicks * 2)
            fnCutsceneBlocker()
            Cutscene_CreateEvent("ActorEvent", "Actor")
                ActorEvent_SetProperty("Subject Name", "Aquillia")
                ActorEvent_SetProperty("Face", -1, 0)
            DL_PopActiveObject()
            fnCutsceneWait(ciRotationTicks)
            fnCutsceneBlocker()
            Cutscene_CreateEvent("ActorEvent", "Actor")
                ActorEvent_SetProperty("Subject Name", "Aquillia")
                ActorEvent_SetProperty("Face", 0, -1)
            DL_PopActiveObject()
            fnCutsceneWait(ciRotationTicks)
            fnCutsceneBlocker()
            Cutscene_CreateEvent("ActorEvent", "Actor")
                ActorEvent_SetProperty("Subject Name", "Aquillia")
                ActorEvent_SetProperty("Face", 1, 0)
            DL_PopActiveObject()
            fnCutsceneWait(ciRotationTicks)
            fnCutsceneBlocker()
            Cutscene_CreateEvent("ActorEvent", "Actor")
                ActorEvent_SetProperty("Subject Name", "Aquillia")
                ActorEvent_SetProperty("Face", 0, 1)
            DL_PopActiveObject()
            fnCutsceneWait(ciRotationTicks)
            fnCutsceneBlocker()
            
            --Aquillia switches sprites.
            fnCutscene([[ EM_PushEntity("Aquillia")
                fnSetCharacterGraphics("Root/Images/Sprites/AquilliaNoJacket/", false)
            DL_PopActiveObject() ]])
            
            --KEEP SPINNING!
            Cutscene_CreateEvent("ActorEvent", "Actor")
                ActorEvent_SetProperty("Subject Name", "Aquillia")
                ActorEvent_SetProperty("Face", -1, 0)
            DL_PopActiveObject()
            fnCutsceneWait(ciRotationTicks)
            fnCutsceneBlocker()
            Cutscene_CreateEvent("ActorEvent", "Actor")
                ActorEvent_SetProperty("Subject Name", "Aquillia")
                ActorEvent_SetProperty("Face", 0, -1)
            DL_PopActiveObject()
            fnCutsceneWait(ciRotationTicks)
            fnCutsceneBlocker()
            Cutscene_CreateEvent("ActorEvent", "Actor")
                ActorEvent_SetProperty("Subject Name", "Aquillia")
                ActorEvent_SetProperty("Face", 1, 0)
            DL_PopActiveObject()
            fnCutsceneWait(ciRotationTicks)
            fnCutsceneBlocker()
            Cutscene_CreateEvent("ActorEvent", "Actor")
                ActorEvent_SetProperty("Subject Name", "Aquillia")
                ActorEvent_SetProperty("Face", 0, 1)
            DL_PopActiveObject()
            fnCutsceneWait(ciRotationTicks)
            fnCutsceneBlocker()
            Cutscene_CreateEvent("ActorEvent", "Actor")
                ActorEvent_SetProperty("Subject Name", "Aquillia")
                ActorEvent_SetProperty("Face", -1, 0)
            DL_PopActiveObject()
            fnCutsceneWait(ciRotationTicks * 2)
            fnCutsceneBlocker()
            Cutscene_CreateEvent("ActorEvent", "Actor")
                ActorEvent_SetProperty("Subject Name", "Aquillia")
                ActorEvent_SetProperty("Face", 0, -1)
            DL_PopActiveObject()
            fnCutsceneWait(ciRotationTicks * 2)
            fnCutsceneBlocker()
            Cutscene_CreateEvent("ActorEvent", "Actor")
                ActorEvent_SetProperty("Subject Name", "Aquillia")
                ActorEvent_SetProperty("Face", 1, 0)
            DL_PopActiveObject()
            fnCutsceneWait(ciRotationTicks * 2)
            fnCutsceneBlocker()
            Cutscene_CreateEvent("ActorEvent", "Actor")
                ActorEvent_SetProperty("Subject Name", "Aquillia")
                ActorEvent_SetProperty("Face", 0, 1)
            DL_PopActiveObject()
            fnCutsceneWait(ciRotationTicks * 2)
            fnCutsceneBlocker()
            
            --Wait a bit.
            fnCutsceneWait(30)
            fnCutsceneBlocker()
            
            --Face Mei again.
            fnCutscene([[ EM_PushEntity("Aquillia")
                TA_SetProperty("Face Character", "PlayerEntity")
            DL_PopActiveObject() ]])
            fnCutsceneBlocker()
        
            -- |[Dialogue]|
            --Start up.
            fnStandardMajorDialogue()
            
            --Aquillia takes slot 4, this time in her proper portrait.
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Aquillia", "Cast") ]])
            fnCutscene([[ Append("Lady: Ta-[P]freakin-[P]dah,[P] baby.[P] Do I look good or what?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Laugh] Oh my gosh, I'm so sorry I didn't recognize you![B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] You're okay?[P] Have you seen a doctor about that arm?[B][C]") ]])
            fnCutscene([[ Append("Lady: Oh, this?[P] Yeah I got it patched up.[P] I'll be fine in no time.[B][C]") ]])
            
            --If Florentina is present:
            if(iHasSeenTrannadarFlorentinaScene == 1.0) then
                fnCutscene([[ Append("Florentina:[E|Neutral] Really?[P] Who patched it up?[P] I didn't see you at the trading post...[B][C]") ]])
                fnCutscene([[ Append("Lady: A friend of mine.[P] She's good at the whole \"Helping the sick\" gig.[B][C]") ]])
            end
            
            --If Mei is a Bee during the conversation:
            if(sMeiForm == "Bee") then
                fnCutscene([[ Append("Mei:[E|Blush] I could get you some royal jelly to help speed it up...[B][C]") ]])
                fnCutscene([[ Append("Lady: Hah![B][C]") ]])
                fnCutscene([[ Append("Lady: Thanks for the offer, but I think I'll be fine.[B][C]") ]])
            
            --If Mei is a ghost during the conversation:
            elseif(sMeiForm == "Ghost") then
                fnCutscene([[ Append("Mei:[E|Smirk] I could take a look with my first aid kit.[B][C]") ]])
                fnCutscene([[ Append("Lady: I'll be fine, don't worry about it.[B][C]") ]])
            
            --If Mei is a Slime during the conversation:
            elseif(sMeiForm == "Slime") then
                fnCutscene([[ Append("Mei:[E|Neutral] Is there anything I can do to help?[B][C]") ]])
                fnCutscene([[ Append("Lady: Er, I'd rather not get slime on any of my bruises.[B][C]") ]])
                fnCutscene([[ Append("Lady: Besides, you kind of already saved my life.[P] I should be asking what I can do for you![B][C]") ]])
            
            --If Mei is a Gravemarker during the conversation:
            elseif(sMeiForm == "Gravemarker") then
                fnCutscene([[ Append("Mei:[E|Neutral] But the recognition goes so much deeper.[P] Like I should be furious with you.[P] Take you and drown you in that river.[B][C]") ]])
                fnCutscene([[ Append("Lady: It's the angel in you.[P] Can you fight it?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Oh, don't worry.[P] My runestone protects me from the worst of it.[B][C]") ]])
                fnCutscene([[ Append("Lady: Oh.[P] So you're not *with* them.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] No.[P] This was not voluntary.[B][C]") ]])
        
            --Wisphag
            elseif(sMeiForm == "Wisphag") then
                fnCutscene([[ Append("Mei: I can help! I can use your soul to patch wounds to your body.[B][C]") ]])
                fnCutscene([[ Append("Lady: I appreciate the offer but I'll let the magic run its course.[B][C]") ]])
            
            --Mannequin
            elseif(sMeiForm == "Mannequin") then
                fnCutscene([[ Append("Mei:[E|Neutral] Is there anything I can do to help?[B][C]") ]])
                fnCutscene([[ Append("Lady: I'm perfectly capable on my own, but thanks for the offer.[B][C]") ]])
            
            --Human/unhandled.
            else
                fnCutscene([[ Append("Mei:[E|Happy] Well, I wish you a speedy recovery![B][C]") ]])
                fnCutscene([[ Append("Lady: Well, thanks for that.[P] And the rescue, too.[B][C]") ]])
            end
            
            --Conversation continues.
            fnCutscene([[ Append("Mei:[E|Neutral] Oh![P] Where are my manners?[P] My name is Mei.[P] I don't think I introduced myself back in that dingy basement.[B][C]") ]])
            if(iHasSeenTrannadarFlorentinaScene == 1.0) then
                fnCutscene([[ Append("Mei:[E|Neutral] This is my friend, Florentina.[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Neutral] Friend?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Associate?[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Happy] Business partner.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Happy] That works...[B][C]") ]])
                fnCutscene([[ Append("Lady: You two get along so well.[B][C]") ]])
            end
            fnCutscene([[ Append("Aquillia: Aquillia Whitebride, at your service.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Sad] Say, Aquillia, you didn't happen to see how I...[P] got into that prison, did you?[B][C]") ]])
            fnCutscene([[ Append("Aquillia: No, no I didn't.[P] Figured you got chucked in there like me.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] What did they want with you?[B][C]") ]])
            fnCutscene([[ Append("Aquillia: I was snooping around their place for, shall we say, reasons of my own.[P] They didn't like that one bit.[B][C]") ]])
            fnCutscene([[ Append("Aquillia: I may also have gouged an eye out of one of their lackeys.[B][C]") ]])
            if(iHasSeenTrannadarFlorentinaScene == 1.0) then
                fnCutscene([[ Append("Florentina:[E|Offended] *growl*[B][C]") ]])
                fnCutscene([[ Append("Aquillia: What?[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Facepalm] Oh, nothing.[P] Nothing at all.[B][C]") ]])
                fnCutscene([[ Append("Aquillia: Hey, I paid a price for it.[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Neutral] I can tell by the bruises.[B][C]") ]])
                fnCutscene([[ Append("Aquillia: Mostly administered by a half-blind and very angry initiate.[B][C]") ]])
            else
                fnCutscene([[ Append("Mei:[E|Happy] I like you a lot already.[B][C]") ]])
                fnCutscene([[ Append("Aquillia: It cost me.[P] They beat me up pretty bad, as you saw.[B][C]") ]])
            end
            fnCutscene([[ Append("Aquillia: Kept demanding I join up with them and acting all surprised when I said no.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Surprise] What did they expect?[B][C]") ]])
            fnCutscene([[ Append("Aquillia: Dunno, but I did see the same treatment get extended to others when I was spying on them.[B][C]") ]])
            fnCutscene([[ Append("Aquillia: Difference is that it seemed to work on them.[P] Eerie, the way they acted.[B][C]") ]])
            if(iHasSeenTrannadarFlorentinaScene == 1.0) then
                fnCutscene([[ Append("Florentina:[E|Neutral] You should probably tell Captain Blythe what you know.[P] He'll want whatever info he can get.[B][C]") ]])
                fnCutscene([[ Append("Aquillia: Tch, tell the mercs?[P] They're probably bought off already.[B][C]") ]])
                fnCutscene([[ Append("Aquillia: And if they're not, they could be.[P] I'm not putting my trust in them.[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Happy] Paranoid?[P] I like that.[B][C]") ]])
            end
            fnCutscene([[ Append("Mei:[E|Neutral] So is this your cabin?[B][C]") ]])
            fnCutscene([[ Append("Aquillia: Me?[P] No.[P] This belongs to a friend of mine.[B][C]") ]])
            fnCutscene([[ Append("Aquillia: I've gotta wait for my arm to heal up before I start on getting a bit of sweet, sweet revenge.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Offended] After what happened last time?[P] Please, don't take any risks.[B][C]") ]])
            fnCutscene([[ Append("Aquillia: I know what they're up to.[P] I know how they work now.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Offended] But they'll kill you if they catch you again![B][C]") ]])
            fnCutscene([[ Append("Aquillia: You're way too sweet for someone who's good with a sword.[B][C]") ]])
            fnCutscene([[ Append("Aquillia: You haven't even known me for a day and already you're acting like we're old chums.[P] Touching.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Surprise] I just -[P] promise you'll be extra careful.[B][C]") ]])
            fnCutscene([[ Append("Aquillia: Yeah okay, *mom*.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] Heh, all right all right.[B][C]") ]])
            fnCutscene([[ Append("Aquillia: What about you, Mei?[P] Want to get some delicious vengeance?[B][C]") ]])
            if(iHasSeenTrannadarFlorentinaScene == 1.0) then
                fnCutscene([[ Append("Florentina:[E|Neutral] We're currently trying to find her a way back home.[B][C]") ]])
                fnCutscene([[ Append("Aquillia: Forgive and forget, huh?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Sad] When you set out for revenge, first, dig two graves.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] It's a saying where I'm from.[B][C]") ]])
                fnCutscene([[ Append("Aquillia: You're a far better person than I, Mei. I hope you find your way.") ]])
            else
                
                --Alraune form.
                if(sMeiForm == "Alraune") then
                    fnCutscene([[ Append("Mei:[E|Sad] Revenge is...[P] not the Alraune way.[B][C]") ]])
                    fnCutscene([[ Append("Aquillia: Pah![B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] Even if it was, everyone back home is probably worried about me.[P] I have to let them know I'm okay.[P] That takes priority.[B][C]") ]])
                    fnCutscene([[ Append("Aquillia: That, I can't fault.[B][C]") ]])
                    fnCutscene([[ Append("Aquillia: I hope you find your way back home.") ]])
                
                --If Mei is a Bee during the conversation:
                elseif(sMeiForm == "Bee") then
                    fnCutscene([[ Append("Mei:[E|Neutral] The hive has given me a much more important mission.[P] Besides, I need to find a way back home.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] I need to at least tell my parents that I'm okay.[B][C]") ]])
                    fnCutscene([[ Append("Aquillia: You're thinking of them even at a time like this?[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Sad] They'll worry.[B][C]") ]])
                    fnCutscene([[ Append("Aquillia: ...[B][C]") ]])
                    fnCutscene([[ Append("Aquillia: I have a lot of respect for that, Mei.[P] Good luck.") ]])
                
                --If Mei is a Ghost during the conversation:
                elseif(sMeiForm == "Ghost") then
                    fnCutscene([[ Append("Mei:[E|Neutral] No, not at all. I've seen what revenge does to a person.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] I need to get home. I need to at least tell my parents that I'm okay.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Sad] Erm, in a manner of speaking...[B][C]") ]])
                    fnCutscene([[ Append("Aquillia: You're thinking of them even at a time like this?[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Sad] They'll worry.[B][C]") ]])
                    fnCutscene([[ Append("Aquillia: Maybe if you avoid letting them hug you?[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Happy] Yeah, that's the ticket![B][C]") ]])
                    fnCutscene([[ Append("Aquillia: I have a lot of respect for that, Mei.[P] Good luck.") ]])
                
                --If Mei is a Slime during the conversation:
                elseif(sMeiForm == "Slime") then
                    fnCutscene([[ Append("Mei:[E|Neutral] I need to go back home.[P] Let my parents know I'm okay.[B][C]") ]])
                    fnCutscene([[ Append("Aquillia: I think you're being a slime will have an impact on that.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Happy] A phone call will have to do, in that case.[B][C]") ]])
                    fnCutscene([[ Append("Aquillia: A what?[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] It's -[P] oh nevermind.[P] The point is, I can't let them worry about me.[B][C]") ]])
                    fnCutscene([[ Append("Aquillia: ...[B][C]") ]])
                    fnCutscene([[ Append("Aquillia: I have a lot of respect for that, Mei.[P] Good luck.") ]])
                
                --Human/unhandled.
                else
                    fnCutscene([[ Append("Mei:[E|Neutral] I need to get home.[P] My parents are probably worried sick.[B][C]") ]])
                    fnCutscene([[ Append("Aquillia: You're thinking of them even at a time like this?[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] I have obligations and I intend to honor them.[B][C]") ]])
                    fnCutscene([[ Append("Aquillia: You're a far better person than I, Mei.[P] I hope you find your way.") ]])
                end
            end
            
            --Common.
            fnCutsceneBlocker()
            
        --Normal dialogue.
        else
        
            --Common.
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Aquillia", "Cast") ]])
            
            --Variables.
            local iHasFlorentinaMetAquillia = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasFlorentinaMetAquillia", "N")
        
            --Florentina is present, but has not met Aquillia.
            if(iHasSeenTrannadarFlorentinaScene == 1.0 and iHasFlorentinaMetAquillia == 0.0) then
                VM_SetVar("Root/Variables/Chapter1/Scenes/iHasFlorentinaMetAquillia", "N", 1.0)
                fnCutscene([[ Append("Aquillia: Well what do we have here?[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Surprise] And just what kind of question is that?[B][C]") ]])
                fnCutscene([[ Append("Aquillia: You smell like money and perfume.[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Happy] Get used to it.[B][C]") ]])
                fnCutscene([[ Append("Aquillia: Is she a friend of yours, Mei?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] This is Florentina.[P] Florentina, meet Aquillia.[P] We broke out of cultist prison together.[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Surprise] Cultist prison?[P] That explains the excitement earlier.[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Neutral] All the plants started jabbering about a bunch of very angry people fanning out from the old mansion.[B][C]") ]])
                fnCutscene([[ Append("Aquillia: I guess they noticed we slipped out.[P] Took them long enough.[B][C]") ]])
                fnCutscene([[ Append("Aquillia: Wish I had time to leave them a fulminating caustic surprise, but I'm not much good with my arm like this.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Florentina promised to help me find a way back home.[B][C]") ]])
                fnCutscene([[ Append("Aquillia: Good luck with that.[P] Sorry I can't do much more than wish you well.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Happy] Knowing you're okay is enough.[B][C]") ]])
                fnCutscene([[ Append("Aquillia: Yeah, sure.[P] You sap.[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Happy] I think I like you, Aquillia.") ]])
                
            --Florentina is present and has met Aquillia.
            elseif(iHasSeenTrannadarFlorentinaScene == 1.0 and iHasFlorentinaMetAquillia == 1.0) then
            
                --If Mei happens to need some Kokayanee...
                local iCokeCount = AdInv_GetProperty("Item Count", "Kokayanee")
                local iTakenPieJob = VM_GetVar("Root/Variables/Chapter1/Breanne/iTakenPieJob", "N")
                if(iTakenPieJob == 1.0 and iCokeCount < 1) then
                    fnCutscene([[ Append("Mei:[E|Neutral] Aquillia, please don't take this the wrong way, but...[P][CLEAR]") ]])
                    fnCutscene([[ Append("Florentina:[E|Happy] Mei needs to get high.[P] You cool?[B][C]") ]])
                    fnCutscene([[ Append("Aquillia: Word.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Surprise] No, I - [P][CLEAR]") ]])
                    fnCutscene([[ Append("Aquillia: S'the least I can do for you.[P] What do you like?[P] Pixie-sticks?[P] LDS?[P] Bike-Chain?[B][C]") ]])
                    fnCutscene([[ Append("Florentina:[E|Neutral] Kokayanee, actually.[B][C]") ]])
                    fnCutscene([[ Append("Aquillia: Nerd.[B][C]") ]])
                    fnCutscene([[ Append("Florentina:[E|Happy] I want to start her off slow.[P] She's a neophyte in the light.[B][C]") ]])
                    fnCutscene([[ Append("Aquillia: You're still a nerd, but Florentina here is cool.[P] Here ya go.[P] First hit is free.[B][C]") ]])
                    fnCutscene([[ Append("[SOUND|World|TakeItem]*Got Kokayanee*[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Happy] Well, thanks, Aquillia![B][C]") ]])
                    fnCutscene([[ Append("Aquillia: I'm kidding, I'm not a dealer.[P] But I know some people who know some people.[P] If you want any of the good stuff, you talk to me.[B][C]") ]])
                    fnCutscene([[ Append("Florentina:[E|Neutral] This will do for now.[P] I'll tell you how she takes it.[B][C]") ]])
                    fnCutscene([[ Append("Aquillia: Natch.[P] Enjoy talking to the clouds.") ]])
            
                    --Add the item.
                    LM_ExecuteScript(gsItemListing, "Kokayanee")
            
                    --Reset flag.
                    WD_SetProperty("Clear Topic Read", "Pepper Pie")
            
                --Normal case:
                else
                    fnCutscene([[ Append("Aquillia: Waiting is the freakin' worst.[B][C]") ]])
                    fnCutscene([[ Append("Aquillia: It'll all gonna be worth it when I get my mitts on those taffing cultists...[B][C]") ]])
                    fnCutscene([[ Append("Florentina:[E|Surprise] Violence as a first resort?[B][C]") ]])
                    fnCutscene([[ Append("Florentina:[E|Happy] Aquillia...[P] Will you be my new best friend?[B][C]") ]])
                    fnCutscene([[ Append("Aquillia: ...[B][C]") ]])
                    fnCutscene([[ Append("Aquillia: You know, I thought today was going pretty badly.[P] But now?[P] I got a new best friend.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] (This is so ghastly...)") ]])
                end
            
            else
                fnCutscene([[ Append("Aquillia: If you change your mind, I'm not in a rush here.[B][C]") ]])
                fnCutscene([[ Append("Aquillia: My friend said it'll still be at least a week before my arm heals.[P] Arcane medicine ain't what it used to be.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Get better first.[B][C]") ]])
                fnCutscene([[ Append("Aquillia: Like I've got a choice.") ]])
            end
            
            --Common.
            fnCutsceneBlocker()
        end
    end
end
