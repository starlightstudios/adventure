-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
if(sObjectName == "RubberFinale") then
	
    -- |[Checking]|
    --Case check:
    local iIsRubberMode = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iIsRubberMode", "N")
    if(iIsRubberMode == 0.0) then return end
    
    -- |[Variables]|
    --Variables.
    local iRubberedAdina   = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iRubberedAdina", "N")
    local iRubberedPolaris = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iRubberedPolaris", "N")
    
    -- |[Flags]|
    VM_SetVar("Root/Variables/Chapter1/Scenes/Rubber|iIsRubberMode", "N", 0.0)
    
    --Issue load instruction.
    fnLoadDelayedBitmapsFromList("Chapter 1 Rubber Meteor", gciDelayedLoadLoadImmediately)
    
    --Blackout.
    fnCutscene([[ AL_SetProperty("Music", "Null") ]])
    fnCutscene([[ AL_SetProperty("Activate Fade", 0, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Move Aquillia away.
    fnCutsceneTeleport("Aquillia", -10.25, -27.50)
    
    -- |[Special NPCs]|
    --Position, spawn NPCs.
    fnCutsceneTeleport("Mei", 10.25, 27.50)
    TA_Create("Rochea")
        TA_SetProperty("Position", 10, 28)
        fnSetCharacterGraphics("Root/Images/Sprites/RocheaR/", false)
        TA_SetProperty("Facing", gci_Face_East)
    DL_PopActiveObject()
    if(iRubberedAdina == 1.0) then
        TA_Create("Adina")
            TA_SetProperty("Position", 10, 26)
            fnSetCharacterGraphics("Root/Images/Sprites/AdinaR/", false)
            TA_SetProperty("Facing", gci_Face_East)
        DL_PopActiveObject()
    else
        TA_Create("Adina")
            TA_SetProperty("Position", 10, 26)
            fnSetCharacterGraphics("Root/Images/Sprites/BeeGirlR/", false)
            TA_SetProperty("Facing", gci_Face_East)
        DL_PopActiveObject()
    end
    if(iRubberedPolaris == 1.0) then
        TA_Create("Polaris")
            TA_SetProperty("Position", 9, 27)
            fnSetCharacterGraphics("Root/Images/Sprites/PolarisR/", false)
            TA_SetProperty("Facing", gci_Face_East)
        DL_PopActiveObject()
    else
        TA_Create("Polaris")
            TA_SetProperty("Position", 9, 27)
            fnSetCharacterGraphics("Root/Images/Sprites/WerecatR/", false)
            TA_SetProperty("Facing", gci_Face_East)
        DL_PopActiveObject()
    end
    TA_Create("Miho")
        TA_SetProperty("Position", 9, 26)
        fnSetCharacterGraphics("Root/Images/Sprites/MihoR/", false)
        TA_SetProperty("Facing", gci_Face_East)
    DL_PopActiveObject()
    
    -- |[Generic NPCs]|
    --Spawner function.
    local fnSpawnAlly = function(sName, iX, iY)
        local iRoll = LM_GetRandomNumber(1, 4)
        local sSprite = "Root/Images/Sprites/AlrauneR/"
        if(iRoll == 2) then
            sSprite = "Root/Images/Sprites/WerecatR/"
        elseif(iRoll == 3) then
            sSprite = "Root/Images/Sprites/WerecatThiefR/"
        elseif(iRoll == 4) then
            sSprite = "Root/Images/Sprites/BeeGirlR/"
        end
        
        TA_Create(sName)
            TA_SetProperty("Position", iX, iY)
            fnSetCharacterGraphics(sSprite, false)
            TA_SetProperty("Facing", gci_Face_East)
            if(iRoll == 4) then
                TA_SetProperty("Y Oscillates", true)
                TA_SetProperty("Auto Animates", true)
            end
        DL_PopActiveObject()
    end
    
    --Spawn allies.
    local i = 0
    for x = 7, 10, 1 do
        for y = 26, 30, 1 do
            if    (x == 10 and y == 28) then
            elseif(x == 10 and y == 27) then
            elseif(x == 10 and y == 26) then
            elseif(x ==  9 and y == 27) then
            elseif(x ==  9 and y == 26) then
            else
                fnSpawnAlly("RubberAlly" .. i, x, y)
                i = i + 1
            end
        end
    end
    
    -- |[Hostile Army]|
    --Spawn hostiles.
    TA_Create("Florentina")
        TA_SetProperty("Position", 16, 27)
        fnSetCharacterGraphics("Root/Images/Sprites/Florentina/", false)
        TA_SetProperty("Facing", gci_Face_West)
    DL_PopActiveObject()
    TA_Create("Nadia")
        TA_SetProperty("Position", 16, 28)
        fnSetCharacterGraphics("Root/Images/Sprites/Nadia/", false)
        TA_SetProperty("Facing", gci_Face_West)
    DL_PopActiveObject()
    local fnSpawnHostile = function(sName, iX, iY)
        local iRoll = LM_GetRandomNumber(1, 4)
        local sSprite = "Root/Images/Sprites/Alraune/"
        if(iRoll == 2) then
            sSprite = "Root/Images/Sprites/Werecat/"
        elseif(iRoll == 3) then
            sSprite = "Root/Images/Sprites/WerecatThief/"
        elseif(iRoll == 4) then
            sSprite = "Root/Images/Sprites/BeeGirl/"
        end
        
        TA_Create(sName)
            TA_SetProperty("Position", iX, iY)
            fnSetCharacterGraphics(sSprite, false)
            TA_SetProperty("Facing", gci_Face_West)
            if(iRoll == 4) then
                TA_SetProperty("Y Oscillates", true)
                TA_SetProperty("Auto Animates", true)
            end
        DL_PopActiveObject()
    end
    
    --Spawn hostiles.
    fnSpawnHostile("Hostile0", 16, 29)
    fnSpawnHostile("Hostile1", 16, 30)
    fnSpawnHostile("Hostile2", 17, 27)
    fnSpawnHostile("Hostile3", 17, 28)
    fnSpawnHostile("Hostile4", 17, 29)
    fnSpawnHostile("Hostile5", 17, 30)
    fnSpawnHostile("Hostile6", 18, 27)
    fnSpawnHostile("Hostile7", 18, 28)
    fnSpawnHostile("Hostile8", 18, 29)
    fnSpawnHostile("Hostile9", 18, 30)
    
    -- |[Scene]|
    --Camera focus position.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 5.0)
        CameraEvent_SetProperty("Focus Position", (13.25 * gciSizePerTile), (28.00 * gciSizePerTile))
    DL_PopActiveObject()
    
    --Fade in.
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 1, gcf_Night_R, gcf_Night_G, gcf_Night_B, gcf_Night_A, true) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Florentina", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Nadia", "Neutral") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] Is this all we could get?[P] A dozen forest animals?[B][C]") ]])
    fnCutscene([[ Append("Nadia:[E|Neutral] It's the middle of the night![P] The Cap'n will be here any minute with the scouts![B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] I don't think another seven mercs is going to swing it.[P] This is just the first wave.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] There's at least thirty more behind them.[P] And...[P] Mei...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Florentina", 15.25, 27.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Miho", "Rubber") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Rochea", "Rubber") ]])
    if(iRubberedAdina == 1.0) then
        fnCutscene([[ WD_SetProperty("Actor In Slot", 6, "Adina", "Rubber") ]])
    end
    fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Florentina", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Nadia", "Neutral") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] Mei...[P] We'll save you...[P] somehow...[B][C]") ]])
    fnCutscene([[ Append("Nadia:[E|Neutral] Yeah![P] Just hold on, this stuff has to have a weakness![B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] (If they do not know our weakness by now, no amount of warriors will stop our spread.)[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] (I will achieve enlightenment soon...)[B][C]") ]])
    fnCutscene([[ Append("Miho:[E|Rubber] (Let me, master.[P] I must spread...)[B][C]") ]])
    if(iRubberedAdina == 1.0) then
        fnCutscene([[ Append("Rochea:[E|Rubber] (No, let me![P] Spread![P] Spread...)[B][C]") ]])
        fnCutscene([[ Append("Adina:[E|Rubber] (Don't think.[P] Let the master think.[P] Only spread...)") ]])
    else
        fnCutscene([[ Append("Rochea:[E|Rubber] (No, let me![P] Spread![P] Spread...)") ]])
    end
    fnCutsceneBlocker()
    
    -- |[Polaris Was Rubbered]|
    if(iRubberedPolaris == 1.0) then
        VM_SetVar("Root/Variables/Chapter1/Scenes/iRubberQuestState", "N", 4.0)
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Polaris", "Rubber") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Florentina", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Nadia", "Neutral") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] (No.[P] Stand away, thralls.[P] Mage-thrall.)[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Rubber] (Let me spread.[P] Command me.)[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] (Use your most powerful spell.[P] Spread.)[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Rubber] (Spread...)") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Fade to black.
        fnCutscene([[ AL_SetProperty("Activate Fade", 0, gci_Fade_Under_GUI, true, gcf_Night_R, gcf_Night_G, gcf_Night_B, gcf_Night_A, 0, 0, 0, 1, false) ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Activate Scene") ]])
        fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/RubberMeteor/RubberMeteor") ]])
        fnCutscene([[ Append("Florentina:[VOICE|Florentina] They're not attacking.[P] Get ready for something, everyone![B][C]") ]])
        fnCutscene([[ Append("Nadia:[VOICE|Nadia] Fl-[P]florry![P] Look up![P] It's a meteor![B][C]") ]])
        fnCutscene([[ Append("Florentina:[VOICE|Florentina] M-[P]made of pure rubber![P] They must have gotten Polaris![P] Scatter!!!") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(125)
        fnCutsceneBlocker()
        
        for i = 1, 30, 1 do
            local iRoll = LM_GetRandomNumber(1, 4)
            if(iRoll == 1) then
                fnCutscene([[ AudioManager_PlaySound("Rubber|SpreadA") ]])
            elseif(iRoll == 2) then
                fnCutscene([[ AudioManager_PlaySound("Rubber|ChangeB") ]])
            elseif(iRoll == 3) then
                fnCutscene([[ AudioManager_PlaySound("Rubber|SpreadBig") ]])
            elseif(iRoll == 4) then
                fnCutscene([[ AudioManager_PlaySound("Rubber|SpreadC") ]])
            end
            fnCutsceneBlocker()
            fnCutsceneWait(5)
            fnCutsceneBlocker()
        end
        fnCutsceneWait(125)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Florentina:[VOICE|Florentina] (Spread...)[B][C]") ]])
        fnCutscene([[ Append("Nadia:[VOICE|Nadia] (Spread...)") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Polaris", "Rubber") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Miho", "Rubber") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Florentina", "Rubber") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Nadia", "Rubber") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] (Well done, mage-thrall.[P] I have no further need of thralls...)[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Rubber] (Spread...)[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Rubber] (Spread...)[B][C]") ]])
        fnCutscene([[ Append("Nadia:[E|Rubber] (Master does not want to spread?)[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] (Thanks to this host-body, Mei, I have achieved enlightenment.[P] I can think clearly.[P] I understand my purpose.)[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] (But what has happened?[P] Have I been dormant for a thousand years?)[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] (None of your memories even know the name of the places I once conquered.)[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Rubber] (All our knowledge is yours.)[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] (No matter, I need time to reflect.[P] Time to think.[P] We will conquer this world when I have determined what has happened.)[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] (At that time my host, once called Mei, shall become my rubber queen.[P] All of you shall become my grinning thralls.)[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] (But even now our enemies stir.[P] If they unite against us, we will be defeated.[P] I see this clearly.[P] Already I have overextended.)[B][C]") ]])
        fnCutscene([[ Append("Miho:[E|Rubber] (As you wish...)[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] (I will withdraw to my fortress.[P] I shall take all your memories of this moment with me.[P] All of you will forget what has happened, but shall know of your service to me.)[B][C]") ]])
        fnCutscene([[ Append("Nadia:[E|Rubber] (We will forget...)[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] (When I have determined my course of action, I shall send out an inaudible call.[P] You will hear it.[P] You will come and become my thralls.)[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Rubber] (Yes...)[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Rubber] (We are yours...)[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] (And you, Mei, shall become my queen, my hostform.[P] Now, forget all.[P] Become as flesh again, thralls!)") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(125)
        fnCutsceneBlocker()
        
        --Clean images.
        fnCutscene([[ fnUnloadBitmapsFromList("Chapter 1 Rubber Meteor") ]])
        
        --Next scene.
        fnCutscene([[ AL_BeginTransitionTo("StarfieldSwampG", "FORCEPOS:41.0x32.0x0") ]])
        fnCutsceneBlocker()
        
    -- |[Polaris Was Not Rubbered]|
    else
        VM_SetVar("Root/Variables/Chapter1/Scenes/iRubberQuestState", "N", 10.0)
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Miho", "Rubber") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Rochea", "Rubber") ]])
        if(iRubberedAdina == 1.0) then
            fnCutscene([[ WD_SetProperty("Actor In Slot", 6, "Adina", "Rubber") ]])
        end
        fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Florentina", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Nadia", "Neutral") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] (We will defeat them.[P] Spread, thralls, spread!)[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Confused] This looks pretty bad.[P] We need a miracle right about now.[B][C]") ]])
        fnCutscene([[ Append("Nadia:[E|Neutral] Weapons up, here they come!") ]])
        fnCutsceneBlocker()
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Florentina", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Polaris", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Nadia", "Neutral") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] Hey, you said something about a miracle?[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Surprise] Woah, where'd you come from?[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] Haste magic.[P] I saw these...[P] things...[P] going by my cabin, and I did an experiment.[P] You know what they hate?[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Upset] Water.[P] It washes the rubber coating off.[B][C]") ]])
        fnCutscene([[ Append("Nadia:[E|Neutral] Quick, let's splash them with the river![B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] Oh, no need for that.[P] I wasn't twiddling my thumbs this whole time, you know.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] You should be feeling the results any second now...[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Surprise] ...[P] It's raining![B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Happy] Good thinking![P] A magical rainstorm![B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] What's the matter, rubber Mei?[P] Not feeling so confident now?[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] (N-[P]no![P] I was so close![P] Thralls![P] Thralls?[P] No, I'm losing them!)") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Fade to black.
        fnCutscene([[ AL_SetProperty("Activate Fade", 0, gci_Fade_Under_GUI, true, gcf_Night_R, gcf_Night_G, gcf_Night_B, gcf_Night_A, 0, 0, 0, 1, false) ]])
        fnCutsceneBlocker()
        fnCutsceneWait(125)
        fnCutsceneBlocker()
        
        --Clean images.
        fnCutscene([[ fnUnloadBitmapsFromList("Chapter 1 Rubber Meteor") ]])
        
        --Next scene.
        fnCutscene([[ AL_BeginTransitionTo("StarfieldSwampG", "FORCEPOS:45.0x32.0x0") ]])
        fnCutsceneBlocker()
    
    end
end
