-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

--Clear censor bars.
WD_SetProperty("Clear Censor Bars")

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then

    -- |[Name Resolve]|
    --Resolve the name of the calling entity.
    local sActorName = TA_GetProperty("Name")
	
    --Facing.
    TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[Slime Guide]|
    if(sActorName == "Guide") then
	
        --Set facing.
        TA_SetProperty("Face Character", "PlayerEntity")
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Slime:[VOICE|Slime] Hi again![P] Want to go back to the river?[BLOCK]") ]])
        
        --Decision script is this script. It must be surrounded by quotes.
        local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
        fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"Yes\") ")
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"No\") ")
        fnCutsceneBlocker()
    
    -- |[Gallery Attendant]|
    elseif(sActorName == "GalleryAttendant") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Slime:[VOICE|Slime] Come on in![P] We're currently showing some of our Slime Renaissance pieces as well as some Slime Modernist sculptures.[B][C]") ]])
        fnCutscene([[ Append("Slime:[VOICE|Slime] Be sure to check out 'Big Slime Portrait' and 'Big Gob of Slime', which are world famous!") ]])

    -- |[Gallery Slime A]|
    elseif(sActorName == "GallerySlimeA") then
        WD_SetProperty("Show")
        Append("Slime:[VOICE|Slime] Truly incredible.[P] The strokes, the posing, the color choices...")
    
    -- |[Gallery Slime B]|
    elseif(sActorName == "GallerySlimeB") then
        WD_SetProperty("Show")
        Append("Slime:[VOICE|Slime] I put my slime sculpture here but...[P] well, look at it.")
    
    -- |[Gallery Slime C]|
    elseif(sActorName == "GallerySlimeC") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Slime:[VOICE|Slime] This museum really only contains modern slime art.[P] If you want to see anything from the impressionist era, you're out of luck.[B][C]") ]])
        fnCutscene([[ Append("Mei:[VOICE|Mei] Impressionist era?[B][C]") ]])
        fnCutscene([[ Append("Slime:[VOICE|Slime] It was a very popular art form involving making an impression of an object in slime.[P] Very intellectually stimulating.[B][C]") ]])
        fnCutscene([[ Append("Slime:[VOICE|Slime] It just seems the art itself didn't last very long...[B][C]") ]])
        fnCutscene([[ Append("Mei:[VOICE|Mei] Gee, I wonder why...") ]])
    
    -- |[Gallery Slime D]|
    elseif(sActorName == "GallerySlimeD") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Slime:[VOICE|Slime] I call this piece 'Helmet I Found and Filled With Slime'.[P] What do you think?[B][C]") ]])
        fnCutscene([[ Append("Mei:[VOICE|Mei] ...[P] Breathtaking?[B][C]") ]])
        fnCutscene([[ Append("Slime:[VOICE|Slime] Thanks![P] I worked really hard on it!") ]])
    
    -- |[Gallery Slime E]|
    elseif(sActorName == "GallerySlimeE") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Slime:[VOICE|Slime] Have you met my jelly-belly buddies?[P] Aren't they so cute?![B][C]") ]])
        fnCutscene([[ Append("Slime:[VOICE|Slime] Lots of slimes keep jellyfish as pets, but I got mine on display so everyone can see them![P] Look, this little guy is happy to see you!") ]])
    
    -- |[House Slime A]|
    elseif(sActorName == "HouseSlimeA") then
        WD_SetProperty("Show")
        Append("Slime:[VOICE|Slime] Like, totally hi![P] Hi hi hi![P] This is my house!")
    
    -- |[House Slime B]|
    elseif(sActorName == "HouseSlimeB") then
        WD_SetProperty("Show")
        Append("Slime:[VOICE|Slime] We use leftover bones and hard animal bits to make art.[P] Isn't that cool?")
    
    -- |[House Slime C]|
    elseif(sActorName == "HouseSlimeC") then
	
        --Variables.
        local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
        
        --Common.
        fnStandardMajorDialogue()
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Slime", "Green") ]])
        
        --If Mei is a slime:
        if(sMeiForm == "Slime") then
            fnCutscene([[ Append("Slime: Hey there, cutie![P] I love your cytoplasm![P] How do you get it so firm?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] Uhhhh...[P] Diet and exercise?[B][C]") ]])
            fnCutscene([[ Append("Slime: Why didn't I think of that![P] Duh![P] You're so smart!") ]])
        
        --If Mei is a human:
        else
            fnCutscene([[ Append("Slime: Hey, are you a human?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] ...[P] Yes?[B][C]") ]])
            fnCutscene([[ Append("Slime: Wow![P] A human visitor![P] We don't get those very often![P] Or, like, ever![P] I don't know the etiquette, TBH.[B][C]") ]])
            fnCutscene([[ Append("Slime: Would you like something to drink?[BLOCK]") ]])

            --Decision script is this script. It must be surrounded by quotes.
            local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
            fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Sure\", " .. sDecisionScript .. ", \"Sure\") ")
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"No Thanks\",  " .. sDecisionScript .. ", \"No Thanks\") ")
            fnCutsceneBlocker()
        
        end
    
    -- |[Crowbar Chan]|
    elseif(sActorName == "CrowbarChan") then
	
        --Variables.
        local sMeiForm              = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
        local iMetCrowbarChan       = VM_GetVar("Root/Variables/Chapter1/Scenes/iMetCrowbarChan", "N")
        local iGotSlimeDancersDress = VM_GetVar("Root/Variables/Chapter1/Scenes/iGotSlimeDancersDress", "N")
        
        --Achievement.
        AM_SetPropertyJournal("Unlock Achievement", "MeetCrowbarChan")
        
        --Dialogue.
        fnStandardMajorDialogue()
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "CrowbarChan", "Neutral") ]])
        
        --Haven't met her yet:
        if(iMetCrowbarChan == 0.0) then
            fnCutscene([[ Append("CrowbarChan:[E|Neutral] Mei![P] Nyuuu![P] Good to see you![B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] Well I -[P] I -[P] I don't remember taking any drugs...[B][C]") ]])
            fnCutscene([[ Append("CrowbarChan:[E|Neutral] What, you don't recognize me?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Happy] I believe I'd remember a talking, floating crowbar.[P] So, no, I don't.[B][C]") ]])
            fnCutscene([[ Append("CrowbarChan:[E|Neutral] Oh, right.[P] This is adventure mode, isn't it?[B][C]") ]])
            fnCutscene([[ Append("CrowbarChan:[E|Neutral] I managed to score a cameo, nyuuu![P] I can break the fourth wall and nobody can do anything about it.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] So maybe you should tell me how I'm supposed to know you, then?[B][C]") ]])
            fnCutscene([[ Append("CrowbarChan:[E|Neutral] Well, I came in second-to-last place during the popularity poll.[P] You came in first.[P] That's why Chapter One is about you.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] Chapter One?[B][C]") ]])
            fnCutscene([[ Append("CrowbarChan:[E|Neutral] It's -[P] oh why bother.[P] We used to be best friends, but that was in Classic Mode.[B][C]") ]])
            fnCutscene([[ Append("CrowbarChan:[E|Neutral] Hopefully, once this is all over, we can all go out for drinks or something.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Happy] Sure.[P] Okay.[B][C]") ]])
            fnCutscene([[ Append("CrowbarChan:[E|Neutral] Nice seeing you again![P] Good luck on your adventure![P] Nyuuu![B][C]") ]])
            fnCutscene([[ Append("CrowbarChan:[E|Neutral] Oh, and Salty said he'd like to apologize.[P] He said there was absolutely no excuse for this.[B][C]") ]])
            
            
            --Not in slime form:
            if(sMeiForm ~= "Slime") then
                fnCutscene([[ Append("Mei:[E|Neutral] Backing away slowly now...") ]])
            
            --In slime form:
            else
                VM_SetVar("Root/Variables/Chapter1/Scenes/iGotSlimeDancersDress", "N", 1.0)
                LM_ExecuteScript(gsItemListing, "Dress of the Slime Dancer")
                fnCutscene([[ Append("Mei:[E|Neutral] Backing away slowly now...[B][C]") ]])
                fnCutscene([[ Append("CrowbarChan:[E|Neutral] Now hold up a second, Mei![P] It's time for a special present![B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] (What is this magic crowbar talking about?)[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] [SOUND|World|TakeItem](Received Dress of the Slime Dancer!)[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] It's a dress?[P] Uhm, I happen to like my body right now.[B][C]") ]])
                fnCutscene([[ Append("CrowbarChan:[E|Neutral] No, it's a slime dress![P] It's made specially for slime dancers![B][C]") ]])
                fnCutscene([[ Append("CrowbarChan:[E|Neutral] Because when you do a twirl and splatter the audience, they tend to get upset for some reason.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Oh, I see.[P] It merges with my body.[P] That's really cool![B][C]") ]])
                fnCutscene([[ Append("CrowbarChan:[E|Neutral] I knew you'd like it![P] Nyuu![P] Good luck!") ]])
            
            end
        
            VM_SetVar("Root/Variables/Chapter1/Scenes/iMetCrowbarChan", "N", 1.0)
        
        --Have met her:
        else
        
            --Haven't received the slime dancer dress yet:
            if(iGotSlimeDancersDress == 0.0 and sMeiForm == "Slime") then
                VM_SetVar("Root/Variables/Chapter1/Scenes/iGotSlimeDancersDress", "N", 1.0)
                LM_ExecuteScript(gsItemListing, "Dress of the Slime Dancer")
                fnCutscene([[ Append("CrowbarChan:[E|Neutral] Oh, Mei![P] There you are![P] Did you know it's Version 1.05?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] (What is this magic crowbar talking about?)[B][C]") ]])
                fnCutscene([[ Append("CrowbarChan:[E|Neutral] And that means I have a special present for nyuuu![B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] [SOUND|World|TakeItem](Received Dress of the Slime Dancer!)[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] It's a dress?[P] Uhm, I happen to like my body right now.[B][C]") ]])
                fnCutscene([[ Append("CrowbarChan:[E|Neutral] No, it's a slime dress![P] It's made specially for slime dancers![B][C]") ]])
                fnCutscene([[ Append("CrowbarChan:[E|Neutral] Because when you do a twirl and splatter the audience, they tend to get upset for some reason.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Oh, I see.[P] It merges with my body.[P] That's really cool![B][C]") ]])
                fnCutscene([[ Append("CrowbarChan:[E|Neutral] I knew you'd like it! Nyuu! Good luck!") ]])
        
            --Normal:
            else
                fnCutscene([[ Append("CrowbarChan:[E|Neutral] I'm still kinda steamed about the popularity poll.[P] I heard Sanya got a big makeover, how come I didn't?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Smirk] Because you're -[P] so cute already?[B][C]") ]])
                fnCutscene([[ Append("CrowbarChan:[E|Neutral] Nyuuu![P] You always know what to say!") ]])
            end
        end
    end

-- |[ ================================ House Slime C Responses ================================= ]|
--Drink something given to you by a slime. By the way, you're an idiot.
elseif(sTopicName == "Sure") then
	
	--Variables.
	local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
    
    --Load images.
    fnLoadDelayedBitmapsFromList("Chapter 1 Mei Slime TF", gciDelayedLoadLoadAtEndOfTick)
    
    --Flags.
    VM_SetVar("Root/Variables/Chapter1/Scenes/iGotSlimedFromDrink", "N", 1.0)
	
	--Reboot the dialogue.
	fnStandardMajorDialogue(true)
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Slime", "Green") ]])
	
	--Base.
	fnCutscene([[ Append("Mei:[E|Neutral] Sure, why not?[B][C]") ]])
	
	--Florentina has a bit of a problem with this:
	if(bIsFlorentinaPresent == true) then
		fnCutscene([[ Append("Florentina:[E|Confused] Seriously, Mei?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] Oh, come off it.[P] She seems really nice.[B][C]") ]])
		fnCutscene([[ Append("Slime: I'm totally the nicest slime and junk![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Smirk] See?[P] Look at those puppy-dog eyes![B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] If you get sick, it's not my problem.[B][C]") ]])
	end
	
	--Resume.
	fnCutscene([[ Append("Mei:[E|Smirk] *Glug*[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] ...[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] ...[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Happy] Delicious![P] What was it?[B][C]") ]])
	fnCutscene([[ Append("Slime: Good old plain water![P] It's all we slimes drink![B][C]") ]])
	
	--Florentina isn't going to eat any crow.
	if(bIsFlorentinaPresent == true) then
		fnCutscene([[ Append("Mei:[E|Offended] See, Florentina?[P] No need to be suspicious![B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] Oh yeah, of course it was water.[P] Thick, viscous, green water.[B][C]") ]])
	end
	fnCutscene([[ Append("Slime: Oh, whoops.[P] I think I got some of my slime in there...[P] whoopsie doodle![B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Smirk] ...[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Sad] I feel...[P] kinda light headed...") ]])
	fnCutsceneBlocker()
	
	--Switch to scene mode.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Null") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/SlimeTF0") ]])
	fnCutscene([[ Append("Mei:[Voice|Mei] Oh no![B][C]") ]])
	fnCutscene([[ Append("Slime:[Voice|Slime] I'm sorry![P] Here, let me help you![B][C]") ]])
	fnCutscene([[ Append("Mei:[Voice|Mei] You're just getting more slime on me![B][C]") ]])
	fnCutscene([[ Append("Mei began to panic, thrashing about as she tried to shake the slime off.[P] Her efforts were in vain, as the slime infection was coming from deep within her.[B][C]") ]])
	fnCutscene([[ Append("The slime girl desperately padded at Mei, trying to wipe away what she could.[P] Everywhere she touched, more slime went.") ]])
	fnCutsceneBlocker()
	
	--Next scene.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/SlimeTF0") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/SlimeTF1") ]])
	fnCutscene([[ Append("Mei:[Voice|Mei] K-keep going...[B][C]") ]])
	fnCutscene([[ Append("The infection reached up to Mei's head.[P] Suddenly, the spread of the slime wasn't a problem.[P] In fact, it began to feel pleasurable...[B][C]") ]])
	fnCutscene([[ Append("Slime:[Voice|Slime] Oops![P] Oh that -[P] oh my![B][C]") ]])
	fnCutscene([[ Append("Mei:[Voice|Mei] More slime...[P] rub me...[B][C]") ]])
	fnCutscene([[ Append("Slime:[Voice|Slime] Oh geez, I'm messing this up![B][C]") ]])
	fnCutscene([[ Append("Mei:[Voice|Mei] More...[B][C]") ]])
	fnCutscene([[ Append("The slime took over Mei's body.[P] Her slime sister ran her goopy hands all over Mei's body, arousing her more and more with each passing moment.[B][C]") ]])
	fnCutscene([[ Append("She began to rub herself in tandem, swirling her own slime around.[P] The sensation was incredible, an orgasm more intense than any she had experienced as a human...") ]])
	fnCutsceneBlocker()
	
	--Finished.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Slime.lua") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/SlimeTF1") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Mei/SlimeTF2") ]])
    fnCutscene([[ WD_SetProperty("Register Censor Bar", 612, 182, 100, 41) ]])
	fnCutscene([[ Append("Mei:[Voice|Mei] Yeah...[P] yeah...[P] I'm feeling so much better...[B][C]") ]])
	fnCutscene([[ Append("She had become slime like the girl next to her.[P] Every inch of her body was now an erogenous zone, and she slid her slimy hands up and down her slimy membrane.[P] The squishing, slopping feeling brought her to climax as her slime sister withdrew.[B][C]") ]])
	fnCutscene([[ Append("The slime stepped back, head hung in shame.[P] Mei placed her slimy hand under the girl's chin and lifted her head up.[P] Mei smiled as broadly as she could, and brought solace to the poor slime girl.") ]])
	fnCutsceneBlocker()
	
	--Reboot the dialogue.
	fnStandardMajorDialogue(true)
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Slime", "Green") ]])
	fnCutscene([[ Append("Slime: Ah geez I am sooooooo super sorry![B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Happy] Whatever for?[P] I'm all slimy![B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Happy] And I got your smarty fruit in me![P] I'm a smarty slime![B][C]") ]])
	if(bIsFlorentinaPresent == true) then
		fnCutscene([[ Append("Florentina:[E|Happy] ...[P] Dumbass.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Laugh] You're just jealous![B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] Well, you can't be any dumber now that you're a slime.[P] So, no great loss there.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Thanks, Florentina![B][C]") ]])
	end
	fnCutscene([[ Append("Mei:[E|Neutral] Well, thank you for this eye-opening experience.[B][C]") ]])
	fnCutscene([[ Append("Slime: You're okay with this?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Happy] Sure![P] This is awesome![B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Happy] But I've got an adventure to get back to.[P] See you later!") ]])
	fnCutsceneBlocker()
    
    --Clean up images.
    fnCutscene([[ fnUnloadBitmapsFromList("Chapter 1 Mei Slime TF") ]])

--No thank you.
elseif(sTopicName == "No Thanks") then
	
	--Variables.
	local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnStandardMajorDialogue(true)
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Slime", "Green") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Erm, no thanks.[P] Not thirsty.[B][C]") ]])
	
	if(bIsFlorentinaPresent == true) then
		fnCutscene([[ Append("Slime: Oh, okay![P] See you later, human![B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] *You're smarter than I gave you credit for, Mei.*[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] *What?[P] I'm really not thirsty!*") ]])
	else	
		fnCutscene([[ Append("Slime: Oh, okay![P] See you later, human!") ]])
	end

-- |[ ================================== Slime Guide Responses ================================= ]|
--Go back.
elseif(sTopicName == "Yes") then
	
    --Unload the scene data.
    fnUnloadBitmapsFromList("Chapter 1 Slime Gallery")
    
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnCutscene([[ WD_SetProperty("FastShow") ]])
	fnCutscene([[ Append("Slime:[VOICE|Slime] All right![P] Follow me!") ]])
	fnCutsceneBlocker()
	
	--Transition to Evermoon East.
	fnCutscene([[ AL_BeginTransitionTo("EvermoonE", "FORCEPOS:41.0x13.0x0") ]])
	fnCutsceneBlocker()
	
	--Wait.
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Mei faces north.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Face",  0, -1)
	DL_PopActiveObject()
	
--Stay.
elseif(sTopicName == "No") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnCutscene([[ WD_SetProperty("FastShow") ]])
	fnCutscene([[ Append("Slime:[VOICE|Slime] Okay![P] Just let me know when you're ready!") ]])
	fnCutsceneBlocker()
end
