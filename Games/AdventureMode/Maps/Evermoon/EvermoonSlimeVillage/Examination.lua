-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Execution]|
--Barrels full of bones.
if(sObjectName == "BoneBarrels") then

	fnStandardDialogue([[ [VOICE|Mei](Barrels full of bones.[P] Looks like there's bits of paint on some of them.[P] And a lot of slime.) ]])

--Fruit wot makes da slimes smarta!
elseif(sObjectName == "SmartyFruits") then

	fnStandardDialogue([[ [VOICE|Mei](These barrels are all labelled 'Smarty Fruits'.[P] They're full of apples.) ]])
	
--Water!
elseif(sObjectName == "WaterBarrels") then

	fnStandardDialogue([[ [VOICE|Mei](Barrels full of water.[P] That's all the slimes need.) ]])
	
--Food!
elseif(sObjectName == "FoodShelves") then

	fnStandardDialogue([[ [VOICE|Mei](Slime delicacies, such as 'Gob of meat' and 'I can't tell what this is'.[P] Slimes aren't exactly picky.) ]])
	
--Aquarium with Hagfish.
elseif(sObjectName == "AquariumHagfish") then

	fnStandardDialogue([[ [VOICE|Mei](An aquarium with a big hagfish in it.[P] According to the sign, hagfish are often called 'Slime Eels'.) ]])
	
--Aquarium with Jellyfish.
elseif(sObjectName == "AquariumJellyfish") then

	fnStandardDialogue([[ [VOICE|Mei](An aquarium with several jellyfish floating in it.[P] They seem happy enough, considering they don't have brains.) ]])
	
--Aquarium with nothing in it.
elseif(sObjectName == "AquariumEmpty") then

	fnStandardDialogue([[ [VOICE|Mei](An aquarium.[P] This one has some small shells and fish swimming around, but nothing big.) ]])
	
--Slime books!
elseif(sObjectName == "SlimeBookA") then
	fnStandardDialogue([[ [VOICE|Mei](Goo-livers Travels.) ]])
elseif(sObjectName == "SlimeBookB") then
	fnStandardDialogue([[ [VOICE|Mei](Slime and Prejudice.) ]])
elseif(sObjectName == "SlimeBookC") then
	fnStandardDialogue([[ [VOICE|Mei](A Tale of One Big Goopy City.) ]])
elseif(sObjectName == "SlimeBookD") then
	fnStandardDialogue([[ [VOICE|Mei](The Count of Sloppy Cristo.) ]])
elseif(sObjectName == "SlimeBookE") then
	fnStandardDialogue([[ [VOICE|Mei](The Phantom of the Slopra.) ]])
elseif(sObjectName == "SlimeBookF") then
	fnStandardDialogue([[ [VOICE|Mei](War, Peace, and Slime Orgies.) ]])
elseif(sObjectName == "SlimeBookG") then
	fnStandardDialogue([[ [VOICE|Mei](The Slime Woman in White.) ]])
elseif(sObjectName == "SlimeBookH") then
	fnStandardDialogue([[ [VOICE|Mei](The Picture of Dorothy Grey.[P] A slime gets a painting made of her, but neither she nor the painting ever ages -[P] because slimes don't age.[P] What a twist!) ]])
elseif(sObjectName == "SlimeBookI") then
	fnStandardDialogue([[ [VOICE|Mei](Goo-lysses.) ]])
elseif(sObjectName == "SlimeBookJ") then
	fnStandardDialogue([[ [VOICE|Mei](Animal Farm.[P] Unlike the other slime books, this is George Orwell's classic and has no slime puns whatsoever.) ]])

--Lady Goodiva.
elseif(sObjectName == "Goodiva") then

    --Clear censor bars.
    WD_SetProperty("Clear Censor Bars")

	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/ArtGallery/Goodiva") ]])
	fnCutscene([[ WD_SetProperty("Set Dialogue Hiding", true) ]])
	fnCutsceneBlocker()
	fnCutscene([[ WD_SetProperty("FastShow") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/ArtGallery/Goodiva") ]])
	fnCutscene([[ Append("[VOICE|Mei]'Lady Goodiva'.[P] Long ago, a slime countess appealed to her wife to reduce the high taxes paid by their slime subjects.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|Mei]Her wife declared she would abolish the taxes if Goodiva rode through their hamlet while clothed in pajamas -[P] unconscionable at the time, much less today.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|Mei]While Goodiva believed she was sacrificing her own reputation by appearing in pajamas, the slimes of the hamlet took pity upon her disgusting, clothed form.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|Mei]Today, Goodiva represents the quiet dignity of a clothed slime.[P] A great heart overcomes physical appearance in the end.") ]])
	
--The Molda Lisa
elseif(sObjectName == "MoldaLisa") then

    --Clear censor bars.
    WD_SetProperty("Clear Censor Bars")
    
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/ArtGallery/SlimaLisa") ]])
	fnCutscene([[ WD_SetProperty("Set Dialogue Hiding", true) ]])
	fnCutsceneBlocker()
	fnCutscene([[ WD_SetProperty("FastShow") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/ArtGallery/SlimaLisa") ]])
	fnCutscene([[ Append("[VOICE|Mei]'The Molda Lisa'.[P] While a subject of much debate amongst slime art historians, this painting by Blechanardo Goopvinci has captivated many slimes through history.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|Mei]Some slime historians believe it is an image of the Virgin Moldy, said to be the first slime girl and therefore a 'virgin birth'.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|Mei]Others believe that Molda Lisa is actually an exercise in partirhuman visual specialization.[P] Molda Lisa's smile is less pronounced when seen by non-slimes, suggesting the perfection of the slime form.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|Mei]Regardless, Molda Lisa continues to be one of the greatest works of slime art in history.") ]])
	
--Washlimeton
elseif(sObjectName == "Washlimeton") then

    --Clear censor bars.
    WD_SetProperty("Clear Censor Bars")
    
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/ArtGallery/Washlimeton") ]])
	fnCutscene([[ WD_SetProperty("Set Dialogue Hiding", true) ]])
	fnCutsceneBlocker()
	fnCutscene([[ WD_SetProperty("FastShow") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
	fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/ArtGallery/Washlimeton") ]])
	fnCutscene([[ Append("[VOICE|Mei]'Washlimeton Crossing the Delasquelch'.[P] This image depicts the famous slime general Washlimeton leading her army over the Delasquelch river in a daring night raid.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|Mei]Fought during the famed Revosquelchary War, Washlimeton's army of slime girls brought liberty and freedom from the taxes and oppression of the hated Britsquish Empire.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|Mei]This image was actually painted many years after the fact by a slime girl who wanted to inspire the liberal masses of her own slime country that likewise felt the yolk of hated arisloshcracy.[B][C]") ]])
	fnCutscene([[ Append("[VOICE|Mei]The strong pose of Washlimeton and her soldiers demonstrate the strength of a slime girl who maintains her legs and is not an amorphous mass.[P] Surely something we all aspire to.") ]])
	
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
