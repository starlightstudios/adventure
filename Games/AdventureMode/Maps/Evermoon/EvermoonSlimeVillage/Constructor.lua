-- |[Constructor]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "EvermoonSlimeVillage"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[Standard]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
	AL_SetProperty("Music", "TownTheme")
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
    
    -- |[Map Handling]|
    --Resolve the map's variable.
    fnStandardCh1Map(sLevelName)
	
	-- |[Spawn NPCS Here]|
	--NPCs set to spawn here will be available for post-exec cutscenes.
    fnStandardNPCByPosition("Guide")

-- |[Post-Exec]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	-- |[Spawn NPCs Here]|
	--If an entity needs to spawn after a post-exec cutscene, put them here.
    fnSpawnNPCPattern("GallerySlime", "A", "E")
    fnSpawnNPCPattern("HouseSlime",   "A", "C")
    fnStandardNPCByPosition("GalleryAttendant")
    fnStandardNPCByPosition("CrowbarChan")
	
	--Crowbar-Chan. Has her own special sprite set.
	EM_PushEntity("CrowbarChan")
		for i = 1, 8, 1 do
			TA_SetProperty("Move Frame", i-1, 0, "Root/Images/Sprites/CrowbarChan/0")
			TA_SetProperty("Move Frame", i-1, 1, "Root/Images/Sprites/CrowbarChan/1")
			TA_SetProperty("Move Frame", i-1, 2, "Root/Images/Sprites/CrowbarChan/2")
			TA_SetProperty("Move Frame", i-1, 3, "Root/Images/Sprites/CrowbarChan/1")
		end
	DL_PopActiveObject()
	
    -- |[Overlays]|
    fnHeavyForest()

    -- |[Scenes]|
    fnLoadDelayedBitmapsFromList("Chapter 1 Slime Gallery", gciDelayedLoadLoadAtEndOfTick)

end
