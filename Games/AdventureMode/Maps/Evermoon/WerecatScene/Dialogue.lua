-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)


-- |[Variables]|
--If variables need to be resolved commonly, do so here.

--Clear censor bars.
WD_SetProperty("Clear Censor Bars")

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[Name Resolve]|
    --Resolve the name of the calling entity.
    local sActorName = TA_GetProperty("Name")

    -- |[Nadia]|
    if(sActorName == "Nadia") then
	
        --This is always a major dialogue.
        fnStandardMajorDialogue()
        fnCutscene([[ Append("Nadia:[VOICE|Nadia] Zzzzzz...[B][C]") ]])
        
        --Dialogue if Mei hasn't slept with Nadia yet:
        local iMeiHasDoneNadia = VM_GetVar("Root/Variables/Chapter1/ScenesWerecat/iMeiHasDoneNadia", "N")
        if(iMeiHasDoneNadia == 0.0) then
            fnCutscene([[ Append("Mei:[E|Blush] (She's sleeping.[P] She's...[P] so pretty...)[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Blush] (Why am I so horny all of the sudden?[P] The moonlight[P] is making my skin tingle...)[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Blush] (I could...[P] take her...[P] yes...[P] she would want it...)[BLOCK]") ]])

            --Decision script is this script. It must be surrounded by quotes.
            local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
            fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Take Her\", " .. sDecisionScript .. ", \"Take\") ")
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Leave Her\",  " .. sDecisionScript .. ", \"Leave\") ")
            fnCutsceneBlocker()
        
        else
            fnCutscene([[ Append("Mei:[E|Blush] (How beautiful her form is...[P] but the night calls...)") ]])
        end
    end
    
-- |[ =================================== Response to Nadia ==================================== ]|
--H-Scene with Nadia.
elseif(sTopicName == "Take") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Set flags.
	VM_SetVar("Root/Variables/Chapter1/ScenesWerecat/iMeiHasDoneNadia", "N", 1.0)
	
	--Don't set the topic if this is a relive case.
	local iIsRelivingScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iIsRelivingScene", "N")
	if(iIsRelivingScene == 0.0) then
		WD_SetProperty("Unlock Topic", "Lover", 1)
	end
	
	--Reboot the dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Nadia", "Neutral") ]])
	fnCutscene([[ Append("Nadia: ..![P] Mei?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] ...[B][C]") ]])
	fnCutscene([[ Append("Nadia: What are you doing?[P] S-[P]Stop![B][C]") ]])
	fnCutscene([[ Append("Mei: Shhhhhhhh.") ]])
	fnCutsceneBlocker()
	
	--Scene.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Activate Scene") ]])
	fnCutscene([[ Append("Mei's fingers danced across Nadia's lithe, prone form.[P] Her armor, covered in the dirt of the day's travels, had been set aside, leaving the plant girl in her leafy underwear.[B][C]") ]])
	fnCutscene([[ Append("Nadia attempted to protest, pushing against Mei, but her hand was limp, uncertain, and her arms grew weak.[P] Mei held firm as her fingers traced the contours of Nadia's form.[P] Nadia pressed back less each moment.[B][C]") ]])
	fnCutscene([[ Append("'S-[P]stop...' Nadia pleaded.[P] Mei delicately traced her hands along Nadia's stiffening nipples, pausing only long enough to give each a delicate flick.[P] Nadia's protests soon ceased.[B][C]") ]])
	fnCutscene([[ Append("Now Mei lay down next to Nadia, her tongue following the paths her fingers had so recently danced along.[P] A final appeal began to rise from the plant girl's lips, but Mei silenced it with her tongue.[B][C]") ]])
	fnCutscene([[ Append("As the two kissed and caressed one another, Mei pressed her body closer and felt Nadia's trembling fingers begin their own exploration of her body.[P] There was no hesitation, no withdrawal, just desire.[B][C]") ]])
	fnCutscene([[ Append("Nadia's touch was passive and delicate, waiting for each move Mei made.[P] She would give only a token resistance before surrendering in the end.[P] A quiet moan was her only protest as Mei began to massage the Alraune's flowering sex.[B][C]") ]])
	fnCutscene([[ Append("Mei had masturbated many times before, and her fingers worked with an expert's touch.[P] Arousing her mate proved to be simple and enjoyable as she rubbed the firm bud of Nadia's clitoris with her fingers.[P] The first hint of nectar began to waft from the girl's body, its sweet scent encouraging Mei's fingers.[B][C]") ]])
	fnCutscene([[ Append("'Aren't you going to stop me?' Mei asked with a sinister tone.[P] She smiled wickedly as Nadia meekly looked away.[B][C]") ]])
	fnCutscene([[ Append("Mei used her free hand to pull Nadia's head back, and kissed her again.[P] Her smooth, blue flesh tasted faintly of the perfume that all Alraunes emitted, and the scent of nectar caught in her breath.[P] Mei treasured the sensation.[B][C]") ]])
	fnCutscene([[ Append("Soon Nadia's hips began to move in time with Mei's fingers as she continued to massage the Alraune's sex, thrusting and drawing away in tempo with the gasping moans of her breath.[B][C]") ]])
	fnCutscene([[ Append("Reluctantly, Nadia's own fingers, still trembling along the edges of Mei's body, slid inward, resting for only a moment in the heat of Mei's engorged sex before making their first explorative plunge within.[P] Moonlight glinted in from a gap in the clouds, lighting up every nerve on Mei's skin as it danced across her.[B][C]") ]])
	fnCutscene([[ Append("Her awareness suddenly heightened, Mei increased her pace, and the Alraune gasped.[P] Nadia could not weather the sudden barrage of pleasure, and she grasped for her lover's hand.[P] Mei smiled as she allowed her to pleasure herself as she slid her own hands, slick with a hot nectar, to toy once more with Nadia's breasts.[B][C]") ]])
	fnCutscene([[ Append("Moaning, sweating, Nadia brought herself to orgasm, but Mei did not relent.[P] Taking pleasure in the vulnerable, exposed form of Nadia, her fingers continued to massage the girl's erect nipples.[P] She could see perfectly in the darkness.[P] She could see the interplay of light and shadow over Nadia's darkened, curvaceous form.[B][C]") ]])
	fnCutscene([[ Append("Only with great reluctance could Mei pry herself away.[P] Nadia had already fallen asleep after the intensity of her orgasm.[P] Mei stood and drew the blanket over the sleeping Alraune.[P] Something in the night was calling her, and she had to answer...") ]])
	
--Leave Nadia alone.
elseif(sTopicName == "Leave") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
end
