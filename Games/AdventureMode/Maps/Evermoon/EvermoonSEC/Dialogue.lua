-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    --Set facing.
    TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[Ginny]|
    if(sActorName == "Ginny") then
    
        --Variables.
        local iMetGinny = VM_GetVar("Root/Variables/Chapter1/Scenes/iMetGinny", "N")
        local iHasSeenTrannadarFlorentinaScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFlorentinaScene", "N")
        
        --First meeting.
        if(iMetGinny == 0.0) then
            
            --Dialogue.
            fnStandardMajorDialogue()
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Ginny", "Neutral") ]])
            VM_SetVar("Root/Variables/Chapter1/Scenes/iMetGinny", "N", 1.0)
            
            --Florentina is not here:
            if(iHasSeenTrannadarFlorentinaScene == 0.0) then
                fnCutscene([[ Append("Werecat:[E|Neutral] Oh, hello.[P] Are you here to purchase fresh fish?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Surprise] You're -[P] are you Ginny?[B][C]") ]])
                fnCutscene([[ Append("Ginny:[E|Neutral] Does my reputation precede me?[P] I am a fine fisher, I suppose, but not that fine.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Well, when Alicia mentioned her wife...[B][C]") ]])
                fnCutscene([[ Append("Ginny:[E|Neutral] Ah, Alicia sent you![P] Very good, but it's not time for dinner quite yet.[P] Tell her I'll be at least another hour.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Blush] So...[P] Uh...[P] Did you marry her before or after...[B][C]") ]])
                fnCutscene([[ Append("Ginny:[E|Neutral] Ha ha ha![P] My friend, I can see on your face that you think our relationship is unusual.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Surprise] N-[P]no![B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Sad] ...[P] I don't want to be insensitive...[B][C]") ]])
                fnCutscene([[ Append("Ginny:[E|Neutral] I take no offense, ha ha ha![B][C]") ]])
                fnCutscene([[ Append("Ginny:[E|Neutral] If you must know, I met her only a few winters ago.[P] She is quite a human![B][C]") ]])
                fnCutscene([[ Append("Ginny:[E|Neutral] And no, I am not interested in turning her, or anyone else.[P] While she would make a fine fang, she has no interest in it.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Oh, so...[P] Okay.[B][C]") ]])
                fnCutscene([[ Append("Ginny:[E|Neutral] If you've heard any rumours about her, they are probably false.[P] There are many humans who despise partirhumans, and relationships between us and them -[P] forbidden.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Offended] Not so different to where I'm from...[P] but in a different way.[B][C]") ]])
                fnCutscene([[ Append("Ginny:[E|Neutral] I used to think the same way.[P] But, then I met her.[P] She changed my mind, for the better.[B][C]") ]])
                fnCutscene([[ Append("Ginny:[E|Neutral] And no, she is at no risk living out here in the woods.[P] Any other who casts a hostile glance at her would have to deal with me.[B][C]") ]])
                fnCutscene([[ Append("Ginny:[E|Neutral] Though I prefer fishing to fighting.[P] Speaking of...[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Smirk] Ah, I'm not hungry right now.[P] But thank you for the offer.[B][C]") ]])
                fnCutscene([[ Append("Ginny:[E|Neutral] Well, then back to work.[P] Come to me, fishies!") ]])
                fnCutsceneBlocker()
            
            --Florentina is here:
            else
                fnCutscene([[ Append("Werecat:[E|Neutral] Oh, hello.[P] Are you here to purchase fresh fish?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Surprise] You're -[P] are you Ginny?[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Happy] Oh my, the rumours really were true.[B][C]") ]])
                fnCutscene([[ Append("Ginny:[E|Neutral] Rumours?[P] Oh, you are Florentina, yes?[P] Alicia mentioned you.[P] The Alraune with an eyepatch.[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Confused] Uh oh...[B][C]") ]])
                fnCutscene([[ Append("Ginny:[E|Neutral] Ha ha![P] If Alicia has a problem with you, she is strong enough to solve it herself.[B][C]") ]])
                fnCutscene([[ Append("Ginny:[E|Neutral] But of course, there are two of you.[P] A two-on-two may be fun![B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Smirk] Uhhhh, pass?[B][C]") ]])
                fnCutscene([[ Append("Ginny:[E|Neutral] Suit yourself.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] So, er, did you marry Alicia before -[P] or after...[B][C]") ]])
                fnCutscene([[ Append("Ginny:[E|Neutral] Do you think our relationship unusual?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Surprise] N-[P]no![B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Sad] ...[P] I don't want to be insensitive...[B][C]") ]])
                fnCutscene([[ Append("Ginny:[E|Neutral] I take no offense, ha ha![P] There are those, human and otherwise, who view such relationships with contempt.[B][C]") ]])
                fnCutscene([[ Append("Ginny:[E|Neutral] I counted myself among them, but then I met Alicia.[P] She is a special human, and my mind was changed.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Kind of reminds me of home...[P] I guess things aren't so different.[B][C]") ]])
                fnCutscene([[ Append("Ginny:[E|Neutral] I've learned much by keeping my mind open.[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Neutral] You know what?[P] Good for you, Ginny.[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Confused] People are always looking for an excuse to hate, like they need permission.[P] Nuts to them.[B][C]") ]])
                fnCutscene([[ Append("Ginny:[E|Neutral] Hmm, maybe the things Alicia said about you were not true after all...[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Happy] Oh they probably were.[P] I'm a jerk.[B][C]") ]])
                fnCutscene([[ Append("Ginny:[E|Neutral] Ha ha ha ha![P] Very good![B][C]") ]])
                fnCutscene([[ Append("Ginny:[E|Neutral] But if you are not here to purchase fish, I must return to work.[P] Come to me, fishies!") ]])
            
            end
            
        --Successive meetings.
        else
            fnStandardMajorDialogue()
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Ginny", "Neutral") ]])
            fnCutscene([[ Append("Ginny:[E|Neutral] Heh, I've caught many fish today, but none of the rare species.[P] My luck will turn around soon, though!") ]])
        
        end
    end
end
