-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Execution]|
--Bag, just flavour.
if(sObjectName == "Bag") then
	fnStandardDialogue([[[VOICE|Mei] (Nothing in the bag except dust.) ]])

--Door. Can be opened from the south side only.
elseif(sObjectName == "StuckDungeonDoor") then

	--Get the side variable. If it's 0, we're on the north side.
	local iEvermoonNEDoorFromSouth = VM_GetVar("Root/Variables/Chapter1/WorldState/iEvermoonNEDoorFromSouth", "N")
	if(iEvermoonNEDoorFromSouth == 0.0) then
		
		--SFX.
		AudioManager_PlaySound("World|RemoteDoor")
		
		--If the player has a pry bar...
		local iHasPryBar = AdInv_GetProperty("Item Count", "Pry Bar")
		if(iHasPryBar >= 1) then
			fnStandardDialogue([[ [VOICE|Mei](The door is stuck.[P] I can't get it open from this side, not even with my prying bar.) ]])
		
		--No pry bar.
		else
			fnStandardDialogue([[ [VOICE|Mei](The door is stuck.[P] I can't get it open from this side.) ]])
		end
		
	--We're on the south side, open the door.
	else
		AL_SetProperty("Open Door", "Door")
		AudioManager_PlaySound("World|OpenDoor")
		fnStandardDialogue([[ (There, got the door unstuck.) ]])
		VM_SetVar("Root/Variables/Chapter1/WorldState/iEvermoonNEDoorOpen", "N", 1.0)
	end

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
