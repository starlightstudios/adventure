-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[ ==================================== Campsite Battle ===================================== ]|
if(sObjectName == "CampFight") then
	
	--In all cases, mark the combat intro dialogues as complete.
	VM_SetVar("Root/Variables/Chapter1/Counts/sMeiSeenPartirhuman", "S", "Complete")

	--Nothing happens if this encounter has already played out.
	local iEncounteredCatCampsite = VM_GetVar("Root/Variables/Chapter1/Scenes/iEncounteredCatCampsite", "N")
	if(iEncounteredCatCampsite == 2.0) then return end

	--Variables.
	local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	
	-- |[Movement]|
	--Move the party to the east.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Mei")
		ActorEvent_SetProperty("Move To", (19.25 * gciSizePerTile), (19.50 * gciSizePerTile))
	DL_PopActiveObject()
	if(bIsFlorentinaPresent) then
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Florentina")
			ActorEvent_SetProperty("Move To", (19.25 * gciSizePerTile), (20.50 * gciSizePerTile))
		DL_PopActiveObject()
	end
	fnCutsceneBlocker()
	
	--Wait a bit.
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	--Cats turn to face the party.
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "WerecatA")
		ActorEvent_SetProperty("Face", -1, 0)
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "WerecatB")
		ActorEvent_SetProperty("Face", -1, 0)
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "WerecatC")
		ActorEvent_SetProperty("Face", -1, 0)
	DL_PopActiveObject()
	
	--Wait a bit.
	fnCutsceneWait(25)
	fnCutsceneBlocker()
	
	-- |[Dialogue]|
	--If this is 0.0, then Mei hasn't seen these cats before:
	if(iEncounteredCatCampsite == 0.0) then
		
		--Set variable:
		VM_SetVar("Root/Variables/Chapter1/Scenes/iEncounteredCatCampsite", "N", 1.0)
		
		--Setup.
		fnStandardMajorDialogue()
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Werecat", "Neutral") ]])

		--Mei is a human:
		if(sMeiForm == "Human") then
			fnCutscene([[ Append("Werecat: Purrrr,[P] this human walks with confidence...[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] You wouldn't happen to be friendly cats, would you?[B][C]") ]])
			fnCutscene([[ Append("Werecat: Ha ha![P] The human knows nothing![P] Stupid![B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Offended] A no would suffice...[B][C]") ]])
			
			--Mei is alone:
			if(bIsFlorentinaPresent == false) then
				fnCutscene([[ Append("Werecat: Have you the strength to challenge us, weak human?[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Offended] I've half a mind to teach you some manners![B][C]") ]])
				fnCutscene([[ Append("Werecat: A fight![P] That is more like it![P] Show us your fury!") ]])
			
			--Florentina is present:
			else
				fnCutscene([[ Append("Florentina:[E|Happy] Shall we knock some manners into them?[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Happy] I'd be delighted![B][C]") ]])
				fnCutscene([[ Append("Werecat: A fight![P] That is more like it![P] Show us your fury!") ]])
			end
		
		--Mei is a werecat:
		elseif(sMeiForm == "Werecat") then
			fnCutscene([[ Append("Werecat: Ah, this kinfang wishes to join our pride?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] *sniff*[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Smirk] I smell weakness.[B][C]") ]])
			fnCutscene([[ Append("Werecat: Pah, you smell your own stink![B][C]") ]])
		
			--Mei is alone:
			if(bIsFlorentinaPresent == false) then
				fnCutscene([[ Append("Mei:[E|Offended] Shall we see who is stronger?[P] Come, face me, kinfang![P] Prove yourself!") ]])
			
			--Florentina is present:
			else
				fnCutscene([[ Append("Mei:[E|Offended] Shall we see who is stronger?[P] Come, face me, kinfang![P] Prove yourself![B][C]") ]])
				fnCutscene([[ Append("Florentina:[E|Happy] I wonder exactly how many ways there are to skin a cat...[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Happy] Let's find out!") ]])
			end
			
		--Mei is something else:
		else
			fnCutscene([[ Append("Werecat: Purrrr...[P] wait...[P] you smell like a human, but are not?[B][C]") ]])
			fnCutscene([[ Append("Werecat: A disguise, perhaps?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Err...[P] you wouldn't happen to be friendly cats, would you?[B][C]") ]])
			fnCutscene([[ Append("Werecat: Ha ha![P] The hidden human knows nothing![P] Stupid![B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Offended] A no would suffice...[B][C]") ]])
			
			--Mei is alone:
			if(bIsFlorentinaPresent == false) then
				fnCutscene([[ Append("Werecat: Have you the strength to challenge us, weak hidden human?[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Offended] I've half a mind to teach you some manners![B][C]") ]])
				fnCutscene([[ Append("Werecat: A fight![P] That is more like it![P] Show us your fury!") ]])
			
			--Florentina is present:
			else
				fnCutscene([[ Append("Florentina:[E|Happy] Shall we knock some manners into them?[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Happy] I'd be delighted![B][C]") ]])
				fnCutscene([[ Append("Werecat: A fight![P] That is more like it![P] Show us your fury!") ]])
			end
		end
		fnCutsceneBlocker()
		
		--Wait a bit.
		fnCutsceneWait(15)
		fnCutsceneBlocker()
		
		--Start a battle.
        fnCutscene([[ AdvCombat_SetProperty("World Pulse", true) ]])
		fnCutscene([[ AdvCombat_SetProperty("Reinitialize") ]])
		fnCutscene([[ AdvCombat_SetProperty("Activate") ]])
        fnCutscene([[ AdvCombat_SetProperty("Unretreatable", true) ]])
		fnCutscene([[ AdvCombat_SetProperty("Victory Script", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraScenes/Scene_DefeatCampCats.lua") ]])
		fnCutscene([[ AdvCombat_SetProperty("Defeat Script", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraDefeat/Scene_Begin.lua") ]])
        fnCutscene([[ LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/003 Evermoon North/Werecat Hunter.lua", 0) ]])
        fnCutscene([[ LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/003 Evermoon North/Werecat Hunter.lua", 0) ]])
        fnCutscene([[ LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/003 Evermoon North/Werecat Hunter.lua", 0) ]])
		fnCutsceneBlocker()
		
	--If this is 1.0, Mei fought them and lost.
	elseif(iEncounteredCatCampsite == 1.0) then
		
		--Setup.
		fnStandardMajorDialogue()
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Werecat", "Neutral") ]])
		
		fnCutscene([[ Append("Werecat: Ah![P] You challenge us again![P] We shall become strong together![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] You got lucky last time.[P] This time will be different![B][C]") ]])
		fnCutscene([[ Append("Werecat: We shall sharpen each other's claws![P] Come![P] To battle!") ]])
		fnCutsceneBlocker()
		
		--Wait a bit.
		fnCutsceneWait(15)
		fnCutsceneBlocker()
		
		--Start a battle.
        fnCutscene([[ AdvCombat_SetProperty("World Pulse", true) ]])
		fnCutscene([[ AdvCombat_SetProperty("Reinitialize") ]])
		fnCutscene([[ AdvCombat_SetProperty("Activate") ]])
		fnCutscene([[ AdvCombat_SetProperty("Victory Script", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraScenes/Scene_DefeatCampCats.lua") ]])
		fnCutscene([[ AdvCombat_SetProperty("Defeat Script", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraDefeat/Scene_Begin.lua") ]])
        fnCutscene([[ LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/003 Evermoon North/Werecat Hunter.lua", 0) ]])
        fnCutscene([[ LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/003 Evermoon North/Werecat Hunter.lua", 0) ]])
        fnCutscene([[ LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/003 Evermoon North/Werecat Hunter.lua", 0) ]])
		fnCutsceneBlocker()

    end

-- |[ ====================================== Want To Quit? ===================================== ]|
elseif(sObjectName == "GoBack") then

    --Does nothing if the Cassandra events are not active.
	local iStartedCassandraEvent = VM_GetVar("Root/Variables/Chapter1/Scenes/iStartedCassandraEvent", "N")
    if(iStartedCassandraEvent ~= 1.0) then return end
    
	--Variables.
	local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
    
    --Florentina is not here.
    if(bIsFlorentinaPresent == false) then
		fnStandardMajorDialogue()
        fnCutscene([[ Append("Mei:[E|Neutral] (I can't go back without at least trying to find that girl...)[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Sad] (But I have my own problems to worry about.[P] I can't save every random person I meet.[P] Maybe I should just let her go...)[BLOCK]") ]])

    --Florentina is here:
    else
		fnStandardMajorDialogue()
        fnCutscene([[ Append("Florentina:[E|Neutral] Had a change of heart, Mei?[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Sad] We should try to save her, but we can't save every random person in the world by ourselves.[P] Maybe we should just let it go...[BLOCK]") ]])
    end

    --Decision script is this script. It must be surrounded by quotes.
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Save Her\", " .. sDecisionScript .. ", \"Save\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Leave Her\",  " .. sDecisionScript .. ", \"Leave\") ")
    fnCutsceneBlocker()
    
-- |[ ==================================== I Will Save You ===================================== ]|
elseif(sObjectName == "Save") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
    
	--Variables.
	local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
    
    --Florentina is not here.
    if(bIsFlorentinaPresent == false) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
        fnCutscene([[ Append("Mei:[E|Offended] (No way![P] Whoever you are, hold on![P] Mei is coming!)") ]])
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneMove("Mei", 10.25, 19.50)

    --Florentina is here:
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
        fnCutscene([[ Append("Mei:[E|Offended] No way![P] Come on Florentina, let's go rough up some cats and save someone![B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Happy] Well when you put it that way...") ]])
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneMove("Mei", 10.25, 19.50)
        fnCutsceneMove("Florentina", 10.25, 19.50)
        fnCutsceneBlocker()
        fnAutoFoldParty()
        fnCutsceneBlocker()
    end
    
-- |[ ===================================== Quit This Place ==================================== ]|
elseif(sObjectName == "Leave") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
    
    --Deactivate the music.
    fnCutscene([[ AL_SetProperty("Music", "Null") ]])
    
	--Variables.
	local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
    
    --Flags.
    VM_SetVar("Root/Variables/Chapter1/Scenes/iStartedCassandraEvent", "N", 2.0)
    VM_SetVar("Root/Variables/Chapter1/Scenes/iCassandraEncounters", "N", 100)
    VM_SetVar("Root/Variables/Chapter1/Scenes/iCassandraTooLate", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter1/Scenes/iCassandraWayTooLate", "N", 1.0)
    
    --Florentina is not here.
    if(bIsFlorentinaPresent == false) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] (It can't be helped.[P] I'm sure she'll enjoy her life as a werecat.[P] I need to worry about getting back home...)") ]])
        fnCutsceneBlocker()

    --Florentina is here:
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
        fnCutscene([[ Append("Mei:[E|Sad] Sorry for getting worked up, Florentina, but we'll have to just leave her.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] Not a problem.[P] In fact, it's very mature to admit you can't do everything by yourself.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] The werecats won't kill her, they'll just turn her into a werecat.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Confused] Becoming a monstergirl is...[P] maybe not the best, but certainly not the worst.[P] The world will get on fine without us.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] Thanks, Florentina.[P] Let's get going.") ]])
        fnCutsceneBlocker()
    end
    
    --Forest music.
    fnCutscene([[ AL_SetProperty("Music", "ForestTheme") ]])

-- |[ ============================== Nadia News Delivery: Mycela =============================== ]|
--Nadia delivering news after the player warps.
elseif(sObjectName == "NadiaNews") then

    -- |[Activation Check]|
    --To activate this scene, the player must complete the Mycela quest in St. Fora's, then warp three or more
    -- times and then touch one of these triggers. Talking to Mycela at Breanne's disables the scene.
    local iWarpsSinceMycela  = VM_GetVar("Root/Variables/Chapter1/Mycela/iWarpsSinceMycela", "N")
    local iNadiaToldOfMycela = VM_GetVar("Root/Variables/Chapter1/Mycela/iNadiaToldOfMycela", "N")
    if(iNadiaToldOfMycela == 1.0 or iWarpsSinceMycela < 3.0) then return end
    
    -- |[Flag]|
    VM_SetVar("Root/Variables/Chapter1/Mycela/iNadiaToldOfMycela", "N", 1.0)
    
    -- |[Spawn Nadia]|
    TA_Create("Nadia")
        TA_SetProperty("Position", -1, -1)
        TA_SetProperty("Facing", gci_Face_South)
        TA_SetProperty("Clipping Flag", false)
        fnSetCharacterGraphics("Root/Images/Sprites/Nadia/", false)
    DL_PopActiveObject()

    -- |[Initial Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Voice:[VOICE|Nadia] Hey![P] Mei![P] Hold up a minute!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    -- |[Movement]|
    fnCutsceneTeleport("Nadia", 12.25, 44.50)
    fnCutsceneBlocker()
    fnCutsceneMove("Nadia", 12.25, 40.50)
    fnCutsceneMove("Mei", 11.75, 39.50)
    fnCutsceneFace("Mei", 0, 1)
    fnCutsceneMove("Florentina", 12.75, 39.50)
    fnCutsceneFace("Florentina", 0, 1)
    
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    -- |[Call Scene]|
    --This handles the dialogue.
    LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/Nadia_InformOfMycela/Scene_Begin.lua")

    -- |[Nadia's Exit]|
    fnCutsceneMove("Nadia", 12.25, 44.50)
    fnCutsceneTeleport("Nadia", -1.25, -1.50)
    
    -- |[Finish Up]|
    fnAutoFoldParty()
    fnCutsceneBlocker()
end
