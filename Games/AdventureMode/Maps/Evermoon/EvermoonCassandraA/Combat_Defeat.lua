-- |[Combat Defeat]|
--After the battle is over, and the party was defeated, play this scene.

--Variables.
local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")

--Wait a bit.
fnCutsceneWait(25)
fnCutsceneBlocker()

--Party takes the crouch pose.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Special Frame", "Crouch")
DL_PopActiveObject()
if(bIsFlorentinaPresent) then
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Special Frame", "Crouch")
	DL_PopActiveObject()
end
fnCutsceneWait(25)
fnCutsceneBlocker()

--Party takes the wounded pose.
fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Special Frame", "Wounded")
DL_PopActiveObject()
if(bIsFlorentinaPresent) then
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "Florentina")
		ActorEvent_SetProperty("Special Frame", "Wounded")
	DL_PopActiveObject()
end
fnCutsceneWait(25)
fnCutsceneBlocker()

--Fade to black.
fnCutscene([[ AL_SetProperty("Activate Fade", 60, gci_Fade_Over_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])

--Wait a bit.
fnCutsceneWait(120)
fnCutsceneBlocker()

--Transition to a new map:
fnCutscene([[ AL_BeginTransitionTo("EvermoonNE", gsRoot .. "Chapter 1/Scenes/EvermoonCassandraDefeat/Scene_PostTransition.lua") ]])
fnCutsceneBlocker()
