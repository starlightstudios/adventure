-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Examination]|
--Sign near Trannadar.
if(sObjectName == "Sign West") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Leader] (\"West:: Trannadar Trading Post.[P] Come on in!\"[P] -Nadia.)") ]])
	fnCutsceneBlocker()
	
	VM_SetVar("Root/Variables/Chapter1/Scenes/iHasReadNadiaSign", "N", 1.0)
	WD_SetProperty("Unlock Topic", "Signs", 1)
    
--Sign near the Alraune Coven
elseif(sObjectName == "Sign East") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought:[VOICE|Leader] (\"Dear Nadia::[P] Please stop telling such rotten jokes to the little ones.[P] They're becoming unbearable.\"[P] -Rochea.)") ]])
	fnCutsceneBlocker()
	
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
