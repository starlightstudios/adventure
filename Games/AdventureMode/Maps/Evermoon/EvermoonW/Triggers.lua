-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ==================================== Warp Activation ===================================== ]|
if(sObjectName == "WarpActivate") then
    fnWarpUnlockHandler("Root/Variables/Chapter1/Campfires/iEvermoonW", 14.25, 30.50)
    
-- |[ ================================= Post-Alraune TF Scene ================================== ]|
--Mei must be in Alraune form for this to work.
elseif(sObjectName == "SetRetreatToStairs") then
    
	--Variables.
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	local iMeiVolunteeredToAlraune = VM_GetVar("Root/Variables/Chapter1/Scenes/iMeiVolunteeredToAlraune", "N")
	
	--If this flag is set, trigger the Florentina cutscene.
	local iAlrauneMeetFlorentina = VM_GetVar("Root/Variables/Chapter1/Scenes/iAlrauneMeetFlorentina", "N")
	if(iAlrauneMeetFlorentina == 1.0) then
		
		-- |[Flag]|
		VM_SetVar("Root/Variables/Chapter1/Scenes/iAlrauneMeetFlorentina", "N", 0.0)
	
		-- |[Movement]|
		--Wait.
		fnCutsceneWait(120)
		fnCutsceneBlocker()
	
		--Florentina looks around.
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Florentina")
			ActorEvent_SetProperty("Face", -1.0, 1.0)
		DL_PopActiveObject()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Florentina")
			ActorEvent_SetProperty("Face", 0.0, 1.0)
		DL_PopActiveObject()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Florentina")
			ActorEvent_SetProperty("Face", 1.0, 1.0)
		DL_PopActiveObject()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Florentina")
			ActorEvent_SetProperty("Face", 1.0, 0.0)
		DL_PopActiveObject()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Florentina moves to the door.
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Florentina")
			ActorEvent_SetProperty("Move To", (49.25 * gciSizePerTile), (38.50 * gciSizePerTile))
		DL_PopActiveObject()
		fnCutsceneBlocker()
		fnCutsceneWait(5)
		fnCutsceneBlocker()
		
		--Face north to open the door.
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Florentina")
			ActorEvent_SetProperty("Face", 0.0, -1.0)
		DL_PopActiveObject()
		fnCutsceneWait(15)
		fnCutsceneBlocker()
		
		--Open the door.
		fnCutscene([[AL_SetProperty("Open Door", "CabinDoor")]])
		fnCutscene([[AudioManager_PlaySound("World|RemoteDoor")]])
		fnCutsceneBlocker()
		
		--Wait a bit.
		fnCutsceneWait(30)
		fnCutsceneBlocker()
		
		--Mei walks to greet her.
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Move To", (49.25 * gciSizePerTile), (36.50 * gciSizePerTile))
		DL_PopActiveObject()
		fnCutsceneBlocker()
		
		--Wait a bit.
		fnCutsceneWait(30)
		fnCutsceneBlocker()
	
		-- |[Dialogue]|
		--Setup.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
		
		--If Mei did not volunteer:
		if(iMeiVolunteeredToAlraune == 0.0) then
			fnCutscene([[ Append("Florentina:[E|Confused] So.[P] Are they done with you, then?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Florentina...[P] This is...[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Neutral] Hrmph.[P] They wouldn't even let me be there.[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Confused] I tried to stop them...[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] No need to apologize.[P] I feel wonderful, leaf-sister.[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Confused] Don't start with that leaf-sister crap, okay?[P] Bad enough they used force like that, worse that you'd enjoy it.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] I'm not angry.[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Offended] No?[P] Beaten up, dragged off, and turned into a plant against your will, and you're not angry?[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Offended] Then allow me to be angry on your behalf, yeah?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Surprise] Did I do something wrong?[P] I'm sorry...[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Confused] You just don't understand.[P] Can we just go?[P] Get out of here, away from here and all these smiling freaks...[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] All right, all right.[P] Focus on the task at hand, right?[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Neutral] Yeah.[P] That's it.[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Facepalm] ...[P] Just standing here is bringing back bad memories...") ]])
		
		--If Mei volunteered:
		else
			fnCutscene([[ Append("Florentina:[E|Confused] So.[P] Are they done with you, then?[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] Florentina...[P] This is...[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Confused] Hrmph.[P] They wouldn't even let me be there.[P] I should have...[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Neutral] I wish you could have been there, leaf-sister.[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Offended] Knock that off.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Sad] Don't call you - [P][CLEAR]") ]])
			fnCutscene([[ Append("Florentina:[E|Confused] No.[P] I'm not like them.[P] I don't want to be like them.[B][C]") ]])
			fnCutscene([[ Append("Mei:[E|Sad] I...[P] I'm sorry I put you through this.[B][C]") ]])
			fnCutscene([[ Append("Florentina:[E|Neutral] It's nothing.[P] If you're happy, we can get going.") ]])
			
		end
		fnCutsceneBlocker()
		
		-- |[Movement]|
		--Move Mei onto Florentina.
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Mei")
			ActorEvent_SetProperty("Move To", (49.25 * gciSizePerTile), (38.50 * gciSizePerTile))
		DL_PopActiveObject()
		fnCutsceneBlocker()
		
		--Fold the party up.
		fnCutscene([[ AL_SetProperty("Fold Party") ]])
		fnCutsceneBlocker()
	
		-- |[System]|
		--These lines add Florentina to the party. They execute as soon as the scene starts, but take effect afterwards.
		fnAddPartyMember("Florentina")
	
	--If the flag is 100, then teleport Florentina to Mei and have her join the party.
	elseif(iAlrauneMeetFlorentina == 100.0 and giFollowersTotal < 1) then
	
		-- |[Movement]|
		--Move Mei onto Florentina.
		Cutscene_CreateEvent("ActorEvent", "Actor")
			ActorEvent_SetProperty("Subject Name", "Florentina")
			ActorEvent_SetProperty("Teleport To", (50.25 * gciSizePerTile), (33.50 * gciSizePerTile))
		DL_PopActiveObject()
		fnCutsceneBlocker()
		
		--Fold the party up.
		fnCutscene([[ AL_SetProperty("Fold Party") ]])
		fnCutsceneBlocker()
	
		-- |[System]|
		--These lines add Florentina to the party. They execute as soon as the scene starts, but take effect afterwards.
		fnAddPartyMember("Florentina")
        VM_SetVar("Root/Variables/Chapter1/Scenes/iAlrauneMeetFlorentina", "N", 0.0)
	
    end

-- |[ ===================================== Stairs Removal ===================================== ]|
--Remove the stairs at the secret alraune HQ if Mei is not an alraune:
elseif(sObjectName == "FormRemoveStairs") then

    --Variables.
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
    
    --Remove stairs.
	if(sMeiForm ~= "Alraune") then
		AL_RemoveObject("Exit", "AlrauneStairs")
    
    --Spawn the stairs.
    else
        AL_SetProperty("Create Staircase", "AlrauneStairs", 800, 528, 16, 16, "DR", "ExitStairs", "AlrauneChamber")
	end
    
-- |[ ============================== Nadia News Delivery: Mycela =============================== ]|
--Nadia delivering news after the player warps.
elseif(sObjectName == "NadiaNewsW" or sObjectName == "NadiaNewsS" or sObjectName == "NadiaNewsE") then

    -- |[Activation Check]|
    --To activate this scene, the player must complete the Mycela quest in St. Fora's, then warp three or more
    -- times and then touch one of these triggers. Talking to Mycela at Breanne's disables the scene.
    local iWarpsSinceMycela  = VM_GetVar("Root/Variables/Chapter1/Mycela/iWarpsSinceMycela", "N")
    local iNadiaToldOfMycela = VM_GetVar("Root/Variables/Chapter1/Mycela/iNadiaToldOfMycela", "N")
    if(iNadiaToldOfMycela == 1.0 or iWarpsSinceMycela < 3.0) then return end
    
    -- |[Flag]|
    VM_SetVar("Root/Variables/Chapter1/Mycela/iNadiaToldOfMycela", "N", 1.0)
    
    -- |[Spawn Nadia]|
    TA_Create("Nadia")
        TA_SetProperty("Position", -1, -1)
        TA_SetProperty("Facing", gci_Face_South)
        TA_SetProperty("Clipping Flag", false)
        fnSetCharacterGraphics("Root/Images/Sprites/Nadia/", false)
    DL_PopActiveObject()

    -- |[Initial Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Voice:[VOICE|Nadia] Hey![P] Mei![P] Hold up a minute!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    -- |[Movement]|
    --Varies based on which trigger is in use.
    if(sObjectName == "NadiaNewsW" or sObjectName == "NadiaNewsS") then
        fnCutsceneTeleport("Nadia", 0.25, 33.50)
        fnCutsceneBlocker()
        fnCutsceneMove("Nadia", 9.25, 33.50)
        fnCutsceneMove("Mei", 10.25, 33.00)
        fnCutsceneFace("Mei", -1, 0)
        fnCutsceneMove("Florentina", 10.25, 34.00)
        fnCutsceneFace("Florentina", -1, 0)
    
    elseif(sObjectName == "NadiaNewsE") then
        fnCutsceneTeleport("Nadia", 16.25, 47.50)
        fnCutsceneBlocker()
        fnCutsceneMove("Nadia", 16.25, 31.50)
        fnCutsceneMove("Nadia", 16.75, 31.50)
        fnCutsceneFace("Nadia", 0, -1)
        fnCutsceneMove("Mei", 16.25, 30.50)
        fnCutsceneFace("Mei", 0, 1)
        fnCutsceneMove("Florentina", 17.25, 30.50)
        fnCutsceneFace("Florentina", 0, 1)
    end
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    -- |[Call Scene]|
    --This handles the dialogue.
    LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/Nadia_InformOfMycela/Scene_Begin.lua")

    -- |[Nadia's Exit]|
    if(sObjectName == "NadiaNewsW" or sObjectName == "NadiaNewsS") then
        fnCutsceneMove("Nadia", 0.25, 33.50)
        fnCutsceneTeleport("Nadia", -1.25, -1.50)
    
    elseif(sObjectName == "NadiaNewsE") then
        fnCutsceneMove("Nadia", 16.25, 31.50)
        fnCutsceneMove("Nadia", 16.25, 47.50)
        fnCutsceneTeleport("Nadia", -1.25, -1.50)
    end
    
    -- |[Finish Up]|
    fnAutoFoldParty()
    fnCutsceneBlocker()
end
