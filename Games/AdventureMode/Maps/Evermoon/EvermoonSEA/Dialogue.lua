-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)


-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[Name Resolve]|
    --Resolve the name of the calling entity.
    local sActorName = TA_GetProperty("Name")

	--Set facing.
	TA_SetProperty("Face Character", "PlayerEntity")
	
    -- |[Alicia]|
    if(sActorName == "Alicia") then
	
        --Variables.
        local iMetAlicia = VM_GetVar("Root/Variables/Chapter1/Scenes/iMetAlicia", "N")
        local iHasSeenTrannadarFlorentinaScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFlorentinaScene", "N")
        
        --First meeting.
        if(iMetAlicia == 0.0) then
            
            --Dialogue.
            fnStandardMajorDialogue()
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Alicia", "Neutral") ]])
            VM_SetVar("Root/Variables/Chapter1/Scenes/iMetAlicia", "N", 1.0)
            
            -- |[Achievement]|
            AM_SetPropertyJournal("Unlock Achievement", "FindGemcutter")
            
            --Florentina is not here:
            if(iHasSeenTrannadarFlorentinaScene == 0.0) then
                fnCutscene([[ Append("Lady:[E|Neutral] Ah, a customer![P] I hope you've got a good story, as I'm not serving boring clients.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] You're running a shop?[P] Way out here?[B][C]") ]])
                fnCutscene([[ Append("Lady:[E|Neutral] Huh.[P] You haven't heard of me?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] No.[B][C]") ]])
                fnCutscene([[ Append("Alicia:[E|Neutral] Alicia of Jeffespeir?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Nope.[B][C]") ]])
                fnCutscene([[ Append("Alicia:[E|Neutral] World-famous gemcutter?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Nothing.[B][C]") ]])
                fnCutscene([[ Append("Alicia:[E|Neutral] The recluse who'd turn down a duchess for being tawdry?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Sorry.[B][C]") ]])
                fnCutscene([[ Append("Alicia:[E|Neutral] Well that is a special kind of ignorance.[P] Where are you from that you've not heard of Alicia Neuroth?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Hong Kong.[P] Hey, you wouldn't know how to get there, would you?[P] I'm a little lost.[B][C]") ]])
                fnCutscene([[ Append("Alicia:[E|Neutral] ...[P] I have traversed every corner of this world and never heard that name before.[B][C]") ]])
                fnCutscene([[ Append("Alicia:[E|Neutral] ...[P] And thus I am intrigued.[P] So, I may decide to cut gems for you after all.[P] Well played.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Laugh] Erm, why would I need gems cut again?[B][C]") ]])
                fnCutscene([[ Append("Alicia:[E|Neutral] Just -[P] fascinating![B][C]") ]])
                fnCutscene([[ Append("Alicia:[E|Neutral] Your sword, have you noticed the little notch there on the grip?[P] You can insert gems into those.[B][C]") ]])
                fnCutscene([[ Append("Alicia:[E|Neutral] It's not just for appearance's sake, mind.[P] The magic instilled into the gem flows around the sword and enhances it.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Smirk] Oh so [P]*that's*[P] what those are for.[P] I've been kind of winging it since I got here.[B][C]") ]])
                fnCutscene([[ Append("Alicia:[E|Neutral] Well wing it no more, my friend, for you have the finest gem cutter in the lands at your service.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Smirk] Okay, so how do I get a gem cut?[B][C]") ]])
                fnCutscene([[ Append("Alicia:[E|Neutral] I have plenty of raw gems,[P] for a reasonable price, of course.[B][C]") ]])
                fnCutscene([[ Append("Alicia:[E|Neutral] But the cutting process requires Adamantite.[P] With it, I can reshape a gem, and fit it into another.[B][C]") ]])
                fnCutscene([[ Append("Alicia:[E|Neutral] A gem placed inside another has the properties of both, but there is a catch.[P] No amount of adamantite can merge two gems that share a color.[B][C]") ]])
                fnCutscene([[ Append("Alicia:[E|Neutral] And when two gems merge, they merge colors as well.[P] So, it's my job to make your weapons more powerful by making your gems more powerful.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Happy] Oh, excellent![B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Smirk] So, you live out here all by yourself and you let customers come to you.[P] I get it now.[B][C]") ]])
                fnCutscene([[ Append("Alicia:[E|Neutral] By myself, no.[P] My friend, I live here with my wife, Ginny.[B][C]") ]])
                fnCutscene([[ Append("Alicia:[E|Neutral] She's at the lake south of here fishing.[P] You should go say hello![B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Okey dokey.[P] Thanks for the advice, Ms. Alicia.") ]])
                fnCutsceneBlocker()
            
            --Florentina is here:
            else
                fnCutscene([[ Append("Lady:[E|Neutral] Florentina, back at it again, are you?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Do you know this lady, Florentina?[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Confused] Oh yeah.[P] This is Neuroth the famous gemcutter.[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Confused] Famous for being a real headache, that is.[B][C]") ]])
                fnCutscene([[ Append("Alicia:[E|Neutral] My first name is Alicia.[P] You'd do well to learn it, glumprongbutt.[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Offended] Okay, that's it.[P] You wanna fight?[P] Let's go![B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Surprise] Florentina, no![P] Stop![B][C]") ]])
                fnCutscene([[ Append("Alicia:[E|Neutral] Any time, any place, mulchbreath![B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Angry] Both of you, grow up![B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Offended] So it's clear there's some bad blood here.[P] What did you do to get each other so angry?[B][C]") ]])
                fnCutscene([[ Append("Alicia:[E|Neutral] I paid Florentina here fifty platina for a batch of fresh eggs, and when I got them, they were rotten![B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Confused] You paid for them on monday and picked them up two sundays later.[P] It's not my fault they went bad![B][C]") ]])
                fnCutscene([[ Append("Alicia:[E|Neutral] Give[P] me[P] my[P] EGGS![B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Offended] Make me![P][SOUND|World|Thump][B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Angry] Stop it![B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Offended] Miss Alicia, I am terribly sorry about the eggs.[P] I'll cover the difference.[B][C]") ]])
                fnCutscene([[ Append("Alicia:[E|Neutral] ...[P] You will?[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Surprise] You -[P] will?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Offended] Honestly, throwing a punch over a bunch of eggs![P] Are we children or adults?[B][C]") ]])
                fnCutscene([[ Append("Alicia:[E|Neutral] ...[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Happy] ...[P] Children![P][SOUND|World|Thump][B][C]") ]])
                fnCutscene([[ Append("Alicia:[E|Neutral] Owch![B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Angry] Florentina![P][SOUND|World|Thump][B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Surprise] Booogh!![B][C]") ]])
                fnCutscene([[ Append("Alicia:[E|Neutral] ...[B][C]") ]])
                fnCutscene([[ Append("Alicia:[E|Neutral] Well, that was just a superb hit![P] Where'd you learn to punch like that?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Uh, I didn't.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Smirk] ...[P] Sorry, Florentina.[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Facepalm] Puh...[P] Didn't hurt...[P] Ooh oww...[B][C]") ]])
                fnCutscene([[ Append("Alicia:[E|Neutral] What's your name, madam?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Smirk] Mei.[P] I hope you can forgive the violence.[B][C]") ]])
                fnCutscene([[ Append("Alicia:[E|Neutral] ...[P] Well, as you've heard from your blue friend, I am Alicia Neuroth, gemcutter.[B][C]") ]])
                fnCutscene([[ Append("Alicia:[E|Neutral] I do business with who I care to, and people seek me out from across the land.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Oh, I see.[P] So this is your workshop?[B][C]") ]])
                fnCutscene([[ Append("Alicia:[E|Neutral] Quite![P] I live here with my wife, Ginny.[P] She's out fishing right now.[B][C]") ]])
                fnCutscene([[ Append("Alicia:[E|Neutral] Now, I happen to have taken a liking to you, so.[P] Gems.[P] Would you like one cut?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] And why would I need a gem cut again?[P] Sorry if I'm ignorant.[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Facepalm] Ugh...[P] right in the stomach...[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Surprise] Oh, I really got you didn't I?[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Facepalm] Thatdidn'thurtshutup...[B][C]") ]])
                fnCutscene([[ Append("Alicia:[E|Neutral] Gems can be socketed into equipment.[P] That's what those little slots on your sword's hilt are for.[B][C]") ]])
                fnCutscene([[ Append("Alicia:[E|Neutral] The magic of the gem flows into the weapon, enhancing it.[B][C]") ]])
                fnCutscene([[ Append("Alicia:[E|Neutral] Attach too many gems to a weapon and they interfere with each other.[P] Don't socket more into a weapon than it can handle.[B][C]") ]])
                fnCutscene([[ Append("Alicia:[E|Neutral] As you might imagine, a finely-cut gem is therefore worth a great deal.[P] Quantity is nothing, quality is everything.[B][C]") ]])
                fnCutscene([[ Append("Alicia:[E|Neutral] I have some simple gems here for sale, but what I really need is Adamantite.[P] With that I can draw out the true power of the gemstone.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Smirk] So, I get you the Adamantite, and a gem, and you can make it a better gem?[B][C]") ]])
                fnCutscene([[ Append("Alicia:[E|Neutral] Sort of -[P] you bring me *two* gems, and I can make *one* better gem, by merging them together.[B][C]") ]])
                fnCutscene([[ Append("Alicia:[E|Neutral] A merged gem has the properties of both, and the colors. No amount of adamantite can merge two gems of the same color.[B][C]") ]])
                fnCutscene([[ Append("Alicia:[E|Neutral] You've only got so many sockets, so you can see how a gemcutter like me can give you a crucial edge.[B][C]") ]])
                fnCutscene([[ Append("Alicia:[E|Neutral] Of course, if you're a head for battle, then you'll spare no expense.[P] I imagine you know, by now, how slim the margin between life and death can be.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Happy] Thank you very much, Miss Neuroth![B][C]") ]])
                fnCutscene([[ Append("Alicia:[E|Neutral] Any time.[P] And, Florentina?[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Confused] What?[B][C]") ]])
                fnCutscene([[ Append("Alicia:[E|Neutral] Try not to rub off on this very nice girl.[P] She's sweet.[P] Keep it that way.[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Confused] Yeah, real sweet.[P] And then she sucker punches you...") ]])
            
            end
            
        --Successive meetings.
        else
            fnStandardMajorDialogue()
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Alicia", "Neutral") ]])
            fnCutscene([[ Append("Alicia:[E|Neutral] Hello![P] What can I do for you?[BLOCK]") ]])

            --Decision script is this script. It must be surrounded by quotes.
            local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
            fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Can you cut some gems?\",  " .. sDecisionScript .. ", \"Gemcutting\") ")
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"How does gemcutting work?\", " .. sDecisionScript .. ", \"Instructions\") ")
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"Just saying hi.\", " .. sDecisionScript .. ", \"Hi\") ")
            fnCutsceneBlocker()
        
        end
    end

--Opens the gemcutting interface.
elseif(sTopicName == "Gemcutting") then

	--Dialogue.
	WD_SetProperty("Hide")
	fnStandardMajorDialogue(true)
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Alicia", "Neutral") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Could I bother you for some gem cutting?[B][C]") ]])
	fnCutscene([[ Append("Alicia:[E|Neutral] No bother at all, let's see what I can do with your gems!") ]])
	fnCutsceneBlocker()

	--Run the shop.
    local sPath = fnResolvePath () .. "Shop Setup.lua"
    local sInstruction = "AM_SetShopProperty(\"Show\", \"Alicia's Gem Shop\", \"" .. sPath .. "\", \"Null\")"
	fnCutscene(sInstruction)
	fnCutsceneBlocker()

--Description of how gemcutting works.
elseif(sTopicName == "Instructions") then
	
	--Dialogue.
	WD_SetProperty("Hide")
	fnStandardMajorDialogue(true)
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Alicia", "Neutral") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Not to be annoying, but can you give me some details on gemcutting?[B][C]") ]])
	fnCutscene([[ Append("Alicia:[E|Neutral] Well, let's see.[P] I suppose I can go over the basics for you.[B][C]") ]])
	fnCutscene([[ Append("Alicia:[E|Neutral] There are lots of gems out there with magic in them.[P] Yemite, Rubose, Glintsteel...[B][C]") ]])
	fnCutscene([[ Append("Alicia:[E|Neutral] If you can't find any, I sell some raw ones, too.[P] Sometimes Ginny finds them washed up on the lakeshore.[P] She really likes shiny things, don't get me started![B][C]") ]])
	fnCutscene([[ Append("Alicia:[E|Neutral] Every gem has a color, and some properties, like an increase in attack power.[B][C]") ]])
	fnCutscene([[ Append("Alicia:[E|Neutral] The color is important, as I cannot merge together two gems that share a color.[P] And when I merge gems, it gains the color of both.[B][C]") ]])
	fnCutscene([[ Append("Alicia:[E|Neutral] With each cut, the gem gains more properties, and more colors, and takes more pure adamantite to cut again.[B][C]") ]])
	fnCutscene([[ Append("Alicia:[E|Neutral] Since there's a limit to how many gems you can socket into a piece of equipment, the better the gems, the better the gear.[P] So, be on the lookout for rare and pure adamantite.[B][C]") ]])
	fnCutscene([[ Append("Alicia:[E|Neutral] Socketing powerful gems into a weapon can make even a rusty piece of junk into a deadly implement.[P] In the right hands, of course.[B][C]") ]])
	fnCutscene([[ Append("Alicia:[E|Neutral] Oh, and if you unequip something, the gems are unsocketed automatically.[P] So, you don't need to worry about accidentally selling something with gems in it![B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Happy] Great![P] Thanks, Alicia![P] I think I get it now.") ]])

--Effectively the same as goodbye.
elseif(sTopicName == "Hi") then
	
	--Variables.
	local iMetGinny       = VM_GetVar("Root/Variables/Chapter1/Scenes/iMetGinny", "N")
	local iHasWerecatForm = VM_GetVar("Root/Variables/Global/Mei/iHasWerecatForm", "N")
	
	--Dialogue.
	WD_SetProperty("Hide")
	fnStandardMajorDialogue(true)
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Alicia", "Neutral") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] I was just in the area and thought I'd say hello.[B][C]") ]])
	fnCutscene([[ Append("Alicia:[E|Neutral] It's nice to talk about something other than business sometimes.[P] Have you met Ginny yet?[B][C]") ]])
	
	--Switching dialogue.
	if(iMetGinny == 1.0) then
		if(iHasWerecatForm == 1.0) then
			fnCutscene([[ Append("Mei:[E|Happy] Oh yes, she's a very strong and pretty fang![P] I'm a little jealous![B][C]") ]])
			fnCutscene([[ Append("Alicia:[E|Neutral] You know, she sometimes calls me a fang by mistake.[P] And then she gets all nervous.[P] If you've never seen a werecat get flustered, it's quite a sight.[B][C]") ]])
			fnCutscene([[ Append("Alicia:[E|Neutral] But don't let me keep you, good luck out there!") ]])
		else
			fnCutscene([[ Append("Mei:[E|Happy] She's the very nice werecat at the lake?[B][C]") ]])
			fnCutscene([[ Append("Alicia:[E|Neutral] Indeed![P] She prefers fishing to the more conventional hunting of the other werecats, and I happen to love trout.[P] A match made in heaven![B][C]") ]])
			fnCutscene([[ Append("Alicia:[E|Neutral] Ah, but I've prattled long enough.[P] Good luck on your adventure.") ]])
		end
	else
		fnCutscene([[ Append("Mei:[E|Neutral] We're not acquainted, sorry.[B][C]") ]])
		fnCutscene([[ Append("Alicia:[E|Neutral] She spends most of her day fishing at the dock south of here, we even set up a hut for her.[B][C]") ]])
		fnCutscene([[ Append("Alicia:[E|Neutral] If you're going down that way, say hello to her.[P] Good luck, my friend!") ]])
	end
end
