-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ==================================== Overlays / Warps ==================================== ]|
if(sObjectName == "WarpActivate") then
    fnWarpUnlockHandler("Root/Variables/Chapter1/Campfires/iEvermoonSEA", 42.25, 31.50)
    
--Disables the forest overlay.
elseif(sObjectName == "NoOverlay") then
    AL_SetProperty("Foreground Alpha", 0, 0.00, 15)
    AL_SetProperty("Foreground Alpha", 1, 0.00, 15)
    AL_SetProperty("Foreground Alpha", 2, 0.00, 15)
	
--Disables the forest overlay instantly.
elseif(sObjectName == "NoOverlayFast") then
    AL_SetProperty("Foreground Alpha", 0, 0.00, 0)
    AL_SetProperty("Foreground Alpha", 1, 0.00, 0)
    AL_SetProperty("Foreground Alpha", 2, 0.00, 0)
	
--Enables the forest overlay.
elseif(sObjectName == "YesOverlay") then
    AL_SetProperty("Foreground Alpha", 0, 0.50, 15)
    AL_SetProperty("Foreground Alpha", 1, 0.50, 15)
    AL_SetProperty("Foreground Alpha", 2, 0.50, 15)

-- |[ ============================== Nadia News Delivery: Mycela =============================== ]|
--Nadia delivering news after the player warps.
elseif(sObjectName == "NadiaNewsN" or sObjectName == "NadiaNewsW") then

    -- |[Activation Check]|
    --To activate this scene, the player must complete the Mycela quest in St. Fora's, then warp three or more
    -- times and then touch one of these triggers. Talking to Mycela at Breanne's disables the scene.
    local iWarpsSinceMycela  = VM_GetVar("Root/Variables/Chapter1/Mycela/iWarpsSinceMycela", "N")
    local iNadiaToldOfMycela = VM_GetVar("Root/Variables/Chapter1/Mycela/iNadiaToldOfMycela", "N")
    if(iNadiaToldOfMycela == 1.0 or iWarpsSinceMycela < 3.0) then return end
    
    -- |[Flag]|
    VM_SetVar("Root/Variables/Chapter1/Mycela/iNadiaToldOfMycela", "N", 1.0)
    
    -- |[Spawn Nadia]|
    TA_Create("Nadia")
        TA_SetProperty("Position", -1, -1)
        TA_SetProperty("Facing", gci_Face_South)
        TA_SetProperty("Clipping Flag", false)
        fnSetCharacterGraphics("Root/Images/Sprites/Nadia/", false)
    DL_PopActiveObject()

    -- |[Initial Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Voice:[VOICE|Nadia] Hey![P] Mei![P] Hold up a minute!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    -- |[Movement]|
    if(sObjectName == "NadiaNewsN") then
        fnCutsceneTeleport("Nadia", 37.25, 10.50)
        fnCutsceneBlocker()
        fnCutsceneMove("Nadia", 37.25, 23.50)
        fnCutsceneMove("Mei", 36.25, 23.50)
        fnCutsceneFace("Mei", 0, -1)
        fnCutsceneMove("Florentina", 37.25, 23.50)
        fnCutsceneFace("Florentina", 0, -1)
    else
        fnCutsceneTeleport("Nadia", 7.25, 28.50)
        fnCutsceneBlocker()
        fnCutsceneMove("Nadia", 18.25, 28.50)
        fnCutsceneMove("Nadia", 18.25, 29.50)
        fnCutsceneMove("Nadia", 24.25, 29.50)
        fnCutsceneMove("Mei", 25.25, 29.50)
        fnCutsceneFace("Mei", -1, 0)
        fnCutsceneMove("Florentina", 25.25, 30.50)
        fnCutsceneFace("Florentina", -1, 0)
    end
    
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    -- |[Call Scene]|
    --This handles the dialogue.
    LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/Nadia_InformOfMycela/Scene_Begin.lua")

    -- |[Nadia's Exit]|
    if(sObjectName == "NadiaNewsN") then
        fnCutsceneMove("Nadia", 37.25, 10.50)
        fnCutsceneTeleport("Nadia", -1.25, -1.50)
    else
        fnCutsceneMove("Nadia", 18.25, 29.50)
        fnCutsceneMove("Nadia", 18.25, 28.50)
        fnCutsceneMove("Nadia",  8.25, 28.50)
        fnCutsceneTeleport("Nadia", -1.25, -1.50)
    end
    
    -- |[Finish Up]|
    fnAutoFoldParty()
    fnCutsceneBlocker()
end
