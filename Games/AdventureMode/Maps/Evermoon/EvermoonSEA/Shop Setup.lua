-- |[ =================================== Alicia's Shop Setup ================================== ]|
--Not fully set yet.

-- |[Alicia cuts gems!]|
AM_SetShopProperty("Allow Gemcutting")

-- |[Item Stock Listing]|
--Adamantite
AM_SetShopProperty("Add Item", "Adamantite Powder", -1, -1)

--Gems
AM_SetShopProperty("Add Item", "Yemite Gem",     -1, -1)
AM_SetShopProperty("Add Item", "Romite Gem",     -1, -1)
AM_SetShopProperty("Add Item", "Glintsteel Gem", -1, -1)
AM_SetShopProperty("Add Item", "Rubose Gem",     -1, -1)
AM_SetShopProperty("Add Item", "Mordreen Gem",   -1, -1)
