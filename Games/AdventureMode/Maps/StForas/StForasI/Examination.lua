-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= Exits ========================================== ]|
if(sObjectName == "ToStForasH") then
    
    --Variables.
    local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
    local iUnlockedStForasBack = VM_GetVar("Root/Variables/Chapter1/Sharelock/iUnlockedStForasBack", "N")
    
    --Locked.
    if(iUnlockedStForasBack == 0.0) then
        if(bIsFlorentinaPresent == false) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Leader] (The door won't budge.)") ]])
            fnCutsceneBlocker()
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Mei:[VOICE|Mei] The door won't budge.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[VOICE|Florentina] The handle isn't locked, it's barred from the other side.") ]])
            fnCutsceneBlocker()
        end
    
    --Unlocked.
    else
        AudioManager_PlaySound("World|FlipSwitch")
        AL_BeginTransitionTo("StForasH", "FORCEPOS:4.0x6.0x0")
    end
    
-- |[ ====================================== Examinables ======================================= ]|
elseif(sObjectName == "SampleExaminable") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Text here.)") ]])
    fnCutsceneBlocker()

-- |[ ========================================== Other ========================================= ]|
-- |[ ========================================== Debug ========================================= ]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
