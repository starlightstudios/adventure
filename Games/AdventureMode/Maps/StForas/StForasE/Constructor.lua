-- |[ ======================================= Constructor ====================================== ]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "StForasE"
local sLevelMusic = "Apprehension"
local sMapResolveName = "StForasE"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[ ======================================== Standard ======================================== ]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

    -- |[Construction]|
	--Music.
	AL_SetProperty("Music", sLevelMusic)
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
    
    -- |[Map Handling]|
    --Resolve the map's variable.
    fnStandardCh1Map(sLevelName)
	
	-- |[Spawn NPCS Here]|
	--NPCs set to spawn here will be available for post-exec cutscenes.

-- |[ ======================================== Post-Exec ======================================= ]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	-- |[Spawn NPCs Here]|
	--If an entity needs to spawn after a post-exec cutscene, put them here.
    
    -- |[Layer Handling]|
    local iStForaSwitchA = VM_GetVar("Root/Variables/Chapter1/Sharelock/iStForaSwitchA", "N")
    local iStForaSwitchB = VM_GetVar("Root/Variables/Chapter1/Sharelock/iStForaSwitchB", "N")
    local iStForaSwitchC = VM_GetVar("Root/Variables/Chapter1/Sharelock/iStForaSwitchC", "N")
    local iStForaSwitchD = VM_GetVar("Root/Variables/Chapter1/Sharelock/iStForaSwitchD", "N")
    
    --If all four switches are pressed, open the passage.
    if(iStForaSwitchA == 1.0 and iStForaSwitchB == 1.0 and iStForaSwitchC == 1.0 and iStForaSwitchD == 1.0) then
        AL_SetProperty("Set Collision", 62, 33, 0, 0)
        AL_SetProperty("Set Layer Disabled", "BarredDoorH", true)
        AL_SetProperty("Set Layer Disabled", "BarredDoorL", true)
    end

    --Each switch disables a bar.
    if(iStForaSwitchA == 1.0) then
        AL_SetProperty("Set Layer Disabled", "DoorBarLL", true)
        AL_SetProperty("Switch State", "SwitchA", true)
    end
    if(iStForaSwitchB == 1.0) then
        AL_SetProperty("Set Layer Disabled", "DoorBarLH", true)
        AL_SetProperty("Switch State", "SwitchB", true)
    end
    if(iStForaSwitchC == 1.0) then
        AL_SetProperty("Set Layer Disabled", "DoorBarHL", true)
        AL_SetProperty("Switch State", "SwitchC", true)
    end
    if(iStForaSwitchD == 1.0) then
        AL_SetProperty("Set Layer Disabled", "DoorBarHH", true)
        AL_SetProperty("Switch State", "SwitchD", true)
    end
end
