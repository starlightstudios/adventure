-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= Exits ========================================== ]|
if(sObjectName == "SampleExit") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("LevelName", "FORCEPOS:28.0x16.0x0")
    
-- |[ ====================================== Examinables ======================================= ]|
elseif(sObjectName == "SwitchA" or sObjectName == "SwitchB" or sObjectName == "SwitchC" or sObjectName == "SwitchD") then

    --Flag.
    AL_SetProperty("Switch Handled Update")

    --Variables.
    local iStForaSwitch = VM_GetVar("Root/Variables/Chapter1/Sharelock/iStFora"..sObjectName, "N")
    local bIsSwitchUp = AL_GetProperty("Switch State")
    
    --Switch not flipped up:
    if(iStForaSwitch == 0.0) then
        
        --Set variable and switch state.
        VM_SetVar("Root/Variables/Chapter1/Sharelock/iStFora"..sObjectName, "N", 1.0)
        AudioManager_PlaySound("World|FlipSwitch")
        AL_SetProperty("Switch State", sObjectName, true)
        
        --Toggle:
        if(sObjectName == "SwitchA") then
            AL_SetProperty("Set Layer Disabled", "DoorBarLL", true)
        elseif(sObjectName == "SwitchB") then
            AL_SetProperty("Set Layer Disabled", "DoorBarLH", true)
        elseif(sObjectName == "SwitchC") then
            AL_SetProperty("Set Layer Disabled", "DoorBarHL", true)
        elseif(sObjectName == "SwitchD") then
            AL_SetProperty("Set Layer Disabled", "DoorBarHH", true)
        end

        --Ending case:
        local iStForaSwitchA = VM_GetVar("Root/Variables/Chapter1/Sharelock/iStForaSwitchA", "N")
        local iStForaSwitchB = VM_GetVar("Root/Variables/Chapter1/Sharelock/iStForaSwitchB", "N")
        local iStForaSwitchC = VM_GetVar("Root/Variables/Chapter1/Sharelock/iStForaSwitchC", "N")
        local iStForaSwitchD = VM_GetVar("Root/Variables/Chapter1/Sharelock/iStForaSwitchD", "N")
        if(iStForaSwitchA == 1.0 and iStForaSwitchB == 1.0 and iStForaSwitchC == 1.0 and iStForaSwitchD == 1.0) then
            AL_SetProperty("Set Collision", 62, 33, 0, 0)
            AL_SetProperty("Set Layer Disabled", "BarredDoorH", true)
            AL_SetProperty("Set Layer Disabled", "BarredDoorL", true)
        end

    --Already flipped:
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (I already flipped the switch.)") ]])
        fnCutsceneBlocker()
    end

elseif(sObjectName == "BarredDoor") then

    --Variables.
    local iStForaSwitchA = VM_GetVar("Root/Variables/Chapter1/Sharelock/iStForaSwitchA", "N")
    local iStForaSwitchB = VM_GetVar("Root/Variables/Chapter1/Sharelock/iStForaSwitchB", "N")
    local iStForaSwitchC = VM_GetVar("Root/Variables/Chapter1/Sharelock/iStForaSwitchC", "N")
    local iStForaSwitchD = VM_GetVar("Root/Variables/Chapter1/Sharelock/iStForaSwitchD", "N")
    local iCount = 0
    if(iStForaSwitchA == 0.0) then iCount = iCount + 1 end
    if(iStForaSwitchB == 0.0) then iCount = iCount + 1 end
    if(iStForaSwitchC == 0.0) then iCount = iCount + 1 end
    if(iStForaSwitchD == 0.0) then iCount = iCount + 1 end
    
    --How many are left.
    if(iCount == 1.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (The door is held shut by one metal bar.[P] It looks like it's opened remotely.)") ]])
        fnCutsceneBlocker()
    elseif(iCount == 2.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (The door is held shut by two metal bars.[P] They look like they're opened remotely.)") ]])
        fnCutsceneBlocker()
    elseif(iCount == 3.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (The door is held shut by three metal bars.[P] They look like they're opened remotely.)") ]])
        fnCutsceneBlocker()
    elseif(iCount == 4.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (The door is held shut by four metal bars.[P] They look like they're opened remotely.)") ]])
        fnCutsceneBlocker()
    else
        
    end

elseif(sObjectName == "Statue") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A statue of a hooded woman with four pointed feet.[P] Her hands have four fingers each.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Knight") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A statue of a knightly woman wearing a robe.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookcaseA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Saint Fora'.[P] It's a biography, and not a fairy-tale.[P] Saint Fora was a crusader who exercised her brand of justice on hapless passerby, cutting off limbs for misbehavior and issuing beatings for speaking out of turn.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (She was also a knight who would slay dangerous creatures and defeat notorious bandits, but the people she was protecting feared her just as much.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Oddly, it seems the biographer approves of this and writes glowingly of the punishment.[P] I don't think I can read any more.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookcaseB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('The Demonic Threat'.[P] It mostly details the best way to gut a succubus.[P] It's...[P] graphic...)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookcaseC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('The Light Forgives'.[P] A set of essays about the best way to convince people to join the Holy Light.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookcaseD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Gravemarkers:: The Silent Protectors'.[P] It is a series of religious poems about the Gravemarkers protecting people and crushing evil.)") ]])
    fnCutsceneBlocker()

-- |[Rilmani Language Guide]|
elseif(sObjectName == "ImportantBook") then
    
	--Flags.
    local iStForaLanguageGuide = VM_GetVar("Root/Variables/Chapter1/Sharelock/iStForaLanguageGuide", "N")
	VM_SetVar("Root/Variables/Chapter1/Scenes/iMeiKnowsRilmani", "N", 1.0)
	VM_SetVar("Root/Variables/Chapter1/Sharelock/iStForaLanguageGuide", "N", 1.0)
	
	--Topic.
	WD_SetProperty("Unlock Topic", "Rilmani", 1)
    
    --First time:
    if(iStForaLanguageGuide == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        fnCutscene([[ Append("Mei:[E|Neutral] This book was left out when the room was boarded up.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] Can you read it?[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Surprise] You can't?[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] All the books in here are written in some script I've never seen before.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Confused] Are you saying you've been reading them effortlessly?[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Sad] I'm sorry, I thought you could read them too...[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] So?[P] What's it say?[P] Don't keep me waiting.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] It's a book about Saint Fora.[P] Apparently, she once battled the Rilmani.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Surprise] Oh?[P] What for?[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] Skip ahead a bit...[P] yadda yadda...[P] the part where she makes out with one of her travelling companions is like two chapters long...[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Happy] That's called good writing, Mei.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Offended] Yeah well, when do we get to the Rilmani?[P] Stop making out and - [B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Surprise] Oh my goodness...[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] Hey, that's the symbol on your pocket rock![B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Happy] They're exactly the same![P] The Rilmani was carrying a scroll that had my runestone's image on it![B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Smirk] Hmmm...[P] she says the Rilmani walks through a mirror and disappears, and that the rune means 'Bravery'...[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Smirk] There's a couple more runes here, and a translation.[P] I better write these down...[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Smirk] And then she closes a portal and traps Saint Fora someplace, I guess.[P] There doesn't seem to be any more Rilmani stuff in the book.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Happy] So it's a Rilmani symbol, then?[P] That's a good sign.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Happy] And the Rilmani have something to do with mirrors?[P] Assuming this book is accurate.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] Well they got the rune right.[P] The odds of some random illustration being identical is miniscule.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] I think the best place to look is that mansion you woke up in.[P] Except now we know we're looking for Rilmani stuff.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Laugh] Great![P] We're making progress![B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Happy] In between getting clobbered by statue ladies and wading through foul-smelling muck, yes.[P] Progress.") ]])
        fnCutsceneBlocker()

    --Repeats.
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        fnCutscene([[ Append("Mei:[E|Neutral] This book is about Saint Fora battling the Rilmani.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] She meets a Rilmani who seals her behind some sort of spatial portal, but the Rilmani uses a mirror to vanish, and had a scroll.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] The illustration on it matched my runestone exactly, so we need to check that mansion I woke up in for similar symbols.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] Nice recap, now let's get going.[P] I need to get me some Rilmani loot!") ]])
        fnCutsceneBlocker()

    end

-- |[ ========================================== Other ========================================= ]|
-- |[ ========================================== Debug ========================================= ]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
