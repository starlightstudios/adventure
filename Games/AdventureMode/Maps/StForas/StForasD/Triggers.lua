-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
if(sObjectName == "SharelockAgain") then

    --Repeat check.
    local iSawSharelockBasement = VM_GetVar("Root/Variables/Chapter1/Sharelock/iSawSharelockBasement", "N")
    if(iSawSharelockBasement == 1.0) then return end
    VM_SetVar("Root/Variables/Chapter1/Sharelock/iSawSharelockBasement", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter1/Sharelock/iSharelockQuestState", "N", 3.0)

    --Movement.
    fnCutsceneMove("Mei", 22.25, 6.50)
    fnCutsceneFace("Mei", -1, 1)
    fnCutsceneMove("Florentina", 21.25, 6.50)
    fnCutsceneFace("Florentina", 0, 1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Sharelock", 0, -1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sharelock", "Neutral") ]])
    fnCutscene([[ Append("Share;Lock:[E|Neutral] Found a button, did you?[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Surprise] Are you all right?[B][C]") ]])
    fnCutscene([[ Append("Share;Lock:[E|AhHa] I'm better than all right![P] I'm vindicated![P] We've solved the mystery![B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Laugh] Just as long as you're not hurt.[B][C]") ]])
    fnCutscene([[ Append("Share;Lock:[E|Neutral] I'll live.[P] I was wondering about that odd path that just stops, and then the ground opened under me.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Happy] Secret floor switch next to a harmless looking stain under a pew.[P] Classic.[B][C]") ]])
    fnCutscene([[ Append("Share;Lock:[E|AhHa] Stu-[P]pendous![B][C]") ]])
    fnCutscene([[ Append("Share;Lock:[E|Sheepish] Erm, unfortunately...[P] This is the part where we part ways.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Surprise] What?[P] But we're just getting to the fun part where we get all the secret loot![B][C]") ]])
    fnCutscene([[ Append("Share;Lock:[E|Sheepish] Well, you see, I...[P] Am not very much good in a fight.[P] And I think I damaged my leg in that fall.[B][C]") ]])
    fnCutscene([[ Append("Share;Lock:[E|Thinking] My body is very strong, but if I am some manner of clockwork machine, I am not well maintained.[B][C]") ]])
    fnCutscene([[ Append("Share;Lock:[E|Thinking] I would like to continue with you, but I do not heal normally and have not met anyone who would be able to repair me.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Sad] Oh...[B][C]") ]])
    fnCutscene([[ Append("Share;Lock:[E|Thinking] Fighting is also not my style.[P] You, however, seem well equipped for the task.[B][C]") ]])
    fnCutscene([[ Append("Share;Lock:[E|Neutral] Instead, I shall turn over the investigation to you.[P] Please let me know what you find.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] Of course![P] Where are you going?[B][C]") ]])
    fnCutscene([[ Append("Share;Lock:[E|Neutral] There is a fishing village to the west of here.[P] Lots of traders come and go.[P] Perhaps I can find someone who knows about machines there.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Happy] Oh yeah, my friend runs La D'amarrage.[P] Tell her Florentina sent you, she'll give you a room.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] And tell her I fully intend to cover the cost of the pumpkin festival last year, if she gets all whiny.[B][C]") ]])
    fnCutscene([[ Append("Share;Lock:[E|Neutral] Very good, my friends.[P] Best of luck, and I shall see you soon.[B][C]") ]])
    fnCutscene([[ Append("Share;Lock:[E|AhHa] Oh, I must write of this in my journal![P] 'Share;Lock and the Mystery of Saint Fora!'[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Smirk] Leave it to us, you go rest your leg.[P] We'll be fine.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Movement.
    fnCutsceneMove("Mei", 23.25, 6.50)
    fnCutsceneFace("Mei", -1, 0)
    fnCutsceneMove("Florentina", 22.25, 6.50)
    fnCutsceneFace("Florentina", -1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneMove("Sharelock", 21.25, 4.50)
    fnCutsceneMove("Sharelock", 22.25, 4.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Sharelock", 0, -1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneTeleport("Sharelock", -1.25, -1.50)
    fnCutscene([[ AudioManager_PlaySound("World|ClimbLadder") ]])
    fnCutsceneBlocker()

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ Append("Florentina:[E|Blush] Hey, Mei![B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] Hmm?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Blush] Loot loot loot loot![P] This place has to be loaded with treasure![B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Smirk] And maybe clues about my runestone?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Happy] Probably![B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Smirk] Yippee![P] Let's get looking!") ]])
    fnCutsceneBlocker()
    fnAutoFoldParty()
    fnCutsceneBlocker()

end
