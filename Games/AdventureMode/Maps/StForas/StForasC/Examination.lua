-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= Exits ========================================== ]|
if(sObjectName == "SampleExit") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("LevelName", "FORCEPOS:28.0x16.0x0")
    
-- |[ ====================================== Examinables ======================================= ]|
elseif(sObjectName == "StatueL") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A statue of a knight woman.[P] Her armor is covered by a long robe.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "StatueR") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The base of a destroyed statue.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Campsite") then
    VM_SetVar("Root/Variables/Chapter1/Sharelock/iSawCampsite", "N", 1.0)
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A cooking pot and bedroll of a traveller..[P] The pot is empty.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Altar") then
    VM_SetVar("Root/Variables/Chapter1/Sharelock/iSawAltar", "N", 1.0)
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A marble altar with a torn red pattern atop it.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Throne") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A fine throne made of solid marble.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "FalseFloor") then
    local iLookingForSecretFloor = VM_GetVar("Root/Variables/Chapter1/Sharelock/iLookingForSecretFloor", "N")
    local iFoundSecretFloor = VM_GetVar("Root/Variables/Chapter1/Sharelock/iFoundSecretFloor", "N")
    if(iLookingForSecretFloor == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (A spot on the floor where somebody spilled something and stained it.)") ]])
        fnCutsceneBlocker()
    elseif(iFoundSecretFloor == 0.0) then
        VM_SetVar("Root/Variables/Chapter1/Sharelock/iFoundSecretFloor", "N", 1.0)
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        fnCutscene([[ Append("Mei:[E|Neutral] Hmm, someone spilled something here.[P] Hey, wait![B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Surprise] [SOUND|World|FlipSwitch]*Click* there was a switch on the brick near the spill![B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Happy] The 'Spill' was a marker.[P] Clever.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] Did anything happen?[B][C]") ]])
        fnCutscene([[ Append("Share;Lock: [VOICE|Sharelock]Waaaauuughh!![B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Surprise] Guess whatever we did happened outside.") ]])
        fnCutsceneBlocker()
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (A marked spot on the floor which was hiding a secret switch.)") ]])
        fnCutsceneBlocker()
    end

-- |[ ========================================== Other ========================================= ]|
-- |[ ========================================== Debug ========================================= ]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
