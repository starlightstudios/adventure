-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[Sharelock]|
    if(sActorName == "Sharelock") then
        
        -- |[Variables]|
        local sMeiForm                 = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
        local sSharelockFirstForm      = VM_GetVar("Root/Variables/Chapter1/Sharelock/sSharelockFirstForm", "S")
        local iSharelockKnowsTransform = VM_GetVar("Root/Variables/Chapter1/Sharelock/iSharelockKnowsTransform", "N")
        local iSawKitchen              = VM_GetVar("Root/Variables/Chapter1/Sharelock/iSawKitchen", "N")
        local iSawCampsite             = VM_GetVar("Root/Variables/Chapter1/Sharelock/iSawCampsite", "N")
        local iSawAltar                = VM_GetVar("Root/Variables/Chapter1/Sharelock/iSawAltar", "N")
        local iSawGarden               = VM_GetVar("Root/Variables/Chapter1/Sharelock/iSawGarden", "N")
        local iSawGraves               = VM_GetVar("Root/Variables/Chapter1/Sharelock/iSawGraves", "N")
        local iTalkedToCrops           = VM_GetVar("Root/Variables/Chapter1/Sharelock/iTalkedToCrops", "N")
        local iLookingForSecretFloor   = VM_GetVar("Root/Variables/Chapter1/Sharelock/iLookingForSecretFloor", "N")
        local iFoundSecretFloor        = VM_GetVar("Root/Variables/Chapter1/Sharelock/iFoundSecretFloor", "N")
        
        -- |[Mei's Form Dialogue]|
        if(sMeiForm ~= sSharelockFirstForm and iSharelockKnowsTransform == 0.0) then
            VM_SetVar("Root/Variables/Chapter1/Sharelock/iSharelockKnowsTransform", "N", 1.0)
            
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sharelock", "Neutral") ]])
            
            --Human
            if(sMeiForm == "Human") then
                fnCutscene([[ Append("Share;Lock:[E|Neutral] Most interesting.[P] Mei, when we first met, you were not a human.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Offended] What's that supposed to mean?[B][C]") ]])
                fnCutscene([[ Append("Share;Lock:[E|Neutral] You are a human now.[P] I know you said you were not from here, but that is not a common occurrence.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Smirk] Oh, sorry.[P] I thought you were implying something else.[B][C]") ]])
                fnCutscene([[ Append("Share;Lock:[E|Thinking] Hmmm, let me see.[P] Disguise magic is fairly common, but you do not seem magically inclined.[B][C]") ]])
                
            --Slime
            elseif(sMeiForm == "Slime") then
                fnCutscene([[ Append("Share;Lock:[E|Neutral] Most interesting.[P] Mei, when we first met, you were not a slime.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Yeah, happens to the best of us.[B][C]") ]])
                fnCutscene([[ Append("Share;Lock:[E|Neutral] ...[P] Come again?[B][C]") ]])
                fnCutscene([[ Append("Share;Lock:[E|Thinking] Nevermind.[P] I've lost most of my memories, but I seem to recall magical disguises being real.[B][C]") ]])
                fnCutscene([[ Append("Share;Lock:[E|Thinking] But you gave no indication of being a mage...[P] and to disguise as a slime?[P] Oh well.[B][C]") ]])
            
            --Alraune
            elseif(sMeiForm == "Alraune") then
                fnCutscene([[ Append("Share;Lock:[E|Neutral] Most interesting.[P] Mei, when we first met, you were not an alraune.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Oh, uh, this is my true form.[P] I love being a plant![B][C]") ]])
                fnCutscene([[ Append("Share;Lock:[E|Thinking] A disguise, then?[P] My memories were lost, is that common?[P] You do not seem magically inclined.[B][C]") ]])
            
            --Bee
            elseif(sMeiForm == "Bee") then
                fnCutscene([[ Append("Share;Lock:[E|Neutral] Most interesting.[P] Mei, when we first met, you were not an bee.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Oh, uh, this is my true form.[P] I'm never alone with my hive sisters there![B][C]") ]])
                fnCutscene([[ Append("Share;Lock:[E|Neutral] I see.[P] My memories were lost in an unknown incident.[P] May I ask if magical disguises are common?[B][C]") ]])
            
            --Ghost
            elseif(sMeiForm == "Ghost") then
                fnCutscene([[ Append("Share;Lock:[E|Neutral] Most interesting.[P] Mei, are you undead now, or were you before, and merely hiding it?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Uh, both?[P] It's a long story.[B][C]") ]])
                fnCutscene([[ Append("Share;Lock:[E|Neutral] A magical disguise to hide undeath.[P] Hm.[P] Is that a common skill?[P] Is it one I should know myself?[B][C]") ]])
            
            --Werecat
            elseif(sMeiForm == "Werecat") then
                fnCutscene([[ Append("Share;Lock:[E|Neutral] Most interesting.[P] Mei, when we first met, you were not an werecat.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Blush] Well, a lot can happen to a girl.[B][C]") ]])
                fnCutscene([[ Append("Share;Lock:[E|Neutral] Were you disguising yourself in some way?[P] Is that some sort of magic?[B][C]") ]])
            
            --Gravemarker
            elseif(sMeiForm == "Gravemarker") then
                fnCutscene([[ Append("Share;Lock:[E|Neutral] Most interesting.[P] What sort of a creature are you?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] A gravemarker.[P] An angelic woman made of stone.[B][C]") ]])
                fnCutscene([[ Append("Share;Lock:[E|Neutral] I see.[P] So is this the disguise, or was your previous form the disguise?[P] I'm not trying to be rude.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Uhhhhh, this is the disguise?[P] Can I do that?[B][C]") ]])
    
            --Wisphag
            elseif(sMeiForm == "Wisphag") then
                fnCutscene([[ Append("Share;Lock:[E|Neutral] Most interesting.[P] Mei, when we first met, you were not a...[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] A...[B][C]") ]])
                fnCutscene([[ Append("Share;Lock:[E|Neutral] Wisp...[P] crone?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] That's what I've been trying to say, but everyone calls us wisphags.[B][C]") ]])
                fnCutscene([[ Append("Share;Lock:[E|Neutral] So is this a disguise of some sort?[B][C]") ]])
            
            --Mannequin
            elseif(sMeiForm == "Mannequin") then
                fnCutscene([[ Append("Share;Lock:[E|Neutral] Most interesting.[P] Do you have any sort of internal mechanisms?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Huh?[P] No.[P] Just magical resin.[B][C]") ]])
                fnCutscene([[ Append("Share;Lock:[E|Neutral] Blast.[P] I had wondered if you were yourself another clue.[B][C]") ]])
                fnCutscene([[ Append("Share;Lock:[E|Neutral] Is this a disguise of some sort, then?[B][C]") ]])
            end
            fnCutscene([[ Append("Florentina:[E|Neutral] It's not that hard, even a total amateur can magically disguise themselves as a human.[B][C]") ]])
            fnCutscene([[ Append("Share;Lock:[E|Thinking] I do not have any reliable way of determining if this is a disguise or not, unfortunately.[P] Magic is not my specialty.[B][C]") ]])
            fnCutscene([[ Append("Share;Lock:[E|Neutral] Mei, I would advise being more careful around others when changing disguises.[P] If you are attempting to hide your true form...[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] It's all right.[P] Uh, is my secret safe with you?[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] Most people don't really care about magic disguises.[P] Not in Trannadar, anyway.[B][C]") ]])
            fnCutscene([[ Append("Share;Lock:[E|Neutral] Yes, but I have been informed some of the more...[P] prejudiced...[P] human societies are greatly upset by them.[B][C]") ]])
            fnCutscene([[ Append("Share;Lock:[E|Neutral] So long as you're aware of that.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Happy] Thanks for the tip, Share;Lock.") ]])
        
        -- |[Mystery Dialogue]|
        elseif(iLookingForSecretFloor == 0.0) then
        
            --Setup.
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sharelock", "Neutral") ]])
            fnCutscene([[ Append("Share;Lock:[E|Neutral] Have you taken a look around the area for clues?[B][C]") ]])
            
            --Not yet.
            if(iSawKitchen == 0.0 or iSawCampsite == 0.0 or iSawAltar == 0.0 or iSawGarden == 0.0 or iSawGraves == 0.0) then
                fnCutscene([[ Append("Mei:[E|Neutral] Not quite yet.[P] What are we looking for?[B][C]") ]])
                fnCutscene([[ Append("Share;Lock:[E|Neutral] Check both inside and outside.[P] Check the kitchen, altar, campsite, graves, and the crops over there.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] We're on it.") ]])
            
            --Done investigating.
            else
                VM_SetVar("Root/Variables/Chapter1/Sharelock/iLookingForSecretFloor", "N", 1.0)
                VM_SetVar("Root/Variables/Chapter1/Sharelock/iSharelockQuestState", "N", 2.0)
                fnCutscene([[ Append("Mei:[E|Neutral] We had a look around, but I'm not sure what it is you wanted us to see.[B][C]") ]])
                fnCutscene([[ Append("Share;Lock:[E|Neutral] Less what I wanted you to see, but rather, what was not present.[P] Let us go through what we have.[B][C]") ]])
                fnCutscene([[ Append("Share;Lock:[E|Thinking] The west side was likely a kitchen.[P] The stove still works, though anything of use was looted long ago.[B][C]") ]])
                fnCutscene([[ Append("Share;Lock:[E|Thinking] The wooden building in the middle was likely a gardener's shed.[P] The grounds are well-maintained, with carefully arranged flowers.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Happy] The gardener had excellent taste![B][C]") ]])
                fnCutscene([[ Append("Share;Lock:[E|Thinking] There are a number of graves over there, though I couldn't read the headstones.[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Neutral] It's not common in the culture here to place a headstone, that's more of a western thing.[P] Jeffespeir or further.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] How do you mark graves in Trannadar?[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Happy] By getting really really wasted and burning the body.[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Neutral] It's to return the ash to the little ones.[P] Supposed to make the person be reborn as a plant.[B][C]") ]])
                fnCutscene([[ Append("Share;Lock:[E|Thinking] That is very interesting Florentina, thank you.[P] This further informs us that this area was constructed before that custom was established...[B][C]") ]])
                fnCutscene([[ Append("Share;Lock:[E|Thinking] Or that it was occupied by people who did not share that custom.[P] Foreigners.[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Neutral] That's what I was thinking, too.[P] You don't see graves that often here, unless the grave is really old.[B][C]") ]])
                fnCutscene([[ Append("Share;Lock:[E|Thinking] There is an altar within the main building, and rotted wooden pews.[P] That suggests a religious nature.[B][C]") ]])
                fnCutscene([[ Append("Share;Lock:[E|Thinking] Many hierarchical religious groups use long benches to separate the followers from the leader.[B][C]") ]])
                fnCutscene([[ Append("Share;Lock:[E|Thinking] Finally, there is a small campsite within the main building.[P] It is not mine, but its owner is nowhere to be found.[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Neutral] Could be that they just abandoned it.[B][C]") ]])
                fnCutscene([[ Append("Share;Lock:[E|Thinking] Very possible, but I think the owner is still here.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Surprise] Really?[P] But I didn't see anyone.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Surprise] Are they invisible?[P] Are they right next to me?[B][C]") ]])
                fnCutscene([[ Append("Share;Lock:[E|Neutral] Ah, no.[P] That was not what I was implying.[B][C]") ]])
                fnCutscene([[ Append("Share;Lock:[E|Neutral] Rather, what is important is what we did not see in our investigation.[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Neutral] A kitchen for food, some gardens, some wild crops, and a worship area.[B][C]") ]])
                fnCutscene([[ Append("Share;Lock:[E|Neutral] Have you deduced what is missing?[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Confused] Beds.[P] There's no place to sleep.[B][C]") ]])
                fnCutscene([[ Append("Share;Lock:[E|AhHa] Pre-[P]cisely the conclusion I came to![B][C]") ]])
                fnCutscene([[ Append("Share;Lock:[E|AhHa] There must be a hidden location where the worshippers lived and worked, a space hidden from the public.[B][C]") ]])
                fnCutscene([[ Append("Share;Lock:[E|AhHa] Considering how well kept the gardens are despite centuries of so-called abandonment, it is quite likely that those worshippers, or some other group, is still here and safely hidden in that location![B][C]") ]])
                fnCutscene([[ Append("Share;Lock:[E|AhHa] The person who set up the campsite may have stumbled upon it, and been captured, or even simply gotten stuck![P] Regardless, we must find them![B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Surprise] Amazing![P] I hadn't even thought of that![B][C]") ]])
                
                local iTalkedToCrops = VM_GetVar("Root/Variables/Chapter1/Sharelock/iTalkedToCrops", "N")
                if(iTalkedToCrops == 1.0) then
                    fnCutscene([[ Append("Mei:[E|Smirk] That would explain the crops speaking about a winged woman taking care of them.[B][C]") ]])
                    fnCutscene([[ Append("Share;Lock:[E|Sheepish] The crops -[P] winged -[P] what?[B][C]") ]])
                    fnCutscene([[ Append("Florentina:[E|Neutral] I'm an alraune, we can speak to plants.[P] These tomatoes said they were being taken care of by winged white ladies.[B][C]") ]])
                    fnCutscene([[ Append("Share;Lock:[E|Sheepish] You can do that?[B][C]") ]])
                    fnCutscene([[ Append("Share;Lock:[E|Thinking] Ahem, fascinating.[P] I was not even aware there were witnesses we could question.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Smirk] Well now you are.[B][C]") ]])
                    fnCutscene([[ Append("Share;Lock:[E|Thinking] Indeed.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] So, we should try to find out where the secret area is?[B][C]") ]])
                else
                    fnCutscene([[ Append("Mei:[E|Neutral] So, what next?[B][C]") ]])
                    fnCutscene([[ Append("Share;Lock:[E|Thinking] There must be a secret area somewhere.[P] We'll have to search carefully.[B][C]") ]])
                end
                fnCutscene([[ Append("Share;Lock:[E|Neutral] I have been examining this outdoor area.[P] I believe I can see some sort of passage at the bottom of the pond, but have been stumped on how to enter it.[B][C]") ]])
                fnCutscene([[ Append("Share;Lock:[E|Neutral] If you two could please search the other areas, I'm sure we'll find a way inside.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Happy] All right![P] Come on, Florentina!") ]])
            end
        
        -- |[Looking for Secret Floor]|
        elseif(iFoundSecretFloor == 0.0) then
        
            --Setup.
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sharelock", "Neutral") ]])
            fnCutscene([[ Append("Share;Lock:[E|Neutral] Find anything yet?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] Not yet.[P] You?[B][C]") ]])
            fnCutscene([[ Append("Share;Lock:[E|Neutral] The stone below is waterproof as there are no air bubbles coming up, or totally flooded, which is less likely.[B][C]") ]])
            fnCutscene([[ Append("Share;Lock:[E|Neutral] It seems to run north to south.[P] I would advise searching the altar room first.[B][C]") ]])
            fnCutscene([[ Append("Share;Lock:[E|Neutral] There could also be a switch hidden in the pond, that's what I'm searching for.[P] Good luck.") ]])
        
        end
        
        
    end
end
