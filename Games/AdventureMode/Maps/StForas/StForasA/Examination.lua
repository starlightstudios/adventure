-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= Exits ========================================== ]|
if(sObjectName == "ToStForasD") then
    local iFoundSecretFloor = VM_GetVar("Root/Variables/Chapter1/Sharelock/iFoundSecretFloor", "N")
    if(iFoundSecretFloor == 1.0) then
        AudioManager_PlaySound("World|ClimbLadder")
        AL_BeginTransitionTo("StForasD", "FORCEPOS:22.0x4.0x0")
    end
    
-- |[ ====================================== Examinables ======================================= ]|
elseif(sObjectName == "PondL") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (An overgrown pond with a small dock in it.[P] Reeds break the surface.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "PondR") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A pond, without as much swamp plantlife in it as the other pond.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Barrels") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Barrels full of what I can only assume is fertilizer.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Crop") then
    VM_SetVar("Root/Variables/Chapter1/Sharelock/iSawGarden", "N", 1.0)
    local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
    
    fnCutscene([[ WD_SetProperty("Show") ]])
    if(sMeiForm ~= "Alraune") then
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Untended wild crops.)") ]])
    else
        VM_SetVar("Root/Variables/Chapter1/Sharelock/iTalkedToCrops", "N", 1.0)
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Untended wild crops.[P] Is there anyone taking care of this area?)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (...)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Winged rock women?[P] Are you sure?[P] Well, all right.)") ]])
    end
    fnCutsceneBlocker()
    
elseif(sObjectName == "Graves") then
    VM_SetVar("Root/Variables/Chapter1/Sharelock/iSawGraves", "N", 1.0)
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Clearly visible graves, nothing is growing on top of them.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Headstone") then
    VM_SetVar("Root/Variables/Chapter1/Sharelock/iSawGraves", "N", 1.0)
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The headstone's text is so worn as to be illegible.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Boulders") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Some boulders are here, not part of the brickwork.[P] They must have been moved here to block the entrance.)") ]])
    fnCutsceneBlocker()

-- |[ ========================================== Other ========================================= ]|
-- |[ ========================================== Debug ========================================= ]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
