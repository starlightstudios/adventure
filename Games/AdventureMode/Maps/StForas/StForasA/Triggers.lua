-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
if(sObjectName == "WarpActivate") then
    fnWarpUnlockHandler("Root/Variables/Chapter1/Campfires/iStForasA", 21.25, 18.50)

-- |[ =================================== Meeting Share;Lock =================================== ]|
elseif(sObjectName == "Sharelock") then

    --Repeat check.
    local iMetSharelock = VM_GetVar("Root/Variables/Chapter1/Sharelock/iMetSharelock", "N")
    if(iMetSharelock == 1.0) then return end
    VM_SetVar("Root/Variables/Chapter1/Sharelock/iMetSharelock", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter1/Sharelock/iSharelockQuestState", "N", 1.0)
    
    --Store Mei's initial form.
    local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
    VM_SetVar("Root/Variables/Chapter1/Sharelock/sSharelockFirstForm", "S", sMeiForm)
        
    -- |[Achievement]|
    AM_SetPropertyJournal("Unlock Achievement", "MeetSharelock")
    
    --Movement.
    fnCutsceneMergeParty()
    fnCutsceneBlocker()
    fnCutsceneMove("Mei", 14.25, 16.50)
    fnCutsceneMove("Florentina", 15.25, 16.50)
    fnCutsceneFace("Mei", 0, 1)
    fnCutsceneFace("Florentina", 0, 1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ Append("Florentina:[E|Neutral] *Hold up, Mei. Hostile, dead ahead.*[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] *How do you know she's hostile?*[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Surprise] *All the other monstergirls we've run into have been.*[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Offended] *Maybe if you tried talking to them?*[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Happy] *Me?[P] You didn't try either!*") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneFace("Sharelock", 0, -1)
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutsceneMove("Sharelock", 15.25, 20.50)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sharelock", "Neutral") ]])
    fnCutscene([[ Append("Robot:[E|Neutral] Excuse me, I can hear everything you're saying.[P] You're talking very loudly, but in an atonal way.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] Yeah that's whispering.[B][C]") ]])
    fnCutscene([[ Append("Robot:[E|Neutral] Whispering is quiet, you're just being atonal.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] Okay, so are you a hostile or not?[B][C]") ]])
    fnCutscene([[ Append("Robot:[E|Neutral] Clearly I am not, because otherwise I would have attacked you instead of patiently listening to you talk about attacking me.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] Hey, she's smart.[P] We're in luck.[B][C]") ]])
    fnCutscene([[ Append("Robot:[E|Neutral] I do not have any money for you to steal.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Surprise] What is it about me that makes everyone assume I'm going to rob them?[B][C]") ]])
    fnCutscene([[ Append("Robot:[E|Neutral] Nothing about your appearance and everything about your circumstances.[B][C]") ]])
    fnCutscene([[ Append("Robot:[E|Thinking] You are armed, rather conspicuously, and in a ruin which shows no immediate signs of habitation.[P] You are explorers, or looters.[B][C]") ]])
    fnCutscene([[ Append("Robot:[E|Thinking] Travellers and traders avoid dangerous locations like this and stay on the main road.[P] And if you cannot find discarded treasure, you take it.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Laugh] She's got you pegged, Florentina![B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Happy] Yeah, she does.[P] But allow me to do one of my own.[B][C]") ]])
    fnCutscene([[ Append("Robot:[E|Neutral] Oh? Are you about to impress me?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] You're none of the above.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] You're here for the fun of it.[P] The thrill of doing something nobody else has done.[P] The loot means nothing to you.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] And you have bad eyesight.[P] That's from the magnifying glass.[B][C]") ]])
    fnCutscene([[ Append("Robot:[E|Neutral] The observation is accurate, mostly, but it is explaining its basis that shows the mark of a developed mind.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] You know the types of person that would be in a place like this, you're here, and you are certainly not inhabiting it.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] Therefore, you fall into one of the categories, but you're not a looter, because you're not armed.[P] Also, you'd have sold your loot to me.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] I run one of the shops at the trading post, by the way.[B][C]") ]])
    fnCutscene([[ Append("Robot:[E|Neutral] Keep going.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] Your logical, deductive, calm demeanor suggests someone who thinks rather than fights.[P] You're missing out, because beating people up is great, but to each their own.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] You heard about the mysterious ruin where people go missing and decided to get to the bottom of it, and came from a long way away to do it.[P] And you just arrived, too, because you haven't set up a campsite.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Happy] Or maybe you think you're so good you'll have it all taken care of in a single day?[B][C]") ]])
    fnCutscene([[ Append("Robot:[E|Neutral] Impressive.[P] Mostly correct, though some of it was wrong for reasons you can't quite know.[P] After all, I could have set a campsite in that main building there that you haven't seen.[B][C]") ]])
    fnCutscene([[ Append("Robot:[E|Neutral] I have not, by the way.[P] There is a sleeping bag within, but it was here when I arrived.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] You two are having fun, but who are you exactly?[B][C]") ]])
    fnCutscene([[ Append("Robot:[E|Neutral] So caught up in the fun that I failed to introduce myself?[P] Where are my manners?[B][C]") ]])
    fnCutscene([[ Append("Share;Lock:[E|Neutral] Share;Lock is my name, or SL-22, if the carvings on my arm are to be believed.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] (Share;Lock?[P] Really?[P] That can't be a coincidence...)[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] I'm Florentina, and this goober is Mei.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Smirk] Nice to meet you.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] So, Share;Lock.[P] What the heck kind of monstergirl are you?[P] I've never seen someone like you.[B][C]") ]])
    fnCutscene([[ Append("Share;Lock:[E|Thinking] Unfortunate.[P] When you displayed your wit, I had hoped perhaps you knew.[P] The search continues.[B][C]") ]])
    fnCutscene([[ Append("Share;Lock:[E|Neutral] I do not know what species of partirhuman I am, though I have deduced I am some sort of elaborate machine.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Smirk] Maybe if you retraced your steps?[P] Find someone who knows you?[B][C]") ]])
    fnCutscene([[ Append("Share;Lock:[E|Thinking] I awoke with no memories in a building to the east of here a few weeks ago.[P] There were some rather unpleasant religious fanatics there.[B][C]") ]])
    fnCutscene([[ Append("Share;Lock:[E|Thinking] I learned my chassis is firmer than a knife at that time.[P] The first of many observations.[B][C]") ]])
    fnCutscene([[ Append("Share;Lock:[E|Neutral] Fortunately, I found my journal nearby.[P] I have managed to piece together some of my past from it.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] Oh really?[P] That's a lucky find.[P] May I see it?[B][C]") ]])
    fnCutscene([[ Append("Share;Lock:[E|Neutral] [SOUND|World|TakeItem]Take a look.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] (It says 'The Collected Tales of Sherlock Holmes'.[P] It's an anthology of Doyle's stories.)[B][C]") ]])
    fnCutscene([[ Append("Share;Lock:[E|Neutral] Obviously, it was written in code, though I had no issue deciphering it.[P] I can - [P][CLEAR]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] This is written in English.[B][C]") ]])
    fnCutscene([[ Append("Share;Lock:[E|Neutral] Yes![P] That is what the code said it was, after I had deciphered it![P] How did you know?[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] I took it as a second language in school.[P] Hong Kong used to be an English colony.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Smirk] Plus, some of my favourite indie games are in English.[P] I can read it pretty well.[B][C]") ]])
    fnCutscene([[ Append("Share;Lock:[E|Thinking] Well, as you can see, my name is Share;Lock, and I am a detective.[B][C]") ]])
    fnCutscene([[ Append("Share;Lock:[E|Thinking] I am not sure how I came to be a machine girl, or lost my memories, but I am working on that.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] (I really don't know if I should tell her this is a work of fiction.[P] Will she even believe me?)[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Smirk] (Maybe I should write it in the back here, so when she reads it...)[B][C]") ]])
    fnCutscene([[ Append("Share;Lock:[E|Thinking] Oh, feel free to take notes.[P] Just make sure to write it in English code.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] I did, don't worry.[B][C]") ]])
    fnCutscene([[ Append("Share;Lock:[E|Neutral] Perhaps English is a common encoding method where you are from.[P] Where is Hong Kong?[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] Uh, Earth.[B][C]") ]])
    fnCutscene([[ Append("Share;Lock:[E|Neutral] Is it near London?[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Smirk] Pretty far from it, actually, but you can get to London by ship.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] We're trying to find a way back home for Mei, actually.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] It's possible there's a clue in St. Fora's.[B][C]") ]])
    fnCutscene([[ Append("Share;Lock:[E|AhHa] Spec-tacular![P] I was looking for something similar![B][C]") ]])
    fnCutscene([[ Append("Share;Lock:[E|AhHa] You see, I have heard tales that there are some sort of solid, hard monstergirl in this area![P] Much like my metallic form![B][C]") ]])
    if(sMeiForm == "Gravemarker") then
        fnCutscene([[ Append("Share;Lock:[E|AhHa] I'd ask if you know anything about that but it is self-evident.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Smirk] I actually don't.[P] I've never been to this place.[P] I became this way...[P] elsewhere.[B][C]") ]])
        fnCutscene([[ Append("Share;Lock:[E|Thinking] Truly?[P] Still, your knowledge of English and fortuitous arrival are still welcome, and your curiousity shall be sated.[P] I, Share;Lock, shall help you solve this mystery![B][C]") ]])
    elseif(sMeiForm == "Mannequin") then
        fnCutscene([[ Append("Share;Lock:[E|Thinking] Plastic, or maybe resin, no. I had heard they were stone. Do you know what I'm talking about?[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] Not at all. This mannequin form is the result of a curse.[B][C]") ]])
        fnCutscene([[ Append("Share;Lock:[E|Neutral] Then the joy of discovery, we shall share in.[P] I, Share;Lock, shall help you solve this mystery![B][C]") ]])
    else
        fnCutscene([[ Append("Share;Lock:[E|AhHa] Clearly our paths have crossed because we share a similar point of origin.[P] I, Share;Lock, shall help you solve this mystery, Mei from Hong Kong![B][C]") ]])
    end
    fnCutscene([[ Append("Mei:[E|Neutral] Great! So what have you found so far?[B][C]") ]])
    fnCutscene([[ Append("Share;Lock:[E|Neutral] While I have only just arrived, my preliminary search has yielded some oddities.[P] However, I suggest you take a look around yourself, first.[P] Speak to me when you've had a look around, and we'll compare notes.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnAutoFoldParty()
    fnCutsceneBlocker()

-- |[ ===================================== Mycela Defeated ==================================== ]|
--Mycela Cutscene
elseif(sObjectName == "Mycela") then

    -- |[ ==================== Setup =================== ]|
    -- |[Variables]|
    local iHasAlrauneForm = VM_GetVar("Root/Variables/Global/Mei/iHasAlrauneForm", "N")
    
    -- |[Flags]|
    --Set Variables.
    VM_SetVar("Root/Variables/Chapter1/Alraunes/iMetRochea", "N", 1.0)
    
    --Topics.
    WD_SetProperty("Unlock Topic", "Mycela", 1)
    
    --Mark Achievement
    AM_SetPropertyJournal("Unlock Achievement", "FinishConvent")

    -- |[ ================ Construction ================ ]|
    --Cut to black.
    fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Position Mei and Florentina.
    fnCutsceneTeleport("Mei", 18.25, 16.50)
    fnCutsceneFace("Mei", 0, 1) 
    fnCutsceneTeleport("Florentina", 18.25, 17.50)
    fnCutsceneFace("Florentina", 0, 1)
    
    --Spawn Mycela
    TA_Create("Mycela")
        TA_SetProperty("Position", 18, 18)
        TA_SetProperty("Facing", gci_Face_South)
        TA_SetProperty("Clipping Flag", false)
        fnSetCharacterGraphics("Root/Images/Sprites/Mycela/", false)
        TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/Mycela|Wounded")
        TA_SetProperty("Set Special Frame", "Wounded")
    DL_PopActiveObject()
    
    --Spawn Rochea
    TA_Create("Rochea")
        TA_SetProperty("Position", -1, -1)
        TA_SetProperty("Facing", gci_Face_South)
        TA_SetProperty("Clipping Flag", false)
        fnSetCharacterGraphics("Root/Images/Sprites/Rochea/", false)
    DL_PopActiveObject()
    TA_Create("Alraune")
        TA_SetProperty("Position", -1, -1)
        TA_SetProperty("Facing", gci_Face_South)
        TA_SetProperty("Clipping Flag", false)
        fnSetCharacterGraphics("Root/Images/Sprites/Alraune/", false)
    DL_PopActiveObject()
    
    -- |[ ============= Cutscene Execution ============= ]|
    --Fade in.
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    
    --Sound, movement.
    fnCutscene([[ AudioManager_PlaySound("World|FlipSwitch") ]])
    fnCutsceneTeleport("Rochea", 11.25, 13.50)
    fnCutsceneBlocker()
    fnCutsceneMove("Rochea", 11.25, 14.50)
    fnCutsceneBlocker()
    fnCutsceneFace("Mei", -1, 0) 
    fnCutsceneFace("Florentina", -1, 0)
    fnCutsceneTeleport("Alraune", 11.25, 13.50)
    fnCutsceneBlocker()
    fnCutsceneMove("Rochea", 11.25, 16.50)
    fnCutsceneMove("Rochea", 15.25, 16.50)
    fnCutsceneMove("Alraune", 11.25, 17.50)
    fnCutsceneMove("Alraune", 15.25, 17.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Rochea", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Alraune", "Neutral") ]])
    fnCutscene([[ Append("Rochea:[E|Neutral] I have come as you requested, Florentina, and I have only brought one follower.[P] Please meet Agapa.[B][C]") ]])
    fnCutscene([[ Append("Agapa:[E|Neutral] Hello.[B][C]") ]])
    if(iHasAlrauneForm == 1.0) then
        fnCutscene([[ Append("Mei:[E|Happy] Thank you for coming, leaf-sister.[P] It is a matter most dire.[B][C]") ]])
    else
        fnCutscene([[ Append("Mei:[E|Happy] Thank you for coming, miss Rochea.[P] It is a matter most dire.[B][C]") ]])
    end
    fnCutscene([[ Append("Florentina:[E|Neutral] Knock off the formalities.[P] See this alraune on the ground?[P] Is she one of yours?[B][C]") ]])
    fnCutscene([[ Append("Rochea:[E|Neutral] May I?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Movement.
    fnCutsceneMove("Rochea", 17.25, 16.50)
    fnCutsceneMove("Rochea", 17.25, 18.50)
    fnCutsceneFace("Rochea", 1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Rochea", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Alraune", "Neutral") ]])
    fnCutscene([[ Append("Rochea:[E|Neutral] I do not recognize her at all.[P] Agapa, was she joined by your coven?[B][C]") ]])
    fnCutscene([[ Append("Agapa:[E|Neutral] No, I do not recognize her.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] Hmmm...[B][C]") ]])
    fnCutscene([[ Append("Rochea:[E|Neutral] What precisely was she doing?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] Sticking mushrooms on alraunes, which made them act...[P] off.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] She seemed to think the mushrooms were little ones.[B][C]") ]])
    fnCutscene([[ Append("Agapa:[E|Neutral] The room you described sounded like a spawning pool.[P] Is that in the basement here?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] It looked like it, but it hadn't been used for a long time.[B][C]") ]])
    fnCutscene([[ Append("Rochea:[E|Neutral] If she has contravened the laws of nature, she must be cleansed.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Surprise] Wait, no![B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Surprise] Ask her what happened when she wakes up![P] She was taking orders from the mushroom we cut off![B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] It was really big, covered her with webs of fungus.[P] Something was up with it.[B][C]") ]])
    fnCutscene([[ Append("Rochea:[E|Neutral] I see.[P] You wish us to take care of her.[B][C]") ]])
    fnCutscene([[ Append("Rochea:[E|Neutral] It is good that you asked me here, Florentina.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] I'm not sure I trust the trading post to deal with this.[P] That's not saying I trust you, either.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] This is a sensitive issue, and I need to do some more research into it.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] Did you see the mushraunes as well?[B][C]") ]])
    fnCutscene([[ Append("Agapa:[E|Neutral] Yes, some of them are members of our coven.[P] We thought they had simply decided to migrate elsewhere.[B][C]") ]])
    fnCutscene([[ Append("Agapa:[E|Neutral] It is uncommon to simply leave without a word, but not unheard of.[B][C]") ]])
    fnCutscene([[ Append("Rochea:[E|Neutral] We will need to be careful to extract the fungus from our leaf sisters, now.[B][C]") ]])
    fnCutscene([[ Append("Rochea:[E|Neutral] Thank you for informing us of this problem.[B][C]") ]])
    fnCutscene([[ Append("Rochea:[E|Neutral] There have been many odd occurrences in Trannadar as of late.[P] This is merely yet another we must confront.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] You're not alone.[B][C]") ]])
    fnCutscene([[ Append("Rochea:[E|Neutral] Nor are you.[P] Thank you, and do not hesitate to summon us if we may be of aid, Florentina.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] ...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Rochea drags Mycela.
    fnCutsceneMove("Rochea", 13.25, 18.50)
    fnCutsceneMove("Mycela", 14.25, 18.50)
    fnCutsceneBlocker()
    fnCutsceneMove("Alraune", 15.25, 18.50)
    fnCutsceneBlocker()
    fnCutsceneMove("Rochea", 11.25, 18.50)
    fnCutsceneMove("Mycela", 12.25, 18.50)
    fnCutsceneMove("Alraune", 13.25, 18.50)
    fnCutsceneBlocker()
    fnCutsceneMove("Rochea", 11.25, 17.50)
    fnCutsceneMove("Mycela", 11.25, 18.50)
    fnCutsceneMove("Alraune", 12.25, 18.50)
    fnCutsceneBlocker()
    fnCutsceneMove("Rochea", 11.25, 13.50)
    fnCutsceneMove("Mycela", 11.25, 14.50)
    fnCutsceneMove("Alraune", 11.25, 15.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|FlipSwitch") ]])
    fnCutsceneTeleport("Rochea", -1.25, -1.50)
    fnCutsceneTeleport("Alraune", -1.25, -1.50)
    fnCutsceneTeleport("Mycela", -1.25, -1.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Mei", 0, 1)
    fnCutsceneFace("Florentina", 0, -1)
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("Mei:[E|Sad] You think they'll be okay?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] Probably.[P] We can check back in with them later.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] Rochea is the best of a bad set of options.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Offended] You keep saying that.[P] What's so bad about Rochea?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] She wanted to cleanse her again, make her forget.[P] Wipe the slate clean.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] Every time you do something she deems to be bad, she wipes your memory and re-educates you.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] That kind of attitude makes me sick.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] Oh.[P] I hadn't thought about it that way.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] If we handed her over to any humans, they'd probably just execute her.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Surprise] Even Captain Blythe?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Happy] Him, no,[P] probably not.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] But he wouldn't have any idea what to do with her, either.[P] Throw her in the brig?[P] For how long, forever?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] For what crime?[P] Transforming others happens all the time, next you'll ask him to brig the werecats.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Facepalm] (Actually maybe he [P]*should*[P] do that...)[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] At least with Rochea, she'll have another chance, and might be helpful in the future.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Sad] I'm sorry, Florentina.[P] I'm sure things will work out.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] Maybe, maybe not.[P] Let's keep going.[P] We can check in with her later if you're worried.") ]])
    fnCutsceneBlocker()
    fnAutoFoldParty()
    fnCutsceneBlocker()

-- |[ ================================= End of Mushraune Scene ================================= ]|
elseif(sObjectName == "Mushraune") then
    
    -- |[Setup]|
    --Snap to black.
    fnCutsceneFadeOut()

    --Position Mei and Florentina.
    fnCutsceneTeleport("Mei", 21.25, 17.50)
    fnCutsceneFace("Mei", 0, 1) 
    fnCutsceneTeleport("Florentina", 22.25, 17.50)
    fnCutsceneFace("Florentina", -1, 0)
    fnCutsceneSetFrame("Mei", "Crouch")
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()

    -- |[Fade In]|
    fnCutsceneFadeIn(125)
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("Florentina:[E|Surprise] Can you walk?[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Sad] Ah ah ah, yeah, I think so.[P] They really went in on my legs.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Happy] Thank goodness for that runestone healing thing.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Happy] Still stings but I think I can stand.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneSetFrame("Mei", "Null")
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutsceneFace("Mei", 1, 0)
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("Florentina:[E|Neutral] Good, if you can walk you can fight.[P] And I'm itching for some revenge.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Offended] Just breezing right past the fact that you've joined the mushraunes?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Blush] Joined?[P] No, you dope, this is a business arrangement.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Blush] Huh?[P] Oh, he says he's pleased to meet you.[P] What a sap.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] Who said what now?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] You can't hear him?[P] I guess only I can.[P] That's fine.[P] Yeah, this fungus is a buddy of mine.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] We met in the clink.[P] He helped bluff the guards.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Smirk] You make friends very easily.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Happy] The deal is I take him around, we see the world, he helps me change appearances on a dime.[P] I ditch him in a cave when he's had enough.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Smirk] Are you sure you're not going to hang on to your fungus buddy for a bit longer than that?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] He's useful.[P] I keep useful people around.[P] Otherwise I'd have left you in the cave.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Offended] Thanks a lot.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] So, can you...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneFace("Florentina", -1, -1)
    fnCutsceneWait(5)
    fnCutsceneBlocker()
    fnCutsceneFace("Florentina", 0, -1)
    fnCutsceneWait(5)
    fnCutsceneBlocker()
    fnCutsceneFace("Florentina", 1, -1)
    fnCutsceneWait(5)
    fnCutsceneBlocker()
    fnCutscenePlaySound("World|TakeItem")
    fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Florentina/Job_Merchant.lua") ]])
    fnCutsceneBlocker()
    fnCutsceneFace("Florentina", 1, 0)
    fnCutsceneWait(5)
    fnCutsceneBlocker()
    fnCutsceneFace("Florentina", 1, 1)
    fnCutsceneWait(5)
    fnCutsceneBlocker()
    fnCutsceneFace("Florentina", 0, 1)
    fnCutsceneWait(5)
    fnCutsceneBlocker()
    fnCutsceneFace("Florentina", -1, 1)
    fnCutsceneWait(5)
    fnCutsceneBlocker()
    fnCutsceneFace("Florentina", -1, 0)
    fnCutsceneWait(5)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("Florentina:[E|Neutral] Boom, regular ol' Florentina.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] You good in there?[P] Yeah he's fine.[P] In case I don't feel like walking around in my underwear.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Happy] That's really cool![P] It's like you can transform!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneFace("Florentina", -1, -1)
    fnCutsceneWait(5)
    fnCutsceneBlocker()
    fnCutsceneFace("Florentina", 0, -1)
    fnCutsceneWait(5)
    fnCutsceneBlocker()
    fnCutsceneFace("Florentina", 1, -1)
    fnCutsceneWait(5)
    fnCutsceneBlocker()
    fnCutscenePlaySound("World|TakeItem")
    fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Florentina/Job_Agarist.lua") ]])
    fnCutsceneBlocker()
    fnCutsceneFace("Florentina", 1, 0)
    fnCutsceneWait(5)
    fnCutsceneBlocker()
    fnCutsceneFace("Florentina", 1, 1)
    fnCutsceneWait(5)
    fnCutsceneBlocker()
    fnCutsceneFace("Florentina", 0, 1)
    fnCutsceneWait(5)
    fnCutsceneBlocker()
    fnCutsceneFace("Florentina", -1, 1)
    fnCutsceneWait(5)
    fnCutsceneBlocker()
    fnCutsceneFace("Florentina", -1, 0)
    fnCutsceneWait(5)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("Florentina:[E|Neutral] Now, a bit of breathing room.[P] Hey, I don't mind the extra strength.[P] And, I gotta say, having an on-demand disguise is going to become very handy.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] How come he doesn't make you like the...[P] other mushraunes?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] He said they were in some sort of horny cult, but their leader left after getting the grift in place.[P] Real messiah complex.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] The other mushraunes have true-believers parasitizing them, hence their behavior.[P] So just like Rochea and her gang but less reasonable.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Happy] He didn't like where it was going so they jailed him.[P] I can relate.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] What is it with this place and cults?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] It's recent, just like the other disturbances.[P] More bandits, human cultists, and now mushroom cultists?[P] I don't like where it's going.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] But, that's neither here nor there.[P] We need to keep working on getting you home. Let's go.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    -- |[Finish Up]|
    fnAutoFoldParty()
    fnCutsceneBlocker()
end
