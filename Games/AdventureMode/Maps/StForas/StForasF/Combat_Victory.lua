-- |[ ===================================== Combat Victory ===================================== ]|
--Party wins in combat.

-- |[Setup]|
--Flag.
VM_SetVar("Root/Variables/Chapter1/Sharelock/iDefeatedMycela", "N", 1.0)

--Darken the screen.
AL_SetProperty("Activate Fade", 30, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1)
AudioManager_PlayMusic("Null")

-- |[Dialogue]|
--Setup.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
fnCutscene([[ Append("Mei:[E|Neutral] I think she passed out.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Offended] Cut that -[P] thing -[P] off her back![B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Happy] Way ahead of you.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Offended] What are we going to do with her?[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Confused] ...[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Confused] I hate myself for saying this, but...") ]])
fnCutsceneBlocker()

--Wait a while.
fnCutsceneWait(30)
fnCutsceneBlocker()

-- |[Transition]|
--Scene resumes on the top floor of the beehive.
fnCutscene([[ AL_BeginTransitionTo("StForasA", "FORCEPOS:3.0x14.0x0") ]])
fnCutsceneBlocker()
