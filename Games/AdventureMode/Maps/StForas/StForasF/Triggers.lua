-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ====================================== Exec TF Scene ===================================== ]|
if(sObjectName == "MushrauneTf") then
    
    --TF Scene, Florentina into Mushraune.
    
    
-- |[ ===================================== See Mushraunes ===================================== ]|
elseif(sObjectName == "YouSeeingThis") then

    --Repeat check.
    local iSawMushraunes = VM_GetVar("Root/Variables/Chapter1/Sharelock/iSawMushraunes", "N")
    if(iSawMushraunes == 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter1/Sharelock/iSawMushraunes", "N", 1.0)
    
    --Camera movement.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 2.0)
        CameraEvent_SetProperty("Focus Position", (52.25 * gciSizePerTile), (20.50 * gciSizePerTile))
    DL_PopActiveObject()
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 2.0)
        CameraEvent_SetProperty("Focus Position", (64.25 * gciSizePerTile), (18.50 * gciSizePerTile))
    DL_PopActiveObject()
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 2.0)
        CameraEvent_SetProperty("Focus Position", (64.25 * gciSizePerTile), (11.50 * gciSizePerTile))
    DL_PopActiveObject()
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 2.0)
        CameraEvent_SetProperty("Focus Actor Name", "Mei")
    DL_PopActiveObject()
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("Mei:[E|Neutral] What's going on here, Florentina?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] It was my assumption that those mushraunes we saw were just a different species of monstergirl.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] But now that I'm seeing these mushrooms growing all over, I'm thinking not so.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Sad] It's like a parasite growing on them.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] I don't like where this is going.[P] Come on, let's see what we can dig up.") ]])
    fnCutsceneBlocker()
    
-- |[ ==================================== Encounter Mycela ==================================== ]|
elseif(sObjectName == "Mycela") then

    --Repeat check.
    local iDefeatedMycela = VM_GetVar("Root/Variables/Chapter1/Sharelock/iDefeatedMycela", "N")
    if(iDefeatedMycela == 1.0) then return end
    
    --Variables.
    local sMeiForm   = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
    local iMetMycela = VM_GetVar("Root/Variables/Chapter1/Sharelock/iMetMycela", "N")
    
    --Set flag.
    VM_SetVar("Root/Variables/Chapter1/Sharelock/iMetMycela", "N", 1.0)

    --First meeting.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 2.0)
        CameraEvent_SetProperty("Focus Actor Name", "Mycela")
    DL_PopActiveObject()
    fnCutsceneMergeParty()
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Mei", 13.25, 23.50)
    fnCutsceneFace("Mei", 0, -1)
    fnCutsceneMove("Florentina", 14.25, 23.50)
    fnCutsceneFace("Florentina", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Mycela", "Neutral") ]])
    
    --First meeting:
    if(iMetMycela == 0.0) then
        
        --Mei is an alraune:
        if(sMeiForm == "Alraune") then
            fnCutscene([[ Append("Alraune:[E|Neutral] Oh, hello there.[P] Have you come to be joined, leaf-sisters?[B][C]") ]])
        
        --Mei is anything else:
        else
            fnCutscene([[ Append("Alraune:[E|Neutral] Oh, hello there.[P] Have you come to be joined, leaf-sister?[B][C]") ]])
            fnCutscene([[ Append("Alraune:[E|Neutral] Your friend there, well, perhaps the little ones may still find her compatible.[B][C]") ]])
        end
        fnCutscene([[ Append("Mei:[E|Offended] So you're the leader of this little coven?[B][C]") ]])
        fnCutscene([[ Append("Alraune:[E|Neutral] I suppose so, if you define a coven as a group of alraunes.[P] Is that what they are called?[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Confused] Don't play dumb.[P] You've got the biggest mushroom on you.[P] You must be the first one.[B][C]") ]])
        fnCutscene([[ Append("Alraune:[E|Neutral] Heh, I suppose my little one is now a big one.[P] Yours will be as big as mine someday.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Offended] Not likely![B][C]") ]])
        fnCutscene([[ Append("Alraune:[E|Neutral] Why are you so upset?[P] You are free to refuse the offer, though I cannot see why.[B][C]") ]])
        fnCutscene([[ Append("Alraune:[E|Neutral] To live without the joy of conversing with the little ones, I cannot fathom.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Confused] Mushrooms aren't little ones.[P] They don't speak, the spirits of nature don't inhabit them.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Confused] What are you getting at?[P] What's your plan?[B][C]") ]])
        fnCutscene([[ Append("Alraune:[E|Neutral] Hmm?[P] Yes?[B][C]") ]])
        fnCutscene([[ Append("Alraune:[E|Neutral] But they're so polite.[B][C]") ]])
        fnCutscene([[ Append("Alraune:[E|Neutral] I see.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Offended] Who are you talking to?[B][C]") ]])
        fnCutscene([[ Append("Alraune:[E|Neutral] We alraunes can converse with mushrooms, you see.[P] You can too, if you accept them.[B][C]") ]])
        fnCutscene([[ Append("Alraune:[E|Neutral] Which you will.[P] They are telling me you cannot be allowed to leave without being joined.[B][C]") ]])
        fnCutscene([[ Append("Alraune:[E|Neutral] Please don't make this difficult.[P] We are a peaceful people by nature.") ]])
        fnCutsceneBlocker()
    
        --Boss battle.
        fnCutscene([[ AdvCombat_SetProperty("World Pulse", true) ]])
        fnCutscene([[ AdvCombat_SetProperty("Reinitialize") ]])
        fnCutscene([[ AdvCombat_SetProperty("Activate") ]])
        fnCutscene([[ AdvCombat_SetProperty("Unretreatable", true) ]])
        fnCutscene([[ AdvCombat_SetProperty("Victory Script", gsRoot .. "Maps/StForas/StForasF/Combat_Victory.lua") ]])
        fnCutscene([[ AdvCombat_SetProperty("Defeat Script", gsStandardGameOver) ]])
        fnCutscene([[ LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/000 Scripted Enemies/Boss Mycela.lua", 0) ]])
    
    --Repeats:
    else
        fnCutscene([[ Append("Alraune:[E|Neutral] Despite whatever trickery saved you, you return.[B][C]") ]])
        fnCutscene([[ Append("Alraune:[E|Neutral] Why do you struggle?[P] Join us, and all will become clear.") ]])
        fnCutsceneBlocker()
    
        --Boss battle.
        fnCutscene([[ AdvCombat_SetProperty("World Pulse", true) ]])
        fnCutscene([[ AdvCombat_SetProperty("Reinitialize") ]])
        fnCutscene([[ AdvCombat_SetProperty("Activate") ]])
        fnCutscene([[ AdvCombat_SetProperty("Unretreatable", true) ]])
        fnCutscene([[ AdvCombat_SetProperty("Victory Script", gsRoot .. "Maps/StForas/StForasF/Combat_Victory.lua") ]])
        fnCutscene([[ AdvCombat_SetProperty("Defeat Script", gsStandardGameOver) ]])
        fnCutscene([[ LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/000 Scripted Enemies/Boss Mycela.lua", 0) ]])

    end
    fnCutsceneBlocker()

end
