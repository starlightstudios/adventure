-- |[Standard Warp Handler]|
--99% of the time, this is the warp handler that the game uses. Most maps just need to reposition the actors slightly. This script expects
-- the X and Y position of the party to be passed in.

-- |[Argument Check]|
--Defaults.
local fWarpX = 0.0
local fWarpY = 0.0

--Verify.
if(fnArgCheck(2) == true) then
	fWarpX = LM_GetScriptArgument(0, "N")
	fWarpY = LM_GetScriptArgument(1, "N")
end

-- |[Execution]|
--Black the screen out instantly.
fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneBlocker()

-- |[Flashwhite]|
--Party leader flashes white.
Cutscene_CreateEvent("Flash Leader White", "Actor")
	ActorEvent_SetProperty("Subject Name", gsPartyLeaderName)
	ActorEvent_SetProperty("Flashwhite Quickly")
DL_PopActiveObject()
fnCutsceneFace(gsPartyLeaderName, 0, 1)

--Iterate across the party characters. Flashwhite for all of them. It's possible there are no followers.
for i = 1, giFollowersTotal, 1 do
	Cutscene_CreateEvent("Flash Follower White", "Actor")
		ActorEvent_SetProperty("Subject Name", gsaFollowerNames[i])
		ActorEvent_SetProperty("Flashwhite Quickly")
	DL_PopActiveObject()
	fnCutsceneFace(gsaFollowerNames[i], 0, 1)
end
fnCutsceneBlocker()

-- |[Positioning]|
--Position the party leader.
fnCutsceneTeleport(gsPartyLeaderName, fWarpX / gciSizePerTile, fWarpY / gciSizePerTile)

--Position the followers.
for i = 1, giFollowersTotal, 1 do
	fnCutsceneTeleport(gsaFollowerNames[i], fWarpX / gciSizePerTile, fWarpY / gciSizePerTile)
end
fnCutsceneBlocker()

-- |[Folding and Cleanup]|
--Fold the party.
if(giFollowersTotal > 0) then
	fnCutscene([[ AL_SetProperty("Fold Party") ]])
end
fnCutsceneWait(25)
fnCutsceneBlocker()

--Fade the screen in.
fnCutscene([[ AudioManager_PlaySound("World|Teleport") ]])
fnCutscene([[ AL_SetProperty("Activate Fade", 30, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
fnCutsceneBlocker()

fnCutsceneWait(gci_Flashwhite_Ticks_Total * 0.66)
fnCutsceneBlocker()
