-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Common Variables]|
local iOverlayState      = VM_GetVar("Root/Variables/Chapter1/Scenes/iOverlayState", "N")
local iOverlayPulseState = VM_GetVar("Root/Variables/Chapter1/Scenes/iOverlayPulseState", "N")

-- |[ ======================================== Triggers ======================================== ]|
--Overlay Clear. Removes the overlay if one is present.
if(sObjectName == "OverlayClear") then
	VM_SetVar("Root/Variables/Chapter1/Scenes/iOverlayState", "N", 0.0)
	if(iOverlayState == 1.0) then
		AL_SetProperty("Activate Fade", 15, gci_Fade_Under_GUI, false, gfaTrapDungeonMixers[1][1], gfaTrapDungeonMixers[1][2], gfaTrapDungeonMixers[1][3], 1, 1, 1, 1, 1, true)
	end
	
--Overlay pulse 0. Always sets the overlay, may also trigger a cutscene.
elseif(sObjectName == "OverlayPulse0") then
	
	VM_SetVar("Root/Variables/Chapter1/Scenes/iOverlayState", "N", 1.0)
	if(iOverlayState == 0.0) then
		AL_SetProperty("Activate Fade", 15, gci_Fade_Under_GUI, true, 1, 1, 1, 1, gfaTrapDungeonMixers[1][1], gfaTrapDungeonMixers[1][2], gfaTrapDungeonMixers[1][3], 1, true)
	elseif(iOverlayState == 2.0) then
		AL_SetProperty("Activate Fade", 15, gci_Fade_Under_GUI, true, gfaTrapDungeonMixers[2][1], gfaTrapDungeonMixers[2][2], gfaTrapDungeonMixers[2][3], 1, gfaTrapDungeonMixers[1][1], gfaTrapDungeonMixers[1][2], gfaTrapDungeonMixers[1][3], 1, true)
	end
	
	--Catch up variable.
	if(iOverlayPulseState < 1) then
		VM_SetVar("Root/Variables/Chapter1/Scenes/iOverlayPulseState", "N", 1)
	end
	
--Overlay pulse 1. Always sets the overlay, may also trigger a cutscene.
elseif(sObjectName == "OverlayPulse1") then
	
	--Determine mix and order the mixer change.
	local iMixTo = 2
	local iMixFrom = iOverlayState
	if(iOverlayState ~= iMixTo) then
		AL_SetProperty("Activate Fade", 15, gci_Fade_Under_GUI, true, gfaTrapDungeonMixers[iMixFrom][1], gfaTrapDungeonMixers[iMixFrom][2], gfaTrapDungeonMixers[iMixFrom][3], 1, gfaTrapDungeonMixers[iMixTo][1], gfaTrapDungeonMixers[iMixTo][2], gfaTrapDungeonMixers[iMixTo][3], 1, true)
	end
	
	--Flag.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iOverlayState", "N", 2.0)
	
	--Catch up variable. Also plays a cutscene.
	if(iOverlayPulseState < iMixTo) then
	
		--Flag.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iOverlayPulseState", "N", iMixTo)
		
		--Setup.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Sad") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
		
		--Speaking.
		fnCutscene([[ Append("Mei:[E|Sad] F-[P]Florentina?[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] Huh?[P] What?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] Did you hear that?[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] No.[P] Hear what?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] (..?[P] It was my runestone -[P] it...[P] pulsed?)[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] It's actually eerily quiet.[P] We must be getting close.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] Yeah.[P] Those cultists must be almost done the ritual!") ]])
		
	end
	
--Overlay pulse 2. Always sets the overlay, may also trigger a cutscene.
elseif(sObjectName == "OverlayPulse2") then
	
	--Determine mix and order the mixer change.
	local iMixTo = 3
	local iMixFrom = iOverlayState
	if(iOverlayState ~= iMixTo) then
		AL_SetProperty("Activate Fade", 15, gci_Fade_Under_GUI, true, gfaTrapDungeonMixers[iMixFrom][1], gfaTrapDungeonMixers[iMixFrom][2], gfaTrapDungeonMixers[iMixFrom][3], 1, gfaTrapDungeonMixers[iMixTo][1], gfaTrapDungeonMixers[iMixTo][2], gfaTrapDungeonMixers[iMixTo][3], 1, true)
	end
	
	--Flag.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iOverlayState", "N", 3.0)
	
	--Catch up variable.
	if(iOverlayPulseState < iMixTo) then
		VM_SetVar("Root/Variables/Chapter1/Scenes/iOverlayPulseState", "N", iMixTo)
	end
	
--Overlay pulse 2. Always sets the overlay, may also trigger a cutscene.
elseif(sObjectName == "OverlayPulse3") then
	
	--Determine mix and order the mixer change.
	local iMixTo = 4
	local iMixFrom = iOverlayState
	if(iOverlayState ~= iMixTo) then
		AL_SetProperty("Activate Fade", 15, gci_Fade_Under_GUI, true, gfaTrapDungeonMixers[iMixFrom][1], gfaTrapDungeonMixers[iMixFrom][2], gfaTrapDungeonMixers[iMixFrom][3], 1, gfaTrapDungeonMixers[iMixTo][1], gfaTrapDungeonMixers[iMixTo][2], gfaTrapDungeonMixers[iMixTo][3], 1, true)
	end
	
	--Flag.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iOverlayState", "N", 4.0)
	
	--Catch up variable.
	if(iOverlayPulseState < iMixTo) then
	
		--Flag.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iOverlayPulseState", "N", iMixTo)
		
		--Override the overlay.
		AL_SetProperty("Activate Fade", 15, gci_Fade_Under_GUI, true, 0, 0, 0, 1, gfaTrapDungeonMixers[iMixTo][1], gfaTrapDungeonMixers[iMixTo][2], gfaTrapDungeonMixers[iMixTo][3], 1, true)
		
		--Music stops.
		AL_SetProperty("Music", "Null")
		
		--Sound effect.
		fnCutscene([[ AudioManager_PlaySound("World|Heartbeat") ]])
		fnCutsceneWait(30)
		fnCutsceneBlocker()
		
		--Setup.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Sad") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
		
		--Speaking.
		fnCutscene([[ Append("Florentina:[E|Confused] Okay, I heard it that time.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] It sounded like a heartbeat...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] Just a heartbeat?[P] Not a voice...[P] saying...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] No, no voice.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] Saying...[P] saying...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] Saying what?[P] Mei?[P] Saying what!?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Cry] I don't know.[P] I know that I understood it, but I don't know what was said...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] I'll -[P] I'll be fine![P] We've got to stop them![B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Surprise] Yeah.[P] Stay focused.") ]])
	end
	
--Overlay pulse 2. Always sets the overlay, may also trigger a cutscene.
elseif(sObjectName == "OverlayPulse4") then
	
	--Determine mix and order the mixer change.
	local iMixTo = 5
	local iMixFrom = iOverlayState
	if(iOverlayState ~= iMixTo) then
		AL_SetProperty("Activate Fade", 15, gci_Fade_Under_GUI, true, gfaTrapDungeonMixers[iMixFrom][1], gfaTrapDungeonMixers[iMixFrom][2], gfaTrapDungeonMixers[iMixFrom][3], 1, gfaTrapDungeonMixers[iMixTo][1], gfaTrapDungeonMixers[iMixTo][2], gfaTrapDungeonMixers[iMixTo][3], 1, true)
	end
	
	--Flag.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iOverlayState", "N", 5.0)
	
	--Catch up variable.
	if(iOverlayPulseState < iMixTo) then
		VM_SetVar("Root/Variables/Chapter1/Scenes/iOverlayPulseState", "N", iMixTo)
	end
	
--Overlay pulse 2. Always sets the overlay, may also trigger a cutscene.
elseif(sObjectName == "OverlayPulse5") then
	
	--Determine mix and order the mixer change.
	local iMixTo = 6
	local iMixFrom = iOverlayState
	if(iOverlayState ~= iMixTo) then
		AL_SetProperty("Activate Fade", 15, gci_Fade_Under_GUI, true, gfaTrapDungeonMixers[iMixFrom][1], gfaTrapDungeonMixers[iMixFrom][2], gfaTrapDungeonMixers[iMixFrom][3], 1, gfaTrapDungeonMixers[iMixTo][1], gfaTrapDungeonMixers[iMixTo][2], gfaTrapDungeonMixers[iMixTo][3], 1, true)
	end
	
	--Flag.
	VM_SetVar("Root/Variables/Chapter1/Scenes/iOverlayState", "N", 6.0)
	
	--Catch up variable.
	if(iOverlayPulseState < iMixTo) then
	
		--Flag.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iOverlayPulseState", "N", iMixTo)
		
		--Override the overlay.
		AL_SetProperty("Activate Fade", 15, gci_Fade_Under_GUI, true, 0, 0, 0, 1, gfaTrapDungeonMixers[iMixTo][1], gfaTrapDungeonMixers[iMixTo][2], gfaTrapDungeonMixers[iMixTo][3], 1, true)
		
		--Sound effect.
		fnCutscene([[ AudioManager_PlaySound("World|Heartbeat") ]])
		fnCutsceneWait(30)
		fnCutsceneBlocker()
		
		--Setup.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
		
		--Speaking.
		fnCutscene([[ Append("Mei:[E|Neutral] (...?)[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] (...)[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] (...)[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] (...)[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] Mei?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] What?[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] I can smell something...[P] wrong...[P] up ahead.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] This might be it.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] I'm not afraid.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] Maybe we should be.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] 'Nobody can hurt me without my permission.'[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] Is that a quote or something?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] A wise man named Gandhi said that, once.[P] I'll show them how Earthers fight![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] (...)[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] (..?)[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] (...)") ]])
	end

end
