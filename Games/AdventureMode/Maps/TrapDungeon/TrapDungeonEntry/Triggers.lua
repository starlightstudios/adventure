-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
--Alraune cutscene.
if(sObjectName == "FlorentinaScene") then

	--If we haven't seen this scene, but the trap dungeon was completed, play the scene as the player exits.
	local iPostDungeonScene     = VM_GetVar("Root/Variables/Chapter1/Scenes/iPostDungeonScene", "N")
	local iCompletedTrapDungeon = VM_GetVar("Root/Variables/Chapter1/Scenes/iCompletedTrapDungeon", "N")
	if(iPostDungeonScene == 0.0 and iCompletedTrapDungeon == 1.0) then
	
		-- |[Flag]|
		VM_SetVar("Root/Variables/Chapter1/Scenes/iPostDungeonScene", "N", 1.0)
	
		-- |[Dialogue Sequence]|
		--Dialogue setup.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] All right, the little ones shouldn't be able to hear us.[P] Start talking.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] ?[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] You put up a pretty good front back there.[P] I bet Rochea even fell for it.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] Well I didn't.[P] What happened back there?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] Oh...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] Mei, I know you think I'm just in this for the money.[P] Actually, I still am.[P] I want the money.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] But that [P]*big*[P] thing?[P] That thing was wrong.[P] I've been to a lot of places. I have [P]*never*[P] seen anything like that.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] But when you looked at it, it was like you...[P] respected it.[P] Like you already knew what it was.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] [P].[P].[P].[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] I'm not trying to hurt you here.[P] Please.[P] Just, tell me.[P] Tell me what you know.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] The problem is that I don't know.[P] I can feel it, but I don't understand it.[P] I would need a whole new vocabulary to begin to describe it...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] Try.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] (Deep breaths, Mei...)[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] I felt something surge, and my runestone seemed to react.[P] It...[P] got stronger as we got closer to that thing.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] When I saw it, I just -[P] I just knew what I was supposed to do.[P] Instantly.[P] Like I had been born for that moment.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] Fantastic![P] That's the best news I've heard in weeks![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] This is probably a bad time for jokes.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] Tch, you're a real basket case aren't you?[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] Mei, when you're playing cards and you don't have the best hand, do you know what you do?[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] Act like you do, and make whoever [P]*does*[P] have the best hand think twice about it.[P] Make 'em wonder,[P] make 'em doubt.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] You put that doubt in them, you can make them fold.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] And this is a card game to you?[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] They distill out certain parts of life.[P] Ignore them at your peril.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] The point is, I didn't feel what you did when you saw that thing.[P] Why should I?[P] I'm just some misanthrope.[P] I'm not a threat.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] But you, and that runestone, scare them.[P] So, they're trying to scare you into not using it.[P] It's a bluff.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] The harder they bluff, the more you know you've got them beat.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] [P].[P].[P]. Wow.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Laugh] Florentina, you really are the best![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Happy] If anything, we're the most dangerous team on Pandemonium![P] They should be running from us![B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] See?[P] Despair is the real enemy.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Happy] Thanks, Florentina.[P] You really are a - [P][CLEAR]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] Business associate.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Yeah.[P] You're the best business associate I've ever had.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] Enough yakking.[P] Let's get to finding out how to get you back to Earth.") ]])
	end
end
