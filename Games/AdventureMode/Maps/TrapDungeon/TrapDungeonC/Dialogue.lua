-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
	--Set facing.
	TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[Cultist]|
    if(sActorName == "Cultist") then
        
        --Variables.
        local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
        
        --Flags.
        VM_SetVar("Root/Variables/Chapter1/Scenes/iMetSpecialAcolyte", "N", 1.0)
        
        -- |[Dialogue]|
        --Setup.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "CultistF", "Neutral") ]])

        --Talking.
        fnCutscene([[ Append("Cultist: ![B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Surprise] ![B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Surprise] ![B][C]") ]])
        fnCutscene([[ Append("Cultist: !!![B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Surprise] !!![B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Surprise] !!![B][C]") ]])
        fnCutscene([[ Append("Cultist: !!!!![B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Surprise] !!!!![B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Confused] Are we waiting for something?[B][C]") ]])
        fnCutscene([[ Append("Cultist: Thank the Great Swamp you came![B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] What...[B][C]") ]])
        fnCutscene([[ Append("Cultist: I must have hit the wrong switch![P] I've been stuck in here for hours, and nobody has come to let me out![B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] Well, we - [P][CLEAR]") ]])
        fnCutscene([[ Append("Cultist: That's twice you've helped me out, Initiate Tess.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Surprise] Tess?[P][CLEAR]") ]])
        fnCutscene([[ Append("Mei:[E|Happy] Oh yeah![P] That's...[P] me![B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Confused] What the hey...[B][C]") ]])
        fnCutscene([[ Append("Cultist: Well I see your secret mission is going well.[P] You've already captured an Alraune![P] Great work![B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Laugh] Oh yeah, totally.[P] This is my prisoner.[P] Right, prisoner?[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Confused] Yeah.[P] Sure.[P] That.[B][C]") ]])
        if(sMeiForm == "Alraune") then
            fnCutscene([[ Append("Cultist: So is that like a costume or something?[P] It's very realistic![B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Laugh] It's a disguise.[P] I am a master of disguise.[B][C]") ]])
            fnCutscene([[ Append("Cultist: Is that how you captured the alraune?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] Nope, that was with a good old beartrap.[B][C]") ]])
            fnCutscene([[ Append("Cultist: You can build traps, too?[P] What can't you do?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] If you put your mind to it, you can accomplish anything.[B][C]") ]])
        elseif(sMeiForm == "Slime") then
            fnCutscene([[ Append("Cultist: So did you disguise yourself as a slime and infiltrate the trading post?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Laugh] Well they didn't see through it, but you did.[P] You're super observant![B][C]") ]])
        elseif(sMeiForm == "Bee") then
            fnCutscene([[ Append("Cultist: Where did you get that cool bee disguise?[P] The wings even move and everything![B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Laugh] I can't give away all my secrets![B][C]") ]])
        elseif(sMeiForm == "Werecat") then
            fnCutscene([[ Append("Cultist: So that's a werecat costume, right?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] No, I actually am a werecat.[B][C]") ]])
            fnCutscene([[ Append("Cultist: That's some next-level infiltration stuff![P] I would never have thought of it![B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Laugh] The trick is to not act suspiciously.[B][C]") ]])
        elseif(sMeiForm == "Ghost") then
            fnCutscene([[ Append("Cultist: I almost didn't recognize you under the ghost costume.[P] That's a costume, right?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Laugh] Absolutely.[P] Please don't ask how I'm floating...[B][C]") ]])
        elseif(sMeiForm == "Gravemarker") then
            fnCutscene([[ Append("Cultist: So did you get some body paint or something?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Laugh] Oh yeah, it's very realistic! Let me tell you, it took hours to get my hair to look like this.[B][C]") ]])
            fnCutscene([[ Append("Cultist: And the wings?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Offended] Oh no you don't, I'm not giving away all my secrets![B][C]") ]])
            fnCutscene([[ Append("Cultist: Oh come on![B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Happy] Look, if I told you, I wouldn't be the number-one spy, now would I?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] Let's just say you won't find these just anywhere.[B][C]") ]])
            fnCutscene([[ Append("Cultist: And you feel...[P] so firm![B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] Rock hard muscles take years of dedication, you know.[B][C]") ]])
            fnCutscene([[ Append("Cultist: Wow, you're so cool, Tess![B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Happy] Maybe someday I'll teach you and you can be as cool as me![B][C]") ]])
        elseif(sMeiForm == "Wisphag") then
            fnCutscene([[ Append("Cultist: Did you get some lights or - oh did you swallow a candle? Is that how your disguise works?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Happy] It's magic, you big goof. I made the hands from clay. Pretty cool, right?[B][C]") ]])
            fnCutscene([[ Append("Cultist: Nobody will suspect a thing. You're so awesome, Tess![B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Laugh] Oh, you're flattering me.[P] How have you been?[B][C]") ]])
        elseif(sMeiForm == "Mannequin") then
            fnCutscene([[ Append("Cultist: I take it the disguise is to just absolutely smother yourself in butter.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] No, that'd ruin your skin. Use vaseline.[B][C]") ]])
            fnCutscene([[ Append("Cultist: And the arms?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] My real arms are tied behind my back and I have a set of strings rigged to these ones.[B][C]") ]])
            fnCutscene([[ Append("Cultist: Wow, that looks so realistic, but also very complicated.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] I take my spycraft very seriously.[B][C]") ]])
        end
        fnCutscene([[ Append("Cultist: Awesome![P] I got a bit of a promotion, too.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] Hey, since I'm totally a cultist, uh...[P] can you tell me what ritual is going on in here?[B][C]") ]])
        fnCutscene([[ Append("Cultist: I dunno, it's a bit above my pay grade.[P] I heard it's big, though.[P] We're supposed to be getting pure ones to help us against the unbelievers.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] I totally know what a pure one is, but if you could just...[P] inform my prisoner here?[B][C]") ]])
        fnCutscene([[ Append("Cultist: They are beings wrought from the Holy Peat.[P] Instruments of His will![B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Offended] *This sounds really bad!*[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Happy] *The better that we stop them now, then.*[B][C]") ]])
        fnCutscene([[ Append("Cultist: Now if you'll excuse me...[P] There's not exactly a lavatory down here...[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] One more thing...[P] How exactly do we get through this maze?[B][C]") ]])
        fnCutscene([[ Append("Cultist: Oh, you need two people.[P] You have to use the eastern switch like it's a floodgate.[P] The switch on the other side will let the other person through.[P] Go give it a try![B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] Oh, okay![P] Great![B][C]") ]])
        fnCutscene([[ Append("Cultist: I tell you, Tess.[P] You and me?[P] We are going places![P] See you later!") ]])
        fnCutsceneBlocker()
        
        -- |[Movement]|
        --Cultist walks off to the right.
        Cutscene_CreateEvent("ActorEvent", "Actor")
            ActorEvent_SetProperty("Subject Name", "Cultist")
            ActorEvent_SetProperty("Move To", (8.25 * gciSizePerTile), (8.50 * gciSizePerTile))
        DL_PopActiveObject()
        fnCutsceneBlocker()
        
        --Cultist keeps walking. Mei and Florentina turn.
        Cutscene_CreateEvent("ActorEvent", "Actor")
            ActorEvent_SetProperty("Subject Name", "Mei")
            ActorEvent_SetProperty("Face", 1, 0)
        DL_PopActiveObject()
        Cutscene_CreateEvent("ActorEvent", "Actor")
            ActorEvent_SetProperty("Subject Name", "Florentina")
            ActorEvent_SetProperty("Face", 1, 0)
        DL_PopActiveObject()
        Cutscene_CreateEvent("ActorEvent", "Actor")
            ActorEvent_SetProperty("Subject Name", "Cultist")
            ActorEvent_SetProperty("Move To", (14.25 * gciSizePerTile), (8.50 * gciSizePerTile))
        DL_PopActiveObject()
        fnCutsceneBlocker()
        
        --Cultist walks south.
        Cutscene_CreateEvent("ActorEvent", "Actor")
            ActorEvent_SetProperty("Subject Name", "Cultist")
            ActorEvent_SetProperty("Move To", (14.25 * gciSizePerTile), (12.50 * gciSizePerTile))
        DL_PopActiveObject()
        Cutscene_CreateEvent("ActorEvent", "Actor")
            ActorEvent_SetProperty("Subject Name", "Cultist")
            ActorEvent_SetProperty("Move To", (19.25 * gciSizePerTile), (12.50 * gciSizePerTile))
        DL_PopActiveObject()
        Cutscene_CreateEvent("ActorEvent", "Actor")
            ActorEvent_SetProperty("Subject Name", "Cultist")
            ActorEvent_SetProperty("Move To", (19.25 * gciSizePerTile), (20.50 * gciSizePerTile))
        DL_PopActiveObject()
        fnCutsceneBlocker()
        
        --Cultist despawns.
        Cutscene_CreateEvent("ActorEvent", "Actor")
            ActorEvent_SetProperty("Subject Name", "Cultist")
            ActorEvent_SetProperty("Teleport To", (-100 * gciSizePerTile), (-100 * gciSizePerTile))
        DL_PopActiveObject()
        fnCutsceneBlocker()
        
        -- |[Dialogue]|
        --Setup.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])

        --Talking.
        fnCutscene([[ Append("Florentina:[E|Neutral] So are you going to explain yourself?[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Happy] Those guys are reaaaally stupid.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] I may have tricked them into thinking I was one of them, and then escaping.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Blush] Oh, really?[P] I like that...[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] Okay, she said to use the eastern switch like it's a floodgate?[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Happy] I think I know which one she was talking about.[P] Let's go take a look.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] (How could she have hit the wrong switch?[P] There are no switches in this room...)[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Happy] (What an idiot!)") ]])
        fnCutsceneBlocker()
    end
end
