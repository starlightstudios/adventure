-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Examinations]|
--This door can't be opened if the dungeon is considered complete.
if(sObjectName == "ToDungeonE") then

	--Variables.
	local iCompletedTrapDungeon = VM_GetVar("Root/Variables/Chapter1/Scenes/iCompletedTrapDungeon", "N")

	--Dungeon not complete, open the door.
	if(iCompletedTrapDungeon == 0.0) then
		AudioManager_PlaySound("World|FlipSwitch")
		AL_BeginTransitionTo("TrapDungeonE", "Null")
	
	--Can't open the door.
	else
		fnStandardDialogue("(There's a lot of rubble past here...)")
	end
	
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
