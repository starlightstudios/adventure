-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    --Common.
	TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[Alraune A]|
    if(sActorName == "AlrauneA") then
        fnStandardDialogue("Alraune:[VOICE|Alraune] Nature will not sit idle as these humans defile it!")
        
    -- |[Alraune B]|
    elseif(sActorName == "AlrauneB") then
        fnStandardDialogue("Alraune:[VOICE|Alraune] We are prepared to give everything for the good of all.")
        
    -- |[Alraune C]|
    elseif(sActorName == "AlrauneC") then
        fnStandardDialogue("Alraune:[VOICE|Alraune] With my leaf-sisters at my side, I have no fear.")
    
    -- |[Alraune D]|
    elseif(sActorName == "AlrauneD") then
        fnStandardDialogue("Alraune:[VOICE|Alraune] Must violence always be the solution?")
    
    -- |[Alraune F and G]|
    elseif(sActorName == "AlrauneF" or sActorName == "AlrauneG") then
	
        --Variables.
        local iCompletedTrapDungeon = VM_GetVar("Root/Variables/Chapter1/Scenes/iCompletedTrapDungeon", "N")
        
        --If the player has not completed the dungeon:
        if(iCompletedTrapDungeon == 0.0) then
            fnStandardDialogue("Alraune:[VOICE|Alraune] We will prevent the cultists from pincering you.[P] Please, leaf-sister, hurry!")
        
        --Line changes after completing the dungeon.
        else
            fnStandardDialogue("Alraune:[VOICE|Alraune] The defilers have felt this blow, but they will rally, yet...")
        end
    
    -- |[Rochea]|
    elseif(sActorName == "Rochea") then
        fnStandardMajorDialogue()
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Rochea", "Neutral") ]])
        fnCutscene([[ Append("Rochea: We have cleared the garrison here, but more remain further inside.[B][C]") ]])
        fnCutscene([[ Append("Rochea: Worse, it seems they have erected some sort of mechanical blockade.[P] We cannot pass it.[B][C]") ]])
        fnCutscene([[ Append("Rochea: I just wanted to tell you both, good luck.[P] We're all counting on you.") ]])
    
    end
end
