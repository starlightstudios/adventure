-- |[Constructor]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "TrapDungeonB"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = gci_Constructor_Start
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[Standard]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
	AL_SetProperty("Music", "TheyKnowWeAreHere")
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then
		fnStandardCharacter() 
	end
	
	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
    
    -- |[Map Handling]|
    --Resolve the map's variable.
    fnStandardCh1Map(sLevelName)

-- |[Post-Exec]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	--Variables.
	local iCompletedTrapDungeon = VM_GetVar("Root/Variables/Chapter1/Scenes/iCompletedTrapDungeon", "N")
	
	--Spawn everyone if the dungeon is not completed yet.
	if(iCompletedTrapDungeon == 0.0) then
	
		--Spawn some Alraunes and downed Cultists.
        fnStandardNPCByPosition("CultistA")
        fnStandardNPCByPosition("CultistB")
        fnStandardNPCByPosition("AlrauneA")
        fnStandardNPCByPosition("AlrauneB")
        fnStandardNPCByPosition("AlrauneC")
        fnStandardNPCByPosition("AlrauneD")
        fnStandardNPCByPosition("Rochea")
        
        --Mark the cultists as wounded.
        EM_PushEntity("CultistA")
			TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/CultistF|Wounded")
			TA_SetProperty("Set Special Frame", "Wounded")
        DL_PopActiveObject()
        EM_PushEntity("CultistB")
			TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/CultistF|Wounded")
			TA_SetProperty("Set Special Frame", "Wounded")
        DL_PopActiveObject()
	end
    
    --Set this flag to mark the Dungeon A campfire as the last used campfire. If the player is defeated they warp back to it,
    -- which can prevent a bug if they defeat Infirm and then warp outside the dungeon without seeing a scene with Florentina.
    AL_SetProperty("Last Save Point", "TrapDungeonA")

end
