-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Examinations]|
--Door covered in vines.
if(sObjectName == "VineDoor") then
	
	--Note: It's not possible to get here without Florentina being present.
	local sMeiForm             = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	local iMeiUnlockedVineDoor = VM_GetVar("Root/Variables/Chapter1/Scenes/iMeiUnlockedVineDoor", "N")
	
	--Always trip this flag:
	VM_SetVar("Root/Variables/Chapter1/Scenes/iInformedOfDungeon", "N", 1.0)
	
	--If Mei is not an Alraune:
	if(sMeiForm ~= "Alraune") then
		
		--Set this flag.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iMeiUnlockedVineDoor", "N", 2.0)
	
		--Setup.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
		
		--Dialogue.
		fnCutscene([[ Append("Mei:[E|Offended] Hm, this door is covered in vines.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] I can't get it open![B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] Give me a minute.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] ...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Surprise] How rude![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] I didn't say anything -[P] did I?[P] I'm sorry...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] Wha-[P] no, the vine.[P] He's insulting me...[P] I think.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] You can -[P] oh right, talking to plants.[P] I forgot.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] I've half a mind to cut you up, vine![P] What do you think of that?[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] ...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Surprise] I can't understand anything he's saying.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] But I thought - [P][CLEAR]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] His accent is really thick.[P] I don't think he can understand me, either.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] So what do we do?[P] Cut our way through?[P][CLEAR]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] This vine is here for a reason, and it looks like it's enchanted.[P] It might go badly for us if we cut it up.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Enchanted?[P][CLEAR]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] There's Alraune magic afoot.[P] And not mine, I mean.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] Maybe if we had someone who could understand this nightshade...") ]])
		fnCutsceneBlocker()

	--If Mei is an Alraune, and has not inspected the door before:
	elseif(iMeiUnlockedVineDoor == 0.0) then
	
		--Flag.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iMeiUnlockedVineDoor", "N", 1.0)
	
		--Setup.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
		
		--Dialogue.
		fnCutscene([[ Append("Mei:[E|Neutral] Hm, this door is covered in vines.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] I can't get it open![B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] Give me a minute.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] ...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Surprise] How rude![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] What?[P] He didn't say anything mean...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] You can understand that thick accent?[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] Huh...[P] I think this vine is a nightshade, like you.[P] Maybe.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] What's that, little guy?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] ...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Happy] Yeah![P] I guess we are![P] That's so cool.[P] Maybe I have some of your pollen in me, little one![B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] Yeah yeah, skip the family reunion.[P] Can you get us through?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] ...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Apparently the other leaf-sisters asked him to bar this door.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Laugh] But I think he's willing to make an exception for us.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Laugh] Yeah, thanks![P] Sure, sure.[P] I'll tell her.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] What?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Happy] He says you're very pretty.[P] I think he likes you.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Blush] Well I do like a vine with an accent...") ]])
		fnCutsceneBlocker()

		--Remove the door.
		fnCutscene([[ AudioManager_PlaySound("World|FlipSwitch") ]])
		fnCutscene([[ AL_RemoveObject("Door", "VineDoor") ]])
		fnCutsceneBlocker()


	--If Mei is an Alraune, and has inspected the door before:
	else
	
		--Flag.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iMeiUnlockedVineDoor", "N", 1.0)
	
		--Setup.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
		
		--Dialogue.
		fnCutscene([[ Append("Mei:[E|Neutral] Hello, little one![B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] Back to the gibberish-talking vine, I see.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] ...?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Happy] Oh really?[P] *Oh really*?[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] You can understand him?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Yep![P] Perfectly clearly![P] Apparently we're a similar species of nightshade![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Happy] So we're, sorta,[P] like,[P] cousins?[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] Yeah whatever.[P] Can you get us through the door or not?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] ...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Hmm, he said the other Alraunes asked him to block off this door, and gave him a magical boost.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] But he'll make an exception because...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] Because...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Happy] He says you're very pretty.[P] I think he likes you.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Blush] Oh, well.[P] Uh.[P] Okay.[P] Onward!") ]])
		fnCutsceneBlocker()

		--Remove the door.
		fnCutscene([[ AudioManager_PlaySound("World|FlipSwitch") ]])
		fnCutscene([[ AL_RemoveObject("Door", "VineDoor") ]])
		fnCutsceneBlocker()
	
	end

--Door that leads further into cultist territory.
elseif(sObjectName == "Unenterable") then
	fnStandardDialogue("(Looks like the Alraunes blocked this door to keep the cultists from getting through...)")

--Tunnel dug by the Alraunes.
elseif(sObjectName == "AlrauneTunnel") then
	fnStandardDialogue("(Seems this is where the Alraunes broke through.[P] Not much of interest in there.)")
	
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
