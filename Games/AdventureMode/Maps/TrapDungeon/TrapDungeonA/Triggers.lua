-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
if(sObjectName == "WarpActivate") then
    fnWarpUnlockHandler("Root/Variables/Chapter1/Campfires/iTrapDungeonA", 8.25, 24.50)
    
--Alraune cutscene.
elseif(sObjectName == "AlrauneScene") then

	--If the scene has already played, stop here.
	local iSeenAlrauneBattleIntroScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iSeenAlrauneBattleIntroScene", "N")
	if(iSeenAlrauneBattleIntroScene == 1.0) then return end
	
	--Execute the scene.
	LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/TrapDungeon_AlrauneBattleScene/Scene_Begin.lua")

end
