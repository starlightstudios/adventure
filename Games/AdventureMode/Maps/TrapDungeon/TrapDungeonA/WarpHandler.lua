-- |[Warp Handler]|
--If the player warps here using the campfire, this script is called to reposition them as necessary.

--Constants. This is the important part that changes between each room.
local fTargetX = ( 8.25 * gciSizePerTile)
local fTargetY = (23.50 * gciSizePerTile)

--Common function.
fnCommonCh1Warp(fnResolveDirectory())

--Execute.
LM_ExecuteScript(gsStandardWarpHandler, fTargetX, fTargetY)
