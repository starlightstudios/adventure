-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
if(sObjectName == "WarpActivate") then
    fnWarpUnlockHandler("Root/Variables/Chapter1/Campfires/iWildsTowerA", 40.25, 31.50)

elseif(sObjectName == "RubberStart") then

    --Activates rubber mode.
    VM_SetVar("Root/Variables/Chapter1/Scenes/Rubber|iIsRubberMode", "N", 1.0)
    
    --Execute a rest action.
    fnCutscene([[ AM_SetProperty("Execute Rest") ]])

    --Remove Florentina's field abilities.
    for i = 0, gciFieldAbility_Slots-1, 1 do
        local sAbilityName = AdvCombat_GetProperty("Field Ability Name", i)
        if(sAbilityName == "Pick Lock") then
            AdvCombat_SetProperty("Set Field Ability", i, "Null")
        end
    end
    
    --Switch Mei's form to rubber.
    LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Rubber.lua")
    
    --Black the screen out.
    fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Position.
    fnCutsceneTeleport("Mei", 40.25, 25.50)
    fnCutsceneFace("Mei", 0, 1)
    if(EM_Exists("Florentina") == true) then
        fnRemovePartyMember("Florentina", true)
    end
    
    --Fade in.
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Must...[P] spread...[P] want to...[P] spread...[P] spread...)") ]])
    fnCutsceneBlocker()

--Ends rubber mode.
elseif(sObjectName == "Unrubber") then

    --Activates rubber mode.
    VM_SetVar("Root/Variables/Chapter1/Scenes/Rubber|iIsRubberMode", "N", 0.0)
    VM_SetVar("Root/Variables/Chapter1/Scenes/iRubberQuestState", "N", 10.0)
    
    --Execute a rest action.
    fnCutscene([[ AM_SetProperty("Execute Rest") ]])
    
    --Switch Mei's form to human.
    LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Human.lua")
    
    --Remove the rubber thralls if they exist.
    local iHasThralls = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iHasThralls", "N")
    if(iHasThralls == 1.0) then
        fnCutsceneTeleport("Rubberraune", -1.25, -1.50)
        fnCutsceneTeleport("Rubberbee", -1.25, -1.50)
        AL_SetProperty("Unfollow Actor Name", "Rubberraune")
        AL_SetProperty("Unfollow Actor Name", "Rubberbee")
        giFollowersTotal = 0
        gsaFollowerNames = {}
        giaFollowerIDs = {}
    end
    
    --Black the screen out.
    fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    fnCutsceneTeleport("Mei", 40.25, 30.50)
    fnCutsceneFace("Mei", 0, 1)
    fnCutsceneSetFrame("Mei", "Wounded")
    
    --Spawn Florentina.
    if(EM_Exists("Florentina") == false) then
        fnAddPartyMember("Florentina")
    end
    fnCutsceneTeleport("Florentina", 41.25, 30.50)
    fnCutsceneFace("Florentina", -1, 0)
    fnCutsceneWait(45)
    fnCutsceneBlocker()

    --Narration.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Narrator:[VOICE|Narrator] (That night, a powerful rainstorm broke over Trannadar...)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(125)
    fnCutsceneBlocker()

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Florentina:[VOICE|Florentina] Hey![P] Wake up, kid![P] Come on, wake up!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Fade in.
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneSetFrame("Mei", "Crouch")
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    fnCutsceneSetFrame("Mei", "Null")
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Mei", 1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("Mei:[E|Neutral] Florentina?[P] What happened?[P] Where are we?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] Dormine tower.[P] You gave me the slip there, but the little ones told me you were here, so I doubled back.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] Are you going to tell me why you ran off like that?[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Sad] Ran off?[P] What are you talking about?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] We got to the top of the tower and you just left without telling me.[P] Not a good idea.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Sad] I...[P] don't remember anything.[P] I -[P] we crossed some logs to get across the river, and it's all black after that.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] You're serious?[P] You don't remember anything?[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Sad] I'm sorry...[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] Huh, odd.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] I'd guess your runestone warped you back here.[P] Maybe you took a hit to the head.[P] You look fine to me, though.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Smirk] I feel fine.[P] In fact, I feel pretty good![B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] But...[P] what happened to me?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] Just don't vanish without telling me, I was chasing down leads all across the forest.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] The little ones are eager, but they think anyone with black hair and a grey outfit is you.[P] They're not exactly reliable.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] Okay.[P] So, on with the adventure?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Happy] Let's get back to it!") ]])
    fnCutsceneBlocker()
    fnAutoFoldParty()
    fnCutsceneBlocker()

--No going back into the tower.
elseif(sObjectName == "NoExit") then
    local iIsRubberMode = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iIsRubberMode", "N")
    if(iIsRubberMode == 0.0) then return end
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Not...[P] this...[P] way...)") ]])
    fnCutsceneBlocker()
    Cutscene_CreateEvent("ActorEvent", "Actor")
        ActorEvent_SetProperty("Subject Name", "Mei")
        ActorEvent_SetProperty("Move Amount", (0.00 * gciSizePerTile), (1.00 * gciSizePerTile))
    DL_PopActiveObject()
    
end
