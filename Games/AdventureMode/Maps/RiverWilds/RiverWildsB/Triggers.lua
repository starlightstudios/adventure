-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
if(sObjectName == "RubberTrigger") then
    
    --Repeat check.
    local iIsRubberMode = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iIsRubberMode", "N")
    if(iIsRubberMode == 0.0) then return end
    
    --Fade.
    fnCutscene([[ AL_SetProperty("Activate Fade", 0, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Position entities.
    fnCutsceneTeleport("Mei", 64.25, 14.50)
	fnSpecialCharacter("Florentina", 65, 66, gci_Face_East, false, nil)
    TA_Create("Merc")
        TA_SetProperty("Position", 66, 66)
        TA_SetProperty("Facing", gci_Face_West)
        fnSetCharacterGraphics("Root/Images/Sprites/MercF/", false)
    DL_PopActiveObject()
    
    --Camera focus.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Focus Actor Name", "Florentina")
    DL_PopActiveObject()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Fade in. Maintain a sundown overlay.
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 1, gcf_Evening_R, gcf_Evening_G, gcf_Evening_B, gcf_Evening_A, true) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Florentina:[VOICE|Florentina] You're sure it was her?[P] Long black hair, grey outfit?[B][C]") ]])
    fnCutscene([[ Append("Merc:[VOICE|MercF] Yeah, she was going towards the trading post last I saw.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[VOICE|Florentina] Splendid.[P] Here's a platina for your trouble.") ]])
    fnCutsceneBlocker()
    
    --Movement.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Focus Actor Name", "Mei")
    DL_PopActiveObject()
    fnCutsceneMove("Florentina", 65.25, 74.50)
    fnCutsceneMove("Merc", 66.25, 74.50)
    fnCutsceneBlocker()
    fnCutsceneTeleport("Florentina", -1.00, -1.00)
    fnCutsceneTeleport("Merc", -1.00, -1.00)
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Must...[P] spread...[P] to...[P] all...)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "NoExit") then
    
    --Repeat check.
    local iIsRubberMode = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iIsRubberMode", "N")
    if(iIsRubberMode == 0.0) then return end
    
    --No going back.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Hosts...[P] south...[P] spread...[P] to...[P] all...)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    Cutscene_CreateEvent("ActorEvent", "Actor")
        ActorEvent_SetProperty("Subject Name", "Mei")
        ActorEvent_SetProperty("Move Amount", (0.00 * gciSizePerTile), (1.00 * gciSizePerTile))
    DL_PopActiveObject()
    fnCutsceneBlocker()

-- |[Rubber Exit]|
--Special exit handler.
elseif(sObjectName == "RubberExit") then

    --Error check.
    local iIsRubberMode = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iIsRubberMode", "N")
    if(iIsRubberMode == 0.0) then return end
    
    --Change to a special instance of the river village.
    VM_SetVar("Root/Variables/Chapter1/Scenes/Rubber|iInitializedMihoEvent", "N", 0.0)
    fnCutscene([[ AL_BeginTransitionTo("FishingVillageRubber", "FORCEPOS:46.0x2.0x0") ]])
end
