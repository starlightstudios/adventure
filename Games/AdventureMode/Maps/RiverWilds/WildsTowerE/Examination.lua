-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= Exits ========================================== ]|
if(sObjectName == "SampleExit") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("LevelName", "FORCEPOS:28.0x16.0x0")
    
-- |[ ====================================== Examinables ======================================= ]|
elseif(sObjectName == "PrimalRubber") then

    --Variables.
    local iPolarisGavePhial  = VM_GetVar("Root/Variables/Chapter1/Scenes/iPolarisGavePhial", "N")
    local iFilledPhial       = VM_GetVar("Root/Variables/Chapter1/Scenes/iFilledPhial", "N")
    local iReturnedPhial     = VM_GetVar("Root/Variables/Chapter1/Scenes/iReturnedPhial", "N")
    
    --No phial.
    if(iPolarisGavePhial == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (This looks like the source of the rubber.[P] It's uncannily smooth.)") ]])
        fnCutsceneBlocker()
    
    --Have phial, haven't filled it.
    elseif(iPolarisGavePhial == 1.0 and iFilledPhial == 0.0) then
    
        --Flag. Remove/Add items.
        VM_SetVar("Root/Variables/Chapter1/Scenes/iFilledPhial", "N", 1.0)
        LM_ExecuteScript(gsItemListing, "Vacuum Phial With Rubber")
        AdInv_SetProperty("Remove Item", "Empty Vacuum Phial", 1)
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (This looks like the source of the rubber.[P] It's uncannily smooth.)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ([SOUND|Rubber|ChangeA]All right, I filled the phial up with the rubber stuff. I hope this is the sample Polaris needs.)") ]])
        fnCutsceneBlocker()
        
        --Reset topic.
        WD_SetProperty("Clear Topic Read", "Rubberines")
    
    --Have phial, filled it, haven't returned it to Polaris.
    elseif(iPolarisGavePhial == 1.0 and iFilledPhial == 1.0 and iReturnedPhial == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (This looks like the source of the rubber.[P] It's uncannily smooth.[P] I took a sample of it, I need to get that back to Polaris.)") ]])
        fnCutsceneBlocker()
    
    --Have phial, filled it, have returned it.
    elseif(iPolarisGavePhial == 1.0 and iFilledPhial == 1.0 and iReturnedPhial == 1.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (This looks like the source of the rubber.[P] It's uncannily smooth.[P] I hope Polaris figures out what this stuff is and how to stop it.)") ]])
        fnCutsceneBlocker()
    end

-- |[ ========================================== Other ========================================= ]|
-- |[ ========================================== Debug ========================================= ]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
