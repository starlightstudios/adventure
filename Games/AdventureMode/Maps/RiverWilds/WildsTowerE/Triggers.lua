-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
if(sObjectName == "RubberSequence") then

    -- |[Repeat Check]|
    local iSawTowerIntro = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iSawTowerIntro", "N")
    if(iSawTowerIntro == 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter1/Scenes/Rubber|iSawTowerIntro", "N", 1.0)
    
    --Variables.
    local iPolarisGavePhial = VM_GetVar("Root/Variables/Chapter1/Scenes/iPolarisGavePhial", "N")
    
    --Form check. If Mei is not human, nothing happens.
    local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
    if(sMeiForm ~= "Human") then
        return
    end
    
    --Rubber quest increments if below this value.
    local iRubberQuestState = VM_GetVar("Root/Variables/Chapter1/Scenes/iRubberQuestState", "N")
    if(iRubberQuestState < 3.0 or iRubberQuestState == 10.0) then
        VM_SetVar("Root/Variables/Chapter1/Scenes/iRubberQuestState", "N", 3.0)
    end
    
    --Give Florentina a landing sound.
    EM_PushEntity("Florentina")
        TA_SetProperty("Ignores Gravity", false)
        TA_SetProperty("Landing Sound", "FieldAbi|PounceImpact")
    DL_PopActiveObject()
    
    --Play the scene out.
    fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    
    --Camera focus point.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 5.0)
        CameraEvent_SetProperty("Focus Position", (22.50 * gciSizePerTile), (23.50 * gciSizePerTile))
    DL_PopActiveObject()
    
    --Activate layer that shows a 'fake' Mei.
    fnCutsceneLayerDisabled("RubberFrame00", false)
        
    --Move Florentina and Mei.
    fnCutsceneTeleport("Mei", -1.25, -1.50)
    fnCutsceneTeleport("Florentina", 25.25, 19.50)
    fnCutsceneFace("Florentina", -1, 0)
    fnCutsceneBlocker()
    
    --Fade in.
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("Florentina:[E|Neutral] Nothing here, Mei.[P] No big monsters, no evidence of anything, just more of this inert rubbery crap.[B][C]") ]])
    if(iPolarisGavePhial == 0.0) then
        fnCutscene([[ Append("Mei:[E|Neutral] But this is the source of the stuff.[P] What are we missing?[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] Are we sure it's the source?[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] It seems to be spreading out from here, and the floor is unusually smooth...") ]])
    else
        fnCutscene([[ Append("Mei:[E|Neutral] This must be the source, right?[P] Should we get a sample?[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] I'd figure the source would be more...[P] animated.[P] Maybe if I...") ]])
    end
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Florentina", 22.75, 19.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Florentina", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("FieldAbi|Pounce") ]])
    fnCutsceneJumpNPC("Florentina", gcfStdJumpSpeed)
    fnCutsceneBlocker()
    fnCutsceneWait(35)
    fnCutsceneBlocker()
    fnCutsceneFace("Florentina", -1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("FieldAbi|Pounce") ]])
    fnCutsceneJumpNPC("Florentina", gcfStdJumpSpeed)
    fnCutsceneBlocker()
    fnCutsceneWait(35)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("Florentina:[E|Neutral] Nothing![B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Surprise] Stop jumping on it![B][C]") ]])
    if(iPolarisGavePhial == 0.0) then
        fnCutscene([[ Append("Florentina:[E|Neutral] Look, if you want to keep investigating, go ahead.[P] I'll be outside looking for other clues.[P] Don't take too long.") ]])
    else
        fnCutscene([[ Append("Florentina:[E|Neutral] Just get that sample thing Polaris wanted.[P] I'll cover you, don't take too long.") ]])
    end
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 1.0)
        CameraEvent_SetProperty("Focus Position", (22.50 * gciSizePerTile), (20.50 * gciSizePerTile))
    DL_PopActiveObject()
    
    --Movement.
    fnCutsceneMove("Florentina", 21.25, 19.50)
    fnCutsceneMove("Florentina", 21.25, 24.50)
    fnCutsceneMove("Florentina", 22.75, 24.50)
    fnCutsceneMove("Florentina", 22.75, 31.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
    if(iPolarisGavePhial == 0.0) then
        fnCutscene([[ Append("Mei:[E|Sad] What are we missing?[P] This has to be it, it's the source, but there's nothing here...") ]])
    else
        fnCutscene([[ Append("Mei:[E|Sad] What are we missing?[P] This has to be it, it's the source, but there's nothing here...[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] Maybe Polaris can figure it out.[P] I was expecting a big monster but I guess science isn't always exciting.[P] So why am I so on edge?") ]])
    end
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneLayerDisabled("RubberFrame00", true)
    fnCutsceneLayerDisabled("RubberFrame01", false)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneLayerDisabled("RubberFrame01", true)
    fnCutsceneLayerDisabled("RubberFrame02", false)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneLayerDisabled("RubberFrame02", true)
    fnCutsceneLayerDisabled("RubberFrame03", false)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneLayerDisabled("RubberFrame03", true)
    fnCutsceneLayerDisabled("RubberFrame04", false)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Mei:[VOICE|Mei] Maybe there really is nothing here...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("Rubber|ChangeA") ]])
    for i = 5, 21, 1 do
        if(i == 10) then
            fnCutscene([[ AudioManager_PlaySound("Rubber|MeiShout") ]])
        elseif(i == 17) then
            fnCutscene([[ AudioManager_PlaySound("Rubber|SpreadBig") ]])
        end
        local sFirstString  = string.format("RubberFrame%02i", i-1)
        local sSecondString = string.format("RubberFrame%02i", i)
        fnCutsceneLayerDisabled(sFirstString, true)
        fnCutsceneLayerDisabled(sSecondString, false)
        fnCutsceneWait(10)
        fnCutsceneBlocker()
    end
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Florentina", 22.75, 24.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Florentina:[VOICE|Florentina] Mei?[P] Hey, Mei![P] Where did you go?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Florentina", -1, -1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Florentina", 0, -1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Florentina", 1, -1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Florentina", 0, -1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Florentina:[VOICE|Florentina] Mei you big idiot, you weren't supposed to leave without me![B][C]") ]])
    fnCutscene([[ Append("Florentina:[VOICE|Florentina] If you're still here, I'll meet you back at the fishing village.[P] If you're not -[P] I'll find you![P] Nobody ghosts Florentina and gets away with it!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneMove("Florentina", 22.75, 31.50)
    fnCutsceneBlocker()
    fnCutsceneTeleport("Florentina", -1.25, -1.50)
    fnCutsceneBlocker()
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("Rubber|ChangeA") ]])
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("Rubber|ChangeB") ]])
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("Rubber|ChangeC") ]])
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    fnCutsceneWait(125)
    fnCutsceneBlocker()
            
    for i = 22, 30, 1 do
        if(i == 22) then
            fnCutscene([[ AudioManager_PlaySound("Rubber|SpreadB") ]])
        elseif(i == 27) then
            fnCutscene([[ AudioManager_PlaySound("Rubber|SpreadC") ]])
        end
        local sFirstString  = string.format("RubberFrame%02i", i-1)
        local sSecondString = string.format("RubberFrame%02i", i)
        fnCutsceneLayerDisabled(sFirstString, true)
        fnCutsceneLayerDisabled(sSecondString, false)
        fnCutsceneWait(10)
        fnCutsceneBlocker()
    end
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Mei:[VOICE|Mei] (Spread...[P] spread...[P] must...[P] spread...[P] rubber...[P] must...[P] spread...)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Move to the next section, at the base of the tower.
    fnCutscene([[ AL_BeginTransitionTo("WildsTowerA", "FORCEPOS:52.0x41.0x0") ]])
    fnCutsceneBlocker()
end
