-- |[ ======================================= Constructor ====================================== ]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "RiverWildsA"
local sLevelMusic = "TownTheme"
local sMapResolveName = "RiverWildsA"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[ ======================================== Standard ======================================== ]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

    -- |[Construction]|
	--Music.
	AL_SetProperty("Music", sLevelMusic)
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
    
    -- |[Map Handling]|
    --Resolve the map's variable.
    fnStandardCh1Map(sLevelName)
	
	-- |[Spawn NPCS Here]|
	--NPCs set to spawn here will be available for post-exec cutscenes.

-- |[ ======================================== Post-Exec ======================================= ]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	-- |[Spawn NPCs Here]|
	--If an entity needs to spawn after a post-exec cutscene, put them here.
    fnSpawnNPCPattern("Guard", "A", "B")
    fnSpawnNPCPattern("Farmer", "A", "G")
    fnSpawnNPCPattern("Merchant", "A", "E")
    fnSpawnNPCPattern("Patron", "A", "G")
    fnSpawnNPCPattern("Renter", "A", "C")
    fnSpawnNPCPattern("Dockworker", "A", "F")
    fnStandardNPCByPosition("Sammie")
    fnStandardNPCByPosition("Jackie")
    fnStandardNPCByPosition("Norah")
    fnStandardNPCByPosition("VendorA")
    fnStandardNPCByPosition("VendorB")
    fnStandardNPCByPosition("Worker")
    fnStandardNPCByPosition("Olivia")
    
    --Conditional.
    local iSawSharelockBasement = VM_GetVar("Root/Variables/Chapter1/Sharelock/iSawSharelockBasement", "N")
    if(iSawSharelockBasement == 1.0) then
        fnStandardNPCByPosition("Sharelock")
    end
    if(true) then
        fnStandardNPCByPosition("Aquillia")
    end
    local iCompletedMannequinQuest = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iCompletedSequence", "N")
    if(iCompletedMannequinQuest == 0.0) then
        fnStandardNPCByPosition("Victoria")
    end

    -- |[Layers]|
    local iMihoUnconscious = VM_GetVar("Root/Variables/Chapter1/Vendrilstadt/iMihoUnconscious", "N")
    if(iMihoUnconscious == 0.0) then
        fnStandardNPCByPosition("Miho")
        AL_SetProperty("Set Layer Disabled", "MihoSleep", true)
    else
        VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iMihoUnconscious", "N", iMihoUnconscious + 1)
        AL_SetProperty("Set Layer Disabled", "MihoSleep", false)
        if(iMihoUnconscious >= 4.0) then 
            VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iMihoUnconscious", "N", 0)
            fnStandardNPCByPosition("Miho")
            AL_SetProperty("Set Layer Disabled", "MihoSleep", true)
        end
    end
    
    -- |[Skillbook]|
    local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
    local iSkillbook5 = VM_GetVar("Root/Variables/Global/Florentina/iSkillbook5", "N")
    if(iSkillbook5 == 1.0 or bIsFlorentinaPresent == false) then
		AL_SetProperty("Set Layer Disabled", "SkillbookIcoA", true)
    end
    local iSkillbook6 = VM_GetVar("Root/Variables/Global/Mei/iSkillbook6", "N")
    if(iSkillbook6 == 1.0) then
		AL_SetProperty("Set Layer Disabled", "SkillbookIcoB", true)
    end
end
