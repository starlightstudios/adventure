-- |[ ================================= Sammie/Jackie Dialogue ================================= ]|
--Sammie and Jackie share a dialogue file.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Name is fixed.
--local sActorName = "Aquillia"

-- |[ ========================================= "Hello" ======================================== ]|
--Standard entry topic.
if(sTopicName == "Hello") then
    
    -- |[Variables]|
    --Variables.
    local iHasAlrauneForm = VM_GetVar("Root/Variables/Global/Mei/iHasAlrauneForm", "N")
    local iMetSammie      = VM_GetVar("Root/Variables/Chapter1/Vendrilstadt/iMetSammie", "N")
    local sCurrentJob     = VM_GetVar("Root/Variables/Global/Florentina/sCurrentJob", "S")

    -- |[Achievement]|
    AM_SetPropertyJournal("Unlock Achievement", "Marriedraunes")

    -- |[Normal Florentina]|
    if(sCurrentJob ~= "Agarist") then

        --First time:
        if(iMetSammie == 0.0) then
            VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iMetSammie", "N", 1.0)
        
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Marriedraunes", "SNeutral") ]])
            fnCutscene([[ Append("Sammie:[E|SNeutral] [VOICE|MarriedSammie]Ah, what have we here.[P] Dare I presume my company's name?[B][C]") ]])
            fnCutscene([[ Append("Jackie:[E|JNeutral] [VOICE|MarriedJackie]Hiya hiya hiya![B][C]") ]])
            fnCutscene([[ Append("Sammie:[E|SNeutral] [VOICE|MarriedSammie]You, my dear, are the famous Florentina, are you not?[B][C]") ]])
            fnCutscene([[ Append("Sammie:[E|SNeutral] [VOICE|MarriedSammie]I am Sammie of Jeffespeir, and this adoring creature is Jackie of Maplehaven.[B][C]") ]])
            fnCutscene([[ Append("Jackie:[E|JClimb] [VOICE|MarriedJackie]Hey, Florentina, you're the one that the trading post guard mentioned![B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] Does she always climb on you?[B][C]") ]])
            fnCutscene([[ Append("Sammie:[E|SClimb] [VOICE|MarriedSammie]Every day, actually.[B][C]") ]])
            fnCutscene([[ Append("Jackie:[E|JGrab] [VOICE|MarriedJackie]Honk honk![B][C]") ]])
            fnCutscene([[ Append("Sammie:[E|SGrab] [VOICE|MarriedSammie]Oh my goodness, keep it in your pants until I get you back to the cabin![B][C]") ]])
            fnCutscene([[ Append("Jackie:[E|JGrab] [VOICE|MarriedJackie]No deal![B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Happy] So, Jackie and Sammie, eh?[P] I'm Mei, this is Florentina as you already guessed.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] I think I might be getting too famous around here.[B][C]") ]])
            fnCutscene([[ Append("Sammie:[E|SNeutral] [VOICE|MarriedSammie]No no, I asked the little ones.[P] We are not from this region.[B][C]") ]])
            fnCutscene([[ Append("Jackie:[E|JNeutral] [VOICE|MarriedJackie]We're from Leora's coven up north![B][C]") ]])
            fnCutscene([[ Append("Sammie:[E|SNeutral] [VOICE|MarriedSammie]The little ones inform us that you are an adept businesswoman.[P] You can get things done.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Surprise] And you believed them?[B][C]") ]])
            fnCutscene([[ Append("Sammie:[E|SNeutral] [VOICE|MarriedSammie]They have yet to steer us wrong.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Happy] Hey, if it's business you're into, let me hear your pitch.[B][C]") ]])
            fnCutscene([[ Append("Jackie:[E|JClimb] [VOICE|MarriedJackie]We're building a paper mill![B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] How is she so fast?[B][C]") ]])
            fnCutscene([[ Append("Sammie:[E|SClimb] [VOICE|MarriedSammie]She has boundless energy, you see.[B][C]") ]])
            fnCutscene([[ Append("Jackie:[E|JGrab] [VOICE|MarriedJackie]Honk honk![B][C]") ]])
            fnCutscene([[ Append("Sammie:[E|SGrab] [VOICE|MarriedSammie]Business, Jackie![P] The long term survival of our coven depends on this![B][C]") ]])
            fnCutscene([[ Append("Jackie:[E|JGrab] [VOICE|MarriedJackie]You think better when you're horny![B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Blush] Oh no, Jackie figured out my secret![B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] But seriously, a paper mill here in Trannadar?[P] Gutsy, but you've got a problem.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] The raw materials will come cheap, and you're on a trade route, but where are you going to get the labour power?[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] I don't think you'll be able to convince the local werecats or slimes to work here, is what I'm saying.[B][C]") ]])
            fnCutscene([[ Append("Sammie:[E|SNeutral] [VOICE|MarriedSammie]That is the problem that afflicts this whole province, but the opposite problem is in Jeffespeir.[B][C]") ]])
            fnCutscene([[ Append("Sammie:[E|SNeutral] [VOICE|MarriedSammie]Landlords have been upping rental costs and driving workers out of the city.[B][C]") ]])
            fnCutscene([[ Append("Jackie:[E|JNeutral] [VOICE|MarriedJackie]They have no place to go, so we'll make a place for them here![B][C]") ]])
            fnCutscene([[ Append("Sammie:[E|SNeutral] [VOICE|MarriedSammie]Land prices are non-existent here.[P] Miss Vendril will rent out cabins for a fraction of the price in Jeffespeir, and we can build more.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] Labourers imported from a big city housed in shantytowns?[P] I like it.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] How about buyers?[B][C]") ]])
            fnCutscene([[ Append("Sammie:[E|SClimb] [VOICE|MarriedSammie]Selling to Jeffespeir should be no difficulty, and we are hoping to pick up customers along the other parts of this major route.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] You might have problems with Sturnheim, they sell but they don't buy.[P] Even if they need paper, they'll tariff you to hell and back.[B][C]") ]])
            fnCutscene([[ Append("Jackie:[E|JClimb] [VOICE|MarriedJackie]Spoilsports![B][C]") ]])
            fnCutscene([[ Append("Sammie:[E|SNeutral] [VOICE|MarriedSammie]How have you dealt with this problem?[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] I provide equipment to the traders themselves, and liquidate excess inventory.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Happy] But I happen to know some people who might be able to, shall we say, [P]'expedite'[P] the process of getting a trade license.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] Are you going to bribe people?[B][C]") ]])
            fnCutscene([[ Append("Sammie:[E|SNeutral] [VOICE|MarriedSammie]Yes.[B][C]") ]])
            fnCutscene([[ Append("Jackie:[E|JNeutral] [VOICE|MarriedJackie]Yeah.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] Please, don't call it a bribe.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] Anyway, I can help you out.[P] Swing by the trading post east of here later, or leave a message with Hypatia.[B][C]") ]])
            fnCutscene([[ Append("Sammie:[E|SNeutral] [VOICE|MarriedSammie]Of course.[P] We have only laid the foundations, our goal is to get the mill ready next spring.[B][C]") ]])
            fnCutscene([[ Append("Jackie:[E|JNeutral] [VOICE|MarriedJackie]Gotta get the frame up before the snows set in![B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] I hope it goes well for you![P] See you later!") ]])
            
        
        --Repeats:
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Marriedraunes", "SNeutral") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] So are you two married?[B][C]") ]])
            fnCutscene([[ Append("Jackie:[E|JClimb] [VOICE|MarriedJackie]I've never been so married in my life![B][C]") ]])
            fnCutscene([[ Append("Sammie:[E|SClimb] [VOICE|MarriedSammie]Is she not wonderful?[P] Would that I had two hearts that I could love her twice as much.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] How did you two meet?[B][C]") ]])
            fnCutscene([[ Append("Sammie:[E|SNeutral] [VOICE|MarriedSammie]Our leaf-sister Leora has a coven on an island in an inlet near Jeffespeir.[B][C]") ]])
            fnCutscene([[ Append("Sammie:[E|SNeutral] [VOICE|MarriedSammie]We provide produce to all who come past, as they spread the seeds on their journeys.[B][C]") ]])
            fnCutscene([[ Append("Jackie:[E|JClimb] [VOICE|MarriedJackie]I had stopped by for a bushel of apples one day when -[P] WHAM -[P] most beautiful woman I ever laid eyes on gave me a basket![B][C]") ]])
            fnCutscene([[ Append("Sammie:[E|SClimb] [VOICE|MarriedSammie]I joined her myself.[B][C]") ]])
            fnCutscene([[ Append("Jackie:[E|JGrab] [VOICE|MarriedJackie]I'm so glad I was hungry![P] Ha ha![B][C]") ]])
            fnCutscene([[ Append("Sammie:[E|SGrab] [VOICE|MarriedSammie]One of life's little coincidences.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Happy] I'm so happy for you![P] Good luck with the mill!") ]])
            fnCutsceneBlocker()
        end
    
    -- |[Agarist Florentina]|
    else

        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Marriedraunes", "SNeutral") ]])
        fnCutscene([[ Append("Sammie:[E|SNeutral] [VOICE|MarriedSammie]Hello. I confess I have not seen a partirhuman like you before.[P] Are you...[P] sick?[B][C]") ]])
        
        --If you met the pair, Florentina comments on this.
        if(iMetSammie == 0.0) then
            fnCutscene([[ Append("Florentina:[E|Neutral] (They don't recognize me at all.[P] Great job, buddy.)[B][C]") ]])
        end
        
        --Resume.
        fnCutscene([[ Append("Florentina:[E|Neutral] Never felt better.[B][C]") ]])
        fnCutscene([[ Append("Jackie:[E|JNeutral] [VOICE|MarriedJackie]Hey, you're an alraune, right!?[P] That's so cool![P] What's your secret?[B][C]") ]])
        fnCutscene([[ Append("Jackie:[E|JNeutral] [VOICE|MarriedJackie]Sammie![P] Sammie![P] Wouldn't I look cool like that?[B][C]") ]])
        fnCutscene([[ Append("Sammie:[E|SNeutral] [VOICE|MarriedSammie]You look just fine exactly as you are.[B][C]") ]])
        fnCutscene([[ Append("Jackie:[E|JNeutral] [VOICE|MarriedJackie]But I wanna look cooool![B][C]") ]])
        fnCutscene([[ Append("Sammie:[E|SNeutral] [VOICE|MarriedSammie]Well, how about it, stranger?[P] Will you tell my lovely lady your secret?[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] For free?[B][C]") ]])
        fnCutscene([[ Append("Sammie:[E|SNeutral] [VOICE|MarriedSammie]Oh ho.[P] How much is this worth to you, Jackie?[B][C]") ]])
        fnCutscene([[ Append("Jackie:[E|JNeutral] [VOICE|MarriedJackie]That's no fair, we just sunk a lot into this paper mill![P] No fair![B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Happy] If you understand business you understand the importance of secrets.[P] Quickest way to get rich isn't to write a lot of checks.[B][C]") ]])
        fnCutscene([[ Append("Jackie:[E|JClimb] [VOICE|MarriedJackie]Bah![P] Just you wait, we'll have so much money that we'll have an entire coven of hot and cool alraunes![P] Then you'll be sorry![B][C]") ]])
        fnCutscene([[ Append("Sammie:[E|SClimb] [VOICE|MarriedSammie]Sorry, that she sold us her secret?[P] I believe she will be counting the money.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Happy] Heh heh heh...") ]])
    end
end