-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
if(sObjectName == "HotStuff") then
    
    -- |[Repeat Check]|
    local iMetVictoriaInVillage    = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iMetVictoriaInVillage", "N")
    local iFlorentinaReportedGoods = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iFlorentinaReportedGoods", "N")
    if(iFlorentinaReportedGoods == 1.0 or iMetVictoriaInVillage == 0.0) then return end
    
    -- |[Flag]|
    VM_SetVar("Root/Variables/Chapter1/Scenes/Mann|iFlorentinaReportedGoods", "N", 1.0)
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("Florentina:[E|Confused] *Psst, Mei.*[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Smirk] *What's up?[P] Why are we whispering?*[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] *This stuff is definitely hot.[P] The little ones said Victoria here used to run with a gang of bandits down south.*[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] *I think she's looking to offload the stuff and make a getaway, but something isn't adding up...*[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Surprise] *Oh my!*[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] *I can't figure out why she's trying to get full price on these.[P] If anything she's overcharging.[P] What's her angle?*[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Surprise] *So what do we do?*[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] *Might be worth checking out the bandit gang south of here.[P] The little ones said they're in a fort.*[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] *Up to you if you think it's worth your time.*[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Offended] *I don't think I can just let her get away with it.*[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] *Always a goodie-goodie.[P] Anyway, lead the way.*") ]])
    fnCutsceneBlocker()
    
-- |[ ================================== Mannequin Quest Ends ================================== ]|
elseif(sObjectName == "VictoryScene") then

    -- |[ ========= Setup ========== ]|
    --Variables.
    local iNoticedFormChange = VM_GetVar("Root/Variables/Chapter1/Polaris/iNoticedFormChange", "N")
    local iPolarisRuneQuestState = VM_GetVar("Root/Variables/Chapter1/Polaris/iPolarisRuneQuestState", "N")
    
    --Flags.
    VM_SetVar("Root/Variables/Chapter1/Polaris/iNoticedFormChange", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter1/Polaris/iPolarisSuggestStForas", "N", 1.0)
    if(iPolarisRuneQuestState < 1) then
        VM_SetVar("Root/Variables/Chapter1/Polaris/iPolarisRuneQuestState", "N", 1.0)
    end
    WD_SetProperty("Unlock Topic", "Victoria", 1)
    
    --Snap to black.
    fnCutsceneFadeOut()
    fnCutsceneBlocker()

    --Spawn NPCs.
    fnStandardNPCByPosition("Polaris")
    
    --Position characters.
    fnCutsceneTeleport("Mei", 132.25, 58.50)
    fnCutsceneFace("Mei", 1, 0)
    fnCutsceneTeleport("Florentina", 132.25, 59.50)
    fnCutsceneFace("Florentina", 1, 0)
    
    --Wait a bit.
    fnCutsceneWait(125)
    fnCutsceneBlocker()

    --Achievement
    AM_SetPropertyJournal("Unlock Achievement", "FinishMannequin")

    -- |[ ==== Scene Execution ===== ]|
    --Fade in.
    fnCutsceneFadeIn(45)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Aquillia", "Cast") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Polaris", "Neutral") ]])
    fnCutscene([[ Append("Polaris:[E|Upset] How's everyone feeling?[P] Nice and rested?[P] Doing great?[P] Have a lot of fun?[B][C]") ]])
    fnCutscene([[ Append("Aquillia:[E|Cast] You didn't get a wink, did you?[B][C]") ]])
    fnCutscene([[ Append("Polaris:[E|Upset] The minute I am done here I am going home and sleeping until tomorrow.[P] My sleep schedule is going to be totally sandbagged.[P] I feel awful.[B][C]") ]])
    fnCutscene([[ Append("Polaris:[E|Neutral] But none of that is your fault.[P] Sorry.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] What did you do with Victoria?[B][C]") ]])
    fnCutscene([[ Append("Polaris:[E|Thinking] Wizard jail, for the time being.[P] She's self-taught so she's likely not a signatory of the Sorcerer's Pacts but considering what the merchants' union would do to her if we turned her over to them, I get the feeling she isn't complaining.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Happy] Would that I am inclined towards clemency shock you?[B][C]") ]])
    fnCutscene([[ Append("Polaris:[E|Upset] It would, actually.[P] Why?[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Blush] Did her honeyed words get through to you?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Blush] Flattery, oh, I like flattery.[P] But that's not it.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] I can use people with skills, that's all.[P] If she were given a debt-penalty and it were sold to me, I can use a mercenary wizard.[B][C]") ]])
    fnCutscene([[ Append("Polaris:[E|Upset] Good to know.[P] You realize she was enslaving people to get petty revenge, right?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Surprise] So she ought to work off her debt to society by beating up bandits who attack my shipments![B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] What about all the people she turned into mannequins?[B][C]") ]])
    fnCutscene([[ Append("Aquillia:[E|Cast] I got a couple of them while the control spell was jammed, but they started acting independently once Victoria was gone.[B][C]") ]])
    fnCutscene([[ Append("Aquillia:[E|Cast] Some of the merchants pitched in to hire some mercenaries to capture and cure the rest.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Happy] They want their stuff back and it's currently in that fort.[P] I'll chip in, too.[B][C]") ]])
    fnCutscene([[ Append("Polaris:[E|Neutral] On the plus side, I had some time to get a closer look at that spell.[P] I want to thank you for this, this is the first major breakthrough in centuries.[B][C]") ]])
    fnCutscene([[ Append("Polaris:[E|Neutral] At the very least, I managed to turn a sleepless night into a serious magical discovery.[P] Haven't done that in ages, feels great![B][C]") ]])
    fnCutscene([[ Append("Polaris:[E|Neutral] My best guess given what I've seen is that the mannequin group needs to hit a critical mass of members before they...[P] do whatever it is that makes them vanish.[B][C]") ]])
    fnCutscene([[ Append("Polaris:[E|Thinking] While we obviously can't ethically let that happen, I can study the spell in detail with active members.[P] Maybe we can even figure out how it's been spreading.[B][C]") ]])
    fnCutscene([[ Append("Polaris:[E|Neutral] You did great, but we'll take it from here.[P] Seriously, Mei, thank you.[P] And you too, Florentina.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] All we did was accidentally fall victim to a curse.[B][C]") ]])
    fnCutscene([[ Append("Polaris:[E|Upset] Victoria got so horny for you that she exposed her plan while she was being watched.[P] Take credit for that or don't, it's up to you.[B][C]") ]])
    fnCutscene([[ Append("Aquillia:[E|Cast] You kinda ruined that girl's brain.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Blush] All the more reason I should oversee her rehabilitation.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Smirk] And I don't need the cursed bracelet to transform, either![B][C]") ]])
    fnCutscene([[ Append("Polaris:[E|Upset] About that.[P] Where's the necklace, Florentina?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Surprise] Firmly in my possession.[P] A curse that lets me turn into a mannequin at will?[P] Sounds like a great deal![B][C]") ]])
    fnCutscene([[ Append("Polaris:[E|Upset] It's not a toy.[P] It's dangerous magic we don't fully understand.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Surprise] Then I just need to avoid critical mass, right?[P] If it's just me...[B][C]") ]])
    fnCutscene([[ Append("Polaris:[E|Upset] ...[B][C]") ]])
    fnCutscene([[ Append("Polaris:[E|Neutral] I am far, far too tired to argue with you about this.[P] The day you disrespect the curse is the day it claims you.[P] You know that.[P] So don't toy with it.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] Merchant's honour, I won't toy with it.[B][C]") ]])
    fnCutscene([[ Append("Polaris:[E|Upset] Ugh.[P] I'm going home.[B][C]") ]])
    fnCutscene([[ Append("Polaris:[E|Upset] If you have other questions, I can deal with that after I've fed Dot and had a good sleep.[B][C]") ]])
    if(iNoticedFormChange == 0.0) then
        fnCutscene([[ Append("Mei:[E|Happy] You deserve it, go get some rest.[P] And thanks for saving us![B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] Oh, right, before I forget.[P] One last thing.[P] Mei, about your runestone.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Upset] Yes yes, powerful magic, transformation, very impressed.[P] Come talk to me about it at my hut, I think I can improve it.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] I need my equipment there, and some sleep, so take your time.[P] All right, that's it.[P] Goodbye.") ]])
        WD_SetProperty("Unlock Topic", "Polaris_Runestone", 1)
    else
        fnCutscene([[ Append("Mei:[E|Happy] You deserve it, go get some rest.[P] And thanks for saving us!") ]])
    end
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneMove("Polaris", 137.25, 57.50)
    fnCutsceneBlocker()
    fnCutsceneFace("Aquillia", 1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Polaris:[VOICE|Polaris] It'd be faster if I teleport but...[P] no, just walk.[P] Exercise is good for you.[P] Oh good, I'm talking to myself now.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneMove("Polaris", 146.25, 57.50)
    fnCutsceneMove("Polaris", 146.25, 56.50)
    fnCutsceneMove("Polaris", 156.25, 56.50)
    fnCutsceneTeleport("Polaris", -1.25, -1.50)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneFace("Aquillia", -1, 0)
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Aquillia", "Cast") ]])
    fnCutscene([[ Append("Aquillia:[E|Cast] What a wreck.[P] Me, I'm used to waking up at odd hours.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Smirk] Did you know Polaris before all this?[B][C]") ]])
    fnCutscene([[ Append("Aquillia:[E|Cast] Friend of a friend.[P] I thought that Victoria was a creep but I had no idea she was...[P] that.[B][C]") ]])
    fnCutscene([[ Append("Aquillia:[E|Cast] Anyway, I'll be heading back myself later.[P] See you around, Mei.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Finish Up]|
    fnAutoFoldParty()
    fnCutsceneBlocker()
    
-- |[ ================================== The Drinking Contest ================================== ]|
elseif(sObjectName == "DrinkingTime") then

    --Repeat check.
    local iStartedDrinking = VM_GetVar("Root/Variables/Chapter1/Vendrilstadt/iStartedDrinking", "N")
    if(iStartedDrinking == 1.0) then return end
    
    --Variables.
    local sMeiForm       = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
    local sFlorentinaJob = VM_GetVar("Root/Variables/Global/Florentina/sCurrentJob", "S")
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iMetMiho", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iStartedDrinking", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/sFirstMihoForm", "S", sMeiForm)
    
    --Positioning.
    fnCutsceneMergeParty()
    fnCutsceneBlocker()

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("Florentina:[E|Neutral] Hey, Mei, you ever had a pan-galactic snoggleblaster?[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Smirk] Seems vaguely familiar.[P] If you're buying...[B][C]") ]])
    if(sFlorentinaJob == "Merchant" or sFlorentinaJob == "Mediator" or sFlorentinaJob == "Treasure Hunter") then
        fnCutscene([[ Append("Florentina:[E|Happy] It'll be worth it to see the look on your face.[P] Come on.") ]])
    else
        fnCutscene([[ Append("Florentina:[E|Happy] It'll be worth it to see the look on your face.[P] One second, though.") ]])
    end
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Change jobs!
    if(sFlorentinaJob == "Agarist" or sFlorentinaJob == "Lurker") then
        fnCutsceneFace("Florentina", 0, 1)
        fnCutsceneWait(5)
        fnCutsceneBlocker()
        fnCutsceneFace("Florentina", -1, 0)
        fnCutsceneWait(5)
        fnCutsceneBlocker()
        fnCutsceneFace("Florentina", 0, -1)
        fnCutsceneWait(5)
        fnCutsceneBlocker()
        fnCutsceneFace("Florentina", 1, 0)
        fnCutsceneWait(5)
        fnCutsceneBlocker()
        fnCutscenePlaySound("World|TakeItem")
        fnCutsceneCall(gsRoot .. "FormHandlers/Florentina/Job_Merchant.lua")
        fnCutsceneFace("Florentina", 0, 1)
        fnCutsceneWait(5)
        fnCutsceneBlocker()
        fnCutsceneFace("Florentina", -1, 0)
        fnCutsceneWait(5)
        fnCutsceneBlocker()
        fnCutsceneFace("Florentina", 0, -1)
        fnCutsceneWait(5)
        fnCutsceneBlocker()
        fnCutsceneFace("Florentina", 1, 0)
        fnCutsceneWait(5)
        fnCutsceneBlocker()
        fnCutsceneFace("Florentina", 0, 1)
        fnCutsceneWait(5)
        fnCutsceneBlocker()

        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        if(sFlorentinaJob == "Lurker") then
            fnCutscene([[ Append("Florentina:[E|Neutral] Much better.[P] Having tastebuds will likely improve this experience.") ]])
        else
            fnCutscene([[ Append("Florentina:[E|Neutral] Nothing personal, buddy, but I don't even know if you're of drinking age.[P] Can't be too careful.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] ...[P] Come on, it was a joke.[P] Fine![P] You can have whatever fungal drink you want, I'll pay.") ]])
        end
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    
    end
        
    --Movement.
    fnCutsceneMove("Mei", 63.25, 46.50)
    fnCutsceneMove("Mei", 63.25, 45.50)
    fnCutsceneMove("Florentina", 64.25, 46.50)
    fnCutsceneMove("Florentina", 64.25, 45.50)
    fnCutsceneMove("Olivia", 63.25, 43.50)
    fnCutsceneFace("Olivia", 0, 1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Bartender", "Neutral") ]])
    fnCutscene([[ Append("Bartender:[E|Neutral] What'll ya have?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] One PGS, and just a water for me.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Smirk] Just water for you?[B][C]") ]])
    if(sMeiForm == "Alraune") then
        fnCutscene([[ Append("Florentina:[E|Neutral] It's more about the taste than the booze.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] I don't follow.[B][C]") ]])
    else
        fnCutscene([[ Append("Florentina:[E|Neutral] There's not much for me to drink in here, though I do like a good orange juice now and then.[B][C]") ]])
    end
    fnCutscene([[ Append("Bartender:[E|Neutral] One blaster, one water.[P] Put it on your tab, Florentina?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] Yeah.[P] And don't call me that.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneFace("Miho", -1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneMove("Miho", 64.25, 46.50)
    fnCutsceneFace("Miho", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Mei", 0, 1)
    fnCutsceneFace("Florentina", 0, 1)
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Miho", "Neutral") ]])
    fnCutscene([[ Append("Kitsune:[E|Neutral] So, you could have at least made it hard for me.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] Oh?[P] Made what hard for you?[B][C]") ]])
    fnCutscene([[ Append("Kitsune:[E|Neutral] [MUSIC|Null]Florentina.[B][C]") ]])
    if(sFlorentinaJob == "Lurker" or sFlorentinaJob == "Agarist") then
        fnCutscene([[ Append("Kitsune:[E|Neutral] The disguise was something else, but the bartender gave it all away.[P] Good try, though.[B][C]") ]])
    end
    fnCutscene([[ Append("Florentina:[E|Happy] Hey, good news Mei![P] You get to see something really fun![B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Smirk] Oh, great![P] After all the fighting we've been doing, I could use a bit of relaxation.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Blush] Are you a dancer or something?[P] How exotic![B][C]") ]])
    fnCutscene([[ Append("Miho:[E|Neutral] I'm not a dancer.[P] I'm Miho, the famous three-tailed bounty hunter.[B][C]") ]])
    fnCutscene([[ Append("Miho:[E|Neutral] And your friend here is a notoriously hard to find alraune.[P] Amazingly, I managed to find her.[B][C]") ]])
    fnCutscene([[ Append("Miho:[E|Neutral] It was kind of surprising, actually.[P] I heard nobody could ever pin you down, and you always conveniently left town just before a hunter came by.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Happy] So, how you want to solve this?[P] Merry chase?[P] Fistfight?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] It's two on one, genius, and my friend here is actually pretty handy in a fight.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Happy] Thanks![B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Smirk] But, miss Miho the famous bounty hunter, could you maybe not drag Florentina off?[P] She's helping me find a way home right now.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Smirk] She's actually really nice if you can get past the barbs, and I'm sure we can work out some sort of deal.[P] What's the bounty on her head, anyway?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Surprise] Mei, are you offering to work out a deal with her?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Blush] Am I rubbing off on you?[B][C]") ]])
    fnCutscene([[ Append("Miho:[E|Neutral] I don't care about the bounty.[P] It's the quarry I'm after.[B][C]") ]])
    fnCutscene([[ Append("Miho:[E|Neutral] You know how many bounty hunters have gone after you, only to come up empty-handed?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Blush] Oh you're flattering me![B][C]") ]])
    fnCutscene([[ Append("Miho:[E|Angry] Come quietly.[P] I don't expect you to, but I have to say that.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] Nah, not happening.[P] But seriously, I will make you a deal.[B][C]") ]])
    fnCutscene([[ Append("Miho:[E|Angry] No deals, alraune![B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Happy] The deal is, I'll come quietly.[P] If you can beat me in a battle of wits.[B][C]") ]])
    fnCutscene([[ Append("Miho:[E|Neutral] Of wits?[P] Do you know nothing of kitsunes?[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Happy] Uhm, your tails are really cute![B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Smirk] Though on Earth, kitsunes are supposed to have nine tails.[B][C]") ]])
    fnCutscene([[ Append("Miho:[E|Neutral] No, no.[P] Nine tails would be the Fox Goddess.[P] No mortal kitsune has ever had more than eight.[B][C]") ]])
    fnCutscene([[ Append("Miho:[E|Neutral] Still, 'Mei', do you know what we kitsunes are known for?[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Happy] Playing tricks on people![B][C]") ]])
    fnCutscene([[ Append("Miho:[E|Neutral] Cleverness, wit, guile, ingenuity.[P] Challenging me to a battle of wits...[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Happy] So that's a yes?[B][C]") ]])
    fnCutscene([[ Append("Miho:[E|Neutral] What did you have in mind?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Happy] We're in a bar.[P] How about a drinking contest?[P] I'll handle the expenses.[B][C]") ]])
    fnCutscene([[ Append("Miho:[E|Neutral] This is going to be easy![P] I've drunk people under the table twice your size![B][C]") ]])
    fnCutscene([[ Append("Miho:[E|Neutral] Bartender![P] Let's get this going![B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Offended] What, you think this is some backwater?[P] Olivia, send out a call![B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Happy] Anyone who wants in on this, names on the board![P] Florentina's buying, tell everyone!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Cut to the drinking contest.
    fnCutscene([[ AL_BeginTransitionTo("DrinkingContest", "FORCEPOS:1.0x1.0x0") ]])
    fnCutsceneBlocker()

--After the drinking contest.
elseif(sObjectName == "PostDrink") then

    --Black the screen out, stop music.
    fnCutscene([[ AudioManager_PlayMusic("Null") ]])
    fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    -- |[Achievement]|
    AM_SetPropertyJournal("Unlock Achievement", "DrinkingContest")
    
    --Position actors.
    fnCutsceneTeleport("Mei", 91.25, 37.50)
    fnCutsceneFace("Mei", 1, -1)
    fnCutsceneTeleport("Florentina", 91.25, 36.50)
    fnCutsceneFace("Florentina", 1, 0)
    fnCutsceneSetFrame("Florentina", "Null")
    fnCutsceneWait(305)
    fnCutsceneBlocker()
    
    --Fade in.
    fnCutsceneWait(45)
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ Append("Florentina:[E|Happy] Sleep tight, you beautiful fox woman.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Happy] You did it, Florentina![P] You won![B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Happy] The outcome was never in doubt.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Surprise] Hey, you don't sound so drunk anymore![B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] Oh, of course.[P] I'm not drunk at all.[P] The whole thing was an act.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Angry] What!?[P] You mean you made me carry you in here for nothing!?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Happy] Not for nothing, certainly.[P] Besides, you're strong.[P] You can handle it.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Offended] So what the hey, Florentina![P] I saw you down practically a whole keg![B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Happy] Hey, I told you I had it under control.[P] I knew something Miho didn't.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Happy] Alraunes don't process alcohol.[P] It goes right through us.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Surprise] Really![B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] Yep.[P] We're plants, after all.[P] Closest thing to drunk we get is maple syrup.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Happy] I've won a few drinking contests by the strength of my acting, not my liver.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Laugh] So the slurring, the flirting, and the wobbling -[P] all an act?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Happy] The flirting was legitimate, you know.[P] Miho's really cute.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Smirk] So I guess I learned something today.[P] What about Miho?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] I gave the doctor some extra platina to make sure she's okay.[P] She's going to do checkups on all the contestants.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Sad] But what if she comes after you again?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] I don't think she will.[P] Besides, that was just one of my many, many tricks.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Happy] Stick with me a while and maybe you'll see the other hundred.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Happy] Hundred?[P] Are they all as good as 'Pretend to be drunk'?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Happy] Only one way to find out.[P] Now, back to the adventure!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Fold.
    fnAutoFoldParty()
    fnCutsceneBlocker()
    
    --Music resumes.
	fnCutscene([[ AudioManager_PlayMusic("TownTheme") ]])
    
end
