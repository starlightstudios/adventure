-- |[ ==================================== Victoria Dialogue =================================== ]|
--Victoria has enough dialogue variations that a standalone script is needed.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Name is fixed.
local sActorName = "Victoria"

-- |[ ========================================= "Hello" ======================================== ]|
--Standard entry topic.
if(sTopicName == "Hello") then

    -- |[ ========== Setup ========= ]|
    --Facing.
    TA_SetProperty("Face Character", "PlayerEntity")

    --Variables.
    local iMetVictoriaInVillage = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iMetVictoriaInVillage", "N")

    --Always a major dialogue.
    fnStandardMajorDialogue()
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Victoria", "Neutral") ]])

    -- |[ ======= First Time ======= ]|
    if(iMetVictoriaInVillage == 0.0) then
        
        -- |[Flag]|
        VM_SetVar("Root/Variables/Chapter1/Scenes/Mann|iMetVictoriaInVillage", "N", 1.0)
        
        -- |[Items]|
        LM_ExecuteScript(gsItemListing, "Diamond Bracelet")
        LM_ExecuteScript(gsItemListing, "Ruby Necklace")
        
        -- |[Dialogue]|
        fnCutscene([[ Append("Victoria:[E|Neutral] Hello![P] Welcome to Victoria's Fashion Popup![P] Are you browsing, or do you have something specific in mind?[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Smirk] Just browsing right now.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] ...[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] All right, sure.[P] Little ones say you're okay.[P] Name's Florentina, this is Mei.[B][C]") ]])
        fnCutscene([[ Append("Victoria:[E|Apology] The little ones?[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] Us alraunes can talk to plants.[P] I'm a businesswoman, always helps to know what sorts of things someone has gotten up to.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] You're new in the area, right?[P] I run a store in the trading post east of here.[P] You need anything, I can get it.[B][C]") ]])
        fnCutscene([[ Append("Victoria:[E|Neutral] Ah, a fellow merchant.[P] I may need to enlist your services myself, actually.[P] Are you interested in a bulk purchase?[B][C]") ]])
        fnCutscene([[ Append("Victoria:[E|Concern] You see, my ship will be coming in soon, but the hold has been oversold.[P] I need to dispose of some inventory that I won't have space for.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] Oh, a motivated seller?[P] What are you selling?[B][C]") ]])
        fnCutscene([[ Append("Victoria:[E|Neutral] Capes, clothes, gems, accessories, anything that makes someone a spectacle to behold.[P] If you'd like anything, it's at a reduced price.[P] This would make a noblewoman jealous![B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Offended] But how do I know it's real?[P] I've been ripped off before...[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Happy] Relax, Mei.[P] I know someone who can check for forgeries.[P] In fact...[P] Victoria.[P] You want to do business?[B][C]") ]])
        fnCutscene([[ Append("Victoria:[E|Apology] Absolutely.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Happy] I will take this, and this, and this.[P] On layaway.[P] [SOUND|World|TakeItem]Here's their posted value, to be returned to me when I have them checked.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Happy] Won't take long, but I don't want you to think I'm robbing you.[P] I do that during the negotiations, heh.[B][C]") ]])
        fnCutscene([[ Append("Victoria:[E|Neutral] Of course, standard practice.[P] I assure you, my pieces are all authentic.[P] For example, Mei, take a look.[B][C]") ]])
        fnCutscene([[ Append("Victoria:[E|Apology] This diamond bracelet, yes, real diamond, is enchanted to increase your attack power.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Smirk] Really?[P] So it's not just for looks?[B][C]") ]])
        fnCutscene([[ Append("Victoria:[E|Disgust] My first love was magic, but necessity has moved me into the realm of sales.[P] Florentina?[B][C]") ]])
        fnCutscene([[ Append("Victoria:[E|Neutral] This necklace you've selected?[P] An enchantment increases your defense and accuracy.[P] Feel free to wear them while you get them examined.[B][C]") ]])
        fnCutscene([[ Append("Victoria:[E|Neutral] I'd consider it to be free advertisement, just tell everyone to come here to get their fashion needs filled![B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Happy] Deal.[P] Nice doing business with you.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] (Got a Diamond Bracelet and a Ruby Necklace)[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] Just so you know, I'm helping Mei here with a job.[P] If you need to find me, Norah Vendril here can put us in contact, or you can speak with my assistant at the trading post.[B][C]") ]])
        fnCutscene([[ Append("Victoria:[E|Concern] And your collateral?[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] If there's a discrepency, settle up with Vendril before you ship out.[P] She's actually holding some of my assets at the moment, hell, they might be coming here on the ship you're leaving on.[B][C]") ]])
        fnCutscene([[ Append("Victoria:[E|Neutral] Splendid.[P] I shall await your return, Florentina.[P] Good day, Mei.") ]])
    
    -- |[ ========= Repeat ========= ]|
    else
        fnCutscene([[ Append("Victoria:[E|Apology] Was there anything else you were interested in?[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Happy] Just looking, thank you.[P] I'm fighting the urge to buy all of it![B][C]") ]])
        fnCutscene([[ Append("Victoria:[E|Apology] I think that rubies are your color, Florentina.[P] You'd look lovely in them.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] Hey, I'm just getting them appraised.[B][C]") ]])
        fnCutscene([[ Append("Victoria:[E|Neutral] There's no cost in sampling, feel free to wear them.") ]])
    end
end