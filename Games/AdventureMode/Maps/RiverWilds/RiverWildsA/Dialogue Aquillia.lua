-- |[ =================================== Aquillia Dialogue ==================================== ]|
--Aquillia has enough dialogue variations that a standalone script is needed.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Name is fixed.
local sActorName = "Aquillia"

-- |[ ========================================= "Hello" ======================================== ]|
--Standard entry topic.
if(sTopicName == "Hello") then

    -- |[ ========== Setup ========= ]|
    --Facing.
    TA_SetProperty("Face Character", "PlayerEntity")
    
    --Variables.
    local sMeiForm                  = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
    local iHasMetAquillia           = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasMetAquillia", "N")
    local iMetAquilliaInVillage     = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iMetAquilliaInVillage", "N")
    local iHasFlorentinaMetAquillia = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasFlorentinaMetAquillia", "N")

    --Always a major dialogue.
    fnStandardMajorDialogue()
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Aquillia", "Cast") ]])

    -- |[ ======= First Time ======= ]|
    --If the player walked past Aquillia earlier, then this is the first time meeting her.
    if(iHasMetAquillia == 0.0) then

        -- |[Flags]|
        VM_SetVar("Root/Variables/Chapter1/Scenes/iHasMetAquillia", "N", 1.0)
        VM_SetVar("Root/Variables/Chapter1/Scenes/iHasFlorentinaMetAquillia", "N", 1.0)
        VM_SetVar("Root/Variables/Chapter1/Scenes/Mann|iMetAquilliaInVillage", "N", 1.0)

        -- |[Dialogue]|
        --If Mei is an Alraune during this conversation:
        if(sMeiForm == "Alraune") then
            fnCutscene([[ Append("Lady:[E|Cast] Hey, you made it![P] Though you had a run-in with the plants I see.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Happy] This isn't my fault.[P] Mei is a sucker and I can't do anything about that.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Offended] I happen to like being an alraune.[P] I'm glad you made it out, too.[B][C]") ]])
            fnCutscene([[ Append("Lady:[E|Cast] Those guys were pretty dumb, I just lied a few times and they didn't look too close.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Surprise] Mei, I'm crushed that you haven't introduced us yet![B][C]") ]])
        
        --If Mei is a Bee during the conversation:
        elseif(sMeiForm == "Bee") then
            fnCutscene([[ Append("Lady:[E|Cast] You got out of that cultist nest and right into a beehive?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] It's complicated.[B][C]") ]])
            fnCutscene([[ Append("Lady:[E|Cast] And you can talk?[P] Didn't know bees could do that.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Sad] I am...[P] different.[P] Look, don't worry too much about it.[B][C]") ]])
            fnCutscene([[ Append("Lady:[E|Cast] If you're happy then it's fine.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Happy] Mei, are you going to introduce me to your friend here?[B][C]") ]])
    
        --If Mei is a Slime during the conversation:
        elseif(sMeiForm == "Slime") then
            fnCutscene([[ Append("Lady:[E|Cast] Hey, you made it -[P] for some definition of 'making it'.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] You'd think getting eaten by the blob would be bad but I enjoyed it![B][C]") ]])
            fnCutscene([[ Append("Lady:[E|Cast] You did?[P] I guess it's slime mating season.[P] Ugh.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Happy] Mei's slimier but no dumber so I think it's a net gain.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Blush] You think that's an insult but to me, it's a compliment![B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] Anyway, I haven't seen you around, miss...[B][C]") ]])
        
        --If Mei is a Ghost during the conversation:
        elseif(sMeiForm == "Ghost") then
            fnCutscene([[ Append("Lady:[E|Cast] Oh geez.[P] Did they get you?[P] Are you a spirit of vengeance?[P] Because it'll have to wait a bit.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] Uh, no, I'm a spirit of...[P] forgiveness?[P] Look, it's complicated.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Confused] Is that how you tell people you're dead?[P] It's complicated?[B][C]") ]])
            fnCutscene([[ Append("Lady:[E|Cast] It is true, though.[P] I tell people things are complicated all the time.[P] It makes them leave me alone.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] It's shorthand for 'explaining this would take too long'.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] Or 'shut up'.[P] How I love ambiguity.[P] Anyway, are you going to introduce me, Mei?[B][C]") ]])
        
        --If Mei is a Gravemarker during the conversation:
        elseif(sMeiForm == "Gravemarker") then
            fnCutscene([[ Append("Lady:[E|Cast] Woah.[P] What happened to you?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] Statues tried to turn me into a statue and I booked it before I cut my own head off.[P] Runestone warped me to safety. [P]You?[B][C]") ]])
            fnCutscene([[ Append("Lady:[E|Cast] Slipped out of the dungeon and got a friend to fix up my arm.[P] What a day.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Laugh] Girl, tell me about it![B][C]") ]])
            fnCutscene([[ Append("Lady:[E|Cast] Just out of curiosity, you don't feel any 'compulsions' by 'the light', do you?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] Not really.[B][C]") ]])
            fnCutscene([[ Append("Lady:[E|Cast] Good enough for me.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] What a fascinating exchange.[P] So are you going to introduce me, Mei?[B][C]") ]])
    
        --Wisphag
        elseif(sMeiForm == "Wisphag") then
            fnCutscene([[ Append("Lady:[E|Cast] Hey, I recognize you.[P] You're the girl who helped me in that filthy dungeon.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] Nice to see you're all right![P] Other than the arm...[B][C]") ]])
            fnCutscene([[ Append("Lady:[E|Cast] I'll be fine.[P] What about you?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Happy] I'm a wisphag![P] I can see spirits![P] It's been a real trip.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] I'm helping her find a way home.[P] Hey, who are you again?[B][C]") ]])
        
        --Mannequin
        elseif(sMeiForm == "Mannequin") then
            fnCutscene([[ Append("Lady:[E|Cast] Eerie.[P] I'm getting a premonition or something.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] Try to ignore it and focus on the here and now.[P] Yes, I'm a mannequin, but it's because of a curse.[B][C]") ]])
            fnCutscene([[ Append("Lady:[E|Cast] Oh, that makes it okay then.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] You two know each other?[B][C]") ]])
        
        --Human/unhandled.
        else
            fnCutscene([[ Append("Lady:[E|Cast] Hey, you made it![P] Didn't think I'd see you again.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Happy] You made it too![P] I guess I shouldn't have worried, those guys were pretty stupid.[B][C]") ]])
            fnCutscene([[ Append("Lady:[E|Cast] Yeah, they believe anything you tell them.[P] Like they haven't figured out how lying works.[P] Guess that's how you end up in a cult.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] You two know each other?[B][C]") ]])

        end
        fnCutscene([[ Append("Aquillia:[E|Cast] Name's Aquillia.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] Mei, and this is Florentina.[P] Sorry we didn't have time to swap names.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] And how'd you two meet?[B][C]") ]])
        fnCutscene([[ Append("Aquillia:[E|Cast] I was looking into some rumours about a religious cult taking up residence in that big mansion by the lake.[P] They caught me and chucked me in a prison.[B][C]") ]])
        fnCutscene([[ Append("Aquillia:[E|Cast] Mei helped me bust out.[P] In the nick of time, too.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Sad] You didn't happen to see how I wound up in there, did you?[B][C]") ]])
        fnCutscene([[ Append("Aquillia:[E|Cast] Figured you got caught like me.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Sad] Rats.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] Mei showed up at the trading post and asked for help finding her way to 'Earth'.[P] So I'm doing that.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Smirk] She gets to keep the valuables we find.[B][C]") ]])
        fnCutscene([[ Append("Aquillia:[E|Cast] Smart.[P] I'm a little indisposed, though.[P] If you're still looking in a few weeks, I owe you one.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Smirk] I'll keep that in mind.[P] What brings you here?[B][C]") ]])
        fnCutscene([[ Append("Aquillia:[E|Cast] Groceries.[P] I can't exactly go hunting right now, but I have some cash stashed away.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] You know the trading post has food, right?[P] I can - [B][C]") ]])
        fnCutscene([[ Append("Aquillia:[E|Cast] No thanks.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Happy] Ooh, what about a delivery?[B][C]") ]])
        fnCutscene([[ Append("Aquillia:[E|Cast] That, I will consider.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Surprise] You crossed the swamp all by yourself?[B][C]") ]])
        fnCutscene([[ Append("Aquillia:[E|Cast] My arm doesn't work, my legs are fine, if that's what you're implying.[P] I'm guessing I have you to thank for getting that door open?[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] Perhaps I should charge for my services.[B][C]") ]])
        fnCutscene([[ Append("Aquillia:[E|Cast] You can pick locks?[P] Maybe you're not half bad, Florentina.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] Two-thirds at most.[B][C]") ]])
        fnCutscene([[ Append("Aquillia:[E|Cast] Now before I let you go, you know anything about this fashion merchant over here, Florentina?[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] Never seen her before, but the little ones said her name was 'Victoria'.[P] She doesn't do business with anyone I know of.[B][C]") ]])
        fnCutscene([[ Append("Aquillia:[E|Cast] All right then.[P] Nice seeing you again, Mei.[P] Don't worry about me.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Smirk] You too, Aquillia![P] I wish you a speedy recovery.") ]])
    
    -- |[Has Met Aquillia But Not At The Village]|
    elseif(iHasMetAquillia == 1.0 and iMetAquilliaInVillage == 0.0) then
    
        -- |[Flags]|
        VM_SetVar("Root/Variables/Chapter1/Scenes/Mann|iMetAquilliaInVillage", "N", 1.0)
        VM_SetVar("Root/Variables/Chapter1/Scenes/iHasFlorentinaMetAquillia", "N", 1.0)
    
        -- |[Dialogue]|
        fnCutscene([[ Append("Mei:[E|Surprise] Aquillia![P] I'm -[P] how did you get all the way over here?[B][C]") ]])
        fnCutscene([[ Append("Aquillia:[E|Cast] By walking.[P] My legs are fine, you know.[P] Thanks for getting that door open, by the way.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] Maybe I should be charging for my services.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Happy] And you'd give Polaris a cut because you're using her tools?[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Surprise] Wow, Mei's a lawyer, too.[P] What other skills are you hiding?[B][C]") ]])
        
        -- |[Aquillia Hasn't Met Florentina]|
        if(iHasFlorentinaMetAquillia == 0.0) then
            fnCutscene([[ Append("Mei:[E|Smirk] Where are my manners?[P] Aquillia, meet Florentina.[P] She's helping me find a way home.[B][C]") ]])
            fnCutscene([[ Append("Aquillia:[E|Cast] Mei helped me bust out of cult jail.[P] She's a lifesaver.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] Oh so that was you?[P] The little ones said a bunch of very angry people were fanning out of the manor a while ago.[B][C]") ]])
            fnCutscene([[ Append("Aquillia:[E|Cast] I know how to stay hidden, I'm not worried.[P] Took them long enough.[P] I'd leave some traps for them but...[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] The local alraune coven doesn't like them either, so don't worry about it.[B][C]") ]])
            fnCutscene([[ Append("Aquillia:[E|Cast] Good to know.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] So, Aquillia, what brings you all the way out here?[B][C]") ]])
        
        -- |[Has Met]|
        else
            fnCutscene([[ Append("Mei:[E|Smirk] So, Aquillia, what brings you all the way out here?[B][C]") ]])
        end
        
        -- |[Dialogue]|
        fnCutscene([[ Append("Aquillia:[E|Cast] Shopping.[P] I can't hunt with my arm like this.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Surprise] The trading post has all your delectable needs at very reasonable prices![P] Potatoes are ten percent off and - [B][C]") ]])
        fnCutscene([[ Append("Aquillia:[E|Cast] That's okay.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Happy] Not a potato girl?[P] Pork sausage for - [B][C]") ]])
        fnCutscene([[ Append("Aquillia:[E|Cast] No thanks.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] What about if I arrange deliveries?[P] No fee![B][C]") ]])
        fnCutscene([[ Append("Aquillia:[E|Cast] I might take you up on that, actually.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] Phew.[P] Had me worried, there.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Smirk] Just don't take any risks.[P] The swamp is dangerous![B][C]") ]])
        fnCutscene([[ Append("Aquillia:[E|Cast] Your concern is touching, really, but I'll be fine.[P] Actually, can I ask you something?[B][C]") ]])
        fnCutscene([[ Append("Aquillia:[E|Cast] You know anything about this fashion merchant over here, Florentina?[P] Done business with her or anything?[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] Never seen her before, but the little ones said her name was 'Victoria'.[P] She doesn't trade with anyone I know of.[B][C]") ]])
        fnCutscene([[ Append("Aquillia:[E|Cast] All right then.[P] Nice seeing you again, Mei.[P] Don't worry about me, really.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Smirk] Okay, okay.[P] Speedy recovery to you!") ]])
    
    -- |[Repeat Cases]|
    else
        --If Mei happens to need some Kokayanee...
        local iCokeCount = AdInv_GetProperty("Item Count", "Kokayanee")
        local iTakenPieJob = VM_GetVar("Root/Variables/Chapter1/Breanne/iTakenPieJob", "N")
        local iCompletedSequence = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iCompletedSequence", "N")
        if(iTakenPieJob == 1.0 and iCokeCount < 1) then
            fnCutscene([[ Append("Mei:[E|Neutral] Aquillia, please don't take this the wrong way, but...[P][CLEAR]") ]])
            fnCutscene([[ Append("Florentina:[E|Happy] Mei needs to get high.[P] You cool?[B][C]") ]])
            fnCutscene([[ Append("Aquillia:[E|Cast] Word.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Surprise] No, I - [P][CLEAR]") ]])
            fnCutscene([[ Append("Aquillia:[E|Cast] Sorry, Mei, but I don't carry my stuff on me.[P] You know, because of the cast.[B][C]") ]])
            fnCutscene([[ Append("Aquillia:[E|Cast] Meet me at my place east of the swamp, I can hook you up there.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Offended] It's for a pie![B][C]") ]])
            fnCutscene([[ Append("Aquillia:[E|Cast] Sure it is.[P] See you there.") ]])
    
        elseif(iCompletedSequence == 1.0) then
            fnCutscene([[ Append("Aquillia:[E|Cast] Yo, Mei.[P] Been wondering.[P] What's it like being a mannequin?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] I guess it's quiet?[P] When I was under Victoria's control, I was aware, and could see and think, but all the extra thoughts that normally bubble up, weren't there.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] I just focused on the task at hand, thought of nothing else, and did it.[B][C]") ]])
            fnCutscene([[ Append("Aquillia:[E|Cast] Freaky.[P] So it was mind control magic, yeah?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] Yeah, definitely, but it was different than you'd think.[P] Whatever order she gave me was my only thought.[P] I felt like I could do what I want, I just wanted that.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] Why do you ask?[B][C]") ]])
            fnCutscene([[ Append("Aquillia:[E|Cast] I got a front row seat, and I like to know about things.[P] This'll make a hell of a story to my friends, if nothing else.[B][C]") ]])
            fnCutscene([[ Append("Aquillia:[E|Cast] And, if worst comes to worst, now I know that I might be able to help someone under the same curse.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] Hopefully, you won't!") ]])
    
        else
            fnCutscene([[ Append("Aquillia:[E|Cast] I hate waiting around all day.[P] It's gonna be worth it when I get my mitts on those taffing cultists.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Surprise] Violence as a first resort?[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Happy] Aquillia...[P] Will you be my new best friend?[B][C]") ]])
            fnCutscene([[ Append("Aquillia:[E|Cast] ...[B][C]") ]])
            fnCutscene([[ Append("Aquillia:[E|Cast] You know, I thought today was going pretty badly.[P] But now?[P] I got a new best friend.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] Try to keep a positive outlook![P] Waiting around isn't so bad, you can go for a walk![P] Talk to friends![B][C]") ]])
            fnCutscene([[ Append("Aquillia:[E|Cast] And I'm doing both of those things right now.[P] Thanks, Mei.") ]])
        end
    end
end