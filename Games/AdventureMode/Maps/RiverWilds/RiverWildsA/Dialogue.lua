-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[Rerouting]|
    if(sActorName == "Aquillia") then
        LM_ExecuteScript(fnResolvePath() .. "Dialogue Aquillia.lua", "Hello")
    elseif(sActorName == "Victoria") then
        LM_ExecuteScript(fnResolvePath() .. "Dialogue Victoria.lua", "Hello")
    elseif(sActorName == "Sammie" or sActorName == "Jackie") then
        LM_ExecuteScript(fnResolvePath() .. "Dialogue Sammie.lua", "Hello")
    
    -- |[Local Dialogue]|
    elseif(sActorName == "MerchantA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Merchant:[VOICE|HumanF0] You going west or east?[P] Don't cross the swamp, it's faster to go around through Trafal.[P] Yeah it's steeper, but you won't get stuck.[B][C]") ]])
        fnCutscene([[ Append("Merchant:[VOICE|HumanF0] What are you looking at me like that for?[P] Werecats can find hot deals as well as we find unsuspecting birds!") ]])
	
    elseif(sActorName == "MerchantB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Merchant:[VOICE|HumanF1] The boats all got held up, so we pitched camp for a few days.[P] I'm not blowing my platina on a cabin!") ]])
	
    elseif(sActorName == "MerchantC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Mercenary:[VOICE|HumanM0] It's a lot of walking, but the pay is good, and all you have to do is protect a few merchants.") ]])
	
    elseif(sActorName == "MerchantD") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Merchant:[VOICE|HumanF0] This is a nice break after all the uphill walking and cold.[P] I can catch up on my reading without my fingers falling off.") ]])
	
    elseif(sActorName == "MerchantE") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Merchant:[VOICE|HumanM1] I think I need to retire.[P] We almost got eaten by alraunes the other day.") ]])
        
    elseif(sActorName == "FarmerA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Farmer:[VOICE|HumanM0] Good crop this year![P] I just hope we get it all harvested before the birds get at it.") ]])
        
    elseif(sActorName == "FarmerB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Farmer:[VOICE|Alraune] The humans pay very well for my help raising crops.[P] I'm just happy to see them all fed and happy.[B][C]") ]])
        fnCutscene([[ Append("Farmer:[VOICE|Alraune] Perhaps I should invest in some sort of business with it all, but what?") ]])
        
    elseif(sActorName == "FarmerC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Farmer:[VOICE|Alraune] Alraunes are allowed to own property out here in the sticks, but if you go to someplace big like Jeffespeir, only humans can legally have a shop or business.[B][C]") ]])
        fnCutscene([[ Append("Farmer:[VOICE|Alraune] Something to do with the big noble houses keeping an iron grip on finances.[P] Snobs.") ]])
        
    elseif(sActorName == "FarmerD") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Farmer:[VOICE|HumanF0] Looking for work?[P] It's almost harvest time.[P] Come back in a week and I can guarantee you'll have a job for at least a month.[B][C]") ]])
        fnCutscene([[ Append("Farmer:[VOICE|HumanF0] Hiring alraunes to supervise is really paying off.[P] This is our third bumper crop this year!") ]])
        
    elseif(sActorName == "FarmerE") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Farmer:[VOICE|HumanF0] We're going to sell the hemp to the weavers in Jeffespeir, but corn keeps a while.[P] We'll probably sell that to traders.") ]])
        
    elseif(sActorName == "FarmerG") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Farmer:[VOICE|HumanF0] Not enough time before the snows set in for another batch of grapes.[P] If you're looking to buy, ask Marcie by the docks.")  ]])
            
    elseif(sActorName == "GuardA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Guard:[VOICE|HumanF0] I have the night shift, but I can't sleep.[P] This sucks.")  ]])
    
    elseif(sActorName == "GuardB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Guard:[VOICE|HumanM1] If you want to fight someone, don't do it inside the walls.[P] Otherwise I have to care.")  ]])
    
    elseif(sActorName == "RenterA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Trader:[VOICE|HumanF0] It's absolutely worth it to rent a cabin for your stay.[P] Sleeping on a proper bed beats the ground!")  ]])
        
    elseif(sActorName == "RenterB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Trader:[VOICE|HumanM0] Most of this village is for-rent cabins.[P] I wonder what the permanent population actually is?")  ]])
        
    elseif(sActorName == "RenterC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Trader:[VOICE|HumanF0] I wonder if I ought to buy a cabin here and just stay year-round.[P] It's so cozy, with none of the pressures of the city.")  ]])
        
    elseif(sActorName == "VendorA") then
        TA_SetProperty("Face Character", "PlayerEntity")

        --Setup.
        local sBasePath = fnResolvePath()
        local sString = "AM_SetShopProperty(\"Show\", \"Vendrilstadt General Goods\", \"" .. sBasePath .. "Shop Setup.lua\", \"Null\")"

        --Run the shop.
        fnCutscene(sString)
        fnCutsceneBlocker()
        
    elseif(sActorName == "VendorB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Worker:[VOICE|HumanF0] I just finished setting up the stalls for next week's farmer's market.[P] Don't miss the harvest festival next month!")  ]])
        
    elseif(sActorName == "DockworkerA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Sailor:[VOICE|HumanF0] It ain't rainin' unless it makes your head bleed![P] I can't wait to get back to the sea.")  ]])
        
    elseif(sActorName == "DockworkerB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Fisher:[VOICE|HumanM1] All I caught today was cod, cod, cod.[P] Oh well, at least the traders will buy it.")  ]])
        
    elseif(sActorName == "DockworkerC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Fisher:[VOICE|HumanM0] It ain't a glamorous job, but fishing gives you lots of time to think.")  ]])
        
    elseif(sActorName == "DockworkerD") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Fisher:[VOICE|HumanF0] The village has gotten so big since Norah Vendril decided to build rental cabins.[P] We used to just be a hub for traders to come and go.")  ]])
        
    elseif(sActorName == "DockworkerE") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Fisher:[VOICE|HumanF0] Becoming a cat was the best decision I ever made![P] Fish tastes a hundred times better!")  ]])
        
    elseif(sActorName == "DockworkerF") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Worker:[VOICE|HumanF0] Oh, hello, Miss Florentina.[P] No parcels for you today.")  ]])
    
    elseif(sActorName == "Worker") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Worker:[VOICE|HumanF0] Good pay, good food.[P] Working for alraunes is way better than any job I had up north.")  ]])
        
    elseif(sActorName == "Sharelock") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sharelock", "Neutral") ]])
        fnCutscene([[ Append("Share;Lock:[E|Neutral] My friends! Good to see you![B][C]") ]])
        
        --Activate topics mode once the dialogue is complete.
        fnCutscene([[ WD_SetProperty("Activate Topics After Dialogue", "Sharelock") ]])
            
    elseif(sActorName == "Olivia") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Olivia:[VOICE|MercF] Drinks, ten platina.[P] Thirty platina covercharge.[P] If you want to dance on top of a table, that's fifteen.") ]])
        
    elseif(sActorName == "Norah") then
        
        --Variables.
        local sMeiForm  = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
        local iMetNorah = VM_GetVar("Root/Variables/Chapter1/Vendrilstadt/iMetNorah", "N")
        
        --Common.
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "HumanF1", "Neutral") ]])
        
        --First time.
        if(iMetNorah == 0.0) then
            VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iMetNorah", "N", 1.0)
            fnCutscene([[ Append("Norah:[E|Neutral] Well well well, look what the cat dragged in.[B][C]") ]])
            if(sMeiForm == "Werecat") then
                fnCutscene([[ Append("Mei:[E|Offended] Don't make me kick your ass, lady.[B][C]") ]])
                fnCutscene([[ Append("Norah:[E|Neutral] Sorry![P] It's an expression, I meant no offense.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] I figured.[P] Don't worry about it.[B][C]") ]])
            else
                fnCutscene([[ Append("Mei:[E|Smirk] Hello there![B][C]") ]])
            end
            fnCutscene([[ Append("Florentina:[E|Happy] Norah![P] Long time no see![B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Confused] Your payment is in the mail.[B][C]") ]])
            fnCutscene([[ Append("Norah:[E|Neutral] Don't worry about it, I trust you.[B][C]") ]])
            fnCutscene([[ Append("Norah:[E|Neutral] You know the difference between the long and short terms, which is more than most in our profession can say.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] Good.[P] Just making that clear.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] Do you two know each other?[B][C]") ]])
            fnCutscene([[ Append("Norah:[E|Neutral] I do business, Florentina does business.[P] That's about it.[B][C]") ]])
            fnCutscene([[ Append("Norah:[E|Neutral] You here looking for something again?[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] Trying to find this dolt a way back to Earth.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] Hello![P] My name is Mei, and I'm from Hong Kong.[P] Ever heard of it?[B][C]") ]])
            fnCutscene([[ Append("Norah:[E|Neutral] Hong Kong?[P] Nope.[P] You could check my maps if you wanted.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Sad] Crud.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] But, I have Florentina here helping me, so I know we'll make it.[B][C]") ]])
            fnCutscene([[ Append("Norah:[E|Neutral] Very nice to meet you, Mei.[P] Please avail yourself of the facilities here.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Happy] Norah probably gets a cut.[B][C]") ]])
            fnCutscene([[ Append("Norah:[E|Neutral] But of course![P] I oversee the docks and maintenance, everyone needs me to sell.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Happy] See you around, Norah.") ]])
        
        else
            fnCutscene([[ Append("Norah:[E|Neutral] Looking to rent a cabin or anything, Mei?[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] Actually, I wanted to ask about that.[P] Where'd they all come from?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] Were they not always here?[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] Not since I was here last year.[P] Usually I send a messenger when I need to buy something.[B][C]") ]])
            fnCutscene([[ Append("Norah:[E|Neutral] I had some workers come in to build them.[P] Our rental prices are very reasonable![B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] And your customers are the merchants and fishermen who come here?[B][C]") ]])
            fnCutscene([[ Append("Norah:[E|Neutral] Yes, but -[P] Vacation rentals have been surging.[P] Jeffespeir rich people come to the country to relax.[B][C]") ]])
            fnCutscene([[ Append("Norah:[E|Neutral] And I have it on good authority there will be a much larger amount of trade coming through Trannadar fairly soon.[B][C]") ]])
            fnCutscene([[ Append("Norah:[E|Neutral] Go find Sammie just outside the gate.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Surprise] And you didn't even ask me for investment?[P] I'm crushed![B][C]") ]])
            fnCutscene([[ Append("Norah:[E|Neutral] Hey, hey, hey, Florentina, we're all friends here![P] I just didn't need help with basic construction and sourcing.[B][C]") ]])
            fnCutscene([[ Append("Norah:[E|Neutral] I could definitely use some luxury goods, maybe some fine carpet and linens?[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Happy] Oh you sweet-talker![P] I'll see what I can rustle up.[P] I probably have a bit of decoration for you, too.[B][C]") ]])
            fnCutscene([[ Append("Norah:[E|Neutral] We talking paintings?[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Happy] We're talking paintings, we're talking lamps, we're talking furs.[P] Norah, we're talking everything![B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Happy] Seems things are going well for you, Miss Norah.[P] Good luck!") ]])
        end
        fnCutsceneBlocker()
        
    elseif(sActorName == "Miho") then
        
        --Variables.
        local sMeiForm            = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
        local iSpokeToMiho        = VM_GetVar("Root/Variables/Chapter1/Vendrilstadt/iSpokeToMiho", "N")
        local sFirstMihoForm      = VM_GetVar("Root/Variables/Chapter1/Vendrilstadt/sFirstMihoForm", "S")
        local iMihoKnowsRunestone = VM_GetVar("Root/Variables/Chapter1/Vendrilstadt/iMihoKnowsRunestone", "N")
        
        --Dialogue setup.
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Miho", "Neutral") ]])
        
        --First speech:
        if(iSpokeToMiho == 0.0) then
            
            --Flag.
            VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iSpokeToMiho", "N", 1.0)
            
            --Mei is in the same form as she first met Miho:
            if(sMeiForm == sFirstMihoForm) then
                fnCutscene([[ Append("Miho:[E|Neutral] Oh.[P] You're back.[P] How's it going?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Happy] Hello Miss Miho![P] How's your head?[B][C]") ]])
                fnCutscene([[ Append("Miho:[E|Neutral] Throbbing.[P] Because I woke up in a bed with a doctor staring at my tails, I assume I lost.[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Happy] Yeah, but if it makes you feel better, it was really close.[B][C]") ]])
                fnCutscene([[ Append("Miho:[E|Neutral] It does not.[B][C]") ]])
                fnCutscene([[ Append("Miho:[E|Neutral] But know this, Florentina.[B][C]") ]])
                fnCutscene([[ Append("Miho:[E|Neutral] You have bested me, but I have learned.[P] In the end, that was my goal.[B][C]") ]])
                fnCutscene([[ Append("Miho:[E|Angry] And if your bounty was for something vicious, I'd not be speaking to you so kindly right now.[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Happy] Really?[P] Nobody thinks I'm vicious?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] What exactly is her bounty for?[B][C]") ]])
                fnCutscene([[ Append("Miho:[E|Neutral] Some rich person down east wants you alive and unharmed.[P] Everything I've found in my research points to 'spurned business partner'.[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Neutral] Problem is, there's at least six people I can think of immediately who could have put it out.[P] Oh well.[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Neutral] You know, if you turned me in and I 'escaped', we could split the takings...[B][C]") ]])
                fnCutscene([[ Append("Miho:[E|Neutral] I told you, I am uninterested in the money.[B][C]") ]])
                fnCutscene([[ Append("Miho:[E|Neutral] No matter.[P] What did you want to talk about?[B][C]") ]])
            
            --Mei has changed forms:
            else
                VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iMihoKnowsRunestone", "N", 1.0)
                fnCutscene([[ Append("Miho:[E|Neutral] Oh.[P] You're back.[P] Where did Mei go?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Happy] Hello Miss Miho![P] How's your head?[B][C]") ]])
                fnCutscene([[ Append("Miho:[E|Neutral] Throbbing.[P] So much so that I almost didn't notice your voice sounds exactly the same as Mei's did.[B][C]") ]])
                fnCutscene([[ Append("Miho:[E|Neutral] Just who are you, exactly?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Smirk] It's not a trick, it's me![B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Smirk] Don't worry, I have a magic runestone that lets me transform![P] It's very nifty.[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Neutral] I didn't believe it until I saw it either.[B][C]") ]])
                if(sFirstMihoForm == "Human") then
                    fnCutscene([[ Append("Miho:[E|Neutral] I wouldn't have thought it unusual to get transformed, you didn't need to mention the runestone.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Surprise] It's not a secret.[P] Should it be?[B][C]") ]])
                    fnCutscene([[ Append("Florentina:[E|Neutral] Everything is a secret.[P] Every fact is something that can be traded.[P] You gave it away for nothing.[B][C]") ]])
                    fnCutscene([[ Append("Miho:[E|Neutral] But Florentina, you give away something by treating everything as something to be traded.[P] Something more valuable.[B][C]") ]])
                    fnCutscene([[ Append("Miho:[E|Neutral] Connection.[B][C]") ]])
                    fnCutscene([[ Append("Florentina:[E|Happy] It's not that I didn't know that, I just made that choice.[B][C]") ]])
                    fnCutscene([[ Append("Miho:[E|Neutral] I'm not going to try to argue with you about it, just making sure you know.[B][C]") ]])
                    fnCutscene([[ Append("Florentina:[E|Happy] Well it seems we're all best pals around here now.[B][C]") ]])
                else
                    fnCutscene([[ Append("Miho:[E|Neutral] Very, very interesting.[P] You said you were trying to find a way back home earlier.[B][C]") ]])
                    fnCutscene([[ Append("Miho:[E|Neutral] Are you perhaps one of the people from the other world?[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] It's called Earth.[B][C]") ]])
                    fnCutscene([[ Append("Miho:[E|Neutral] Tch, once again I find a wonder.[P] The elder will want to hear this.[B][C]") ]])
                    fnCutscene([[ Append("Miho:[E|Neutral] Mei, if you don't find a way home, you should visit our village in Trafal.[P] Elder Yukina can probably help you.[B][C]") ]])
                    fnCutscene([[ Append("Miho:[E|Neutral] She is the wisest kitsune, and has seen a great many things.[P] Perhaps she has met someone from this 'Earth' as well.[B][C]") ]])
                    fnCutscene([[ Append("Florentina:[E|Happy] Well it seems we're all best pals around here now.[B][C]") ]])
                end
                
                fnCutscene([[ Append("Miho:[E|Neutral] Do not be so quick to judge my motives, Florentina.[B][C]") ]])
                fnCutscene([[ Append("Miho:[E|Neutral] You have bested me, but I have learned.[P] In the end, that was my goal.[B][C]") ]])
                fnCutscene([[ Append("Miho:[E|Angry] And if your bounty was for something vicious, I'd not be speaking to you so kindly right now.[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Happy] Really?[P] Nobody thinks I'm vicious?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] What exactly is her bounty for?[B][C]") ]])
                fnCutscene([[ Append("Miho:[E|Neutral] Some rich person down east wants you alive and unharmed.[P] Everything I've found in my research points to 'spurned business partner'.[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Neutral] Problem is, there's at least six people I can think of immediately who could have put it out.[P] Oh well.[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Neutral] You know, if you turned me in and I 'escaped', we could split the takings...[B][C]") ]])
                fnCutscene([[ Append("Miho:[E|Neutral] I told you, I am uninterested in the money.[B][C]") ]])
                fnCutscene([[ Append("Miho:[E|Neutral] No matter.[P] What did you want to talk about?[B][C]") ]])
            end
        
        --Repeats:
        else
        
            --Mei is not in the same form as when she first met Miho, and Miho hasn't seen that:
            if(sMeiForm ~= sFirstMihoForm and iMihoKnowsRunestone == 0.0) then
                VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iMihoKnowsRunestone", "N", 1.0)
                fnCutscene([[ Append("Miho:[E|Neutral] Oh.[P] You're back.[P] Where did Mei go?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Happy] Hello Miss Miho![P] How's your head?[B][C]") ]])
                fnCutscene([[ Append("Miho:[E|Neutral] Throbbing.[P] So much so that I almost didn't notice your voice sounds exactly the same as Mei's did.[B][C]") ]])
                fnCutscene([[ Append("Miho:[E|Neutral] Just who are you, exactly?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Smirk] It's not a trick, it's me![B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Smirk] Don't worry, I have a magic runestone that lets me transform![P] It's very nifty.[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Neutral] I didn't believe it until I saw it either.[B][C]") ]])
                if(sFirstMihoForm == "Human") then
                    fnCutscene([[ Append("Miho:[E|Neutral] I wouldn't have thought it unusual to get transformed, you didn't need to mention the runestone.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Surprise] It's not a secret.[P] Should it be?[B][C]") ]])
                    fnCutscene([[ Append("Florentina:[E|Neutral] Everything is a secret.[P] Every fact is something that can be traded.[P] You gave it away for nothing.[B][C]") ]])
                    fnCutscene([[ Append("Miho:[E|Neutral] But Florentina, you give away something by treating everything as something to be traded.[P] Something more valuable.[B][C]") ]])
                    fnCutscene([[ Append("Miho:[E|Neutral] Connection.[B][C]") ]])
                    fnCutscene([[ Append("Florentina:[E|Happy] It's not that I didn't know that, I just made that choice.[B][C]") ]])
                    fnCutscene([[ Append("Miho:[E|Neutral] I'm not going to try to argue with you about it, just making sure you know.[B][C]") ]])
                else
                    fnCutscene([[ Append("Miho:[E|Neutral] Very, very interesting.[P] You said you were trying to find a way back home earlier.[B][C]") ]])
                    fnCutscene([[ Append("Miho:[E|Neutral] Are you perhaps one of the people from the other world?[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] It's called Earth.[B][C]") ]])
                    fnCutscene([[ Append("Miho:[E|Neutral] Tch, once again I find a wonder.[P] The elder will want to hear this.[B][C]") ]])
                    fnCutscene([[ Append("Miho:[E|Neutral] Mei, if you don't find a way home, you should visit our village in Trafal.[P] Elder Yukina can probably help you.[B][C]") ]])
                    fnCutscene([[ Append("Miho:[E|Neutral] She is the wisest kitsune, and has seen a great many things.[P] Perhaps she has met someone from this 'Earth' as well.[B][C]") ]])
                end
                fnCutscene([[ Append("Miho:[E|Neutral] Now I take it you didn't come here just to show me that?[B][C]") ]])

            --Normal case:
            else
                fnCutscene([[ Append("Miho:[E|Neutral] Mei.[P] Florentina.[P] What's on your mind?[B][C]") ]])
            end
        end
        
        --Activate topics mode once the dialogue is complete.
        fnCutscene([[ WD_SetProperty("Activate Topics After Dialogue", "Miho") ]])
        
    elseif(sActorName == "PatronA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Patron:[VOICE|HumanF0] Hey cutie![P] Pull up a chair!")  ]])
        
    elseif(sActorName == "PatronB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Patron:[VOICE|Werecat] Work, make money, spend money on drink to forget I work, repeat.[P] Life is great!")  ]])
        
    elseif(sActorName == "PatronC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Patron:[VOICE|HumanM1] What even is in sausages?[P] How can you tell?")  ]])
        
    elseif(sActorName == "PatronD") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Mercenary:[VOICE|HumanF0] I like axes because they're big and circular, which is what weapons should be.[P] None of this pointy crap.")  ]])
        
    elseif(sActorName == "PatronE") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Mercenary:[VOICE|HumanF0] So then I said, 'It can't be bigger than a goat' and let me tell you, he was![P] I was walking funny for a week!")  ]])
        
    elseif(sActorName == "PatronF") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Mercenary:[VOICE|HumanM0] How is this appropriate dinner conversation?")  ]])
        
    elseif(sActorName == "PatronG") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Mercenary:[VOICE|HumanF0] I don't even know what astrology is, but when I ask 'what's your sign' people just hang their heads.[P] Am I doing something wrong?")  ]])
    end
end
