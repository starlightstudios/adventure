-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= Exits ========================================== ]|
if(sObjectName == "SampleExit") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("LevelName", "FORCEPOS:28.0x16.0x0")
    
-- |[ ====================================== Examinables ======================================= ]|
elseif(sObjectName == "FoodshelfA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The shelf has some animal feed.[P] Raisins!)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "FoodshelfB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Dried corn and jerky.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "FoodshelfC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The food doesn't look edible.[P] I'm not getting closer to find out what it is.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "FoodshelfD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Oooh, veggie samosas![P] Whoever's shelf this is is eating good tonight!)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "FoodshelfE") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Dried noodles in little leather sacks.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "FoodshelfF") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Dried fruit.[P] Raisins, apricots, watermelons -[P] how do you dry a watermelon?)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "FoodshelfG") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Hard tack.[P] The kind of bread you eat when spoilage takes priority over flavour.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "FoodshelfH") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Bottles full of various drinks, with no labels.[P] I guess the owner knows what's what.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "FoodshelfI") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (All kinds of foods for travel, available for a reasonable price!)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "FoodshelfJ") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Stocks of dried foods for travel, presumably ready to be loaded into barrels for the ships.)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "BagA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The bag has corn kernels in it.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BagB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A bag of assorted crystals, not cut or polished.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BagC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Crushed up pieces of paper.[P] Nothing is written on them, I think these were failed origami attempts.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BagD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (There's make-up supplies in the bag.[P] Either the owner needs to look their best, or it's for sale.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BagE") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The bag has fish bones in it.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BagF") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Carving tools and some bones to practice on.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BagG") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The bag is empty.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookshelfA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('The alraune threw her heaving bosom across the heaving bosom of the mermaid, who kissed her.[P] Then the werewolf placed her heaving bosom on the alraune's back and began to paw at her, playfully.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('From nowhere, a shriek was heard.[P] A harpy, with a heaving bosom, was above them![P] She flew down and began to - '[P] It goes on like this.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookshelfB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Magical Settlement, The Collected Criticism'.[P] Apparently, there's an entire genre of non-fiction dedicated to analyzing this one literary series.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookshelfC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('How to Get Pack Animals to Follow You'.[P] There are hundreds of pages about the topic.[P] I don't have time to read them all, but how complex can it be?)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookshelfD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('The Metalites Are Real!'.[P] The book contains a bunch of conspiracy rambling about metal girls who abduct people and take them to the moon.[P] Looks pretty unhinged.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookshelfE") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('So You Want to Fuck Fish'.[P] It seems to be a guide to dating mermaids.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookshelfF") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Shiny Scales, a Guide to Underwater Fashion'.[P] Seems the mermaid bikini top is ever popular.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookshelfG") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A bunch of navigational charts and river maps.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "SignA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Market Square')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "SignB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Equipment Shop')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "SignC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Cabin Rentals::[P] Inquire Within!')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "SignD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('La D'amarrage')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "SignE") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Please do not loiter on the docks')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "SignF") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Day labourers, please see Daisy to apply!')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "SignG") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Competitive pay![P] Apply today![P] Talk to Rose.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "SignH") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Coming Soon::[P] Vendrilstadt Paper Mill')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "NoteA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Abandoned items.[P] If you can carry it, you can take it.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "EmptyBarrel") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The barrel is empty.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BeerBarrel") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The barrel is full of golden beer.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "WaterBarrel") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The barrel is full of clean water.)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "WineBarrel") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The barrel is full of wine.[P] Smells strong!)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "StorageBarrel") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The barrel's lid is sealed tight.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "FishBarrel") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A barrel of salted fish.[P] It's like eating a brick!)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "VinegarBarrel") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The barrel smells like it's full of vinegar.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "MilkBarrel") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The barrel is full of milk.[P] Looks like it's going to be made into cheese.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "JuiceBarrel") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Lemon juice![P] Helps fight scurvy and adds some zip to your rice!)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "MihoSleep") then
    local iMihoUnconscious = VM_GetVar("Root/Variables/Chapter1/Vendrilstadt/iMihoUnconscious", "N")
    if(iMihoUnconscious > 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Miho is sleeping like a baby.)") ]])
        fnCutsceneBlocker()
    end

elseif(sObjectName == "HatMannequin") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A mannequin modelling a maid headpiece.)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "JewelBag") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A bag full of assorted gems.[P] Not magical, but boy are they pretty!)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "DressMannequin") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A mannequin modelling a stunning red dress.[P] That's got to be out of my price range.)") ]])
    fnCutsceneBlocker()

-- |[ ========================================== Other ========================================= ]|
-- |[Skillbook]|
elseif(sObjectName == "SkillbookA") then
    AL_SetProperty("Set Layer Disabled", "SkillbookIcoA", true)
	LM_ExecuteScript(gsFlorentinaSkillbook, 5)
    
elseif(sObjectName == "SkillbookB") then
    AL_SetProperty("Set Layer Disabled", "SkillbookIcoB", true)
	LM_ExecuteScript(gsMeiSkillbook, 6)
    
-- |[ ========================================== Debug ========================================= ]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
