-- |[ ============================= Trannadar Equipment Shop Setup ============================= ]|
--Sells some basic equipment.

-- |[Item Stock Listing]|
--Adamantite
AM_SetShopProperty("Add Item", "Adamantite Powder",           -1, -1)
AM_SetShopProperty("Add Item", "Rusty Katana",                -1, -1)
AM_SetShopProperty("Add Item", "Steel Katana",                -1, -1)
AM_SetShopProperty("Add Item", "Blackjack",                   -1, -1)
AM_SetShopProperty("Add Item", "Troubadour's Robe",           -1, -1)
AM_SetShopProperty("Add Item", "Striker's Tunic",             -1, -1)
AM_SetShopProperty("Add Item", "Light Leather Vest",          -1, -1)
AM_SetShopProperty("Add Item", "Decorative Bracer",           -1, -1)
AM_SetShopProperty("Add Item", "Swamp Boots",                 -1, -1)
AM_SetShopProperty("Add Item", "Healing Tincture",            -1, -1)
AM_SetShopProperty("Add Item", "Palliative",                  -1, -1)
AM_SetShopProperty("Add Item", "Smelling Salts",              -1, -1)
AM_SetShopProperty("Add Item", "Controversial Sugar Cookies", -1, -1)
AM_SetShopProperty("Add Item", "Infused Rye Bread",           -1, -1)
AM_SetShopProperty("Add Item", "Sparkling Sourdough",         -1, -1)
AM_SetShopProperty("Add Item", "Bubbling Healing Tincture",   -1, -1)
