-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= Exits ========================================== ]|
if(sObjectName == "SampleExit") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("LevelName", "FORCEPOS:28.0x16.0x0")
    
-- |[ ====================================== Examinables ======================================= ]|
elseif(sObjectName == "Barrels") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Whatever was in these barrels, it's all rubber now.)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "MechLockedDoorA") then
    local iOpenedTowerLatch = VM_GetVar("Root/Variables/Chapter1/Scenes/iOpenedTowerLatch", "N")
    if(iOpenedTowerLatch == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (It won't open, there's no locking mechanism.[P] Must be a switch somewhere.)") ]])
        fnCutsceneBlocker()
    else
        AL_SetProperty("Open Door", "MechLockedDoorA")
        AudioManager_PlaySound("World|FlipSwitch")
    end

elseif(sObjectName == "MechLockedDoorB") then
    local iOpenedTowerLatch = VM_GetVar("Root/Variables/Chapter1/Scenes/iOpenedTowerLatch", "N")
    if(iOpenedTowerLatch == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (It won't open, there's no locking mechanism.[P] Must be a switch somewhere.)") ]])
        fnCutsceneBlocker()
    else
        AL_SetProperty("Open Door", "MechLockedDoorB")
        AudioManager_PlaySound("World|FlipSwitch")
    end

elseif(sObjectName == "BookshelfA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('The Life and Times of Charlotte Whey'.[P] A biography of a fisherwoman.[P] She gets married seventeen times and all her husbands and wives die horribly.[P] Not a very happy story...)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "BookshelfB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Things I Regret Inhaling' by Jack of Soswitch.[P] It's just a list of things he evidently didn't enjoy inhaling, like banana peppers and soot.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookshelfC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Famous Names in Sturnheim'.[P] Victoria and Evelyn are at the top of the list since the ruling families have seven Victorias and five Evelyns between them.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookshelfD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (It's a list of caravans that the bandits here ambushed and who got which loot.[P] Blythe will probably be interested at some point.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookshelfE") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A personnel list of guards at the trading post.[P] Seems the bandits were keeping tabs on them.)") ]])
    fnCutsceneBlocker()

-- |[ ========================================== Other ========================================= ]|
-- |[ ========================================== Debug ========================================= ]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
