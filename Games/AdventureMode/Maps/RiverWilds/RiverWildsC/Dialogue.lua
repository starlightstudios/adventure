-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    --Common.
    local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
    
    if(sActorName == "MonsterA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Werecat:[VOICE|Werecat] If you think I'm going over there, think again!") ]])
    elseif(sActorName == "MonsterB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        if(sMeiForm ~= "Bee") then
            fnCutscene([[ Append("(The bee fidgets nervously in place.)") ]])
        else
            fnCutscene([[ Append("Bee:[VOICE|Bee] (Pleazz be careful, drone.)") ]])
        end
    elseif(sActorName == "MonsterC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Alraune:[VOICE|Alraune] You have boundless courage if you're going over there.[P] Take no risks.") ]])
    end
end
