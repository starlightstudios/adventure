-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
if(sObjectName == "MonsterSceneA" or sObjectName == "MonsterSceneB") then

    --Repeat check.
    local iSawRiverParty = VM_GetVar("Root/Variables/Chapter1/Scenes/iSawRiverParty", "N")
    if(iSawRiverParty == 1.0) then return end
    
    --Variables.
    local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
    local iSawMediatorFight = VM_GetVar("Root/Variables/Chapter1/Scenes/iSawMediatorFight", "N")
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter1/Scenes/iSawRiverParty", "N", 1.0)
    
    --Rubber quest increments if below this value.
    local iRubberQuestState = VM_GetVar("Root/Variables/Chapter1/Scenes/iRubberQuestState", "N")
    if(iRubberQuestState < 2.0) then
        VM_SetVar("Root/Variables/Chapter1/Scenes/iRubberQuestState", "N", 2.0)
    end
    
    --Movement.
    fnCutsceneMergeParty()
    fnCutsceneBlocker()
    if(sObjectName == "MonsterSceneA") then
        fnCutsceneMove("Mei", 58.25, 48.50)
        fnCutsceneMove("Florentina", 59.25, 48.50)
        fnCutsceneBlocker()
        fnCutsceneMove("Mei", 58.25, 43.50)
        fnCutsceneFace("Mei", -1, -1)
        fnCutsceneMove("Florentina", 59.25, 43.50)
        fnCutsceneFace("Florentina", -1, -1)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneFace("MonsterA", 0, 1)
        fnCutsceneFace("MonsterB", 0, 1)
        fnCutsceneFace("MonsterC", 1, 0)
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    else
        fnCutsceneMove("Mei", 62.25, 44.50)
        fnCutsceneMove("Florentina", 63.25, 44.50)
        fnCutsceneBlocker()
        fnCutsceneMove("Mei", 58.25, 43.50)
        fnCutsceneFace("Mei", -1, -1)
        fnCutsceneMove("Florentina", 59.25, 43.50)
        fnCutsceneFace("Florentina", -1, -1)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneFace("MonsterA", 0, 1)
        fnCutsceneFace("MonsterB", 0, 1)
        fnCutsceneFace("MonsterC", 1, 0)
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    end
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Werecat", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Alraune", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Bee", "Neutral") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] Hey eggheads, what's got into you?[P] Has our reputation preceded us?[P] Too scared to fight?[B][C]") ]])
    fnCutscene([[ Append("Alraune:[E|Neutral] Hush, Florentina.[P] This is serious.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Happy] Don't flatter me by knowing my name all the way out here, you'll make me think I'm famous![B][C]") ]])
    fnCutscene([[ Append("Werecat:[E|Neutral] You know this jabbermouth?[B][C]") ]])
    fnCutscene([[ Append("Alraune:[E|Neutral] Know of her, yes.[P] Rochea's coven speaks of her.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] Mei, let's kick their asses.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Offended] How about no, you crazy egomaniac?[P] Can't you see something is wrong?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] Yeah, there's no footprints on their rumps.[B][C]") ]])
    if(sMeiForm == "Bee") then
        fnCutscene([[ Append("Mei:[E|Neutral] I apologize, drone.[P] Erm, you're not from my hive, are you?[B][C]") ]])
        fnCutscene([[ Append("Bee:[E|Neutral] Zzzz.[P] Bzzz zzzbzz. (No, sorry.[P] Will you help anyway?)[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] Seems I can understand you well enough.[P] What's going on?[B][C]") ]])
    elseif(sMeiForm == "Alraune") then
        fnCutscene([[ Append("Mei:[E|Neutral] I apologize, leaf-sister.[P] Florentina is a bit brash.[B][C]") ]])
        fnCutscene([[ Append("Alraune:[E|Neutral] Ah, are you from Rochea's coven?[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Confused] She isn't.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] It's complicated.[P] What's going on?[B][C]") ]])
    elseif(sMeiForm == "Werecat") then
        fnCutscene([[ Append("Mei:[E|Neutral] My friend does have a point, kinfang.[P] Is there some reason you're conspiring with these two?[P] Are they your friends?[B][C]") ]])
        fnCutscene([[ Append("Werecat:[E|Neutral] Nothing of the sort![P] But, you should know, don't hunt alraunes, or bees.[P] That's some free advice, they come in big groups.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Happy] This cat knows what's up.[P] Don't mess with alraunes, sucker![B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] So then what's going on?[B][C]") ]])
    else
        fnCutscene([[ Append("Mei:[E|Neutral] I apologize for my friend here, but it is kind of odd you're not trying to beat us up.[B][C]") ]])
        fnCutscene([[ Append("Alraune:[E|Neutral] Normally, I would be happy to educate you in the ways of nature![B][C]") ]])
        fnCutscene([[ Append("Bee:[E|Neutral] Zzzz![B][C]") ]])
        fnCutscene([[ Append("Werecat:[E|Neutral] Are you kidding?[P] Look at that sword, that sword has stomped faces![B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Happy] I think this cat and I would get along well.[B][C]") ]])
        fnCutscene([[ Append("Alraune:[E|Neutral] She mixes metaphors a tad too often for my taste, but...[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] So then what's going on?[B][C]") ]])
    end
    fnCutscene([[ Append("Alraune:[E|Neutral] Take a look across that river.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Camera.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 2.0)
        CameraEvent_SetProperty("Focus Position", (50.25 * gciSizePerTile), (32.50 * gciSizePerTile))
    DL_PopActiveObject()
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 2.0)
        CameraEvent_SetProperty("Focus Actor Name", "Mei")
    DL_PopActiveObject()
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Werecat", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Alraune", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Bee", "Neutral") ]])
    fnCutscene([[ Append("Mei:[E|Sad] Is it me, or do they look [P]*alarmingly*[P] happy?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] Dunno, they seem to be doing what you're doing, and not fighting.[P] Are you just doing the world's crappiest counter-protest?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] If they're getting along, you ought not to stick your nose in their business.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Smirk] Who are you and what have you done with Florentina?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Happy] My love of tolerance is more than my love of violence.[P] Particularly as peace is conducive to trade.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Smirk] Money is higher than fighting on the list, got it.[B][C]") ]])
    fnCutscene([[ Append("Alraune:[E|Neutral] No no, look closer at them.[P] Not just their odd cooperation, but their skin.[B][C]") ]])
    fnCutscene([[ Append("Werecat:[E|Neutral] It's way too shiny, and their faces are flat.[P] Look, look![B][C]") ]])
    fnCutscene([[ Append("Alraune:[E|Neutral] The little ones are quiet as well, wherever that...[P] stuff...[P] goes.[P] And those poor souls seem to be spreading it.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] Hold on...[B][C]") ]])
    if(sMeiForm == "Alraune") then
        fnCutscene([[ Append("Mei:[E|Sad] Yeah, the little ones are saying the same thing.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Confused] I suddenly don't like this.[B][C]") ]])
    else
        fnCutscene([[ Append("Mei:[E|Neutral] What are you doing?[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Confused] I asked the flowers if these weirdos are for real, and they said they are.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Confused] This doesn't look like a prank.[B][C]") ]])
    end
    if(sMeiForm == "Bee") then
        fnCutscene([[ Append("Mei:[E|Neutral] But what are we going to do?[B][C]") ]])
        fnCutscene([[ Append("Bee:[E|Neutral] Zzzz![P] (That's what we were trying to figure out!)[B][C]") ]])
    else
        fnCutscene([[ Append("Mei:[E|Neutral] But what are we going to do?[B][C]") ]])
        fnCutscene([[ Append("Bee:[E|Neutral] Zzzz![B][C]") ]])
        fnCutscene([[ Append("Alraune:[E|Neutral] We've been trying to figure precisely that.[B][C]") ]])
    end
    fnCutscene([[ Append("Alraune:[E|Neutral] It seems that...[P] stuff...[P] cannot cross the water.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] Have you checked?[B][C]") ]])
    fnCutscene([[ Append("Werecat:[E|Neutral] No way in hell am I going over there![P] I saw it![B][C]") ]])
    fnCutscene([[ Append("Werecat:[E|Neutral] Touch too much of that stuff and you'll be just like them![B][C]") ]])
    fnCutscene([[ Append("Alraune:[E|Neutral] Please, don't panic them.[P] We aren't certain of that.[B][C]") ]])
    fnCutscene([[ Append("Werecat:[E|Neutral] I don't care if you believe me, but I ain't going over there.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] Well - [P][CLEAR]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] Hold up a second, Mei.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] You're about to volunteer to heroically find out what's going on over there.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] I - [P][CLEAR]") ]])
    fnCutscene([[ Append("Florentina:[E|Happy] Oh no, we couldn't accept a reward![B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] We - [P][CLEAR]") ]])
    fnCutscene([[ Append("Florentina:[E|Surprise] I'm sure it will be dangerous, but I'm not afraid![P] We have to do what's right![B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Offended] Oh for pete's sake![P] That wasn't what I was going to say at all![B][C]") ]])
    fnCutscene([[ Append("Alraune:[E|Neutral] My goodness.[P] Mei?[P] Your name is Mei?[B][C]") ]])
    fnCutscene([[ Append("Alraune:[E|Neutral] You are a true hero.[B][C]") ]])
    fnCutscene([[ Append("Werecat:[E|Neutral] Incredibly brave.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Offended] Hey![B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Offended] Florentina, now look what you've done![P] We have to go or else I'll look like a jerk![B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] Yeah, a fancy bit of work, isn't it?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] Can you dopes give us any information before we check it out?[B][C]") ]])
    fnCutscene([[ Append("Werecat:[E|Neutral] I heard from one of the other cats that there's some kinda tower up north.[P] Might want to see if its related.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] Yeah that's Dormine Tower.[P] Old draconic ruin.[P] Last I heard it had a bandit problem.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] Guess that's where we have to check next.[P] Can you three make sure nobody tries to get across the river?[B][C]") ]])
    fnCutscene([[ Append("Alraune:[E|Neutral] It's the least we can do.[P] Please be safe, hero.[B][C]") ]])
    fnCutscene([[ Append("Werecat:[E|Neutral] So brave...[P] brings a tear to my eye...[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Offended] Oh stop it already!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnAutoFoldParty()
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Bonus:
    if(iSawMediatorFight == 1.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        fnCutscene([[ Append("Florentina:[E|Neutral] *Pssst, Mei.*[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] *What?*[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] *Keep your voice down.*[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Confused] *That stuff over there.[P] You think that's what Stana was talking about?*[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Sad] *She said something about their skin being shiny.[P] I think this might be the stuff.*[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Confused] *Great.[P] If we're going over there, keep your weapon ready and your ears open.*") ]])
        fnCutsceneBlocker()
    end

-- |[You First]|
elseif(sObjectName == "YouFirst") then

    --Repeat check.
    local iSawYouFirst = VM_GetVar("Root/Variables/Chapter1/Scenes/iSawYouFirst", "N")
    if(iSawYouFirst == 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter1/Scenes/iSawYouFirst", "N", 1.0)
    
    --Merge.
    fnCutsceneMergeParty()
    fnCutsceneBlocker()
    
    --Position.
    fnCutsceneMove("Mei", 18.25, 59.50)
    fnCutsceneFace("Mei", 0, -1)
    fnCutsceneMove("Florentina", 19.25, 59.50)
    fnCutsceneFace("Florentina", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("Mei:[E|Neutral] So this is the stuff, huh.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] Yep.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] Step on it, see what happens.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Offended] What!?[P] No way, you step on it![B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] Me?[P] No no, this stuff is calling to you![P] It was practically made for you to step on it![B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Offended] But what if it grabs me![B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] But what if it grabs [P]*me*[P]?[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Offended] Oh for crying out loud!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Mei", 18.25, 58.50, 0.10)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneMove("Mei", 18.25, 57.50, 0.10)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Mei", 0, 1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("Florentina:[E|Neutral] Safe?[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Offended] Seems inert, you pansy.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] I am mostly poppy, thank you.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] I guess we only need to watch out for the afflicted forest creatures.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] Okay, but don't take any chances around this stuff.[P] If it starts to bubble, move your ass.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Fold.
    fnAutoFoldParty()
    fnCutsceneBlocker()

end
