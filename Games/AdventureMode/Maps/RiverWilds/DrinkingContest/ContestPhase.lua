-- |[ ================================= Drinking Contest Phase ================================= ]|
--Changes the phase when Mei has delivered the drinks.
local iDrinkingPhase = VM_GetVar("Root/Variables/Chapter1/Vendrilstadt/iDrinkingPhase", "N")

-- |[ ========================================= Phase 1 ======================================== ]|
--First drink!
if(iDrinkingPhase == 1.0) then
    
    --Camera lock.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 5.0)
        CameraEvent_SetProperty("Focus Position", (15.25 * gciSizePerTile), (12.50 * gciSizePerTile))
    DL_PopActiveObject()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Olivia:[VOICE|MercF] Trading time![P] Make it snappy, lads and lassies!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Mei moves to the middle.
    fnCutsceneMove("Mei", 15.25,  8.50)
    fnCutsceneMove("Mei", 15.25, 11.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Florentina and Miho.
    fnCutsceneFace("Mei", -1, 0)
    fnCutsceneFace("Miho", 1, 0)
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Florentina:[VOICE|Florentina] Trade with Barry.[B][C]") ]])
    fnCutscene([[ Append("Miho:[VOICE|Miho] No trades.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Mei zips over!
    fnCutscene([[ AudioManager_PlaySound("World|GlassMove") ]])
    fnCutsceneFace("Mei", 0, -1)
    fnCutsceneTeleport("Mei", 12.25, 11.00)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|GlassMove") ]])
    fnCutsceneFace("Mei", 0, -1)
    fnCutsceneTeleport("Mei", 18.25, 11.00)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|GlassMove") ]])
    fnCutsceneFace("Mei", 0, -1)
    fnCutsceneTeleport("Mei", 12.25, 11.00)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Mei", -1, 0)
    fnCutsceneTeleport("Mei", 15.25, 11.50)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Miho", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Florentina", "Surprise") ]])
    fnCutscene([[ Append("Mei:[E|Happy] Next![B][C]") ]])
    fnCutscene([[ Append("Miho:[E|Neutral] What the fox?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Surprise] Where'd you get that speed, Mei?[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Smirk] Professional waitress powers![P] Next!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Barry and Bette.
    fnCutsceneFace("PlayerA", -1, 0)
    fnCutsceneFace("PlayerB", -1, 0)
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Barry:[VOICE|MercM] No trade.[B][C]") ]])
    fnCutscene([[ Append("Better:[VOICE|MercF] Trade with Lore!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Mei zips over!
    fnCutscene([[ AudioManager_PlaySound("World|GlassMove") ]])
    fnCutsceneFace("Mei", 0, -1)
    fnCutsceneTeleport("Mei", 18.25, 11.00)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|GlassMove") ]])
    fnCutsceneFace("Mei", 0, 1)
    fnCutsceneTeleport("Mei", 18.25, 13.50)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|GlassMove") ]])
    fnCutsceneFace("Mei", 0, -1)
    fnCutsceneTeleport("Mei", 18.25, 11.00)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("PlayerA", 1, 0)
    fnCutsceneFace("Mei", 1, 0)
    fnCutsceneTeleport("Mei", 15.25, 11.50)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Cris and Nancy.
    fnCutsceneFace("PlayerC", 1, 0)
    fnCutsceneFace("PlayerD", 1, 0)
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Mei:[VOICE|Mei] Next![B][C]") ]])
    fnCutscene([[ Append("Cris:[VOICE|HumanF0] Trade with Nancy.[B][C]") ]])
    fnCutscene([[ Append("Nancy:[VOICE|HumanF1] Trade with Miho.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Mei zips over!
    fnCutscene([[ AudioManager_PlaySound("World|GlassMove") ]])
    fnCutsceneFace("Mei", 0, -1)
    fnCutsceneTeleport("Mei", 12.25, 11.00)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|GlassMove") ]])
    fnCutsceneFace("Mei", 0, 1)
    fnCutsceneTeleport("Mei", 12.25, 13.50)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|GlassMove") ]])
    fnCutsceneFace("Mei", 0, -1)
    fnCutsceneTeleport("Mei", 12.25, 11.00)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("PlayerD", -1, 0)
    fnCutsceneFace("Mei", 1, 0)
    fnCutsceneTeleport("Mei", 15.25, 11.50)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Ira and Lore.
    fnCutsceneFace("PlayerE", -1, 0)
    fnCutsceneFace("PlayerF", -1, 0)
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Mei:[VOICE|Mei] Next![B][C]") ]])
    fnCutscene([[ Append("Ira:[VOICE|Hypatia] No trades.[B][C]") ]])
    fnCutscene([[ Append("Lore:[VOICE|Alicia] No trades.[B][C]") ]])
    fnCutscene([[ Append("Mei:[VOICE|Mei] Anyone else?[B][C]") ]])
    fnCutscene([[ Append("Mei:[VOICE|Mei] Going, going, gone!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    fnCutsceneFace("PlayerE", 1, 0)
    fnCutsceneFace("PlayerF", -1, 0)
    
    --Drinking time!
    fnCutscene([[ VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iJumpMode", "N", 1.0) ]])
    fnCutscene([[ VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iDrinkTimer", "N", 0.0) ]])
    fnCutsceneLayerDisabled("DrinkAL", true)
    fnCutsceneLayerDisabled("DrinkBL", true)
    fnCutsceneLayerDisabled("DrinkCL", true)
    fnCutsceneLayerDisabled("DrinkDL", true)
    fnCutsceneLayerDisabled("DrinkEL", true)
    fnCutsceneLayerDisabled("DrinkFL", true)
    fnCutsceneLayerDisabled("DrinkGL", true)
    fnCutsceneLayerDisabled("DrinkHL", true)
    fnCutsceneLayerDisabled("DrinkAU", true)
    fnCutsceneLayerDisabled("DrinkBU", true)
    fnCutsceneLayerDisabled("DrinkCU", true)
    fnCutsceneLayerDisabled("DrinkDU", true)
    fnCutsceneLayerDisabled("DrinkEU", true)
    fnCutsceneLayerDisabled("DrinkFU", true)
    fnCutsceneLayerDisabled("DrinkGU", true)
    fnCutsceneLayerDisabled("DrinkHU", true)
    fnCutsceneLayerDisabled("DrinkLower", false)
    fnCutsceneLayerDisabled("DrinkUpper", false)
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Everyone:[VOICE|Crowd] Droobly[P] Chug[P] Chug[P] Chug![B][C]") ]])
    fnCutscene([[ Append("Everyone:[VOICE|Crowd] Yeaaahhh!!!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iJumpMode", "N", 0.0) ]])
    
    --Reset flags.
    fnCutsceneFace("Florentina", 1, 0)
    fnCutsceneFace("Miho", -1, 0)
    fnCutsceneFace("PlayerA", 1, 0)
    fnCutsceneFace("PlayerB", -1, 0)
    fnCutsceneFace("PlayerC", 1, 0)
    fnCutsceneFace("PlayerD", -1, 0)
    fnCutsceneFace("PlayerE", 1, 0)
    fnCutsceneFace("PlayerF", -1, 0)
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Olivia:[VOICE|MercF] [SOUND|World|GlassMove]Place orders![P] Next round is up![P] Droobly Drunks Chug!") ]])
    fnCutsceneBlocker()
    
-- |[ ========================================= Phase 2 ======================================== ]|
--Barry is out quick.
elseif(iDrinkingPhase == 2.0) then
    
    -- |[Function Setup]|
    gsaDrinkNameList = {"Florentina", "Miho", "PlayerA", "PlayerB", "PlayerC", "PlayerD", "PlayerE", "PlayerF"}
    gsaDrinkXList    = {       12.25,  12.25,     18.25,     18.25,     12.25,     12.25,     18.25,     18.25}
    gsaDrinkYList    = {       11.00,  11.00,     11.00,     11.00,     13.50,     13.50,     13.50,     13.50}
    local function fnRandomTable()
        
        --Figure out how to move.
        local iRoll = LM_GetRandomNumber(1, 8)
        local sCharName = gsaDrinkNameList[iRoll]
        
        --First four characters face down.
        if(iRoll <= 4) then
            fnCutsceneFace(sCharName, 0, 1)
        --Otherwise, face up.
        else
            fnCutsceneFace(sCharName, 0, -1)
        end
        
        --Mei teleports to their table.
        fnCutscene([[ AudioManager_PlaySound("World|GlassMove") ]])
        fnCutsceneTeleport("Mei", gsaDrinkXList[iRoll], gsaDrinkYList[iRoll])
        if(gsaDrinkYList[iRoll] < 12.0) then
            fnCutsceneFace("Mei", 0, -1)
        else
            fnCutsceneFace("Mei", 0, 1)
        end
        fnCutsceneWait(10)
        fnCutsceneBlocker()
        
        --Mei teleports to another table. This is done by adding 2-6 to the roll, using modulus if needed.
        local iModRoll = iRoll + LM_GetRandomNumber(2, 6)
        if(iModRoll > 8) then iModRoll = iModRoll - 8 end
        fnCutscene([[ AudioManager_PlaySound("World|GlassMove") ]])
        fnCutsceneTeleport("Mei", gsaDrinkXList[iModRoll], gsaDrinkYList[iModRoll])
        if(gsaDrinkYList[iModRoll] < 12.0) then
            fnCutsceneFace("Mei", 0, -1)
        else
            fnCutsceneFace("Mei", 0, 1)
        end
        fnCutsceneWait(10)
        fnCutsceneBlocker()
        
        --Everyone returns to their original facing.
        fnCutsceneFace("Florentina", 1, 0)
        fnCutsceneFace("Miho",      -1, 0)
        fnCutsceneFace("PlayerA",    1, 0)
        fnCutsceneFace("PlayerB",   -1, 0)
        fnCutsceneFace("PlayerC",    1, 0)
        fnCutsceneFace("PlayerD",   -1, 0)
        fnCutsceneFace("PlayerE",    1, 0)
        fnCutsceneFace("PlayerF",   -1, 0)
    end
    
    --Camera lock.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 5.0)
        CameraEvent_SetProperty("Focus Position", (15.25 * gciSizePerTile), (12.50 * gciSizePerTile))
    DL_PopActiveObject()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Olivia:[VOICE|MercF] Trading time![P] Quick as you like!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Mei moves to the middle.
    fnCutsceneMove("Mei", 15.25,  8.50)
    fnCutsceneMove("Mei", 15.25, 11.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Characters change orientations at random and Mei teleports a bunch of times.
    for i = 1, 10, 1 do
        fnRandomTable()
    end
    
    --Mei teleports back to the middle.
    fnCutsceneTeleport("Mei", 15.25, 12.50)
    fnCutsceneFace("Mei", 0, 1)
    
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Mei:[VOICE|Mei] Anyone else?[P] No?[P] Droobly time![P] Wheee!!") ]])
    fnCutsceneBlocker()
    
    --Drinking time!
    fnCutscene([[ VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iJumpMode", "N", 1.0) ]])
    fnCutscene([[ VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iDrinkTimer", "N", 0.0) ]])
    fnCutsceneLayerDisabled("DrinkAL", true)
    fnCutsceneLayerDisabled("DrinkBL", true)
    fnCutsceneLayerDisabled("DrinkCL", true)
    fnCutsceneLayerDisabled("DrinkDL", true)
    fnCutsceneLayerDisabled("DrinkEL", true)
    fnCutsceneLayerDisabled("DrinkFL", true)
    fnCutsceneLayerDisabled("DrinkGL", true)
    fnCutsceneLayerDisabled("DrinkHL", true)
    fnCutsceneLayerDisabled("DrinkAU", true)
    fnCutsceneLayerDisabled("DrinkBU", true)
    fnCutsceneLayerDisabled("DrinkCU", true)
    fnCutsceneLayerDisabled("DrinkDU", true)
    fnCutsceneLayerDisabled("DrinkEU", true)
    fnCutsceneLayerDisabled("DrinkFU", true)
    fnCutsceneLayerDisabled("DrinkGU", true)
    fnCutsceneLayerDisabled("DrinkHU", true)
    fnCutsceneLayerDisabled("DrinkLower", false)
    fnCutsceneLayerDisabled("DrinkUpper", false)
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Everyone:[VOICE|Crowd] Droobly[P] Chug[P] Chug[P] Chug![B][C]") ]])
    fnCutscene([[ Append("Everyone:[VOICE|Crowd] Woooooo drink drink!!!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iJumpMode", "N", 0.0) ]])
    
    --Reset flags.
    fnCutsceneFace("Florentina", 1, 0)
    fnCutsceneFace("Miho", -1, 0)
    fnCutsceneFace("PlayerA", 1, 0)
    fnCutsceneFace("PlayerB", -1, 0)
    fnCutsceneFace("PlayerC", 1, 0)
    fnCutsceneFace("PlayerD", -1, 0)
    fnCutsceneFace("PlayerE", 1, 0)
    fnCutsceneFace("PlayerF", -1, 0)
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Olivia:[VOICE|MercF] [SOUND|World|GlassMove]Place orders![P] Keep it going!") ]])
    fnCutsceneBlocker()
    fnCutscene([[ VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iDontEditPlayerStates", "N", 1.0) ]])
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("PlayerA", 0, 1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Barry:[VOICE|MercM] Ooooh, not doing so good...[B][C]") ]])
    fnCutscene([[ Append("Florentina:[VOICE|Florentina] Already?[P] That was the second round![B][C]") ]])
    fnCutscene([[ Append("Barry:[VOICE|MercM] Oooohh going...[P] down...") ]])
    fnCutsceneBlocker()
    fnCutsceneFace("Mei", 1, -1)
    fnCutsceneFace("Miho", 1, 0)
    fnCutsceneFace("PlayerC", 0, -1)
    fnCutsceneFace("PlayerD", 0, -1)
    fnCutsceneFace("PlayerE", 0, -1)
    fnCutsceneFace("PlayerF", 0, -1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneMoveFace("PlayerA", 15.75, 10.50, 0, 1, 0.20)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
    fnCutsceneSetFrame("PlayerA", "Wounded")
    fnCutscene([[ VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iPlayerADown", "N", 1.0) ]])
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iDontEditPlayerStates", "N", 0.0) ]])
    fnCutscene([[ VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iJumpMode", "N", 1.0) ]])
    fnCutscene([[ VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iDrinkTimer", "N", -1.0) ]])
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Everyone:[VOICE|Crowd] First one down![P] Give Barry a cheer![P] Droobly drunks droobly drunks!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iJumpMode", "N", 0.0) ]])
    fnCutsceneFace("Florentina", 1, 0)
    fnCutsceneFace("Miho", -1, 0)
    fnCutsceneFace("PlayerB", -1, 0)
    fnCutsceneFace("PlayerC", 1, 0)
    fnCutsceneFace("PlayerD", -1, 0)
    fnCutsceneFace("PlayerE", 1, 0)
    fnCutsceneFace("PlayerF", -1, 0)
    
    -- |[Clean Up]|
    gsaDrinkNameList = nil
    
-- |[ ========================================= Phase 3 ======================================== ]|
--Function is modified to remove Barry from the list.
elseif(iDrinkingPhase == 3.0) then
    
    -- |[Function Setup]|
    gsaDrinkNameList = {"Florentina", "Miho", "PlayerB", "PlayerC", "PlayerD", "PlayerE", "PlayerF"}
    gsaDrinkXList    = {       12.25,  12.25,     18.25,     12.25,     12.25,     18.25,     18.25}
    gsaDrinkYList    = {       11.00,  11.00,     11.00,     13.50,     13.50,     13.50,     13.50}
    local function fnRandomTable()
        
        --Figure out how to move.
        local iRoll = LM_GetRandomNumber(1, 7)
        local sCharName = gsaDrinkNameList[iRoll]
        
        --First four characters face down.
        if(iRoll <= 4) then
            fnCutsceneFace(sCharName, 0, 1)
        --Otherwise, face up.
        else
            fnCutsceneFace(sCharName, 0, -1)
        end
        
        --Mei teleports to their table.
        fnCutscene([[ AudioManager_PlaySound("World|GlassMove") ]])
        fnCutsceneTeleport("Mei", gsaDrinkXList[iRoll], gsaDrinkYList[iRoll])
        if(gsaDrinkYList[iRoll] < 12.0) then
            fnCutsceneFace("Mei", 0, -1)
        else
            fnCutsceneFace("Mei", 0, 1)
        end
        fnCutsceneWait(10)
        fnCutsceneBlocker()
        
        --Mei teleports to another table. This is done by adding 2-5 to the roll, using modulus if needed.
        local iModRoll = iRoll + LM_GetRandomNumber(2, 5)
        if(iModRoll > 7) then iModRoll = iModRoll - 7 end
        fnCutscene([[ AudioManager_PlaySound("World|GlassMove") ]])
        fnCutsceneTeleport("Mei", gsaDrinkXList[iModRoll], gsaDrinkYList[iModRoll])
        if(gsaDrinkYList[iModRoll] < 12.0) then
            fnCutsceneFace("Mei", 0, -1)
        else
            fnCutsceneFace("Mei", 0, 1)
        end
        fnCutsceneWait(10)
        fnCutsceneBlocker()
        
        --Everyone returns to their original facing.
        fnCutsceneFace("Florentina", 1, 0)
        fnCutsceneFace("Miho",      -1, 0)
        fnCutsceneFace("PlayerB",   -1, 0)
        fnCutsceneFace("PlayerC",    1, 0)
        fnCutsceneFace("PlayerD",   -1, 0)
        fnCutsceneFace("PlayerE",    1, 0)
        fnCutsceneFace("PlayerF",   -1, 0)
    end
    
    --Camera lock.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 5.0)
        CameraEvent_SetProperty("Focus Position", (15.25 * gciSizePerTile), (12.50 * gciSizePerTile))
    DL_PopActiveObject()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Olivia:[VOICE|MercF] Roll it roll it![P] Trades up!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Mei moves to the middle.
    fnCutsceneMove("Mei", 15.25,  8.50)
    fnCutsceneMove("Mei", 15.25, 11.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Characters change orientations at random and Mei teleports a bunch of times.
    for i = 1, 10, 1 do
        fnRandomTable()
    end
    
    --Mei teleports back to the middle.
    fnCutsceneTeleport("Mei", 15.25, 12.50)
    fnCutsceneFace("Mei", 0, 1)
    
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Mei:[VOICE|Mei] Chug chug![P] Ha ha ha!") ]])
    fnCutsceneBlocker()
    
    --Drinking time!
    fnCutscene([[ VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iJumpMode", "N", 1.0) ]])
    fnCutscene([[ VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iDrinkTimer", "N", 0.0) ]])
    fnCutsceneLayerDisabled("DrinkAL", true)
    fnCutsceneLayerDisabled("DrinkBL", true)
    fnCutsceneLayerDisabled("DrinkCL", true)
    fnCutsceneLayerDisabled("DrinkDL", true)
    fnCutsceneLayerDisabled("DrinkEL", true)
    fnCutsceneLayerDisabled("DrinkFL", true)
    fnCutsceneLayerDisabled("DrinkGL", true)
    fnCutsceneLayerDisabled("DrinkHL", true)
    fnCutsceneLayerDisabled("DrinkAU", true)
    fnCutsceneLayerDisabled("DrinkBU", true)
    fnCutsceneLayerDisabled("DrinkCU", true)
    fnCutsceneLayerDisabled("DrinkDU", true)
    fnCutsceneLayerDisabled("DrinkEU", true)
    fnCutsceneLayerDisabled("DrinkFU", true)
    fnCutsceneLayerDisabled("DrinkGU", true)
    fnCutsceneLayerDisabled("DrinkHU", true)
    fnCutsceneLayerDisabled("DrinkLower", false)
    fnCutsceneLayerDisabled("DrinkUpper", false)
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Everyone:[VOICE|Crowd] Droobly[P] Chug[P] Chug[P] Chug![B][C]") ]])
    fnCutscene([[ Append("Everyone:[VOICE|Crowd] Slam it back!!!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iJumpMode", "N", 0.0) ]])
    
    --Reset flags.
    fnCutsceneFace("Florentina", 1, 0)
    fnCutsceneFace("Miho", -1, 0)
    fnCutsceneFace("PlayerB", -1, 0)
    fnCutsceneFace("PlayerC", 1, 0)
    fnCutsceneFace("PlayerD", -1, 0)
    fnCutsceneFace("PlayerE", 1, 0)
    fnCutsceneFace("PlayerF", -1, 0)
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Olivia:[VOICE|MercF] [SOUND|World|GlassMove]Who's still standing?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneFace("PlayerC", 0, -1)
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Cris:[VOICE|HumanF0] Hey, what wash in sat last one?[P] Heeyyyyyyyy...[B][C]") ]])
    fnCutscene([[ Append("Cris:[VOICE|HumanF0] Thish ishn't muchs but if one of you could just slide me into bed?[B][C]") ]])
    fnCutscene([[ Append("Lore:[VOICE|Alicia] Take me wish you?[P] *Hic* ha ha ha!") ]])
    fnCutsceneBlocker()
    fnCutscene([[ VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iDontEditPlayerStates", "N", 1.0) ]])
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
    fnCutsceneSetFrame("PlayerC", "Wounded")
    fnCutscene([[ VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iPlayerCDown", "N", 1.0) ]])
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneTeleport("PlayerF", 19.85, 14.20)
    fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
    fnCutsceneSetFrame("PlayerF", "Wounded")
    fnCutscene([[ VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iPlayerFDown", "N", 1.0) ]])
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    fnCutsceneFace("Mei", -1, -1)
    fnCutsceneFace("Miho", -1, 0)
    fnCutsceneFace("PlayerD", 0, -1)
    fnCutsceneFace("PlayerE", 0, -1)
    fnCutscene([[ VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iJumpMode", "N", 1.0) ]])
    fnCutscene([[ VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iDrinkTimer", "N", -1.0) ]])
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Everyone:[VOICE|Crowd] Yeah yeah yeah![P] Five to go, drink drink drink!") ]])
    fnCutsceneBlocker()
    fnCutscene([[ VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iDontEditPlayerStates", "N", 0.0) ]])
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iJumpMode", "N", 0.0) ]])
    fnCutsceneFace("Florentina", 1, 0)
    fnCutsceneFace("Miho", -1, 0)
    fnCutsceneFace("PlayerB", -1, 0)
    fnCutsceneFace("PlayerC", 1, 0)
    fnCutsceneFace("PlayerD", -1, 0)
    fnCutsceneFace("PlayerE", 1, 0)
    fnCutsceneFace("PlayerF", -1, 0)
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Blush") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Miho", "VeryDrunk") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Mei", "Neutral") ]])
    fnCutscene([[ Append("Miho:[E|VeryDrunk] [P]*Hic*[P] No fair, [P]*hic*[P] I didn't know Trannadar had such good hooch before I started![P] Do-over![B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Blush] Shample the local [P]*hic*[P] cuisine before you go for a bounty [P]*hic*[P] nexsht time.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Blush] No no, rishe above it, Florentina.[B][C]") ]])
    fnCutscene([[ Append("Miho:[E|VeryDrunk] Gimme a kiss, sweetie![P] *Hic*![B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Blush] Not right now.[P] The game is still on![B][C]") ]])
    fnCutscene([[ Append("Miho:[E|VeryDrunk] Yeah, nexsht round!") ]])
    fnCutsceneBlocker()
    
    -- |[Clean Up]|
    gsaDrinkNameList = nil
    
-- |[ ========================================= Phase 4 ======================================== ]|
--Function is modified to remove Barry, Cris, and Lore from the list.
elseif(iDrinkingPhase == 4.0) then
    
    -- |[Function Setup]|
    gsaDrinkNameList = {"Florentina", "Miho", "PlayerB", "PlayerD", "PlayerE"}
    gsaDrinkXList    = {       12.25,  12.25,     18.25,     12.25,     18.25}
    gsaDrinkYList    = {       11.00,  11.00,     11.00,     13.50,     13.50}
    local function fnRandomTable()
        
        --Figure out how to move.
        local iRoll = LM_GetRandomNumber(1, 5)
        local sCharName = gsaDrinkNameList[iRoll]
        
        --First four characters face down.
        if(iRoll <= 3) then
            fnCutsceneFace(sCharName, 0, 1)
        --Otherwise, face up.
        else
            fnCutsceneFace(sCharName, 0, -1)
        end
        
        --Mei teleports to their table.
        fnCutscene([[ AudioManager_PlaySound("World|GlassMove") ]])
        fnCutsceneTeleport("Mei", gsaDrinkXList[iRoll], gsaDrinkYList[iRoll])
        if(gsaDrinkYList[iRoll] < 12.0) then
            fnCutsceneFace("Mei", 0, -1)
        else
            fnCutsceneFace("Mei", 0, 1)
        end
        fnCutsceneWait(10)
        fnCutsceneBlocker()
        
        --Mei teleports to another table. This is done by adding 2-4 to the roll, using modulus if needed.
        local iModRoll = iRoll + LM_GetRandomNumber(2, 4)
        if(iModRoll > 5) then iModRoll = iModRoll - 5 end
        fnCutscene([[ AudioManager_PlaySound("World|GlassMove") ]])
        fnCutsceneTeleport("Mei", gsaDrinkXList[iModRoll], gsaDrinkYList[iModRoll])
        if(gsaDrinkYList[iModRoll] < 12.0) then
            fnCutsceneFace("Mei", 0, -1)
        else
            fnCutsceneFace("Mei", 0, 1)
        end
        fnCutsceneWait(10)
        fnCutsceneBlocker()
        
        --Everyone returns to their original facing.
        fnCutsceneFace("Florentina", 1, 0)
        fnCutsceneFace("Miho",      -1, 0)
        fnCutsceneFace("PlayerB",   -1, 0)
        fnCutsceneFace("PlayerC",    1, 0)
        fnCutsceneFace("PlayerD",   -1, 0)
        fnCutsceneFace("PlayerE",    1, 0)
        fnCutsceneFace("PlayerF",   -1, 0)
    end
    
    --Camera lock.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 5.0)
        CameraEvent_SetProperty("Focus Position", (15.25 * gciSizePerTile), (12.50 * gciSizePerTile))
    DL_PopActiveObject()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Olivia:[VOICE|MercF] Oooh, running low on tokens![P] Trades up!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Mei moves to the middle.
    fnCutsceneMove("Mei", 15.25,  8.50)
    fnCutsceneMove("Mei", 15.25, 11.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Characters change orientations at random and Mei teleports a bunch of times.
    for i = 1, 10, 1 do
        fnRandomTable()
    end
    
    --Mei teleports back to the middle.
    fnCutsceneTeleport("Mei", 15.25, 12.50)
    fnCutsceneFace("Mei", 0, 1)
    
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Mei:[VOICE|Mei] Nobody else?[P] Get it going!") ]])
    fnCutsceneBlocker()
    
    --Drinking time!
    fnCutscene([[ VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iJumpMode", "N", 1.0) ]])
    fnCutscene([[ VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iDrinkTimer", "N", 0.0) ]])
    fnCutsceneLayerDisabled("DrinkAL", true)
    fnCutsceneLayerDisabled("DrinkBL", true)
    fnCutsceneLayerDisabled("DrinkCL", true)
    fnCutsceneLayerDisabled("DrinkDL", true)
    fnCutsceneLayerDisabled("DrinkEL", true)
    fnCutsceneLayerDisabled("DrinkFL", true)
    fnCutsceneLayerDisabled("DrinkGL", true)
    fnCutsceneLayerDisabled("DrinkHL", true)
    fnCutsceneLayerDisabled("DrinkAU", true)
    fnCutsceneLayerDisabled("DrinkBU", true)
    fnCutsceneLayerDisabled("DrinkCU", true)
    fnCutsceneLayerDisabled("DrinkDU", true)
    fnCutsceneLayerDisabled("DrinkEU", true)
    fnCutsceneLayerDisabled("DrinkFU", true)
    fnCutsceneLayerDisabled("DrinkGU", true)
    fnCutsceneLayerDisabled("DrinkHU", true)
    fnCutsceneLayerDisabled("DrinkLower", false)
    fnCutsceneLayerDisabled("DrinkUpper", false)
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Everyone:[VOICE|Crowd] Droobly[P] Chug[P] Chug[P] Chug![B][C]") ]])
    fnCutscene([[ Append("Everyone:[VOICE|Crowd] Do it do it do it! Yeahhhhh!!!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iJumpMode", "N", 0.0) ]])
    
    --Reset flags.
    fnCutsceneFace("Florentina", 1, 0)
    fnCutsceneFace("Miho", -1, 0)
    fnCutsceneFace("PlayerB", -1, 0)
    fnCutsceneFace("PlayerC", 1, 0)
    fnCutsceneFace("PlayerD", -1, 0)
    fnCutsceneFace("PlayerE", 1, 0)
    fnCutsceneFace("PlayerF", -1, 0)
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Olivia:[VOICE|MercF] [SOUND|World|GlassMove]Glasses down, finish those drinks!") ]])
    fnCutsceneBlocker()
    fnCutscene([[ VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iDontEditPlayerStates", "N", 1.0) ]])
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneFace("PlayerB", 0, 1)
    fnCutsceneFace("PlayerE", 0, -1)
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Ira:[VOICE|Hypatia] Okay, might be time to bow out gracshfully before I make myself look dumb.[B][C]") ]])
    fnCutscene([[ Append("Bette:[VOICE|MercF] Wait, was that moonshine?[P] Ugh....") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
    fnCutsceneSetFrame("PlayerE", "Wounded")
    fnCutscene([[ VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iPlayerEDown", "N", 1.0) ]])
    fnCutsceneWait(25)
    fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
    fnCutsceneSetFrame("PlayerB", "Wounded")
    fnCutscene([[ VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iPlayerBDown", "N", 1.0) ]])
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Nancy:[VOICE|HumanF1] Ha ha![P] Ira and Bette are down![P] The sugar sweet kiss of my impending victory!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
    fnCutsceneSetFrame("PlayerD", "Wounded")
    fnCutscene([[ VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iPlayerDDown", "N", 1.0) ]])
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iDontEditPlayerStates", "N", 0.0) ]])
    
    fnCutsceneFace("Miho", -1, 0)
    fnCutscene([[ VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iJumpMode", "N", 1.0) ]])
    fnCutscene([[ VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iDrinkTimer", "N", -1.0) ]])
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Everyone:[VOICE|Crowd] Woooo![P] It's down to two!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iJumpMode", "N", 0.0) ]])
    fnCutsceneFace("Florentina", 1, 0)
    fnCutsceneFace("Miho", -1, 0)
    fnCutsceneFace("PlayerB", -1, 0)
    fnCutsceneFace("PlayerC", 1, 0)
    fnCutsceneFace("PlayerD", -1, 0)
    fnCutsceneFace("PlayerE", 1, 0)
    fnCutsceneFace("PlayerF", -1, 0)
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Blush") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Miho", "VeryDrunk") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Mei", "Neutral") ]])
    fnCutscene([[ Append("Miho:[E|VeryDrunk] I've got you now![B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Blush] Jusht one more round...[B][C]") ]])
    fnCutscene([[ Append("Miho:[E|VeryDrunk] I'm one drink away from capshuring the famoush Florentina![P] Moonshine![P] Moonshine![B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Blush] Oh you want to go?[P] You want to go?[P] Moonshine for me, too![B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Surprise] Two moonshines!?[B][C]") ]])
    fnCutscene([[ Append("Miho:[E|VeryDrunk] Go big or go home, baybeeee![P] Woooo!") ]])
    fnCutsceneBlocker()
    
    -- |[Clean Up]|
    gsaDrinkNameList = nil
    
    --Special: Only one table needs booze this round.
    VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iBoozeTableB", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iBoozeTableC", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iBoozeTableD", "N", 1.0)
    
-- |[ ========================================= Phase 5 ======================================== ]|
--No swapping on this one as they both have the same drink.
elseif(iDrinkingPhase == 5.0) then
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iMihoLostContest", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iMihoUnconscious", "N", 1.0)
    
    --Camera lock.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 5.0)
        CameraEvent_SetProperty("Focus Position", (15.25 * gciSizePerTile), (12.50 * gciSizePerTile))
    DL_PopActiveObject()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Olivia:[VOICE|MercF] This is it![P] Can you take one more?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Mei moves to the middle.
    fnCutsceneMove("Mei", 12.25,  9.50)
    fnCutsceneFace("Mei", 0, 1)
    fnCutsceneFace("Florentina", 1, 0)
    fnCutsceneFace("Miho", -1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Mei:[VOICE|Mei] You both have the same drink, slam it!") ]])
    fnCutsceneBlocker()
    
    --Drinking time!
    fnCutscene([[ VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iJumpMode", "N", 1.0) ]])
    fnCutscene([[ VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iDrinkTimer", "N", 0.0) ]])
    fnCutsceneLayerDisabled("DrinkAL", true)
    fnCutsceneLayerDisabled("DrinkBL", true)
    fnCutsceneLayerDisabled("DrinkCL", true)
    fnCutsceneLayerDisabled("DrinkDL", true)
    fnCutsceneLayerDisabled("DrinkEL", true)
    fnCutsceneLayerDisabled("DrinkFL", true)
    fnCutsceneLayerDisabled("DrinkGL", true)
    fnCutsceneLayerDisabled("DrinkHL", true)
    fnCutsceneLayerDisabled("DrinkAU", true)
    fnCutsceneLayerDisabled("DrinkBU", true)
    fnCutsceneLayerDisabled("DrinkCU", true)
    fnCutsceneLayerDisabled("DrinkDU", true)
    fnCutsceneLayerDisabled("DrinkEU", true)
    fnCutsceneLayerDisabled("DrinkFU", true)
    fnCutsceneLayerDisabled("DrinkGU", true)
    fnCutsceneLayerDisabled("DrinkHU", true)
    fnCutsceneLayerDisabled("DrinkLower", false)
    fnCutsceneLayerDisabled("DrinkUpper", false)
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Everyone:[VOICE|Crowd] Droobly[P] Chug[P] Chug[P] Chug![B][C]") ]])
    fnCutscene([[ Append("Everyone:[VOICE|Crowd] Let's see who the drunkest drunk is!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iJumpMode", "N", 0.0) ]])
    
    --Reset flags.
    fnCutsceneFace("Florentina", 1, 0)
    fnCutsceneFace("Miho", -1, 0)
    fnCutsceneSetFrame("Florentina", "Null")
    fnCutsceneSetFrame("Miho", "Null")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Olivia:[VOICE|MercF] [SOUND|World|GlassMove]Glasses down![P] Can you do another round?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Blush") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Miho", "VeryDrunk") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Mei", "Neutral") ]])
    fnCutscene([[ Append("Miho:[E|VeryDrunk] Jussshhttt one more?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Blush] *hic*[P] yeah?[P] Yeah?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Blush] Can't...[P] talk...[B][C]") ]])
    fnCutscene([[ Append("Miho:[E|VeryDrunk] You can [P]*hic*[P] barely shtand![P] I've got this one eashy![B][C]") ]])
    fnCutscene([[ Append("Miho:[E|VeryDrunk] But hey, if you promish me someshing, I'll letcha go?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Blush] Like what?[B][C]") ]])
    fnCutscene([[ Append("Miho:[E|VeryDrunk] *Hic*[P] A date, hot shtuff?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Blush] You're embarrasshing me...[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Smirk] It seems they're both on their feet...[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Surprise] Wait, she's going down!") ]])
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlayMusic("Null") ]])
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    
    --Cut to black.
    fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Everyone:[VOICE|Crowd] Droobly[P] Drunks![P] We have a winner!") ]])
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Re-add Florentina to the party.
    fnAddPartyMember("Florentina")
    
    --Clear all parallel scripts.
    fnCutscene([[ Cutscene_HandleParallel("ClearAll") ]])
    
    --Transition to next scene.
    fnCutscene([[ AL_BeginTransitionTo("RiverWildsA", "FORCEPOS:92.0x15.0x0") ]])
    fnCutsceneBlocker()
    
    -- |[Clean Up]|
    gsaDrinkNameList = nil
end
