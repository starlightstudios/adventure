-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
-- |[Drinking Contest Setup]|
--Should be called each time we boot the event. This handles all NPC creation as well as scene setup.
if(sObjectName == "StartTrigger") then
    
    -- |[ ========== Setup ========== ]|
    --Constants.
    local sBasePath = fnResolvePath()
    local sDialoguePath = sBasePath .. "Dialogue.lua"
    
    --Parallel flags.
    AL_SetProperty("Set Parallel Cutscenes During Dialogue Flag", true)
    AL_SetProperty("Set Parallel Cutscenes During Cutscenes Flag", true)
    
    --Reset script variables.
    VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iDrinkingPhase", "N", 0.0)
    VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iTakenBooze", "N", 0.0)
    VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iBoozeTableA", "N", 0.0)
    VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iBoozeTableB", "N", 0.0)
    VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iBoozeTableC", "N", 0.0)
    VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iBoozeTableD", "N", 0.0)
    VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iJumpMode", "N", 0.0)

    -- |[ ========== Disable Drink Layers ========== ]|
    AL_SetProperty("Set Layer Disabled", "DrinkAL", true)
    AL_SetProperty("Set Layer Disabled", "DrinkBL", true)
    AL_SetProperty("Set Layer Disabled", "DrinkCL", true)
    AL_SetProperty("Set Layer Disabled", "DrinkDL", true)
    AL_SetProperty("Set Layer Disabled", "DrinkEL", true)
    AL_SetProperty("Set Layer Disabled", "DrinkFL", true)
    AL_SetProperty("Set Layer Disabled", "DrinkGL", true)
    AL_SetProperty("Set Layer Disabled", "DrinkHL", true)
    AL_SetProperty("Set Layer Disabled", "DrinkAU", true)
    AL_SetProperty("Set Layer Disabled", "DrinkBU", true)
    AL_SetProperty("Set Layer Disabled", "DrinkCU", true)
    AL_SetProperty("Set Layer Disabled", "DrinkDU", true)
    AL_SetProperty("Set Layer Disabled", "DrinkEU", true)
    AL_SetProperty("Set Layer Disabled", "DrinkFU", true)
    AL_SetProperty("Set Layer Disabled", "DrinkGU", true)
    AL_SetProperty("Set Layer Disabled", "DrinkHU", true)
    AL_SetProperty("Set Layer Disabled", "DrinkLower", false)
    AL_SetProperty("Set Layer Disabled", "DrinkUpper", false)
    
    -- |[ ========== NPC Creation ========== ]|
    --Place NPCs around the area. This is done in a grid format. First, initialize. These values get scrapped later,
    -- but are globals since the crowd handler needs them.
    iNPCMaxX = 34-1
    iNPCMaxY = 23-1
    saNPCList = {}
    for x = 0, iNPCMaxX, 1 do
        saNPCList[x] = {}
        for y = 0, iNPCMaxY, 1 do
            saNPCList[x][y] = {}
            saNPCList[x][y].sName = "NPC"..x.."_"..y
            saNPCList[x][y].sSprite = "Null"
            saNPCList[x][y].iFacing = gci_Face_East
            saNPCList[x][y].bHasDialogue = false
        end
    end
    
    -- |[West Side]|
    for x = 6, 8, 1 do
        for y = 7, 16, 1 do
            saNPCList[x][y].sSprite = "GenericF0"
            saNPCList[x][y].iFacing = gci_Face_East
            saNPCList[x][y].bHasDialogue = false
        end
    end
    
    -- |[East Side]|
    for x = 22, 23, 1 do
        for y = 8, 16, 1 do
            saNPCList[x][y].sSprite = "GenericF0"
            saNPCList[x][y].iFacing = gci_Face_West
            saNPCList[x][y].bHasDialogue = false
        end
    end
    for x = 24, 25, 1 do
        for y = 12, 17, 1 do
            saNPCList[x][y].sSprite = "GenericF0"
            saNPCList[x][y].iFacing = gci_Face_West
            saNPCList[x][y].bHasDialogue = false
        end
    end
    
    -- |[North Side]|
    for x = 7, 11, 1 do
        for y = 6, 7, 1 do
            saNPCList[x][y].sSprite = "GenericF0"
            saNPCList[x][y].iFacing = gci_Face_South
            saNPCList[x][y].bHasDialogue = false
        end
    end
    for x = 10, 23, 1 do
        for y = 5, 5, 1 do
            saNPCList[x][y].sSprite = "GenericF0"
            saNPCList[x][y].iFacing = gci_Face_South
            saNPCList[x][y].bHasDialogue = false
        end
    end
    for x = 18, 24, 1 do
        for y = 6, 6, 1 do
            saNPCList[x][y].sSprite = "GenericF0"
            saNPCList[x][y].iFacing = gci_Face_South
            saNPCList[x][y].bHasDialogue = false
        end
    end
    for x = 12, 16, 1 do
        for y = 4, 4, 1 do
            saNPCList[x][y].sSprite = "GenericF0"
            saNPCList[x][y].iFacing = gci_Face_South
            saNPCList[x][y].bHasDialogue = false
        end
    end
    
    -- |[Scramble the Sprites]|
    for x = 0, iNPCMaxX, 1 do
        for y = 0, iNPCMaxY, 1 do
            if(saNPCList[x][y].sSprite == "GenericF0") then
                local iRoll = LM_GetRandomNumber(1, 120)
                if(iRoll < 10) then
                    saNPCList[x][y].sSprite = "GenericF0"
                elseif(iRoll < 20) then
                    saNPCList[x][y].sSprite = "GenericF1"
                elseif(iRoll < 30) then
                    saNPCList[x][y].sSprite = "GenericF3"
                elseif(iRoll < 40) then
                    saNPCList[x][y].sSprite = "GenericF4"
                elseif(iRoll < 50) then
                    saNPCList[x][y].sSprite = "GenericM0"
                elseif(iRoll < 60) then
                    saNPCList[x][y].sSprite = "GenericM1"
                elseif(iRoll < 70) then
                    saNPCList[x][y].sSprite = "GenericM2"
                elseif(iRoll < 80) then
                    saNPCList[x][y].sSprite = "GenericM3"
                elseif(iRoll < 90) then
                    saNPCList[x][y].sSprite = "GenericM4"
                elseif(iRoll < 100) then
                    saNPCList[x][y].sSprite = "MercF"
                elseif(iRoll < 110) then
                    saNPCList[x][y].sSprite = "MercM"
                elseif(iRoll < 115) then
                    saNPCList[x][y].sSprite = "Werecat"
                else
                    saNPCList[x][y].sSprite = "Alraune"
                end
            end
        end
    end
    
    -- |[ ========== Special NPC Handling ========== ]|
    --First, create the bartender.
    TA_Create("Olivia")
        TA_SetProperty("Position", 14, 6)
        TA_SetProperty("Facing", gci_Face_South)
        TA_SetProperty("Clipping Flag", true)
        TA_SetProperty("Ignores Gravity", false)
        TA_SetProperty("Activation Script", sDialoguePath)
        TA_SetProperty("Extended Activation Direction", gci_Face_South)
        fnSetCharacterGraphics("Root/Images/Sprites/GenericF1/", false)
    DL_PopActiveObject()
    
    --Create Miho
    TA_Create("Miho")
        TA_SetProperty("Position", 14, 10)
        TA_SetProperty("Facing", gci_Face_West)
        TA_SetProperty("Clipping Flag", true)
        TA_SetProperty("Ignores Gravity", false)
        TA_SetProperty("Activation Script", sDialoguePath)
        fnSetCharacterGraphics("Root/Images/Sprites/Miho/", false)
        TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/Miho|Wounded")
        TA_SetProperty("Add Special Frame", "Drink0", "Root/Images/Sprites/Special/Miho|Drink0")
        TA_SetProperty("Add Special Frame", "Drink1", "Root/Images/Sprites/Special/Miho|Drink1")
        TA_SetProperty("Add Special Frame", "Drink2", "Root/Images/Sprites/Special/Miho|Drink2")
    DL_PopActiveObject()
    
    --Other players
    TA_Create("PlayerA")
        TA_SetProperty("Position", 16, 10)
        TA_SetProperty("Facing", gci_Face_East)
        TA_SetProperty("Clipping Flag", true)
        TA_SetProperty("Ignores Gravity", false)
        TA_SetProperty("Activation Script", sDialoguePath)
        fnSetCharacterGraphics("Root/Images/Sprites/MercM/", false)
        TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/MercM|Wounded")
        TA_SetProperty("Add Special Frame", "Drink0", "Root/Images/Sprites/Special/MercM|Drink0")
        TA_SetProperty("Add Special Frame", "Drink1", "Root/Images/Sprites/Special/MercM|Drink1")
        TA_SetProperty("Add Special Frame", "Drink2", "Root/Images/Sprites/Special/MercM|Drink2")
    DL_PopActiveObject()
    TA_Create("PlayerB")
        TA_SetProperty("Position", 20, 10)
        TA_SetProperty("Facing", gci_Face_West)
        TA_SetProperty("Clipping Flag", true)
        TA_SetProperty("Ignores Gravity", false)
        TA_SetProperty("Activation Script", sDialoguePath)
        fnSetCharacterGraphics("Root/Images/Sprites/GenericF1/", false)
        TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/GenericF1|Wounded")
        TA_SetProperty("Add Special Frame", "Drink0", "Root/Images/Sprites/Special/GenericF1|Drink0")
        TA_SetProperty("Add Special Frame", "Drink1", "Root/Images/Sprites/Special/GenericF1|Drink1")
        TA_SetProperty("Add Special Frame", "Drink2", "Root/Images/Sprites/Special/GenericF1|Drink2")
    DL_PopActiveObject()
    TA_Create("PlayerC")
        TA_SetProperty("Position", 10, 14)
        TA_SetProperty("Facing", gci_Face_East)
        TA_SetProperty("Clipping Flag", true)
        TA_SetProperty("Ignores Gravity", false)
        TA_SetProperty("Activation Script", sDialoguePath)
        fnSetCharacterGraphics("Root/Images/Sprites/GenericF0/", false)
        TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/GenericF0|Wounded")
        TA_SetProperty("Add Special Frame", "Drink0", "Root/Images/Sprites/Special/GenericF0|Drink0")
        TA_SetProperty("Add Special Frame", "Drink1", "Root/Images/Sprites/Special/GenericF0|Drink1")
        TA_SetProperty("Add Special Frame", "Drink2", "Root/Images/Sprites/Special/GenericF0|Drink2")
    DL_PopActiveObject()
    TA_Create("PlayerD")
        TA_SetProperty("Position", 14, 14)
        TA_SetProperty("Facing", gci_Face_West)
        TA_SetProperty("Clipping Flag", true)
        TA_SetProperty("Ignores Gravity", false)
        TA_SetProperty("Activation Script", sDialoguePath)
        fnSetCharacterGraphics("Root/Images/Sprites/GenericF3/", false)
        TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/GenericF3|Wounded")
        TA_SetProperty("Add Special Frame", "Drink0", "Root/Images/Sprites/Special/GenericF3|Drink0")
        TA_SetProperty("Add Special Frame", "Drink1", "Root/Images/Sprites/Special/GenericF3|Drink1")
        TA_SetProperty("Add Special Frame", "Drink2", "Root/Images/Sprites/Special/GenericF3|Drink2")
    DL_PopActiveObject()
    TA_Create("PlayerE")
        TA_SetProperty("Position", 16, 14)
        TA_SetProperty("Facing", gci_Face_East)
        TA_SetProperty("Clipping Flag", true)
        TA_SetProperty("Ignores Gravity", false)
        TA_SetProperty("Activation Script", sDialoguePath)
        fnSetCharacterGraphics("Root/Images/Sprites/GenericF0/", false)
        TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/GenericF0|Wounded")
        TA_SetProperty("Add Special Frame", "Drink0", "Root/Images/Sprites/Special/GenericF0|Drink0")
        TA_SetProperty("Add Special Frame", "Drink1", "Root/Images/Sprites/Special/GenericF0|Drink1")
        TA_SetProperty("Add Special Frame", "Drink2", "Root/Images/Sprites/Special/GenericF0|Drink2")
    DL_PopActiveObject()
    TA_Create("PlayerF")
        TA_SetProperty("Position", 20, 14)
        TA_SetProperty("Facing", gci_Face_West)
        TA_SetProperty("Clipping Flag", true)
        TA_SetProperty("Ignores Gravity", false)
        TA_SetProperty("Activation Script", sDialoguePath)
        fnSetCharacterGraphics("Root/Images/Sprites/Werecat/", false)
        TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/Werecat|Slump")
        TA_SetProperty("Add Special Frame", "Drink0", "Root/Images/Sprites/Special/Werecat|Drink0")
        TA_SetProperty("Add Special Frame", "Drink1", "Root/Images/Sprites/Special/Werecat|Drink1")
        TA_SetProperty("Add Special Frame", "Drink2", "Root/Images/Sprites/Special/Werecat|Drink2")
    DL_PopActiveObject()
    
    -- |[Special Characters]|
    --These characters typically appear if Mei has met them before. Otherwise, they are
    -- replaced with a generic NPC.
    local iHasSeenTrannadarFirstScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFirstScene", "N")
    local iMetNadiaInWerecatScene     = VM_GetVar("Root/Variables/Chapter1/ScenesWerecat/iMetNadiaInWerecatScene", "N")
    if(iHasSeenTrannadarFirstScene == 1.0 or iMetNadiaInWerecatScene == 1.0) then
        saNPCList[11][7].sName = "Nadia"
        saNPCList[11][7].sSprite = "Nadia"
        saNPCList[11][7].iFacing = gci_Face_South
        saNPCList[11][7].bHasDialogue = true
    end
    
    --Polaris, by default, cannot be skipped. We check anyway.
    local iMetPolaris = VM_GetVar("Root/Variables/Chapter1/Polaris/iMetPolaris", "N")
    if(iMetPolaris == 1.0) then
        saNPCList[8][14].sName = "Polaris"
        saNPCList[8][14].sSprite = "Polaris"
        saNPCList[8][14].iFacing = gci_Face_East
        saNPCList[8][14].bHasDialogue = true
    end
    
    --Adina. Must have at least met her.
	local iHasMetAdina = VM_GetVar("Root/Variables/Chapter1/SaltFlats/iHasMetAdina", "N")
    if(iHasMetAdina >= 1.0) then
        saNPCList[22][12].sName = "Adina"
        saNPCList[22][12].sSprite = "Adina"
        saNPCList[22][12].iFacing = gci_Face_West
        saNPCList[22][12].bHasDialogue = true
    end
    
    --Rochea. Mei must have alraune form.
    local iHasAlrauneForm = VM_GetVar("Root/Variables/Global/Mei/iHasAlrauneForm", "N")
    if(iHasAlrauneForm == 1.0) then
        
        --Rochea.
        saNPCList[22][16].sName = "Rochea"
        saNPCList[22][16].sSprite = "Rochea"
        saNPCList[22][16].iFacing = gci_Face_West
        saNPCList[22][16].bHasDialogue = true
        
        --Some adjacent alraunes.
        saNPCList[22][15].sName = "AlrauneA"
        saNPCList[22][15].sSprite = "Alraune"
        saNPCList[22][15].iFacing = gci_Face_West
        saNPCList[22][15].bHasDialogue = true
        
        saNPCList[23][15].sName = "AlrauneB"
        saNPCList[23][15].sSprite = "Alraune"
        saNPCList[23][15].iFacing = gci_Face_West
        saNPCList[23][15].bHasDialogue = false
        
        saNPCList[23][16].sName = "AlrauneC"
        saNPCList[23][16].sSprite = "Alraune"
        saNPCList[23][16].iFacing = gci_Face_West
        saNPCList[23][16].bHasDialogue = false
        
    end
    
    --Cassandra. Only appears if you saved her.
	local iSavedCassandra = VM_GetVar("Root/Variables/Chapter1/Scenes/iSavedCassandra", "N")
    if(iSavedCassandra == 1.0) then
        saNPCList[22][8].sName = "Cassandra"
        saNPCList[22][8].sSprite = "CassandraH"
        saNPCList[22][8].iFacing = gci_Face_West
        saNPCList[22][8].bHasDialogue = true
    end
    
    --Lydie. Only appears if Mei is a ghost and defeated the Warden.
    local iHasGhostForm = VM_GetVar("Root/Variables/Global/Mei/iHasGhostForm", "N")
    local iCompletedQuantirMansion = VM_GetVar("Root/Variables/Chapter1/Scenes/iCompletedQuantirMansion", "N")
    if(iHasGhostForm == 1.0 and iCompletedQuantirMansion == 1.0) then
        saNPCList[8][12].sName = "Lydie"
        saNPCList[8][12].sSprite = "Lydie"
        saNPCList[8][12].iFacing = gci_Face_East
        saNPCList[8][12].bHasDialogue = true
    end
    
    --Sharelock.
    local iSawSharelockBasement = VM_GetVar("Root/Variables/Chapter1/Sharelock/iSawSharelockBasement", "N")
    if(iSawSharelockBasement == 1.0) then
        saNPCList[8][8].sName = "Sharelock"
        saNPCList[8][8].sSprite = "Sharelock"
        saNPCList[8][8].iFacing = gci_Face_East
        saNPCList[8][8].bHasDialogue = true
    end
    
    -- |[Spawning Characters]|
    --Iterate across the list and spawn everyone.
    for x = 1, iNPCMaxX, 1 do
        for y = 1, iNPCMaxY, 1 do
            if(saNPCList[x][y].sSprite ~= "Null") then
                TA_Create(saNPCList[x][y].sName)
                    TA_SetProperty("Position", x, y)
                    TA_SetProperty("Facing", saNPCList[x][y].iFacing)
                    TA_SetProperty("Clipping Flag", true)
                    TA_SetProperty("Ignores Gravity", false)
                    if(saNPCList[x][y].bHasDialogue) then
                        TA_SetProperty("Activation Script", sDialoguePath)
                    end
                    fnSetCharacterGraphics("Root/Images/Sprites/"..saNPCList[x][y].sSprite.."/", false)
                    
                DL_PopActiveObject()
            end
        end
    end
    
    --Run the crowd handler. This causes NPCs to look around. This must be done after spawning.
    Cutscene_HandleParallel("Create", "Crowd Handler", sBasePath .. "CrowdHandler.lua")
    
    -- |[Remove Florentina and Place Her]|
    --Florentina briefly leaves the party.
    fnRemovePartyMember("Florentina", false)
    fnCutsceneTeleport("Florentina", 10.25, 10.50)
    fnCutsceneFace("Mei", 1, 0)
    fnCutsceneFace("Florentina", 1, 0)
    
    --Set Florentina's collision and activation flag.
    EM_PushEntity("Florentina")
        TA_SetProperty("Clipping Flag", true)
        TA_SetProperty("Activation Script", sDialoguePath)
    DL_PopActiveObject()
    
    -- |[Position Mei]|
    fnCutsceneTeleport("Mei", 10.25, 9.50)
    
    -- |[Scene]|
    --Cut to black.
    fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Fade in.
    fnCutscene([[ AL_SetProperty("Activate Fade", 125, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Miho", "Neutral") ]])
    fnCutscene([[ Append("Mei:[E|Smirk] There we go![P] Miss Olivia sure did get the liquor ready quickly![B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Happy] This town loves two things.[P] Fish, and booze.[P] Pretty sure she has the kegs on standby.[B][C]") ]])
    fnCutscene([[ Append("Miho:[E|Neutral] You're awfully chipper for someone with their life on the line.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Surprise] You're right![P] I should panic instead, that will make this so much easier![B][C]") ]])
    fnCutscene([[ Append("Miho:[E|Angry] Don't toy with me, alraune.[P] I know full well you intend to cheat.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Happy] Me?[P] Never![B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] Though since you seemed pretty gung-ho, I'm going to assume you've never done the Droobly Drunken Game before.[B][C]") ]])
    fnCutscene([[ Append("Miho:[E|Neutral] No.[P] Is this a Trannadar tradition?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] Yep.[P] Centuries old, you can ask anyone if you think I'm making up the rules.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Happy] Mei, you want to be our waitress for this game?[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Happy] Okay, but don't expect me to help you cheat![B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Smirk] Miss Miho seems very nice.[P] She deserves a fair competition.[B][C]") ]])
    fnCutscene([[ Append("Miho:[E|Neutral] Are you kidding?[P] I'm going to cheat, too.[B][C]") ]])
    fnCutscene([[ Append("Miho:[E|Neutral] The entire point is to cheat and not get caught.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] Mei's just a dope, don't worry.[P] Neither of us will get caught.[B][C]") ]])
    fnCutscene([[ Append("Miho:[E|Angry] Spike my drink, Mei, and I'll carve you in two.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Smirk] You don't mean that.[B][C]") ]])
    fnCutscene([[ Append("Miho:[E|Angry] Yes I do![B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Smirk] Nobody as cute as you could be mean.[P] Look at how floofy your tails are![B][C]") ]])
    fnCutscene([[ Append("Miho:[E|Angry] Argh![P] What is it going to take?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] As I said, she's a dope.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] So for your benefit, here are the rules to Droobly Drunks.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] Olivia over there has picked out three drinks for the day.[P] Water, Moosefur Beer, and a lovely Sinian Moonshine.[B][C]") ]])
    fnCutscene([[ Append("Miho:[E|Neutral] I take it the moonshine is much stronger than the beer.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Happy] Yep, that stuff kicks you in the head.[P] I've also heard it gives men an erection.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Surprise] Really?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] That's what they say.[P] Don't ask Barry over there, though.[P] He won't be able to stand after the second round.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneFace("PlayerA", -1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Barry:[VOICE|MercM] Screw you, Florentina![P] I'm winning this one and leaving you sucking lemons!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneFace("PlayerA", 1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Miho", "Neutral") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] Anyway, you start with three tokens.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Smirk] Is that what these poker chips are?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] Yep.[P] Each round, you place an order for one of the drinks.[P] But you don't get to decide which one you get.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] Your cup is picked at random.[P] But if most of us placed orders for water...[B][C]") ]])
    fnCutscene([[ Append("Miho:[E|Neutral] Then most of us would get water, but some would get the beer.[P] I thus won't know what drink I have, but can roughly guess.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] Yep.[P] And if you think you've got something strong, or I've got something weak, you can spend a token to trade cups with me.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] The water has foam in it to make it look like beer, but no sniffing your mug before you have to drink![B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Happy] Then everyone drinks when the crowd shouts 'Droobly Chug Chug Chug!'.[P] So if you want to swap, do it quick![B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] After the round ends, you get more tokens based on what you ordered.[P] Water, none.[P] Beer, one.[P] Moonshine, two.[B][C]") ]])
    fnCutscene([[ Append("Miho:[E|Neutral] The rules seem simple.[P] How is the winner picked?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] If you can't finish your drink when the crowd starts chanting, you lose.[P] Whether this is because you fell over or just can't handle it, doesn't matter.[B][C]") ]])
    fnCutscene([[ Append("Miho:[E|Neutral] So, let's see.[P] You could try to bribe the waitress, or the bartender, or the other players.[B][C]") ]])
    fnCutscene([[ Append("Miho:[E|Neutral] Or perhaps you'll just try to keep me from finishing my drink.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Surprise] Oh, please.[P] Like I'd resort to such cheap tactics![B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Happy] No, you lost this contest long ago.[P] I just have to make sure you don't figure out how until you're black-out drunk![P] Ha ha![B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Happy] This is gonna be so much fun![P] So what do I have to do?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] Just talk to Olivia to get the drinks, and then give us each a cup.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Smirk] (Hmm, I could try to help Florentina by sniffing the drinks myself.[P] She didn't say I couldn't!)[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Smirk] (But should I?[P] What if it's part of her plan to try to bluff Miho?[P] Hmmmmmm...)[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Smirk] (Better not cheat...)[B][C]") ]])
    fnCutscene([[ Append("Miho:[E|Neutral] I suppose you were not lying when you said you'd come quietly.[P] You'll be passed out.[B][C]") ]])
    fnCutscene([[ Append("Miho:[E|Neutral] I'll need to make sure I'm not too inebriated to drag you off.[P] Besting you will be a pleasure, Florentina![B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Happy] Then let's get it going, Mei![P] First round up!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Music!
    fnCutscene([[ AudioManager_PlayMusic("Gladiator") ]])
    fnCutsceneBlocker()
    
end
