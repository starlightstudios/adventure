-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= Exits ========================================== ]|
if(sObjectName == "SampleExit") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("LevelName", "FORCEPOS:28.0x16.0x0")
    
-- |[ ====================================== Examinables ======================================= ]|
-- |[Table A]|
elseif(sObjectName == "TableA") then

    --Variables.
    local iBoozeTableA   = VM_GetVar("Root/Variables/Chapter1/Vendrilstadt/iBoozeTableA", "N")
    local iTakenBooze    = VM_GetVar("Root/Variables/Chapter1/Vendrilstadt/iTakenBooze", "N")
    local iDrinkingPhase = VM_GetVar("Root/Variables/Chapter1/Vendrilstadt/iDrinkingPhase", "N")
    
    --Get the booze.
    if(iTakenBooze == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (I need to go get the drinks first.)") ]])
        fnCutsceneBlocker()
        
    --Need to place booze:
    elseif(iBoozeTableA == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Mei:[VOICE|Mei] [SOUND|World|GlassMove]Rounds up!") ]])
        fnCutsceneBlocker()
        
        --Flags.
        VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iBoozeTableA", "N", 1.0)
        AL_SetProperty("Set Layer Disabled", "DrinkAL", false)
        AL_SetProperty("Set Layer Disabled", "DrinkBL", false)
        AL_SetProperty("Set Layer Disabled", "DrinkAU", false)
        AL_SetProperty("Set Layer Disabled", "DrinkBU", false)
        
    --Already placed.
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Mei] (I already put the drinks down, it's up to them now.)") ]])
        fnCutsceneBlocker()
    end

-- |[Table B]|
elseif(sObjectName == "TableB") then

    --Variables.
    local iBoozeTableB   = VM_GetVar("Root/Variables/Chapter1/Vendrilstadt/iBoozeTableB", "N")
    local iTakenBooze    = VM_GetVar("Root/Variables/Chapter1/Vendrilstadt/iTakenBooze", "N")
    local iDrinkingPhase = VM_GetVar("Root/Variables/Chapter1/Vendrilstadt/iDrinkingPhase", "N")
    
    --Get the booze.
    if(iTakenBooze == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (I need to go get the drinks first.)") ]])
        fnCutsceneBlocker()
        
    --Phase 0:
    elseif(iDrinkingPhase == 0.0) then
        
        --Need to place booze:
        if(iBoozeTableB == 0.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Mei:[VOICE|Mei] [SOUND|World|GlassMove]Rounds up!") ]])
            fnCutsceneBlocker()
        
            --Flags.
            VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iBoozeTableB", "N", 1.0)
            AL_SetProperty("Set Layer Disabled", "DrinkCL", false)
            AL_SetProperty("Set Layer Disabled", "DrinkDL", false)
            AL_SetProperty("Set Layer Disabled", "DrinkCU", false)
            AL_SetProperty("Set Layer Disabled", "DrinkDU", false)
            
        --Already placed.
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Mei] (I already put the drinks down, it's up to them now.)") ]])
            fnCutsceneBlocker()
        end
        
    --Phase 1:
    elseif(iDrinkingPhase == 1.0) then
        
        --Need to place booze:
        if(iBoozeTableB == 0.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Mei:[VOICE|Mei] [SOUND|World|GlassMove]Rounds up!") ]])
            fnCutsceneBlocker()
        
            --Flags.
            VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iBoozeTableB", "N", 1.0)
            AL_SetProperty("Set Layer Disabled", "DrinkCL", false)
            AL_SetProperty("Set Layer Disabled", "DrinkDL", false)
            AL_SetProperty("Set Layer Disabled", "DrinkCU", false)
            AL_SetProperty("Set Layer Disabled", "DrinkDU", false)
            
        --Already placed.
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Mei] (I already put the drinks down, it's up to them now.)") ]])
            fnCutsceneBlocker()
        end
        
    --Phase 2:
    elseif(iDrinkingPhase == 2.0) then
        
        --Need to place booze:
        if(iBoozeTableB == 0.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Mei:[VOICE|Mei] [SOUND|World|GlassMove]Rounds up!") ]])
            fnCutsceneBlocker()
        
            --Flags.
            VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iBoozeTableB", "N", 1.0)
            AL_SetProperty("Set Layer Disabled", "DrinkDL", false)
            AL_SetProperty("Set Layer Disabled", "DrinkDU", false)
            
        --Already placed.
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Mei] (I already put the drinks down, it's up to them now.)") ]])
            fnCutsceneBlocker()
        end
        
    --Phase 3:
    elseif(iDrinkingPhase == 3.0) then
        
        --Need to place booze:
        if(iBoozeTableB == 0.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Mei:[VOICE|Mei] [SOUND|World|GlassMove]Rounds up!") ]])
            fnCutsceneBlocker()
        
            --Flags.
            VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iBoozeTableB", "N", 1.0)
            AL_SetProperty("Set Layer Disabled", "DrinkDL", false)
            AL_SetProperty("Set Layer Disabled", "DrinkDU", false)
            
        --Already placed.
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Mei] (I already put the drinks down, it's up to them now.)") ]])
            fnCutsceneBlocker()
        end
        
    --Phase 4:
    elseif(iDrinkingPhase == 4.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Mei] (Everyone is passed out.[P] No need to put a drink here.)") ]])
        fnCutsceneBlocker()
    end

-- |[Table C]|
elseif(sObjectName == "TableC") then

    --Variables.
    local iBoozeTableC   = VM_GetVar("Root/Variables/Chapter1/Vendrilstadt/iBoozeTableC", "N")
    local iTakenBooze    = VM_GetVar("Root/Variables/Chapter1/Vendrilstadt/iTakenBooze", "N")
    local iDrinkingPhase = VM_GetVar("Root/Variables/Chapter1/Vendrilstadt/iDrinkingPhase", "N")
    
    --Get the booze.
    if(iTakenBooze == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (I need to go get the drinks first.)") ]])
        fnCutsceneBlocker()
        
    --Phase 0:
    elseif(iDrinkingPhase == 0.0) then
        
        --Need to place booze:
        if(iBoozeTableC == 0.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Mei:[VOICE|Mei] [SOUND|World|GlassMove]Rounds up!") ]])
            fnCutsceneBlocker()
        
            --Flags.
            VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iBoozeTableC", "N", 1.0)
            AL_SetProperty("Set Layer Disabled", "DrinkEL", false)
            AL_SetProperty("Set Layer Disabled", "DrinkFL", false)
            AL_SetProperty("Set Layer Disabled", "DrinkEU", false)
            AL_SetProperty("Set Layer Disabled", "DrinkFU", false)
            
        --Already placed.
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Mei] (I already put the drinks down, it's up to them now.)") ]])
            fnCutsceneBlocker()
        end
        
    --Phase 1:
    elseif(iDrinkingPhase == 1.0) then
        
        --Need to place booze:
        if(iBoozeTableC == 0.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Mei:[VOICE|Mei] [SOUND|World|GlassMove]Rounds up!") ]])
            fnCutsceneBlocker()
        
            --Flags.
            VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iBoozeTableC", "N", 1.0)
            AL_SetProperty("Set Layer Disabled", "DrinkEL", false)
            AL_SetProperty("Set Layer Disabled", "DrinkFL", false)
            AL_SetProperty("Set Layer Disabled", "DrinkEU", false)
            AL_SetProperty("Set Layer Disabled", "DrinkFU", false)
            
        --Already placed.
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Mei] (I already put the drinks down, it's up to them now.)") ]])
            fnCutsceneBlocker()
        end
        
    --Phase 2:
    elseif(iDrinkingPhase == 2.0) then
        
        --Need to place booze:
        if(iBoozeTableC == 0.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Mei:[VOICE|Mei] [SOUND|World|GlassMove]Rounds up!") ]])
            fnCutsceneBlocker()
        
            --Flags.
            VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iBoozeTableC", "N", 1.0)
            AL_SetProperty("Set Layer Disabled", "DrinkEL", false)
            AL_SetProperty("Set Layer Disabled", "DrinkFL", false)
            AL_SetProperty("Set Layer Disabled", "DrinkEU", false)
            AL_SetProperty("Set Layer Disabled", "DrinkFU", false)
            
        --Already placed.
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Mei] (I already put the drinks down, it's up to them now.)") ]])
            fnCutsceneBlocker()
        end
        
    --Phase 3:
    elseif(iDrinkingPhase == 3.0) then
        
        --Need to place booze:
        if(iBoozeTableC == 0.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Mei:[VOICE|Mei] [SOUND|World|GlassMove]Rounds up!") ]])
            fnCutsceneBlocker()
        
            --Flags.
            VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iBoozeTableC", "N", 1.0)
            AL_SetProperty("Set Layer Disabled", "DrinkFL", false)
            AL_SetProperty("Set Layer Disabled", "DrinkFU", false)
            
        --Already placed.
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Mei] (I already put the drinks down, it's up to them now.)") ]])
            fnCutsceneBlocker()
        end
        
    --Phase 4:
    elseif(iDrinkingPhase == 4.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Mei] (Everyone is passed out.[P] No need to put a drink here.)") ]])
        fnCutsceneBlocker()
    end
    
-- |[Table D]|
elseif(sObjectName == "TableD") then

    --Variables.
    local iBoozeTableD   = VM_GetVar("Root/Variables/Chapter1/Vendrilstadt/iBoozeTableD", "N")
    local iTakenBooze    = VM_GetVar("Root/Variables/Chapter1/Vendrilstadt/iTakenBooze", "N")
    local iDrinkingPhase = VM_GetVar("Root/Variables/Chapter1/Vendrilstadt/iDrinkingPhase", "N")
    
    --Get the booze.
    if(iTakenBooze == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (I need to go get the drinks first.)") ]])
        fnCutsceneBlocker()
    
    --Phase 0:
    elseif(iDrinkingPhase == 0.0) then
        
        --Need to place booze:
        if(iBoozeTableD == 0.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Mei:[VOICE|Mei] [SOUND|World|GlassMove]Rounds up!") ]])
            fnCutsceneBlocker()
        
            --Flags.
            VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iBoozeTableD", "N", 1.0)
            AL_SetProperty("Set Layer Disabled", "DrinkGL", false)
            AL_SetProperty("Set Layer Disabled", "DrinkHL", false)
            AL_SetProperty("Set Layer Disabled", "DrinkGU", false)
            AL_SetProperty("Set Layer Disabled", "DrinkHU", false)
            
        --Already placed.
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Mei] (I already put the drinks down, it's up to them now.)") ]])
            fnCutsceneBlocker()
        end
        
    --Phase 1:
    elseif(iDrinkingPhase == 1.0) then
        
        --Need to place booze:
        if(iBoozeTableD == 0.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Mei:[VOICE|Mei] [SOUND|World|GlassMove]Rounds up!") ]])
            fnCutsceneBlocker()
        
            --Flags.
            VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iBoozeTableD", "N", 1.0)
            AL_SetProperty("Set Layer Disabled", "DrinkGL", false)
            AL_SetProperty("Set Layer Disabled", "DrinkHL", false)
            AL_SetProperty("Set Layer Disabled", "DrinkGU", false)
            AL_SetProperty("Set Layer Disabled", "DrinkHU", false)
            
        --Already placed.
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Mei] (I already put the drinks down, it's up to them now.)") ]])
            fnCutsceneBlocker()
        end
        
    --Phase 2:
    elseif(iDrinkingPhase == 2.0) then
        
        --Need to place booze:
        if(iBoozeTableD == 0.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Mei:[VOICE|Mei] [SOUND|World|GlassMove]Rounds up!") ]])
            fnCutsceneBlocker()
        
            --Flags.
            VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iBoozeTableD", "N", 1.0)
            AL_SetProperty("Set Layer Disabled", "DrinkGL", false)
            AL_SetProperty("Set Layer Disabled", "DrinkHL", false)
            AL_SetProperty("Set Layer Disabled", "DrinkGU", false)
            AL_SetProperty("Set Layer Disabled", "DrinkHU", false)
            
        --Already placed.
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Mei] (I already put the drinks down, it's up to them now.)") ]])
            fnCutsceneBlocker()
        end
        
    --Phase 3:
    elseif(iDrinkingPhase == 3.0) then
        
        --Need to place booze:
        if(iBoozeTableD == 0.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Mei:[VOICE|Mei] [SOUND|World|GlassMove]Rounds up!") ]])
            fnCutsceneBlocker()
        
            --Flags.
            VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iBoozeTableD", "N", 1.0)
            AL_SetProperty("Set Layer Disabled", "DrinkGL", false)
            AL_SetProperty("Set Layer Disabled", "DrinkGU", false)
            
        --Already placed.
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Mei] (I already put the drinks down, it's up to them now.)") ]])
            fnCutsceneBlocker()
        end
        
    --Phase 4:
    elseif(iDrinkingPhase == 4.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Mei] (Everyone is passed out.[P] No need to put a drink here.)") ]])
        fnCutsceneBlocker()
    end

-- |[ ========================================== Other ========================================= ]|
-- |[ ========================================== Debug ========================================= ]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
