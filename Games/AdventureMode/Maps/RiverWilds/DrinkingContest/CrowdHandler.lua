-- |[ ============================= Parallel Script - Crowd Handler ============================ ]|
--Parallel script, handles the crowd interactions. Causes them to move around.
local iJumpMode = VM_GetVar("Root/Variables/Chapter1/Vendrilstadt/iJumpMode", "N")
if(iJumpMode == 0.0) then
    
    --Stop drinking.
    local iDrinkingPhase = VM_GetVar("Root/Variables/Chapter1/Vendrilstadt/iDrinkingPhase", "N")
    
    --Run across all the NPCs. Ignore special NPCs.
    for x = 0, iNPCMaxX, 1 do
        for y = 0, iNPCMaxY, 1 do
            
            --NPC must have the first three letters be "NPC" to be valid.
            if(string.sub(saNPCList[x][y].sName, 1, 3) == "NPC" and saNPCList[x][y].sSprite ~= "Null") then
            
                --Roll.
                local iRoll = LM_GetRandomNumber(1, 12)
                local iFaceRoll = LM_GetRandomNumber(1, 6)
                if(iRoll == 12) then
            
                    --Facing east. Switches to north, south, or east at random.
                    if(saNPCList[x][y].iFacing == gci_Face_East) then
                        if(iFaceRoll == 1) then
                            fnCutsceneFace(saNPCList[x][y].sName, 0, -1)
                        elseif(iFaceRoll == 2) then
                            fnCutsceneFace(saNPCList[x][y].sName, 0, 1)
                        else
                            fnCutsceneFace(saNPCList[x][y].sName, 1, 0)
                        end
                    
                    --Facing west. Switches to north, south, or west at random.
                    elseif(saNPCList[x][y].iFacing == gci_Face_West) then
                        if(iFaceRoll == 1) then
                            fnCutsceneFace(saNPCList[x][y].sName, 0, -1)
                        elseif(iFaceRoll == 2) then
                            fnCutsceneFace(saNPCList[x][y].sName, 0, 1)
                        else
                            fnCutsceneFace(saNPCList[x][y].sName, -1, 0)
                        end
                    
                    --Facing south. Switches to west, east, or south at random.
                    else
                        if(iFaceRoll == 1) then
                            fnCutsceneFace(saNPCList[x][y].sName, -1, 0)
                        elseif(iFaceRoll == 2) then
                            fnCutsceneFace(saNPCList[x][y].sName, 1, 0)
                        else
                            fnCutsceneFace(saNPCList[x][y].sName, 0, 1)
                        end
                    end
                end
            end
        end
    end
    
    -- |[Player States]|
    --If this variable is set, don't edit player states.
    local iDontEditPlayerStates = VM_GetVar("Root/Variables/Chapter1/Vendrilstadt/iDontEditPlayerStates", "N")
    if(iDontEditPlayerStates == 0.0) then
    
        --Setter function.
        local fnSetFrame = function(sCharName, sFrameName)
            fnCutsceneSetFrame(sCharName, sFrameName)
        end
        
        --These two always stop drinking.
        fnSetFrame("Florentina", "Null")
        fnSetFrame("Miho", "Null")
        
        --Iterate. Check if a character is down, and if so, set them to wounded.
        for i = 1, 6, 1 do
            local sLetter = string.char(string.byte("A") + i - 1)
            local iPlayerDown = VM_GetVar("Root/Variables/Chapter1/Vendrilstadt/iPlayer" .. sLetter .. "Down", "N")
            if(iPlayerDown == 0.0) then
                fnSetFrame("Player" .. sLetter, "Null")
            else
                fnSetFrame("Player" .. sLetter, "Wounded")
            end
        end
    end
    
    --Create a wait action.
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    
-- |[ ======================== Parallel Script - Crowd Handler Jumping ========================= ]|
--Parallel script, handles the crowd interactions. Causes them to move around.
else

    --Jump scatterer.
    local fnScatterJump = function()
        local iRoll = LM_GetRandomNumber(100, 200) / 100.0
        return iRoll * gcfStdJumpSpeed
    end

    --Normal NPCs.
    for x = 0, iNPCMaxX, 1 do
        for y = 0, iNPCMaxY, 1 do
            
            --NPC must have the first three letters be "NPC" to be valid.
            if(string.sub(saNPCList[x][y].sName, 1, 3) == "NPC" and saNPCList[x][y].sSprite ~= "Null") then
                EM_PushEntity(saNPCList[x][y].sName)
                    TA_SetProperty("Z Speed", fnScatterJump())
                DL_PopActiveObject()
            
                --Facing east. Switches to north, south, or east at random.
                if(saNPCList[x][y].iFacing == gci_Face_East) then
                    fnCutsceneFace(saNPCList[x][y].sName, 1, 0)
                
                --Facing west. Switches to north, south, or west at random.
                elseif(saNPCList[x][y].iFacing == gci_Face_West) then
                    fnCutsceneFace(saNPCList[x][y].sName, -1, 0)
                
                --Facing south. Switches to west, east, or south at random.
                else
                    fnCutsceneFace(saNPCList[x][y].sName, 0, 1)
                end
            end
        end
    end
    
    --Special NPCs.
    local function fnJumpNPC(sNPCName)
        if(EM_Exists(sNPCName) == true) then
            EM_PushEntity(sNPCName)
                TA_SetProperty("Z Speed", fnScatterJump())
            DL_PopActiveObject()
        end
    end
    fnJumpNPC("Polaris")
    fnJumpNPC("Cassandra")
    fnJumpNPC("Adina")
    fnJumpNPC("Rochea")
    fnJumpNPC("Sharelock")
    fnJumpNPC("Nadia")
    fnJumpNPC("Lydie")
    fnJumpNPC("AlrauneA")
    fnJumpNPC("AlrauneB")
    fnJumpNPC("AlrauneC")
    
    --Function.
    local fnSetFrame = function(sCharName, sFrameName)
        fnCutsceneSetFrame(sCharName, sFrameName)
    end
    
    --Loop.
    for i = 1, 2, 1 do
    
        --Prime the drinking:
        local iDrinkingPhase = VM_GetVar("Root/Variables/Chapter1/Vendrilstadt/iDrinkingPhase", "N")
        local iDrinkTimer    = VM_GetVar("Root/Variables/Chapter1/Vendrilstadt/iDrinkTimer", "N")
        if(iDrinkTimer == 0.0) then
            VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iDrinkTimer", "N", 1.0)
            fnSetFrame("Florentina", "Drink0")
            fnSetFrame("Miho", "Drink0")
            if(iDrinkingPhase == 0.0 or iDrinkingPhase == 1.0 or iDrinkingPhase == 2.0) then
                fnSetFrame("PlayerA", "Drink0")
            end
            if(iDrinkingPhase == 0.0 or iDrinkingPhase == 1.0 or iDrinkingPhase == 2.0 or iDrinkingPhase == 3.0 or iDrinkingPhase == 4.0) then
                fnSetFrame("PlayerB", "Drink0")
                fnSetFrame("PlayerD", "Drink0")
                fnSetFrame("PlayerE", "Drink0")
            end
            if(iDrinkingPhase == 0.0 or iDrinkingPhase == 1.0 or iDrinkingPhase == 2.0 or iDrinkingPhase == 3.0) then
                fnSetFrame("PlayerC", "Drink0")
                fnSetFrame("PlayerF", "Drink0")
            end
        
        --Drinking frame 1:
        elseif(iDrinkTimer == 1.0) then
            VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iDrinkTimer", "N", 2.0)
            fnSetFrame("Florentina", "Drink1")
            fnSetFrame("Miho", "Drink1")
            if(iDrinkingPhase == 0.0 or iDrinkingPhase == 1.0 or iDrinkingPhase == 2.0) then
                fnSetFrame("PlayerA", "Drink1")
            end
            if(iDrinkingPhase == 0.0 or iDrinkingPhase == 1.0 or iDrinkingPhase == 2.0 or iDrinkingPhase == 3.0 or iDrinkingPhase == 4.0) then
                fnSetFrame("PlayerB", "Drink1")
                fnSetFrame("PlayerD", "Drink1")
                fnSetFrame("PlayerE", "Drink1")
            end
            if(iDrinkingPhase == 0.0 or iDrinkingPhase == 1.0 or iDrinkingPhase == 2.0 or iDrinkingPhase == 3.0) then
                fnSetFrame("PlayerC", "Drink1")
                fnSetFrame("PlayerF", "Drink1")
            end
        
        --Drinking frame 2:
        elseif(iDrinkTimer == 2.0) then
            VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iDrinkTimer", "N", 1.0)
            fnSetFrame("Florentina", "Drink2")
            fnSetFrame("Miho", "Drink2")
            if(iDrinkingPhase == 0.0 or iDrinkingPhase == 1.0 or iDrinkingPhase == 2.0) then
                fnSetFrame("PlayerA", "Drink2")
            end
            if(iDrinkingPhase == 0.0 or iDrinkingPhase == 1.0 or iDrinkingPhase == 2.0 or iDrinkingPhase == 3.0 or iDrinkingPhase == 4.0) then
                fnSetFrame("PlayerB", "Drink2")
                fnSetFrame("PlayerD", "Drink2")
                fnSetFrame("PlayerE", "Drink2")
            end
            if(iDrinkingPhase == 0.0 or iDrinkingPhase == 1.0 or iDrinkingPhase == 2.0 or iDrinkingPhase == 3.0) then
                fnSetFrame("PlayerC", "Drink2")
                fnSetFrame("PlayerF", "Drink2")
            end
        end
    
        --Wait a bit.
        fnCutsceneWait(12)
        fnCutsceneBlocker()
    end
end
