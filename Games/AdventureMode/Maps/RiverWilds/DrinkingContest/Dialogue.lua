-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[Common]|
    local iDrinkingPhase = VM_GetVar("Root/Variables/Chapter1/Vendrilstadt/iDrinkingPhase", "N")
    local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
    
    -- |[Nadia]|
    --Most players will have met Nadia by this point.
    if(sActorName == "Nadia") then
        
        --Variables.
        local iSpokeToNadia = VM_GetVar("Root/Variables/Chapter1/Vendrilstadt/iSpokeToNadia", "N")
        VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iSpokeToNadia", "N", 1.0)
        
        --First pass.
        if(iSpokeToNadia == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Nadia", "Neutral") ]])
            fnCutscene([[ Append("Mei:[E|Happy] Nadia![P] You came![B][C]") ]])
            fnCutscene([[ Append("Nadia:[E|Neutral] Of course![P] I never miss a drinking contest unless the Cap'n says so![B][C]") ]])
            fnCutscene([[ Append("Nadia:[E|Neutral] That goes triple if you and Florry are involved![B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] You know what's at stake, right?[B][C]") ]])
            fnCutscene([[ Append("Nadia:[E|Neutral] Oh yeah, the little ones said this kitsune is a bounty hunter.[B][C]") ]])
            fnCutscene([[ Append("Nadia:[E|Neutral] I'm not worried, though.[P] Florry's always got a trick up her sleeve.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] But Miho has tricks, too.[B][C]") ]])
            fnCutscene([[ Append("Nadia:[E|Neutral] As if she could outsmart Florentina![B][C]") ]])
            fnCutscene([[ Append("Nadia:[E|Neutral] Go Florentina![P] The entire forest has your back! Woooo!") ]])
            fnCutsceneBlocker()
        
        --Repeats.
        else
        
            if(iDrinkingPhase == 0.0) then
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Nadia", "Neutral") ]])
                fnCutscene([[ Append("Nadia:[E|Neutral] Give them heck, Florentina![B][C]") ]])
                fnCutscene([[ Append("Nadia:[E|Neutral] Rochea said I'm not supposed to cuss in front of the little ones, but they're everywhere![B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Smirk] You'll just need to learn to watch your language.[B][C]") ]])
                fnCutscene([[ Append("Nadia:[E|Neutral] Get em Florry![P] Drink till she drops!") ]])
                fnCutsceneBlocker()
                
            elseif(iDrinkingPhase == 1.0) then
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Nadia", "Neutral") ]])
                fnCutscene([[ Append("Nadia:[E|Neutral] Woo, get em, Florry![B][C]") ]])
                fnCutscene([[ Append("Nadia:[E|Neutral] Wheee, she's gonna win!") ]])
                fnCutsceneBlocker()
                
            elseif(iDrinkingPhase == 2.0) then
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Nadia", "Neutral") ]])
                fnCutscene([[ Append("Nadia:[E|Neutral] One down![P] You think she's getting a little tipsy?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Smirk] Who, Florentina, or Miho?[B][C]") ]])
                fnCutscene([[ Append("Nadia:[E|Neutral] Miho, all the way.[P] Florentina has this in the bag!") ]])
                fnCutsceneBlocker()
                
            elseif(iDrinkingPhase == 3.0) then
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Nadia", "Neutral") ]])
                fnCutscene([[ Append("Nadia:[E|Neutral] I think I saw Florentina wobble a bit there.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Surprise] Really?[P] You think she'll make it?[B][C]") ]])
                fnCutscene([[ Append("Nadia:[E|Neutral] I'm more worried about Miho.[P] I don't know if she can take much more!") ]])
                fnCutsceneBlocker()
                
            elseif(iDrinkingPhase == 4.0) then
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Nadia", "Neutral") ]])
                fnCutscene([[ Append("Nadia:[E|Neutral] One on one![P] I can't tell who's gonna win![B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Surprise] Miho can really hold her own.[B][C]") ]])
                fnCutscene([[ Append("Nadia:[E|Neutral] Go Florentina![P] Just focus and stand up straight!") ]])
                fnCutsceneBlocker()
            end
        end
        
    -- |[Polaris]|
    --You can't bypass Polaris without using the debug menu.
    elseif(sActorName == "Polaris") then
        
        --Variables.
        local iSpokeToPolaris = VM_GetVar("Root/Variables/Chapter1/Vendrilstadt/iSpokeToPolaris", "N")
        VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iSpokeToPolaris", "N", 1.0)
        
        --First pass.
        if(iSpokeToPolaris == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Polaris", "Neutral") ]])
            fnCutscene([[ Append("Mei:[E|Happy] Miss Polaris![P] Glad to see you here![B][C]") ]])
            fnCutscene([[ Append("Polaris:[E|Upset] That trading post guard was absolutely insistent.[P] Nadia, I think.[B][C]") ]])
            fnCutscene([[ Append("Polaris:[E|Neutral] Is this some sort of local tradition?[P] I'm not much of a drinker myself.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] Florentina says it's a game that goes back centuries.[B][C]") ]])
            fnCutscene([[ Append("Polaris:[E|Neutral] Sheesh, you get into situations don't you![B][C]") ]])
            fnCutscene([[ Append("Polaris:[E|Neutral] Can't say I don't hope you win, but this Miho person looks pretty fearsome.[P] Be on the lookout.") ]])
            fnCutsceneBlocker()
        
        --Repeats.
        else
        
            if(iDrinkingPhase == 0.0) then
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Polaris", "Neutral") ]])
                fnCutscene([[ Append("Polaris:[E|Neutral] The first round is the hardest, I'd say.[P] The more players, the less likely you are to know your own drink's contents.[B][C]") ]])
                fnCutscene([[ Append("Polaris:[E|Thinking] Then again, it's the round where you're the least inebriated.[P] I hope Florentina is as clever as she thinks -[P] when she's drunk!") ]])
                fnCutsceneBlocker()
                
            elseif(iDrinkingPhase == 1.0) then
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Polaris", "Neutral") ]])
                fnCutscene([[ Append("Polaris:[E|Thinking] What trick does Florentina have that I am not seeing?[P] Perhaps a trick mug, or maybe she intends to attack Miho...?") ]])
                fnCutsceneBlocker()
                
            elseif(iDrinkingPhase == 2.0) then
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Polaris", "Neutral") ]])
                fnCutscene([[ Append("Polaris:[E|Upset] That guy seemed a bit too eager, poor fellow.[P] Even I could probably handle more than that.") ]])
                fnCutsceneBlocker()
                
            elseif(iDrinkingPhase == 3.0) then
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Polaris", "Neutral") ]])
                fnCutscene([[ Append("Polaris:[E|Neutral] The field is winnowing.[P] If she's going to make her move, she best do it when she can still stand up straight.") ]])
                fnCutsceneBlocker()
                
            elseif(iDrinkingPhase == 4.0) then
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Polaris", "Neutral") ]])
                fnCutscene([[ Append("Polaris:[E|Upset] I'm a little worried.[P] Neither of them look like they can take another one.[B][C]") ]])
                fnCutscene([[ Append("Polaris:[E|Upset] Though they could be faking it.[P] By the way, do you think Miho is looking a little...[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Blush] Flirtatious?[B][C]") ]])
                fnCutscene([[ Append("Polaris:[E|Neutral] Heh, yes.[P] Maybe this was the plan all along?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Who, Miho's, or Florentina's?[B][C]") ]])
                fnCutscene([[ Append("Polaris:[E|Neutral] Good point, Ha ha![P] Go, Florentina!") ]])
                fnCutsceneBlocker()
            end
        end
        
    -- |[Lydie]|
    --Have to have ghost form and done the manor.
    elseif(sActorName == "Lydie") then
        
        --Variables.
        local iSpokeToLydie = VM_GetVar("Root/Variables/Chapter1/Vendrilstadt/iSpokeToLydie", "N")
        VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iSpokeToLydie", "N", 1.0)
        
        --First pass.
        if(iSpokeToLydie == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Lydie", "Neutral") ]])
            fnCutscene([[ Append("Mei:[E|Surprise] Lydie![P] What are you doing here?[B][C]") ]])
            fnCutscene([[ Append("Lydie:[E|Neutral] Natalie![P] Or Mei, whichever.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Happy] Both are fine![P] So, how did you hear about this?[B][C]") ]])
            fnCutscene([[ Append("Lydie:[E|Neutral] I'm feeling...[P] clearheaded.[P] So I decided to take a look out of the manor.[B][C]") ]])
            fnCutscene([[ Append("Lydie:[E|Neutral] I accidentally scared an alraune, but after she managed to stop shaking, she told me you were getting into some sort of trouble.[B][C]") ]])
            if(sMeiForm == "Ghost") then
                fnCutscene([[ Append("Lydie:[E|Neutral] Shame it wasn't you doing the drinking.[P] I'm sure you could drink anyone under the table.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Smirk] I don't know, maybe the alcohol would mix with the -[P] ectoplasm?[B][C]") ]])
                fnCutscene([[ Append("Lydie:[E|Neutral] Maybe I should find that out next![B][C]") ]])
            else
                fnCutscene([[ Append("Lydie:[E|Neutral] Out of curiosity, is Natalie still in there when you're not a -[P] ghost?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Smirk] She and I are the same, now.[P] Don't worry, we're having a blast![B][C]") ]])
                fnCutscene([[ Append("Lydie:[E|Neutral] Me too![P] I'm going to tell all the other maids about how much fun it is out here![B][C]") ]])
                fnCutscene([[ Append("Lydie:[E|Neutral] Well, whenever they 'wake up', that is.[P] Most of them are coming about, but slowly.[B][C]") ]])
            end
            fnCutscene([[ Append("Lydie:[E|Neutral] Anyway, I hope your friend wins![P] Go Florentina!") ]])
            fnCutsceneBlocker()
        
        --Repeats.
        else
        
            if(iDrinkingPhase == 0.0) then
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Lydie", "Neutral") ]])
                fnCutscene([[ Append("Lydie:[E|Neutral] Your friend's name was Florentina, right?[P] Did I get that right?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Smirk] Yep, that's her![B][C]") ]])
                fnCutscene([[ Append("Lydie:[E|Neutral] I heard she's got a lot of money.[P] You think she needs a cleaning lady?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Surprise] You want to be a maid again, after all this?[B][C]") ]])
                fnCutscene([[ Append("Lydie:[E|Neutral] It's what I'm good at, you know.[P] I'll ask her later if she wants to hire anyone.") ]])
                fnCutsceneBlocker()
                
            elseif(iDrinkingPhase == 1.0) then
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Lydie", "Neutral") ]])
                fnCutscene([[ Append("Lydie:[E|Neutral] She can really hold her liquor![P] Is she single?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Who, Florentina or Miho?[B][C]") ]])
                fnCutscene([[ Append("Lydie:[E|Neutral] Miho, the bounty hunter.[P] She's so cool![P] Look at her outfit, I bet she beats up all kinds of villains![B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Laugh] In retrospect, Florentina doesn't seem like a villain, but maybe she is!") ]])
                fnCutsceneBlocker()
                
            elseif(iDrinkingPhase == 2.0) then
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Lydie", "Neutral") ]])
                fnCutscene([[ Append("Lydie:[E|Neutral] You're not [P]*cheating*[P], are you, Natalie?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Offended] N-[P]no![P] How dare you accuse me![B][C]") ]])
                fnCutscene([[ Append("Lydie:[E|Neutral] Ha ha![P] You get flustered so easily![B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Smirk] Don't scare me like that![P] I don't know what Miho will do if she catches someone cheating![B][C]") ]])
                fnCutscene([[ Append("Lydie:[E|Neutral] I think you could take her, honestly.") ]])
                fnCutsceneBlocker()
                
            elseif(iDrinkingPhase == 3.0) then
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Lydie", "Neutral") ]])
                fnCutscene([[ Append("Lydie:[E|Neutral] This is getting intense.[P] Get them the next round, quick!") ]])
                fnCutsceneBlocker()
                
            elseif(iDrinkingPhase == 4.0) then
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Lydie", "Neutral") ]])
                fnCutscene([[ Append("Lydie:[E|Neutral] Oh my goodness, who's going to win?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Smirk] Florentina by a dry mile![B][C]") ]])
                fnCutscene([[ Append("Lydie:[E|Neutral] I hope so![P] Go, Florentina!") ]])
                fnCutsceneBlocker()
            end
        end
        
    -- |[Cassandra]|
    --Have to have saved Cassandra, no werecat variant!
    elseif(sActorName == "Cassandra") then
        
        --Variables.
        local iSpokeToCassandra = VM_GetVar("Root/Variables/Chapter1/Vendrilstadt/iSpokeToCassandra", "N")
        VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iSpokeToCassandra", "N", 1.0)
        
        --First pass.
        if(iSpokeToCassandra == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] Cassandra![P] Are you feeling all right?[B][C]") ]])
            fnCutscene([[ Append("Cassandra:[E|Neutral] Oh, yes![P] I still feel a little weak, but I could handle the walk here.[B][C]") ]])
            fnCutscene([[ Append("Cassandra:[E|Neutral] Those cats took a lot more blood out of me than I thought, but I'll be fine.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] Good to hear![P] So, who do you think is going to win?[B][C]") ]])
            fnCutscene([[ Append("Cassandra:[E|Neutral] Hate to say it, but my money's on the kitsune.[P] She looks like she can handle a lot of booze.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] Oh yeah?[P] You bet on that with anyone?[B][C]") ]])
            fnCutscene([[ Append("Cassandra:[E|Neutral] I put fifty platina on it against that gruff guy at the trading post.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Happy] Florentina has it in the bag, trust me.[P] I don't know what her plan is, but she's got one.[B][C]") ]])
            fnCutscene([[ Append("Cassandra:[E|Neutral] Probably.[P] Good luck in any case!") ]])
            fnCutsceneBlocker()
        
        --Repeats.
        else
        
            if(iDrinkingPhase == 0.0) then
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
                fnCutscene([[ Append("Cassandra:[E|Neutral] I wonder if I should have signed up, but I'm not sure a lot of liquor is a good idea when you're recovering.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Happy] Watching is fun enough![B][C]") ]])
                fnCutscene([[ Append("Cassandra:[E|Neutral] Yeah, but could I beat Florentina?[P] Now I'm curious!") ]])
                fnCutsceneBlocker()
                
            elseif(iDrinkingPhase == 1.0) then
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
                fnCutscene([[ Append("Cassandra:[E|Neutral] Go, Miho![P] Drink drink drink![B][C]") ]])
                fnCutscene([[ Append("Cassandra:[E|Neutral] I got money on you, don't you screw this up for me!") ]])
                fnCutsceneBlocker()
                
            elseif(iDrinkingPhase == 2.0) then
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
                fnCutscene([[ Append("Cassandra:[E|Neutral] Gotta give that guy some credit, he had a lot of bravery.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Smirk] I think he has a history with Florentina.[B][C]") ]])
                fnCutscene([[ Append("Cassandra:[E|Neutral] I bet.[P] She seems to know everyone in Trannadar.") ]])
                fnCutsceneBlocker()
                
            elseif(iDrinkingPhase == 3.0) then
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
                fnCutscene([[ Append("Cassandra:[E|Neutral] You still sure Florentina is going to take this one?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Smirk] Never lose faith in your friends.[B][C]") ]])
                fnCutscene([[ Append("Cassandra:[E|Neutral] Huh, well, what if you have money on it?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Laugh] Oh, then faith goes right out the window!") ]])
                fnCutsceneBlocker()
                
            elseif(iDrinkingPhase == 4.0) then
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
                fnCutscene([[ Append("Cassandra:[E|Neutral] Come on, fifty-fifty.[P] Come onnnn....[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Laugh] Either way, you win, right?[P] Either your friend gets kidnapped by a bounty hunter, or you get fifty platina![B][C]") ]])
                fnCutscene([[ Append("Cassandra:[E|Neutral] Oh man, if she loses, you'll break her out, right?[P] I don't want to see Florentina go to jail.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Smirk] Absolutely.[P] That'd be small potatoes.[P] I wonder who put the bounty out, though...") ]])
                fnCutsceneBlocker()
            end
        end
        
    -- |[Share;lock]|
    --Hard to miss, but some players might.
    elseif(sActorName == "Sharelock") then
        
        --Variables.
        local iSpokeToSharelock = VM_GetVar("Root/Variables/Chapter1/Vendrilstadt/iSpokeToSharelock", "N")
        VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iSpokeToSharelock", "N", 1.0)
        
        --First pass.
        if(iSpokeToSharelock == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sharelock", "Neutral") ]])
            fnCutscene([[ Append("Share;Lock:[E|Neutral] Seems you two are just full of surprises, aren't you.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] It's one adventure after the other![B][C]") ]])
            fnCutscene([[ Append("Share;Lock:[E|Neutral] So long as you have confidence in Florentina, I suppose I cannot fault your commitment.[B][C]") ]])
            fnCutscene([[ Append("Share;Lock:[E|Thinking] But what is her angle?[P] She must have a plan, but absolutely nothing about what she's said or done betrays it.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] Maybe she hasn't put it into motion?[B][C]") ]])
            fnCutscene([[ Append("Share;Lock:[E|Thinking] Perhaps, or perhaps this whole contest was rigged from the start.[P] We shall see.[B][C]") ]])
            fnCutscene([[ Append("Share;Lock:[E|Thinking] Wish her luck for me.") ]])
            fnCutsceneBlocker()
        
        --Repeats.
        else
        
            if(iDrinkingPhase == 0.0) then
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sharelock", "Neutral") ]])
                fnCutscene([[ Append("Share;Lock:[E|Neutral] I'm keeping my eyes on Florentina's hands.[P] When you're at a disadvantage, sometimes a quick pickpocket is all you need.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Surprise] Well, she does talk about robbery a lot.[B][C]") ]])
                fnCutscene([[ Append("Share;Lock:[E|Thinking] She doesn't strike me as a common burglar, though.[P] No, there's some other factor I'm missing here.") ]])
                fnCutsceneBlocker()
                
            elseif(iDrinkingPhase == 1.0) then
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sharelock", "Neutral") ]])
                fnCutscene([[ Append("Share;Lock:[E|Neutral] My, how exciting![P] Unfortunately, I do not know how well my systems would handle alcohol.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Smirk] Do you know exactly what metal you're made of?[B][C]") ]])
                fnCutscene([[ Append("Share;Lock:[E|Neutral] Brass, or bronze, I think.[P] I got a lead on someone who may be able to help me.[B][C]") ]])
                fnCutscene([[ Append("Share;Lock:[E|Neutral] But Florentina is clearly made of something sterner yet.[P] She looks quite alert.") ]])
                fnCutsceneBlocker()
                
            elseif(iDrinkingPhase == 2.0) then
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sharelock", "Neutral") ]])
                fnCutscene([[ Append("Share;Lock:[E|Neutral] I am starting to think Florentina is playing a front.[B][C]") ]])
                fnCutscene([[ Append("Share;Lock:[E|Thinking] Her strategy is simply to bluff her opponent into thinking she is consuming only water, and she has practiced balance and speech to avoid betraying her state.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Smirk] I think her plans run deeper than that.[B][C]") ]])
                fnCutscene([[ Append("Share;Lock:[E|Neutral] Remind me never to underestimate her.") ]])
                fnCutsceneBlocker()
                
            elseif(iDrinkingPhase == 3.0) then
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sharelock", "Neutral") ]])
                fnCutscene([[ Append("Share;Lock:[E|Neutral] They're both looking pretty shaky right now, to say nothing of the other contestants.[B][C]") ]])
                fnCutscene([[ Append("Share;Lock:[E|Neutral] I don't like to admit it, but I haven't seen anything tricky yet.[P] What is her plan?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Smirk] I have total faith in Florentina.[P] I know she'll make it.") ]])
                fnCutsceneBlocker()
                
            elseif(iDrinkingPhase == 4.0) then
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sharelock", "Neutral") ]])
                fnCutscene([[ Append("Share;Lock:[E|AhHa] Stu-[P]pendous![P] Right down to the wire![P] She certainly is making it close if nothing else!") ]])
                fnCutsceneBlocker()
            end
        end
        
    -- |[Adina]|
    --Must have at least met her, but her initial dialogue changes based on how long you stayed at the salt flats.
    elseif(sActorName == "Adina") then
        
        --Variables.
        local iSpokeToAdina  = VM_GetVar("Root/Variables/Chapter1/Vendrilstadt/iSpokeToAdina",  "N")
        local iMeiLovesAdina = VM_GetVar("Root/Variables/Chapter1/Scenes/iMeiLovesAdina", "N")
        VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iSpokeToAdina", "N", 1.0)
        
        --First pass.
        if(iSpokeToAdina == 0.0) then
            
            --Mei and Adina are friends.
            if(iMeiLovesAdina == 0.0) then
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])
                fnCutscene([[ Append("Mei:[E|Happy] Miss Adina![P] What are you doing here?[B][C]") ]])
                fnCutscene([[ Append("Adina:[E|Neutral] I came to watch Florentina do her thing.[P] She has quite a reputation in the area, you know.[B][C]") ]])
                fnCutscene([[ Append("Adina:[E|Neutral] I don't particularly like her brash methods, but she is from Trannadar.[P] She is one of us.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Really?[P] Do you think that way of me?[B][C]") ]])
                fnCutscene([[ Append("Adina:[E|Neutral] If you make this place your home and live with its people, of course.[P] You are welcome here, Mei.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Happy] Thanks![B][C]") ]])
                fnCutscene([[ Append("Adina:[E|Neutral] By the way, have you ever tried beer?[P] I don't think I've ever drank anything alcoholic.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Smirk] Sometimes we would drink wines back home, but I'm not a drinker, either.[B][C]") ]])
                fnCutscene([[ Append("Adina:[E|Neutral] Hmm.[P] I wonder what Florentina's plan is, then.[P] Be sure to keep an eye on her.[B][C]") ]])
                fnCutscene([[ Append("Adina:[E|Neutral] The rules of fair competition mean nothing to you, you are her friend.[P] Protect her.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Happy] Naturally!") ]])
                fnCutsceneBlocker()
        
            --Mei and Adina are lovers.
            else
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])
                fnCutscene([[ Append("Mei:[E|Happy] Mistress Adina![P] What are you doing here?[B][C]") ]])
                fnCutscene([[ Append("Adina:[E|Neutral] I came to watch Florentina do her thing.[P] She has quite a reputation in the area, you know.[B][C]") ]])
                fnCutscene([[ Append("Adina:[E|Neutral] I don't particularly like her brash methods, but she is from Trannadar.[P] She is one of us.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Really?[P] Do you think that way of me?[B][C]") ]])
                fnCutscene([[ Append("Adina:[E|Neutral] Of course, my loving thrall.[P] You are always welcome to stay when the road's call is quieted.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Blush] I'd love to, but I need to make sure Florentina stays out of trouble.[B][C]") ]])
                fnCutscene([[ Append("Adina:[E|Neutral] By the way, have you ever tried beer?[P] I don't think I've ever drank anything alcoholic.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Smirk] Sometimes we would drink wines back home, but I'm not a drinker, either.[B][C]") ]])
                fnCutscene([[ Append("Adina:[E|Neutral] Hmm.[P] I wonder what Florentina's plan is, then.[P] Be sure to keep an eye on her.[B][C]") ]])
                fnCutscene([[ Append("Adina:[E|Neutral] The rules of fair competition mean nothing to you, you are her friend.[P] Protect her.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Happy] Yes, mistress!") ]])
                fnCutsceneBlocker()
        
        
            end
        
        --Repeats.
        else
        
            if(iDrinkingPhase == 0.0) then
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])
                fnCutscene([[ Append("Adina:[E|Neutral] This Miho person does not seem to be a callous mercenary.[P] There is more to her.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Smirk] What do you mean?[B][C]") ]])
                fnCutscene([[ Append("Adina:[E|Neutral] She is seeking enlightenment, but she is prepared to fight for it.[P] She is a wise person.[B][C]") ]])
                fnCutscene([[ Append("Adina:[E|Neutral] That is what the little ones have whispered to me.[P] She takes care to honor the spirits of the wild.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Huh.[P] She's mostly been shouting and threatening me a lot.[B][C]") ]])
                fnCutscene([[ Append("Adina:[E|Neutral] Because she believes Florentina is an enemy, and a criminal, yes.[P] The bounty on her head is quite high.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Do you know who put that bounty out?[B][C]") ]])
                fnCutscene([[ Append("Adina:[E|Neutral] No, and I don't think any of the alraunes do, either.[P] She must have done something truly heinous.") ]])
                fnCutsceneBlocker()
                
            elseif(iDrinkingPhase == 1.0) then
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])
                fnCutscene([[ Append("Adina:[E|Neutral] The challenge begins in the third or fourth round, as one's focus begins to be on staying upright rather than calculating odds.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Happy] Florentina's really smart, she has this one.[B][C]") ]])
                fnCutscene([[ Append("Adina:[E|Neutral] As is this bounty hunter, I am afraid.[P] Has Florentina met her match?") ]])
                fnCutsceneBlocker()
                
            elseif(iDrinkingPhase == 2.0) then
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])
                fnCutscene([[ Append("Adina:[E|Neutral] That fellow has a great deal of courage.[P] Admirable, for a human.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Smirk] Do you know him?[B][C]") ]])
                fnCutscene([[ Append("Adina:[E|Neutral] I know all the people of Trannadar, through the little ones.[P] They tell me he is a diligent soldier but is prone to making exaggerations.[B][C]") ]])
                fnCutscene([[ Append("Adina:[E|Neutral] He dislikes animals except for birds and often spends time on his duty watching them.[P] He is unmarried.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Surprise] The little ones sure do know a lot about people![B][C]") ]])
                fnCutscene([[ Append("Adina:[E|Neutral] I can judge a person's character without ever meeting them.[P] They tell me who people are when they think they are alone.") ]])
                fnCutsceneBlocker()
                
            elseif(iDrinkingPhase == 3.0) then
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])
                fnCutscene([[ Append("Adina:[E|Neutral] The field is narrowing.[P] The kitsune looks unsteady.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] So does Florentina, I think she's just better at hiding it.[B][C]") ]])
                fnCutscene([[ Append("Adina:[E|Neutral] I have faith in her.[P] She may not call me her leaf-sister, but she is one of us at heart.") ]])
                fnCutsceneBlocker()
                
            elseif(iDrinkingPhase == 4.0) then
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])
                fnCutscene([[ Append("Adina:[E|Neutral] Oh my, my heart flutters.[P] Who will win?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Smirk] Go, Florentina![B][C]") ]])
                fnCutscene([[ Append("Adina:[E|Neutral] Yes, go, Florentina.[P] I believe in you.") ]])
                fnCutsceneBlocker()
            end
        end
        
    -- |[Rochea]|
    --Must have alraune form.
    elseif(sActorName == "Rochea") then
        
        --Variables.
        local iSpokeToRochea = VM_GetVar("Root/Variables/Chapter1/Vendrilstadt/iSpokeToRochea", "N")
        VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iSpokeToRochea", "N", 1.0)

        --First pass.
        if(iSpokeToRochea == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Rochea", "Neutral") ]])
            fnCutscene([[ Append("Mei:[E|Happy] Leaf-sister![P] You have come![B][C]") ]])
            fnCutscene([[ Append("Rochea:[E|Neutral] Hello, leaf-sister Mei.[P] It is good to see you again.[B][C]") ]])
            fnCutscene([[ Append("Rochea:[E|Neutral] I trust you are here to keep an eye on Florentina?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] Or she's keeping her eye on me.[P] How did you hear about the contest?[B][C]") ]])
            fnCutscene([[ Append("Rochea:[E|Neutral] Nadia told the whole forest.[P] She loves these games dearly.[B][C]") ]])
            fnCutscene([[ Append("Rochea:[E|Neutral] Florentina will win this contest, Mei.[P] Rest assured.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] Are you sure?[P] Miho looks pretty clever.[B][C]") ]])
            fnCutscene([[ Append("Rochea:[E|Neutral] She is, the little ones say she is wise and respectful.[P] But she does not know one thing that we alraunes do.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] I haven't been an alraune for long.[P] What do you know?[B][C]") ]])
            fnCutscene([[ Append("Rochea:[E|Neutral] It will become quite clear in a short time.[P] Please enjoy the show.") ]])
            fnCutsceneBlocker()
        
        --Repeats.
        else
        
            if(iDrinkingPhase == 0.0) then
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Rochea", "Neutral") ]])
                fnCutscene([[ Append("Mei:[E|Smirk] Did you come her to cheer on Florentina?[B][C]") ]])
                fnCutscene([[ Append("Rochea:[E|Neutral] Please do not tell her we did so.[P] If she asks, we were here to support you.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Why don't you two get along?[B][C]") ]])
                fnCutscene([[ Append("Rochea:[E|Neutral] A disagreement that cannot be covered by niceties.[P] Even then, she is an alraune, and she abides by the laws of nature.[B][C]") ]])
                fnCutscene([[ Append("Rochea:[E|Neutral] She will always have a place with us so long as she does.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Smirk] I guess it's nice to know there's forgiveness waiting out there.") ]])
                fnCutsceneBlocker()
                
            elseif(iDrinkingPhase == 1.0) then
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Rochea", "Neutral") ]])
                fnCutscene([[ Append("Rochea:[E|Neutral] Oh, I can see already that this contest will be one to remember.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Smirk] Go, Florentina![B][C]") ]])
                fnCutscene([[ Append("Rochea:[E|Neutral] The wilds are with you, Florentina!") ]])
                fnCutsceneBlocker()
                
            elseif(iDrinkingPhase == 2.0) then
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Rochea", "Neutral") ]])
                fnCutscene([[ Append("Rochea:[E|Neutral] He was brash and brazen.[P] Typical, for a human, but his spirit is admirable.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Smirk] Who, that Barry guy?[P] Yeah, but his eyes were bigger than his stomach.[B][C]") ]])
                fnCutscene([[ Append("Rochea:[E|Neutral] Is that a saying where you are from?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Smirk] If you want to borrow it, it's all yours!") ]])
                fnCutsceneBlocker()
                
            elseif(iDrinkingPhase == 3.0) then
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Rochea", "Neutral") ]])
                fnCutscene([[ Append("Rochea:[E|Neutral] The field narrows.[P] It is no surprise our antagonist remains standing.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Smirk] Miho's not going to go down easy.[B][C]") ]])
                fnCutscene([[ Append("Rochea:[E|Neutral] Sporting of her to put up a good struggle.[P] We would not want to come all the way here for nothing.") ]])
                fnCutsceneBlocker()
                
            elseif(iDrinkingPhase == 4.0) then
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Rochea", "Neutral") ]])
                fnCutscene([[ Append("Rochea:[E|Neutral] This contest is Florentina's, I am sure of it.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Smirk] Do I hear a slight waver in your voice?[B][C]") ]])
                fnCutscene([[ Append("Rochea:[E|Neutral] She has my total confidence.") ]])
                fnCutsceneBlocker()
            end
        end
        
    -- |[Contestants]|
    --Other drinkers. They pass out after a certain number of rounds.
    elseif(sActorName == "PlayerA") then
        if(iDrinkingPhase == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Barry:[VOICE|MercM] Let's get this show on the road.[P] You're going down, alraune!") ]])
            fnCutsceneBlocker()
            
        elseif(iDrinkingPhase == 1.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Barry:[VOICE|MercM] Geez, why did I switch with Bette?[P] Ughhhh...[B][C]") ]])
            fnCutscene([[ Append("Mei:[VOICE|Mei] You didn't, Florentina switched with you.[B][C]") ]])
            fnCutscene([[ Append("Barry:[VOICE|MercM] Uh oh...[P] That is not a good sign...") ]])
            fnCutsceneBlocker()
            
        elseif(iDrinkingPhase == 2.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Leader] (He passed out.)") ]])
            fnCutsceneBlocker()
            
        elseif(iDrinkingPhase == 3.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Leader] (He passed out.)") ]])
            fnCutsceneBlocker()
            
        elseif(iDrinkingPhase == 4.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Leader] (He passed out.)") ]])
            fnCutsceneBlocker()
        end
            
    elseif(sActorName == "PlayerB") then
        if(iDrinkingPhase == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Bette:[VOICE|MercF] I don't drink that much, but I'm not saying no to free booze!") ]])
            fnCutsceneBlocker()
            
        elseif(iDrinkingPhase == 1.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Bette:[VOICE|MercF] Darn it, I got water.[P] Oh well, next round!") ]])
            fnCutsceneBlocker()
            
        elseif(iDrinkingPhase == 2.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Bette:[VOICE|MercF] Water again, sheesh.[P] I'll get something with some bite next round.") ]])
            fnCutsceneBlocker()
            
        elseif(iDrinkingPhase == 3.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Bette:[VOICE|MercF] I keep getting water![P] This isn't fair![P] Give me some booze!") ]])
            fnCutsceneBlocker()
            
        elseif(iDrinkingPhase == 4.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Leader] (She passed out.)") ]])
            fnCutsceneBlocker()
        end
        
    elseif(sActorName == "PlayerC") then
        if(iDrinkingPhase == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Cris:[VOICE|HumanF0] Yeah![P] Everyone's looking at me![P] I'm gonna win, I know it!") ]])
            fnCutsceneBlocker()
            
        elseif(iDrinkingPhase == 1.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Cris:[VOICE|HumanF0] *Hic*[P] Whoops, that one was a bit stiff.[P] I got this!") ]])
            fnCutsceneBlocker()
            
        elseif(iDrinkingPhase == 2.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Cris:[VOICE|HumanF0] Hey, you're really pretty, you know that?[P] It's not just the drink talking, I mean it.") ]])
            fnCutsceneBlocker()
            
        elseif(iDrinkingPhase == 3.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Leader] (She passed out.)") ]])
            fnCutsceneBlocker()
            
        elseif(iDrinkingPhase == 4.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Leader] (She passed out.)") ]])
            fnCutsceneBlocker()
        end
        
    elseif(sActorName == "PlayerD") then
        if(iDrinkingPhase == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Nancy:[VOICE|HumanF1] IN IT, TO, WIN IT.[P] AWOOOO![P] BRING IT ON!") ]])
            fnCutsceneBlocker()
            
        elseif(iDrinkingPhase == 1.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Nancy:[VOICE|HumanF1] YEAH, FEEL IT.[P] FEEL IT.[P] HERE COMES THE NIGHT TRAIN, CHOO CHOO!") ]])
            fnCutsceneBlocker()
            
        elseif(iDrinkingPhase == 2.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Nancy:[VOICE|HumanF1] CHOO CHOO, CHOO CHOO Cha oh my.[P] Oh my.[P] Feelings of invincibility, wearing off.[P] Change of plans.") ]])
            fnCutsceneBlocker()
            
        elseif(iDrinkingPhase == 3.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Nancy:[VOICE|HumanF1] Ah, moonshine, the great communicator...[P] I got something to saaayyy![P] *hic*") ]])
            fnCutsceneBlocker()
            
        elseif(iDrinkingPhase == 4.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Leader] (She passed out.)") ]])
            fnCutsceneBlocker()
        end
        
    elseif(sActorName == "PlayerE") then
        if(iDrinkingPhase == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Ira:[VOICE|Hypatia] My strategy is simple.[P] Figure out who's the least drunk and switch with them.[P] Easy!") ]])
            fnCutsceneBlocker()
            
        elseif(iDrinkingPhase == 1.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Ira:[VOICE|Hypatia] I got water last round![P] No problem, just gotta keep doing that.") ]])
            fnCutsceneBlocker()
            
        elseif(iDrinkingPhase == 2.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Ira:[VOICE|Hypatia] Hoo, that hooch is like getting bucked by a goat.[P] But I can keep this up.") ]])
            fnCutsceneBlocker()
            
        elseif(iDrinkingPhase == 3.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Ira:[VOICE|Hypatia] Oh I got dubble [P]*hic*[P] moonshine, this isn't going well...") ]])
            fnCutsceneBlocker()
            
        elseif(iDrinkingPhase == 4.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Leader] (She passed out.)") ]])
            fnCutsceneBlocker()
        end
        
    elseif(sActorName == "PlayerF") then
        if(iDrinkingPhase == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Lore:[VOICE|Alicia] I hope I get beer, I love moosefur beer!") ]])
            fnCutsceneBlocker()
            
        elseif(iDrinkingPhase == 1.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Lore:[VOICE|Alicia] Nothing beats a sunny day, free beer, and people yelling at you![P] Bottom's up!") ]])
            fnCutsceneBlocker()
            
        elseif(iDrinkingPhase == 2.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Lore:[VOICE|Alicia] Ha ha, I'm having too much *hic* fun![P] Wheee!") ]])
            fnCutsceneBlocker()
            
        elseif(iDrinkingPhase == 3.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Leader] (She passed out.)") ]])
            fnCutsceneBlocker()
            
        elseif(iDrinkingPhase == 4.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Leader] (She passed out.)") ]])
            fnCutsceneBlocker()
        end
    
    -- |[Olivia]|
    elseif(sActorName == "Olivia") then
    
        --Variables.
        local iTakenBooze  = VM_GetVar("Root/Variables/Chapter1/Vendrilstadt/iTakenBooze", "N")
        local iBoozeTableA = VM_GetVar("Root/Variables/Chapter1/Vendrilstadt/iBoozeTableA", "N")
        local iBoozeTableB = VM_GetVar("Root/Variables/Chapter1/Vendrilstadt/iBoozeTableB", "N")
        local iBoozeTableC = VM_GetVar("Root/Variables/Chapter1/Vendrilstadt/iBoozeTableC", "N")
        local iBoozeTableD = VM_GetVar("Root/Variables/Chapter1/Vendrilstadt/iBoozeTableD", "N")
    
        --Pick up the booze.
        if(iTakenBooze == 0.0) then
            VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iTakenBooze", "N", 1.0)
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Olivia:[VOICE|MercF] [SOUND|World|TakeItem]Round up.[P] Give cups to each contestant by placing them on the table.[P] When they're all down, talk to me again.") ]])
            fnCutsceneBlocker()
            AL_SetProperty("Set Layer Disabled", "DrinkLower", true)
            AL_SetProperty("Set Layer Disabled", "DrinkUpper", true)
        
        --Got the booze.
        elseif(iBoozeTableA == 0.0 or iBoozeTableB == 0.0 or iBoozeTableC == 0.0 or iBoozeTableD == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Olivia:[VOICE|MercF] Just check each table to put down the drinks.[P] No peeking!") ]])
            fnCutsceneBlocker()
        
        --Put all the booze down.
        else
        
            -- |[Variables]|
            local iDrinkingPhase = VM_GetVar("Root/Variables/Chapter1/Vendrilstadt/iDrinkingPhase", "N")
            VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iDrinkingPhase", "N", iDrinkingPhase + 1.0)
            VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iTakenBooze", "N", 0.0)
            VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iBoozeTableA", "N", 0.0)
            VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iBoozeTableB", "N", 0.0)
            VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iBoozeTableC", "N", 0.0)
            VM_SetVar("Root/Variables/Chapter1/Vendrilstadt/iBoozeTableD", "N", 0.0)
            
            -- |[Call Subscript]|
            LM_ExecuteScript(fnResolvePath() .. "ContestPhase.lua")
        end
    
    -- |[Florentina]|
    elseif(sActorName == "Florentina") then
    
        if(iDrinkingPhase == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] Feeling confident, Florentina?[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Happy] Gonna school this wannabe and then we'll get some of those corn nachos Olivia makes.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Happy] Great to hear![B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] Just make sure you don't lose...[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Sad] I'm not sure what I'd do if you got taken away...[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] Stop worrying, it's a fun drinking contest.[P] Get into it!") ]])
            
        elseif(iDrinkingPhase == 1.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] How'd the first round go?[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] Beer, but I ordered a moonshine.[P] Two tokens for me.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Happy] I usually like to get a few extra tokens early on in case someone tries to play chicken with me.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] Play chicken?[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] If they think they've got a lighter drink, they trade with me, and I trade back, until one us runs out of tokens.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Happy] If I have more tokens, I can screw them pretty good.") ]])
            
        elseif(iDrinkingPhase == 2.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] Still feeling fine?[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Confused] The buzz can get to you, but if you focus you can keep your movements sharp.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Confused] Try not to knock things over, really gives you away.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Happy] You're doing great!") ]])
            fnCutsceneBlocker()
            
        elseif(iDrinkingPhase == 3.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
            fnCutscene([[ Append("Florentina:[E|Confused] I can...[P] *hic*[P] handle thish...[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Confused] She'sh going down...[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] Hang in there, Florentina...") ]])
            fnCutsceneBlocker()
            
        elseif(iDrinkingPhase == 4.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
            fnCutscene([[ Append("Florentina:[E|Confused] Come...[P] on...[P] hang in shere...[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Surprise] You're almost there, focus!") ]])
            fnCutsceneBlocker()
        end
        
    -- |[Miho]|
    elseif(sActorName == "Miho") then
    
        if(iDrinkingPhase == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Miho", "Neutral") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] Got a plan for winning, Miho?[B][C]") ]])
            fnCutscene([[ Append("Miho:[E|Neutral] Maybe I do, maybe I'm winging it.[P] Not telling you, certainly.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Happy] Oh, come on![P] Lighten up a bit![P] I'm not going to cheat![B][C]") ]])
            fnCutscene([[ Append("Miho:[E|Neutral] You are stunningly naive.[P] How have you lasted so long in this dangerous region?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] Make lots of friends, be nice to people, and also be bizarrely good with my sword?[B][C]") ]])
            fnCutscene([[ Append("Miho:[E|Neutral] Hm, lot of weird characters out here.[P] If I win, perhaps you'd want to help me take Florentina to justice?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] Heh, you're not going to win.[P] She's got this.") ]])
            fnCutsceneBlocker()
            
        elseif(iDrinkingPhase == 1.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Miho", "Drunk") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] Still feeling good?[B][C]") ]])
            fnCutscene([[ Append("Miho:[E|Drunk] If I lose, I will still have found a new favourite beer.[P] This is excellent stuff![B][C]") ]])
            fnCutscene([[ Append("Miho:[E|Drunk] All we ever drink in Trafal is snowvine wine.[P] It's almost totally tasteless.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Happy] See![P] Everyone's a winner![B][C]") ]])
            fnCutscene([[ Append("Miho:[E|Drunk] I've still got my eyes on you, waitress.[P] No tricks!") ]])
            fnCutsceneBlocker()
            
        elseif(iDrinkingPhase == 2.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Miho", "Drunk") ]])
            fnCutscene([[ Append("Miho:[E|Drunk] I don't enjoy this contest, not at all![B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] Gotta put up a tough front, right?[B][C]") ]])
            fnCutscene([[ Append("Miho:[E|Drunk] I am extremely fierce, don't undereshtimate me!") ]])
            fnCutsceneBlocker()
            
        elseif(iDrinkingPhase == 3.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Miho", "VeryDrunk") ]])
            fnCutscene([[ Append("Miho:[E|VeryDrunk] Even the moonshine is good![P] Is there shtrawberries in it?[P] I love shtrawberries![B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] I think so, but it doesn't smell like it.[B][C]") ]])
            fnCutscene([[ Append("Miho:[E|VeryDrunk] Hey, you don't think...[P] Florentina wash doing shis because she wanted to get with me?[P] She looks really nice...[B][C]") ]])
            fnCutscene([[ Append("Miho:[E|VeryDrunk] She's very fit, I like fit.[P] Mmmm...[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] (Just hold out Florentina, she's tipsy!)") ]])
            fnCutsceneBlocker()
            
        elseif(iDrinkingPhase == 4.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Miho", "VeryDrunk") ]])
            fnCutscene([[ Append("Miho:[E|VeryDrunk] You got any more in you, Florentina?[P] Jusht you and me now![P] *Hic*![B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Blush] Ask her for a date when you lose![B][C]") ]])
            fnCutscene([[ Append("Miho:[E|VeryDrunk] Did I shay that?[P] *Hic*![P] Hee hee, maybe I did.[P] More![P] More!") ]])
            fnCutsceneBlocker()
        end
    end
end
