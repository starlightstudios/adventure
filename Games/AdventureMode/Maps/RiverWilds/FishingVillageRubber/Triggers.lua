-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
--Resets all variables.
if(sObjectName == "BeginTrigger") then
    
    --Repeat check:
    local iInitializedMihoEvent = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iInitializedMihoEvent", "N")
    if(iInitializedMihoEvent == 1.0) then return end
    
    --Flags.
    VM_SetVar("Root/Variables/Chapter1/Scenes/Rubber|iInitializedMihoEvent", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter1/Scenes/Rubber|iSawMihoTrigger", "N", 0.0)
    
    --Set the overlay.
    AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 1, gcf_Night_R, gcf_Night_G, gcf_Night_B, gcf_Night_A, true)
    fnCutsceneSetFrame("Miho", "Null")

--Shows Miho to imply the objective.
elseif(sObjectName == "MihoTrigger") then

    --Repeat check:
    local iSawMihoTrigger = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iSawMihoTrigger", "N")
    if(iSawMihoTrigger == 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter1/Scenes/Rubber|iSawMihoTrigger", "N", 1.0)
    
    --Movement.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 3.0)
        CameraEvent_SetProperty("Focus Position", (65.25 * gciSizePerTile), (12.50 * gciSizePerTile))
    DL_PopActiveObject()
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Miho:[VOICE|Miho] A terrible aura pervades this place, but nothing seems amiss.[P] I must meditate.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneSetFrame("Miho", "Meditate")
    fnCutsceneBlocker()
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Miho:[VOICE|Miho] Fox Goddess, please hear my prayer.[P] How must I protect this village?[P] Who is the enemy?[P] Show me the answer...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 3.0)
        CameraEvent_SetProperty("Focus Actor Name", "Mei")
    DL_PopActiveObject()
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Narrator:[VOICE|Narrator] (If any guards or citizens see you, you'll have to retry the event.[P] Don't be seen.)") ]])
    fnCutsceneBlocker()

--Finale.
elseif(sObjectName == "Finale") then

    --Fade.
    fnCutscene([[ AL_SetProperty("Activate Fade", 0, gci_Fade_Under_GUI, true, gcf_Night_R, gcf_Night_G, gcf_Night_B, gcf_Night_A, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Miho", "Neutral") ]])
    fnCutscene([[ Append("Miho:[E|Neutral] Hm?[P] Who's there?[B][C]") ]])
    fnCutscene([[ Append("Miho:[E|Neutral] What the -[P] what are you?[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] [SOUND|Rubber|SpreadA]*squeak*[B][C]") ]])
    fnCutscene([[ Append("Miho:[E|Neutral] The hell is this stuff?[B][C]") ]])
    fnCutscene([[ Append("Miho:[E|Neutral] [SOUND|Rubber|ChangeA]I...[B][C]") ]])
    fnCutscene([[ Append("Miho:[E|Neutral] [SOUND|Rubber|ChangeB]I live to...[P] spread?[P] Yes...[P] spread...[B][C]") ]])
    fnCutscene([[ Append("Miho:[E|Neutral] [SOUND|Rubber|ChangeC]I will cover this village for you...[B][C]") ]])
    fnCutscene([[ Append("Miho:[E|Rubber] [SOUND|Rubber|SpreadA]*squeak*[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] (...[P] I spread...[P] I can...[P] think?[P] I can think...)[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] (I like to spread rubber.[P] Miho will cover this village.[P] I must show everyone...[P] how wonderful this is...)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Transition.
    fnCutscene([[ AL_BeginTransitionTo("StarfieldSwampF", "FORCEPOS:2.0x38.0x0") ]])
    fnCutsceneBlocker()
end
