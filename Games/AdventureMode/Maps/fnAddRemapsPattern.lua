-- |[ ================================== fnAddRemapsPattern() ================================== ]|
--Adds a set of remappings linearly across a set of letters. Used for simple level names in a series.
function fnAddRemapsPattern(psList, psName, psRemap, psStartLetter, psEndLetter)
	
	-- |[Argument Check]|
	if(psList == nil) then return end
	if(psName == nil) then return end
	if(psRemap == nil) then return end
	if(psStartLetter == nil) then return end
	if(psEndLetter == nil) then return end
	
    -- |[Setup]|
	local sCurrentLetter = psStartLetter
	
	--Range check. If the letter is out of the normal range, fail.
	local iByteValue = string.byte(sCurrentLetter)
	if(iByteValue < string.byte("A") or iByteValue > string.byte("Z")) then return end
	
    -- |[Iterate]|
	--Iterate across the letters. Always stops on Z. Also always stops on non-capitals.
	while(true) do
		
		--Add the remap.
		local sLevelName = psName .. sCurrentLetter
		local sRemapName = psRemap .. sCurrentLetter
		fnAddRemapping(psList, sLevelName, sRemapName)
		
		--If this was the ending letter, or the letter 'Z', stop.
		if(sCurrentLetter == psEndLetter or sCurrentLetter == "Z") then break end
		
		--Move to the next letter.
		local iByteValue = string.byte(sCurrentLetter)
		sCurrentLetter = string.char(iByteValue + 1)
	end
end