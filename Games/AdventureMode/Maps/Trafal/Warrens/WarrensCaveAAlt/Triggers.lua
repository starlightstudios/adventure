-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================== Exits ========================================= ]|
if(sObjectName == "ToWarrensA") then
    AL_BeginTransitionTo("WarrensA", "FORCEPOS:33.5x3.0x0")
    
elseif(sObjectName == "ToWestwoodsNA") then
    AL_BeginTransitionTo("WestwoodsNA", "FORCEPOS:30.0x7.0x0")
    
-- |[ ======================================= Cutscenes ======================================== ]|
elseif(sObjectName == "Cutscene") then

    -- |[ ========== Setup ========= ]|
    -- |[Variables]|
    local iEmpressJoined   = VM_GetVar("Root/Variables/Chapter2/MtSarulente/iEmpressJoined", "N")
    local iFlankedBlockade = VM_GetVar("Root/Variables/Chapter2/HarpyBlockade/iFlankedBlockade", "N")

    -- |[Blackout]|
    fnCutscene([[ AL_SetProperty("Activate Fade", 0, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    -- |[Topic]|
    WD_SetProperty("Unlock Topic", "Bunny Story", 1)
    
    -- |[Party Position]|
    fnCutsceneTeleport("Sanya", 57.25, 11.50)
    fnCutsceneTeleport("Izuna", 56.25, 12.50)
    fnCutsceneTeleport("Zeke",  58.25, 12.50)
    if(iEmpressJoined == 1.0) then
        fnCutsceneTeleport("Empress", 57.25, 12.50)
    end

    -- |[ ======= Execution ======== ]|
    -- |[Fade In]|
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneWait(165)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Angelface", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "CrowbarChanMob", "Neutral") ]])
    fnCutscene([[ Append("CC:[E|Neutral] And that's about it.[P] Sanya's an interdimensional hero who can transform and needs to save the world.[B][C]") ]])
    fnCutscene([[ Append("Angelface:[E|Neutral] CC, what?[P] We got to my secondary office and you just said 'And that's about it'.[P] You didn't explain anything.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Happy] It's true, though![P] I'm an interdimensional hero who can transform, and is going to save the world![B][C]") ]])
    fnCutscene([[ Append("CC:[E|Neutral] What if I put them the other way.[P] Sanya's an interdimens - [P][CLEAR]") ]])
    fnCutscene([[ Append("Angelface:[E|Neutral] Okay, fine, whatever.[P] I don't care.[B][C]") ]])
    fnCutscene([[ Append("Angelface:[E|Neutral] You can't join my gang.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Why not!?[B][C]") ]])
    fnCutscene([[ Append("Angelface:[E|Neutral] I don't like girls, and I don't want competition with the boys.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] Wait, you said...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Sad] Boys?[P] Yeah, you called them your boys back there, too.[B][C]") ]])
    fnCutscene([[ Append("Angelface:[E|Neutral] The bunnies aren't girls.[P] They're boys.[P] All of them.[P] I'm a minority among monstergirls.[P] I prefer men, and I love dick.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Blush] Me too![P] Izuna's just so sweet I had to walk on the other side, you know?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Blush] Heh...[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Jot] But then, how did you turn the bunnies into monsterboys?[P] How is that possible?[B][C]") ]])
    fnCutscene([[ Append("Angelface:[E|Neutral] I didn't.[P] They're technically still humans.[B][C]") ]])
    fnCutscene([[ Append("Angelface:[E|Neutral] I went all the way through the transformation, but stopped right at the end.[P] If you left them alone for a few weeks, they'd shed their fur.[B][C]") ]])
    fnCutscene([[ Append("Angelface:[E|Neutral] Stunningly useful for slipping things into places where monstergirls aren't allowed.[P] Let them turn back for a job, and then...[B][C]") ]])
    fnCutscene([[ Append("CC:[E|Neutral] The boss makes sure all her boys get lots of attention![B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Jot] So you've got a harem of cute boys![P] Awesome![B][C]") ]])
    fnCutscene([[ Append("Angelface:[E|Neutral] Heh.[P] Go right ahead and print that, lady.[P] If any cute guys want to 'try it out' for a while, I'd like to meet them.[B][C]") ]])
    fnCutscene([[ Append("Angelface:[E|Neutral] But Sanya, I don't need more girls in my gang.[P] They're mine.[P] I don't share.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] Can you turn me into a bunny anyway?[P] I'm not interested in smuggling.[B][C]") ]])
    fnCutscene([[ Append("Angelface:[E|Neutral] Normally I'd tell you off, but CC vouches for you.[B][C]") ]])
    fnCutscene([[ Append("CC:[E|Neutral] We go way back.[P] Sanya's got a heart of gold, but she won't rat to the feds.[B][C]") ]])
    fnCutscene([[ Append("Angelface:[E|Neutral] How about this.[P] Do me a favour, and I'll do you one.[B][C]") ]])
    fnCutscene([[ Append("Angelface:[E|Neutral] There's some harpies who set up a blockade up the cliff.[P] Get them to let my smugglers through, by whatever means, and I'll give you what you want.[B][C]") ]])
    if(iFlankedBlockade == 1.0) then
        if(iEmpressJoined == 1.0) then
            fnCutscene([[ Append("Empress:[E|Neutral] The harpies are seeking to cut the flow of smuggled weapons to the bandits approaching from the south.[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] I wouldn't allow smugglers through the pass, either.[B][C]") ]])
            fnCutscene([[ Append("Angelface:[E|Neutral] Weapons?[P] We don't smuggle weapons that way.[P] Down south buys drugs, medicine, food, but they don't buy weapons.[P] They make their own.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Happy] So we'll just explain that, and get the blockade lowered![B][C]") ]])
            fnCutscene([[ Append("Angelface:[E|Neutral] I don't care how you do it, just do it.[P] Come back here when it's done.[P] Use my secondary office, I don't want you around the warrens.[B][C]") ]])
            fnCutscene([[ Append("Angelface:[E|Neutral] I ain't telling the boys not to shoot on sight.[P] Get me?[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Smirk] We understand.[P] Come on, Sanya!") ]])
        else
            fnCutscene([[ Append("Sanya:[E|Neutral] I think they said they were looking for weapons.[B][C]") ]])
            fnCutscene([[ Append("Angelface:[E|Neutral] Weapons?[P] We don't smuggle weapons that way.[P] Down south buys drugs, medicine, food, but they don't buy weapons.[P] They make their own.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Happy] So we'll just explain that, and get the blockade lowered![B][C]") ]])
            fnCutscene([[ Append("Angelface:[E|Neutral] I don't care how you do it, just do it.[P] Come back here when it's done.[P] Use my secondary office, I don't want you around the warrens.[B][C]") ]])
            fnCutscene([[ Append("Angelface:[E|Neutral] I ain't telling the boys not to shoot on sight.[P] Get me?[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Smirk] We understand.[P] Come on, Sanya!") ]])
        end
    else
        if(iEmpressJoined == 1.0) then
            fnCutscene([[ Append("Empress:[E|Neutral] If the harpies are blockading you, they must have a reason.[P] Perhaps we can broker a deal.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Happy] We'll just go explain our side of things![P] This will be super easy![B][C]") ]])
            fnCutscene([[ Append("Angelface:[E|Neutral] I don't care how you do it, just do it.[P] Come back here when it's done.[P] Use my secondary office, I don't want you around the warrens.[B][C]") ]])
            fnCutscene([[ Append("Angelface:[E|Neutral] I ain't telling the boys not to shoot on sight.[P] Get me?[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Smirk] We understand.[P] Come on, Sanya!") ]])
        else
            fnCutscene([[ Append("Izuna:[E|Smirk] I'm sure we can convince the harpies.[P] We'll just explain ourselves.[P] Right, Sanya?[B][C]") ]])
            fnCutscene([[ Append("Angelface:[E|Neutral] I don't care how you do it, just do it.[P] Come back here when it's done.[P] Use my secondary office, I don't want you around the warrens.[B][C]") ]])
            fnCutscene([[ Append("Angelface:[E|Neutral] I ain't telling the boys not to shoot on sight.[P] Get me?[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Smirk] We understand.[P] Come on, Sanya!") ]])
        end
    end
    fnCutsceneBlocker()
    
    -- |[Fold]|
    fnAutoFoldParty()
    fnCutsceneBlocker()
    
    -- |[Topic]|
    WD_SetProperty("Unlock Topic", "Smugglers", 1)
end
