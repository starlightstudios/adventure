-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[Name Resolve]|
    --Resolve the name of the calling entity.
    local sActorName = TA_GetProperty("Name")

    --Set facing.
    TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[Angelface]|
    if(sActorName == "Angelface") then
        
        --Music.
        glLevelMusic = AL_GetProperty("Music")
        AL_SetProperty("Music", "Smuggling Operations")
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Angelface", "Neutral") ]])
        fnCutscene([[ Append("Angelface:[E|Neutral] Oh, good.[P] I was wondering if you were going to bore me today.[B][C]") ]])

        --Activate topics mode once the dialogue is complete.
        fnCutscene([[ WD_SetProperty("Activate Topics After Dialogue", "Angelface") ]])
    
    -- |[CrowbarChan]|
    elseif(sActorName == "CrowbarChan") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "CrowbarChanMob", "Neutral") ]])
        fnCutscene([[ Append("CC:[E|Neutral] What's up, Sanya?[B][C]") ]])

        --Activate topics mode once the dialogue is complete.
        fnCutscene([[ WD_SetProperty("Activate Topics After Dialogue", "CrowbarChan") ]])
    end
end
