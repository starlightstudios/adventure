-- |[ ============================= Pamphlet: Bandits in Westwoods ============================= ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ==== Minor Characters ==== ]|
-- |[Angelface]|
if(sActorName == "Angelface") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Angelface", "Neutral") ]])
    fnCutscene([[ Append("Angelface:[E|Neutral] Oh hey, a pamphlet about my boys.[P] Wait, what?[P] Oh, this is those dopes down south.[P] They're gonna get creamed.[P] They should stay in the plains.") ]])

-- |[CrowbarChan]|
elseif(sActorName == "CrowbarChan") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "CrowbarChanMob", "Neutral") ]])
    fnCutscene([[ Append("CC:[E|Neutral] I'm not scared of any dumb bandits, and the boys can outrun them any day!") ]])
end

-- |[Clean Up]|
DL_PopActiveObject()
