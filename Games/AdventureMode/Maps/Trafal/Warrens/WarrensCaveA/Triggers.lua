-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
if(sObjectName == "ToWarrensA") then
    AL_BeginTransitionTo("WarrensA", "FORCEPOS:33.5x3.0x0")
    
elseif(sObjectName == "ToWestwoodsNA") then
    AL_BeginTransitionTo("WestwoodsNA", "FORCEPOS:30.0x7.0x0")

elseif(sObjectName == "REVENGE") then

    -- |[Activation Check]|
    local iSawFailedBunnyTF = VM_GetVar("Root/Variables/Chapter2/BunnyWarrens/iSawFailedBunnyTF", "N")
    local iSawBunnyInCaves  = VM_GetVar("Root/Variables/Chapter2/BunnyWarrens/iSawBunnyInCaves", "N")
    if(iSawBunnyInCaves == 1.0 or iSawFailedBunnyTF == 0.0) then return end
    
    -- |[Flag]|
    VM_SetVar("Root/Variables/Chapter2/BunnyWarrens/iSawBunnyInCaves", "N", 1.0)
    
    -- |[Variables]|
    local iEmpressJoined = VM_GetVar("Root/Variables/Chapter2/MtSarulente/iEmpressJoined", "N")
    
    -- |[Movement]|
    fnCutsceneMove("Sanya", 60.25, 31.50, 2.50)
    fnCutsceneFace("Sanya", -1, 0)
    fnCutsceneMove("Izuna", 61.25, 31.50, 2.50)
    fnCutsceneFace("Izuna", -1, 0)
    fnCutsceneMove("Zeke", 61.25, 32.50, 2.50)
    fnCutsceneFace("Zeke", -1, 0)
    if(iEmpressJoined == 1.0) then
        fnCutsceneMove("Empress", 60.25, 32.50, 2.50)
        fnCutsceneFace("Empress", -1, 0)
    end
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneCamera("Actor Name", gcfCamera_Cutscene_Speed, "Smuggler")
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Enforcer:[VOICE|ManA] So I says to Mabel I says, 'The red specter of vengeance perches upon your shoulder!'[B][C]") ]])
    fnCutscene([[ Append("Enforcer:[VOICE|ManA] 'We need to run![P] Run for your lives!'[B][C]") ]])
    fnCutscene([[ Append("Smuggler:[VOICE|Bunny] Really?[P] Why'd you say that?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneMove("Enforcer", 54.25, 24.50, 2.50)
    fnCutsceneBlocker()
    fnCutsceneFace("Smuggler", -1, 0)
    fnCutsceneMove("Enforcer", 39.25, 24.50, 2.50)
    fnCutsceneTeleport("Enforcer", -1.25, -1.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Smuggler:[VOICE|Bunny] The heck was that all about?[P] Specter of vengeance?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    -- |[Movement]|
    fnCutsceneMove("Sanya", 56.25, 31.50, 2.50)
    fnCutsceneMove("Sanya", 56.25, 26.50, 2.50)
    fnCutsceneBlocker()
    fnCutscenePlaySound("World|Thump")
    fnCutsceneMoveFace("Smuggler", 56.25, 20.20, 0, 1, 2.50)
    fnCutsceneMove("Sanya",        56.25, 21.20, 2.50)
    fnCutsceneBlocker()
    fnCutscenePlaySound("World|Thump")
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Offended") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Bunny", "Neutral") ]])
    fnCutscene([[ Append("Sanya:[E|Offended] She's talking about me, punk![B][C]") ]])
    fnCutscene([[ Append("Bunny:[E|Neutral] You [P]*cough*[P] again!?[P] What's your problem!?[P] Are you a narc!?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Offended] Don't call me a narc or I'll break your legs![P] You need to bun me up, right now![B][C]") ]])
    fnCutscene([[ Append("Bunny:[E|Neutral] Can't [P]*cough*[P] do that![P] Only the boss can![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Happy] Now we're getting somewhere![P] Point me to your boss' ass so I can kick that![B][C]") ]])
    fnCutscene([[ Append("Bunny:[E|Neutral] Let me go![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] Certainly![P] Run along now!") ]])
    fnCutsceneBlocker()
    fnCutsceneCamera("Actor Name", gcfCamera_Cutscene_Speed, "Sanya")
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneFace("Sanya", -1, 0)
    fnCutsceneMove("Smuggler", 52.25, 20.50, 2.50)
    fnCutsceneMove("Smuggler", 52.25, 23.50, 2.50)
    fnCutsceneMove("Smuggler", 39.25, 23.50, 2.50)
    fnCutsceneTeleport("Smuggler", -1.25, -1.50)
    fnCutsceneBlocker()
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    fnCutsceneMove("Sanya", 56.25, 20.50)
    fnCutsceneFace("Sanya", 0, 1)
    fnCutsceneMove("Izuna", 56.25, 31.50)
    fnCutsceneMove("Izuna", 56.25, 21.50)
    fnCutsceneFace("Izuna", 0, -1)
    fnCutsceneMove("Zeke", 56.25, 32.50)
    fnCutsceneMove("Zeke", 56.25, 22.50)
    fnCutsceneFace("Zeke", 0, -1)
    if(iEmpressJoined == 1.0) then
        fnCutsceneMove("Empress", 57.25, 32.50)
        fnCutsceneMove("Empress", 57.25, 20.50)
        fnCutsceneFace("Empress", -1, 0)
    end
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    if(iEmpressJoined == 0.0) then
        fnCutscene([[ Append("Sanya:[E|Laugh] That's right, lead me right to your boss![B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Happy] And now the smugglers will know we're coming, and be ready for us![B][C]") ]])
        fnCutscene([[ Append("Zeke:[E|Neutral] Bah![B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] Wait why are you so happy about that?[P] That sounds bad.[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Neutral] I didn't want to bring down the mood...[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Neutral] Besides, I'm sure we can handle them.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] Great, let's go get bunned up![P] Follow me!") ]])
    else
        fnCutscene([[ Append("Empress:[E|Neutral] And now we follow the pup back to its mother.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Smirk] Exactly what I was thinking![B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Happy] And now the smugglers will know we're coming, and be ready for us![B][C]") ]])
        fnCutscene([[ Append("Zeke:[E|Neutral] Bah![B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] Wait why are you so happy about that?[P] That sounds bad.[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Neutral] I didn't want to bring down the mood...[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Neutral] Besides, I'm sure we can handle them.[B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] They are not soldiers, just very greedy.[P] They will put up a defense, but will flee if their lives are in danger.[B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] Still, they do appear to have flintlock firearms.[P] They pay their security well.[P] No doubt their headquarters is nearby.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] Great, let's go get bunned up!") ]])
    end
    fnCutsceneBlocker()
    
    -- |[Finish up]|
    fnAutoFoldParty()
    fnCutsceneBlocker()
end
