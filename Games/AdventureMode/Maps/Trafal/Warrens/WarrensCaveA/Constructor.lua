-- |[ ======================================= Constructor ====================================== ]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = fnResolveDirectory() --Map's name is the name of the folder it is in.
local sLevelMusic = "Choking Clouds"
local sMapResolveName = "Map Resolve Name"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[ ======================================== Standard ======================================== ]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

    -- |[Construction]|
	--Music.
	AL_SetProperty("Music", sLevelMusic)
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
    
    -- |[Shooting Setup]|
    AL_SetProperty("Shooting Depth", 1)
    
    -- |[Map Handling]|
    --Resolve the map's variable.
    local sMapVariable = fnResolveMapVarFromName(sLevelName)
    if(sMapVariable ~= "Null") then
        
        --Flag the map as uncovered.
        VM_SetVar(sMapVariable, "N", 1.0)
        
        --Run subroutine to build map properties.
        fnHandleWestwoodsMap(sLevelName)

    end

-- |[ ======================================== Post-Exec ======================================= ]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	-- |[NPC Spawning]|
	--If an entity needs to spawn after a post-exec cutscene, put them here.
    local iSawFailedBunnyTF = VM_GetVar("Root/Variables/Chapter2/BunnyWarrens/iSawFailedBunnyTF", "N")
    local iSawBunnyInCaves = VM_GetVar("Root/Variables/Chapter2/BunnyWarrens/iSawBunnyInCaves", "N")
    if(iSawBunnyInCaves == 0.0 and iSawFailedBunnyTF == 1.0) then
        TA_Create("Smuggler")
            TA_SetProperty("Position", 56, 25)
            TA_SetProperty("Facing", gci_Face_North)
            TA_SetProperty("Clipping Flag", false)
            fnSetCharacterGraphics("Root/Images/Sprites/Bunny/", false)
        DL_PopActiveObject()
        TA_Create("Enforcer")
            TA_SetProperty("Position", 56, 24)
            TA_SetProperty("Facing", gci_Face_South)
            TA_SetProperty("Clipping Flag", false)
            fnSetCharacterGraphics("Root/Images/Sprites/Enforcer/", false)
        DL_PopActiveObject()
    end

    -- |[Overlays]|
    --Place overlay code here.
    
    -- |[Skillbooks]|
    --Place skillbook overlay code here.
    
    -- |[Rifle Handling]|
    VM_SetVar("Root/Variables/Global/Sanya/iRifleCode", "N", gciSanyaRifle_CanFire)
    
    -- |[Mark Collision Depth 1 As Deep Water]|
    AL_SetProperty("Register Deep Layer", 2)
    AL_SetProperty("Deep Layer Blocks Run", 2, true)
    AL_SetProperty("Deep Layer Pixels Sink", 2, 12)
    AL_SetProperty("Deep Layer Overlay Name", 2, "WaterOver")
    AL_SetProperty("Deep Layer Walk Sound", 2, "World|Wading")
    AL_SetProperty("Deep Layer Run Sound", 2, "World|Wading")

end
