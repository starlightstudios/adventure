-- |[ ============================= Pamphlet: Bandits in Westwoods ============================= ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[Alraune]|
if(sActorName == "Alraune") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Alraune:[VOICE|Alraune] I'd serve bandits too if they got in here.[P] I get the feeling the bandits won't be interested in paying though.[P] I got a gun under the bar but it might be time to see if I can bribe some of the bunnies into digging me an escape tunnel.[P] Free drinks is a powerful motivator.") ]])

-- |[DrunkBun]|
elseif(sActorName == "DrunkBun") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Enforcer:[VOICE|ManA] Shilly bandits, they'll never get my lucky glasses.") ]])
end

-- |[Clean Up]|
DL_PopActiveObject()
