-- |[ =============================== Pamphlet: Green Westwoods =============================== ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[Alraune]|
if(sActorName == "Alraune") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Alraune:[VOICE|Alraune] So my leaf-sisters are healing the land over in the cauldron?[P] Maybe,[P] maybe,[P] this is it, this is the sign I've been waiting for.[P] It's time to return to my roots and help the little ones.[P][P] Nah I'm just pulling your leg, I like running a bar.") ]])

-- |[DrunkBun]|
elseif(sActorName == "DrunkBun") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Enforcer:[VOICE|ManA] *snore*[P] what were we talkish about?") ]])
end

-- |[Clean Up]|
DL_PopActiveObject()
