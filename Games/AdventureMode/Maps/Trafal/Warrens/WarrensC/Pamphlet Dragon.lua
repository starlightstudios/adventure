-- |[ ==================================== Pamphlet: Dragon ==================================== ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[Alraune]|
if(sActorName == "Alraune") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Alraune:[VOICE|Alraune] Uh huh, right as I get a good thing going an ancient hero shows up?[P] Lady take your religious pamphlets elsewhere, I'm not donating.") ]])

-- |[DrunkBun]|
elseif(sActorName == "DrunkBun") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Enforcer:[VOICE|ManA] Ish the hero![P] Cheers to you hero![P] Your horns are very *hic* heroic.") ]])
end

-- |[Clean Up]|
DL_PopActiveObject()
