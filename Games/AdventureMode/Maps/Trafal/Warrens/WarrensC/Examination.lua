-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= Exits ========================================== ]|
if(sObjectName == "ToWarrensD") then
    
    --Hasn't seen the cutscene yet:
    local iSawAngelfaceScene = VM_GetVar("Root/Variables/Chapter2/BunnyWarrens/iSawAngelfaceScene", "N")
    if(iSawAngelfaceScene == 0.0) then
        AudioManager_PlaySound("World|FlipSwitch")
        AL_BeginTransitionTo("WarrensD", "FORCEPOS:1.0x1.0x0")
    
    --Already seen it.
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Better not go back in there.[P] I don't want to risk walking in on a bunny orgy.)") ]])
        fnCutsceneBlocker()
    end
    
-- |[ ====================================== Examinables ======================================= ]|
elseif(sObjectName == "Booze") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (This bunny has positively housed these beers.[P] I respect the hell out of that.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "LiquorBarrel") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (What kind of booze do you make from carrots?[P] Whatever, it smells like schnapps.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Bag") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A bunch of black powder and some musket balls.[P] Someone's doing target practice.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Cooler") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Some sort of box enchanted to be cool to the touch.[P] It's full of sandwiches.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Box") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A bunch of clothes.[P] Suspenders are really in this season.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "LootNote") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Sticky fingers get chopped off' -Angelface)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Barrels") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Various kinds of booze.[P] Smells expensive.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Rugs") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Rugs.[P] I'm not an expert on rugs so I'm going to assume they aren't cheap.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "SmugglingRecords") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Records of who smuggled what from where to where, and how much to pay them.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Gems") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Bags full of polished glass that look like gems.[P] Is there no low these crooks won't stoop to?)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "GoldBag") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A bag made out of gold, or as we call it in the pyrite community, \"fool's pyrite\".)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Eats") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Expensive looking foods.[P] Caviar, steak, eggs, and spices.[P] It must be enchanted to stay fresh somehow.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Note") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('If you hear springs, wait your turn' -Angelface)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "MoneyTable") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A counting table, with a machine for sorting paper bills, a counter for coins, and ribbons for tying things up.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Safe") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The kind of safe even I can't punch my way into.)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "Moneybags") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Paper money from a dozen different denominations, plus many universal platina coins that are used everywhere.[P] I'd borrow some but where the hell would I even spend half this stuff?)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "Desk") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The note on the desk is a schedule of who is assigned to counting duty.)") ]])
    fnCutsceneBlocker()

-- |[ ========================================== Other ========================================= ]|
-- |[ ========================================== Debug ========================================= ]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
