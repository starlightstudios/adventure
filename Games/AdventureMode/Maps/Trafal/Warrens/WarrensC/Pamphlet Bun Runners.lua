-- |[ ============================= Pamphlet: The Bunny Smugglers ============================== ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[Alraune]|
if(sActorName == "Alraune") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Alraune:[VOICE|Alraune] I gathered that was what she was doing based on what the boys were saying.[P] But if she's willing to advertise like this maybe she'll let me watch the next time she's reupping one of the boys.[P] Be fun to see at least.") ]])

-- |[DrunkBun]|
elseif(sActorName == "DrunkBun") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Enforcer:[VOICE|ManA] Aww that Angelface.[P] You think'll she'll only hurt me if I tell her I love her?") ]])
end

-- |[Clean Up]|
DL_PopActiveObject()
