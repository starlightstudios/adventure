-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
if(sObjectName == "Cutscene") then
    
    -- |[ ======= Functions ======== ]|
    --Sets all frames not equal to the requested one to hidden.
    local function fnSetBunnyFrame(psBunnyPrefix, psFrame)
        
        --Shown frame:
        local sShownFrameName = "Bunny" .. psBunnyPrefix .. psFrame
        fnCutsceneLayerDisabled(sShownFrameName, false)
        
        --Hide other:
        if(sShownFrameName ~= "Bunny" .. psBunnyPrefix .. "A") then fnCutsceneLayerDisabled("Bunny" .. psBunnyPrefix .. "A", true) end
        if(sShownFrameName ~= "Bunny" .. psBunnyPrefix .. "B") then fnCutsceneLayerDisabled("Bunny" .. psBunnyPrefix .. "B", true) end
        if(sShownFrameName ~= "Bunny" .. psBunnyPrefix .. "C") then fnCutsceneLayerDisabled("Bunny" .. psBunnyPrefix .. "C", true) end
        if(sShownFrameName ~= "Bunny" .. psBunnyPrefix .. "D") then fnCutsceneLayerDisabled("Bunny" .. psBunnyPrefix .. "D", true) end
        
    end
    
    -- |[ ========== Setup ========= ]|
    -- |[Variables]|
    local iEmpressJoined = VM_GetVar("Root/Variables/Chapter2/MtSarulente/iEmpressJoined", "N")
    
    -- |[Flag]|
    VM_SetVar("Root/Variables/Chapter2/BunnyWarrens/iSawAngelfaceScene", "N", 1.0)

    -- |[Blackout]|
    fnCutscene([[ AL_SetProperty("Activate Fade", 0, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()

    -- |[Character Creation]|
    --Angelface.
    TA_Create("Angelface")
    
        --Basics.
        TA_SetProperty("Position", 10, 4)
        TA_SetProperty("Facing", gci_Face_South)
        TA_SetProperty("Clipping Flag", false)
        fnSetCharacterGraphics("Root/Images/Sprites/Angelface/", false)
        
		--Special frames.
        TA_SetProperty("Wipe Special Frames")
        TA_SetProperty("Add Special Frame", "Draw0", "Root/Images/Sprites/Special/Angelface|Draw0")
        TA_SetProperty("Add Special Frame", "Draw1", "Root/Images/Sprites/Special/Angelface|Draw1")
        TA_SetProperty("Add Special Frame", "Draw2", "Root/Images/Sprites/Special/Angelface|Draw2")
    DL_PopActiveObject()
    
    --Smuggler.
    TA_Create("Smuggler")
        TA_SetProperty("Position", 10, 8)
        TA_SetProperty("Facing", gci_Face_North)
        TA_SetProperty("Clipping Flag", false)
        fnSetCharacterGraphics("Root/Images/Sprites/Bunny/", false)
    DL_PopActiveObject()

    --CrowbarChan!
    TA_Create("CrowbarChan")
    
        --Basics.
        TA_SetProperty("Position", 13, 1)
        TA_SetProperty("Facing", gci_Face_South)
        TA_SetProperty("Clipping Flag", false)
        TA_SetProperty("Auto Animates", true)
        
        --Frames.
		for i = 1, 8, 1 do
			TA_SetProperty("Move Frame", i-1, 0, "Root/Images/Sprites/CrowbarChanMobster/0")
			TA_SetProperty("Move Frame", i-1, 1, "Root/Images/Sprites/CrowbarChanMobster/1")
			TA_SetProperty("Move Frame", i-1, 2, "Root/Images/Sprites/CrowbarChanMobster/2")
			TA_SetProperty("Move Frame", i-1, 3, "Root/Images/Sprites/CrowbarChanMobster/1")
		end
    DL_PopActiveObject()

    -- |[Layer Setup]|
    AL_SetProperty("Set Layer Disabled", "BunnyAB", true)
    AL_SetProperty("Set Layer Disabled", "BunnyAC", true)
    AL_SetProperty("Set Layer Disabled", "BunnyAD", true)
    AL_SetProperty("Set Layer Disabled", "BunnyBB", true)
    AL_SetProperty("Set Layer Disabled", "BunnyBC", true)
    AL_SetProperty("Set Layer Disabled", "BunnyBD", true)
    AL_SetProperty("Set Layer Disabled", "BunnyCB", true)
    AL_SetProperty("Set Layer Disabled", "BunnyCC", true)
    AL_SetProperty("Set Layer Disabled", "BunnyCD", true)
    AL_SetProperty("Set Layer Disabled", "BunnyDB", true)
    AL_SetProperty("Set Layer Disabled", "BunnyDC", true)
    AL_SetProperty("Set Layer Disabled", "BunnyDD", true)
    AL_SetProperty("Set Layer Disabled", "BunnyEB", true)
    AL_SetProperty("Set Layer Disabled", "BunnyEC", true)
    AL_SetProperty("Set Layer Disabled", "BunnyED", true)
    AL_SetProperty("Set Layer Disabled", "BunnyFB", true)
    AL_SetProperty("Set Layer Disabled", "BunnyFC", true)
    AL_SetProperty("Set Layer Disabled", "BunnyFD", true)
    
    -- |[Party Position]|
    fnCutsceneTeleport("Sanya", 10.25,  9.50)
    fnCutsceneTeleport("Izuna",  9.25, 10.50)
    fnCutsceneTeleport("Zeke",  11.25, 10.50)
    if(iEmpressJoined == 1.0) then
        fnCutsceneTeleport("Empress", 10.25, 10.50)
    end

    -- |[ ======= Execution ======== ]|
    -- |[Fade In]|
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Bunny", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Angelface", "Neutral") ]])
    fnCutscene([[ Append("Bunny:[E|Neutral] ...[P] And she nearly broke my collarbone![P] She's a psycho and she's coming this way, boss![B][C]") ]])
    fnCutscene([[ Append("Angelface:[E|Neutral] Interesting.[P] Tell me.[P] Is she ugly?[B][C]") ]])
    fnCutscene([[ Append("Bunny:[E|Neutral] Ugly?[P] Uh, not really.[P] Kinda plain.[P] Got a lean sort of muscle build, you know?[B][C]") ]])
    fnCutscene([[ Append("Bunny:[E|Neutral] Why?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneFace("Smuggler", 1, 0)
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    fnCutsceneFace("Smuggler", 0, 1)
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    fnCutsceneMove("Smuggler", 5.25, 8.50, 3.00)
    fnCutsceneMove("Smuggler", 5.25, 9.50, 3.00)
    fnCutsceneMove("Smuggler", 0.25, 9.50, 3.00)
    fnCutscenePlaySound("World|Flee")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Smuggler:[VOICE|Bunny] AIEEEEEE!!!!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    
    --Angelface moves.
    fnCutsceneCamera("Position", 0.5, 10.25, 6.50)
    fnCutsceneMove("Angelface", 13.25, 4.50)
    fnCutsceneMove("Angelface", 13.25, 6.50)
    fnCutsceneMove("Angelface", 10.25, 6.50)
    fnCutsceneFace("Angelface", 0, 1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Angelface", "Neutral") ]])
    fnCutscene([[ Append("Angelface:[E|Neutral] So you're the galoot roughing up my bunnies.[P] Name's Angelface, toutse.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Happy] Sanya![B][C]") ]])
    fnCutscene([[ Append("Angelface:[E|Neutral] Now, I need to put this with some degree of delicacy.[P] You see, I'm running an operation, and we don't like it when people beat up our workers.[B][C]") ]])
    fnCutscene([[ Append("Angelface:[E|Neutral] But at the same time, roughing up people is distasteful.[P] Classy types like us, we don't do that.[P] Right?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Speak for yourself.[B][C]") ]])
    fnCutscene([[ Append("Angelface:[E|Neutral] Fine.[P] So I'll ask you.[P] What the hell do you want?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Offended] Turn me into a bunny![B][C]") ]])
    fnCutscene([[ Append("Angelface:[E|Neutral] The question was rhetor -[P] what?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Offended] Bunny![P] Me![B][C]") ]])
    fnCutscene([[ Append("Angelface:[E|Neutral] We ain't hiring.[P] Especially not...[P] dames like you.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Angry] Give me your bunny powers, damn it![B][C]") ]])
    fnCutscene([[ Append("Angelface:[E|Neutral] Oh, there's one thing I'll give you.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    local iTPF = 3
    local iPause = 15
    
    --Angelface.
    fnCutsceneSetFrame("Angelface", "Draw0")
    fnCutsceneWait(iTPF)
    fnCutsceneBlocker()
    fnCutsceneSetFrame("Angelface", "Draw1")
    fnCutsceneWait(iTPF)
    fnCutsceneBlocker()
    fnCutscenePlaySound("Firearm|ReadyGun")
    fnCutsceneSetFrame("Angelface", "Draw2")
    fnCutsceneWait(iTPF)
    fnCutsceneBlocker()
    fnCutsceneWait(60)
    fnCutsceneBlocker()

    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Angelface:[VOICE|Angelface] I'm going to give you to the count of ten...[B][C]") ]])
    fnCutscene([[ Append("Angelface:[VOICE|Angelface] To get your ugly,[P][P][P] rotten,[P][P][P] no good keister the hell off my property...[B][C]") ]])
    fnCutscene([[ Append("Angelface:[VOICE|Angelface] Before we pump your guts full of lead![P] Boys!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    --Bunny A.
    fnSetBunnyFrame("A", "B")
    fnCutsceneWait(iTPF)
    fnCutsceneBlocker()
    fnCutscenePlaySound("Firearm|ReadyGun")
    fnSetBunnyFrame("A", "C")
    fnCutsceneWait(iTPF)
    fnCutsceneBlocker()
    fnSetBunnyFrame("A", "D")
    fnCutsceneWait(iTPF)
    fnCutsceneBlocker()
    fnCutsceneWait(iPause)
    fnCutsceneBlocker()
    
    --Bunny B and D.
    fnSetBunnyFrame("B", "B")
    fnCutsceneWait(iTPF)
    fnCutsceneBlocker()
    fnCutscenePlaySound("Firearm|ReadyGun")
    fnSetBunnyFrame("D", "B")
    fnSetBunnyFrame("B", "C")
    fnCutsceneWait(iTPF)
    fnCutsceneBlocker()
    fnCutscenePlaySound("Firearm|ReadyGun")
    fnSetBunnyFrame("D", "C")
    fnSetBunnyFrame("B", "D")
    fnCutsceneWait(iTPF)
    fnCutsceneBlocker()
    fnSetBunnyFrame("D", "D")
    fnCutsceneWait(iTPF)
    fnCutsceneBlocker()
    
    --Bunny E.
    fnSetBunnyFrame("E", "B")
    fnCutsceneWait(iTPF)
    fnCutsceneBlocker()
    fnCutscenePlaySound("Firearm|ReadyGun")
    fnSetBunnyFrame("E", "C")
    fnCutsceneWait(iTPF)
    fnCutsceneBlocker()
    fnSetBunnyFrame("E", "D")
    fnCutsceneWait(iTPF)
    fnCutsceneBlocker()
    fnCutsceneWait(iPause)
    fnCutsceneBlocker()
    
    --Bunny C and F.
    fnSetBunnyFrame("C", "B")
    fnCutsceneWait(iTPF)
    fnCutsceneBlocker()
    fnCutscenePlaySound("Firearm|ReadyGun")
    fnSetBunnyFrame("F", "B")
    fnSetBunnyFrame("C", "C")
    fnCutsceneWait(iTPF)
    fnCutsceneBlocker()
    fnCutscenePlaySound("Firearm|ReadyGun")
    fnSetBunnyFrame("F", "C")
    fnSetBunnyFrame("C", "D")
    fnCutsceneWait(iTPF)
    fnCutsceneBlocker()
    fnSetBunnyFrame("F", "D")
    fnCutsceneWait(iTPF)
    fnCutsceneBlocker()
    fnCutsceneWait(125)
    fnCutsceneBlocker()

    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Angelface:[VOICE|Angelface] ONE.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Okay, we're going![B][C]") ]])
    fnCutscene([[ Append("Angelface:[VOICE|Angelface] TWO!![B][C]") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Run![B][C]") ]])
    fnCutscene([[ Append("Angelface:[VOICE|Angelface] TEN!!!![B][C]") ]])
    fnCutscene([[ Append("Voice:[VOICE|CrowbarChanMob] WAAAIIIIITT!!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneMove("CrowbarChan", 13.25, 7.50, 2.50)
    fnCutsceneMove("CrowbarChan", 10.25, 7.50, 2.50)
    fnCutsceneFace("CrowbarChan", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "CrowbarChanMob", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sanya", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Angelface", "Neutral") ]])
    fnCutscene([[ Append("CC:[E|Neutral] Hold your fire![P] Hold your fire![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Surprised] Crowbar-Chan!?[B][C]") ]])
    fnCutscene([[ Append("Angelface:[E|Neutral] What the hell are you doing, CC!?[B][C]") ]])
    fnCutscene([[ Append("CC:[E|Neutral] She's with me, boss![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Surprised] Crowbar-Chan, you -[P] you're with the smugglers?[B][C]") ]])
    fnCutscene([[ Append("CC:[E|Neutral] Fancy that, you come stumbling into my life all over again.[P] You never change, do you, Sanya?[B][C]") ]])
    fnCutscene([[ Append("Angelface:[E|Neutral] God damn it.[P] Boys![P] Guns down until I figure out what's going on.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    --Angelface.
    fnCutsceneSetFrame("Angelface", "Draw1")
    fnCutsceneWait(iTPF)
    fnCutsceneBlocker()
    fnCutsceneSetFrame("Angelface", "Draw0")
    fnCutsceneWait(iTPF)
    fnCutsceneBlocker()
    fnCutscenePlaySound("Firearm|LowerGun")
    fnCutsceneSetFrame("Angelface", "Null")
    fnCutsceneWait(iTPF)
    fnCutsceneBlocker()

    local iWaitLower = 15
    fnCutscenePlaySound("Firearm|LowerGun")
    fnSetBunnyFrame("A", "C")
    fnCutsceneWait(iWaitLower)
    fnCutsceneBlocker()
    fnCutscenePlaySound("Firearm|LowerGun")
    fnSetBunnyFrame("B", "C")
    fnSetBunnyFrame("D", "C")
    fnSetBunnyFrame("E", "C")
    fnCutsceneWait(iWaitLower)
    fnCutsceneBlocker()
    fnCutscenePlaySound("Firearm|LowerGun")
    fnSetBunnyFrame("C", "C")
    fnSetBunnyFrame("F", "C")
    fnCutsceneWait(iWaitLower)
    fnCutsceneBlocker()
    fnCutsceneBlocker()
    fnCutsceneWait(125)
    fnCutsceneBlocker()

    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "CrowbarChanMob", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Sanya", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Angelface", "Neutral") ]])
    fnCutscene([[ Append("CC:[E|Neutral] Boss, I can explain everything.[P] Honest![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Blush] Wow, you bailed me out yet again, Crowbar-chan.[B][C]") ]])
    fnCutscene([[ Append("CC:[E|Neutral] I go by CC these days, pal.[P] I guess you owe me another one.[B][C]") ]])
    fnCutscene([[ Append("Angelface:[E|Neutral] Look, whatever stupid crap you're about to prattle about, we're not doing it here.[P] Boys, we're going to get some air.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    
    -- |[Transition]|
    fnCutscene([[ AL_BeginTransitionTo("WarrensCaveAAlt", "FORCEPOS:1.0x1.0x0") ]])
    fnCutsceneBlocker()
    
end
