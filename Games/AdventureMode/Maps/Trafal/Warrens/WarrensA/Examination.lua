-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= Exits ========================================== ]|
if(sObjectName == "ToWarrensB") then
    
    --Variables.
    local iOpenedWarrens = VM_GetVar("Root/Variables/Chapter2/BunnyWarrens/iOpenedWarrens", "N")
    
    --Only works if opened.
    if(iOpenedWarrens == 1.0) then
        AudioManager_PlaySound("World|FlipSwitch")
        AL_BeginTransitionTo("WarrensB", "FORCEPOS:10.0x14.0x0")
    end
    
-- |[ ====================================== Examinables ======================================= ]|
elseif(sObjectName == "SecretSwitch") then

    --Variables.
    local iOpenedWarrens = VM_GetVar("Root/Variables/Chapter2/BunnyWarrens/iOpenedWarrens", "N")
    
    --Flags.
    VM_SetVar("Root/Variables/Chapter2/BunnyWarrens/iSawFailedBunnyTF", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter2/BunnyWarrens/iSawBunnyInCaves", "N", 1.0)
    
    --Not opened yet:
    if(iOpenedWarrens == 0.0) then
        VM_SetVar("Root/Variables/Chapter2/BunnyWarrens/iOpenedWarrens", "N", 1.0)
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Hey, there's a switch at the bottom of the bag!)") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutscenePlaySound("World|FlipSwitch")
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneCamera("Position", gcfCamera_Cutscene_Speed, 23.25, 21.50)
        fnCutsceneWait(45)
        fnCutsceneBlocker()
        fnCutscenePlaySound("World|DistantDoorOpen")
        fnCutsceneLayerDisabled("DoorLo", false)
        fnCutsceneLayerDisabled("DoorHi", false)
        fnCutsceneWait(75)
        fnCutsceneBlocker()
        fnCutsceneCamera("Actor Name", gcfCamera_Cutscene_Speed, "Sanya")
        fnCutsceneWait(45)
        fnCutsceneBlocker()

    --Already opened.
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (The bag is stuck to the ground and just covers the button...)") ]])
        fnCutsceneBlocker()
    end

-- |[ ========================================== Other ========================================= ]|
-- |[ ========================================== Debug ========================================= ]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
