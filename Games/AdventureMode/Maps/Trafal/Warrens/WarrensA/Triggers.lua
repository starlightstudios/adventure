-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
if(sObjectName == "ToWarrensCaveA") then
    local iSawAngelfaceScene = VM_GetVar("Root/Variables/Chapter2/BunnyWarrens/iSawAngelfaceScene", "N")
    if(iSawAngelfaceScene == 0.0) then
        AL_BeginTransitionTo("WarrensCaveA", "FORCEPOS:19.5x34.0x0")
    else
        AL_BeginTransitionTo("WarrensCaveAAlt", "FORCEPOS:19.5x34.0x0")
    end
end
