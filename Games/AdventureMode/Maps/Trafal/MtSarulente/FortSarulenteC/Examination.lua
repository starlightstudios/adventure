-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= Exits ========================================== ]|
if(sObjectName == "SampleExit") then
    
-- |[ ====================================== Examinables ======================================= ]|
--Checking if the switch responds to being shot.
elseif(sObjectName == "BridgeSwitch SHOT CHECK") then
    AL_SetProperty("Examinable Reply To Hit")

--If shot, this script executes when Sanya has finished her animations.
elseif(sObjectName == "BridgeSwitch SHOT HANDLER") then
    local iActivatedFortSwitch = VM_GetVar("Root/Variables/Chapter2/MtSarulente/iActivatedFortSwitch", "N")
    if(iActivatedFortSwitch == 1.0) then return end
    
    --Set.
    VM_SetVar("Root/Variables/Chapter2/MtSarulente/iActivatedFortSwitch", "N", 1.0)
    
    --SFX.
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|DistantDoorOpen") ]])
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|DistantDoorOpen") ]])
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|DistantDoorOpen") ]])
    fnCutsceneWait(35)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Sounds like that did something outside.)") ]])
    fnCutsceneBlocker()
    
-- |[ ========================================== Other ========================================= ]|
-- |[ ========================================== Debug ========================================= ]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
