-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= Exits ========================================== ]|
if(sObjectName == "ToSarulenteLibraryA") then
    
    -- |[Variables]|
    local iEmpressOpenedLibrary = VM_GetVar("Root/Variables/Chapter2/MtSarulente/iEmpressOpenedLibrary", "N")
    local iEmpressJoined        = VM_GetVar("Root/Variables/Chapter2/MtSarulente/iEmpressJoined", "N")
    
    -- |[Done]|
    if(iEmpressOpenedLibrary == 1.0) then
        AL_BeginTransitionTo("SarulenteLibraryA", "FORCEPOS:29.5x29.0x0")
        AudioManager_PlaySound("World|FlipSwitch")
        
    -- |[Open It]|
    elseif(iEmpressJoined == 1.0) then
        VM_SetVar("Root/Variables/Chapter2/MtSarulente/iEmpressOpenedLibrary", "N", 1.0)
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        fnCutscene([[ Append("Sanya:[E|Neutral] It's locked.[P] Anyone good at lock picking?[B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] Allow me.[P] I have the key.[P] This is one of our old book archives.[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Happy] Really?[P] What wealth of knowledge must lie within!") ]])
        fnCutsceneBlocker()
        fnCutscene([[ AL_BeginTransitionTo("SarulenteLibraryA", "FORCEPOS:29.5x29.0x0") ]])
        fnCutscene([[ AudioManager_PlaySound("World|FlipSwitch") ]])
    
    -- |[Won't Open]|
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        fnCutscene([[ Append("Sanya:[E|Neutral] It's locked.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Offended] Defy me at your peril, door![P] Prepare for your end![B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Ugh] Sanya, please don't smash the door.[P] Smashing things inside a cave is a good way to cause a cave-in.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] Damn it.[P] Who would have a key?[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Neutral] No idea.[P] This might be someone's house for all we know.[P] Let's come back later.") ]])
    end
    
-- |[ ====================================== Examinables ======================================= ]|
-- |[ ========================================== Other ========================================= ]|
-- |[ ========================================== Debug ========================================= ]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
