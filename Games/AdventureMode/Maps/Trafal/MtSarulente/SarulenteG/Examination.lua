-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= Exits ========================================== ]|
-- |[ ====================================== Examinables ======================================= ]|
if(sObjectName == "NopeSign") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (This ladder will work in a later version.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Memoriam") then
    
    --Variables.
    local iEmpressJoined = VM_GetVar("Root/Variables/Chapter2/MtSarulente/iEmpressJoined", "N")
    
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("Izuna:[E|Neutral] I can't read draconic without a translation guide.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] I think it's a poem.[P] 'All creatures who walk, wiggle, soar, or squirm;[P] We hold ourselves above ye, and are fools;[P] Older than laws is flame;[P] Older than words is death.'[B][C]") ]])
    if(iEmpressJoined == 0.0) then
        fnCutscene([[ Append("Izuna:[E|Neutral] Hm, it's a lamentation.[P] This must be the site of the empress' tomb.[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Neutral] Let's look around, the entrance won't be subtle.") ]])
    else
        fnCutscene([[ Append("Izuna:[E|Neutral] A lamentation.[B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] This commemorates the site of my hometown.[P] Six hundred people, dead in a day.[P] Everything comes back to nothing, where it began.[B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] If you're curious, yes, it did rhyme in Draconic tongue.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] Powerful.") ]])
    end
    fnCutsceneBlocker()
    
-- |[ ========================================== Other ========================================= ]|
-- |[ ========================================== Debug ========================================= ]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
