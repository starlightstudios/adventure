-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
if(sObjectName == "Trigger") then

    -- |[Repeat Check]|
    local iIzunaSawLibrary = VM_GetVar("Root/Variables/Chapter2/MtSarulente/iIzunaSawLibrary", "N")
    if(iIzunaSawLibrary == 1.0) then return end
    
    -- |[Flag]|
    VM_SetVar("Root/Variables/Chapter2/MtSarulente/iIzunaSawLibrary", "N", 1.0)
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("Empress:[E|Neutral] Well?[P] Is it everything you hoped?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] The roof collapsed...[P] how long ago?[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] Ages.[P] I'm surprised there's any books on the shelves, I thought they had been cleaned out.[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] Perhaps the ink ran or the books rotted.[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] Let this be seen as the result of any great work we do.[P] No matter how long you sit with the mountain, eventually, only the mountain remains.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] What a shame.[P] Maybe we should check the surviving books anyway?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] Whatever makes you smile, babe.") ]])
    fnCutsceneBlocker()

end
