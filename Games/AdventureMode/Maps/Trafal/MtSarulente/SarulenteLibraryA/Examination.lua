-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= Exits ========================================== ]|
if(sObjectName == "ToSarulenteB") then
	AudioManager_PlaySound("World|FlipSwitch")
	AL_BeginTransitionTo("SarulenteB", "FORCEPOS:4.5x17.0x0")
    
-- |[ ====================================== Examinables ======================================= ]|
elseif(sObjectName == "NopeSign") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (There will be a stairway here in a later version.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BooksA") then
    
    --Variables.
    local iIzunaFoundBook       = VM_GetVar("Root/Variables/Chapter2/MtSarulente/iIzunaFoundBook", "N")
    local iSawFoundBookTutorial = VM_GetVar("Root/Variables/Chapter2/KitsuneVillage/iSawFoundBookTutorial", "N")
    
    --First time:
    if(iIzunaFoundBook == 0.0) then
    
        --Flag.
        VM_SetVar("Root/Variables/Chapter2/MtSarulente/iIzunaFoundBook", "N", 1.0)
        VM_SetVar("Root/Variables/Chapter2/KitsuneVillage/iSawFoundBookTutorial", "N", 1.0)
    
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        fnCutscene([[ Append("Izuna:[E|Neutral] The books in here are...[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Happy] Oh my goodness![P] Sanya, look![B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Happy] Horny for books![P] Fuck yeah![B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Smirk] No, you big silly goof![P] This is a biography of Ilo Granvire![B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Happy] Horny for Ilo Granvire![P] Fuck yeah![B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Blush] (Is this an Earth thing?)[B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] Is there some reason that is important to you, Izuna?[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Explain] All the copies of this book were thought lost![P] We can bring this back to the village![P] We can make copies![B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Happy] Sanya, we're restoring lost histories![P] We're heroes![B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Happy] Horny for knowledge![P] Fuck yeah![B][C]") ]])
        fnCutscene([[ Append("Zeke:[E|Neutral] Bah![B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Happy] Horny for knowledge![P] Absolutely![B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] (Be steady as a rock.[P] The children have their own culture.[P] Don't judge.)[B][C]") ]])
        if(iSawFoundBookTutorial == 1.0) then
            fnCutscene([[ Append("Izuna:[E|Smirk] [SOUND|World|TakeItem]Gotta be careful with it.[P] Okay, let's go!") ]])
        else
            fnCutscene([[ Append("Izuna:[E|Smirk] [SOUND|World|TakeItem]Gotta be careful with it.[P] Okay, let's go![B][C]") ]])
            fnCutscene([[ Append("Narrator: You can read any important books you find in the kitsune library basement.") ]])
        end
        fnCutsceneBlocker()
    
    --Repeats:
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        fnCutscene([[ Append("Sanya:[E|Neutral] Any other cool finds in here?[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Neutral] No, the rest of the books are too damaged.[P] Unfortunate.[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Neutral] We should keep our eyes out for other books like this, though![P] Perhaps copies of lost books can be found elsewhere!") ]])
    end
    
elseif(sObjectName == "BooksB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (All the books are unreadable.[P] The pages are eaten out, destroyed by water, or don't have enough illustrations.)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "BooksC") then
    AL_SetProperty("Set Layer Disabled", "SkillbookIco", true)
	LM_ExecuteScript(gsIzunaSkillbook, 0)

-- |[ ========================================== Other ========================================= ]|
-- |[ ========================================== Debug ========================================= ]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
