-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
if(sObjectName == "Trigger") then
    
    -- |[Repeat Check]|
    local iSawTomb = VM_GetVar("Root/Variables/Chapter2/MtSarulente/iSawTomb", "N")
    if(iSawTomb == 1.0) then return end
    
    -- |[Flag]|
    VM_SetVar("Root/Variables/Chapter2/MtSarulente/iSawTomb", "N", 1.0)

    -- |[Movement]|
    fnCutsceneMergeParty()
    fnCutsceneBlocker()
    fnCutsceneMove("Sanya", 13.25, 12.50)
    fnCutsceneFace("Sanya", 1, 0)
    fnCutsceneMove("Izuna", 11.25, 12.50)
    fnCutsceneFace("Izuna", -1, 0)
    fnCutsceneMove("Zeke",  12.25, 12.50)
    fnCutsceneFace("Zeke",  1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Izuna:[VOICE|Izuna] Hmm...") ]])
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneMove("Izuna", 11.25, 11.50)
    fnCutsceneMove("Izuna", 10.25, 11.50)
    fnCutsceneMove("Izuna", 10.25, 10.50)
    fnCutsceneMove("Izuna",  7.25, 10.50)
    fnCutsceneFace("Izuna", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutsceneMove("Izuna",  6.25, 10.50)
    fnCutsceneFace("Izuna", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutsceneMove("Sanya", 11.25, 12.50)
    fnCutsceneMove("Sanya", 11.25, 11.50)
    fnCutsceneMove("Sanya", 10.25, 11.50)
    fnCutsceneMove("Sanya", 10.25, 10.50)
    fnCutsceneMove("Sanya",  7.25, 10.50)
    fnCutsceneFace("Sanya", -1, 0)
    fnCutsceneMove("Zeke", 11.25, 12.50)
    fnCutsceneMove("Zeke", 11.25, 11.50)
    fnCutsceneMove("Zeke", 10.25, 11.50)
    fnCutsceneMove("Zeke", 10.25, 10.50)
    fnCutsceneMove("Zeke",  8.25, 10.50)
    fnCutsceneFace("Zeke", -1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Izuna", "Neutral") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Find anything cool?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] Not really.[P] There's no way this is the real tomb of the dragon empress.[P] Look at how small it is![P] There's not even any traps![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Blush] Traps?[P] Sounds awesome![P] Let's go put you in traction for another month![B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Ugh] Oh yeah.[P] But that's a good point, too.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] The real tomb would probably have sevavi guarding it, right?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] Okay, let's keep looking.[P] At the very least we found [P]*something*[P] up here, right?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Explain] Finding nothing is better than not looking.[P] But...[P] you take point, okay?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Fold]|
    fnAutoFoldParty()
    fnCutsceneBlocker()
end
