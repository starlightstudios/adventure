-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
if(sObjectName == "TheScene") then

    -- |[ ===== Constants ===== ]|
    local ciRoarTicks = 5

    -- |[Local Functions]|
    --Sets the dragon tile layers to the given index.
    local function fnSetDragonTo(piIndex)
        for i = 0, 4, 1 do
            if(i == piIndex) then
                fnCutsceneLayerDisabled("Dragon" .. i, false)
            else
                fnCutsceneLayerDisabled("Dragon" .. i, true)
            end
        end
    end
    
    --Sets the dragon to "Speaking".
    local function fnSetDragonSpeaking()
        fnSetDragonTo(1)
        fnCutsceneWait(ciRoarTicks)
        fnCutsceneBlocker()
        fnSetDragonTo(0)
        fnCutsceneBlocker()
    end
    
    --Sets the dragon to its neutral face.
    local function fnSetDragonNeutral()
        fnCutsceneBlocker()
        fnSetDragonTo(1)
        fnCutsceneWait(ciRoarTicks)
        fnCutsceneBlocker()
        fnSetDragonTo(2)
        fnCutsceneBlocker()
    end

    -- |[Repeat Check]|
    local iSawBigScene = VM_GetVar("Root/Variables/Chapter2/MtSarulente/iSawBigScene", "N")
    if(iSawBigScene >= 1.0) then return end
    
    -- |[Flag]|
    VM_SetVar("Root/Variables/Chapter2/MtSarulente/iMiaGaveEmpressQuest", "N", 1.0) --Set even if Mia didn't give you the quest
    VM_SetVar("Root/Variables/Chapter2/MtSarulente/iSawBigScene", "N", 1.0)
    
    -- |[Setup]|
    fnSetDragonTo(3)
    AL_SetProperty("Music", "Null")
    fnCutsceneTeleport("Zeke", -1.25, -1.50)
    
    -- |[Movement]|
    fnCutsceneMove("Sanya", 22.75, 16.50)
    fnCutsceneFace("Sanya", 0, -1)
    fnCutsceneMove("Izuna", 23.75, 16.50)
    fnCutsceneFace("Izuna", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Izuna", "Neutral") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Huh.[P] A big cave.[P] End of the line I guess.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] Strange, very strange.[P] With that many monuments out front, this should be a grand tomb.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Cry] Oh no, you don't think there was a tomb here, but it collapsed?[P] Think of the lost histories...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Laugh] So you're tearing up because you [P]*don't*[P] have to rob a tomb?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Cry] Think of the lost knowledge![P] We could have gained all sorts of wisdom from the old times, and it's gone because of a rockslide.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] I guess it sucks, but no use crying over spilled milk.[P] C'mon...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Surprised] ...[P] Did you hear that?!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneCamera("Position", gcfCamera_Cutscene_Speed, 23.25, 15.50)
    fnCutscene([[ AL_SetProperty("Music", "Meeting Empress") ]])
    fnCutscene([[ AL_SetProperty("Screen Shake Periodicity", 150, 200, 25) ]])
    fnCutscene([[ AL_SetProperty("Add Shake Sound", "World|RockRumbleA") ]])
    fnCutscene([[ AL_SetProperty("Add Shake Sound", "World|RockRumbleB") ]])
    fnCutscene([[ AL_SetProperty("Add Shake Sound", "World|RockRumbleC") ]])
    fnCutscene([[ AL_SetProperty("Rock Fall Chance", math.floor(10000 / 500)) ]])
    fnCutscene([[ AL_SetProperty("Add Rock Fall Image", "Root/Images/Sprites/RockFalls/RockRegA") ]])
    fnCutscene([[ AL_SetProperty("Add Rock Fall Image", "Root/Images/Sprites/RockFalls/RockRegB") ]])
    fnCutscene([[ AL_SetProperty("Add Rock Fall Image", "Root/Images/Sprites/RockFalls/RockRegC") ]])
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    fnCutsceneLayerDisabled("DragonShade4", true)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneLayerDisabled("DragonShade3", true)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneLayerDisabled("DragonShade2", true)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneLayerDisabled("DragonShade1", true)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneLayerDisabled("DragonShade0", true)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneMoveFace("Sanya", 22.75, 18.50, 0, -1, 0.40)
    fnCutsceneMoveFace("Izuna", 23.75, 18.50, 0, -1, 0.40)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnSetDragonTo(2)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnSetDragonSpeaking()
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Dragon:[VOICE|Empress] [LOUD]Mortals![P] Who dares disturb my slumber?[NOLOUD] ") ]])
    fnCutsceneBlocker()
    fnSetDragonNeutral()
    fnCutsceneBlocker()
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] [QUIET]Oh my God we're about to die.[NOQUIET] [B][C]") ]])
    fnCutscene([[ Append("Izuna:[VOICE|Izuna] [QUIET]Sanya, I love you.[P] I love you so much.[NOQUIET] [B][C]") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] [QUIET]I love you Izuna.[P] I wish we had spent more time together.[NOQUIET] ") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnSetDragonSpeaking()
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Dragon:[VOICE|Empress] [LOUD]Silence![P] What is your purpose here?[NOLOUD] ") ]])
    fnCutsceneBlocker()
    fnSetDragonNeutral()
    fnCutsceneBlocker()
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Uh, we uh, um.[P] We were. Um...[B][C]") ]])
    fnCutscene([[ Append("Izuna:[VOICE|Izuna] She's gonna eat us in one bite...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnSetDragonSpeaking()
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Dragon:[VOICE|Empress] [LOUD]I care not for trivialities![NOLOUD] [B][C]") ]])
    fnCutscene([[ Append("Dragon:[VOICE|Empress] [LOUD]Leave your tribute and begone![NOLOUD] ") ]])
    fnCutsceneBlocker()
    fnSetDragonNeutral()
    fnCutsceneBlocker()
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] B-b-b-but we...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Need your help...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] [QUIET]Saving the world...[NOQUIET] ") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnSetDragonTo(1)
    fnCutsceneWait(ciRoarTicks)
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Music", "Null") ]])
    fnCutsceneTeleport("Zeke", 23.25, 14.50)
    fnCutsceneFace("Zeke", 1, 0)
    fnSetDragonTo(0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Dragon:[VOICE|Empress] [LOUD]Do not test my patience![NOLOUD] ") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Zeke", 0, 1)
    
    -- |[Dialogue]|
    fnCutscene([[ AL_SetProperty("Screen Shake Periodicity", 0, 0, 0) ]])
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Zeke:[VOICE|Zeke] ...[P] Bah?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(325)
    fnCutsceneBlocker()
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Izuna", "Neutral") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] What.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Ugh] Um, so I.[P] Uh.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Ugh] Do you know you have a goat in your mouth, giant dragon lady?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Blush] (Sanya said she loves me, oh my...)[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Blush] (Are we in that stage of our relationship?[P] It all feels so fast...)[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Blush] (Do I really want to spend the rest of my life with her?[P] Now that we're not going to die in ten seconds...)[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Zeke, how did you get in there?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneTeleport("Zeke", -1.25, -1.50)
    fnSetDragonNeutral()
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Sounds.
    fnCutscenePlaySound("World|FlipSwitch")
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscenePlaySound("World|FlipSwitch")
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscenePlaySound("World|FlipSwitch")
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscenePlaySound("World|FlipSwitch")
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    fnCutsceneTeleport("Zeke", 14.25, 18.50)
    fnCutsceneLayerDisabled("BlackoutDoor", true)
    fnCutscenePlaySound("World|Thump")
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneMove("Zeke", 14.25, 17.50)
    fnCutsceneBlocker()
    fnCutsceneMove("Zeke", 21.75, 17.50)
    fnCutsceneMove("Sanya", 22.75, 17.50)
    fnCutsceneMove("Izuna", 23.75, 17.50)
    fnCutsceneFace("Sanya", -1, 0)
    fnCutsceneFace("Izuna", -1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutsceneCamera("Actor Name", gcfCamera_Cutscene_Speed, "Sanya")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Sanya", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Izuna", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Zeke", "Neutral") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] Nyeh.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] That doesn't answer the question at all.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] But I don't understand.[P] Why isn't the dragon...[P] doing anything?[P] It's just staring at us.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] I think we just got Wizard of Oz'd, Izuna.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] Wizard of what?[P][CLEAR]") ]])
    fnCutscene([[ Append("Sanya:[E|Happy] But you can't fool a goat![P] Let's go see who the man behind the curtain is![B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] Bah!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnAutoFoldParty()
    fnCutsceneBlocker()
    
elseif(sObjectName == "Go Forward Idiot") then
    
    -- |[Activation Check]|
    local iSawBigScene = VM_GetVar("Root/Variables/Chapter2/MtSarulente/iSawBigScene", "N")
    local iMetEmpress  = VM_GetVar("Root/Variables/Chapter2/MtSarulente/iMetEmpress", "N")
    if(iSawBigScene ~= 1.0 or iMetEmpress > 0.0) then return end
    
    -- |[Execution]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Are you serious right now?[P] You see a giant dragon head and you don't want to investigate further?)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Go through that goddamn door!)") ]])
    fnCutsceneBlocker()
    fnCutsceneMove("Sanya", 23.25, 17.50)
    fnCutsceneBlocker()
    fnAutoFoldParty()
    fnCutsceneBlocker()
    
end
