-- |[ ============================= Pamphlet: The Bunny Smugglers ============================== ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ==== Minor Characters ==== ]|
-- |[Sevavi A]|
if(sActorName == "SevaviA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sevavi:[VOICE|Sevavi] ...[P] Hot!") ]])
    fnCutsceneBlocker()

-- |[Sevavi B]|
elseif(sActorName == "SevaviB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sevavi:[VOICE|Sevavi] My my.[P] Perhaps the lady would have Angelface over for tea?[P] I think they'd have much to discuss.") ]])
    fnCutsceneBlocker()

-- |[Lady Morus]|
elseif(sActorName == "LadyMorus") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Morus:[VOICE|Morus] After so many centuries, I am glad to see there is still so much innovation going on.") ]])
    fnCutsceneBlocker()

-- |[Empress]|
elseif(sActorName == "Empress") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Empress:[VOICE|Empress] In my time, such things were taboo.[P] I suppose I ought to let it go, but I find the practice of 'monsterboys' to be disgusting.") ]])
    fnCutsceneBlocker()
end

-- |[Clean Up]|
DL_PopActiveObject()
