-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
if(sObjectName == "Meeting Empress") then

    -- |[Activation Check]|
    local iMetEmpress = VM_GetVar("Root/Variables/Chapter2/MtSarulente/iMetEmpress", "N")
    if(iMetEmpress == 1.0) then return end

    -- |[Flag]|
    VM_SetVar("Root/Variables/Chapter2/MtSarulente/iMetEmpress", "N", 1.0)

    -- |[Variables]|
    local sSanyaForm = VM_GetVar("Root/Variables/Global/Sanya/sForm", "S")
    local iHasSevaviForm = VM_GetVar("Root/Variables/Global/Sanya/iHasSevaviForm", "N")
    
    -- |[Special Frames]|
    EM_PushEntity("LadyMorus")
        TA_SetProperty("Add Special Frame", "Crouch", "Root/Images/Sprites/Special/Sevavi|Crouch")
    DL_PopActiveObject()
    
    -- |[Movement]|
    fnCutsceneMergeParty()
    fnCutsceneBlocker()
    fnCutsceneMove("Sanya", 33.25, 10.50)
    fnCutsceneFace("Sanya", 0, 1)
    fnCutsceneMove("Izuna", 32.25, 10.50)
    fnCutsceneFace("Izuna", 0, 1)
    fnCutsceneMove("Zeke",  34.25, 10.50)
    fnCutsceneFace("Zeke",  0, 1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Voice:[VOICE|Sevavi] I think the phonograph got stuck again...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    -- |[Movement]|
    fnCutsceneMove("SevaviA", 33.25, 15.50)
    fnCutsceneMove("SevaviA", 33.25, 11.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sevavi:[VOICE|Sevavi] Ahem.") ]])
    fnCutsceneBlocker()
    fnCutsceneMoveFace("Sanya", 33.25, 9.50, 0, 1, 1.00)
    fnCutsceneMoveFace("Sanya", 34.25, 9.50, -1, 0, 1.00)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneMove("SevaviA", 33.25, 4.50)
    fnCutsceneMove("SevaviA", 29.25, 4.50)
    fnCutsceneFace("Izuna", 1, 0)
    fnCutsceneFace("Zeke", -1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Empress.
    fnCutscene([[ AL_SetProperty("Music", "Empress Maj") ]])
    fnCutsceneMove("Empress", 33.25, 15.50)
    fnCutsceneMove("LadyMorus", 32.25, 15.50)
    fnCutsceneFace("Empress", 0, -1)
    fnCutsceneFace("LadyMorus", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneMove("Sanya", 33.25, 9.50)
    fnCutsceneMove("Sanya", 33.25, 10.50)
    fnCutsceneFace("Sanya", 0, 1)
    fnCutsceneFace("Izuna", 0, 1)
    fnCutsceneFace("Zeke",  0, 1)
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Empress", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Morus", "Neutral") ]])
    fnCutscene([[ Append("Dragon:[E|Neutral] Well well, it seems we have more guests.[P] Are they with you, Lady Morus?[B][C]") ]])
    fnCutscene([[ Append("Morus:[E|Neutral] No.[P] They look armed.[P] Perhaps they are here to kill you?[B][C]") ]])
    fnCutscene([[ Append("Dragon:[E|Neutral] Maybe.[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] You there, interlopers.[P] Have you come to challenge the great Dragon Empress?[P] I must inform you that the ground here is quite cold.[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] Burying you will prove difficult.[P] You must agree that the loser's mangled corpse be tossed into a gorge.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Happy] OH MY GOODNESS![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Happy] HELL YEAH![B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] BAH![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Izuna, I like yelling but why are we doing it again?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Explain] Oh gee, nothing to be excited about except that this is her![P] The great dragon![P] Empress Arulente![B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] That is me, yes.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Explain] All of the shrines and roads and bridges you see in Trafal?[P] She built those centuries ago![P] And we're talking to her right now![B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] But...[P] how are you alive?[P] The empire fell seven hundred years ago...[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] We dragons are very long-lived.[P] I don't know how long I will live, but I feel I've got another few centuries in me.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Cry] Sanya...[P] she...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] She.[P] She?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Cry] Empress Arulente is the only person to survive the battle with Perfect Hatred...[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] The evidence is on my neck.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] I just can't believe we're talking to a historical figure![P] An Empress![P] This is amazing![B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] I do have that effect on people.[P] But I think I take your meaning.[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] You there, Sanya.[P] You have a runestone, I see.[P] You come here seeking glory, is that it?[P] You want to be the prophecied hero?[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] That road ends in destruction.[P] You will get no glory.[P] If you are a pretender, you will be dead before you reap any sort of benefit.[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] I have seen others, foolhardy, greedy, brave.[P] They didn't save the world, they found a nameless grave somewhere.[P] Is that what you want?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] No, not really.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] It doesn't matter if there's glory or whatever involved, if what Izuna says is true, this world is boned if I don't stop this bad guy.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] I'm starting to like it here, actually.[P] No microplastics, and everyone is really nice, and the food is good, so if this Perfect Hatred dingus is a problem, he and I will go at it sooner or later.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Sad] Frankly, if I'm the hero or not doesn't matter.[P] Someone's gotta stop him.[B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] Bah...[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] I appreciate your outlook.[P] Glory-seeking is fundamentally an exercise in vanity.[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] So, Sanya.[P] Please tell me a bit more about where you're from.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Dalmatia.[P] It's in a place called Croatia, on Earth.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Explain] I found her at the Shrine of the Hero![P] Where she was fated to appear![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Blush] By 'found' she means I had to rescue her from falling into a river.[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] Hm, so far, so good.[B][C]") ]])
    
    -- |[ ======= Sevavi Form ====== ]|
    -- |[Is Sevavi]|
    if(sSanyaForm == "Sevavi") then
        LM_ExecuteScript(LM_GetCallStack(0), "Meeting Empress Sevavi", 1)
    
    -- |[Has Sevavi Form]|
    elseif(iHasSevaviForm == 1.0) then
        LM_ExecuteScript(LM_GetCallStack(0), "Meeting Empress Has Sevavi", 1)
    
    -- |[Neither]|
    else
        LM_ExecuteScript(LM_GetCallStack(0), "Meeting Empress No Sevavi", 1)
    
    end
    
-- |[ ==================================== Sanya Was Sevavi ==================================== ]|
elseif(sObjectName == "Meeting Empress Sevavi") then

    -- |[Dialogue Continuation]|
    fnCutscene([[ Append("Empress:[E|Neutral] Now, this may sound unusual, but I take it you are not a sevavi by accident.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] I had to haul Izuna's unconscious body through a shrine or something full of statue girls.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] I happened to like their style, so I borrowed it.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Blush] (Is that how it happened?)[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] Very good.[P] That saves us some time.[P] I would like you to transform back into a human, if you are indeed capable.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] You got it.") ]])
    fnCutsceneBlocker()
    
    -- |[Mark Empress In the Party]|
    if(true) then
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter2/MtSarulente/iEmpressJoined", "N", 1.0)
        VM_SetVar("Root/Variables/Chapter2/MtSarulente/iEmpressWaiting", "N", 1.0)
        
        --Field Sprite.
        fnCutscene([[ 
        local iSlot = giFollowersTotal
        giFollowersTotal = giFollowersTotal + 1
        gsaFollowerNames[iSlot+1] = "Empress"

        --Get character's uniqueID. 
        EM_PushEntity("Empress")
            local iCharacterID = RE_GetID()
            TA_SetProperty("Activation Script", "Null")
        DL_PopActiveObject()

        --Store it and tell her to follow.
        giaFollowerIDs[iSlot+1] = iCharacterID
        AL_SetProperty("Follow Actor ID", iCharacterID)
        ]])

        --Place character in combat lineup.
        local iPlacedInSlot = 0
        if(AdvCombat_GetProperty("Is Member In Active Party", "Empress") == false) then
            for i = 0, 3, 1 do
                if(AdvCombat_GetProperty("Name of Active Member", i) == "Null") then
                    AdvCombat_SetProperty("Party Slot", i, "Empress")
                    iPlacedInSlot = i
                    break
                end
            end
        end

        --Normalize his EXP with the party leader's. She will have 120% of her EXP, 0 JP.
        local iLeaderXP = 0
        if(iPlacedInSlot ~= 0) then
            local sLeaderName = AdvCombat_GetProperty("Name of Active Member", 0)
            AdvCombat_SetProperty("Push Party Member", sLeaderName)
                iLeaderXP = AdvCombatEntity_GetProperty("Exp")
            DL_PopActiveObject()
        end

        --Set.
        AdvCombat_SetProperty("Push Party Member", "Sanya")
            AdvCombatEntity_SetProperty("Current Exp", math.floor(iLeaderXP * 1.20))
            AdvCombatEntity_SetProperty("Current JP", 0)
        DL_PopActiveObject()
    end

    -- |[Movement]|
    --Flashwhite.
    Cutscene_CreateEvent("Flash Christine White", "Actor")
        ActorEvent_SetProperty("Subject Name", "Sanya")
        ActorEvent_SetProperty("Flashwhite Quickly", "Null")
    DL_PopActiveObject()
    fnCutsceneBlocker()

    fnCutsceneWait(75)
    fnCutsceneBlocker()
    fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Sanya/Form_Human.lua") ]])
    fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Empress", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Morus", "Neutral") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Ta-daa.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] See?[P] I told you![B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] Indeed, you told me that you know about the prophecy.[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] You know the details, the names, all of the things I made public knowledge.[P] You did your research.[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] Now we will see if you are indeed, the real thing.[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] Apologies for this, Lady Morus.[P] Would you please stand there in front of me?[B][C]") ]])
    fnCutscene([[ Append("Morus:[E|Neutral] It's no trouble.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneMove("Sanya", 33.25, 12.50)
    fnCutsceneFace("Sanya", 0, 1)
    fnCutsceneMove("LadyMorus", 32.25, 12.50)
    fnCutsceneFace("LadyMorus", 0, 1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Rejoin common scene.
    LM_ExecuteScript(LM_GetCallStack(0), "Rejoin", 1)

-- |[ ==================================== Sanya Has Sevavi ==================================== ]|
elseif(sObjectName == "Meeting Empress Has Sevavi") then

    -- |[Dialogue Continuation]|
    fnCutscene([[ Append("Empress:[E|Neutral] For the next step, I will require you to become a sevavi.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Way ahead of you.[P] Just a second.") ]])
    fnCutsceneBlocker()

    -- |[Movement]|
    --Flashwhite.
    Cutscene_CreateEvent("Flash Christine White", "Actor")
        ActorEvent_SetProperty("Subject Name", "Sanya")
        ActorEvent_SetProperty("Flashwhite Quickly", "Null")
    DL_PopActiveObject()
    fnCutsceneBlocker()

    fnCutsceneWait(75)
    fnCutsceneBlocker()
    fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Sanya/Form_Sevavi.lua") ]])
    fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Empress", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Morus", "Neutral") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Stone cold, baby.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Explain] You see?[P] Her power to transform proves her authenticity![B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] How, precisely, did this come about?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] I had to haul Izuna's unconscious body through a shrine or something full of statue girls.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] I happened to like their style, so I borrowed it.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Blush] (Is that how it happened?)[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] So it was not casually offered.[P] I see.[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] Now if you would, transform back to human.[P] There is one last thing I need to see.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] You got it.") ]])
    fnCutsceneBlocker()
    
    -- |[Mark Empress In the Party]|
    if(true) then
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter2/MtSarulente/iEmpressJoined", "N", 1.0)
        VM_SetVar("Root/Variables/Chapter2/MtSarulente/iEmpressWaiting", "N", 1.0)
        
        --Field Sprite.
        fnCutscene([[ 
        local iSlot = giFollowersTotal
        giFollowersTotal = giFollowersTotal + 1
        gsaFollowerNames[iSlot+1] = "Empress"

        --Get character's uniqueID. 
        EM_PushEntity("Empress")
            local iCharacterID = RE_GetID()
            TA_SetProperty("Activation Script", "Null")
        DL_PopActiveObject()

        --Store it and tell her to follow.
        giaFollowerIDs[iSlot+1] = iCharacterID
        AL_SetProperty("Follow Actor ID", iCharacterID)
        ]])

        --Place character in combat lineup.
        local iPlacedInSlot = 0
        if(AdvCombat_GetProperty("Is Member In Active Party", "Empress") == false) then
            for i = 0, 3, 1 do
                if(AdvCombat_GetProperty("Name of Active Member", i) == "Null") then
                    AdvCombat_SetProperty("Party Slot", i, "Empress")
                    iPlacedInSlot = i
                    break
                end
            end
        end

        --Normalize his EXP with the party leader's. She will have 120% of her EXP, 0 JP.
        local iLeaderXP = 0
        if(iPlacedInSlot ~= 0) then
            local sLeaderName = AdvCombat_GetProperty("Name of Active Member", 0)
            AdvCombat_SetProperty("Push Party Member", sLeaderName)
                iLeaderXP = AdvCombatEntity_GetProperty("Exp")
            DL_PopActiveObject()
        end

        --Set.
        AdvCombat_SetProperty("Push Party Member", "Empress")
            AdvCombatEntity_SetProperty("Current Exp", math.floor(iLeaderXP * 1.20))
            AdvCombatEntity_SetProperty("Current JP", 0)
        DL_PopActiveObject()
    end

    -- |[Movement]|
    --Flashwhite.
    Cutscene_CreateEvent("Flash Sanya White", "Actor")
        ActorEvent_SetProperty("Subject Name", "Sanya")
        ActorEvent_SetProperty("Flashwhite Quickly", "Null")
    DL_PopActiveObject()
    fnCutsceneBlocker()

    fnCutsceneWait(75)
    fnCutsceneBlocker()
    fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Sanya/Form_Human.lua") ]])
    fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Empress", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Morus", "Neutral") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Now, what was it you needed?[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] Apologies for this, Lady Morus.[P] Would you please stand there in front of me?[P] Both of you.[B][C]") ]])
    fnCutscene([[ Append("Morus:[E|Neutral] It's no trouble.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneMove("Sanya", 33.25, 12.50)
    fnCutsceneFace("Sanya", 0, 1)
    fnCutsceneMove("LadyMorus", 32.25, 12.50)
    fnCutsceneFace("LadyMorus", 0, 1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Rejoin common scene.
    LM_ExecuteScript(LM_GetCallStack(0), "Rejoin", 1)

-- |[ ========================================= Rejoin ========================================= ]|
elseif(sObjectName == "Rejoin") then
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Right here?[B][C]") ]])
    fnCutscene([[ Append("Empress:[VOICE|Empress] That's fine.[P] Now.[B][C]") ]])
    fnCutscene([[ Append("Empress:[VOICE|Empress] [LOUD]Kneel, Sevavi.[NOLOUD]") ]])
    fnCutsceneBlocker()

    -- |[Movement]|
    fnCutsceneSetFrame("Sanya", "Crouch")
    fnCutsceneSetFrame("LadyMorus", "Crouch")
    fnCutsceneBlocker()
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Izuna:[VOICE|Izuna] *Psst, Zeke.[P] Should we be kneeling?*[B][C]") ]])
    fnCutscene([[ Append("Zeke:[VOICE|Zeke] *Baah?*[B][C]") ]])
    fnCutscene([[ Append("Empress:[VOICE|Empress] That's enough.[P] Please rise.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    -- |[Movement]|
    fnCutsceneSetFrame("Sanya", "Null")
    fnCutsceneSetFrame("LadyMorus", "Null")
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Empress", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Morus", "Neutral") ]])
    fnCutscene([[ Append("Sanya:[E|Sad] My head feels funny...[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] I suppose this is it, then.[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] Lady Morus, may I ask you to see to the preparations here?[P] The time has come.[B][C]") ]])
    fnCutscene([[ Append("Morus:[E|Neutral] Certainly.[P] I've got butterflies in my stomach![B][C]") ]])
    fnCutscene([[ Append("Morus:[E|Neutral] All this preparation and the time is finally here...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Okay so now you're on Team Sanya?[P] Because I kneeled all of the sudden?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] Who cares the reason![B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] Nyeh![B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] Well my dear, I don't fault you for not knowing that.[P] Nobody has known that for seven centuries.[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] We left some details out of the history books to fool charlatans and pretenders.[P] You can change forms, yes.[P] So could they.[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] But they never truly changed forms.[P] Body paint, magical trickery, one even tried to use mirrors and a body double.[P] Not the real thing.[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] When we created the Sevavi, we repurposed a spell once used for convicts.[P] A prisoner could agree to become a statue and perform public works to get a shorter sentence.[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] To keep them from causing problems when not on a work site, they had obedience compulsions in their heads to accompany their stone bodies.[P] Try as we might, we couldn't get all of them out when we adjusted the spell to make the Sevavi.[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] That compulsion to kneel is all that remains.[P] And you did it, even though you are a human.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Sad] Because I keep part of them with me even when I change...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] So that means you know I wasn't faking.[P] A faker wouldn't kneel.[B][C]") ]])
    fnCutscene([[ Append("Morus:[E|Neutral] It feels a little peculiar, but you get used to it.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Happy] So you'll help us?[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] I suppose these old bones might take a hit or two for you, yes.[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] Now, while you do have my assistance, I still recommend completing the Trials of the Dragon.[P] Some of the finest equipment ever forged is stored in the vaults deep within.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Happy] How about turning me into a dragon?[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] That is a step too far, at the moment.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Offended] Really?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Ugh] Sanya, don't be offended...[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] If you do indeed need the form for our purposes, then yes.[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] But you must realize, Sanya, that anything with the power to oppose Perfect Hatred likewise has the power to pose the same threat.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Offended] Bitch I am twice as dangerous as he is![B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] ...[P] Precisely.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Blush] Sanya has a rather unique candor...[B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] Nyeh![B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] Young one, the time will come when you have the powers of the dragon.[P] But you must realize that the prophecy to which I was party has little to say of the world that comes after.[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] I cannot be responsible for unleashing a greater evil than the one I sought to contain.[P] I must see your character before I give you this form.[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] There are few things in this world that can defeat a dragon in their prime.[P] That is why there are so few of us, and we are so hesitant to make more.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Sad] Man, everyone around here is making such good points.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Explain] You know what the elder says about things like this?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Explain] 'You've waited your whole life for this moment, what's a little longer?'[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Gee, when you put it that way.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Okay fine.[P] You can tag along, help us rough up rogues, and confirm that I'm as awesome as I look.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Offended] But Perfect Hatred is mine.[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] Confident.[P] Brash.[P] Ambitious.[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] You remind me of me when I was your age.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Old people.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Ugh] What have you got against old people, Sanya?[P] They're founts of wisdom![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] They're also the ones responsible for all the fuckups in the world we live in.[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] ...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Blush] But hey, if they want to help us fix things, let 'em.[P] Right?[B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] Bah![B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] Very good.[P] Now then. I take it you did not find the map at the Shrine of the Hero?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] There was a map?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] I saw some sort of altar there.[P] Not a map.[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] The shrine was built at the place you would arrive explicitly to provide you with a map to the Trials of the Dragon.[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] The sevavi there -[P] grrr.[P] Fine.[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] That shall be our first destination.[P] I have some...[P] things I need to say to the sevavi there.[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] Oh, and I suppose I ought to ask your names.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] I am Izuna![P] And this is Zeke![B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] Nyeh.[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] You know mine.[P] Refer to me by Empress.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Don't got a first name?[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] Lady Arulente if Empress does not work for you.[P] We didn't go by first names in the old days.[B][C]") ]])
    fnCutscene([[ Append("Morus:[E|Neutral] She's shy about it.[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] I hear people these days only have first names if they have no noble title.[P] Must make tax collecting a nightmare.[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] Enough talk.[P] Let us be off.") ]])
    fnCutsceneBlocker()
    
    --Fold the party.
    fnAutoFoldParty()
    fnCutsceneBlocker()
    
    --Collision.
    fnCutscene([[ TA_ChangeCollisionFlag("LadyMorus", true) ]])

-- |[ =============================== Sanya Does Not Have Sevavi =============================== ]|
elseif(sObjectName == "Meeting Empress No Sevavi") then

    -- |[Flags]|
    VM_SetVar("Root/Variables/Chapter2/MtSarulente/iEmpressWaiting", "N", 1.0)
    TA_ChangeCollisionFlag("Empress", true)
    TA_ChangeCollisionFlag("LadyMorus", true)

    -- |[Dialogue Continuation]|
    fnCutscene([[ Append("Empress:[E|Neutral] For the next step, I will require you to become a sevavi.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] You mean those statue chicks from the Shrine of the Hero?[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] ...[P] They did not offer you the form at the customary feast?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] I could go for a feast right now...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Yeah that didn't happen.[P] Zeke would've eaten till he exploded.[B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] BAH!?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] It's true, buddy.[P] Don't front.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Remember when you snuck into the chicken coop to eat all the seeds?[B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] ...[P] bah.[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] I see.[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] Fortunately, we have a pedestal here, in the servant's quarters.[P] Simply stand on it and it will transform you.[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] That is, unless you have some good reason to avoid transformative magics.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Yeah okay.[P] You just wait right there.") ]])
    fnCutsceneBlocker()
    
    --Fold the party.
    fnAutoFoldParty()
    fnCutsceneBlocker()
    
end
