-- |[ ==================================== Pamphlet: Dragon ==================================== ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ==== Minor Characters ==== ]|
-- |[Sevavi A]|
if(sActorName == "SevaviA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sevavi:[VOICE|Sevavi] Oh, this is about us![P] How exciting!") ]])
    fnCutsceneBlocker()

-- |[Sevavi B]|
elseif(sActorName == "SevaviB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sevavi:[VOICE|Sevavi] Does this mean we're celebrities?") ]])
    fnCutsceneBlocker()

-- |[Lady Morus]|
elseif(sActorName == "LadyMorus") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Morus:[VOICE|Morus] Heh.[P] You may gain fame from this pamphlet, but the people of Trafal cannot do your quest for you.[P] Remain focused.") ]])
    fnCutsceneBlocker()

-- |[Empress]|
elseif(sActorName == "Empress") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Empress:[VOICE|Empress] Ah, I believe we had issues with time travel on our to-do list for the researchers.[P] Seems they got around to it, posthumously.") ]])
    fnCutsceneBlocker()
end

-- |[Clean Up]|
DL_PopActiveObject()
