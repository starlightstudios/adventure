-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= Exits ========================================== ]|
if(sObjectName == "SampleExit") then
    
-- |[ ====================================== Examinables ======================================= ]|
elseif(sObjectName == "Phonograph") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (I believe this is called a phonograph, playing sound etched onto wax discs.[P] This doesn't explain anything.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Weights") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Each of these weights is 200 lbs, or 90kg.[P] I can lift this, easy!)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Pool") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (This doesn't look like a 'relaxing' pool, it looks like a 'swimming to keep fit' pool.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Barrels") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Very sweet juices.[P] Grape, strawberry, and lemon![P] Okay maybe not all sweet.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Kitchen") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A fully stocked kitchen set, with all the modern conveniences![P] Fit for an empress.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookshelfA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Literature from around the globe.[P] Lots of faraway places I don't care about and animals that look like you glued Earth animals together.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookshelfB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Some books with little notes on the front cover indicating they belong to the kitsune library and when to return them.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "WardrobeA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (There's some of this yellowish skirt stuff which looks fantastic, but also some more casual clothes for a night on the town.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "WardrobeB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Stylish capes and armor polish.[P] What else does a dragon need besides her scales?)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Mirror") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    local iRoll = LM_GetRandomNumber(1, 4)
    if(iRoll == 1) then
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Damn, I'm lookin' good.)") ]])
    elseif(iRoll == 2) then
        fnCutscene([[ Append("Thought:[VOICE|Leader] (This mirror looks really nice.[P] Oh wait, that's just me.)") ]])
    elseif(iRoll == 3) then
        fnCutscene([[ Append("Thought:[VOICE|Leader] (The light in here really captures my good side.)") ]])
    elseif(iRoll == 4) then
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Oh no, there's an extremely attractive woman right there!)") ]])
    end
    fnCutsceneBlocker()

elseif(sObjectName == "Pedestal") then

    -- |[Variables]|
    local iHasSevaviForm = VM_GetVar("Root/Variables/Global/Sanya/iHasSevaviForm", "N")

    -- |[Doesn't Have Statue]|
    if(iHasSevaviForm == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (A stone pedestal.[P] Nothing is written on or around it.[P] It's supposed to turn me into a sevavi.)") ]])

        --Decision script is this script. It must be surrounded by quotes.
        local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
        fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Stand On It\", " .. sDecisionScript .. ", \"Stand\") ")
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Leave It\",  " .. sDecisionScript .. ", \"Stop\") ")
        fnCutsceneBlocker()
    
    -- |[Has Statue]|
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (A stone pedestal.[P] These turn people into statues when stood on.[P] Can't recommend it enough.)") ]])
        fnCutsceneBlocker()
    end

elseif(sObjectName == "Stand") then
	WD_SetProperty("Hide")
    LM_ExecuteScript(gsRoot .. "Chapter 2/Scenes/400 Volunteer/SevaviSarulente/Scene_Begin.lua")

elseif(sObjectName == "Stop") then
	WD_SetProperty("Hide")

-- |[ ========================================== Other ========================================= ]|
-- |[ ========================================== Debug ========================================= ]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
