-- |[ ======================================= Constructor ====================================== ]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = fnResolveDirectory() --Map's name is the name of the folder it is in.
local sLevelMusic = "Null"
local sMapResolveName = "Map Resolve Name"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[ ======================================== Standard ======================================== ]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

    -- |[Construction]|
	--Music.
    local iMetEmpress    = VM_GetVar("Root/Variables/Chapter2/MtSarulente/iMetEmpress", "N")
    local iEmpressJoined = VM_GetVar("Root/Variables/Chapter2/MtSarulente/iEmpressJoined", "N")
    if(iEmpressJoined == 1.0 or iMetEmpress == 1.0) then
        AL_SetProperty("Music", "Empress Maj")
    else
        AL_SetProperty("Music", "Null")
    end
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
    
    -- |[Map Handling]|
    --Resolve the map's variable.
    local sMapVariable = fnResolveMapVarFromName(sLevelName)
    if(sMapVariable ~= "Null") then
        
        --Flag the map as uncovered.
        VM_SetVar(sMapVariable, "N", 1.0)
        
        --Run subroutine to build map properties.
        fnHandleMtSarulenteMap(sLevelName)

    end

-- |[ ======================================== Post-Exec ======================================= ]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	-- |[NPC Spawning]|
	--If an entity needs to spawn after a post-exec cutscene, put them here.
    fnSpawnNPCPattern("Sevavi", "A", "B")
    fnStandardNPCByPosition("LadyMorus")
    
    --Entity Movement.
    local iMetEmpress    = VM_GetVar("Root/Variables/Chapter2/MtSarulente/iMetEmpress", "N")
    local iEmpressJoined = VM_GetVar("Root/Variables/Chapter2/MtSarulente/iEmpressJoined", "N")
    
    --If Empress joined the party, the Sevavi path around.
    if(iEmpressJoined == 1.0) then
    
        --Flags.
        TA_ChangeCollisionFlag("SevaviA", false)
        TA_ChangeCollisionFlag("SevaviB", false)
        
        --Parallel scripts.
        local sBasePath = fnResolvePath()
        Cutscene_HandleParallel("Create", "SevaviA", sBasePath .. "PathScript_SevaviA.lua", false)
        Cutscene_HandleParallel("Create", "SevaviB", sBasePath .. "PathScript_SevaviB.lua", false)
    
    --Empress has not joined, but has been met. Spawn entities in certain positions.
    elseif(iMetEmpress == 1.0) then
        fnSpecialCharacter("Empress", -100, -100, gci_Face_South, true, fnResolvePath() .. "Dialogue.lua")
        LM_ExecuteScript(gsCostumeAutoresolve, "Empress_Conquerer")
        fnCutsceneTeleport("SevaviA", 30.25, 4.50)
        fnCutsceneFace("SevaviA", -1, 0)
        fnCutsceneTeleport("Empress", 33.25, 15.50)
        fnCutsceneFace("Empress", -1, 0)
        fnCutsceneTeleport("LadyMorus", 32.25, 15.50)
        fnCutsceneFace("LadyMorus", 1, 0)
    
    --First time entering.
    else
        fnSpecialCharacter("Empress", -100, -100, gci_Face_South, true, fnResolvePath() .. "Dialogue.lua")
        LM_ExecuteScript(gsCostumeAutoresolve, "Empress_Conquerer")

        fnCutsceneTeleport("SevaviA", 8.25, 15.50)
        fnCutsceneTeleport("Empress", 7.25, 15.50)
        fnCutsceneTeleport("LadyMorus", 6.25, 15.50)
        TA_ChangeCollisionFlag("Empress", false)
        TA_ChangeCollisionFlag("LadyMorus", false)
    end

    -- |[Overlays]|
    --Place overlay code here.
    
    -- |[Skillbooks]|
    --Place skillbook overlay code here.

    -- |[Rifle Handling]|
    VM_SetVar("Root/Variables/Global/Sanya/iRifleCode", "N", gciSanyaRifle_CanFire)

end
