-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
    
    -- |[Variables]|
    local iEmpressJoined = VM_GetVar("Root/Variables/Chapter2/MtSarulente/iEmpressJoined", "N")
    
    -- |[Sevavi A]|
    if(sActorName == "SevaviA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        if(iEmpressJoined == 0.0) then
            fnCutscene([[ Append("Sevavi:[VOICE|Sevavi] There's fur in the intake.[P] How did it get there?[B][C]") ]])
            fnCutscene([[ Append("Sanya:[VOICE|Sanya] (That's an extremely good question.)") ]])
        else
            fnCutscene([[ Append("Sevavi:[VOICE|Sevavi] We will keep your quarters clean for you, Empress.[P] Good luck!") ]])
        end
        fnCutsceneBlocker()
    
    -- |[Sevavi B]|
    elseif(sActorName == "SevaviB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        if(iEmpressJoined == 0.0) then
            fnCutscene([[ Append("Sevavi:[VOICE|Sevavi] Lady Morus, and now more guests?[P] How exciting!") ]])
        else
            fnCutscene([[ Append("Sevavi:[VOICE|Sevavi] I suppose I can spend more time on the pedastal, then.[P] I'll miss you, Empress.") ]])
        end
        fnCutsceneBlocker()
    
    -- |[Lady Morus]|
    elseif(sActorName == "LadyMorus") then
    
        --Variables.
        local sSanyaForm = VM_GetVar("Root/Variables/Global/Sanya/sForm", "S")
    
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        if(iEmpressJoined == 0.0) then
            
            if(sSanyaForm ~= "Sevavi") then
                fnCutscene([[ Append("Morus:[VOICE|Morus] If you really are the hero, you'll need to become a sevavi first.[P] Help yourself to the pedastal in the servant's quarters.") ]])
            else
                fnCutscene([[ Append("Morus:[VOICE|Morus] Splendid.[P] Speak to Empress for the next test.") ]])
            end
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Morus", "Neutral") ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutscene([[ Append("Morus:[E|Neutral] I certainly didn't expect to meet the prophecied hero on my visit.[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] Apologies for not seeing you out.[P] Stay as long as you like.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] So are you some sort of big shot?[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Explain] Lady Morus was the great economist who built the industrial might of the Arulente empire![B][C]") ]])
            fnCutscene([[ Append("Morus:[E|Neutral] Oh no, my dear, that was my grandmother.[P] I'm afraid I don't have the same sort of intellect she did.[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] Joelle Morus was a captain in the imperial army, back when we had one.[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] She became a sevavi along with the others to help contain the threat of Perfect Hatred.[P] She's also a friend of mine, and visits from time to time.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Jot] Oh![P] Hmm, hmm, let me get that down.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Smirk] Wow, I can't believe this![P] It's like interviewing a history book![B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Smirk] So how come you aren't coming with us, then?[B][C]") ]])
            fnCutscene([[ Append("Morus:[E|Neutral] I was overseeing one of the Trials of the Dragon, actually.[B][C]") ]])
            fnCutscene([[ Append("Morus:[E|Neutral] I assume you're going to at least attempt them, we did make them for a reason.[P] I'll head over there and make sure everything is in order.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Jot] Fascinating...[P] Great![P] Wow, history is alive right in front of us!") ]])
            fnCutsceneBlocker()
        end
        fnCutsceneBlocker()
    
    -- |[Empress]|
    elseif(sActorName == "Empress") then
    
        -- |[Variables]|
        local sSanyaForm = VM_GetVar("Root/Variables/Global/Sanya/sForm", "S")
    
        -- |[Dialogue]|
        if(sSanyaForm ~= "Sevavi") then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Empress", "Neutral") ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutscene([[ Append("Izuna:[E|Happy] Can I just say again how amazing this is?[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] I'm a fool for praise, but there is a pressing issue before us.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Jot] Actually, could I ask you a question or two?[P] Really quickly?[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] That depends on what the questions are.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Smirk] She's a writer.[P] Publishes pamphlets and everything.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Smirk] Okay, most important question, ummm...[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Smirk] Actually this might be a bit selfish but...[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Smirk] The great Lady Erickson.[P] A real person, or several people that got amalgamated into one?[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] You mean the famous kitsune bard?[P] A real person.[P] Why do you ask?[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Jot] Wow...[P] Gotta get this down...[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Neutral] Many of the books detailing her life were lost, all we have is her plays.[P] Some historians think no one person could have written so many.[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] Heh, well in that case, she was indeed three people.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Jot] Oh I see...[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] There were two good friends of hers who helped her write all her plays.[P] No one could have written so many alone.[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] But as for the boistrous personality, that is all too real.[P] She performed for me, actually.[P] I didn't know her socially, though.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] I am so lost.[B][C]") ]])
            fnCutscene([[ Append("Zeke:[E|Neutral] Nyeh.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Jot] A great playwright, Sanya.[P] A kitsune, in fact, who came from the very temple I do![B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Explain] The imperial library burned down some centuries ago, and a lot of books were lost.[P] We try to piece together what we can.[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] I heard of that.[P] Such a shame.[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] Now please, the pedestal, Sanya.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] If I can drag Izuna away for a second...[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Jot] O-[P]one more![B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] C'mon you!") ]])
            fnCutsceneBlocker()
        
        -- |[Join Sequence]|
        else
        
            --Unset flags.
            TA_ChangeCollisionFlag("Empress", false)
            TA_ChangeCollisionFlag("LadyMorus", false)
            
            -- |[Special Frames]|
            EM_PushEntity("LadyMorus")
                TA_SetProperty("Add Special Frame", "Crouch", "Root/Images/Sprites/Special/Sevavi|Crouch")
            DL_PopActiveObject()
        
            --Merge.
            fnCutsceneMergeParty()
            fnCutsceneBlocker()
        
            --Dialogue.
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Empress", "Neutral") ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutscene([[ Append("Empress:[E|Neutral] There we are.[P] How do you feel?[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] Like I could take on the whole world![B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] (Longer hair, eh?[P] Perhaps that's in style again.)[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] Good.[P] Now would you please stand just over there with Lady Morus?[P] And transform to a human, if you would.") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
        
            --Movement.
            fnCutsceneMove("Sanya", 33.25, 13.50)
            fnCutsceneFace("Sanya", 0, 1)
            fnCutsceneMove("LadyMorus", 32.25, 13.50)
            fnCutsceneFace("LadyMorus", 0, 1)
            fnCutsceneMove("Izuna", 31.25, 14.50)
            fnCutsceneFace("Izuna", 1, 0)
            fnCutsceneMove("Zeke", 31.25, 15.50)
            fnCutsceneFace("Zeke", 1, 0)
            fnCutsceneFace("Empress", 0, -1)
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
        
            --Flashwhite.
            Cutscene_CreateEvent("Flash Sanya White", "Actor")
                ActorEvent_SetProperty("Subject Name", "Sanya")
                ActorEvent_SetProperty("Flashwhite Quickly", "Null")
            DL_PopActiveObject()
            fnCutsceneBlocker()
            fnCutsceneWait(75)
            fnCutsceneBlocker()
            fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Sanya/Form_Human.lua") ]])
            fnCutsceneWait(gci_Flashwhite_Ticks_Total - 75 + 15)
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
        
            -- |[Dialogue]|
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Empress:[VOICE|Empress] Now, the most important part.[B][C]") ]])
            fnCutscene([[ Append("Empress:[VOICE|Empress] [LOUD]Kneel, Sevavi.[NOLOUD]") ]])
            fnCutsceneBlocker()

            -- |[Movement]|
            fnCutsceneSetFrame("Sanya", "Crouch")
            fnCutsceneSetFrame("LadyMorus", "Crouch")
            fnCutsceneBlocker()
            fnCutsceneWait(125)
            fnCutsceneBlocker()
            
            -- |[Dialogue]|
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Izuna:[VOICE|Izuna] *Psst, Zeke.[P] Should we be kneeling?*[B][C]") ]])
            fnCutscene([[ Append("Zeke:[VOICE|Zeke] *Baah?*[B][C]") ]])
            fnCutscene([[ Append("Empress:[VOICE|Empress] That's enough.[P] Please rise.") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()

            -- |[Movement]|
            fnCutsceneSetFrame("Sanya", "Null")
            fnCutsceneSetFrame("LadyMorus", "Null")
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
    
            -- |[Dialogue]|
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Empress", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Morus", "Neutral") ]])
            fnCutscene([[ Append("Sanya:[E|Sad] My head feels funny...[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] I suppose this is it, then.[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] Lady Morus, may I ask you to see to the preparations here?[P] The time has come.[B][C]") ]])
            fnCutscene([[ Append("Morus:[E|Neutral] Certainly.[P] I've got butterflies in my stomach![B][C]") ]])
            fnCutscene([[ Append("Morus:[E|Neutral] All this preparation and the time is finally here...[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] Okay so now you're on Team Sanya?[P] Because I kneeled all of the sudden?[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Smirk] Who cares the reason![B][C]") ]])
            fnCutscene([[ Append("Zeke:[E|Neutral] Nyeh![B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] Well my dear, I don't fault you for not knowing that.[P] Nobody has known that for seven centuries.[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] We left some details out of the history books to fool charlatans and pretenders.[P] You can change forms, yes.[P] So could they.[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] But they never truly changed forms.[P] Body paint, magical trickery, one even tried to use mirrors and a body double.[P] Not the real thing.[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] When we created the Sevavi, we repurposed a spell once used for convicts.[P] A prisoner could agree to become a statue and perform public works to get a shorter sentence.[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] To keep them from causing problems when not on a work site, they had obedience compulsions in their heads to accompany their stone bodies.[P] Try as we might, we couldn't get all of them out when we adjusted the spell to make the Sevavi.[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] That compulsion to kneel is all that remains.[P] And you did it, even though you are a human.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Sad] Because I keep part of them with me even when I change...[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] So that means you know I wasn't faking.[P] A faker wouldn't kneel.[B][C]") ]])
            fnCutscene([[ Append("Morus:[E|Neutral] It feels a little peculiar, but you get used to it.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Happy] So you'll help us?[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] I suppose these old bones might take a hit or two for you, yes.[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] Now, while you do have my assistance, I still recommend completing the Trials of the Dragon.[P] Some of the finest equipment ever forged is within the vaults deep within.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Happy] How about turning me into a dragon?[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] That is a step too far, at the moment.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Offended] Really?[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Ugh] Sanya, don't be offended...[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] If you do indeed need the form for our purposes, then yes.[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] But you must realize, Sanya, that anything with the power to oppose Perfect Hatred likewise has the power to pose the same threat.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Offended] Bitch I am twice as dangerous as he is![B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] ...[P] Precisely.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Blush] Sanya has a rather unique candor...[B][C]") ]])
            fnCutscene([[ Append("Zeke:[E|Neutral] Nyeh![B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] Young one, the time will come when you have the powers of the dragon.[P] But you must realize that the prophecy to which I was party has little to say of the world that comes after.[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] I cannot be responsible for unleashing a greater evil than the one I sought to contain.[P] I must see your character before I give you this form.[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] There are few things in this world that can defeat a dragon in their prime.[P] That is why there are so few of us, and we are so hesitant to make more.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Sad] Man, everyone around here is making such good points.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Explain] You know what the elder says about things like this?[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Explain] 'You've waited your whole life for this moment, what's a little longer?'[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] Gee, when you put it that way.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] Okay fine.[P] You can tag along, help us rough up rogues, and confirm that I'm as awesome as I look.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Offended] But Perfect Hatred is mine.[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] Confident.[P] Brash.[P] Ambitious.[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] You remind me of me when I was your age.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] Old people.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Ugh] What have you got against old people, Sanya?[P] They're founts of wisdom![B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] They're also the ones responsible for all the fuckups in the world we live in.[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] ...[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Blush] But hey, if they want to help us fix things, let 'em. Right?[B][C]") ]])
            fnCutscene([[ Append("Zeke:[E|Neutral] Bah![B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] Very good.[P] Now then. I take it you did not find the map at the Shrine of the Hero?[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Smirk] There was a map?[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Neutral] I saw some sort of altar there.[P] Not a map.[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] The shrine was built at the place you would arrive explicitly to provide you with a map to the Trials of the Dragon.[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] The sevavi there -[P] grrr.[P] Fine.[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] That shall be our first destination.[P] I have some...[P] things I need to say to the sevavi there.[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] Oh, and I suppose I ought to ask your names.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Neutral] I am Izuna![P] And this is Zeke![B][C]") ]])
            fnCutscene([[ Append("Zeke:[E|Neutral] Nyeh.[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] You know mine.[P] Refer to me by Empress.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] Don't got a first name?[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] Lady Arulente if Empress does not work for you.[P] We didn't go by first names in the old days.[B][C]") ]])
            fnCutscene([[ Append("Morus:[E|Neutral] She's shy about it.[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] I hear people these days only have first names if they have no noble title.[P] Must make tax collecting a nightmare.[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] Enough talk.[P] Let us be off.") ]])
            fnCutsceneBlocker()
            
            -- |[Mark Empress In the Party]|
            if(true) then
                
                --Flag.
                VM_SetVar("Root/Variables/Chapter2/MtSarulente/iEmpressJoined", "N", 1.0)
                
                --Field Sprite.
                fnCutscene([[ 
                local iSlot = giFollowersTotal
                giFollowersTotal = giFollowersTotal + 1
                gsaFollowerNames[iSlot+1] = "Empress"

                --Get character's uniqueID. 
                EM_PushEntity("Empress")
                    local iCharacterID = RE_GetID()
                    TA_SetProperty("Activation Script", "Null")
                DL_PopActiveObject()

                --Store it and tell her to follow.
                giaFollowerIDs[iSlot+1] = iCharacterID
                AL_SetProperty("Follow Actor ID", iCharacterID)
                ]])

                --Place character in combat lineup.
                local iPlacedInSlot = 0
                if(AdvCombat_GetProperty("Is Member In Active Party", "Empress") == false) then
                    for i = 0, 3, 1 do
                        if(AdvCombat_GetProperty("Name of Active Member", i) == "Null") then
                            AdvCombat_SetProperty("Party Slot", i, "Empress")
                            iPlacedInSlot = i
                            break
                        end
                    end
                end

                --Normalize his EXP with the party leader's. She will have 120% of her EXP, 0 JP.
                local iLeaderXP = 0
                if(iPlacedInSlot ~= 0) then
                    local sLeaderName = AdvCombat_GetProperty("Name of Active Member", 0)
                    AdvCombat_SetProperty("Push Party Member", sLeaderName)
                        iLeaderXP = AdvCombatEntity_GetProperty("Exp")
                    DL_PopActiveObject()
                end

                --Set.
                AdvCombat_SetProperty("Push Party Member", "Empress")
                    AdvCombatEntity_SetProperty("Current Exp", math.floor(iLeaderXP * 1.20))
                    AdvCombatEntity_SetProperty("Current JP", 0)
                DL_PopActiveObject()
            end
            
            --Fold the party.
            fnAutoFoldParty()
            fnCutsceneBlocker()
        
        end
    end
end
