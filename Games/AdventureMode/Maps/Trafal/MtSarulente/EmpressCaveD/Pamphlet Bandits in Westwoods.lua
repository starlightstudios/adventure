-- |[ ============================= Pamphlet: Bandits in Westwoods ============================= ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ==== Minor Characters ==== ]|
-- |[Sevavi A]|
if(sActorName == "SevaviA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sevavi:[VOICE|Sevavi] Bunch of thugs, attacking innocent merchants![P] Well, maybe not innocent...") ]])
    fnCutsceneBlocker()

-- |[Sevavi B]|
elseif(sActorName == "SevaviB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sevavi:[VOICE|Sevavi] Back in the old days, we never had anything like a bandit army.[P] The imperial garrison was more than enough for them!") ]])
    fnCutsceneBlocker()

-- |[Lady Morus]|
elseif(sActorName == "LadyMorus") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Morus:[VOICE|Morus] Bandits?[P] Shouldn't be a problem for a true hero, right?") ]])
    fnCutsceneBlocker()

-- |[Empress]|
elseif(sActorName == "Empress") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Empress:[VOICE|Empress] A problem you, the hero, will need to solve.[P] Your quest is not just to do one thing, but to assist all the peoples of the world.") ]])
    fnCutsceneBlocker()
end

-- |[Clean Up]|
DL_PopActiveObject()
