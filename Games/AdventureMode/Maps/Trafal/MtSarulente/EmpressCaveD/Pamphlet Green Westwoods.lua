-- |[ =============================== Pamphlet: Green Westwoods =============================== ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ==== Minor Characters ==== ]|
-- |[Sevavi A]|
if(sActorName == "SevaviA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sevavi:[VOICE|Sevavi] Oh, the scarred land.[P] Can one tree have done so much?") ]])
    fnCutsceneBlocker()

-- |[Sevavi B]|
elseif(sActorName == "SevaviB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sevavi:[VOICE|Sevavi] Why did I not think to call upon the alraunes to help?[P] Perhaps we have stewed in guilt for too long.") ]])
    fnCutsceneBlocker()

-- |[Lady Morus]|
elseif(sActorName == "LadyMorus") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Morus:[VOICE|Morus] Such a bold group of people, to follow their faith like that.[P] An enviable trait.") ]])
    fnCutsceneBlocker()

-- |[Empress]|
elseif(sActorName == "Empress") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Empress:[VOICE|Empress] If the alraunes can heal the scarred earth, perhaps the people's scarred souls are not beyond redemption.") ]])
    fnCutsceneBlocker()
end

-- |[Clean Up]|
DL_PopActiveObject()
