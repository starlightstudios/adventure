-- |[ =============================== Pamphlet: Green Westwoods =============================== ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[Old Guy]|
if(sActorName == "Old Guy") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Man:[VOICE|HumanM3] Reading material?[P] Put it on the shelf.[P] Believe me, I'll get to it when I'm out fishing.") ]])
end

-- |[Clean Up]|
DL_PopActiveObject()
