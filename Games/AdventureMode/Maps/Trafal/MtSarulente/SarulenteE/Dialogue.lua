-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[Name Resolve]|
    --Resolve the name of the calling entity.
    local sActorName = TA_GetProperty("Name")
    
    -- |[Old Guy]|
    if(sActorName == "Old Guy") then
    
        --Variables.
        local iEmpressJoined = VM_GetVar("Root/Variables/Chapter2/MtSarulente/iEmpressJoined", "N")
        
        --Common.
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        
        --No Empress.
        if(iEmpressJoined == 0.0) then
            fnCutscene([[ Append("Man:[VOICE|HumanM3] Been a while since I had a visitor.[P] What brings you to this old man's home?[B][C]") ]])
            fnCutscene([[ Append("Izuna:[VOICE|Izuna] We're looking for the tomb of the dragon empress.[B][C]") ]])
            fnCutscene([[ Append("Man:[VOICE|HumanM3] Ho ho![P] What an adventurer you are![B][C]") ]])
            fnCutscene([[ Append("Man:[VOICE|HumanM3] Keep going up the peak to the west.[P] There's a monument past the old fort.[B][C]") ]])
            fnCutscene([[ Append("Man:[VOICE|HumanM3] You might need to go inside the fort if the bridge isn't extended.[P] I'm sure you'll figure it out.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[VOICE|Izuna] Thank you, elder.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[VOICE|Sanya] (Izuna is so damn polite.[P] I don't know how she does it.[P] I was going to piledrive the old guy.)") ]])
        
        --Yes Empress.
        else
            fnCutscene([[ Append("Man:[VOICE|HumanM3] I see the young ones found you, Empress.[B][C]") ]])
            fnCutscene([[ Append("Empress:[VOICE|Empress] Indeed.[P] I've decided to travel with them.[P] Will you be all right by yourself?[P] Should I have a sevavi look after you?[B][C]") ]])
            fnCutscene([[ Append("Man:[VOICE|HumanM3] Ho ho![P] I can see to myself.[P] Hurry back.[P] Good luck you lot!") ]])
        end
        fnCutsceneBlocker()
    end
end
