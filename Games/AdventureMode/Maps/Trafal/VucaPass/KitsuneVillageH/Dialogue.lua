-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[Name Resolve]|
    --Resolve the name of the calling entity.
    local sActorName = TA_GetProperty("Name")

    --Set facing.
    TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[Common]|
    local iKitsuneState = fnResolveKitsuneState()
    
    -- |[KitsuneA]|
    if(sActorName == "KitsuneA") then
        
        --Intro:
        fnCutscene([[ WD_SetProperty("Show") ]])
        if(iKitsuneState == gciKitsuneIntro) then
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneC] Hello![P] Are you new in the village?[P] Welcome to our dance hall![B][C]") ]])
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneC] It looks roomy when nobody's here, but believe me, it really fills up during a dance.[B][C]") ]])
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneC] Me?[P] Oh I like to eat lunch in here.[P] It's nice and quiet.") ]])
        elseif(iKitsuneState == gciKitsuneRescuedIzuna) then
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneC] Well if it isn't the talk of the village![P] Izuna says you can transform, like the hero of legend![B][C]") ]])
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneC] Can you turn into a big giant monster or something?[B][C]") ]])
            fnCutscene([[ Append("Sanya:[VOICE|Sanya] Just monstergirls, and only ones I've been before.[B][C]") ]])
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneC] Awww.[P] I've heard dragons can turn into, like, mountain-sized dragons![P] You should do that![B][C]") ]])
            fnCutscene([[ Append("Izuna:[VOICE|Izuna] Then she'd have mountain-sized tits![B][C]") ]])
            fnCutscene([[ Append("Sanya:[VOICE|Sanya] Izuna, I insist we find a dragon to transform me!") ]])
        end
    end
end
