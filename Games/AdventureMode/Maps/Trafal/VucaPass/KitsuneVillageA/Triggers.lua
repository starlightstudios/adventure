-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ====================================== Kidnap Intro ====================================== ]|
if(sObjectName == "VillageIntro") then

    -- |[Repeat Check]|
    local iSawKidnapIntro = VM_GetVar("Root/Variables/Chapter2/KitsuneVillage/iSawKidnapIntro", "N")
    if(iSawKidnapIntro == 1.0) then return end
    
    -- |[Variables]|
    local iEmpressJoined = VM_GetVar("Root/Variables/Chapter2/MtSarulente/iEmpressJoined", "N")

    -- |[Flag]|
    VM_SetVar("Root/Variables/Chapter2/KitsuneVillage/iSawKidnapIntro", "N", 1.0)
    
    -- |[Movement]|
    fnCutsceneMergeParty()
    fnCutsceneBlocker()
    fnCutsceneMove("Izuna", 13.25, 16.50)
    fnCutsceneFace("Izuna", -1, 0)
    fnCutsceneMove("Sanya", 12.25, 16.00)
    fnCutsceneFace("Sanya", 1, 0)
    fnCutsceneMove("Zeke",  12.25, 17.00)
    fnCutsceneFace("Zeke",  1, 0)
    if(iEmpressJoined == 1.0) then
        fnCutsceneMove("Empress",  12.25, 18.00)
        fnCutsceneFace("Empress",  1, 0)
    end
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Sanya", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Zeke", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Izuna", "Neutral") ]])
    if(iEmpressJoined == 1.0) then
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Empress", "Neutral") ]])
    end
    fnCutscene([[ Append("Izuna:[E|Smirk] All right, this is the place.[P] Better let me handle the talking.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] And why is that?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] You're outsiders.[P] We don't usually allow outsiders into the village, and especially not the temple.[B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] Nyeh?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] Even you, Zeke.[P] Though if a wild fox came wandering in...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneMove("GuardA", 14.25, 14.50)
    fnCutsceneFace("GuardA", 0, 1)
    fnCutsceneMove("GuardB", 14.25, 16.50)
    fnCutsceneBlocker()
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    fnCutsceneFace("Izuna", 1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Izuna:[VOICE|Izuna] My friends![P] I'm back!") ]])
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    fnCutsceneFace("GuardB", 0, 1)
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneFace("GuardB", -1, 0)
    fnCutscenePlaySound("World|Thump")
    fnCutsceneSetFrame("Izuna", "Crouch")
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Izuna:[VOICE|Izuna] Owch! Ow![B][C]") ]])
    fnCutscene([[ Append("Guard:[VOICE|KitsuneA] You're coming with me![P] Right now!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneMove("Izuna", 17.25, 16.50)
    fnCutsceneMove("GuardB", 18.25, 16.50)
    fnCutsceneBlocker()
    fnCutsceneMove("Sanya", 13.25, 16.00)
    fnCutsceneMove("Zeke", 13.25, 17.00)
    fnCutsceneMove("GuardA", 14.25, 16.50, 2.50)
    fnCutsceneFace("GuardA", -1, 0)
    fnCutsceneMove("Izuna", 25.25, 16.50)
    fnCutsceneMove("Izuna", 25.25, 8.50)
    fnCutsceneMove("GuardB", 25.25, 16.50)
    fnCutsceneMove("GuardB", 25.25, 8.50)
    fnCutsceneTeleport("Izuna", -1.25, -1.50)
    fnCutsceneTeleport("GuardB", -1.25, -1.50)
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Izuna![B][C]") ]])
    fnCutscene([[ Append("Guard:[VOICE|KitsuneB] Hold it right there![B][C]") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Let her go![B][C]") ]])
    fnCutscene([[ Append("Guard:[VOICE|KitsuneB] Are you the one who found her?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Yeah![P] Fished her out of a river![P] She almost died, so let her go!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutsceneFace("GuardA", 0, 1)
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutsceneFace("GuardA", -1, 0)
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Guard:[VOICE|KitsuneB] Very well.[P] You are a guest among the kitsunes, please feel free to explore our village.[B][C]") ]])
    fnCutscene([[ Append("Guard:[VOICE|KitsuneB] The hot springs are a good way to relax, and if you are hungry, visit our cantina.[P] You may also visit our temple if you show proper respect to the elder.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Wh-[P] what?[B][C]") ]])
    fnCutscene([[ Append("Guard:[VOICE|KitsuneB] I'm sorry, was something unclear?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Yeah![P] You just kidnapped Izuna![B][C]") ]])
    fnCutscene([[ Append("Guard:[VOICE|KitsuneB] I assure you we did nothing of the sort.[B][C]") ]])
    fnCutscene([[ Append("Guard:[VOICE|KitsuneB] You will need to speak to the elder about this matter.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] We'll save you, Izuna![P] Just hang on![B][C]") ]])
    if(iEmpressJoined == 1.0) then
        fnCutscene([[ Append("Empress:[VOICE|Empress] Goodness, do you listen to yourself?[B][C]") ]])
        fnCutscene([[ Append("Sanya:[VOICE|Sanya] Nope![P] I'ma comin' Izuna![B][C]") ]])
    end
    fnCutscene([[ Append("Zeke:[VOICE|Zeke] Nyeyeyeh!") ]])
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Music", "KitsuneVillage") ]])
    
    -- |[Remove Izuna]|
    --Remove her temporarily from the party lineup.
    fnRemovePartyMember("Izuna", false)
    
    -- |[Fold]|
    fnAutoFoldParty()
    fnCutsceneBlocker()

-- |[ ======================================= No Leaving ======================================= ]|
elseif(sObjectName == "NoBack") then

    -- |[Activation Check]|
    local iSawKidnapIntro = VM_GetVar("Root/Variables/Chapter2/KitsuneVillage/iSawKidnapIntro", "N")
    local iRescuedIzuna = VM_GetVar("Root/Variables/Chapter2/KitsuneVillage/iRescuedIzuna", "N")
    if(iSawKidnapIntro == 0.0) then return end
    if(iRescuedIzuna == 1.0) then return end
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] We can't leave without rescuing Izuna!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneMove("Sanya", 9.25, 18.50)
    fnCutsceneBlocker()
    fnAutoFoldParty()
    fnCutsceneBlocker()


end
