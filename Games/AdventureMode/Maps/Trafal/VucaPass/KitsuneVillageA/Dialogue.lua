-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[Name Resolve]|
    --Resolve the name of the calling entity.
    local sActorName = TA_GetProperty("Name")

    --Set facing.
    TA_SetProperty("Face Character", "PlayerEntity")

    -- |[Common]|
    local iKitsuneState = fnResolveKitsuneState()
    
    -- |[GuardA]|
    if(sActorName == "GuardA") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        if(iKitsuneState == gciKitsuneIntro) then
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneB] You are welcome among the kitsunes.[P] Just don't track too much snow inside, it just snowed the other night.") ]])
        elseif(iKitsuneState == gciKitsuneRescuedIzuna) then
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneB] Thank you again for rescuing Izuna.") ]])
        end
    
    -- |[GuardB]|
    elseif(sActorName == "GuardB") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        
        --Special:
        local iSkillbookQuest = VM_GetVar("Root/Variables/Chapter2/KitsuneVillage/iSkillbookQuest", "N")
        if(iSkillbookQuest == 1.0) then
            VM_SetVar("Root/Variables/Chapter2/KitsuneVillage/iSkillbookQuest", "N", 2.0)
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] May I help you?[B][C]") ]])
            fnCutscene([[ Append("Izuna:[VOICE|Izuna] Has Chikage left the village?[B][C]") ]])
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] Yes, she just left.[P] Why?[B][C]") ]])
            fnCutscene([[ Append("Izuna:[VOICE|Izuna] Did she say where she was going?[B][C]") ]])
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] Poterup village.[P] Like usual.[B][C]") ]])
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] Why?[P] Did you need her for something?[B][C]") ]])
            fnCutscene([[ Append("Sanya:[VOICE|Sanya] I needed to talk to her.[B][C]") ]])
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] Poterup village, then.[P] That's your best shot at catching up with her.") ]])
            return
        end
        
        --Normal:
        if(iKitsuneState == gciKitsuneIntro) then
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] Guard duty is boring, but very important.[P] Plus, it gives me time to reflect.[P] And drink!") ]])
        elseif(iKitsuneState == gciKitsuneRescuedIzuna) then
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] The elder let you out already?[P] Don't go getting lost again!") ]])
        end
        
    -- |[Debug Gemcutter]|
    elseif(sActorName == "TempCutter") then
        TA_SetProperty("Face Character", "PlayerEntity")
        local sBasePath = fnResolvePath()
        local sString = "AM_SetShopProperty(\"Show\", \"Gemcutter\", \"" .. sBasePath .. "Gem Shop Setup.lua\", \"Null\")"
        fnCutscene(sString)
        fnCutsceneBlocker()
    end
end
