-- |[ ===================================== Item Shop Setup ==================================== ]|
--Sells basic gems, offers gemcutting.

-- |[Gemcutter Flag]|
AM_SetShopProperty("Allow Gemcutting")

-- |[Item Stock Listing]|
AM_SetShopProperty("Add Item", "Adamantite Powder",  -1, -1)
AM_SetShopProperty("Add Item", "Glintsteel Gem",     -1, -1)
AM_SetShopProperty("Add Item", "Ardrion Gem",        -1, -1)
AM_SetShopProperty("Add Item", "Samlion Gem",        -1, -1)
AM_SetShopProperty("Add Item", "Iniorose Gem",       -1, -1)
AM_SetShopProperty("Add Item", "Thatophage Gem",     -1, -1)
