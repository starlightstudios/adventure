-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[Name Resolve]|
    --Resolve the name of the calling entity.
    local sActorName = TA_GetProperty("Name")

    --Set facing.
    TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[Common]|
    local iKitsuneState = fnResolveKitsuneState()
    
    -- |[KitsuneA]|
    if(sActorName == "KitsuneA") then
        
        --Intro:
        fnCutscene([[ WD_SetProperty("Show") ]])
        if(iKitsuneState == gciKitsuneIntro) then
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] Oh where was it, I'm never going to find it...") ]])
        elseif(iKitsuneState == gciKitsuneRescuedIzuna) then
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] It was right here...") ]])
        end
    
    -- |[KitsuneB]|
    elseif(sActorName == "KitsuneB") then

        --Intro:
        fnCutscene([[ WD_SetProperty("Show") ]])
        if(iKitsuneState == gciKitsuneIntro) then
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneC] *num*[P] You're not supposed to eat in here but I just can't help myself.[P] I love grapes too much![P] *num*") ]])
        elseif(iKitsuneState == gciKitsuneRescuedIzuna) then
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneC] Oh, hey, Izuna.[P] What stories are you working on?[B][C]") ]])
            fnCutscene([[ Append("Izuna:[VOICE|Izuna] I'm currently doing a piece on the hero of legend who is going to save the world.[B][C]") ]])
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneC] I heard about that, but you're serious?[P] Is this her?[B][C]") ]])
            fnCutscene([[ Append("Izuna:[VOICE|Izuna] Yep![B][C]") ]])
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneC] Sorry if I don't immediately believe you, but Izuna always gets the facts right in the end.[P] I'll wait for the published copy.") ]])
        end
    
    -- |[KitsuneC]|
    elseif(sActorName == "KitsuneC") then

        --Intro:
        fnCutscene([[ WD_SetProperty("Show") ]])
        if(iKitsuneState == gciKitsuneIntro or iKitsuneState == gciKitsuneRescuedIzuna) then
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] Did you know this library was built centuries ago?[P] It's all in my book![B][C]") ]])
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] This temple is older than the old Arulente empire, but they built us a library into the rock to keep the books safe.[B][C]") ]])
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] If there's a fire, the stone breaks under the heat and releases non-flammable gas to douse the fire.[P] Nobody knows how to make it any more, it was lost when the empire fell.[B][C]") ]])
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] I'm hoping to find out how to make it again!") ]])
        end
    
    -- |[KitsuneD]|
    elseif(sActorName == "KitsuneD") then

        --Intro:
        fnCutscene([[ WD_SetProperty("Show") ]])
        if(iKitsuneState == gciKitsuneIntro) then
            fnCutscene([[ Append("Anzu:[VOICE|KitsuneD] Fiction is just as important as non-fiction when studying.[P] Anyone who tells you to read purely for technical reasons is not your friend.") ]])
        elseif(iKitsuneState == gciKitsuneRescuedIzuna) then
            fnCutscene([[ Append("Anzu:[VOICE|KitsuneD] Izuna![P] Izuna![P] The latest Magical Settlement came out while you were away![B][C]") ]])
            fnCutscene([[ Append("Anzu:[VOICE|KitsuneD] We got a copy here, want to read it?[B][C]") ]])
            fnCutscene([[ Append("Izuna:[VOICE|Izuna] I'm a little busy so [P]*no spoilers*[P] until I get to read it myself!") ]])
        end
    
    -- |[KitsuneE]|
    elseif(sActorName == "KitsuneE") then

        --Intro:
        fnCutscene([[ WD_SetProperty("Show") ]])
        if(iKitsuneState == gciKitsuneIntro) then
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] Another pilgrim?[P] If you want a book for study, you'll need to get a kitsune to take it out for you.[P] Those are the rules, sorry.") ]])
        elseif(iKitsuneState == gciKitsuneRescuedIzuna) then
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] Hello, pilgrim![P] It's a lovely day for learning!") ]])
        end
    
    -- |[KitsuneF]|
    elseif(sActorName == "KitsuneF") then

        --Intro:
        fnCutscene([[ WD_SetProperty("Show") ]])
        if(iKitsuneState == gciKitsuneIntro) then
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneD] Yes?[P] May I help you?[B][C]") ]])
            fnCutscene([[ Append("Sanya:[VOICE|Sanya] Can I get that book back there?[P] The one on the table just on the other side of this wall?[B][C]") ]])
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneD] The -[P] how did you -[P] I mean, no.[P] I'm sorry, a kitsune must take out the book for you, pilgrim.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[VOICE|Sanya] Oh, I have an idea of who I could get to borrow it for me...") ]])
        elseif(iKitsuneState == gciKitsuneRescuedIzuna) then
            
            --Variables.
            local iSkillbookQuest = VM_GetVar("Root/Variables/Chapter2/KitsuneVillage/iSkillbookQuest", "N")
            
            --First time:
            if(iSkillbookQuest == 0.0) then
                
                --Flag.
                VM_SetVar("Root/Variables/Chapter2/KitsuneVillage/iSkillbookQuest", "N", 1.0)
                
                --Spawn.
                TA_Create("Chikage")
                    TA_SetProperty("Position", 12, 29)
                    TA_SetProperty("Facing", gci_Face_South)
                    TA_SetProperty("Clipping Flag", false)
                    fnSetCharacterGraphics("Root/Images/Sprites/Chikage/", false)
                DL_PopActiveObject()
                
                --Dialogue.
                fnCutscene([[ Append("Kitsune:[VOICE|KitsuneD] Yes?[P] May I help you?[B][C]") ]])
                fnCutscene([[ Append("Sanya:[VOICE|Sanya] Can I get that book back there?[P] The one on the table just on the other side of this wall?[B][C]") ]])
                fnCutscene([[ Append("Kitsune:[VOICE|KitsuneD] I'm sorry, one moment.[P] There's someone else ahead of you in line.") ]])
                fnCutsceneBlocker()
                fnCutsceneWait(25)
                fnCutsceneBlocker()
            
                --Movement.
                fnCutsceneMove("Sanya", 13.25, 17.50)
                fnCutsceneFace("Sanya", 0, 1)
                fnCutsceneMove("Izuna", 12.25, 17.50)
                fnCutsceneFace("Izuna", 0, 1)
                fnCutsceneMove("Zeke",  11.25, 17.50)
                fnCutsceneFace("Zeke", 0, 1)
                fnCutsceneMove("KitsuneF", 15.25, 18.50)
                fnCutsceneFace("KitsuneF", -1, 0)
                fnCutsceneMove("Chikage", 13.25, 18.50)
                fnCutsceneFace("Chikage", 1, 0)
                fnCutsceneBlocker()
                fnCutsceneWait(25)
                fnCutsceneBlocker()
                
                --Dialogue.
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Chikage", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "KitsuneA", "Neutral") ]])
                fnCutscene([[ Append("Warrior:[VOICE|Chikage] Sorry to butt in line, I had to step out.[P] Was that book ready?[B][C]") ]])
                fnCutscene([[ Append("Librarian:[VOICE|KitsuneD] Of course, I'll get it for you.") ]])
                fnCutsceneBlocker()
                fnCutsceneWait(25)
                fnCutsceneBlocker()
                
                --Movement.
                fnCutsceneMove("KitsuneF", 16.25, 18.50)
                fnCutsceneMove("KitsuneF", 16.25, 13.50)
                fnCutsceneMove("KitsuneF", 14.25, 13.50)
                fnCutsceneFace("KitsuneF", 0, 1)
                fnCutsceneBlocker()
                fnCutsceneWait(25)
                fnCutsceneBlocker()
                fnCutscenePlaySound("World|TakeItem")
                fnCutsceneLayerDisabled("SpecialBook", true)
                fnCutsceneLayerDisabled("SkillbookIco", true)
                fnCutsceneWait(65)
                fnCutsceneBlocker()
                fnCutsceneMove("KitsuneF", 16.25, 13.50)
                fnCutsceneMove("KitsuneF", 16.25, 18.50)
                fnCutsceneMove("KitsuneF", 15.25, 18.50)
                fnCutsceneFace("KitsuneF", -1, 0)
                fnCutsceneBlocker()
                fnCutsceneWait(25)
                fnCutsceneBlocker()
                
                --Dialogue.
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Chikage", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "KitsuneA", "Neutral") ]])
                fnCutscene([[ Append("Warrior:[VOICE|KitsuneA] Thank you very much![B][C]") ]])
                fnCutscene([[ Append("Librarian:[VOICE|KitsuneD] No problem.[P] See you later.") ]])
                fnCutsceneBlocker()
                fnCutsceneWait(25)
                fnCutsceneBlocker()
                
                --Movement.
                fnCutsceneMove("KitsuneF", 15.25, 17.50)
                fnCutsceneFace("KitsuneF", -1, 0)
                fnCutsceneBlocker()
                fnCutsceneFace("Sanya", 1, 0)
                fnCutsceneFace("Izuna", 1, 0)
                fnCutsceneFace("Zeke", 1, 0)
                
                --Dialogue.
                fnCutsceneMove("Chikage", 12.25, 18.50)
                fnCutsceneMove("Chikage", 12.25, 32.50)
                fnCutsceneTeleport("Chikage", -1.25, -1.50)
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ Append("Librarian:[VOICE|KitsuneD] Sorry about that.[P] What can I do for you?[B][C]") ]])
                fnCutscene([[ Append("Sanya:[VOICE|Sanya] You son of a bitch![P][CLEAR]") ]])
                fnCutscene([[ Append("Librarian:[VOICE|KitsuneD] Excuse me?[B][C]") ]])
                fnCutscene([[ Append("Librarian:[VOICE|KitsuneD] How...[P] how did you know about all the terrible things I've done?[B][C]") ]])
                fnCutscene([[ Append("Librarian:[VOICE|KitsuneD] I'm not a bad person![P] I got scared![P] Carried away![P] I wish I could take it all back![B][C]") ]])
                fnCutscene([[ Append("Sanya:[VOICE|Sanya] N-[P]never mind!") ]])
                fnCutsceneBlocker()
                fnCutsceneWait(25)
                fnCutsceneBlocker()
                
                --Dialogue.
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
                fnCutscene([[ Append("Sanya:[E|Offended] Shit bastard![P] Ass shit bastard![B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Cry] Sanya, what's gotten into you?[B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Offended] We needed that book![B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Neutral] The one with the crossed swords above it?[B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Neutral] (Wait, did she see it too?)[B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Neutral] But it's a library.[P] You borrow books.[P] Someone else borrowed it first.[B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Neutral] Okay, well, we find them and borrow it from them.[P] I don't need to read it for long.[B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Neutral] Who's the kitsune who borrowed it?[B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Explain] That's Chikage.[P] She's one of the warrior kitsunes who patrol the glacier.[B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Smirk] She's probably long gone.[P] The warriors usually only stop in to pray or pick things up, and then it's right back onto the road.[B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Neutral] Do you know where she normally goes?[B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Neutral] Hm, I'm not sure.[P] We'd have to ask around.[B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Smirk] The guards near the entrance would know, she'd probably check in with them first.[B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Smirk] Okay okay, let's ask them.") ]])
                fnCutsceneBlocker()
                
                --Fold.
                fnAutoFoldParty()
                fnCutsceneBlocker()
            
            --Repeats.
            else
                fnCutscene([[ Append("Kitsune:[VOICE|KitsuneD] *sob*[P] It's my fault![P] Oh I'm so sorry![P] Fox Goddess, forgive me![B][C]") ]])
                fnCutscene([[ Append("Sanya:[VOICE|Sanya] (Great, what land mine did I step on this time...)") ]])
            
            end
        end
    
    -- |[KitsuneG]|
    elseif(sActorName == "KitsuneG") then

        --Intro:
        fnCutscene([[ WD_SetProperty("Show") ]])
        if(iKitsuneState == gciKitsuneIntro) then
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] Hello, visitor![P] Do keep your voice down in the library.[P] And make sure your goat doesn't headbutt anyone.[B][C]") ]])
            fnCutscene([[ Append("Zeke:[VOICE|Zeke] Nyah.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[VOICE|Sanya] He promises.") ]])
        elseif(iKitsuneState == gciKitsuneRescuedIzuna) then
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] You have such a cute goat, there.[B][C]") ]])
            fnCutscene([[ Append("Zeke:[VOICE|Zeke] Bah nyeh![B][C]") ]])
            fnCutscene([[ Append("Sanya:[VOICE|Sanya] Oh great, that'll go straight to his head.") ]])
        end
    end
end
