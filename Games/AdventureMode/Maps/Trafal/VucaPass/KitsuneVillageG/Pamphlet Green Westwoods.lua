-- |[ =============================== Pamphlet: Green Westwoods =============================== ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[KitsuneA]|
if(sActorName == "KitsuneA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] Sorry Izuna, I'm looking for something, give me the short version?[B][C]") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] Huh I hope the alraunes find what they are looking for.[P] Be it purpose or just good times.") ]])

-- |[KitsuneB]|
elseif(sActorName == "KitsuneB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneC] *num*[P] Oh hey Izuna, a new pamphlet?[P] I'll read and eat at the same time.[P] *num*[B][C]") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneC] Goddess, the Westwoods is really dangerous and you crossed it twice?[P] You are even braver than I thought Izuna!") ]])

-- |[KitsuneC]|
elseif(sActorName == "KitsuneC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] I like the printing on this one.[P] So the Alarunes are healing the pollution in the cauldron?[P] That's great to hear.[P] I don't think there are many books on healing pollution here, but maybe?[B][C]") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] I'm sure the Alraunes have an instinctive knowledge on what to do but it never hurts to check the wisdom of the past.") ]])

-- |[KitsuneD]|
elseif(sActorName == "KitsuneD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneD] Ooo![P] New pamphlet Izuna?[P] Give me a minute, you know the words like to jump around the page for me.[B][C]") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneD] So Alraunes are healing the cauldron?[P] I read this one story about a zone where the rules of reality inverted after aliens visited and there are these special guys who sneak in and try to bring back these special items that do magical stuff.[P] And then one of them finds a wish granter, but get this...[B][C]") ]])
    fnCutscene([[ Append("Izuna:[VOICE|Izuna] (Get out of here with that, Anzu.)") ]])

-- |[KitsuneE]|
elseif(sActorName == "KitsuneE") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] Howdy pilgrims![P] Whatcha up to?[P] Handing out pamphlets?[B][C]") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] Alraunes are trying to heal the cauldron?[P] Not a lot of good new lately, but that I'm happy to hear.[B][C]") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] I shall meditate and pray to the Goddess for their success.") ]])

-- |[KitsuneF]|
elseif(sActorName == "KitsuneF") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneD] *sob*[P] I didn't know they lived there.[P] There weren't any witnesses.") ]])

-- |[KitsuneG]|
elseif(sActorName == "KitsuneG") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] Hey thanks, Izuna![P] But Mia already gave us a copy so it's been filed away.[P][P] 'It Came from the WestWoods! Tales of corruption and redemption in the barren wastes' is in our archives.") ]])
end

-- |[Clean Up]|
DL_PopActiveObject()
