-- |[ ======================================== Kitsune A ======================================= ]|
--This kitsune is looking through the books.
local fWalkSpeed = 0.50
local fStairsSpeed = 0.20
local iLingerTicks = 125

--Mid Left
fnCutsceneMove("KitsuneA", 6.25, 28.50, fWalkSpeed)
fnCutsceneFace("KitsuneA", 0, -1)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

--Right
fnCutsceneMove("KitsuneA", 8.25, 28.50, fWalkSpeed)
fnCutsceneFace("KitsuneA", 0, -1)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

--Far Left
fnCutsceneMove("KitsuneA", 5.25, 28.50, fWalkSpeed)
fnCutsceneFace("KitsuneA", 0, -1)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

--Loop.
