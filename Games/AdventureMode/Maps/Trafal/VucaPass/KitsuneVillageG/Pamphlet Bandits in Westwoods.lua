-- |[ ============================= Pamphlet: Bandits in Westwoods ============================= ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[KitsuneA]|
if(sActorName == "KitsuneA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] Sorry Izuna, I'm looking for something, give me the short version?[B][C]") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] So there's an army of bandits?[P] I'm guessing they'll make a lot of stuff go missing.") ]])

-- |[KitsuneB]|
elseif(sActorName == "KitsuneB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneC] *num*[P] Oh hey Izuna, a new pamphlet?[P] I'll read and eat at the same time.[P] *num*[B][C]") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneC] So you guys saved the merchants by yourselves?[P] You are heroes![P] Great job Izuna.") ]])

-- |[KitsuneC]|
elseif(sActorName == "KitsuneC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] Oh handing out pamphlets, Izuna?[P] Let me see here.[P] Bandit problem.[P] I'm no fighter.[P] The Arulente empire had some really let's say 'impressive' ways of dealing with bandits.[P] I put the book back on the shelf after I flicked through.[P] Gave me nightmares.") ]])

-- |[KitsuneD]|
elseif(sActorName == "KitsuneD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Anzu:[VOICE|KitsuneD] Ooo![P] New pamphlet Izuna?[P] Give me a minute, you know the words like to jump around the page for me.[B][C]") ]])
    fnCutscene([[ Append("Anzu:[VOICE|KitsuneD] Bandits?[P] You know I read this really interesting story once.[P] It was about these adventurers guarding a caravan that's going through this primeval forest, but then they get ambushed by spider-girls![P] And then...[B][C]") ]])
    fnCutscene([[ Append("Izuna:[VOICE|Izuna] (She's been obsessed with that story for years...)") ]])

-- |[KitsuneE]|
elseif(sActorName == "KitsuneE") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] Howdy pilgrims![P] Whatcha up to?[P] Handing out pamphlets?[B][C]") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] Bandits, huh?[P] Starving bandits no less.[P] Everyone is the hero of their own story and everyone who stands against them a villain.[P] Been that way as long as there have been people.[P] I wish the right thing was always clear.[B][C]") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] I shall meditate and pray to the Goddess for insight and courage.") ]])

-- |[KitsuneF]|
elseif(sActorName == "KitsuneF") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneD] *sob*[P] They didn't have to die![P] I didn't enjoy it![P] I didn't enjoy it!") ]])

-- |[KitsuneG]|
elseif(sActorName == "KitsuneG") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] Hey thanks, Izuna![P] But Mia already gave us a copy so it's been filed away.[P][P] 'Bandit Attack! Real Life tales of adventure and derring do!' is in our archives.") ]])
end

-- |[Clean Up]|
DL_PopActiveObject()
