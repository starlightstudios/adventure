-- |[ ============================= Pamphlet: The Bunny Smugglers ============================== ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[KitsuneA]|
if(sActorName == "KitsuneA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] Sorry Izuna, I'm looking for something, give me the short version?[B][C]") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] So there's a criminal gang of bunnies.[P] If one of those took what I'm looking for I'm going to give them a carrot in a place they won't like.") ]])

-- |[KitsuneB]|
elseif(sActorName == "KitsuneB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneC] *num*[P] Oh hey Izuna, a new pamphlet?[P] I'll read and eat at the same time.[P] *num*[B][C]") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneC] So you broke up a smuggling ring by yourselves?[P] Is there nothing you won't do to get the facts Izuna?[P] Some great reporting![B][C]") ]])
    fnCutscene([[ Append("Izuna:[VOICE|Izuna] (That's not what happened but I guess that's what she got from reading it...)") ]])

-- |[KitsuneC]|
elseif(sActorName == "KitsuneC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] Wait what?[P] What did I just read?[P][P] Why is this not the biggest news anywhere?[P] Something we thought we knew about the fundamental nature of reality is just wrong![P] Goddess' Whiskers![B][C]") ]])
    fnCutscene([[ Append("Izuna:[VOICE|Izuna] (Fundamental nature of reality?[P] Really?[P] It's not that big of a deal...)") ]])

-- |[KitsuneD]|
elseif(sActorName == "KitsuneD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneD] Ooo![P] New pamphlet Izuna?[P] Give me a minute, you know the words like to jump around the page for me.[B][C]") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneD] So these bunnies are running weapons?[P] That reminds me of this one story I read, there's this mining town you see, it's being run by these rival gangs, and this private investigator tries to clean up the town by playing the gangs off each other.[P] But then he gets attached...[B][C]") ]])
    fnCutscene([[ Append("Izuna:[VOICE|Izuna] (How does everything remind her of another story?)") ]])

-- |[KitsuneE]|
elseif(sActorName == "KitsuneE") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] Howdy pilgrims![P] Whatcha up to?[P] Handing out pamphlets?[B][C]") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] I've heard sex is all about power but that's next level.[P] I wonder how many sign up for food, or because lives on Pandemonium are so very short?[P] Still interesting news at the least.[B][C]") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] I shall masturbate and pray to the Goddess for- [P]I meant to say meditate.[P] I meant meditate.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[VOICE|Izuna] (Yeah, I believe you.)") ]])

-- |[KitsuneF]|
elseif(sActorName == "KitsuneF") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneD] *sob*[P] The blood it was so warm, and all over my hands.[P] These hands have killed!") ]])

-- |[KitsuneG]|
elseif(sActorName == "KitsuneG") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] Hey thanks, Izuna![P] But Mia already gave us a copy so it's been filed away.[P][P] 'I Never Thought It Could Happen to Me: Lurid Bunny Tales!' is in our archives.[P][P] Expecting a lot of traffic on that one.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[VOICE|Izuna] For the record, Mia picks the titles.") ]])
end

-- |[Clean Up]|
DL_PopActiveObject()
