-- |[ ==================================== Pamphlet: Dragon ==================================== ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[KitsuneA]|
if(sActorName == "KitsuneA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] Sorry Izuna, I'm looking for something, give me the short version?[B][C]") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] So you found the legendary hero and the empress?[P] I guess she had been missing for 700 years.[P] Get less lost next time.") ]])

-- |[KitsuneB]|
elseif(sActorName == "KitsuneB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneC] *num*[P] Oh hey Izuna, a new pamphlet?[P] I'll read and eat at the same time.[P] *num*[B][C]") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneC] Oh wow so you really did write an article about the hero of legend.[P] I'll say the word of the 8 foot tall dragon empress behind is a pretty strong argument.[B][C]") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneC] Let's hope your facts are right Izuna, or opening the seal on perfect hatred is going to be a disaster.") ]])

-- |[KitsuneC]|
elseif(sActorName == "KitsuneC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] Hey Izuna, wait is that the dragon empress behind you?[P] Did you build this library?[P] Do you know how it works.[P] Oh I have so many questions, where is my pen?[P] My pen damn it!") ]])

-- |[KitsuneD]|
elseif(sActorName == "KitsuneD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Anzu:[VOICE|KitsuneD] Ooo![P] New pamphlet Izuna?[P] Give me a minute, you know the words like to jump around the page for me.[B][C]") ]])
    fnCutscene([[ Append("Anzu:[VOICE|KitsuneD] So the legend was true?[P] You know I read a whole bunch of stories about legends.[P] In one there is this guy who is prophesied to be beat evil, but it's in this whole cycle of time thingy so a version of him has to beat evil every time.[P] And then...[B][C]") ]])
    fnCutscene([[ Append("Izuna:[VOICE|Izuna] (I'll just slip away before Anzu recites the plot again...)") ]])

-- |[KitsuneE]|
elseif(sActorName == "KitsuneE") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] Howdy pilgrims![P] Whatcha up to?[P] Handing out pamphlets?[B][C]") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] So this is the time of prophecy.[P] Oh dear.[P] I guess it's a blessing to live in interesting times. Still, we have each other and the temple has withstood worse.[B][C]") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] I shall meditate and pray to the Fox Goddess for insight on how I can help.") ]])

-- |[KitsuneF]|
elseif(sActorName == "KitsuneF") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneD] *sob*[P] It's my fault![P] Oh I'm so sorry![P] It had to end, I had to do it!") ]])

-- |[KitsuneG]|
elseif(sActorName == "KitsuneG") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] Hey thanks, Izuna![P] But Mia already gave us a copy so it's been filed away.[P][P] 'Dragon Empress found in Tomb!' is in our archives.") ]])
end

-- |[Clean Up]|
DL_PopActiveObject()
