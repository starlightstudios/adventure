-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= Exits ========================================== ]|
if(sObjectName == "SampleExit") then
    
-- |[ ====================================== Examinables ======================================= ]|
elseif(sObjectName == "Locked") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Locked.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "EmptyBarrel") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A barrel formerly filled with liquid.[P] It's empty.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The kitsune is reading something about chemistry.[P] I didn't pay attention in school so I'm not about to argue with them.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (This book is something about a dragon empire from centuries ago.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The kitsune is reading a fiction book about fiction authors.[P] They all get together and beat up giant monsters.[P] Sounds pretty awesome.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('The Butler Did It'.[P] An anthology of short stories where the butler is always the killer, but in different ways.[P] Butlers just out there slaughtering people.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookshelfA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Here's a book.[P] 'Damn, that Quake bastard works fast![P] He heard about Operation Counterstrike, and hit first.[P] Racing back, I saw the place was overrun.'[P] Not much of a story but what would I know.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookshelfB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Here's a book.[P] 'For several hours, the vox picked up the sounds of combat::[P] guns firing, people yelling orders, screams, bones cracking, then finally, silence.'[P] Seems pretty metal.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookshelfC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Here's a book.[P] 'Once in the chamber, he climbed the ladder and started the rotors for the anti-mass spectrometer.'[P] Boooring!)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookshelfD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Here's a book.[P] 'On the colony, a secret military project to create cyborg super-soldiers takes on newfound importance.[P] The soldiers of the RULENT-II project rack up an impressive record against the Wasteriders in test deployments, but there are too few of them to turn the tide of the war.'[P] Military fiction?[P] Lame.)") ]])
    fnCutsceneBlocker()

-- |[ ========================================== Other ========================================= ]|
-- |[ ========================================== Debug ========================================= ]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
