-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= Exits ========================================== ]|
if(sObjectName == "SampleExit") then
    
-- |[ ====================================== Examinables ======================================= ]|
elseif(sObjectName == "PatronShelf") then
    
    --Dialogue.
    WD_SetProperty("Show")
    Append("Thought:[VOICE|Leader] (A row of books on famous supporters of the old dragon empire.[P] These sound like really special cool people.)[B][C]")
    
    --Mark topics to run after dialogue.
    WD_SetProperty("Activate Topics After Dialogue", "BackersKitsune") 
    
elseif(sObjectName == "BookA") then
    local iIzunaFoundBook = VM_GetVar("Root/Variables/Chapter2/MtSarulente/iIzunaFoundBook", "N")
    if(iIzunaFoundBook == 0.0) then return end
    
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Ilo Granvire, History, Life')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Apparently this Granvire guy was a military genius who lived during the tail end of the dragon empire.[P] He was known for his brilliant battlefield tactics.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (He wrote books about the adoption of cavalry and siege weapon commanding.[P] In the war of south succession, he used a new battlefield weapon.[P] Reputation.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The enemy generals were so scared of his bold flanking tactics and surprise attacks that they simply refused to move troops along winding forest routes.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Granvire figured this out and reallocated troops to hold his center, effectively defending his flanks with nothing but a massive bluff.[P] It worked.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Granvire Pass was named after him in honor of his service to the empire.[P] When it fell, he was one of the few surviving soldiers.[P] He was not personally at the Westwoods Massacre, having fallen ill in the weeks before it.[P] That illness saved his life, as casualties were over 97 percent.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (He disappeared sometime after that, as the record-keeping of the empire collapsed with its administration.[P] No one knows where he went or what happened to him.)") ]])
    fnCutsceneBlocker()

-- |[ ========================================== Other ========================================= ]|
-- |[ ========================================== Debug ========================================= ]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
