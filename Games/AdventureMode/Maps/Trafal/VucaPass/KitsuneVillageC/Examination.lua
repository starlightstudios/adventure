-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= Exits ========================================== ]|
if(sObjectName == "SampleExit") then
    
-- |[ ====================================== Examinables ======================================= ]|
elseif(sObjectName == "Berries") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The strawberries look fresh, like they were just plucked.)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "Kitchen") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A fully-featured kitchen set.[P] It isn't even necessary to spit roast bird meat, the future is here!)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "WineBarrel") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Wine that smells like it's made of figs.[P] Can you make fig wine?[P] Either way, it doesn't appeal to me.)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "WaterBarrel") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Barrels full of spring water, loaded with all kinds of minerals and vitamins and whatever else springs have in them.)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "Shelves") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Magically-preserved foods.[P] That's all I know, what am I a mage?[P] It looks fresh and there's snow on the ground.)") ]])
    fnCutsceneBlocker()

-- |[ ========================================== Other ========================================= ]|
-- |[ ========================================== Debug ========================================= ]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
