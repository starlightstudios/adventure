-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[Name Resolve]|
    --Resolve the name of the calling entity.
    local sActorName = TA_GetProperty("Name")

    --Set facing.
    TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[Common]|
    local iKitsuneState = fnResolveKitsuneState()
    
    -- |[KitsuneA]|
    if(sActorName == "KitsuneA") then
        
        --Intro:
        fnCutscene([[ WD_SetProperty("Show") ]])
        if(iKitsuneState == gciKitsuneIntro) then
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneC] I'm sorry, you just missed lunchtime.[P] We're just cleaning up.[B][C]") ]])
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneC] Is this your first day here?[P] It's nice to meet you!") ]])
        elseif(iKitsuneState == gciKitsuneRescuedIzuna) then
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneC] Did you get assigned clean-up duty as part of your punishment?[B][C]") ]])
            fnCutscene([[ Append("Izuna:[VOICE|Izuna] No, but I could ask the elder to change it.[P] Do you need a hand?[B][C]") ]])
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneC] Not right now, we're almost done.[P] We can always use more hands at meal time, though.") ]])
        end
    
    -- |[KitsuneB]|
    elseif(sActorName == "KitsuneB") then
        
        --Intro:
        fnCutscene([[ WD_SetProperty("Show") ]])
        if(iKitsuneState == gciKitsuneIntro) then
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] I'm just cleaning the place up.[P] Everyone acts all nice but they have the table manners of -[P] well, wild foxes![B][C]") ]])
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] If you're hungry, there's some strawberries over at the entry.[P] Get them while they're juicy![P] Seriously, the magic will wear off in a day or so and they'll spoil.") ]])
        elseif(iKitsuneState == gciKitsuneRescuedIzuna) then
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] Hey Izuna![P] Is this your girlfriend?[B][C]") ]])
            fnCutscene([[ Append("Izuna:[VOICE|Izuna] N-[P]No![B][C]") ]])
            fnCutscene([[ Append("Sanya:[VOICE|Sanya] Yes.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[VOICE|Izuna] Sanya![B][C]") ]])
            fnCutscene([[ Append("Sanya:[VOICE|Sanya] Are we supposed to hide it?[B][C]") ]])
            fnCutscene([[ Append("Izuna:[VOICE|Izuna] Now everyone is going to spread rumours![B][C]") ]])
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] It's not a rumour if it's true!") ]])
        end
    end
end
