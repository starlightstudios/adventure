-- |[ ======================================== Kitsune A ======================================= ]|
--Cleaning the kitchen.
local fWalkSpeed = 0.50
local fStairsSpeed = 0.20
local iLingerTicks = 180
fnCutsceneMove("KitsuneA", 12.25, 6.50, fWalkSpeed)
fnCutsceneFace("KitsuneA", 0, 1)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

fnCutsceneMove("KitsuneA", 13.25, 6.50, fWalkSpeed)
fnCutsceneFace("KitsuneA", 0, 1)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

fnCutsceneMove("KitsuneA", 16.25, 6.50, fWalkSpeed)
fnCutsceneMove("KitsuneA", 16.25, 5.50, fWalkSpeed)
fnCutsceneFace("KitsuneA", 0, -1)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

fnCutsceneMove("KitsuneA", 5.25, 5.50, fWalkSpeed)
fnCutsceneFace("KitsuneA", 0, -1)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

fnCutsceneMove("KitsuneA", 4.25, 5.50, fWalkSpeed)
fnCutsceneFace("KitsuneA", 0, -1)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

fnCutsceneMove("KitsuneA", 7.25, 5.50, fWalkSpeed)
fnCutsceneMove("KitsuneA", 7.25, 8.50, fWalkSpeed)
fnCutsceneMove("KitsuneA", 5.25, 8.50, fWalkSpeed)
fnCutsceneFace("KitsuneA", 0, -1)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

fnCutsceneMove("KitsuneA",  8.25, 8.50, fWalkSpeed)
fnCutsceneMove("KitsuneA",  8.25, 6.50, fWalkSpeed)
fnCutsceneMove("KitsuneA", 11.25, 6.50, fWalkSpeed)
fnCutsceneFace("KitsuneA", 0, 1)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()
