-- |[ ======================================== Kitsune B ======================================= ]|
--Cleaning the dining room.
local fWalkSpeed = 0.50
local fStairsSpeed = 0.20
local iLingerTicks = 180
fnCutsceneMove("KitsuneB", 6.25, 14.50, fWalkSpeed)
fnCutsceneFace("KitsuneB", -1, 0)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

fnCutsceneMove("KitsuneB", 6.25, 16.50, fWalkSpeed)
fnCutsceneFace("KitsuneB", -1, 0)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

fnCutsceneMove("KitsuneB",  7.25, 16.50, fWalkSpeed)
fnCutsceneMove("KitsuneB",  7.25, 19.50, fWalkSpeed)
fnCutsceneMove("KitsuneB", 16.25, 19.50, fWalkSpeed)
fnCutsceneMove("KitsuneB", 16.25, 18.50, fWalkSpeed)
fnCutsceneMove("KitsuneB", 17.25, 18.50, fWalkSpeed)
fnCutsceneFace("KitsuneB", 1, 0)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

fnCutsceneMove("KitsuneB", 17.25, 14.50, fWalkSpeed)
fnCutsceneFace("KitsuneB", 1, 0)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

fnCutsceneMove("KitsuneB", 17.25, 13.50, fWalkSpeed)
fnCutsceneMove("KitsuneB", 18.25, 13.50, fWalkSpeed)
fnCutsceneFace("KitsuneB", 0, -1)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

fnCutsceneMove("KitsuneB", 13.25, 13.50, fWalkSpeed)
fnCutsceneMove("KitsuneB", 13.25,  8.50, fWalkSpeed)
fnCutsceneFace("KitsuneB", 0, -1)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

fnCutsceneMove("KitsuneB", 13.25, 11.50, fWalkSpeed)
fnCutsceneMove("KitsuneB",  5.25, 11.50, fWalkSpeed)
fnCutsceneFace("KitsuneB", 0, 1)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

fnCutsceneMove("KitsuneB",  7.25, 11.50, fWalkSpeed)
fnCutsceneMove("KitsuneB",  7.25, 14.50, fWalkSpeed)
