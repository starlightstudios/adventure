-- |[ ============================= Pamphlet: Bandits in Westwoods ============================= ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[KitsuneA]|
if(sActorName == "KitsuneA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] Bandits are nothing new.[P] Is there something special about these ones?") ]])
    
-- |[KitsuneB]|
elseif(sActorName == "KitsuneB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneB] I hope war doesn't come to Trafal.[P] I enjoy the peaceful days and the plentiful food.") ]])
    
-- |[Yukina]|
elseif(sActorName == "Yukina") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Yukina", "Neutral") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] Oh, the bandits.[P] Yes, I've heard from some of the warriors about this horde.[P] I suppose we will have to do something about it soon.[B][C]") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] It is beyond my abilities, obviously.[P] It will be up to the council to decide what is the best course.[B][C]") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] Sanya, you have encountered them directly.[P] What is your take on them?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Offended] Pansies![B][C]") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] ...[P] Is that all?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Robbing merchants is the thing a jerk would do![P] Unless the merchants are jerks, too.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] It's a cycle of jerks.[P] Jerking begets jerking.[B][C]") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] There is wisdom in you, if one is willing to dig for it.[P] In any case, well done helping those trapped travellers.") ]])
end

-- |[Clean Up]|
DL_PopActiveObject()
