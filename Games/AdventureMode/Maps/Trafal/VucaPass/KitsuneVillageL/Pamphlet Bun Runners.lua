-- |[ ============================= Pamphlet: The Bunny Smugglers ============================== ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[KitsuneA]|
if(sActorName == "KitsuneA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] Where does someone get a name like Angelface anyway?") ]])
    
-- |[KitsuneB]|
elseif(sActorName == "KitsuneB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneB] Angelface could, in a different life, have made a wonderful kitsune.[P] Shame.") ]])
    
-- |[Yukina]|
elseif(sActorName == "Yukina") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Yukina", "Neutral") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] Ah ha![P] I had a smile on my face as I read this one, Izuna.[P] I've not seen these bunnies myself, but Angelface reminds me of a woman I knew some time ago.[B][C]") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] She was so brash and arrogant.[P] She broke all the rules, often just for the reaction it stirred up.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] What happened to her?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] She's gonna say 'it was me!' and laugh.[B][C]") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] Ha![P] No.[P] She got swept up by soldiers from Dresseheim and thrown in prison.[P] She died during an escape attempt.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Surprised] Cripes![B][C]") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] And them that take the sword shall perish by the sword.[P] An old kitsune saying.[P] She chose that life, lived it, and died in it.[B][C]") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] I wish Angelface the best.[P] Nobody deserves to die in prison.[P] But this is the risk the smugglers are taking.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Cry] I was worried the story might glorify her.[P] Flock people to her side.[B][C]") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] It very well might, but we must not censor the truth.[P] In its proper context, with full information, we must allow people to make their own decisions.[B][C]") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] So long as you do not publish lies, half-truths, or conspiratorial nonsense, I will encourage all your publications.") ]])
end

-- |[Clean Up]|
DL_PopActiveObject()
