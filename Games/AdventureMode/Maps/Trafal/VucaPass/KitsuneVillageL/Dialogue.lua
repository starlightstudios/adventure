-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[Name Resolve]|
    --Resolve the name of the calling entity.
    local sActorName = TA_GetProperty("Name")

    --Set facing.
    TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[Common]|
    local iKitsuneState = fnResolveKitsuneState()
    
    -- |[KitsuneA]|
    if(sActorName == "KitsuneA") then
        
        --Intro:
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] Don't go getting hurt again, Izuna!") ]])
    
    -- |[KitsuneB]|
    elseif(sActorName == "KitsuneB") then

        --Intro:
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Kitsune:[VOICE|KitsuneB] Please take care of Izuna for us.[P] She's a lot to handle but we love her dearly.") ]])
    
    -- |[Yukina]|
    elseif(sActorName == "Yukina") then

        --Variables
        local iEmpressJoined    = VM_GetVar("Root/Variables/Chapter2/MtSarulente/iEmpressJoined", "N")
        local iYukinaMetEmpress = VM_GetVar("Root/Variables/Chapter2/KitsuneVillage/iYukinaMetEmpress", "N")
        
        --Common.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Yukina", "Neutral") ]])
        
        --Meeting Empress:
        if(iEmpressJoined == 1.0 and iYukinaMetEmpress == 0.0) then
            
            --Flag.
            VM_SetVar("Root/Variables/Chapter2/KitsuneVillage/iYukinaMetEmpress", "N", 1.0)
            
            --Dialogue.
            fnCutscene([[ Append("Yukina:[E|Neutral] Now look what the fox dragged in.[P] Empress Arulente, conquerer of the world.[P] Are you travelling with these children?[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] Hello, Yukina.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Happy] You two know each other?[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] I have often conferred with the kitsune elders.[P] I did not know Yukina had ascended to that position.[B][C]") ]])
            fnCutscene([[ Append("Yukina:[E|Neutral] You don't look like you've aged a day.[P] Me, I cannot say the same.[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] Age has been kind to you.[P] Perhaps too kind.[P] Has it dulled your tongue?[B][C]") ]])
            fnCutscene([[ Append("Yukina:[E|Neutral] Very bold, Empress.[P] Bold indeed.[P] But there are children present.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Sad] What?[P] Zeke's a big boy![B][C]") ]])
            fnCutscene([[ Append("Zeke:[E|Neutral] Bah![B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] I suppose we should not get into it here.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Smirk] You knew she was still alive, elder?[P] She has agreed to help us on our quest![B][C]") ]])
            fnCutscene([[ Append("Yukina:[E|Neutral] Oh, I knew she was indeed alive.[P] However, she does enjoy her privacy.[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] I am often away from my home, touring the world.[P] Besides, people scarcely believe me even when I tell them.[B][C]") ]])
            fnCutscene([[ Append("Yukina:[E|Neutral] Those that have heard of Arulente, anyway.[P] How far is your name known?[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] Every corner of the world, Yukina.[P] My reach equals the Fox Goddess.[B][C]") ]])
            fnCutscene([[ Append("Yukina:[E|Neutral] A shame![B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Cry] Elder![P] Petty insults ought to be beneath you![B][C]") ]])
            fnCutscene([[ Append("Yukina:[E|Neutral] Let me just say I was not the elder when I met Empress, so these insults are part of my vintage reserve.[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] I am not interested in trading barbs with you.[B][C]") ]])
            fnCutscene([[ Append("Yukina:[E|Neutral] If you would just apologize, I would happily accept it.[P] But you have not come for that, have you?[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] ...[B][C]") ]])
            fnCutscene([[ Append("Yukina:[E|Neutral] You know your flaws but do not seek to change them.[P] If you are not here to apologize, what exactly is it you wanted?[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] The children seek your wisdom.[P] Is it not a point in my favour that I will not stand in their way?[B][C]") ]])
            fnCutscene([[ Append("Yukina:[E|Neutral] But you are so wise, Empress![P] You who have lived much longer than all of us combined.[P] Why stain your wisdom with mine?[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] ...[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] Because you were right.[P] There.[P] Are you happy?[P] You're right, Yukina.[P] Even I ought to seek your wisdom from time to time.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] *Hey Izuna, I think that was really important.*[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Neutral] *I guess they have a history...*[B][C]") ]])
            fnCutscene([[ Append("Yukina:[E|Neutral] You surprise even me.[P] I guess you can teach an old dragon new tricks.[P] Very well.[B][C]") ]])
            fnCutscene([[ Append("Yukina:[E|Neutral] So, young ones, what would you like to learn today?") ]])
            fnCutscene([[ WD_SetProperty("Activate Topics After Dialogue", "Yukina") ]])
            return
        end
    
        --Intro:
        fnCutscene([[ Append("Yukina:[E|Neutral] So, my aspiring heroes, what would you like to learn about today?[B][C]") ]])
        fnCutscene([[ WD_SetProperty("Activate Topics After Dialogue", "Yukina") ]])
    end
end
