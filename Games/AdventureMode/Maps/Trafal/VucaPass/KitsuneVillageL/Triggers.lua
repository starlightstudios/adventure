-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ====================================== Save Izuna! ======================================= ]|
if(sObjectName == "Save Izuna") then
    
    -- |[Repeat Check]|
    local iRescuedIzuna = VM_GetVar("Root/Variables/Chapter2/KitsuneVillage/iRescuedIzuna", "N")
    if(iRescuedIzuna == 1.0) then return end

    -- |[Flag]|
    VM_SetVar("Root/Variables/Chapter2/KitsuneVillage/iRescuedIzuna", "N", 1.0)
    AL_SetProperty("Music", "Null")
    
    -- |[Variables]|
    local iEmpressJoined = VM_GetVar("Root/Variables/Chapter2/MtSarulente/iEmpressJoined", "N")
    if(iEmpressJoined == 1.0) then
        VM_SetVar("Root/Variables/Chapter2/KitsuneVillage/iYukinaMetEmpress", "N", 1.0)
    end
    
    -- |[Re-Add Izuna]|
    --Put Izuna back in the party. She has to be in the second slot, so clear the party and reassemble it.
    if(iEmpressJoined == 0.0) then
        fnRemovePartyMember("Zeke", false)
        fnAddPartyMember("Izuna", false)
        fnAddPartyMember("Zeke", false)
    else
        fnRemovePartyMember("Empress", false)
        fnRemovePartyMember("Zeke", false)
        fnAddPartyMember("Izuna", false)
        fnAddPartyMember("Zeke", false)
        fnAddPartyMember("Empress", false)
    end
    
    -- |[Movement]|
    fnCutsceneMove("Sanya",  9.25, 17.50, 2.50)
    fnCutsceneFace("Sanya", 1, -1)
    fnCutsceneMove("Zeke",  10.25, 17.50, 2.50)
    fnCutsceneFace("Zeke", 1, -1)
    if(iEmpressJoined == 1.0) then
        fnCutsceneMove("Empress",   8.25, 17.50, 2.50)
        fnCutsceneFace("Empress", 1, -1)
    end
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Izuna![P] We're here to rescue you![B][C]") ]])
    fnCutscene([[ Append("Zeke:[VOICE|Zeke] Bah![B][C]") ]])
    fnCutscene([[ Append("Guard:[VOICE|KitsuneA] Hold it right there!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneMove("Yukina", 9.75, 13.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneFace("Sanya", 0, -1)
    fnCutsceneFace("Zeke", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Sanya", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Zeke", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Izuna", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Yukina", "Neutral") ]])
    if(iEmpressJoined == 1.0) then
        fnCutscene([[ WD_SetProperty("Actor In Slot", 6, "Empress", "Neutral") ]])
    end
    fnCutscene([[ Append("Sanya:[E|Offended] Let Izuna go![P] Don't make me smack you![B][C]") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] She is free to leave at any time.[B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] ...[P] Nyeh?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Offended] You won't -[P] what?[B][C]") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] She is free to leave at any time.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Sad] Then -[P] what was up with your guards being like 'Hold it right there!' and all?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] That is because of our custom.[P] See the steps there?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Sanya", 1, 0)
    fnCutsceneFace("Zeke", -1, 0)
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    fnCutsceneFace("Sanya", 0, -1)
    fnCutsceneFace("Zeke", 0, -1)
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Sanya", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Zeke", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Izuna", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Yukina", "Neutral") ]])
    if(iEmpressJoined == 1.0) then
        fnCutscene([[ WD_SetProperty("Actor In Slot", 6, "Empress", "Neutral") ]])
    end
    fnCutscene([[ Append("Sanya:[E|Neutral] Nine steps.[P] Nine tails.[P] You guys really keep to the theme don't you.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] You may ascend one step for each tail you have earned.[P] The floors don't count as steps, don't worry.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] But you may ascend to any step so long as you are doing so in service of the fox goddess.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] Good, I was about to ask how you clean the place up.[B][C]") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] And you, miss Sanya, are no doubt the one who Izuna says rescued her.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Offended] Yeah.[P] And I'm here to rescue her -[P] again![P] From you![B][C]") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] As I said, she is free to go.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Offended] I did hear that, but it just isn't jiving with the way things are going.[P] Izuna, why are you washing the floors, and why did the guards haul you off when we got here?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Ugh] It is my punishment.[B][C]") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] Which she is free to not do.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Sad] That isn't supposed to be how punishments work.[B][C]") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] She has wronged her fellow adherents, and must make amends.[P] Right now, she is cleaning the temple floors.[P] If she wants to be a miko in good standing with this temple, she must fulfill her punishment.[B][C]") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] Until she does, she is not in good standing.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Sad] Are we just going to skip over the part where she's being punished for apparently not dying?[B][C]") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] She is being punished for running off on a fool's errand without telling any of us where she was going![B][C]") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] And then she got lost and all of us were worried sick when we could not find her![P] We sent out search parties![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] And how come I didn't see any of these search parties?[B][C]") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] We did not know where to look.[P] She said you had found her at the Shrine of the Hero.[P] We checked there, on a hunch from Mia, but did not find her.[B][C]") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] However, we did not check the building you took her to.[P] We could not have known you would carry her so far, you see.[P] Trafal is a great place, and we have few to spare.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] Oh, I see.[P] So her punishment is for being a dope.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Cry] It was quite foolish of me to run off like that.[P] There is no excuse for it.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Happy] And she can leave whenever?[P] No hard feelings?[B][C]") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] She will not be allowed to participate in any of our ceremonies until she fulfills her punishment.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] So what you're really saying is...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Happy] WE DID IT ZEKE WE SAVED IZUNA![B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] BAHHH NYEH![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Happy] High five, buddy![P] We did it![B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Ugh] ...[B][C]") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] ...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] C'mon Izuna, let's blow this pop stand.[P] We got a world to save![B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Cry] Sanya, I must do this.[P] Don't you understand?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] I'm not saying you're not going to do it, I'm saying it will have to wait until we save the world.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] Hey octopus-butt - [P][CLEAR]") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] Octopus butt![P] Oh, oh, I love it![P] I love it![P] I've not heard that one before![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] I need to borrow Izuna for a while.[P] I'm the isekai hero or something.[P] Gotta go kill the great evil.[P] I'll bring her right back.[B][C]") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] As if anything I say could possibly dissuade you.[P] She can finish her penance later.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Happy] It's really all right?[B][C]") ]])
    if(iEmpressJoined == 0.0) then
        fnCutscene([[ Append("Yukina:[E|Neutral] Of course, dear.[P] Just please let us know if you decide to do something reckless again.[P] And Miss Sanya, I will be holding you responsible for her safety.") ]])
    else
        fnCutscene([[ Append("Yukina:[E|Neutral] Of course, dear.[P] Just please let us know if you decide to do something reckless again.[P] And Miss Sanya, I will be holding you responsible for her safety.[B][C]") ]])
        fnCutscene([[ Append("Yukina:[E|Neutral] That goes for you too, Empress.[P] Do you still go by that name?[P] You've been awfully quiet.[B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] I would say I was hoping you wouldn't notice me.[B][C]") ]])
        fnCutscene([[ Append("Yukina:[E|Neutral] But you're so tall.[B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] Very funny.[B][C]") ]])
        fnCutscene([[ Append("Yukina:[E|Neutral] Have you been doing well?[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Happy] Do you two know each other?[B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] Last I was here, Yukina was not the temple's elder.[P] Congratulations.[B][C]") ]])
        fnCutscene([[ Append("Yukina:[E|Neutral] Perhaps if you sent a postcard.[P] I've seen your underlings borrowing books on your behalf.[B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] Yes.[P] If I had known you did not hold a grudge, perhaps I would come in person.[B][C]") ]])
        fnCutscene([[ Append("Yukina:[E|Neutral] I don't.[P] If you ask for forgiveness, I will happily give it.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Happy] This is starting to sound spicy![B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] Correct, which is why we will not be relitigating it in front of the children.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Sad] Come on, Zeke's a big boy.[P] He can handle it.[B][C]") ]])
        fnCutscene([[ Append("Zeke:[E|Neutral] Bah![P] Bah![B][C]") ]])
        fnCutscene([[ Append("Yukina:[E|Neutral] Stubborn as always.[P] Not like I can talk.[P] Very well.[P] As long as you keep them safe, I suppose I can't object.") ]])
    end
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneFace("Sanya", 0, -1)
    fnCutsceneFace("Zeke",  0, -1)
    fnCutsceneMove("Izuna", 14.25, 13.50)
    fnCutsceneMove("Izuna", 11.25, 13.50)
    fnCutsceneMove("Izuna", 11.25, 17.50)
    fnCutsceneFace("Izuna",  0, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Sanya", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Zeke", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Izuna", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Yukina", "Neutral") ]])
    if(iEmpressJoined == 1.0) then
        fnCutscene([[ WD_SetProperty("Actor In Slot", 6, "Empress", "Neutral") ]])
    end
    fnCutscene([[ Append("Sanya:[E|Smirk] Come on, let's get going.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] Wait, Sanya.[P] We really should consult Elder Yukina before we go.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] She is the wisest of the kitsunes.[P] We could benefit from her wisdom![B][C]") ]])
    if(iEmpressJoined == 1.0) then
        fnCutscene([[ Append("Empress:[E|Neutral] Is she now?[B][C]") ]])
        fnCutscene([[ Append("Yukina:[E|Neutral] Oh hush, you.[P] You've filled these children's heads with rot.[P] What good will my wisdom be if the wisest kitsune councils against it, but they proceed anyway?[B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] So you are aware, it was they who came to me convinced of this destiny.[B][C]") ]])
    else
        fnCutscene([[ Append("Sanya:[E|Smirk] Yeah, that's the reason we came here in the first place.[B][C]") ]])
        fnCutscene([[ Append("Yukina:[E|Neutral] Pah.[P] The wisest of the kitsunes is telling you not to do this, but you are young and headstrong.[P] What good will my wisdom be?[B][C]") ]])
    end
    fnCutscene([[ Append("Sanya:[E|Surprised] Hey what, hold on.[P] I'm the destined hero Sanya.[P] Right?[P] Izuna, you said I was![B][C]") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] You are not.[P] Please understand, Izuna says it and she believes it wholly.[P] But she is wrong.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Ugh] Maybe we should just go...[B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] ...[P] Nyeh...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Offended] Oh no, no no no, now we're staying![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Offended] There is a prophecy that says some big evil dude named Perfect Hatred is going to kill everyone unless a great, powerful warrior named Sanya, from another world, stops him, right?[B][C]") ]])
    if(iEmpressJoined == 1.0) then
        fnCutscene([[ Append("Empress:[E|Neutral] That is the overall sense of it.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] Good because I am really winging it here.[B][C]") ]])
    else
        fnCutscene([[ Append("Yukina:[E|Neutral] That is the gist of it, yes.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] Just making sure I got the prophecy right.[P] I'm not from around here.[B][C]") ]])
    end
    fnCutscene([[ Append("Sanya:[E|Happy] And this hero's name is Sanya.[P] My name's Sanya![P] Bam![B][C]") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] Indeed?[P] Four of the kitsune here are named Sanya.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Happy] What!?[P] Actually, good name.[P] Good names, everyone.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Explain] It's actually one of the most common names in Trafal.[B][C]") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] It means one is strong.[P] To be named after the prophesied hero means a mother thinks their daughter will be mighty.[B][C]") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] And what mother doesn't think so?[P] Even some of the boys are named Solon, the masculine version![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Okay fine, the name, whatever.[P] What about my runestone?[B][C]") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] Probably the most common symbol in Trafal![P] Everyone has seen it, it is on everything.[B][C]") ]])
    if(iEmpressJoined == 1.0) then
        fnCutscene([[ Append("Empress:[E|Neutral] There are tapestries and banners in Poterup village with the symbol on it.[P] It is also a letter in the old draconic written language.[B][C]") ]])
        fnCutscene([[ Append("Yukina:[E|Neutral] Right.[P] The imperial highway signs have the symbol on them.[P] Everyone has seen it.[B][C]") ]])
    end
    fnCutscene([[ Append("Yukina:[E|Neutral] Yours is no doubt a very well made piece of stone, but it is a piece of stone.[P] There are hundreds just like it.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Happy] What about my incredible strength!?[P] Izuna, you told them about that, right?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] Yes, but [P][CLEAR]") ]])
    if(iEmpressJoined == 1.0) then
        fnCutscene([[ Append("Yukina:[E|Neutral] I have met women mightier than you.[P] One is standing next to you.[B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] I appreciate the flattery, but I think with a little training, Sanya is the stronger of the two of us.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Happy] I can punch a boulder into shards![P] My dad yelled at me not to![P] My brothers couldn't stop cheering, it was awesome![B][C]") ]])
        fnCutscene([[ Append("Yukina:[E|Neutral] Anecodotes aside, the fact of your strength is still not that impressive. [B][C]") ]])
    else
        fnCutscene([[ Append("Yukina:[E|Neutral] I have met women mightier than you.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Happy] I can punch a boulder into shards![B][C]") ]])
        fnCutscene([[ Append("Yukina:[E|Neutral] Indeed.[P] So can those others I met.[P] In fact, one in particular would put whatever feats you have performed to shame.[B][C]") ]])
    end
    fnCutscene([[ Append("Sanya:[E|Offended] Grrr...[P] I can transform![B][C]") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] Spare me.[P] Illusion magic.[B][C]") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] I have seen hundreds of pretenders who could also change their forms.[P] I don't know what spells or tricks they did, but none of them were the great hero, were they?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Sad] You're telling me other people can transform?[B][C]") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] No, in fact I am telling you the opposite.[P] None of them could, but they could fake it.[P] And whatever technique you have will not fool me.[B][C]") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] I am sorry to tell you this, but what Izuna told you is a prophecy.[P] A fairy tale.[P] A story.[P] It is not the truth.[B][C]") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] She believes it very sincerely, and there are many here in Trafal who do as well.[P] But that does not make that belief true.[B][C]") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] That a great hero would emerge to save the world is a prophecy told by a dying empire that lionized the strength and power of one solitary individual above all others.[P] That empire compressed its ideology into a story about its own rebirth.[B][C]") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] Since then, dozens of warlords and brigands have declared themselves the inheritors of that empire's legacy.[P] Conveniently, all could 'transform' and perform feats of strength.[P] They rallied against some evil, real or imagined, to burnish their legitimacy.[B][C]") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] If you do not know the history of this place, as you surely do not, Miss Sanya, then you do not realize that your beliefs make you a slave to a long-dead philosopher.[B][C]") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] That is my wisdom that I will impart to you.[P] Abandon this errand.[P] Confirm for yourself what is happening in the world and seek to change it, but do not rely on a fairy tale.[B][C]") ]])
    if(iEmpressJoined == 1.0) then
        fnCutscene([[ Append("Sanya:[E|Sad] But the person who created that fairy tale is...[B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] Perhaps now you understand better why I was not eager to reconnect with elder Yukina.[B][C]") ]])
        fnCutscene([[ Append("Yukina:[E|Neutral] Your ego does not even avail you of a simple apology, Empress.[B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] ...[B][C]") ]])
        fnCutscene([[ Append("Yukina:[E|Neutral] You know of your flaw but you neither answer it nor correct it.[P] Pathetic.[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Cry] Can we not fight?[P] Please[P] Elder, I do not know what your past here is, but insults are beneath you.[B][C]") ]])
        fnCutscene([[ Append("Yukina:[E|Neutral] So they are, my child.[P] I think I'm getting bitter in my old age.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Sad] But is what she said true, Empress?[B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] It is.[P] There have been many pretenders.[P] It is true my empire lionized power and seeks revenge for past wrongs.[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Explain] But just because she's seen so many pretenders doesn't mean you aren't the real deal![B][C]") ]])
    else
        fnCutscene([[ Append("Sanya:[E|Sad] Izuna, is all this true?[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Neutral] Of course it is.[P] Everything she says is right, according to the history books.[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Explain] But just because she's seen so many pretenders doesn't mean you aren't the real deal![B][C]") ]])
    end
    fnCutscene([[ Append("Sanya:[E|Sad] Now I don't know what to think.[P] I assume if I told you I was from Earth then you'd dismiss that too?[B][C]") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] I am very sorry, but how could I possibly confirm that?[P] From my perspective, you could simply be from a faraway place.[P] The world is big.[B][C]") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] I do not want to call you a liar, but what difference is there between lies and truth if neither can be confirmed?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Sad] Well then...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] How about this.[P] Let's say hypothetically that all of these prophecies were true, and I was the chosen hero.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] Hypothetically, what advice would you give me then?[B][C]") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] Mwehehehe![P] I do like you, Miss Sanya.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Happy] Right?[P] Right![P] She's the best![B][C]") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] My advice to you in this hypothetical alternate reality would be to seek out the Trials of the Dragon.[P] There are three such trials.[B][C]") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] They were built by the empire that made that prophecy, you see, and nobody has ever completed them.[P] In seven centuries.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Really?[P] In 700 years nobody completed them?[B][C]") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] It seems like something that would be in a history book if they did.[P] Certainly nobody alive has made a claim on it.[B][C]") ]])
    if(iEmpressJoined == 1.0) then
        fnCutscene([[ Append("Empress:[E|Neutral] The Sevavi overseeing the trials are to tell me if someone has completed one.[B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] And yes, I inspect the trials periodically myself.[P] I can confirm that much.[B][C]") ]])
    end
    fnCutscene([[ Append("Yukina:[E|Neutral] The trials contain artifacts of great power, made for the prophesied hero.[P] So if someone had completed them, they would have those artifacts.[P] And we've never seen them.[B][C]") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] If you were to complete the trials, well, I would have to change my opinion about you.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Sad] But what about Perfect Hatred?[P] Izuna said his seal was breaking.[B][C]") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] If a scourge were plaguing the land and wiping out its inhabitants, as the histories said the last time that creature walked the earth, then that, too, would be something we know about.[B][C]") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] And if, in this alternate reality, you were the hero, you would need the artifacts left for you.[P] To help you destroy Perfect Hatred, that is why they were made.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] That's true.[P] They're supposed to be incredible magic armors made to protect you, Sanya.[P] We could really use them![B][C]") ]])
    if(iEmpressJoined == 1.0) then
        fnCutscene([[ Append("Empress:[E|Neutral] They are some of the finest pieces ever made.[B][C]") ]])
    end
    fnCutscene([[ Append("Yukina:[E|Neutral] That is my advice to your alternate self, Sanya.[P] My advice to your real self is that this is a waste of time.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] I get you.[P] Thanks for your time, elder.[B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] Nyeh.[B][C]") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] And do drop by to let me know how this goes, hypothetically.[P] If you can complete even one trial, I should like to put it in our histories.[B][C]") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] I will allow you to walk up the steps here to seek my wisdom.[P] If you really are on this quest, then you are in service to the Fox Goddess.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] I'll keep it in mind.[P] C'mon, Izuna.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] I'll be back later, elder![P] I promise I'm not shirking!") ]])
    fnCutsceneBlocker()

    -- |[Fold]|
    fnAutoFoldParty()
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Music", "KitsuneVillage") ]])

end
