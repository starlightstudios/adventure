-- |[ =============================== Pamphlet: Green Westwoods =============================== ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[KitsuneA]|
if(sActorName == "KitsuneA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] I hope those alraunes have great success.[P] The Fox Goddess will watch over them.") ]])
    
-- |[KitsuneB]|
elseif(sActorName == "KitsuneB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneB] I believe the Fox Goddess helps the spirits of nature just like she guides the kitsunes.[P] I know they'll succeed.") ]])
    
-- |[Yukina]|
elseif(sActorName == "Yukina") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Yukina", "Neutral") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] Ah, your pamphlet on the alraunes, yes?[P] I'm sorry, my sight isn't quite what it used to be.[B][C]") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] I'll be bringing this up at the next council of elders, Izuna.[P] We have other problems, but we can at least symbolically show support here.[B][C]") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] You've made me proud.[P] You're truly doing the will of the Fox Goddess by finding this out.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Happy] Thank you so much, elder![B][C]") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] And you, Sanya.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Hey, I didn't do anything.[B][C]") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] Westwoods is very dangerous.[P] You've kept Izuna safe, and made this story a possibility.[P] You deserve some credit, too.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] Ah, it was nothing.[B][C]") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] Keep up the good work, children.") ]])
end

-- |[Clean Up]|
DL_PopActiveObject()
