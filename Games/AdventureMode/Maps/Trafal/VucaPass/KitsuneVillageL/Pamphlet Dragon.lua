-- |[ ==================================== Pamphlet: Dragon ==================================== ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[KitsuneA]|
if(sActorName == "KitsuneA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] I'm skeptical of this, but I hope you succeed, Izuna.") ]])
    
-- |[KitsuneB]|
elseif(sActorName == "KitsuneB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneB] I swear I've seen the -[P] oh, of course.[P] Your face is on the signposts on the imperial highway!") ]])
    
-- |[Yukina]|
elseif(sActorName == "Yukina") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Yukina", "Neutral") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] Oh, yes.[P] I read this pamphlet.[P] You already know my feelings on it.[B][C]") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] I suppose there is nothing untrue in it, but you should recognize that the people who read it are steeped in the old legend.[B][C]") ]])
    fnCutscene([[ Append("Yukina:[E|Neutral] You should publish follow-ups detailing your adventures.[P] They deserve to know the full context of their superstitions.") ]])
end

-- |[Clean Up]|
DL_PopActiveObject()
