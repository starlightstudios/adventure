-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
-- |[ =================================== Izuna Explanation ==================================== ]|
if(sObjectName == "IzunaExplain") then

    -- |[Repeat Check]|
    local iExplainedVucaPass = VM_GetVar("Root/Variables/Chapter2/IzunaExplains/iExplainedVucaPass", "N")
    if(iExplainedVucaPass == 1.0) then return end
    
    -- |[Activation Check]|
    local iCompletedHunt = VM_GetVar("Root/Variables/Chapter2/Northwoods/iCompletedHunt", "N")
    if(iCompletedHunt == 0.0) then return end
    
    -- |[Variables]|
    local iEmpressJoined = VM_GetVar("Root/Variables/Chapter2/MtSarulente/iEmpressJoined", "N")
    
    -- |[Flag]|
    VM_SetVar("Root/Variables/Chapter2/IzunaExplains/iExplainedVucaPass", "N", 1.0)
    
    -- |[Execution]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("Izuna:[E|Explain] This is Vuca Pass![P] It's where my temple is located![B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Explain] It's just south of here, then you take a turn at the bridge.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] Is it always snowy?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] It takes a long time for snow to melt this high up, but it does eventually melt out in the full swing of summer.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] Sometimes we get a storm in late spring and enjoy a fresh layer of snow.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Happy] Just like home, then![B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] Nyeh.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] So why's it called Vuca Pass?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Explain] General Morgarde Vuca was one of the Arulente empire's greatest generals.[P] She is the one who captured the port cities in the north and established the overseas trade routes that brought a lot of wealth to Trafal.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] If you hear something named anywhere on this continent, chances are it was named that as a reward to someone for their service to the empire.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Conquer some cities and you get a pass named after you?[P] What's it take to get a province named after you?[B][C]") ]])
    if(iEmpressJoined == 1.0) then
        fnCutscene([[ Append("Empress:[E|Neutral] Ally with the empire, rather than be subjugated, and your homeland will be named for you.[B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] Raid us, and we erased you.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] Sounds kinda brutal, Empress.[B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] I was trying to unite the continent in peace and harmony.[P] The people who got in the way were not using violence from desperation, but from habit.[B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] Many petty tyrants use force to bully their own people, and raid their neighbours.[P] Believe me, whatever resolution you are thinking I should have tried, I did.[B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] If they rejected peaceful envoys, so be it.[P] If they wished to remain neutral, they could.[P] I would try again with their grandchildren.[B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] But if they attacked our merchants, they died.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Sad] Guess that answers my question.[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Neutral] But what of the history of the peoples who did ally?[B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] Lost, unfortunately.[P] Along the coasts, people had written language.[P] Much of the interior was oral history.[P] It is lost to time.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Smirk] Final question.[P] What are the chances of 'Republic of Zeke'?[B][C]") ]])
        fnCutscene([[ Append("Zeke:[E|Neutral] Nyeh![B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] With his boundless charisma?[P] Very good.") ]])
    else
        fnCutscene([[ Append("Izuna:[E|Smirk] Ooh, good question.[P] I think the provinces are named after the native peoples who allied with the empire as it conquered the continent.[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Ugh] Unfortunately, not a lot of their history remains.[P] Writing wasn't very common before the empire standardized the practice of education, and their cultures got absorbed into the Trafalian culture.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Sad] Sorry, Zeke.[P] Guess we'll have to put The Republic of Zeke on the back burner.[B][C]") ]])
        fnCutscene([[ Append("Zeke:[E|Neutral] .......[P] Bah.") ]])
    end
    fnCutsceneBlocker()

end
