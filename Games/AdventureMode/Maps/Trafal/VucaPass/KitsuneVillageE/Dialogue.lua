-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[Name Resolve]|
    --Resolve the name of the calling entity.
    local sActorName = TA_GetProperty("Name")

    --Set facing.
    TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[ =============================== Mia ============================== ]|
    --Izuna's editor.
    if(sActorName == "Mia") then
        
        -- |[Variables]|
        local sSanyaForm       = VM_GetVar("Root/Variables/Global/Sanya/sForm", "S")
        local iMetMia          = VM_GetVar("Root/Variables/Chapter2/Mia/iMetMia", "N")
        local iMetMiaWithIzuna = VM_GetVar("Root/Variables/Chapter2/Mia/iMetMiaWithIzuna", "N")
        local iMiaIsMad        = VM_GetVar("Root/Variables/Chapter2/Mia/iMiaIsMad", "N")
        local iEmpressJoined   = VM_GetVar("Root/Variables/Chapter2/MtSarulente/iEmpressJoined", "N")
        local bIsIzunaPresent  = fnIsCharacterPresent("Izuna")
        
        -- |[Common]|
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Mia", "Neutral") ]])
        
        -- |[ ========================= Mia is Mad! ======================== ]|
        if(iMiaIsMad == 1.0) then
            
            -- |[Izuna is Not Present]|
            if(bIsIzunaPresent == false) then
                fnCutscene([[ Append("Mia:[E|Neutral] Sanya![P] Stop dawdling and bring Izuna here!") ]])
            
            -- |[Izuna is Present]|
            else
                VM_SetVar("Root/Variables/Chapter2/Mia/iMetMiaWithIzuna", "N", 1.0)
                VM_SetVar("Root/Variables/Chapter2/Mia/iMiaIsMad", "N", 0.0)
                fnCutscene([[ Append("Mia:[E|Neutral] IZUNA![B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Cry] Oh no, what did I do?[P] Please don't be mad![P] I'm sorry![B][C]") ]])
                fnCutscene([[ Append("Mia:[E|Neutral] You do NOT get to pretend as though everything is okay just because you were screamin' yellow right about everything![B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Neutral] What?[B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Surprised] I don't know, don't look at me![B][C]") ]])
                fnCutscene([[ Append("Zeke:[E|Neutral] Bah![B][C]") ]])
                fnCutscene([[ Append("Mia:[E|Neutral] Sanya came in here and, would you look at that, she's a shapeshifting superstrong person who fits the description of the legendary hero![B][C]") ]])
                fnCutscene([[ Append("Mia:[E|Neutral] But you running off without telling me where you were going and me thinking you were dead for a month does NOT make it okay![B][C]") ]])
                fnCutscene([[ Append("Mia:[E|Neutral] You were right but you're still an idiot![B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Neutral] Oh don't worry about that.[P] Still an idiot over here.[B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Smirk] So you believe me?[B][C]") ]])
                fnCutscene([[ Append("Mia:[E|Neutral] I saw it for myself.[P] Sanya can transform.[B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Neutral] Yukina didn't believe me.[B][C]") ]])
                fnCutscene([[ Append("Mia:[E|Neutral] The elder has been wrong before, you know.[P] And I doubt she truly opposes you, she just needs more proof.[B][C]") ]])
                fnCutscene([[ Append("Mia:[E|Neutral] It's up to people like Izuna to believe in you even without proof so you have the chance to get some.[B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Happy] Great![B][C]") ]])
                fnCutscene([[ Append("Mia:[E|Neutral] Now if I were you, first thing I'd do is go handle the Trials of the Dragon.[P] If you can pull those off, I'm sure everyone in Trafal would throw their support behind you.[B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Smirk] The elder said the same thing.[B][C]") ]])
                fnCutscene([[ Append("Mia:[E|Neutral] Because she was not-so-subtly telling you what you'd need to do to prove you're this great hero.[P] And if there really is an ancient evil stirring, you could use those magical artifacts.[B][C]") ]])
                fnCutscene([[ Append("Mia:[E|Neutral] The old fox is cagey, but she's not misleading you.[B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Smirk] Okay, so we just need to go to the trials.[P] That shouldn't be a problem, right?[B][C]") ]])
                fnCutscene([[ Append("Mia:[E|Neutral] Not if you're the real deal.[B][C]") ]])
                fnCutscene([[ Append("Mia:[E|Neutral] But just because you're cavorting with the great hero doesn't mean you're off the hook, Izuna.[B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Ugh] I know.[P] I have to complete my punishment when I'm done helping Sanya.[B][C]") ]])
                fnCutscene([[ Append("Mia:[E|Neutral] Not that, you dunce![P] You still have to write articles for me to publish![B][C]") ]])
                fnCutscene([[ Append("Mia:[E|Neutral] You're out there plumbing depths and discovering lost secrets, you better believe I have to tell the world about it.[B][C]") ]])
                fnCutscene([[ Append("Mia:[E|Neutral] Whenever you've got a scoop, come back and I'll get it on the next pamphlets going out.[B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Smirk] I knew we could count on you![B][C]") ]])
                fnCutscene([[ Append("Mia:[E|Neutral] And I'll keep my ear to the ground for you.[P] There's going to be rumours going around if this big evil is on the rise.[B][C]") ]])
                fnCutscene([[ Append("Mia:[E|Neutral] If I get any leads, I'll be sure to pass them on to you.[B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Neutral] Thanks, Mia.") ]])
            end
        
        -- |[ ======================= First Meeting ======================== ]|
        elseif(iMetMia == 0.0) then
            
            -- |[Flags]|
            VM_SetVar("Root/Variables/Chapter2/Mia/iMetMia", "N", 1.0)
            VM_SetVar("Root/Variables/Chapter2/Mia/sFormMetMiaIn", "S", sSanyaForm)
            
            -- |[Izuna Not Present]|
            --Can only happen during the intro to the village.
            if(bIsIzunaPresent == false) then
            
                --Human:
                if(sSanyaForm == "Human") then
                    fnCutscene([[ Append("Mia:[E|Neutral] Oh, hello there.[P] Are you a visitor?[P] My name is Mia, and I'm the editor of the Nine-Tails Post.[B][C]") ]])
                    fnCutscene([[ Append("Sanya:[E|Smirk] Editor, huh?[P] You publish news stories?[B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] Of course.[P] Well, I did.[P] My reporter went missing some time ago.[B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] I publish what I can based on what I can dig up myself, but it's not my forte.[B][C]") ]])
                    fnCutscene([[ Append("Sanya:[E|Smirk] Your reporter went missing?[P] White hair, really cute, incredible body?[B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] Uh...[P] yes?[P] She has white hair.[P] Have you seen Izuna?[B][C]") ]])
                    fnCutscene([[ Append("Sanya:[E|Happy] I saved her when she fell into a river and nursed her back to health.[B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] She's back!?[P] She's alive!?!?[B][C]") ]])
                    fnCutscene([[ Append("Sanya:[E|Neutral] You hadn't heard?[B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] Well I would probably know what was going on in town if [P]*my reporter was here*![B][C]") ]])
                    fnCutscene([[ Append("Sanya:[E|Neutral] The guards hauled her off.[P] Dunno why.[B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] Oh I know exactly why![P] She'll be at the temple with the elder, north side of town.[P] You can't miss it.[B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] I -[P] well I suppose the elder will give her what's coming to her, so there's no need for me to double up.[B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] And you are, miss..?[B][C]") ]])
                    fnCutscene([[ Append("Sanya:[E|Smirk] Sanya, and this is Zeke.[P] He helped, too.[B][C]") ]])
                    fnCutscene([[ Append("Zeke:[E|Neutral] Bah![B][C]") ]])
                    if(iEmpressJoined == 1.0) then
                        fnCutscene([[ Append("Sanya:[E|Smirk] The big one in the back who isn't saying anything is Empress.[B][C]") ]])
                        fnCutscene([[ Append("Empress:[E|Neutral] Hello.[B][C]") ]])
                        fnCutscene([[ Append("Mia:[E|Neutral] Wow...[P] she's really tall...[B][C]") ]])
                        fnCutscene([[ Append("Sanya:[E|Offended] Scares the crap out of me![P] I love having her around, every minute is a thrill.[B][C]") ]])
                        fnCutscene([[ Append("Empress:[E|Neutral] ...[P] And she complains I don't speak.[B][C]") ]])
                        fnCutscene([[ Append("Mia:[E|Neutral] Miss Sanya, Empress, and mister Zeke, thank you very much for helping Izuna.[P] She really means well, but she's a handful.[B][C]") ]])
                        fnCutscene([[ Append("Mia:[E|Neutral] You should probably go and speak to the elder.[P] Thank you again.") ]])
                    else
                        fnCutscene([[ Append("Mia:[E|Neutral] Miss Sanya and mister Zeke, thank you very much for helping Izuna.[P] She really means well, but she's a handful.[B][C]") ]])
                        fnCutscene([[ Append("Mia:[E|Neutral] You should probably go and speak to the elder.[P] Thank you again.") ]])
                    end
                
                --Sevavi:
                elseif(sSanyaForm == "Sevavi") then
                    fnCutscene([[ Append("Mia:[E|Neutral] Oh, hello there.[P] My name is Mia, and I'm the editor of the Nine-Tails Post.[P] Were you here to borrow another book?[B][C]") ]])
                    fnCutscene([[ Append("Sanya:[E|Neutral] Books?[P] Mia, I'm afraid you could not be more wrong about me.[B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] I'm sorry.[P] I assumed you were a courier.[P] One of your colleagues comes by to borrow books on occasion.[B][C]") ]])
                    fnCutscene([[ Append("Sanya:[E|Blush] My colleagues?[P] (Does she mean my boobs?)[B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] (This one is even dumber than the others...)[B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] Yes, the other Sevavi.[P] You know, the organization whose uniform you are wearing, and whose stone-hewn bodies you have?[B][C]") ]])
                    fnCutscene([[ Append("Sanya:[E|Smirk] Well, that's a pretty nifty name![P] I just figured I was a statue.[P] Sevavi, huh.[B][C]") ]])
                    if(iEmpressJoined == 1.0) then
                        fnCutscene([[ Append("Empress:[E|Neutral] I have used the term multiple times and I do not think it has penetrated her head.[B][C]") ]])
                    end
                    fnCutscene([[ Append("Sanya:[E|Neutral] Nah, I'm just in town because Izuna said this should be our first stop.[B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] IZUNA?[B][C]") ]])
                    fnCutscene([[ Append("Sanya:[E|Happy] Yeah, you know her?[B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] She's my -[P] *reporter*![P] For the news pamphlets we publish![P] She went missing![P] I thought she was dead![B][C]") ]])
                    fnCutscene([[ Append("Sanya:[E|Neutral] Nah.[P] Zeke and I rescued her, nurtured her, basically saved the day.[B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] *Mwah*[B][C]") ]])
                    fnCutscene([[ Append("Sanya:[E|Blush] Woah![B][C]") ]])
                    fnCutscene([[ Append("Zeke:[E|Neutral] Bah![B][C]") ]])
                    if(iEmpressJoined == 1.0) then
                        fnCutscene([[ Append("Empress:[E|Neutral] ...[B][C]") ]])
                    end
                    fnCutscene([[ Append("Sanya:[E|Blush] Thanks for the kiss, lady, but how come Zeke doesn't get one?[B][C]") ]])
                    fnCutscene([[ Append("Zeke:[E|Neutral] BAH![B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] I -[P] will he settle for a pat on the head?[B][C]") ]])
                    fnCutscene([[ Append("Sanya:[E|Smirk] Take what you can get, Zeke.[B][C]") ]])
                    fnCutscene([[ Append("Zeke:[E|Neutral] .........................[P] nyeh.[B][C]") ]])
                    fnCutscene([[ Append("Sanya:[E|Neutral] So what was that about?[B][C]") ]])
                    if(iEmpressJoined == 1.0) then
                        fnCutscene([[ Append("Empress:[E|Neutral] You saved Izuna's life.[P] Is that not an important action where you come from?[B][C]") ]])
                        fnCutscene([[ Append("Sanya:[E|Neutral] Sort of.[P] It's about on par with getting wasted together.[B][C]") ]])
                        fnCutscene([[ Append("Sanya:[E|Neutral] And just like getting wasted together, she's my girlfriend now and we're totally in love.[B][C]") ]])
                    else
                        fnCutscene([[ Append("Mia:[E|Neutral] Oh gee, I don't know, you saved my best friend's life?[P] Thank you thank you thank you?[B][C]") ]])
                        fnCutscene([[ Append("Sanya:[E|Smirk] It's no big deal.[P] I had to punch a guy to do it and she, like, wants to be my girlfriend.[P] It worked out really well.[B][C]") ]])
                    end
                    fnCutscene([[ Append("Mia:[E|Neutral] I can absolutely see that.[P] She likes strong people.[P] She definitely has a type.[B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] Well, I suppose the elder will likely be giving her what-for.[P] You'll need to go smooth things out.[B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] I expect she'll be in the temple, up north.[P] Don't let me keep you any longer.") ]])

                --Werebat:
                elseif(sSanyaForm == "Werebat") then
                    fnCutscene([[ Append("Mia:[E|Neutral] Oh, hello there.[P] My name is Mia, and I'm the editor of the Nine-Tails Post.[P] Don't get a lot of werebats in here.[B][C]") ]])
                    fnCutscene([[ Append("Sanya:[E|Smirk] What do you get a lot of?[B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] Kitsunes.[P] Which follows because most of the people in this village are kitsunes.[B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] But even then, typically just the two of us.[B][C]") ]])
                    fnCutscene([[ Append("Sanya:[E|Neutral] I only count one of you.[B][C]") ]])
                    fnCutscene([[ Append("Zeke:[E|Neutral] Bah.[B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] Yeah, because the other one went missing.[P] Say, you werebats have exemplary senses.[P] Maybe you could help find her?[B][C]") ]])
                    fnCutscene([[ Append("Sanya:[E|Neutral] Sure.[P] Where'd she go missing?[B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] We're not sure, but I have a hunch she was going to visit the Shrine of the Hero.[P] It's north of here.[B][C]") ]])
                    fnCutscene([[ Append("Sanya:[E|Neutral] What's she look like?[B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] White hair, one tail, yellow eyes.[P] She took her blue robe so she'll probably still be wearing that.[P] They're very durable clothes.[B][C]") ]])
                    fnCutscene([[ Append("Sanya:[E|Neutral] Any other distinguishing characteristics?[B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] She's very sweet, but tends to act before her brain catches up.[P] Reads everything she can get her hands on.[P] Good head for details, remembers things without even needing to write them down, for years![B][C]") ]])
                    fnCutscene([[ Append("Sanya:[E|Neutral] Does she have a name?[B][C]") ]])
                    if(iEmpressJoined == 1.0) then
                        fnCutscene([[ Append("Empress:[E|Neutral] Izuna, if I don't miss my guess.[B][C]") ]])
                        fnCutscene([[ Append("Mia:[E|Neutral] That's right, how'd you know!?[B][C]") ]])
                    else
                        fnCutscene([[ Append("Mia:[E|Neutral] Izuna.[B][C]") ]])
                    end
                    fnCutscene([[ Append("Sanya:[E|Smirk] Hey, Zeke.[B][C]") ]])
                    fnCutscene([[ Append("Zeke:[E|Neutral] Bah?[B][C]") ]])
                    fnCutscene([[ Append("Sanya:[E|Smirk] Remember that white-haired yellow-eyed fox girl with one tail in a blue robe we fished out of a river near the Shrine of the Hero?[B][C]") ]])
                    fnCutscene([[ Append("Sanya:[E|Smirk] And then nursed back to health for a month, before coming here to seek the advice of the elder?[B][C]") ]])
                    fnCutscene([[ Append("Sanya:[E|Neutral] What was her name?[B][C]") ]])
                    fnCutscene([[ Append("Zeke:[E|Neutral] ...[P] Nyeh.[B][C]") ]])
                    fnCutscene([[ Append("Sanya:[E|Neutral] Yeah me too, I'm totally blanking here.[B][C]") ]])
                    if(iEmpressJoined == 1.0) then
                        fnCutscene([[ Append("Empress:[E|Neutral] I can never tell when these children are joking.[B][C]") ]])
                    end
                    fnCutscene([[ Append("Mia:[E|Neutral] Y-[P]you...[B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] *Mwah*[P] Oh my goodness, thank you so much miss werebat![B][C]") ]])
                    fnCutscene([[ Append("Sanya:[E|Blush] Sweet![P] I got a kiss for no reason![P] Does Zeke get one?[B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] A few headpats will have to suffice.[P] Where did you take her?[P] We were looking all over for her![B][C]") ]])
                    fnCutscene([[ Append("Sanya:[E|Happy] Right, Izuna![P] That was her name![B][C]") ]])
                    fnCutscene([[ Append("Zeke:[E|Neutral] Bah bahhhhhh![B][C]") ]])
                    fnCutscene([[ Append("Sanya:[E|Smirk] Yeah she fell into the river under the shrine.[P] We took her to a cabin near a mine to find her a place to recover.[B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] Well, that's why we didn't find her.[P] You took her halfway across the glacier.[P] Very impressive![B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] I can't thank you enough.[P] On behalf of everyone here, thank you![B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] Oh but I'm sure the elder is going to give her a real talking-to.[P] She ran off without telling us where she was going.[B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] You'll probably find her at the temple.[P] Go bring her back here, I have a piece of my mind to give her, too!") ]])
                
                --Harpy:
                elseif(sSanyaForm == "Harpy") then
                    fnCutscene([[ Append("Mia:[E|Neutral] Oh, hello there.[P] My name is Mia, and I'm the editor of the Nine-Tails Post.[P] I'm surprised the guards let you in.[B][C]") ]])
                    fnCutscene([[ Append("Sanya:[E|Smirk] Why's that?[P] They afraid I'm too good looking?[B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] Because we don't usually allow outsiders in, unless they're pilgrims.[P] Are you a pilgrim?[B][C]") ]])
                    fnCutscene([[ Append("Sanya:[E|Smirk] Probably not.[P] Fifty-fifty that Zeke is, though.[B][C]") ]])
                    fnCutscene([[ Append("Zeke:[E|Neutral] Bah.[B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] If you're not a pilgrim, and you're not a kitsune, why are you here, harpy?[B][C]") ]])
                    fnCutscene([[ Append("Sanya:[E|Neutral] My friend said we should visit the kitsune temple.[P] She's a kitsune.[B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] Oh?[P] Do I know her?[B][C]") ]])
                    fnCutscene([[ Append("Sanya:[E|Neutral] Yeah.[P] Her name's Izuna.[P] I found her in a river and nursed her back to health.[P] She's really hot.[B][C]") ]])
                    fnCutscene([[ Append("Sanya:[E|Neutral] We're kinda dating.[P] Also I have to save the world and stuff.[B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] You -[P] you -[P] oh, thank you so much![B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] *kiss*[B][C]") ]])
                    fnCutscene([[ Append("Sanya:[E|Blush] Do you kiss all the strapping soldiers who come through here?[B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] Izuna is my reporter![P] We've been friends since -[P] forever![P] You saved her life![B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] Ugh![P] She ran off without telling anyone where she was going.[P] I'm sure she's at the temple right now, getting yelled at by the elder.[B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] You had better go get her.[P] Don't worry, I can wait, soldier-bird!") ]])
                
                --Bunny:
                elseif(sSanyaForm == "Bunny") then
                    fnCutscene([[ Append("Mia:[E|Neutral] Oh, hello there.[P] My name is Mia, and I'm the editor of the Nine-Tails Post.[P] Are you a pilgrim?[B][C]") ]])
                    fnCutscene([[ Append("Sanya:[E|Neutral] Nah.[B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] The guards don't usually allow travellers in.[P] Oh, were you hurt?[P] The mikos will take people in to tend to them.[B][C]") ]])
                    fnCutscene([[ Append("Sanya:[E|Neutral] Nah.[P] Maybe my feelings.[B][C]") ]])
                    fnCutscene([[ Append("Sanya:[E|Neutral] Some goons kidnapped my girlfriend at the gate.[P] We're going to get her back.[B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] You get into some situations, don't you.[P] Maybe I can talk to the elder for you.[P] What's your girlfriend's name?[B][C]") ]])
                    fnCutscene([[ Append("Sanya:[E|Neutral] Izuna.[P] I rescued her from a river and treated her wounds.[B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] And -[P] she's not dead?[B][C]") ]])
                    fnCutscene([[ Append("Sanya:[E|Smirk] Be really weird if a dead body's wounds healed.[B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] I don't know how to thank you![P] Thank you![P] Izuna's been my friend since forever, she's the reporter her![B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] Please go to the temple and bring her back here.[P] I'm sure the elder is screaming at her.[P] Thank you so much, miss bunny!") ]])
                end
        
            -- |[Izuna Is Present]|
            else
                VM_SetVar("Root/Variables/Chapter2/Mia/iMetMiaWithIzuna", "N", 1.0)
                fnCutscene([[ Append("Mia:[E|Neutral] Izuna![P] You're alive![B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Happy] Thanks to Sanya here![P] Hello, Mia![P] How have you been?[B][C]") ]])
                fnCutscene([[ Append("Mia:[E|Neutral] Worried sick![P] I thought you were dead![P] You disappeared for a month![B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Ugh] Uhhhh...[B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Neutral] The elder said she just ran off without saying where she went.[B][C]") ]])
                fnCutscene([[ Append("Mia:[E|Neutral] I had a guess.[P] The Shrine of the Hero.[P] That's where I figured she'd have gone but the search parties came back empty-handed.[B][C]") ]])
                fnCutscene([[ Append("Mia:[E|Neutral] Where did you find her, Sanya?[B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Sad] The river underneath the shrine.[P] I got to watch her fall in.[B][C]") ]])
                fnCutscene([[ Append("Mia:[E|Neutral] Well, I don't know how to thank you.[P] Whatever you need, it's yours.[B][C]") ]])
                fnCutscene([[ Append("Mia:[E|Neutral] As for YOU, Izuna![B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Ugh] Elder Yukina already scolded me plenty![B][C]") ]])
                fnCutscene([[ Append("Mia:[E|Neutral] Mgrmgr, fine.[P] She got to have all the fun.[B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Explain] But I did find something important while I was at the shrine.[P] I told you I would.[B][C]") ]])
                if(iEmpressJoined == 1.0) then
                    fnCutscene([[ Append("Mia:[E|Neutral] You mean the giant dragon?[B][C]") ]])
                    fnCutscene([[ Append("Empress:[E|Neutral] A prize greater than that, even.[B][C]") ]])
                end
                fnCutscene([[ Append("Mia:[E|Neutral] Oh, and what's that?[B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Smirk] She means Zeke here.[B][C]") ]])
                fnCutscene([[ Append("Zeke:[E|Neutral] Bah?[B][C]") ]])
                fnCutscene([[ Append("Mia:[E|Neutral] What a find![B][C]") ]])
                if(iEmpressJoined == 1.0) then
                    fnCutscene([[ Append("Empress:[E|Neutral] ...[B][C]") ]])
                end
                fnCutscene([[ Append("Izuna:[E|Smirk] No no, I found Sanya, the legendary hero![P] I told you the prophecies were real![B][C]") ]])
                fnCutscene([[ Append("Mia:[E|Neutral] Really?[P] This is her?[B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Smirk] Got a runestone and everything.[B][C]") ]])
                fnCutscene([[ Append("Mia:[E|Neutral] I will let you know that we are journalists, miss Sanya.[P] We need some evidence before we'll publish anything.[B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Smirk] The elder thinks we should attempt the Trials of the Dragon.[B][C]") ]])
                fnCutscene([[ Append("Mia:[E|Neutral] I was just about to suggest that.[P] Goddess' Whiskers, hero or not, completing the trials would be newsworthy.[B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Smirk] So is this like, your newsroom?[B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Explain] I suppose I haven't told you, have I?[P] Mia is my editor![B][C]") ]])
                fnCutscene([[ Append("Mia:[E|Neutral] I handle the typesetting and run the printing press.[P] We got it imported from Jeffespeir.[P] Nothing else like it in Trafal![B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Smirk] And I do the interviews and research to get the stories![B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Smirk] How's your circulation?[B][C]") ]])
                fnCutscene([[ Append("Mia:[E|Neutral] Half the continent reads our pamphlets.[P] Traders coming through Trafal get a discount on kitsune-grown produce if they take some along.[B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Smirk] Does that mean the elder supports this?[B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Smirk] Yeah![P] She agrees that knowledge is nothing if nobody knows it.[P] Our job is to make sure everyone learns the lessons we teach here.[B][C]") ]])
                fnCutscene([[ Append("Mia:[E|Neutral] And since I'm assuming Izuna is going to be attached to your hip, she's going to be out there getting the scoops with you.[B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Blush] She offered to be my guide.[P] I also kissed her a few times.[B][C]") ]])
                fnCutscene([[ Append("Mia:[E|Neutral] I am not even a little surprised.[B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Blush] Shut up, Mia![B][C]") ]])
                fnCutscene([[ Append("Mia:[E|Neutral] She likes strong, forward types.[B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Blush] I said shut up, Mia![B][C]") ]])
                fnCutscene([[ Append("Mia:[E|Neutral] So listen.[P] You guys go out and do your thing.[P] Whenever you do, come back here and I'll make sure the entire glacier knows.[B][C]") ]])
                fnCutscene([[ Append("Mia:[E|Neutral] Things have been getting tough lately, and people need hope.[P] You go rough up some rogues and complete some trials, and the people will surely benefit.[P] Just get the story, and I'll get the word out.[B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Smirk] Thanks, Mia.[P] I knew we could count on you.[B][C]") ]])
                fnCutscene([[ Append("Zeke:[E|Neutral] Nyeh![B][C]") ]])
                fnCutscene([[ Append("Mia:[E|Neutral] And don't be a stranger whenever you're in town.[P] I'll try to keep track of the rumours, might be leads for you to follow up.") ]])
            end
        
        -- |[ ===================== Successive Meeting ===================== ]|
        else
            
            -- |[Variables]|
            local sFormMetMiaIn = VM_GetVar("Root/Variables/Chapter2/Mia/sFormMetMiaIn", "S")
        
            -- |[Izuna Not Present]|
            --Briefly tell the player to go get her. 
            if(bIsIzunaPresent == false) then
        
                -- |[Form change]|
                if(sFormMetMiaIn ~= sSanyaForm) then
        
                    --First form was human:
                    if(sFormMetMiaIn == "Human") then
        
                        --Current form is Sevavi:
                        if(sSanyaForm == "Sevavi") then
                            VM_SetVar("Root/Variables/Chapter2/Mia/iMiaIsMad", "N", 1.0)
                            fnCutscene([[ Append("Mia:[E|Neutral] Wa-ho![P] Miss Sanya, did you -[P] become stone?[B][C]") ]])
                            fnCutscene([[ Append("Sanya:[E|Blush] I see you noticed.[B][C]") ]])
                            fnCutscene([[ Append("Mia:[E|Neutral] How could I not?[P] Your -[P] assets![P] Oh my![B][C]") ]])
                            fnCutscene([[ Append("Sanya:[E|Blush] Like what you see, Mia?[B][C]") ]])
                            fnCutscene([[ Append("Mia:[E|Neutral] I -[P] I want to lick them![P] May I?[B][C]") ]])
                            fnCutscene([[ Append("Sanya:[E|Blush] You know what?[P] Yes.[P] Zeke, look away.[B][C]") ]])
                            fnCutscene([[ Append("Mia:[E|Neutral] Mmm...[P] they're so...[P] big and round...[P] mmm...[B][C]") ]])
                            fnCutscene([[ Append("Mia:[E|Neutral] Well I suppose they tasted exactly like what I expected.[P] But I still liked it...[B][C]") ]])
                            fnCutscene([[ Append("Sanya:[E|Blush] Anytime, cutie.[B][C]") ]])
                            fnCutscene([[ Append("Mia:[E|Neutral] Now, uh, business.[P] So why did you choose to join the Sevavi?[B][C]") ]])
                            fnCutscene([[ Append("Sanya:[E|Smirk] The what now?[B][C]") ]])
                            fnCutscene([[ Append("Mia:[E|Neutral] The statue guardians are called the Sevavi.[P] How did you not know that?[B][C]") ]])
                            fnCutscene([[ Append("Sanya:[E|Smirk] Well isn't that neat to find out.[P] Sevavi.[P] Sounds cool.[B][C]") ]])
                            fnCutscene([[ Append("Sanya:[E|Neutral] I wasn't paying attention, because the transformation gave me a perfect body.[P] That was the most important part.[B][C]") ]])
                            fnCutscene([[ Append("Sanya:[E|Neutral] But get this -[P] I can transform back into a human if I want![B][C]") ]])
                            fnCutscene([[ Append("Mia:[E|Neutral] No no no, no you definitely cannot![B][C]") ]])
                            fnCutscene([[ Append("Sanya:[E|Smirk] I can show you.[B][C]") ]])
                            fnCutscene([[ Append("Mia:[E|Neutral] I forbid you from showing me![P] Go get Izuna from the temple and bring her back here, right now![B][C]") ]])
                            fnCutscene([[ Append("Sanya:[E|Offended] Sheesh, you went from horny to shouty pretty quick.[B][C]") ]])
                            fnCutscene([[ Append("Mia:[E|Neutral] I continue to be horny.[P] I want to lick them again.[B][C]") ]])
                            fnCutscene([[ Append("Mia:[E|Neutral] But indignation trumps boobs every time![P] Go get Izuna, right now!") ]])
        
                        --Current form is Werebat:
                        elseif(sSanyaForm == "Werebat") then
                            VM_SetVar("Root/Variables/Chapter2/Mia/iMiaIsMad", "N", 1.0)
                            fnCutscene([[ Append("Mia:[E|Neutral] Oh my, Miss Sanya.[P] How did this happen?[P] You know the curse is quite easy to remove.[B][C]") ]])
                            fnCutscene([[ Append("Sanya:[E|Neutral] Yeah, I know.[P] I did it on purpose.[B][C]") ]])
                            fnCutscene([[ Append("Mia:[E|Neutral] I see.[P] Any reason why?[B][C]") ]])
                            fnCutscene([[ Append("Sanya:[E|Smirk] Might be useful to be a werebat.[P] It gives me some new abilities, and an unquenchable thirst for blood.[P] And fruit.[B][C]") ]])
                            fnCutscene([[ Append("Mia:[E|Neutral] Seems a tad permanent.[B][C]") ]])
                            fnCutscene([[ Append("Sanya:[E|Neutral] Nah.[P] Should have told you earlier but I can transform with this runestone.[B][C]") ]])
                            fnCutscene([[ Append("Mia:[E|Neutral] You can?[P] And you found Izuna at...[B][C]") ]])
                            fnCutscene([[ Append("Sanya:[E|Smirk] The Shrine of the Hero.[B][C]") ]])
                            fnCutscene([[ Append("Mia:[E|Neutral] On a scale from 1 to 10, how strong, physically, would you describe yourself?[B][C]") ]])
                            fnCutscene([[ Append("Sanya:[E|Happy] A number bigger than 10.[P] I keep on looking for metal bars to bend but everyone gives me the stinkeye when I do.[B][C]") ]])
                            fnCutscene([[ Append("Mia:[E|Neutral] WELL ISN'T THAT INTERESTING.[B][C]") ]])
                            fnCutscene([[ Append("Mia:[E|Neutral] So, Sanya, the transforming superstrong werebat.[P] Can I ask you to go fetch Izuna for me and bring her back in one piece?[B][C]") ]])
                            fnCutscene([[ Append("Mia:[E|Neutral] I swear I just want to talk to her.[B][C]") ]])
                            fnCutscene([[ Append("Sanya:[E|Smirk] That was on my to-do list, yes.[P] I'll go get her.") ]])
                        
                        --Current form is Harpy:
                        elseif(sSanyaForm == "Harpy") then
                            VM_SetVar("Root/Variables/Chapter2/Mia/iMiaIsMad", "N", 1.0)
                            fnCutscene([[ Append("Mia:[E|Neutral] Oh dear![P] How -[P] don't harpies -[P] transform via eggs?[P] How did you -[P] are we under attack?[P] How are you a harpy, miss Sanya?[B][C]") ]])
                            fnCutscene([[ Append("Sanya:[E|Neutral] Oh, I can transform with my runestone.[P] It's really cool.[B][C]") ]])
                            fnCutscene([[ Append("Mia:[E|Neutral] You can what!?[B][C]") ]])
                            fnCutscene([[ Append("Sanya:[E|Neutral] Did I not mention that I'm a hero or something that's here to kill a big monster?[B][C]") ]])
                            fnCutscene([[ Append("Mia:[E|Neutral] WELL.[P] HOW.[P] INTERSESTING.[B][C]") ]])
                            fnCutscene([[ Append("Mia:[E|Neutral] PLEASE GO FETCH IZUNA FOR ME.[P] I JUST WANT TO TALK.[B][C]") ]])
                            fnCutscene([[ Append("Sanya:[E|Sad] Uhhh, okay...") ]])
                        
                        --Current form is Bunny:
                        elseif(sSanyaForm == "Bunny") then
                            VM_SetVar("Root/Variables/Chapter2/Mia/iMiaIsMad", "N", 1.0)
                            fnCutscene([[ Append("Mia:[E|Neutral] Are -[P] are you a bunny?[P] Were you a bunny a moment ago?[B][C]") ]])
                            fnCutscene([[ Append("Sanya:[E|Neutral] Oh, I can transform with my runestone.[P] It's really cool.[B][C]") ]])
                            fnCutscene([[ Append("Mia:[E|Neutral] You can what!?[B][C]") ]])
                            fnCutscene([[ Append("Sanya:[E|Neutral] Did I not mention that I'm a hero or something that's here to kill a big monster?[B][C]") ]])
                            fnCutscene([[ Append("Sanya:[E|Neutral] I should have probably mentioned that.[B][C]") ]])
                            fnCutscene([[ Append("Mia:[E|Neutral] I actually want to talk about this rather extensively, I really do, but I don't want to repeat myself.[P] So.[B][C]") ]])
                            fnCutscene([[ Append("Mia:[E|Neutral] Go get Izuna and bring her back here.[P] Won't you please?[B][C]") ]])
                            fnCutscene([[ Append("Sanya:[E|Smirk] I was going to anyway, so sure.") ]])
                        end
                    
                    --First form was non-human:
                    elseif(sFormMetMiaIn == "Sevavi") then
                        VM_SetVar("Root/Variables/Chapter2/Mia/iMiaIsMad", "N", 1.0)
                        fnCutscene([[ Append("Mia:[E|Neutral] Oh no, no no no...[B][C]") ]])
                        fnCutscene([[ Append("Mia:[E|Neutral] Miss Sanya, may I touch your hand?[B][C]") ]])
                        fnCutscene([[ Append("Sanya:[E|Blush] I find myself getting hit on by a lot of fox girls it seems.[P] Obviously, yes, touch away.[B][C]") ]])
                        fnCutscene([[ Append("Sanya:[E|Blush] Man...[P] maybe I really am gay...[B][C]") ]])
                        fnCutscene([[ Append("Mia:[E|Neutral] Hmm...[P] no trace of magic...[P] no, this is not a disguise spell.[B][C]") ]])
                        fnCutscene([[ Append("Mia:[E|Neutral] Or is it?[P] Sanya, if you are in a disguise, I request to - [P][C]") ]])
                        fnCutscene([[ Append("Sanya:[E|Neutral] I don't even know what you're talking about.[P] Zeke and I have been here, like, a month.[B][C]") ]])
                        fnCutscene([[ Append("Sanya:[E|Laugh] Zeke ate some berries and started sparkling, and that is about all the magic we can do between us.[B][C]") ]])
                        fnCutscene([[ Append("Zeke:[E|Neutral] Nyeh nyeh![B][C]") ]])
                        fnCutscene([[ Append("Mia:[E|Neutral] Oh, that little -[P] Miss Sanya![B][C]") ]])
                        fnCutscene([[ Append("Mia:[E|Neutral] Please go to the temple and bring Izuna here, straight away![P] I must -[P] yell at her![P] Oh she is so annoying![B][C]") ]])
                        fnCutscene([[ Append("Sanya:[E|Surprised] What'd she do!?[B][C]") ]])
                        fnCutscene([[ Append("Mia:[E|Neutral] Bring her here and I will tell you.[P] Go![P] Now!") ]])
                    end
                    
                -- |[Same Form]|
                else
                    fnCutscene([[ Append("Mia:[E|Neutral] Please see to Izuna for me, won't you?[P] She'll be at the temple up north.[B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] I'm really glad she's okay, but I might say something I regret.[B][C]") ]])
                    fnCutscene([[ Append("Sanya:[E|Neutral] Such as?[B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] Call her an idiot, sorhole, taffbrains, moron, goober, dolt, imbecile, neko-face...[B][C]") ]])
                    fnCutscene([[ Append("Sanya:[E|Laugh] Dumbass?[B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] Oh I like that one![P] What do you think of blockhead?[B][C]") ]])
                    fnCutscene([[ Append("Sanya:[E|Laugh] Yeah![P] So you want me to call her all those things for you?[B][C]") ]])
                    fnCutscene([[ Append("Zeke:[E|Neutral] Bah![B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] What?[P] No no![P] Those are what I might call her and then regret it![P] Don't say those![B][C]") ]])
                    fnCutscene([[ Append("Zeke:[E|Neutral] Bah![P] Nyeh nyeh![B][C]") ]])
                    fnCutscene([[ Append("Sanya:[E|Neutral] Okay, fine.[P] Even Zeke says no.[B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] Her heart is in the right place, it really is, but she can be very frustrating at times.") ]])
                end
        
            -- |[Izuna Is Present]|
            else
            
                -- |[Previously Spoke To Mia When Izuna Was Not Present]|
                if(iMetMiaWithIzuna == 0.0) then
                    VM_SetVar("Root/Variables/Chapter2/Mia/iMetMiaWithIzuna", "N", 1.0)
                    fnCutscene([[ Append("Mia:[E|Neutral] Izuna![P] There you are![B][C]") ]])
                    fnCutscene([[ Append("Izuna:[E|Ugh] Good to see you, Mia.[P] Sorry I didn't come sooner.[P] Liara just grabbed me by the ear and hauled me up to the temple.[B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] Oh I can absolutely see her doing that.[B][C]") ]])
                    fnCutscene([[ Append("Izuna:[E|Happy] But Sanya rescued me, again![P] Despite being, you know, free to leave.[B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] She stopped in on the way, actually.[P] We're acquainted.[B][C]") ]])
                    fnCutscene([[ Append("Sanya:[E|Neutral] Apparently this is a newspaper office.[P] You guys ever heard of hanging up a sign?[B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] Sure, but why?[P] The only people allowed in the village are kitsunes who already know where everything is, or pilgrims, who intend to find out.[B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] And heroes who rescue our wayward friends.[B][C]") ]])
                    fnCutscene([[ Append("Izuna:[E|Smirk] Sanya is more a hero than you know![P] She's the one, the prophesied hero![B][C]") ]])
                    fnCutscene([[ Append("Sanya:[E|Happy] Got a runestone and everything.[P] Izuna says you need a great evil vanquished.[B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] I will let you know that we are journalists, miss Sanya.[P] We need some evidence before we'll publish anything.[B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] But Izuna has a real nose for a scoop, so I'll just assume you're the real thing for now.[B][C]") ]])
                    fnCutscene([[ Append("Izuna:[E|Smirk] The elder thinks we should attempt the Trials of the Dragon.[B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] I was just about to suggest that.[P] Goddess' Whiskers, hero or not, completing the trials would be newsworthy.[B][C]") ]])
                    fnCutscene([[ Append("Sanya:[E|Smirk] So is this like, your newsroom?[B][C]") ]])
                    fnCutscene([[ Append("Izuna:[E|Explain] I suppose I haven't told you, have I?[P] Mia is my editor![B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] I handle the typesetting and run the printing press.[P] We got it imported from Jeffespeir.[P] Nothing else like it in Trafal![B][C]") ]])
                    fnCutscene([[ Append("Izuna:[E|Smirk] And I do the interviews and research to get the stories![B][C]") ]])
                    fnCutscene([[ Append("Sanya:[E|Neutral] How's your circulation?[B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] Half the continent reads our pamphlets.[P] Traders coming through Trafal get a discount on kitsune-grown produce if they take some along.[B][C]") ]])
                    fnCutscene([[ Append("Sanya:[E|Smirk] Does that mean the elder supports this?[B][C]") ]])
                    fnCutscene([[ Append("Izuna:[E|Smirk] Yeah![P] She agrees that knowledge is nothing if nobody knows it.[P] Our job is to make sure everyone learns the lessons we teach here.[B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] And since I'm assuming Izuna is going to be attached to your hip, she's going to be out there getting the scoops with you.[B][C]") ]])
                    fnCutscene([[ Append("Sanya:[E|Blush] She offered to be my guide.[P] I also kissed her a few times.[B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] I am not even a little surprised.[B][C]") ]])
                    fnCutscene([[ Append("Izuna:[E|Blush] Shut up, Mia![B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] She likes strong, forward types.[B][C]") ]])
                    fnCutscene([[ Append("Izuna:[E|Blush] I said shut up, Mia![B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] So listen.[P] You guys go out and do your thing.[P] Whenever you do, come back here and I'll make sure the entire glacier knows.[B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] Things have been getting tough lately, and people need hope.[P] You go rough up some rogues and complete some trials, and the people will surely benefit.[P] Just get the story, and I'll get the word out.[B][C]") ]])
                    fnCutscene([[ Append("Izuna:[E|Smirk] Thanks, Mia.[P] I knew we could count on you.[B][C]") ]])
                    fnCutscene([[ Append("Zeke:[E|Neutral] Nyeh![B][C]") ]])
                    fnCutscene([[ Append("Mia:[E|Neutral] And don't be a stranger whenever you're in town.[P] I'll try to keep track of the rumours, might be leads for you to follow up.") ]])
            
                -- |[Normal Case]|
                else
                    fnCutscene([[ Append("Mia:[E|Neutral] Got any scoops for me, or need some leads?[B][C]") ]])
                    fnCutscene([[ WD_SetProperty("Activate Topics After Dialogue", "Mia") ]])
                end
            end
        end
        fnCutsceneBlocker()
    end
end
