-- |[ ============================= Pamphlet: The Bunny Smugglers ============================== ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[Mia]|
if(sActorName == "Mia") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Mia:[VOICE|Mia] Yes, Izuna, I remember printing that pamphlet and giving it to you.[P] You're such a silly fox!") ]])
end

-- |[Clean Up]|
DL_PopActiveObject()
