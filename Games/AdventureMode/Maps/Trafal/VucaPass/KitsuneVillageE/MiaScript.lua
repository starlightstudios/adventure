-- |[ =========================================== Mia ========================================== ]|
--Working in the printing office.
local fWalkSpeed = 0.50
local fStairsSpeed = 0.20
local iLingerTicks = 300

-- Visit bookshelf.
fnCutsceneMove("Mia", 14.25, 10.50, fWalkSpeed)
fnCutsceneMove("Mia",  8.25, 10.50, fWalkSpeed)
fnCutsceneMove("Mia",  8.25,  8.50, fWalkSpeed)
fnCutsceneFace("Mia", 0, -1)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

--Back shelves.
fnCutsceneMove("Mia", 4.25, 8.50, fWalkSpeed)

--Door is open:
if(AL_GetProperty("Is Door Open", "Door") == true) then
    fnCutsceneMove("Mia", 4.25, 4.50, fWalkSpeed)
    fnCutsceneMove("Mia", 5.25, 4.50, fWalkSpeed)
    fnCutsceneFace("Mia", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(iLingerTicks)
    fnCutsceneBlocker()

--Closed, needs to be opened:
else
    fnCutsceneMove("Mia", 4.25, 8.50, fWalkSpeed)
    fnCutsceneBlocker()
    fnCutsceneWait(5)
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Open Door", "Door") ]])
    
    --This code only plays the door-opening sound when not emulating the script.
    fnCutscene([[ 
    if(Cutscene_GetProperty("Is Scattering") == false) then
        AudioManager_PlaySound("World|FlipSwitch")
    end
    ]])
    
    fnCutsceneWait(5)
    fnCutsceneBlocker()
    fnCutsceneMove("Mia", 4.25, 4.50, fWalkSpeed)
    fnCutsceneMove("Mia", 5.25, 4.50, fWalkSpeed)
    fnCutsceneFace("Mia", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(iLingerTicks)
    fnCutsceneBlocker()

end

--Right-side shelf.
fnCutsceneMove("Mia", 8.25, 4.50, fWalkSpeed)
fnCutsceneFace("Mia", 0, -1)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

--Front desk.
fnCutsceneMove("Mia", 4.25, 4.50, fWalkSpeed)
fnCutsceneMove("Mia", 4.25, 9.50, fWalkSpeed)
fnCutsceneFace("Mia", 0, 1)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

--Printing press.
fnCutsceneMove("Mia",  8.25,  9.50, fWalkSpeed)
fnCutsceneMove("Mia",  8.25, 10.50, fWalkSpeed)
fnCutsceneMove("Mia", 14.25, 10.50, fWalkSpeed)
fnCutsceneMove("Mia", 14.25,  9.50, fWalkSpeed)
fnCutsceneFace("Mia", 0, -1)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

--Loop.
