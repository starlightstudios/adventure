-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= Exits ========================================== ]|
if(sObjectName == "SampleExit") then
    
-- |[ ====================================== Examinables ======================================= ]|
elseif(sObjectName == "Papers") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Papers and pamphlets that fell on the floor and nobody has bothered to pick them up.)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "Press") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A crank operated printing press.[P] Primitive by Earth standards, but still effective.)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "InkBarrels") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Thick black inky sludge.[P] Oh, it's actually ink.)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "BooksA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Some books about political systems.[P] Some place called 'Sturnheim' is a constitutional monarchy, while 'Soswitch' is a republic of some sort.)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "BooksB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Magical Settlement', some lame looking series about a guy turning into a magical girl.[P] In this copy, he whinges as a dryad gives him bigger boobs.[P] Oh no, what a tragedy, nerd!)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "BooksC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Cooking recipes.[P] Soup, salad, stuffed roasts, it's all here!)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "BooksD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A series of books on the history of Trafal.[P] Lots of names conquering each other.[P] Sounds like home.)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "BooksE") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A book on various animal anatomies and reproductive processes.[P] Some of the animals on Earth are the same as here, and many are just not.[P] How the hell is that possible?)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "BooksF") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A guide on naval conflict.[P] Seems the people of this world favour ramming ships and boarding actions.)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "BooksG") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A book full of rumours about the various areas around Trafal.[P] Maybe it was written down by Izuna?)") ]])
    fnCutsceneBlocker()

-- |[ ========================================== Other ========================================= ]|
-- |[ ========================================== Debug ========================================= ]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
