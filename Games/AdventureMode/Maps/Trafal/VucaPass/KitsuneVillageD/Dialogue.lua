-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[Name Resolve]|
    --Resolve the name of the calling entity.
    local sActorName = TA_GetProperty("Name")

    --Set facing.
    TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[Common]|
    local iKitsuneState = fnResolveKitsuneState()
    
    -- |[Sleeping Kitsunes]|
    if(sActorName == "HiddenFoxA") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Kitsune:[VOICE|KitsuneB] Zzzzz...") ]])
        fnCutsceneBlocker()

    elseif(sActorName == "HiddenFoxB") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Kitsune:[VOICE|KitsuneD] Mmm...[P] *snore*") ]])
        fnCutsceneBlocker()
        
    elseif(sActorName == "HiddenFoxC") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Kitsune:[VOICE|KitsuneD] Me?[P] I'm not sleeping.[P] It's just cold as hell outside!") ]])
        fnCutsceneBlocker()
    
    -- |[KitsuneA]|
    elseif(sActorName == "KitsuneA") then
        
        --Intro:
        fnCutscene([[ WD_SetProperty("Show") ]])
        if(iKitsuneState == gciKitsuneIntro) then
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneD] Goddess' Whiskers, my girlfriend always messes up the clothes cabinet![P] And as soon as I have it all sorted, she's got it wrecked within the day!") ]])
        elseif(iKitsuneState == gciKitsuneRescuedIzuna) then
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneD] Oh, hi there.[P] So you're the one who saved Izuna?[P] It's nice to know there are some kind people out there in the world...") ]])
        end
    
    -- |[KitsuneB]|
    elseif(sActorName == "KitsuneB") then
        
        --Intro:
        fnCutscene([[ WD_SetProperty("Show") ]])
        if(iKitsuneState == gciKitsuneIntro) then
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] Hi there![P] Are you staying with us for a while?[P] Just let me know what room you're in and I'll deliver water to it.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[VOICE|Sanya] Why do you deliver water to the rooms?[B][C]") ]])
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] Just in case you need something to drink in the middle of the night.[P] Clean water can be tough to get in the mountains.") ]])
        elseif(iKitsuneState == gciKitsuneRescuedIzuna) then
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] Will you be staying long, Izuna, or are you off to get your next story?[B][C]") ]])
            fnCutscene([[ Append("Izuna:[VOICE|Izuna] Won't be staying too long, you don't need to send water to my room.[P] In fact, if anyone wants it, they can have it.[B][C]") ]])
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] Are you sure?[B][C]") ]])
            fnCutscene([[ Append("Izuna:[VOICE|Izuna] It's all right.[P] I can't hold on to a room if someone else needs it.[P] Besides, I'm gone most of the time anyway.[B][C]") ]])
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] I'll put your things in the storage room if someone needs the spot, then.[P] Good luck!") ]])
        end
    
    -- |[KitsuneC]|
    elseif(sActorName == "KitsuneC") then
        
        --Intro:
        fnCutscene([[ WD_SetProperty("Show") ]])
        if(iKitsuneState == gciKitsuneIntro) then
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] The blizzard soaked everything on the clotheslines.[P] Please don't waste towels, or you might just not have one when you need it!") ]])
        elseif(iKitsuneState == gciKitsuneRescuedIzuna) then
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] Was it really the old mining office you stayed in for a month?[B][C]") ]])
            fnCutscene([[ Append("Izuna:[VOICE|Izuna] Yep.[P] I fell into a freezing river.[B][C]") ]])
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] Brrrr![P] Now I'm cold just hearing it!") ]])
        end
    end
end
