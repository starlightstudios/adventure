-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= Exits ========================================== ]|
if(sObjectName == "SampleExit") then
    
-- |[ ====================================== Examinables ======================================= ]|
elseif(sObjectName == "WaterBarrels") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Barrels full of cold, pure water.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Bath") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A private bath, heated from below.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Pool") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A big pool, heated from below, for public bathing.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Towels") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Towels for people using the pool.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "WardrobeA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Just a wardrobe full of kitsune robes.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "WardrobeB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Assorted junk.[P] Books, utensils, jewelry, clothes.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "WardrobeC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The wardrobe is empty.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "WardrobeD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (An old sword and a pile of dirty clothes.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "WardrobeE") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Some carefully folded and sorted miko robes and gloves.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "WardrobeF") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Cold-weather wear.[P] Gloves, earmuffs, heavy coats.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "WardrobeG") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Some beaten-up looking miko robes.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "WardrobeClosed") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Towels, soap, some cups and bowls.)") ]])
    fnCutsceneBlocker()

-- |[ ========================================== Other ========================================= ]|
-- |[ ========================================== Debug ========================================= ]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
