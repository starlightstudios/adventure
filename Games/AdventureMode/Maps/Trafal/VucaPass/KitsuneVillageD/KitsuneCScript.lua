-- |[ ======================================== Kitsune C ======================================= ]|
--Delivering towels.
local fWalkSpeed = 0.50
local fStairsSpeed = 0.20
local iLingerTicks = 125

--Pool Room
fnCutsceneMove("KitsuneC", 23.25, 15.50, fWalkSpeed)
fnCutsceneMove("KitsuneC", 23.25, 12.50, fWalkSpeed)
fnCutsceneFace("KitsuneC", 0, -1)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

--Hallway.
fnCutsceneMove("KitsuneC", 23.25, 15.50, fWalkSpeed)
fnCutsceneMove("KitsuneC",  7.25, 15.50, fWalkSpeed)
fnCutsceneFace("KitsuneC", 0, -1)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

--Loop.
