-- |[ ============================= Pamphlet: Bandits in Westwoods ============================= ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[KitsuneA]|
if(sActorName == "KitsuneA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneD] Well that's not good.[P] I've learned a bit of combat.[P] Those bandits don't have to be bandits.[P] They choose this.[P] At least that's what I'll tell myself.") ]])

-- |[KitsuneB]|
elseif(sActorName == "KitsuneB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] Oh, pamphlets![P] Hmm, not great news this time.[P] Still better to know than not.[P] Let's hope things turn out okay.") ]])

-- |[KitsuneC]|
elseif(sActorName == "KitsuneC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] Is it alright if I ask some questions?[B][C]") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] Were you able to find out if the bandits have a leader?[P] If we can make their forces fracture we will stand a better chance.") ]])
    
elseif(sActorName == "HiddenFoxA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Izuna:[VOICE|Izuna] (I'm not waking someone up just to show them a pamphlet.)") ]])

elseif(sActorName == "HiddenFoxB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Izuna:[VOICE|Izuna] (I'm not waking someone up just to show them a pamphlet.)") ]])

elseif(sActorName == "HiddenFoxC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneD] Funny story, I'm so cold because I just got off guard duty.[P] I'm all for peace if it means I can stay inside more.") ]])

end

-- |[Clean Up]|
DL_PopActiveObject()
