-- |[ ==================================== Pamphlet: Dragon ==================================== ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[KitsuneA]|
if(sActorName == "KitsuneA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneD] Goddess' Whiskers, the legend is true![P] I wonder if there were any nine-tail kitsunes in the time of the empire.[P] I wonder what questions they would ask then?[P] The same ones we might ask now?") ]])

-- |[KitsuneB]|
elseif(sActorName == "KitsuneB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] Oh wow, and that's her isn't it?[P] The dragon empress.[P] Hmm I'll put more candles in the rooms so people can stay up late reading about your adventures, Izuna.") ]])

-- |[KitsuneC]|
elseif(sActorName == "KitsuneC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] Hey, handing out pamphlets?[P] Is it alright if I ask some questions?[P] I like to read them out to others while they are warming themselves up.[B][C]") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] Oh wow, so the empress says the hero has really arrived?[P] Are we sure it can't be faked?[P] I guess if the hero passes the Trials of the Dragon that'll will be proof enough.") ]])
    
elseif(sActorName == "HiddenFoxA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Izuna:[VOICE|Izuna] (I'm not waking someone up just to show them a pamphlet.)") ]])

elseif(sActorName == "HiddenFoxB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Izuna:[VOICE|Izuna] (I'm not waking someone up just to show them a pamphlet.)") ]])

elseif(sActorName == "HiddenFoxC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneD]  So the empress is an ice dragon?[P] I bet she's not so cold all the time.") ]])

end

-- |[Clean Up]|
DL_PopActiveObject()
