-- |[ ======================================= Constructor ====================================== ]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = fnResolveDirectory() --Map's name is the name of the folder it is in.
local sLevelMusic = "KitsuneVillage"
local sMapResolveName = sLevelName

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[ ======================================== Standard ======================================== ]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

    -- |[Construction]|
	--Music.
	AL_SetProperty("Music", sLevelMusic)
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
    
    -- |[Map Handling]|
    --Resolve the map's variable.
    local sMapVariable = fnResolveMapVarFromName(sLevelName)
    if(sMapVariable ~= "Null") then
        
        --Flag the map as uncovered.
        VM_SetVar(sMapVariable, "N", 1.0)
        
        --Run subroutine to build map properties.
        gfnLastMapFunction = fnHandleWestwoodsMap
        fnHandleWestwoodsMap(sLevelName)

    end

-- |[ ======================================== Post-Exec ======================================= ]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	-- |[NPC Spawning]|
	--If an entity needs to spawn after a post-exec cutscene, put them here.
    fnSpawnNPCPattern("Kitsune", "A", "C")
    fnSpawnNPCPattern("HiddenFox", "A", "C")
    
    --Deactivate collisions on these NPCs when they are moving around.
    TA_ChangeCollisionFlag("KitsuneB", false)
    TA_ChangeCollisionFlag("KitsuneC", false)

    -- |[Overlays]|
    --Place overlay code here.
    
    -- |[Skillbooks]|
    --Place skillbook overlay code here.
    
    -- |[Rifle Handling]|
    VM_SetVar("Root/Variables/Global/Sanya/iRifleCode", "N", gciSanyaRifle_NotHere)
    
    -- |[Parallel Scripts]|
    local sBasePath = fnResolvePath()
    Cutscene_HandleParallel("Create", "KitsuneB", sBasePath .. "KitsuneBScript.lua", false)
    Cutscene_HandleParallel("Create", "KitsuneC", sBasePath .. "KitsuneCScript.lua", false)
    
    --Scattering.
    Cutscene_HandleParallel("Scatter", "KitsuneB", 0, 5170-1)
    Cutscene_HandleParallel("Scatter", "KitsuneC", 0, 1466-1)

end
