-- |[ ============================= Pamphlet: The Bunny Smugglers ============================== ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[KitsuneA]|
if(sActorName == "KitsuneA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneD] If the bunnies are going to be traveling, think I could pay one to bring back a nice gift?[P] It'd be fun to surprise my girlfriend for once.") ]])

-- |[KitsuneB]|
elseif(sActorName == "KitsuneB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] Hey, thanks for these, Izuna![P] I'll make sure they get put in all the rooms.[P] Hmm, might have to change a lot of sheets after this one goes out.") ]])

-- |[KitsuneC]|
elseif(sActorName == "KitsuneC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] So is it especially easy for bunnies to make monster boys, or is it just something that's unlikely to happen on it's own?[P] I guess we could check with kitsunes, but we don't work quite the same.[B][C]") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] Is it weird that I said that?[P] A foxboy, ooh.[P] What a thought!") ]])
    
elseif(sActorName == "HiddenFoxA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Izuna:[VOICE|Izuna] (I'm not waking someone up just to show them a pamphlet.)") ]])

elseif(sActorName == "HiddenFoxB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Izuna:[VOICE|Izuna] (I'm not waking someone up just to show them a pamphlet.)") ]])

elseif(sActorName == "HiddenFoxC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneD] And these bun runners, they run around in the nude?[P] Like nothing on nude?[P] And don't get cold?[P] Lucky bastards.") ]])

end

-- |[Clean Up]|
DL_PopActiveObject()
