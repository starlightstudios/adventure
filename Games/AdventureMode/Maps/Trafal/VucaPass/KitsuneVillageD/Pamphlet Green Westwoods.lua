-- |[ =============================== Pamphlet: Green Westwoods =============================== ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[KitsuneA]|
if(sActorName == "KitsuneA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneD] Oh Alraunes?[P] So you went there to write the article, right?[P] Pretty lucky to get to go on travel dates with your girlfriend all day Izuna.[P] I'm only a little jealous.") ]])

-- |[KitsuneB]|
elseif(sActorName == "KitsuneB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] So there's a new coven of alarunes around.[P] I wonder if we will get any alarunes guests?[P] I'll have to figure out what alarunes like to eat as snacks.[P] Fertilizer?[P][P] No that's silly.") ]])

-- |[KitsuneC]|
elseif(sActorName == "KitsuneC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] Mia's been busy again I see.[P] You know shes going to come back just drenched in ink.[B][C]") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] You went there to find this out right?[P] Does the Alraune clearing not have that terrible Cauldron smell?") ]])
    
elseif(sActorName == "HiddenFoxA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Izuna:[VOICE|Izuna] (I'm not waking someone up just to show them a pamphlet.)") ]])

elseif(sActorName == "HiddenFoxB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Izuna:[VOICE|Izuna] (I'm not waking someone up just to show them a pamphlet.)") ]])

elseif(sActorName == "HiddenFoxC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneD] So alraunes just wear leaf bikinis, right?[P] Do they just not get cold very easily?[P] Maybe I should've become an alraune.") ]])

end

-- |[Clean Up]|
DL_PopActiveObject()
