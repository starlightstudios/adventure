-- |[ ======================================== Kitsune B ======================================= ]|
--Distributing water.
local fWalkSpeed = 0.50
local fStairsSpeed = 0.20
local iLingerTicks = 125

--Top Left
fnCutsceneMove("KitsuneB", 4.25, 8.50, fWalkSpeed)
fnCutsceneFace("KitsuneB", 0, -1)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

--Bottom Left
fnCutsceneMove("KitsuneB", 18.25,  8.50, fWalkSpeed)
fnCutsceneMove("KitsuneB", 18.25, 22.50, fWalkSpeed)
fnCutsceneMove("KitsuneB",  4.25, 22.50, fWalkSpeed)
fnCutsceneFace("KitsuneB", 0, -1)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

--Bottom Right
fnCutsceneMove("KitsuneB", 35.25, 22.50, fWalkSpeed)
fnCutsceneFace("KitsuneB", 0, -1)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

--Top Right
fnCutsceneMove("KitsuneB", 21.25, 22.50, fWalkSpeed)
fnCutsceneMove("KitsuneB", 21.25,  8.50, fWalkSpeed)
fnCutsceneMove("KitsuneB", 35.25,  8.50, fWalkSpeed)
fnCutsceneFace("KitsuneB", 0, -1)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

--Loop.
