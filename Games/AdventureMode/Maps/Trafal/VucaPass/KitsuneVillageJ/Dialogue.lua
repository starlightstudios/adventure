-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[Name Resolve]|
    --Resolve the name of the calling entity.
    local sActorName = TA_GetProperty("Name")

    --Set facing.
    TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[KitsuneA]|
    if(sActorName == "KitsuneA") then
        
        -- |[Setup]|
        local iKitsuneState = fnResolveKitsuneState()
        fnCutscene([[ WD_SetProperty("Show") ]])
        
        -- |[Intro]|
        if(iKitsuneState == 0) then
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneD] Oh, hello there![P] Are you a visitor?[P] The village is up the steps, just east of here![B][C]") ]])
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneD] Me?[P] Oh I was just delivering fresh linens to the guard post.") ]])
        elseif(iKitsuneState == gciKitsuneRescuedIzuna) then
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneD] Going out already?[P] I'm looking forward to your next big story!") ]])
        end
    end
end
