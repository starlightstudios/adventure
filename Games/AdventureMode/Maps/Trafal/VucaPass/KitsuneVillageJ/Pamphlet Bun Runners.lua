-- |[ ============================= Pamphlet: The Bunny Smugglers ============================== ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[KitsuneA]|
if(sActorName == "KitsuneA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneD] So the bunnies are smuggling things?[P] Not the biggest deal, we don't have many laws out here.[P] Still, I'll make sure the guards know.[P] Wouldn't want them to bring anything in they shouldn't or take anything out that's not theirs.") ]])
end

-- |[Clean Up]|
DL_PopActiveObject()
