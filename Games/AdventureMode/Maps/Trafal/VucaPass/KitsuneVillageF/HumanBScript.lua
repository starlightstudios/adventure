-- |[ ========================================= Human B ======================================== ]|
--Making the beds.
local fWalkSpeed = 0.50
local fStairsSpeed = 0.20
local iLingerTicks = 125

--Bed TCR
fnCutsceneMove("HumanB", 21.25, 11.50, fWalkSpeed)
fnCutsceneFace("HumanB", 0, -1)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

--Bed TCL
fnCutsceneMove("HumanB", 19.25, 11.50, fWalkSpeed)
fnCutsceneFace("HumanB", 0, -1)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

--Bed TL
fnCutsceneMove("HumanB", 17.25, 11.50, fWalkSpeed)
fnCutsceneFace("HumanB", 0, -1)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

--Bed BL
fnCutsceneMove("HumanB", 17.25, 14.50, fWalkSpeed)
fnCutsceneFace("HumanB", 0, 1)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

--Bed BCL
fnCutsceneMove("HumanB", 19.25, 14.50, fWalkSpeed)
fnCutsceneFace("HumanB", 0, 1)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

--Bed BCR
fnCutsceneMove("HumanB", 21.25, 14.50, fWalkSpeed)
fnCutsceneFace("HumanB", 0, 1)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

--Bed BR
fnCutsceneMove("HumanB", 23.25, 14.50, fWalkSpeed)
fnCutsceneFace("HumanB", 0, 1)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

--Bed TR
fnCutsceneMove("HumanB", 23.25, 11.50, fWalkSpeed)
fnCutsceneFace("HumanB", 0, -1)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

--Loop.
