-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[Name Resolve]|
    --Resolve the name of the calling entity.
    local sActorName = TA_GetProperty("Name")

    --Set facing.
    TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[Common]|
    local iKitsuneState = fnResolveKitsuneState()
    
    -- |[KitsuneA]|
    if(sActorName == "KitsuneA") then
        
        --Intro:
        fnCutscene([[ WD_SetProperty("Show") ]])
        if(iKitsuneState == gciKitsuneIntro) then
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneD] I'm keeping an eye on the pilgrims.[P] We don't normally allow outsiders in, unless they are truly without anywhere else to go.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[VOICE|Sanya] So what about these dudes?[B][C]") ]])
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneD] They are studying to become mikos, of course![P] But I still must keep watch.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[VOICE|Sanya] You don't trust them?[B][C]") ]])
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneD] Me?[P] Mostly.[P] But the elder requires it.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[VOICE|Sanya] And do you agree with the elder?[B][C]") ]])
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneD] Mostly.[P] By the time we trust them, they are ready to become kitsunes.[P] That's the whole point!") ]])
        elseif(iKitsuneState == gciKitsuneRescuedIzuna) then
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneD] Oh, I'm sorry.[P] I thought you were another outsider.[P] You're the one who saved Izuna?[B][C]") ]])
            fnCutscene([[ Append("Sanya:[VOICE|Sanya] That's me.[B][C]") ]])
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneD] If that's not a great deed, I don't know what is.[P] I'd be happy to take you in as a pilgrim if you want to become a miko someday.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[VOICE|Sanya] I will...[P] consider it.") ]])
        end
    
    -- |[HumanA]|
    elseif(sActorName == "HumanA") then

        --Intro:
        fnCutscene([[ WD_SetProperty("Show") ]])
        if(iKitsuneState == gciKitsuneIntro or iKitsuneState == gciKitsuneRescuedIzuna) then
            fnCutscene([[ Append("Mercenary:[VOICE|MercF] I used to be a soldier.[P] I still want to be a warrior, but I want to learn how the kitsunes fight.[P] I want my skills to be useful for something.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[VOICE|Sanya] What can they teach you about fighting?[B][C]") ]])
            fnCutscene([[ Append("Mercenary:[VOICE|MercF] They're incredibly smart.[P] Their grasp of tactics is unmatched.[P] Kitsunes are famous for their tricks, and I want to learn those.[B][C]") ]])
            fnCutscene([[ Append("Mercenary:[VOICE|MercF] But not just that, they fight for things that matter.[P] Defending the weak, making the world better.[P] I want to do that, too.") ]])
        end
    
    -- |[HumanB]|
    elseif(sActorName == "HumanB") then

        --Intro:
        fnCutscene([[ WD_SetProperty("Show") ]])
        if(iKitsuneState == gciKitsuneIntro or iKitsuneState == gciKitsuneRescuedIzuna) then
            fnCutscene([[ Append("Human:[VOICE|ManA] Self-discipline is important.[P] I have to do chores between study sessions.[P] Manual labour is very relaxing when you don't have to do it![B][C]") ]])
            fnCutscene([[ Append("Sanya:[VOICE|Sanya] But aren't they making you do it?[B][C]") ]])
            fnCutscene([[ Append("Human:[VOICE|ManA] No, I can leave if I want.[P] If you work at the harbour and decide you want to quit, the boss sends thugs to smash your house up.[B][C]") ]])
            fnCutscene([[ Append("Human:[VOICE|ManA] I can take my time here.[P] Talk with my friends.[P] Only do what needs to be done.[P] The kitsunes don't care about money.[B][C]") ]])
            fnCutscene([[ Append("Human:[VOICE|ManA] I hope they can teach me how to not want [P]*things*[P] like that...") ]])
        end
    end
end
