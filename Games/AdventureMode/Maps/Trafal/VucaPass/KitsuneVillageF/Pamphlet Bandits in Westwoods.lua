-- |[ ============================= Pamphlet: Bandits in Westwoods ============================= ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[KitsuneA]|
if(sActorName == "KitsuneA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneD] So there is an army of bandits approaching.[P] I wonder how many of those bandits would have made good, no, excellent mikos if things had been different?[P] War is a tragedy.") ]])

-- |[HumanA]|
elseif(sActorName == "HumanA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Mercenary:[VOICE|MercF] If things are as bad as they sound, those bandits have their families in tow.[P] Can we really bring ourselves to fight and kill an army of starving people?[P] I mean I get that they are the ones starting it but not something I'm going to feel good about.") ]])

-- |[HumanB]|
elseif(sActorName == "HumanB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Human:[VOICE|ManA] You mean a whole army of blood thirsty bandits are coming our way?[P] *Gulp*[P] Okay, okay, okay.[P] I really do love this place, I'm no fighter but if it comes to it I'll help protect it.[P] Selflessness is something I need to learn.") ]])
end

-- |[Clean Up]|
DL_PopActiveObject()
