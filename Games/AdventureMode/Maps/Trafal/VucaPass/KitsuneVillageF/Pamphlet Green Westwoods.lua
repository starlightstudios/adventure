-- |[ =============================== Pamphlet: Green Westwoods =============================== ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[KitsuneA]|
if(sActorName == "KitsuneA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneD] Healing the cauldron you say?[P] That'll be hard work.[P] Maybe when things calm down a bit we can have some of the pilgrims help out.[P] Few things more selfless than healing the land.") ]])

-- |[HumanA]|
elseif(sActorName == "HumanA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Mercenary:[VOICE|MercF] Okay so there's a magic tree that's super good at healing the ground.[P] And they only have one of them...[P] why?[P] Why not just take cuttings from the thing and grow it everywhere?") ]])

-- |[HumanB]|
elseif(sActorName == "HumanB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Human:[VOICE|ManA] Huh, plant ladies?[P] I mean I guess it makes sense they exist.[P] After all there are fox ladies walking around.[P] But still.[P] Not hating the unknown is a big part of being a kitsune.") ]])
end

-- |[Clean Up]|
DL_PopActiveObject()
