-- |[ ==================================== Pamphlet: Dragon ==================================== ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[KitsuneA]|
if(sActorName == "KitsuneA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneD] Oh wow, so the legend was true?[P] Izuna's pamphlets are usually reliable.[P] She's pretty good at taking notes.[P] Looks like you might become a kitsune sooner than I thought, Sanya.") ]])

-- |[HumanA]|
elseif(sActorName == "HumanA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Mercenary:[VOICE|MercF] Okay but what was she doing the whole time?[P] 700 years was a long time.[P] She let her empire crumble and that's it?[P] She has no desire to reclaim what was lost?[P] You don't build an empire through hugs.[B][C]") ]])
    fnCutscene([[ Append("Empress:[VOICE|Empress] The world changed and moved on, so did I.[P] There is more to life than ruling.[P] But please, lecture me more on a subject you cannot possibly know about.[B][C]") ]])
    fnCutscene([[ Append("Mercenary:[VOICE|MercF] Oh dear![P] H-[P]hello![P] I didn't...[B][C]") ]])
    fnCutscene([[ Append("Empress:[VOICE|Empress] The kitsune way is to approach questions in good faith, and to seek to understand.[P] You will learn that, I hope.") ]])

-- |[HumanB]|
elseif(sActorName == "HumanB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Human:[VOICE|ManA] Hang on a second, takes me a minute.[P] Oh wow![P] A legend I grew up hearing is coming true![P] Just think, a dock worker like me might be part of a legend![P] Oh wait, gotta not want that.") ]])
end

-- |[Clean Up]|
DL_PopActiveObject()
