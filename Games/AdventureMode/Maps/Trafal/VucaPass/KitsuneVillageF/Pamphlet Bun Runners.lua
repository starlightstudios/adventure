-- |[ ============================= Pamphlet: The Bunny Smugglers ============================== ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[KitsuneA]|
if(sActorName == "KitsuneA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneD] Wait, what did I just read?[P] I'll keep an eye out for former bunny pilgrims, I guess.[P] A life of crime often leaves the survivors looking for more meaning to life.[P] It's always a pleasure to help them find it.") ]])

-- |[HumanA]|
elseif(sActorName == "HumanA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Mercenary:[VOICE|MercF] And she's advertising that boy bunnies,[P] which only she is making,[P] are all part of a criminal gang?[P] And that's something she is telling people of her own free will?[P] I wonder how many bunnies will be hassled so people can check between their legs?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Are you the kind of person who checks someone's pants to verify their gender?[P] Follow-up, what the hell is wrong with you?") ]])

-- |[HumanB]|
elseif(sActorName == "HumanB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Human:[VOICE|ManA] Wait, partirhumans can like men?[P] Where I grew up I was told they could only like girls.[P] I, a lot of things just started to make sense in an unpleasant way.[P] I wonder if things could have been different?[P] Oh well, too late now.[P] Still I might have to send a letter.[P] Letting go of regrets is part of being a kitsune I guess.") ]])
end

-- |[Clean Up]|
DL_PopActiveObject()
