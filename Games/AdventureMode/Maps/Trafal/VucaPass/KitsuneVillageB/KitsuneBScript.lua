-- |[ ======================================== Kitsune B ======================================= ]|
--This kitsune is tending to the orchard.
local fWalkSpeed = 0.50
local fStairsSpeed = 0.20
local iLingerTicks = 125

--Middle
fnCutsceneMove("KitsuneB", 14.25, 14.50, fWalkSpeed)
fnCutsceneFace("KitsuneB", -1, -1)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

--Top
fnCutsceneMove("KitsuneB", 14.25, 13.50, fWalkSpeed)
fnCutsceneMove("KitsuneB", 12.25, 13.50, fWalkSpeed)
fnCutsceneFace("KitsuneB", 0, 1)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

--Middle
fnCutsceneMove("KitsuneB", 14.25, 13.50, fWalkSpeed)
fnCutsceneMove("KitsuneB", 14.25, 14.50, fWalkSpeed)
fnCutsceneFace("KitsuneB", -1, 0)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

--Bottom
fnCutsceneMove("KitsuneB", 14.25, 19.50, fWalkSpeed)
fnCutsceneFace("KitsuneB", -1, 0)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

--Loop.
