-- |[ ======================================== Kitsune F ======================================= ]|
--Admiring the sights.
local fWalkSpeed = 0.50
local fStairsSpeed = 0.20
local iLingerTicks = 125

--Walk down the steps.
fnCutsceneMove("KitsuneF", 20.25,  8.50, fWalkSpeed)
fnCutsceneMove("KitsuneF", 20.25,  9.50, fWalkSpeed)
fnCutsceneMove("KitsuneF", 20.25, 12.50, fStairsSpeed)
fnCutsceneMove("KitsuneF", 20.25, 13.50, fWalkSpeed)
fnCutsceneMove("KitsuneF", 22.25, 13.50, fWalkSpeed)
fnCutsceneFace("KitsuneF", 0, -1)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

--Look at the banner.
fnCutsceneMove("KitsuneF", 22.25, 19.50, fWalkSpeed)
fnCutsceneMove("KitsuneF", 22.25, 23.50, fStairsSpeed)
fnCutsceneMove("KitsuneF", 24.25, 23.50, fWalkSpeed)
fnCutsceneFace("KitsuneF", 0, -1)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

--The field.
fnCutsceneMove("KitsuneF", 24.25, 27.50, fWalkSpeed)
fnCutsceneMove("KitsuneF", 13.25, 27.50, fWalkSpeed)
fnCutsceneFace("KitsuneF", -1, 0)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

--Back up the steps, stand on the roof.
fnCutsceneMove("KitsuneF", 13.25, 24.50, fWalkSpeed)
fnCutsceneMove("KitsuneF", 22.25, 24.50, fWalkSpeed)
fnCutsceneMove("KitsuneF", 22.25, 22.50, fWalkSpeed)
fnCutsceneMove("KitsuneF", 22.25, 19.50, fStairsSpeed)
fnCutsceneMove("KitsuneF", 21.25, 19.50, fWalkSpeed)
fnCutsceneMove("KitsuneF", 21.25, 12.50, fWalkSpeed)
fnCutsceneMove("KitsuneF", 21.25,  8.50, fStairsSpeed)
fnCutsceneMove("KitsuneF", 26.25,  8.50, fWalkSpeed)
fnCutsceneMove("KitsuneF", 27.25, 13.50, fWalkSpeed)
fnCutsceneFace("KitsuneF", 0, 1)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

--Loop.
fnCutsceneMove("KitsuneF", 27.25, 8.50, fWalkSpeed)
