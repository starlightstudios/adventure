-- |[ =============================== Pamphlet: Green Westwoods =============================== ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[KitsuneA]|
if(sActorName == "KitsuneA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] Alraunes healing Westwoods![P] If they need any equipment, we'd happily loan it to them!") ]])

-- |[KitsuneB]|
elseif(sActorName == "KitsuneB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneB] Wow, really?[P] Imagine if we could grow some figs in Westwoods![P] The humidity would be good for them, I think.") ]])

-- |[KitsuneC]|
elseif(sActorName == "KitsuneC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneC] These alraunes follow their own purpose, but in the end we all serve the Fox Goddess.") ]])

-- |[KitsuneD]|
elseif(sActorName == "KitsuneD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneD] Having dreams about a tree?[P] I wouldn't walk across a continent for that.") ]])

-- |[KitsuneE]|
elseif(sActorName == "KitsuneE") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] Maybe we should help renovate that fort once the snows melt![P] I'm sure the alraunes would appreciate it.") ]])

-- |[KitsuneF]|
elseif(sActorName == "KitsuneF") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneD] Making the cauldron livable would be a wonder of the world if they succeeded.") ]])
    
-- |[Vendor]|
elseif(sActorName == "Vendor") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] Izuna, you know better than to try to interview me at work!") ]])
end

-- |[Clean Up]|
DL_PopActiveObject()
