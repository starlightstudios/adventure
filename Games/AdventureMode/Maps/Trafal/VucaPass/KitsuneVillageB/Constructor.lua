-- |[ ======================================= Constructor ====================================== ]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = fnResolveDirectory() --Map's name is the name of the folder it is in.
local sLevelMusic = "KitsuneVillage"
local sMapResolveName = sLevelName

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[ ======================================== Standard ======================================== ]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

    -- |[Construction]|
	--Music.
	AL_SetProperty("Music", sLevelMusic)
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
    
    -- |[Map Handling]|
    --Resolve the map's variable.
    local sMapVariable = fnResolveMapVarFromName(sLevelName)
    if(sMapVariable ~= "Null") then
        
        --Flag the map as uncovered.
        VM_SetVar(sMapVariable, "N", 1.0)
        
        --Run subroutine to build map properties.
        gfnLastMapFunction = fnHandleWestwoodsMap
        fnHandleWestwoodsMap(sLevelName)

    end

-- |[ ======================================== Post-Exec ======================================= ]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	-- |[NPC Spawning]|
	--If an entity needs to spawn after a post-exec cutscene, put them here.
    fnSpawnNPCPattern("Kitsune", "A", "F")
	fnStandardNPCByPosition("Vendor")
    
    --Deactivate collisions on these NPCs when they are moving around.
    TA_ChangeCollisionFlag("KitsuneA", false)
    TA_ChangeCollisionFlag("KitsuneB", false)
    TA_ChangeCollisionFlag("KitsuneD", false)
    TA_ChangeCollisionFlag("KitsuneF", false)
    
    -- |[Special Frames]|
    --Kitsune sweeping.
    EM_PushEntity("KitsuneD")
        TA_SetProperty("Add Special Frame", "Sweep0", "Root/Images/Sprites/Special/KitsuneD|Sweep0")
        TA_SetProperty("Add Special Frame", "Sweep1", "Root/Images/Sprites/Special/KitsuneD|Sweep1")
        TA_SetProperty("Add Special Frame", "Sweep2", "Root/Images/Sprites/Special/KitsuneD|Sweep2")
        TA_SetProperty("Add Special Frame", "Sweep3", "Root/Images/Sprites/Special/KitsuneD|Sweep3")
    DL_PopActiveObject()

    -- |[Overlays]|
    --Place overlay code here.
    
    -- |[Skillbooks]|
    --Place skillbook overlay code here.
    
    -- |[Rifle Handling]|
    VM_SetVar("Root/Variables/Global/Sanya/iRifleCode", "N", gciSanyaRifle_NotHere)
    
    -- |[Parallel Scripts]|
    local sBasePath = fnResolvePath()
    Cutscene_HandleParallel("Create", "KitsuneA", sBasePath .. "KitsuneAScript.lua", false)
    Cutscene_HandleParallel("Create", "KitsuneB", sBasePath .. "KitsuneBScript.lua", false)
    Cutscene_HandleParallel("Create", "KitsuneD", sBasePath .. "KitsuneDScript.lua", false)
    Cutscene_HandleParallel("Create", "KitsuneF", sBasePath .. "KitsuneFScript.lua", false)
    
    --Scattering.
    Cutscene_HandleParallel("Scatter", "KitsuneA", 0, 1362-1)
    Cutscene_HandleParallel("Scatter", "KitsuneB", 0,  851-1)
    Cutscene_HandleParallel("Scatter", "KitsuneD", 0, 1153-1)
    Cutscene_HandleParallel("Scatter", "KitsuneF", 0, 3680-1)

end
