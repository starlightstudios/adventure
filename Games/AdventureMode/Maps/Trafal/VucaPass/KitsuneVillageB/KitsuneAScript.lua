-- |[ ======================================== Kitsune A ======================================= ]|
--This kitsune is tending to the orchard.
local fWalkSpeed = 0.50
local fStairsSpeed = 0.20
local iLingerTicks = 125

--Center
fnCutsceneMove("KitsuneA", 9.25, 18.50, fWalkSpeed)
fnCutsceneFace("KitsuneA", 0, -1)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

--Go left and around
fnCutsceneMove("KitsuneA", 7.25, 18.50, fWalkSpeed)
fnCutsceneMove("KitsuneA", 7.25, 15.50, fWalkSpeed)
fnCutsceneMove("KitsuneA", 9.25, 15.50, fWalkSpeed)
fnCutsceneFace("KitsuneA", 0, 1)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

--Top
fnCutsceneMove("KitsuneA", 12.25, 15.50, fWalkSpeed)
fnCutsceneFace("KitsuneA", 0, -1)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

--Back to bottom middle.
fnCutsceneMove("KitsuneA",  7.25, 15.50, fWalkSpeed)
fnCutsceneMove("KitsuneA",  7.25, 18.50, fWalkSpeed)
fnCutsceneMove("KitsuneA", 13.25, 18.50, fWalkSpeed)
fnCutsceneFace("KitsuneA", 0, 1)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

--Loop.
