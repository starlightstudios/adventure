-- |[ ==================================== Pamphlet: Dragon ==================================== ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[KitsuneA]|
if(sActorName == "KitsuneA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] I don't care what anyone else says, I think Sanya's a hero just for saving you, Izuna.[P] Good luck out there!") ]])

-- |[KitsuneB]|
elseif(sActorName == "KitsuneB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneB] Is that...[P] oh my, she's so...[P] tall...") ]])

-- |[KitsuneC]|
elseif(sActorName == "KitsuneC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneC] You know, I heard some rumours once that some of the elders had known about the dragon empress.[P] Do you think Elder Yukina is one of them?") ]])

-- |[KitsuneD]|
elseif(sActorName == "KitsuneD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneD] The latest gossip is all about a legendary hero?[P] Beats rumblings about war to the north.") ]])

-- |[KitsuneE]|
elseif(sActorName == "KitsuneE") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] You weren't kidding about the Shrine of the Hero, huh.[P] I guess you were right!") ]])

-- |[KitsuneF]|
elseif(sActorName == "KitsuneF") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneD] Yeah, writing a story about this doesn't let you off the hook.[P] Don't go wandering into any ruins alone, you hear?") ]])
    
-- |[Vendor]|
elseif(sActorName == "Vendor") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] Izuna, you know better than to try to interview me at work!") ]])
end

-- |[Clean Up]|
DL_PopActiveObject()
