-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[Name Resolve]|
    --Resolve the name of the calling entity.
    local sActorName = TA_GetProperty("Name")

    --Set facing.
    TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[Common]|
    local iKitsuneState = fnResolveKitsuneState()
    
    -- |[KitsuneA]|
    if(sActorName == "KitsuneA") then
        
        --Intro:
        fnCutscene([[ WD_SetProperty("Show") ]])
        if(iKitsuneState == gciKitsuneIntro) then
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] I hope the fig trees will be all right despite the sudden snow.[P] They're not native to this area.") ]])
        elseif(iKitsuneState == gciKitsuneRescuedIzuna) then
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] Izuna![P] Glad to see you're all right.") ]])
        end
    
    -- |[KitsuneB]|
    elseif(sActorName == "KitsuneB") then

        --Intro:
        fnCutscene([[ WD_SetProperty("Show") ]])
        if(iKitsuneState == gciKitsuneIntro) then
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneB] It's a lot of work keeping the fig trees healthy.[P] It's not helped when we get a blizzard right before spring is supposed to start!") ]])
        elseif(iKitsuneState == gciKitsuneRescuedIzuna) then
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneB] Don't keep Izuna too far from the village or you won't get fresh figs when they grow in![B][C]") ]])
            fnCutscene([[ Append("Izuna:[VOICE|Izuna] I love fresh figs![P] But it will be months before they're ready...") ]])
        end
    
    -- |[KitsuneC]|
    elseif(sActorName == "KitsuneC") then

        --Intro:
        fnCutscene([[ WD_SetProperty("Show") ]])
        if(iKitsuneState == gciKitsuneIntro) then
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneC] It's such a nice day out.[P] Bright and sunny.[P] Good time to take it easy!") ]])
        elseif(iKitsuneState == gciKitsuneRescuedIzuna) then
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneC] Serves you right for running off, Izuna![P] The elder was far too lenient on you!") ]])
        end
    
    -- |[KitsuneD]|
    elseif(sActorName == "KitsuneD") then

        --Intro:
        fnCutscene([[ WD_SetProperty("Show") ]])
        if(iKitsuneState == gciKitsuneIntro) then
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneD] My sister is so lazy![P] The paths aren't going to clear themselves!") ]])
        elseif(iKitsuneState == gciKitsuneRescuedIzuna) then
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneD] So you really think this Sanya is the hero, Izuna?[B][C]") ]])
            fnCutscene([[ Append("Izuna:[VOICE|Izuna] I wouldn't be journeying with her if I didn't.[B][C]") ]])
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneD] Are you sure you're not just using her as an excuse to avoid shovelling snow?[B][C]") ]])
            fnCutscene([[ Append("Izuna:[VOICE|Izuna] And deny you the pleasure?[P] No thank you!") ]])
        end
    
    -- |[KitsuneE]|
    elseif(sActorName == "KitsuneE") then

        --Intro:
        fnCutscene([[ WD_SetProperty("Show") ]])
        if(iKitsuneState == gciKitsuneIntro) then
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] Hi there![P] Are you a pilgrim, or perhaps just a visitor?") ]])
        elseif(iKitsuneState == gciKitsuneRescuedIzuna) then
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneA] Oh you're the one who saved Izuna!?[P] Thank you so much!") ]])
        end
    
    -- |[KitsuneF]|
    elseif(sActorName == "KitsuneF") then

        --Intro:
        fnCutscene([[ WD_SetProperty("Show") ]])
        if(iKitsuneState == gciKitsuneIntro) then
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneD] This village is just the picture of beauty, isn't it?[P] Never be in a hurry, or you'll miss out on living.") ]])
        elseif(iKitsuneState == gciKitsuneRescuedIzuna) then
            fnCutscene([[ Append("Kitsune:[VOICE|KitsuneD] Izuna![P] Oh, remember to let Mia know you're all right.[P] She'll be in the printing office like usual.") ]])
        end
	
    -- |[Vendor]|
	elseif(sActorName == "Vendor") then
		AM_SetShopProperty("Show", "Kitsune Vendor", fnResolvePath() .. "Shopping.lua", "Null")
    end
end
