-- |[ ======================================== Kitsune D ======================================= ]|
--She's sweeping.
local fWalkSpeed = 0.50
local fStairsSpeed = 0.20
local ciTPF = 20

--Move.
fnCutsceneMove("KitsuneD", 31.25, 26.50, fWalkSpeed)
fnCutsceneBlocker()

--Do the entire sweep animation 6 times.
for p = 1, 6, 1 do
    for i = 0, 3, 1 do
        fnCutsceneSetFrame("KitsuneD", "Sweep"..i)
        fnCutsceneWait(ciTPF)
        fnCutsceneBlocker()
    end
end
fnCutsceneSetFrame("KitsuneD", "Null")
fnCutsceneWait(25)
fnCutsceneBlocker()

--Move.
fnCutsceneMove("KitsuneD", 25.25, 26.50, fWalkSpeed)
fnCutsceneBlocker()

--Do the entire sweep animation 6 times.
for p = 1, 6, 1 do
    for i = 0, 3, 1 do
        fnCutsceneSetFrame("KitsuneD", "Sweep"..i)
        fnCutsceneWait(ciTPF)
        fnCutsceneBlocker()
    end
end
fnCutsceneSetFrame("KitsuneD", "Null")
fnCutsceneWait(25)
fnCutsceneBlocker()
