-- |[ ======================================= Constructor ====================================== ]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = fnResolveDirectory() --Map's name is the name of the folder it is in.
local sLevelMusic = "SanyasTheme"
local sMapResolveName = "Map Resolve Name"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[ ======================================== Standard ======================================== ]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

    -- |[Construction]|
	--Music.
    local iCompletedMines = VM_GetVar("Root/Variables/Chapter2/SanyaMines/iCompletedMines", "N")
    if(iCompletedMines == 1.0) then
        AL_SetProperty("Music", sLevelMusic)
    else
        AL_SetProperty("Music", "Null")
    end
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
    
    -- |[Map Setup]|
    --Appears on the Northwoods side of the map.
    local iCompletedHunt = VM_GetVar("Root/Variables/Chapter2/Northwoods/iCompletedHunt", "N")
    if(iCompletedHunt == 1.0) then
        fnHandleNorthwoodsMap(sLevelName)
    end

-- |[ ======================================== Post-Exec ======================================= ]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	-- |[NPC Spawning]|
    --Unset the time if the player already left the plateau.
    local iCompletedMines = VM_GetVar("Root/Variables/Chapter2/SanyaMines/iCompletedMines", "N")
    if(iCompletedMines == 1.0) then
        VM_SetVar("Root/Variables/Chapter2/HeroPlateau/iTime", "N", 0)
    end

	--If an entity needs to spawn after a post-exec cutscene, put them here.
    local iTime = VM_GetVar("Root/Variables/Chapter2/HeroPlateau/iTime", "N")
    if(iTime >= 1.0 and iTime <= 5.0) then
        local iSlot = math.floor(iTime)
        local faColor = gcfaPlateauColor[iSlot]
		AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, faColor[1], faColor[2], faColor[3], faColor[4], faColor[1], faColor[2], faColor[3], faColor[4], true)
    end

    -- |[Overlays]|
    --Place overlay code here.
    
    -- |[Skillbooks]|
    --Place skillbook overlay code here.
    
    -- |[Rifle Handling]|
    VM_SetVar("Root/Variables/Global/Sanya/iRifleCode", "N", gciSanyaRifle_CanFire)

end
