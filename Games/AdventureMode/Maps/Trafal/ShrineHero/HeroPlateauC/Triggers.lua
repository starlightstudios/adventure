-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
if(sObjectName == "Time5") then
    
    --Variables.
    local iTime       = VM_GetVar("Root/Variables/Chapter2/HeroPlateau/iTime", "N")
    local iIgnoreTime = VM_GetVar("Root/Variables/Chapter2/HeroPlateau/iIgnoreTime", "N")
    local iCompletedMines = VM_GetVar("Root/Variables/Chapter2/SanyaMines/iCompletedMines", "N")
    if(iTime >= 5.0 or iCompletedMines == 1.0) then return end

    --Flag.
    VM_SetVar("Root/Variables/Chapter2/HeroPlateau/iTime", "N", 5.0)
    
    --Constants.
    local iFadeTicks = 125
    
    --Crossfade:
    if(iTime >= 1.0 and iTime <= 5.0) then
        local iOldSlot = math.floor(iTime)
        local iCurSlot = 5
        local faOldColor = gcfaPlateauColor[iOldSlot]
        local faCurColor = gcfaPlateauColor[iCurSlot]
		AL_SetProperty("Activate Fade", iFadeTicks, gci_Fade_Under_GUI, true, faOldColor[1], faOldColor[2], faOldColor[3], faOldColor[4], faCurColor[1], faCurColor[2], faCurColor[3], faCurColor[4], true)
    
    --Error, so just snap the color:
    else
        local iOldSlot = 5
        local iCurSlot = 5
        local faOldColor = gcfaPlateauColor[iOldSlot]
        local faCurColor = gcfaPlateauColor[iCurSlot]
		AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, faOldColor[1], faOldColor[2], faOldColor[3], faOldColor[4], faCurColor[1], faCurColor[2], faCurColor[3], faCurColor[4], true)
    end

elseif(sObjectName == "Stumble") then

    --Repeat check.
    local iStumbleScene = VM_GetVar("Root/Variables/Chapter2/HeroPlateau/iStumbleScene", "N")
    if(iStumbleScene == 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter2/HeroPlateau/iStumbleScene", "N", 1.0)
    
    --Black out the screen.
    local faCurColor = gcfaPlateauColor[5]
    AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, faCurColor[1], faCurColor[2], faCurColor[3], faCurColor[4], 0.0, 0.0, 0.0, 1.0, true)
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Cripes it got dark fast.[P] We had less time than I thought.[B][C]") ]])
    fnCutscene([[ Append("Zeke:[VOICE|Zeke] Bahh?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Just stay close.[P] Look for light sources.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneWait(245)
    fnCutsceneBlocker()

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Zeke?[B][C]") ]])
    fnCutscene([[ Append("Zeke:[VOICE|Zeke] Nyeh.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] I think I saw something over there.[P] Come on.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneWait(245)
    fnCutsceneBlocker()

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] [SOUND|World|Thump]Ouch![P] I think I hit something![B][C]") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Zeke?[P] Zeke where are you?[B][C]") ]])
    fnCutscene([[ Append("Zeke:[VOICE|Zeke] BAHH![B][C]") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Oh, oops.[P] I think it was you I tripped on.[B][C]") ]])
    fnCutscene([[ Append("Zeke:[VOICE|Zeke] NYEH BAHH?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Come on, don't pout.[P] I didn't mean it.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneWait(245)
    fnCutsceneBlocker()
    
    --Level transition.
    fnCutscene([[ AL_BeginTransitionTo("SanyaCabinA", "FORCEPOS:20.0x31.0x0") ]])
    fnCutsceneBlocker()
end
