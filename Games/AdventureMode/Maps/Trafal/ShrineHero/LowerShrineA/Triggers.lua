-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
--Chapter 2 introduction.
if(sObjectName == "Trigger") then


    --Make sure Sanya is in the correct costume.
    LM_ExecuteScript(gsCostumeAutoresolve, "Sanya_Human")
    
	if(gbBypassIntro) then
        
        --Set Sanya as the party leader.
        EM_PushEntity("Sanya")
            giPartyLeaderID = RO_GetID()
        DL_PopActiveObject()
        
        --Transition to the next room.
        fnCutscene([[ AL_SetProperty("Activate Fade", 0, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 1) ]])
        fnCutscene([[ AL_BeginTransitionTo("LowerShrineB", "FORCEPOS:1.0x1.0x0") ]])
        fnCutsceneBlocker()
        return
    end
	
	-- |[Image Streaming]|
	fnLoadDelayedBitmapsFromList("Chapter 2 Introduction", gciDelayedLoadLoadAtEndOfTick)
    
    --Make sure Sanya's ID is the party leader ID.
    EM_PushEntity("Sanya")
        giPartyLeaderID = RO_GetID()
        TA_SetProperty("No Automatic Shadow", true)
    DL_PopActiveObject()

    --Black the screen out.
    fnCutscene([[ AL_SetProperty("Activate Fade", 0, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Introduction text.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Activate Scene") ]])
    fnCutscene([[ Append("The clap of a rifle shot rang out, carried over the fresh snow by a gentle, but cold, breeze.[P] Further up the mountain, an antelope bounded away unharmed.[P] A deep laugh echoed after the shot and a smile stretched across Sanya's face that tugged her shaded goggles out of place.[B][C]") ]])
    fnCutscene([[ Append("\"That's what you get for bragging,\"[P] Sanya said as her eldest brother lowered his rifle.[B][C]") ]])
    fnCutscene([[ Append("He shook his head and adjusted his own goggles.[P] \"The sun's glare makes it hard to aim.\"") ]])
    fnCutsceneBlocker()
	
	fnCutscene([[ WD_SetProperty("Transition Scene", "Null", "Root/Images/Scenes/Introduction/Introduction00") ]])
    fnCutscene([[ Append("Sanya hearkened back to the first time her father had taken her out into the mountains to shoot. He had seen some things in his lifetime, but never spoke much of them.[B][C]") ]])
    fnCutscene([[ Append("The first day they had gone out, they had gone out without their weapons. He had decided to simply show her what it was they would be doing. She needed to see the animals, up close, as they are.[B][C]") ]])
    fnCutscene([[ Append("She had been almost uninterested, but when she had seen the faces of the animals, she choked up. She couldn't do it, destroy something so innocent.[B][C]") ]])
    fnCutscene([[ Append("Yet, her father told her, that if her life depended on it, she could not hesitate. That trip, she hadn't made a kill. Her father understood, though she wouldn't admit why.[B][C]") ]])
    fnCutscene([[ Append("Instead she had practiced shooting targets hung in the woods. She practiced concentrating only on the shot, not on the consequences.[B][C]") ]])
    fnCutscene([[ Append("The second time they had gone out, she made a kill. The third time, she made two.") ]])
    fnCutsceneBlocker()
	
	fnCutscene([[ WD_SetProperty("Transition Scene", "Root/Images/Scenes/Introduction/Introduction00", "Null") ]])
    fnCutscene([[ Append("It was better to admit to one's own failures instead of making excuses.[P] Otherwise, a person would never improve.[P] Her father clicked his tongue at the excuse her brother had made.[B][C]") ]])
    fnCutscene([[ Append("The three had been traveling through the lower Alps for several days, tracking a herd of antelope who had descended from the peaks to weather the winter in lower and warmer climates where the vegetation would still grow.[B][C]") ]])
    fnCutscene([[ Append("Sanya adjusted her own goggles against the light of the sun.[P] There was not a single streak of white in the sky, save for the wisps of snow that blew from higher in the mountains.[P] These wisps fell not far from their peaks and created a shimmering rainbow in the light of the noon sun.[P] \"I'll show you how it's done,\" she said.[B][C]") ]])
    fnCutscene([[ Append("She adjusted the strap of her own rifle, a worn Enfield that her grandfather had used during the second World War and palmed a small stone charm in her pocket.[P] He had passed both on to her only days before his passing.[B][C]") ]])
    fnCutscene([[ Append("The weight of the rifle comforted her and its wood felt warm against her back despite the cold of the lower mountains, even through the thick fabric of her red turtle neck and vest.[P] However warm the aged wood of the rifle felt, the stone in her pocket felt warmer.[B][C]") ]])
    fnCutscene([[ Append("She began to walk forward, turning the stone over in her pocket as she followed the tracks of the antelope as they lead over a crest.[P] The stone had always been cold, but ever since she had come on this hunting trip with her brother and father, it had begun to grow warmer.[P] It unnerved her to a degree, but she would not begrudge the unexpected warmth in these cold climes.[B][C]") ]])
    fnCutscene([[ Append("Her thoughts turned over her grandfather's words about the stone as she stepped above the rise.[P] There, just beyond a small mound, was the antelope her brother had missed.[B][C]") ]])
    fnCutscene([[ Append("She crouched and began to maneuver herself into a better position with slow deliberation.[P] The glare of the sun was getting brighter, enough that it might affect her aim.[P] She had commented on her brother's bragging but had done similar herself only immediately after.[P] She could not afford to miss this shot.[B][C]") ]])
    fnCutscene([[ Append("Still gripping the stone charm for warmth, she edged herself closer.[P] The glare of the sun had become a brilliant glow, and just as she was setting her foot down for her last step, she heard her brother and father shout a great alarm.[B][C]") ]])
    fnCutscene([[ Append("The antelope darted away at the sound, but Sanya could give it no thought.[P] Her boot had broken through the snow, and the white path in front of her vanished as the snow bridge crumbled away.[P] She pulled her hand free of her pocket as she began to fall, her vision blurred as she tumbled through the open space where only moments before she had thought was solid ground.[B][C]") ]])
    fnCutscene([[ Append("A blinding glow emanated from her left hand where she held the stone charm.[P] The light shone even through her skin, making her hand appear as if it were glowing red, and a realization came to her:[P] the blinding light she had blamed on the sun had been coming from the stone charm in her pocket....") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(120)
    fnCutsceneBlocker()

    --Warp Sanya offscreen.
    fnCutsceneTeleport("Sanya", 9.75, 1.50)
    fnCutsceneSetFrame("Sanya", "Falling")
    
    --Fade in.
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    
    --Move Sanya and open a dialogue.
    fnCutsceneMove("Sanya", 9.75, 35.50, 2.50)
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] AAIIIEEEEE!!!!") ]])
    fnCutsceneBlocker()
    
    --Wait a while.
    fnCutsceneWait(120)
    fnCutsceneBlocker()
    
    --Black out, play a sound.
    fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutscene([[ AudioManager_PlaySound("World|HardLanding") ]])
    fnCutsceneBlocker()

    --Wait a bit.
    fnCutsceneWait(120)
    fnCutsceneBlocker()
    
    --Transition to next map.
    fnCutscene([[ AL_BeginTransitionTo("LowerShrineB", "FORCEPOS:1.0x1.0x0") ]])
    fnCutsceneBlocker()
	
	-- |[Unload Images]|
	fnCutscene([[ fnUnloadBitmapsFromList("Chapter 2 Introduction") ]])

end
