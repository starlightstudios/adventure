-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
--Chapter 2 introduction.
if(sObjectName == "Wake Up") then

    --Re-enable Sanya's shadows.
    EM_PushEntity("Sanya")
        TA_SetProperty("No Automatic Shadow", false)
    DL_PopActiveObject()

    --Black the screen out.
    fnCutscene([[ AL_SetProperty("Activate Fade", 0, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Positioning.
    fnCutsceneFace("Sanya", 0, 1)
    fnCutsceneTeleport("Sanya", 27.25, 7.50)
    fnCutsceneSetFrame("Sanya", "Wounded")
    fnCutsceneBlocker()

    --Fade in.
    fnCutsceneWait(240)
    fnCutscene([[ AL_SetProperty("Activate Fade", 240, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] *cough*") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(120)
    fnCutsceneBlocker()

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] I'm okay...[P] The ground and...[P] jagged rocks broke my fall.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(120)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneSetFrame("Sanya", "Crouch")
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneSetFrame("Sanya", "Null")
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Sanya", -1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Sanya", 1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Sanya", -1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Hello?[P] Anyone?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Sanya", 27.75, 4.50)
    fnCutsceneFace("Sanya", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Hey![P] Anyone...[P] Hello?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] ...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Nothing.[P] It looks like I fell a kilometer.[P] I bet the sound doesn't even travel that far.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Damn, that's the kind of fall that blows you into gibs.[P] I guess I survived because I'm so tough even the ground wouldn't win in a fight.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneFace("Sanya", -1, 1)
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    fnCutsceneFace("Sanya", 1, 1)
    fnCutsceneWait(125)
    fnCutsceneBlocker()

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Well, like Father Maric says, 'If God plans for you to fall, He plans for you to survive the fall.'[B][C]") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Though a little warning would be appreciated, Lord.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] I may as well see if this cave goes someplace.[P] Maybe I can find some mole people, and they'll have a working cell tower.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] This hunting trip is finally getting interesting.") ]])
    fnCutsceneBlocker()

end
