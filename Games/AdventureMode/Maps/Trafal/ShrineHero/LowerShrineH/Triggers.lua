-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
if(sObjectName == "RageTime") then

    -- |[Repeat Check]|
    local iEmpressMetSevavi = VM_GetVar("Root/Variables/Chapter2/ShrineOfTheHero/iEmpressMetSevavi", "N")
    local iEmpressJoined    = VM_GetVar("Root/Variables/Chapter2/MtSarulente/iEmpressJoined", "N")
    if(iEmpressMetSevavi == 1.0 or iEmpressJoined == 0.0) then return end
    
    -- |[Flag]|
    VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iEmpressMetSevavi", "N", 1.0)
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("Empress:[E|Neutral] Now Sanya, you informed me you had not seen a map of any sort while you were here?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Nope.[P] Other than the one Izuna gave me later.[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] Hm.[P] Interesting...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(65)
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    -- |[Transition]|
    fnCutscene([[ AL_BeginTransitionTo("LowerShrineF", "FORCEPOS:2.0x2.0x0") ]])
    fnCutsceneBlocker()

end
