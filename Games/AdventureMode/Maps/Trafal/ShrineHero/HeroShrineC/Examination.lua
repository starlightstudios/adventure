-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= Exits ========================================== ]|
if(sObjectName == "SampleExit") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("LevelName", "FORCEPOS:28.0x16.0x0")
    
-- |[ ====================================== Examinables ======================================= ]|
elseif(sObjectName == "Sign") then

    --Variables.
    local iGotResetAbility = VM_GetVar("Root/Variables/Chapter2/ShrineOfTheHero/iGotResetAbility", "N")

    --Don't have it yet:
    if(iGotResetAbility == 0.0) then
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iGotResetAbility", "N", 1.0)
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (The Hero has the power to Reset a puzzle with a mere thought.)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (This might also be magic.[P] Actually it's definitely magic.)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Okay so most people have the power to Reset a puzzle with a mere thought.)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ([SOUND|Menu|SpecialItem]Gained the 'Reset' Field Ability![P] This ability will reset a puzzle in case you cock it up.)") ]])
        fnCutsceneBlocker()
        AdvCombat_SetProperty("Set Field Ability", 0, "Root/Special/Combat/FieldAbilities/Reset")
    
    --Already have it.
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (The Hero has the power to Reset a puzzle with a mere thought.)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (This might also be magic. Actually it's definitely magic.)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Okay so most people have the power to Reset a puzzle with a mere thought.)") ]])
        fnCutsceneBlocker()

    end

elseif(sObjectName == "OptionSign") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (If you find yourself pushing the wrong block a lot, you can change an option in the options menu.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Puzzle Blocks Must Be Activated', if true, will cause you to push blocks by pressing the 'Activate' key when facing them.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Give it a try if you find yourself accidentally pushing blocks a lot!)") ]])

-- |[ ========================================== Other ========================================= ]|
-- |[ ========================================== Debug ========================================= ]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
