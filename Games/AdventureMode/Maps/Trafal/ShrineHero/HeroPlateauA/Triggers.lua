-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
if(sObjectName == "Mountains") then
    
    -- |[Repeat check]|
    local iEscapedShrine = VM_GetVar("Root/Variables/Chapter2/ShrineOfTheHero/iEscapedShrine", "N")
    if(iEscapedShrine == 1.0) then return end
    
    -- |[Variables]|
    local sSanyaForm = VM_GetVar("Root/Variables/Global/Sanya/sForm", "S")
    
    -- |[Flag]|
    VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iEscapedShrine", "N", 1.0)
    
    --Movement.
    fnCutsceneMove("Sanya", 28.25, 14.50, 1.50)
    fnCutsceneMove("Sanya", 28.25, 11.50, 1.00)
    fnCutsceneMove("Zeke", 29.25, 14.50, 1.50)
    fnCutsceneMove("Zeke", 29.25, 11.50, 1.00)
    fnCutsceneBlocker()
    fnCutsceneWait(300)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("Sanya:[E|Sad] Zeke...[P] I don't think we're in the Alps anymore.[B][C]") ]])
    if(sSanyaForm == "Human") then
        fnCutscene([[ Append("Zeke:[E|Neutral] Bahh...[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Sad] I think I was being a bit optimistic about this whole thing.[P] I don't...[P] I don't think we're on Earth anymore.[B][C]") ]])
        fnCutscene([[ Append("Zeke:[E|Neutral] ...[P] Bahh...[B][C]") ]])
    
    else
        fnCutscene([[ Append("Zeke:[E|Neutral] Nyeh?[P] NYEH?[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Angry] Oh what makes you so confident there's not a secret race of statue people living under Switzerland who can transform people into statues?[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Angry] The fact that there is also a secret race of statue people living under THIS particular mountain does in no way mean there aren't any under that other mountain we are not currently at, genius![B][C]") ]])
        fnCutscene([[ Append("Zeke:[E|Neutral] ...[P] Bahh...[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Sad] Zeke, I'm sorry...[B][C]") ]])
    
    end
    
    fnCutscene([[ Append("Sanya:[E|Sad] God watch over us, buddy...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] ...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] I suppose it doesn't matter where we are if someone needs our help, right?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] The sun is setting and we have a badly injured girl in our care.[P] We need to find somewhere safe before dark.[B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] ...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] Come on, buddy.[P] I'm sure we'll see your goat friends soon enough.[B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] Nyeh.") ]])
    fnCutsceneBlocker()
    fnAutoFoldParty()
    fnCutsceneBlocker()

end
