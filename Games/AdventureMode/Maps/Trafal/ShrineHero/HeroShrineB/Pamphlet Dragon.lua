-- |[ ==================================== Pamphlet: Dragon ==================================== ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ==== Minor Characters ==== ]|
-- |[SevaviA]|
if(sActorName == "SevaviA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sevavi:[VOICE|Sevavi] All hail her empress the majesty![P] Wait.[P] All hail her majesty the empress![P] Also sorry again about tossing you.") ]])
    fnCutsceneBlocker()
    
-- |[SevaviB]|
elseif(sActorName == "SevaviB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sevavi:[VOICE|Sevavi] I'll admit that to know that I made it to the time of Perfect Hatred's return fills me with joy, sadness and trepidation.[P] You wait so long and time passes all the same.[P] I wish you luck hero.") ]])
    fnCutsceneBlocker()

-- |[SevaviC]|
elseif(sActorName == "SevaviC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sevavi:[VOICE|Sevavi] After so many years the sevavi can keep our promises![P] I always wondered what the legendary hero would look like but now that I see her I'd say you are perfect for the role, Sanya.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Maybe that's why you didn't recognize me.[P] You were expecting someone taller.") ]])
    fnCutsceneBlocker()

-- |[SevaviD]|
elseif(sActorName == "SevaviD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sevavi:[VOICE|Sevavi] So this is the time of legends.[P] I wonder what will become of the Sevavi after Perfect Hatred's defeat?[P] We won't be bound here.[P] I think I'd like to be a statue in a city square.[P] My perfection admired from all directions.") ]])
    fnCutsceneBlocker()
end

-- |[Clean Up]|
DL_PopActiveObject()
