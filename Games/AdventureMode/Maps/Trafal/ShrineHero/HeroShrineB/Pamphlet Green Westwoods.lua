-- |[ =============================== Pamphlet: Green Westwoods =============================== ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ==== Minor Characters ==== ]|
-- |[SevaviA]|
if(sActorName == "SevaviA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sevavi:[VOICE|Sevavi] I became a sevavi after Perfect Hatred was defeated.[P] But I remember the stories of how wonderful the cauldron was before the battle.[P] Perhaps I'll see it in its glory yet.") ]])
    fnCutsceneBlocker()
    
-- |[SevaviB]|
elseif(sActorName == "SevaviB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sevavi:[VOICE|Sevavi] I became a Sevavi before Perfect Hatred was sealed away.[P] The cauldron was a beautiful place then.[P] Perhaps I'll live to swim in it's waters again?[P] I can't wait.") ]])
    fnCutsceneBlocker()

-- |[SevaviC]|
elseif(sActorName == "SevaviC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sevavi:[VOICE|Sevavi] Oh what regular letters, congratulations to the scribe who copied it.[P] So the alraunes are healing the cauldron.[P] That's wonderful.[B][C]") ]])
    fnCutscene([[ Append("Sevavi:[VOICE|Sevavi] Perhaps after Perfect Hatred is defeated I'll go and help them tend to the soil.[P] Saving the day doesn't mean much if the land is dead.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Real altruistic bunch aren't you.") ]])
    fnCutsceneBlocker()

-- |[SevaviD]|
elseif(sActorName == "SevaviD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sevavi:[VOICE|Sevavi] I- I was quite young when the battle against Perfect Hatred happened.[P] We watched from a nearby mountain top.[P] You could see the land die under it.[P] I, I was marked by it's effects.[P] In time I became a Sevavi and became perfect.[P][P] I shouldn't talk when I'm like this..") ]])
    fnCutsceneBlocker()
end

-- |[Clean Up]|
DL_PopActiveObject()
