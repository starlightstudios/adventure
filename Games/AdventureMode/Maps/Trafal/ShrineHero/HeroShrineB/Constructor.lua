-- |[ ======================================= Constructor ====================================== ]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = fnResolveDirectory() --Map's name is the name of the folder it is in.
local sLevelMusic = "TrafalDungeon"
local sMapResolveName = "Map Resolve Name"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[ ======================================== Standard ======================================== ]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

    -- |[Construction]|
	--Music.
    local iSawHostiles = VM_GetVar("Root/Variables/Chapter2/ShrineOfTheHero/iSawHostiles", "N")
    if(iSawHostiles == 1.0) then
        AL_SetProperty("Music", sLevelMusic)
    else
        AL_SetProperty("Music", "Null")
    end
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
    
    -- |[Map Setup]|
    --Appears on the Northwoods side of the map.
    local iCompletedHunt = VM_GetVar("Root/Variables/Chapter2/Northwoods/iCompletedHunt", "N")
    if(iCompletedHunt == 1.0) then
        fnHandleNorthwoodsMap(sLevelName)
    end
    
    -- |[Shooting Setup]|
    AL_SetProperty("Shooting Depth", 2)

-- |[ ======================================== Post-Exec ======================================= ]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	-- |[NPC Spawning]|
	--If an entity needs to spawn after a post-exec cutscene, put them here.
    fnSpawnNPCPattern("Block", "A", "C")
    fnSpawnNPCPattern("Switch", "A", "C")
    fnSpawnNPCPattern("WalkSwitch", "A", "C")
    
    --This enemy ignores world stop cases.
    local iSawHostiles = VM_GetVar("Root/Variables/Chapter2/ShrineOfTheHero/iSawHostiles", "N")
    if(iSawHostiles == 0.0) then
        EM_PushEntity("EnemyAA")
            TA_SetProperty("Set Ignore World Stop", true)
        DL_PopActiveObject()
    end

    --If Sanya has Sevavi form, or has otherwise made friends with them, despawn the enemy versions and spawn the NPC version.
    local sSanyaForm         = VM_GetVar("Root/Variables/Global/Sanya/sForm", "S")
    local iFriendsWithSevavi = VM_GetVar("Root/Variables/Chapter2/ShrineOfTheHero/iFriendsWithSevavi", "N")
    local iEmpressMetSevavi  = VM_GetVar("Root/Variables/Chapter2/ShrineOfTheHero/iEmpressMetSevavi",  "N")
    if((iFriendsWithSevavi == 1.0 and sSanyaForm == "Sevavi") or iEmpressMetSevavi == 1.0) then
        
        --Despawn enemies.
        fnDespawnNPC("EnemyAA")
        fnDespawnNPC("EnemyBA")
        fnDespawnNPC("EnemyCA")
        fnDespawnNPC("EnemyDA")
        
        --Spawn NPCs.
        fnSpawnNPCPattern("Sevavi", "A", "D")
        
        --Unset collision flags for moving NPCs.
        TA_ChangeCollisionFlag("SevaviA", false)
        TA_ChangeCollisionFlag("SevaviB", false)
        TA_ChangeCollisionFlag("SevaviC", false)
    
        --Parallel scripts.
        local sBasePath = fnResolvePath()
        Cutscene_HandleParallel("Create", "SevaviA", sBasePath .. "SevaviAScript.lua", false)
        Cutscene_HandleParallel("Create", "SevaviB", sBasePath .. "SevaviBScript.lua", false)
        Cutscene_HandleParallel("Create", "SevaviC", sBasePath .. "SevaviCScript.lua", false)
    
        --Scattering.
        Cutscene_HandleParallel("Scatter", "SevaviA", 0,  882-1)
        Cutscene_HandleParallel("Scatter", "SevaviB", 0, 1466-1)
        Cutscene_HandleParallel("Scatter", "SevaviC", 0, 1929-1)
    
    --Otherwise, the enemies have spawned. Their properties may need to be adjusted.
    else
    
        --If the player is still carrying Izuna, then these enemies cannot be mugged.
        local iCompletedMines = VM_GetVar("Root/Variables/Chapter2/SanyaMines/iCompletedMines", "N")
        if(iCompletedMines == 0.0) then
            EM_PushEntity("EnemyAA")
                TA_SetProperty("Can Be Mugged", false)
            DL_PopActiveObject()
            EM_PushEntity("EnemyBA")
                TA_SetProperty("Can Be Mugged", false)
            DL_PopActiveObject()
            EM_PushEntity("EnemyCA")
                TA_SetProperty("Can Be Mugged", false)
            DL_PopActiveObject()
            EM_PushEntity("EnemyDA")
                TA_SetProperty("Can Be Mugged", false)
            DL_PopActiveObject()
        end
    end

    -- |[Complete]|
    --If the player has already completed the shrine, set both of these flags so they can move freely.
    local iEscapedShrine = VM_GetVar("Root/Variables/Chapter2/ShrineOfTheHero/iEscapedShrine", "N")
    if(iEscapedShrine == 1.0) then
        VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iHeroShrineBBridge", "N", 1.0)
        VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iHeroShrineBDoor", "N", 1.0)
    end

    -- |[Overlays]|
    --If the Bridge has already been activated:
    local iHeroShrineBBridge = VM_GetVar("Root/Variables/Chapter2/ShrineOfTheHero/iHeroShrineBBridge", "N")
    if(iHeroShrineBBridge == 1.0) then
        fnCutsceneFace("WalkSwitchB", 0, -1)
        fnCutsceneFace("WalkSwitchC", 0, -1)
        AL_SetProperty("Set Layer Disabled", "HiddenBridge", false)
        AL_SetProperty("Set Collision", 37, 28, 0, 0)
        AL_SetProperty("Set Collision", 37, 29, 0, 0)
    else
        fnCutsceneFace("WalkSwitchB", 0, 1)
        fnCutsceneFace("WalkSwitchC", 0, 1)
        AL_SetProperty("Set Layer Disabled", "HiddenBridge", true)
        AL_SetProperty("Set Collision", 37, 28, 0, 1)
        AL_SetProperty("Set Collision", 37, 29, 0, 1)
    end
    
    --If the door has already been opened. This is the middle door.
    local iHeroShrineBDoor = VM_GetVar("Root/Variables/Chapter2/ShrineOfTheHero/iHeroShrineBDoor", "N")
    if(iHeroShrineBDoor == 1.0) then
        fnCutsceneFace("WalkSwitchA", 0, -1)
        AL_SetProperty("Set Layer Disabled", "MidDoorLo", true)
        AL_SetProperty("Set Layer Disabled", "MidDoorHi", true)
        AL_SetProperty("Set Collision", 36, 18, 0, 27)
        AL_SetProperty("Set Collision", 37, 18, 0, 28)
    else
        fnCutsceneFace("WalkSwitchA", 0, 1)
        AL_SetProperty("Set Layer Disabled", "MidDoorLo", false)
        AL_SetProperty("Set Layer Disabled", "MidDoorHi", false)
        AL_SetProperty("Set Collision", 36, 18, 0, 1)
        AL_SetProperty("Set Collision", 37, 18, 0, 1)
    end
    
    --If the block pushing "puzzle" is already solved.
    local iHeroShrineBSolved = VM_GetVar("Root/Variables/Chapter2/ShrineOfTheHero/iHeroShrineBSolved", "N")
    if(iHeroShrineBSolved == 1.0) then
        fnCutsceneTeleport("BlockA", 21.25, 23.50)
        fnCutsceneTeleport("BlockB", 21.25, 26.50)
        fnCutsceneTeleport("BlockC", 21.25, 29.50)
        AL_SetProperty("Set Layer Disabled", "WestDoorLo", true)
        AL_SetProperty("Set Layer Disabled", "WestDoorHi", true)
        AL_SetProperty("Set Collision",  9, 24, 0, 27)
        AL_SetProperty("Set Collision", 10, 24, 0, 28)
    else
        AL_SetProperty("Set Layer Disabled", "WestDoorLo", false)
        AL_SetProperty("Set Layer Disabled", "WestDoorHi", false)
        AL_SetProperty("Set Collision",  9, 24, 0, 1)
        AL_SetProperty("Set Collision", 10, 24, 0, 1)
    end
    
    -- |[Skillbooks]|
    --Place skillbook overlay code here.
    
    -- |[Rifle Handling]|
    VM_SetVar("Root/Variables/Global/Sanya/iRifleCode", "N", gciSanyaRifle_CanFire)

end
