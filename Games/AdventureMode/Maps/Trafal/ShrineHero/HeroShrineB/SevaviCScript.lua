-- |[ ======================================== Sevavi C ======================================== ]|
--Hallway patrol.
local fWalkSpeed = 0.50
local fStairsSpeed = 0.20
local iLingerTicks = 125

--Block A
fnCutsceneMove("SevaviC", 15.75,  9.00, fWalkSpeed)
fnCutsceneFace("SevaviC", 0, 1)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

--Block B
fnCutsceneMove("SevaviC", 15.75, 20.00, fWalkSpeed)
fnCutsceneFace("SevaviC", 0, 1)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

--Block C
fnCutsceneMove("SevaviC",  4.75, 20.00, fWalkSpeed)
fnCutsceneFace("SevaviC", 0, 1)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

--Block D
fnCutsceneMove("SevaviC",  4.75,  9.00, fWalkSpeed)
fnCutsceneFace("SevaviC", 0, 1)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

--Loop.
