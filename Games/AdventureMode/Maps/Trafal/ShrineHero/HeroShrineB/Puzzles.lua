-- |[ ========================================= Puzzles ======================================== ]|
--Script when the player "talks" to a puzzle object, like a block or switch.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================== Opening ========================================= ]|
--Because we're borrowing the dialogue code, the starting topic is "Hello".
if(sTopicName == "Hello") then
	
    -- |[Name Resolve]|
    --Resolve the name of the calling entity.
    local sActorName = TA_GetProperty("Name")
    if(string.sub(sActorName, 1, 5) ~= "Block") then return end

    -- |[Entity List]|
    --List of all blocking blocks. Blocks collide with each other.
    local saBlockList = {"BlockA", "BlockB", "BlockC"}

    -- |[Order Movement]|
    --Blocks can be pushed by talking to them.
    local iMovedX, iMovedY = fnMoveBlock(sActorName, saBlockList, 1)
    
    -- |[Post-Exec]|
    --If the block moved, the returned coordinates will not be -1,-1. If this happens, enqueue
    -- a post-exec so the script can handle the blocks moving onto switches.
    if(iMovedX ~= -1 and iMovedY ~= -1) then
        local sThisPath = LM_GetCallStack(0)
        local sString = "LM_ExecuteScript(\"" .. sThisPath .. "\", \"Post Exec\")"
        fnCutscene(sString)
    end

-- |[ ======================================= Post Exec ======================================== ]|
--After the blocks have moved, they will call this script again to check position cases.
elseif(sTopicName == "Post Exec") then

    --Debug
    --io.write("Calling post-exec.\n")

    -- |[Switch Coordinates]|
    --List of all switches. This script requires all three to be down.
    local saSwitchList = {"SwitchA", "SwitchB", "SwitchC"}
    local iaSwitchPositions = {}
    local baSwitchHandled = {}
    for i = 1, #saSwitchList, 1 do
        
        --World-coordinates.
        EM_PushEntity(saSwitchList[i])
            local fSwitchX, fSwitchY = TA_GetProperty("Position")
        DL_PopActiveObject()
        
        --Store in tile-coordinates.
        iaSwitchPositions[i] = {}
        iaSwitchPositions[i].iX = math.floor(fSwitchX / gciSizePerTile)
        iaSwitchPositions[i].iY = math.floor(fSwitchY / gciSizePerTile)
        
        --Flag.
        baSwitchHandled[i] = false
    end

    -- |[Block Coordinates]|
    --Get positions of the blocks.
    local saBlockList = {"BlockA", "BlockB", "BlockC"}
    local iaBlockPositions = {}
    for i = 1, #saBlockList, 1 do
        
        --World-coordinates.
        EM_PushEntity(saBlockList[i])
            local fBlockX, fBlockY = TA_GetProperty("Position")
        DL_PopActiveObject()
        
        --Store in tile-coordinates.
        iaBlockPositions[i] = {}
        iaBlockPositions[i].iX = math.floor(fBlockX / gciSizePerTile)
        iaBlockPositions[i].iY = math.floor(fBlockY / gciSizePerTile)
        
        --Check if this matches a switch. If so, flag the switch as handled.
        for p = 1, #saSwitchList, 1 do
            if(iaBlockPositions[i].iX == iaSwitchPositions[p].iX and iaBlockPositions[i].iY == iaSwitchPositions[p].iY) then
                baSwitchHandled[p] = true
                break
            end
        end
    end

    -- |[All Switches Handled]|
    --Check if there are any false switches.
    local bAllSwitchesHandled = true
    for i = 1, #saSwitchList, 1 do
        if(baSwitchHandled[i] == false) then
            bAllSwitchesHandled = false
            break
        end
    end

    --Current solve state.
    local iHeroShrineBSolved = VM_GetVar("Root/Variables/Chapter2/ShrineOfTheHero/iHeroShrineBSolved", "N")

    --If all switches are handled:
    if(bAllSwitchesHandled) then
        AL_SetProperty("Set Layer Disabled", "WestDoorLo", true)
        AL_SetProperty("Set Layer Disabled", "WestDoorHi", true)
        AL_SetProperty("Set Collision",  9, 24, 0, 0)
        AL_SetProperty("Set Collision", 10, 24, 0, 0)
        
        --If not already solved, play a sound effect.
        if(iHeroShrineBSolved == 0.0) then
            VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iHeroShrineBSolved", "N", 1.0)
            fnCutscene([[ AudioManager_PlaySound("World|DistantDoorOpen") ]])
        end

    --All switches were not handled:
    else
        AL_SetProperty("Set Layer Disabled", "WestDoorLo", false)
        AL_SetProperty("Set Layer Disabled", "WestDoorHi", false)
        AL_SetProperty("Set Collision",  9, 24, 0, 1)
        AL_SetProperty("Set Collision", 10, 24, 0, 1)
        
        --If already solved and the player un-solved it, play a sound.
        if(iHeroShrineBSolved == 1.0) then
            VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iHeroShrineBSolved", "N", 0.0)
            fnCutscene([[ AudioManager_PlaySound("World|DistantDoorClose") ]])
        end
    end

-- |[ ========================================== Debug ========================================= ]|
else
	fnStandardDialogue("Warning: Unable to parse puzzle case: " .. sObjectName .. "!")
end
