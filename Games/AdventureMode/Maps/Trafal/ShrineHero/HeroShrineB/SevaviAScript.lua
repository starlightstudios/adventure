-- |[ ======================================== Sevavi A ======================================== ]|
--Cleaning Tetra's quarters.
local fWalkSpeed = 0.50
local fStairsSpeed = 0.20
local iLingerTicks = 125

--Block A
fnCutsceneMove("SevaviA", 54.25, 23.50, fWalkSpeed)
fnCutsceneFace("SevaviA", 0, -1)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

--Block B
fnCutsceneMove("SevaviA", 55.25, 23.50, fWalkSpeed)
fnCutsceneFace("SevaviA", 0, -1)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

--Block C
fnCutsceneMove("SevaviA", 55.25, 28.50, fWalkSpeed)
fnCutsceneFace("SevaviA", 0, 1)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

--Block D
fnCutsceneMove("SevaviA", 54.25, 28.50, fWalkSpeed)
fnCutsceneFace("SevaviA", 0, 1)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

--Loop.
