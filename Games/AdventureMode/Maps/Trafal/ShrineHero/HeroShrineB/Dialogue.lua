-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[Name Resolve]|
    --Resolve the name of the calling entity.
    local sActorName = TA_GetProperty("Name")

    --Set facing.
    TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[Variables]|
    local iEscapedShrine    = VM_GetVar("Root/Variables/Chapter2/ShrineOfTheHero/iEscapedShrine", "N")
    local iEmpressMetSevavi = VM_GetVar("Root/Variables/Chapter2/ShrineOfTheHero/iEmpressMetSevavi",  "N")
    
    -- |[SevaviA]|
    if(sActorName == "SevaviA") then
        
        if(iEscapedShrine == 0.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sevavi", "Neutral") ]])
            fnCutscene([[ Append("Sevavi:[E|Neutral] I'm just cleaning the Lady's room while she's away.[B][C]") ]])
            fnCutscene([[ Append("Sevavi:[E|Neutral] Please take care of that intruder for me.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] You bet.[P] I was just on my way to do that.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] (Seems she believed me...)[B][C]") ]])
            fnCutscene([[ Append("Sevavi:[E|Neutral] (Seems the new girl believed me...)") ]])
            fnCutsceneBlocker()
            
        elseif(iEmpressMetSevavi == 0.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sevavi", "Neutral") ]])
            fnCutscene([[ Append("Sevavi:[E|Neutral] Oh, I'm just tidying the Lady's room.[P] Don't mind me.[B][C]") ]])
            fnCutscene([[ Append("Sevavi:[E|Neutral] Are these your friends?[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] Yeah.[P] Is this a problem?[B][C]") ]])
            fnCutscene([[ Append("Sevavi:[E|Neutral] No.[P] Just please keep an eye on them.[P] We're supposed to be protecting this place, you know.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] Anywhere I should be explicitly keeping dangerous outsiders away from?[B][C]") ]])
            fnCutscene([[ Append("Sevavi:[E|Neutral] I'd say the main altar room to the east, but of course that can only be unlocked with the true hero's runestone.[B][C]") ]])
            fnCutscene([[ Append("Sevavi:[E|Neutral] So really, just don't let them track in any dirt.[P] Or at least get them to clean it up.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] Right.[P] Gotcha.") ]])
            fnCutsceneBlocker()
        
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sevavi", "Neutral") ]])
            fnCutscene([[ Append("Sevavi:[E|Neutral] I'm so sorry about before![P] I didn't know you were the hero![B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] No problem.[P] Don't beat yourself up about it.[B][C]") ]])
            fnCutscene([[ Append("Sevavi:[E|Neutral] But what if master Izuna died from our carelessness?[P] Oh please forgive me...") ]])
            fnCutsceneBlocker()
        end
    
    -- |[SevaviB]|
    elseif(sActorName == "SevaviB") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Sevavi:[VOICE|Sevavi] The water doesn't become clogged often, but it is my task to clean it out when it does.[B][C]") ]])
        fnCutscene([[ Append("Sevavi:[VOICE|Sevavi] I love the feeling of it pressing against my skin...[P] Perhaps I should have become a mermaid.") ]])
        fnCutsceneBlocker()
    
    -- |[SevaviC]|
    elseif(sActorName == "SevaviC") then
        
        if(iEscapedShrine == 0.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sevavi", "Neutral") ]])
            fnCutscene([[ Append("Sevavi:[E|Neutral] Dispose of that intruder.[P] No dallying![B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] I was just going to do that.[P] Which way is it?[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] (They really believe I'm one of them...)[B][C]") ]])
            fnCutscene([[ Append("Sevavi:[E|Neutral] The door on the southwest leads to the back entrance, and the southeast leads to the altar room.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] But the altar room's path is blocked.[P] The ceiling collapsed.[B][C]") ]])
            fnCutscene([[ Append("Sevavi:[E|Neutral] It did?[P] Oh dear, how long was I asleep for last...?") ]])
            fnCutsceneBlocker()
            
        elseif(iEmpressMetSevavi == 0.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sevavi", "Neutral") ]])
            fnCutscene([[ Append("Sevavi:[E|Neutral] I never got your name, new girl.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] Sanya.[P] And this is Izuna and Zeke.[B][C]") ]])
            fnCutscene([[ Append("Sevavi:[E|Neutral] I see.[P] I'm sorry for hurting you, miss Izuna.[P] Intruders are supposed to be ejected.[B][C]") ]])
            fnCutscene([[ Append("Sevavi:[E|Neutral] But so long as one of us escorts you, I suppose it will be all right.") ]])
            fnCutsceneBlocker()
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sevavi", "Neutral") ]])
            fnCutscene([[ Append("Sevavi:[E|Neutral] You were the hero we've been waiting for, and we didn't even recognize you.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] Seems to be a common refrain.[B][C]") ]])
            fnCutscene([[ Append("Sevavi:[E|Neutral] The people outside have likely forgotten the old promises, but not us![P] That's the sole reason we're here![B][C]") ]])
            fnCutscene([[ Append("Sevavi:[E|Neutral] Please accept out apologies.[P] The Sevavi will do whatever they can to help you, Hero.") ]])
            fnCutsceneBlocker()
        end
    
    -- |[SevaviD]|
    elseif(sActorName == "SevaviD") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Sevavi:[VOICE|Sevavi] How difficult it must be to be less than perfect.[P] I pity the mortals outside.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[VOICE|Sanya] Me too, sister!") ]])
        fnCutsceneBlocker()
    end
end
