-- |[ ============================= Pamphlet: The Bunny Smugglers ============================== ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ==== Minor Characters ==== ]|
-- |[SevaviA]|
if(sActorName == "SevaviA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sevavi:[VOICE|Sevavi] Hmm, monster boys, you say?[P] I wonder if that might work with statues.[P] Would probably need some quite study ropes and mounts, maybe a codpiece for the man so you don't do any damage at first?[P][P] Oh sorry I'm lost in my own little world.") ]])
    fnCutsceneBlocker()
    
-- |[SevaviB]|
elseif(sActorName == "SevaviB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sevavi:[VOICE|Sevavi] Hmm, smuggling?[P] Think they are smuggling that which would enhance how good I feel already?[P] I've never heard of statues doing drugs,[P] but I'm willing to try.") ]])
    fnCutsceneBlocker()

-- |[SevaviC]|
elseif(sActorName == "SevaviC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sevavi:[VOICE|Sevavi] Bunnies?[P] A couple of them tried to break in here, actually.[P] I caught one and he was ejected beautifully and landed well.[P] Swore like you wouldn't believe when he got up.") ]])
    fnCutsceneBlocker()

-- |[SevaviD]|
elseif(sActorName == "SevaviD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sevavi:[VOICE|Sevavi] Stopping the transformation just short creates a monster boy you say?[P] But why would anyone do that?[P] Why not spread the perfection of being a monster girl?[P] Takes all kinds I suppose.[P] All kinds of wrong.") ]])
    fnCutsceneBlocker()
end

-- |[Clean Up]|
DL_PopActiveObject()
