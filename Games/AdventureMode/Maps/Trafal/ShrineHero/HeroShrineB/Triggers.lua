-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
if(sObjectName == "FloorSwitchA") then
    
    --Repeat check:
    local iHeroShrineBDoor = VM_GetVar("Root/Variables/Chapter2/ShrineOfTheHero/iHeroShrineBDoor", "N")
    if(iHeroShrineBDoor == 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iHeroShrineBDoor", "N", 1.0)
    
    --Hit the switch.
    AudioManager_PlaySound("World|FlipSwitch")
    fnCutsceneFace("WalkSwitchA", 0, -1)
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    
    --Remove door.
    fnCutscene([[ AL_SetProperty("Set Layer Disabled", "MidDoorHi", true) ]])
    fnCutscene([[ AL_SetProperty("Set Layer Disabled", "MidDoorLo", true) ]])
    AL_SetProperty("Set Collision", 36, 18, 0, 0)
    AL_SetProperty("Set Collision", 37, 18, 0, 0)
    fnCutscene([[ AudioManager_PlaySound("World|DistantDoorOpen") ]])

elseif(sObjectName == "FloorSwitchB") then

    --Repeat check:
    local iHeroShrineBSwitchB = VM_GetVar("Root/Variables/Chapter2/ShrineOfTheHero/iHeroShrineBSwitchB", "N")
    local iHeroShrineBSwitchC = VM_GetVar("Root/Variables/Chapter2/ShrineOfTheHero/iHeroShrineBSwitchC", "N")
    if(iHeroShrineBSwitchB == 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iHeroShrineBSwitchB", "N", 1.0)
    
    --Hit the switch.
    AudioManager_PlaySound("World|FlipSwitch")
    fnCutsceneFace("WalkSwitchB", 0, -1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --If the player has also hit switch C, solve the puzzle.
    if(iHeroShrineBSwitchC == 1.0) then
        VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iHeroShrineBBridge", "N", 1.0)
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "HiddenBridge", false) ]])
        AL_SetProperty("Set Collision", 37, 28, 0, 0)
        AL_SetProperty("Set Collision", 37, 29, 0, 0)
        fnCutscene([[ AudioManager_PlaySound("World|DistantDoorOpen") ]])
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Looks like that extended a bridge.)") ]])
        fnCutsceneBlocker()
        
    --Otherwise, print a message:
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (There must be another switch in this room...)") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "FloorSwitchC") then

    --Repeat check:
    local iHeroShrineBSwitchB = VM_GetVar("Root/Variables/Chapter2/ShrineOfTheHero/iHeroShrineBSwitchB", "N")
    local iHeroShrineBSwitchC = VM_GetVar("Root/Variables/Chapter2/ShrineOfTheHero/iHeroShrineBSwitchC", "N")
    if(iHeroShrineBSwitchC == 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iHeroShrineBSwitchC", "N", 1.0)
    
    --Hit the switch.
    AudioManager_PlaySound("World|FlipSwitch")
    fnCutsceneFace("WalkSwitchC", 0, -1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --If the player has also hit switch B, solve the puzzle.
    if(iHeroShrineBSwitchB == 1.0) then
        VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iHeroShrineBBridge", "N", 1.0)
        fnCutscene([[ AL_SetProperty("Set Layer Disabled", "HiddenBridge", false) ]])
        AL_SetProperty("Set Collision", 37, 28, 0, 0)
        AL_SetProperty("Set Collision", 37, 29, 0, 0)
        fnCutscene([[ AudioManager_PlaySound("World|DistantDoorOpen") ]])
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Looks like that extended a bridge.)") ]])
        fnCutsceneBlocker()
        
    --Otherwise, print a message:
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (There must be another switch in this room...)") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "Hostiles") then
    
    -- |[Repeat Check]|
    local iSawHostiles = VM_GetVar("Root/Variables/Chapter2/ShrineOfTheHero/iSawHostiles", "N")
    if(iSawHostiles == 1.0) then return end
    
    -- |[Variables]|
    local sSanyaForm = VM_GetVar("Root/Variables/Global/Sanya/sForm", "S")
    
    -- |[Flags]|
    VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iSawHostiles", "N", 1.0)
    
    -- |[Human Form]|
    if(sSanyaForm == "Human") then
    
        -- |[Camera]|
        Cutscene_CreateEvent("CameraEvent", "Camera")
            CameraEvent_SetProperty("Max Move Speed", 2.0)
            CameraEvent_SetProperty("Focus Position", (49.25 * gciSizePerTile), (19.50 * gciSizePerTile))
        DL_PopActiveObject()
        fnCutsceneWait(245)
        fnCutsceneBlocker()
        Cutscene_CreateEvent("CameraEvent", "Camera")
            CameraEvent_SetProperty("Max Move Speed", 2.0)
            CameraEvent_SetProperty("Focus Actor Name", "Sanya")
        DL_PopActiveObject()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        -- |[Dialogue]|
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Stone people![P] I guess that's like mole people, if mole people dug through...[P] other moles.)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Weirdness aside, there's no way I'll win a fight if I have to haul this cosplayer girl.)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (I better stay out of sight...[P] though their outfits *are* really cute.)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (I wonder if I could maybe steal one.[P] I'll come back later when I've gotten this girl to safety and loot the place blind.)") ]])
        fnCutsceneBlocker()
        fnCutscene([[ AL_SetProperty("Music", "TrafalDungeon") ]])
        
        -- |[Re-Enable World Stop]|
        fnCutscene([[
        EM_PushEntity("EnemyAA")
            TA_SetProperty("Set Ignore World Stop", false)
        DL_PopActiveObject()
        ]])
    
    -- |[Statue]|
    else
    
        -- |[Camera]|
        Cutscene_CreateEvent("CameraEvent", "Camera")
            CameraEvent_SetProperty("Max Move Speed", 2.0)
            CameraEvent_SetProperty("Focus Position", (49.25 * gciSizePerTile), (19.50 * gciSizePerTile))
        DL_PopActiveObject()
        fnCutsceneWait(245)
        fnCutsceneBlocker()
        Cutscene_CreateEvent("CameraEvent", "Camera")
            CameraEvent_SetProperty("Max Move Speed", 2.0)
            CameraEvent_SetProperty("Focus Actor Name", "Sanya")
        DL_PopActiveObject()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        -- |[Dialogue]|
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Okay stay calm.[P] There's just an underground civilization of stone people living under the Alps.[P] It's fine!)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (I'm stone, they're stone, we're all stone and nobody is going to eat anybody.)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Just pretend we're all pals, if one of them tries to nibble on the cosplayer, run.[P] Should be a cakewalk.)") ]])
        fnCutsceneBlocker()
        fnCutscene([[ AL_SetProperty("Music", "TrafalDungeon") ]])
    
    end
end
