-- |[ ============================= Pamphlet: Bandits in Westwoods ============================= ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ==== Minor Characters ==== ]|
-- |[SevaviA]|
if(sActorName == "SevaviA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sevavi:[VOICE|Sevavi] Huh, bandits have always been a problem.[P] How sad that even with the benefit of centuries some things don't change.[P] I wonder if such things are just inherent to the world, or a product of what we make of it?") ]])
    fnCutsceneBlocker()
    
-- |[SevaviB]|
elseif(sActorName == "SevaviB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sevavi:[VOICE|Sevavi] Bandits are a passing mortal concern.[P] But still it'll be nice to have something new in the library.[P] I shouldn't take it though, my hands are wet.[P] The water tickles my finger tips.") ]])
    fnCutsceneBlocker()

-- |[SevaviC]|
elseif(sActorName == "SevaviC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sevavi:[VOICE|Sevavi] Hmm what's this say, letters have changed a bit.[P] Oh bandits?[P] Well you don't need to worry about us Sanya, any that get in here they will regret it.") ]])
    fnCutsceneBlocker()

-- |[SevaviD]|
elseif(sActorName == "SevaviD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sevavi:[VOICE|Sevavi] Poor bandits, if we were anything but Sevavi they might get to know our perfection.[P] But no[P], no bandits will enjoy the perfection of the Sevavi.") ]])
    fnCutsceneBlocker()
end

-- |[Clean Up]|
DL_PopActiveObject()
