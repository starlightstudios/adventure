-- |[ ======================================== Sevavi B ======================================== ]|
--Hanging out near the water.
local fWalkSpeed = 0.50
local fStairsSpeed = 0.20
local iLingerTicks = 125

--Block A
fnCutsceneMove("SevaviB", 55.25,  6.50, fWalkSpeed)
fnCutsceneMove("SevaviB", 55.25, 13.50, fWalkSpeed)
fnCutsceneFace("SevaviB", 0, 1)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

--Block B
fnCutsceneMove("SevaviB", 55.25, 6.50, fWalkSpeed)
fnCutsceneMove("SevaviB", 43.25, 6.50, fWalkSpeed)
fnCutsceneFace("SevaviB", 0, 1)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

--Loop.
