-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= Exits ========================================== ]|
if(sObjectName == "SampleExit") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("LevelName", "FORCEPOS:28.0x16.0x0")
    
-- |[ ====================================== Examinables ======================================= ]|
elseif(sObjectName == "BathA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A bathtub big enough for a dozen people.[P] Doesn't look like it's for clothes, it has to be a bathtub right?)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "Mirror") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    local iRoll = LM_GetRandomNumber(1, 4)
    if(iRoll == 1) then
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Damn, I'm lookin' good.)") ]])
    elseif(iRoll == 2) then
        fnCutscene([[ Append("Thought:[VOICE|Leader] (This mirror looks really nice.[P] Oh wait, that's just me.)") ]])
    elseif(iRoll == 3) then
        fnCutscene([[ Append("Thought:[VOICE|Leader] (The light in here really captures my good side.)") ]])
    elseif(iRoll == 4) then
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Oh no, there's an extremely attractive woman right there!)") ]])
    end
    fnCutsceneBlocker()

elseif(sObjectName == "WardrobeA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Several outfits made of fine cloth, all showing lots of leg.[P] This lady is my kind of dresser.)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "WardrobeB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (This lady really likes red.[P] Red sashes, red bandanas, red shoes.)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "WardrobeC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Pristine clothes folded and categorized.)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "WardrobeClosed") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A closed wardrobe.)") ]])
    fnCutsceneBlocker()

-- |[ ========================================== Other ========================================= ]|
-- |[ ========================================== Debug ========================================= ]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
