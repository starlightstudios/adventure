-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
if(sObjectName == "Snide") then

    --Repeat check.
    local iSnidePond = VM_GetVar("Root/Variables/Chapter2/ShrineOfTheHero/iSnidePond", "N")
    if(iSnidePond == 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iSnidePond", "N", 1.0)

    --Movement.
    fnCutsceneMove("Sanya", 29.25, 21.50)
    fnCutsceneFace("Sanya", 0, -1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] *Gulp*[B][C]") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Yum, my favourite. Snow runoff.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] And it looks like someone left a firepit over there, so the mole people theory continues to gain traction.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] But I'd better not find an underground civilization in the Alps too quickly, otherwise drinking that water when I didn't have to is going to look really dumb.") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "Fire") then

    -- |[Activation Check]|
    local iSwissPeople  = VM_GetVar("Root/Variables/Chapter2/ShrineOfTheHero/iSwissPeople", "N")
    local iSawFireScene = VM_GetVar("Root/Variables/Chapter2/ShrineOfTheHero/iSawFireScene", "N")
    local iMetZeke      = VM_GetVar("Root/Variables/Chapter2/ShrineOfTheHero/iMetZeke", "N")
    if(iSwissPeople == 0.0 or iSawFireScene == 1.0 or iMetZeke == 1.0) then return end
    
    -- |[Flags]|
    VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iSawFireScene", "N", 1.0)
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Hmmm, I could make a fire, but it's extremely cold here and there's no way to the surface by this way.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Unless by sheer coincidence a bone doctor also falls down that pit I fell down and brings their surgical tools, this won't work.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Better keep looking for a way out.") ]])
    fnCutsceneBlocker()
    
end
