-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ====================================== Seeing Zeke ======================================= ]|
if(sObjectName == "SeeZeke") then

    --Repeat check.
    local iSawZeke = VM_GetVar("Root/Variables/Chapter2/ShrineOfTheHero/iSawZeke", "N")
    if(iSawZeke == 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iSawZeke", "N", 1.0)
    
    --Movement.
    fnCutsceneMove("Sanya", 58.75, 7.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] The movies...[P] never show the....[P] part where....[P] ....[P] everyone is panting...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Huff, at least this cosplayer is light as a feather...[P] Just need a minute...[B][C]") ]])
    fnCutscene([[ Append("???:[VOICE|Zeke] Baaah?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Sanya", 55.25, 7.50)
    fnCutsceneBlocker()
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    fnCutsceneMove("Sanya", 41.25, 7.50, 2.00)
    fnCutsceneMove("Zeke", 31.25, 7.50, 2.00)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Zeke", "Neutral") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Could it be?[P] But how?[B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] Nyehehee![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Happy] Zeke, it [P]*is*[P] you![P] I can't believe it![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] ...[P] Because I left you back home and we're currently 800 kilometers from there![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Laugh] You know what, I don't care how you got here, I'm just glad to see you.[B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] Bah![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] Hmm...[P] You must have got in here somehow, but the bridge is out.[P] I think I see a hallway behind you, right?[B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] Nyeehhe![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] So there must be a way around through these ruins.[P] Stay right there.[B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] Bah?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Zeke, even if I was spry enough to jump the gap, do you think I could do it while carrying a weeaboo?[B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] Nyeh...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Happy] Yeah that's what I thought.[P] So you stay there and I'll be right back.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
-- |[ ================================== Escaping the Shrine =================================== ]|
elseif(sObjectName == "Escape") then
    
    -- |[Repeat Check]|
    local iMetZeke = VM_GetVar("Root/Variables/Chapter2/ShrineOfTheHero/iMetZeke", "N")
    if(iMetZeke == 1.0) then return end
    
    -- |[Variables]|
    local sSanyaForm = VM_GetVar("Root/Variables/Global/Sanya/sForm", "S")
    
    -- |[Flags]|
    VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iMetZeke", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter2/HeroPlateau/iTime", "N", 1.0)
    
    --Mark Zeke as in the party.
    fnAddPartyMember("Zeke")
    
    -- |[Movement]|
    fnCutsceneMove("Sanya", 11.25, 7.50)
    fnCutsceneMove("Sanya", 14.25, 7.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Zeke", -1, 0)
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutsceneMove("Zeke", 15.55, 7.50, 2.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Zeke", "Neutral") ]])
    fnCutscene([[ Append("Sanya:[E|Happy] Told you I'd find a way, Zeke![B][C]") ]])
    
    --Human Form:
    if(sSanyaForm ~= "Sevavi") then
        fnCutscene([[ Append("Zeke:[E|Neutral] Baahhh!![B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Sad] Man, you are not going to believe the bullshit that was in there.[P] Hoo boy.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Sad] Get this.[P] Block puzzles![P] Like it's a goddamn calculator game![B][C]") ]])
        fnCutscene([[ Append("Zeke:[E|Neutral] Nyeh![B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Smirk] Hah, yeah it sucked, but at least there won't be any more stupid puzzles from now on.[P] Follow me, we're getting the heck out of here and getting this girl some medical attention.[B][C]") ]])
        fnCutscene([[ Append("Zeke:[E|Neutral] Bah![B][C]") ]])
        fnCutscene([[ Append("Zeke:[E|Neutral] [SOUND|Menu|SpecialItem](Zeke has joined the party!)") ]])
    
    --Transformed:
    else
        fnCutscene([[ Append("Zeke:[E|Neutral] ...[P] Bah?[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Sad] Man, you are not going to believe the bullshit that was in there.[P] Hoo boy.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Blush] Okay so get this.[P] I got turned into a statue, duh.[P] Turns out it's not so bad.[P] I can turn back, too.[B][C]") ]])
        fnCutscene([[ Append("Zeke:[E|Neutral] Nyeh.[P] Nyeh.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Sad] But that's not the half of it.[P] Also, puzzles![P] Freakin' puzzles, Zeke![B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Sad] There's blocks and you have to push them onto switches, it's like a calculator game.[B][C]") ]])
        fnCutscene([[ Append("Zeke:[E|Neutral] Bah![B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Offended] I know![B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Laugh] Luckily, that's the last block puzzle we'll be seeing.[P] Let's get out of here, boy![B][C]") ]])
        fnCutscene([[ Append("Zeke:[E|Neutral] [SOUND|Menu|SpecialItem](Zeke has joined the party!)") ]])
    end
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnAutoFoldParty()
    fnCutsceneBlocker()
    
-- |[ ====================================== Round 2, Baby ===================================== ]|
elseif(sObjectName == "Return") then

    -- |[Repeat Check]|
    local iIzunaExplained = VM_GetVar("Root/Variables/Chapter2/ShrineOfTheHero/iIzunaExplained", "N")
    if(iIzunaExplained == 1.0) then return end

    -- |[Activation Check]|
    local iCompletedHunt = VM_GetVar("Root/Variables/Chapter2/Northwoods/iCompletedHunt", "N")
    if(iCompletedHunt == 0.0) then return end

    -- |[Variables]|
    local sSanyaForm     = VM_GetVar("Root/Variables/Global/Sanya/sForm", "S")
    local iHasSevaviForm = VM_GetVar("Root/Variables/Global/Sanya/iHasSevaviForm", "N")

    -- |[Flag]|
    VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iIzunaExplained", "N", 1.0)
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("Sanya:[E|Neutral] Okay.[P] This place is the Shrine of the Hero, right?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] Yep.[P] The legend says this is where the hero would appear.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] What else does the legend say about it?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] Not a lot, but that doesn't mean there's nothing here for us.[P] The shrine was built because you would appear here, so there's probably information meant for you.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] Let's take a look around, see what we can find.[B][C]") ]])
    if(sSanyaForm ~= "Sevavi" and iHasSevaviForm == 1.0) then
        fnCutscene([[ Append("Izuna:[E|Ugh] It might be a good idea to change forms.[P] The sevavi weren't too friendly last time I was here.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Offended] We're not unarmed and hauling around a wounded girl this time.[P] The fight will be more even.[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Ugh] They're supposed to be our allies, Sanya![B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Offended] Yeah, well, they suck at it.[B][C]") ]])
    elseif(sSanyaForm == "Sevavi") then
        fnCutscene([[ Append("Sanya:[E|Blush] Let me handle the talking.[P] They'll think I'm one of them.[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Neutral] You are one of them, Sanya.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Offended] No I'm not, I'm on a mission to save the world.[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Ugh] So are they, they're supposed to be helping us![B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] Yeah, well, they suck at it.[B][C]") ]])
    else
        fnCutscene([[ Append("Sanya:[E|Happy] Stay close in case things get rough![B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Neutral] Don't remind me.[P] I'd rather not spend another month in bed.[B][C]") ]])
    end
    fnCutscene([[ Append("Sanya:[E|Smirk] Okay, let's go.") ]])
    fnCutsceneBlocker()

-- |[ ==================================== Empress Unhappy ===================================== ]|
elseif(sObjectName == "RageA") then

    -- |[Blackout]|
    fnCutscene([[ AL_SetProperty("Activate Fade", 0, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()

    -- |[Setup]|
    fnCutsceneTeleport("Sanya", 29.25,  8.50)
    fnCutsceneFace("Sanya", 1, 0)
    fnCutsceneTeleport("Izuna", 29.25,  7.50)
    fnCutsceneFace("Izuna", 1, 0)
    fnCutsceneTeleport("Zeke",  29.25,  9.50)
    fnCutsceneFace("Zeke", 1, 0)
    fnCutsceneTeleport("Empress", 32.25,  8.50)
    fnCutsceneFace("Empress", 1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    -- |[Fade In]|
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(185)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneFace("Empress", -1, 0)
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("Empress:[E|Neutral] How.[P] Long.[P] Has the bridge been in this condition?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] Zeke, you saw it before me, right?[P] It was broken when you got here?[B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] Bah![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Offended] (IS HE LYING!?)[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Explain] The bridge has been collapsed for as long as any of the kitsunes know![P] I asked![B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] What of the histories?[P] Did it collapse during some event that was written down?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Explain] I looked that up, too![P] Nobody knows quite when, but it was in poor condition for a long time![B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] I.[P] See.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(65)
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    -- |[Transition]|
    fnCutscene([[ AL_BeginTransitionTo("LowerShrineG", "FORCEPOS:2.0x2.0x0") ]])
    fnCutsceneBlocker()

end
