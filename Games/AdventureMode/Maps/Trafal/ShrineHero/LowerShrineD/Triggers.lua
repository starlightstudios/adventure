-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
if(sObjectName == "Swiss") then

    -- |[Repeat Check]|
    local iSwissPeople = VM_GetVar("Root/Variables/Chapter2/ShrineOfTheHero/iSwissPeople", "N")
    if(iSwissPeople == 1.0) then return end
    
    -- |[Flag]|
    VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iSwissPeople", "N", 1.0)
    
    -- |[Inventory Handling]|
    --Create Sanya's "Placeholder" equipment.
    LM_ExecuteScript(gsItemListing, "Bare Hands")
    LM_ExecuteScript(gsItemListing, "Anger")
    
    --Remove Sanya's equipment.
    AdvCombat_SetProperty("Push Party Member", "Sanya")
        AdvCombatEntity_SetProperty("Equip Item To Slot", "Weapon", "Bare Hands")
        AdvCombatEntity_SetProperty("Equip Item To Slot", "Ammo", "Anger")
    DL_PopActiveObject()
    
    --Remove Sanya's equipment from the inventory.
    AdInv_SetProperty("Remove Item", "Enfield Rifle")
    AdInv_SetProperty("Remove Item", "7.62x56mmR Rounds")
    
    --Doctor Bag is lost.
    gbAutoSetDoctorBagCurrentValues = false
    VM_SetVar("Root/Variables/CrossChapter/DoctorBag/iDoctorBagChargesMax", "N", 0)
    VM_SetVar("Root/Variables/CrossChapter/DoctorBag/iDoctorBagChargesCur", "N", 0)
    AdInv_SetProperty("Doctor Bag Charges", 0)
    AdInv_SetProperty("Doctor Bag Charges Max", 0)

    -- |[Spawning]|
    --Spawn Izuna. Move her offscreen.
    fnSpecialCharacter("Izuna", -100, -100, gci_Face_South, false, nil)
    EM_PushEntity("Izuna")
        TA_SetProperty("Y Vertical Offset", (12*gciSizePerTile)*-1)
        TA_SetProperty("Ignores Gravity", true)
    DL_PopActiveObject()
    fnCutsceneSetFrame("Izuna", "Fall0")
    
    -- |[Movement]|
    fnCutsceneMove("Sanya", 33.25, 19.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneMove("Sanya", 35.25, 19.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Sanya", 1, -1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ Append("Sanya:[E|Neutral] Would you look at that.[P] An underground cathedral that puts the Sistine Chapel to shame.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] Complete with a communion delivery river down the middle.[P] Guess the mole people are fans of efficiency.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Oh ho, and I see a stairwell in the back.[P] Time to get out - [P][CLEAR]") ]])
    fnCutscene([[ Append("???:[VOICE|Izuna] EEEEEEEKKK!!![P]") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Camera.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 5.0)
        CameraEvent_SetProperty("Focus Position", (49.25 * gciSizePerTile), (11.50 * gciSizePerTile))
    DL_PopActiveObject()
    
    -- |[Movement]|
    --Izuna hits the ground at the end of this.
    fnCutsceneTeleport("Izuna", 49.25, 11.50)
    fnCutsceneSetFrame("Izuna", "Fall0")
    fnCutsceneBlocker()
    for i = 1, 12*gciSizePerTile, 1 do
        fnCutsceneSetHeight("Izuna", ((12*gciSizePerTile)-i)*-1)
        if(i % 2 == 0) then
            fnCutsceneWait(2)
        end
        fnCutsceneBlocker()
    end
    fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
    fnCutsceneBlocker()
    
    --Izuna bounces and travels off to the right.
    fnCutsceneSetFrame("Izuna", "Fall2")
    local cfBounceHeight = -5.0
    for x = 49.25, 51.25, 0.20 do
        
        --Lateral translation.
        fnCutsceneTeleport("Izuna", x, 11.50)
        
        --Compute how high Izuna bounces.
        local fPercent = (x-49.25)/(51.25-49.25)
        local fHeight = math.sin(math.pi * fPercent) * cfBounceHeight
        fnCutsceneSetHeight("Izuna", fHeight)
    
        --Past the halfway point, switch falling frames.
        if(fPercent >= 0.20) then
            fnCutsceneSetFrame("Izuna", "Fall1")
        end
    
        --Timing.
        fnCutsceneWait(3)
        fnCutsceneBlocker()
    end
    
    --Izuna hits the water.
    fnCutsceneTeleport("Izuna", -100.0, -100.0)
    fnCutscene([[ AudioManager_PlaySound("World|BigSplash") ]])
    fnCutsceneBlocker()
    
    --Animate the splash.
    fnCutscene([[ AL_SetProperty("Set Layer Disabled", "Splash0", false) ]])
    fnCutsceneWait(7)
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Set Layer Disabled", "Splash0", true) ]])
    fnCutscene([[ AL_SetProperty("Set Layer Disabled", "Splash1", false) ]])
    fnCutsceneWait(7)
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Set Layer Disabled", "Splash1", true) ]])
    fnCutscene([[ AL_SetProperty("Set Layer Disabled", "Splash2", false) ]])
    fnCutsceneWait(7)
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Set Layer Disabled", "Splash2", true) ]])
    fnCutscene([[ AL_SetProperty("Set Layer Disabled", "Splash3", false) ]])
    fnCutsceneWait(7)
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Set Layer Disabled", "Splash3", true) ]])
    fnCutsceneWait(7)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    --Sanya runs over to help!
    fnCutsceneMove("Sanya", 48.25, 19.50, 2.50)
    fnCutsceneMove("Sanya", 48.25, 12.50, 2.50)
    fnCutsceneMove("Sanya", 49.25, 11.50, 2.50)
    fnCutsceneFace("Sanya", 1, 0)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Jimminy pope![P] Hold on, I'm coming!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(7)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    --Sanya jumps in.
    local cfJumpHeight = -5.0
    for x = 49.25, 51.25, 0.20 do
        
        --Lateral translation.
        fnCutsceneTeleport("Sanya", x, 11.50)
        
        --Compute how high Sanya bounces.
        local fPercent = (x-49.25)/(51.25-49.25)
        local fHeight = math.sin(math.pi * fPercent) * cfBounceHeight
        fnCutsceneSetHeight("Sanya", fHeight)
    
        --Timing.
        fnCutsceneWait(3)
        fnCutsceneBlocker()
    end
    
    --Sanya hits the water.
    fnCutsceneTeleport("Sanya", -100.0, -100.0)
    fnCutscene([[ AudioManager_PlaySound("World|BigSplash") ]])
    fnCutsceneBlocker()
    
    --Animate the splash.
    fnCutscene([[ AL_SetProperty("Set Layer Disabled", "Splash0", false) ]])
    fnCutsceneWait(7)
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Set Layer Disabled", "Splash0", true) ]])
    fnCutscene([[ AL_SetProperty("Set Layer Disabled", "Splash1", false) ]])
    fnCutsceneWait(7)
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Set Layer Disabled", "Splash1", true) ]])
    fnCutscene([[ AL_SetProperty("Set Layer Disabled", "Splash2", false) ]])
    fnCutsceneWait(7)
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Set Layer Disabled", "Splash2", true) ]])
    fnCutscene([[ AL_SetProperty("Set Layer Disabled", "Splash3", false) ]])
    fnCutsceneWait(7)
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Set Layer Disabled", "Splash3", true) ]])
    fnCutsceneWait(7)
    fnCutsceneBlocker()
    
    --Cut to black.
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Wait a bit.
    fnCutsceneWait(300)
    fnCutsceneBlocker()
    
    --Execute script to change Sanya's costume. She loses her rifle.
    fnCutscene([[ VM_SetVar("Root/Variables/Costumes/Sanya/sCostumeHuman", "S", "Normal") ]])
    fnCutscene([[ LM_ExecuteScript(gsCostumeAutoresolve, "Sanya_Human") ]])
    
    --Reposition.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 5.0)
        CameraEvent_SetProperty("Focus Position", (48.75 * gciSizePerTile), (6.50 * gciSizePerTile))
    DL_PopActiveObject()
    fnCutsceneSetHeight("Sanya", 0.0)
    fnCutsceneSetHeight("Izuna", 0.0)
    fnCutsceneTeleport("Sanya", 48.75, 5.50)
    fnCutsceneSetFrame("Sanya", "Crouch")
    fnCutsceneTeleport("Izuna", 48.75, 6.50)
    fnCutsceneSetFrame("Izuna", "LyingClosed")
    fnCutsceneBlocker()
    
    --Fade in.
    fnCutsceneWait(45)
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Wake up...[P] come on, wake up...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneWait(60)
    fnCutsceneSetFrame("Izuna", "LyingOpen")
    fnCutsceneBlocker()
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Izuna", "Neutral") ]])
    fnCutscene([[ Append("Weirdo:[E|Cry] *Cough*[P] Heh, heh.[P] I was...[P] right...[P] ha ha ha...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Offended] Right about what?[P] Hey![P] Stay with me here![B][C]") ]])
    fnCutscene([[ Append("Weirdo:[E|Cry] I'm so cold...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Izuna's healing spell.
    fnCutsceneSetFrame("Izuna", "LyingClosed")
    fnCutsceneWait(50)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|MagicA") ]])
    fnCutsceneSetFrame("Izuna", "LyingHeal0")
    fnCutsceneWait(7)
    fnCutsceneBlocker()
    fnCutsceneSetFrame("Izuna", "LyingHeal1")
    fnCutsceneWait(7)
    fnCutsceneBlocker()
    fnCutsceneSetFrame("Izuna", "LyingHeal2")
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneSetFrame("Izuna", "LyingHeal1")
    fnCutsceneWait(7)
    fnCutsceneBlocker()
    fnCutsceneSetFrame("Izuna", "LyingHeal0")
    fnCutsceneWait(7)
    fnCutsceneBlocker()
    fnCutsceneSetFrame("Izuna", "LyingClosed")
    fnCutsceneWait(120)
    fnCutsceneBlocker()
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] The heck was that?[P] Hey![P] Wake up!") ]])
    fnCutsceneBlocker()
    
    --Sanya moves back a bit.
    fnCutsceneSetFrame("Sanya", "Null")
    fnCutsceneMoveFace("Sanya", 48.75, 5.00, 0, 1, 0.40)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])
    fnCutscene([[ Append("Sanya:[E|Sad] She seems to be sleeping.[P] She's still breathing at least.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] What the heck is going on here?[P] Why is a cosplayer in the middle of a mole people kingdom buried under a mountain?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Blush] ...[P] Damn she's cute.[P] Those ears look great, and the tail and robes.[P] I wonder if she made it herself.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Sad] Hmm...[P] yeah, her legs are broken.[P] Spine's probably not doing so hot either.[P] And - [B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Offended] Aw cripe![P] Damn it![P] Friggin -[P] ARGH![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Sad] I don't have my medical bag and -[P] ugh, I must have lost all my stuff when I dove in.[P] Stupid![P] Think, Sanya![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Sad] Okay, lost my gun -[P] gonna catch hell for that.[P] Phone's gone, first aid kit is gone...[P] wallet, nope.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] All I've got is this old rune trinket and my rugged charm.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Okay, got to get her someplace warm and dry so she doesn't die of hypothermia.[P] All right, up we go.[P] Hup hup!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Move Sanya onto Izuna. Switch costumes to Carrying.
    fnCutscene([[ VM_SetVar("Root/Variables/Costumes/Sanya/sCostumeHuman", "S", "Carrying") ]])
    fnCutscene([[ VM_SetVar("Root/Variables/Costumes/Sanya/sCostumeSevavi", "S", "Carrying") ]])
    fnCutsceneMove("Sanya", 48.75, 6.50, 0.50) 
    fnCutsceneBlocker()
    fnCutsceneTeleport("Izuna", -100.0, -100.0)
    fnCutscene([[ AudioManager_PlaySound("World|TakeItem") ]])
    fnCutscene([[ LM_ExecuteScript(gsCharacterAutoresolve, "Sanya") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Phew, she's light as a feather![P] All that time at the gym is paying off in spades!") ]])
    fnCutsceneBlocker()

--Entrance to the shrine.
elseif(sObjectName == "RageTime") then

    -- |[Repeat Check]|
    local iEmpressMetSevavi = VM_GetVar("Root/Variables/Chapter2/ShrineOfTheHero/iEmpressMetSevavi", "N")
    local iEmpressJoined    = VM_GetVar("Root/Variables/Chapter2/MtSarulente/iEmpressJoined", "N")
    if(iEmpressMetSevavi == 1.0 or iEmpressJoined == 0.0) then return end
    
    -- |[Flag]|
    VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iEmpressMetSevavi", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter2/IzunaExplains/iExplainedShrineMountain", "N", 1.0)
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("Empress:[E|Neutral] Now Sanya, you informed me you had not seen a map of any sort while you were here?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Nope.[P] Other than the one Izuna gave me later.[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] Hm.[P] Interesting...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(65)
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    -- |[Transition]|
    fnCutscene([[ AL_BeginTransitionTo("LowerShrineF", "FORCEPOS:2.0x2.0x0") ]])
    fnCutsceneBlocker()

-- |[ =================================== Izuna Explanation ==================================== ]|
elseif(sObjectName == "Explanation") then

    -- |[Repeat Check]|
    local iExplainedShrineMountain = VM_GetVar("Root/Variables/Chapter2/IzunaExplains/iExplainedShrineMountain", "N")
    if(iExplainedShrineMountain == 1.0) then return end
    
    -- |[Activation Check]|
    local iCompletedHunt = VM_GetVar("Root/Variables/Chapter2/Northwoods/iCompletedHunt", "N")
    if(iCompletedHunt == 0.0) then return end
    
    -- |[Variables]|
    local iEmpressJoined = VM_GetVar("Root/Variables/Chapter2/MtSarulente/iEmpressJoined", "N")
    
    -- |[Flag]|
    VM_SetVar("Root/Variables/Chapter2/IzunaExplains/iExplainedShrineMountain", "N", 1.0)
    
    -- |[Execution]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("Izuna:[E|Explain] Sanya, this is Hero's Shrine Mountain.[P] You know, above us.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] Named very literally, is it?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] It's also called the 'Bull's Horn', but the name in the official history books is 'Hero's Shrine Mountain'.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] Obviously it got that name once the shrine was built inside it.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Laugh] Nah, they should have called it that before![B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Ugh] Oh you know what I mean, smartass.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] So, what's with that underground river and stuff?[P] I saw another one in the mines near the cabin.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Explain] Underground rivers criss-cross the glacier as part of the old empire's irrigation system.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] They dug through the mountains to redirect rivers?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Explain] Yeah![P] Though mostly making use of existing cave systems.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Explain] Trafal has a lot of underground caves.[P] The people here grow food in them year-round, thanks to the aqueducts made back then.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Explain] Harvests take a long time but the growing season never ends.[P] It is -[P] not the tastiest, but better than nothing.[B][C]") ]])
    if(iEmpressJoined == 1.0) then
        fnCutscene([[ Append("Empress:[E|Neutral] I was raised on such underground produce.[P] It is an...[P] acquired taste.[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Ugh] You can say that again...[B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] I once held a contest for chefs to try to cook something from it.[P] The best dish was bland.[P] The rest were worse.[B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] Hunger is the best spice.[P] That should say enough.[B][C]") ]])
    end
    fnCutscene([[ Append("Sanya:[E|Smirk] Why this mountain?[P] Why here?[P] Why build a shrine specifically on top of this mountain?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] I'm not sure.[P] Apparently, you were to appear at this location, but the old legend doesn't say why.[B][C]") ]])
    if(iEmpressJoined == 1.0) then
        fnCutscene([[ Append("Empress:[E|Neutral] Fate chose the mountain, not us. It was quite clear that you would arrive here, it was not clear why.[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Smirk] I suppose you had to arrive somewhere.[P] One mountain is as good as another.[B][C]") ]])
    else
        fnCutscene([[ Append("Izuna:[E|Neutral] It's not like it's close to Perfect Hatred or anything.[P] It's an otherwise unremarkable place.[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Smirk] I suppose you had to arrive somewhere.[P] One mountain is as good as another.[B][C]") ]])
    end
    fnCutscene([[ Append("Zeke:[E|Neutral] Bah![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Sad] Hey wait a minute -[P] why is Zeke here?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Sad] Did the legends say anything about him?[B][C]") ]])
    if(iEmpressJoined == 0.0) then
        fnCutscene([[ Append("Izuna:[E|Neutral] No, no they didn't.[B][C]") ]])
    else
        fnCutscene([[ Append("Empress:[E|Neutral] He was not mentioned.[B][C]") ]])
    end
    fnCutscene([[ Append("Sanya:[E|Sad] Strange.[P] Oh well.[P] So what's in the shrine that's so important?[B][C]") ]])
    if(iEmpressJoined == 1.0) then
        fnCutscene([[ Append("Empress:[E|Neutral] A map that would show you where to go, and the sevavi who would tell you of your task.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] Nifty.[P] Let's go get that map, then.[B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] We don't need it, I already know where the trials are.[P] I just...[P] need to talk to the sevavi.") ]])
    else
        fnCutscene([[ Append("Izuna:[E|Explain] I'm not sure, but I know your runestone will be needed inside.[P] Let's go search!") ]])
    end
    fnCutsceneBlocker()
end
