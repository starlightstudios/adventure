-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
if(sObjectName == "TiredA") then
    
    --Repeat check.
    local iTiredA = VM_GetVar("Root/Variables/Chapter2/ShrineOfTheHero/iTiredA", "N")
    if(iTiredA == 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iTiredA", "N", 1.0)
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] *Huff*[P] *Puff*") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TiredB") then
    
    --Repeat check.
    local iTiredB = VM_GetVar("Root/Variables/Chapter2/ShrineOfTheHero/iTiredB", "N")
    if(iTiredB == 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iTiredB", "N", 1.0)
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] This...[P] *Pant*[P] Sucks...[P] *Puff*") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TiredC") then
    
    --Repeat check.
    local iTiredC = VM_GetVar("Root/Variables/Chapter2/ShrineOfTheHero/iTiredC", "N")
    if(iTiredC == 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter2/ShrineOfTheHero/iTiredC", "N", 1.0)
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] I hate...[P] stairs...") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "NoBackA") then

    --Must have not escaped yet.
    local iEscapedShrine = VM_GetVar("Root/Variables/Chapter2/ShrineOfTheHero/iEscapedShrine", "N")
    local iTiredA = VM_GetVar("Root/Variables/Chapter2/ShrineOfTheHero/iTiredA", "N")
    if(iEscapedShrine == 1.0 or iTiredA == 0.0) then return end
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] (No way in hell am I going back down these stupid stairs!)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(15)
    fnCutsceneBlocker()

    --Movement.
    fnCutsceneMove("Sanya", 3.75, 10.10)
    fnCutsceneBlocker()


elseif(sObjectName == "NoBackB") then

    --Must have not escaped yet.
    local iEscapedShrine = VM_GetVar("Root/Variables/Chapter2/ShrineOfTheHero/iEscapedShrine", "N")
    local iTiredB = VM_GetVar("Root/Variables/Chapter2/ShrineOfTheHero/iTiredB", "N")
    if(iEscapedShrine == 1.0 or iTiredB == 0.0) then return end
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] (No way in hell am I going back down these stupid stairs!)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(15)
    fnCutsceneBlocker()

    --Movement.
    fnCutsceneMove("Sanya", 5.25, 5.00)

elseif(sObjectName == "NoBackC") then

    --Must have not escaped yet.
    local iEscapedShrine = VM_GetVar("Root/Variables/Chapter2/ShrineOfTheHero/iEscapedShrine", "N")
    local iTiredC = VM_GetVar("Root/Variables/Chapter2/ShrineOfTheHero/iTiredC", "N")
    if(iEscapedShrine == 1.0 or iTiredC == 0.0) then return end
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] (No way in hell am I going back down these stupid stairs!)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(15)
    fnCutsceneBlocker()

    --Movement.
    fnCutsceneMove("Sanya", 6.75, 7.50)

end
