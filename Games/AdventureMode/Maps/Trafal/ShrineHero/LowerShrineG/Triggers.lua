-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
if(sObjectName == "RageA") then

    -- |[Blackout]|
    fnCutscene([[ AL_SetProperty("Activate Fade", 0, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()

    -- |[Setup]|
    fnCutsceneTeleport("Sanya", 9.25, 18.50)
    fnCutsceneFace("Sanya", 1, 0)
    fnCutsceneTeleport("Izuna", 9.25, 19.50)
    fnCutsceneFace("Izuna", 1, 0)
    fnCutsceneTeleport("Zeke",  9.25, 17.50)
    fnCutsceneFace("Zeke", 1, 0)
    fnCutsceneTeleport("Empress", 11.25, 18.50)
    fnCutsceneFace("Empress", 1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    -- |[Fade In]|
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(185)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneFace("Empress", -1, 0)
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("Empress:[E|Neutral] I am noticing a pattern.[P] Izuna, how did you get into the shrine if the bridge is destroyed and the passage blocked?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Explain] I crawled in![P] There's a hole up near the top, and I managed to squeeze in![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] The advantages of being small.[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] Indeed.[P] It is quite an advantage.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Sad] *Uh, Izuna, do you think she's mad at us?*[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Cry] *I don't know but she sounds like she's mad.*") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(65)
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    -- |[Transition]|
    fnCutscene([[ AL_BeginTransitionTo("HeroShrineA", "FORCEPOS:2.0x2.0x0") ]])
    fnCutsceneBlocker()

end
