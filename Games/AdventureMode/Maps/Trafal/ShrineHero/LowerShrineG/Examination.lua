-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= Exits ========================================== ]|
if(sObjectName == "SampleExit") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("LevelName", "FORCEPOS:28.0x16.0x0")
    
-- |[ ====================================== Examinables ======================================= ]|
elseif(sObjectName == "Collapse") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Looks like the roof collapsed here.[P] There's a tiny passage high up but I don't think I can crawl through it.)") ]])
    fnCutsceneBlocker()

-- |[ ==================================== Pedestal Series ===================================== ]|
elseif(sObjectName == "Pedestal") then

    -- |[Variables]|
    local iHasSevaviForm = VM_GetVar("Root/Variables/Global/Sanya/iHasSevaviForm", "N")

    -- |[Doesn't Have Statue]|
    if(iHasSevaviForm == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (A stone pedestal.[P] Nothing is written on or around it.)") ]])

        --Decision script is this script. It must be surrounded by quotes.
        local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
        fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Look Closer\", " .. sDecisionScript .. ", \"Closer\") ")
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Leave It\",  " .. sDecisionScript .. ", \"Stop\") ")
        fnCutsceneBlocker()
    
    -- |[Has Statue]|
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (A stone pedestal.[P] These turn people into statues when stood on.[P] Can't recommend it enough.)") ]])
        fnCutsceneBlocker()
    end

elseif(sObjectName == "Closer") then
	WD_SetProperty("Hide")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (It looks like there's a spot to stand.[P] Something is calling me to stand there...)") ]])

    --Decision script is this script. It must be surrounded by quotes.
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Stand on it\", " .. sDecisionScript .. ", \"Stand\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Leave It\",  " .. sDecisionScript .. ", \"Stop\") ")
    fnCutsceneBlocker()

elseif(sObjectName == "Stand") then
	WD_SetProperty("Hide")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (...[P] Maybe I should strike a pose?)") ]])

    --Decision script is this script. It must be surrounded by quotes.
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Pose\", " .. sDecisionScript .. ", \"Pose\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Leave It\",  " .. sDecisionScript .. ", \"Stop\") ")
    fnCutsceneBlocker()
    
elseif(sObjectName == "Pose") then
	WD_SetProperty("Hide")
    LM_ExecuteScript(gsRoot .. "Chapter 2/Scenes/400 Volunteer/Sevavi/Scene_Begin.lua")

elseif(sObjectName == "Stop") then
	WD_SetProperty("Hide")

-- |[ ========================================== Other ========================================= ]|
-- |[ ========================================== Debug ========================================= ]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
