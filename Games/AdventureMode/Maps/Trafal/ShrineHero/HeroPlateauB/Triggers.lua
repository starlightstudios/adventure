-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
if(sObjectName == "Time2") then
    
    --Variables.
    local iTime           = VM_GetVar("Root/Variables/Chapter2/HeroPlateau/iTime", "N")
    local iIgnoreTime     = VM_GetVar("Root/Variables/Chapter2/HeroPlateau/iIgnoreTime", "N")
    local iCompletedMines = VM_GetVar("Root/Variables/Chapter2/SanyaMines/iCompletedMines", "N")
    if(iTime >= 2.0 or iCompletedMines == 1.0) then return end

    --Flag.
    VM_SetVar("Root/Variables/Chapter2/HeroPlateau/iTime", "N", 2.0)
    
    --Constants.
    local iFadeTicks = 125
    
    --Crossfade:
    if(iTime >= 1.0 and iTime <= 5.0) then
        local iOldSlot = math.floor(iTime)
        local iCurSlot = 2
        local faOldColor = gcfaPlateauColor[iOldSlot]
        local faCurColor = gcfaPlateauColor[iCurSlot]
		AL_SetProperty("Activate Fade", iFadeTicks, gci_Fade_Under_GUI, true, faOldColor[1], faOldColor[2], faOldColor[3], faOldColor[4], faCurColor[1], faCurColor[2], faCurColor[3], faCurColor[4], true)
    
    --Error, so just snap the color:
    else
        local iOldSlot = 2
        local iCurSlot = 2
        local faOldColor = gcfaPlateauColor[iOldSlot]
        local faCurColor = gcfaPlateauColor[iCurSlot]
		AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, faOldColor[1], faOldColor[2], faOldColor[3], faOldColor[4], faCurColor[1], faCurColor[2], faCurColor[3], faCurColor[4], true)
    end
    
elseif(sObjectName == "Time3") then
    
    --Variables.
    local iTime       = VM_GetVar("Root/Variables/Chapter2/HeroPlateau/iTime", "N")
    local iIgnoreTime = VM_GetVar("Root/Variables/Chapter2/HeroPlateau/iIgnoreTime", "N")
    local iCompletedMines = VM_GetVar("Root/Variables/Chapter2/SanyaMines/iCompletedMines", "N")
    if(iTime >= 3.0 or iCompletedMines == 1.0) then return end

    --Flag.
    VM_SetVar("Root/Variables/Chapter2/HeroPlateau/iTime", "N", 3.0)
    
    --Constants.
    local iFadeTicks = 125
    
    --Crossfade:
    if(iTime >= 1.0 and iTime <= 5.0) then
        local iOldSlot = math.floor(iTime)
        local iCurSlot = 3
        local faOldColor = gcfaPlateauColor[iOldSlot]
        local faCurColor = gcfaPlateauColor[iCurSlot]
		AL_SetProperty("Activate Fade", iFadeTicks, gci_Fade_Under_GUI, true, faOldColor[1], faOldColor[2], faOldColor[3], faOldColor[4], faCurColor[1], faCurColor[2], faCurColor[3], faCurColor[4], true)
    
    --Error, so just snap the color:
    else
        local iOldSlot = 3
        local iCurSlot = 3
        local faOldColor = gcfaPlateauColor[iOldSlot]
        local faCurColor = gcfaPlateauColor[iCurSlot]
		AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, faOldColor[1], faOldColor[2], faOldColor[3], faOldColor[4], faCurColor[1], faCurColor[2], faCurColor[3], faCurColor[4], true)
    end
    
elseif(sObjectName == "Time4") then
    
    --Variables.
    local iTime       = VM_GetVar("Root/Variables/Chapter2/HeroPlateau/iTime", "N")
    local iIgnoreTime = VM_GetVar("Root/Variables/Chapter2/HeroPlateau/iIgnoreTime", "N")
    local iCompletedMines = VM_GetVar("Root/Variables/Chapter2/SanyaMines/iCompletedMines", "N")
    if(iTime >= 4.0 or iCompletedMines == 1.0) then return end

    --Flag.
    VM_SetVar("Root/Variables/Chapter2/HeroPlateau/iTime", "N", 4.0)
    
    --Constants.
    local iFadeTicks = 125
    
    --Crossfade:
    if(iTime >= 1.0 and iTime <= 5.0) then
        local iOldSlot = math.floor(iTime)
        local iCurSlot = 4
        local faOldColor = gcfaPlateauColor[iOldSlot]
        local faCurColor = gcfaPlateauColor[iCurSlot]
		AL_SetProperty("Activate Fade", iFadeTicks, gci_Fade_Under_GUI, true, faOldColor[1], faOldColor[2], faOldColor[3], faOldColor[4], faCurColor[1], faCurColor[2], faCurColor[3], faCurColor[4], true)
    
    --Error, so just snap the color:
    else
        local iOldSlot = 4
        local iCurSlot = 4
        local faOldColor = gcfaPlateauColor[iOldSlot]
        local faCurColor = gcfaPlateauColor[iCurSlot]
		AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, faOldColor[1], faOldColor[2], faOldColor[3], faOldColor[4], faCurColor[1], faCurColor[2], faCurColor[3], faCurColor[4], true)
    end
    
-- |[ =================================== Izuna Explanation ==================================== ]|
elseif(sObjectName == "IzunaExplain") then

    -- |[Repeat Check]|
    local iExplainedShrineMountain = VM_GetVar("Root/Variables/Chapter2/IzunaExplains/iExplainedShrineMountain", "N")
    if(iExplainedShrineMountain == 1.0) then return end
    
    -- |[Activation Check]|
    local iCompletedHunt = VM_GetVar("Root/Variables/Chapter2/Northwoods/iCompletedHunt", "N")
    if(iCompletedHunt == 0.0) then return end
    
    -- |[Variables]|
    local iEmpressJoined = VM_GetVar("Root/Variables/Chapter2/MtSarulente/iEmpressJoined", "N")
    
    -- |[Flag]|
    VM_SetVar("Root/Variables/Chapter2/IzunaExplains/iExplainedShrineMountain", "N", 1.0)
    
    -- |[Execution]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("Izuna:[E|Explain] Sanya, this is Hero's Shrine Mountain.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] Named very literally, is it?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] It's also called the 'Bull's Horn', but the name in the official history books is 'Hero's Shrine Mountain'.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] Obviously it got that name once the shrine was built inside it.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Laugh] Nah, they should have called it that before![B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Ugh] Oh you know what I mean, smartass.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] So, what's with that underground river and stuff?[P] I saw another one in the mines near the cabin.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Explain] Underground rivers criss-cross the glacier as part of the old empire's irrigation system.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] They dug through the mountains to redirect rivers?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Explain] Yeah![P] Though mostly making use of existing cave systems.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Explain] Trafal has a lot of underground caves.[P] The people here grow food in them year-round, thanks to the aqueducts made back then.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Explain] Harvests take a long time but the growing season never ends.[P] It is -[P] not the tastiest, but better than nothing.[B][C]") ]])
    if(iEmpressJoined == 1.0) then
        fnCutscene([[ Append("Empress:[E|Neutral] I was raised on such underground produce.[P] It is an...[P] acquired taste.[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Ugh] You can say that again...[B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] I once held a contest for chefs to try to cook something from it.[P] The best dish was bland.[P] The rest were worse.[B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] Hunger is the best spice.[P] That should say enough.[B][C]") ]])
    end
    fnCutscene([[ Append("Sanya:[E|Smirk] Why this mountain?[P] Why here?[P] Why build a shrine specifically on top of this mountain?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] I'm not sure.[P] Apparently, you were to appear at this location, but the old legend doesn't say why.[B][C]") ]])
    if(iEmpressJoined == 1.0) then
        fnCutscene([[ Append("Empress:[E|Neutral] Fate chose the mountain, not us.[P] It was quite clear that you would arrive here, it was not clear why.[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Smirk] I suppose you had to arrive somewhere.[P] One mountain is as good as another.[B][C]") ]])
    else
        fnCutscene([[ Append("Izuna:[E|Neutral] It's not like it's close to Perfect Hatred or anything.[P] It's an otherwise unremarkable place.[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Smirk] I suppose you had to arrive somewhere.[P] One mountain is as good as another.[B][C]") ]])
    end
    fnCutscene([[ Append("Zeke:[E|Neutral] Bah![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Sad] Hey wait a minute -[P] why is Zeke here?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Sad] Did the legends say anything about him?[B][C]") ]])
    if(iEmpressJoined == 1.0) then
        fnCutscene([[ Append("Empress:[E|Neutral] He was not mentioned.[B][C]") ]])
    else
        fnCutscene([[ Append("Izuna:[E|Neutral] No, no they didn't.[B][C]") ]])
    end
    fnCutscene([[ Append("Sanya:[E|Sad] Strange.[P] Oh well.[P] So what's in the shrine that's so important?[B][C]") ]])
    if(iEmpressJoined == 1.0) then
        fnCutscene([[ Append("Empress:[E|Neutral] A map that would show you where to go, and the sevavi who would tell you of your task.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] Nifty.[P] Let's go get that map, then.[B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] We don't need it, I already know where the trials are.[P] I just...[P] need to talk to the sevavi.") ]])
    else
        fnCutscene([[ Append("Izuna:[E|Explain] I'm not sure, but I know your runestone will be needed inside.[P] Let's go search!") ]])
    end
    fnCutsceneBlocker()
end
