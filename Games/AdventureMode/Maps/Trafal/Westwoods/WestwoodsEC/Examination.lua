-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= Exits ========================================== ]|
-- |[ ====================================== Examinables ======================================= ]|
if(sObjectName == "Book") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Looks like the harpies have set up on the cliff.[P] They tried to build an outpost, but abandoned it.[P] Likely due to the monster population.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('They left behind some water and supplies, but nothing major.[P] Moving into Westwoods will be extremely dangerous, and the harpies have the high ground.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Must be a report from some bandit toady.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Barrels") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Barrels of alcohol, smells like rum.[P] The bandits have consumed most of it.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Water") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Barrels of pure water, a commodity in this part of the world.)") ]])
    fnCutsceneBlocker()
    
-- |[ ========================================== Other ========================================= ]|
-- |[ ========================================== Debug ========================================= ]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
