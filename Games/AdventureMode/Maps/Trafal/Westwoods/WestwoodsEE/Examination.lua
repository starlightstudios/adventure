-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= Exits ========================================== ]|
-- |[ ====================================== Examinables ======================================= ]|
if(sObjectName == "Bookshelf") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The books are all rotted and waterlogged.[P] Nothing is legible.[P] I guess nobody has bothered to get rid of them.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Book") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Our great leader sent us to scout this hellhole.[P] There's nothing to eat or drink, and plenty of things to sting you.[P] The merchants, however, are easy pickings.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('They're already exhausted from the wildlife, and nobody dares to camp here.[P] Hit them towards the end of the route and they'll be exhausted.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (That's all there is.[P] Must be a report written by the bandits here.)") ]])
    fnCutsceneBlocker()
    
-- |[ ========================================== Other ========================================= ]|
-- |[ ========================================== Debug ========================================= ]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
