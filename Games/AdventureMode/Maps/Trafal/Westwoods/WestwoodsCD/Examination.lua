-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= Exits ========================================== ]|
if(sObjectName == "ToWestwoodsCC") then
    
    --If not opened:
    local iBefriendedAlraunes = VM_GetVar("Root/Variables/Chapter2/FortKirysu/iBefriendedAlraunes", "N")
    local iSpawnMarigold      = VM_GetVar("Root/Variables/Chapter2/FortKirysu/iSpawnMarigold", "N")
    if(iBefriendedAlraunes == 0.0 or iSpawnMarigold == 1.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (It's locked.[P] Which is odd because it's a ruined fort.)") ]])
        fnCutsceneBlocker()
    else
        AudioManager_PlaySound("World|FlipSwitch")
        AL_BeginTransitionTo("WestwoodsCC", "FORCEPOS:42.0x27.0x0")
    end
    
-- |[ ====================================== Examinables ======================================= ]|
-- |[ ========================================== Other ========================================= ]|
-- |[ ========================================== Debug ========================================= ]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
