-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
if(sObjectName == "EmpressWaitN" or sObjectName == "EmpressWaitS") then
    
    -- |[Repeat Check]|
    local iEmpressSawTree    = VM_GetVar("Root/Variables/Chapter2/FortKirysu/iEmpressSawTree", "N")
    local iEmpressExtraScene = VM_GetVar("Root/Variables/Chapter2/FortKirysu/iEmpressExtraScene", "N")
    if(iEmpressSawTree == 0.0 or iEmpressExtraScene == 1.0) then return end
    
    -- |[Flag]|
    VM_SetVar("Root/Variables/Chapter2/FortKirysu/iEmpressExtraScene", "N", 1.0)
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("Empress:[E|Neutral] Hm, hold a moment.[P] I seem to have misplaced my broach.[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] I likely left it in the fort.[P] I will be right back.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] Okay![P] Don't take too long!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Transition.
    fnCutscene([[ AL_BeginTransitionTo("WestwoodsCC", "FORCEPOS:7.0x2.0x0") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "Recovery") then

    -- |[Fade]|
    fnCutscene([[ AL_SetProperty("Activate Fade", 0, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    -- |[Position]|
    fnCutsceneTeleport("Sanya", 33.25, 20.50)   
    fnCutsceneFace("Sanya", 1, 0)
    fnCutsceneTeleport("Izuna", 33.25, 21.50)   
    fnCutsceneFace("Izuna", 1, 0)
    fnCutsceneTeleport("Zeke", 33.25, 22.50)   
    fnCutsceneFace("Zeke", 1, 0)
    fnCutsceneTeleport("Empress", 21.25, 21.50)  
    fnCutsceneBlocker() 
    
    -- |[Fade In]|
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneMove("Empress", 32.25, 21.50)
    fnCutsceneBlocker()
    fnCutsceneFace("Sanya", -1, 0)
    fnCutsceneFace("Izuna", -1, 0)
    fnCutsceneFace("Zeke", -1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("Izuna:[E|Neutral] Did you find whatever you lost?[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] Yes.[P] We can head out now.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Finish Up]|
    fnAutoFoldParty()
    fnCutsceneBlocker()

end
