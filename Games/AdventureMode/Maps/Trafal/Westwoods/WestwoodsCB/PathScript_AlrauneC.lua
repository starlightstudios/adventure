-- |[ ==================================== Movement Script ===================================== ]|
--Movement script used by an entity that paths around.
local sEntityName = "AlrauneC"
local fWalkSpeed = 0.50

-- |[Node Data]|
--List of all nodes this entity can stop at.
local saNodeList = {"C0", "C1", "C2"}

-- |[Run Routine]|
fnNPCPathToRandomNode(sEntityName, fWalkSpeed, saNodeList)
