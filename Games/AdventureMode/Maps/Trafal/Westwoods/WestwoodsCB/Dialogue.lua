-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[Name Resolve]|
    --Resolve the name of the calling entity.
    local sActorName = TA_GetProperty("Name")

    --Set facing.
    TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[Alraunes]|
    if(sActorName == "AlrauneA") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Alraune:[VOICE|Alraune] The task of cleaning never ends in this old fort![P] We've been at it for a week!") ]])
    elseif(sActorName == "AlrauneB") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Alraune:[VOICE|Alraune] The journey here was perilous, but things are so calm in this glade.") ]])
    elseif(sActorName == "AlrauneC") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Alraune:[VOICE|Alraune] I wonder if the unmutated woodland creatures can ever be convinced to come here...") ]])
    elseif(sActorName == "AlrauneD") then
        
        local iSpawnMarigold = VM_GetVar("Root/Variables/Chapter2/FortKirysu/iSpawnMarigold", "N")
        if(iSpawnMarigold == 1.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Alraune:[VOICE|Alraune] Talk to our coven leader first, please.") ]])
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Alraune:[VOICE|Alraune] Sorry if I said anything untoward earlier.[P] There are bandits and other undesirable sorts around.[P] I cannot be too careful.[B][C]") ]])
            fnCutscene([[ Append("Alraune:[VOICE|Alraune] I'll make sure the door on the eastern side of the fort is open when you need it.[P] The little ones will tell me of your approach.") ]])
        end
    end
end
