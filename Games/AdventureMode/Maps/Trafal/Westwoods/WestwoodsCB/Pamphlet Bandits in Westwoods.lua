-- |[ ============================= Pamphlet: Bandits in Westwoods ============================= ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========== Generics ========== ]|
if(sActorName == "AlrauneA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Alraune:[VOICE|Alraune] There was the occasional bandit in the south.[P] I'm sure that my sisters will be able to put them to quick rout.") ]])
    
elseif(sActorName == "AlrauneB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Alraune:[VOICE|Alraune] And you are just giving it away?[P] Oh, it's about the bandits.[P] Yes we had a long trip so we were on the move by time things started to decline, but it was only getting worse.") ]])
    
elseif(sActorName == "AlrauneC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Alraune:[VOICE|Alraune] A steady inflow of bandits is probably feeding the local wildlife[P]. I won't say they deserve it, but you have to be driven or foolish to enter the Westwoods.") ]])
    
elseif(sActorName == "AlrauneD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Alraune:[VOICE|Alraune] The walls are strong, and we need only protect the grandfather.[P] Army of bandits or not, we might make it and heal this place.") ]])
end

-- |[Clean Up]|
DL_PopActiveObject()
