-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
if(sObjectName == "MeetTheRaunes") then

    -- |[Repeat Check]|
    local iSpawnMarigold = VM_GetVar("Root/Variables/Chapter2/FortKirysu/iSpawnMarigold", "N")
    if(iSpawnMarigold ~= 1.0) then return end
    
    -- |[Variables]|
    local iEmpressJoined = VM_GetVar("Root/Variables/Chapter2/MtSarulente/iEmpressJoined", "N")
    
    -- |[Flag]|
    VM_SetVar("Root/Variables/Chapter2/FortKirysu/iSpawnMarigold", "N", 0.0)

    -- |[Movement]|
    fnCutsceneMergeParty()
    fnCutsceneBlocker()
    fnCutsceneMove("Sanya", 7.25, 15.50)
    fnCutsceneFace("Sanya", 1, 0)
    fnCutsceneMove("Izuna", 7.25, 14.50)
    fnCutsceneFace("Izuna", 1, 0)
    if(iEmpressJoined == 0.0) then
        fnCutsceneMove("Zeke", 7.25, 16.50)
        fnCutsceneFace("Zeke", 1, 0)
    else
        fnCutsceneMove("Empress", 7.25, 16.50)
        fnCutsceneFace("Empress", 1, 0)
        fnCutsceneMove("Zeke", 6.25, 15.50)
        fnCutsceneFace("Zeke", 1, 0)
    end
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Marigold", "Neutral") ]])
    fnCutscene([[ Append("Marigold:[E|Neutral] Hello there![P] My name is Marigold, and welcome to my coven's grounds.[B][C]") ]])
    if(iEmpressJoined == 0.0) then
        fnCutscene([[ Append("Izuna:[E|Happy] It's great to meet you![P] I'm Izuna, and this is Sanya and Zeke![B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] Howdy.[B][C]") ]])
        fnCutscene([[ Append("Zeke:[E|Neutral] Nyeeeeeeehhh?[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Sad] Okay so for reference, this is like a talking steak walking up to me.[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Neutral] No Zeke, control yourself.[B][C]") ]])
        fnCutscene([[ Append("Marigold:[E|Neutral] Goats are honored among the alraunes, as are all grazers.[B][C]") ]])
        fnCutscene([[ Append("Zeke:[E|Neutral] Bah![B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] Why's that?[B][C]") ]])
        fnCutscene([[ Append("Marigold:[E|Neutral] Grazers live in harmony with the plants they eat, recycling them into manure which grows the next generation.[B][C]") ]])
        fnCutscene([[ Append("Marigold:[E|Neutral] They are an important part of the cycle of life.[P] We venerate them.[B][C]") ]])
        fnCutscene([[ Append("Marigold:[E|Neutral] ...[P] Just don't eat an alraune, mister Zeke.[P] The grass, by all means.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] Right so, what's the deal with you guys being in this fort?[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Jot] Yeah, it sounds like it'll make a great story![B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Jot] Are you responsible for the blighted earth of Westwoods being restored?[B][C]") ]])
        fnCutscene([[ Append("Marigold:[E|Neutral] No, that was the grandfather who called us here.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Offended] Old people are responsible for this!?[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Explain] No no, alraunes refer to trees as their grandparents![B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] Okay good.[B][C]") ]])
        fnCutscene([[ Append("Marigold:[E|Neutral] My coven and I are from the south, about a month's travel.[P] One night, when we went to sleep, we all had dreams of being called to this place.[B][C]") ]])
        fnCutscene([[ Append("Marigold:[E|Neutral] At first we ignored it, but once we spoke to one another and realized we all had the same dream, we wanted to come here.[B][C]") ]])
        fnCutscene([[ Append("Marigold:[E|Neutral] The dreams came again the next night, and we knew we had to go.[P] So we came here.[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Jot] Plant girls having all the same dream?[P] Interesting...[B][C]") ]])
        fnCutscene([[ Append("Marigold:[E|Neutral] When we arrived, we found this place of beauty and healed earth, in the middle of this blasted hellscape.[B][C]") ]])
        fnCutscene([[ Append("Marigold:[E|Neutral] The grandfather here was calling for help, so we are here now.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] So a tree is responsible for the grass and clean water.[P] That's pretty neat.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Blush] Hey can you turn me into a plant?[P] I've got a runestone and stuff, it's no big deal.[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Blush] (Oh my goodness, imagine Sanya in a leafy bikini...[P] blue skin, shapely plant legs...)[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Blush] (And a big blue butt out to here...)[B][C]") ]])
        fnCutscene([[ Append("Marigold:[E|Neutral] Unfortunately, we cannot turn anyone into alraunes for some time.[P] We have only just arrived.[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Ugh] (Curses.)[B][C]") ]])
        fnCutscene([[ Append("Marigold:[E|Neutral] To create a pool with the pollen of the little ones will require considerably more time, and more little ones.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] Rats.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] But since I'm a great hero charged with saving the world, we're on the same team, you know?[P] You're saving this part of the world.[B][C]") ]])
        fnCutscene([[ Append("Marigold:[E|Neutral] A great hero?[P] It is an honor![B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Explain] If you need any help, the kitsune temple is just northeast of here![P] I'm sure they'll want to help restore Westwoods![B][C]") ]])
        fnCutscene([[ Append("Marigold:[E|Neutral] My thanks.[P] Oh, I've unlocked the eastern door in case you need to go that way.[B][C]") ]])
        fnCutscene([[ Append("Marigold:[E|Neutral] Now I must go and attempt to commune with the grandfather.[P] He is not talktative, but I know if I keep trying I will be able to speak to him.[B][C]") ]])
        fnCutscene([[ Append("Marigold:[E|Neutral] Feel free to come see him.[P] I should be getting back to it.[P] It is very nice meeting you.") ]])
        
    else
        fnCutscene([[ Append("Empress:[E|Neutral] So you have elected to make Fort Kirysu your home?[P] Why?[B][C]") ]])
        fnCutscene([[ Append("Marigold:[E|Neutral] I don't believe I got your name.[B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] I am Empress Arulente.[P] These are my entourage, Sanya, Izuna, and Zeke.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] Howdy.[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Happy] It's great to meet you![B][C]") ]])
        fnCutscene([[ Append("Zeke:[E|Neutral] ...[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] Zeke say bah.[B][C]") ]])
        fnCutscene([[ Append("Zeke:[E|Neutral] Bah.[B][C]") ]])
        fnCutscene([[ Append("Marigold:[E|Neutral] Well then, to your question.[P] This place is called Fort Kirysu?[B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] It was named after a minister of trade in the old empire.[P] It was his idea to route the imperial highway through this area, and build this fort to protect it.[B][C]") ]])
        fnCutscene([[ Append("Marigold:[E|Neutral] My, you are knowledgable.[P] Are you a scholar of imperial history?[B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] Something like that.[B][C]") ]])
        fnCutscene([[ Append("Marigold:[E|Neutral] My coven and I are from the south, about a month's travel.[P] One night, when we went to sleep, we all had dreams of being called to this place.[B][C]") ]])
        fnCutscene([[ Append("Marigold:[E|Neutral] At first we ignored it, but once we spoke to one another and realized we all had the same dream, we wanted to come here.[B][C]") ]])
        fnCutscene([[ Append("Marigold:[E|Neutral] The dreams came again the next night, and we knew we had to go.[P] So we came here.[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Jot] Plant girls having all the same dream?[P] Interesting...[P] Do you mind if I do a story on this?[B][C]") ]])
        fnCutscene([[ Append("Marigold:[E|Neutral] Go right ahead.[B][C]") ]])
        fnCutscene([[ Append("Marigold:[E|Neutral] When we arrived, we found this place of beauty and healed earth, in the middle of this blasted hellscape.[B][C]") ]])
        fnCutscene([[ Append("Marigold:[E|Neutral] The grandfather here was calling for help, so we are here now.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] Grandfather?[B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] Alraunes refer to trees as their grandparents.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] Well that's neat.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Blush] Hey can you turn me into a plant?[P] I've got a runestone and stuff, it's no big deal.[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Blush] (Oh my goodness, imagine Sanya in a leafy bikini...[P] blue skin, shapely plant legs...)[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Blush] (And a big blue butt out to here...)[B][C]") ]])
        fnCutscene([[ Append("Marigold:[E|Neutral] Unfortunately, we cannot turn anyone into alraunes for some time.[P] We have only just arrived.[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Ugh] (Curses.)[B][C]") ]])
        fnCutscene([[ Append("Marigold:[E|Neutral] To create a pool with the pollen of the little ones will require considerably more time, and more little ones.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] Rats.[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Jot] So the blasted earth of Westwoods actually can be saved?[B][C]") ]])
        fnCutscene([[ Append("Marigold:[E|Neutral] Yes, and we intend to save it.[P] I hope we can make friends with the other peoples of Trafal and that they will aid us.[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Explain] Count the kitsunes in![B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Laugh] And us too![P] If you need any weeds trimmed, you just call Zeke![B][C]") ]])
        fnCutscene([[ Append("Zeke:[E|Neutral] Bah![P] Bah![B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] ...[P] Where is this grandfather?[B][C]") ]])
        fnCutscene([[ Append("Marigold:[E|Neutral] In the clearing in the middle of the fort.[P] I have been trying to commune with him for some time, but he does not speak.[B][C]") ]])
        fnCutscene([[ Append("Marigold:[E|Neutral] Feel free to come see him.[P] I should be getting back to it, actually.[P] It is very nice meeting you.[B][C]") ]])
        fnCutscene([[ Append("Marigold:[E|Neutral] Oh, I've unlocked the eastern door in case you need to go that way.[P] Good luck, my friends.") ]])
    end
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneMove("Marigold", 23.25, 15.50)
    fnCutsceneMove("Marigold", 23.25, 16.50)
    fnCutsceneBlocker()
    fnCutsceneTeleport("Marigold", -1.25, -1.50)
    fnCutscenePlaySound("World|FlipSwitch")
    fnCutsceneBlocker()
    fnAutoFoldParty()
    fnCutsceneBlocker()
end
