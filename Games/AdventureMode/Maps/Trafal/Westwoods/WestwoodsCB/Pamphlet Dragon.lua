-- |[ ==================================== Pamphlet: Dragon ==================================== ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========== Generics ========== ]|
if(sActorName == "AlrauneA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Alraune:[VOICE|Alraune] And you say the people here believe in this prophecy and this legendary hero?[P] I mean dreams is one thing but prophecy really?[B][C]") ]])
    fnCutscene([[ Append("Alraune:[VOICE|Alraune] Everyone![P] Everyone![P] There's some sort of prophecy afoot.[P] Oho![P] How very provincial.") ]])
    
elseif(sActorName == "AlrauneB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Alraune:[VOICE|Alraune] Oh, some sort of writing?[P] So there's this big legend thing coming true.[P] That might explain why the trip was so dangerous.[P] Although it's always been pretty wild down there.") ]])
    
elseif(sActorName == "AlrauneC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Alraune:[VOICE|Alraune] I wonder if the grandfather was here in the time of the dragon empire?[P] I'd ask the dragon behind you, but I don't want to assume that all dragons were in the dragon empire.[P] It was very long ago.") ]])
    
elseif(sActorName == "AlrauneD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Alraune:[VOICE|Alraune] I'll admit my knowledge of history is mostly gone after I was cleansed, but this fort is well made.[P] Thank you Empress.") ]])
end

-- |[Clean Up]|
DL_PopActiveObject()
