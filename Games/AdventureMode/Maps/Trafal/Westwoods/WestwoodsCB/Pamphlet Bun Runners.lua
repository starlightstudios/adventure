-- |[ ============================= Pamphlet: The Bunny Smugglers ============================== ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========== Generics ========== ]|
if(sActorName == "AlrauneA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Alraune:[VOICE|Alraune] Bunny boys?[P] I can't say we had those back south.[P] You think they'd like to slow down for some tea?[P] A shame they're always in a hurry.") ]])
    
elseif(sActorName == "AlrauneB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Alraune:[VOICE|Alraune] If the bunnies weren't running weapons, then what are the bandits using?[P] Home-made swords?[P] Whoever made it for them, they'll get what's coming to them.") ]])
    
elseif(sActorName == "AlrauneC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Alraune:[VOICE|Alraune] The bunnies were running goods, eh?[P] Unfortunately, unless they are content to haul pure water, we have no use of their services.") ]])
    
elseif(sActorName == "AlrauneD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Alraune:[VOICE|Alraune] I've heard the little ones talk about two legged creatures running around nibbling on things occasionally.[P] I guess that's the bunnies.[B][C]") ]])
    fnCutscene([[ Append("Zeke:[VOICE|Zeke] Bah.[B][C]") ]])
    fnCutscene([[ Append("Alraune:[VOICE|Alraune] They also whisper about you, master goat.") ]])
end

-- |[Clean Up]|
DL_PopActiveObject()
