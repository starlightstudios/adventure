-- |[ =============================== Pamphlet: Green Westwoods =============================== ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========== Generics ========== ]|
if(sActorName == "AlrauneA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Alraune:[VOICE|Alraune] So you have made your little pamphlet have you?[P] Well, don't leave them around here.[P] We don't need any more clutter.") ]])
    
elseif(sActorName == "AlrauneB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Alraune:[VOICE|Alraune] A letter?[P] It's about us![P] Do me a favor and spread these far and wide.[P] More people should know about the work we are doing.[P] It's always an honor and a pleasure to join another leaf sister.") ]])
    
elseif(sActorName == "AlrauneC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Alraune:[VOICE|Alraune] Hey, wait a minute![P] All of your pamphlets are printed on paper, not parchment![P] Where do you get off giving paper to an Alarune![B][C]") ]])
    fnCutscene([[ Append("Izuna:[VOICE|Izuna] But I thought - [B][C]") ]])
    fnCutscene([[ Append("Alraune:[VOICE|Alraune] Ha ha![P] That's a joke, my friend.[P] The little ones are always happy to help by lending their bodies.[P] So long as you do not waste them.") ]])
    
elseif(sActorName == "AlrauneD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Alraune:[VOICE|Alraune] We made the news![P] That our coven was summoned here is a miracle.[P] I like to think it was part of some larger purpose.[P] But maybe the grandfather was just lonely.") ]])
end

-- |[Clean Up]|
DL_PopActiveObject()
