-- |[ ============================= Pamphlet: The Bunny Smugglers ============================== ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ==== Minor Characters ==== ]|
-- |[Alraunes]|
if(sActorName == "AlrauneA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Alraune:[VOICE|Alraune] The bunnies have been running around?[P] You can't hear them but the grandfathers and little ones are always talking.[P] I'll ask them if they know anything.[B][C]") ]])
    fnCutscene([[ Append("Alraune:[VOICE|Alraune] So about half of them are claiming that they knew it all along, and the other half are claiming that the bunnies are too cute to do any harm.[P] No wait, now they are asking if your goat is up to anything.[P] Are you up to anything Mr. Goat?[B][C]") ]])
    fnCutscene([[ Append("Zeke:[VOICE|Zeke] Bah.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Deny everything, Zeke.") ]])

elseif(sActorName == "AlrauneB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Alraune:[VOICE|Alraune] So the bunnies running around are all men?[P] That changes some things.[P] If our numbers start to decline we might have to aggressively recruit.[P] I will do whatever it takes to save this place.") ]])
    
elseif(sActorName == "AlrauneC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Alraune:[VOICE|Alraune] I really think Alraunes should advertise the amount of sunbathing you get to do more. [P]Hey put that in your pamphlet whatever.[P] Also get out of my light, I'm working on a tan here.") ]])
    
elseif(sActorName == "AlrauneD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Alraune:[VOICE|Alraune] I'm sort of swimming but I guess you can read it to me?[B][C]") ]])
    fnCutscene([[ Append("Alraune:[VOICE|Alraune] Oh wow, she made an entire gang of bunny boys through being super good at sex?[P] She must be amazing at orgasm denial.[P] Like wow.") ]])
    
elseif(sActorName == "Marigold") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Marigold:[VOICE|Marigold] If the bunny-boys need a place to rest, I suppose they are welcome here.[P] I know some of the sisters would be...[P] fascinated by their company.") ]])
end

-- |[Clean Up]|
DL_PopActiveObject()
