-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[Name Resolve]|
    --Resolve the name of the calling entity.
    local sActorName = TA_GetProperty("Name")

    --Set facing.
    TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[Alraunes]|
    if(sActorName == "AlrauneA") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Alraune:[VOICE|Alraune] These trees are much, much younger than the one in the middle of this little pond.[P] All of them say that grandfather was here when they grew.") ]])
    elseif(sActorName == "AlrauneB") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Alraune:[VOICE|Alraune] Even birds do not visit this place. We must do the work that the wild animals cannot.") ]])
    elseif(sActorName == "AlrauneC") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Alraune:[VOICE|Alraune] This pond water is so pure, it feels so nice to just sit here and enjoy the sun.") ]])
    elseif(sActorName == "AlrauneD") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Alraune:[VOICE|Alraune] Mmm, the water is wonderful!") ]])
    elseif(sActorName == "Marigold") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Marigold:[VOICE|Marigold] Grandfather seems to have nothing to say.[P] I am certain he can hear me.[P] Why doesn't he speak?") ]])
    end
end
