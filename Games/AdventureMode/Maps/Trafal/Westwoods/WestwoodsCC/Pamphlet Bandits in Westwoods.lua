-- |[ ============================= Pamphlet: Bandits in Westwoods ============================= ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ==== Minor Characters ==== ]|
-- |[Alraunes]|
if(sActorName == "AlrauneA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Alraune:[VOICE|Alraune] Some sort of bandit army?[P] You can't hear them but the grandfathers and little ones are always talking.[P] I'll ask them if they know anything.[B][C]") ]])
    fnCutscene([[ Append("Alraune:[VOICE|Alraune] They don't like war.[P] The ones who have seen it are telling scary stories about about fire and roving bands destroying things for kicks.[P] We will have to be ready!") ]])
    
elseif(sActorName == "AlrauneB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Alraune:[VOICE|Alraune] We come to one of the worst places in Pandemonium.[P] A place choked over with pollution and infested with dangerous mutated animals.[P] And now we have to deal with a bandit army?[P] It never ends!") ]])
    
elseif(sActorName == "AlrauneC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Alraune:[VOICE|Alraune] Lady I'm displacing water through my toes and enjoying how it tickles.[P] Could you please get out of my light?") ]])
    
elseif(sActorName == "AlrauneD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Alraune:[VOICE|Alraune] I'm sort of swimming but I guess you can read it to me?[B][C]") ]])
    fnCutscene([[ Append("Alraune:[VOICE|Alraune] Yep that's gonna be trouble.[P] I wonder if there are any alraunes in that bandit army.[P] We love peace and nature but we also love staying alive.") ]])
    
elseif(sActorName == "Marigold") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Marigold:[VOICE|Marigold] The bandits will have to contend with the walls of this fort.[P] Still, I cannot help but lament.[P] It is true, the lands to the south wither.[P] We do not know why.") ]])
    
end

-- |[Clean Up]|
DL_PopActiveObject()
