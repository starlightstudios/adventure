-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= Exits ========================================== ]|
-- |[ ====================================== Examinables ======================================= ]|
if(sObjectName == "BigTree") then
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (An old, worn out tree stands in the middle of the pond.[P] It is surrounded by flowers.)") ]])
    fnCutsceneBlocker()
    
    --Flags.
    local iEmpressJoined = VM_GetVar("Root/Variables/Chapter2/MtSarulente/iEmpressJoined", "N")
    if(iEmpressJoined == 1.0) then
        VM_SetVar("Root/Variables/Chapter2/FortKirysu/iEmpressSawTree", "N", 1.0)
    end
    
-- |[ ========================================== Other ========================================= ]|
-- |[ ========================================== Debug ========================================= ]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
