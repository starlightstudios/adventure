-- |[ ==================================== Pamphlet: Dragon ==================================== ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ==== Minor Characters ==== ]|
-- |[Alraunes]|
if(sActorName == "AlrauneA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Alraune:[VOICE|Alraune] Some sort of ancient dragon prophecy?[P] You can't hear them but the grandfathers and little ones are always talking.[P] I'll ask them if they know anything.[B][C]") ]])
    fnCutscene([[ Append("Alraune:[VOICE|Alraune] Yeah, everyone here except for the grandfather in the middle is much younger than the time of Perfect Hatred's defeat.[P] They believe in you though!") ]])
    
elseif(sActorName == "AlrauneB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Alraune:[VOICE|Alraune] I was one of the Alarunes who helped guard our leaf-sisters as we made our dangerous journey.[P] I'm telling you that miss 'started an empire and am 8 feet tall' dragon so that you know that if you breath ice or fire next to the grandfather I will put you on your ass.") ]])
    
elseif(sActorName == "AlrauneC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Alraune:[VOICE|Alraune] Lady I'm trying to photosynthesize over here.[P] Could you please get out of my light?") ]])
    
elseif(sActorName == "AlrauneD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Alraune:[VOICE|Alraune] I'm sort of swimming but I guess you can read it to me?[B][C]") ]])
    fnCutscene([[ Append("Alraune:[VOICE|Alraune] So you didn't put the name of the legendary hero, but you are walking around with the giant ice dragon in tow and you think you won't be recognized?") ]])
    
elseif(sActorName == "Marigold") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Marigold:[VOICE|Marigold] Do you think that the timing lines up too conveniently?[P] I wonder if the grandfather was not calling us to help Westwoods, but to help you...") ]])
end

-- |[Clean Up]|
DL_PopActiveObject()
