-- |[ =============================== Pamphlet: Green Westwoods =============================== ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ==== Minor Characters ==== ]|
-- |[Alraunes]|
if(sActorName == "AlrauneA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Alraune:[VOICE|Alraune] Wait, a pamphlet about us?[P] You wrote it and you didn't interview the little ones or the grandfathers![P] Here I'll ask them if they have any comment.[B][C]") ]])
    fnCutscene([[ Append("Alraune:[VOICE|Alraune] They say they like the article, but would have appreciated you making them sound cooler.[P] It's tough for non-mutated plants down here and they would've appreciated a bit more respect for the work they did.[B][C]") ]])
    fnCutscene([[ Append("Alraune:[VOICE|Alraune] Thanks for writing it though!") ]])
    
elseif(sActorName == "AlrauneB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Alraune:[VOICE|Alraune] I- thank you for writing this.[P] Our journey was hard and our task seems at times hopeless.[P] But to know that others care as much as we do is heartening.[P] Perhaps in time we will succeed, I gotta keep believing that.") ]])
    
elseif(sActorName == "AlrauneC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Alraune:[VOICE|Alraune] Hey, it's about us![P] Tell everyone how great it is here![P] Strong sunlight and warm waters.[P] Speaking of,[P] could you back up?") ]])
    
elseif(sActorName == "AlrauneD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Alraune:[VOICE|Alraune] I'm sort of swimming but I guess you can read it to me?[B][C]") ]])
    fnCutscene([[ Append("Alraune:[VOICE|Alraune] Yeah, that's us![P] It doesn't look like it but I'm doing something super important right now.[P] No I won't tell you what it is.") ]])
    
elseif(sActorName == "Marigold") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Marigold:[VOICE|Marigold] Oh, thank you for spreading the word![P] I hope more leaf-sisters come to aid us.") ]])
    
end

-- |[Clean Up]|
DL_PopActiveObject()
