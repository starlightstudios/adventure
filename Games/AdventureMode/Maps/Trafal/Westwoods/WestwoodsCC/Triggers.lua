-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
if(sObjectName == "WestwoodsCA" or sObjectName == "WestwoodsCD") then

    -- |[Blackout]|
    fnCutscene([[ AL_SetProperty("Activate Fade", 0, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    -- |[Position]|
    fnCutsceneTeleport("Sanya", -1.25, -1.50)
    fnCutsceneTeleport("Izuna", -1.25, -1.50)
    fnCutsceneTeleport("Zeke", -1.25, -1.50)
    fnCutsceneTeleport("Marigold", -1.25, -1.50)
    fnCutsceneTeleport("AlrauneA", -1.25, -1.50)
    fnCutsceneTeleport("AlrauneB", -1.25, -1.50)
    fnCutsceneTeleport("AlrauneC", -1.25, -1.50)
    fnCutsceneTeleport("AlrauneD", -1.25, -1.50)
    fnCutsceneTeleport("Empress", 22.75, 28.50)
    fnCutsceneCamera("Actor Name", gcfCamera_Cutscene_Speed, "Empress")
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Fade In]|
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneFace("Empress", -1, 0)
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    fnCutsceneFace("Empress", 1, 0)
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    fnCutsceneFace("Empress", 0, -1)
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    fnCutsceneMove("Empress", 22.75, 16.50)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Empress", "Neutral") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] So this is where you ended up.[P] You don't need to say anything, I know it is you.[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] ...[P] If you had told me this was your plan, maybe I could have helped.[P] If you would have accepted it.[P] Well.[P] I'm here now.[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] Then again, maybe you didn't have a plan.[P] Still, you've gotten much more done than I have.[P] I guess you finally outdid me.[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] The children, my my.[P] You can sense them, I hope.[P] Such enthusiasm.[P] The alraunes you called, they're just like Sanya and Izuna.[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] They want to help, and they're so exuberant.[P] Full of life.[P] The future is in good hands.[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] I guess Elina was right.[P] We don't have to do everything ourselves.[P] We should have listened to her.[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] Yes, that one with me.[P] That's Sanya.[P] The one from the prophecy.[P] It's really time for this.[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] If I survive, well.[P] If you'll accept me, maybe I'll help you here.[P] And if I don't, well...[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] Never ford the same river twice, right?[P] Never too late to make amends.[P] If you can forgive me.[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] ...") ]])
    fnCutsceneBlocker()
    fnCutsceneCamera("Position", gcfCamera_Cutscene_Speed, 22.75, 16.50)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Empress", 0, 1)
    fnCutsceneWait(95)
    fnCutsceneBlocker()
    fnCutsceneMove("Empress", 22.75, 29.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    
    -- |[Transition]|
    if(sObjectName == "WestwoodsCA") then
        fnCutscene([[ AL_BeginTransitionTo("WestwoodsCA", "FORCEPOS:2.0x1.0x0") ]])
    else
        fnCutscene([[ AL_BeginTransitionTo("WestwoodsCD", "FORCEPOS:2.0x2.0x0") ]])
    end
end
