-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
if(sObjectName == "Cutscene") then

    -- |[Repeat Check]|
    local iMetTakahn = VM_GetVar("Root/Variables/Chapter2/Westwoods/iMetTakahn", "N")
    if(iMetTakahn == 1.0) then return end
    
    -- |[Flag]|
    VM_SetVar("Root/Variables/Chapter2/Westwoods/iMiaGaveCartQuest", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter2/Westwoods/iMetTakahn", "N", 1.0)
    
    -- |[Variables]|
    local iEmpressJoined = VM_GetVar("Root/Variables/Chapter2/MtSarulente/iEmpressJoined", "N")

    -- |[Setup]|
    fnCutsceneMergeParty()
    fnCutsceneBlocker()

    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Elf:[VOICE|Takahn] Hold it right there, rogues!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    -- |[Movement]|
    fnCutsceneJumpTo("Takahn", 23.25, 11.50, 25)
    fnCutsceneMove("Sanya", 27.25, 11.50)
    fnCutsceneFace("Sanya", -1, 0)
    fnCutsceneMove("Izuna", 28.25, 11.50)
    fnCutsceneFace("Izuna", -1, 0)
    fnCutsceneMove("Zeke", 27.25, 12.50)
    fnCutsceneFace("Zeke", -1, 0)
    if(iEmpressJoined == 1.0) then
        fnCutsceneMove("Empress", 28.25, 12.50)
        fnCutsceneFace("Empress", -1, 0)
    end

    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Takahn", "Neutral") ]])
    fnCutscene([[ Append("Sanya:[E|Offended] Watch out everyone, there are rogues right behind us![B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Ugh] No, Sanya, he thinks we're the rogues.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Offended] Hey you, clean-shaven guy with long ears![P] How many rogues are there?[B][C]") ]])
    fnCutscene([[ Append("Elf:[E|Neutral] Zero.[P] Cripes.[P] Save me the comedy bit.[B][C]") ]])
    fnCutscene([[ Append("Elf:[E|Neutral] Why is a group of travelling jesters in this blasted hellscape?[B][C]") ]])
    if(iEmpressJoined == 1.0) then
        fnCutscene([[ Append("Empress:[E|Neutral] Someone has seen through your facade, Sanya.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Offended] No![P] I need my facade![P] Put up another one![B][C]") ]])
        fnCutscene([[ Append("Zeke:[E|Neutral] Bah![B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Blush] Plan B![P] Seduction![B][C]") ]])
        fnCutscene([[ Append("Elf:[E|Neutral] Dear god no, please stop. Stop.[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Ugh] Why can't meeting random people on the road ever go smoothly?[B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] So, what is the pressing issue, mercenary?[B][C]") ]])
    else
        fnCutscene([[ Append("Sanya:[E|Offended] Zeke![P] He called you a jester![P] Prove how serious you are![B][C]") ]])
        fnCutscene([[ Append("Zeke:[E|Neutral] Bah![P] Bah![B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] See![P] That joke had no punchline and the timing was terrible![B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Ugh] Look, mister.[P] We're not here to hurt you.[P] Can you just tell us why you're here?[B][C]") ]])
    end
    fnCutscene([[ Append("Takahn:[E|Neutral] Name's Takahn.[P] We were on our way to Poterup when some bandits ambushed us.[B][C]") ]])
    fnCutscene([[ Append("Takahn:[E|Neutral] We smoked most of them, but in the crossfire...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Laugh] Hey man, one of your cart wheels is burned off.[B][C]") ]])
    fnCutscene([[ Append("Takahn:[E|Neutral] Actually that was unrelated.[P] It did that on its own.[P] Have you ever heard of spontaneous combustion?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] We call that fire magic.[B][C]") ]])
    if(iEmpressJoined == 1.0) then
        fnCutscene([[ Append("Empress:[E|Neutral] Regardless of the cause, I am to assume that you cannot move your goods without a replacement wheel.[B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] And your employers would be unhappy if you abandoned the goods.[B][C]") ]])
        fnCutscene([[ Append("Takahn:[E|Neutral] Spot on.[B][C]") ]])
        fnCutscene([[ Append("Takahn:[E|Neutral] The bandits are all over the place.[P] I can't leave to get fresh wood, and all the wood around this dump is rotten.[B][C]") ]])
    else
        fnCutscene([[ Append("Takahn:[E|Neutral] Look we are talking an awful lot about what happened in the past.[P] Let's focus on the future.[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Ugh] Yeah.[P] Let's change the subject.[B][C]") ]])
        fnCutscene([[ Append("Takahn:[E|Neutral] I can't leave the merchants to get wood for a new wheel.[P] All the wood around here is rotten.[B][C]") ]])
        fnCutscene([[ Append("Takahn:[E|Neutral] So we're stuck here until either the bandits starve us out, or...[B][C]") ]])
    end
    fnCutscene([[ Append("Sanya:[E|Neutral] You want us to get you some wood for a new wheel.[B][C]") ]])
    fnCutscene([[ Append("Takahn:[E|Neutral] That would help immensely.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] What do we get out of it?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Explain] The satisfaction of doing a good deed![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Blush] God you are so hot when you're being selfless.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] Okay bucko.[P] What exactly do you need us to do?[B][C]") ]])
    fnCutscene([[ Append("Takahn:[E|Neutral] I'm going to need probably three hardened barks and one light fiber.[P] I can slap together a new wheel with that.[B][C]") ]])
    fnCutscene([[ Append("Takahn:[E|Neutral] It won't be high quality, but it will let us get the goods to Poterup.[P] We can get the cart fixed properly there.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] And what happens if we don't?[B][C]") ]])
    fnCutscene([[ Append("Takahn:[E|Neutral] Uh.[P] We won't get paid?[P] Because we had to ditch the shipment?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Offended] Your bosses would stiff you!?[P] They're the real villains![B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] Let's just get the materials he needs, Sanya.[P] The creatures of Northwoods should have the wood and fiber.") ]])
    fnCutsceneBlocker()
    
    -- |[Finish Up]|
    fnAutoFoldParty()
    fnCutsceneBlocker()
end
