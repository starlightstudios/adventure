-- |[ ======================================= Constructor ====================================== ]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = fnResolveDirectory() --Map's name is the name of the folder it is in.
local sLevelMusic = "Choking Clouds"
local sMapResolveName = "Map Resolve Name"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[ ======================================== Standard ======================================== ]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

    -- |[Construction]|
	--Music.
	AL_SetProperty("Music", sLevelMusic)
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
    
    -- |[Shooting Setup]|
    AL_SetProperty("Shooting Depth", 0)
    
    -- |[Map Handling]|
    --Resolve the map's variable.
    local sMapVariable = fnResolveMapVarFromName(sLevelName)
    if(sMapVariable ~= "Null") then
        
        --Flag the map as uncovered.
        VM_SetVar(sMapVariable, "N", 1.0)
        
        --Run subroutine to build map properties.
        fnHandleWestwoodsMap(sLevelName)

    end

-- |[ ======================================== Post-Exec ======================================= ]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	-- |[NPC Spawning]|
	--If an entity needs to spawn after a post-exec cutscene, put them here.
    local iHelpedTakahn = VM_GetVar("Root/Variables/Chapter2/Westwoods/iHelpedTakahn", "N")
    if(iHelpedTakahn == 0.0) then
        fnSpawnNPCPattern("Merchant", "A", "B")
        fnStandardNPCByPosition("Merc")
        fnStandardNPCByPosition("Takahn")
    else
        AL_SetProperty("Set Collision", 14, 11, 0, 0)
        AL_SetProperty("Set Collision", 15, 11, 0, 0)
        AL_SetProperty("Set Collision", 16, 11, 0, 0)
        AL_SetProperty("Set Collision", 17, 11, 0, 0)
        AL_SetProperty("Set Collision", 18, 11, 0, 0)
        AL_SetProperty("Set Collision", 14,  8, 0, 0)
        AL_SetProperty("Set Collision", 19,  8, 0, 0)
        AL_SetProperty("Set Collision", 20,  8, 0, 0)
        AL_SetProperty("Set Collision", 21,  8, 0, 0)
        AL_SetProperty("Set Collision", 25, 10, 0, 0)
        AL_SetProperty("Set Layer Disabled", "Cart", true)
    end

    -- |[Overlays]|
    --Place overlay code here.
    
    -- |[Skillbooks]|
    --Place skillbook overlay code here.

    -- |[Rifle Handling]|
    VM_SetVar("Root/Variables/Global/Sanya/iRifleCode", "N", gciSanyaRifle_NotHere)

end
