-- |[ ==================================== Pamphlet: Dragon ==================================== ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ==== Minor Characters ==== ]|
-- |[Merchants]|
if(sActorName == "Merc") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Mercenary:[VOICE|MercF] Whoa![P] Sorry, but please be a bit slower in taking out those pamphlets.[P] I barely slept for days and I'm on edge.[B][C]") ]])
    fnCutscene([[ Append("Mercenary:[VOICE|MercF] I don't want to hurt you by reflex.[P] Still, short of the legendary hero showing up and fixing the broken wagon, I really don't care about the legend right now.") ]])

elseif(sActorName == "MerchantA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Merchant:[VOICE|HumanM0] Oh thank goodness, paper![P] I ran out of wipe last night and you [P]*really*[P] don't want to use the leaves down here.") ]])

elseif(sActorName == "MerchantB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Merchant:[VOICE|HumanF0] Legendary hero teams up with dragon, huh?[P] You know, I bet there's good money in selling legendary hero merch.[P] You know where this hero is?[P] If they don't mind doing autographs I can find a market.") ]])
    
-- |[ ==== Named Characters ==== ]|
-- |[Takahn]|
elseif(sActorName == "Takahn") then

end

-- |[Clean Up]|
DL_PopActiveObject()
