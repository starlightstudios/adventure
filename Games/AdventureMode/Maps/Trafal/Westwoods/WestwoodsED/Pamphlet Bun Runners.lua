-- |[ ============================= Pamphlet: The Bunny Smugglers ============================== ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ==== Minor Characters ==== ]|
-- |[Merchants]|
if(sActorName == "Merc") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Mercenary:[VOICE|MercF] Whoa![P] Sorry, but please be a bit slower in taking out those pamphlets.[P] I barely slept for days and I'm on edge.[B][C]") ]])
    fnCutscene([[ Append("Mercenary:[VOICE|MercF] So the bunnies say they aren't giving these bandits their weapons.[P] They would say that.[P][P] You find out who is, put that in a pamphlet.[P] I have a score to settle.") ]])

elseif(sActorName == "MerchantA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Merchant:[VOICE|HumanM0] Oh thank the stars, porn.[P] It's been a couple of really stressful days so I could use this.[P] You got good taste.[P] Now to figure out how to get some privacy.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[VOICE|Izuna] (Uhh, glad to help...)") ]])

elseif(sActorName == "MerchantB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Merchant:[VOICE|HumanF0] Bunnies running guns?[P] Not great margins in courier work, and proper shipping loses a lot of money in the guards.[P] Smuggling means it's more individual and you can shift the costs onto the operator.[P] Honestly only way it could get better is if they had to take a loan to get started.[B][C]") ]])
    fnCutscene([[ Append("Merchant:[VOICE|HumanF0] Listen to me, detailing business plans to some stranger.[P] The stress is getting to me.") ]])
    
-- |[ ==== Named Characters ==== ]|
-- |[Takahn]|
elseif(sActorName == "Takahn") then

end

-- |[Clean Up]|
DL_PopActiveObject()
