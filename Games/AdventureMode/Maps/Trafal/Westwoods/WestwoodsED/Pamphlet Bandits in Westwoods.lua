-- |[ ============================= Pamphlet: Bandits in Westwoods ============================= ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ==== Minor Characters ==== ]|
-- |[Merchants]|
if(sActorName == "Merc") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Mercenary:[VOICE|MercF] Whoa![P] Sorry, but please be a bit slower in taking out those pamphlets.[P] I barely slept for days and I'm on edge.[B][C]") ]])
    fnCutscene([[ Append("Mercenary:[VOICE|MercF] Hey wait is that my name?[P] Are you showing me what happens to me?[P] But that hasn't happened yet.[P] You know I read a story about someone caught in a time loop once.[P] It didn't have a very happy ending") ]])

elseif(sActorName == "MerchantA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Merchant:[VOICE|HumanM0] Wait, merchants escape bandit attack?[P] I'm a merchant and I'm in a bandit attack![P] Does showing me something from the future collapse the possible outcomes?[P] Thanks I guess, well except for all the outcomes where I suddenly became fabulously rich.[P] No thanks for collapsing those") ]])

elseif(sActorName == "MerchantB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Merchant:[VOICE|HumanF0] So you are giving me a piece of paper which says I will escape deadly peril.[P] I bet it also contains a great investment opportunity to take as soon as I'm safe.[P] I'm not falling for that one...[P] again..") ]])
    
-- |[ ==== Named Characters ==== ]|
-- |[Takahn]|
elseif(sActorName == "Takahn") then

end

-- |[Clean Up]|
DL_PopActiveObject()
