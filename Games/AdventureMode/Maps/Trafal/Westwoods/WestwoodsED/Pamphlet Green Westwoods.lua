-- |[ =============================== Pamphlet: Green Westwoods =============================== ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ==== Minor Characters ==== ]|
-- |[Merchants]|
if(sActorName == "Merc") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Mercenary:[VOICE|MercF] Whoa![P] Sorry, but please be a bit slower in taking out those pamphlets.[P] I barely slept for days and I'm on edge.[B][C]") ]])
    fnCutscene([[ Append("Mercenary:[VOICE|MercF] Alraunes you say?[P] Well this place cannot suck any more than it does.[P] So more power to them.[P] Seriously I've been staring at a bleeding tree and I think it's starting to crawl towards me.") ]])

elseif(sActorName == "MerchantA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Merchant:[VOICE|HumanM0] Oh thank goodness, paper.[P] We've been setting a lot of watch fires and I've learned [P]*not*[P] to ask the mage to start them.[P] You lose your eyebrows if you are too close.") ]])

elseif(sActorName == "MerchantB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Merchant:[VOICE|HumanF0] Hmm, you could maybe make this a vacation spot.[P] Right now all I can think about is how more trees would make it easier for bandits to cut my throat in my sleep.") ]])
    
-- |[ ==== Named Characters ==== ]|
-- |[Takahn]|
elseif(sActorName == "Takahn") then

end

-- |[Clean Up]|
DL_PopActiveObject()
