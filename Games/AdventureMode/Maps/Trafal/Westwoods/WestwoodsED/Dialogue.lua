-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[ ========= Setup ========== ]|
    -- |[Name Resolve]|
    --Resolve the name of the calling entity.
    local sActorName = TA_GetProperty("Name")
    
    -- |[ ==== Minor Characters ==== ]|
    -- |[Merchants]|
    if(sActorName == "Merc") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Mercenary:[VOICE|MercF] One of the bandits had a rifle, but the mage torched it.[P] I'd have loved to get my hands on it.") ]])

    elseif(sActorName == "MerchantA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Merchant:[VOICE|HumanM0] I hate this place, but it's usually the wildlife that's a problem.[P] With bandit attacks, this road might be totally unusable.") ]])
    
    elseif(sActorName == "MerchantB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Merchant:[VOICE|HumanF0] If we lose the shipment, we're ruined.[P] At least the mercs we hired fought the bandits off.") ]])
        
    -- |[ ==== Named Characters ==== ]|
    -- |[Takahn]|
    elseif(sActorName == "Takahn") then
    
        --Variables.
        local iBarkCount  = AdInv_GetProperty("Item Count", "Hardened Bark")
        local iFiberCount = AdInv_GetProperty("Item Count", "Light Fiber")
    
        --Dialogue.
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Takahn", "Neutral") ]])
        
        --Hasn't helped yet:
        local iHelpedTakahn = VM_GetVar("Root/Variables/Chapter2/Westwoods/iHelpedTakahn", "N")
        if(iHelpedTakahn == 0.0) then
        
            fnCutscene([[ Append("Takahn:[E|Neutral] Hey, you got the materials I need?[B][C]") ]])
            
            --Report.
            local sString = "Append(\"Sanya:[E|Neutral] (I have " .. iBarkCount .. " Hardened Bark and " .. iFiberCount .. " Light Fiber.)[B][C]\")"
            fnCutscene(sString)
            
            --Does not have the materials:
            if(iBarkCount < 3 or iFiberCount < 1) then
                fnCutscene([[ Append("Sanya:[E|Neutral] Not yet.[P] Remind me how much you needed?[B][C]") ]])
                fnCutscene([[ Append("Takahn:[E|Neutral] Three Hardened Bark and one Light Fiber should do it.[B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Neutral] Right.[P] I'm on it.") ]])
                return
            
            --Has the materials.
            else
                fnCutscene([[ Append("Sanya:[E|Neutral] (Should I give him what he needs?)[B]") ]])
            
                --Decision script is this script. It must be surrounded by quotes.
                local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
                fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
                fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"Yes\") ")
                fnCutscene(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"No\") ")
                fnCutsceneBlocker()
            end
        else
            fnCutscene([[ Append("Takahn:[E|Neutral] Can't wait to get the hell out of here![P] Thanks again.") ]])
        
        end
    end
    
-- |[ ====================================== Giving Items ====================================== ]|
--Give Takahn the items.
elseif(sTopicName == "Yes") then

    --Setup.
	WD_SetProperty("Hide")
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter2/Westwoods/iHelpedTakahn", "N", 1.0)

    --Remove items.
    AdInv_SetProperty("Remove Item", "Hardened Bark")
    AdInv_SetProperty("Remove Item", "Hardened Bark")
    AdInv_SetProperty("Remove Item", "Hardened Bark")
    AdInv_SetProperty("Remove Item", "Light Fiber")
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Takahn", "Neutral") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] [SOUND|World|TakeItem]Here you go, chuckles.[B][C]") ]])
    fnCutscene([[ Append("Takahn:[E|Neutral] Thank you![P] I'll get to work on this right away.[P] Hey, you should meet up with us in Poterup![B][C]") ]])
    fnCutscene([[ Append("Takahn:[E|Neutral] I can't say if the shipping company will have something for you, but maybe I'll buy you a drink.") ]])

--Nope.
elseif(sTopicName == "No") then
	WD_SetProperty("Hide")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Takahn", "Neutral") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Still working on it.[B][C]") ]])
    fnCutscene([[ Append("Takahn:[E|Neutral] Sure, sure.[P] Uh, please hurry.") ]])

end
