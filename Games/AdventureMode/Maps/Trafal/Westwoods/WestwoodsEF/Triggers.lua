-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
if(sObjectName == "Introduction") then

    -- |[Repeat Check]|
    local iAreaExplained = VM_GetVar("Root/Variables/Chapter2/Westwoods/iAreaExplained", "N")
    if(iAreaExplained == 1.0) then return end
    
    -- |[Flag]|
    VM_SetVar("Root/Variables/Chapter2/Westwoods/iAreaExplained", "N", 1.0)
    
    -- |[Variables]|
    local iEmpressJoined = VM_GetVar("Root/Variables/Chapter2/MtSarulente/iEmpressJoined", "N")
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("Izuna:[E|Explain] And this is Westwoods, better known as 'The Cauldron'![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] What a dump![B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] Bah![B][C]") ]])
    if(iEmpressJoined == 0.0) then
        fnCutscene([[ Append("Izuna:[E|Ugh] Tell me about it.[P] I hate this place.[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Neutral] It's one of the most dangerous places in Trafal, possibly the continent.[P] Pure water is hard to find and the wildlife is mutated.[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Neutral] It is said that this is the site of the grand battle that ended the empire 700 years ago.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Offended] You mean he's still here!?[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Ugh] N-[P]no...[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Smirk] This is just where the battle happened.[P] His prison is far east of here.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Smirk] Okay, but I still want to kick his ass.[B][C]") ]])
        fnCutscene([[ Append("Zeke:[E|Neutral] Nyeh![B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Jot] Now let's see...[P] according to my notes, the area is uninhabited.[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Jot] But it's also one of the easier ways to enter Trafal, and the old imperial highway runs through here.[P] So bandits might be a problem.[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Jot] Oh, and don't drink the water in the lake.[P] Trust me on that.") ]])
    else
        fnCutscene([[ Append("Empress:[E|Neutral] Westwoods was the site of the battle 700 years ago that gave me this scar.[B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] This is where the wave of my empire crested and rolled back.[P] It was not always a toxic hellhole.[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Jot] Really?[P] What was it like before?[B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] Like Northwoods, but more fertile.[P] The lake in the center was filled with the runoff from the mountains.[B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] It was excellent farmland, dotted with small villages.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] So what happened?[B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] While they said it was the foul blood of the beast that cursed this place, the reality is more mundane.[B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] My mages threw everything they had at him.[P] The resulting blast of deadly poisons and flames poisoned the ground.[P] It was not enough.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Sad] Jeez...[B][C]") ]])
        fnCutscene([[ Append("Zeke:[E|Neutral] Nyeh...[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Sad] Maybe don't eat any of the grass, buddy.[P] Let's get going.") ]])
    end
    fnCutsceneBlocker()
end
