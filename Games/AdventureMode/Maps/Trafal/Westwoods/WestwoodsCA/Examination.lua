-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= Exits ========================================== ]|
if(sObjectName == "SampleExit") then

-- |[ ====================================== Examinables ======================================= ]|
elseif(sObjectName == "Entrance") then
    
    -- |[Already Befriended the Alraunes]|
    local iBefriendedAlraunes = VM_GetVar("Root/Variables/Chapter2/FortKirysu/iBefriendedAlraunes", "N")
    if(iBefriendedAlraunes == 1.0) then
        AL_BeginTransitionTo("WestwoodsCB", "FORCEPOS:5.0x16.0x0")
        AudioManager_PlaySound("World|FlipSwitch")
    
    -- |[Quest!]|
    else
    
        --Variables.
        local iMetAlraune     = VM_GetVar("Root/Variables/Chapter2/FortKirysu/iMetAlraune", "N")
        local iEmpressJoined  = VM_GetVar("Root/Variables/Chapter2/MtSarulente/iEmpressJoined", "N")
        local iGotSpringwater = VM_GetVar("Root/Variables/Chapter2/FortKirysu/iGotSpringwater", "N")
    
        --Common code.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Alraune", "Neutral") ]])
    
        --Got the springwater.
        if(iGotSpringwater == 1.0) then
            
            --Flag.
            VM_SetVar("Root/Variables/Chapter2/FortKirysu/iBefriendedAlraunes", "N", 1.0)
            VM_SetVar("Root/Variables/Chapter2/FortKirysu/iSpawnMarigold", "N", 1.0)
            
            --Remove item.
            AdInv_SetProperty("Remove Item", "Jug of Springwater")
    
            --Dialogue.
            fnCutscene([[ Append("Alraune:[E|Neutral] Who g-[P] oh, it's you again.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Blush] Take a weeper with your peepers, lady.[P] Fresh springwater from Northwoods, in a convenient jug.[B][C]") ]])
            fnCutscene([[ Append("Alraune:[E|Neutral] Weeper with your peepers...[B][C]") ]])
            fnCutscene([[ Append("Alraune:[E|Neutral] Nevermind.[P] I see that you did indeed get us the springwater.[P] Please, come in.") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            fnCutscene([[ AL_BeginTransitionTo("WestwoodsCB", "FORCEPOS:5.0x16.0x0") ]])
            fnCutscene([[ AudioManager_PlaySound("World|FlipSwitch") ]])
    
        --Hasn't met the alraunes.
        elseif(iMetAlraune == 0.0) then
            
            --Flags.
            VM_SetVar("Root/Variables/Chapter2/FortKirysu/iMetAlraune", "N", 1.0)
            VM_SetVar("Root/Variables/Chapter2/FortKirysu/iMiaGaveAlrauneQuest", "N", 1.0)
        
            --No Empress:
            if(iEmpressJoined == 0.0) then
                fnCutscene([[ Append("Alraune:[E|Neutral] Who goes?[B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Neutral] Two steel-eyed psychos and a fox.[P] Open up.[B][C]") ]])
                fnCutscene([[ Append("Zeke:[E|Neutral] Bah?[B][C]") ]])
                fnCutscene([[ Append("Alraune:[E|Neutral] Are you friends of nature?[B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Happy] Me and nature go way back![P] We used to romp![B][C]") ]])
                fnCutscene([[ Append("Alraune:[E|Neutral] ...[B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Happy] Bang.[P] Get busy.[P] Do the nasty.[P] Bump uglies.[B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Ugh] I got it the first time, Sanya.[B][C]") ]])
                fnCutscene([[ Append("Alraune:[E|Neutral] I still have not got it.[B][C]") ]])
                fnCutscene([[ Append("Zeke:[E|Neutral] Bah...[B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Offended] Sex![B][C]") ]])
                fnCutscene([[ Append("Alraune:[E|Neutral] Ohhhhhhh![B][C]") ]])
                fnCutscene([[ Append("Alraune:[E|Neutral] ...[P] By nature, I hope you know I am referring to the trees and plants.[B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Blush] Absolutely.[B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Happy] Miss alraune![P] May I ask how you've managed to rejuvinate the soil here?[B][C]") ]])
                fnCutscene([[ Append("Alraune:[E|Neutral] We're not accepting visitors, and not answering questions.[P] Those who are not friends of nature must be turned away.[B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Offended] I told you![P] We used to get wild![B][C]") ]])
                fnCutscene([[ Append("Alraune:[E|Neutral] I don't believe you.[B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Offended] God damn it![B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Smirk] Ask what we can do to prove it.[P] Ask![B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Neutral] Yeah, how can I prove it?[B][C]") ]])
                fnCutscene([[ Append("Alraune:[E|Neutral] Hm.[P] We are in need of pure springwater.[P] Get us a bottle of pure spring water, and I will let you in.[B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Neutral] Deal![B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Neutral] Okay.[P] There's probably some springwater in Northwoods someplace.[B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Explain] This smells like a story![P] Come on![B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Explain] Remember, they want springwater.[P] Most of the water in Northwoods is runoff.[P] We need a spot where the water springs from the ground.[B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Explain] And to be pure, it'd need to be a small enough spring.[P] Check the map!") ]])
                fnCutsceneBlocker()
        
            --Empress:
            else
                fnCutscene([[ Append("Alraune:[E|Neutral] Who goes?[B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Neutral] Three steel-eyed psychos and a fox.[P] Open up.[B][C]") ]])
                fnCutscene([[ Append("Zeke:[E|Neutral] Bah?[B][C]") ]])
                fnCutscene([[ Append("Alraune:[E|Neutral] Are you friends of nature?[B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Happy] Me and nature go way back![P] We used to romp![B][C]") ]])
                fnCutscene([[ Append("Alraune:[E|Neutral] ...[B][C]") ]])
                fnCutscene([[ Append("Empress:[E|Neutral] ...[P] Romp?[B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Happy] Bang.[B][C]") ]])
                fnCutscene([[ Append("Alraune:[E|Neutral] By nature, I hope you know I am referring to the trees and plants.[B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Blush] Absolutely.[B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Happy] Miss alraune![P] May I ask how you've managed to rejuvinate the soil here?[B][C]") ]])
                fnCutscene([[ Append("Empress:[E|Neutral] And what are you doing in Fort Kirysu?[B][C]") ]])
                fnCutscene([[ Append("Alraune:[E|Neutral] We're not accepting visitors.[P] Those who are not friends of nature must be turned away.[B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Offended] I told you![P] We used to get wild![B][C]") ]])
                fnCutscene([[ Append("Alraune:[E|Neutral] I don't believe you.[B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Offended] God damn it![B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Smirk] Ask what we can do to prove it.[P] Ask![B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Neutral] Yeah, how can I prove it?[B][C]") ]])
                fnCutscene([[ Append("Alraune:[E|Neutral] Hm.[P] We are in need of pure springwater.[P] Get us a bottle of pure spring water, and I will let you in.[B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Neutral] Deal![B][C]") ]])
                fnCutscene([[ Append("Empress:[E|Neutral] Hm.[P] We should be able to find some in Northwoods.[B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Explain] This smells like a story![P] Come on![B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Explain] Remember, they want springwater.[P] Most of the water in Northwoods is runoff.[P] We need a spot where the water springs from the ground.[B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Explain] And to be pure, it'd need to be a small enough spring.[P] Check the map!") ]])
                fnCutsceneBlocker()
            end
        
        --Has met them, repeat.
        else
            fnCutscene([[ Append("Alraune:[E|Neutral] Do you have the springwater?[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] No, but we're working on that.[B][C]") ]])
            fnCutscene([[ Append("Alraune:[E|Neutral] Then why have you returned?[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Blush] No reason.[P] Just to talk.[P] You got a real pretty voice.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Blush] It's true, you do.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Blush] I just went gay recently so I'm real open to new experiences, you know?[B][C]") ]])
            fnCutscene([[ Append("Alraune:[E|Neutral] You cannot flatter your way in here.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Blush] Damn, shot down again.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Blush] ...[P] Maybe we can try again when we have the springwater?[B][C]") ]])
            fnCutscene([[ Append("Zeke:[E|Neutral] ..........[P] Bah?[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] Shut up, Zeke.[P] Like you've never been horny.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Explain] Remember, most of the water in Northwoods is runoff.[P] We need a spot where the water springs from the ground.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Explain] And to be pure, it'd need to be a small enough spring.[P] Check the map!") ]])
        end
    end
    
-- |[ ========================================== Other ========================================= ]|
-- |[ ========================================== Debug ========================================= ]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
