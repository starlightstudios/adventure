-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
if(sObjectName == "SpotTracks") then

    --Repeat check.
    local iSawTracks = VM_GetVar("Root/Variables/Chapter2/Northwoods/iSawTracks", "N")
    if(iSawTracks == 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter2/Northwoods/iSawTracks", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter2/Northwoods/iIsTutorialHuntActive", "N", 1.0)

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] All right, let's see here.[P] I haven't got any idea if any of the local shrubs are fit for consumption, but meat?[P] Meat is the good stuff.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] If I look around I'm sure I can find some tracks to follow.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Once I do, I can walk up to the tracks and examine them to see which direction they're going, if it's not obvious by just looking at them.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] After that, I just need to keep following the tracks until I find the prey.") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "NoLeaveYetW") then

    --Does nothing if the tutorial is not active.
    local iIsTutorialHuntActive = VM_GetVar("Root/Variables/Chapter2/Northwoods/iIsTutorialHuntActive", "N")
    if(iIsTutorialHuntActive == 0.0) then return end
    
    --If the player examined the tracks, do nothing.
    local iExaminedTracks = VM_GetVar("Root/Variables/Chapter2/Northwoods/iExaminedTracks", "N")
    if(iExaminedTracks == 1.0) then return end

    --Dialogue, move player back a bit.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Maybe I should take a closer look at those tracks...)") ]])
    fnCutsceneBlocker()
    fnCutsceneMoveAmount("Sanya", 1.00, 0.00)

elseif(sObjectName == "NoLeaveYetE") then

    --Does nothing if the tutorial is not active.
    local iIsTutorialHuntActive = VM_GetVar("Root/Variables/Chapter2/Northwoods/iIsTutorialHuntActive", "N")
    if(iIsTutorialHuntActive == 0.0) then return end

    --Dialogue, move player back a bit.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (I don't think this is the right way...)") ]])
    fnCutsceneBlocker()
    fnCutsceneMoveAmount("Sanya", -1.00, 0.00)
    
elseif(sObjectName == "NoLeaveYetS") then

    --Does nothing if the tutorial is not active.
    local iIsTutorialHuntActive = VM_GetVar("Root/Variables/Chapter2/Northwoods/iIsTutorialHuntActive", "N")
    if(iIsTutorialHuntActive == 0.0) then return end

    --Dialogue, move player back a bit.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (I don't think this is the right way...)") ]])
    fnCutsceneBlocker()
    fnCutsceneMoveAmount("Sanya", 0.00, -1.00)
    
end
