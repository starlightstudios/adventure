-- |[ =============================== Pamphlet: Green Westwoods =============================== ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ===== Generics ===== ]|
-- |[KitsuneA]|
if(sActorName == "KitsuneA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "KitsuneA", "Neutral") ]])
    fnCutscene([[ Append("Melody:[E|Neutral] So a coven of Alraunes is healing the Cauldron?[P] Oh, that's great to hear![P] I wonder if it will strengthen the mana flows up here?[P] Maybe we will get a longer growing season.[P][P] Just when I already thought I was busy!") ]])
    fnCutsceneBlocker()

-- |[KitsuneB]|
elseif(sActorName == "KitsuneB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "KitsuneB", "Neutral") ]])
    fnCutscene([[ Append("Aria:[E|Neutral] Hang on, my copy is smudged...[P][P] So a coven of Alraunes is healing the cauldron?[P] That's great to hear![P] You get the occasional animal from down there up in the Northwoods.[P] The poor things never live for long, they need the toxicity somehow.[P] It's tragic.") ]])
end

-- |[Clean Up]|
DL_PopActiveObject()
