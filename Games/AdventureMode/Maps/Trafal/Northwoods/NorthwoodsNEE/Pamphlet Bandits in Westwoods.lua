-- |[ ============================= Pamphlet: Bandits in Westwoods ============================= ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ===== Generics ===== ]|
-- |[KitsuneA]|
if(sActorName == "KitsuneA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "KitsuneA", "Neutral") ]])
    fnCutscene([[ Append("Melody:[E|Neutral] Rampaging bandit army, you say?[P] If they reach Northwoods they won't care about maintaining the ecosystem at all.[P] Take too much from the land and it'll take years, maybe decades for things to recover.") ]])
    fnCutsceneBlocker()

-- |[KitsuneB]|
elseif(sActorName == "KitsuneB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "KitsuneB", "Neutral") ]])
    fnCutscene([[ Append("Aria:[E|Neutral] A hungry bandit army doesn't sound good.[P] The temple has been trying to cut down on banditry via generosity but an army could pick this field in no time at all.[P] We will have to make plans, maybe we can reduce their numbers by just feeding them?") ]])
end

-- |[Clean Up]|
DL_PopActiveObject()
