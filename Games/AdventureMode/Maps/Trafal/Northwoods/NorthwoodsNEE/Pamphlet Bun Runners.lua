-- |[ ============================= Pamphlet: The Bunny Smugglers ============================== ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ===== Generics ===== ]|
-- |[KitsuneA]|
if(sActorName == "KitsuneA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "KitsuneA", "Neutral") ]])
    fnCutscene([[ Append("Melody:[E|Neutral] Wait the bunnies are boys?[P] One of them came by and dug for carrots.[P] They were making such a mess of it, I dug out the carrots in exchange for getting to pet the rabbit's tail.[P] No idea he had something in front.[P] Touching fluffy tail can be distracting.") ]])
    fnCutsceneBlocker()

-- |[KitsuneB]|
elseif(sActorName == "KitsuneB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "KitsuneB", "Neutral") ]])
    fnCutscene([[ Append("Aria:[E|Neutral] We've been getting a lot of bunnies recently.[P] They've probably been smuggling up to the north.[P] We must be a waystation for them.[P] I'll make sure they don't cause too much trouble around the farm at least.") ]])
end

-- |[Clean Up]|
DL_PopActiveObject()
