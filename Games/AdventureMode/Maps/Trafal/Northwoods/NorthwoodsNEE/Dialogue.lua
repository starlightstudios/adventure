-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[Name Resolve]|
    --Resolve the name of the calling entity.
    local sActorName = TA_GetProperty("Name")

    --Set facing.
    TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[Common]|
    local iKitsuneState = fnResolveKitsuneState()
    
    -- |[KitsuneA]|
    if(sActorName == "KitsuneA") then
        
        -- |[Variables]|
        local iMetFarmers   = VM_GetVar("Root/Variables/Chapter2/KitsuneVillage/iMetFarmers", "N")
        local iRescuedIzuna = VM_GetVar("Root/Variables/Chapter2/KitsuneVillage/iRescuedIzuna", "N")
        
        -- |[Setup]|
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "KitsuneA", "Neutral") ]])
        
        -- |[First Meeting]|
        if(iMetFarmers == 0.0) then
            VM_SetVar("Root/Variables/Chapter2/KitsuneVillage/iMetFarmers", "N", 1.0)
            fnCutscene([[ Append("Farmer:[E|Neutral] Izuna![P] You're -[P] alive![P] Thank the Goddess![B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Explain] I'm safe and sound, thanks to Sanya here![B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Smirk] Sanya, this is Melody.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] Are you a farmer?[B][C]") ]])
            fnCutscene([[ Append("Melody:[E|Neutral] Of course.[P] Just taking care of the winter crops before spring planting.[B][C]") ]])
            fnCutscene([[ Append("Melody:[E|Neutral] Izuna, you need to go tell the village that you're okay![P] We were searching for you![B][C]") ]])
            if(iRescuedIzuna == 0.0) then
                fnCutscene([[ Append("Izuna:[E|Neutral] That's on the to-do list, but it's up to Sanya where we go next.[B][C]") ]])
                fnCutscene([[ Append("Melody:[E|Neutral] I'm glad you're all right.[P] Where were you?[B][C]") ]])
            else
                fnCutscene([[ Append("Izuna:[E|Neutral] Don't worry, I already told them.[P] We just came from there.[B][C]") ]])
                fnCutscene([[ Append("Melody:[E|Neutral] Oh, good. Without your pamphlets, word doesn't travel that fast.[P] Where were you?[B][C]") ]])
                
            end
            fnCutscene([[ Append("Izuna:[E|Neutral] You know the old mining office that those termites left behind?[P] Sanya put me up in there.[B][C]") ]])
            fnCutscene([[ Append("Melody:[E|Neutral] The mining site...[P] who was supposed to search there?[P] We should have found you...[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Smirk] It's all right, really.[P] Sanya kept me safe until my legs and back healed.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] She fell off a bridge.[B][C]") ]])
            fnCutscene([[ Append("Melody:[E|Neutral] You did!?[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Neutral] If Sanya says so.[P] I don't remember it at all.[B][C]") ]])
            fnCutscene([[ Append("Melody:[E|Neutral] Goodness me.[P] One day you'll realize that all this excitement isn't worth the risk.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] Nah.[P] Live fast, die young.[P] Better to burn out than fade away.[B][C]") ]])
            fnCutscene([[ Append("Zeke:[E|Neutral] Nyeh![B][C]") ]])
            fnCutscene([[ Append("Melody:[E|Neutral] Tch.[P] Just remember that people care about you, Izuna.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Smirk] If two good things have come out of this, the first is that I learned that lesson.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] What's the other good thing?[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Happy] I met you and Zeke, silly![B][C]") ]])
            fnCutscene([[ Append("Melody:[E|Neutral] Stay as long as you like, then.[P] If you want to help with the crops, more hands are welcome.") ]])
        
        -- |[Repeats]|
        else
            fnCutscene([[ Append("Sanya:[E|Neutral] You know, I'm something of a farmer myself.[P] Isn't it too cold to grow crops?[B][C]") ]])
            fnCutscene([[ Append("Melody:[E|Neutral] Not the winter crops.[P] They're cold-resistant, because they don't freeze.[B][C]") ]])
            fnCutscene([[ Append("Melody:[E|Neutral] Thousands of years ago, the first kitsunes came to Trafal and created magical plants that feed off the latent mana here.[B][C]") ]])
            fnCutscene([[ Append("Melody:[E|Neutral] Magical winds blow through the rock beneath us.[P] We can harness it to grow food year round.[P] The animals here do the same thing.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] So despite being a mountain, you have a long growing season.[P] They'd be jealous back home.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Neutral] But the system is delicate, because all of the life here relies on it.[P] We can only take what we need, and must give back what we don't use.[B][C]") ]])
            fnCutscene([[ Append("Melody:[E|Neutral] The soil must be composted and fertilized, use every part of the animal, and return your mana to the earth.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] ...[P] How do I do that last one?[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Happy] Through the ancient kitsune rite of dance, Sanya![P] It's a lot of fun![B][C]") ]])
            fnCutscene([[ Append("Zeke:[E|Neutral] Nyeh![P] Bah![B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] You have zero rythym, Zeke.[P] You have four left feet.[B][C]") ]])
            fnCutscene([[ Append("Zeke:[E|Neutral] Baaah![B][C]") ]])
            fnCutscene([[ Append("Melody:[E|Neutral] He's free to try if he wants.") ]])
        end
        fnCutsceneBlocker()
    
    -- |[KitsuneB]|
    elseif(sActorName == "KitsuneB") then
        
        -- |[Variables]|
        local iMetFarmers   = VM_GetVar("Root/Variables/Chapter2/KitsuneVillage/iMetFarmers", "N")
        local iRescuedIzuna = VM_GetVar("Root/Variables/Chapter2/KitsuneVillage/iRescuedIzuna", "N")
        
        -- |[Setup]|
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "KitsuneB", "Neutral") ]])
        
        -- |[First Meeting]|
        if(iMetFarmers == 0.0) then
            VM_SetVar("Root/Variables/Chapter2/KitsuneVillage/iMetFarmers", "N", 1.0)
            fnCutscene([[ Append("Farmer:[E|Neutral] Izuna![P] Goddess' Whiskers, you're alive![B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Happy] Thanks to Sanya here![B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Neutral] Sanya, this is Aria.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] Are you a farmer?[B][C]") ]])
            fnCutscene([[ Append("Aria:[E|Neutral] Of course.[P] Just taking care of the winter crops before spring planting.[B][C]") ]])
            fnCutscene([[ Append("Aria:[E|Neutral] Izuna, you need to go tell the village that you're okay![P] We were searching for you![B][C]") ]])
            if(iRescuedIzuna == 0.0) then
                fnCutscene([[ Append("Izuna:[E|Neutral] That's on the to-do list, but it's up to Sanya where we go next.[B][C]") ]])
                fnCutscene([[ Append("Aria:[E|Neutral] I'm glad you're all right.[P] Where were you?[B][C]") ]])
            else
                fnCutscene([[ Append("Izuna:[E|Neutral] Don't worry, I already told them.[P] We just came from there.[B][C]") ]])
                fnCutscene([[ Append("Aria:[E|Neutral] Oh, good. Without your pamphlets, word doesn't travel that fast.[P] Where were you?[B][C]") ]])
                
            end
            fnCutscene([[ Append("Izuna:[E|Neutral] You know the old mining office that those termites left behind?[P] Sanya put me up in there.[B][C]") ]])
            fnCutscene([[ Append("Aria:[E|Neutral] The mining site...[P] but I searched the mining site![P] I was assigned to![B][C]") ]])
            fnCutscene([[ Append("Aria:[E|Neutral] Wait -[P] which one?[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Ugh] There's more than one?[B][C]") ]])
            fnCutscene([[ Append("Aria:[E|Neutral] Yeah -[P] wait, yeah![P] You go south from the mining office, then west a bit.[P] That's where the miner's barracks were.[B][C]") ]])
            fnCutscene([[ Append("Aria:[E|Neutral] ...[P] Damn it![P] I bet Mimori and I both searched the barracks![P] She's not as familiar with this part of the mountains.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Ugh] I wouldn't trust her with a map, much less heading a search party.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] I have no dog in this fight at all.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Neutral] Mimori is one of the warrior kitsunes.[B][C]") ]])
            fnCutscene([[ Append("Aria:[E|Neutral] She's good at fighting, not navigating.[P] She usually patrols the roads.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Neutral] But it all worked out in the end, so I won't blame her.[P] She tried her best.[B][C]") ]])
            fnCutscene([[ Append("Aria:[E|Neutral] Well then.[P] If you're hungry, you can eat anything you pick yourself.[P] Enjoy![B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] Is that the custom?[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Explain] Every farmer has a plot for travellers to harvest.[P] Not only does it keep people from going hungry...[B][C]") ]])
            fnCutscene([[ Append("Aria:[E|Neutral] It cuts down on banditry.[P] Being good to others is good for everyone.[P] See you around!") ]])
        
        -- |[Repeats]|
        else
            fnCutscene([[ Append("Sanya:[E|Neutral] Do the local creatures bother you?[B][C]") ]])
            fnCutscene([[ Append("Aria:[E|Neutral] Sometimes, but for the most part they know not to pick a fight with a kitsune.[P] Only if they're extremely hungry.[B][C]") ]])
            fnCutscene([[ Append("Aria:[E|Neutral] Any fight could be your last, so the animals prefer to eat the sick or old prey.[B][C]") ]])
            fnCutscene([[ Append("Aria:[E|Neutral] But I will say the brimhogs and omnigeese have been certainly getting bolder this past few years.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Explain] I did a story on it.[P] Wild animal attacks have been going up every year for the last five years.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Neutral] At first I thought it was because of more travellers on the glacier, but that just isn't it.[P] There were actually fewer travellers the last three years.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] Maybe their food supplies are low?[B][C]") ]])
            fnCutscene([[ Append("Aria:[E|Neutral] That's my guess.[P] I heard some of the farmers in the southeast were really struggling.[P] The warriors are saying bandits are starting to trickle in from the south, looking for food.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Neutral] Could be a drought, or a war.[P] But I won't speculate any further because a journalist needs more than rumours.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Neutral] I'm sure we'll get to the bottom of it.[P] Come on, Sanya!") ]])
        end
        fnCutsceneBlocker()
    end
end
