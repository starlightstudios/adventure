-- |[ ====================================== Field Abilities ====================================== ]|
--Called when a field ability is used, at the discretion of the field ability script. The variable
-- gbFieldAbilityHandledInput should be set to true if the script handled the ability.

-- |[Arguments]|
--Argument Listing:
-- 0: iSwitchCode - Switch code used by the ability. One of the gciFieldAbility_Activate_[X] series.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local iSwitchCode = tonumber(LM_GetScriptArgument(0))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================== Handling ========================================= ]|
--Extra, Extra!
if(iSwitchCode == gciFieldAbility_Activate_ExtraExtra) then
    fnExtraExtra(fnResolvePath())
end
