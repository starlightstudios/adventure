-- |[ ==================================== Pamphlet: Dragon ==================================== ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ===== Generics ===== ]|
-- |[KitsuneA]|
if(sActorName == "KitsuneA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "KitsuneA", "Neutral") ]])
    fnCutscene([[ Append("Melody:[E|Neutral] Back to work and writing pamphlets again already Izuna?[P] Just be more careful this time.[P] You do after all have the legendary hero in tow.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] Don't worry Melody, anything tries to throw me again, I have Sanya to protect me![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Happy] I'll go -[P] POW![P] No need for a follow-up hit, their grandchildren will feel it.") ]])
    fnCutsceneBlocker()

-- |[KitsuneB]|
elseif(sActorName == "KitsuneB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "KitsuneB", "Neutral") ]])
    fnCutscene([[ Append("Aria:[E|Neutral] So you found the legendary hero, Izuna?[P] Congratulations![P] Just next time you go looking for a long lost legend -[P] tell someone first, okay?") ]])
end


-- |[Clean Up]|
DL_PopActiveObject()
