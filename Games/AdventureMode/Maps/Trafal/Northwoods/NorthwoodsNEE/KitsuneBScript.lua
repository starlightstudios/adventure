-- |[ ======================================== Kitsune B ======================================= ]|
--This kitsune is watering the crops.
local fWalkSpeed = 0.50
local fStairsSpeed = 0.20
local iLingerTicks = 125

--Stop A
fnCutsceneMove("KitsuneB", 28.25, 18.50, fWalkSpeed)
fnCutsceneMove("KitsuneB", 28.25, 20.50, fWalkSpeed)
fnCutsceneFace("KitsuneB", 1, 0)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

--Stop B
fnCutsceneMove("KitsuneB", 28.25, 23.50, fWalkSpeed)
fnCutsceneFace("KitsuneB", 1, 0)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

--Stop C
fnCutsceneMove("KitsuneB", 28.25, 26.50, fWalkSpeed)
fnCutsceneFace("KitsuneB", 1, 0)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

--Stop D
fnCutsceneMove("KitsuneB", 25.25, 26.50, fWalkSpeed)
fnCutsceneMove("KitsuneB", 25.25, 18.50, fWalkSpeed)
fnCutsceneFace("KitsuneB", -1, 0)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

--Loop.
