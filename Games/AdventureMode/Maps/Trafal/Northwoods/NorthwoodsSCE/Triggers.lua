-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
if(sObjectName == "IzunaExplain") then

    -- |[Repeat Check]|
    local iExplainedPufferLake = VM_GetVar("Root/Variables/Chapter2/IzunaExplains/iExplainedPufferfinLake", "N")
    if(iExplainedPufferLake == 1.0) then return end
    
    -- |[Activation Check]|
    local iCompletedHunt = VM_GetVar("Root/Variables/Chapter2/Northwoods/iCompletedHunt", "N")
    if(iCompletedHunt == 0.0) then return end
    
    -- |[Variables]|
    local iEmpressJoined = VM_GetVar("Root/Variables/Chapter2/MtSarulente/iEmpressJoined", "N")
    
    -- |[Flag]|
    VM_SetVar("Root/Variables/Chapter2/IzunaExplains/iExplainedPufferfinLake", "N", 1.0)
    
    -- |[Execution]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("Izuna:[E|Explain] Sanya, this is Pufferfin Lake.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] Nice name.[P] Doesn't look all that interesting.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Explain] Right now, no, but centuries ago this is where the Arulente Marines used to train![B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Explain] There's not much left of it right now, but they would do their aquatic training here.[P] They had to swim across the lake in full armor.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Explain] There was a watch tower here, barracks on the north shore, and supposedly an indoor facility.[B][C]") ]])
    if(iEmpressJoined == 1.0) then
        fnCutscene([[ Append("Empress:[E|Neutral] We used the indoor facility for sporting events, actually.[P] The soldiers were allowed to compete in our twice-yearly competitions, if they wanted.[B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] Athletes came from across the empire to compete.[P] We'd have a feast to commemorate the event.[B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] Remember to wait after eating so you don't cramp when you go swimming.[P] I don't want to have to pull you out.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Happy] Did someone say swimming![P] I love swimming![P] Wanna swim across the lake?[P] Race you![B][C]") ]])
    else
        fnCutscene([[ Append("Sanya:[E|Happy] Awesome![P] I love swimming![P] Wanna swim across the lake?[P] Race you![B][C]") ]])
    end
    fnCutscene([[ Append("Izuna:[E|Happy] Sure![P] Last one in is a soggy fox![B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] BAH![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Offended] What do you want, horn-head?[P] We're going swimming![B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] BAH NYEH![B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] Actually, maybe he doesn't want us to leave him and go swimming for no reason.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Sad] You can't swim, can you, Zeke?[P] Sorry.[B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] Nyeh.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] Well, whatever.[P] Maybe later.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] So how do you know about this marines stuff?[P] There's just an old wall over there.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Explain] There's a painting of the old barracks in one the estates in the Old Capital grounds, and the rest is in the history books.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Explain] The Arulente Marines were the elite of the elite.[P] The toughest soldiers, sent in to deal with problems nobody else could.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Happy] My kind of guys![P] But I bet I could beat their asses.[B][C]") ]])
    if(iEmpressJoined == 1.0) then
        fnCutscene([[ Append("Empress:[E|Neutral] They could give you pause, at least.[P] These were the soldiers who volunteered for the extra training.[P] Athletes by habit.[B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] They can turn your weight and momentum against you.[P] You will need to become as agile as you are powerful.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Happy] Good, I need a challenge![P] Let's go beat up some Olympians![B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Smirk] (I don't know what they are but, by the Goddess, she is so excited about the idea...)") ]])
    else
        fnCutscene([[ Append("Izuna:[E|Smirk] I don't know, Sanya.[P] Those soldiers trained all the time, lived for exertion.[P] It's all they did.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Happy] Yeah, me too![P] That's how I know I'd win!") ]])
    end
    fnCutsceneBlocker()
end
