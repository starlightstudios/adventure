-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= Exits ========================================== ]|
if(sObjectName == "SampleExit") then
    
-- |[ ====================================== Examinables ======================================= ]|
elseif(sObjectName == "Springwater") then

    --Variables.
    local iMetAlraune     = VM_GetVar("Root/Variables/Chapter2/FortKirysu/iMetAlraune", "N")
    local iGotSpringwater = VM_GetVar("Root/Variables/Chapter2/FortKirysu/iGotSpringwater", "N")
    if(iMetAlraune == 1.0 and iGotSpringwater == 0.0) then
        VM_SetVar("Root/Variables/Chapter2/FortKirysu/iGotSpringwater", "N", 1.0)
        LM_ExecuteScript(gsItemListing, "Jug of Springwater")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Fresh springwater, eh?[P] This should make that plant lady happy.)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] [SOUND|World|TakeItem](There, one jug of springwater.)") ]])
        fnCutsceneBlocker()
    end

-- |[ ========================================== Other ========================================= ]|
-- |[ ========================================== Debug ========================================= ]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
