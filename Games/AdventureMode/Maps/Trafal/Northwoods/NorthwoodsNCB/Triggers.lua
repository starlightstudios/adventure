-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
if(sObjectName == "WrongWayW") then

    --Does nothing if the tutorial is not active.
    local iIsTutorialHuntActive = VM_GetVar("Root/Variables/Chapter2/Northwoods/iIsTutorialHuntActive", "N")
    if(iIsTutorialHuntActive == 0.0) then return end

    --Dialogue, move player back a bit.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (I don't think this is the right way...)") ]])
    fnCutsceneBlocker()
    fnCutsceneMoveAmount("Sanya", 1.00, 0.00)
    
elseif(sObjectName == "WrongWayS") then

    --Does nothing if the tutorial is not active.
    local iIsTutorialHuntActive = VM_GetVar("Root/Variables/Chapter2/Northwoods/iIsTutorialHuntActive", "N")
    if(iIsTutorialHuntActive == 0.0) then return end

    --Dialogue, move player back a bit.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (I don't think this is the right way...)") ]])
    fnCutsceneBlocker()
    fnCutsceneMoveAmount("Sanya", 0.00, -1.00)

end
