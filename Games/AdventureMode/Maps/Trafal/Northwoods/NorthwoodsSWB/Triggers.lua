-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
-- |[ =================================== Izuna Explanation ==================================== ]|
if(sObjectName == "IzunaExplain") then

    -- |[Repeat Check]|
    local iExplainedSarulente = VM_GetVar("Root/Variables/Chapter2/IzunaExplains/iExplainedSarulente", "N")
    if(iExplainedSarulente == 1.0) then return end
    
    -- |[Activation Check]|
    local iCompletedHunt = VM_GetVar("Root/Variables/Chapter2/Northwoods/iCompletedHunt", "N")
    if(iCompletedHunt == 0.0) then return end
    
    -- |[Flag]|
    VM_SetVar("Root/Variables/Chapter2/IzunaExplains/iExplainedSarulente", "N", 1.0)
    
    -- |[Execution]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("Izuna:[E|Explain] We're coming up on Mt. Sarulente, the western eye.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Explain] It's the tallest peak in western Trafal.[P] It was said that if you went up on a clear enough day, you could see all the way to the ocean![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Laugh] Sarulente?[P] Didn't you say this continent was called Arulenta?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Explain] Yep![P] Because the great dragon, Empress Arulente, named it after herself.[P] And according to the history books, the village she was born in was partway up this mountain.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] I guess I can let that go.[P] Half the mountains in Europe are just the local word for 'Mountain'.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] The Germans named a river the Elbe river.[P] Which was just the local word for river.[P] Very inventive, guys.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] So, what does Mt. Sarulente have to offer?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Explain] Some people say that, once the empire had crumbled, the dragon empress retired to the ruins of her village, destroyed by Perfect Hatred.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] It's possible she left behind writings or artifacts.[P] We may even be able to find her tomb, and enter it with your runestone![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Offended] It's not a compass, Izuna![B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] Well of course, silly![P] But it can unlock things.[P] There are magic locks that only the runestone can open.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] That only raises further questions![P] How could they make a lock they can't open?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Explain] It's really easy, as long as you don't actually need to get back inside.[P] Which you wouldn't, [P]*if it was a tomb*.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] Good point.[P] Okay.[P] So we're looking for a tomb.[P] Let's go!") ]])
    fnCutsceneBlocker()
end
