-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
if(sObjectName == "WrongWayE") then

    --Does nothing if the tutorial is not active.
    local iIsTutorialHuntActive = VM_GetVar("Root/Variables/Chapter2/Northwoods/iIsTutorialHuntActive", "N")
    if(iIsTutorialHuntActive == 0.0) then return end

    --Dialogue, move player back a bit.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (I don't think this is the right way...)") ]])
    fnCutsceneBlocker()
    fnCutsceneMoveAmount("Sanya", -1.00, 0.00)

elseif(sObjectName == "NoLeave") then

    --Does nothing if the tutorial is not active.
    local iIsTutorialHuntActive = VM_GetVar("Root/Variables/Chapter2/Northwoods/iIsTutorialHuntActive", "N")
    local iSpottedPrey          = VM_GetVar("Root/Variables/Chapter2/Northwoods/iSpottedPrey", "N")
    if(iIsTutorialHuntActive == 0.0 or iSpottedPrey == 0.0) then return end

    --Dialogue, move player back a bit.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Neither of us is leaving until one of us, possibly the goose, is dead.)") ]])
    fnCutsceneBlocker()
    fnCutsceneMoveAmount("Sanya", 1.00, 0.00)
    
elseif(sObjectName == "RoundTwo") then
    
    --This scene activates if the player somehow lost against the goose, went to the campfire, and came back.
    local iSpottedPrey = VM_GetVar("Root/Variables/Chapter2/Northwoods/iSpottedPrey", "N")
    local iCompletedHunt = VM_GetVar("Root/Variables/Chapter2/Northwoods/iCompletedHunt", "N")
    
    --Hunt completed, do nothing:
    if(iCompletedHunt == 1.0) then return end
    
    --Prey has not been spotted yet.
    if(iSpottedPrey == 0.0) then return end
    
    --Scene.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Offended") ]])
    fnCutscene([[ Append("Sanya:[E|Offended] You've got fight in you, goose, and I respect that.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Offended] But this isn't about right and wrong, it's about survival.[P] And no amount of respect means I'll let that fox die![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Offended] This goes to the bell, you little shit!") ]])
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Sanya", 9.25, 37.50)
    
elseif(sObjectName == "SpotPrey") then

    --Repeat check.
    local iSpottedPrey = VM_GetVar("Root/Variables/Chapter2/Northwoods/iSpottedPrey", "N")
    if(iSpottedPrey == 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter2/Northwoods/iSpottedPrey", "N", 1.0)
    
    --Variables.
    local sSanyaForm = VM_GetVar("Root/Variables/Global/Sanya/sForm", "S")
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (..?[P] Something is nearby!)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Camera.
    fnCutsceneCamera("Actor Name", 2.5, "The Goose")
    fnCutsceneMove("Sanya", 8.25, 36.50)
    fnCutsceneFace("Sanya", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    fnCutsceneCamera("Actor Name", 2.5, "Sanya")
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])
    if(sSanyaForm == "Human") then
        fnCutscene([[ Append("Sanya:[E|Neutral] (My mouth is watering...[P] oh man I didn't eat a thing yesterday.)[B][C]") ]])
    elseif(sSanyaForm == "Sevavi") then
        fnCutscene([[ Append("Sanya:[E|Neutral] (Huh, now that I think about it, do I need to eat stuff?[P] Do I eat rocks now?[P] I'm not hungry at all.)[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] (In any case, that fox girl will need to eat.[P] I hope I don't have to transform just to taste food...)[B][C]") ]])
    end
    fnCutscene([[ Append("Sanya:[E|Neutral] (The best approach is to get a bit closer, then use my rifle with the field ability keys.)[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] (If I get into a fight, I'll probably bruise the meat.[P] A clean shot will give me a much better cut.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TheKill") then
    
    --If the hunt is complete, does nothing.
    local iCompletedHunt = VM_GetVar("Root/Variables/Chapter2/Northwoods/iCompletedHunt", "N")
    if(iCompletedHunt == 1.0) then return end
    
    --Unlock this ability for Agontea.
    AdvCombat_SetProperty("Push Party Member", "Sanya")
    
        --Add it.
        AdvCombatEntity_SetProperty("Register Ability To Job", "Agontea|Kiss Izuna", "Agontea")
        
        --Move it up 3 slots.
        AdvCombatEntity_SetProperty("Push Job S", "Agontea")
            AdvCombatJob_SetProperty("Move Ability Up One Slot", "Agontea|Kiss Izuna")
            AdvCombatJob_SetProperty("Move Ability Up One Slot", "Agontea|Kiss Izuna")
            AdvCombatJob_SetProperty("Move Ability Up One Slot", "Agontea|Kiss Izuna")
        DL_PopActiveObject()
    DL_PopActiveObject()
    
    --Check if the player has the bruised meat in their inventory. If so, they screwed up. Chastise them and move on.
    local iBruisedMeat = AdInv_GetProperty("Item Count", "Bruised Meat")
    if(iBruisedMeat > 0) then
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter2/Northwoods/iCompletedHunt", "N", 1.0)
    
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])
        fnCutscene([[ Append("Sanya:[E|Offended] Stupid goose![P] Now you're stupid and dead![B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Sad] Hm, the meat's a bit beaten up.[P] I guess it's edible, but in the future I should use my rifle at range and try to get the goose without getting got myself.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Smirk] I'll get better materials if I use my rifle instead of my rifle and fists and knife and possibly my feet.[P] Yep.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Smirk] All right, time to go feed a hungry cosplayer.") ]])
        fnCutsceneBlocker()
        
        --Fade out.
        fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
        fnCutsceneWait(65)
        fnCutsceneBlocker()
        
        --Cut to next scene.
        fnCutscene([[ AL_BeginTransitionTo("SanyaCabinA", "FORCEPOS:30.0x31.0x0") ]])
        return
    end
    
    --High quality meat, means they did not screw up.
    local iBirdMeat = AdInv_GetProperty("Item Count", "Bird Meat")
    if(iBirdMeat > 0) then
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter2/Northwoods/iCompletedHunt", "N", 1.0)
    
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])
        fnCutscene([[ Append("Sanya:[E|Happy] Ha ha![P] Gonna be needing a new use for that long neck![B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Smirk] Don't worry, pal.[P] You didn't suffer, and your death is going to a good cause.[P] Feeding a fox girl.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Smirk] Guess I better get this prime back to the cabin and make something to eat.") ]])
        fnCutsceneBlocker()
        
        --Fade out.
        fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
        fnCutsceneWait(65)
        fnCutsceneBlocker()
        
        --Cut to next scene.
        fnCutscene([[ AL_BeginTransitionTo("SanyaCabinA", "FORCEPOS:30.0x31.0x0") ]])
        return
    end
end
