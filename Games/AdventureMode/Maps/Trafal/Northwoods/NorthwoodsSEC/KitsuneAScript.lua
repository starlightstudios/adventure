-- |[ ======================================== Kitsune A ======================================= ]|
--This kitsune is tending to the crops.
local fWalkSpeed = 0.50
local fStairsSpeed = 0.20
local iLingerTicks = 125

--Stop A
fnCutsceneMove("KitsuneA", 16.25, 23.50, fWalkSpeed)
fnCutsceneFace("KitsuneA", 0, -1)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

--Stop B
fnCutsceneMove("KitsuneA", 18.25, 23.50, fWalkSpeed)
fnCutsceneFace("KitsuneA", 0, -1)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

--Stop C
fnCutsceneMove("KitsuneA", 21.25, 23.50, fWalkSpeed)
fnCutsceneFace("KitsuneA", 0, -1)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

--Stop D
fnCutsceneMove("KitsuneA", 15.25, 23.50, fWalkSpeed)
fnCutsceneFace("KitsuneA", 0, -1)
fnCutsceneBlocker()
fnCutsceneWait(iLingerTicks)
fnCutsceneBlocker()

--Loop.
