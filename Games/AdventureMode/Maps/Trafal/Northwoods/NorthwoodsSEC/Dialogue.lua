-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[Name Resolve]|
    --Resolve the name of the calling entity.
    local sActorName = TA_GetProperty("Name")

    --Set facing.
    TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[Common]|
    local iKitsuneState = fnResolveKitsuneState()
    
    -- |[KitsuneA]|
    if(sActorName == "KitsuneA") then
        
        -- |[Variables]|
        local iMetSonoko            = VM_GetVar("Root/Variables/Chapter2/KitsuneVillage/iMetSonoko", "N")
        local sSanyaForm            = VM_GetVar("Root/Variables/Global/Sanya/sForm", "S")
        local sFormMetSonokoIn      = VM_GetVar("Root/Variables/Chapter2/KitsuneVillage/sFormMetSonokoIn", "S")
        local iSonokoKnowsTransform = VM_GetVar("Root/Variables/Chapter2/KitsuneVillage/iSonokoKnowsTransform", "N")
        
        -- |[Setup]|
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "KitsuneC", "Neutral") ]])
        
        -- |[First Meeting]|
        if(iMetSonoko == 0.0) then
            
            --Flags.
            VM_SetVar("Root/Variables/Chapter2/KitsuneVillage/iMetSonoko", "N", 1.0)
            VM_SetVar("Root/Variables/Chapter2/KitsuneVillage/sFormMetSonokoIn", "S", sSanyaForm)
            
            --Dialogue.
            fnCutscene([[ Append("Warrior:[E|Neutral] Hello, Izuna![P] It has been too long![B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Explain] Sanya, meet Sonoko.[P] She's a warrior kitsune![B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Smirk] Charmed.[P] You kill anything big?[B][C]") ]])
            fnCutscene([[ Append("Sonoko:[E|Neutral] No, no.[P] A true warrior does not measure themselves by what they kill, but what they do not kill.[B][C]") ]])
            fnCutscene([[ Append("Sonoko:[E|Neutral] With a single stroke of my sword, I ended the lord of Soswitch who was preparing for war with Rondheim.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Laugh] So that's one kill for you but negative a thousand kills for them?[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Smirk] She's joking, Sonoko![B][C]") ]])
            fnCutscene([[ Append("Sonoko:[E|Neutral] Ha, I see.[P] Sometimes in this grim business, I forget to laugh.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Smirk] Where I'm from, black humour is the only kind you're going to get.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] We made an entire play based off a man lying on top of a mine.[B][C]") ]])
            fnCutscene([[ Append("Sonoko:[E|Neutral] A...[P] mine?[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] Bombs that are hidden on the ground.[P] You step on one and it explodes.[P] At best you die instantly.[P] Worst, you lose your legs and bleed to death.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Smirk] Gives you a rough idea of where I grew up.[B][C]") ]])
            fnCutscene([[ Append("Sonoko:[E|Neutral] And just where is that?[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Smirk] Croatia.[P] I'm from Earth.[B][C]") ]])
            fnCutscene([[ Append("Sonoko:[E|Neutral] And how did you meet Izuna?[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Smirk] She saved me when I almost drowned![B][C]") ]])
            fnCutscene([[ Append("Sonoko:[E|Neutral] I see.[P] When did this happen?[P] I was not told![B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Smirk] A month ago.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Neutral] Sonoko is a warrior who travels the world.[P] She's often gone for years at a time.[B][C]") ]])
            fnCutscene([[ Append("Sonoko:[E|Neutral] I'm sorry I could not have tried to help, Izuna.[P] If I had known, I would have come back and searched for you.[B][C]") ]])
            fnCutscene([[ Append("Sonoko:[E|Neutral] But it seems you have made good friends in my absence.[P] Sanya, if you have the heart of a warrior, you may want to apply at the temple.[P] I'm sure Izuna could put in a good word for you.[B][C]") ]])
            if(sSanyaForm == "Human") then
                fnCutscene([[ Append("Sanya:[E|Happy] Could you transform me into a kitsune for that?[B][C]") ]])
                fnCutscene([[ Append("Sonoko:[E|Neutral] No, Sanya.[P] Only you can do that, and it takes study.[P] Discipline.[P] Focus.[B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Neutral] Okay so I'll put it on my 'maybe' list.[P] Furry tails would be useful but I'm on a schedule here.[B][C]") ]])
            elseif(sSanyaForm == "Sevavi") then
                fnCutscene([[ Append("Sanya:[E|Happy] Do you accept statues?[B][C]") ]])
                fnCutscene([[ Append("Sonoko:[E|Neutral] Any who would devote themselves, yes.[B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Neutral] Might have to pass.[P] We're on the clock.[B][C]") ]])
            elseif(sSanyaForm == "Werebat") then
                fnCutscene([[ Append("Sanya:[E|Happy] Is it okay if I'm a bat?[B][C]") ]])
                fnCutscene([[ Append("Sonoko:[E|Neutral] We take any who can prove themselves disciplined and wise.[B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Sad] Oh man, wise?[P] Maybe you can do it, Zeke.[B][C]") ]])
                fnCutscene([[ Append("Zeke:[E|Neutral] Bah.[B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Neutral] He doesn't seem enthused.[B][C]") ]])
            elseif(sSanyaForm == "Harpy") then
                fnCutscene([[ Append("Sanya:[E|Happy] Do they take harpies?[B][C]") ]])
                fnCutscene([[ Append("Sonoko:[E|Neutral] Certainly.[P] Should we not?[B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Happy] Do they take goats?[B][C]") ]])
                fnCutscene([[ Append("Zeke:[E|Neutral] Bah.[B][C]") ]])
                fnCutscene([[ Append("Sonoko:[E|Neutral] Of course.[P] They do not need to prove their wisdom.[B][C]") ]])
            elseif(sSanyaForm == "Bunny") then
                fnCutscene([[ Append("Sanya:[E|Neutral] Sounds like a hot ticket.[B][C]") ]])
                fnCutscene([[ Append("Sonoko:[E|Neutral] A hot.[P] Ticket?[B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Neutral] I don't understand half of her euphemisms but I can always guess from the context.[B][C]") ]])
            end
            fnCutscene([[ Append("Sonoko:[E|Neutral] And what errand are you on?[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Ugh] Perfect Hatred is coming back, Sonoko.[P] Sanya is going to stop him.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Ugh] The thought of that thing laying waste to the people of Trafal...[P] like it did so many years ago...[B][C]") ]])
            fnCutscene([[ Append("Sonoko:[E|Neutral] The old legend?[P] Hm.[B][C]") ]])
            fnCutscene([[ Append("Sonoko:[E|Neutral] A year ago I would have doubted you, but my time abroad has changed my opinion.[B][C]") ]])
            fnCutscene([[ Append("Sonoko:[E|Neutral] War is threatened everywhere.[P] Crops wilt on the vines.[P] The seas grow stormy, and in some places, the wind refuses to blow.[B][C]") ]])
            fnCutscene([[ Append("Sonoko:[E|Neutral] The blackness that lies in the heart of every man finds new purchase in a world of turmoil.[P] That is what I have seen.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Laugh] Man, do I have to kick the whole world's ass?[P] How am I going to punch the wind to make it blow?[B][C]") ]])
            fnCutscene([[ Append("Sonoko:[E|Neutral] That is why the kitsune warriors seek wisdom, Sanya.[P] Not every enemy can be punched.[P] I certainly would if I could.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Happy] You're all right in my book, Sonoko.") ]])
        
        -- |[Changed Form]|
        elseif(iSonokoKnowsTransform == 0.0 and sFormMetSonokoIn ~= sSanyaForm) then
        
            --Flag.
            VM_SetVar("Root/Variables/Chapter2/KitsuneVillage/iSonokoKnowsTransform", "N", 1.0)
            
            --Dialogue.
            fnCutscene([[ Append("Sonoko:[E|Neutral] Sanya?[P] Is that you?[B][C]") ]])
            if(sSanyaForm == "Human") then
                fnCutscene([[ Append("Sanya:[E|Happy] In the flesh, baby![B][C]") ]])
            elseif(sSanyaForm == "Sevavi") then
                fnCutscene([[ Append("Sanya:[E|Happy] New and improved![P] I mean my butt, by the way.[B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Blush] It's a very nice butt![B][C]") ]])
                fnCutscene([[ Append("Zeke:[E|Neutral] Bah![B][C]") ]])
            elseif(sSanyaForm == "Werebat") then
                fnCutscene([[ Append("Sanya:[E|Neutral] Yeah, what of it?[B][C]") ]])
            else
                fnCutscene([[ Append("Sanya:[E|Neutral] Unhandled form bug![B][C]") ]])
            end
            fnCutscene([[ Append("Sonoko:[E|Neutral] I did not mean to offend.[P] It's just that you seem to have transformed.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Blush] Oh yeah, that transforming thing?[P] Yeah a lot of people can't do that.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Smirk] I told you of the legend.[P] Sanya's the real thing.[B][C]") ]])
            fnCutscene([[ Append("Sonoko:[E|Neutral] The elder always cautions us against believing things so easily, but she has not see what I have seen.[B][C]") ]])
            fnCutscene([[ Append("Sonoko:[E|Neutral] If the dark days prophesied really are upon us, then we will need your help, Sanya.[B][C]") ]])
            fnCutscene([[ Append("Sonoko:[E|Neutral] I will try to convince the others, but...[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Smirk] It's never easy.[P] Easier to pretend like it's not happening.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Neutral] Do your best, Sonoko.[P] We'll try, too...") ]])
            
        -- |[General Repeat]|
        else
            fnCutscene([[ Append("Sanya:[E|Smirk] So what are you doing, Sonoko?[B][C]") ]])
            fnCutscene([[ Append("Sonoko:[E|Neutral] I have decided to spend some time at the farm here, tending to it and making sure nobody harms the farmers.[B][C]") ]])
            fnCutscene([[ Append("Sonoko:[E|Neutral] The people of Trafal are tough, but I have seen brigands roaming the roads.[P] And those harpies...[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] What of the harpies?[B][C]") ]])
            fnCutscene([[ Append("Sonoko:[E|Neutral] They arrived recently, and seem to be dressed for war.[P] But I don't know who they are or what they want.[B][C]") ]])
            fnCutscene([[ Append("Sonoko:[E|Neutral] Though they seem to have no love for the brigands, so I leave them alone.[P] We're keeping our eyes on them.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Smirk] Good to know.") ]])
        end
        fnCutsceneBlocker()
    
    -- |[AlrauneA]|
    elseif(sActorName == "AlrauneA") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Alraune:[VOICE|Alraune] What a lovely day it is![P] The mountain air is always fresh, and the little ones always lively.") ]])
        fnCutsceneBlocker()
    
    -- |[HumanA]|
    elseif(sActorName == "HumanA") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Woman:[VOICE|HumanF1] There's so many fish that we almost don't need the crops!") ]])
        fnCutsceneBlocker()
    
    -- |[HumanB]|
    elseif(sActorName == "HumanB") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Man:[VOICE|HumanM1] It's so peaceful here in Trafal.[P] I hope it stays this way forever.") ]])
        fnCutsceneBlocker()
    
    
    end
end
