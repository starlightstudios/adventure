-- |[ ============================= Pamphlet: Bandits in Westwoods ============================= ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ====== Minor Characters ====== ]|
if(sActorName == "KitsuneA") then
    
    -- |[Setup]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "KitsuneC", "Neutral") ]])
    fnCutscene([[ Append("Sonoko:[E|Neutral] I am unsurprised by this news.[P] I have been hearing rumours of a horde of disaffected farmers and petty thieves coming from the south.[B][C]") ]])
    fnCutscene([[ Append("Sonoko:[E|Neutral] Unfortunately, once one has a taste for action, it is difficult to dislodge.[P] Will they put away their swords and go home?[P] Do they have homes to go to?[B][C]") ]])
    fnCutscene([[ Append("Sonoko:[E|Neutral] I will be sure to be at the next temple meeting.[P] My expertise abroad will be needed.") ]])

-- |[ ========== Generics ========== ]|
-- |[AlrauneA]|
elseif(sActorName == "AlrauneA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Alraune:[VOICE|Alraune] Crop failures are causing bandits.[P] But why?[P] If they had alraunes to tend the little ones they would have caught the problem.[P] Perhaps the environment around the little ones changed?[P] I'll have to tend to these ones even more carefully.") ]])
    fnCutsceneBlocker()

-- |[HumanA]|
elseif(sActorName == "HumanA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Woman:[VOICE|HumanF1] A pamphlet?[P] Let me see that.[P] Bandits, driven by hunger?[P] I hope they don't fish out my lake.[P] It's a big part of the ecosystem up here.[P][P] Be nice if they ate the toads though, make a horrible racket at night.") ]])
    fnCutsceneBlocker()

-- |[HumanB]|
elseif(sActorName == "HumanB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Man:[VOICE|HumanM1] Bandits will disturb the peace but so will crop failure if they spread.[P] Do you know why they failed?[P] Is there a blight down there?[P] I don't think we've used any seeds or cuttings from down south, but we should still be cautious.[P] I'll have my Alraune friend talk to all the plants.") ]])
    fnCutsceneBlocker()
end

-- |[Clean Up]|
DL_PopActiveObject()
