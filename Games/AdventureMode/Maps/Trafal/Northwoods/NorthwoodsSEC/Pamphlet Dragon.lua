-- |[ ==================================== Pamphlet: Dragon ==================================== ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ====== Minor Characters ====== ]|
if(sActorName == "KitsuneA") then
    
    -- |[Setup]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "KitsuneC", "Neutral") ]])
    fnCutscene([[ Append("Sonoko:[E|Neutral] Truly, the time of legends is upon us.[P] The kitsunes support you all the way, Sanya.[B][C]") ]])
    fnCutscene([[ Append("Sonoko:[E|Neutral] Empress, please give your wisdom to these ones.[P] Trafal rests on your shoulders.") ]])

-- |[ ========== Generics ========== ]|
-- |[AlrauneA]|
elseif(sActorName == "AlrauneA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Alraune:[VOICE|Alraune] Is strange old ladies in caves really a good basis for picking heroes?[P] I guess we will see.[P] It doesn't say if the dragon breathed fire.[P] She tries to breathe fire here she's not going to like what happens next.") ]])
    fnCutsceneBlocker()

-- |[HumanA]|
elseif(sActorName == "HumanA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Woman:[VOICE|HumanF1] You know I heard about the legend of the hero my whole life growing up.[P] If she's really appeared I hope she's doing alright.[P] It must be hard to have everyone expect things from her.") ]])
    fnCutsceneBlocker()

-- |[HumanB]|
elseif(sActorName == "HumanB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Man:[VOICE|HumanM1] So a rich old lady is coming out of hiding and it's news?[P] What is she going to want next, a parade?[P] Sonoko is a hero who saved my life and hundreds of other lives, seems like that is worth a legend to me.") ]])
    fnCutsceneBlocker()
end

-- |[Clean Up]|
DL_PopActiveObject()
