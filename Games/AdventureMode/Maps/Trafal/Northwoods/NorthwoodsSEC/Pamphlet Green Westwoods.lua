-- |[ =============================== Pamphlet: Green Westwoods =============================== ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ====== Minor Characters ====== ]|
if(sActorName == "KitsuneA") then
    
    -- |[Setup]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "KitsuneC", "Neutral") ]])
    fnCutscene([[ Append("Sonoko:[E|Neutral] Hah![P] Look at me, being perpetually gloomy, and you come to me with the best news I've heard in a while.[P] Alraunes working to clear the blighted cauldron![B][C]") ]])
    fnCutscene([[ Append("Sonoko:[E|Neutral] I know that if we work together, we can accomplish anything.[P] Thank you, Izuna.") ]])

-- |[ ========== Generics ========== ]|
-- |[AlrauneA]|
elseif(sActorName == "AlrauneA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Alraune:[VOICE|Alraune] And you say the leaf sisters were all summoned by a dream?[P] It's like something from a story.[P] I've lived here a long time and the grandfather never thought to summon me.[P] I guess it really would take a coven but still.") ]])
    fnCutsceneBlocker()

-- |[HumanA]|
elseif(sActorName == "HumanA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Woman:[VOICE|HumanF1] Oh wow, the alraunes are healing the cauldron?[P] That's wonderful![P] Think they need any fish if they manage to purify the water?[P] The sooner we get the ecosystem reestablished, the faster it will heal.") ]])
    fnCutsceneBlocker()

-- |[HumanB]|
elseif(sActorName == "HumanB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Man:[VOICE|HumanM1] The cauldron is getting healed?[P] Good farm land down there with the lake,[P] if the environment stops being awful.[P] I've known enough alraunes to know they might not be the happiest with people trying to cultivate the revitalized environment though.") ]])
    fnCutsceneBlocker()
end

-- |[Clean Up]|
DL_PopActiveObject()
