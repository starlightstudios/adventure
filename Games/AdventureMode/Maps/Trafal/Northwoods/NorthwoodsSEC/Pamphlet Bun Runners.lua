-- |[ ============================= Pamphlet: The Bunny Smugglers ============================== ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ====== Minor Characters ====== ]|
if(sActorName == "KitsuneA") then
    
    -- |[Setup]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "KitsuneC", "Neutral") ]])
    fnCutscene([[ Append("Sonoko:[E|Neutral] You know, I caught one of the bunnies a while back.[P] She, er, he, had been running across a farmer's field.[P] Turns out it was just a shortcut, he wasn't stealing anything.[B][C]") ]])
    fnCutscene([[ Append("Sonoko:[E|Neutral] I thought the lack of a pronounced chest was odd, but not everyone is gifted, so I thought nothing of it.[P] Turns out my first instinct was right![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Was he hot?[B][C]") ]])
    fnCutscene([[ Append("Sonoko:[E|Neutral] In retrospect?[P] Six.[P] Six and a half.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] Nice.") ]])

-- |[ ========== Generics ========== ]|
-- |[AlrauneA]|
elseif(sActorName == "AlrauneA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Alraune:[VOICE|Alraune] I have no problem with bunnies, I do wonder how many men are going to try and get partially joined only to find out that it doesn't work quite like that.[P] We might see a boom in leaf sisters soon![P] Wouldn't that be nice, little ones?") ]])
    fnCutsceneBlocker()

-- |[HumanA]|
elseif(sActorName == "HumanA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Woman:[VOICE|HumanF1] So that's why you see bunnies running around so much.[P] You think they might take food for payment?[P] Would they punch me if I offered carrots?") ]])
    fnCutsceneBlocker()

-- |[HumanB]|
elseif(sActorName == "HumanB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Man:[VOICE|HumanM1] Huh, she's got her own harem of monster boys?[P] Good for her.[P] I wouldn't be surprised if the boys are in high demand among some.[P] All of the monster, none of the risk of transformation just for a lay.") ]])
    fnCutsceneBlocker()
end

-- |[Clean Up]|
DL_PopActiveObject()
