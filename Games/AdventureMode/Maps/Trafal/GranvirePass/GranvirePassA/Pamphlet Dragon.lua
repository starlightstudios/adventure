-- |[ ==================================== Pamphlet: Dragon ==================================== ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[RecruitA]|
if(sActorName == "RecruitA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HarpyRecruit] Dragons and legendary heroes?[P] Pass.") ]])

-- |[RecruitB]|
elseif(sActorName == "RecruitB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HarpyRecruit] Fairy tales are for kids.[P] No thanks.") ]])

-- |[RecruitC]|
elseif(sActorName == "RecruitC") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HarpyRecruit] A dragon?[P] Hey I've heard there's a dragon's tomb on the mountain west of here.[P] What's a dragon look like, anyway?") ]])

-- |[Officer]|
elseif(sActorName == "Officer") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Officer:[VOICE|HarpyOfficer] Is this some sort of kitsune fairy tail?[P] A prank?[P] I've heard they like to prank people.") ]])
end

-- |[Clean Up]|
DL_PopActiveObject()
