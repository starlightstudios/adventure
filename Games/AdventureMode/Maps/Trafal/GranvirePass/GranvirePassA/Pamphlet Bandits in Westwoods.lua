-- |[ ============================= Pamphlet: Bandits in Westwoods ============================= ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[RecruitA]|
if(sActorName == "RecruitA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HarpyRecruit] Bandits are just what we're here to stop!") ]])

-- |[RecruitB]|
elseif(sActorName == "RecruitB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HarpyRecruit] Oh good, maybe they'll reassign us down south now that our priorities have shifted.") ]])

-- |[RecruitC]|
elseif(sActorName == "RecruitC") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HarpyRecruit] I asked for this assignment because I thought it'd be low-danger. I am happy where I am, actually. Let the others fight the bandits.") ]])

-- |[Officer]|
elseif(sActorName == "Officer") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Officer:[VOICE|HarpyOfficer] The bandits are down south, and we're watching this gate? Bah, I hate strategy. Assign us to fight them!") ]])
end

-- |[Clean Up]|
DL_PopActiveObject()
