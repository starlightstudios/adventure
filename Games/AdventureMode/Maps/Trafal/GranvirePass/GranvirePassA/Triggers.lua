-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ================================== Seeing the Blockade =================================== ]|
if(sObjectName == "TheHarpies") then
    
    -- |[Repeat Check]|
    local iBlockadeResolved = VM_GetVar("Root/Variables/Chapter2/HarpyBlockade/iBlockadeResolved", "N")
    local iSawBlockade      = VM_GetVar("Root/Variables/Chapter2/HarpyBlockade/iSawBlockade", "N")
    if(iBlockadeResolved == 1.0 or iSawBlockade == 1.0) then return end
    
    -- |[Variables]|
    local sSanyaForm = VM_GetVar("Root/Variables/Global/Sanya/sForm", "S")
    local bHasWingBadgeOn = fnIsItemEquipped("Wing Badge")
    local iEmpressJoined = VM_GetVar("Root/Variables/Chapter2/MtSarulente/iEmpressJoined", "N")
    
    -- |[Flag]|
    VM_SetVar("Root/Variables/Chapter2/HarpyBlockade/iSawBlockade", "N", 1.0)
    
    -- |[Movement]|
    fnCutsceneMergeParty()
    fnCutsceneBlocker()
    fnCutsceneMove("Sanya", 16.25, 27.50)
    fnCutsceneFace("Sanya", 0, 1)
    fnCutsceneMove("Izuna", 17.25, 27.50)
    fnCutsceneFace("Izuna", 0, 1)
    fnCutsceneMove("Zeke",  15.25, 27.50)
    fnCutsceneFace("Zeke",  0, 1)
    if(iEmpressJoined == 1.0) then
        fnCutsceneMove("Empress", 16.25, 26.50)
        fnCutsceneFace("Empress", 0, 1)
    end
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    --Sanya is in Harpy form or has the wing badge on:
    if(sSanyaForm == "Harpy" or bHasWingBadgeOn == true) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        fnCutscene([[ Append("Izuna:[E|Neutral] What's this?[P] The harpies put up a gate?[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] I'm sure they'll let us through.") ]])
        fnCutsceneBlocker()
    
    --Empress not present.
    elseif(iEmpressJoined == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        fnCutscene([[ Append("Izuna:[E|Neutral] What's this?[P] Someone put up a gate.[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Explain] Let's go politely ask them to let us through![B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Laugh] Yeah![P] People who put up gates are famous for letting people through when asked![P] Let me handle this![B][C]") ]])
        fnCutscene([[ Append("Zeke:[E|Neutral] ...[P] Nyeh...") ]])
        fnCutsceneBlocker()

    --Empress is present.
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        fnCutscene([[ Append("Izuna:[E|Neutral] What's this?[P] Someone put up a gate.[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Explain] Let's go politely ask them to let us through![B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] This will end well.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Laugh] What are you talking about?[P] I know they'll let us through, because I'm sure they're very nice people.[B][C]") ]])
        fnCutscene([[ Append("Zeke:[E|Neutral] ...[P] Nyeh...[B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] Very well.[P] I will be watching your...[P] diplomacy skills.") ]])
        fnCutsceneBlocker()
    end
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    -- |[Movement]|
    fnCutsceneMove("Sanya", 16.25, 30.50)
    fnCutsceneMove("Sanya", 17.25, 30.50)
    fnCutsceneMove("Sanya", 17.25, 31.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[ ====== Harpy Variant ===== ]|
    if(sSanyaForm == "Harpy") then
        
        -- |[Flags]|
        VM_SetVar("Root/Variables/Chapter2/HarpyBlockade/iBlockadeResolved", "N", 1.0)
        VM_SetVar("Root/Variables/Chapter2/HarpyBlockade/iSawBlockade", "N", 1.0)
        VM_SetVar("Root/Variables/Chapter2/HarpyBlockade/iFlankedBlockade", "N", 1.0)
        
        -- |[Dialogue]|
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "HarpyRecruit", "Neutral") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] Hey comrade, could you open the gate?[B][C]") ]])
        fnCutscene([[ Append("Recruit:[E|Neutral] Sure thing![P] One moment.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutscenePlaySound("World|FlipSwitch")
        AL_SetProperty("Set Collision", 17, 34, 0, 0)
        AL_SetProperty("Set Collision", 18, 34, 0, 0)
        fnCutsceneLayerDisabled("BarsHi", true)
        fnCutsceneLayerDisabled("BarsLo", true)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "HarpyRecruit", "Neutral") ]])
        fnCutscene([[ Append("Recruit:[E|Neutral] All set! Hey, when's your break?[P] You should meet up with us for drinks when we're off rotation![B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Smirk] I'm kind of on an odd assignment.[P] Gotta save the world and stuff.[P] Where should I meet you?[B][C]") ]])
        fnCutscene([[ Append("Recruit:[E|Neutral] Poterup village, of course.[P] Good luck with your assignment!") ]])
        fnCutsceneBlocker()
        fnCutsceneMove("RecruitA", 16.25, 35.50)
        fnCutsceneFace("RecruitA", 1, 0)
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        -- |[Finish Up]|
        fnAutoFoldParty()
        fnCutsceneBlocker()
    
    -- |[ === Wing Badge Variant === ]|
    elseif(bHasWingBadgeOn == true) then
        
        -- |[Flags]|
        VM_SetVar("Root/Variables/Chapter2/HarpyBlockade/iBlockadeResolved", "N", 1.0)
        VM_SetVar("Root/Variables/Chapter2/HarpyBlockade/iSawBlockade", "N", 1.0)
        VM_SetVar("Root/Variables/Chapter2/HarpyBlockade/iFlankedBlockade", "N", 1.0)
        
        -- |[Dialogue]|
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "HarpyRecruit", "Neutral") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] Hey comrade, could you open the gate?[B][C]") ]])
        fnCutscene([[ Append("Recruit:[E|Neutral] No.[P] We're -[P] oh![P] I'm sorry.[B][C]") ]])
        fnCutscene([[ Append("Recruit:[E|Neutral] You've got the flock leader's badge on, I see.[P] Go right through.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutscenePlaySound("World|FlipSwitch")
        AL_SetProperty("Set Collision", 17, 34, 0, 0)
        AL_SetProperty("Set Collision", 18, 34, 0, 0)
        fnCutsceneLayerDisabled("BarsHi", true)
        fnCutsceneLayerDisabled("BarsLo", true)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "HarpyRecruit", "Neutral") ]])
        fnCutscene([[ Append("Sanya:[E|Smirk] Thanks a bunch, pal.[P] You're all right.[B][C]") ]])
        fnCutscene([[ Append("Recruit:[E|Neutral] Are you hitting on me?[P] Or...[P] maybe being out here all the time is getting to me.[P] Anyway, we should meet up for drinks later.[B][C]") ]])
        fnCutscene([[ Append("Recruit:[E|Neutral] Swing by Poterup village if you've got time, sweet thing.") ]])
        fnCutsceneBlocker()
        fnCutsceneMove("RecruitA", 16.25, 35.50)
        fnCutsceneFace("RecruitA", 1, 0)
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        -- |[Finish Up]|
        fnAutoFoldParty()
        fnCutsceneBlocker()
    
    -- |[ ==== Failed Diplomacy ==== ]|
    else
    
        -- |[Dialogue]|
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "HarpyRecruit", "Neutral") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] Hey![P] Can I come through here?[B][C]") ]])
        fnCutscene([[ Append("Recruit:[E|Neutral] No.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Angry] YOU SON OF A - [P][CLEAR]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] Wait that was my bad, sorry.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] [P]*May*[P] I come through here?[B][C]") ]])
        fnCutscene([[ Append("Recruit:[E|Neutral] No.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Angry] I'LL KILL YOU![P] I'LL KILL YOUR WHOLE FAMILY![P] YOU'LL LIVE JUST LONG ENOUGH TO REGRET THIS![B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Angry] LET ME THROUGH THIS GATE BEFORE I BITE THROUGH IT WITH MY GODDAMN TEETH.[B][C]") ]])
        fnCutscene([[ Append("Recruit:[E|Neutral] Woah![P] Calm down![B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Offended] I'll calm down when I skin you and hang you above my fireplace![B][C]") ]])
        fnCutscene([[ Append("Recruit:[E|Neutral] Holy moley![P] No way in hell am I letting you through here now![B][C]") ]])
        fnCutscene([[ Append("Recruit:[E|Neutral] Get lost before I call the others and we start pelting you with arrows![B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Angry] LINE UP SO I CAN SAVE AMMO AS I BLOW YOUR HEADS OFF!!!!") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        -- |[Movement]|
        fnCutsceneMove("Zeke",  15.25, 29.50)
        fnCutsceneMove("Zeke",  17.25, 29.50)
        fnCutsceneMove("Zeke",  17.25, 30.50)
        fnCutsceneBlocker()
        fnCutsceneWait(15)
        fnCutsceneBlocker()
        fnCutsceneMoveFace("Zeke",  17.25, 27.50, 0, 1)
        fnCutsceneMoveFace("Sanya", 17.25, 28.50, 0, 1)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneFace("Sanya", 0, -1)
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        -- |[Dialogue]|
        --Empress not present.
        if(iEmpressJoined == 0.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
            fnCutscene([[ Append("Izuna:[E|Explain] That went incredibly badly![B][C]") ]])
            fnCutscene([[ Append("Zeke:[E|Neutral] Bah...[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Sad] Why you gotta smile when you say that, Izuna?[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Smirk] Because you tried really hard, and failed spectacularly![B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Explain] But if you can work on it even a little bit, you'll have all that passion, and the skills to match![B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Smirk] That relentless optimism is really sweet, sweetie.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Blush] Thanks![B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Smirk] So, are you going to give it another try?[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Sad] No, I think they wouldn't even yell a warning before firing on me.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Happy] Oh my goodness, you know what that means?[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Happy] SNEAK APOLOGY![B][C]") ]])
            fnCutscene([[ Append("Zeke:[E|Neutral] Nyeh!?[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Smirk] The hell is a sneak apology?[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Explain] We're gonna sneak up on them and beg for forgiveness![P] It'll be awesome![B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Explain] I think there's a way around the gate there, or we could go through Vuca pass to get behind them![B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Happy] And then you'll apologize so hard that everyone will want to be your friend and we'll all be best pals![B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Smirk] (My god she is the sweetest person in any universe I've ever been in.)[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Smirk] Okay then.[P] Let's get going.") ]])
            fnCutsceneBlocker()

        --Empress is present.
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
            fnCutscene([[ Append("Empress:[E|Neutral] You sure did impress me, just not in the way I had hoped.[B][C]") ]])
            fnCutscene([[ Append("Zeke:[E|Neutral] Bah...[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Sad] I, uh, maaaaay have lost my cool a bit there.[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] Immediately?[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Sad] It took like five seconds.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Cry] Okay so maybe it didn't go well.[P] Could any of us have done any better?[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] Yes.[B][C]") ]])
            fnCutscene([[ Append("Zeke:[E|Neutral] Bah.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Neutral] Yes.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Angry] Don't cheat and answer your own question![B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] You're doing it again.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Sad] Oh crap.[P] This is what mom always called a 'growth opportunity'.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Happy] REALLY?![B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Happy] Because Elder Yukina says the same thing![P] Your mom must be really wise![B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] So anyway, now that I've screwed the pooch and left it by the side of the road, what now?[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] I would suggest finding an alternate path so that you might apologize.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Offended] What kind of diplomacy is apologizing?[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] The core of the field, actually.[P] Unless you happen to be a warmonger.[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] Which I am going to state, right now, I would not approve of.[B][C]") ]])
            fnCutscene([[ Append("Zeke:[E|Neutral] Nyeh![B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] (Maybe next time we should send Zeke instead.)[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Explain] I believe there's another path that should take us around the barricade, or we could go through Vuca pass instead![B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Explain] Up to you, Sanya.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] Okay, let's go.") ]])
            fnCutsceneBlocker()
        end
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        -- |[Finish Up]|
        fnAutoFoldParty()
        fnCutsceneBlocker()
    end

-- |[ ================================ Got Behind The Blockade ================================= ]|
elseif(sObjectName == "FlankAttack") then
    
    -- |[Repeat Check]|
    local iBlockadeResolved = VM_GetVar("Root/Variables/Chapter2/HarpyBlockade/iBlockadeResolved", "N")
    local iSawBlockade      = VM_GetVar("Root/Variables/Chapter2/HarpyBlockade/iSawBlockade", "N")
    if(iBlockadeResolved == 1.0) then return end
    
    -- |[Variables]|
    local iEmpressJoined  = VM_GetVar("Root/Variables/Chapter2/MtSarulente/iEmpressJoined", "N")
    local sSanyaForm      = VM_GetVar("Root/Variables/Global/Sanya/sForm", "S")
    local bHasWingBadgeOn = fnIsItemEquipped("Wing Badge")
    
    -- |[Flag]|
    VM_SetVar("Root/Variables/Chapter2/HarpyBlockade/iBlockadeResolved", "N", 1.0)
    
    -- |[Setup]|
    fnCutsceneMergeParty()
    fnCutsceneBlocker()
    fnCutsceneMove("Sanya", 29.25, 32.50)
    fnCutsceneFace("Sanya", -1, 0)
    fnCutsceneMove("Izuna", 30.25, 32.50)
    fnCutsceneFace("Izuna", -1, 0)
    fnCutsceneMove("Zeke",  30.25, 31.50)
    fnCutsceneFace("Zeke",  -1, 0)
    if(iEmpressJoined == 1.0) then
        fnCutsceneMove("Empress", 30.25, 33.50)
        fnCutsceneFace("Empress", -1, 0)
    end
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Turn off entity collisions.
    TA_ChangeCollisionFlag("RecruitA", false)
    TA_ChangeCollisionFlag("RecruitB", false)
    TA_ChangeCollisionFlag("RecruitC", false)
    TA_ChangeCollisionFlag("Officer", false)
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HarpyRecruit] Flank attack![P] Enemies coming from the east!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneMove("RecruitA", 17.25, 37.50, 2.50)
    fnCutsceneMove("RecruitA", 23.25, 37.50, 2.50)
    fnCutsceneMove("RecruitA", 23.25, 34.50, 2.50)
    fnCutsceneMove("RecruitA", 26.25, 34.50, 2.50)
    fnCutsceneMove("RecruitA", 26.25, 31.50, 2.50)
    fnCutsceneFace("RecruitA", 1, 0)
    fnCutsceneMove("RecruitB", 26.25, 32.50, 2.50)
    fnCutsceneFace("RecruitB", 1, 0)
    fnCutsceneMove("RecruitC", 26.25, 37.50, 2.50)
    fnCutsceneMove("RecruitC", 23.25, 37.50, 2.50)
    fnCutsceneMove("RecruitC", 23.25, 33.50, 2.50)
    fnCutsceneMove("RecruitC", 26.25, 33.50, 2.50)
    fnCutsceneFace("RecruitC", 1, 0)
    fnCutsceneMove("Officer",  22.25, 33.50, 2.50)
    fnCutsceneMove("Officer",  25.25, 33.50, 2.50)
    fnCutsceneFace("Officer",  1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    
    -- |[Didn't See The Harpies At The Start]|
    if(iSawBlockade == 0.0) then
        
        -- |[Harpy Form]|
        if(sSanyaForm == "Harpy") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "HarpyRecruit", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "HarpyOfficer", "Neutral") ]])
            fnCutscene([[ Append("Recruit:[E|Neutral] Wait a minute -[P] that's one of ours![B][C]") ]])
            fnCutscene([[ Append("Officer:[E|Neutral] Sheesh, you had us scared![P] What are you doing way out here?[P] Are you lost?[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] No.[P] I'm on a special assignment to save the glacier from a big super monster.[B][C]") ]])
            fnCutscene([[ Append("Recruit:[E|Neutral] Oh![P] Sanya![P] I heard about you![B][C]") ]])
            fnCutscene([[ Append("Recruit:[E|Neutral] She's really strong, and has a goat with her who's super smart![B][C]") ]])
            fnCutscene([[ Append("Officer:[E|Neutral] Okay, okay.[P] Return to your positions, everyone.[P] Real bandits could try to slip through.[P] And get the gate open for her.") ]])
            
        -- |[Wing Badge]|
        elseif(bHasWingBadgeOn == true) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "HarpyRecruit", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "HarpyOfficer", "Neutral") ]])
            fnCutscene([[ Append("Recruit:[E|Neutral] Wait a minute -[P] that's one of ours![P] She's got the flock leader's badge on![B][C]") ]])
            fnCutscene([[ Append("Officer:[E|Neutral] Okay, okay.[P] Return to your positions, everyone.[P] Real bandits could try to slip through.[P] And get the gate open for her.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] Sorry, did I scare you?[B][C]") ]])
            fnCutscene([[ Append("Recruit:[E|Neutral] Popping out of the bushes like that, phew![P] I guess this gate can be flanked.[B][C]") ]])
            fnCutscene([[ Append("Officer:[E|Neutral] From the north, yes.[P] Keep your eyes open, everyone.") ]])
            
        -- |[Default]|
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "HarpyRecruit", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "HarpyOfficer", "Neutral") ]])
            fnCutscene([[ Append("Recruit:[E|Neutral] Who are you?[P] What are you doing?[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] My name's Sanya and we're going through the pass.[B][C]") ]])
            fnCutscene([[ Append("Officer:[E|Neutral] Why are you armed?[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Smirk] Because my friends think that the monsters and stuff that live around here are too tough for a beat down.[B][C]") ]])
            fnCutscene([[ Append("Officer:[E|Neutral] You're not a bandit?[P] That kitsune isn't a hostage?[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Laugh] She's my girlfriend![B][C]") ]])
            fnCutscene([[ Append("Recruit:[E|Neutral] Wow.[P] Really?[B][C]") ]])
            fnCutscene([[ Append("Recruit:[E|Neutral] She's a little out of your league isn't she?[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Offended] Don't make me kick your tailfeathers, beakface![B][C]") ]])
            fnCutscene([[ Append("Officer:[E|Neutral] Calm down everyone.[B][C]") ]])
            fnCutscene([[ Append("Officer:[E|Neutral] We're not here to impede ordinary traffic, just bandits and smugglers.[P] Please go right through.[B][C]") ]])
            fnCutscene([[ Append("Recruit:[E|Neutral] What's she got that I don't?[B][C]") ]])
            fnCutscene([[ Append("Officer:[E|Neutral] Enough![B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Sad] (I didn't even need to beat anyone up.[P] Maybe diplomacy really is the way forward?)[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Sad] (Am I a master diplomat now?[P] But will I go mad with power...)") ]])
        end
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    
    -- |[Did See The Harpies]|
    else
    
        -- |[Common]|
        fnCutsceneMove("Sanya", 27.25, 32.50, 0.50)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    
        -- |[Harpy Form]|
        if(sSanyaForm == "Harpy") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "HarpyRecruit", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "HarpyOfficer", "Neutral") ]])
            fnCutscene([[ Append("Recruit:[E|Neutral] Hey![P] It's that psycho I was telling you about![B][C]") ]])
            fnCutscene([[ Append("Officer:[E|Neutral] The one who jumped right from refusal to death threats?[B][C]") ]])
            fnCutscene([[ Append("Officer:[E|Neutral] ... Was the psycho a harpy?[B][C]") ]])
            fnCutscene([[ Append("Recruit:[E|Neutral] No but - I don't understand this! What happened to you?[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Sad] I think things would have gone a lot smoother if you had known I was a harpy. I can transform and stuff.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Sad] But I'm still mega-sorry, comrade. I've got a short temper.[B][C]") ]])
            fnCutscene([[ Append("Recruit:[E|Neutral] I'll say! Save it for the bandits![B][C]") ]])
            fnCutscene([[ Append("Officer:[E|Neutral] False alarm, everyone. Return to your positions. And get that gate open for her... squad?[B][C]") ]])
            fnCutscene([[ Append("Recruit:[E|Neutral] Do you have a battle-goat?[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Happy] Hell yes! That's Zeke and he's hardcore![B][C]") ]])
            fnCutscene([[ Append("Recruit:[E|Neutral] Cool! I love goats![B][C]") ]])
            fnCutscene([[ Append("Recruit:[E|Neutral] All right, I guess it was a misunderstanding. I'll get the gate for you.") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()

        -- |[Wing Badge]|
        elseif(bHasWingBadgeOn == true) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "HarpyRecruit", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "HarpyOfficer", "Neutral") ]])
            fnCutscene([[ Append("Recruit:[E|Neutral] Hey![P] It's that psycho I was telling you about![B][C]") ]])
            fnCutscene([[ Append("Officer:[E|Neutral] The one who jumped right from refusal to death threats?[B][C]") ]])
            fnCutscene([[ Append("Officer:[E|Neutral] ... Did she have the flock leader's wing badge on at the time?[B][C]") ]])
            fnCutscene([[ Append("Recruit:[E|Neutral] Uh oh! Did I not see it?[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Sad] No, I just didn't have it on. I'm still mega-sorry about all the screaming. Mondo sorry. Huge mega massive sorry.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Sad] I've got a bit of a short temper.[B][C]") ]])
            fnCutscene([[ Append("Recruit:[E|Neutral] I'll say! But I guess you apologized so it's okay.[B][C]") ]])
            fnCutscene([[ Append("Officer:[E|Neutral] False alarm, everyone. Return to your positions.") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
        
        -- |[Default]|
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "HarpyRecruit", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "HarpyOfficer", "Neutral") ]])
            fnCutscene([[ Append("Recruit:[E|Neutral] Hey![P] It's that psycho I was telling you about![B][C]") ]])
            fnCutscene([[ Append("Officer:[E|Neutral] The one who jumped right from refusal to death threats?[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Sad] Yeah that's me.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Sad] (Oh man this is so hard...[P] I suck so much at this...)[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Sad] I...[P] I...[B][C]") ]])
            fnCutscene([[ Append("Officer:[E|Neutral] Form a line, like we trained![P] Here she comes![B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Sad] ...[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Sad] (I am screwing this up.[P] Why is apologizing so hard?)[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] (Wait a minute, what if I'm approaching this the wrong way?)[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Offended] (My best trait is my barely constrained anger![P] Channel that, Sanya!)[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Offended] You lovely flockblockers![B][C]") ]])
            fnCutscene([[ Append("Recruit:[E|Neutral] What.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Offended] You birds are super cool and I wanted to apologize![B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Offended] I shouldn't have flown off the handle like that.[P] I feel terrible for saying those mean things.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Angry] CAN YOU EVER FUCKING FORGIVE ME?[B][C]") ]])
            fnCutscene([[ Append("Recruit:[E|Neutral] ...[B][C]") ]])
            fnCutscene([[ Append("Officer:[E|Neutral] ...[B][C]") ]])
            fnCutscene([[ Append("Recruit:[E|Neutral] Uh, yeah, sure.[B][C]") ]])
            fnCutscene([[ Append("Officer:[E|Neutral] What did you say to her to make her so angry?[B][C]") ]])
            fnCutscene([[ Append("Recruit:[E|Neutral] I told her we can't let her through the gate.[B][C]") ]])
            fnCutscene([[ Append("Officer:[E|Neutral] That's still true.[P] Orders are orders.[B][C]") ]])
            fnCutscene([[ Append("Officer:[E|Neutral] But, I think you made a mistake, recruit.[B][C]") ]])
            fnCutscene([[ Append("Recruit:[E|Neutral] I did?[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Offended] I'm the one who made the mistake![P] Are you blind?[B][C]") ]])
            fnCutscene([[ Append("Officer:[E|Neutral] No, the mistake is that we're here to keep smugglers and bandits from crossing Trafal.[B][C]") ]])
            fnCutscene([[ Append("Officer:[E|Neutral] I see in your entourage a kitsune.[P] Is that correct?[B][C]") ]])
            fnCutscene([[ Append("Recruit:[E|Neutral] ...[P] Oh crap![B][C]") ]])
            fnCutscene([[ Append("Officer:[E|Neutral] The kitsunes are allowed to travel as they see fit.[P] My apologies.[B][C]") ]])
            fnCutscene([[ Append("Officer:[E|Neutral] We're new in the region and not all of us even know what a kitsune looks like.[B][C]") ]])
            fnCutscene([[ Append("Recruit:[E|Neutral] I'm sorry, please forgive me.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Angry] I FORGIVE YOU IF YOU FORGIVE ME![B][C]") ]])
            fnCutscene([[ Append("Recruit:[E|Neutral] Okay![B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] Now let's be friends.[B][C]") ]])
            fnCutscene([[ Append("Recruit:[E|Neutral] Let's not go nuts.[P] I'll just open the gate for you.[B][C]") ]])
            fnCutscene([[ Append("Officer:[E|Neutral] Very sorry about that, madam.[P] There's so many weapon smugglers around here, it's a serious security concern.[B][C]") ]])
            fnCutscene([[ Append("Officer:[E|Neutral] Let us know if you see any suspicious activity in the area.[P] Have a nice day.") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
        end
    end
    
    -- |[Movement]|
    fnCutsceneMove("RecruitA", 26.25, 34.50)
    fnCutsceneMove("RecruitA", 23.25, 34.50)
    fnCutsceneMove("RecruitA", 23.25, 37.50)
    fnCutsceneMove("RecruitA", 16.25, 37.50)
    fnCutsceneMove("RecruitA", 16.25, 35.50)
    fnCutsceneFace("RecruitA", 1, 0)
    fnCutsceneMove("RecruitB", 24.25, 32.50)
    fnCutsceneFace("RecruitB", 1, 0)
    fnCutsceneMove("RecruitC", 23.25, 33.50)
    fnCutsceneMove("RecruitC", 23.25, 37.50)
    fnCutsceneMove("RecruitC", 26.25, 37.50)
    fnCutsceneFace("RecruitC", 1, 0)
    fnCutsceneMove("Officer", 25.25, 34.50)
    fnCutsceneMove("Officer", 22.25, 34.50)
    fnCutsceneMove("Officer", 22.25, 32.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Gate.
    fnCutscenePlaySound("World|FlipSwitch")
    AL_SetProperty("Set Collision", 17, 34, 0, 0)
    AL_SetProperty("Set Collision", 18, 34, 0, 0)
    fnCutsceneLayerDisabled("BarsHi", true)
    fnCutsceneLayerDisabled("BarsLo", true)
    
    --Sanya.
    fnCutsceneMove("Sanya", 29.25, 32.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Didn't See The Harpies At The Start]|
    if(iSawBlockade == 0.0) then
        
        -- |[Harpy Form]|
        if(sSanyaForm == "Harpy") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
            fnCutscene([[ Append("Sanya:[E|Neutral] Man, we scared them good.[P] Guess being out here all alone does makes you jumpy.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Smirk] I'd be nervous if I was this close to Westwoods, too.[P] I wonder if the monsters make their way up here.[P] Anyway, let's go!") ]])
            
        -- |[Wing Badge]|
        elseif(bHasWingBadgeOn == true) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
            fnCutscene([[ Append("Sanya:[E|Neutral] Misunderstanding all cleared up.[P] Phew.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Smirk] I'd be nervous if I was this close to Westwoods, too.[P] I wonder if the monsters make their way up here.[P] Anyway, let's go!") ]])
        
        -- |[Default]|
        else
            --Empress not present.
            if(iEmpressJoined == 0.0) then
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
                fnCutscene([[ Append("Sanya:[E|Neutral] I didn't even get into a fistfight with the cops.[P] Am I getting soft?[B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Cry] Soft?[B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Smirk] Just because they're wearing uniforms doesn't mean you need to beat them up.[P] Turns out they're looking out for rogues and rapscallions.[B][C]") ]])
                fnCutscene([[ Append("Zeke:[E|Neutral] Bah![B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Sad] Was it wrong all those other times I threw cops into dumpsters?[B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Ugh] Maybe?[B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Neutral] In any case, they opened the gate for us.[P] I'll let the other kitsunes know that they built one here.[B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Neutral] If they're serious about stopping bandits, we should be working together!") ]])
        
            --Empress is present.
            else
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
                fnCutscene([[ Append("Sanya:[E|Neutral] I didn't even get into a fistfight with the cops.[P] Am I getting soft?[B][C]") ]])
                fnCutscene([[ Append("Empress:[E|Neutral] Restraint requires as much strength as violence does.[B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Explain] Yeah![P] That's what the elder says![B][C]") ]])
                fnCutscene([[ Append("Empress:[E|Neutral] Oh really.[P] Interesting.[B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Neutral] I guess those other harpy patrols must think we're bandits or something.[P] Oh well.[B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Offended] I'm not going to feel bad for pantsing them because they struck first.[B][C]") ]])
                fnCutscene([[ Append("Zeke:[E|Neutral] Bah![B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Neutral] In any case, they opened the gate for us.[P] I'll let the other kitsunes know that they built one here.[B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Neutral] If they're serious about stopping bandits, we should be working together!") ]])
        
            end
        end
    -- |[Saw the Harpies]|
    else
        -- |[Harpy Form]|
        if(sSanyaForm == "Harpy") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
            fnCutscene([[ Append("Sanya:[E|Neutral] Making friends with harpies is so easy, now.[P] We're on the same wavelength.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Smirk] That's great![P] It's always lovely to make new friends![P] Let's go!") ]])
            
        -- |[Wing Badge]|
        elseif(bHasWingBadgeOn == true) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
            fnCutscene([[ Append("Sanya:[E|Neutral] Apologizing is really cool.[P] I should do it more often.[P] I get to yell and still be the good guy.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Smirk] I don't know if that's the right lesson but I'm glad you didn't find it hard to do.[P] Let's get going.") ]])
        
        -- |[Default]|
        else
        
            --Empress not present.
            if(iEmpressJoined == 0.0) then
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
                fnCutscene([[ Append("Sanya:[E|Sad] Izuna, I'm overflowing with a brand new power...[P] I fear I might be unstoppable now.[B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Cry] Ha ha ha what?[B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Happy] With my incredible diplomatic skills, all my enemies will become my friends![B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Happy] The entire world will be united beneath my umbrella of friendship![P] Nobody can resist me screaming at them for forgiveness![B][C]") ]])
                fnCutscene([[ Append("Zeke:[E|Neutral] ...[B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Blush] ...[B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Blush] Okay then?[B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Happy] But I'm super duper proud of you, sweetie![P] You did so well![P] Great work![B][C]") ]])
                fnCutscene([[ Append("Zeke:[E|Neutral] Bah![B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Neutral] Thanks.[P] So, gate's open.[P] We can head through the pass now.[B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Explain] Terrific![P] I'll be sure to let the other kitsunes know the harpies made a gate here![B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Explain] If they're working to stop the bandits and smugglers, we should team up with them!") ]])
                fnCutsceneBlocker()

            --Empress is present.
            else
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
                fnCutscene([[ Append("Sanya:[E|Sad] Izuna, I'm overflowing with a brand new power...[P] I fear I might be unstoppable now.[B][C]") ]])
                fnCutscene([[ Append("Empress:[E|Neutral] The power of diplomacy.[P] It is mightier than a ten-thousand-strong army.[B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Blush] Ha ha what are you talking about?[B][C]") ]])
                fnCutscene([[ Append("Empress:[E|Neutral] I've negotiated many treaties and deals, children.[P] We all had to start somewhere.[B][C]") ]])
                fnCutscene([[ Append("Empress:[E|Neutral] You have learned to unlock your passion and redirect it.[B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Neutral] It's pretty sweet.[P] Rough, but sweet.[B][C]") ]])
                fnCutscene([[ Append("Empress:[E|Neutral] Rough indeed, but all gems are rough before they are polished.[B][C]") ]])
                fnCutscene([[ Append("Zeke:[E|Neutral] Nyeh![P] Nyeh![B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Neutral] Thanks, buddy.[B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Happy] I'm super duper proud of you, sweetie![P] You did so well! Great work![B][C]") ]])
                fnCutscene([[ Append("Zeke:[E|Neutral] Bah![B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Neutral] Thanks.[P] So, gate's open.[P] We can head through the pass now.[B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Explain] Terrific![P] I'll be sure to let the other kitsunes know the harpies made a gate here![B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Explain] If they're working to stop the bandits and smugglers, we should team up with them!") ]])
                fnCutsceneBlocker()
            end
        end
    end
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Collisions]|
    fnCutscene([[ TA_ChangeCollisionFlag("RecruitA", true) ]])
    fnCutscene([[ TA_ChangeCollisionFlag("RecruitB", true) ]])
    fnCutscene([[ TA_ChangeCollisionFlag("RecruitC", true) ]])
    fnCutscene([[ TA_ChangeCollisionFlag("Officer", true) ]])
    
    -- |[Finish Up]|
    fnAutoFoldParty()
    fnCutsceneBlocker()

-- |[ ============================ Approached Blockade From Behind ============================= ]|
elseif(sObjectName == "RightBehindYou") then
    
    -- |[ ================ Common Code ================= ]|
    -- |[Repeat Check]|
    local iBlockadeResolved = VM_GetVar("Root/Variables/Chapter2/HarpyBlockade/iBlockadeResolved", "N")
    local iSawBlockade      = VM_GetVar("Root/Variables/Chapter2/HarpyBlockade/iSawBlockade", "N")
    if(iBlockadeResolved == 1.0) then return end
    
    -- |[Variables]|
    local iEmpressJoined = VM_GetVar("Root/Variables/Chapter2/MtSarulente/iEmpressJoined", "N")
    
    -- |[Flag]|
    VM_SetVar("Root/Variables/Chapter2/HarpyBlockade/iBlockadeResolved", "N", 1.0)

    -- |[Setup]|
    fnCutsceneMergeParty()
    fnCutsceneBlocker()
    fnCutsceneMove("Sanya", 17.25, 38.50)
    fnCutsceneFace("Sanya", 0, -1)
    fnCutsceneMove("Izuna", 16.25, 39.50)
    fnCutsceneFace("Izuna", 0, -1)
    fnCutsceneMove("Zeke",  18.25, 39.50)
    fnCutsceneFace("Zeke",  0, -1)
    if(iEmpressJoined == 1.0) then
        fnCutsceneMove("Empress", 17.25, 39.50)
        fnCutsceneFace("Empress", 0, -1)
    end
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[ ======= Didn't See The Harpies Earlier ======= ]|
    if(iSawBlockade == 0.0) then
        
        -- |[Movement]|
        fnCutsceneMove("RecruitA", 17.25, 36.50)
    
        -- |[Dialogue]|
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "HarpyRecruit", "Neutral") ]])
        fnCutscene([[ Append("Recruit:[E|Neutral] (Okay, stick to the script.)[B][C]") ]])
        fnCutscene([[ Append("Recruit:[E|Neutral] Hello, travellers.[P] Anything to declare?[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] No?[P] Why did you build a gate here?[B][C]") ]])
        fnCutscene([[ Append("Recruit:[E|Neutral] Smuggler and bandit activity has been increasing, we're trying to clamp down on it.[B][C]") ]])
        fnCutscene([[ Append("Recruit:[E|Neutral] As you are travelling with a kitsune, you are granted access.[P] Please let us know if you encounter any suspicious persons.[B][C]") ]])
        fnCutscene([[ Append("Recruit:[E|Neutral] Have a nice day!") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    
        -- |[Movement]|
        fnCutsceneMove("RecruitA", 17.25, 35.50)
        fnCutsceneMove("RecruitA", 16.25, 35.50)
        fnCutsceneFace("RecruitA", 1, 0)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Gate.
        fnCutscenePlaySound("World|FlipSwitch")
        AL_SetProperty("Set Collision", 17, 34, 0, 0)
        AL_SetProperty("Set Collision", 18, 34, 0, 0)
        fnCutsceneLayerDisabled("BarsHi", true)
        fnCutsceneLayerDisabled("BarsLo", true)
        
        --Sanya.
        fnCutsceneFace("Sanya", 0, 1)
    
        -- |[Dialogue]|
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Izuna", "Neutral") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] Well that was painless.[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Neutral] I'll make sure to tell the other kitsunes that the harpies built a gate here.[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Explain] If they're serious about stopping bandits, we should work together!") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    
    -- |[ =========== Saw The Harpies Earlier ========== ]|
    else
        --Turn off entity collisions.
        TA_ChangeCollisionFlag("RecruitA", false)
        TA_ChangeCollisionFlag("RecruitB", false)
        TA_ChangeCollisionFlag("RecruitC", false)
        TA_ChangeCollisionFlag("Officer", false)
        
        --Variables.
        local sSanyaForm = VM_GetVar("Root/Variables/Global/Sanya/sForm", "S")
    
        -- |[Dialogue]|
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Harpy:[VOICE|HarpyRecruit] Flank attack![P] Enemies coming from the south!") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        -- |[Movement]|
        fnCutsceneMove("RecruitA", 17.25, 37.50, 2.50)
        fnCutsceneFace("RecruitA", 0, 1)
        fnCutsceneMove("RecruitB", 24.25, 34.50, 2.50)
        fnCutsceneMove("RecruitB", 23.25, 34.50, 2.50)
        fnCutsceneMove("RecruitB", 23.25, 37.50, 2.50)
        fnCutsceneMove("RecruitB", 16.25, 37.50, 2.50)
        fnCutsceneFace("RecruitB", 0, 1)
        fnCutsceneMove("RecruitC", 26.25, 37.50, 2.50)
        fnCutsceneMove("RecruitC", 18.25, 37.50, 2.50)
        fnCutsceneFace("RecruitC", 0, 1)
        fnCutsceneMove("Officer",  22.25, 34.50, 2.50)
        fnCutsceneMove("Officer",  23.25, 34.50, 2.50)
        fnCutsceneMove("Officer", 23.25, 37.50, 2.50)
        fnCutsceneMove("Officer", 17.25, 37.50, 2.50)
        fnCutsceneMove("Officer", 17.25, 36.50, 2.50)
        fnCutsceneFace("Officer",  0, 1)
        fnCutsceneBlocker()
        fnCutsceneWait(125)
        fnCutsceneBlocker()
        fnCutsceneMove("Sanya", 17.25, 38.50, 0.50)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        -- |[Dialogue]|
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "HarpyRecruit", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "HarpyOfficer", "Neutral") ]])
        fnCutscene([[ Append("Recruit:[E|Neutral] Hey![P] It's that psycho I was telling you about![B][C]") ]])
        
        if(sSanyaForm ~= "Harpy") then
            fnCutscene([[ Append("Officer:[E|Neutral] The one who jumped right from refusal to death threats?[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Sad] Yeah that's me.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Sad] (Oh man this is so hard...[P] I suck so much at this...)[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Sad] I...[P] I...[B][C]") ]])
            fnCutscene([[ Append("Officer:[E|Neutral] Form a line, like we trained![P] Here she comes![B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Sad] ...[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Sad] (I am screwing this up.[P] Why is apologizing so hard?)[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] (Wait a minute, what if I'm approaching this the wrong way?)[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Offended] (My best trait is my barely constrained anger![P] Channel that, Sanya!)[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Offended] You lovely flockblockers![B][C]") ]])
            fnCutscene([[ Append("Recruit:[E|Neutral] What.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Offended] You birds are super cool and I wanted to apologize![B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Offended] I shouldn't have flown off the handle like that.[P] I feel terrible for saying those mean things.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Angry] CAN YOU EVER FUCKING FORGIVE ME?[B][C]") ]])
            fnCutscene([[ Append("Recruit:[E|Neutral] ...[B][C]") ]])
            fnCutscene([[ Append("Officer:[E|Neutral] ...[B][C]") ]])
            fnCutscene([[ Append("Recruit:[E|Neutral] Uh, yeah, sure.[B][C]") ]])
            fnCutscene([[ Append("Officer:[E|Neutral] What did you say to her to make her so angry?[B][C]") ]])
            fnCutscene([[ Append("Recruit:[E|Neutral] I told her we can't let her through the gate.[B][C]") ]])
            fnCutscene([[ Append("Officer:[E|Neutral] That's still true.[P] Orders are orders.[B][C]") ]])
            fnCutscene([[ Append("Officer:[E|Neutral] But, I think you made a mistake, recruit.[B][C]") ]])
            fnCutscene([[ Append("Recruit:[E|Neutral] I did?[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Offended] I'm the one who made the mistake![P] Are you blind?[B][C]") ]])
            fnCutscene([[ Append("Officer:[E|Neutral] No, the mistake is that we're here to keep smugglers and bandits from crossing Trafal.[B][C]") ]])
            fnCutscene([[ Append("Officer:[E|Neutral] I see in your entourage a kitsune.[P] Is that correct?[B][C]") ]])
            fnCutscene([[ Append("Recruit:[E|Neutral] ...[P] Oh crap![B][C]") ]])
            fnCutscene([[ Append("Officer:[E|Neutral] The kitsunes are allowed to travel as they see fit.[P] My apologies.[B][C]") ]])
            fnCutscene([[ Append("Officer:[E|Neutral] We're new in the region and not all of us even know what a kitsune looks like.[B][C]") ]])
            fnCutscene([[ Append("Recruit:[E|Neutral] I'm sorry, please forgive me.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Angry] I FORGIVE YOU IF YOU FORGIVE ME![B][C]") ]])
            fnCutscene([[ Append("Recruit:[E|Neutral] Okay![B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] Now let's be friends.[B][C]") ]])
            fnCutscene([[ Append("Recruit:[E|Neutral] Let's not go nuts.[P] I'll just open the gate for you.[B][C]") ]])
            fnCutscene([[ Append("Officer:[E|Neutral] Very sorry about that, madam.[P] There's so many weapon smugglers around here, it's a serious security concern.[B][C]") ]])
            fnCutscene([[ Append("Officer:[E|Neutral] Let us know if you see any suspicious activity in the area.[P] Have a nice day.") ]])
        else
            fnCutscene([[ Append("Officer:[E|Neutral] ...[P] Was the psycho a harpy the whole time?[B][C]") ]])
            fnCutscene([[ Append("Recruit:[E|Neutral] I recognize that hair and that gun.[P] I know it's you.[B][C]") ]])
            fnCutscene([[ Append("Officer:[E|Neutral] Are you sure?[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Sad] It's me.[P] I'm not going to hide it.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Sad] (Oh man this is so hard...[P] I suck so much at this...)[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Sad] I'm sorry for shouting at you earlier.[P] I lost my temper real bad.[P] I...[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Sad] (I am screwing this up.[P] Why is apologizing so hard?)[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] (Wait a minute, what if I'm approaching this the wrong way?)[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Offended] (My best trait is my barely constrained anger![P] Channel that, Sanya!)[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Offended] Wonderful sisters in arms![P] I've never been so sorry in my entire life and I'll stab anyone who is sorrier than me![B][C]") ]])
            fnCutscene([[ Append("Officer:[E|Neutral] Are we just not screening recruits anymore?[B][C]") ]])
            fnCutscene([[ Append("Recruit:[E|Neutral] Look, if I accept your apology will you stop shouting?[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Offended] FUCK YES I WILL![B][C]") ]])
            fnCutscene([[ Append("Recruit:[E|Neutral] Okay![P] Apology accepted![B][C]") ]])
            fnCutscene([[ Append("Officer:[E|Neutral] False alarm, everyone.[P] Back to your posts.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] Can we be friends now, comrade?[B][C]") ]])
            fnCutscene([[ Append("Recruit:[E|Neutral] As long as you bring that fire with you to battle, sure.[P] Just don't point it at a fellow harpy.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] You got it, pal.") ]])
        end
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        -- |[Movement]|
        fnCutsceneMove("RecruitA", 17.25, 35.50)
        fnCutsceneMove("RecruitA", 16.25, 35.50)
        fnCutsceneFace("RecruitA", 1, 0)
        fnCutsceneMove("RecruitB", 19.25, 37.50)
        fnCutsceneMove("RecruitB", 19.25, 35.50)
        fnCutsceneFace("RecruitB", 0, 1)
        fnCutsceneMove("RecruitC", 26.25, 37.50)
        fnCutsceneFace("RecruitC", 1, 0)
        fnCutsceneMove("Officer",  17.25, 37.50)
        fnCutsceneMove("Officer",  23.25, 37.50)
        fnCutsceneMove("Officer",  23.25, 34.50)
        fnCutsceneMove("Officer",  22.25, 34.50)
        fnCutsceneMove("Officer",  22.25, 32.50)
        fnCutsceneFace("Officer",  0, -1)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    
        --Gate.
        fnCutscenePlaySound("World|FlipSwitch")
        AL_SetProperty("Set Collision", 17, 34, 0, 0)
        AL_SetProperty("Set Collision", 18, 34, 0, 0)
        fnCutsceneLayerDisabled("BarsHi", true)
        fnCutsceneLayerDisabled("BarsLo", true)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneFace("Sanya",  0, 1)
        
        -- |[Dialogue]|
        --Empress not present.
        if(iEmpressJoined == 0.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
            fnCutscene([[ Append("Sanya:[E|Sad] Izuna, I'm overflowing with a brand new power...[P] I fear I might be unstoppable now.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Cry] Ha ha ha what?[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Happy] With my incredible diplomatic skills, all my enemies will become my friends![B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Happy] The entire world will be united beneath my umbrella of friendship![P] Nobody can resist me screaming at them for forgiveness![B][C]") ]])
            fnCutscene([[ Append("Zeke:[E|Neutral] ...[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Blush] ...[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Blush] Okay then?[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Happy] But I'm super duper proud of you, sweetie![P] You did so well![P] Great work![B][C]") ]])
            fnCutscene([[ Append("Zeke:[E|Neutral] Bah![B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] Thanks.[P] So, gate's open.[P] We can head through the pass now.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Explain] Terrific![P] I'll be sure to let the other kitsunes know the harpies made a gate here![B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Explain] If they're working to stop the bandits and smugglers, we should team up with them!") ]])
            fnCutsceneBlocker()

        --Empress is present.
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
            fnCutscene([[ Append("Sanya:[E|Sad] Izuna, I'm overflowing with a brand new power...[P] I fear I might be unstoppable now.[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] The power of diplomacy.[P] It is mightier than a ten-thousand-strong army.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Blush] Ha ha what are you talking about?[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] I've negotiated many treaties and deals, children.[P] We all had to start somewhere.[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] You have learned to unlock your passion and redirect it.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] It's pretty sweet.[P] Rough, but sweet.[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] Rough indeed, but all gems are rough before they are polished.[B][C]") ]])
            fnCutscene([[ Append("Zeke:[E|Neutral] Nyeh![P] Nyeh![B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] Thanks, buddy.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Happy] I'm super duper proud of you, sweetie![P] You did so well! Great work![B][C]") ]])
            fnCutscene([[ Append("Zeke:[E|Neutral] Bah![B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] Thanks.[P] So, gate's open.[P] We can head through the pass now.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Explain] Terrific![P] I'll be sure to let the other kitsunes know the harpies made a gate here![B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Explain] If they're working to stop the bandits and smugglers, we should team up with them!") ]])
            fnCutsceneBlocker()
        end
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    end
    
    -- |[Collisions]|
    fnCutscene([[ TA_ChangeCollisionFlag("RecruitA", true) ]])
    fnCutscene([[ TA_ChangeCollisionFlag("RecruitB", true) ]])
    fnCutscene([[ TA_ChangeCollisionFlag("RecruitC", true) ]])
    fnCutscene([[ TA_ChangeCollisionFlag("Officer", true) ]])
    
    -- |[Finish Up]|
    fnAutoFoldParty()
    fnCutsceneBlocker()

-- |[ ================================== Shot Depth Triggers =================================== ]|
elseif(sObjectName == "ShotDepthD1") then
    AL_SetProperty("Shooting Depth", 1)
elseif(sObjectName == "ShotDepthD2") then
    AL_SetProperty("Shooting Depth", 2)
elseif(sObjectName == "ShotDepthD3") then
    AL_SetProperty("Shooting Depth", 3)

end
