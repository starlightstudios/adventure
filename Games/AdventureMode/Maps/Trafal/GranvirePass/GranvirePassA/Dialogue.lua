-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[Name Resolve]|
    --Resolve the name of the calling entity.
    local sActorName = TA_GetProperty("Name")

    --Set facing.
    TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[RecruitA]|
    if(sActorName == "RecruitA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Harpy:[VOICE|HarpyRecruit] Hey if you want to be friends, meet me for drinks in Poterup some time.") ]])
    
    -- |[RecruitB]|
    elseif(sActorName == "RecruitB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Harpy:[VOICE|HarpyRecruit] I trained hard to keep Trafal safe![P] Let me at those bandits!") ]])
    
    -- |[RecruitC]|
    elseif(sActorName == "RecruitC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Harpy:[VOICE|HarpyRecruit] The water's freezing...[P] but I want to take a swim so bad...") ]])
    
    -- |[Officer]|
    elseif(sActorName == "Officer") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Officer:[VOICE|HarpyOfficer] The bandits don't attack since we built the gate, but I have to assume they found another way around...") ]])
    end
end
