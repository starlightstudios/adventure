-- |[ ============================= Pamphlet: The Bunny Smugglers ============================== ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[RecruitA]|
if(sActorName == "RecruitA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HarpyRecruit] Bunnies are -[P] hey, I'm in this story![P] Wow, I'm famous!") ]])

-- |[RecruitB]|
elseif(sActorName == "RecruitB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HarpyRecruit] So that explains the change in orders all of a sudden.[P] Uh, do I have to avoid searching their -[P] you know what, nevermind.") ]])

-- |[RecruitC]|
elseif(sActorName == "RecruitC") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HarpyRecruit] Those bunnies are dudes![P] That's pretty cool.[P] Do you think there could be male harpies?") ]])

-- |[Officer]|
elseif(sActorName == "Officer") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Officer:[VOICE|HarpyOfficer] We haven't had any problems with the bunnies since the new orders came down.[P] Now I know why.") ]])
end

-- |[Clean Up]|
DL_PopActiveObject()
