-- |[ =============================== Pamphlet: Green Westwoods =============================== ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[RecruitA]|
if(sActorName == "RecruitA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HarpyRecruit] Well I'll be.[P] Shame I'm stuck here, I'd love to go help protect those alraunes.") ]])

-- |[RecruitB]|
elseif(sActorName == "RecruitB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HarpyRecruit] I don't really know a lot about Trafal.[P] Is Westwoods the place that sucks down that cliff?") ]])

-- |[RecruitC]|
elseif(sActorName == "RecruitC") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HarpyRecruit] I'd actually like to go visit Westwoods.[P] Probably warmer than it is here.") ]])

-- |[Officer]|
elseif(sActorName == "Officer") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Officer:[VOICE|HarpyOfficer] Good for those alraunes.[P] Anything else?") ]])
end

-- |[Clean Up]|
DL_PopActiveObject()
