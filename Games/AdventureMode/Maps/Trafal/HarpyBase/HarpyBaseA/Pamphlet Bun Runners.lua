-- |[ ============================= Pamphlet: The Bunny Smugglers ============================== ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ==== Minor Characters ==== ]|
-- |[HumanA]|
if(sActorName == "HumanA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Trader:[VOICE|HumanF1] Bunnies? Yeah I know all about them. Except the harem part. You think she'd share?") ]])

-- |[HumanB]|
elseif(sActorName == "HumanB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Izuna:[VOICE|Izuna] (This human isn't paying attention.)") ]])

-- |[HumanC]|
elseif(sActorName == "HumanC") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Trader:[VOICE|HumanF2] Angelface? Where do you think I get half my stock!") ]])

-- |[HumanD]|
elseif(sActorName == "HumanD") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Trader:[VOICE|HumanM0] Wow. After reading this pamphlet, you think the bunnies are... hiring?") ]])

-- |[HumanE]|
elseif(sActorName == "HumanE") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Trader:[VOICE|HumanM1] Ugh. No thanks, I prefer men. I doubt that'd go over well with the bunny boss.") ]])

-- |[HumanF]|
elseif(sActorName == "HumanF") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Trader:[VOICE|HumanF3] That bunny's name was Angelface? All I ever saw her do was bark orders at her underlings.") ]])

-- |[MercA]|
elseif(sActorName == "MercA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Mercenary:[VOICE|MercF] You gotta be tough in order to run stuff through Westwoods. The bunnies are plenty tough.") ]])

-- |[MercB]|
elseif(sActorName == "MercB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Mercenary:[VOICE|MercM] I see those bunnies all over the place. Never knew what they were doing and they kept their distance.") ]])

-- |[WerecatA]|
elseif(sActorName == "WerecatA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Trader:[VOICE|Werecat] Do bunnies eat eggs? I'm looking for new markets!") ]])

-- |[HarpyA]|
elseif(sActorName == "HarpyA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Izuna:[VOICE|Izuna] (This harpy isn't paying attention.)") ]])

-- |[KitsuneA]|
elseif(sActorName == "KitsuneA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Miko:[VOICE|KitsuneA] Your latest article? The bunnies are - oh my! I had no idea! Good work, Izuna.") ]])

-- |[KitsuneB]|
elseif(sActorName == "KitsuneB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Warrior:[VOICE|KitsuneB] Damn, she has a gang of boys wrapped around her little finger? They're still crooks, so I'm not going to trust them.") ]])

-- |[Guard]|
elseif(sActorName == "Guard") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HarpyC] Woah, the bunnies do *that*?[P] I'm going to ask the next one I see about it.") ]])

-- |[ ===== Miso Characters ==== ]|
-- |[Miso]|
elseif(sActorName == "Miso") then
    LM_ExecuteScript(gsRoot .. "Chapter 2/Dialogue/Miso/Pamphlet Bun Runners.lua")
end

-- |[Clean Up]|
DL_PopActiveObject()
