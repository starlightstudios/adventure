-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[ ========= Setup ========== ]|
    -- |[Name Resolve]|
    --Resolve the name of the calling entity.
    local sActorName = TA_GetProperty("Name")
    
    -- |[ ==== Minor Characters ==== ]|
    -- |[HumanA]|
    if(sActorName == "HumanA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Trader:[VOICE|HumanF1] Trafal is such a beautiful place.[P] Even the Cauldron can be beautiful...[P] given enough distance.") ]])

    -- |[HumanB]|
    elseif(sActorName == "HumanB") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Woman:[VOICE|HumanF0] Do I really want to spend the rest of my life as a harpy?[P] This is such a big decision...") ]])

    -- |[HumanC]|
    elseif(sActorName == "HumanC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Trader:[VOICE|HumanF2] Looking for some gems or materials?") ]])
        fnCutsceneBlocker()
        
        local sBasePath = fnResolvePath()
        local sString = "AM_SetShopProperty(\"Show\", \"Merchant\", \"" .. sBasePath .. "Merchant Setup.lua\", \"Null\")"
        fnCutscene(sString)
        fnCutsceneBlocker()

    -- |[HumanD]|
    elseif(sActorName == "HumanD") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Trader:[VOICE|HumanM0] This klutz spilled the bone barrel.") ]])

    -- |[HumanE]|
    elseif(sActorName == "HumanE") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Trader:[VOICE|HumanM1] We found some human bones in Westwoods, among all the animal bones.[P] It's all fertilizer in the end, right?") ]])
    
    -- |[HumanF]|
    elseif(sActorName == "HumanF") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Trader:[VOICE|HumanF3] The harpies won't let my goods through, but at least they didn't seize them.[P] Now I have to talk the mercs into going through Vuca or Granvire...") ]])
    
    -- |[MercA]|
    elseif(sActorName == "MercA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Mercenary:[VOICE|MercF] We already came through the Cauldron.[P] I am not looking forward to going back there.") ]])
    
    -- |[MercB]|
    elseif(sActorName == "MercB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Mercenary:[VOICE|MercM] The harpies tried to build an outpost in Westwoods and abandoned it.[P] That place protects itself, no need to post soldiers.") ]])
    
    -- |[WerecatA]|
    elseif(sActorName == "WerecatA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Trader:[VOICE|Werecat] Do you think the harpies will buy chickens and eggs?[P] I wonder if that will cause an existential crisis.") ]])
    
    -- |[HarpyA]|
    elseif(sActorName == "HarpyA") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Harpy:[VOICE|HarpyRecruit] Becoming a harpy was the best decision I ever made.[P] Even if you don't, I still want to be with you.") ]])

    -- |[KitsuneA]|
    elseif(sActorName == "KitsuneA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Miko:[VOICE|KitsuneA] We'll keep an eye on things here.[P] I'd tell you not to get involved, Izuna, but you really never just let it go.") ]])

    -- |[KitsuneB]|
    elseif(sActorName == "KitsuneB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Warrior:[VOICE|KitsuneB] So far, the harpies seem genuine.[P] But, the saying goes, 'Never turn your back on a dagger'.") ]])

    -- |[Guard]|
    elseif(sActorName == "Guard") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Harpy:[VOICE|HarpyOfficer] You're not hauling anything?[P] Nothing to declare?[P] Very well, go on in.") ]])
    
    -- |[ ===== Miso Characters ==== ]|
    -- |[Miso]|
    elseif(sActorName == "Miso") then
        TA_SetProperty("Face Character", "PlayerEntity")
        LM_ExecuteScript(gsRoot .. "Chapter 2/Dialogue/Miso/Hello.lua")
    end
end
