-- |[ ============================= Pamphlet: Bandits in Westwoods ============================= ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ==== Minor Characters ==== ]|
-- |[HumanA]|
if(sActorName == "HumanA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Trader:[VOICE|HumanF1] Bandits in Westwoods? As if the monstrous animals weren't bad enough!") ]])

-- |[HumanB]|
elseif(sActorName == "HumanB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Izuna:[VOICE|Izuna] (This human isn't paying attention.)") ]])

-- |[HumanC]|
elseif(sActorName == "HumanC") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Trader:[VOICE|HumanF2] Oh dear, bandits? Shipping is already dangerous and now this?") ]])

-- |[HumanD]|
elseif(sActorName == "HumanD") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Trader:[VOICE|HumanM0] Oh my. Maybe some of these bones are bandit bones.") ]])

-- |[HumanE]|
elseif(sActorName == "HumanE") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Trader:[VOICE|HumanM1] As if I needed more reasons not to go to the cauldron.") ]])

-- |[HumanF]|
elseif(sActorName == "HumanF") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Trader:[VOICE|HumanF3] Bandits? I think I'll stick to shipping across Trannadar.") ]])

-- |[MercA]|
elseif(sActorName == "MercA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Mercenary:[VOICE|MercF] Bandits, monsters, who cares. I'm not scared of Westwoods.") ]])

-- |[MercB]|
elseif(sActorName == "MercB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Mercenary:[VOICE|MercM] What kind of dolt is so desperate that they join an army of bandits?") ]])

-- |[WerecatA]|
elseif(sActorName == "WerecatA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Trader:[VOICE|Werecat] Bandits? Oh no. I know some werecats down south. I hope none of them got any bad ideas.") ]])

-- |[HarpyA]|
elseif(sActorName == "HarpyA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Izuna:[VOICE|Izuna] (This harpy isn't paying attention.)") ]])

-- |[KitsuneA]|
elseif(sActorName == "KitsuneA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Miko:[VOICE|KitsuneA] Now you're saying there are bandits in Westwoods? Do we have enough warriors to even stop them?") ]])

-- |[KitsuneB]|
elseif(sActorName == "KitsuneB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Warrior:[VOICE|KitsuneB] Bandits, Izuna? Well, I don't like it, but allying with the harpies might be required.") ]])

-- |[Guard]|
elseif(sActorName == "Guard") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HarpyC] Damn it, bandits already seeping into Trafal and I'm stuck on guard duty!") ]])

-- |[ ===== Miso Characters ==== ]|
-- |[Miso]|
elseif(sActorName == "Miso") then
    LM_ExecuteScript(gsRoot .. "Chapter 2/Dialogue/Miso/Pamphlet Bandits in Westwoods.lua")
end

-- |[Clean Up]|
DL_PopActiveObject()
