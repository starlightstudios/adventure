-- |[ =============================== Pamphlet: Green Westwoods =============================== ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ==== Minor Characters ==== ]|
-- |[HumanA]|
if(sActorName == "HumanA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Trader:[VOICE|HumanF1] Westwoods is already green![P] But I guess grass is better than corrosive bile, or whatever the lake is full of.") ]])

-- |[HumanB]|
elseif(sActorName == "HumanB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Izuna:[VOICE|Izuna] (This human isn't paying attention.)") ]])

-- |[HumanC]|
elseif(sActorName == "HumanC") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Trader:[VOICE|HumanF2] Oh good, maybe the wildlife will become less deadly and shipping prices will go down.") ]])

-- |[HumanD]|
elseif(sActorName == "HumanD") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Trader:[VOICE|HumanM0] Those alraunes are all right in my book.") ]])

-- |[HumanE]|
elseif(sActorName == "HumanE") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Trader:[VOICE|HumanM1] I'd love to start a farm in Westwoods someday, if the alraunes succeed.") ]])

-- |[HumanF]|
elseif(sActorName == "HumanF") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Trader:[VOICE|HumanF3] Alraunes fixing Westwoods?[P] Can't come too soon!") ]])

-- |[MercA]|
elseif(sActorName == "MercA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Mercenary:[VOICE|MercF] Hell, I'd go join up with those alraunes.[P] Are they hiring?") ]])

-- |[MercB]|
elseif(sActorName == "MercB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Mercenary:[VOICE|MercM] Alraunes in Westwoods.[P] I wish them well.") ]])

-- |[WerecatA]|
elseif(sActorName == "WerecatA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Trader:[VOICE|Werecat] Ooh, there's alraunes in Westwoods?[P] They're always fun to play with![P] But I don't want to risk taking the chickens down there.") ]])

-- |[HarpyA]|
elseif(sActorName == "HarpyA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Izuna:[VOICE|Izuna] (This harpy isn't paying attention.)") ]])

-- |[KitsuneA]|
elseif(sActorName == "KitsuneA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Miko:[VOICE|KitsuneA] The alraunes are healing the land?[P] Let's hope the elders agree to help them!") ]])

-- |[KitsuneB]|
elseif(sActorName == "KitsuneB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Warrior:[VOICE|KitsuneB] Alraunes are usually pretty wise.[P] I congratulate them for taking such a risk to heal the land.") ]])

-- |[Guard]|
elseif(sActorName == "Guard") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HarpyC] I'm sorry but I won't be believing this about a green Westwoods until I see it myself.[P] I can't believe I actually want patrol duty there now.") ]])

-- |[ ===== Miso Characters ==== ]|
-- |[Miso]|
elseif(sActorName == "Miso") then
    LM_ExecuteScript(gsRoot .. "Chapter 2/Dialogue/Miso/Pamphlet Green Westwoods.lua")
end

-- |[Clean Up]|
DL_PopActiveObject()
