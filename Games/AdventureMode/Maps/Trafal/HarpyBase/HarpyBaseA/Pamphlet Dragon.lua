-- |[ ==================================== Pamphlet: Dragon ==================================== ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ==== Minor Characters ==== ]|
-- |[HumanA]|
if(sActorName == "HumanA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Trader:[VOICE|HumanF1] Oh, am I standing in the presence of the dragon empress? I'm honored!") ]])

-- |[HumanB]|
elseif(sActorName == "HumanB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Izuna:[VOICE|Izuna] (This human isn't paying attention.)") ]])

-- |[HumanC]|
elseif(sActorName == "HumanC") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Trader:[VOICE|HumanF2] Legendary hero, eh. You wanna buy something?") ]])

-- |[HumanD]|
elseif(sActorName == "HumanD") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Trader:[VOICE|HumanM0] I should follow behind this dragon empress. Probably leaves a lot of bones in her wake. Ha ha! That's a joke!") ]])

-- |[HumanE]|
elseif(sActorName == "HumanE") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Trader:[VOICE|HumanM1] If there's a great evil or something, it doesn't matter. Doesn't make my next meal any easier to find.") ]])

-- |[HumanF]|
elseif(sActorName == "HumanF") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Trader:[VOICE|HumanF3] Who cares about legendary heroes? We've got real problems!") ]])

-- |[MercA]|
elseif(sActorName == "MercA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Mercenary:[VOICE|MercF] If there's really a great hero, tell them to clean up Westwoods first!") ]])

-- |[MercB]|
elseif(sActorName == "MercB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Mercenary:[VOICE|MercM] A great hero named Sanya? Is she hot? Asking for a friend.") ]])

-- |[WerecatA]|
elseif(sActorName == "WerecatA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Trader:[VOICE|Werecat] I don't know if I believe this, but all the other pamphlets were on point. I guess we're in some real trouble, eh?") ]])

-- |[HarpyA]|
elseif(sActorName == "HarpyA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Izuna:[VOICE|Izuna] (This harpy isn't paying attention.)") ]])

-- |[KitsuneA]|
elseif(sActorName == "KitsuneA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Miko:[VOICE|KitsuneA] Oh, I was already on-board, Izuna. I know you can save us! We're behind you all the way!") ]])

-- |[KitsuneB]|
elseif(sActorName == "KitsuneB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Warrior:[VOICE|KitsuneB] Ha ha! The Empress being tall? I didn't believe it but woah, she really is!") ]])

-- |[Guard]|
elseif(sActorName == "Guard") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HarpyC] A great hero?[P] Old legend?[P] Sounds like a fairy-tale.") ]])

-- |[ ===== Miso Characters ==== ]|
-- |[Miso]|
elseif(sActorName == "Miso") then
    LM_ExecuteScript(gsRoot .. "Chapter 2/Dialogue/Miso/Pamphlet Dragon.lua")
end

-- |[Clean Up]|
DL_PopActiveObject()
