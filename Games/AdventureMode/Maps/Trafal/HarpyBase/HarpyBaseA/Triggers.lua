-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
if(sObjectName == "WarpActivate") then
    fnWarpUnlockHandler("Root/Variables/Chapter2/Campfires/iHarpyBaseA", 40.25, 19.50)

elseif(sObjectName == "KitsuneMeeting") then

    -- |[Repeat Check]|
    local iSawKitsuneMeeting = VM_GetVar("Root/Variables/Chapter2/HarpyBase/iSawKitsuneMeeting", "N")
    if(iSawKitsuneMeeting == 1.0) then return end

    -- |[Variables]|
    local iMetMiso          = VM_GetVar("Root/Variables/Chapter2/Miso/iMetMiso", "N")
    local iEmpressJoined    = VM_GetVar("Root/Variables/Chapter2/MtSarulente/iEmpressJoined", "N")
    local iMisoMetEmpress   = VM_GetVar("Root/Variables/Chapter2/Miso/iMisoMetEmpress", "N")
    local sSanyaForm        = VM_GetVar("Root/Variables/Global/Sanya/sForm", "S")
    local iBlockadeResolved = VM_GetVar("Root/Variables/Chapter2/HarpyBlockade/iBlockadeResolved", "N")
    
    -- |[Flag]|
    VM_SetVar("Root/Variables/Chapter2/HarpyBase/iSawKitsuneMeeting", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter2/Miso/iMisoMetEmpress", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter2/Miso/iMetMiso", "N", 1.0)
    
    -- |[Movement]|
    fnCutsceneMergeParty()
    fnCutsceneBlocker()
    fnCutsceneMove("Sanya", 18.25, 40.50)
    fnCutsceneFace("Sanya", 0, -1)
    fnCutsceneMove("Izuna", 17.25, 40.50)
    fnCutsceneFace("Izuna", 0, -1)
    fnCutsceneMove("Zeke",  19.25, 40.50)
    fnCutsceneFace("Zeke", 0, -1)
    if(iEmpressJoined == 1.0) then
        fnCutsceneMove("Empress",  18.25, 41.50)
        fnCutsceneFace("Empress", 0, -1)
    end
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Miko:[VOICE|KitsuneA] Miso, look![P] Izuna is here!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneFace("Miso", 0, 1)
    fnCutsceneFace("KitsuneB", -1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneMove("Miso", 18.25, 37.50)
    fnCutsceneMove("Miso", 18.25, 39.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Miso", "Neutral") ]])
    
    -- |[ ======================= No-Empress Version ======================= ]|
    if(iEmpressJoined == 0.0) then
        
        -- |[ === Somehow Hasn't Met Miso ==== ]|
        if(iMetMiso == 0.0) then
        
            -- |[Variables]|
            local iSawKidnapIntro = VM_GetVar("Root/Variables/Chapter2/KitsuneVillage/iSawKidnapIntro", "N")
            
            -- |[Dialogue]|
            fnCutscene([[ Append("Miso:[E|Neutral] Well well, if it isn't Izuna.[P] I knew you weren't dead.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] You're sounding awfully blase about this.[B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] The Fox Goddess protects her followers.[P] I knew you'd turn up sooner or later.[B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] Which is not to say I didn't look.[P] We sent out search parties.[B][C]") ]])
            
            -- |[Has Not Visited Village]|
            if(iSawKidnapIntro == 0.0) then
                fnCutscene([[ Append("Izuna:[E|Ugh] Oh dear...[B][C]") ]])
                fnCutscene([[ Append("Miso:[E|Neutral] I assume you were on your way to let everyone know you're all right.[B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Smirk] Of course![P] But if you get there first, please let them know for me.[B][C]") ]])
                fnCutscene([[ Append("Miso:[E|Neutral] If my route takes me there, sure.[B][C]") ]])
            
            -- |[Has Visited the Village]|
            else
                fnCutscene([[ Append("Izuna:[E|Ugh] So I heard.[P] The elder gave me quite a scolding![B][C]") ]])
                fnCutscene([[ Append("Miso:[E|Neutral] And a well-deserved one, at that.[P] Maybe you'll learn this time.[B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Smirk] Never again![B][C]") ]])
                fnCutscene([[ Append("Miso:[E|Neutral] If I run into any of the search parties I'll let them know you're all right.[B][C]") ]])
            end
            fnCutscene([[ Append("Sanya:[E|Neutral] So are you a wandering priest or something?[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Neutral] Miso is a great warrior, Sanya![P] We can learn much from her![B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] Oh please, Izuna.[P] War does not make one great.[B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] No, I left that life behind me.[P] I'm a travelling cook.[P] Well, I make other things, too, but my soup is my calling.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] I was about to ask how you got the name Miso...[B][C]") ]])
            fnCutscene([[ Append("Zeke:[E|Neutral] Bah...[B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] My name before I became a kitsune was Orla.[B][C]") ]])
            fnCutscene([[ Append("Zeke:[E|Neutral] Nyeh![P] Nyeh![B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] Oh, hungry?[P] Here, try some of this broth, little guy.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Smirk] That's Zeke, and this is Sanya.[B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] Seems he likes it.[B][C]") ]])
            fnCutscene([[ Append("Zeke:[E|Neutral] *slurp gulp*[P] nyaaahhhhh![B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] Hey Miso.[P] Can you do anything with this stuff I've found in my hunts?[P] I'm not exactly a chef here.[B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] A hunter, are you?[P] I suppose I can.[B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] Tell you what, Sanya.[P] Since you're a friend of Izuna's, I can offer you my skills.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Happy] We'd be honored![P] Thank you very much![B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] So here's how this works.[P] The creatures of Trafal have many unique properties.[P] Leather, fiber, scales, all enchanted.[B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] The mountain winds here focus natural magical flows coming up from beneath us.[P] It's what makes our hot springs hot, and allows the underground to grow food.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] I'm not really from around here.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Smirk] Miso knows a lot about Trafal.[P] She's very wise, you can tell because she has five tails![B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] Ho ho![P] Izuna, don't go saying that![B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] Because now the Fox Goddess will take it upon herself to make a kitsune with many tails say something truly foolish, to test you![B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] You must not believe what someone says simply because of their credentials, young ones.[P] Divine tails or otherwise, nobody is so wise that they cannot lead you astray.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] Except the Fox Goddess?[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Smirk] She might, just to screw with you.[B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] Now as I was saying, the creatures here have magical ingredients I can use.[P] I can take some of them from you to make items you'll not find anywhere else in the world.[B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] Once you have provided me enough the ingredients I need for an item, I'll add it to my shop for cheap.[P] After all, providing me the materials is most of the work.[B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] I can make armors, accessories, even some items you can use to heal.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Smirk] Thank you, Miso.[P] I'm sure these will help on our journey.[B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] Oh, what story are you after this time, Izuna?[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] Saving the world.[B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] Really?[P] Come off it, Izuna.[P] It's an old legend.[P] Don't believe everything you read.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] She already knows about me?[B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] Izuna wouldn't shut up about the hero of legend, and then she just left.[P] I put the pieces together.[P] Especially with a name like Sanya.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Smirk] I happen to believe the legend is true.[P] Can I ask you to believe it as I do?[B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] Oh, I don't doubt that you believe it is true.[P] But a wise kitsune knows to require a bit more than faith, no matter how solid.[B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] Besides, the most powerful aspect of well-placed faith is the knowledge that doubt can do it no harm.[B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] If Sanya here really is the hero, then a bit of doubt won't change that, will it?[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Explain] Of course not![B][C]") ]])
            fnCutscene([[ Append("Zeke:[E|Neutral] Nggrlbbyeh![B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Sad] Zeke don't speak with your mouth full![B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] So, what are you doing standing around looking nervous?[B][C]") ]])
            
            fnCutscene([[ Append("Miso:[E|Neutral] The harpies built a fort sometime in the last few weeks or two.[P] None of us even knew about it.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Neutral] All of this in a few weeks?[B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] Armies can build things quickly, they have at least a few hundred troops. We've been seeing a handful of patrols on the roads, but this?[B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] I had figured they were a visiting army passing through.[P] If they've built a fort, this might be a problem.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Blush] Want me to seduce their leader and convince them to leave?[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Blush] She is totally serious. It might just work.[B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] You don't even know who their leader is and immediately jump to seduction?[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Smirk] Their patrols have been trying to beat us up, but make love, not war, right?[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Neutral] It's true.[P] The harpies have been hassling us.[B][C]") ]])
            if(sSanyaForm == "Human") then
                fnCutscene([[ Append("Miso:[E|Neutral] Maybe because Sanya looks like a bandit.[B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Angry] HEY![B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Angry] I look like a really hot and sexy bandit![B][C]") ]])
            elseif(sSanyaForm == "Sevavi") then
                fnCutscene([[ Append("Miso:[E|Neutral] Maybe because Sanya looks like a monster.[B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Laugh] They're probably just jealous that I'm so hot.[B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Blush] I know I am![B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Blush] [P]*kiss*[P] You're the hot one, Izuna![B][C]") ]])
            elseif(sSanyaForm == "Werebat") then
                fnCutscene([[ Append("Miso:[E|Neutral] Maybe because Sanya looks like a monster.[B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Laugh] I [P]*am*[P] a monster.[P] So uh, fair play to them.[B][C]") ]])
            end
            fnCutscene([[ Append("Miso:[E|Neutral] In any case, these two asked me to come take a look.[P] We're a little nervous about going inside, but it seems the local traders aren't.[B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] They seem to be pretty well armed.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Explain] Sounds like a story to me![B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] Oh boy.[P] Izuna, remember that you represent all of the kitsunes, not just yourself.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Explain] The people of Trafal deserve to know who these harpies are and what they're planning![P] I'm going to find out![B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Happy] Hell yeah![B][C]") ]])
            if(iBlockadeResolved == 1.0) then
                fnCutscene([[ Append("Izuna:[E|Smirk] Besides, we ran into some harpies at a blockade in Granvire not too long ago.[P] They're searching people for weapons.[B][C]") ]])
            else
                fnCutscene([[ Append("Izuna:[E|Smirk] Besides, there have been a lot of bandits around lately.[P] Maybe they're just here to put a stop to that?[B][C]") ]])
            end
            fnCutscene([[ Append("Miso:[E|Neutral] Sure, but if this is the start of a military occupation, that's the excuse they'd give.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] As a resident of a mountainous country with a multi-century history of having armies pass through it and then 'decide' to stay, Miso is totally right.[B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] Just don't say anything that will make the temple look bad, okay?[P] There hasn't been a war in Trafal since...[P] I don't know.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Ugh] What could I say that could spark a war, Miso?[B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] Heh, you're incorrigible when it comes to the truth.[P] Keep in mind that even the truth can be dangerous.") ]])
        
        -- |[Has Met Miso]|
        else
            fnCutscene([[ Append("Miso:[E|Neutral] Well well, you get around almost as much as I do.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Smirk] Hello again, Miso![B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] Why are you guys standing around looking nervous?[B][C]") ]])
            
            fnCutscene([[ Append("Miso:[E|Neutral] The harpies built a fort sometime in the last few weeks or two.[P] None of us even knew about it.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Neutral] All of this in a few weeks?[B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] Armies can build things quickly, they have at least a few hundred troops. We've been seeing a handful of patrols on the roads, but this?[B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] I had figured they were a visiting army passing through.[P] If they've built a fort, this might be a problem.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Blush] Want me to seduce their leader and convince them to leave?[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Blush] She is totally serious. It might just work.[B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] You don't even know who their leader is and immediately jump to seduction?[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Smirk] Their patrols have been trying to beat us up, but make love, not war, right?[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Neutral] It's true.[P] The harpies have been hassling us.[B][C]") ]])
            if(sSanyaForm == "Human") then
                fnCutscene([[ Append("Miso:[E|Neutral] Maybe because Sanya looks like a bandit.[B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Angry] HEY![B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Angry] I look like a really hot and sexy bandit![B][C]") ]])
            elseif(sSanyaForm == "Sevavi") then
                fnCutscene([[ Append("Miso:[E|Neutral] Maybe because Sanya looks like a monster.[B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Laugh] They're probably just jealous that I'm so hot.[B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Blush] I know I am![B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Blush] [P]*kiss*[P] You're the hot one, Izuna![B][C]") ]])
            elseif(sSanyaForm == "Werebat") then
                fnCutscene([[ Append("Miso:[E|Neutral] Maybe because Sanya looks like a monster.[B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Laugh] I [P]*am*[P] a monster.[P] So uh, fair play to them.[B][C]") ]])
            elseif(sSanyaForm == "Harpy") then
                fnCutscene([[ Append("Sanya:[E|Neutral] It is extremely difficult to explain so I'm not going to bother.[B][C]") ]])
            elseif(sSanyaForm == "Bunny") then
                fnCutscene([[ Append("Miso:[E|Neutral] Maybe because Sanya looks like a smuggler.[B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Angry] I knew suspenders were the wrong choice but they were so ME![B][C]") ]])
            end
            fnCutscene([[ Append("Miso:[E|Neutral] In any case, these two asked me to come take a look.[P] We're a little nervous about going inside, but it seems the local traders aren't.[B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] They seem to be pretty well armed.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Explain] Sounds like a story to me![B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] Oh boy.[P] Izuna, remember that you represent all of the kitsunes, not just yourself.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Explain] The people of Trafal deserve to know who these harpies are and what they're planning![P] I'm going to find out![B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Happy] Hell yeah![B][C]") ]])
            if(iBlockadeResolved == 1.0) then
                fnCutscene([[ Append("Izuna:[E|Smirk] Besides, we ran into some harpies at a blockade in Granvire not too long ago.[P] They're searching people for weapons.[B][C]") ]])
            else
                fnCutscene([[ Append("Izuna:[E|Smirk] Besides, there have been a lot of bandits around lately.[P] Maybe they're just here to put a stop to that?[B][C]") ]])
            end
            fnCutscene([[ Append("Miso:[E|Neutral] Sure, but if this is the start of a military occupation, that's the excuse they'd give.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] As a resident of a mountainous country with a multi-century history of having armies pass through it and then 'decide' to stay, Miso is totally right.[B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] Just don't say anything that will make the temple look bad, okay?[P] There hasn't been a war in Trafal since...[P] I don't know.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Ugh] What could I say that could spark a war, Miso?[B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] Heh, you're incorrigible when it comes to the truth.[P] Keep in mind that even the truth can be dangerous.") ]])
        
        
        end

    -- |[ ======================== Empress Version ========================= ]|
    else
        
        -- |[Has Not Met Miso, Somehow]|
        if(iMetMiso == 0.0) then
            fnCutscene([[ Append("Miso:[E|Neutral] Izuna![P] You're safe![B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Smirk] Hello, Miso![B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] I figured you were okay.[P] We sent out search parties, you know.[P] Don't tell me you were just cavorting around without telling anyone where you went.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Explain] I got really badly injured, and then Sanya and Zeke saved my life![B][C]") ]])
            fnCutscene([[ Append("Zeke:[E|Neutral] Bah![B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Blush] Then we kissed and stuff.[B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] You mean, you and Sanya kissed?[P] Or by we...[P] do you mean all three of you, including the goat?[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Ugh] AndthenwemetEmpressthedragon![B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] Hello.[B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] Seems you've been having a real adventure.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Blush] Thanks, Miso![B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] Oh and I'm a great hero and stuff.[P] Got a runestone, I can transform into monsters.[P] It's no big deal.[B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] I can't wait to read your next pamphlet, Izuna.[P] It's going to be a doozy.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Explain] Sanya, Miso is a great kitsune warrior![P] She might be able to help us![B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] Ha ha, warrior.[P] Izuna, I left that life behind.[P] You know that.[B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] I'm better known for my soup.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Smirk] It's really good soup![B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] Can you make anything we could use on our journey?[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] She's an artisan.[P] She's carrying an alchemy flux bag.[B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] Very sharp eyes on you.[P] Yes, Sanya, I can make items for you.[B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] The winds of Trafal carry magic on them.[P] If you get me certain materials, I can make special equipment for you.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Happy] Really!?[P] It'd be an honor, Miso![P] Thank you so much![B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] Given the story you're telling me, I get the feeling you'll need all the help you can get.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] So, why are you all standing around looking nervous?[B][C]") ]])
            
            fnCutscene([[ Append("Miso:[E|Neutral] Oh dear, I almost forgot.[B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] The harpies built a fort sometime in the last few weeks or two.[P] None of us even knew about it.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Neutral] All of this in a few weeks?[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] With proper discipline, a wooden fort can be constructed in a day, depending on the size of an army.[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] Given the grade of the entrenchment, the terrain, local water sources, and so on, I'd say this army has fewer than two-hundred heads.[B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] Sounds about right.[P] We've been noticing some armed harpy patrols on the roads lately, but they never accost kitsunes.[B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] I had figured they were a visiting army passing through.[P] If they've built a fort, this might be a problem.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Blush] Need me to beat the stuffing out of any of them?[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Ugh] Sanya...[B][C]") ]])
            fnCutscene([[ Append("Zeke:[E|Neutral] Bah![B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Blush] Come on![P] How many chances does one get to beat up an army!?[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] As many as you like, until you actually take it.[P] Then you get one.[B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] Besides, why do you jump immediately to violence?[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Smirk] Because they've been doing the same to us.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Neutral] It's true.[P] The harpies have been hassling us.[B][C]") ]])
            if(sSanyaForm == "Human") then
                fnCutscene([[ Append("Miso:[E|Neutral] Maybe because Sanya looks like a bandit.[B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Angry] HEY![B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Angry] I look like a really hot and sexy bandit![B][C]") ]])
            elseif(sSanyaForm == "Sevavi") then
                fnCutscene([[ Append("Miso:[E|Neutral] Maybe because Sanya looks like a monster.[B][C]") ]])
                fnCutscene([[ Append("Empress:[E|Neutral] They are likely unfamiliar with the Sevavi.[B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Laugh] They're probably just jealous that I'm so hot.[B][C]") ]])
            elseif(sSanyaForm == "Werebat") then
                fnCutscene([[ Append("Miso:[E|Neutral] Maybe because Sanya looks like a monster.[B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Laugh] I [P]*am*[P] a monster.[P] So uh, fair play to them.[B][C]") ]])
            elseif(sSanyaForm == "Harpy") then
                fnCutscene([[ Append("Sanya:[E|Neutral] It is extremely difficult to explain so I'm not going to bother.[B][C]") ]])
            elseif(sSanyaForm == "Bunny") then
                fnCutscene([[ Append("Miso:[E|Neutral] Maybe because Sanya looks like a smuggler.[B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Angry] I knew suspenders were the wrong choice but they were so ME![B][C]") ]])
            end
            fnCutscene([[ Append("Miso:[E|Neutral] In any case, these two asked me to come take a look.[P] We're a little nervous about going inside, but it seems the local traders aren't.[B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] They seem to be pretty well armed.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Explain] Sounds like a story to me![B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] Oh boy.[P] Izuna, remember that you represent all of the kitsunes, not just yourself.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Explain] The people of Trafal deserve to know who these harpies are and what they're planning![P] I'm going to find out![B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Happy] Hell yeah![B][C]") ]])
            if(iBlockadeResolved == 1.0) then
                fnCutscene([[ Append("Izuna:[E|Smirk] Besides, we ran into some harpies at a blockade in Granvire not too long ago.[P] They're searching people for weapons.[B][C]") ]])
            else
                fnCutscene([[ Append("Izuna:[E|Smirk] Besides, there have been a lot of bandits around lately.[P] Maybe they're just here to put a stop to that?[B][C]") ]])
            end
            fnCutscene([[ Append("Miso:[E|Neutral] Sure, but if this is the start of a military occupation, that's the excuse they'd give.[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] To invade Trafal is insane, and to occupy it doubly so.[P] The mountains themselves are an army's nemesis.[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] There must be another reason.[P] The only way to find that out is to follow Izuna's instincts.[B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] Just don't say anything that will make the temple look bad, okay?[P] There hasn't been a war in Trafal since...[P] I don't know.[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] Would the kitsunes declare war on this harpy army?[B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] No, but the people of Poterup might.[P] Let's make sure that doesn't happen.") ]])
        
        -- |[Already Met Miso]|
        else
            fnCutscene([[ Append("Miso:[E|Neutral] Well well, you get around almost as much as I do.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Smirk] Hello again, Miso![B][C]") ]])
            
            --Miso hasn't met Empress.
            if(iMisoMetEmpress == 0.0) then
                fnCutscene([[ Append("Miso:[E|Neutral] And who's your...[P] rather tall friend?[B][C]") ]])
                fnCutscene([[ Append("Empress:[E|Neutral] Empress Arulente.[B][C]") ]])
                fnCutscene([[ Append("Miso:[E|Neutral] Uh huh.[P] Normally I'd question such an assertion, but you are extremely tall, and a dragon.[P] So you fit the bill.[B][C]") ]])
                fnCutscene([[ Append("Miso:[E|Neutral] That, or it's just a coincidence that your face is all over the imperial highway.[B][C]") ]])
                fnCutscene([[ Append("Empress:[E|Neutral] Whether or not you believe my assertion has no bearing, because who I am does not particularly matter.[B][C]") ]])
                fnCutscene([[ Append("Empress:[E|Neutral] I have decided to travel with these children, and assist them.[P] I could be a beggar and it would change nothing.[B][C]") ]])
                fnCutscene([[ Append("Miso:[E|Neutral] True enough, but when someone calls themselves Empress, it comes off as imperious.[B][C]") ]])
                fnCutscene([[ Append("Empress:[E|Neutral] That is exactly what the word means, yes.[B][C]") ]])
                fnCutscene([[ Append("Miso:[E|Neutral] Right.[B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Neutral] She wants to help.[P] What's going on here, anyway?[B][C]") ]])
            
            --Miso has.
            else
                fnCutscene([[ Append("Sanya:[E|Neutral] Why are you guys standing around looking nervous?[B][C]") ]])
            end
            fnCutscene([[ Append("Miso:[E|Neutral] The harpies built a fort sometime in the last few weeks or two.[P] None of us even knew about it.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Neutral] All of this in a few weeks?[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] With proper discipline, a wooden fort can be constructed in a day, depending on the size of an army.[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] Given the grade of the entrenchment, the terrain, local water sources, and so on, I'd say this army has fewer than two-hundred heads.[B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] Sounds about right.[P] We've been noticing some armed harpy patrols on the roads lately, but they never accost kitsunes.[B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] I had figured they were a visiting army passing through.[P] If they've built a fort, this might be a problem.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Blush] Need me to beat the stuffing out of any of them?[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Ugh] Sanya...[B][C]") ]])
            fnCutscene([[ Append("Zeke:[E|Neutral] Bah![B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Blush] Come on![P] How many chances does one get to beat up an army!?[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] As many as you like, until you actually take it.[P] Then you get one.[B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] Besides, why do you jump immediately to violence?[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Smirk] Because they've been doing the same to us.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Neutral] It's true.[P] The harpies have been hassling us.[B][C]") ]])
            if(sSanyaForm == "Human") then
                fnCutscene([[ Append("Miso:[E|Neutral] Maybe because Sanya looks like a bandit.[B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Angry] HEY![B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Angry] I look like a really hot and sexy bandit![B][C]") ]])
            elseif(sSanyaForm == "Sevavi") then
                fnCutscene([[ Append("Miso:[E|Neutral] Maybe because Sanya looks like a monster.[B][C]") ]])
                fnCutscene([[ Append("Empress:[E|Neutral] They are likely unfamiliar with the Sevavi.[B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Laugh] They're probably just jealous that I'm so hot.[B][C]") ]])
            elseif(sSanyaForm == "Werebat") then
                fnCutscene([[ Append("Miso:[E|Neutral] Maybe because Sanya looks like a monster.[B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Laugh] I [P]*am*[P] a monster.[P] So uh, fair play to them.[B][C]") ]])
            elseif(sSanyaForm == "Harpy") then
                fnCutscene([[ Append("Sanya:[E|Neutral] It is extremely difficult to explain so I'm not going to bother.[B][C]") ]])
            elseif(sSanyaForm == "Bunny") then
                fnCutscene([[ Append("Miso:[E|Neutral] Maybe because Sanya looks like a smuggler.[B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Angry] I knew suspenders were the wrong choice but they were so ME![B][C]") ]])
            end
            fnCutscene([[ Append("Miso:[E|Neutral] In any case, these two asked me to come take a look.[P] We're a little nervous about going inside, but it seems the local traders aren't.[B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] They seem to be pretty well armed.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Explain] Sounds like a story to me![B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] Oh boy.[P] Izuna, remember that you represent all of the kitsunes, not just yourself.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Explain] The people of Trafal deserve to know who these harpies are and what they're planning![P] I'm going to find out![B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Happy] Hell yeah![B][C]") ]])
            if(iBlockadeResolved == 1.0) then
                fnCutscene([[ Append("Izuna:[E|Smirk] Besides, we ran into some harpies at a blockade in Granvire not too long ago.[P] They're searching people for weapons.[B][C]") ]])
            else
                fnCutscene([[ Append("Izuna:[E|Smirk] Besides, there have been a lot of bandits around lately.[P] Maybe they're just here to put a stop to that?[B][C]") ]])
            end
            fnCutscene([[ Append("Miso:[E|Neutral] Sure, but if this is the start of a military occupation, that's the excuse they'd give.[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] To invade Trafal is insane, and to occupy it doubly so.[P] The mountains themselves are an army's nemesis.[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] There must be another reason.[P] The only way to find that out is to follow Izuna's instincts.[B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] Just don't say anything that will make the temple look bad, okay?[P] There hasn't been a war in Trafal since...[P] I don't know.[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] Would the kitsunes declare war on this harpy army?[B][C]") ]])
            fnCutscene([[ Append("Miso:[E|Neutral] No, but the people of Poterup might.[P] Let's make sure that doesn't happen.") ]])
        
        end
        fnCutsceneBlocker()
    end
    
    -- |[Fold]|
    fnAutoFoldParty()
    fnCutsceneBlocker()
end
