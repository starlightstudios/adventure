-- |[ ===================================== Item Shop Setup ==================================== ]|
--Sells some crafting items and miscellaneous equipment.

-- |[Item Stock Listing]|
AM_SetShopProperty("Add Item", "Adamantite Powder",  -1, -1)
AM_SetShopProperty("Add Item", "Light Leather",      -1, -1)
AM_SetShopProperty("Add Item", "Light Fiber",        -1, -1)
AM_SetShopProperty("Add Item", "Light Carapace",     -1, -1)
AM_SetShopProperty("Add Item", "Tough Leather",      -1, -1)
AM_SetShopProperty("Add Item", "Tough Fiber",        -1, -1)
AM_SetShopProperty("Add Item", "Tough Carapace",     -1, -1)
AM_SetShopProperty("Add Item", "Gossamer",           -1, -1)
AM_SetShopProperty("Add Item", "Hardened Bark",      -1, -1)
AM_SetShopProperty("Add Item", "Unmelting Snow",     -1, -1)
AM_SetShopProperty("Add Item", "Crystallized Acid",  -1, -1)
AM_SetShopProperty("Add Item", "Glintsteel Gem",     -1, -1)
AM_SetShopProperty("Add Item", "Ardrion Gem",        -1, -1)
AM_SetShopProperty("Add Item", "Samlion Gem",        -1, -1)
AM_SetShopProperty("Add Item", "Iniorose Gem",       -1, -1)
AM_SetShopProperty("Add Item", "Thatophage Gem",     -1, -1)
