-- |[ ==================================== Pamphlet: Dragon ==================================== ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ==== Minor Characters ==== ]|
-- |[Ranchers]|
if(sActorName == "RancherA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Rancher:[VOICE|HarpyRecruit] Ha ha![P] Really, legendary heroes?[P] Dragons?[P] Sure, whatever.[P] By the way, what kind of partirhuman is your tall friend there?") ]])
    
elseif(sActorName == "RancherB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Rancher:[VOICE|HumanF3] Ancient hero?[P] Sounds like a fairy tale to me.[P] I hope it's real, though.") ]])

-- |[Guards]|
elseif(sActorName == "GuardA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Guard:[VOICE|HarpyRecruit] The kitsunes sure have some tall tales, don't they![P] No offense.") ]])

elseif(sActorName == "GuardB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Guard:[VOICE|HarpyC] If the world needs saving, trust the harpies to do it.[P] Tell this hero to sign up!") ]])

elseif(sActorName == "GuardC") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Guard:[VOICE|HarpyD] Oh look, someone who thinks I was born yesterday.") ]])

-- |[Cooks]|
elseif(sActorName == "CookA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Cook:[VOICE|HarpyOfficer] Okay, that's a stretch.[P] How could someone from 700 years ago be alive still?") ]])

-- |[Alraunes]|
elseif(sActorName == "AlrauneA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Gardener:[VOICE|Alraune] Ah, I have heard the legends.[P] I only regret that there are no alraune covenants here to give the great hero our form.") ]])

elseif(sActorName == "AlrauneB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Gardener:[VOICE|Alraune] The little ones have whispered of such a hero.[P] I hope she's an ally of nature.") ]])

-- |[Cleaners]|
elseif(sActorName == "CleanerA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Recruit:[VOICE|HumanM0] Woah, great hero?[P] Dragon empress?[P] I know we'll take out those bandits if we team up!") ]])

elseif(sActorName == "CleanerB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Recruit:[VOICE|HumanM1] Thanks, but no thanks.[P] You can't win a war with stories.") ]])

-- |[Lookouts]|
elseif(sActorName == "LookoutA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HumanF1] I'll believe in a legendary hero when she whups the bandit army for us.") ]])

elseif(sActorName == "LookoutB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HumanF2] Yeah I could take this hero in an armwrestling competition.[P] I have a secret trick.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] (Izuna will get mad at me if I break her arm...)") ]])

-- |[Counting]|
elseif(sActorName == "Counting") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HarpyOfficer] (This harpy is busy counting.)") ]])

-- |[Searching]|
elseif(sActorName == "SearchingA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HarpyRecruit] (This harpy is busy searching the bunks.)") ]])

-- |[Wounded]|
elseif(sActorName == "WoundedA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HumanF0] Oh, reading material![P] Thanks![P] I'll add it to my pile![P] Trust me, I'll get through all of it pretty quick.") ]])

elseif(sActorName == "WoundedB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HarpyD] Oh no, I hope I don't cross paths with no hero or whatever.[P] That's got trouble written all over it.") ]])

-- |[Off Duty]|
elseif(sActorName == "OffDutyA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HarpyRecruit] Ha ha, nice fairy tale, kitsune![P] Your writing is always interesting.") ]])

elseif(sActorName == "OffDutyB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HarpyD] Oh my, what other legends does Trafal have?[P] I love local stories!") ]])
    
-- |[Nurses]|
elseif(sActorName == "NurseA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Nurse:[VOICE|HumanM0] If this is true, dark times are upon us.") ]])

elseif(sActorName == "NurseB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Nurse:[VOICE|HumanF2] Wow, a great hero could be our presence and we'd never know it.[P] What do they look like?") ]])
    
-- |[ ==== Major Characters ==== ]|
elseif(sActorName == "CookB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Cook:[VOICE|Cook] A legendary hero has to eat too, you know.[P] Don't take your strength for granted, you rely on others for everything you do.") ]])
end

-- |[Clean Up]|
DL_PopActiveObject()
