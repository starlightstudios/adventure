-- |[ ============================= Pamphlet: The Bunny Smugglers ============================== ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ==== Minor Characters ==== ]|
-- |[Ranchers]|
if(sActorName == "RancherA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Rancher:[VOICE|HarpyRecruit] Hey you're right, we should have some bunnies at the ranch![P] Wait what's this about -[P] oh.[P] OH.") ]])
    
elseif(sActorName == "RancherB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Rancher:[VOICE|HumanF3] So that's why those bunnies are always so eager when they pass back through here.[P] That explains everything.") ]])

-- |[Guards]|
elseif(sActorName == "GuardA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Guard:[VOICE|HarpyRecruit] As long as they're not selling weapons down south, they can do whatever they want.[P] Weirdos.") ]])

elseif(sActorName == "GuardB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Guard:[VOICE|HarpyC] What's this about bunny guys?[P] No thanks.") ]])

elseif(sActorName == "GuardC") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Guard:[VOICE|HarpyD] Holy cow, the bunnies are dudes? Thanks, now I can make fun of them better.") ]])

-- |[Cooks]|
elseif(sActorName == "CookA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Cook:[VOICE|HarpyOfficer] A harem of monstermen?[P] What kind of crazy place did we move to?") ]])

-- |[Alraunes]|
elseif(sActorName == "AlrauneA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Gardener:[VOICE|Alraune] The little ones do not inhabit Westwoods, otherwise, I'd know where the bunnies have their headquarters.[P] This Angelface sounds very clever.") ]])

elseif(sActorName == "AlrauneB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Gardener:[VOICE|Alraune] I don't know if it is against the will of nature to partially transform a human like that, but I will credit the ingenuity of these bunnies.") ]])

-- |[Cleaners]|
elseif(sActorName == "CleanerA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Recruit:[VOICE|HumanM0] Ha ha, nice try, but I'm harpy all the way.") ]])

elseif(sActorName == "CleanerB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Recruit:[VOICE|HumanM1] You know I had one of those bunnies hit on me earlier?[P] I couldn't tell he was technically a man.[P] You know what else?[P] I kind of liked it.") ]])

-- |[Lookouts]|
elseif(sActorName == "LookoutA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HumanF1] Look, I have more important things to worry about than bunny shenanigans.") ]])

elseif(sActorName == "LookoutB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HumanF2] The bunnies can do whatever they want as long as they keep the supply of Jeffespeirian cheese coming.[P] I'd kill for a wheel of that.") ]])

-- |[Counting]|
elseif(sActorName == "Counting") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HarpyOfficer] (This harpy is busy counting.)") ]])

-- |[Searching]|
elseif(sActorName == "SearchingA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HarpyRecruit] (This harpy is busy searching the bunks.)") ]])

-- |[Wounded]|
elseif(sActorName == "WoundedA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HumanF0] Oh, reading material![P] Thanks! I'll add it to my pile![P] Trust me, I'll get through all of it pretty quick.") ]])

elseif(sActorName == "WoundedB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HarpyD] Ha ha, imagine using transformation for sex![P] It's a community-building ability.") ]])

-- |[Off Duty]|
elseif(sActorName == "OffDutyA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HarpyRecruit] Woah, really?[P] The bunnies are doing that?[P] Uh, maybe I can...[P] watch?[P] Uhhh...") ]])

elseif(sActorName == "OffDutyB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HarpyD] This explains why the bunnies I hit on just ignore me.") ]])
    
-- |[Nurses]|
elseif(sActorName == "NurseA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Nurse:[VOICE|HumanM0] The bunnies provide some rare medical herbs for us, so I'm not going to judge them.") ]])

elseif(sActorName == "NurseB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Nurse:[VOICE|HumanF2] Very clever, those bunnies.[P] Clever indeed.") ]])
    
-- |[ ==== Major Characters ==== ]|
elseif(sActorName == "CookB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Cook:[VOICE|Cook] The locals around here sure are weird.[P] You won't see anyone pulling an all-male bunny harem where I'm from.") ]])
end

-- |[Clean Up]|
DL_PopActiveObject()
