-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= Exits ========================================== ]|
if(sObjectName == "SampleExit") then
    
-- |[ ====================================== Examinables ======================================= ]|
elseif(sObjectName == "NopeSign") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (This path will lead to the Ice Spires in a later version.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "NopeSign2") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (This path will lead to the Old Capital Grounds in a later version.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "MedicalBooks") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (This book has a lot of pictures! Unfortunately it's all anatomy and stuff.[P] Must be a surgery book.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "MedicalFood") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (While it looks like conventional food, the labels indicate these are medicines cooked into food to make them taste better.[P] Don't eat the ipecac brownie.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "MedicalBarrels") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Barrels of medical cleaners.[P] One of them is straight alcohol, one is bleach for cleaning the floor.[P] Maybe they should actually use it.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Booze") then
    local sSanyaForm = VM_GetVar("Root/Variables/Global/Sanya/sForm", "S")
    
    if(sSanyaForm ~= "Harpy") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Booze, smells awful.[P] Must be for harpies.)") ]])
        fnCutsceneBlocker()
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Booze, smells lovely.[P] I'd probably get murked if I sneaked some, though.)") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "Rations") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Rations for the soldiers stationed here.[P] Smells better than what I'd expect from army food.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Schedule") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Guard post schedule.[P] They rotate posts every few days, and every week they have a new code word to allow passage if spotted in the dark.[P] Doesn't say what it is, though.)") ]])
    fnCutsceneBlocker()


-- |[ ========================================== Other ========================================= ]|
-- |[ ========================================== Debug ========================================= ]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
