-- |[ ==================================== Movement Script ===================================== ]|
--Movement script used by an entity that paths around.
local sEntityName = "SearchingA"
local fWalkSpeed = 0.50

-- |[Node Data]|
--List of all nodes this entity can stop at.
local saNodeList = {"D0", "D1", "D2", "D3", "D4", "D5"}

-- |[Run Routine]|
fnNPCPathToRandomNode(sEntityName, fWalkSpeed, saNodeList)
