-- |[ ==================================== Movement Script ===================================== ]|
--Movement script used by an entity that paths around.
local sEntityName = "OffDutyA"
local fWalkSpeed = 0.50

-- |[Node Data]|
--List of all nodes this entity can stop at.
local saNodeList = {"B2", "A1", "F3", "F2", "F1", "F4", "C3"}

-- |[Run Routine]|
fnNPCPathToRandomNode(sEntityName, fWalkSpeed, saNodeList)
