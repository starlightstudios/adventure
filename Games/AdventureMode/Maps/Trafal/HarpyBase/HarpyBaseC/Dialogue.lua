-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[ ========= Setup ========== ]|
    -- |[Name Resolve]|
    --Resolve the name of the calling entity.
    local sActorName = TA_GetProperty("Name")
    
    -- |[ ==== Minor Characters ==== ]|
    -- |[Ranchers]|
    if(sActorName == "RancherA") then
        
        --Variables.
        local iHasHarpyForm = VM_GetVar("Root/Variables/Global/Sanya/iHasHarpyForm", "N")
        local iDidHarpyFeet = VM_GetVar("Root/Variables/Chapter2/HarpyBase/iDidHarpyFeet", "N")
        
        --Common.
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        
        --No harpy, or not feet:
        if(iHasHarpyForm == 0.0 or iDidHarpyFeet == 0.0) then
            fnCutscene([[ Append("Rancher:[VOICE|HarpyRecruit] These chickens are nothing but trouble![P] They peck worse than a new recruit!") ]])
        
        --Feet stuff.
        else
            fnCutscene([[ Append("Rancher:[VOICE|HarpyRecruit] I know you haven't been part of the flock long, but you can, uh, use the water there to clean your talons off.[P] You know, if you want.") ]])
            
        end
        
    elseif(sActorName == "RancherB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Rancher:[VOICE|HumanF3] I don't know about you, but I am nearly having an existential crisis about eating chicken eggs.[P] This is getting to me.") ]])

    -- |[Guards]|
    elseif(sActorName == "GuardA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Guard:[VOICE|HarpyRecruit] It's a lovely day to be on guard duty!") ]])
    
    elseif(sActorName == "GuardB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Guard:[VOICE|HarpyC] I get so antsy on guard duty.[P] I gotta move around![P] Spread my wings!") ]])
    
    elseif(sActorName == "GuardC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Guard:[VOICE|HarpyD] Nobody ever goes to the ice spires region but someone has to watch the bridge access.") ]])
    
    -- |[Cooks]|
    elseif(sActorName == "CookA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Cook:[VOICE|HarpyOfficer] Nothing beats roasted pheasant![P] Cooked over an open flame just like the fox goddess intended![B][C]") ]])
        fnCutscene([[ Append("Izuna:[VOICE|Izuna] (Oh, a fellow adherant among the harpies!)") ]])

    -- |[Alraunes]|
    elseif(sActorName == "AlrauneA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Gardener:[VOICE|Alraune] We came from Jeffespeir on contract to help with the farming.[P] I love it here![P] I'm thinking of staying and opening a shop.") ]])
    
    elseif(sActorName == "AlrauneB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Gardener:[VOICE|Alraune] I'm not in it for the money, I'm in it to make sure the little ones have a good time.[P] The harpies picked a lovely spot!") ]])
    
    -- |[Cleaners]|
    elseif(sActorName == "CleanerA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Recruit:[VOICE|HumanM0] Potato peeling duty again, pah![P] Transform me, I'm ready![P] I want to protect Trafal!") ]])
    
    elseif(sActorName == "CleanerB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Recruit:[VOICE|HumanM1] The harpies make sure you're okay with menial work before they recruit you.[P] Smart move.[P] I've seen a lot of mercs who can't handle cleanup, you don't want to hire them.[P] Me?[P] I like this stuff.[P] But I still want to be a harpy!") ]])
    
    -- |[Lookouts]|
    elseif(sActorName == "LookoutA") then
        
        --Variables.
        local iHasHarpyForm = VM_GetVar("Root/Variables/Global/Sanya/iHasHarpyForm", "N")
        local iDidHarpyFeet = VM_GetVar("Root/Variables/Chapter2/HarpyBase/iDidHarpyFeet", "N")
        
        --Common.
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        
        --No harpy, or not feet:
        if(iHasHarpyForm == 0.0 or iDidHarpyFeet == 0.0) then
            fnCutscene([[ Append("Harpy:[VOICE|HumanF1] No pillars of smoke or anything from the south.[P] We're watching for bandit gangs, but they know we're watching...") ]])
        
        --Feet stuff:
        else
            fnCutscene([[ Append("Harpy:[VOICE|HumanF1] That smell -[P] Greta went through with it, did she?[P] It was bound to work on someone sooner or later.[B][C]") ]])
            fnCutscene([[ Append("Harpy:[VOICE|HumanF1] You probably had a latent thing for feet.[P] Yeah, she told me about it once.[P] I guess it's not a problem, just don't stand too close to me, okay?") ]])
        end
    
    elseif(sActorName == "LookoutB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Harpy:[VOICE|HumanF2] The scouts haven't seen any movement from the south, but word is there's an army of pillaging bandits coming.") ]])
    
    -- |[Counting]|
    elseif(sActorName == "Counting") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Harpy:[VOICE|HarpyOfficer] Hmm hmm, seven days worth, three days of that...") ]])
    
    -- |[Searching]|
    elseif(sActorName == "SearchingA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Harpy:[VOICE|HarpyRecruit] It's my job to check the bunks for contraband.[P] Drugs and alcohol are considered acceptable to use, but not when on duty.[P] If you want them, you need to give them to the quartermaster for safekeeping!") ]])
    
    -- |[Wounded]|
    elseif(sActorName == "WoundedA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Harpy:[VOICE|HumanF0] Phew, I have to stay off this leg after a few hours.[P] Doc said it'll be a week before it heals![B][C]") ]])
        fnCutscene([[ Append("Izuna:[VOICE|Izuna] You have to make sure the bone sets correctly, because if it heals wrong, you'll need to break it again and heal it again![B][C]") ]])
        fnCutscene([[ Append("Harpy:[VOICE|HumanF0] Yeah, I know.[P] But I don't want to be in bed most of the day for a week![B][C]") ]])
        fnCutscene([[ Append("Izuna:[VOICE|Izuna] It could be a lot worse.[P] I broke my back and was in bed for months.[B][C]") ]])
        fnCutscene([[ Append("Harpy:[VOICE|HumanF0] ...[P] You're pretty tough, kitsune. I respect that.") ]])
    
    elseif(sActorName == "WoundedB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Harpy:[VOICE|HarpyD] I got my wing clipped in a scuffle with some religious weirdos.[P] They wouldn't consent to a search and tried to force their way in.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[VOICE|Sanya] What happened to them?[B][C]") ]])
        fnCutscene([[ Append("Harpy:[VOICE|HarpyD] They ran off when they discovered I know how to fight.[P] Bullies!") ]])
    
    -- |[Off Duty]|
    elseif(sActorName == "OffDutyA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Harpy:[VOICE|HarpyRecruit] I've got five days of leave.[P] Should I head to Poterup?[P] Maybe go see some of Trafal's sights?") ]])
    
    elseif(sActorName == "OffDutyB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Harpy:[VOICE|HarpyD] I heard there's a really nice spa in the mountains northeast of Poterup.[P] You think that's true?[P] I've got a few days of leave, might be worth it.") ]])
        
    -- |[Nurses]|
    elseif(sActorName == "NurseA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Nurse:[VOICE|HumanM0] Goodness the harpies get into so many fights.[P] I've worked in other army hospitals, and they don't do nearly as much fighting!") ]])
    
    elseif(sActorName == "NurseB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Nurse:[VOICE|HumanF2] The harpies pay very well for medical staff.[P] Maybe I should become a harpy, stay with them, you know?") ]])
        
    -- |[ ==== Major Characters ==== ]|
    elseif(sActorName == "CookB") then
    
        -- |[Setup]|
        --Variables
        local iSawCookTroubles = VM_GetVar("Root/Variables/Chapter2/HarpyBase/iSawCookTroubles", "N")
        local iEmpressJoined   = VM_GetVar("Root/Variables/Chapter2/MtSarulente/iEmpressJoined", "N")
    
        --Common.
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "HarpyRecruit", "Neutral") ]])
    
        -- |[Has Not Seen Troubles]|
        if(iSawCookTroubles == 0.0) then
            VM_SetVar("Root/Variables/Chapter2/HarpyBase/iSawCookTroubles", "N", 1.0)
            fnCutscene([[ Append("Cook:[E|Neutral] Oh hey, are you traders?[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] No, I'm a hardass and the goat is a hardhead.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Smirk] We're travellers![P] Is something wrong?[B][C]") ]])
            fnCutscene([[ Append("Cook:[E|Neutral] I was wondering if you knew how much it'd cost to get a proper kitchen set.[P] We've had the budget for one but we can't find anyone selling.[B][C]") ]])
            fnCutscene([[ Append("Cook:[E|Neutral] We're cooking using primitive implements here.[P] It's not exactly fast and we have to feed a whole army.[B][C]") ]])
            if(iEmpressJoined == 1.0) then
                fnCutscene([[ Append("Empress:[E|Neutral] And?[P] Do you need advanced equipment?[P] A fire and a stick should be enough.[B][C]") ]])
                fnCutscene([[ Append("Cook:[E|Neutral] It ain't about being fancy, it's about speed.[P] Open flames aren't as hot.[B][C]") ]])
                fnCutscene([[ Append("Empress:[E|Neutral] I see.[P] Efficiency is important.[B][C]") ]])
                fnCutscene([[ Append("Cook:[E|Neutral] Plus, having a nice cast iron pan really locks in the flavor.[P] Just because we're soldiers doesn't mean we don't have tastebuds.[B][C]") ]])
                fnCutscene([[ Append("Empress:[E|Neutral] Of course.[P] An army's morale is in its stomach.[B][C]") ]])
                fnCutscene([[ Append("Cook:[E|Neutral] So is there any chance you know who'd be able to get me a quote?[B][C]") ]])
            else
                fnCutscene([[ Append("Sanya:[E|Neutral] What's primitive about a fire and a stick?[B][C]") ]])
                fnCutscene([[ Append("Zeke:[E|Neutral] ...[P] Bah?[B][C]") ]])
                fnCutscene([[ Append("Sanya:[E|Offended] Shut up, Zeke![P] I don't think before I speak, damn it![P] That's my charm![P] That's why people love me![B][C]") ]])
                fnCutscene([[ Append("Izuna:[E|Blush] I can't criticize...[B][C]") ]])
                fnCutscene([[ Append("Cook:[E|Neutral] Obvious answers aside, do you happen to know?[B][C]") ]])
            end
            fnCutscene([[ Append("Sanya:[E|Happy] I'll do you one better, pal![P] I'll go get you the kitchen set myself![B][C]") ]])
            fnCutscene([[ Append("Cook:[E|Neutral] You'd do that?[P] For me?[P] On a random offhanded request?[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Happy] Helping people rules![B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Happy] I love helping people![B][C]") ]])
            fnCutscene([[ Append("Zeke:[E|Neutral] Nyeh.[B][C]") ]])
            if(iEmpressJoined == 1.0) then
                fnCutscene([[ Append("Empress:[E|Neutral] (What enthusiasm the children can muster over such a small thing...)[B][C]") ]])
            end
            fnCutscene([[ Append("Cook:[E|Neutral] Uh, okay.[P] I'll reimburse you if there are expenses, up to 800 platina.[P] That's my budget.[B][C]") ]])
            fnCutscene([[ Append("Cook:[E|Neutral] I don't know where you're going to find a kitchen set for sale, but I'm not going to say no to free help.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Offended] Nothing will stand in the way of justice and high quality food![P] I swear it![P] I will avenge you![B][C]") ]])
            fnCutscene([[ Append("Cook:[E|Neutral] Uh, can you promise me you won't kill anyone over this?[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] ...[B][C]") ]])
            fnCutscene([[ Append("Zeke:[E|Neutral] Bah![P] BAH![B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Smirk] We won't.[P] Kill.[P] Anyone for a kitchen set.[B][C]") ]])
            fnCutscene([[ Append("Cook:[E|Neutral] I do not like the way you worded that.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Jot] Okay let's see here...[P] kitchen equipment for harpies...[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Explain] Best place to start looking is Poterup village, Sanya![B][C]") ]])
            fnCutscene([[ Append("Cook:[E|Neutral] There wasn't anything for sale there, and the blacksmith said she was backlogged.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Explain] Still a good place to start, we can find a lead!") ]])
        
        -- |[Has Seen Troubles]|
        else
            fnCutscene([[ Append("Cook:[E|Neutral] Any luck?[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Sad] Not yet, but I'm working on it.[B][C]") ]])
            fnCutscene([[ Append("Cook:[E|Neutral] Don't sweat it.[P] Just keep your eyes open.[P] Thanks!") ]])
        end
        fnCutsceneBlocker()
    end
end
