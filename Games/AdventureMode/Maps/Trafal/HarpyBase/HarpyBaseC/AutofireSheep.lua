-- |[Sheep Autofire]|
--Script that gets called whenever the player is close to a sheep. Causes the sheep to periodically
-- play sheep noises. This is called 'realism'.
local iSheepCooldown = VM_GetVar("Root/Variables/Chapter5/Scenes/iSheepCooldown", "N")
if(iSheepCooldown < 1) then
    
    --Roll to see if we actually play a noise.
    local iRoll = LM_GetRandomNumber(1, 100)
    if(iRoll < 10) then
    
        --Cooldown.
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSheepCooldown", "N", 120.0)
        
        --Sound.
        local iSoundRoll = LM_GetRandomNumber(0, 7)
        AudioManager_PlaySound("Sheep0" .. iSoundRoll)
        
    end
    
--Decrement cooldown.
else
    VM_SetVar("Root/Variables/Chapter5/Scenes/iSheepCooldown", "N", iSheepCooldown - 1.0)
end
