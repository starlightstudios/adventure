-- |[ ============================= Pamphlet: Bandits in Westwoods ============================= ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ==== Minor Characters ==== ]|
-- |[Ranchers]|
if(sActorName == "RancherA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Rancher:[VOICE|HarpyRecruit] The bandits are closer than I thought.[P] Guess that means we'll be doing combat patrols soon.") ]])
    
elseif(sActorName == "RancherB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Rancher:[VOICE|HumanF3] Westwoods?[P] Really?[P] Time to fight!") ]])

-- |[Guards]|
elseif(sActorName == "GuardA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Guard:[VOICE|HarpyRecruit] I'm not surprised the bandits are already in Trafal.[P] Rumours have been going around for a while now.") ]])

elseif(sActorName == "GuardB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Guard:[VOICE|HarpyC] Ugh.[P] I don't want to fight in Westwoods.[P] Can't we attack them somewhere nicer?") ]])

elseif(sActorName == "GuardC") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Guard:[VOICE|HarpyD] Is this news?[P] Those dumb bandits were going to get eaten by animals sooner or later, let them enjoy the fruits of Westwoods.") ]])

-- |[Cooks]|
elseif(sActorName == "CookA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Cook:[VOICE|HarpyOfficer] If the bandits are so desperate they'd ambush in Westwoods, maybe we can just feed them to get them to go away.") ]])

-- |[Alraunes]|
elseif(sActorName == "AlrauneA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Gardener:[VOICE|Alraune] What a tragedy.[P] So desperate they would attack defenseless merchants and defensive mercenaries.") ]])

elseif(sActorName == "AlrauneB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Gardener:[VOICE|Alraune] Does this mean that the harpies will be marching to war soon?[P] Was it a mistake to make friends here?") ]])

-- |[Cleaners]|
elseif(sActorName == "CleanerA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Recruit:[VOICE|HumanM0] Sheesh, tell them to transform me already![P] Let me at those bandits!") ]])

elseif(sActorName == "CleanerB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Recruit:[VOICE|HumanM1] I'm eager to join the harpies, but we ought to be careful.[P] The bandits are desperate, and that means, vicious.") ]])

-- |[Lookouts]|
elseif(sActorName == "LookoutA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HumanF1] Their patrols are already in Westwoods?[P] They must be camping without fires, or moving at night.[P] Clever.") ]])

elseif(sActorName == "LookoutB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HumanF2] Grr, stupid bandits![P] If I had been there...") ]])

-- |[Counting]|
elseif(sActorName == "Counting") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HarpyOfficer] (This harpy is busy counting.)") ]])

-- |[Searching]|
elseif(sActorName == "SearchingA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HarpyRecruit] (This harpy is busy searching the bunks.)") ]])

-- |[Wounded]|
elseif(sActorName == "WoundedA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HumanF0] Oh, reading material![P] Thanks! I'll add it to my pile![P] Trust me, I'll get through all of it pretty quick.") ]])

elseif(sActorName == "WoundedB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HarpyD] Damn, bandits already![P] I hope I've recovered in time.") ]])

-- |[Off Duty]|
elseif(sActorName == "OffDutyA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HarpyRecruit] Hey, I don't want to think about that.[P] I'm off duty for a while.") ]])

elseif(sActorName == "OffDutyB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HarpyD] What's this?[P] Cancel my leave, let me at those bandits!") ]])
    
-- |[Nurses]|
elseif(sActorName == "NurseA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Nurse:[VOICE|HumanM0] More bandits, more fighting, more wounded.[P] Sad.") ]])

elseif(sActorName == "NurseB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Nurse:[VOICE|HumanF2] Will the harpies ask me to care for wounded bandits, too?") ]])
    
-- |[ ==== Major Characters ==== ]|
elseif(sActorName == "CookB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Cook:[VOICE|Cook] Oh good, bandits.[P] Another problem to add to the pile.") ]])
end

-- |[Clean Up]|
DL_PopActiveObject()
