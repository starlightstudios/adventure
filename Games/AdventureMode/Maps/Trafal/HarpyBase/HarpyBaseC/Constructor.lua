-- |[ ======================================= Constructor ====================================== ]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = fnResolveDirectory() --Map's name is the name of the folder it is in.
local sLevelMusic = "SanyasTheme"
local sMapResolveName = sLevelName

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[ ======================================== Standard ======================================== ]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

    -- |[Construction]|
	--Music.
	AL_SetProperty("Music", sLevelMusic)
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation(sMapResolveName)
    
    -- |[Backing]|
    fnTrafalMountainUnderlay(true)
    
    -- |[Shooting Setup]|
    AL_SetProperty("Shooting Depth", 1)
    
    -- |[Map Setup]|
    --Vuca Pass temporarily uses Northwoods.
    fnHandleWestwoodsMap(sLevelName)

-- |[ ======================================== Post-Exec ======================================= ]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	-- |[NPC Spawning]|
	--If an entity needs to spawn after a post-exec cutscene, put them here.
    fnSpawnNPCPattern("Chicken", "A", "C")
    fnSpawnNPCPattern("Sheep", "A", "B")
    fnSpawnNPCPattern("Rancher", "A", "B")
    fnSpawnNPCPattern("Guard", "A", "C")
    fnSpawnNPCPattern("Alraune", "A", "B")
    fnSpawnNPCPattern("Cook", "A", "B")
    fnSpawnNPCPattern("Lookout", "A", "B")
    fnSpawnNPCPattern("Wounded", "A", "B")
    fnSpawnNPCPattern("Nurse", "A", "B")
    fnSpawnNPCPattern("OffDuty", "A", "B")
    fnSpawnNPCPattern("Cleaner", "A", "B")
    fnStandardNPCByPosition("SearchingA")
    fnStandardNPCByPosition("Counting")
    
    --Pathing.
    local sBasePath = fnResolvePath()
    fnCreateStandardPathScript("AlrauneA",   sBasePath .. "PathScript_AlrauneA.lua",   gcbHideParallelTiming)
    fnCreateStandardPathScript("AlrauneB",   sBasePath .. "PathScript_AlrauneB.lua",   gcbHideParallelTiming)
    fnCreateStandardPathScript("CleanerB",   sBasePath .. "PathScript_CleanerB.lua",   gcbHideParallelTiming)
    fnCreateStandardPathScript("GuardB",     sBasePath .. "PathScript_GuardB.lua",     gcbHideParallelTiming)
    fnCreateStandardPathScript("OffDutyA",   sBasePath .. "PathScript_OffDutyA.lua",   gcbHideParallelTiming)
    fnCreateStandardPathScript("OffDutyB",   sBasePath .. "PathScript_OffDutyB.lua",   gcbHideParallelTiming)
    fnCreateStandardPathScript("RancherA",   sBasePath .. "PathScript_RancherA.lua",   gcbHideParallelTiming)
    fnCreateStandardPathScript("RancherB",   sBasePath .. "PathScript_RancherB.lua",   gcbHideParallelTiming)
    fnCreateStandardPathScript("SearchingA", sBasePath .. "PathScript_SearchingA.lua", gcbHideParallelTiming)

    -- |[Overlays]|
    --Place overlay code here.
    
    -- |[Skillbooks]|
    --Place skillbook overlay code here.
    
    -- |[Rifle Handling]|
    VM_SetVar("Root/Variables/Global/Sanya/iRifleCode", "N", gciSanyaRifle_NotHere)

	-- |[Underlays]|
    fnTrafalMountainUnderlay(false, 0, 300)

end
