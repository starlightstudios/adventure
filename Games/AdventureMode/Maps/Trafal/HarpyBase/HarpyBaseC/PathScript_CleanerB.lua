-- |[ ==================================== Movement Script ===================================== ]|
--Movement script used by an entity that paths around.
local sEntityName = "CleanerB"
local fWalkSpeed = 0.50

-- |[Node Data]|
--List of all nodes this entity can stop at.
local saNodeList = {"E0", "E1", "E2", "E3"}

-- |[Run Routine]|
fnNPCPathToRandomNode(sEntityName, fWalkSpeed, saNodeList)
