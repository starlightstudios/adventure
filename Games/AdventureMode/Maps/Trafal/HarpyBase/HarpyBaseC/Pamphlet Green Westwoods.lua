-- |[ =============================== Pamphlet: Green Westwoods =============================== ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ==== Minor Characters ==== ]|
-- |[Ranchers]|
if(sActorName == "RancherA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Rancher:[VOICE|HarpyRecruit] Westwoods wasn't always a dump?[P] Search me, I figured it was.") ]])
    
elseif(sActorName == "RancherB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Rancher:[VOICE|HumanF3] Tell those alraunes to hurry the hell up.") ]])

-- |[Guards]|
elseif(sActorName == "GuardA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Guard:[VOICE|HarpyRecruit] I'll go give a big fat kiss to those alraunes if they pull it off.[P] Is it like kissing a tree?[P] Is that weird?") ]])

elseif(sActorName == "GuardB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Guard:[VOICE|HarpyC] Finally, some good news!") ]])

elseif(sActorName == "GuardC") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Guard:[VOICE|HarpyD] Westwoods?[P] No, this way is the Ice Spires region.[P] No, I don't want a pamphlet.") ]])

-- |[Cooks]|
elseif(sActorName == "CookA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Cook:[VOICE|HarpyOfficer] Did you write this article, kitsune?[P] What lengths did you go to for this story?") ]])

-- |[Alraunes]|
elseif(sActorName == "AlrauneA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Gardener:[VOICE|Alraune] Oh my![P] I should go visit Marigold, perhaps her covenant needs help!") ]])

elseif(sActorName == "AlrauneB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Gardener:[VOICE|Alraune] We all have our roles to play in the grand play that is nature.[P] I wish the alraunes all the luck.") ]])

-- |[Cleaners]|
elseif(sActorName == "CleanerA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Recruit:[VOICE|HumanM0] So many noble causes, I wish I could commit to more than one.") ]])

elseif(sActorName == "CleanerB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Recruit:[VOICE|HumanM1] Those alraunes have to be tough to stick it out in Westwoods.[P] Good for them.") ]])

-- |[Lookouts]|
elseif(sActorName == "LookoutA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HumanF1] Hey, that's an idea.[P] Maybe we can hire some alraunes to help sniff out bandit patrols.") ]])

elseif(sActorName == "LookoutB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HumanF2] No, leave Westwoods a toxic hellhole![P] You can find greenery damn near everywhere!") ]])

-- |[Counting]|
elseif(sActorName == "Counting") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HarpyOfficer] (This harpy is busy counting.)") ]])

-- |[Searching]|
elseif(sActorName == "SearchingA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HarpyRecruit] (This harpy is busy searching the bunks.)") ]])

-- |[Wounded]|
elseif(sActorName == "WoundedA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HumanF0] Oh, reading material![P] Thanks![P] I'll add it to my pile![P] Trust me, I'll get through all of it pretty quick.") ]])

elseif(sActorName == "WoundedB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HarpyD] I've heard alraunes can heal from any injury.[P] What I'd give to have that power right about now.") ]])

-- |[Off Duty]|
elseif(sActorName == "OffDutyA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HarpyRecruit] Repairing Westwoods?[P] I'll toast to that!") ]])

elseif(sActorName == "OffDutyB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HarpyD] Those alraunes are always up to something.[P] Very secretive bunch.") ]])
    
-- |[Nurses]|
elseif(sActorName == "NurseA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Nurse:[VOICE|HumanM0] Hm, I wonder if there are medicines that only grow in the toxic Westwoods.[P] Now you have me thinking...") ]])

elseif(sActorName == "NurseB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Nurse:[VOICE|HumanF2] You know, I almost joined an alraune covenant once.[P] Got too scared to go through with it.") ]])
    
-- |[ ==== Major Characters ==== ]|
elseif(sActorName == "CookB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Cook:[VOICE|Cook] I wonder if alraunes taste like vegetables.[P] Don't look at me like that, you probably think a harpy tastes like chicken.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[VOICE|Izuna] (I didn't but now I can't get the idea out of my head...)") ]])
end

-- |[Clean Up]|
DL_PopActiveObject()
