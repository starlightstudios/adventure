-- |[ ============================= Pamphlet: The Bunny Smugglers ============================== ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ==== Minor Characters ==== ]|
-- |[HumanA]|
if(sActorName == "HumanA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Recruit:[VOICE|HumanM1] Sorry, you can't lure me away with promises of bunny sex![P] I'm committed to the harpy cause.") ]])

-- |[HumanB]|
elseif(sActorName == "HumanB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Recruit:[VOICE|HumanM0] Wow, I wonder if the harpies can do that, too?[P] Make me a harpy man?") ]])

-- |[Guard]|
elseif(sActorName == "Guard") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HarpyC] Woah, the bunnies do *that*?[P] I'm going to ask the next one I see about it.") ]])

-- |[WatchA]|
elseif(sActorName == "WatchA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HarpyD] Woah.[P] Uh.[P] Can I have a minute alone?[P] Uh, I gotta wait until I'm off duty...") ]])

-- |[WatchB]|
elseif(sActorName == "WatchB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HarpyRecruit] Holy cow that's hot.[P] This Angelface has her priorities straight.") ]])

-- |[Bunny and Officer]|
elseif(sActorName == "BunnyA" or sActorName == "Officer") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Izuna:[VOICE|Izuna] (These two are too busy talking to notice...)") ]])

-- |[Quartermaster]|
elseif(sActorName == "Quartermaster") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Quartermaster:[VOICE|KitsuneC] Sorry, I'm on duty.[P] I'll take a look at the pamphlet later, though.") ]])

-- |[Alraunes]|
elseif(sActorName == "AlrauneA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Bartender:[VOICE|Alraune] God damn, I should have thought of making a harem of monsterboys![P] There's more heterosexual monstergirls out there than you think, and they have platina...") ]])

elseif(sActorName == "AlrauneB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Gardener:[VOICE|Alraune] Huh, interesting.[P] I can't say I approve of this 'male monsterboy' stuff.[P] Does she somehow not like breasts?[P] How bizarre.") ]])

-- |[Drinker]|
elseif(sActorName == "Drinker") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Soldier:[VOICE|HarpyD] Do I look like I want to read a pamphlet right now?") ]])

-- |[Raiju]|
elseif(sActorName == "RaijuA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Raiju:[VOICE|HumanF0] Ha ha![P] Reading is not why I do what I do, sweetie.") ]])

-- |[Cleaners]|
elseif(sActorName == "CleanerA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Cleaner:[VOICE|HumanF1] Oh, to be a smuggler.[P] Think of the danger, the excitement...") ]])

elseif(sActorName == "CleanerB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Cleaner:[VOICE|HumanM1] A bunny boy?[P] Bunny *boys*?[P] Oh wow, I have to think about this...") ]])

-- |[Trainees]|
elseif(sActorName == "TraineeA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Trainee:[VOICE|HarpyRecruit] I knew those smugglers were up to something, but this?[P] Wow.") ]])

elseif(sActorName == "TraineeB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Trainee:[VOICE|HarpyOfficer] Who cares about those dumb bunnies anyway?[P] As long as they're not running weapons, screw 'em.") ]])

elseif(sActorName == "TraineeC") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Trainee:[VOICE|HarpyC] Ugh, no pamphlets right now.[P] I gotta focus on my next flight.") ]])

elseif(sActorName == "TraineeD") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Trainee:[VOICE|HarpyRecruit] Hah![P] Good for you, Angelface![P] I'll toast to that at the bar!") ]])

elseif(sActorName == "FlightMaster") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Officer:[VOICE|HarpyD] Those bunnies are almost as quick on the ground as we are in the air.[P] Angelface would do well to remember the hawk preys on the rabbit.") ]])

-- |[Farmer]|
elseif(sActorName == "FarmerA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Gardener:[VOICE|HarpyC] Huh.[P] I bought some...[P] uh...[P] toys from the smugglers a while back.[P] I guess I know why they didn't keep them for themselves.") ]])

-- |[Stablekeepers]|
elseif(sActorName == "StablekeeperA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Rancher:[VOICE|HarpyRecruit] The bunnies are quick, but we can run them down with our horses.[P] They had better not think of selling weapons to the bandits!") ]])

elseif(sActorName == "StablekeeperB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Rancher:[VOICE|HarpyRecruit] I appreciate the hustle of this Angelface person.[P] Managing a bunch of rowdy boys is what I do in the stable every day![P] But, uh, I don't bone them.") ]])

-- |[Clerks]|
elseif(sActorName == "ClerkA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Clerk:[VOICE|HarpyRecruit] Oh, I've seen all the pamphlets.[P] You're doing great by keeping the people informed!") ]])

elseif(sActorName == "ClerkB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Clerk:[VOICE|HarpyD] You can't fool me, you're the one who wrote those pamphlets.[P] Superb work![P] Don't be so modest.") ]])

-- |[ ==== Major Characters ==== ]|
-- |[Esmerelda]|
elseif(sActorName == "Esmerelda") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Esmerelda", "Neutral") ]])
    fnCutscene([[ Append("Esmerelda:[E|Neutral] Interesting.[P] She makes a harem of loyal bunny-men?[P] Hm.[B][C]") ]])
    fnCutscene([[ Append("Esmerelda:[E|Neutral] I don't really see making new harpies as a sexual event, but I suppose the feeling is different for those being transformed.[B][C]") ]])
    fnCutscene([[ Append("Esmerelda:[E|Neutral] I'll be keeping an eye on Angelface in any case.[P] We made a deal, but you can never be sure with a mobster if they'll keep their end of it.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] I think she's trustworthy so long as you keep her economics in mind.") ]])
end

-- |[Clean Up]|
DL_PopActiveObject()
