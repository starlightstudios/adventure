-- |[ ======================================= Constructor ====================================== ]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = fnResolveDirectory() --Map's name is the name of the folder it is in.
local sLevelMusic = "SanyasTheme"
local sMapResolveName = sLevelName

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[ ======================================== Standard ======================================== ]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

    -- |[Construction]|
	--Music.
	AL_SetProperty("Music", sLevelMusic)
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation(sMapResolveName)

    -- |[Backing]|
    fnTrafalMountainUnderlay(true)
    
    -- |[Shooting Setup]|
    AL_SetProperty("Shooting Depth", 1)
    
    -- |[Map Setup]|
    --Vuca Pass temporarily uses Northwoods.
    fnHandleWestwoodsMap(sLevelName)

-- |[ ======================================== Post-Exec ======================================= ]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	-- |[NPC Spawning]|
	--If an entity needs to spawn after a post-exec cutscene, put them here.
    fnSpawnNPCPattern("Human", "A", "B")
    fnSpawnNPCPattern("Cleaner", "A", "B")
    fnSpawnNPCPattern("Alraune", "A", "B")
    fnSpawnNPCPattern("Watch", "A", "B")
    fnSpawnNPCPattern("Trainee", "A", "D")
    fnSpawnNPCPattern("Stablekeeper", "A", "B")
    fnSpawnNPCPattern("Clerk", "A", "B")
    fnSpawnNPCPattern("Horse", "A", "B")
    fnStandardNPCByPosition("RaijuA")
    fnStandardNPCByPosition("Drinker")
    fnStandardNPCByPosition("FarmerA")
    fnStandardNPCByPosition("Quartermaster")
    fnStandardNPCByPosition("Guard")
    fnStandardNPCByPosition("BunnyA")
    fnStandardNPCByPosition("Officer")
    fnStandardNPCByPosition("FlightMaster")
    fnStandardNPCByPosition("Esmerelda")
    
    --Pathing.
    local sBasePath = fnResolvePath()
    fnCreateStandardPathScript("AlrauneB",      sBasePath .. "PathScript_AlrauneB.lua",      gcbHideParallelTiming)
    fnCreateStandardPathScript("CleanerA",      sBasePath .. "PathScript_CleanerA.lua",      gcbHideParallelTiming)
    fnCreateStandardPathScript("CleanerB",      sBasePath .. "PathScript_CleanerB.lua",      gcbHideParallelTiming)
    fnCreateStandardPathScript("StablekeeperA", sBasePath .. "PathScript_StablekeeperA.lua", gcbHideParallelTiming)
    fnCreateStandardPathScript("StablekeeperB", sBasePath .. "PathScript_StablekeeperB.lua", gcbHideParallelTiming)
    fnCreateStandardPathScript("TraineeA",      sBasePath .. "PathScript_TraineeA.lua",      gcbHideParallelTiming)

    --Reposition if the player hasn't met Esmerelda yet.
    local iMetEsmerelda = VM_GetVar("Root/Variables/Chapter2/HarpyBase/iMetEsmerelda", "N")
    if(iMetEsmerelda == 0.0) then
        fnCutsceneTeleport("ClerkA", 41.25, 36.50)
        fnCutsceneFace("ClerkA", 1, 0)
        fnCutsceneTeleport("Esmerelda", 42.25, 36.50)
        fnCutsceneFace("Esmerelda", -1, 0)
    end

    -- |[Overlays]|
    --Place overlay code here.
    
    -- |[Skillbook]|
    local iSkillbook0 = VM_GetVar("Root/Variables/Global/Sanya/iSkillbook0", "N")
    if(iSkillbook0 == 1.0) then
		AL_SetProperty("Set Layer Disabled", "SkillbookIco", true)
    end
    
    -- |[Skillbooks]|
    --Place skillbook overlay code here.
    
    -- |[Rifle Handling]|
    VM_SetVar("Root/Variables/Global/Sanya/iRifleCode", "N", gciSanyaRifle_NotHere)
	
	-- |[Underlays]|
    fnTrafalMountainUnderlay(false, 0, 300)

end
