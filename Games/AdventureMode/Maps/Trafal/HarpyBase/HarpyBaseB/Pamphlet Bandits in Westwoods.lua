-- |[ ============================= Pamphlet: Bandits in Westwoods ============================= ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ==== Minor Characters ==== ]|
-- |[HumanA]|
if(sActorName == "HumanA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Recruit:[VOICE|HumanM1] Cursed bandits! Let me at them!") ]])

-- |[HumanB]|
elseif(sActorName == "HumanB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Recruit:[VOICE|HumanM0] This is exactly why I want to become a harpy! Let's put a stop to those crooks!") ]])

-- |[Guard]|
elseif(sActorName == "Guard") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HarpyC] Damn it, bandits already seeping into Trafal and I'm stuck on guard duty!") ]])

-- |[WatchA]|
elseif(sActorName == "WatchA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HarpyD] Those bandits are getting closer by the day. I hope we're ready in time.") ]])

-- |[WatchB]|
elseif(sActorName == "WatchB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HarpyRecruit] Seems the people of Trafal don't like the bandits, either. This should help recruiting.") ]])

-- |[Bunny and Officer]|
elseif(sActorName == "BunnyA" or sActorName == "Officer") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Izuna:[VOICE|Izuna] (These two are too busy talking to notice...)") ]])

-- |[Quartermaster]|
elseif(sActorName == "Quartermaster") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Quartermaster:[VOICE|KitsuneC] Sorry, I'm on duty.[P] I'll take a look at the pamphlet later, though.") ]])

-- |[Alraunes]|
elseif(sActorName == "AlrauneA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Bartender:[VOICE|Alraune] Criminals are bad for business. Reliability is what's key. The harpies better give them what for.") ]])

elseif(sActorName == "AlrauneB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Gardener:[VOICE|Alraune] Why do the bandits resort to violence? If you live in harmony with the wilds, there is plenty of food for all.") ]])

-- |[Drinker]|
elseif(sActorName == "Drinker") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Soldier:[VOICE|HarpyD] Do I look like I want to read a pamphlet right now?") ]])

-- |[Raiju]|
elseif(sActorName == "RaijuA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Raiju:[VOICE|HumanF0] Ha ha![P] Reading is not why I do what I do, sweetie.") ]])

-- |[Cleaners]|
elseif(sActorName == "CleanerA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Cleaner:[VOICE|HumanF1] Nasty, scary bandits. No thanks!") ]])

elseif(sActorName == "CleanerB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Cleaner:[VOICE|HumanM1] Ugh, violence. Why can't the bandits just get a nice job and live quietly?") ]])

-- |[Trainees]|
elseif(sActorName == "TraineeA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Trainee:[VOICE|HarpyRecruit] If I see one of those crooks, I will mess them up!") ]])

elseif(sActorName == "TraineeB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Trainee:[VOICE|HarpyOfficer] Let me at those bandits!") ]])

elseif(sActorName == "TraineeC") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Trainee:[VOICE|HarpyC] Ugh, no pamphlets right now.[P] I gotta focus on my next flight.") ]])

elseif(sActorName == "TraineeD") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Trainee:[VOICE|HarpyRecruit] If I see those mercs, I'll buy them a drink. Good job!") ]])

elseif(sActorName == "FlightMaster") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Officer:[VOICE|HarpyD] My troops would have sent those bandits home in traction, I tell you!") ]])

-- |[Farmer]|
elseif(sActorName == "FarmerA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Gardener:[VOICE|HarpyC] An army marches on its stomach. The bandits must be raiding caravans for food and supplies.") ]])

-- |[Stablekeepers]|
elseif(sActorName == "StablekeeperA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Rancher:[VOICE|HarpyRecruit] I hope our mounted forces are ready in time. The horses want those bandits taken care of, too!") ]])

elseif(sActorName == "StablekeeperB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Rancher:[VOICE|HarpyRecruit] Those petty crooks, attacking merchants on the roads? Pathetic!") ]])

-- |[Clerks]|
elseif(sActorName == "ClerkA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Clerk:[VOICE|HarpyRecruit] Oh, I've seen all the pamphlets.[P] You're doing great by keeping the people informed!") ]])

elseif(sActorName == "ClerkB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Clerk:[VOICE|HarpyD] You can't fool me, you're the one who wrote those pamphlets.[P] Superb work![P] Don't be so modest.") ]])

-- |[ ==== Major Characters ==== ]|
-- |[Esmerelda]|
elseif(sActorName == "Esmerelda") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Esmerelda", "Neutral") ]])
    fnCutscene([[ Append("Esmerelda:[E|Neutral] Ah, excellent.[P] I've already seen this pamphlet being passed around.[B][C]") ]])
    fnCutscene([[ Append("Esmerelda:[E|Neutral] The people of Trafal need to know about this rising threat.[P] Only by working together can we stop them.[B][C]") ]])
    fnCutscene([[ Append("Esmerelda:[E|Neutral] Thank you, Izuna.[P] You're performing a vital service.") ]])
end

-- |[Clean Up]|
DL_PopActiveObject()
