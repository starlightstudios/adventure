-- |[ ==================================== Pamphlet: Dragon ==================================== ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ==== Minor Characters ==== ]|
-- |[HumanA]|
if(sActorName == "HumanA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Recruit:[VOICE|HumanM1] The great dragon empress is alive?[P] Never heard of her.[P] What's a dragon even look like?") ]])

-- |[HumanB]|
elseif(sActorName == "HumanB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Recruit:[VOICE|HumanM0] Great dragon, eh?[P] She's got the same goal as we do!") ]])

-- |[Guard]|
elseif(sActorName == "Guard") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HarpyC] A great hero?[P] Old legend?[P] Sounds like a fairy-tale.") ]])

-- |[WatchA]|
elseif(sActorName == "WatchA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HarpyD] Hm.[P] Whoever this dragon empress is, tell her we need help with those bandits.") ]])

-- |[WatchB]|
elseif(sActorName == "WatchB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HarpyRecruit] Great dragons?[P] Do you believe everything you read?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[VOICE|Izuna] (But I wrote it...)") ]])

-- |[Bunny and Officer]|
elseif(sActorName == "BunnyA" or sActorName == "Officer") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Izuna:[VOICE|Izuna] (These two are too busy talking to notice...)") ]])

-- |[Quartermaster]|
elseif(sActorName == "Quartermaster") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Quartermaster:[VOICE|KitsuneC] Sorry, I'm on duty.[P] I'll take a look at the pamphlet later, though.") ]])

-- |[Alraunes]|
elseif(sActorName == "AlrauneA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Bartender:[VOICE|Alraune] You believe those old fairy tales?[P] Well, to each their own.") ]])

elseif(sActorName == "AlrauneB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Gardener:[VOICE|Alraune] The dragon empress still lives?[P] She must be as old as the oldest grandfather!") ]])

-- |[Drinker]|
elseif(sActorName == "Drinker") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Soldier:[VOICE|HarpyD] Do I look like I want to read a pamphlet right now?") ]])

-- |[Raiju]|
elseif(sActorName == "RaijuA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Raiju:[VOICE|HumanF0] Ha ha![P] Reading is not why I do what I do, sweetie.") ]])

-- |[Cleaners]|
elseif(sActorName == "CleanerA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Cleaner:[VOICE|HumanF1] Great evil?[P] I hope this mighty dragon and hero can slay it.") ]])

elseif(sActorName == "CleanerB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Cleaner:[VOICE|HumanM1] Oh my.[P] If I join the harpies, will I have to fight this great evil?[P] I just want to do my job and keep my head down...") ]])

-- |[Trainees]|
elseif(sActorName == "TraineeA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Trainee:[VOICE|HarpyRecruit] Great evil?[P] Well, well, well![P] The might of the harpies will be needed sooner than we thought!") ]])

elseif(sActorName == "TraineeB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Trainee:[VOICE|HarpyOfficer] A transforming hero?[P] She should become a harpy!") ]])

elseif(sActorName == "TraineeC") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Trainee:[VOICE|HarpyC] Ugh, no pamphlets right now.[P] I gotta focus on my next flight.") ]])

elseif(sActorName == "TraineeD") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Trainee:[VOICE|HarpyRecruit] I'd feel a lot better if a foretold hero was on our side!") ]])

elseif(sActorName == "FlightMaster") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Officer:[VOICE|HarpyD] Great hero?[P] Dragon empress?[P] Tell them the recruiting office is that way, I say!") ]])

-- |[Farmer]|
elseif(sActorName == "FarmerA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Gardener:[VOICE|HarpyC] I don't want nothin' to do with villains of legend, but I hope these heroes succeed.") ]])

-- |[Stablekeepers]|
elseif(sActorName == "StablekeeperA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Rancher:[VOICE|HarpyRecruit] Hey, you must be that dragon in the pamphlet![P] We're all cheering for you!") ]])

elseif(sActorName == "StablekeeperB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Rancher:[VOICE|HarpyRecruit] If you need a mounted unit to kill the great evil, we'll be right beside you!") ]])

-- |[Clerks]|
elseif(sActorName == "ClerkA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Clerk:[VOICE|HarpyRecruit] Oh, I've seen all the pamphlets.[P] You're doing great by keeping the people informed!") ]])

elseif(sActorName == "ClerkB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Clerk:[VOICE|HarpyD] You can't fool me, you're the one who wrote those pamphlets.[P] Superb work![P] Don't be so modest.") ]])

-- |[ ==== Major Characters ==== ]|
-- |[Esmerelda]|
elseif(sActorName == "Esmerelda") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Esmerelda", "Neutral") ]])
    fnCutscene([[ Append("Esmerelda:[E|Neutral] Ah, I had a feeling it was you who wrote the pamphlet, Izuna.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Blush] Really?[B][C]") ]])
    fnCutscene([[ Append("Esmerelda:[E|Neutral] Who else would be a kitsune who wrote about a legendary hero and was hanging out with a giant blue dragon woman?[B][C]") ]])
    fnCutscene([[ Append("Empress:[E|Neutral] She has a point.[P] Why deny authorship?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Blush] I just don't want fame to go to my head...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Put Zeke's name on the byline.[P] Let the fame go to his head.[P] He is limitlessly humble.[B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] Bah.") ]])
end

-- |[Clean Up]|
DL_PopActiveObject()
