-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[ ========= Setup ========== ]|
    -- |[Name Resolve]|
    --Resolve the name of the calling entity.
    local sActorName = TA_GetProperty("Name")
    
    -- |[ ==== Minor Characters ==== ]|
    -- |[HumanA]|
    if(sActorName == "HumanA") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Recruit:[VOICE|HumanM1] I've got butterflies in my stomach![P] I can't wait to soar through the air!") ]])

    -- |[HumanB]|
    elseif(sActorName == "HumanB") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Recruit:[VOICE|HumanM0] None of the birds have big tits.[P] Will I?[P] Maybe it's bad aerodynamics.[B][C]") ]])
        fnCutscene([[ Append("Recruit:[VOICE|HumanM0] Doesn't matter.[P] I hope I have big tits![B][C]") ]])
        fnCutscene([[ Append("Sanya:[VOICE|Sanya] (This guy...[P] He has the right attitude.[P] I hope you have garage-sized titties, my friend.)") ]])

    -- |[Guard]|
    elseif(sActorName == "Guard") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Harpy:[VOICE|HarpyC] Do you not know how desks work?[P] You talk to me over the desk.[P] This isn't hard and yet so many people mess it up!") ]])
    
    -- |[WatchA]|
    elseif(sActorName == "WatchA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Harpy:[VOICE|HarpyD] Watch duty is a lot easier than patrolling.[P] We rotate in and out.[P] I hope I don't have to do Westwoods next week.") ]])
    
    -- |[WatchB]|
    elseif(sActorName == "WatchB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Harpy:[VOICE|HarpyRecruit] Nobody makes trouble.[P] It's a shame.[P] I joined up to fight rogues!") ]])
    
    -- |[Bunny and Officer]|
    elseif(sActorName == "BunnyA" or sActorName == "Officer") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Bunny:[VOICE|Bunny] So you're telling me that it ain't smuggling?[B][C]") ]])
        fnCutscene([[ Append("Officer:[VOICE|HarpyOfficer] No, because it's not illegal. Nobody cares if you sell kokayanee around here![P] Just don't sell it in the fort![B][C]") ]])
        fnCutscene([[ Append("Bunny:[VOICE|Bunny] But if it ain't smuggling, what is it?[B][C]") ]])
        fnCutscene([[ Append("Officer:[VOICE|HarpyOfficer] Uh, trading?[P] Merchant transport?[B][C]") ]])
        fnCutscene([[ Append("Bunny:[VOICE|Bunny] Is I goin' soft?[P] Am I now a legitimate businessbunny?[P] What is the world coming to...") ]])
    
    -- |[Quartermaster]|
    elseif(sActorName == "Quartermaster") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Quartermaster:[VOICE|KitsuneC] I'm sorry, this isn't a shop.[P] These supplies are for troops being deployed.") ]])
    
    -- |[Alraunes]|
    elseif(sActorName == "AlrauneA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Bartender:[VOICE|Alraune] Got a sweet deal going on here.[P] I run an inn, the harpies handle protection.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[VOICE|Sanya] Yeah, but you have to rat on anyone smuggling in here, don't you?[B][C]") ]])
        fnCutscene([[ Append("Bartender:[VOICE|Alraune] So what?[P] I don't owe these people anything.[P] You try to run weapons through Trafal, that's on you.") ]])
    
    elseif(sActorName == "AlrauneB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Gardener:[VOICE|Alraune] The soil here is thin, but given the right attenuation, we can grow some crops.[B][C]") ]])
        fnCutscene([[ Append("Gardener:[VOICE|Alraune] It's more to make sure the soldiers have hobbies in their off time, but the food is just as yummy!") ]])
    
    -- |[Drinker]|
    elseif(sActorName == "Drinker") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Soldier:[VOICE|HarpyD] So the Jambleflakes aren't actually alcoholic.[P] Neither is the Frenzied Pickle Spiker.[P] Just what the hell are these, and where's the vodka?") ]])
    
    -- |[Raiju]|
    elseif(sActorName == "RaijuA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Raiju:[VOICE|HumanF0] Hey, sweetie![P] Looking for a fun time?[P] I'm amped!") ]])
    
    -- |[Cleaners]|
    elseif(sActorName == "CleanerA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Cleaner:[VOICE|HumanF1] Check in time isn't for a few hours.[P] We're still cleaning the rooms.[P] Please come back later!") ]])
    
    elseif(sActorName == "CleanerB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Cleaner:[VOICE|HumanM1] I love this job.[P] Maybe the harpies will let me become a soldier who just cleans the bunks?[P] Not everyone has to fight.") ]])
    
    -- |[Trainees]|
    elseif(sActorName == "TraineeA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Trainee:[VOICE|HarpyRecruit] Remember to warm up those wings![P] Never get a cramp while soaring!") ]])
    
    elseif(sActorName == "TraineeB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Trainee:[VOICE|HarpyOfficer] I hate flight practice, but navigating the cold air thermals is tricky.[P] This isn't like the coasts.") ]])
    
    elseif(sActorName == "TraineeC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Trainee:[VOICE|HarpyC] You can see all the way to Trannadar when you get up on the northern thermals!") ]])
    
    elseif(sActorName == "TraineeD") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Trainee:[VOICE|HarpyRecruit] I am absolutely not used to the cold air.[P] I hope the next round of recruits is better build for the cold.") ]])
    
    elseif(sActorName == "FlightMaster") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Officer:[VOICE|HarpyD] My trainees will be the best damn aerial warriors you've ever seen.[P] Ten flights a day, minimum!") ]])
    
    -- |[Farmer]|
    elseif(sActorName == "FarmerA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Gardener:[VOICE|HarpyC] Maybe I should have been a farmer.[P] When I get older, I hope I get a nice plot of land to settle own on.") ]])
    
    -- |[Stablekeepers]|
    elseif(sActorName == "StablekeeperA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Rancher:[VOICE|HarpyRecruit] Sorry, we don't rent or lease out horses.[P] These are for our mounted units.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[VOICE|Sanya] I haven't seen any mounted harpies...[B][C]") ]])
        fnCutscene([[ Append("Rancher:[VOICE|HarpyRecruit] We're building for the future![P] Plus, the horses are fun to hang out with.[P] Helps morale.") ]])
    
    elseif(sActorName == "StablekeeperB") then
    
        --Variables.
        local iHasHarpyForm = VM_GetVar("Root/Variables/Global/Sanya/iHasHarpyForm", "N")
        local iDidHarpyFeet = VM_GetVar("Root/Variables/Chapter2/HarpyBase/iDidHarpyFeet", "N")
    
        --Common.
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
    
        --No harpy form yet:
        if(iHasHarpyForm == 0.0) then
            fnCutscene([[ Append("Rancher:[VOICE|HarpyRecruit] The mountain breeze means the smell isn't too bad!") ]])
    
        --Harpy form, no feet:
        elseif(iHasHarpyForm == 1.0 and iDidHarpyFeet == 0.0) then
            fnCutscene([[ Append("Rancher:[VOICE|HarpyRecruit] H-hello, sister...[P] Um, say, you didn't happen to hear me during the song, did you?[P] You're not having any particularly strong feeling about, um...[B][C]") ]])
            fnCutscene([[ Append("Rancher:[VOICE|HarpyRecruit] Er, n-no, you know what?[P] Never mind, welcome to the flock.[B][C]") ]])
            fnCutscene([[ Append("Rancher:[VOICE|HarpyRecruit] (I guess I need more practice...)") ]])
        
        --Harpy form, yes feet:
        else
            fnCutscene([[ Append("Rancher:[VOICE|HarpyRecruit] Hi, sister![P] You have no idea how much practice it took to perfect my vocal chords so that my little harmony would fold into our song.[P] I could instantly tell from the look in your eyes...[P] and from the smell of your feet, that my plan worked![B][C]") ]])
            fnCutscene([[ Append("Rancher:[VOICE|HarpyRecruit] The rest of the flock are probably going to be really upset at me, I...[P] Uh, I wasn't supposed to do that.[P] But I'm so glad that I did!") ]])
        end
    
    -- |[Clerks]|
    elseif(sActorName == "ClerkA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Clerk:[VOICE|HarpyRecruit] Phew, we've got dispatches from all over the mountains here.[P] I can barely keep up!") ]])
    
    elseif(sActorName == "ClerkB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Clerk:[VOICE|HarpyD] I wish we had some kind of machine that would type for us.[P] Doing all this writing makes my wrist hurt.") ]])

    -- |[Esmerelda]|
    elseif(sActorName == "Esmerelda") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Activate Topics After Dialogue", "Esmerelda") ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Esmerelda", "Neutral") ]])
        fnCutscene([[ Append("Esmerelda:[E|Neutral] Hello![P] What can I do for you?[B][C]") ]])
        fnCutsceneBlocker()
    end
end
