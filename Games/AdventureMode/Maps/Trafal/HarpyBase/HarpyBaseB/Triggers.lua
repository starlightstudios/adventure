-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
if(sObjectName == "MeetEsmerelda") then

    -- |[Repeat Check]|
    local iMetEsmerelda = VM_GetVar("Root/Variables/Chapter2/HarpyBase/iMetEsmerelda", "N")
    if(iMetEsmerelda == 1.0) then return end
    
    -- |[Variables]|
    local iEmpressJoined = VM_GetVar("Root/Variables/Chapter2/MtSarulente/iEmpressJoined", "N")
    
    -- |[Flag]|
    VM_SetVar("Root/Variables/Chapter2/HarpyBase/iMetEsmerelda", "N", 1.0)
    if(iEmpressJoined == 1.0) then
        VM_SetVar("Root/Variables/Chapter2/HarpyBase/iEmpressMetEsmerelda", "N", 1.0)
    end
    
    --Toggle off Esmerelda collision flag.
    fnCutscene([[ TA_ChangeCollisionFlag("Esmerelda", false) ]])
    
    -- |[Items]|
    --Player gets a Wing Badge.
    LM_ExecuteScript(gsItemListing, "Wing Badge")

    -- |[Folding]|
    fnCutsceneMergeParty()
    fnCutsceneBlocker()
    fnCutsceneMove("Sanya", 42.25, 33.50)
    fnCutsceneFace("Sanya", 0, 1)
    fnCutsceneMove("Izuna", 41.25, 33.50)
    fnCutsceneFace("Izuna", 0, 1)
    fnCutsceneMove("Zeke",  43.25, 33.50)
    fnCutsceneFace("Zeke", 0, 1)
    if(iEmpressJoined == 1.0) then
        fnCutsceneMove("Empress", 42.25, 32.50)
        fnCutsceneFace("Empress", 0, 1)
    end

    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|Esmerelda] She's coming this way?[P] Why am I just getting this dispatch now?[P] She's probably at the front gate already.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneFace("ClerkA", 0, -1)
    fnCutsceneFace("Esmerelda", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(85)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|Esmerelda] I should have expected this.[P] Back to work, then.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneMove("ClerkA", 39.25, 36.50)
    fnCutsceneFace("ClerkA", 0, -1)
    fnCutsceneMove("Esmerelda", 42.25, 34.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Esmerelda", "Neutral") ]])
    fnCutscene([[ Append("Harpy:[E|Neutral] I see you've found your way to my headquarters.[P] It's lovely to meet you.[P] I am - [P][CLEAR]") ]])
    fnCutscene([[ Append("Sanya:[E|Offended] Zeke![P] It's go time![P] Do it![B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] BAH!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Action!]|
    --Jump A.
    fnCutscenePlaySound("World|Jump")
    fnCutsceneFace("Zeke", 0, 1)
    fnCutsceneJumpTo("Zeke", 41.25, 34.50, 25)
    fnCutsceneBlocker()
    fnCutsceneFace("Esmerelda", -1, 0)
    fnCutsceneFace("Zeke", 1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Zeke:[VOICE|Zeke] Bah!") ]])
    fnCutsceneBlocker()

    --Jump B.
    fnCutscenePlaySound("World|Jump")
    fnCutsceneFace("Zeke", 1, 0)
    fnCutsceneJumpTo("Zeke", 43.25, 34.50, 25)
    fnCutsceneBlocker()
    fnCutsceneFace("Esmerelda", 1, 0)
    fnCutsceneFace("Zeke", -1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Zeke:[VOICE|Zeke] Bah!") ]])
    fnCutsceneBlocker()

    --Jump C.
    fnCutscenePlaySound("World|Jump")
    fnCutsceneFace("Zeke", 0, 1)
    fnCutsceneJumpTo("Zeke", 42.25, 35.50, 25)
    fnCutsceneBlocker()
    fnCutsceneFace("Esmerelda", 0, 1)
    fnCutsceneFace("Zeke", 0, -1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Spin.
    local iTPF = 3
    for i = 1, 5, 1 do
        fnCutsceneFace("Zeke", 1, 0)
        fnCutsceneWait(iTPF)
        fnCutsceneBlocker()
        fnCutsceneFace("Zeke", 0, 1)
        fnCutsceneWait(iTPF)
        fnCutsceneBlocker()
        fnCutsceneFace("Zeke", -1, 0)
        fnCutsceneWait(iTPF)
        fnCutsceneBlocker()
        fnCutsceneFace("Zeke", 0, -1)
        fnCutsceneWait(iTPF)
        fnCutsceneBlocker()
    end
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Zeke:[VOICE|Zeke] NYEH!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(225)
    fnCutsceneBlocker()
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|Esmerelda] .[P].[P].[P]?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Esmerelda", "Neutral") ]])
    fnCutscene([[ Append("Sanya:[E|Happy] Hot damn, Zeke says you're all right![B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] Nyeh.[B][C]") ]])
    fnCutscene([[ Append("Harpy:[E|Neutral] Is this some sort of ritual from Earth?[P] Should I bow?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] Nah.[P] Goats can detect bad vibes.[P] They just don't normally tell people about them.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Happy] Zeke graciously offers us his limitless wisdom.[B][C]") ]])
    fnCutscene([[ Append("Harpy:[E|Neutral] I...[P] see?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] Excuse me, how did you know Sanya is from Earth?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Happy] ...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Offended] Yeah![P] How did you know that![B][C]") ]])
    if(iEmpressJoined == 0.0) then
        fnCutscene([[ Append("Harpy:[E|Neutral] I know all about your runestone, your incredible strength, and yes, that you are from Earth.[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Neutral] Just who are you?[B][C]") ]])
    else
        fnCutscene([[ Append("Harpy:[E|Neutral] I know all about your runestone, your incredible strength, and yes, that you are from Earth.[P] And you, towering dragon, are Empress Arulente.[B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] That saves us some introductions.[P] And you are?[B][C]") ]])
    end
    fnCutscene([[ Append("Esmerelda:[E|Neutral] Esmerelda de Artezzi.[P] Well, formerly.[P] It's a long story.[P] I'm the leader of the harpy brigade here.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Jot] Ah, I see.[P] So you've been spying on Sanya![B][C]") ]])
    fnCutscene([[ Append("Esmerelda:[E|Neutral] Something like that.[P] A little while ago, one of my scouts came in with a cracked beak.[P] She said a human had punched her in the face.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Surprised] I was defending myself![B][C]") ]])
    fnCutscene([[ Append("Esmerelda:[E|Neutral] After being knocked down, the human then called her a 'nerd'.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Surprised] I was trying to scare her away![B][C]") ]])
    fnCutscene([[ Append("Esmerelda:[E|Neutral] Since we don't have that insult here, she explained she was from Earth, where 'nerd' is the worst possible insult.[B][C]") ]])
    fnCutscene([[ Append("Esmerelda:[E|Neutral] She then danced around the scout, shouting the insult over and over.[P] The scout started crying.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Sad] Okay maybe I went a bit overboard...[B][C]") ]])
    if(iEmpressJoined == 1.0) then
        fnCutscene([[ Append("Empress:[E|Neutral] A bit?[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Sad] Okay maybe I went a [P]*lot*[P] overboard...[B][C]") ]])
    end
    fnCutscene([[ Append("Esmerelda:[E|Neutral] We've been keeping an eye on you since then.[P] Don't want any more cracked beaks.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Ugh] That explains why the harpies have been attacking us on the roads.[P] Why didn't you tell me about this?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Happy] I completely forgot![B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Ugh] Really?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Yeah.[P] The next day, you woke up, so it was kind of a blur.[P] I got distracted![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Sad] Tell the harpy I punched that I'm sorry, okay?[B][C]") ]])
    fnCutscene([[ Append("Esmerelda:[E|Neutral] I'll pass the message along.[P] In fact, I wanted you to come here.[P] Not just for that, but to ask you to join us.[B][C]") ]])
    fnCutscene([[ Append("Esmerelda:[E|Neutral] We know about your transformative abilities, and your runestone.[P] If the scout reports are true, you're the hero of Trafal legend, aren't you?[B][C]") ]])
    if(iEmpressJoined == 1.0) then
        fnCutscene([[ Append("Empress:[E|Neutral] And just what do you know of that legend?[B][C]") ]])
        fnCutscene([[ Append("Esmerelda:[E|Neutral] It has percolated far beyond the borders of Trafal, Empress.[P] Why, even today, someone claiming to be the inheritor of your legacy has conquered my home city of Jeffespeir.[B][C]") ]])
        fnCutscene([[ Append("Esmerelda:[E|Neutral] Many harpies in our flock know of it.[P] The hero will be incredibly strong, from another world called Earth, able to transform.[B][C]") ]])
        fnCutscene([[ Append("Esmerelda:[E|Neutral] How am I doing?[B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] Well enough.[P] What interest do you have in Sanya, if she is this hero?[P] Why should she join your army?[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] I'm not saying no to another form.[B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] The harpy transformation comes with more than just that, doesn't it?[B][C]") ]])
        fnCutscene([[ Append("Esmerelda:[E|Neutral] For your first question, we have the same goal.[P] The harpies are here to protect Trafal, and the world.[B][C]") ]])
        fnCutscene([[ Append("Esmerelda:[E|Neutral] For the second, yes.[P] Our song will carry with it the knowledge of our culture.[P] Sanya will become one of us.[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Jot] (Magic song...[P] knowledge of culture...[P] she speaks way too fast!)[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] What do you mean, become one of you?[P] Other than, like, literally.[B][C]") ]])
        fnCutscene([[ Append("Esmerelda:[E|Neutral] Among our people, we have certain customs.[P] Holidays, legends, even ways of greeting one another.[P] You will learn all of those as though you had been born with them.[P] We call it 'imprinting'.[B][C]") ]])
        fnCutscene([[ Append("Esmerelda:[E|Neutral] We harpies are of a single feather.[P] That is why I organized my flock into an army.[B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] To unite Trafal under your wings.[B][C]") ]])
        fnCutscene([[ Append("Esmerelda:[E|Neutral] Not just Trafal, the whole world![B][C]") ]])
        fnCutscene([[ Append("Esmerelda:[E|Neutral] People fight one another over pointless cultural squabbles, or to steal one another's resources.[P] We harpies never fight our flock![B][C]") ]])
        fnCutscene([[ Append("Esmerelda:[E|Neutral] We are one great big family, we are all on the same team.[B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] Because of imprinting.[B][C]") ]])
        fnCutscene([[ Append("Esmerelda:[E|Neutral] Precisely![B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] That works for your army.[P] Will it work for a continent?[P] Will a million subjects find themselves of one mind?[P] Won't divisions form?[B][C]") ]])
        fnCutscene([[ Append("Esmerelda:[E|Neutral] Of course they will.[P] People still fight over such things.[P] Even harpy flocks fight one another, just not within the same flock.[B][C]") ]])
        fnCutscene([[ Append("Esmerelda:[E|Neutral] To hold the entire world together in peace is the most difficult task.[P] I merely think we harpies have an advantage, where another might not.[B][C]") ]])
        fnCutscene([[ Append("Esmerelda:[E|Neutral] After all, your empire had another advantage, did it not?[B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] ...[P] You have studied my writings.[B][C]") ]])
        fnCutscene([[ Append("Esmerelda:[E|Neutral] Your legacy is your education system, Empress.[P] I was well schooled.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] Not to interrupt, but what was Empress' big advantage?[B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] Succession.[P] We dragons live a long time.[P] A monarch having a poor successor is a common reason for a kingdom to collapse.[B][C]") ]])
        fnCutscene([[ Append("Esmerelda:[E|Neutral] That is a problem we harpies will need to contend with.[P] Still, I hope my empire can overcome it.[B][C]") ]])
        fnCutscene([[ Append("Esmerelda:[E|Neutral] Sanya, having a powerful ally like you will go a long way to helping us.[P] We can work together to stop Perfect Hatred![B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Jot] (Well read... [P]cognizant of the future...[P] This is a real scoop!)[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Sad] I'm going to consider it.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Sad] What you said about becoming one of you...[P] made me homesick all of the sudden.[B][C]") ]])
    else
        fnCutscene([[ Append("Izuna:[E|Happy] She certainly is![B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] Izuna's really happy about it.[P] I'm just here to kill the ultimate evil, or something.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] I'm a little suspicious that you're asking me to join up after giving a vivid description of an administered beatdown.[B][C]") ]])
        fnCutscene([[ Append("Esmerelda:[E|Neutral] ...[P] Do you know what an army does?[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Blush] Fair enough.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Smirk] What I mean is, how do you know you can trust someone like me?[B][C]") ]])
        fnCutscene([[ Append("Esmerelda:[E|Neutral] Because you and I have the same goal.[P] We want to protect Trafal from the evil that stirs.[P] In the hearts of men, or in the form of a monster, it makes no difference to us.[B][C]") ]])
        fnCutscene([[ Append("Esmerelda:[E|Neutral] We harpies can imprint our culture on our new hatchlings.[P] We come together as one family, one flock.[P] I hope to use this power to bring the world together in peace.[B][C]") ]])
        fnCutscene([[ Append("Esmerelda:[E|Neutral] I hope you agree with my goals and want to aid them.[P] Imagine it, a continent, a world free of wars![B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Sad] Yeah, that does sound great...[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Jot] (Continent free of war...[P] gonna need to ask some follow-ups...)[B][C]") ]])
        fnCutscene([[ Append("Esmerelda:[E|Neutral] You don't look happy about that.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Sad] To gain someone else's culture...[P] I need to think about it.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Sad] I just got really homesick all of a sudden.[B][C]") ]])
    end
    fnCutscene([[ Append("Esmerelda:[E|Neutral] I understand.[P] Even if you choose not to, we could still use your help.[P] There is an army of bandits to the south, and they have been drawing ever closer to Trafal.[B][C]") ]])
    fnCutscene([[ Append("Esmerelda:[E|Neutral] But I've kept you long enough.[P] Please let me know right away if you want to accept my offer, or any of the other tasks we need help with.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] Could you maybe tell your harpies to leave us alone on the trails?[B][C]") ]])
    fnCutscene([[ Append("Esmerelda:[E|Neutral] Hm.[P] While I would normally say no, as they would attack you as a bandit...[B][C]") ]])
    fnCutscene([[ Append("Esmerelda:[E|Neutral] [SOUND|World|TakeItem]Take this badge.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] Oh, what cute little wings![B][C]") ]])
    fnCutscene([[ Append("Esmerelda:[E|Neutral] If you wear this badge, my troops will know you're a friend.[P] I don't give that out to just anyone, so don't lose it.[B][C]") ]])
    if(iEmpressJoined == 1.0) then
        fnCutscene([[ Append("Esmerelda:[E|Neutral] Take it as a mark of my sincerity.[P] And Empress?[P] If you have more time to converse, I'd love to.[P] I could use your advice![B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] Assuming I see in you the potential of an empress, well, it is up to Sanya if we have time.[B][C]") ]])
        fnCutscene([[ Append("Esmerelda:[E|Neutral] Very well.[P] Good luck on your journey, heroes.") ]])
    else
        fnCutscene([[ Append("Sanya:[E|Neutral] Bitchin'.[B][C]") ]])
        fnCutscene([[ Append("Esmerelda:[E|Neutral] Good luck on your journey, heroes.") ]])
    end
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    -- |[Movement]|
    fnCutsceneMove("Esmerelda", 42.25, 40.50)
    fnCutsceneMove("Esmerelda", 40.25, 40.50)
    fnCutsceneFace("Esmerelda", 0, 1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscenePlaySound("World|FlipSwitch")
    fnCutscene([[ AL_SetProperty("Open Door", "Office Door") ]])
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneMove("Esmerelda", 40.25, 43.50)
    fnCutsceneMove("Esmerelda", 38.25, 43.50)
    fnCutsceneFace("Esmerelda", 0, -1)
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("Izuna:[E|Cry] Why are you feeling homesick, sweetie?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Sad] When she said I'd become one of them, it made me suddenly realize something.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Sad] I'm a Croat, and there aren't any others here...[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Explain] What about Zeke?[B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] Bah![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] Heh.[P] Thanks, buddy.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Sad] But it hit me that I'm really all alone over here.[P] What if I never see my family again?[P] Will my identity as a Croat, a European, an Earthling survive?[B][C]") ]])
    if(iEmpressJoined == 1.0) then
        fnCutscene([[ Append("Empress:[E|Neutral] You had no such compunction about becoming a Sevavi.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Sad] That was different.[P] My body just turned to stone.[P] It wasn't like I was becoming someone else.[B][C]") ]])
    end
    fnCutscene([[ Append("Izuna:[E|Neutral] Maybe you aren't as lonely as you think, Sanya.[P] When I became a kitsune, I had to make a choice like that.[P] To become a new person.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] We often took new names, learned a new culture, studied a new religion, to become kitsunes.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Explain] I am happy to report that I survived the process![B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] Bah![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] That helps.[P] Okay.[P] I don't feel so bad about it.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] It's ultimately your choice.[P] Don't let anyone pressure you into it.[B][C]") ]])
    if(iEmpressJoined == 1.0) then
        fnCutscene([[ Append("Empress:[E|Neutral] My opinion differs from Izuna's.[P] You should not see this as some sort of unwanted change.[P] It is merely a change.[B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] You are not the same person as you were when you came here.[P] Do not lament some alternate lost history where you stayed on Earth.[B][C]") ]])
        fnCutscene([[ Append("Empress:[E|Neutral] Change cannot be prevented.[P] You instead choose what you will do with the change that is thrust upon you.[P] Life, itself, is coping.[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Jot] (That's a good line..)[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] Okay, this is getting way too heavy.[P] Let's get going.") ]])
    else
        fnCutscene([[ Append("Sanya:[E|Neutral] Yeah, yeah.[P] Let's get going.") ]])
    end
    fnCutsceneBlocker()

    -- |[Clean Up]|
    fnCutscene([[ TA_ChangeCollisionFlag("Esmerelda", true) ]])
    fnAutoFoldParty()
    fnCutsceneBlocker()

end
