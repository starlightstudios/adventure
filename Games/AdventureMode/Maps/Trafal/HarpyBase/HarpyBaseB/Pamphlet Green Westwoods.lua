-- |[ =============================== Pamphlet: Green Westwoods =============================== ]|
--Showing the Nine Tails Post to an NPC.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sActorName = LM_GetScriptArgument(0)

-- |[Push Entity]|
--In order to change the facing to match the player, the entity must be pushed.
EM_PushEntity(sActorName)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ==== Minor Characters ==== ]|
-- |[HumanA]|
if(sActorName == "HumanA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Recruit:[VOICE|HumanM1] *The* westwoods?[P] The hellish mess of twisted trees and scorched earth?[P] That's greening now?[P] Amazing!") ]])

-- |[HumanB]|
elseif(sActorName == "HumanB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Recruit:[VOICE|HumanM0] Oh, I can't wait to be a harpy![P] I'd love to help the alraunes heal the earth!") ]])

-- |[Guard]|
elseif(sActorName == "Guard") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HarpyC] I'm sorry but I won't be believing this about a green Westwoods until I see it myself.[P] I can't believe I actually want patrol duty there now.") ]])

-- |[WatchA]|
elseif(sActorName == "WatchA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HarpyD] A green Westwoods?[P] Can you imagine it?[P] I might actually want to take a picnic there!") ]])

-- |[WatchB]|
elseif(sActorName == "WatchB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Harpy:[VOICE|HarpyRecruit] Whatever those alraunes are doing, tell them to do it faster. [P]I hate the cauldron!") ]])

-- |[Bunny and Officer]|
elseif(sActorName == "BunnyA" or sActorName == "Officer") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Izuna:[VOICE|Izuna] (These two are too busy talking to notice...)") ]])

-- |[Quartermaster]|
elseif(sActorName == "Quartermaster") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Quartermaster:[VOICE|KitsuneC] Sorry, I'm on duty.[P] I'll take a look at the pamphlet later, though.") ]])

-- |[Alraunes]|
elseif(sActorName == "AlrauneA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Bartender:[VOICE|Alraune] Hah![P] If they want to waste their lives fixing that dump, let them.[P] World's too big to worry about every little ones' feelings.") ]])

elseif(sActorName == "AlrauneB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Gardener:[VOICE|Alraune] My goodness![P] I should go pay my respects to the wise grandfather![P] But it's so dangerous there...") ]])

-- |[Drinker]|
elseif(sActorName == "Drinker") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Soldier:[VOICE|HarpyD] Do I look like I want to read a pamphlet right now?") ]])

-- |[Raiju]|
elseif(sActorName == "RaijuA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Raiju:[VOICE|HumanF0] Ha ha![P] Reading is not why I do what I do, sweetie.") ]])

-- |[Cleaners]|
elseif(sActorName == "CleanerA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Cleaner:[VOICE|HumanF1] I knew the alraunes could do amazing things, but healing Westwoods?[P] Incredible.") ]])

elseif(sActorName == "CleanerB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Cleaner:[VOICE|HumanM1] It's a miracle that there's anything of Westwoods to save.") ]])

-- |[Trainees]|
elseif(sActorName == "TraineeA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Trainee:[VOICE|HarpyRecruit] Westwoods?[P] Green?[P] Hm, I thought that was just the lake leaking over.[P] I should check that out next flight.") ]])

elseif(sActorName == "TraineeB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Trainee:[VOICE|HarpyOfficer] I kind of like flying over Westwoods.[P] It's hotter than the other areas, the thermals are more navigable.") ]])

elseif(sActorName == "TraineeC") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Trainee:[VOICE|HarpyC] Ugh, no pamphlets right now.[P] I gotta focus on my next flight.") ]])

elseif(sActorName == "TraineeD") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Trainee:[VOICE|HarpyRecruit] Maybe we can form an alliance with the alraunes?[P] We have a similar goal.") ]])

elseif(sActorName == "FlightMaster") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Officer:[VOICE|HarpyD] Yes![P] Citizens taking charge of their destiny![P] I like these alraunes!") ]])

-- |[Farmer]|
elseif(sActorName == "FarmerA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Gardener:[VOICE|HarpyC] Purifying the water?[P] Really?[P] Maybe I can get an assignment to help them plant!") ]])

-- |[Stablekeepers]|
elseif(sActorName == "StablekeeperA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Rancher:[VOICE|HarpyRecruit] I wonder if the grass in Westwoods will be edible for the mounts, now!") ]])

elseif(sActorName == "StablekeeperB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Rancher:[VOICE|HarpyRecruit] The horses absolutely hate Westwoods.[P] I hope the alraunes succeed.") ]])

-- |[Clerks]|
elseif(sActorName == "ClerkA") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Clerk:[VOICE|HarpyRecruit] Oh, I've seen all the pamphlets.[P] You're doing great by keeping the people informed!") ]])

elseif(sActorName == "ClerkB") then
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Clerk:[VOICE|HarpyD] You can't fool me, you're the one who wrote those pamphlets.[P] Superb work![P] Don't be so modest.") ]])

-- |[ ==== Major Characters ==== ]|
-- |[Esmerelda]|
elseif(sActorName == "Esmerelda") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Esmerelda", "Neutral") ]])
    fnCutscene([[ Append("Esmerelda:[E|Neutral] So that's what the scouts were talking about.[P] I got some reports of green areas and pure water, but the scouts didn't want to land in case it was a trick.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] It's not a trick![P] You should send someone over, perhaps form an alliance![P] I know the kitsunes will, soon.[B][C]") ]])
    fnCutscene([[ Append("Esmerelda:[E|Neutral] Maybe we can really help.[P] I know the alraunes won't like the bandits.[P] Good idea.") ]])
end

-- |[Clean Up]|
DL_PopActiveObject()
