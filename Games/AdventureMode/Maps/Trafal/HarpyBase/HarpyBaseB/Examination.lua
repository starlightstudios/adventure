-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= Exits ========================================== ]|
if(sObjectName == "SampleExit") then
    
-- |[ ====================================== Examinables ======================================= ]|
elseif(sObjectName == "SignW") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Supply Office')") ]])

elseif(sObjectName == "SignN") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('No Practice Takeoffs Without Instructor Supervision')") ]])

elseif(sObjectName == "SignS") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Advanced Takeoff Course'.[P] It'd be awesome if they just put a rake on the runway.)") ]])

elseif(sObjectName == "WaterBarrels") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Barrels full of precious water.)") ]])

elseif(sObjectName == "WineBarrels") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Some strong-smelling alcohol.[P] I could use a hit...)") ]])

elseif(sObjectName == "OpenBarrel") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (An open barrel, smells like schnapps.)") ]])

elseif(sObjectName == "Seeds") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A bag full of seeds.[P] I guess that's trail-mix to a harpy.)") ]])

elseif(sObjectName == "Oats") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Zeke is salivating just looking at all the oats...)") ]])

elseif(sObjectName == "Horse") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (This horse is a real winner, I can tell by his big, strong legs.[P] Teeth look good, too.[P] You got good stablehands, boy.)") ]])

elseif(sObjectName == "FoodShelfA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Delicious cheeses![P] Salted pork![P] Pickled... celery?[P] Okay...)") ]])
    
elseif(sObjectName == "FoodShelfB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (There's a sausage just split open, sitting there on the wood.[P] Not very sanitary.)") ]])
    
elseif(sObjectName == "FoodShelfC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The milk isn't refrigerated, but it's not spoiling.[P] Must be magic!)") ]])
    
elseif(sObjectName == "FoodShelfD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Oranges, pineapples, and tangerines?[P] Must be imported.)") ]])
    
elseif(sObjectName == "BookshelfA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (In between the books are slips of paper indicating someone borrowed one.)") ]])
    
elseif(sObjectName == "BookshelfB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('PSYCHO SEX KILLER'.[P] I guess it's the harpy equivalent of an airport novel.[P] It's stupid, but very to-the-point.[P] I like the part where the psycho sex-kills someone.)") ]])
    
elseif(sObjectName == "BookshelfC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('The Art of Leadership', by... Empress S. Arulente.[P] Snore!)") ]])
    
elseif(sObjectName == "BookshelfD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('History of Trafal' by some dude, who cares.)") ]])

elseif(sObjectName == "BookshelfE") then
    AL_SetProperty("Set Layer Disabled", "SkillbookIco", true)
	LM_ExecuteScript(gsSanyaSkillbook, 0)
    
elseif(sObjectName == "Papers") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Papers that look like intelligence reports.[P] Actually reading one is a good way to get my ass kicked by people who don't like spies.)") ]])

elseif(sObjectName == "Telegraph") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (This looks like a telegraph I saw in a museum once.[P] Looks like it's made of brass.)") ]])

-- |[ ========================================== Other ========================================= ]|
-- |[ ========================================== Debug ========================================= ]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
