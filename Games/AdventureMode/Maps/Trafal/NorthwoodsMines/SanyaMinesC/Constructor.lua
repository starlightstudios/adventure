-- |[ ======================================= Constructor ====================================== ]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = fnResolveDirectory() --Map's name is the name of the folder it is in.
local sLevelMusic = "TrafalCave"
local sMapResolveName = "Map Resolve Name"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[ ======================================== Standard ======================================== ]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

    -- |[Construction]|
	--Music.
	AL_SetProperty("Music", sLevelMusic)
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation(sMapResolveName)

	-- |[Lights]|
    AL_SetProperty("Activate Lights")
    
    -- |[Shooting Setup]|
    AL_SetProperty("Shooting Depth", 2)

-- |[ ======================================== Post-Exec ======================================= ]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	-- |[NPC Spawning]|
	--If an entity needs to spawn after a post-exec cutscene, put them here.
    fnSpawnNPCPattern("Block", "A", "C")
    fnSpawnNPCPattern("Switch", "A", "C")

    -- |[Overlays]|
    --Place overlay code here.
    
    -- |[Skillbooks]|
    --Place skillbook overlay code here.
    
    -- |[Puzzles]|
    local iMinesCSolved = VM_GetVar("Root/Variables/Chapter2/SanyaMines/iMinesCSolved", "N")
    if(iMinesCSolved == 1.0) then
        fnCutsceneTeleport("BlockA", 40.25, 13.50)
        fnCutsceneTeleport("BlockB", 15.25,  9.50)
        fnCutsceneTeleport("BlockC", 28.25, 24.50)
    else
        AL_SetProperty("Set Layer Disabled", "BridgeA", true)
        AL_SetProperty("Set Layer Disabled", "BridgeB", true)
        AL_SetProperty("Set Collision", 38, 21, 0, 1)
        AL_SetProperty("Set Collision", 39, 21, 0, 1)
        AL_SetProperty("Set Collision", 40, 21, 0, 1)
        AL_SetProperty("Set Collision", 38, 22, 0, 1)
        AL_SetProperty("Set Collision", 39, 22, 0, 1)
        AL_SetProperty("Set Collision", 40, 22, 0, 1)
        AL_SetProperty("Set Collision", 38, 23, 0, 1)
        AL_SetProperty("Set Collision", 39, 23, 0, 1)
        AL_SetProperty("Set Collision", 40, 23, 0, 1)
    end
    
    -- |[Rifle Handling]|
    VM_SetVar("Root/Variables/Global/Sanya/iRifleCode", "N", gciSanyaRifle_CanFire)

end
