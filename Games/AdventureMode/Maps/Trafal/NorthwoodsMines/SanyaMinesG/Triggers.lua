-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
if(sObjectName == "PuzzleFight") then

    -- |[Repeat Check]|
    local iDidPuzzleFight = VM_GetVar("Root/Variables/Chapter2/SanyaMines/iDidPuzzleFight", "N")
    if(iDidPuzzleFight == 1.0) then return end
    
    -- |[Variables]|
    local sSanyaForm = VM_GetVar("Root/Variables/Global/Sanya/sForm", "S")

    -- |[Flag]|
    VM_SetVar("Root/Variables/Chapter2/SanyaMines/iDidPuzzleFight", "N", 1.0)
    VM_SetVar("Root/Variables/Global/Sanya/iRifleCode", "N", gciSanyaRifle_CanFire)

    -- |[Spawn Odar]|
    fnSpecialCharacter("Odar", 10, 25, gci_Face_North, false, nil)
    fnCutsceneTeleport("Odar", 10.25, 19.50)

    -- |[Inventory Handling]|
    --Return Sanya's weapons and ammo.
    LM_ExecuteScript(gsItemListing, "Enfield Rifle")
    LM_ExecuteScript(gsItemListing, "7.62x56mmR Rounds")

    --Remove Sanya's equipment.
    AdvCombat_SetProperty("Push Party Member", "Sanya")
        AdvCombatEntity_SetProperty("Equip Item To Slot", "Weapon", "Enfield Rifle")
        AdvCombatEntity_SetProperty("Equip Item To Slot", "Ammo", "7.62x56mmR Rounds")
    DL_PopActiveObject()

    --Remove Sanya's placeholder items.
    AdInv_SetProperty("Remove Item", "Bare Hands")
    AdInv_SetProperty("Remove Item", "Gloved Hands")
    AdInv_SetProperty("Remove Item", "Anger")
    --[REMOVECHAPTER2] --Only gloved hands needs to be removed, old save files with Bare Hands should be phased out by the time the chapter is done

    --Doctor Bag.
    gbAutoSetDoctorBagCurrentValues = true
    LM_ExecuteScript(gsComputeDoctorBagTotalPath)
    
    -- |[Cutscene Skip]|
    if(false) then
        
        --Remove Sanya's equipment. Switch to her rifle setup.
        fnCutsceneLayerDisabled("Gear", true)
        LM_ExecuteScript(gsRoot .. "CostumeHandlers/Sanya/Human_Rifle.lua")
        LM_ExecuteScript(gsRoot .. "CostumeHandlers/Sanya/Sevavi_Rifle.lua")
        fnCutsceneTeleport("Sanya", 10.75, 12.00)
        
        --Activate the minigame.
        fnCutscene([[ AL_SetProperty("Music", "PuzzleFight") ]])
        VM_SetVar("Root/Variables/Global/PuzzleFight/iLocation", "N", gci_PuzzleFightLocation_SanyaMinesG)
        fnCutscene([[ AL_SetProperty("Activate Puzzle Fight Minigame") ]])
    
    -- |[Cutscene Play]|
    else

        --Movement.
        fnCutsceneMove("Sanya", 10.75, 7.50)
        fnCutsceneBlocker()
        fnCutsceneWait(65)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Sanya:[VOICE|Sanya] Is that...?") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneMove("Sanya", 10.25, 11.50, 2.00)
        fnCutsceneBlocker()
        fnCutsceneWait(65)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Sanya:[VOICE|Sanya] Gun, lighter, first aid kit![P] My gear, it's all here![P] Woohoo![B][C]") ]])
        fnCutscene([[ Append("Sanya:[VOICE|Sanya] Hopefully I can at least clean that girl's wounds and dad won't bite my head off for losing great grandpa's rifle![B][C]") ]])
        fnCutscene([[ Append("Sanya:[VOICE|Sanya] Seems things are finally looking up around here!") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Take the items.
        fnCutsceneLayerDisabled("Gear", true)
        fnCutscene([[ LM_ExecuteScript(gsRoot .. "CostumeHandlers/Sanya/Human_Rifle.lua") ]])
        fnCutscene([[ LM_ExecuteScript(gsRoot .. "CostumeHandlers/Sanya/Sevavi_Rifle.lua") ]])
        fnCutscene([[ AudioManager_PlaySound("World|TakeItem") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(125)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("???:[VOICE|Odar] Superbly well done, my friend.[P] Superb indeed.[B][C]") ]])
        fnCutscene([[ Append("???:[VOICE|Odar] You have masterfully walked into my elaborate trap, and fallen for my ruse.[P] But I would expect nothing less!") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneMove("Odar", 10.75, 15.50)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Odar", "Neutral") ]])
        fnCutscene([[ Append("Sanya:[E|Offended] Look pal, you're starting to sound bizarrely hostile for someone who just fished all my stuff out of an underground river and delivered it to me.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Offended] Am I going to have to kick your ass?[P] I don't want to and you seem like a nice guy.[B][C]") ]])
        fnCutscene([[ Append("Odar:[E|Neutral] Fool![P] Do you not know who I am?[P] I am Prince Odar![B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] ...[B][C]") ]])
        fnCutscene([[ Append("Odar:[E|Neutral] Your nemesis![B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Smirk] Really?[B][C]") ]])
        fnCutscene([[ Append("Odar:[E|Neutral] We've fought before![B][C]") ]])
        fnCutscene([[ Append("Odar:[E|Neutral] Granted it was in the dark, but I'm sure it was you.[P] I recognize your aura.[P] Identical![B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Smirk] (Is that like perfume?[P] Who the fuck can afford perfume in this economy?)[B][C]") ]])
        fnCutscene([[ Append("Odar:[E|Neutral] It was right after one of your brutal hunts.[P] Surely you remember?[B][C]") ]])
        if(sSanyaForm == "Sevavi") then
            fnCutscene([[ Append("Odar:[E|Neutral] (Was she stone last we fought?[P] Maybe that's a recent development.)[B][C]") ]])
        end
        fnCutscene([[ Append("Sanya:[E|Blush] Hey chucklenuts, all my hunts are absolutely brutal, so you need to get more specific.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] But as much as I'd love to discuss my many conquests, there's a girl who - [P][CLEAR]") ]])
        fnCutscene([[ Append("Odar:[E|Neutral] And this shall be our final meeting, foolish hunter![B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Laugh] Thank goodness, because I don't think I could stand much more of you.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Blush] So how about this, 'Prince'.[P] I'll show you what it looks like when doves cry.[B][C]") ]])
        fnCutscene([[ Append("Odar:[E|Neutral] [P][P]Excuse me?[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] Sheesh, you don't know who Prince is?[P] Yeah I have to be on another planet.[B][C]") ]])
        fnCutscene([[ Append("Odar:[E|Neutral] Shut up![P] You talk way too much![P] Shut up and let's fight already![B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Offended] Fine by me![P] I just got my gun back![B][C]") ]])
        fnCutscene([[ Append("Odar:[E|Neutral] Well that's -[P] no, you dolt![P] You stepped on my trap![P] You know how this works, everyone does![B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Offended] How what works, jackass![B][C]") ]])
        fnCutscene([[ Append("Odar:[E|Neutral] Do you not realize you've accepted my duel by stepping into the center of this arena?[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Sad] ...[P] I knew that![P] Obviously![P] I was just distracted!") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneFace("Sanya", -1, 0)
        fnCutsceneBlocker()
        fnCutsceneWait(45)
        fnCutsceneBlocker()
        fnCutsceneFace("Sanya", 1, 0)
        fnCutsceneBlocker()
        fnCutsceneWait(45)
        fnCutsceneBlocker()
        fnCutsceneFace("Sanya", 0, -1)
        fnCutsceneBlocker()
        fnCutsceneWait(45)
        fnCutsceneBlocker()
        fnCutsceneFace("Sanya", 0, 1)
        fnCutsceneBlocker()
        fnCutsceneWait(45)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Odar", "Neutral") ]])
        fnCutscene([[ Append("Sanya:[E|Sad] (Of course even fighting would involve weird puzzle shit here![P] Everything is puzzles![P] I hate this place so much!)[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Sad] So ummm...[P] I might need a quick refresher because it's been so long since I did one of these.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] Are you sure I can't just punch you and be done with it?[B][C]") ]])
        fnCutscene([[ Append("Odar:[E|Neutral] No, it is said the Fox Goddess herself curses those who cheat at her game.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] And like, that's real?[P] Because magic is clearly real here, right?[P] You look pretty magical.[B][C]") ]])
        fnCutscene([[ Append("Odar:[E|Neutral] (This has to be the hunter.[P] The gun, the clothes.[P] It has to be![P] This is all a trick!)[B][C]") ]])
        fnCutscene([[ Append("Odar:[E|Neutral] Yes it's real, damn it![B][C]") ]])
        fnCutscene([[ Append("Odar:[E|Neutral] Ahem, it would not be sporting of me, hunter, to put you at a disadvantage.[P] So I shall explain the rules of the game.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Blush] Which I definitely know.[B][C]") ]])
        fnCutscene([[ Append("Odar:[E|Neutral] Correct, you clearly know them and just need a refresher.[P] This is a duel amongst equals, after all.[P] No unfair advantages.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] Yeah, that's it.[B][C]") ]])
        fnCutscene([[ Append("Odar:[E|Neutral] Very well!") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Activate the minigame.
        fnCutscene([[ AL_SetProperty("Music", "PuzzleFight") ]])
        VM_SetVar("Root/Variables/Global/PuzzleFight/iLocation", "N", gci_PuzzleFightLocation_SanyaMinesG)
        fnCutscene([[ AL_SetProperty("Activate Puzzle Fight Minigame") ]])
    end

--Izuna Explains
elseif(sObjectName == "Explainer") then

    -- |[Repeat Check]|
    local iExplainedPuzzleBattle = VM_GetVar("Root/Variables/Chapter2/IzunaExplains/iExplainedPuzzleBattle", "N")
    if(iExplainedPuzzleBattle == 1.0) then return end
    
    -- |[Activation Check]|
    local iCompletedHunt = VM_GetVar("Root/Variables/Chapter2/Northwoods/iCompletedHunt", "N")
    if(iCompletedHunt == 0.0) then return end
    
    -- |[Flags]|
    VM_SetVar("Root/Variables/Chapter2/IzunaExplains/iExplainedPuzzleBattle", "N", 1.0)

    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("Izuna:[E|Explain] Oh![P] The Goddess' Duel Board![P] I didn't know there was one here![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] These things have a name?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Explain] Of course.[P] This is where civilized followers of the Goddess settle their differences without killing each other.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] Injuries notwithstanding.[P] Be sure to have a miko on hand.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Explain] The Goddess herself oversees the contest, you know.[P] If you cheat, or do not follow through, you will be cursed.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] I'm familiar with the rules.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Offended] Stupid mushroom man...[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] Mushroom man?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] Not important.[P] Point is, I've done these before.[P] Is the curse a real thing?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] Yes, it's very real.[P] I can't even treat it with magic.[P] It saps your strength, makes you listless.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Sad] It is real?[P] Can it be cured?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] Yes, by doing what you were required to by the one who won the duel, or begging for their forgiveness.[P] That, or the Goddess herself forgives you.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Ugh] Otherwise, the condition just gets worse.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Sad] And what if you lost the duel...[P] and were told to...[P] do something abhorrent?[P] Like murdering someone?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Explain] Oh, don't worry.[P] The Goddess does not enforce rules she herself teaches against.[P] In fact, our kitsune texts have a list of punishments that have failed![P] We try to be as specific as possible.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] Okay that's a bit better...[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] But the nekomatas are always pushing at the rules, finding ways around them.[P] Be wary before you agree to duel one of them.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Noted.") ]])
    fnCutsceneBlocker()
end
