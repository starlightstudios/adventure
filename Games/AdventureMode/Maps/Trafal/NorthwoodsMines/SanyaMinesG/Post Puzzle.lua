-- |[ ================================== Post-Puzzle Cutscene ================================== ]|
--Cutscene fired to handle the results of the puzzle game. In this case, win or lose are the same
-- outcome but the scene is different.

-- |[ ====================================== Common Code ======================================= ]|
--In all cases, stop the music.
AudioManager_FadeMusic(45)

--Move Sanya and Odar to the middle.
fnCutsceneMove("Sanya", 10.25, 15.50)
fnCutsceneFace("Sanya", 1, 0)
fnCutsceneMove("Odar",  11.25, 15.50)
fnCutsceneFace("Odar",  -1, 0)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

-- |[ ======================================= Player Won ======================================= ]|
if(gzaPuzzleFight.bPlayerWonGame == true) then
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter2/SanyaMines/iWonPuzzleFight", "N", 1.0)
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Odar", "Neutral") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Hah![P] How do you like them apples, floating mushroom boy![B][C]") ]])
    fnCutscene([[ Append("Odar:[E|Neutral] Blast![P] You -[P] you must have cheated![B][C]") ]])
    fnCutscene([[ Append("Odar:[E|Neutral] My attacks did palty sums while yours blew through my defenses![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Hey man you did kinda give me some free turns at the start of that.[B][C]") ]])
    fnCutscene([[ Append("Odar:[E|Neutral] I do not need your pity, foul hunter![P] I...[P] am at your mercy.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Oh really?[P] Do I get like a prize or something?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Can my prize be going back to Earth?[B][C]") ]])
    fnCutscene([[ Append("Odar:[E|Neutral] You may ask something of the loser indeed, hunter, but this 'Earth' place is far beyond my capabilities.[B][C]") ]])
    fnCutscene([[ Append("Odar:[E|Neutral] Though I am inclined to deny your request even if I can fulfill it on account of your trickery![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Look, you keep saying that.[P] I didn't cheat.[P] I don't like being called a cheater.[B][C]") ]])
    fnCutscene([[ Append("Odar:[E|Neutral] Oh yeah?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Yeah![B][C]") ]])
    fnCutscene([[ Append("Odar:[E|Neutral] Cheater![B][C]") ]])
    fnCutscene([[ Append("Odar:[E|Neutral] Cheater cheater fungoid eater![B][C]") ]])
    fnCutscene([[ Append("Odar:[E|Neutral] Cheat cheat cheat cheat [P][CLEAR]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] ORA!!![P]") ]])
    fnCutsceneBlocker()
    fnCutscenePlaySound("World|Explosion")
    fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Over_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Transition to the next room.
    fnCutscene([[ AL_BeginTransitionTo("SanyaMinesB", "FORCEPOS:32.0x10.0x0") ]])
    

-- |[ ====================================== Player Lost ======================================= ]|
else
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter2/SanyaMines/iWonPuzzleFight", "N", 0.0)
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Odar", "Neutral") ]])
    fnCutscene([[ Append("Odar:[E|Neutral] Oh ho ho ho![P] Ha ha, I am victorious![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Oh gee, what a rush it must be for you, to win at a stupid puzzle fight.[B][C]") ]])
    fnCutscene([[ Append("Odar:[E|Neutral] It is, dear hunter, for now...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] ...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] ...[P] For now what?[B][C]") ]])
    fnCutscene([[ Append("Odar:[E|Neutral] Ha, you see, the winner of a duel may make a demand, overseen by the Fox Goddess herself.[B][C]") ]])
    fnCutscene([[ Append("Odar:[E|Neutral] There are limits, but I...[P] I can...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] ...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] ...[P] You can what?[B][C]") ]])
    fnCutscene([[ Append("Odar:[E|Neutral] Demand that you...[B][C]") ]])
    fnCutscene([[ Append("Odar:[E|Neutral] I am feeling a tad woozy...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    
    --Fall over.
    fnCutsceneSetFrame("Odar", "Wounded")
    fnCutscenePlaySound("World|Thump")
    fnCutsceneBlocker()
    fnCutsceneWait(65)
    fnCutsceneBlocker()

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] [P][P].[P][P].[P][P].[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Huh.[P] He passed out.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] I suppose that's what happens when you get shot and bleed like it's going out of style.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] I guess he [P]*did*[P] get my stuff for me, so I suppose I can return the favour.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] As thanks for finding my first aid kit...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] ...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] [SOUND|Combat|DoctorBag]There you go.[P] At least you won't bleed to death now.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Let it never be said that I am a poor loser.[P] Now, I better find a way to get back to Zeke and that cosplaying fox girl.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Transition to the next room.
    fnCutscene([[ AL_BeginTransitionTo("SanyaMinesB", "FORCEPOS:35.0x10.0x0") ]])
end