-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ==================================== Warp Activation ===================================== ]|
if(sObjectName == "WarpActivate") then
    fnWarpUnlockHandler("Root/Variables/Chapter2/Campfires/iSanyaCabinA", 20.25, 19.50)

-- |[ ====================================== First Night ======================================= ]|
--Scene plays to inform the player to go in the cabin.
elseif(sObjectName == "DropIzunaOff") then

    --Black screen out.
    fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Position.
    fnCutsceneTeleport("Sanya", 12.25, 29.50)
    fnCutsceneTeleport("Zeke", 13.25, 29.50)

    --Wait a bit.
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Lighten up. But not much.
    fnCutsceneMove("Sanya", 12.25, 27.50)
    fnCutsceneMove("Zeke", 13.25, 27.50)
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0.1, 0.1, 0.1, 0.3) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("Sanya:[E|Happy] Hey, I was right![P] It's a cabin![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Happy] C'mon Zeke, maybe they'll have a phone![B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] Bah!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Fold.
    fnAutoFoldParty()
    fnCutsceneBlocker()

--Sanya sees a light near the mines entrance.
elseif(sObjectName == "MysteryLight") then
    
    --Activation check.
    local iSawMysteryLight = VM_GetVar("Root/Variables/Chapter2/SanyaMines/iSawMysteryLight", "N")
    local iDroppedOffIzuna = VM_GetVar("Root/Variables/Chapter2/SanyaMines/iDroppedOffIzuna", "N")
    if(iDroppedOffIzuna == 0.0 or iSawMysteryLight == 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter2/SanyaMines/iSawMysteryLight", "N", 1.0)

    --Spawn Odar.
    TA_Create("Odar")
        TA_SetProperty("Position", 34, 9)
        TA_SetProperty("Facing", gci_Face_SouthWest)
        TA_SetProperty("Clipping Flag", false)
        TA_SetProperty("Activation Script", "Null")
        fnSetCharacterGraphics("Root/Images/Sprites/Odar_Prince/", true)
    DL_PopActiveObject()
    
    --Place a light on Odar.
    AL_SetProperty("Register Radial Light", "Odar Light", 0.0, 0.0, 10000.0)
    AL_SetProperty("Attach Light To Entity", "Odar Light", "Odar")
    
    --Movement.
    fnCutsceneMove("Sanya", 12.25, 23.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Sanya", 1, -1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] (Wait, is that the light I saw earlier?)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Camera.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 3.0)
        CameraEvent_SetProperty("Focus Position", (33.25 * gciSizePerTile), (12.50 * gciSizePerTile))
    DL_PopActiveObject()
    fnCutsceneBlocker()
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Odar", 34.25, 4.50)
    fnCutsceneBlocker()
    fnCutsceneTeleport("Odar", -100.25, -100.50)
    fnCutsceneBlocker()
    
    --Camera.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 5.0)
        CameraEvent_SetProperty("Focus Actor Name", "Sanya")
    DL_PopActiveObject()
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])
    fnCutscene([[ Append("Sanya:[E|Offended] Mother-fucker![P] I don't have time for this cagey bullshit, get back here![P] Someone needs medical attention![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Offended] They probably thinks they're being coy and mysterious with this lantern stuff.[P] Or they don't know I'm here.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Offended] Okay fine, I'll grab them by the neck and politely ask them to help the girl with a broken back!") ]])
    fnCutsceneBlocker()
    
-- |[ ====================================== Hunting Trip ====================================== ]|
--Leaving for the hunting trip.
elseif(sObjectName == "HuntingTrip") then

    --Flag.
    VM_SetVar("Root/Variables/Chapter2/Northwoods/iRunHuntingTutorial", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter2/SanyaMines/iIsZekeInCabin", "N", 0.0)
    
    --Set last save point to here, in case the player somehow loses to the goose.
    AL_SetProperty("Last Save Point", "SanyaCabinA")
    
    --Variables.
    local sSanyaForm = VM_GetVar("Root/Variables/Global/Sanya/sForm", "S")

    --Cut to black.
    fnCutscene([[ AL_SetProperty("Activate Fade", 0, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Position actors. Spawn Zeke.
    fnCutsceneTeleport("Sanya", -1.25, -1.50)
    fnSpecialCharacter("Zeke", -1, -1, gci_Face_South, true, fnResolvePath() .. "Dialogue.lua")
    fnCutsceneTeleport("Zeke", -1.25, -1.50)
    
    --Camera position.
    fnCutsceneCamera("Position", gcfCamera_Cutscene_Speed, 12.25, 22.50)
    fnCutsceneBlocker()
    
    --Fade in.
    fnCutsceneWait(185)
    fnCutscene([[ AL_SetProperty("Activate Fade", 185, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    
    --Spawn Sanya.
    fnCutscenePlaySound("World|FlipSwitch")
    fnCutsceneTeleport("Sanya", 12.25, 22.50)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Spawn Zeke.
    fnCutsceneMove("Sanya", 11.25, 22.50)
    fnCutsceneTeleport("Zeke", 12.25, 22.50)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneFace("Sanya", 1, 0)
    fnCutsceneFace("Zeke", -1, 0)
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Zeke", "Neutral") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] All right, I'm going to go see what I can rustle up.[P] Funny how my hunting trip in the mountains wound up surviving transportation to another world.[B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] Nyeh?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Because I went on a hunting trip, and I'm currently hunting.[P] In the mountains.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Sad] Why do I explain myself to you when you're a goat?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Anyway, you stay here and do goat foraging stuff.[P] I'm sure you can find lots of yummy grass to eat.[B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] Bah![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Laugh] No, you can't come.[P] I need to be quiet and stealthy.[B][C]") ]])
    if(sSanyaForm == "Human") then
        fnCutscene([[ Append("Sanya:[E|Smirk] Us humans, and whatever that foxgirl is, can't eat grass like you.[P] One of the many ways we are inferior.[B][C]") ]])
        fnCutscene([[ Append("Zeke:[E|Neutral] Nyehehe.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Happy] Luckily I've got death up to 2000 meters right here, and I got my knife and lighter.[P] Should be no problem skinning and cooking something.[B][C]") ]])
    else
        fnCutscene([[ Append("Zeke:[E|Neutral] Bah![B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Surprised] Excuse me?[P] I am *extremely* stealthy.[P] If someone looks at me I'll just pretend to be a rock.[B][C]") ]])
        fnCutscene([[ Append("Zeke:[E|Neutral] ...[P] Nyeh?[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] ...[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] ...[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] ...[B][C]") ]])
        fnCutscene([[ Append("Zeke:[E|Neutral] Nyeh bah?[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Happy] See![P] It worked on you![B][C]") ]])
        fnCutscene([[ Append("Zeke:[E|Neutral] Nyeh...[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Happy] Besides, I've got death up to 2000 meters right here, and I got my knife and lighter.[P] Should be no problem skinning and cooking something.[B][C]") ]])
    end
    fnCutscene([[ Append("Sanya:[E|Neutral] Uh...[P] and if you see a mushroom man, pretend you don't know me.[B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] Bah?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Offended] Look don't ask so many questions and get to work on grazing![P] I'll be back soon.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Zeke", 12.25, 23.50)
    fnCutsceneMove("Zeke", 5.25, 23.50)
    fnCutsceneMove("Zeke", 5.25, 21.50)
    fnCutsceneBlocker()

-- |[ ================================== Heal Izuna Sequence =================================== ]|
--Cutscene series taking place over a month as Izuna recovers.
elseif(sObjectName == "SparkleGoat") then

    --Flag.
    VM_SetVar("Root/Variables/Chapter2/SanyaMines/iIsZekeInCabin", "N", 1.0)

    --Cut to black.
    fnCutscene([[ AL_SetProperty("Activate Fade", 0, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Spawn Zeke. Make him sparkle.
    fnSpecialCharacter("Zeke", -100, -100, gci_Face_South, false, nil)
    EM_PushEntity("Zeke")
        TA_SetProperty("Set Auto Overlays", true)
        TA_SetProperty("Auto Overlay TPF", 8.0)
        TA_SetProperty("Allocate Auto Overlays", 16)
        for i = 0, 15, 1 do
            local sPath = string.format("Root/Images/Sprites/Sparkles/%02i", i)
            TA_SetProperty("Auto Overlay Frame", i, sPath)
        end
    DL_PopActiveObject()
    
    --Position characters.
    fnCutsceneTeleport("Sanya", 11.25, 35.50)
    fnCutsceneFace("Sanya", 0, -1)
    fnCutsceneTeleport("Zeke", 5.25, 21.50)
    fnCutsceneFace("Zeke", 1, -1)
    
    --Wait a bit.
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Fade in.
    fnCutsceneWait(25)
    fnCutscene([[ AL_SetProperty("Activate Fade", 125, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Sanya", 11.25, 27.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Zeeeeke, I'm baaaack![B][C]") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Did you miss me?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneFace("Zeke", 1, 1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneMove("Zeke", 5.25, 22.50)
    fnCutsceneMove("Zeke", 10.25, 27.50)
    fnCutsceneFace("Zeke", 1, 0)
    fnCutsceneBlocker()
    fnCutsceneFace("Sanya", -1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Zeke", "Neutral") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] All right, I got this fox some meat.[P] Did you fin - [P][CLEAR]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] ...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Blush] Zeke, why are you sparkling?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneFace("Zeke", 0, 1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Zeke", -1, 1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Zeke", 0, 1)
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    
    --Jump zeke a bit.
    fnCutscenePlaySound("World|Jump")
    fnCutsceneFace("Zeke", -1, 0)
    fnCutsceneJumpTo("Zeke", 10.25, 24.50, 25)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscenePlaySound("World|Jump")
    fnCutsceneFace("Zeke", -1, 0)
    fnCutsceneJumpTo("Zeke", 8.25, 24.50, 25)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscenePlaySound("World|Jump")
    fnCutsceneFace("Zeke", -1, 0)
    fnCutsceneJumpTo("Zeke", 6.25, 24.50, 25)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscenePlaySound("World|Jump")
    fnCutsceneFace("Zeke", 1, 0)
    fnCutsceneJumpTo("Zeke", 8.25, 24.50, 25)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscenePlaySound("World|Jump")
    fnCutsceneFace("Zeke", 1, 0)
    fnCutsceneJumpTo("Zeke", 6.25, 24.50, 25)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Zeke", 1, 0)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Zeke, calm down already!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Sanya", 6.25, 22.50)
    fnCutsceneFace("Sanya", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneFace("Zeke", 0, -1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Hmm...[P] The berries growing here seem to sparkle...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] I'm going to guess it's magic, or something.[P] I'd eat a berry myself but I think I'm good.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneFace("Sanya", 0, 1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Zeke", "Neutral") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] It's probably okay to eat those, but if you gotta hurl, you gotta hurl, right?[B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] Nyeh.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Happy] Okay then.[P] Help me get some sticks so we can get a fire going.[P] That fox is probably starving!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Fade out.
    fnCutsceneWait(65)
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Scene transition.
    fnCutscene([[ AL_BeginTransitionTo("SanyaCabinB", "FORCEPOS:13.0x11.0x0") ]])
    
--One week later.
elseif(sObjectName == "WeekLater") then

    -- |[Setup]|
    --No music.
    AL_SetProperty("Music", "Null")
    
    --Spawn bouncy ball.
    TA_Create("MrBouncey")
        TA_SetProperty("Position", -10, -10)
        TA_SetProperty("Facing", gci_Face_South)
        TA_SetProperty("Clipping Flag", false)
        TA_SetProperty("Activation Script", nil)
        fnSetCharacterGraphics("Root/Images/Sprites/GenericF0/", false)
        TA_SetProperty("Wipe Special Frames")
        TA_SetProperty("Add Special Frame", "Special", "Root/Images/Sprites/BouncyBall/0")
        TA_SetProperty("Set Special Frame", "Special")
    DL_PopActiveObject()
    
    --Spawn Izuna.
    fnSpecialCharacter("Izuna", -100, -100, gci_Face_South, false, nil)
	
	--Switch Izuna's costume.
	LM_ExecuteScript(gsRoot .. "CostumeHandlers/Izuna/Miko_Nude.lua")
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter2/SanyaMines/iIsIzunaInCabin", "N", 0.0)

    -- |[First Scene]|
    --Cut to black.
    fnCutscene([[ AL_SetProperty("Activate Fade", 0, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Spawn Zeke. No sparkles.
    fnSpecialCharacter("Zeke", -100, -100, gci_Face_South, false, nil)
    
    --Position characters.
    fnCutsceneTeleport("Sanya", 11.25, 35.50)
    fnCutsceneFace("Sanya", 0, -1)
    fnCutsceneTeleport("Zeke", 12.25, 35.50)
    fnCutsceneFace("Zeke", 0, -1)
    
    --Wait a bit.
    fnCutsceneWait(245)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Narrator] One week later...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    
    --Fade in and start movement.
    fnCutsceneCall(gsRoot .. "FormHandlers/Sanya/Form_Human.lua")
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneMove("Sanya", 11.25, 24.50)
    fnCutsceneFace("Sanya", 1, 0)
    fnCutsceneMove("Zeke", 12.25, 24.50)
    fnCutsceneFace("Zeke", -1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Zeke", "Neutral") ]])
    fnCutscene([[ Append("Sanya:[E|Happy] Great job today, buddy![P] Who knew goats were excellent foragers?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Sad] But no way are we eating these mushrooms.[P] Berries is one thing, but mushrooms?[P] I am not taking that risk.[B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] Bah![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Sad] Too risky, way too likely to be toxic.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Happy] Let's go check on sleeping beauty and see if she's awake.[B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] ...[P] Neyh?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Blush] What, sleeping beauty?[P] It's just a movie reference.[P] It -[P] it doesn't mean anything![B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] Baaaahhh nyeheh.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Angry] Sh-[P]shut up![P] What would you know anyway?[P] You go get a drink or whatever!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Sanya", 12.25, 22.50)
    fnCutsceneFace("Zeke", -1, -1)
    fnCutsceneBlocker()
    fnCutscenePlaySound("World|FlipSwitch")
    fnCutsceneCamera("Position", gcfCamera_Cutscene_Speed, 12.25, 22.50)
    fnCutsceneTeleport("Sanya", -1.25, -1.50)
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneFace("Zeke", 0, 1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneMove("Zeke", 16.25, 22.50)
    fnCutsceneFace("Zeke", 0, 1)
    fnCutsceneWait(600)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutscenePlaySound("World|FlipSwitch")
    fnCutsceneTeleport("Sanya", 12.25, 22.50)
    fnCutsceneFace("Zeke", -1, 0)
    fnCutsceneBlocker()
    fnCutsceneCamera("Actor Name", gcfCamera_Cutscene_Speed, "Sanya")
    fnCutsceneWait(60)
    fnCutsceneBlocker()
    fnCutsceneFace("Sanya", 1, 0)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Sad") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Zeke", "Neutral") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] Bah?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Sad] Nothing yet...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Sad] Her bones are healing very nicely, but she's still unconscious.[P] Sometimes she mumbles a bit.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Cry] I don't know, Zeke.[P] I'm sure she'll wake up, but I'm not a doctor.[P] What if I'm screwing this up?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Zeke", 13.25, 22.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Sad") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Zeke", "Neutral") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] Nyeh.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Sad] Heh.[P] Thanks, buddy.[P] Gotta keep trying.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Next scene.
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(75)
    fnCutsceneBlocker()
    
    --Lights.
    fnCutscene([[ AL_SetProperty("Activate Lights") ]])
    fnCutscene([[ AL_SetProperty("Set Ambient Light", 0.7, 0.45, 0.45, 1.0) ]])

    --Wait a bit.
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Narrator] Another week later...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(75)
    fnCutsceneBlocker()
    
    -- |[Second Scene]|
    --Position actors.
    fnCutsceneCamera("Position", gcfCamera_Cutscene_Speed, 27.25, 20.50)
    fnCutsceneTeleport("Sanya", -1.25, -1.50)
    fnCutsceneTeleport("Zeke", 9.25, 24.50)
    
    --Layers.
    fnCutsceneLayerDisabled("FloorSanyaBody", false)
    fnCutsceneLayerDisabled("FloorSanyaHeadClosed", false)
    fnCutscene([[ AL_SetProperty("Open Door", "DoorL") ]])
    fnCutsceneBlocker()
    
    --Wait.
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneWait(105)
    fnCutsceneBlocker()
    
    --Scene.
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Mmmmmm...[P] so relaxing...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] How did I live before the invention of hot springs...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] ...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] That girl...[P] she's so cute...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] I wish I could kiss her...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Swap.
    fnCutsceneLayerDisabled("FloorSanyaHeadClosed", true)
    fnCutsceneLayerDisabled("FloorSanyaHeadOpen", false)
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Haha![P] As if![P] I'm not gay, at all![B][C]") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] It's just a fantasy!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneLayerDisabled("FloorSanyaHeadOpen", true)
    fnCutsceneLayerDisabled("FloorSanyaHeadSleepy", false)
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Hah.[P] Just a fantasy, so it's okay to enjoy it.[P] God's not going to get mad about a fantasy...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneLayerDisabled("FloorSanyaHeadSleepy", true)
    fnCutsceneLayerDisabled("FloorSanyaHeadClosed", false)
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Yeah.[P] Mmm, lower...[P] Yeah...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(120)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Zeke", 27.25, 24.50)
    fnCutsceneMove("Zeke", 27.25, 22.50)
    fnCutsceneMove("Zeke", 25.25, 22.50)
    fnCutsceneMove("Zeke", 25.25, 20.50)
    fnCutsceneFace("Zeke", 1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Unghh...[P] I've never felt this way before...[P] this is my first time with another woman...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutsceneFace("Zeke", 0, 1)
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutsceneFace("Zeke", 1, 0)
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Zeke:[VOICE|Zeke] ...................[P] Nyeh?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Oh no.
    fnCutsceneLayerDisabled("FloorSanyaHeadClosed", true)
    fnCutsceneLayerDisabled("FloorSanyaHeadOpen", false)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneLayerDisabled("FloorSanyaHeadOpen", true)
    fnCutsceneLayerDisabled("FloorSanyaHeadLeft", false)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] ZEKE WHAT THE HELL MAN?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] NOT COOL![P] I'M IN HERE![B][C]") ]])
    fnCutscene([[ Append("Zeke:[VOICE|Zeke] Bah.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] What?[P] G-[P]go get a drink somewhere else![B][C]") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Like, all that sweat and dust and stuff?[P] That's in the water when I'm in here, bro![P] What the hell![B][C]") ]])
    fnCutscene([[ Append("Zeke:[VOICE|Zeke] Nyeh![B][C]") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Hey, don't!") ]])
    fnCutsceneBlocker()
    
    --Zeke jumps in.
    fnCutscenePlaySound("World|Jump")
    fnCutsceneJumpTo("Zeke", 27.25, 20.50, 25)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscenePlaySound("World|BigSplash")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] ZEEEEEEEEEEEEEEEEKE!") ]])
    fnCutscene([[ AL_SetProperty("Activate Fade", 0, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(600)
    fnCutsceneBlocker()
    
    -- |[Third Scene]|
    --Clean up, deactivate lights.
    fnCutscene([[ AL_SetProperty("Deactivate Lights") ]])
    fnCutsceneLayerDisabled("FloorSanyaBody", true)
    fnCutsceneLayerDisabled("FloorSanyaHeadLeft", true)
    
    --Izuna's clothes are on the clotheline.
    fnCutsceneLayerDisabled("WallsHiClothes", false)
    
    --Position characters.
    fnCutsceneCamera("Actor Name", gcfCamera_Cutscene_Speed, "Sanya")
    fnCutsceneTeleport("Sanya", 23.25, 17.50)
    fnCutsceneFace("Sanya", -1, 0)
    fnCutsceneTeleport("Zeke", 20.25, 17.50)
    fnCutsceneFace("Zeke", 1, 0)
    fnCutsceneBlocker()
    
    --Fade in.
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Hey Zeke, check what I found by the lake!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutscenePlaySound("World|TakeItem")
    fnCutsceneTeleport("MrBouncey", 22.25, 17.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Tadaa![P] It's a bouncy ball, I think![B][C]") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] No idea whose it is, but we can play fetch now!") ]])
    fnCutsceneBlocker()
    
    --Kick.
    fnCutsceneSetFrame("Sanya", "KickW0")
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscenePlaySound("World|Thump")
    fnCutsceneJumpTo("MrBouncey", 19.25, 17.50, 25)
    fnCutsceneSetFrame("Sanya", "KickW1")
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneSetFrame("Sanya", "Null")
    fnCutsceneFace("Zeke", -1, 0)
    fnCutsceneJumpTo("MrBouncey", 17.25, 17.50, 25)
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneJumpTo("MrBouncey", 18.25, 17.50, 12)
    fnCutsceneWait(12)
    fnCutsceneBlocker()
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Zeke, this is why we always lose at contact sports.[P] You have to be a team player here.[B][C]") ]])
    fnCutscene([[ Append("Zeke:[VOICE|Zeke] Bah.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Izuna appears.
    fnCutscenePlaySound("World|FlipSwitch")
    fnCutsceneTeleport("Izuna", 12.25, 22.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneMove("Izuna", 15.25, 22.50, 0.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneSetFrame("Izuna", "Crouch")
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Girl:[VOICE|Izuna] Ah-ow![P] Ooof...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Sanya", 0, 1)
    fnCutsceneFace("Zeke", 0, 1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Did you hear that?[B][C]") ]])
    fnCutscene([[ Append("Zeke:[VOICE|Zeke] Nyehehe!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutscenePlaySound("World|Jump")
    fnCutsceneMove("Sanya", 23.25, 22.50, 2.00)
    fnCutsceneJumpTo("Zeke", 20.25, 20.50, 25)
    fnCutsceneMove("Zeke", 20.25, 21.50, 2.00)
    fnCutsceneBlocker()
    fnCutsceneFace("Sanya", -1, 0)
    fnCutsceneFace("Zeke", -1, 1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Holy smoke, she's awake![B][C]") ]])
    fnCutscene([[ Append("Girl:[VOICE|Izuna] ...[P] Are you the one who rescued me?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Yeah![P] We found - [P][CLEAR]") ]])
    fnCutscene([[ Append("Girl:[VOICE|Izuna] THEN GIVE ME BACK MY CLOTHES YOU PERVERT!") ]])
    fnCutsceneBlocker()
    
    --Cut to next scene.
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    
    --Next room.
    fnCutscene([[ AL_BeginTransitionTo("SanyaCabinB", "FORCEPOS:17.0x11.0x0") ]])

-- |[ ========================================= Finale ========================================= ]|
elseif(sObjectName == "Finale") then

    --No music.
    AL_SetProperty("Music", "Null")
    
    --Execute a rest. This will change the hunt parameters and respawn chests.
    AM_SetProperty("Execute Rest")
    
    --Flags.
    VM_SetVar("Root/Variables/Chapter2/Northwoods/iIsTutorialHuntActive", "N", 0.0)
    
    --Issue load order.
    fnLoadDelayedBitmapsFromList("Chapter 2 Kissing Scene", gciDelayedLoadLoadAtEndOfTick)
    
    --Cut to black.
    fnCutscene([[ AL_SetProperty("Activate Fade", 0, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Show the blanket.
    AL_SetProperty("Set Layer Disabled", "WallsHiBlanket", false)
    
    --Spawn Izuna and Zeke.
    fnSpecialCharacter("Izuna", -100, -100, gci_Face_South, false, nil)
    fnSpecialCharacter("Zeke", -100, -100, gci_Face_South, false, nil)
    
    --Position.
    fnCutsceneTeleport("Izuna", 24.25, 14.50)
    fnCutsceneFace("Izuna", 1, 0)
    fnCutsceneTeleport("Zeke", 24.25, 15.00)
    fnCutsceneFace("Zeke", 1, 0)
    fnCutsceneTeleport("Sanya", 29.25, 14.50)
    fnCutsceneFace("Sanya", -1, 0)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Narrator] Another week later...") ]])
    fnCutsceneBlocker()
    
    --Wait a bit.
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Fade in.
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sanya", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Zeke", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Izuna", "Ugh") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] All right, let's give it a go.[P] Just lean on Zeke if you feel weak.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Ugh] Honestly, I'm fine![P] We could have left yesterday![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Blush] Then you won't have any problem proving it, now will you?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Izuna", 25.25, 14.50, 0.40)
    fnCutsceneMove("Zeke", 25.25, 15.00, 0.40)
    fnCutsceneBlocker()
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneMove("Izuna", 26.25, 14.50, 0.40)
    fnCutsceneMove("Zeke", 26.25, 15.00, 0.40)
    fnCutsceneBlocker()
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneMove("Izuna", 27.25, 14.50, 0.40)
    fnCutsceneMove("Zeke", 27.25, 15.00, 0.40)
    fnCutsceneBlocker()
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneMove("Izuna", 28.25, 14.50, 1.00)
    fnCutsceneFace("Zeke", 1, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(5)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Sanya", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Zeke", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Izuna", "Neutral") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] Nyeh![B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Happy] I did it![P] I did it![P] Did you see that!?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Happy] Great![P] But take it easy.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] We've lost so much time![P] Perfect Hatred could already have broken his seal![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Blush] Oh no, whatever will we do?[P] I'm sure we can fight him on a broken leg.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Ugh] My leg is fine![P] I - [P]") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Music starts.
    fnCutscene([[ AL_SetProperty("Music", "IzunasTheme") ]])
    
    --Izuna falls onto Sanya.
    fnCutsceneTeleport("Izuna", -1.25, -1.50)
    fnCutsceneSetFrame("Sanya", "Catch0")
    fnCutsceneWait(10)
    fnCutsceneBlocker()
    fnCutsceneSetFrame("Sanya", "Catch1")
    fnCutsceneWait(10)
    fnCutsceneBlocker()
    fnCutscenePlaySound("World|TakeItem")
    fnCutsceneSetFrame("Sanya", "Catch2")
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    fnCutsceneSetFrame("Sanya", "Catch3")
    fnCutsceneWait(50)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Sanya", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Izuna", "Neutral") ]])
    fnCutscene([[ Append("Izuna:[E|Cry] I'm...[P] sorry...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] It's okay, I got you.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Cry] You're...[P] so strong...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] Thanks, I did work out a lot.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] You grow up in a small town in the mountains, just going shopping is a workout.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Cry] And you're so caring...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Blush] Look, I'm just doing it to help you help me save the world.[P] No homo.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] No homo?[P] What does that mean?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Oh, uh.[P] Homosexuality.[P] You know.[P] Two girls?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Cry] Huh?[P] Why not?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Cry] I -[P] It's not that I like you...[P] but...[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Ugh] Actually that's a lie.[P] I like you, a lot.[P] Sorry.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] The elder always counselled that if I caught myself lying, I should catch myself and tell the truth.[P] Even if it's embarassing.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Sad] That's...[P] good advice.[P] I like you too, Izuna.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Sad] But God said that a man shall not lay with another man.[P] Granted I haven't read it myself but the priest was quite adamant.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] Then...[P] nevermind.[P] I don't want to anger your god.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] ...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Can you stand?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] Yeah, I'll be all right.[P] Just was weak for a moment there.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] Will you be ready to move out tomorrow?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] Of course.[P] Of course.[P] I'll just rest for the day, we'll go at first light.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Night Time]|
    --Music stops.
    fnCutscene([[ AL_SetProperty("Music", "Null") ]])
    
    --Fade to black.
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(165)
    fnCutsceneBlocker()
    
    --Camera focus.
    fnCutsceneCamera("Position", gcfCamera_Cutscene_Speed, 27.25, 20.50)
    
    --Reposition.
    fnCutsceneSetFrame("Sanya", "Null")
    fnCutsceneTeleport("Sanya", 12.25, 24.50)
    fnCutsceneTeleport("Zeke",  11.25, 24.50)
    fnCutsceneTeleport("Izuna", -1.25, -1.50)
    
    --Switch to nude costume for Sanya.
    --TODO
    
    --Layers.
    fnCutscene([[ AL_SetProperty("Open Door", "DoorL") ]])
    fnCutsceneLayerDisabled("FloorIzunaBody", false)
    fnCutsceneLayerDisabled("FloorIzunaHeadClosed", false)
    
    --Lights.
    fnCutscene([[ AL_SetProperty("Activate Lights") ]])
    fnCutscene([[ AL_SetProperty("Set Ambient Light", 0.7, 0.45, 0.45, 1.0) ]])
    
    --Fade in.
    fnCutsceneWait(45)
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Izuna:[VOICE|Izuna] Mmmmmm...[P] so relaxing...[B][C]") ]])
    fnCutscene([[ Append("Izuna:[VOICE|Izuna] How did I live before the invention of hot springs...[B][C]") ]])
    fnCutscene([[ Append("Izuna:[VOICE|Izuna] ...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Sanya", 25.25, 24.50)
    fnCutsceneBlocker()
    
    --Music starts.
    fnCutscene([[ AL_SetProperty("Music", "IzunasTheme") ]])
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Izuna:[VOICE|Izuna] Fox Goddess...[P] please, hear me.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneFace("Sanya", 1, -1)
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutsceneFace("Sanya", 0, 1)
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Izuna:[VOICE|Izuna] Give me the power to help Sanya and aid in her quest.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[VOICE|Izuna] Give me the wisdom to overcome my temptation.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[VOICE|Izuna] Give me the courage to face the evil that threatens us...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] (It's probably wrong for me to eavesdrop...)[B][C]") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Coming through!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneLayerDisabled("FloorIzunaHeadClosed", true)
    fnCutsceneLayerDisabled("FloorIzunaHeadOpen", false)
    fnCutsceneMove("Sanya", 27.25, 24.50)
    fnCutsceneMove("Sanya", 27.25, 22.50)
    fnCutsceneMove("Sanya", 28.25, 22.50)
    fnCutsceneMove("Sanya", 28.25, 22.50)
    fnCutsceneBlocker()
    fnCutsceneFace("Sanya", 0, -1)
    fnCutsceneJumpTo("Sanya", 28.25, 20.50, 25)
    fnCutsceneBlocker()
    fnCutscenePlaySound("World|BigSplash")
    fnCutsceneTeleport("Sanya", -1.25, -1.50)
    fnCutsceneLayerDisabled("FloorSanyaBody", false)
    fnCutsceneLayerDisabled("FloorSanyaHeadOpen", false)
    fnCutsceneBlocker()
    fnCutsceneLayerDisabled("FloorSplash0", false)
    fnCutsceneWait(8)
    fnCutsceneBlocker()
    fnCutsceneLayerDisabled("FloorSplash0", true)
    fnCutsceneLayerDisabled("FloorSplash1", false)
    fnCutsceneWait(8)
    fnCutsceneBlocker()
    fnCutsceneLayerDisabled("FloorSplash1", true)
    fnCutsceneLayerDisabled("FloorSplash2", false)
    fnCutsceneWait(8)
    fnCutsceneBlocker()
    fnCutsceneLayerDisabled("FloorSplash2", true)
    fnCutsceneLayerDisabled("FloorSplash3", false)
    fnCutsceneWait(8)
    fnCutsceneBlocker()
    fnCutsceneLayerDisabled("FloorSplash3", true)
    fnCutsceneWait(8)
    fnCutsceneBlocker()
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Izuna", "Neutral") ]])
    fnCutscene([[ Append("Izuna:[E|Ugh] Sanya![P] Were you raised in a barn!?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Blush] As a matter of fact...[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Cry] I'm -[P] in here![P] You pervert![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Blush] So?[P] I've been using this spot for weeks.[P] You're in my hot spring.[P] I had dibs.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Ugh] You-[P]you're just using this as an excuse to gawk at me in my underwear![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Laugh] Pah, I had plenty of opportunities to do that.[P] I had to change your cast and wash your clothes.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Ugh] Why do I even like you...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Blush] Good looks, sharp wit, propensity towards violence.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] ...[P] Damn, you're right on top of it aren't you.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] I'm a straight-shooter.[P] Speak your mind, don't hide things.[P] Nobody thinks you're being clever by not saying what you mean.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] Well -[P] fine![P] I'll say what I mean![B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Ugh] It's not okay for you to look at me naked![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] I had to.[P] You were unconscious.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Cry] That -[P] that doesn't make it okay![B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] Look.[P] Let's say you're carrying a big heavy log.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Happy] Now you're speaking my language.[P] Hypothetically.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] And you bump someone with it.[P] You just didn't see them, right?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] Do you say you're sorry?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Yeah.[P] Unless they're a dink.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Cry] So then why won't you say you're sorry for looking at me, naked?[P] Why not?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Sad] .[P].[P].[P] Huh.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Sad] I really hadn't thought of it that way before.[P] Even if I had a good reason, it still hurts your feelings.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Explain] Yes![P] Yes exactly![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Sad] Well then.[P][P][P] I'm... sorry.[P] I'm sorry about that, Izuna.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Do I have your permission to see you naked in the future?[P] I mean, if it's necessary to save you.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Ugh] Of course![P] If you...[P] you really need to.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] No homo.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] About that...[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] Sanya, can you tell me about your god?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Aren't you a worshipper of the Fox Goddess?[P] Or whatever?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] I'm not being trite.[P] The only person I've seen since I got here is you, and Zeke.[P] He's not much of a conversationalist.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] He's kind of like a politician.[P] Talks a lot, doesn't actually say much.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] If only we could replace politicians with goats.[P] But as to the Fox Goddess...[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] Yes I am a follower, but, following Her is more of a...[P] moral choice.[P] Not that I think She is not divine, but I happen to agree with the words She wrote.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] If the Fox Goddess said that it was not all right to pet a goat, for example, I would know She probably had a good reason.[P] But I can think for myself.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] I am very good at thinking for myself.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Cry] But your god tells you that it is not all right for...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Sad] A man to lay with another man.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] Right, exactly.[P] I understand it is a euphemism.[P] For one thing, you're a woman.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Blush] And you're a fox.[P] But I don't think this counts as bestiality.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Explain] Mwehehe![P] Thank you.[P] It is a compliment to be called a fox in Trafal.[P] It means someone is clever.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Blush] And good looking?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] You really think so?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] ...[P] But if your god tells you to forego that, don't you ask why?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Sad] The bible says it is an abomination.[P] Bad stuff.[P] God forbids it.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Explain] You see, I was thinking.[P] Perhaps the Fox Goddess and your god were the same god.[P] There are many religions here in Pandemonium, but I think all of them share a sort of truth with one another.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Explain] The Fox Goddess teaches us that we must love one another, forgive one another's offenses, and live in harmony.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Sad] My God says the same thing.[P] Did he send a fella named Jesus to die for your sins?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] No...[P] Though I've heard the angels speak of a savior like that.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Surprised] Hold on you have literal angels here?[P] Actual real angels?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] Well, yes.[P] Does Earth not have any?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Look we don't even have monstergirls on Earth.[P] It's kind of a boring place, actually.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] Can you take me to meet any of these angels?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] They don't come around Trafal very often.[P] Indeed they have their own agenda, and have no convents here.[P] But if I hear of one, I'll tell you.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] I don't agree with the angels on a great many things, but they, too, believe in a higher power.[P] They just don't believe she's a fox.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Ugh] (And a whole lot of other things, but maybe I don't need to tell Sanya about those right now...)[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] So is that why you wanted to know about Christianity?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Cry] I.[P] I.[P] I wanted to know why you would follow a god who doesn't.[P] Let you...[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Cry] Be with someone.[P] Someone you like.[P] Maybe want to love.[P] You know what I mean, don't you?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Offended] I'm not gay![B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] Do you like me?[P] Physically?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Blush] ...[P] Yeah.[P] You're beautiful.[P] And you're -[P] smart![P] And -[P] uh...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Blush] Your clothes are really cool?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] Thank you![P] But those are actually regular Miko clothes.[P] I'm supposed to wear them.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Blush] And you like the things I like![B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] Like what?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Blush] Hot springs![P] Pet goats![P] Thinking about stuff![P] Grunting and sweating![P] Cursing![B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] Mwehehe![P] I'm sure we have lots of other things in common.[P] (Maybe not the sweating one...)[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Cry] So if you like all of those things, why don't you want to be with me?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Surprised] Hey![P] Hey![P] I'm not a monogamous girl here![P] There's no exclusivity bargain in the offing![B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] Excuse me?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Blush] And, let's say I'm actually super gay.[P] Just all of the gay.[P] And we touch uglies.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Ugh] (By the nine tails, what are these euphemisms?)[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Blush] I like to have an open field, you know?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] I am not able to understand you due to the many, many layers here.[P] What are you saying?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Blush] If you want to date me, I reserve the right to bone any and all hots guys I encounter with no downside.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] Oh.[P] That's not a problem.[P] Is that normal where you are from?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Well sorta.[P] You're supposed to have one person.[P] It's called monogamy.[P] And having more than one is polygamy and adultery.[P] Also no-nos.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Explain] So this rule, you disagree with and do not follow.[P] But if I am a woman...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Cry] F-[P]fuck it![P] Fuck it![P] C-[P]close your eyes![P] Close your eyes because I don't know if I can do this!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Kissing Sequence]|
    --Dialogue. Fade to black while this is going on.
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Activate Scene") ]])
    fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Ch2Kissing/Kiss00") ]])
	fnCutscene([[ Append("Closing their eyes, the two moved in closer.[P] They puckered and pressed forward, inch by inch.[P] Sanya's heart beat faster.[P] Izuna flared on the inside.[B][C]") ]])
	fnCutscene([[ Append("Both of them had felt the tension.[P] Touching one another had set their skin afire.[P] Sanya couldn't have admitted it, Izuna didn't want to offend.[P] Now all the guards were gone, the barriers removed.[P] The time was now.[B][C]") ]])
	fnCutscene([[ Append("Sanya leaned forward again.[P] Izuna felt the water displace and moved forward herself.[P] She could feel the heat as they got closer and closer...") ]])
    fnCutsceneBlocker()
    
    --Black cut.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
    fnCutscene([[ WD_SetProperty("Set Scene Image", "Null") ]])
    fnCutscene([[ WD_SetProperty("Set Scene Image", "Null") ]])
	fnCutscene([[ Append("Finally, contact.[P] Sanya wondered if she had made a mistake.[P] Had she clumsily kissed Izuna's hair?[P] Or were kitsunes just fuzzy to kiss?") ]])
    fnCutsceneBlocker()
    
    --Zeke appears.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
    fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Ch2Kissing/Kiss01") ]])
	fnCutscene([[ Append("Not wanting to make the awkward situation any more awkward, they both held their position.[P] Kissing another woman for the first time, Sanya reasoned, would have its oddities.[P] Still, Izuna couldn't possibly be this furry.[B][C]") ]])
	fnCutscene([[ Append("Izuna, meanwhile, was taken aback.[P] Sanya's hair smelled horrible![P] Had she never heard of shampoo or soap?[P] She would need to civilize Sanya at least a little, but that roughness was so appealing.[P] Still, this was probably too rough.[P] Too rugged.[P] Something was off...[B][C]") ]])
	fnCutscene([[ Append("[VOICE|Zeke] ......[P] Bah?") ]])
    fnCutsceneBlocker()
    
    --You stupid goat!
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
    fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Ch2Kissing/Kiss01") ]])
    fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Ch2Kissing/Kiss02") ]])
	fnCutscene([[ Append("...[B][C]") ]])
	fnCutscene([[ Append("...[B][C]") ]])
	fnCutscene([[ Append("...") ]])
    fnCutsceneBlocker()
    
    --Black cut.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
    fnCutscene([[ WD_SetProperty("Set Scene Image", "Null") ]])
    fnCutscene([[ WD_SetProperty("Set Scene Image", "Null") ]])
	fnCutscene([[ Append("[VOICE|Sanya]ZEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEKE!") ]])
    fnCutsceneBlocker()
    
    --Music stops.
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Music", "Null") ]])
    
    -- |[Finale]|
    --Camera focus.
    fnCutsceneCamera("Actor Name", gcfCamera_Cutscene_Speed, "Sanya")
    
    --Reposition.
    fnCutsceneTeleport("Sanya", 11.25, 26.50)
    fnCutsceneTeleport("Zeke",  13.25, 26.50)
    fnCutsceneTeleport("Izuna", -1.25, -1.50)
    fnCutsceneFace("Sanya", 1, 0)
    fnCutsceneFace("Zeke", -1, 0)
    
    --Add Izuna and Zeke to the party.
    fnAddPartyMember("Izuna")
    fnAddPartyMember("Zeke")
    
    --Disable all layers from the previous scene.
    fnCutsceneLayerDisabled("WallsHiClothes", true)
    fnCutsceneLayerDisabled("WallsHiBlanket", true)
    fnCutsceneLayerDisabled("FloorSanyaBody", true)
    fnCutsceneLayerDisabled("FloorSanyaHeadLeft", true)
    fnCutsceneLayerDisabled("FloorSanyaHeadOpen", true)
    fnCutsceneLayerDisabled("FloorSanyaHeadClosed", true)
    fnCutsceneLayerDisabled("FloorSanyaHeadSleepy", true)
    fnCutsceneLayerDisabled("FloorIzunaBody", true)
    fnCutsceneLayerDisabled("FloorIzunaHeadOpen", true)
    fnCutsceneLayerDisabled("FloorIzunaHeadClosed", true)
    fnCutsceneLayerDisabled("FloorIzunaHeadSleepy", true)
    fnCutsceneLayerDisabled("FloorSplash0", true)
    fnCutsceneLayerDisabled("FloorSplash1", true)
    fnCutsceneLayerDisabled("FloorSplash2", true)
    fnCutsceneLayerDisabled("FloorSplash3", true)
    
    --Lights.
    fnCutscene([[ AL_SetProperty("Deactivate Lights") ]])
    
    --Fade in.
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    
    --Wait a bit.
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneTeleport("Izuna", 12.25, 22.50)
    fnCutsceneFace("Izuna", 0, -1)
    fnCutscenePlaySound("World|FlipSwitch")
    fnCutsceneWait(25)
    fnCutsceneFace("Sanya", 1, -1)
    fnCutsceneFace("Zeke", -1, -1)
    fnCutsceneBlocker()
    fnCutsceneMove("Izuna", 12.25, 25.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Sanya", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Zeke", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Izuna", "Neutral") ]])
    fnCutscene([[ Append("Sanya:[E|Happy] Ready to go?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Ugh] Are we just not going to -[P][CLEAR]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Never mentioning it again as long as we live.[P] So, I repeat.[P] Ready to go?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] Yep![P] And here you go![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] [SOUND|World|TakeItem]What's this?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Explain] A map of Trafal![P] It was in my bag.[P] You didn't look in there?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Well of course I did, but I don't even know where we are on the map.[P] Where's this cabin?[P] Landmarks?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] I suppose if you're not already familiar with Trafal, the map isn't as useful.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] Do you mind if I write on the map?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Explain] Go ahead![P] You can use my note-taking pencil![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] (Okay so I can just make a few marks here...)[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] (I can update the map whenever I visit a part of it.[P] I'll draw landmarks and connections on it.[P] Easy!)[B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] Nyeh.[P] Nyeh?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Angry] NO CHEWING ON THE MAP ZEKE![P] YOU'RE IN THE DOGHOUSE FOR AT LEAST A DAY![B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] ......[P] bah.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Okay okay.[P] So, miss guide, where do you recommend we go first?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Explain] My temple is southeast of here, in Yuca pass.[P] We should speak to the elder and get her advice.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Happy] And let everyone know you're okay?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] Oh I'm sure they barely noticed I was gone.[P] I'm off getting stories and distributing pamphlets all the time.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] Getting stories?[P] What are you, a writer?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Explain] Yuh huh![P] I'm a journalist![P] Ask me about it later.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Explain] Let's go save the world!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Music starts.
    fnCutscene([[ AL_SetProperty("Music", "SanyasTheme") ]])
    
    --Fold the party.
    fnCutsceneMove("Sanya", 12.25, 26.50)
    fnCutsceneBlocker()
    fnAutoFoldParty()
    fnCutsceneBlocker()
    
    --Clean images.
    fnCutscene([[ fnUnloadBitmapsFromList("Kissing Scene") ]])

-- |[ ======================================= Cant Leaves ====================================== ]|
--Prevent the player from leaving until it's daytime.
elseif(sObjectName == "CantLeave") then

    --Plays if the player has not completed the mines yet.
    local iCompletedMines = VM_GetVar("Root/Variables/Chapter2/SanyaMines/iCompletedMines", "N")
    if(iCompletedMines == 1.0) then return end
    
    --Variables.
    local iDroppedOffIzuna      = VM_GetVar("Root/Variables/Chapter2/SanyaMines/iDroppedOffIzuna", "N")
    local iSawMinesExitDialogue = VM_GetVar("Root/Variables/Chapter2/SanyaMines/iSawMinesExitDialogue", "N")
    
    --Dialogue changes based on variables.
    if(iDroppedOffIzuna == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (I really should get her inside...)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Despite how alluring faffing about in a dark forest is...)") ]])
        fnCutsceneBlocker()
    elseif(iSawMinesExitDialogue == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (There was definitely a light going into that mineshaft...)") ]])
        fnCutsceneBlocker()
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (I'm not wandering around, cold tired and alone, in a forest at night, when I could be hanging out with a goat.)") ]])
        fnCutsceneBlocker()
    end

    --Movement.
    fnCutsceneMove("Sanya", 12.25, 29.50)
    fnCutsceneBlocker()
    fnAutoFoldParty()
    fnCutsceneBlocker()

--Prevent the player from leaving until they drop off Izuna.
elseif(sObjectName == "CantLeaveMines") then

    --Plays if the player has not completed the mines yet.
    local iDroppedOffIzuna = VM_GetVar("Root/Variables/Chapter2/SanyaMines/iDroppedOffIzuna", "N")
    if(iDroppedOffIzuna == 1.0) then return end
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (I really should get her inside...)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (But the prospect of getting lost in a dank filthy mine is positively captivating...)") ]])
    fnCutsceneBlocker()

    --Movement.
    fnCutsceneMove("Sanya", 34.75, 11.50)
    fnCutsceneBlocker()
    fnAutoFoldParty()
    fnCutsceneBlocker()
end
