-- |[ ======================================= Constructor ====================================== ]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = fnResolveDirectory() --Map's name is the name of the folder it is in.
local sLevelMusic = "SanyasTheme"
local sMapResolveName = "Map Resolve Name"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[ ======================================== Standard ======================================== ]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

    -- |[Construction]|
	--Music.
    local iIsNightAtMines = VM_GetVar("Root/Variables/Chapter2/SanyaMines/iIsNightAtMines", "N")
    if(iIsNightAtMines == 0.0) then
        AL_SetProperty("Music", sLevelMusic)
    else
        AL_SetProperty("Music", "Null")
    end
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
    
	-- |[Lights]|
    if(iIsNightAtMines == 1.0) then
        AL_SetProperty("Activate Lights")
    end
    
    -- |[Map Handling]|
    --Resolve the map's variable.
    local iCompletedHunt = VM_GetVar("Root/Variables/Chapter2/Northwoods/iCompletedHunt", "N")
    local sMapVariable = fnResolveMapVarFromName(sLevelName)
    if(sMapVariable ~= "Null" and iCompletedHunt == 1.0) then
        
        --Flag the map as uncovered.
        VM_SetVar(sMapVariable, "N", 1.0)
        
        --Run subroutine to build map properties.
        gfnLastMapFunction = fnHandleNorthwoodsMap
        fnHandleNorthwoodsMap(sLevelName)

    end

-- |[ ======================================== Post-Exec ======================================= ]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	-- |[NPC Spawning]|
	--If an entity needs to spawn after a post-exec cutscene, put them here.
    local iCompletedMines = VM_GetVar("Root/Variables/Chapter2/SanyaMines/iCompletedMines", "N")
    local iCompletedHunt  = VM_GetVar("Root/Variables/Chapter2/Northwoods/iCompletedHunt",  "N")
    if(iCompletedHunt == 0.0 and iCompletedMines == 1.0) then
        fnSpecialCharacter("Zeke", 5, 21, gci_Face_East, true, fnResolvePath() .. "Dialogue.lua")
    end

    -- |[Overlays]|
    --If the cabin has not been upgraded, hide these layers and change the collisions.
    local iUpgradedCabin = VM_GetVar("Root/Variables/Chapter2/Northwoods/iUpgradedCabin", "N")
    if(iUpgradedCabin == 0.0) then
        AL_SetProperty("Set Layer Disabled", "WallsEnhanced", true)
        AL_SetProperty("Set Layer Disabled", "WallsHiEnhanced", true)
        AL_SetProperty("Set Collision", 17, 15, 0, 0)
        AL_SetProperty("Set Collision", 18, 15, 0, 0)
        AL_SetProperty("Set Collision", 19, 15, 0, 0)
        AL_SetProperty("Set Collision", 25, 11, 0, 0)
    end
    
    --These layers are always hidden except for cutscenes.
    AL_SetProperty("Set Layer Disabled", "WallsHiClothes", true)
    AL_SetProperty("Set Layer Disabled", "WallsHiBlanket", true)
    AL_SetProperty("Set Layer Disabled", "FloorSanyaBody", true)
    AL_SetProperty("Set Layer Disabled", "FloorSanyaHeadLeft", true)
    AL_SetProperty("Set Layer Disabled", "FloorSanyaHeadOpen", true)
    AL_SetProperty("Set Layer Disabled", "FloorSanyaHeadClosed", true)
    AL_SetProperty("Set Layer Disabled", "FloorSanyaHeadSleepy", true)
    AL_SetProperty("Set Layer Disabled", "FloorIzunaBody", true)
    AL_SetProperty("Set Layer Disabled", "FloorIzunaHeadOpen", true)
    AL_SetProperty("Set Layer Disabled", "FloorIzunaHeadClosed", true)
    AL_SetProperty("Set Layer Disabled", "FloorIzunaHeadSleepy", true)
    AL_SetProperty("Set Layer Disabled", "FloorSplash0", true)
    AL_SetProperty("Set Layer Disabled", "FloorSplash1", true)
    AL_SetProperty("Set Layer Disabled", "FloorSplash2", true)
    AL_SetProperty("Set Layer Disabled", "FloorSplash3", true)
    
    -- |[Skillbooks]|
    --Place skillbook overlay code here.
    
    -- |[Rifle Handling]|
    VM_SetVar("Root/Variables/Global/Sanya/iRifleCode", "N", gciSanyaRifle_NotHere)

    -- |[Savepoint]|
    --If Sanya doesn't have her lighter yet, the savepoint functions but does not light up.
    if(iCompletedMines == 0.0) then
        AL_SetProperty("Set Savepoint Does Not Light", "Campfire", true)
    end
    

end
