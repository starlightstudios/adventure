-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
if(sObjectName == "StupidDump") then

    --Repeat check.
    local iSawRifleScene        = VM_GetVar("Root/Variables/Chapter2/SanyaMines/iSawRifleScene", "N")
    local iSawMinesExitDialogue = VM_GetVar("Root/Variables/Chapter2/SanyaMines/iSawMinesExitDialogue", "N")
    if(iSawRifleScene == 0.0 or iSawMinesExitDialogue == 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter2/SanyaMines/iSanyaStupidStupid", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter2/SanyaMines/iSawMinesExitDialogue", "N", 1.0)

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])
    fnCutscene([[ Append("Sanya:[E|Offended] I hate this dusty dump so much.[P] Can't wait to get back to the dusty dump with that cute cosplaying foxgirl in it.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] .[P].[P].[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Blush] Hah, I just said she was cute.[P] I just wish I had hair like hers, that's all.[P] It's just jealousy![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] ...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] ...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Offended] Oh crap I hope Zeke's not eating her hair![P] He did promise, but he has precious little self control, like many goats.") ]])
    fnCutsceneBlocker()

end
