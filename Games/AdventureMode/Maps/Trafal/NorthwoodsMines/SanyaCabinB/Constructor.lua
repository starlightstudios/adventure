-- |[ ======================================= Constructor ====================================== ]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = fnResolveDirectory() --Map's name is the name of the folder it is in.
local sLevelMusic = "Null"
local sMapResolveName = "Map Resolve Name"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[ ======================================== Standard ======================================== ]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

    -- |[Construction]|
	--Music.
	AL_SetProperty("Music", sLevelMusic)
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation(sMapResolveName)
    
	-- |[Lights]|
    local iIsNightAtMines = VM_GetVar("Root/Variables/Chapter2/SanyaMines/iIsNightAtMines", "N")
    if(iIsNightAtMines == 1.0) then
        AL_SetProperty("Activate Lights")
    end

-- |[ ======================================== Post-Exec ======================================= ]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	-- |[NPC Spawning]|
	--If an entity needs to spawn after a post-exec cutscene, put them here.
    local iSawMinesExitDialogue = VM_GetVar("Root/Variables/Chapter2/SanyaMines/iSawMinesExitDialogue", "N")
    local iIsZekeInCabin = VM_GetVar("Root/Variables/Chapter2/SanyaMines/iIsZekeInCabin", "N")
    if(iIsZekeInCabin == 1.0) then
        if(iSawMinesExitDialogue == 0.0) then
            fnSpecialCharacter("Zeke", 16, 6, gci_Face_West, true, fnResolvePath() .. "Dialogue.lua")
        else
            fnSpecialCharacter("Zeke", 16, 6, gci_Face_North, false, "Null")
        end
    end

    -- |[Overlays]|
    --If the cabin has not been upgraded, hide these layers and change the collisions.
    local iUpgradedCabin = VM_GetVar("Root/Variables/Chapter2/Northwoods/iUpgradedCabin", "N")
    if(iUpgradedCabin == 0.0) then
        AL_SetProperty("Set Layer Disabled", "WallsEnhanced", true)
        AL_SetProperty("Set Layer Disabled", "WallsHiEnhanced", true)
    end
    
    --Izuna resting.
    local iIsIzunaInCabin = VM_GetVar("Root/Variables/Chapter2/SanyaMines/iIsIzunaInCabin", "N")
    if(iIsIzunaInCabin == 0.0) then
        AL_SetProperty("Set Layer Disabled", "SleepingIzuna", true)
    end
    
    --Fire is only active during certain scenes.
    AL_SetProperty("Set Layer Disabled", "Fire", true)
    
    -- |[Skillbooks]|
    --Place skillbook overlay code here.
    
    -- |[Rifle Handling]|
    VM_SetVar("Root/Variables/Global/Sanya/iRifleCode", "N", gciSanyaRifle_NotHere)

end
