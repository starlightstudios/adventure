-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= Exits ========================================== ]|
if(sObjectName == "SampleExit") then
    
-- |[ ====================================== Examinables ======================================= ]|
elseif(sObjectName == "Pot") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (An old, heavy cast-iron cooking pot.)") ]])

elseif(sObjectName == "Izuna") then

    --Does nothing if Izuna isn't actually here.
    local iIsIzunaInCabin = VM_GetVar("Root/Variables/Chapter2/SanyaMines/iIsIzunaInCabin", "N")
    if(iIsIzunaInCabin == 0.0) then return end
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Girl:[VOICE|Izuna] Mmhhh...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Hey![P] Did you say something?[P] Hello?[B][C]") ]])
    fnCutscene([[ Append("Girl:[VOICE|Izuna] ...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Nothing.[P] Crud.") ]])
    fnCutsceneBlocker()

-- |[ ========================================== Other ========================================= ]|
-- |[ ========================================== Debug ========================================= ]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
