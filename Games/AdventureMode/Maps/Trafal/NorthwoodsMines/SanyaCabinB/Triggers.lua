-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ====================================== First Night ======================================= ]|
if(sObjectName == "DropIzunaOff") then
    
    --Repeat check.
    local iDroppedOffIzuna = VM_GetVar("Root/Variables/Chapter2/SanyaMines/iDroppedOffIzuna", "N")
    if(iDroppedOffIzuna == 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter2/SanyaMines/iDroppedOffIzuna", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter2/SanyaMines/iIsIzunaInCabin", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter2/SanyaMines/iIsZekeInCabin", "N", 1.0)
    
    --Remove Zeke from the party.
    fnRemovePartyMember("Zeke", false)
    
    --Movement.
    fnCutsceneMove("Sanya", 7.25, 14.50)
    fnCutsceneMove("Zeke", 8.25, 14.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Look around.
    fnCutsceneFace("Sanya", -1, 1)
    fnCutsceneFace("Zeke", 1, 1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Sanya", -1, 0)
    fnCutsceneFace("Zeke", 1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Sanya", -1, -1)
    fnCutsceneFace("Zeke", 1, -1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Sanya", 0, -1)
    fnCutsceneFace("Zeke", 0, -1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Sanya", 1, -1)
    fnCutsceneFace("Zeke", -1, -1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Sanya", 1, 0)
    fnCutsceneFace("Zeke", -1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Zeke", "Neutral") ]])
    fnCutscene([[ Append("Sanya:[E|Offended] What a dump![P] This place is filthier than my brother's room![B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] Bah![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] I guess it's an abandoned building, but it's certainly warmer in here than outside.[P] Let's look around.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] And remember, if you see something dangeous, do what you do best.[P] Make a lot of noise and run around in a panic.[B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] Nyeh?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Stick to your strengths, bucko.[P] You're here for your hard head, not your negotiation skills.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Zeke", 9.25, 14.50)
    fnCutsceneMove("Zeke", 9.25, 9.50)
    fnCutsceneMove("Zeke", 6.25, 9.50)
    fnCutsceneMove("Sanya", 9.25, 14.50)
    fnCutsceneMove("Sanya", 9.25, 7.50)
    fnCutsceneMove("Sanya", 12.25, 7.50)
    fnCutsceneBlocker()
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    fnCutsceneFace("Sanya", 1, -1)
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    fnCutsceneMove("Sanya", 15.25, 7.50)
    fnCutsceneMove("Sanya", 16.25, 6.50)
    fnCutsceneFace("Sanya", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Sound.
    fnCutscene([[ AudioManager_PlaySound("World|TakeItem") ]])
    fnCutsceneBlocker()
    
    --Change Sanya's costume to not-carrying. It is set immediately but will not take effect until the script fires.
    VM_SetVar("Root/Variables/Costumes/Sanya/sCostumeHuman", "S", "Normal")
    VM_SetVar("Root/Variables/Costumes/Sanya/sCostumeSevavi", "S", "Normal")
    fnCutsceneLayerDisabled("SleepingIzuna", false)
    fnCutscene([[ LM_ExecuteScript(gsCharacterAutoresolve, "Sanya") ]])
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneMove("Zeke",  8.25, 9.50)
    fnCutsceneMove("Zeke", 10.25, 7.50)
    fnCutsceneMove("Zeke", 12.25, 7.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Zeke", "Neutral") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] This bed is pretty filthy, but at least it's something.[P] Did you find a phone, boy?[B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] Nyeh.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] Oh right, I don't speak goat.[P] Sadly, the dilapidated furnishings suggests we won't be finding one.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] I'll take a quick look at that other building.[P] Maybe they have a telegraph post or something.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] Zeke, you're in charge.[B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] Bah![P] Bah![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Blush] I knew I could count on you, buddy.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Offended] BUT![B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] Nyeeh?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Offended] Do NOT chew on her hair.[P] It's not food, you're not supposed to eat it.[B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] ...[B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] ...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Offended] Zeke, promise me.[B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] Bahhh.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Happy] That's my boy!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Zeke", 16.25, 7.50)
    fnCutsceneBlocker()
    fnCutsceneMove("Zeke", 16.25, 6.50)
    fnCutsceneMove("Sanya", 15.25, 6.50)
    fnCutsceneFace("Sanya", 1, 0)
    fnCutsceneFace("Zeke", -1, 0)
    fnCutsceneBlocker()
    
    --Give Zeke a dialogue handler.
    EM_PushEntity("Zeke")
        TA_SetProperty("Activation Script", fnResolvePath() .. "Dialogue.lua")
        TA_SetProperty("Clipping Flag", true)
    DL_PopActiveObject()
    
    -- |[Equipment Switch]|
    --Because Sanya's Bare Hands weapon has "you're carrying a fox" in the description, we switch them out for
    -- a new item with a different description. This won't happen in NC+ because the item won't be Bare Hands.
    AdvCombat_SetProperty("Push Party Member", "Sanya")
        local sWeaponName = AdvCombatEntity_GetProperty("Equipment In Slot S", "Weapon")
    DL_PopActiveObject()
    
    --Using Bare Hands:
    if(sWeaponName == "Bare Hands") then
        
        --Create and equip the new weapon.
        LM_ExecuteScript(gsItemListing, "Gloved Hands")
        AdvCombat_SetProperty("Push Party Member", "Sanya")
            AdvCombatEntity_SetProperty("Equip Item To Slot", "Weapon", "Gloved Hands")
        DL_PopActiveObject()
        
        --Remove the old weapon.
        AdInv_SetProperty("Remove Item", "Bare Hands")
    end

-- |[Second Scene]|
elseif(sObjectName == "FirstAid") then

    --Repeat check.
    local iSawMinesExitDialogue = VM_GetVar("Root/Variables/Chapter2/SanyaMines/iSawMinesExitDialogue", "N")
    local iCompletedMines       = VM_GetVar("Root/Variables/Chapter2/SanyaMines/iCompletedMines", "N")
    if(iSawMinesExitDialogue == 0.0 or iCompletedMines == 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter2/SanyaMines/iCompletedMines", "N", 1.0)
    
    --Variables.
    local sSanyaForm = VM_GetVar("Root/Variables/Global/Sanya/sForm", "S")

    --Movement.
    fnCutsceneMove("Sanya", 12.25, 7.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Nurse Zeke![P] How is the patient?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneFace("Zeke", -1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    fnCutsceneMove("Zeke", 13.25, 7.50, 2.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Zeke:[VOICE|Zeke] Bah bah bahhhh!!![B][C]") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Sheesh, I was only gone for a minute.[P] I found my first aid kit, so let's get cracking.[B][C]") ]])
    fnCutscene([[ Append("Zeke:[VOICE|Zeke] ...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Look it was a poor choice of words.[P] I can't believe I'm taking sass from a goat.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Sanya", 12.25, 6.50)
    fnCutsceneBlocker()
    fnCutsceneMove("Sanya", 16.25, 6.50)
    fnCutsceneFace("Sanya", 0, -1)
    fnCutsceneMove("Zeke", 12.25, 7.50)
    fnCutsceneMove("Zeke", 12.25, 6.50)
    fnCutsceneMove("Zeke", 15.25, 6.50)
    fnCutsceneFace("Zeke", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Screen fades out.
    fnCutscene([[ AL_SetProperty("Activate Fade", 125, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Zeke", "Neutral") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] All right let's see.[P] Huh.[P] All her cuts and bruises are gone.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Sad] Yeah, Zeke?[P] Pretty sure magic is real and we're in an alternate universe or something.[B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] Nyeh.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Offended] Don't act like you knew all along, you were as confused as I was.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Sad] Hmm, okay, looks like her...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Sad] Yep, legs are still broken.[P] Spine's basically in pieces.[P] She's not walking until those heal.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Sad] I can probably at least try to set them.[P] We'll need to make some bandages, cut some wood...[B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] Bah?[P] *Chomp*[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Angry] Hey, stop that![P] It's a tail not a chew toy![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Oh, you're right.[P] Better set the tailbone too.[B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] Nyehehe...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Laugh] You're a great nurse, boy.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Happy] I've done all I can for now, we'll have to figure out how to get fiber for bandages tomorrow.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Come on, let's huddle together for warmth, buddy.[P] She's got the only bed.[B][C]") ]])
    if(sSanyaForm == "Human") then
        fnCutscene([[ Append("Zeke:[E|Neutral] Nyeeeeehhhh.") ]])
    else
        fnCutscene([[ Append("Zeke:[E|Neutral] Nyeh?[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Blush] What?[P] Come on, my heart is stone literally, not metaphorically.[P] It's okay to cuddle.[P] Very masculine.[P] Chicks dig it.[B][C]") ]])
        fnCutscene([[ Append("Zeke:[E|Neutral] ...[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] Yeah, okay.[P] I'll transform back, buddy.[B][C]") ]])
        fnCutscene([[ Append("Zeke:[E|Neutral] Nyeh!") ]])
    end
    
    fnCutsceneBlocker()
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    
    --Next scene.
    VM_SetVar("Root/Variables/Chapter2/SanyaMines/iIsNightAtMines", "N", 0.0)
    fnCutscene([[ AL_BeginTransitionTo("SanyaCabinA", "FORCEPOS:25.0x31.0x0") ]])

-- |[ ======================================= Taking Care ====================================== ]|
--Scenes when Sanya is taking care of Izuna. This one is the first feeding scene.
elseif(sObjectName == "Feeding") then

    --Flag.
    VM_SetVar("Root/Variables/Chapter2/Northwoods/iUpgradedCabin", "N", 1.0)
    
    --No music.
    AL_SetProperty("Music", "Null")

    --Cut to black.
    fnCutscene([[ AL_SetProperty("Activate Fade", 0, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Spawn Zeke. Make him sparkle.
    fnSpecialCharacter("Zeke", -100, -100, gci_Face_South, false, nil)
    EM_PushEntity("Zeke")
        TA_SetProperty("Set Auto Overlays", true)
        TA_SetProperty("Auto Overlay TPF", 8.0)
        TA_SetProperty("Allocate Auto Overlays", 16)
        for i = 0, 15, 1 do
            local sPath = string.format("Root/Images/Sprites/Sparkles/%02i", i)
            TA_SetProperty("Auto Overlay Frame", i, sPath)
        end
    DL_PopActiveObject()
    
    --Camera focuses on Izuna.
    fnCutsceneCamera("Position", gcfCamera_Cutscene_Speed, 16.25, 5.50)
    
    --Position characters.
    fnCutsceneTeleport("Sanya", 8.25, 15.50)
    fnCutsceneTeleport("Zeke", 7.25, 15.50)
    
    --Wait a bit.
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Fade in, start moving.
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneMove("Sanya",  9.25, 14.50)
    fnCutsceneMove("Sanya",  9.25,  7.50)
    fnCutsceneMove("Sanya", 16.25,  7.50)
    fnCutsceneMove("Sanya", 16.25,  6.50)
    fnCutsceneMove("Zeke",   9.25, 14.50)
    fnCutsceneMove("Zeke",   9.25,  7.50)
    fnCutsceneMove("Zeke",  15.25,  7.50)
    fnCutsceneMove("Zeke",  15.25,  6.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Here goes nothing.[P] Let's hope she likes it.[B][C]") ]])
    fnCutscene([[ Append("Girl:[VOICE|Izuna] Mmm...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] ...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Guess she's aware enough to chew.[P] Hey![P] Wake up![B][C]") ]])
    fnCutscene([[ Append("Girl:[VOICE|Izuna] ...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Do people in comas normally chew food?[B][C]") ]])
    fnCutscene([[ Append("Zeke:[VOICE|Zeke] Nyeh?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] You know, if this is magic too, I'm not actually going to be pissed off.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] I don't know about you, but I wasn't looking forward to pre-chewing her food for her.[B][C]") ]])
    fnCutscene([[ Append("Zeke:[VOICE|Zeke] Bah.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] I guess all we can do now is take care of her and hope she wakes up soon.") ]])
    fnCutsceneBlocker()
    
    --Cut to next scene.
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Transition.
    fnCutscene([[ AL_BeginTransitionTo("SanyaCabinA", "FORCEPOS:35.0x31.0x0") ]])

--Second part of the cutscene in this map.
elseif(sObjectName == "WarmUp") then

    --No music.
    AL_SetProperty("Music", "Null")
	
	--Switch Izuna's costume.
	LM_ExecuteScript(gsRoot .. "CostumeHandlers/Izuna/Miko_Normal.lua")
    
    --Variables.
    local iHasSevaviForm = VM_GetVar("Root/Variables/Global/Sanya/iHasSevaviForm", "N")
    
    --Cut to black.
    fnCutscene([[ AL_SetProperty("Activate Fade", 0, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Spawn Zeke and Izuna.
    fnSpecialCharacter("Izuna", -100, -100, gci_Face_South, false, nil)
    fnSpecialCharacter("Zeke", -100, -100, gci_Face_South, false, nil)
    
    --Position characters.
    fnCutsceneTeleport("Izuna", 5.25, 8.50)
    fnCutsceneFace("Izuna", 0, 1)
    fnCutsceneTeleport("Zeke", 6.25, 9.50)
    fnCutsceneFace("Zeke", -1, 0)
    fnCutsceneTeleport("Sanya", 4.25, 9.50)
    fnCutsceneFace("Sanya", 1, 0)
    
    --Start a fire.
    AL_SetProperty("Set Layer Disabled", "Fire", false)

    --Wait a bit.
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Fade in.
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Sanya", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Zeke", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Izuna", "Neutral") ]])
    fnCutscene([[ Append("Girl: [FOCUS|Izuna][E|Ugh] Brrrr...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] You sure you don't want a blanket?[P] Or rather, [P]*the*[P] blanket because we only have one.[B][C]") ]])
    fnCutscene([[ Append("Girl: [FOCUS|Izuna][E|Ugh]I will be fine, I'm used to the cold.[B][C]") ]])
    fnCutscene([[ Append("Girl: [FOCUS|Izuna][E|Neutral]I'm sorry for yelling at you like that.[P] I suppose it makes perfect sense that my clothes would be soaked after I fall into a river![P] Ha ha![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Sad] Uhhh...[B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] Bahh...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Sad] That was like, two weeks ago.[B][C]") ]])
    fnCutscene([[ Append("Girl: [FOCUS|Izuna][E|Ugh]Two weeks?[P] B-[P]b-[P]but...[B][C]") ]])
    fnCutscene([[ Append("Girl: [FOCUS|Izuna][E|Cry]Then you undressed me![P] You pervert![P] Sicko![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Surprised] Hey it isn't like that, sheesh![P] I've been taking care of you this whole time![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Offended] I took your clothes off to give you a bath and cleaned them while I was at it![B][C]") ]])
    fnCutscene([[ Append("Girl: [FOCUS|Izuna][E|Cry]YOU GAVE ME A BATH?[P] WITHOUT MY PERMISSION?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Offended] How am I supposed to ask you if it's okay if you're unconscious![B][C]") ]])
    fnCutscene([[ Append("Girl: [FOCUS|Izuna][E|Ugh]Wake me up![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Offended] How!?[P] HOW!?[B][C]") ]])
    fnCutscene([[ Append("Girl: [FOCUS|Izuna][E|Ugh] I -[P] I am a fool.[P] Of course you would not know how to dispel a healing coma.[B][C]") ]])
    fnCutscene([[ Append("Girl: [FOCUS|Izuna][E|Ugh]But that doesn't make it okay![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Offended] Zeke and I are about two seconds from leaving you all by yourself if you don't start explaining what the hell is going on![B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] Nyeh![P] Nyeh![B][C]") ]])
    fnCutscene([[ Append("Girl: [FOCUS|Izuna][E|Neutral]Oh, your name is Zeke?[P] Hello Zeke![B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] My name is Izuna.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] And - [P][CLEAR]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] Your name is Sanya.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Offended] One second![P] One second and we walk![B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] Okay, okay, I'm sorry.[P] This is not at all going how I thought it would.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Ugh] I didn't think the chosen hero would be so -[P] naughty![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Angry] Half a second![P] Don't make me go into decimals because I am really bad at math![B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] All right.[P] Hm.[P] Where to begin.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] Do you know where you are right now?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Not at all.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Explain] This place is called Trafal.[P] We are on a world called Pandemonium.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Explain] I know that will seem strange to you, but the texts tell of a prophecy made long ago.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Explain] Centuries ago, this place was the seat of a great dragon empire.[P] But ruin came one day.[P] A terrible monster that slaughtered the empire's armies and razed its cities.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] With great sacrifice, they sealed the beast in a tower on the tallest mountain.[P] The empire fell apart shortly after.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] And they built several special shrines knowing that a great hero would come to slay the beast once and for all.[P] That hero is you, Sanya.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Blush] ...[P] Holy cow, you really deliver, don't you.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] Thanks.[P] I've been waiting to give you this speech for...[P] I suppose months now.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] So you knew about me because of this prophecy or whatever?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] Yep![P] The Shrine of the Hero is where I was looking, because the great hero is supposed to have appeared there.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Explain] And lo and behold, there she is![P] Right here in front of me![P] And she even rescued me from drowning.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Ugh] *And probably snuck a look at me while I was lying helpless*.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Offended] Hey![P] I left your underpants on, didn't I?[P] I'm not a pervert![B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] Well I suppose you did...[B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] Bah.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] I suppose someone who treats their goat well can't be all bad.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] So if I'm right about this, I'm here on a grand quest to kill the ultimate evil?[P] How are you so sure it's me?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Laugh] I've never kicked the ass of a giant beast or anything.[P] Is it big, like a building?[P] Can you kick a building's ass?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] Mwehehe![B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] The hero is to be clad in red, wielding a stick of fire, and able to change her shape.[P] She shall be preceded by a symbol.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] The symbol is on your runestone.[P] You've got a runestone, right?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Sad] Y-[P]yeah.[P] It's an heirloom.[P] Belonged to my great grandpa.[P] My grandpa gave it to me...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] The 'stick of fire' did, too...[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Explain] Hey, we have rifles in this day and age, you know.[P] Keep in mind this prophecy is centuries old.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] Though I've never seen a rifle like that one.[P] It's very impressive.[B][C]") ]])
    
    -- |[Can Transform]|
    if(iHasSevaviForm == 1.0) then
        fnCutscene([[ Append("Sanya:[E|Blush] And about the changing her shape thing -[P] you know about my transformations?[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Explain] Of course![P] Obviously you've encountered the sevavi.[P] The ancient stone guardians protect the shrines in this land.[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Explain] I'm a kitsune.[P] There are many monstergirls across the lands, too.[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Smirk] You'll need all of our powers to take on Perfect Hatred.[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Blush] Can I...[P] Can I see it?[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Happy] Sure![P] Hold on a second, I have to focus.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        Cutscene_CreateEvent("Flash Leader White", "Actor")
            ActorEvent_SetProperty("Subject Name", gsPartyLeaderName)
            ActorEvent_SetProperty("Flashwhite Quickly")
        DL_PopActiveObject()
        fnCutsceneWait(5)
        fnCutsceneBlocker()
        fnCutsceneCall(gsRoot .. "FormHandlers/Sanya/Form_Sevavi.lua")
        fnCutsceneWait(gci_Flashwhite_Ticks_Total)
        fnCutsceneBlocker()
        
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Sanya", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Zeke", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Izuna", "Neutral") ]])
        fnCutscene([[ Append("Sanya:[E|Happy] Kaboom![P] Hey, look at that, my clothes came along for the ride.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Happy] I hadn't noticed that before.[P] Neat![B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Smirk] There can be no doubt.[P] You must truly be the one.[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Blush] (They're so -[P] big![P] She's huge!)[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] Do you mind if I stay human for the time being?[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Neutral] It's no trouble, but why?[P] You can be anything you want.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Smirk] It's kind of like being at work.[P] When I'm not a human, I'm a little tense, active.[P] It's harder to relax.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] Maybe that will go away as I get more used to being a rock.[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Smirk] I see.[P] It could be that you are not truly a statue, because of the runestone.[P] Me, I am truly a kitsune, and Zeke is truly a goat.[B][C]") ]])
        fnCutscene([[ Append("Zeke:[E|Neutral] Nyeh.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] Yeah that's a bit too existential for me.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        Cutscene_CreateEvent("Flash Leader White", "Actor")
            ActorEvent_SetProperty("Subject Name", gsPartyLeaderName)
            ActorEvent_SetProperty("Flashwhite Quickly")
        DL_PopActiveObject()
        fnCutsceneWait(15)
        fnCutsceneBlocker()
        fnCutsceneCall(gsRoot .. "FormHandlers/Sanya/Form_Human.lua")
        fnCutsceneWait(gci_Flashwhite_Ticks_Total)
        fnCutsceneBlocker()
        
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Sanya", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Zeke", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Izuna", "Neutral") ]])
        fnCutscene([[ Append("Sanya:[E|Blush] Oh that's much better.[P] Now, where were we?[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Neutral] The great villain of legend, I believe.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Laugh] Oh yeah, 'Perfect Hatred'.[B][C]") ]])
        fnCutscene([[ Append("Zeke:[E|Neutral] Nyeh nyeh nyeh![B][C]") ]])
    
    -- |[No TFs]|
    else
        fnCutscene([[ Append("Sanya:[E|Neutral] And about the changing her shape thing -[P] how is it that I know what you're talking about?[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Blush] If you transformed me...[P] into one of you...[P] I could turn back.[P] How do I know that?[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Neutral] It is the power of your runestone.[P] It has always been with you, and it is unlocked because now, you need it.[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Explain] I am what is called a 'kitsune'.[P] We are the fox people of the glacier.[P] There are many more monstergirls whose forms you will need to assume.[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Neutral] You'll need all of our powers to take on Perfect Hatred.[B][C]") ]])
    end
    fnCutscene([[ Append("Sanya:[E|Laugh] Is that the name of the great evil?[P] *Because it sounds kinda lame*.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] Of course it sounds lame, but it was probably really scary seven centuries ago.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] But, as a servant of the Fox Goddess, I am here to offer my services to you.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Blush] You'll need a guide to help you master this place.[P] I -[P] will you let me be that guide?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] I guess so.[P] You're already being pretty helpful.[P] What do you think, Zeke?[B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] Bah![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Blush] Yeah but you can't judge her based on looks alone.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Happy] !!![P] Amazing![P] How did you know what he was saying?[B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] ...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] ...[P] I don't.[P] He's a goat.[B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] Nyeh.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Happy] But Zeke is as smart as he is tough, and he's plenty tough![B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] Bah![B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] Ah -[P] oh.[P] I get it.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] I really did think you could understand him.[P] You see, your runestone translates languages for you.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] It's how we're speaking right now.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] Oh good, I was worried that all the signs I've seen weren't written in whatever language Swiss people speak.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] Mwehehe![P] (What is a Swiss person?)[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] Okay![P] Let's get going!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Izuna moves.
    fnCutsceneMove("Izuna", 7.25, 8.50, 1.20)
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutscenePlaySound("World|Thump")
    fnCutsceneSetFrame("Izuna", "Wounded")
    fnCutsceneBlocker()
    fnCutsceneFace("Sanya", 0, -1)
    fnCutsceneFace("Zeke", 1, -1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Sanya", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Zeke", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Izuna", "Neutral") ]])
    fnCutscene([[ Append("Izuna:[E|Ugh] Owie...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Sad] Come on, I'll help you back to your bed.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Ugh] B-[P]but we must go at once![P] The seal is weakening as we speak![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Sad] You can barely walk, we're not going anywhere.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Sad] You need to finish healing.[P] Here, hold on to me.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Sanya", 4.25, 8.50)
    fnCutsceneMove("Sanya", 6.25, 8.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneSetFrame("Izuna", "Crouch")
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Sanya", "Blush") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Zeke", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Izuna", "Neutral") ]])
    fnCutscene([[ Append("Izuna:[E|Blush] You're so strong...[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Blush] And firm...[P] and...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Blush] Come on, back to bed with you.") ]])
    fnCutsceneBlocker()
    
    --Fade.
    fnCutscene([[ AL_SetProperty("Activate Fade", 125, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    
    --Next scene.
    fnCutscene([[ AL_BeginTransitionTo("SanyaCabinA", "FORCEPOS:40.0x31.0x0") ]])
end
