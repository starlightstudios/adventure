-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[Name Resolve]|
    --Resolve the name of the calling entity.
    local sActorName = TA_GetProperty("Name")

    --Set facing.
    TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[NPCName]|
    if(sActorName == "Odar") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (He's unconscious, but he'll be fine when he wakes up.[P] Assuming I didn't liquify whatever these mushroom men use for brains.)") ]])
        fnCutsceneBlocker()
    end
    
-- |[ ====================================== "Shot Check" ====================================== ]|
--If this entity is targeted by a rifle shot, this section of code returns whether or not a response is merited.
elseif(sTopicName == "Shot Check") then

    -- |[Name Resolve]|
    --Resolve the name of the calling entity.
    local sActorName = TA_GetProperty("Name")

    --Set facing.
    TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[NPCName]|
    if(sActorName == "Odar") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (He was kind of a dink, but I'm not going to just shoot him.)") ]])
        fnCutsceneBlocker()
        
        --Flags.
        AL_SetProperty("Examinable Reply To Hit")
        AL_SetProperty("Examinable Reply To Hit Immediately")
        AL_SetProperty("Examinable Stop Firing Sequence")
    end
    
-- |[ ===================================== "Shot Handle" ====================================== ]|
--If this entity is targeted by a rifle shot, and responded to the shot check, this block of code is used.
elseif(sTopicName == "Shot Handle") then

    -- |[Name Resolve]|
    --Resolve the name of the calling entity.
    local sActorName = TA_GetProperty("Name")

    --Set facing.
    TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[NPCName]|
    if(sActorName == "Odar") then
        --Do nothing.
    end

end
