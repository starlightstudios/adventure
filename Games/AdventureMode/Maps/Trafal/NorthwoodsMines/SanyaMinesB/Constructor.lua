-- |[ ======================================= Constructor ====================================== ]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = fnResolveDirectory() --Map's name is the name of the folder it is in.
local sLevelMusic = "TrafalCave"
local sMapResolveName = "Map Resolve Name"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[ ======================================== Standard ======================================== ]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

    -- |[Construction]|
	--Music.
	AL_SetProperty("Music", sLevelMusic)
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation(sMapResolveName)

	-- |[Lights]|
    AL_SetProperty("Activate Lights")
    
    -- |[Shooting Setup]|
    AL_SetProperty("Shooting Depth", 1)

-- |[ ======================================== Post-Exec ======================================= ]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	-- |[NPC Spawning]|
	--If an entity needs to spawn after a post-exec cutscene, put them here.
    fnSpawnNPCPattern("Block", "A", "C")
    fnSpawnNPCPattern("Switch", "A", "C")
    
    --Odar appears here if the player won the puzzle fight and has not yet left the mines.
    local iCompletedMines = VM_GetVar("Root/Variables/Chapter2/SanyaMines/iCompletedMines", "N")
    local iWonPuzzleFight = VM_GetVar("Root/Variables/Chapter2/SanyaMines/iWonPuzzleFight", "N")
    if(iCompletedMines == 0.0 and iWonPuzzleFight == 1.0) then
        fnSpecialCharacter("Odar", -100, -100, gci_Face_South, true, fnResolvePath() .. "Dialogue.lua")
        fnCutsceneTeleport("Odar", 33.75, 25.50)
        fnCutsceneSetFrame("Odar", "Wounded")
        
        --Responds when shot.
        EM_PushEntity("Odar")
            TA_SetProperty("Responds To Shot", true)
        DL_PopActiveObject()
    end

    -- |[Overlays]|
    --Place overlay code here.
    AL_SetProperty("Add Dislocation", "Cart Dislocation", "Cart", 0, 0, 3, 2, 19*16, 21*16)
    local iMinesCartAtNorth = VM_GetVar("Root/Variables/Chapter2/SanyaMines/iMinesCartAtNorth", "N")
    if(iMinesCartAtNorth == 1.0) then
        AL_SetProperty("Set Collision", 19, 21, 0, 0)
        AL_SetProperty("Set Collision", 20, 21, 0, 0)
        AL_SetProperty("Set Collision", 21, 21, 0, 0)
        AL_SetProperty("Set Collision", 19, 22, 0, 0)
        AL_SetProperty("Set Collision", 20, 22, 0, 0)
        AL_SetProperty("Set Collision", 21, 22, 0, 0)
    else
        AL_SetProperty("Modify Dislocation", "Cart Dislocation", 19*16, 26*16)
        AL_SetProperty("Set Collision", 19, 26, 0, 0)
        AL_SetProperty("Set Collision", 20, 26, 0, 0)
        AL_SetProperty("Set Collision", 21, 26, 0, 0)
        AL_SetProperty("Set Collision", 19, 27, 0, 0)
        AL_SetProperty("Set Collision", 20, 27, 0, 0)
        AL_SetProperty("Set Collision", 21, 27, 0, 0)
    end
    
    --Layers.
    local iDidPuzzleFight = VM_GetVar("Root/Variables/Chapter2/SanyaMines/iDidPuzzleFight", "N")
    if(iDidPuzzleFight == 0.0) then
        AL_SetProperty("Set Layer Disabled", "BoardsLBroke", true)
        AL_SetProperty("Set Layer Disabled", "BoardsUBroke", true)
        AL_SetProperty("Set Collision", 33, 17, 0, 1)
        AL_SetProperty("Set Collision", 34, 17, 0, 1)
    else
        AL_SetProperty("Set Layer Disabled", "BoardsL", true)
        AL_SetProperty("Set Layer Disabled", "BoardsU", true)
        AL_SetProperty("Set Collision", 33, 17, 0, 0)
        AL_SetProperty("Set Collision", 34, 17, 0, 0)
    end
    
    -- |[Skillbooks]|
    --Place skillbook overlay code here.
    
    -- |[Rifle Handling]|
    VM_SetVar("Root/Variables/Global/Sanya/iRifleCode", "N", gciSanyaRifle_CanFire)

end
