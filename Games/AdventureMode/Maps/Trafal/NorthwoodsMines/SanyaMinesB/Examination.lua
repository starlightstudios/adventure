-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= Exits ========================================== ]|
if(sObjectName == "SampleExit") then
    
-- |[ ====================================== Examinables ======================================= ]|
elseif(sObjectName == "Pickaxe") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A rusty pickaxe.)") ]])

-- |[Minecart Handler]|
--Manual activation from close range.
elseif(sObjectName == "CartSwitch") then

    --Sound.
    AudioManager_PlaySound("World|FlipSwitch")
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Movement.
    fnCutsceneMove("Sanya", 20.25, 22.50)
    
    --If any additional party members are present, move them as well:
    if(EM_Exists("Izuna") == true) then
        fnCutsceneMove("Izuna", 20.25, 22.50)
    end
    if(EM_Exists("Empress") == true) then
        fnCutsceneMove("Empress", 20.25, 22.50)
    end
    if(EM_Exists("Zeke") == true) then
        fnCutsceneMove("Zeke", 20.25, 22.50)
    end
    
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Sound.
    fnCutscene([[ AudioManager_PlaySound("World|TramRunning") ]])
    
    --Dislocation.
    for y = 21*16, 26*16, 1 do
        local sString = "AL_SetProperty(\"Modify Dislocation\", \"Cart Dislocation\", 19*16, " .. y .. ")"
        fnCutscene(sString)
        fnCutsceneTeleport("Sanya", 20.25, (y/16.0) + 1.50)
                
        --If any additional party members are present, move them as well:
        if(EM_Exists("Izuna") == true) then
            fnCutsceneTeleport("Izuna", 20.25, (y/16.0) + 1.50)
        end
        if(EM_Exists("Empress") == true) then
            fnCutsceneTeleport("Empress", 20.25, (y/16.0) + 1.50)
        end
        if(EM_Exists("Zeke") == true) then
            fnCutsceneTeleport("Zeke", 20.25, (y/16.0) + 1.50)
        end
        fnCutsceneBlocker()
    end
    
    fnCutscene([[ AudioManager_StopSound("World|TramRunning") ]])
    fnAutoFoldParty()
    
    --Collisions.
    for x = 19, 21, 1 do
        for y = 21, 22, 1 do
            local sString = "AL_SetProperty(\"Set Collision\", " .. x .. ", " .. y .. ", 0, 1)"
            fnCutscene(sString)
        end
        for y = 26, 27, 1 do
            local sString = "AL_SetProperty(\"Set Collision\", " .. x .. ", " .. y .. ", 0, 0)"
            fnCutscene(sString)
        end
    end
    
    --Set the cart to spawn at the south end.
    VM_SetVar("Root/Variables/Chapter2/SanyaMines/iMinesCartAtNorth", "N", 0.0)

--Checking if the switch responds to being shot.
elseif(sObjectName == "CartSwitch SHOT CHECK") then
    AL_SetProperty("Examinable Reply To Hit")

--If shot, this script executes when Sanya has finished her animations.
elseif(sObjectName == "CartSwitch SHOT HANDLER") then
    
    -- |[Common]|
    --Get the player's position.
    EM_PushEntity(gsPartyLeaderName)
        local fPlayerX, fPlayerY = TA_GetProperty("Position")
    DL_PopActiveObject()
    
    --Set to tile coordinates.
    fPlayerX = fPlayerX / gciSizePerTile
    fPlayerY = fPlayerY / gciSizePerTile
    
    --Other variables.
    local iSanyaStupidStupid = VM_GetVar("Root/Variables/Chapter2/SanyaMines/iSanyaStupidStupid", "N")
    
    -- |[North Side]|
    --Cart is on the north end, move it south.
    local iMinesCartAtNorth = VM_GetVar("Root/Variables/Chapter2/SanyaMines/iMinesCartAtNorth", "N")
    if(iMinesCartAtNorth == 1.0) then

        --Common.
        fnCutscene([[ AudioManager_PlaySound("World|TramRunning") ]])

        --Hitbox. If the player is on the cart:
        if(fPlayerX >= 19.0 and fPlayerX < 22.0 and fPlayerY >= 21.0 and fPlayerY <= 22.9) then
            local iCounter = 0
            for y = 21*16, 26*16, 1 do
                local sString = "AL_SetProperty(\"Modify Dislocation\", \"Cart Dislocation\", 19*16, " .. y .. ")"
                fnCutscene(sString)
                fnCutsceneTeleport("Sanya", fPlayerX, fPlayerY + (iCounter / gciSizePerTile))
                
                --If any additional party members are present, move them as well:
                if(EM_Exists("Izuna") == true) then
                    fnCutsceneTeleport("Izuna", fPlayerX, fPlayerY + (iCounter / gciSizePerTile))
                end
                if(EM_Exists("Empress") == true) then
                    fnCutsceneTeleport("Empress", fPlayerX, fPlayerY + (iCounter / gciSizePerTile))
                end
                if(EM_Exists("Zeke") == true) then
                    fnCutsceneTeleport("Zeke", fPlayerX, fPlayerY + (iCounter / gciSizePerTile))
                end
                fnCutsceneBlocker()
                iCounter = iCounter + 1
            end
            
        
        --Player is not on the cart.
        else
            for y = 21*16, 26*16, 1 do
                local sString = "AL_SetProperty(\"Modify Dislocation\", \"Cart Dislocation\", 19*16, " .. y .. ")"
                fnCutscene(sString)
                fnCutsceneWait(2)
                fnCutsceneBlocker()
            end
        end
        
        --Common.
        fnCutscene([[ AudioManager_StopSound("World|TramRunning") ]])
        VM_SetVar("Root/Variables/Chapter2/SanyaMines/iMinesCartAtNorth", "N", 0.0)
        fnAutoFoldParty()
        fnCutsceneBlocker()
        
        --Collisions.
        for x = 19, 21, 1 do
            for y = 21, 22, 1 do
                AL_SetProperty("Set Collision", x, y, 0, 1)
            end
            for y = 26, 27, 1 do
                AL_SetProperty("Set Collision", x, y, 0, 0)
            end
        end
        
    -- |[South Side]|
    else

        --Common.
        local bPlayDumbScene = false
        fnCutscene([[ AudioManager_PlaySound("World|TramRunning") ]])

        --Hitbox. If the player is on the cart:
        if(fPlayerX >= 19.0 and fPlayerX < 22.0 and fPlayerY >= 26.0 and fPlayerY <= 27.9) then
            local iCounter = 0
            for y = 26*16, 21*16, -1 do
                local sString = "AL_SetProperty(\"Modify Dislocation\", \"Cart Dislocation\", 19*16, " .. y .. ")"
                fnCutscene(sString)
                fnCutsceneTeleport("Sanya", fPlayerX, fPlayerY - (iCounter / gciSizePerTile))
                
                --If any additional party members are present, move them as well:
                if(EM_Exists("Izuna") == true) then
                    fnCutsceneTeleport("Izuna", fPlayerX, fPlayerY - (iCounter / gciSizePerTile))
                end
                if(EM_Exists("Empress") == true) then
                    fnCutsceneTeleport("Empress", fPlayerX, fPlayerY - (iCounter / gciSizePerTile))
                end
                if(EM_Exists("Zeke") == true) then
                    fnCutsceneTeleport("Zeke", fPlayerX, fPlayerY - (iCounter / gciSizePerTile))
                end
                fnCutsceneBlocker()
                iCounter = iCounter + 1
            end
        
        --Player is not on the cart.
        else
            bPlayDumbScene = true
            for y = 26*16, 21*16, -1 do
                local sString = "AL_SetProperty(\"Modify Dislocation\", \"Cart Dislocation\", 19*16, " .. y .. ")"
                fnCutscene(sString)
                fnCutsceneWait(2)
                fnCutsceneBlocker()
            end
        end
        
        --Other.
        local iCompletedHunt = VM_GetVar("Root/Variables/Chapter2/Northwoods/iCompletedHunt", "N")
        if(iCompletedHunt == 1.0) then bPlayDumbScene = false end
        
        --Common.
        fnCutscene([[ AudioManager_StopSound("World|TramRunning") ]])
        VM_SetVar("Root/Variables/Chapter2/SanyaMines/iMinesCartAtNorth", "N", 1.0)
        fnAutoFoldParty()
        fnCutsceneBlocker()
        
        --Collisions.
        for x = 19, 21, 1 do
            for y = 21, 22, 1 do
                AL_SetProperty("Set Collision", x, y, 0, 0)
            end
            for y = 26, 27, 1 do
                AL_SetProperty("Set Collision", x, y, 0, 1)
            end
        end
        
        --Extra scene:
        if(bPlayDumbScene) then
            VM_SetVar("Root/Variables/Chapter2/SanyaMines/iSanyaStupidStupid", "N", 1.0)
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] Wah ha ha![P] It worked![B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] I'm so smart I forgot to actually stand on the stupid platform![P] Stupid![P] I'm so stupid![B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] Well it shouldn't be a problem to just STAND ON THE PLATFORM and SHOOT THE SWITCH AGAIN.") ]])
            fnCutsceneBlocker()
        end
    end

-- |[Other Objects]|
elseif(sObjectName == "BrokenSwitch") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Nothing happens.[P] Seems to be broken.)") ]])
    fnCutsceneBlocker()

-- |[ ========================================== Other ========================================= ]|
-- |[ ========================================== Debug ========================================= ]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
