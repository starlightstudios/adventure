-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
if(sObjectName == "MysteryLight") then

    --Repeat check.
    local iSawMysteryLightAgain = VM_GetVar("Root/Variables/Chapter2/SanyaMines/iSawMysteryLightAgain", "N")
    if(iSawMysteryLightAgain == 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter2/SanyaMines/iSawMysteryLightAgain", "N", 1.0)

    --Spawn Odar.
    TA_Create("Odar")
        TA_SetProperty("Position", 41, 29)
        TA_SetProperty("Facing", gci_Face_West)
        TA_SetProperty("Clipping Flag", false)
        TA_SetProperty("Activation Script", "Null")
        fnSetCharacterGraphics("Root/Images/Sprites/Odar_Prince/", true)
    DL_PopActiveObject()

    --Place a light on Odar.
    AL_SetProperty("Register Radial Light", "Odar Light", 0.0, 0.0, 10000.0)
    AL_SetProperty("Attach Light To Entity", "Odar Light", "Odar")
    
    --Camera movement.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 3.0)
        CameraEvent_SetProperty("Focus Position", (41.25 * gciSizePerTile), (29.50 * gciSizePerTile))
    DL_PopActiveObject()
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Odar", 41.25, 25.50)
    fnCutsceneBlocker()
    fnCutsceneTeleport("Odar", -100.25, -100.50)
    fnCutsceneBlocker()
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 3.0)
        CameraEvent_SetProperty("Focus Actor Name", "Sanya")
    DL_PopActiveObject()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])
    fnCutscene([[ Append("Sanya:[E|Offended] Is this a trap?[P] This is starting to feel like a trap.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Offended] But why would anyone want to trap me?[P] I'm the nicest person in the entire world![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Angry] I'm so nice I'm only going to break one of their legs when I catch them![P] I could do worse![P] I'm leaving you a spare![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] They had better have a freaking phone or something after all this bullshit.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] How does this contraption work, anyway?[P] Is that blue thing a switch?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
elseif(sObjectName == "DieHere") then
    
    --Repeat check.
    local iDieHere = VM_GetVar("Root/Variables/Chapter2/SanyaMines/iDieHere", "N")
    if(iDieHere == 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter2/SanyaMines/iDieHere", "N", 1.0)
    
    --Variables.
    local sSanyaForm = VM_GetVar("Root/Variables/Global/Sanya/sForm", "S")
    
    --Movement.
    fnCutsceneMove("Sanya", 20.25, 29.50)
    fnCutsceneFace("Sanya", 1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Sanya:[VOICE|Sanya] Well that's pretty neat![P] Let's see if this one works.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Sanya", 22.25, 29.50)
    fnCutsceneFace("Sanya", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|FlipSwitch") ]])
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|FlipSwitch") ]])
    fnCutsceneWait(10)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|FlipSwitch") ]])
    fnCutsceneWait(10)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|FlipSwitch") ]])
    fnCutsceneWait(180)
    fnCutsceneBlocker()
    fnCutsceneFace("Sanya", -1, -1)
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Ah, I see.[P] It's one of those switches that doesn't work.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Offended] Of course it doesn't work, everything is going wrong today.[B][C]") ]])
    if(sSanyaForm == "Human") then
        fnCutscene([[ Append("Sanya:[E|Neutral] I guess all I can do is try to jump the gap and hopefully paint the bottom of this pit a nice color.[B][C]") ]])
    elseif(sSanyaForm == "Sevavi") then
        fnCutscene([[ Append("Sanya:[E|Neutral] I guess all I can do is try to jump the gap and kill everyone below.[P] Rocks fall, everyone dies.[B][C]") ]])
    end
    fnCutscene([[ Append("Sanya:[E|Offended] Guess I'll keep following that light into what is obviously a trap![P] Whoopee!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

-- |[ ================================== Punching Odar Scene =================================== ]|
elseif(sObjectName == "PunchOdar") then

    -- |[Setup]|
    --Set flag.
    VM_SetVar("Root/Variables/Chapter2/SanyaMines/iDidPuzzleFight", "N", 1.0)
    
    --Reset the camera zoom.
    AL_SetProperty("Camera Zoom", 3.0)
    
    --Layer setup.
    fnCutsceneLayerDisabled("BoardsLBroke", true)
    fnCutsceneLayerDisabled("BoardsUBroke", true)
    fnCutsceneLayerDisabled("BoardsL", false)
    fnCutsceneLayerDisabled("BoardsU", false)
    
    --Fragement pieces.
    local sFragmentPrefix = "Root/Images/Sprites/Fragments/"
    local saFragmentList = {"Wood0", "Wood1", "Wood2", "Rock0", "Rock1"}
    
    --Spawn random fragments.
    local iFragments = 25
    for i = 0, iFragments-1, 1 do
        
        --Roll.
        local iFragmentRoll = LM_GetRandomNumber(1, #saFragmentList)
        
        --Create.
        TA_Create("Fragment"..i)
            TA_SetProperty("Position", -10, -10)
            TA_SetProperty("Facing", gci_Face_South)
            TA_SetProperty("Clipping Flag", false)
            TA_SetProperty("Activation Script", nil)
            fnSetCharacterGraphics("Root/Images/Sprites/GenericF0/", false)
            TA_SetProperty("Wipe Special Frames")
            TA_SetProperty("Add Special Frame", "Special", sFragmentPrefix .. saFragmentList[iFragmentRoll])
        DL_PopActiveObject()
        
        --Fragment immediately goes to its special frame.
        fnCutsceneSetFrame("Fragment"..i, "Special")
    end
    
    --Move player offscreen. Spawn Odar.
    fnSpecialCharacter("Odar", -100, -100, gci_Face_South, false, fnResolvePath() .. "Dialogue.lua")
    fnCutsceneTeleport("Sanya", -100.25, -100.50)
    
    --Odar is in his Pain state.
    fnCutsceneSetFrame("Odar", "PainN1")
    
    --Collisions. Remove these before we start moving characters.
    AL_SetProperty("Set Collision", 33, 17, 0, 0)
    AL_SetProperty("Set Collision", 34, 17, 0, 0)

    -- |[Scene Begin]|
    --Fullblack.
    fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Camera focus point.
    fnCutsceneCamera("Position", gcfCamera_Cutscene_Speed, 33.75, 17.50)
    
    --Wait a bit.
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    
    --Fade in.
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    
    --Move Odar and blast the fence open.
    local bHasHitFloor = false
    fnCutscenePlaySound("World|BoardBreak")
    fnCutsceneLayerDisabled("BoardsLBroke", false)
    fnCutsceneLayerDisabled("BoardsUBroke", false)
    fnCutsceneLayerDisabled("BoardsL", true)
    fnCutsceneLayerDisabled("BoardsU", true)
    fnCutsceneTeleport("Odar", 33.75, 17.50)
    fnCutsceneBlocker()
    
    --Teleport all the fragments in and give them movement goals. Each one "jumps" a few times.
    for i = 0, iFragments-1, 1 do
        
        --Setup.
        local sName = "Fragment"..i
        
        --Position scatter.
        local iXRoll = LM_GetRandomNumber(0, 100)
        local fXPos = 33.10 + (1.80 * iXRoll / 100.0)
        local fYPos = 17.50
        
        --Place the object.
        fnCutsceneTeleport(sName, fXPos, fYPos)
        
        --Determine move distances based on percentages.
        local fYSpeed = LM_GetRandomNumber(7, 25) * 0.10
        local fXSpeed = ((iXRoll - 50) / 50.0) * 1.5
        local fSpeedTotal = fYSpeed + fXSpeed
        local iFlightTickScatter = LM_GetRandomNumber(70, 130) / 100.0
        local iFlightTicks = 35 * iFlightTickScatter
        
        --First hop.
        local fXTarget = fXPos + fXSpeed
        local fYTarget = fYPos + fYSpeed
        fnCutsceneJumpTo(sName, fXTarget, fYTarget, iFlightTicks)
        
        --Second hop is from the new position at half speed.
        fXSpeed = fXSpeed * 0.50
        fYSpeed = fYSpeed * 0.50
        fXTarget = fXTarget + fXSpeed
        fYTarget = fYTarget + fYSpeed
        fSpeedTotal = fYSpeed + fXSpeed
        iFlightTicks = math.floor(iFlightTicks * 0.70)
        fnCutsceneJumpTo(sName, fXTarget, fYTarget, iFlightTicks)
        
        --Final hop.
        fXSpeed = fXSpeed * 0.50
        fYSpeed = fYSpeed * 0.50
        fXTarget = fXTarget + fXSpeed
        fYTarget = fYTarget + fYSpeed
        fSpeedTotal = fYSpeed + fXSpeed
        iFlightTicks = math.floor(iFlightTicks * 0.70)
        fnCutsceneJumpTo(sName, fXTarget, fYTarget, iFlightTicks)
    end
    
    --Odar flies to the south. He hits the floor and slides a bit.
    fnCutsceneCamera("Actor Name", gcfCamera_Cutscene_Speed, "Odar")
    fnCutsceneMove("Odar", 33.75, 20.50, 2.50)
    for y = 20.50, 25.50, 0.50 do
        
        --Computations.
        local fPercent = (y - 20.50) / (25.50 - 20.50)
        local fSpeed = 0.50 + (2.00 * (1.0 - fPercent))
        fnCutsceneMove("Odar", 33.75, y, fSpeed)
        
        --Special frames.
        if(fPercent < 0.50) then
        elseif(fPercent < 0.75) then
            fnCutsceneSetFrame("Odar", "Crouch")
        else
            if(bHasHitFloor == false) then
                fnCutscenePlaySound("World|Slide")
            end
            bHasHitFloor = true
            fnCutsceneSetFrame("Odar", "Wounded")
        end
    end
    fnCutsceneBlocker()
    fnCutsceneWait(185)
    fnCutsceneBlocker()
    
    --Spawn sanya and move her.
    fnCutsceneTeleport("Sanya", 34.25, 17.50)
    fnCutsceneBlocker()
    fnCutsceneMove("Sanya", 34.45, 25.70)
    fnCutsceneFace("Sanya", -1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutsceneCamera("Actor Name", 1.0, "Sanya")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])
    fnCutscene([[ Append("Sanya:[E|Angry] Hey, get up, so I can knock you back out![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Angry] ...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] ...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Sad] Uh oh, kinda flew off the handle there.[P] This is what happens when I don't pull my punches.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Looks like he's knocked out.[P] I guess he's still breathing, assuming these mushroom men breathe.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Sad] Uhh...[P] look pal, you did me a favour by getting my stuff out of the river, so the least I can do is use my medkit.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] [SOUND|Combat|DoctorBag]...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] So at least you won't bleed to death.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Angry] And this makes us even, jerk![P] That's what you get for making me do your stupid puzzle crap![P] I - [B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Offended] Oh crap I better get back to that cosplaying fox girl![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Happy] Okay you just stay here, [P]'Prince'[P], and I'll go hang out with people who aren't dicking me over![P] Bye!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Activate Odar's collision flag.
    TA_ChangeCollisionFlag("Odar", true)
    
-- |[ ================================ Not Punching Odar Scene ================================= ]|
elseif(sObjectName == "NoPunchOdar") then

    -- |[Setup]|
    --Set flag.
    VM_SetVar("Root/Variables/Chapter2/SanyaMines/iDidPuzzleFight", "N", 1.0)
    
    --Layer setup.
    fnCutsceneLayerDisabled("BoardsLBroke", true)
    fnCutsceneLayerDisabled("BoardsUBroke", true)
    fnCutsceneLayerDisabled("BoardsL", false)
    fnCutsceneLayerDisabled("BoardsU", false)
    
    --Fragement pieces.
    local sFragmentPrefix = "Root/Images/Sprites/Fragments/"
    local saFragmentList = {"Wood0", "Wood1", "Wood2", "Rock0", "Rock1"}
    
    --Spawn random fragments.
    local iFragments = 25
    for i = 0, iFragments-1, 1 do
        
        --Roll.
        local iFragmentRoll = LM_GetRandomNumber(1, #saFragmentList)
        
        --Create.
        TA_Create("Fragment"..i)
            TA_SetProperty("Position", -10, -10)
            TA_SetProperty("Facing", gci_Face_South)
            TA_SetProperty("Clipping Flag", false)
            TA_SetProperty("Activation Script", nil)
            fnSetCharacterGraphics("Root/Images/Sprites/GenericF0/", false)
            TA_SetProperty("Wipe Special Frames")
            TA_SetProperty("Add Special Frame", "Special", sFragmentPrefix .. saFragmentList[iFragmentRoll])
            TA_SetProperty("Set Special Frame", "Special")
        DL_PopActiveObject()
    end
    
    --Move player offscreen. Spawn Odar.
    fnCutsceneTeleport("Sanya", -100.25, -100.50)
    
    --Collisions. Remove these before we start moving characters.
    AL_SetProperty("Set Collision", 33, 17, 0, 0)
    AL_SetProperty("Set Collision", 34, 17, 0, 0)

    -- |[Scene Begin]|
    --Fullblack.
    fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Camera focus point.
    fnCutsceneCamera("Position", gcfCamera_Cutscene_Speed, 33.75, 17.50)
    
    --Wait a bit.
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    
    --Fade in.
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    
    --Move Odar and blast the fence open.
    local bHasHitFloor = false
    fnCutscenePlaySound("World|BoardBreak")
    fnCutsceneLayerDisabled("BoardsLBroke", false)
    fnCutsceneLayerDisabled("BoardsUBroke", false)
    fnCutsceneLayerDisabled("BoardsL", true)
    fnCutsceneLayerDisabled("BoardsU", true)
    fnCutsceneTeleport("Sanya", 33.75, 17.50)
    fnCutsceneFace("Sanya", 0, 1)
    fnCutsceneBlocker()
    
    --Teleport all the fragments in and give them movement goals. Each one "jumps" a few times.
    for i = 0, iFragments-1, 1 do
        
        --Setup.
        local sName = "Fragment"..i
        
        --Position scatter.
        local iXRoll = LM_GetRandomNumber(0, 100)
        local fXPos = 33.10 + (1.80 * iXRoll / 100.0)
        local fYPos = 17.50
        
        --Place the object.
        fnCutsceneTeleport(sName, fXPos, fYPos)
        
        --Determine move distances based on percentages.
        local fYSpeed = LM_GetRandomNumber(7, 25) * 0.10
        local fXSpeed = ((iXRoll - 50) / 50.0) * 1.5
        local fSpeedTotal = fYSpeed + fXSpeed
        local iFlightTickScatter = LM_GetRandomNumber(70, 130) / 100.0
        local iFlightTicks = 35 * iFlightTickScatter
        
        --First hop.
        local fXTarget = fXPos + fXSpeed
        local fYTarget = fYPos + fYSpeed
        fnCutsceneJumpTo(sName, fXTarget, fYTarget, iFlightTicks)
        
        --Second hop is from the new position at half speed.
        fXSpeed = fXSpeed * 0.50
        fYSpeed = fYSpeed * 0.50
        fXTarget = fXTarget + fXSpeed
        fYTarget = fYTarget + fYSpeed
        fSpeedTotal = fYSpeed + fXSpeed
        iFlightTicks = math.floor(iFlightTicks * 0.70)
        fnCutsceneJumpTo(sName, fXTarget, fYTarget, iFlightTicks)
        
        --Final hop.
        fXSpeed = fXSpeed * 0.50
        fYSpeed = fYSpeed * 0.50
        fXTarget = fXTarget + fXSpeed
        fYTarget = fYTarget + fYSpeed
        fSpeedTotal = fYSpeed + fXSpeed
        iFlightTicks = math.floor(iFlightTicks * 0.70)
        fnCutsceneJumpTo(sName, fXTarget, fYTarget, iFlightTicks)
    end
    
    --Sanya moves a bit.
    fnCutsceneCamera("Actor Name", 1.0, "Sanya")
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Hey, I'm back at this dump.[P] Guess that saves me a bit of walking.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] I can at least take a look at that cart contraption I suppose.[P] Otherwise I'll need to be practicing my running and jumping skills.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] And the related skills of falling and splattering.[P] But you know, optimism!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
-- |[ ===================================== Use Rifle Scene ==================================== ]|
elseif(sObjectName == "RifleIdea") then

    --Repeat check.
    local iDidPuzzleFight = VM_GetVar("Root/Variables/Chapter2/SanyaMines/iDidPuzzleFight", "N")
    local iSawRifleScene = VM_GetVar("Root/Variables/Chapter2/SanyaMines/iSawRifleScene", "N")
    if(iDidPuzzleFight == 0.0 or iSawRifleScene == 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter2/SanyaMines/iSawRifleScene", "N", 1.0)
    VM_SetVar("Root/Variables/Global/Sanya/iRecoveredRifle", "N", 1.0) --This adds the field ability.

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Hmmm...)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Camera moves.
    fnCutsceneCamera("Position", 2.5, 21.25, 20.50)
    fnCutsceneBlocker()
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    fnCutsceneCamera("Actor Name", 2.5, "Sanya")
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Equip the field ability to slot 0, or slot 1 if Reset is in slot 0.
    local sSlotZeroAbility = AdvCombat_GetProperty("Field Ability Name", 0)
    if(sSlotZeroAbility == "Null") then
        fnCutscene([[ AdvCombat_SetProperty("Set Field Ability", 0, "Root/Special/Combat/FieldAbilities/Use Rifle") ]])
    else
        fnCutscene([[ AdvCombat_SetProperty("Set Field Ability", 1, "Root/Special/Combat/FieldAbilities/Use Rifle") ]])
    end
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] You know, that switch looks a lot like a target.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] (Yeah, got my gun back and within five minutes I've found a 'reason' to shoot something with it.[P] Maybe dad was right about me...)[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Offended] From this range?[P] I'm sure I can hit it.[B][C]") ]])
    fnCutscene([[ Append("Narrator: [SOUND|Menu|SpecialItem](Unlocked the field ability 'Use Rifle')[B][C]") ]])
    fnCutscene([[ Append("Narrator: (This ability will ready Sanya's rifle.[P] You can fire by pushing [Activate], or stop with [Cancel])[B][C]") ]])
    fnCutscene([[ Append("Narrator: (The ability can stun enemies or shoot certain switches.)[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Smirk] Since an alternative has not presented itself, may as well give it a try.") ]])
    fnCutsceneBlocker()
    
end
