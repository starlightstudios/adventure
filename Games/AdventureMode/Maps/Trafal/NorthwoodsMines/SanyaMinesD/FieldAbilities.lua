-- |[ ====================================== Field Abilities ====================================== ]|
--Called when a field ability is used, at the discretion of the field ability script. The variable
-- gbFieldAbilityHandledInput should be set to true if the script handled the ability.

-- |[Arguments]|
--Argument Listing:
-- 0: iSwitchCode - Switch code used by the ability. One of the gciFieldAbility_Activate_[X] series.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local iSwitchCode = tonumber(LM_GetScriptArgument(0))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================== Handling ========================================= ]|
--Reset the active puzzle.
if(iSwitchCode == gciFieldAbility_Activate_Reset) then
    
    --Spawn a dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sanya", "Neutral") ]])
	fnCutscene([[ Append("Sanya:[E|Neutral] (Really reset the puzzle?)[BLOCK]") ]])

    --Decision script is this script. It must be surrounded by quotes.
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Reset\", " .. sDecisionScript .. ", \"1000\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"No Thanks\",  " .. sDecisionScript .. ", \"2000\") ")
    fnCutsceneBlocker()

    --Flag.
    gbFieldAbilityHandledInput = true
    
--Confirm reset:
elseif(iSwitchCode == gciFieldAbility_Confirm_Reset) then

	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
    
    --Black the screen out.
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --SFX.
    fnCutscene([[ AudioManager_PlaySound("World|MagicA") ]])
    
    --Reset the layers and collisions.
    fnCutscene([[ AL_SetProperty("Set Layer Disabled", "Bridge", true) ]])
    fnCutscene([[ AL_SetProperty("Set Collision", 34, 22, 0, 1) ]])
    fnCutscene([[ AL_SetProperty("Set Collision", 35, 22, 0, 1) ]])
    fnCutscene([[ AL_SetProperty("Set Collision", 36, 22, 0, 1) ]])
    fnCutscene([[ AL_SetProperty("Set Collision", 34, 23, 0, 1) ]])
    fnCutscene([[ AL_SetProperty("Set Collision", 35, 23, 0, 1) ]])
    fnCutscene([[ AL_SetProperty("Set Collision", 36, 23, 0, 1) ]])
    
    --Position
    local cfWarpX = 50.25
    local cfWarpY = 22.50
    
    --Move objects.
    fnCutsceneTeleport(gsPartyLeaderName, cfWarpX, cfWarpY)
    fnCutsceneFace(gsPartyLeaderName, 1, 0)
    fnCutsceneTeleport("BlockA", 54.25, 22.50)
    fnCutsceneTeleport("BlockB", 56.25, 25.50)
    fnCutsceneTeleport("BlockC", 55.25, 17.50)
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Fold party.
    for i = 1, #gsaFollowerNames, 1 do
        fnCutsceneTeleport(gsaFollowerNames[i], cfWarpX, cfWarpY)
    end
    fnAutoFoldParty()
    
    --If the puzzle was solved, un-solve it and play a sound effect.
    local iMinesDSolved = VM_GetVar("Root/Variables/Chapter2/SanyaMines/iMinesDSolved", "N")
    if(iMinesDSolved == 1.0) then
        VM_SetVar("Root/Variables/Chapter2/SanyaMines/iMinesDSolved", "N", 0.0)
        fnCutscene([[ AudioManager_PlaySound("World|DistantDoorClose") ]])
    end
    
    --Fade in.
    fnCutsceneWait(45)
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()

--Cancel reset:
elseif(iSwitchCode == gciFieldAbility_Cancel_Reset) then

	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
    
end
