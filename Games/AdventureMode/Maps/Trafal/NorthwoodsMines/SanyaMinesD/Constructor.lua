-- |[ ======================================= Constructor ====================================== ]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = fnResolveDirectory() --Map's name is the name of the folder it is in.
local sLevelMusic = "TrafalCave"
local sMapResolveName = "Map Resolve Name"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[ ======================================== Standard ======================================== ]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

    -- |[Construction]|
	--Music.
	AL_SetProperty("Music", sLevelMusic)
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation(sMapResolveName)

	-- |[Lights]|
    AL_SetProperty("Activate Lights")
    
    -- |[Shooting Setup]|
    AL_SetProperty("Shooting Depth", 2)

-- |[ ======================================== Post-Exec ======================================= ]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	-- |[NPC Spawning]|
	--If an entity needs to spawn after a post-exec cutscene, put them here.
    fnSpawnNPCPattern("Block", "A", "C")
    fnSpawnNPCPattern("Switch", "A", "C")

    -- |[Overlays]|
    --Place overlay code here.
    
    -- |[Skillbooks]|
    --Place skillbook overlay code here.
    
    -- |[Puzzles]|
    local iMinesDSolved = VM_GetVar("Root/Variables/Chapter2/SanyaMines/iMinesDSolved", "N")
    if(iMinesDSolved == 1.0) then
        fnCutsceneTeleport("BlockA", 52.25, 28.50)
        fnCutsceneTeleport("BlockB", 53.25, 28.50)
        fnCutsceneTeleport("BlockC", 54.25, 28.50)
        AL_SetProperty("Set Collision", 34, 22, 0, 0)
        AL_SetProperty("Set Collision", 35, 22, 0, 0)
        AL_SetProperty("Set Collision", 36, 22, 0, 0)
        AL_SetProperty("Set Collision", 34, 23, 0, 0)
        AL_SetProperty("Set Collision", 35, 23, 0, 0)
        AL_SetProperty("Set Collision", 36, 23, 0, 0)
    else
        AL_SetProperty("Set Layer Disabled", "Bridge", true)
    end
    
    -- |[Rifle Handling]|
    VM_SetVar("Root/Variables/Global/Sanya/iRifleCode", "N", gciSanyaRifle_CanFire)

end
