-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Examinations]|
--Default.
if(sObjectName == "Plants") then

	--Variables.
	local sMeiForm             = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	local iSplashedPlants      = VM_GetVar("Root/Variables/Chapter1/Scenes/iSplashedPlants", "N")
	local iIsFlorentinaPresent = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenTrannadarFlorentinaScene", "N")

	--Normal dialogue:
	if(sMeiForm ~= "Alraune") then
		fnStandardDialogue([[[VOICE|Mei] (Looks like some crops are growing wild here.) ]])
	
	--Alraune.
	else
	
		--Splash some water on the plants. So thirsty!
		if(iSplashedPlants == 0.0) then
			
			--Variables.
			VM_SetVar("Root/Variables/Chapter1/Scenes/iSplashedPlants", "N", 1.0)
	
			--Dialogue.
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ Append("Mei:[VOICE|Mei] (Hello![P] Certainly, just a moment.)[B][C]") ]])
			fnCutscene([[ Append("*You splash water from the nearby stream on the plants.*[B][C]") ]])
			fnCutscene([[ Append("Mei:[VOICE|Mei] (There you go.[P] Just let me know if you need anything else.)") ]])
		
		--Other case.
		else
		
			--If Florentina is not present:
			if(iIsFlorentinaPresent == 0.0) then
				fnCutscene([[ WD_SetProperty("Show") ]])
				fnCutscene([[ Append("Mei:[VOICE|Mei] (Just let me know if you're thirsty, it's no trouble.)[B][C]Mei:[VOICE|Mei] (...)[B][C]Mei:[VOICE|Mei] (You're welcome!)") ]])
			
			--If Florentina is present.
			else
				fnStandardMajorDialogue()
				fnCutscene([[ Append("Mei:[VOICE|Mei] (Everything all right, little ones?)[B][C]") ]])
				fnCutscene([[ Append("Florentina:[VOICE|Florentina] Don't baby them. They can survive on their own.[B][C]") ]])
				fnCutscene([[ Append("Mei:[VOICE|Mei] A little kindness goes a long way...") ]])
			end
		end
	end
	
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
