-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[Name Resolve]|
    --Resolve the name of the calling entity.
    local sActorName = TA_GetProperty("Name")

    --Set facing.
    TA_SetProperty("Face Character", "PlayerEntity")
        
    -- |[Traveller A]|
    if(sActorName == "Traveller A") then
	
        --Variables.
        local iPlatina = AdInv_GetProperty("Platina")
        local iHasCatalystTone = VM_GetVar("Root/Variables/Global/CatalystTone/iHasCatalystTone", "N")
        
        --Dialogue.
        WD_SetProperty("Show")
        
        --If Mei has the Platinum Compass...
        if(iHasCatalystTone == 1.0) then
            Append("Trader:[VOICE|MercF] This delay is going to eat into our profits...")
            
        --Opportunity to buy the compass.
        else
        
            --Dialogue.
            Append("Trader:[VOICE|MercF] Ugh, there was a rockslide.[P] Now I've got all this stock and we're going to have to climb or take the long way.[B][C]")
            Append("Trader:[VOICE|MercF] You...[P] wouldn't happen to want to purchase a Platinum Compass would you?[P] It sounds a chime if there's a Catalyst nearby.[B][C]")
            Append("Trader:[VOICE|MercF] Very useful if you're an adventuring sort.[B][C]")
            Append("Mei:[VOICE|Mei] Just how much would this compass be?[B][C]")
            Append("Trader:[VOICE|MercF] 700 Platina.[P] I can't go lower than that, sorry.[P] The trading company would kill me.[B][C]")
            Append("Mei:[VOICE|Mei] But you need to get rid of it, right?[P] It's too heavy?[B][C]")
            Append("Trader:[VOICE|MercF] Oh, no.[P] It's a compass.[P] I just hate that damn chime when I'm out walking.[B][C]")
            
            --Mei doesn't have enough Platina...
            if(iPlatina < 700) then
                Append("Trader:[VOICE|MercF] So do you want it or what?[B][C]")
                Append("Mei:[VOICE|Mei] Well, uh, you see...[B][C]")
                Append("Trader:[VOICE|MercF] Don't sweat it if you don't have the cash.[P] We're going to be here a while...")
            
            --Mei has the cash.
            else
                Append("Trader:[VOICE|MercF] So do you want it or what?[BLOCK]")
            
                local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
                fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
                fnCutscene(" WD_SetProperty(\"Add Decision\", \"Sure\", " .. sDecisionScript .. ", \"Buy\") ")
                fnCutscene(" WD_SetProperty(\"Add Decision\", \"No Thanks\",  " .. sDecisionScript .. ", \"Dont\") ")
                fnCutsceneBlocker()
            end
        end
        
    -- |[Traveller B]|
    elseif(sActorName == "Traveller B") then
        WD_SetProperty("Show")
        Append("Merc:[VOICE|MercM] You trying to head south?[P] Good luck, there's a nasty rock slide.[P] It'll be months before this gets cleared.")
    end
	
--"Buy", purchase the compass.
elseif(sTopicName == "Buy") then

	--Dialogue.
	WD_SetProperty("Hide")
	WD_SetProperty("Show")
	Append("Trader:[SOUND|Menu|BuyOrSell][VOICE|MercF] Superb![P] Enjoy your treasure hunting![B][C]")
	Append("*The compass will play a tone when you enter an area that has a catalyst.*")
	
	--Vars.
	AdInv_SetProperty("Remove Platina", 700)
	VM_SetVar("Root/Variables/Global/CatalystTone/iHasCatalystTone", "N", 1.0)
	
--"Dont", don't purchase the compass.
elseif(sTopicName == "Dont") then

	--Dialogue.
	WD_SetProperty("Hide")
	WD_SetProperty("Show")
	Append("Trader:[VOICE|MercF] Hmm, well, if you change your mind...[B][C]")
	Append("Trader:[VOICE|MercF] We're not exactly in a rush here.[P] Good luck.")
end
