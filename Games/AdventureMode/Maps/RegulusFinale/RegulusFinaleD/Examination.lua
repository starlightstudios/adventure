-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--Variables.
local iChristineCorruptedEnding = VM_GetVar("Root/Variables/Chapter5/Scenes/iChristineCorruptedEnding", "N")

-- |[Exits]|
-- |[Objects]|
if(sObjectName == "AirlockDoorEN") then
    AL_SetProperty("Close Door", "DoorS")
    
elseif(sObjectName == "AirlockDoorES") then
    AL_SetProperty("Close Door", "DoorN")
    
elseif(sObjectName == "AirlockDoorWN") then
    AL_SetProperty("Close Door", "DoorSW")
    
elseif(sObjectName == "AirlockDoorWS") then
    AL_SetProperty("Close Door", "DoorNW")

elseif(sObjectName == "MagazineA") then
    if(iChristineCorruptedEnding == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('Seventeen Things YOU can do to help the Rebel Cause'.[P] A pamphlet we had made.[P] They're being distributed all across the city right now.)") ]])
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('Seventeen Things YOU can do to help the Rebel Cause'.[P] A pamphlet made by the rebels.[P] They're being distributed all across the city right now.)") ]])
    end

elseif(sObjectName == "MagazineB") then
    if(iChristineCorruptedEnding == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('Butt Monthly'.[P] Well, gotta look at something while you wait for the tram.)") ]])
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('Butt Monthly'.[P] Filth designed to keep the lower orders distracted.)") ]])
    end

elseif(sObjectName == "TerminalA") then
    if(iChristineCorruptedEnding == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Listings of which units use this domicile block, and who is currently defragmenting.[P] It seems the entire sector is awake right now.)") ]])
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Listings of which units use this domicile block, and who is currently defragmenting.[P] The entire sector is awake right now.)") ]])
    end

elseif(sObjectName == "TerminalB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Emergency conversion pod.[P] Organics caught in a vacuum are taken here and transformed into golems if the alternative is death.)") ]])

elseif(sObjectName == "SignA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('On sale now, all prices slashed![P] Keep your work credits, just please buy my inventory before I literally explode![P] Psycho Sally's Insane Deals Emporium -[P] Just east in sector 45 at address 771-6.xv8!')") ]])
    
elseif(sObjectName == "SignB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Floor level -[P] Sector 165 Light Manufacturing, Clothes, Appliances.')") ]])
    
elseif(sObjectName == "SignC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('First level -[P] Sector 165 eatery and public recharging stations.')") ]])
    
elseif(sObjectName == "SignD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Organic citizens::[P] No access to sector 165 without vac-suit.[P] Inquire at information kiosk below.')") ]])
    
elseif(sObjectName == "Window") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Just behind the window are clothes and mannequins sporting them from sector 165's fashion boutiques.)") ]])

-- |[Debug]|
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
