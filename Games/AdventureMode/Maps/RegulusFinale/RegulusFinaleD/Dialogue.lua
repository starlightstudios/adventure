-- |[Root]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then

    --Resolve the name of the calling entity.
    local sActorName = TA_GetProperty("Name")
	
    --Characters.
    if(sActorName == "GolemA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] The transit system is out and I'm stranded here, and now I'm hearing explosions and gunshots.[P] I'm too scared to go outside...") ]])
        
    elseif(sActorName == "GolemB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] I saw a bunch of units going through the subsurface access system...[P] They all had guns...") ]])
        
    elseif(sActorName == "GolemC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] There was an emergency notification.[P] We're not supposed to defragment, but we don't have any orders.[P] What should I do?") ]])
        
    elseif(sActorName == "GolemD") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] I'm keeping watch.[P] Go ahead, commander![P] There's a team just north of here.") ]])
        
    elseif(sActorName == "GolemE") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] The tram just jumped the track and smashed into the wall.[P] What's going on with our transit network?") ]])
        
    elseif(sActorName == "GolemF") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Nobody was on the tram, so nobody got hurt.[P] Do you think the rebels did this?") ]])
    end
end
