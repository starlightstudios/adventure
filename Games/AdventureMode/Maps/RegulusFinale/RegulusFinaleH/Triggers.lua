-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
if(sObjectName == "TheEnd") then
    
    --Variables.
    local iChristineCorruptedEnding = VM_GetVar("Root/Variables/Chapter5/Scenes/iChristineCorruptedEnding", "N")
    local iSX399JoinsParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
    
    --No repeat check. Fade to black.
    fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    -- |[Good Ending Variant]|
    if(iChristineCorruptedEnding == 0.0) then
    
        --SX-399 variant:
        if(iSX399JoinsParty == 1.0) then
            LM_ExecuteScript(fnResolvePath() .. "GoodEndingWithSX.lua")
        
        --No SX variant:
        else
            LM_ExecuteScript(fnResolvePath() .. "GoodEndingWithoutSX.lua")
        end
    
    -- |[Bad Ending Variant]|
    else
    
        --SX-399 variant:
        if(iSX399JoinsParty == 1.0) then
            LM_ExecuteScript(fnResolvePath() .. "BadEndingWithSX.lua")
        
        --No SX variant:
        else
            LM_ExecuteScript(fnResolvePath() .. "BadEndingWithoutSX.lua")
        end
    end
end
