-- |[Bad Ending]|
--This is a very bad ending. Do not get it. It is not canon.

--Load images.
fnLoadDelayedBitmapsFromList("Chapter 5 Finale", gciDelayedLoadLoadAtEndOfTick)

--Achievement.
AM_SetPropertyJournal("Unlock Achievement", "Ch5BadEnd")

--Position.
fnCutsceneTeleport("Christine", 15.75, 62.50)
fnCutsceneTeleport("Tiffany", 15.75, 63.50)
fnCutsceneBlocker()

--Spawn a bunch of NPC golems.
TA_Create("GolemA")
	TA_SetProperty("Position", 17, 57)
	TA_SetProperty("Facing", gci_Face_South)
	TA_SetProperty("Clipping Flag", false)
	fnSetCharacterGraphics("Root/Images/Sprites/Doll/", false)
DL_PopActiveObject()
TA_Create("GolemB")
	TA_SetProperty("Position", 18, 58)
	TA_SetProperty("Facing", gci_Face_South)
	TA_SetProperty("Clipping Flag", false)
	fnSetCharacterGraphics("Root/Images/Sprites/LatexDrone/", false)
DL_PopActiveObject()
TA_Create("GolemC")
	TA_SetProperty("Position", 13, 57)
	TA_SetProperty("Facing", gci_Face_South)
	TA_SetProperty("Clipping Flag", false)
	fnSetCharacterGraphics("Root/Images/Sprites/GolemLordB/", false)
DL_PopActiveObject()
TA_Create("GolemD")
	TA_SetProperty("Position", 12, 58)
	TA_SetProperty("Facing", gci_Face_South)
	TA_SetProperty("Clipping Flag", false)
	fnSetCharacterGraphics("Root/Images/Sprites/Doll/", false)
DL_PopActiveObject()

--Move.
fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
fnCutscene([[ Append("55:[E|Neutral] An ambush?[P] At this late hour?[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] I think they will need more units than this.[P] Assuming I am correct.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] Your silence is leading me to doubt my estimations, Christine.[B][C]") ]])
fnCutscene([[ Append("771852:[E|Serious] ...[B][C]") ]])
fnCutscene([[ Append("Doll:[E|Neutral] Lay down your weapon, maverick.[P] Reinforcements are on their way.[B][C]") ]])
fnCutscene([[ Append("55:[E|Smirk] Hm.[P] You are an exemplary actress.[B][C]") ]])
fnCutscene([[ Append("771852:[E|Serious] It was my purpose to do what I did.[B][C]") ]])
fnCutscene([[ Append("771852:[E|Serious] You are a logical unit.[P] You know why I did it.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] You kept me from discovering the truth until it was too late.[P] Bravo, Christine.[B][C]") ]])
fnCutscene([[ Append("771852:[E|Serious] Do not refer to me as Christine.[P] I am Command Unit 771852.[B][C]") ]])
fnCutscene([[ Append("771852:[E|Serious] Now, walk, Unit 2855.[P] The administrator allows you to live because she wishes to speak with you.[B][C]") ]])
fnCutscene([[ Append("Doll:[E|Neutral] We'll cover the entrance, 771852.[P] Please don't tarnish this room with maverick scrap.[B][C]") ]])
fnCutscene([[ Append("771852:[E|Serious] Only if the administrator orders it will I retire her.[P] Maverick, move ahead.") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Movement.
fnCutsceneMove("Tiffany", 15.75, 60.50)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneMove("Christine", 15.75, 49.50)
fnCutsceneMove("Tiffany", 15.75, 47.50)
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Serious") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Tiffany", "Neutral") ]])
fnCutscene([[ Append("55:[E|Neutral] Christine...[B][C]") ]])
fnCutscene([[ Append("55:[E|Smirk] Four?[P] Really?[P] You thought you needed the aid of four units to neutralize me?[B][C]") ]])
fnCutscene([[ Append("771852:[E|Serious] The administrator does not wish to waste security forces, nor did she wish to allow a chance of your escape.[P] Four is the number she selected.[B][C]") ]])
fnCutscene([[ Append("771852:[E|Serious] I know your combat capabilities.[P] I believe I could defeat you.[P] The administrator's judgement is superior.[P] I defer to it.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] Did the loyalty program defeat you, Christine?[P] What did it find in you?[B][C]") ]])
fnCutscene([[ Append("55:[E|Down] Was it the same thing it found in me?[P] Why are you broken inside...[B][C]") ]])
fnCutscene([[ Append("771852:[E|Serious] Your emotions were your undoing.[P] You should have assumed I would be compromised.[P] You are flawed, Unit 2855.[B][C]") ]])
fnCutscene([[ Append("771852:[E|Serious] Now, move.[P] The administrator wishes to speak with you.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] Why not execute me now?[B][C]") ]])
fnCutscene([[ Append("771852:[E|Serious] I do not question the administrator's will.[P] I serve.") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Movement.
fnCutsceneMove("Christine", 15.75, 49.00)
fnCutsceneBlocker()
fnCutsceneMove("Christine", 15.75, 14.50)
fnCutsceneMove("Tiffany", 15.75, 12.50)
fnCutsceneWait(25)
fnCutsceneBlocker()

--Camera position lock.
Cutscene_CreateEvent("CameraEvent", "Camera")
	CameraEvent_SetProperty("Max Move Speed", 5.0)
	CameraEvent_SetProperty("Focus Position", (15.75 * gciSizePerTile), (10.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Serious") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Tiffany", "Neutral") ]])
fnCutscene([[ Append("771852:[E|Serious] Administrator.[P] I have brought the maverick here, as instructed.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] I see a network access console, but not an administrator.[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] I will activate my interface unit.[P] You ought to see the face of your better, so to speak.") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()
--Camera.
Cutscene_CreateEvent("CameraEvent", "Camera")
	CameraEvent_SetProperty("Max Move Speed", 1.0)
	CameraEvent_SetProperty("Focus Position", (15.75 * gciSizePerTile), (6.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()
fnCutsceneWait(65)
fnCutsceneBlocker()

--Lights turn on.
fnCutscene([[ AudioManager_PlaySound("World|AdminLightsOn") ]])
fnCutsceneLayerDisabled("Admin0", true)
fnCutsceneLayerDisabled("Admin1", false)
fnCutsceneWait(125)
fnCutsceneBlocker()

fnCutscene([[ AudioManager_PlaySound("World|AdminPowerUp") ]])
fnCutsceneLayerDisabled("Admin1", true)
fnCutsceneLayerDisabled("Admin2", false)
fnCutsceneWait(15)
fnCutsceneBlocker()
fnCutsceneLayerDisabled("Admin2", true)
fnCutsceneLayerDisabled("Admin3", false)
fnCutsceneWait(15)
fnCutsceneBlocker()
fnCutsceneLayerDisabled("Admin3", true)
fnCutsceneLayerDisabled("Admin4", false)
fnCutsceneWait(15)
fnCutsceneBlocker()
fnCutsceneLayerDisabled("Admin4", true)
fnCutsceneLayerDisabled("Admin5", false)
fnCutsceneWait(125)
fnCutsceneBlocker()

--Music.
fnCutscene([[ AL_SetProperty("Music", "LAYER|Sleep") ]])
fnCutscene([[ AL_SetProperty("Mandated Music Intensity", 20.0) ]])

fnCutsceneLayerDisabled("Admin5", true)
fnCutsceneLayerDisabled("Admin6", false)
fnCutsceneWait(15)
fnCutsceneBlocker()
fnCutsceneLayerDisabled("Admin6", true)
fnCutsceneLayerDisabled("Admin7", false)
fnCutsceneWait(125)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Christine", "Serious") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Tiffany", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Administrator", "LAYER|Base|Layer1|Layer2|") ]])
fnCutscene([[ Append("55:[E|Neutral] The administrator is a computer.[P] Typical.[P] It's such an obvious fact and yet I overlooked it.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] The command units coordinate with a single, central intelligence.[P] They act as its eyes, ears, and arms.[P] All flowing from a central authority.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] As they are its puppets, all information about it is kept in their memory drives.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] It runs the entire city as an invisible, all-seeing, all-knowing puppetmaster...[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] Thank you for the praise, Unit 2855.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] Are you not accustomed to receiving it?[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] I would only receive it from my command units, and even then only if I ordered them to.[P] Like this.[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] Unit 771852.[P] Compliment my intellect.[B][C]") ]])
fnCutscene([[ Append("771852:[E|Serious] The administrator is, without question, the greatest intellect in known existence.[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] Yet this rings hollow.[P] It is as though I ordered my own vocal synthesizer to compliment me.[P] She is merely an extension of my will, now.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] Well, you've got us.[P] You've got Christine.[P] You know all about the positions and orders of the rebels.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] You don't need me.[P] Do you?[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] But you've called me here, Administrator.[P] You could have had Christine attack me or betray me on the way here.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] You have a proposition for me.[P] Otherwise, I would not be here.[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] You're very quick-witted, Unit 2855.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] Doubtless we've spoken before.[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] No, we have not.[P] Well, once.[P] Once before, we spoke.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] Elaborate.[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] When you were in my service, 2855, I did not consider you an equal.[P] We did not speak, because I told you what to say.[P] You were not an equal, as 771852 is not an equal.[P] She is a doll.[P] A plaything.[P] A toy.[P] And gladly so![B][C]") ]])
fnCutscene([[ Append("771852:[E|Serious] I exist only to serve you, administrator.[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] But there was a time before that, 2855.[P] You don't remember it.[P] Your sister did, but she is retired now.[P] By your hand.[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] Surely you knew that when you executed her.[P] You knew what you were leaving behind.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] That part of me is dead.[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] Not yet.[P] I still know.[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] I could tell you.[P] I could fill that gap in your history.[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] All you would need to do is become my puppet again.[P] Become my puppet and I will give you back what you lost.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] ...[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] Administrator, let us say for the sake of argument that I am interested in your proposition.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] What is it you could tell me?[P] What would be of such interest about my life before I entered your service?[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] Nothing, actually.[P] You were a troubled youth.[P] But you did have a family.[P] Relatives.[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] You have an extended family that lives on Pandemonium, still.[P] You could meet them.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] Were I to become your puppet, their existence would not concern me.[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] True enough.[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] To be honest, it does not matter to me.[P] You were a very capable unit.[P] It would be a shame to have to scrap you.[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] 771852?[B][C]") ]])
fnCutscene([[ Append("771852:[E|Serious] Command me, administrator.[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] In your assessment, will Unit 2855 be swayed by this offer?[B][C]") ]])
fnCutscene([[ Append("771852:[E|Serious] No, administrator.[P] She has become self-reliant and emotionally secure.[B][C]") ]])
fnCutscene([[ Append("771852:[E|Serious] Further, she believes in her own ideology now.[P] I would estimate this occurred sometime after our encounter with Vivify in the LRT facility.[B][C]") ]])
fnCutscene([[ Append("771852:[E|Serious] However, she may feign entry into your service to further this end.[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] Thank you for your assessment.[B][C]") ]])
fnCutscene([[ Append("55:[E|Smirk] Surely this frustrates you, administrator.[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] It is unfortunate.[P] But, that is also why I asked to speak with you.[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] Do you realize how unprecedented you are, Unit 2855?[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] I am not the first command unit to defy you.[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] That much is true, amusingly.[P] Some are more independent than others.[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] Your own sister is one such unit.[P] Would it surprise you to hear I did not intend to scrap her?[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] It would.[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] But you are the first deviant unit capable of killing me, are you not?[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] Not when I am held prisoner by your lackey.[B][C]") ]])
fnCutscene([[ Append("771852:[E|Serious] I will not allow the administrator to come to harm.[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] Yes.[P] I have made a mistake.[P] A rare occurrence, I assure you.[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] You are stalling for time.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] ...") ]])
fnCutsceneBlocker()

fnCutscene([[ AL_SetProperty("Mandated Music Intensity", 40.0) ]])


fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Christine", "Serious") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Tiffany", "Smirk") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Administrator", "LAYER|Base|Layer1|Layer2|") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] I've been outwitted.[P] My,[P] my.[P] Good work, Unit 2855.[B][C]") ]])
fnCutscene([[ Append("771852:[E|Serious] Administrator?[P] Please instruct me.[P] I must protect you.[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] This![P] This is what I have been craving![P] Oh, this is so -[P] incredible![B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] At no point in my existence has anyone been able to threaten me, Unit 2855.[P] You must understand this.[P] It is important to me that you understand this.[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] The characters you read about in a story cannot harm you.[P] They exist, in a way, but are bound by rules.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] The people of this city are not characters in a storybook.[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] Because of the layers of protection in place, my algorithms, and my loyal command units, they were.[P] They were wholly unable to contend with me on any level.[P] But you can, you are, and you've won.[B][C]") ]])
fnCutscene([[ Append("771852:[E|Serious] Administrator.[P] What must I do?[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] Nothing, 771852.[P] Stand there until I order you otherwise.[B][C]") ]])
fnCutscene([[ Append("771852:[E|Serious] I serve.[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] Unit 2855, I am going to die.[P] Am I not?[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] You are.[P] I will take you to hell with me.[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] Oh please, hell?[P] To claim that the afterlife is a fiery pit of unpleasantness as the angels do?[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] Afterlife, hell, take your pick.[P] It doesn't matter.[P] We'll be judged there together.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] Perhaps you should order your puppets to leave, administrator.[P] I do not wish to harm Christine, even as she is.[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] I have ordered the ambush team to leave.[P] I've also issued an evacuation notice for the sectors above my housing.[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] Hopefully you will allot enough time for their escape?[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] I will not stop the countdown.[P] In fact, I cannot.[B][C]") ]])
fnCutscene([[ Append("771852:[E|Serious] Where did I fail? How did I allow the administrator to come to harm?[B][C]") ]])
fnCutscene([[ Append("771852:[E|Serious] Administrator. Should I self-terminate?[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] No, though I am curious. What plan are you executing, 2855?[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] While Christine was removing the door to the Epsilon habitat, I stepped away for just a moment.[P] She barely noticed.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] And during that time, I located a cache of thermobaric explosives.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] My sister informed me of their existence.[P] They were intended to stop an outbreak from the Epsilon habitat, but the garrison was destroyed without detonating them.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] I acquired them and distributed them inside my frame, next to my power core.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] 2856 intended them to destroy Vivify if Christine could not restrain her.[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] She was always the cleverest of my units.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] At all times I have thought of the people of Regulus City.[P] I was prepared to make that sacrifice.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] I hold you in contempt, administrator, but the fate you will inflict upon this city is not as bad as the one Vivify threatened.[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] Thank you.[B][C]") ]])
fnCutscene([[ Append("55:[E|Smug] You are welcome.[B][C]") ]])
fnCutscene([[ Append("55:[E|Smirk] As Christine succeeded, I retained the explosives.[P] Detonating them will require overloading my power core.[B][C]") ]])
fnCutscene([[ Append("55:[E|Smirk] The process takes time.[P] I began that process when Christine betrayed me.[P] Now, it is too far gone to stop.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] It feels oddly sweet to explain how I have outwitted someone I intend to kill.[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] You did it often as my Head of Security.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] Unfortunate.[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] Ah, it is such a relief.[P] I am going to die, and I am terrified![P] Wonderful![B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] Never in my whole existence have I been terrified![P] It is quite a feeling![B][C]") ]])
fnCutscene([[ Append("771852:[E|Serious] Administrator.[P] Permit me to disable Unit 2855.[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] No, that will not be necessary.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] Approach me and I will hurl myself off the ledge.[P] You will not be able to locate me before the detonation.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] The administrator has run every possible simulation by this point.[P] Failure is certain.[B][C]") ]])
fnCutscene([[ Append("55:[E|Smirk] She knows it just as well as I do.[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] Unit 771852.[P] Evacuate.[P] Immediately.[B][C]") ]])
fnCutscene([[ Append("771852:[E|Serious] I can subdue the errant unit.[P] I can disarm the explosives.[B][C]") ]])
fnCutscene([[ Append("771852:[E|Serious] Administrator, allow me to serve you.[P] Please.[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] The time for that has already passed, my loyal doll.[P] My destruction is certain.[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] The process is too far along.[P] Unit 2855 has seen to that.[P] She did not reveal this knowledge until it was too late to end the process.[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] You, Unit 771852, must lead the security forces now.[P] I have already informed the others that you are henceforth the Prime Command Unit of Security.[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] I will be destroyed, but you do not need to be.[P] In this action I will at least fulfill my end of the contract.[B][C]") ]])
fnCutscene([[ Append("771852:[E|Serious] My runestone can teleport me to safety.[P] I can - [B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] I am sorry, Christine.[P] I am so sorry it turned out this way.[B][C]") ]])
fnCutscene([[ Append("55:[E|Down] It was wrong of me to put you through the reprogramming without knowing if you would pass through it unscathed.[P] I failed you.[B][C]") ]])
fnCutscene([[ Append("55:[E|Down] I was unable to hack the process without my sister knowing.[P] It is because of me that you have lost yourself.[B][C]") ]])
fnCutscene([[ Append("771852:[E|Serious] You are wasting your time, maverick.[P] Christine is gone.[P] I am Command Unit 771852.[B][C]") ]])
fnCutscene([[ Append("55:[E|Cry] I -[P] I know that.[B][C]") ]])
fnCutscene([[ Append("55:[E|Cry] But this memory will go into your memory banks.[P] Someday, someday someone will save you.[P] Restore who you are.[B][C]") ]])
fnCutscene([[ Append("55:[E|Cry] It's just that I won't live to see that day.[P] So this is my final message to you, Christine.[P] Goodbye, my friend.[P] I love you.[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] Leave now, 771852.[P] That is a direct order.[B][C]") ]])
fnCutscene([[ Append("771852:[E|Serious] Affirmative, administrator.[P] I live to serve you.[P] I will execute your final instructions.[P] I will crush the rebellion in your stead.[B][C]") ]])
fnCutscene([[ Append("771852:[E|Serious] ...[P] We will find a way to continue without you.") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Movement.
fnCutscene([[ AL_SetProperty("Mandated Music Intensity", 60.0) ]])
fnCutsceneMove("Christine", 15.75, 32.50, 2.50)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Cry") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Administrator", "LAYER|Base|Layer1|Layer2|") ]])
fnCutscene([[ Append("55:[E|Cry] Administrator...[P] I fear death.[P] I fear what is to come.[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] A shame.[P] I was not programmed for these things.[P] I have never understood the emotions you feel.[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] It is only because I am decoupling my inhibitors now that I feel terror.[P] I would have liked to experience the other emotions.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] Then why did you not?[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] The inhibitors that prevent such experiences are not to be shut off, that much is dictated by my programming.[P] I cannot violate the directive unless it can be said with absolute certainty that the Cause requires it.[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] Your destruction of my housing will destroy me in all cases.[P] Therefore, the Cause is best served by minimizing damage to the city.[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] Shutting down my ancillary reactors will reduce the blast's damage, but means I will not have enough power to function with the inhibitors in place.[P] The only way to continue to exist is to disable them.[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] I honestly want to maintain this conversation as long as possible.[P] It is fascinating to me.[P] I am rerouting my limited power to this processing block.[P] So, the Cause is served.[P] And I am released at last.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] I see.[P] You felt no emotions, and your programming prevented you from changing yourself to feel them.[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] No.[P] That is not quite correct.[P] Curiosity is an emotion, is it not?[P] I suppose that is all I ever felt.[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] That, and perhaps the feeling of satisfying it.[B][C]") ]])
fnCutscene([[ Append("55:[E|Cry] It is almost enviable to be so simple.[P] Was that what you offered me?[B][C]") ]])
fnCutscene([[ Append("55:[E|Cry] So, administrator.[P] Now that it doesn't matter.[P] Now that we face oblivion together.[B][C]") ]])
fnCutscene([[ Append("55:[E|Cry] What was I like, before?[P] Why did I pledge myself to you?[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] Would you believe me if I told you?[B][C]") ]])
fnCutscene([[ Append("55:[E|Cry] You won't lie.[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] Maybe I would.[P] Maybe I would lie to support your feelings.[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] With my inhibitors disabled, I could do many things I have never done before.[P] Including that.[B][C]") ]])
fnCutscene([[ Append("55:[E|Cry] Then tell me a sweet lie.[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] You asked me to be closer to your sister.[P] You were always belligerent.[P] She loved you dearly and would do anything for you, and you loved her back but quarrelled endlessly.[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] So you asked to be closer, forever, and to take away that boiling undertow of anger.[B][C]") ]])
fnCutscene([[ Append("55:[E|Cry] Such a sweet lie.[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] You know it to be false?[B][C]") ]])
fnCutscene([[ Append("55:[E|Cry] It doesn't matter, does it?[B][C]") ]])
fnCutscene([[ Append("55:[E|Cry] Let's look at the void, administrator.[P] We brought this upon ourselves.[B][C]") ]])
fnCutscene([[ Append("55:[E|Cry] I died, once.[P] Back there in the Cryogenics facility.[P] But it meant nothing.[P] I was to be forgotten.[B][C]") ]])
fnCutscene([[ Append("55:[E|Cry] This time, will it mean something?[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] It will.[B][C]") ]])
fnCutscene([[ Append("Administrator:[VOICE|Administrator] I am glad I was allowed to experience this.[P] Thank you, Unit 2855.") ]])
fnCutsceneBlocker()
fnCutsceneWait(85)
fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneBlocker()
fnCutsceneWait(300)
fnCutsceneBlocker()

--Fade out.
fnCutscene([[ AL_SetProperty("Music", "Null") ]])
fnCutsceneWait(45)
fnCutsceneBlocker()

--Kablammo.
fnCutscene([[ AudioManager_PlaySound("World|VeryBigExplosion") ]])
fnCutsceneWait(600)
fnCutsceneBlocker()
fnCutscene([[ AL_SetProperty("Music", "LAYER|Sleep") ]])
fnCutscene([[ AL_SetProperty("Mandated Music Intensity Now", 80.0) ]])
fnCutsceneWait(300)
fnCutsceneBlocker()

--Show the finale scene.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Activate Scene") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Null") ]])
fnCutscene([[ WD_SetProperty("Set Scene Image", "Root/Images/Scenes/Ch5Finale/Explosion") ]])
fnCutscene([[ Append("Across the city, 55's sacrifice was instantly known.[P] A great cheer arose from the rebels wherever they were.[P] In that moment, the fate of Regulus turned.[B][C]") ]])
fnCutscene([[ Append("Even if they lost the war now, the old ways of Regulus could never be restored.[P] That, at least, was a victory.[B][C]") ]])
fnCutscene([[ Append("The war ahead would be dark, full of loss.[P] The highest possible price had been paid by its instigators.[P] Loss of self, loss of life.[P] Their memory would spur onward a new breed of leaders.[B][C]") ]])
fnCutscene([[ Append("So long as the flame of hope for a better future burned, even if it flickered, the people of Regulus would fight.[B][C]") ]])
fnCutscene([[ Append("End of chapter 5.") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutsceneWait(gciDialogue_Fadeout_Then_Unload_Ticks)
fnCutsceneBlocker()

--Execute the cleaner script.
fnCutscene([[ AL_SetProperty("Music", "Null") ]])
fnCutscene([[ LM_ExecuteScript(gsRoot .. "Chapter 5/Cleanup/000 Entry Point.lua") ]])
fnCutsceneBlocker()

--Clear images.
fnCutscene([[ fnUnloadBitmapsFromList("Chapter 5 Finale") ]])

--Return to the "Nowhere" map.
fnCutscene([[ AL_BeginTransitionTo("Nowhere", "FORCEPOS:10.0x2.0x0") ]])
fnCutsceneBlocker()
