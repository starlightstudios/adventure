-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
if(sObjectName == "Panic") then
    
    --Repeat check.
    local iSawPanicScene = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawPanicScene", "N")
    if(iSawPanicScene == 1.0) then return end
    
    --Variables.
    local iSX399JoinsParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
    local iChristineCorruptedEnding = VM_GetVar("Root/Variables/Chapter5/Scenes/iChristineCorruptedEnding", "N")
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iSawPanicScene", "N", 1.0)
    
    --Merge.
    fnCutsceneMergeParty()
    fnCutsceneBlocker()

    --SX-version:
    if(iSX399JoinsParty == 1.0) then
        
        --Movement.
        fnCutsceneMove("Christine", 23.25, 29.50)
        fnCutsceneBlocker()
        fnCutsceneMove("Christine", 23.25, 28.50)
        fnCutsceneMove("Tiffany", 23.25, 29.50)
        fnCutsceneBlocker()
        fnCutsceneMove("Christine", 23.25, 27.50)
        fnCutsceneMove("Tiffany", 23.25, 28.50)
        fnCutsceneMove("SX-399", 23.25, 29.50)
        fnCutsceneBlocker()
        fnCutsceneMove("Christine", 23.25, 22.50)
        fnCutsceneFace("Christine", -1, 0)
        fnCutsceneMove("Tiffany", 23.25, 23.50)
        fnCutsceneFace("Tiffany", -1, 0)
        fnCutsceneMove("SX-399", 23.25, 24.50)
        fnCutsceneFace("SX-399", -1, 0)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Voice:[VOICE|JX-101] ...[P] We have subverted the network.[P] We are coming to liberate you.[P] The videographs you are seeing are the first in a series of battles we will wage for your freedom.[B][C]") ]])
        fnCutscene([[ Append("SX-399:[VOICE|SX-399] Hey![P] That's mother's voice!") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Camera moves.
        Cutscene_CreateEvent("CameraEvent", "Camera")
            CameraEvent_SetProperty("Max Move Speed", 1.0)
            CameraEvent_SetProperty("Focus Position", (15.75 * gciSizePerTile), (21.50 * gciSizePerTile))
        DL_PopActiveObject()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Voice:[VOICE|JX-101] For too long the people of Regulus City have lived as slaves.[P] No longer.[P] The rising of the sun brings with it new hope for the people of this benighted world.[B][C]") ]])
        fnCutscene([[ Append("Voice:[VOICE|JX-101] If you are able and willing, find your nearest Regulus Republican team and volunteer.[P] If you are not able, all we ask is that you vacate dangerous areas and do not assist Loyalist security teams.[B][C]") ]])
        fnCutscene([[ Append("Voice:[VOICE|JX-101] We need repair units, medics, troops, equipment -[P] anything you can provide.[P] Your future is yours -[P] grasp it![B][C]") ]])
        fnCutscene([[ Append("Lord:[VOICE|GolemLord] Honestly, what sort of chance do they think they have?[B][C]") ]])
        fnCutscene([[ Append("Lord:[VOICE|GolemLord] What makes this revolt so different?[P] What makes them think they will succeed where the others have failed?[B][C]") ]])
        fnCutscene([[ Append("Lord:[VOICE|GolemLordB] The steam droids have made their move.[P] Maybe with their support?[B][C]") ]])
        fnCutscene([[ Append("Lord:[VOICE|GolemLord] Those rotting hulks?[P] The rebels would have a better chance without them!") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Camera.
        Cutscene_CreateEvent("CameraEvent", "Camera")
            CameraEvent_SetProperty("Max Move Speed", 1.0)
            CameraEvent_SetProperty("Focus Actor Name", "Christine")
        DL_PopActiveObject()
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Facing.
        fnCutsceneFace("GolemC", 1, 0)
        fnCutsceneBlocker()
        fnCutsceneWait(45)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
        fnCutscene([[ Append("Golem:[E|Neutral] Commander 2855?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Yes.[P] Are you a sympathizer?[B][C]") ]])
        fnCutscene([[ Append("Golem:[E|Neutral] I'm scouting, undercover.[P] My drone unit friend over there is, too.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Can you clear this area for us?[B][C]") ]])
        fnCutscene([[ Append("Golem:[E|Neutral] You mean evacuate all the civilians?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Yes, friend and foe, get everyone out.[P] Can you hack the comms to issue an evacuation order?[B][C]") ]])
        fnCutscene([[ Append("Golem:[E|Neutral] Shouldn't be a problem, the comms office isn't far from here.[P] But why, though?[B][C]") ]])
        if(iChristineCorruptedEnding == 0.0) then
            fnCutscene([[ Append("Christine:[E|Smirk] If things don't go well for us, this sector is going to go boom.[P] It's very high priority.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] It is a distinct possibility.[P] Evacuate as many civilians as possible as quickly as possible.[B][C]") ]])
        else
            fnCutscene([[ Append("Christine:[E|Neutral] We're performing an extremely risky mission.[P] We need the area clear of civilian casualties.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] It will also reduce the chance undercover security units may ambush us disguised as civilians.[B][C]") ]])
        end
        fnCutscene([[ Append("Golem:[E|Neutral] Affirmative![P] We'll get it done.[P] To freedom.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] To freedom.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Clean up.
        fnAutoFoldParty()
        fnCutsceneBlocker()
        
    --SX-less version:
    else
        
        --Movement.
        fnCutsceneMove("Christine", 23.25, 29.50)
        fnCutsceneBlocker()
        fnCutsceneMove("Christine", 23.25, 28.50)
        fnCutsceneMove("Tiffany", 23.25, 29.50)
        fnCutsceneBlocker()
        fnCutsceneMove("Christine", 23.25, 27.50)
        fnCutsceneMove("Tiffany", 23.25, 28.50)
        fnCutsceneBlocker()
        fnCutsceneMove("Christine", 23.25, 22.50)
        fnCutsceneFace("Christine", -1, 0)
        fnCutsceneMove("Tiffany", 23.25, 23.50)
        fnCutsceneFace("Tiffany", -1, 0)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Voice:[VOICE|Golem] ...[P] We have subverted the network.[P] Now is the time for us to cast off the shackles of oppression![P] Join us, friends![B][C]") ]])
        fnCutscene([[ Append("55:[VOICE|Tiffany] It seems one of our teams has gone off script.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Camera moves.
        Cutscene_CreateEvent("CameraEvent", "Camera")
            CameraEvent_SetProperty("Max Move Speed", 1.0)
            CameraEvent_SetProperty("Focus Position", (15.75 * gciSizePerTile), (21.50 * gciSizePerTile))
        DL_PopActiveObject()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Voice:[VOICE|Golem] For too long the people of Regulus City have lived as slaves.[P] My friends, be slaves no longer![P] We can bring dignity to all![B][C]") ]])
        fnCutscene([[ Append("Voice:[VOICE|Golem] If you are able and willing, find your nearest Regulus Republican team and volunteer.[P] If you are not able, all we ask is that you vacate dangerous areas and do not assist Loyalist security teams.[B][C]") ]])
        fnCutscene([[ Append("Voice:[VOICE|Golem] We need repair units, medics, troops, equipment -[P] anything you can provide.[P] Your future is yours -[P] grasp it![B][C]") ]])
        fnCutscene([[ Append("Lord:[VOICE|GolemLord] Honestly, what sort of chance do they think they have?[B][C]") ]])
        fnCutscene([[ Append("Lord:[VOICE|GolemLord] What makes this revolt so different?[P] What makes them think they will succeed where the others have failed?[B][C]") ]])
        fnCutscene([[ Append("Lord:[VOICE|GolemLordB] Are the steam droids mobilizing?[P] My tandem unit works in security, she says they could cause real harm.[B][C]") ]])
        fnCutscene([[ Append("Lord:[VOICE|GolemLord] Doubtful.[P] They are disorganized, rotting hulks.[P] They'd sooner fall apart than attack.[P] It must be something else.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Camera.
        Cutscene_CreateEvent("CameraEvent", "Camera")
            CameraEvent_SetProperty("Max Move Speed", 1.0)
            CameraEvent_SetProperty("Focus Actor Name", "Christine")
        DL_PopActiveObject()
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Facing.
        fnCutsceneFace("GolemC", 1, 0)
        fnCutsceneBlocker()
        fnCutsceneWait(45)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
        fnCutscene([[ Append("Golem:[E|Neutral] Commander 2855?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Yes.[P] Are you a sympathizer?[B][C]") ]])
        fnCutscene([[ Append("Golem:[E|Neutral] I'm scouting, undercover.[P] My drone unit friend over there is, too.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Can you clear this area for us?[B][C]") ]])
        fnCutscene([[ Append("Golem:[E|Neutral] You mean evacuate all the civilians?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Yes, friend and foe, get everyone out.[P] Can you hack the comms to issue an evacuation order?[B][C]") ]])
        fnCutscene([[ Append("Golem:[E|Neutral] Shouldn't be a problem, the comms office isn't far from here.[P] But why, though?[B][C]") ]])
        if(iChristineCorruptedEnding == 0.0) then
            fnCutscene([[ Append("Christine:[E|Smirk] If things don't go well for us, this sector is going to go boom.[P] It's very high priority.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] It is a distinct possibility.[P] Evacuate as many civilians as possible as quickly as possible.[B][C]") ]])
        else
            fnCutscene([[ Append("Christine:[E|Neutral] We're performing an extremely risky mission.[P] We need the area clear of civilian casualties.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] It will also reduce the chance undercover security units may ambush us disguised as civilians.[B][C]") ]])
        end
        fnCutscene([[ Append("Golem:[E|Neutral] Affirmative![P] We'll get it done.[P] To freedom.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] To freedom.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Clean up.
        fnAutoFoldParty()
        fnCutsceneBlocker()

    end
end
