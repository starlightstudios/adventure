-- |[Root]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then

    --Resolve the name of the calling entity.
    local sActorName = TA_GetProperty("Name")
	
    --Characters.
    if(sActorName == "GolemA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Lord:[VOICE|GolemLord] The rebels have an excellent propaganda arm, I will admit.[P] This is the first time I've seen them take over the videograph displays.") ]])
        
    elseif(sActorName == "GolemB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Lord:[VOICE|GolemLordB] These rebels seem much better armed than the usual mavericks.[P] They must be collaborating with someone...") ]])
        
    elseif(sActorName == "GolemC") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] We'll get this area cleared as soon as possible, leave it to us!") ]])
        
    elseif(sActorName == "GolemD") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Stupid rebels![P] Slaves should know their place![P] The administration has given us so much, and they're going to ruin it for the rest of us!") ]])
        
    elseif(sActorName == "GolemE") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Ooh, this is bad.[P] I don't want to get hurt...[P] Why is this happening?") ]])
        
    elseif(sActorName == "GolemF") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Are those the new DB-16 Mk2 Pulse Repeaters?[P] Where did the rebels get those?[P] Uh, you know, I'm kind of a hobbyist...") ]])
        
    elseif(sActorName == "GolemG") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] You can't stop us, bootlicker![P] This is our time![B][C]") ]])
        fnCutscene([[ Append("55:[VOICE|Tiffany] We're maverick sympathizers, actually.[B][C]") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] You are?[P] Command units are joining the rebels?[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] We shall all be equal in the new society.[B][C]") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Yeah![P] We can do it if we work together!") ]])
        
    elseif(sActorName == "GolemH") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] That command unit took a hit and her squad just ditched her in here.[P] We'll take her prisoner and get her to safety as soon as sarge gets back.") ]])
        
    elseif(sActorName == "GolemI") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Commander 2855![P] So far, we've been received very well by the populace, and we even have a few volunteers![P] We've got a real shot at this!") ]])
        
    elseif(sActorName == "DroneA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Drone:[VOICE|LatexDrone] HELLO, COMMANDER 2855.[P] I HAVE JOINED THE REBELLION, AND TURNED MY INHIBITOR CHIP OFF, BUT I CAN'T FIGURE OUT HOW TO RESET THE VOCAL SYNTHESIZER.[P] THE STREET AHEAD IS CLEAR, YOU MAY PROCEED.") ]])
        
    elseif(sActorName == "DroneB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Drone:[VOICE|LatexDrone] THIS DRONE HAS LOST ITS LORD UNIT.[P] PROCEEDING TO NEAREST SECURITY STATION FOR REASSIGNMENT.") ]])
    
    -- |[Cassandra!]|
    elseif(sActorName == "Cassandra") then
    
        --Variables.
        local iResolvedCassandraQuest     = VM_GetVar("Root/Variables/Chapter5/Scenes/iResolvedCassandraQuest", "N")
        local iSX399JoinsParty            = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
        local iCassandraSpokenToPostPanic = VM_GetVar("Root/Variables/Chapter5/Scenes/iCassandraSpokenToPostPanic", "N")
        local iChristineCorruptedEnding = VM_GetVar("Root/Variables/Chapter5/Scenes/iChristineCorruptedEnding", "N")
        
        --Cassandra is a golem:
        if(iResolvedCassandraQuest == 1.0 or iResolvedCassandraQuest == 3.0) then
            
            --First pass:
            if(iCassandraSpokenToPostPanic == 0.0) then
                VM_SetVar("Root/Variables/Chapter5/Scenes/iCassandraSpokenToPostPanic", "N", 1.0)
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Golem") ]])
                fnCutscene([[ Append("Cassandra: Greetings, Units 2855 and 771852.[B][C]") ]])
                if(iChristineCorruptedEnding == 1.0) then
                    fnCutscene([[ Append("Christine:[E|Serious] (Assessment.[P] Unit 771853.[P] Maverick.[P] Low threat, low priority.[P] It is not worth compromising my cover to apprehend her.)[B][C]") ]])
                end
                if(iSX399JoinsParty == 1.0) then
                    fnCutscene([[ Append("Cassandra: And...[B][C]") ]])
                    fnCutscene([[ Append("SX-399:[E|Flirt] SX-399.[P] Steam Droid shock trooper.[B][C]") ]])
                    fnCutscene([[ Append("55:[E|Smirk] In training.[B][C]") ]])
                    fnCutscene([[ Append("Cassandra: I see.[P] Christine was responsible for converting me.[B][C]") ]])
                    fnCutscene([[ Append("SX-399:[E|Smirk] Me too![P] Kinda![P] It's a long story.[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Smirk] [EMOTION|SX-399|Neutral]Great to see you again, 771853![B][C]") ]])
                else
                    fnCutscene([[ Append("Christine:[E|Smirk] Great to see you again, 771853![B][C]") ]])
                end
                fnCutscene([[ Append("55:[E|Neutral] Have you decided to join our cause?[P] I sent you several covert requests but you did not reply.[B][C]") ]])
                fnCutscene([[ Append("Cassandra: No, but do not be offended.[P] I have no allegiance to the administration.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] I am not offended.[B][C]") ]])
                if(iChristineCorruptedEnding == 1.0) then
                    fnCutscene([[ Append("Christine:[E|Neutral] What she means is, she was worried you might betray us to the administrators.[B][C]") ]])
                else
                    fnCutscene([[ Append("Christine:[E|Neutral] What she means is, she was worried you might rat on us.[B][C]") ]])
                end
                fnCutscene([[ Append("Cassandra: Unfounded.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] One must be cautious when outnumbered.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] So then what are you doing here?[P] We're across the city from sector 15.[B][C]") ]])
                fnCutscene([[ Append("Cassandra: I am making my way to the teleportarium complex two sectors over.[P] I intend to leave the city.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Sad] Well the city is kind of blowing up right now...[B][C]") ]])
                if(iSX399JoinsParty == 1.0) then
                    fnCutscene([[ Append("SX-399:[E|Flirt] That's the fun part![B][C]") ]])
                    fnCutscene([[ Append("55:[E|Neutral] Not when the explosions are directed at oneself.[B][C]") ]])
                    fnCutscene([[ Append("SX-399:[E|Neutral] Yeah...[B][C]") ]])
                end
                fnCutscene([[ Append("Cassandra: It is not for that reason.[P] If I could, I would stay and join the revolution.[P] You did me a great favour by converting me, Christine.[P] I would follow you.[B][C]") ]])
                fnCutscene([[ Append("Cassandra: But I have had visions while performing my function assignment.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Visions?[P] Like, daydreams?[B][C]") ]])
                fnCutscene([[ Append("Cassandra: I have spoken to repair units and there is nothing wrong with my hardware.[P] Yet these visions persist.[B][C]") ]])
                fnCutscene([[ Append("Cassandra: They last between seven and twelve seconds and depict a building with red crystals growing from the rock.[P] I see myself, looking at me and smiling, and the vision ends.[B][C]") ]])
                fnCutscene([[ Append("Cassandra: I must find the place this occurred.[P] It is somewhere on Pandemonium, and I must find it.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Sad] So you're going to Pandemonium all alone?[B][C]") ]])
                if(iSX399JoinsParty == 1.0) then
                    fnCutscene([[ Append("SX-399:[E|Neutral] Won't your core run out of power eventually?[B][C]") ]])
                end
                fnCutscene([[ Append("Cassandra: I can recharge my core with organic foodstuffs and disguise myself to blend in with the inhabitants.[B][C]") ]])
                fnCutscene([[ Append("Cassandra: Repairs will be difficult to manage, but I must solve this conundrum.[P] I must.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Sad] So much for talking you out of it.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] Good luck, 771853.[B][C]") ]])
                fnCutscene([[ Append("Cassandra: Thank you.[P] I may return to the city when I have solved this mystery.[B][C]") ]])
                fnCutscene([[ Append("Cassandra: Please make it the utopia we both know it can be.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Well I wasn't going to, but since you asked...[B][C]") ]])
                fnCutscene([[ Append("Cassandra: May our ways cross again, Christine.") ]])
            else
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Golem") ]])
                fnCutscene([[ Append("Cassandra: I will be moving out soon.[P] I was just listening to the maverick journalist report.[B][C]") ]])
                fnCutscene([[ Append("Cassandra: You have good people on your side, Christine.[P] You will succeed.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Thanks, Cassandra.[P] I'm sure you will, too.") ]])
            end
        
        --Cassandra is a human:
        else
            
            --First pass:
            if(iCassandraSpokenToPostPanic == 0.0) then
                VM_SetVar("Root/Variables/Chapter5/Scenes/iCassandraSpokenToPostPanic", "N", 1.0)
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
                if(iChristineCorruptedEnding == 1.0) then
                    fnCutscene([[ Append("Christine:[E|Serious] (Assessment.[P] Cassandra.[P] Rogue human.[P] Low threat, low priority.[P] It is not worth compromising my cover to apprehend her.)[B][C]") ]])
                end
                if(iSX399JoinsParty == 1.0) then
                    fnCutscene([[ Append("Cassandra: Christine![P] 55![P] And -[P] Wow, SX, looking good![B][C]") ]])
                    fnCutscene([[ Append("SX-399:[E|Flirt] You know it, Cassandra![B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Smirk] You've met?[B][C]") ]])
                    fnCutscene([[ Append("SX-399:[E|Neutral] Cassandra is a bit of a celebrity among the steam droids.[P] A rogue human who escaped from Regulus City, battled her way through the mines, dragging a damaged steam droid with her?[P] Classic.[B][C]") ]])
                    fnCutscene([[ Append("55:[E|Neutral] Impressive.[B][C]") ]])
                    fnCutscene([[ Append("Cassandra: I was only dragging her because her ankle broke off...[B][C]") ]])
                    fnCutscene([[ Append("Cassandra: Doesn't matter![P] Great to see you're alive![B][C]") ]])
                else
                    fnCutscene([[ Append("Cassandra: Christine![P] 2855![P] Great to see you're alive![B][C]") ]])
                end
                fnCutscene([[ Append("Christine:[E|Smirk] Apologies for not following up on you, but, well, we got a little busy.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] I transformed into a doll...[B][C]") ]])
                fnCutscene([[ Append("55:[E|Upset] Command Unit.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] We fought some creepy monsters, blew up some stuff...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] [EMOTION|Tiffany|Neutral]And all I can say is you should probably vacate this sector soon.[B][C]") ]])
                fnCutscene([[ Append("Cassandra: Why?[P] Actually, don't tell me.[P] I'm leaving soon anyway.[B][C]") ]])
                fnCutscene([[ Append("Cassandra: Just got to purloin a vac suit and I can head to the teleportarium complex a few sectors over.[B][C]") ]])
                if(iSX399JoinsParty == 1.0) then
                    fnCutscene([[ Append("SX-399:[E|Neutral] Are you going back to Pandemonium?[B][C]") ]])
                else
                    fnCutscene([[ Append("Christine:[E|Neutral] Teleportarium?[P] To go to Pandemonium?[B][C]") ]])
                end
                fnCutscene([[ Append("Cassandra: I'd love to stay and help liberate the city, really, I would.[P] I've got a lot of steam droid friends who deserve better.[B][C]") ]])
                fnCutscene([[ Append("Cassandra: But...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Your vulnerability to extreme environments is not useful to the cause?[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] Your organic synapses are too slow to participate in combat?[B][C]") ]])
                if(iSX399JoinsParty == 1.0) then
                    fnCutscene([[ Append("SX-399:[E|Neutral] You might ruin that lovely top?[B][C]") ]])
                end
                fnCutscene([[ Append("Cassandra: No![P] Well, sort of.[P] But...[B][C]") ]])
                fnCutscene([[ Append("Cassandra: Every night, just before I bed, I have a vision, Christine.[P] It changes but I always have it.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Like a hallucination?[B][C]") ]])
                fnCutscene([[ Append("Cassandra: Yeah![P] But the doctor said there's nothing wrong with me.[B][C]") ]])
                if(iSX399JoinsParty == 1.0) then
                    fnCutscene([[ Append("SX-399:[E|Neutral] You know she's a veterinarian by training, right?[B][C]") ]])
                    fnCutscene([[ Append("Cassandra: ...[P] Big oof, as my contact says...[P] but still.[B][C]") ]])
                end
                fnCutscene([[ Append("Cassandra: I know the vision is connected to a place on Pandemonium.[P] I think I even know where it is.[P] So, I'm going to find it.[B][C]") ]])
                fnCutscene([[ Append("Cassandra: It's a room with red crystals growing out of the moldy brick walls.[P] I see myself, but I'm staring at me and smiling.[P] Sometimes I laugh.[P] And then it ends.[B][C]") ]])
                fnCutscene([[ Append("Cassandra: Besides, there's not much good I can do here, but maybe I can spread the word planetside.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] Most of the inhabitants of Pandemonium are not even aware that Regulus City exists.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] The most they can see of it is a small blinking light on the moon.[B][C]") ]])
                fnCutscene([[ Append("Cassandra: Yeah...[P] Well, that's all in the future.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] I think it's great.[P] You do what you need to.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] And someday, we'll meet again.[P] If Regulus City is freed, there will be a place here for you if you want it.[B][C]") ]])
                fnCutscene([[ Append("Cassandra: Thanks, Christine.[P] For everything.") ]])
            else
                TA_SetProperty("Face Character", "PlayerEntity")
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Cassandra", "Neutral") ]])
                fnCutscene([[ Append("Cassandra: That maverick journalism there...[P] That's bravery.[P] To go into a war armed with a camera and the truth.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] What they do is just as important as the units with the guns.[P] This must be documented, it must be seen.") ]])
            end
        
        end
    end
end
