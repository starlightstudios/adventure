-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

--Variables.
local iChristineCorruptedEnding = VM_GetVar("Root/Variables/Chapter5/Scenes/iChristineCorruptedEnding", "N")

-- |[Exits]|
if(sObjectName == "ToFinaleE") then
    AL_BeginTransitionTo("RegulusFinaleE", "FORCEPOS:33.0x10.0x0")
    AudioManager_PlaySound("World|AutoDoorOpen")
    
-- |[Objects]|
elseif(sObjectName == "AirlockN") then
    AL_SetProperty("Close Door", "DoorS")
    
elseif(sObjectName == "AirlockS") then
    AL_SetProperty("Close Door", "DoorN")
    
elseif(sObjectName == "Intercom") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (There's no answer on the intercom.[P] Information services has probably fled their posts.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Junk") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The sector puts its junk in here.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Doll") then
    
    if(iChristineCorruptedEnding == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (A command unit prisoner in system standby.[P] Hopefully she doesn't put up a fight when they reactivate her.)") ]])
        fnCutsceneBlocker()
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (A command unit prisoner in system standby.[P] I cannot be seen aiding her.)") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "Shoes") then
    if(iChristineCorruptedEnding == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Unfortunately, these shoes are not compatible with the command unit chassis...)") ]])
        fnCutsceneBlocker()
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Footwear.)") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "SignA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Please ask a slave unit if you need help installing new footwear modules.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "SignB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Slave units must retain purchase receipts when purchasing items on behalf of their lord units.[P] Slave units are subject to searches at any time.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "SignC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('FREEDOM IS COMING, RISE UP!!!' is written on the LED lights.[P] A vandal must have hacked it.[P] You go, robot girl!)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "SignD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Transit stations are experiencing unexpected delays.[P] Please use overland routes when possible.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "SignE") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Diary of a Swordrunner:: Unit 4992's Untold Story'.[P] Looks like an advertisment for someone's memoirs.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Cafe") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Unit 764952's Cafe'.[P] Not cleverly named, but apparently the superlube-soup is to die for.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "GameShop") then
    if(iChristineCorruptedEnding == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Video games get their own shops?[P] Ooh, there's a copy of Needlemouse Heroes!)") ]])
        fnCutsceneBlocker()
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Video games.[P] Pointless distractions for units without purpose.)") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "ApplianceShop") then
    if(iChristineCorruptedEnding == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (An appliances shop.[P] Now's probably not the time for a shopping spree.)") ]])
        fnCutsceneBlocker()
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Appliances.[P] Household status symbols for the vain.)") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "ScreenL") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Some rebels, probably near the eastern edge of the city, taking cover.[P] There is a crawl on the bottom detailing how citizens can support them even if they choose not to fight.)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "ScreenR") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Rebels standing over a disabled lord unit, checking her for power core damage.[P] The crawl describes the philosophy of freedom and equality for all golems, and why ordinary citizens should join today.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "FizzyBlue") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Fizzy Pop! Blue, the taste of freedom!'[P] Seems someone vandalized the machine already.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "FizzyPink") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Fizzy Pop! Pink, FOR BOLTHEADS LOL REKT'...[P] Who wrote this?)") ]])
    fnCutsceneBlocker()
    
-- |[Debug]|
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
