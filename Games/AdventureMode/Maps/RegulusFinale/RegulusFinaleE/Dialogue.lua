-- |[Root]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then

    --Resolve the name of the calling entity.
    local sActorName = TA_GetProperty("Name")
	
    --Characters.
    if(sActorName == "GolemA") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Commander 2855![P] We didn't know you'd be coming this way.[P] We're just getting some repairs done before we hit the satellite uplink.[P] Don't worry about us!") ]])
        
    elseif(sActorName == "GolemB") then
        TA_SetProperty("Face Character", "PlayerEntity")
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Echo got a little jumpy and shot a civilian.[P] She'll be okay, but I don't think Echo will forgive herself.") ]])
        
    elseif(sActorName == "GolemC") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Oh no oh no...[P] I can fix a coolant pump...[P] I'll make it better...") ]])
        
    end
end
