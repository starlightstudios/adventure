-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
    
--Variables.
local iChristineCorruptedEnding = VM_GetVar("Root/Variables/Chapter5/Scenes/iChristineCorruptedEnding", "N")

-- |[Exits]|
if(sObjectName == "ToFinaleB") then
    AL_BeginTransitionTo("RegulusFinaleB", "FORCEPOS:3.5x26.0x0")
    
-- |[Objects]|
elseif(sObjectName == "FizzyPop") then

    if(iChristineCorruptedEnding == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Assorted candy and soda machines.[P] I could go for some sili-chips right about now...)") ]])
        fnCutsceneBlocker()
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Assorted candy and soda machines.[P] Power levels above adequate, fuel not required.)") ]])
        fnCutsceneBlocker()
    end
    
elseif(sObjectName == "SignA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (West:: Vending machines, maintenance tunnels, foot access to sector 43.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "SignB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (East:: Maintenance tunnels, foot access to sector 198.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Counter") then
    if(iChristineCorruptedEnding == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (The booth is deserted.[P] I hope the slave units are okay.)") ]])
        fnCutsceneBlocker()
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (The booth is deserted.[P] The slave units will need to be tracked down and reprimanded.)") ]])
        fnCutsceneBlocker()
    end
    
-- |[Debug]|
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
