-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
if(sObjectName == "Tears") then
    
    --Variables.
    local iChristineCorruptedEnding = VM_GetVar("Root/Variables/Chapter5/Scenes/iChristineCorruptedEnding", "N")
    
    --Out-of-bounds cutscene spawner.
    fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Spawn 55.
    TA_Create("Tiffany")
        TA_SetProperty("Position", -10, -10)
        TA_SetProperty("Facing", gci_Face_South)
        for i = 1, 8, 1 do
            TA_SetProperty("Move Frame", i-1, 0, "Root/Images/Sprites/Special/Tiffany|Heavy0")
            TA_SetProperty("Move Frame", i-1, 1, "Root/Images/Sprites/Special/Tiffany|Heavy1")
            TA_SetProperty("Move Frame", i-1, 2, "Root/Images/Sprites/Special/Tiffany|Heavy2")
            TA_SetProperty("Move Frame", i-1, 3, "Root/Images/Sprites/Special/Tiffany|Heavy3")
            TA_SetProperty("Run Frame", i-1, 0, "Root/Images/Sprites/Special/Tiffany|Heavy0")
            TA_SetProperty("Run Frame", i-1, 1, "Root/Images/Sprites/Special/Tiffany|Heavy1")
            TA_SetProperty("Run Frame", i-1, 2, "Root/Images/Sprites/Special/Tiffany|Heavy2")
            TA_SetProperty("Run Frame", i-1, 3, "Root/Images/Sprites/Special/Tiffany|Heavy3")
        end
    DL_PopActiveObject()
    
    --Position.
    fnCutsceneTeleport("Christine", 13.25, 15.50)
    fnCutsceneFace("Christine", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Fade.
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(305)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    if(iChristineCorruptedEnding == 0.0) then
        fnCutscene([[ Append("Christine:[VOICE|Christine] (She's taken an awfully long time.[P] Maybe I should...)") ]])
    else
        fnCutscene([[ Append("Christine:[VOICE|Christine] (Unit 2855 is taking too long to terminate Unit 2856.[P] Her emotions must have gotten in the way.)") ]])
    end
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneFace("Christine", 1, 0)
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutsceneFace("Christine", 0, -1)
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutsceneWait(185)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    if(iChristineCorruptedEnding == 0.0) then
        fnCutscene([[ Append("Christine:[VOICE|Christine] (This is their moment.[P] I should just let them have it.)") ]])
    else
        fnCutscene([[ Append("Christine:[VOICE|Christine] (Re-entering the room would risk revealing me.[P] I will continue the charade...)") ]])
    end
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Sound.
    fnCutsceneFace("Christine", 1, 0)
    fnCutscene([[ AudioManager_PlaySound("World|LaserDistant") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(165)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Christine", 18.25, 15.50)
    fnCutsceneBlocker()
    
    --Door opens. Move 55. Clear collisions.
    fnCutsceneTeleport("Tiffany", 21.75, 11.50)
    fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutsceneLayerDisabled("DoorOpenHi", false)
    fnCutsceneLayerDisabled("DoorOpenLo", false)
    fnCutsceneLayerDisabled("DoorClosedHi", true)
    fnCutsceneLayerDisabled("DoorClosedLo", true)
    AL_SetProperty("Set Collision", 21, 11, 0, 0)
    AL_SetProperty("Set Collision", 22, 11, 0, 0)
    AL_SetProperty("Set Collision", 21, 12, 0, 0)
    AL_SetProperty("Set Collision", 22, 12, 0, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --55 walks south.
    fnCutsceneMove("Tiffany", 21.75, 14.50, 0.25)
    fnCutsceneMove("Christine", 20.75, 15.50)
    fnCutsceneFace("Christine", 1, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    if(iChristineCorruptedEnding == 0.0) then
        fnCutscene([[ Append("55:[VOICE|Tiffany] She's so heavy, Christine...[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] Do you need - [P][CLEAR]") ]])
        fnCutscene([[ Append("55:[VOICE|Tiffany] I can carry her.[B][C]") ]])
        fnCutscene([[ Append("55:[VOICE|Tiffany] Can you wait inside, please?[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] What are you going to do?[B][C]") ]])
        fnCutscene([[ Append("55:[VOICE|Tiffany] She was a monster.[P] She retired and tortured and...[P] so many awful things...[B][C]") ]])
        fnCutscene([[ Append("55:[VOICE|Tiffany] But she was still my sister, and she deserves a burial.") ]])
    else
        fnCutscene([[ Append("55:[VOICE|Tiffany] She's so heavy, Christine...[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] It is good that she is dead.[P][CLEAR]") ]])
        fnCutscene([[ Append("55:[VOICE|Tiffany] Why don't I feel that way, then?[B][C]") ]])
        fnCutscene([[ Append("55:[VOICE|Tiffany] ...[P] Can you wait inside, please?[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] Why?[B][C]") ]])
        fnCutscene([[ Append("55:[VOICE|Tiffany] She was a monster.[P] She retired and tortured and...[P] so many awful things...[B][C]") ]])
        fnCutscene([[ Append("55:[VOICE|Tiffany] But she was still my sister, and she deserves a burial.") ]])
    end
    fnCutsceneBlocker()
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Christine", 21.75, 15.50)
    fnCutsceneBlocker()
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("55:[VOICE|Tiffany] Christine...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    fnCutsceneFace("Christine", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("55:[VOICE|Tiffany] Why can't I cry?[B][C]") ]])
    if(iChristineCorruptedEnding == 0.0) then
        fnCutscene([[ Append("Christine:[VOICE|Christine] Because they didn't give us tear ducts.") ]])
    else
        fnCutscene([[ Append("Christine:[VOICE|Christine] Because command units do not have tear ducts.") ]])
    end
    fnCutsceneBlocker()
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    
    --Variables.
    local iSX399JoinsParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
    fnCutscene([[ WD_SetProperty("Show") ]])
    if(iSX399JoinsParty == 1.0) then
        fnCutscene([[ Append("55:[VOICE|Tiffany] ...[P] We placed SX-399 in the storage room next to the servers.[P] Please see to her.[P] I will be back shortly.") ]])
    else
        fnCutscene([[ Append("55:[VOICE|Tiffany] ...[P] I will be back shortly.") ]])
    end
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Fade to black.
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Transition.
    fnCutscene([[ AL_BeginTransitionTo("RegulusFinaleA", "FORCEPOS:13.0x19.0x0") ]])
    fnCutsceneBlocker()
    
end
