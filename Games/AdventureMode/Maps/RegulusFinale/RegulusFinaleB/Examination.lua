-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
if(sObjectName == "ToFinaleA") then
    AL_BeginTransitionTo("RegulusFinaleA", "FORCEPOS:6.5x16.0x0")
    
elseif(sObjectName == "ToFinaleC") then
    AL_BeginTransitionTo("RegulusFinaleC", "FORCEPOS:9.5x4.0x0")
    
-- |[Objects]|
elseif(sObjectName == "Windows") then
    
    --Variables.
    local iChristineCorruptedEnding = VM_GetVar("Root/Variables/Chapter5/Scenes/iChristineCorruptedEnding", "N")
    
    if(iChristineCorruptedEnding == 0.0) then
        WD_SetProperty("Show")
        Append("Thought:[VOICE|Leader] (The servers that handle the reprogramming algorithm...[P] I'm torn between capturing them for study, and blowing them up.)")
    else
        WD_SetProperty("Show")
        Append("Thought:[VOICE|Leader] (The servers that handle the reprogramming algorithm.[P] They must be kept safe from the rebels if we are to synthesize new command units.)")
    end

-- |[Debug]|
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
