-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
if(sObjectName == "ToFinaleB") then
    AL_BeginTransitionTo("RegulusFinaleB", "FORCEPOS:21.5x13.0x0")
    
-- |[Objects]|
elseif(sObjectName == "DollMachineL") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The first phase of the command unit machine.[P] This one reconstructs the brain and gives it the initial programming.[P] While the subject is still organic...)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "Needles") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The second phase of the command unit machine.[P] The needles inject nanofluid which constructs joints, re-laminates the skin, reconfigures the spinal column, and repurposes the internal organs into more efficient machines.)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "DollMachineR") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The third phase of the command unit machine, which inserts prefabricated components into the command unit.)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "TerminalA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Cognitive and physical activity satisfactory.[P] Command Unit 771852 discharged.')") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "TerminalB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Network uplink severed.[P] Central Administration requesting follow-up.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Warning:: Network access speeds low.[P] Power distributions uncertain.[P] All command units provide situation reports.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Unseeded disturbances detected in multiple sectors.[P] Command units provide situation reports.')") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "TerminalE") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A list of command units created here and backups of their memory files.[P] Most of the backups are deleted by the administrator within a week of conversion. Mine are not on here.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Box") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Spare parts and materials used for constructing the prefabricated command unit parts.[P] They can be fed into the machine and it will synthesize the final parts.)") ]])
    fnCutsceneBlocker()
    
-- |[Debug]|
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
