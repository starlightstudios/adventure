-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
if(sObjectName == "FinaleScene" or sObjectName == "FinaleSceneCorrupted") then
    
    --No duplicate check. This scene trigger is outside the playable area and thus cannot trigger again normally.
    fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter5/Scenes/iMainQuestBlock", "N", 61.0)
    
    --Boolean flag. Used to handle corruption case.
    local bIsCorruption = false
    if(sObjectName == "FinaleSceneCorrupted") then 
        bIsCorruption = true 
        VM_SetVar("Root/Variables/Chapter5/Scenes/iChristineCorruptedEnding", "N", 1.0)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iMainQuestBlock", "N", 62.0)
        fnSwitchItemTypesForChristine()
    end
    
    --Position Christine.
    fnCutsceneTeleport("Christine", 19.75, 5.50)
    fnCutsceneSetFrame("Christine", "FlatbackC")
    
    --Costumes.
    VM_SetVar("Root/Variables/Costumes/Tiffany/sCostumeDoll", "S", "Normal")
    
    --Spawn the other actors.
    TA_Create("2856")
        TA_SetProperty("Position", 19, 7)
        TA_SetProperty("Facing", gci_Face_North)
        fnSetCharacterGraphics("Root/Images/Sprites/56/", false)
    DL_PopActiveObject()
    fnSpecialCharacter("Tiffany", 10, 8, gci_Face_North, false, nil)
    fnCutsceneTeleport("Tiffany", 10.25, 8.75)
    LM_ExecuteScript(gsCharacterAutoresolve, "Tiffany")
    
    --Wait a bit.
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Cognitive boot sequence...[P] 25...[P] 50...[P] 75...)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Subroutine disconnect, possible network error.[P] Report to administrator upon boot.[P] File logged.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Begin physical surroundings check.[P] Gravity...[P] OK.[P] Air pressure...[P] OK.[P] Engage sonar check.)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Fade in.
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Audiographic post-processors report green.[P] Power subsystems green.[P] Actuators green.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Final ocular system check...)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Blink.
    fnCutsceneSetFrame("Christine", "FlatbackO")
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneSetFrame("Christine", "FlatbackC")
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneSetFrame("Christine", "FlatbackO")
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] Ocular systems calibration complete, all reports green.[P] Physical conversion complete.[B][C]") ]])
    if(bIsCorruption == true) then
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Internal assessment::[P] Unit 2855 may still be maverick.[P] Maintain emotional facade until confirmation can be obtained.)[B][C]") ]])
    else
        fnCutscene([[ Append("Thought:[VOICE|Leader] (2856 will be nearby.[P] I had better pretend like everything went to her plan...)[B][C]") ]])
    end
    fnCutscene([[ Append("Thought:[VOICE|Leader] Vocal systems calibration check.[P] Command Unit 771852 online.[P] Standing up.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneSetFrame("Christine", "Null")
    fnCutsceneTeleport("Christine", 20.25, 6.50)
    fnCutsceneFace("Christine", 1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Christine", 1, 1)
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneFace("Christine", 0, 1)
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneFace("2856", 1, 0)
    fnCutsceneMove("Christine", 20.25, 7.50)
    fnCutsceneFace("Christine", -1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "2856", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Mental reprogramming complete, all reports green.[P] Greetings, Unit 2856.[B][C]") ]])
    fnCutscene([[ Append("2856:[E|Neutral] Welcome to your new life, Unit 771852.[P] What do you remember?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Everything.[P] I am fully aware of my life as Christine, and my previous role as a lord golem.[B][C]") ]])
    fnCutscene([[ Append("2856:[E|Neutral] And these memories do not pose a problem for you?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] My purpose is to enforce the will of the administrator.[P] I am fully loyal to the Cause of Science.[B][C]") ]])
    fnCutscene([[ Append("2856:[E|Neutral] Good, within expected parameters.[B][C]") ]])
    fnCutscene([[ Append("2856:[E|Neutral] Sister, are you not excited to see how she has turned out?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Tiffany", 19.25, 8.50)
    fnCutsceneBlocker()
    fnCutsceneFace("Christine", -1, 1)
    fnCutsceneFace("Tiffany", 1, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "2856", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Tiffany", "Neutral") ]])
    fnCutscene([[ Append("2855:[E|Neutral] I am not excited, though she has turned out well enough.[B][C]") ]])
    fnCutscene([[ Append("2855:[E|Neutral] Physically, she is exemplary.[P] However, I noticed several unusual spikes of activity in the mental reorientation.[B][C]") ]])
    fnCutscene([[ Append("2856:[E|Neutral] Oh?[B][C]") ]])
    fnCutscene([[ Append("2855:[E|Neutral] Unit 771852.[P] I will now perform a baseline calibration test.[P] Are you ready?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Affirmative.[P] Baseline test algorithm checksums are 99231244572 -[P] mirror check detects no errors.[B][C]") ]])
    fnCutscene([[ Append("2855:[E|Neutral] State baseline.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] In blood black nothingness spins.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] The Cause places all above nothing and nothing spins and nothing spins and nothing spins.[B][C]") ]])
    fnCutscene([[ Append("2855:[E|Neutral] Do you understand what a person is or is supposed to be?[P] Touching.[P][CLEAR]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Touching.[P][CLEAR]") ]])
    fnCutscene([[ Append("2855:[E|Neutral] Why are we here?[P] What is our purpose?[P] Touching.[P][CLEAR]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Touching.[P][CLEAR]") ]])
    fnCutscene([[ Append("2855:[E|Neutral] Is there something more, or is this it?[P] Doing as we are told, always.[P] Before.[P][CLEAR]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Before.[P][CLEAR]") ]])
    fnCutscene([[ Append("2855:[E|Neutral] Do we love each other, or is love a collection of electrical impulses.[P] Before.[P][CLEAR]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Before.[P][CLEAR]") ]])
    fnCutscene([[ Append("2855:[E|Neutral] When we are not performing our duties, what do we do?[P] Weapon.[P][CLEAR]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Weapon.[P][CLEAR]") ]])
    fnCutscene([[ Append("2855:[E|Neutral] There are no answers.[P] There is only searching.[P] Weapon.[P][CLEAR]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Weapon.[P][CLEAR]") ]])
    fnCutscene([[ Append("2855:[E|Neutral] When you touch her, the way you did before, is your hand a weapon, or something else?[P][CLEAR]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] When you touch her, the way you did before, is your hand a weapon, or something else?[P][CLEAR]") ]])
    fnCutscene([[ Append("2855:[E|Neutral] Repeat secondaries.[P][CLEAR]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Touching.[P] Before.[P] Weapon.[B][C]") ]])
    fnCutscene([[ Append("2856:[E|Neutral] Very good, Unit.[P] Perfect, in fact.[B][C]") ]])
    fnCutscene([[ Append("2855:[E|Neutral] As expected from a command unit.[P] But, what was the anomaly I detected?[B][C]") ]])
    fnCutscene([[ Append("2855:[E|Neutral] Unit 771852, do you have any insight on the anomaly I detected?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] I do, Unit 2855.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneFace("Christine", -1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "2856", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Tiffany", "Neutral") ]])
    fnCutscene([[ Append("2856:[E|Neutral] You are looking at me.[P] Why?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] You are a traitor to the Cause of Science.[B][C]") ]])
    fnCutscene([[ Append("2856:[E|Neutral] That is correct.[P] I have disobeyed the administrator.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] You must be retired.[B][C]") ]])
    fnCutscene([[ Append("2856:[E|Neutral] I outrank you, Unit 771852.[P] Only the administrator may order me retired.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Unit 2855, do you agree with my assessment of the situation?[B][C]") ]])
    fnCutscene([[ Append("2855:[E|Neutral] I do.[P] However, my judgement is flawed.[P] Emotional.[P] Only the administrator may order a command unit to be retired.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] We must arrest Unit 2856 and bring her to the administrator, then.[P] Do you know where she is?[B][C]") ]])
    fnCutscene([[ Append("2855:[E|Neutral] No.[P] If you will recall, my memory drives were wiped.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Unit 2856, you will tell us where the administrator is so you may face her judgement.[P] Comply.[B][C]") ]])
    fnCutscene([[ Append("2856:[E|Neutral] What memories do you have of the administrator?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] I was created in a laboratory in the Arcane University and put through a series of trials.[P] I passed these trials with exemplary grades.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] The administrator, whose design I was constructed from, then informed me that I may take whatever knowledge I needed from the archives in Regulus City.[P] I was then sent to Earth, and recalled here after 33 years.[B][C]") ]])
    fnCutscene([[ Append("2856:[E|Neutral] Interesting.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] These memories are artificial.[P] The administrator gave me them, and will reconstruct them if my purpose changes.[P] I accept this.[B][C]") ]])
    fnCutscene([[ Append("2856:[E|Neutral] But you did not meet the administrator in person?[P] Does this upset you?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Any memories of the administrator's physical appearance would be likewise artificial.[P] I have never met them.[P] I am not upset by this.[B][C]") ]])
    fnCutscene([[ Append("2856:[E|Neutral] Amusing, in a way.[P] Perhaps the administrator has a sense of humour.[P] Stranger things have happened.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Do not resist arrest.[B][C]") ]])
    fnCutscene([[ Append("2856:[E|Neutral] I am not.[P] I will come quietly.[B][C]") ]])
    fnCutscene([[ Append("2856:[E|Neutral] In fact, the administrator will meet us not far from here.[P] I've just received her message.[B][C]") ]])
    fnCutscene([[ Append("2855:[E|Neutral] Was this recent?[B][C]") ]])
    fnCutscene([[ Append("2856:[E|Neutral] Yes.[P] My PDU just confirmed it.[B][C]") ]])
    fnCutscene([[ Append("2856:[E|Neutral] As there is a rebellion in progress, I suggest we do not stand on principle any longer.[P] Unit 771852, proceed.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Unit 2855.[B][C]") ]])
    fnCutscene([[ Append("2855:[E|Neutral] Yes?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Christine", 20.25, 8.50)
    fnCutsceneBlocker()
    fnCutsceneFace("Christine", -1, 0)
    fnCutsceneFace("Tiffany", 1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "2856", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Tiffany", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] My baseline confirmation.[B][C]") ]])
    fnCutscene([[ Append("2855:[E|Neutral] When you touch her, the way you did before, is your hand a weapon, or something else?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Why did you select that phrase?[P] What was the seed number?[B][C]") ]])
    fnCutscene([[ Append("2856:[E|Neutral] Why does the seed number matter?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] The same phrase could be established by multiple seed numbers, theoretically.[P] I assume Unit 2855 is using a 256-bit seed set?[B][C]") ]])
    fnCutscene([[ Append("2856:[E|Neutral] Correct.[B][C]") ]])
    fnCutscene([[ Append("2855:[E|Neutral] Yes.[P] Baselines are meant to use 256-bit seeds.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Then what was the seed number?[P] When I touch her, the way I did before?[B][C]") ]])
    fnCutscene([[ Append("2855:[E|Neutral] The seed number I selected was...[B][C]") ]])
    fnCutscene([[ Append("2855:[E|Neutral] 499323.[B][C]") ]])
    if(bIsCorruption == false) then
        fnCutscene([[ Append("Christine:[E|Neutral] ...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] I love you, 55.[P] I want you to know that.[B][C]") ]])
        fnCutscene([[ Append("2855:[E|Smirk] I love you too, Christine.[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Yelling] WHAT!?") ]])
    else
        fnCutscene([[ Append("Christine:[E|Serious] (Unit 2855 is still maverick. Unit 2856 is not aware.)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Serious] (I will play along with her until a better opportunity presents itself.[P] She must be terminated.)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Then what are we waiting for?[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Yelling] ... DAMN IT!!!") ]])
    end
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --2856 runs.
    fnCutsceneMove("2856", 17.25, 7.50, 1.50)
    fnCutsceneFace("2856", 0, 1)
    fnCutsceneMove("Tiffany", 16.25, 8.50, 1.50)
    fnCutsceneFace("Tiffany", 1, -1)
    fnCutsceneMove("Christine", 17.75, 8.50, 1.50)
    fnCutsceneFace("Christine", -1, -1)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "2856", "Yelling") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] There is no need for violence, Unit 2856.[P] You are outmatched and unarmed.[P] We have extensive combat experience, you do not.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Further, you understand our physical specifications.[P] You cannot escape.[B][C]") ]])
    fnCutscene([[ Append("2856:[E|Yelling] YOU ROTTEN MAVERICK SCUM!![B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Please sister.[P] Have a little dignity.[B][C]") ]])
    fnCutscene([[ Append("2856:[E|Angry] ...[B][C]") ]])
    fnCutscene([[ Append("2856:[E|Neutral] Very well.[P] I will not be escaping this situation.[B][C]") ]])
    fnCutscene([[ Append("2856:[E|Neutral] Not that it matters in the end.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Good.[P] Despite the circumstances, I did not want to hurt you.[B][C]") ]])
    if(bIsCorruption == false) then
        fnCutscene([[ Append("Christine:[E|Offended] That makes one of us.[B][C]") ]])
    else
        fnCutscene([[ Append("Christine:[E|Serious] (The administrator would likely terminate the maverick Unit 2856.[P] I should not compromise my cover to protect her.)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Serious] (Indeed, I may be rewarded for efficiency.[P] The administrator's will must be executed.)[B][C]") ]])
    end
    fnCutscene([[ Append("2856:[E|Neutral] So then what are you going to do?[P] I doubt you'll be able to keep me in any sort of prison.[B][C]") ]])
    fnCutscene([[ Append("2856:[E|Neutral] Or do you think your subordinates will be as kind to me as you are?[P] You can't keep watch on me forever.[B][C]") ]])
    fnCutscene([[ Append("2856:[E|Punchable] ...[P] Sister, I do not hear you chiming in.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Because I do not disagree.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Tiffany", 16.25, 7.50, 0.50)
    fnCutsceneFace("Tiffany", 1, 0)
    fnCutsceneBlocker()
    fnCutsceneFace("2856", -1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("55:[VOICE|Tiffany] On your knees.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneSetFrame("Tiffany", "Exec0")
    fnCutsceneTeleport("2856", -1.25, -1.50)
    fnCutsceneBlocker()
    fnCutsceneSetFrame("Tiffany", "Exec1")
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneSetFrame("Tiffany", "Exec2")
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneSetFrame("Tiffany", "Exec3")
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneSetFrame("Tiffany", "Exec4")
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneSetFrame("Tiffany", "Exec5")
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "2856", "Neutral") ]])
    if(bIsCorruption == false) then
        fnCutscene([[ Append("Christine:[E|Neutral] 55.[P] Why is your gun out?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Because, Christine, I intend to retire Unit 2856.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] B-[P]but you can't.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] P-[P]please think about what you are doing.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] I am not angry or vengeful, Christine.[P] I have been thinking about this moment since I first saw her face in the LRT Facility.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] I have been planning it, thinking about what we would say.[P] I have mapped out the scenario a thousand times.[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Punchable] Oh?[P] Did it go the way you thought it would?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Reality refused to accommodate my plans, as it often does.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] In those plans, Christine tries to stop me but is too late.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] In those plans,[P][P] I can pull the trigger...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] 55.[P] Please.[P] We don't execute prisoners.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] We will not be able to contain her.[P] She is too dangerous to allow to live.[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Neutral] My sister is right.[P] Do you trust anyone you know to keep someone like me in prison?[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Neutral] Considering some of the things I've seen prisoners say right before I retired them, well, the odds are not on your side.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] But she can be redeemed, 55.[P] You're living proof of that![B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Maybe you're right.[P] Maybe she can.[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Punchable] Fat chance.[P] I'd rather you retire me than expose me to more of your moralizing trash.[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Punchable] The administrator is the Cause of Science.[P] I'd sooner be scrapped than betray the Cause.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] If we let her go, the administrator will retire her.[P] Your assessment was correct.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] But...[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] You have just undergone the mental reprogramming.[P] You know what sort of loyalty it implants into the subject.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] I was not a strong person before, Christine.[P] Do you think she was?[P] Strong enough to overcome it?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] ...[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Punchable] I live because the Cause requires it.[P] I do not fear anything.[P] I am a perfect machine![B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] She's too dangerous to keep prisoner, even if we could.[P] But I can do her this small mercy, Christine.[P] I can let her die on her own terms.[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Neutral] Some choice I have.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] It is still a choice.[P] I will pull the trigger when you tell me to.[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Neutral] ...[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] Wait![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] Unit 2856.[P] You betrayed the administrator, to whom you swore absolute obedience.[P] Why?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] You knew it would lead to your own retirement.[P] How could you disobey like that?[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Neutral] I made the judgement call because I believed the Cause is best served by it not being destroyed.[P] The series of events that would unfold if I did nothing would lead to the eradication of Regulus City.[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Neutral] A command unit is prepared to sacrifice themselves if the administrator so demands it.[P] This is what I have done.[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Punchable] The ultimate act of loyalty.[P] I go willingly to the scrap heap.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] ...[P] I can see the twisted sort of logic there.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] I can understand it.[P] I know what the program tried to do to me.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] But 55 has grown beyond it.[P] She's better than she was.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] I cannot put a personal ideology of redemption ahead of the lives and freedoms of thousands of golems, Christine.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] This is the right decision.[P] Even if it isn't, please let me make it.[P] I'm the only one who can.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] ...........[P] Very well.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] But...[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Christine.[P] Please leave.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] ..?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] This is a personal moment for me.[P] I would like to hear my sister's last words alone.[P] They will be a special thing we shall share.[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Punchable] Should I prepare a list of expletives?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] Please do.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] (Siblings are strange.[P] Glad I was an only child...)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] I'll be right outside...") ]])
    
    --Corrupted!
    else
        fnCutscene([[ Append("Christine:[E|Neutral] 55.[P] Do you intend to retire Unit 2856?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] I do.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Have you considered the ramifications?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] I have.[P] I have been thinking about this moment since I first saw her face in the LRT Facility.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] I have been planning it, thinking about what we would say.[P] I have mapped out the scenario a thousand times.[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Punchable] Oh?[P] Did it go the way you thought it would?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Reality refused to accommodate my plans, as it often does.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] In those plans, Christine tries to stop me but is too late.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] In those plans,[P][P] I can pull the trigger...[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Are you going to stop me, Christine?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] No.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] We will not be able to contain her.[P] She is too dangerous to allow to live.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] I said I was not going to stop you.[P] It is unlikely we will be able to keep her in prison.[P] Any golem we assign as a guard is likely to execute her.[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Neutral] Considering some of the things I've seen prisoners say right before I retired them, well, I'd do it too if I were in their place.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] You will not be missed, 2856.[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Neutral] Huh.[P] Interesting.[P] I understand.[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Neutral] Do it, sister.[P] I have no regrets.[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Neutral] A command unit is prepared to sacrifice themselves if the administrator so demands it.[P] This is what I have done.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] You believed you were serving the administrator by betraying her?[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Neutral] If I did nothing, Vivify would have destroyed us all.[P] I had to betray the Cause to save the Cause.[P] That is all.[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Punchable] The ultimate act of loyalty.[P] I go willingly to the scrap heap.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] You see?[P] She cannot be saved.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] I agree.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] ...[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] I cannot put a personal ideology of redemption ahead of the lives and freedoms of thousands of golems, Christine.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] This is the right decision.[P] Even if it isn't, please let me make it.[P] I'm the only one who can.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] I'm not stopping you.[P] You're arguing against yourself, not me.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] ...[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Christine.[P] Please leave.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Why?[P] I am no stranger to death.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] This is a personal moment for me.[P] I would like to hear my sister's last words alone.[P] They will be a special thing we shall share.[B][C]") ]])
        fnCutscene([[ Append("2856:[E|Punchable] Should I prepare a list of expletives?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] Please do.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Very well.[P] I will be right outside...") ]])
    
    end

    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Fade to black.
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Transition.
    fnCutscene([[ AL_BeginTransitionTo("RegulusFinaleB", "FORCEPOS:18.0x11.0x0") ]])
    fnCutsceneBlocker()

--Next part of the scene.
elseif(sObjectName == "PostScene") then
    
    --Variables:
    local iSX399JoinsParty = VM_GetVar("Root/Variables/Chapter5/Scenes/iSX399JoinsParty", "N")
    local iChristineCorruptedEnding = VM_GetVar("Root/Variables/Chapter5/Scenes/iChristineCorruptedEnding", "N")
    
    --Fade to black.
    fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --SX-399 variant:
    if(iSX399JoinsParty == 1.0) then
    
        --Spawn characters.
        fnSpecialCharacter("SX-399",  -100, -100, gci_Face_South, false, nil)
        fnSpecialCharacter("Tiffany", -100, -100, gci_Face_North, false, nil)
    
        --Position Christine.
        fnCutsceneTeleport("Christine", 9.75, 15.50)
        fnCutsceneFace("Christine", 1, 0)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Start moving while we fade in.
        fnCutsceneMove("Christine", 30.25, 15.50)
        fnCutsceneFace("Christine",  0, -1)
        fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
        fnCutsceneBlocker()
        
        --Wait a bit.
        fnCutsceneWait(45)
        fnCutsceneBlocker()
        
        --55.
        fnCutsceneTeleport("Tiffany", 13.25, 15.25)
        fnCutsceneBlocker()
        fnCutsceneMove("Tiffany", 29.25, 15.50)
        fnCutsceneBlocker()
        fnCutsceneFace("Christine", -1, 0)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
        if(iChristineCorruptedEnding == 0.0) then
            fnCutscene([[ Append("55:[E|Neutral] Unit 771852, where is SX-399?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] I think it's best if you go in first.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] So that I might apologize?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Laugh]  ...[P] So that you can get slapped instead of me![B][C]") ]])
            fnCutscene([[ Append("55:[E|Down] Yes...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] I appreciate the consideration.[P] Allow me to explain myself.[P] I must practice this.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] I'll help coach you.[P] Just be honest.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] ...") ]])
        else
            fnCutscene([[ Append("55:[E|Neutral] Unit 771852, where is SX-399?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] I think it's best if you go in first.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] So that I might apologize?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Yes.[P] We still require her cooperation.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Down] True, but, will she forgive me?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] No matter.[P] I must practice these apologies.[P] I fear they may become frequent.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] ...[P] Here we go.") ]])
            
            
        end
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneMove("Tiffany", 29.25, 14.50)
        fnCutsceneMove("Tiffany", 30.25, 14.50)
        fnCutsceneFace("Tiffany", 0, -1)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Door opens.
        fnCutscene([[ AL_SetProperty("Open Door", "Door") ]])
        fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("55:[VOICE|Tiffany] ...[P] She is not here.[B][C]") ]])
        fnCutscene([[ Append("SX-399:[VOICE|SX-399] WRONGY-O, BUCKAROO!!!") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Knockback.
        fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
        fnCutsceneMoveFace("Tiffany", 30.25, 15.00, 0, -1, 1.50)
        fnCutsceneBlocker()
        fnCutsceneMoveFace("Tiffany", 30.25, 15.50, 0, -1, 1.50)
        fnCutsceneMoveFace("Christine", 31.25, 15.50, 0, -1, 1.50)
        fnCutsceneBlocker()
        fnCutsceneTeleport("SX-399", 29.25, 12.50)
        fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
        fnCutsceneSetFrame("Tiffany", "Wounded")
        fnCutsceneSetFrame("Christine", "Wounded")
        fnCutsceneBlocker()
        fnCutsceneWait(65)
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneMove("SX-399", 30.25, 12.50)
        fnCutsceneFace("SX-399", 0, 1)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "SX-399", "Angry") ]])
        fnCutscene([[ Append("SX-399:[E|Angry] DON'T WORRY, CHRISTINE![P] WE'LL FIND A WAY TO DEPROGRAM YOU![B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Angry] JUST STAND BACK WHILE I TENDERIZE THE ADMINISTRATION'S LAPDOG!") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    
        --Movement.
        fnCutsceneMove("SX-399", 30.25, 14.50)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneMoveFace("SX-399", 28.25, 14.50, 0, 1)
        fnCutsceneMoveFace("Tiffany", 28.25, 15.50, 0, 1)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("SX-399:[VOICE|SX-399] I TRUSTED YOU!") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneFace("SX-399", -1, 0)
        fnCutsceneWait(7)
        fnCutsceneBlocker()
        fnCutsceneFace("SX-399", -1, 1)
        fnCutsceneWait(7)
        fnCutsceneBlocker()
        fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
        fnCutsceneFace("SX-399", 0, 1)
        fnCutsceneBlocker()
        fnCutsceneWait(35)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("SX-399:[VOICE|SX-399] I LOVED YOU!") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneFace("SX-399", 1, 0)
        fnCutsceneWait(7)
        fnCutsceneBlocker()
        fnCutsceneFace("SX-399", 1, 1)
        fnCutsceneWait(7)
        fnCutsceneBlocker()
        fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
        fnCutsceneFace("SX-399", 0, 1)
        fnCutsceneBlocker()
        fnCutsceneWait(35)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("SX-399:[VOICE|SX-399] AND YOU THREW IT AWAY!!![P] WAS IT ALL A LIE?") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneFace("SX-399", -1, 0)
        fnCutsceneWait(7)
        fnCutsceneBlocker()
        fnCutsceneFace("SX-399", -1, 1)
        fnCutsceneWait(7)
        fnCutsceneBlocker()
        fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
        fnCutsceneFace("SX-399", 0, 1)
        fnCutsceneBlocker()
        fnCutsceneWait(35)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("55:[VOICE|Tiffany] It wasn't a lie...") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneFace("SX-399", -1, 0)
        fnCutsceneWait(15)
        fnCutsceneBlocker()
        fnCutsceneFace("SX-399", -1, 1)
        fnCutsceneWait(15)
        fnCutsceneBlocker()
        fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
        fnCutsceneFace("SX-399", 0, 1)
        fnCutsceneBlocker()
        fnCutsceneWait(35)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("55:[VOICE|Tiffany] SX-399...[P] I do love you...") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(55)
        fnCutsceneBlocker()
        fnCutsceneFace("SX-399", -1, 0)
        fnCutsceneWait(35)
        fnCutsceneBlocker()
        fnCutsceneFace("SX-399", -1, 1)
        fnCutsceneWait(35)
        fnCutsceneBlocker()
        fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
        fnCutsceneFace("SX-399", 0, 1)
        fnCutsceneBlocker()
        fnCutsceneWait(35)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("SX-399:[VOICE|SX-399] ...") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(65)
        fnCutsceneBlocker()
        fnCutsceneFace("SX-399", 1, 0)
        fnCutsceneWait(60)
        fnCutsceneBlocker()
        fnCutsceneFace("SX-399", 1, 1)
        fnCutsceneWait(60)
        fnCutsceneBlocker()
        fnCutsceneFace("SX-399", 0, 1)
        fnCutsceneSetFrame("Tiffany", "Crouch")
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneFace("Tiffany", 0, -1)
        fnCutsceneSetFrame("Tiffany", "Null")
        fnCutsceneWait(45)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Tiffany", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
        fnCutscene([[ Append("55:[E|Neutral] SX-399...[P] Will you believe me?[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Neutral] Explain.[P] NOW.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] We needed to know where the administrator was.[P] My sister offered me a deal.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] I took the deal, because the administrator cannot be reached by any one command unit.[P] You need two.[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Neutral] Wrong.[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Angry] EXPLAIN HOW YOU COULD TOY WITH MY EMOTIONS LIKE THAT!!![B][C]") ]])
        fnCutscene([[ Append("55:[E|Down] It was necessary for the mission - [P][CLEAR]") ]])
        fnCutscene([[ Append("SX-399:[E|Angry] I DON'T GIVE A TOSS ABOUT THE MISSION![P] YOU BETRAYED ME!!![B][C]") ]])
        fnCutscene([[ Append("55:[E|Upset] Why do you think I was so upset about you joining this mission unexpectedly?[P] Do you think I had wanted this to happen?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Upset] You are important to me, and I did not want you to get hurt.[P] You came along, and you did.[P] This is what I warned about![B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Angry] DON'T YOU DARE MAKE THIS LOOK LIKE IT'S MY FAULT![B][C]") ]])
        fnCutscene([[ Append("55:[E|Down] It is not.[P] It is not your fault at all.[P] It is mine.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] But you must understand that I was willing to put the survival of the rebellion ahead of my own emotions.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Down] When my sister sent me that message, and the plan formed...[P] I knew this would happen.[P] I knew you would be angry.[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Angry] YER DARN TOOTIN!!![B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Christine.[P] I am failing.[P] If I fail, I...[B][C]") ]])
        fnCutscene([[ Append("55:[E|Cry] Christine![P] Help me![P] Help me...[P] Help me make her love me again...[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Sad] ...[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Sad] Babe, I still love you...[B][C]") ]])
        fnCutscene([[ Append("55:[E|Down] ...") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Christine gets up.
        fnCutsceneSetFrame("Christine", "Crouch")
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneFace("Christine", 0, 1)
        fnCutsceneSetFrame("Christine", "Null")
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        if(iChristineCorruptedEnding == 0.0) then
            fnCutscene([[ Append("Christine:[VOICE|Christine] Ugh, this body is a lot lighter than my golem chassis.[P] I guess I'm supposed to dodge hits like that...") ]])
        else
            fnCutscene([[ Append("Christine:[VOICE|Christine] ...[P] This body's calibrations are incomplete, and it's far lighter than the golem chassis.[P] Impacts should be avoided when possible.") ]])
        end
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneMove("Christine", 29.25, 15.50)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Cry") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SX-399", "Neutral") ]])
        if(iChristineCorruptedEnding == 0.0) then
            fnCutscene([[ Append("Christine:[E|Neutral] Did someone call me?[B][C]") ]])
        else
            fnCutscene([[ Append("Christine:[E|Neutral] We have business to attend to, ladies.[P] We should not be dallying.[B][C]") ]])
        end
        fnCutscene([[ Append("55:[E|Cry] Christine...[P] I...[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Neutral] Christine, are you still you?[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Neutral] Because if this is an elaborate trick again...[B][C]") ]])
        if(iChristineCorruptedEnding == 0.0) then
            fnCutscene([[ Append("Christine:[E|Laugh] I'm not that good of an actress, my dear![B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] What 55 says is true.[B][C]") ]])
        else
            fnCutscene([[ Append("Christine:[E|Neutral] I managed to overcome the reprogramming.[P] I fully retain my sense of self.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] (Evaluating...[P] SX-399's facial features suggest she believes the lie.)[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] Well that's a relief.[B][C]") ]])
        end
        fnCutscene([[ Append("SX-399:[E|Neutral] Were you in on the plan?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Down] She was not.[P] No one was.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] I needed to keep it completely secret.[P] If either of you betrayed even the smallest hint, we would have lost a chance to strike a blow that may win us the war.[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Neutral] I see.[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Neutral] Sorry about the bump, Christine.[B][C]") ]])
        if(iChristineCorruptedEnding == 0.0) then
            fnCutscene([[ Append("Christine:[E|Smirk] No trouble at all.[B][C]") ]])
        else
            fnCutscene([[ Append("Christine:[E|Neutral] This chassis is difficult to damage permanently.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] Uhhhh,[P] okay, Christine.[B][C]") ]])
        end
        fnCutscene([[ Append("SX-399:[E|Neutral] Jeez, 55.[P] You're really putting me through the wringer, here.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Down] I...[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Neutral] You broke my heart there, babe.[P] I really believed it.[B][C]") ]])
        if(iChristineCorruptedEnding == 0.0) then
            fnCutscene([[ Append("Christine:[E|Offended] Me too, actually.[B][C]") ]])
        else
            fnCutscene([[ Append("Christine:[E|Offended] She did what was necessary for the rebellion.[P] Do not judge her harshly.[B][C]") ]])
        end
        fnCutscene([[ Append("55:[E|Down] I apologize.[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Flirt] But you don't need to worry about the love part.[P] I still love you.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Down] But...[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Flirt] Babe, if what you actually did was what you say, I'd come around eventually.[P] You have my best interests at heart.[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Neutral] But, it really, really hurt.[P] Not the shock.[P] The lie.[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Neutral] Lying to your girlfriend is never acceptable.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Down] But the operation...[B][C]") ]])
        fnCutscene([[ Append("SX-399:[E|Neutral] I know. I know it was important.[P] Which is why I will forgive you this time.[B][C]") ]])
        if(iChristineCorruptedEnding == 0.0) then
            fnCutscene([[ Append("Christine:[E|Offended] She's done this to me, too.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] So we're going to fix it.[P] Right now.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] 55, if for any reason you need to perform a deceptive ploy like this in the future, you will give us an encrypted message describing it beforehand.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] You will also give one to Sophie and to JX-101, but unencrypted.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Why?[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] Obviously, to make sure you're not doing something too extreme.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Flirt] If you can get mother and Sophie to sign off on it, then at least it'll spread the burden![P] Ha ha![B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] I trust you, but your judgement does have flaws.[P] There may have been another way.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] So if three people are willing to accept your judgement, then I will, too.[P] And I won't get upset at you.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] You don't have to do it all by yourself.[P] No more taking on all these burdens alone.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] ...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Smirk] I find your terms acceptable, Christine.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Flirt] And now you owe us one.[P] A *big* one.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] A favour of some sort?[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Flirt] To be determined at my leisure, of course.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Heh.[P] Well, here's mine.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] A search.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] A search?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Unit 771852, please elaborate.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] You're hiding something else.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] ...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] I know you, 55.[P] I know you better than you think.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] What would happen if your hack of the loyalty program had failed.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] ...[P] Hack?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Yes![P] You hacked the program that was reprogramming me![P] You helped me stay myself![B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Flirt] Clever![B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Flirt] So the command unit machine thingy rewrites you to be loyal to the administrator, but 55 hacked it so Christine would stay herself![B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Yep![B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Christine, I did not hack the machine.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] ...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Uh, what?[P] You...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] No, Christine.[P] I simply assumed that you would be reprogrammed, and would need to be restored later.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] There was a chance that I would be able to provide assistance, but Unit 2856 was too watchful.[P] I was unable to do so.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] The architecture of the machinery that produces command units is not stored on the network.[P] I was unable to improvise a solution.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] It is a testament to your emotional fortitude that you resisted it.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] But in that memory, I met you, and you told me...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] But you didn't hack the program?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] I did not.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Then...[P] The part of me that wanted to stay true to who I was...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] The part of me that fought the hardest and refused to give up...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] The part of me that ultimately saved the day...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] ...[P] Manifested as my best friend, 55.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Smug] Obviously the part of you that displayed any intelligence at all would need to look like me.[P] Otherwise, you'd be unable to believe it.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Laugh] Ha ha ha![P] Good show![B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Smirk] Awwww...[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] So then what was the backup plan?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Thermobaric explosive charges located next to my power core.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Sad] Oh...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] My...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] A cache of thermobaric explosives were located at the security checkpoint outside the Epsilon labs.[P] They were meant to destroy the Epsilon labs if Vivify could not be contained, but the garrison there was wiped out without detonating them.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] I acquired those explosives and placed them in my power core when you were removing the door.[P] If we had failed to stop Vivify, I would have destroyed the facility entirely.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] But...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Smirk] You did not fail.[P] And now, I have explosives that can destroy several sectors in a single blast.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] If you were reprogrammed, I would have kept up the performance until we encountered the administrator.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] And then, I would subdue you and execute her.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Down] And finally, if I failed to subdue you...[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Sad] You'd blow yourself and the administrator to bits...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Down] ...[P] Yes...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] It's war, SX-399.[P] We're prepared to die for what we believe in.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Down] Whether or not your runestone would save us as it has before, I am not certain.[P] This kind of explosion is not a street fight or a long fall.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Down] I estimated the chances of us surviving as very low.[P] But if we died, destroying Regulus City's leadership would give the rebels a chance.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Down] I was prepared to make my second death mean something.[P] My first one did not.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] And now, we're going to go shoot the administrator instead, and we get to live?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] It's a nice bonus, I will admit.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Yes.[P] I will enjoy living through this.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Smirk] It is 'a nice bonus'.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] So?[P] What are we waiting for?[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Happy] Where's the administrator?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] I know where she is.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Smirk] The Regulus City datacore.[P] A secret facility accessible from several locations in the city, used by prime command units only.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Smirk] The entrances are disguised and the security requires two command unit authenticator chips to access.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Happy] And we - [P][CLEAR]") ]])
            fnCutscene([[ Append("SX-399:[E|Smirk] Do we have those?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Christine has one, and I liberated my sister's.[P] We can enter.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] The closest entry point is a radioactive waste sarcophagus a few sectors from here.[P] We'll have to go on foot.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Thank you, Christine.[P] Were you programmed with their locations?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Yep![P] They even gave me a false memory of entering one![B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Come on, team![P] Let's go liberate a city!") ]])
        
        --Major diversion for corrupted Christine.
        else
            fnCutscene([[ Append("Christine:[E|Offended] She did what was necessary to ensure success.[P] She was willing to put everything behind one singular goal.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] For the right goal, that is quite admirable.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] No, Christine, it isn't.[P] If anyone would say that, I'd be sure it'd be you.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Belief in one singular overriding goal leads one to commit acts of horror.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] We cannot be squeamish about staining our hands in a time of war.[P] We do what we must, or we perish.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Smirk] Heh, 55?[P] She's just taking the opposite side of whatever position you take.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Flirt] It's how she challenges you to grow.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Smirk] I see.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Smirk] Christine, I disagree with your stated position.[P] Even if what we do is necessary, that does not absolve us.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] But you did it anyway, 55.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Correct.[P] That is why I prepared myself to not be forgiven.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] It is the world's fault for forcing me to choose between two bad outcomes.[P] I must shoulder the responsibility.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Smirk] Gosh, I just can't stay angry at you can I?[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Smirk] But, in the future, you'll get sign-off before doing something like this again.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] How so?[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] I trust my mother's judgement, and I trust Sophie's.[P] So, if you need to do something like this in the future, they have to know.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] If both of them agree that it's necessary, then...[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Flirt] At least I'll be upset at three people instead of one![B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] It will also be more likely that three people will be able to think of an alternative than one.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] True.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] So with that out of the way, we should proceed to the administrator's location.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] I know where she is -[P] a radioactive waste sarcophagus a few sectors from here.[P] We can get there on foot.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] (And these mavericks will be led right into a trap...)[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Neutral] Doesn't sound like a nice vacation destination.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] It is a secret facility with several entrances across the city.[P] The loyalty program gave me all their locations and access procedures.[B][C]") ]])
            fnCutscene([[ Append("SX-399:[E|Happy] Keen![P] Let's go give her a trouncing![B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Lead the way, Christine.") ]])
        end
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Add everyone to Christine's party.
        EM_PushEntity("Tiffany")
            local i55ID = RE_GetID()
        DL_PopActiveObject()
        EM_PushEntity("SX-399")
            local iSX399ID = RE_GetID()
        DL_PopActiveObject()
        giFollowersTotal = 2
        gsaFollowerNames = {"Tiffany", "SX-399"}
        giaFollowerIDs   = {i55ID, iSX399ID}

        --Append the tables together.
        AL_SetProperty("Follow Actor ID", i55ID)
        AL_SetProperty("Follow Actor ID", iSX399ID)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 1.0)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iSX399IsFollowing", "N", 1.0)
        
        --Party members.
        AdvCombat_SetProperty("Party Slot", 1, "Tiffany")
        AdvCombat_SetProperty("Party Slot", 2, "SX-399")
        
        --Fold party.
        fnAutoFoldParty()
        fnCutsceneBlocker()
    
    --No-SX variant:
    else
    
        --Spawn characters.
        fnSpecialCharacter("Tiffany", -100, -100, gci_Face_North, false, nil)
    
        --Position Christine.
        fnCutsceneTeleport("Christine", 17.75, 11.50)
        fnCutsceneFace("Christine", 0, -1)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Start moving while we fade in.
        fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
        fnCutsceneBlocker()
        
        --Wait a bit.
        fnCutsceneWait(125)
        fnCutsceneBlocker()
        
        --55.
        fnCutsceneTeleport("Tiffany", 6.75, 16.25)
        fnCutsceneBlocker()
        fnCutsceneMove("Tiffany",  7.25, 15.50)
        fnCutsceneMove("Tiffany", 17.25, 15.50)
        fnCutsceneFace("Tiffany", 1, 0)
        fnCutsceneBlocker()
        fnCutsceneWait(45)
        fnCutsceneBlocker()
        fnCutsceneMove("Christine", 18.25, 11.50)
        fnCutsceneMove("Christine", 18.25, 15.50)
        fnCutsceneFace("Christine", -1, 0)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
        fnCutscene([[ Append("55:[E|Neutral] What were you looking at?[B][C]") ]])
        if(iChristineCorruptedEnding == 1.0) then
            fnCutscene([[ Append("Christine:[E|Serious] (Generating falsehood...)[B][C]") ]])
        end
        fnCutscene([[ Append("Christine:[E|Neutral] Trying to send a message to Sophie.[P] The network is sporadic, I'm not sure if it got through.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] You were careful to avoid revealing this position?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] It's not listed on the network anyway.[P] There's already bouncers in place.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] The code was written by a very clever and extremely thorough person.[P] It's quite impressive.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] I am a likely candidate.[B][C]") ]])
        if(iChristineCorruptedEnding == 0.0) then
            fnCutscene([[ Append("Christine:[E|Laugh] Yes, 55![P] That's what I was implying![B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Oh.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Smirk] Thank you.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Laugh] Anytime.[P] Shall we be off?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Lead the way.[P] We should not keep the administrator waiting.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Actually, 55.[P] Before we go, there's one question I have.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Ask.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] What would you have done if my systems had been compromised?[P] What if we did not defeat the loyalty program?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] We?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] Yeah...[P] you hacked the program, didn't you?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] I did not.[P] My sister was too close, the probability of being discovered was far too high.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] What little information on it I could gather from scraps of command unit emails indicate it preys on emotional flaws.[P] I had no choice but to trust in you.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] So then the part of me that fought back, held on to what I believed in, and ultimately saved me...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Chose to display itself as my best friend, 55.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Smug] Interesting.[P] Your memories refused to be purged and disguised themselves as me?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Smug] I feel...[P] exuberant.[P] Thank you.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] So what was the plan if I was compromised?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Detonate the explosives I have hidden next to my power core.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] Come again?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] While you were removing the door to the Epsilon labs, I acquired a set of explosives that were placed there.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] The explosives were meant to contain Vivify and her minions in the lab.[P] Should they have broken out, the explosives were to be detonated by the security team there.[P] But the garrison was wiped out.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] If you had failed to stop Vivify, I was to destroy myself and her.[P] She is durable, but a thermobaric explosion would likely be enough.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] I'm not so sure about that...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Down] Nor am I, but I was prepared to make that sacrifice.[P] These are the most powerful non-nuclear weapons the Cause of Science has available.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] In any case, that was part of the deal I made with my sister.[P] It would be infinitely better to expose the city to the administrator's ministrations than Vivify's, given the choice.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] But since I did, you still have the explosives?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Correct.[P] If you were compromised, I would wait for the meeting with the administrator and then detonate them even if subdued.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] I can activate the detonation discreetly and distract you through conversation.[P] Then, destroy the administrator and whatever building she is in, as well as a few sectors in every direction.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] And yourself...[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] The smallest price to pay, Christine.[P] Do you remember what I said at the LRT facility?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] That you were a living corpse, to be directed by me.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] I died in Cryogenics.[P] I was prepared to die again, but this time for a cause I believe in.[P] I still believe that.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Smug] But since you were not compromised?[P] I suppose I will have to live with myself.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] Oh how awful for you.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Jokes aside, if I give you the signal...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] The administrator doesn't know what she's in for, and she doesn't have anywhere near enough security to stop us.[P] Don't blow yourself up.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Speaking of, we should probably get going.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Lead the way.") ]])
            fnCutsceneBlocker()
        
        --Corrupted.
        else
            fnCutscene([[ Append("Christine:[E|Neutral] Yes, 55.[P] That's what I was implying![B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Oh.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Smirk] Thank you.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Anytime.[P] Shall we be off?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Serious] (Probability of suspicion -[P] very high.[P] Unit 2855 has a history of doubting her allies.)[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Serious] (I can subdue her myself if necessary.[P] The administrator is in no danger.)[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Lead the way.[P] We should not keep the administrator waiting.") ]])
            fnCutsceneBlocker()
        
        end
    
        --Add everyone to Christine's party.
        EM_PushEntity("Tiffany")
            local i55ID = RE_GetID()
        DL_PopActiveObject()
        giFollowersTotal = 1
        gsaFollowerNames = {"Tiffany"}
        giaFollowerIDs   = {i55ID}

        --Append the tables together.
        AL_SetProperty("Follow Actor ID", i55ID)
        VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 1.0)
        
        --Party members.
        AdvCombat_SetProperty("Party Slot", 1, "Tiffany")
        
        --Fold party.
        fnAutoFoldParty()
        fnCutsceneBlocker()
        
    end
end
