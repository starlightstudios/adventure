-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
    
    -- |[Name Resolve]|
    --Resolve the name of the calling entity.
    local sActorName = TA_GetProperty("Name")
    
    -- |[Miniboss AA]|
    if(sActorName == "RedRobeAA" or sActorName == "RedRobeAB") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] Fight the gravemarker paragon?[B]") ]])
        fnDecisionSet(LM_GetCallStack(0), {{"No", "Skip"}, {"Yes", "Fight"}})
    end
  
elseif(sTopicName == "Skip") then
	WD_SetProperty("Hide")

elseif(sTopicName == "Fight") then
	WD_SetProperty("Hide")
    
    --Resolve which entAppty is being talked to.
    local sUseActorName = "RedRobeAA"
    if(EM_Exists("RedRobeAB")) then sUseActorName = "RedRobeAB" end
    
    --Run subroutine to start the battle.
    EM_PushEntity(sUseActorName)
        fnMinibossDialogue(sUseActorName, fnResolvePath(), {"STD|Gravemarker Paragon"})
    DL_PopActiveObject()
end
