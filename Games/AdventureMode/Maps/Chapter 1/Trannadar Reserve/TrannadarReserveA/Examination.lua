-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= Exits ========================================== ]|
-- |[ ====================================== Examinables ======================================= ]|
-- |[ =============== Testament Sequence =============== ]|
if(sObjectName == "Testament") then
    
    --Unlocks an achievement.
    AM_SetPropertyJournal("Unlock Achievement", "FinishDescent")

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Testament of Marietta, Fourth Flock of the Seventh Herald.[P] Seventh Year of St. Zira.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('At last, faith rewarded, saints be praised.[P] We reach the nostalgic realm.[P] Now comes the most challenging part.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('This realm is larger than the ones I had heard described by other expeditions fortunate enough to locate one.[P] The ocean of white seems to go on forever, and there is no guarantee that the device is on what little land there is.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('There is a forest north of us across the water, and what looks like buildings in the distance.[P] We will be heading that way first.[P] Hopefully the food is edible, as our supplies are getting low.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('We already see more of the buildings we saw earlier, made of the chromite brick.[P] They are very old and sit abandoned.[P] Unfortunately, we find no artifacts around them.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('We will now take a short rest stop before continuing across the bridge to the north.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('May Herald Mir live forever in the glory of the Light.')") ]])
    fnCutsceneBlocker()
    
-- |[ ===================== Other ====================== ]|
elseif(sObjectName == "BridgeOut") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("Mei:[E|Neutral] Looks like the bridge is out, and every fiber in my body is saying to not touch that water.[P] Can we fix it?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] I'd give that a firm 'maybe', if you don't mind doing a lot of hauling through absurdly dangerous areas.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] But ask yourself, do you think this strange, dangerous place is the way back to Earth?[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Sad] I don't.[P] I'm curious, but I think you're right.[P] It'll have to be someone else who plumbs the mysteries here.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] The fact that we made it means it's possible![P] If I'm ever feeling particularly reckless I'll put together an expedition.[P] Mei, we did great, but we gotta let it go.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Sad] Yeah...") ]])
    fnCutsceneBlocker()

-- |[ ========================================== Other ========================================= ]|
-- |[ ========================================== Debug ========================================= ]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
