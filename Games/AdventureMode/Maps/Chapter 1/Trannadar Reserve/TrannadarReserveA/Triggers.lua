-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Cutscene ======================================== ]|
if(sObjectName == "Expanse") then

    -- |[Repeat Check]|
    local iSawNostalgicRealm = VM_GetVar("Root/Variables/Chapter1/Scenes/StarMaus|iSawNostalgicRealm", "N")
    if(iSawNostalgicRealm == 1.0) then return end

    -- |[Flag]|
    VM_SetVar("Root/Variables/Chapter1/Scenes/StarMaus|iSawNostalgicRealm", "N", 1.0)
    
    -- |[Variables]|
    local sMeiForm        = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
    local iHasAlrauneForm = VM_GetVar("Root/Variables/Global/Mei/iHasAlrauneForm", "N")

    -- |[Scene]|
    fnCutsceneMove("Mei", 26.25, 28.50)
    fnCutsceneMove("Florentina", 28.25, 28.50)
    fnCutsceneFace("Mei", -1, 1)
    fnCutsceneFace("Florentina", 1, 1)
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutsceneFace("Mei", 0, 1)
    fnCutsceneFace("Florentina", -1, -1)
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutsceneFace("Mei", 1, 0)
    fnCutsceneFace("Florentina", -1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Diaglogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("Mei:[E|Neutral] I'm going to save us both some time and assume you have no idea where we are.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Surprise] Actually, my first thought was that this might be it.[P] Earth.[P] Right?[P] Different world and all that?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] I don't think you did a lot of describing of what Earth looks like.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Offended] I can tell you right now it's not located underground but has a bizarrely bright white sky and white oceans...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Mei", 0, -1)
    fnCutsceneFace("Florentina", 0, -1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("Mei:[E|Neutral] The rock just goes up...[P] into the sky...[P] Was the descent that long?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] I guess this is what those angels were talking about on their big slabs.[P] Place seems tranquil enough.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] I guess this was a bust in terms of finding you a way home, though.[B][C]") ]])
    if(sMeiForm == "Alraune") then
        fnCutscene([[ Append("Mei:[E|Neutral] Yeah.[P] Hey, I'm not hearing anything from the little ones.[P] Are you?[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Surprise] I'm not, they're quiet.[P] In fact, I don't sense them at all.[P] There's clearly plant life here but no little ones.[B][C]") ]])
    elseif(iHasAlrauneForm == 1.0) then
        fnCutscene([[ Append("Mei:[E|Neutral] What are the plants saying?[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Surprise] ...[P] Nothing![P] Not only are they quiet, I can't sense any little ones at all.[B][C]") ]])
    else
        fnCutscene([[ Append("Florentina:[E|Neutral] Hold on a second, maybe I can ask the locals for directions.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Surprise] Huh?[P] Nothing![P] There's plants here but there's no little ones.[P] They're not saying anything at all.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] I'm not an expert but that doesn't seem possible.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Surprise] It isn't![B][C]") ]])
    end
    fnCutscene([[ Append("Florentina:[E|Neutral] The green beaches, I've heard of that.[P] The white water, nope.[P] Plants but no little ones.[P] This place is weird.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] At the very least, nothing is trying to kill us.[P] We may as well look around.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnAutoFoldParty()
    fnCutsceneBlocker()
end