-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= Exits ========================================== ]|
-- |[ ====================================== Examinables ======================================= ]|
-- |[ =============== Testament Sequence =============== ]|
if(sObjectName == "TestamentA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Testament of Marietta, Fourth Flock of the Seventh Herald.[P] Seventh Year of St. Zira.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('I sense this maze is about to lift, and we are nearing the nostalgic realm.[P] Yet again, this place amazes us.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('There are structures here, and not of the kind made in the tombs above.[P] These are something else, made of red bricks fired from chromite.[P] Something not seen in any other realm, and only known to me as I dabbled in architecture and engineering as a child.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('The skills and rituals needed to cast such bricks have only recently been discovered, yet here they are, far predating our civilization.[P] Did the nostalgic ones know more of our universe than we can even fathom?')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('I hope this brings us ever closer to their artifacts and the understanding that I have been tasked to bring to the surface.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('May Herald Mir live forever in the glory of the Light.')") ]])
    fnCutsceneBlocker()
    
-- |[ ================ Shortcut Switch ================= ]|
elseif(sObjectName == "Switch") then

    -- |[Repeat Check]|
    local iDescentShortcut = VM_GetVar("Root/Variables/Chapter1/Scenes/StarMaus|iDescentShortcut", "N")
    if(iDescentShortcut == 1.0) then return end
    
    -- |[Flag]|
    VM_SetVar("Root/Variables/Chapter1/Scenes/StarMaus|iDescentShortcut", "N", 1.0)
    
    -- |[Scene]|
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscenePlaySound("World|FlipSwitch")
    fnCutsceneLayerDisabled("SwitchOffLo", true)
    fnCutsceneLayerDisabled("SwitchOffHi", true)
    fnCutsceneLayerDisabled("SwitchOnLo", false)
    fnCutsceneLayerDisabled("SwitchOnHi", false)
    AL_SetProperty("Set Collision", 49, 25, 0, 0)
    AL_SetProperty("Set Collision", 49, 26, 0, 0)
    AL_SetProperty("Set Collision", 49, 27, 0, 0)
    AL_SetProperty("Set Collision", 49, 28, 0, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscenePlaySound("World|BlockSlide")
    fnCutsceneLayerDisabled("RemoveBlackout", true)
    fnCutsceneLayerDisabled("RemoveWallHi", true)
    fnCutsceneLayerDisabled("RemoveWallLo", true)
    
-- |[ ========================================== Other ========================================= ]|
-- |[ ========================================== Debug ========================================= ]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end