-- |[ ======================================= Constructor ====================================== ]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = fnResolveDirectory() --Map's name is the name of the folder it is in.
local sLevelMusic = "Mausoleum"
local sMapResolveName = sLevelName

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[ ======================================== Standard ======================================== ]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

    -- |[Construction]|
	--Music.
	AL_SetProperty("Music", sLevelMusic)
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
    
    -- |[Map Handling]|
    --Resolve the map's variable.
    fnStandardCh1Map(sLevelName)
	
	-- |[Spawn NPCS Here]|
	--NPCs set to spawn here will be available for post-exec cutscenes.

-- |[ ======================================== Post-Exec ======================================= ]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	-- |[Spawn NPCs Here]|
	--If an entity needs to spawn after a post-exec cutscene, put them here.

    -- |[Minibosses]|
    fnMinibossConstructor("A", "Chapter1", sLevelName)

    -- |[Layers and Collisions]|
    --Northern door.
    local iHitShortcutSwitch = VM_GetVar("Root/Variables/Chapter1/Scenes/StarMaus|iHitShortcutSwitch", "N")
    if(iHitShortcutSwitch == 0.0) then
        AL_SetProperty("Set Layer Disabled", "SwitchOn", true)
        AL_SetProperty("Set Collision", 14, 6, 0, 1)
        AL_SetProperty("Set Collision", 15, 6, 0, 1)
    else
        AL_SetProperty("Set Layer Disabled", "NorthDoorLower", true)
        AL_SetProperty("Set Layer Disabled", "NorthDoorUpper", true)
        AL_SetProperty("Set Layer Disabled", "SwitchOff", true)
    end

    --Southern door.
    local iOpenedMajorDoor   = VM_GetVar("Root/Variables/Chapter1/Scenes/StarMaus|iOpenedMajorDoor", "N")
    if(iOpenedMajorDoor == 0.0) then
        AL_SetProperty("Set Collision", 14, 16, 0, 1)
        AL_SetProperty("Set Collision", 15, 16, 0, 1)
    else
        AL_SetProperty("Set Layer Disabled", "SouthDoorLower", true)
        AL_SetProperty("Set Layer Disabled", "SouthDoorUpper", true)
    end
end
