-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= Exits ========================================== ]|
-- |[ ====================================== Examinables ======================================= ]|
-- |[ =================== The Switch =================== ]|
if(sObjectName == "Switch") then

    -- |[Repeat Check]|
    local iHitShortcutSwitch = VM_GetVar("Root/Variables/Chapter1/Scenes/StarMaus|iHitShortcutSwitch", "N")
    if(iHitShortcutSwitch == 1.0) then return end
    
    -- |[Flag]|
    VM_SetVar("Root/Variables/Chapter1/Scenes/StarMaus|iHitShortcutSwitch", "N", 1.0)
    
    -- |[Scene]|
    fnCutscenePlaySound("World|FlipSwitch")
    fnCutsceneLayerDisabled("SwitchOff", true)
    fnCutsceneLayerDisabled("SwitchOn", false)
    AL_SetProperty("Set Collision", 14, 6, 0, 27)
    AL_SetProperty("Set Collision", 15, 6, 0, 28)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscenePlaySound("World|BlockSlide")
    fnCutsceneLayerDisabled("NorthDoorLower", true)
    fnCutsceneLayerDisabled("NorthDoorUpper", true)
    
-- |[ ================= Open The Door ================== ]|
elseif(sObjectName == "BigDoorN") then
    local iHitShortcutSwitch = VM_GetVar("Root/Variables/Chapter1/Scenes/StarMaus|iHitShortcutSwitch", "N")
    if(iHitShortcutSwitch == 1.0) then return end
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (An enormous door decorated with skulls.[P] It won't budge.)") ]])
    
elseif(sObjectName == "BigDoorS") then

    -- |[Repeat Check]|
    local iOpenedMajorDoor = VM_GetVar("Root/Variables/Chapter1/Scenes/StarMaus|iOpenedMajorDoor", "N")
    if(iOpenedMajorDoor == 1.0) then return end
    
    -- |[Rusted Key Check]|
    AdInv_SetProperty("Stack Items")
    local iKeyCount = AdInv_GetProperty("Item Count", "Rusted Key")
    
    --No keys:
    if(iKeyCount == 0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (An enormous door decorated with skulls.[P] There are two slots for a key, but I don't have any that fit.)") ]])
        
    --One key:
    elseif(iKeyCount == 1) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (An enormous door decorated with skulls.[P] There are two slots for a key, but I only have one that fits.[P] Looks like I need two keys turned at the same time.)") ]])
    
    --Both keys:
    else
        VM_SetVar("Root/Variables/Chapter1/Scenes/StarMaus|iOpenedMajorDoor", "N", 1.0)
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (An enormous door decorated with skulls.[P] I have both of the keys needed to open it.)") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        fnCutscenePlaySound("World|FlipSwitch")
        AL_SetProperty("Set Collision", 14, 16, 0, 27)
        AL_SetProperty("Set Collision", 15, 16, 0, 28)
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutscenePlaySound("World|BlockSlide")
        fnCutsceneLayerDisabled("SouthDoorLower", true)
        fnCutsceneLayerDisabled("SouthDoorUpper", true)
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    end

-- |[ =============== Testament Sequence =============== ]|
elseif(sObjectName == "TestamentA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Testament of Marietta, Fourth Flock of the Seventh Herald.[P] Seventh Year of St. Zira.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Our mission is failed.[P] I stand here now at the precipice of death, with only two of our stone guardians remaining.[P] I do not have very long before I must go into the Light, where I will join the rest of our expedition.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('The realm of nostalgia did indeed have the artifact, but I cannot explain what it is or what it does.[P] I can only state that whatever expedition is sent to assess it next, you must leave it put.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('I can hardly remember what happened when we located it.[P] The realm of nostalgia is populated, and appears to have a self-sustaining ecosystem of unusual species, none hostile.[P] And yet when we made contact with the artifact, it was as though the very walls and water turned against us.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('I lost the others as I trudged through endless hallways that I could hear reshaping themselves to contain me.[P] The rainwater burned us, and the grass grasped us and made us fall.[P] Our wounds refused to close, and the Light itself became so distant.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Even now, I am uncertain that I truly escaped.[P] I will stand no chance against the creatures of the tomb, I may not even survive my wounds were they not there.[P] So I leave this testament.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Leave this place sealed.[P] Our expedition is gone, and my testament must be heard.[P] Go no further.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('May Herald Mir live forever in the glory of the Light.')") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "TestamentB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Testament of Marietta, Fourth Flock of the Seventh Herald.[P] Seventh Year of St. Zira.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('I believe we have reached the end of this so-called tomb.[P] It is a marvel all its own, but it is not our object.[P] Another expedition will need to be sent later to properly study it, as it calls to mind none of the existing histories.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('There is still some distance to travel yet, so I have left this testament.[P] We were unable to decipher the glyphs in this room, but it is clearly one of significance.[P] The creatures also do not tread here, so this room may be a good place to make a base camp.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('I am excited to see the realm of nostalgia, though I must contain my expectations.[P] This is a holy task, and our expedition is in higher spirits now that we near our goal.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('We press ever onward.[P] May Herald Mir live forever in the glory of the Light.')") ]])
    fnCutsceneBlocker()

-- |[ ===================== Murals ===================== ]|
elseif(sObjectName == "MuralA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Florentina:[VOICE|Florentina] Hey, Mei. You probably don't know this, but this looks like a rough map of Arulenta.[P] That's the continent we're on.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[VOICE|Florentina] The coastlines match but I can't say any of the places do.[P] There's dots in spots where there aren't cities or anything.[B][C]") ]])
    fnCutscene([[ Append("Mei:[VOICE|Mei] Do you think this is a map of other burial sites?[P] There's also some dots in the ocean, or maybe those are islands?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[VOICE|Florentina] No idea, but it's clearly a map.") ]])
    
elseif(sObjectName == "MuralB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A mural depicting people being slaughtered.[P] Humans, animals, monstergirls, they're just being massacred.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The bottom part of the mural depicts coffins and burial urns.[P] Does this mean they were being killed just so they could be buried?[P] Why?)") ]])
    
elseif(sObjectName == "MuralC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A beetle is seen parting the clouds, shining like the sun.[P] I don't know what it means, if anything.)") ]])
    
elseif(sObjectName == "MuralD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (I can sort of intuit the language even though I don't understand any of the words.[P] They're gibberish but they make sense.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The writing indicates that everything, everywhere, is the property of the bricks.[P] Or does that mean, the land?[P] The earth?[P] It really isn't clear.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (It just repeats that, over and over. People belong to the bricks, land belongs to the bricks, animals belong to the bricks, the sun belongs to the bricks...)") ]])
    
elseif(sObjectName == "MuralE") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The mural depicts all the body parts of a human and has labels for all the organs, and how they are meant to be interred.)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A lot of them are to be burned and the ashes put in urns, but special attention is given to the liver and kidneys, which are left in the body and mummified.)") ]])

-- |[ ========================================== Other ========================================= ]|
-- |[ ========================================== Debug ========================================= ]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
