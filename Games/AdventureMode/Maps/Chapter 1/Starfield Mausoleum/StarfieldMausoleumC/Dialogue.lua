-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
    
    -- |[Miniboss AA]|
    if(sActorName == "RedRobeAA") then
        fnMinibossDialogue(sActorName, fnResolvePath(), {"STD|Mirror Image", "STD|Best Friend", "STD|Ghost Maid Paragon"})
    
    -- |[Miniboss AB]|
    else
        fnMinibossDialogue(sActorName, fnResolvePath(), {"STD|Mirror Image", "STD|Best Friend", "STD|Ghost Maid Paragon"})
    end
    
end
