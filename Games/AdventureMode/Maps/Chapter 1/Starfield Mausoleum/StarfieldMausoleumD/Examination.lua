-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= Exits ========================================== ]|
-- |[ ====================================== Examinables ======================================= ]|
-- |[ =============== Testament Sequence =============== ]|
if(sObjectName == "Testament") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Testament of Marietta, Fourth Flock of the Seventh Herald.[P] Seventh Year of St. Zira.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('If this place is some sort of security measure, it is one we defeated.[P] Our guardians of stone are incorruptible, and fear not the hallucinations brought about by this tomb.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('In the creatures stalking these halls, I see former friends.[P] I see members of my own troupe.[P] I see the partirhumans we encountered on the surface above.[P] They are tricks, tricks this place uses to remove interlopers.[P] A test of faith, and I will pass it.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Yet this place soon becomes a labyrinth.[P] Is it protecting something?[P] I read the reports, and none of the other objects had such measures.[P] Is this one special?')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Attempts to decipher the glyphs written everywhere came to no head.[P] Identifying the species buried here was for naught.[P] This is not our goal, we press on.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Below us is a door locked by two keys.[P] Both are needed, and we have already found one.[P] When we find the other we shall advance.[P] I think we are close to the nostalgic realm, this tomb shall soon be behind us.')[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('May Herald Mir live forever in the glory of the Light.')") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "BlankPillar") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A large slab of granite.[P] The surface is very smooth.)") ]])
    fnCutsceneBlocker()

-- |[ ========================================== Other ========================================= ]|
-- |[ ========================================== Debug ========================================= ]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
