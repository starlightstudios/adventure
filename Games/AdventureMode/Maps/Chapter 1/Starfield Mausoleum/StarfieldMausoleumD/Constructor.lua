-- |[ ======================================= Constructor ====================================== ]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = fnResolveDirectory() --Map's name is the name of the folder it is in.
local sLevelMusic = "Mausoleum"
local sMapResolveName = sLevelName

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[ ======================================== Standard ======================================== ]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

    -- |[Construction]|
	--Music.
	AL_SetProperty("Music", sLevelMusic)
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
    
    -- |[Map Handling]|
    --Resolve the map's variable.
    fnStandardCh1Map(sLevelName)
	
	-- |[Spawn NPCS Here]|
	--NPCs set to spawn here will be available for post-exec cutscenes.

-- |[ ======================================== Post-Exec ======================================= ]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	-- |[Spawn NPCs Here]|
	--If an entity needs to spawn after a post-exec cutscene, put them here.

    -- |[Minibosses]|
    fnMinibossConstructor("A", "Chapter1", sLevelName)

    -- |[Opened Up]|
    local iHitShortcutSwitch = VM_GetVar("Root/Variables/Chapter1/Scenes/StarMaus|iHitShortcutSwitch", "N")
    if(iHitShortcutSwitch == 1.0) then
        
        --Remove visible layers.
        AL_SetProperty("Set Layer Disabled", "WallsHiRemove", true)
        
        --Open doors, allowing movement past the walls.
        local iLetter = string.byte("A")
        local sDoorName = "DoorRemove" .. string.char(iLetter)
        while(AL_GetProperty("Does Door Exist", sDoorName) == true) do
            AL_SetProperty("Open Door", sDoorName)
            iLetter = iLetter + 1
            sDoorName = "DoorRemove" .. string.char(iLetter)
        end
    end

end
