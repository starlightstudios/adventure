-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= Exits ========================================== ]|
-- |[ ====================================== Examinables ======================================= ]|
if(sObjectName == "NWCoffin") then
    local iCoffinState = VM_GetVar("Root/Variables/Chapter1/Scenes/StarMaus|iCoffinState", "N")
    if(iCoffinState >= 5.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (The coffin lid is extremely heavy and it gives off an unsettling aura.[P] I have no idea what sort of species is depicted on top.)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (I can see something inside vaguely resembling an eye.[P] Looking at it gives me the creeps.)") ]])
        fnCutsceneBlocker()
        
    elseif(iCoffinState == 3.0) then
        VM_SetVar("Root/Variables/Chapter1/Scenes/StarMaus|iCoffinState", "N", 5.0)
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (The coffin lid is extremely heavy and it gives off an unsettling aura.[P] I have no idea what sort of species is depicted on top.)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (I can see something inside vaguely resembling an eye.[P] Looking at it gives me the creeps.)") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Reveal!
        AL_SetProperty("Set Collision", 46, 11, 0, 0)
        AL_SetProperty("Set Collision", 46, 12, 0, 0)
        fnCutsceneLayerDisabled("SecretBlackout", true)
        fnCutsceneLayerDisabled("SecretWallsHiRemove", true)
        fnCutsceneLayerDisabled("SecretWallsHi", false)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutscenePlaySound("World|BlockSlide")
        fnCutsceneWait(15)
        fnCutsceneBlocker()
        fnCutscenePlaySound("World|BlockSlide")
        fnCutsceneWait(15)
        fnCutsceneBlocker()
        fnCutscenePlaySound("World|BlockSlide")
        fnCutsceneWait(5)
        fnCutsceneBlocker()
        fnCutscenePlaySound("World|BlockSlide")
        fnCutsceneWait(5)
        fnCutsceneBlocker()
        fnCutscenePlaySound("World|BlockSlide")
        fnCutsceneWait(5)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneFace("Mei", 1, 0)
        fnCutsceneFace("Florentina", 1, 0)
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (A secret room opened up!)") ]])
    else
        VM_SetVar("Root/Variables/Chapter1/Scenes/StarMaus|iCoffinState", "N", 0.0)
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (The coffin lid is extremely heavy and it gives off an unsettling aura.[P] I have no idea what sort of species is depicted on top.)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (I can see something inside vaguely resembling an eye.[P] Looking at it gives me the creeps.)") ]])
        fnCutsceneBlocker()
    end

    
elseif(sObjectName == "NECoffin") then
    local iCoffinState = VM_GetVar("Root/Variables/Chapter1/Scenes/StarMaus|iCoffinState", "N")
    if(iCoffinState >= 5.0) then
        
    elseif(iCoffinState == 1.0) then
        VM_SetVar("Root/Variables/Chapter1/Scenes/StarMaus|iCoffinState", "N", 2.0)
    else
        VM_SetVar("Root/Variables/Chapter1/Scenes/StarMaus|iCoffinState", "N", 0.0)
    end

    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The coffin lid is extremely heavy and it gives off an unsettling aura.[P] I have no idea what sort of species is depicted on top.)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "SWCoffin") then
    local iCoffinState = VM_GetVar("Root/Variables/Chapter1/Scenes/StarMaus|iCoffinState", "N")
    if(iCoffinState >= 5.0) then
        
    elseif(iCoffinState == 0.0) then
        VM_SetVar("Root/Variables/Chapter1/Scenes/StarMaus|iCoffinState", "N", 1.0)
    else
        VM_SetVar("Root/Variables/Chapter1/Scenes/StarMaus|iCoffinState", "N", 0.0)
    end

    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The coffin lid is extremely heavy and it gives off an unsettling aura.[P] I have no idea what sort of species is depicted on top.)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "SECoffin") then
    local iCoffinState = VM_GetVar("Root/Variables/Chapter1/Scenes/StarMaus|iCoffinState", "N")
    if(iCoffinState >= 5.0) then
        
    elseif(iCoffinState == 2.0) then
        VM_SetVar("Root/Variables/Chapter1/Scenes/StarMaus|iCoffinState", "N", 3.0)
    else
        VM_SetVar("Root/Variables/Chapter1/Scenes/StarMaus|iCoffinState", "N", 0.0)
    end

    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The coffin lid is extremely heavy and it gives off an unsettling aura.[P] I have no idea what sort of species is depicted on top.)") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "EastCoffin") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Large, heavy coffins of some inscrutable thing.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "CastedGold") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Skeletons that look human, except they've been plated with gold.[P] The joints are fused together, allowing them to stand.[P] Their gaze is haunting.)") ]])
    fnCutsceneBlocker()
    
-- |[ ========================================== Other ========================================= ]|
-- |[ ========================================== Debug ========================================= ]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
