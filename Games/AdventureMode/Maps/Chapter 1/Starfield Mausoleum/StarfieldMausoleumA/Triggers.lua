-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================= Execution ======================================== ]|
if(sObjectName == "Tombed") then

    -- |[Repeat Check]|
    local iSawTomb = VM_GetVar("Root/Variables/Chapter1/Scenes/StarMaus|iSawTomb", "N")
    if(iSawTomb == 1.0) then return end
    
    -- |[Flag]|
    VM_SetVar("Root/Variables/Chapter1/Scenes/StarMaus|iSawTomb", "N", 1.0)
    
    -- |[Variables]|
    local sMeiForm      = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
    local iHasGhostForm = VM_GetVar("Root/Variables/Global/Mei/iHasGhostForm", "N")    
    
    -- |[Scene]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("Mei:[E|Sad] What is this place?[P] It's giving me a very bad feeling.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] Never seen anything like it, but it looks expensive.[P] It's got to be a burial chamber of some sort.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] There's skulls all over the wall.[P] Mostly human, some animal, and a bunch I don't recognize at all.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] Is it a long-lost civilization of some sort?[P] Should we watch out for curses?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] I told you, never seen anything like it.[P] I've been around, Mei.[P] Granted, I don't read a lot of scholarship but I think I'd have heard about this.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] And that means it's up to you if we continue.[P] If you're looking for a way back to Earth, a place I've never heard of, you could do worse than a tomb I've never seen.[B][C]") ]])
    if(sMeiForm == "Ghost") then
        fnCutscene([[ Append("Mei:[E|Neutral] Yet I don't feel the same call of the dead that the ghosts of Quantir make.[P] This place is very different.[P] Don't touch anything.[B][C]") ]])
    elseif(iHasGhostForm == 1.0) then
        fnCutscene([[ Append("Mei:[E|Neutral] It's not at all like it was in the manor with the ghosts.[P] Something is very different.[P] Don't touch anything.[B][C]") ]])
    else
        fnCutscene([[ Append("Mei:[E|Neutral] I take your meaning but I still don't like it.[P] It's freaky.[P] Keep your eyes out and your hands to yourself.[B][C]") ]])
    end
    fnCutscene([[ Append("Florentina:[E|Neutral] Oh, don't worry about me.[P] I know better than to get sticky fingers in a tomb that is definitely haunted.[P] You hire some idiot to do that.[P] Never do it yourself.") ]])
    fnCutsceneBlocker()

end
