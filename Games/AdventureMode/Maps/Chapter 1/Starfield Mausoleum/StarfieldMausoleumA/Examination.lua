-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= Exits ========================================== ]|
-- |[ ====================================== Examinables ======================================= ]|
if(sObjectName == "SwitchCoffin") then
    
    -- |[Not Found Switch]|
    local iActivatedBridge = VM_GetVar("Root/Variables/Chapter1/Scenes/StarMaus|iActivatedBridge", "N")
    local iSawTestamentA   = VM_GetVar("Root/Variables/Chapter1/Scenes/StarMaus|iSawTestamentA", "N")
    if(iActivatedBridge == 0.0 and iSawTestamentA == 1.0) then
    
        --Flag.
        VM_SetVar("Root/Variables/Chapter1/Scenes/StarMaus|iActivatedBridge", "N", 1.0)
    
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (There's a casket here, engraved with lettering.[P] A group of urns stand guard above it, with little heads of animals on top.[P] Presumably there's ashes in them.)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (There's a lever under the casket, just like the slab said.)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ([SOUND|World|FlipSwitch]Sounds like it opened something to the south.)") ]])
        fnCutsceneBlocker()
        
        --Bridge.
        AL_SetProperty("Set Layer Disabled", "Bridge", false)
        for x = 51, 53, 1 do
            for y = 30, 34, 1 do
                AL_SetProperty("Set Collision", x, y, 0, 0)
            end
        end
        
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (There's a casket here, engraved with lettering.[P] A group of urns stand guard above it, with little heads of animals on top.[P] Presumably there's ashes in them.)[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (There's no way I'm going to open any of this to see what's inside.)") ]])
        fnCutsceneBlocker()
    end

elseif(sObjectName == "BlankPillar") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A large slab of granite.[P] The surface is very smooth.)") ]])
    fnCutsceneBlocker()

-- |[ ============== Gravemarker Sequence ============== ]|
elseif(sObjectName == "Gravemarker") then
    
    -- |[Setup]|
    local sMeiForm              = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
    local iHasGravemarkerForm   = VM_GetVar("Root/Variables/Global/Mei/iHasGravemarkerForm", "N")
    local iSawSharelockBasement = VM_GetVar("Root/Variables/Chapter1/Sharelock/iSawSharelockBasement", "N")
    local iSawGravemarker       = VM_GetVar("Root/Variables/Chapter1/Scenes/StarMaus|iSawGravemarker", "N")
    
    -- |[First Time]|
    if(iSawGravemarker == 0.0) then
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter1/Scenes/StarMaus|iSawGravemarker", "N", 1.0)
    
        --Common.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
            
        --Currently a gravemarker:
        if(sMeiForm == "Gravemarker" or iHasGravemarkerForm == 1.0) then
            fnCutscene([[ Append("Mei:[E|Neutral] ...[P] How long have you been here, sister?[P] Your aura is faint but...[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] You sympathizing with those things, now?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Offended] If you have a problem with them, you have a problem with their masters.[P] A gravemarker is someone who made a great sacrifice.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] Then again, was this person a volunteer, or not?[P] There is so little left of her that I couldn't tell you.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] So what are they doing here, holding this thing up?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Offended] I don't know anything about the procedures or rituals, only that she was told to hold this up for eternity, or until ordered otherwise.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] She is using her faith to keep it in pristine condition.[P] Maybe for people like us to find it someday.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Happy] Isn't that nifty.[P] As long as she's not attacking us.") ]])
    
        --Has at least seen gravemarkers:
        elseif(iSawSharelockBasement == 1.0) then
            fnCutscene([[ Append("Mei:[E|Surprise] Florentina![P] This is one of those statue monsters we saw in St. Fora's![B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] Except a lot less hostile.[P] Is it sleeping?[P] Is it dead?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] I can feel some sort of energy in it when I touch it, but it's certainly not attacking.[P] Maybe it was ordered to protect this slab?[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] Maybe.[P] Just don't knock it over and these things will hopefully leave us alone.") ]])
    
        --New monstergirl who dis?
        else
            fnCutscene([[ Append("Mei:[E|Surprise] What an incredible carving![P] No, this has to have been a person, once.[P] There's no way.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] Might be petrification magic.[P] I try to keep an open mind when in a dank cave with weird walls and features.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] I can feel some sort of energy inside it, but it's not reacting to us.[P] Still, don't disturb it.[P] It might be dangerous.") ]])
        end
        fnCutsceneBlocker()
        
    -- |[Repeats]|
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (The statue is supporting a slab with writing on it.[P] It gives off a faint energy when touched, but doesn't react otherwise.)") ]])
        fnCutsceneBlocker()
    end
    
-- |[ =============== Testament Sequence =============== ]|
elseif(sObjectName == "Testament") then
    
    -- |[Setup]|
    local sMeiForm              = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
    local iHasGravemarkerForm   = VM_GetVar("Root/Variables/Global/Mei/iHasGravemarkerForm", "N")
    local iSawTestamentA        = VM_GetVar("Root/Variables/Chapter1/Scenes/StarMaus|iSawTestamentA", "N")

    -- |[First Time]|
    if(iSawTestamentA == 0.0) then
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter1/Scenes/StarMaus|iSawTestamentA", "N", 1.0)
    
        --Common.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        fnCutscene([[ Append("Mei:[E|Neutral] 'Testament of Marietta, Fourth Flock of the Seventh Herald.[P] Seventh Year of St. Zira.'[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Confused] You can read that?[B][C]") ]])
            
        --Currently a gravemarker:
        if(sMeiForm == "Gravemarker" or iHasGravemarkerForm == 1.0) then
            fnCutscene([[ Append("Mei:[E|Offended] Of course I can![P] I -[P] oh my.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Sad] Did they...[P] do something to me when they turned me into a gravemarker?[P] I can read it quite plainly.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] That sounds useful.[P] Why are you upset?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Sad] What other knowledge did they put in my head?[P] Are my feelings my own?[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Happy] Oh quit griping about your cool powers.[P] Everyone changes all the time, it's called life.[P] If you don't want to feel a way about something, don't.[P] You can change yourself.[P] So do it.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] I guess that's true.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Surprise] Well?[P] Finish the taffing story![P] What's the rest of it say?[B][C]") ]])
    
        --New monstergirl who dis?
        else
            fnCutscene([[ Append("Mei:[E|Surprise] I can![P] How?[P] It's plain as day, you're saying you can't?[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] I don't even know what script it is.[P] Doesn't look draconic or anything.[P] Is it an Earth thing?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] If I said it was written in Cantonese...[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Happy] Some kind of magic, then.[P] Well, it's not working on me, so whatever.[P] That's the challenge of being grown different.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Happy] Whoever did the magic on this slab, you have my thanks![P] Okay, I'll read the rest to you, then.[B][C]") ]])
        end
        fnCutscene([[ Append("Mei:[E|Neutral] 'In accordance with the mandates and blessings, I leave this testament to record our expedition on behalf of Seventh Herald Mir, bless her and her office.'[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] 'Our expedition has encountered an anomaly most unexpected.[P] We are in great danger, but our stone guardians protect us.[P] We will continue past this edifice and return later for a more complete survey.'[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] 'These tombs belong to an unknown species or unknown partirhuman, possibly of a dynasty heretofore unspoken of.[P] Whether they are relevant to our greater mission remains to be seen.'[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] 'To prevent unwanted trampling upon this sacred ground, I have ordered our guardians to retract the bridge.[P] The mechanism was hidden in the coffin by whoever created this place. We shall be wary for traps.'[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] 'A thousand curses on any who do not follow the Light should they translate this holy writing and bring sacrilige to this place.[P] The Light claim this place, turn your head if you disbelieve.'[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] 'We now press forward.[P] May Herald Mir live forever in the glory of the Light.'[B][C]") ]])
        if(sMeiForm == "Gravemarker" or iHasGravemarkerForm == 1.0) then
            fnCutscene([[ Append("Mei:[E|Neutral] Seventh Herland Ira seems oddly familiar, like I should know that name.[P] Who was Saint Zira?[B][C]") ]])
        else
            fnCutscene([[ Append("Mei:[E|Neutral] It's written very poetically.[P] Who was Saint Zira?[B][C]") ]])
        end
        fnCutscene([[ Append("Florentina:[E|Neutral] Never heard of her, but this was definitely written by an angel, or a member of their many churches.[P] They periodically declare the era of a saint and then count years after the saint is declared.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] If you really care, I could find out when St. Zira was declared and figure out when this was put here.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] Something for later.[P] Why is that slab over there blank?[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] Beats me.[P] If we want to go further, we need to extend the bridge.") ]])
        fnCutsceneBlocker()

    -- |[Repeats]|
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('Testament of Marietta, Fourth Flock of the Seventh Herald.[P] Seventh Year of St. Zira.')[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('In accordance with the mandates and blessings, I leave this testament to record our expedition on behalf of Seventh Herald Ira, bless her and her office.')[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('Our expedition has encountered an anomaly most unexpected.[P] We are in great danger, but our stone guardians protect us.[P] We will continue past this edifice and return later for a more complete survey.')[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('These tombs belong to an unknown species or unknown partirhuman, possibly of a dynasty heretofore unspoken of.[P] Whether they are relevant to our greater mission remains to be seen.')[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('To prevent unwanted trampling upon this sacred ground, I have ordered our guardians to retract the bridge.[P] The mechanism was hidden in the coffin by whoever created this place.[P] We shall be wary for traps.')[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('A thousand curses on any who do not follow the Light should they translate this holy writing and bring sacrilige to this place.[P] The Light claim this place, turn your head if you disbelieve.')[B][C]") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] ('We now press forward.[P] May Herald Mir live forever in the glory of the Light.')") ]])
        fnCutsceneBlocker()
    end

-- |[ ========================================== Other ========================================= ]|
-- |[ ========================================== Debug ========================================= ]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
