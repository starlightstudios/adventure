-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= Exits ========================================== ]|
if(sObjectName == "Whirlpool") then
    
    --Variables.
    local iHasWisphagForm    = VM_GetVar("Root/Variables/Global/Mei/iHasWisphagForm", "N")
    local iHasMannequinForm  = VM_GetVar("Root/Variables/Global/Mei/iHasMannequinForm", "N")
    
    --If the quest is over, the whirlpool doesn't spawn.
    local iCompletedSequence = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iCompletedSequence", "N")
    if(iCompletedSequence == 1.0) then return end
    
    --Must have either of these forms to transition maps.
    if(iHasWisphagForm == 1.0 or iHasMannequinForm == 1.0) then
        fnExecWaterJumpToMap(33+gcfWhirl2x2OffX, 12+gcfWhirl2x2OffX, "BanditForestE", 1, 1)
    end

-- |[ ========================================== Debug ========================================= ]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
