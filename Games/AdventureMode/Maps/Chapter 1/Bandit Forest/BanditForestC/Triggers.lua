-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
if(sObjectName == "Arrival") then
    --fnExecWaterEmerge(33+gcfWhirl2x2OffX, 12+gcfWhirl2x2OffY, 33.75, 11.50)
    
    -- |[Variables]|
    pfWhirlX = 33+gcfWhirl2x2OffX
    pfWhirlY = 12+gcfWhirl2x2OffY
    pfLandingX = 33.75
    pfLandingY = 11.50
    local iBeganSequence     = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iBeganSequence", "N")
    local iCompletedSequence = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iCompletedSequence", "N")
    
    
    -- |[Listing]|
    --Make a list of all party members, including the leader.
    local saList = {gsPartyLeaderName}
    for i = 1, #gsaFollowerNames, 1 do
        table.insert(saList, gsaFollowerNames[i])
    end
    
    -- |[Map Setup]|
    --Blackout.
    fnCutscene([[ AL_SetProperty("Activate Fade", 0, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Camera.
    fnCutsceneCamera("Position", 1000, pfLandingX, pfLandingY)
    
    -- |[Move Party Offscreen]|
    --Reposition everyone offscreen. They teleport in when they jump out of the water.
    for i = 1, #saList, 1 do
        fnCutsceneTeleport(saList[i], -1.25, -1.50)
    end
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Fade in.
    if(iBeganSequence == 1.0 and iCompletedSequence == 0.0) then
        fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, gcf_Night_R, gcf_Night_G, gcf_Night_B, gcf_Night_A, gcf_Night_R, gcf_Night_G, gcf_Night_B, gcf_Night_A, true) ]])
    else
        fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    end
    fnCutsceneBlocker()

    -- |[Entry]|
    --For each party member.
    for i = 1, #saList, 1 do
        
        --Wait a few ticks for each party member after the first.
        if(i > 1) then
            fnCutsceneWait(15)
            fnCutsceneBlocker()
        end
        
        --Create a splash sprite.
        fnSpawnSplash(saList[i] .. "Splash")
        
        --Emerge from the water.
        fnCutscenePlaySound("World|MedSplash")
        fnCutsceneTeleport(saList[i], pfWhirlX, pfWhirlY)
        fnCutsceneFacePos(saList[i], pfLandingX, pfLandingY)
        fnCutsceneJumpTo(saList[i], pfLandingX, pfLandingY, 15)
        fnCutsceneMoveExpirableSprite(saList[i] .. "Splash", pfWhirlX, pfWhirlY)
        fnCutsceneBlocker()
    end

    -- |[Finish]|
    fnAutoFoldParty()
    fnCutsceneBlocker()
end
