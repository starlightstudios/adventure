-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
if(sObjectName == "Scene") then

    -- |[ ========== Setup ========= ]|
    -- |[Cut to Black]|
    fnCutsceneFadeOut()
    fnCutsceneBlocker()
    
    -- |[Variables]|
    --Simple variables.
    local iTFTPF = 15
    local bMeiWearing = false
    local bFlorentinaWearing = false
    local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
    local iFlorentinaKnowsAboutRune = VM_GetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsAboutRune", "N")
    
    --Resolve who is wearing what accessory.
    AdvCombat_SetProperty("Push Party Member", "Mei")
        local sMeiAccessoryA = AdvCombatEntity_GetProperty("Equipment In Slot S", "Accessory A")
        local sMeiAccessoryB = AdvCombatEntity_GetProperty("Equipment In Slot S", "Accessory B")
    DL_PopActiveObject()
    AdvCombat_SetProperty("Push Party Member", "Florentina")
        local sFloAccessoryA = AdvCombatEntity_GetProperty("Equipment In Slot S", "Accessory A")
        local sFloAccessoryB = AdvCombatEntity_GetProperty("Equipment In Slot S", "Accessory B")
    DL_PopActiveObject()
    if(sMeiAccessoryA == "Diamond Bracelet" or sMeiAccessoryB == "Diamond Bracelet") then
        bMeiWearing = true
    end
    if(sFloAccessoryA == "Ruby Necklace" or sFloAccessoryB == "Ruby Necklace") then
        bFlorentinaWearing = true
    end
    
    --Mark Mei as being in her normal costume. Prevents possible conflicts with human TF sequences.
    VM_SetVar("Root/Variables/Costumes/Mei/sCostumeHuman", "S", "Normal")
    if(sMeiForm == "Human") then
        LM_ExecuteScript(gsCharacterAutoresolve, "Mei")
    end
    
    -- |[Flags]|
    local iIsRelivingScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iIsRelivingScene", "N")
    if(iIsRelivingScene == 0.0) then
        VM_SetVar("Root/Variables/Chapter1/Scenes/Mann|iOverheardBandits", "N", 1.0)
        VM_SetVar("Root/Variables/Chapter1/Scenes/Mann|iSawFort", "N", 1.0)
        VM_SetVar("Root/Variables/Chapter1/Scenes/Mann|iBeganSequence", "N", 1.0)
        VM_SetVar("Root/Variables/Global/Mei/iMeiKnowsFlorentinaTFs", "N", 1.0)
    end
    
    -- |[Position]|
    fnCutsceneTeleport("Mei", 10.25, 8.50)
    fnCutsceneFace("Mei", 1, 0)
    fnCutsceneTeleport("Florentina", 14.25, 8.50)
    fnCutsceneFace("Florentina", -1, 0)
    
    -- |[ ======== Cutscene ======== ]|
    -- |[Timing]|
    fnCutsceneBlocker()
    fnCutsceneWait(325)
    fnCutsceneBlocker()
    
    -- |[Fading]|
    fnCutsceneFadeIn(45)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    if(sMeiForm == "Alraune") then
        fnCutscene([[ Append("Mei:[E|Neutral] Boy am I bushed.[P] Ha![P] That one was an accident.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Surprise] You're a terror.[P] The little ones hear your jokes and spread them across the land.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] Oh, don't be so dramatic.[P] But...") ]])
        
    elseif(sMeiForm == "Bee") then
        fnCutscene([[ Append("Mei:[E|Neutral] *Yawn*[P] Boy, are my wings tired.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] You've got perfectly functional legs, you know.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Offended] Most people would kill for the chance to be a bee and fly around![P] I'm taking advantage of it![B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] Though I'm not used to sleeping on my wings or with my stinger so...") ]])
    
    elseif(sMeiForm == "Ghost") then
        fnCutscene([[ Append("Mei:[E|Neutral] I don't know why, but I'm really tired all of the sudden.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] Odd.[P] I was hoping you'd stay up and keep watch.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Sad] Yeah, it is odd.[P] I shouldn't feel tired.[P] Maybe there's limits even to being dead.") ]])
    
    elseif(sMeiForm == "Gravemarker") then
        fnCutscene([[ Append("Mei:[E|Neutral] I'm feeling extremely tired all of the sudden.[P] Looking at the embers of the fire, just...[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Sad] I shouldn't feel tired, right?[P] Do statues sleep?[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] No idea.[P] Maybe they're usually asleep and you're staying up too long.[P] Strike a pose, why don't you.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Offended] I'm not that kind of statue![P] But...") ]])
    
    elseif(sMeiForm == "Slime") then
        fnCutscene([[ Append("Mei:[E|Neutral] *Yawn*[P] I just want to melt into a big puddle...[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] Walking around with legs of goop must be draining.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Happy] You don't know the half of it![P] But still...") ]])
    
    elseif(sMeiForm == "Mannequin") then
        fnCutscene([[ Append("Mei:[E|Neutral] Odd.[P] I am tired.[P] I just got a feeling of deja vu.[P] Maybe I should transform...[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] Yeah I just felt it too.[P] Weird.") ]])
    
    elseif(sMeiForm == "Werecat") then
        fnCutscene([[ Append("Mei:[E|Happy] Phew.[P] All this prowling is really draining![P] I could go for a catnap![B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] Aren't cats supposed to be nocturnal?[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Blush] Like all good cats, I make sure to keep my energy up by sleeping whenever I feel like it.[P] But...") ]])
    
    elseif(sMeiForm == "Wisphag") then
        fnCutscene([[ Append("Mei:[E|Happy] A hard day's work should be rewarded with a nice slumber on mother earth.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] You sure you're not an alraune?[P] You sound like some of the ones I know.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] I will interrogate that sentiment later.[P] For now...") ]])
    
    --Human/Unhandled.
    else
        fnCutscene([[ Append("Mei:[E|Neutral] I'm exhausted.[P] We've been adventuring really hard since like 9 am.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] Yeah, adventuring is tough on you.[P] You look tired.[B][C]") ]])
    end

    -- |[Transformation]|
    if(sMeiForm ~= "Human") then
        fnCutsceneBlocker()
        
        fnCutsceneWait(30)
        fnCutsceneBlocker()
        Cutscene_CreateEvent("Flash Mei White", "Actor")
            ActorEvent_SetProperty("Subject Name", "Mei")
            ActorEvent_SetProperty("Flashwhite Quickly")
        DL_PopActiveObject()
        fnCutsceneWait(5)
        fnCutsceneBlocker()
        fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Human.lua") ]])
        fnCutsceneWait(gci_Flashwhite_Ticks_Total)
        fnCutsceneBlocker()
        fnCutsceneWait(30)
        fnCutsceneBlocker()
    
        -- |[Dialogue]|
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    end
    if(iFlorentinaKnowsAboutRune == 0 and sMeiForm ~= "Human") then
        VM_SetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsAboutRune", "N", 1.0)
        fnCutscene([[ Append("Florentina:[E|Surprise] Woah![P] You're -[P] how'd you do that?[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Blush] How on earth have I not mentioned this yet?[P] I can transform with my runestone.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Surprise] And you weren't going to tell me?[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Blush] ...[P] slipped my mind?[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Happy] Not looking a gift horse in the mouth or anything here but this opens a wide range of possibilities.[P] You think you've seen everything and then, pow![B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] You know nobody else can do that, right?[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] Yeah, I gathered.[B][C]") ]])
    end
    if(sMeiForm == "Alraune") then
        if(iFlorentinaKnowsAboutRune == 0) then
            fnCutscene([[ Append("Florentina:[E|Neutral] Heh, I guess it gives you a bit of peace from the little ones, eh?[B][C]") ]])
        end
        fnCutscene([[ Append("Mei:[E|Sad] Oh, I can't hear them.[P] It's so quiet now.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Happy] Then why'd you switch back?[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] Not really sure.[P] It just felt right to sleep as a human.[P] Maybe I need to get used to sleeping as an alraune.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Happy] Sleeping in a hotel bed is hard enough, I'm trying to sleep in a transformed body![B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] Don't worry, I'll keep the little ones entertained.[P] Oh, what's that?[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Surprise] A big stinky butthead![P] Woah, no, how dare you call Mei that![P] Mei they are roasting you right now![B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Laugh] I didn't teach them those words.[P] They must have gotten it from you!") ]])
        
    elseif(sMeiForm == "Bee") then
        fnCutscene([[ Append("Florentina:[E|Happy] Shame, I liked you in the bee outfit more.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Sad] I can't hear the hive now...[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] I told them I needed my head quiet to sleep.[P] I hope I can get used to sleeping as a bee, it's very strange.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] Some of the drones are asleep all the time, so we all get the benefits.[P] We switch out who is on sleep duty every day or two to prevent cramps.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] Weird.[P] Hive mind stuff.[P] Does that mean one of you is jacki-[P][C]") ]])
        fnCutscene([[ Append("Mei:[E|Blush] Ehehe...") ]])
    
    elseif(sMeiForm == "Ghost") then
        if(iFlorentinaKnowsAboutRune == 0) then
            fnCutscene([[ Append("Florentina:[E|Neutral] Then allow me to welcome you back to the world of the living.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Blush] Thanks, but I only kinda had one foot in the grave, you know?[B][C]") ]])
        end
        fnCutscene([[ Append("Florentina:[E|Neutral] Still feeling tired?[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Sad] Ugh, even moreso now.[P] If anything, undeath held it at bay.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] I'll feel better after a good night's sleep.") ]])
    
    elseif(sMeiForm == "Gravemarker") then
        fnCutscene([[ Append("Florentina:[E|Neutral] Going to nap as a human?[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] At least I understand how to sleep like this, been doing it my whole life.[P] I can experiment with sleeping as a rock later.") ]])
    
    elseif(sMeiForm == "Slime") then
        if(iFlorentinaKnowsAboutRune == 0) then
            fnCutscene([[ Append("Florentina:[E|Neutral] So was it harder to sleep as a slime or something?[B][C]") ]])
        end
        fnCutscene([[ Append("Mei:[E|Neutral] I'm much better practiced at sleeping as a human.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] Yeah it takes years to master sleeping.[P] Look at kittens.[P] They get a lot of practice.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Smirk] I guess I'm like a baby slime, in a way.[P] Needing to learn how my body works.") ]])
    
    elseif(sMeiForm == "Mannequin") then
        fnCutscene([[ Append("Florentina:[E|Neutral] Still feeling tired?[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Sad] Yeah...[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] Nothing a good sleep won't fix.") ]])
    
    elseif(sMeiForm == "Werecat") then
        if(iFlorentinaKnowsAboutRune == 0) then
            fnCutscene([[ Append("Florentina:[E|Neutral] Anything different? Worse nightvision, stiffer joints?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] Yeah, one bit thing.[B][C]") ]])
        end
        fnCutscene([[ Append("Mei:[E|Happy] Phew![P] Quiet at last![B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] What do you mean?[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Smirk] My heightened cat senses let me hear everything in the forest.[P] Every chirping cricket, rustling bird in its nest, and the flitter of bat wings.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Offended] It's a racket![P] I'm trying to sleep, turn it down!") ]])
    
    elseif(sMeiForm == "Wisphag") then
        if(iFlorentinaKnowsAboutRune == 0) then
            fnCutscene([[ Append("Florentina:[E|Neutral] So how's it feel being human again?[B][C]") ]])
        end
        fnCutscene([[ Append("Mei:[E|Happy] Much better![P] Now I can sleep![B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Smirk] I really should practice sleeping as a wisphag but I'm too tired right now.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] Are you going to rethink that statement at all?[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] Can't.[P] Too tired.") ]])
    
    --Human/Unhandled.
    else
        fnCutscene([[ Append("Mei:[E|Neutral] Nothing a good night's sleep can't fix.") ]])
    end
    fnCutsceneBlocker()

    -- |[Movement]|
    fnCutsceneMove("Mei", 11.25, 6.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Florentina:[VOICE|Florentina] Now where are you going?[B][C]") ]])
    fnCutscene([[ Append("Mei:[VOICE|Mei] I have business to do![P] I'm going to go a few trees over.[B][C]") ]])
    if(bMeiWearing) then
        fnCutscene([[ Append("Florentina:[VOICE|Florentina] Can you leave that diamond bracelet with me?[P] I want to take a look at it.[B][C]") ]])
        fnCutscene([[ Append("Mei:[VOICE|Mei] Surely it can wait.") ]])
    else
        fnCutscene([[ Append("Florentina:[VOICE|Florentina] Hey, you decided to wear that diamond bracelet after all?[B][C]") ]])
        fnCutscene([[ Append("Mei:[VOICE|Mei] I was just trying it on.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[VOICE|Florentina] I wanted to take a closer look at it.[B][C]") ]])
        fnCutscene([[ Append("Mei:[VOICE|Mei] It can wait two minutes![P] I gotta go!") ]])
    end
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneMove("Mei", 13.25, 5.50)
    fnCutsceneMove("Florentina", 13.25, 8.50)
    fnCutsceneBlocker()
    fnCutsceneFace("Florentina", 0, 1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Examination]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Florentina:[VOICE|Florentina] I'll settle for this ruby necklace.[P] Bit of an enchantment, nothing major, but there's another magic hidden under it.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[VOICE|Florentina] I swear I've seen this before, but where?[P] It was recent, too.") ]])
    fnCutsceneBlocker()
    fnCutsceneFace("Mei", 0, 1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Sequence.
    for i = 0, 4, 1 do
        fnCutsceneSetFrame("Mei", "MannTF"..i)
        fnCutsceneWait(iTFTPF)
        fnCutsceneBlocker()
    end
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Florentina:[VOICE|Florentina] Come to think of it, it was when we were talking to Victoria.[P] Not her, but the display pieces.[P] Do all her pieces have this?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[VOICE|Florentina] Yeah, that's definitely a spell.[P] She cast it but she didn't make it, it's too clumsy.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Sequence.
    for i = 5, 6, 1 do
        fnCutsceneSetFrame("Mei", "MannTF"..i)
        fnCutsceneWait(iTFTPF)
        fnCutsceneBlocker()
    end
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Florentina:[VOICE|Florentina] She's a mage, all right, but this spell should be smooth and it isn't.[P] It wasn't her first time, either.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[VOICE|Florentina] Hm?[P] Quiet, little ones, I'm trying to focus.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Sequence.
    for i = 7, 10, 1 do
        fnCutsceneSetFrame("Mei", "MannTF"..i)
        fnCutsceneWait(iTFTPF)
        fnCutsceneBlocker()
    end
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Florentina:[VOICE|Florentina] Then why all the -[P] oh, I must be losing my touch.[P] It's a...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Sequence.
    for i = 11, 12, 1 do
        fnCutsceneSetFrame("Mei", "MannTF"..i)
        fnCutsceneWait(iTFTPF)
        fnCutsceneBlocker()
    end
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Florentina:[VOICE|Florentina] Damn it, it's a curse![P] She must have known these items were cursed and is trying to offload them on a sucker.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[VOICE|Florentina] Good thing I didn't put it on for...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    local i = 0
    for y = 5.50, 6.50, 0.05 do
        
        --Position.
        fnCutsceneTeleport("Mei", 13.25, y)
        
        --Frame advance.
        i = i + 1
        if(i < 10) then
            fnCutsceneSetFrame("Mei", "MannTF13")
        else
            fnCutsceneSetFrame("Mei", "MannTF14")
        end
        fnCutsceneWait(5)
        fnCutsceneBlocker()
    end
    fnCutsceneSetFrame("Mei", "MannTF15")
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    i = 0
    for y = 6.50, 7.50, 0.05 do
        
        --Position.
        fnCutsceneTeleport("Mei", 13.25, y)
        
        --Frame advance.
        i = i + 1
        if(i < 10) then
            fnCutsceneSetFrame("Mei", "MannTF15")
        else
            fnCutsceneSetFrame("Mei", "MannTF16")
        end
        fnCutsceneWait(5)
        fnCutsceneBlocker()
    end
    fnCutsceneSetFrame("Mei", "MannTF15")
    
    -- |[Cut To Black]|
    fnCutsceneFadeOut()
    fnCutsceneBlocker()
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Florentina:[VOICE|Florentina] Get off me![P] Control yourself -[P] Mei![P] Mei -") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    fnCutsceneSetFrame("Mei", "Null")

    -- |[ ==================== Finish Up =================== ]|
    -- |[Relive Handling]|
    --If this is a reliving sequence, end it here.
    if(iIsRelivingScene == 1.0) then
        
        --Wait a bit.
        fnCutsceneWait(10)
        fnCutsceneBlocker()
        
        --Return to the last save point and execute the post-script..
        fnCutscene([[ AL_BeginTransitionTo("LASTSAVE", gsStandardReliveEnd) ]])
        fnCutsceneBlocker()

    -- |[Finish Up]|
    --Mei is not reliving this, it's real!
    else
        fnCutscene([[ AL_BeginTransitionTo("BanditForestA", "FORCEPOS:1.0x1.0x0") ]])
    end
end
