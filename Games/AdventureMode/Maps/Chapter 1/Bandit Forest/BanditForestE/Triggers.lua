-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
if(sObjectName == "Arrival") then
    fnExecWaterEmerge(20+gcfWhirl3x2OffX, 7+gcfWhirl3x2OffY, 21.25, 6.50)
    
elseif(sObjectName == "Overhear") then
    
    -- |[Repeat Check]|
    local iOverheardBandits = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iOverheardBandits", "N")
    if(iOverheardBandits == 1.0) then return end
    
    -- |[Flag]|
    VM_SetVar("Root/Variables/Chapter1/Scenes/Mann|iOverheardBandits", "N", 1.0)
    
    -- |[Spawn]|
    TA_Create("BanditF")
        TA_SetProperty("Position", -10, -10)
        TA_SetProperty("Facing", gci_Face_South)
        TA_SetProperty("Clipping Flag", false)
        fnSetCharacterGraphics("Root/Images/Sprites/Bandit_Female/", false)
    DL_PopActiveObject()
    TA_Create("BanditM")
        TA_SetProperty("Position", -10, -10)
        TA_SetProperty("Facing", gci_Face_South)
        TA_SetProperty("Clipping Flag", false)
        fnSetCharacterGraphics("Root/Images/Sprites/Bandit_Male/", false)
    DL_PopActiveObject()
    
    -- |[Camera]|
    fnCutsceneCamera("Position", gcfCamera_Cutscene_Speed, 13.25, 8.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Positioning]|
    fnCutscenePlaySound("World|Stairs")
    fnCutsceneTeleport("BanditF", 16.25, 8.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneMove("BanditF", 16.25, 6.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("BanditF", 0, 1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscenePlaySound("World|Stairs")
    fnCutsceneTeleport("BanditM", 16.25, 8.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneMove("BanditF", 13.25, 6.50)
    fnCutsceneFace("BanditF", 0, 1)
    fnCutsceneMove("BanditM", 16.25, 7.50)
    fnCutsceneFace("BanditM", -1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("BanditF:[VOICE|CultistF] Hey you heard about that mess downriver?[P] Big boat capsize a week back?[B][C]") ]])
    fnCutscene([[ Append("BanditM:[VOICE|CultistM] Intentional.[B][C]") ]])
    fnCutscene([[ Append("BanditF:[VOICE|CultistF] What makes you say that?[B][C]") ]])
    fnCutscene([[ Append("BanditM:[VOICE|CultistM] I heard that the hull got torn out from the inside.[P] Boards ripped up by hand.[B][C]") ]])
    fnCutscene([[ Append("BanditF:[VOICE|CultistF] Oh, not this again.[B][C]") ]])
    fnCutscene([[ Append("BanditM:[VOICE|CultistM] I'm telling you, I see them at night![P] They're at the edge of the forest, just out of sight.[P] They're watching us.[B][C]") ]])
    fnCutscene([[ Append("BanditF:[VOICE|CultistF] And yet when I get night watch I don't see anything.[B][C]") ]])
    fnCutscene([[ Append("BanditM:[VOICE|CultistM] Because you're not looking![B][C]") ]])
    fnCutscene([[ Append("BanditF:[VOICE|CultistF] And now you think they tore a ship apart from the inside?[P] How'd they get on the ship?[P] Can they swim?[B][C]") ]])
    fnCutscene([[ Append("BanditM:[VOICE|CultistM] It's connected, I'm telling you.[B][C]") ]])
    fnCutscene([[ Append("BanditF:[VOICE|CultistF] You're paranoid.[P] Tell you what, next time you see those creeps at night, why don't you go out and get them?[B][C]") ]])
    fnCutscene([[ Append("BanditM:[VOICE|CultistM] And leave the safety of the fort to go into an obvious trap?[P] This is why we have forts.[B][C]") ]])
    fnCutscene([[ Append("BanditF:[VOICE|CultistF] Is that why you keep volunteering for guard duty?[P] And here I thought you just didn't want to risk some caravan guard getting the drop on you.[B][C]") ]])
    fnCutscene([[ Append("BanditM:[VOICE|CultistM] I'm not risking spending a night in the forest.[B][C]") ]])
    fnCutscene([[ Append("BanditM:[VOICE|CultistM] Shh![P] Did you hear that?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("BanditM", 1, 0)
    fnCutsceneFace("BanditF", 1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(85)
    fnCutsceneBlocker()
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("BanditM:[VOICE|CultistM] It sounded like something was on the other side of the wall.[B][C]") ]])
    fnCutscene([[ Append("BanditF:[VOICE|CultistF] We're in a waterhole![P] The other side of that wall is solid rock you doofus.[B][C]") ]])
    fnCutscene([[ Append("BanditM:[VOICE|CultistM] Just finish up and let's get out of here.[B][C]") ]])
    fnCutscene([[ Append("BanditF:[VOICE|CultistF] Fine.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneMove("BanditM", 16.25, 8.50)
    fnCutsceneBlocker()
    fnCutscenePlaySound("World|Stairs")
    fnCutsceneTeleport("BanditM", -10.25, -10.50)
    fnCutsceneBlocker()
    fnCutsceneFace("BanditF", 0, 1)
    fnCutsceneBlocker()
    fnCutsceneWait(85)
    fnCutsceneBlocker()
    fnCutsceneMove("BanditF", 16.25, 6.50)
    fnCutsceneMove("BanditF", 16.25, 8.50)
    fnCutsceneBlocker()
    fnCutscenePlaySound("World|Stairs")
    fnCutsceneTeleport("BanditF", -10.25, -10.50)
    fnCutsceneBlocker()
    
    -- |[Camera]|
    fnCutsceneCamera("Actor Name", gcfCamera_Cutscene_Speed, "Mei")
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("Florentina:[E|Happy] Something's got these crooks spooked.[P] I like that.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] Little ones mentioned something like what he saw at night on the way in but they don't know what they are.[P] Guy's not hallucinating.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Offended] Good, let them be scared.[P] But...[P] what could they be?[P] Werecats, slimes?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] Don't know.[P] Though I do suspect that's why my shipment is late.[P] Figures the local bandits know my supply lines better than I do.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] Tearing up the ship from the inside...[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] No sense dwelling on it.[P] There's a tiny crack over there but it's way too risky, they might hear us even if we could squeeze through.[P] Let's get out of here.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Finish]|
    fnAutoFoldParty()
    fnCutsceneBlocker()
end
