-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= Exits ========================================== ]|
if(sObjectName == "Whirlpool") then
    
    --Variables.
    local iBeganSequence  = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iBeganSequence", "N")
    local iDefeatedLeader = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iDefeatedLeader", "N")
    
    --Disallow during mannequin sequence.
    if(iBeganSequence == 1.0 and iDefeatedLeader == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Remain.[P] Squeeze through.)") ]])
        fnCutsceneBlocker()
    
    else
        fnExecWaterJumpToMap(20+gcfWhirl3x2OffX, 7+gcfWhirl3x2OffX, "BanditForestC", 1, 1)
    end
    
-- |[ ======================================= Wall Gaps ======================================== ]|
--Left to right, does nothing if not during the mannequin sequence.
elseif(sObjectName == "SlipLft") then

    --Activation check.
    local iBeganSequence  = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iBeganSequence", "N")
    local iDefeatedLeader = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iDefeatedLeader", "N")
    if(iBeganSequence == 0.0 or iDefeatedLeader == 1.0) then return end
    
    --Group up.
    fnCutsceneMergeParty()
    fnCutsceneBlocker()
    
    --Reposition.
    fnCutsceneMove("Mei", 15.25, 11.50)
    fnCutsceneFace("Mei", 1, 0)
    fnCutsceneMove("Florentina", 15.25, 11.50)
    fnCutsceneFace("Florentina", 1, 0)
    fnCutsceneBlocker()
    
    --Slip through.
    for x = 15.25, 18.25, 0.10 do
        fnCutsceneTeleport("Mei", x, 11.50)
        fnCutsceneTeleport("Florentina", x, 11.50)
        fnCutsceneBlocker()
    end
    fnCutsceneFace("Mei", 0, -1)
    fnCutsceneFace("Florentina", 0, -1)
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    for y = 11.25, 10.25, -0.10 do
        fnCutsceneTeleport("Mei", 18.25, y)
        fnCutsceneTeleport("Florentina", 18.25, y)
        fnCutsceneBlocker()
    end
    fnAutoFoldParty()
    fnCutsceneBlocker()

--Right to left, does nothing if not during the mannequin sequence.
elseif(sObjectName == "SlipRgt") then

    --Activation check.
    local iBeganSequence  = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iBeganSequence", "N")
    local iDefeatedLeader = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iDefeatedLeader", "N")
    if(iBeganSequence == 0.0 or iDefeatedLeader == 1.0) then return end
    
    --Group up.
    fnCutsceneMergeParty()
    fnCutsceneBlocker()
    
    --Reposition.
    fnCutsceneMove("Mei", 18.25, 10.50)
    fnCutsceneFace("Mei", 0, 1)
    fnCutsceneMove("Florentina", 18.25, 10.50)
    fnCutsceneFace("Florentina", 0, 1)
    fnCutsceneBlocker()
    
    --Slip through.
    for y = 10.25, 11.25, 0.10 do
        fnCutsceneTeleport("Mei", 18.25, y)
        fnCutsceneTeleport("Florentina", 18.25, y)
        fnCutsceneBlocker()
    end
    fnCutsceneFace("Mei", -1, 0)
    fnCutsceneFace("Florentina", -1, 0)
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    for x = 18.25, 15.25, -0.10 do
        fnCutsceneTeleport("Mei", x, 11.50)
        fnCutsceneTeleport("Florentina", x, 11.50)
        fnCutsceneBlocker()
    end
    fnAutoFoldParty()
    fnCutsceneBlocker()

-- |[ ========================================== Debug ========================================= ]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
