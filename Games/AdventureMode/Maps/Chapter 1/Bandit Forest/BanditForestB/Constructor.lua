-- |[ ======================================= Constructor ====================================== ]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = fnResolveDirectory() --Map's name is the name of the folder it is in.
local sLevelMusic = "ForestTheme"
local sMapResolveName = "Map Resolve Name"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[ ======================================== Standard ======================================== ]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

    -- |[Construction]|
	--Music.
	AL_SetProperty("Music", sLevelMusic)
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
    
    -- |[Map Handling]|
    --Resolve the map's variable.
    fnStandardCh1Map(sLevelName)

-- |[ ======================================== Post-Exec ======================================= ]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	-- |[NPC Spawning]|
	--If an entity needs to spawn after a post-exec cutscene, put them here.
    local iHeardDog          = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iHeardDog", "N")
    local iPolarisRunIn      = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iPolarisRunIn", "N")
    local iBeganSequence     = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iBeganSequence", "N")
    local iCompletedSequence = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iCompletedSequence", "N")
    if(iHeardDog == 1.0 and iCompletedSequence == 0.0) then
        fnStandardNPCByPosition("Aquillia")
        fnStandardNPCByPosition("Polaris")
        fnStandardNPCByPosition("Dot")
        if(iPolarisRunIn == 0.0) then
            fnCutsceneTeleport("Polaris", -1.25, -1.50)
        end
    else
        AL_SetProperty("Set Layer Disabled", "Boat", true)
    end
    
    -- |[Enemy Despawning]|
    --If the player has not begun the mannequin sequence, nothing despawns.
    if(iBeganSequence == 0.0 or iCompletedSequence == 1.0) then
    
    --Player has begun the mannequin sequence, and has not completed it, so despawn all enemies.
    else
        local saDespawnList = {"EnemyAA", "EnemyAB", "EnemyAC", "EnemyBA", "EnemyBB", "EnemyCA", "EnemyCB"}
        for i = 1, #saDespawnList, 1 do
            EM_PushEntity(saDespawnList[i])
                RE_SetDestruct(true)
            DL_PopActiveObject()
        end
    end

    -- |[Overlays]|
    --Night time.
    if(iBeganSequence == 1.0 and iCompletedSequence == 0.0) then
        AL_SetProperty("Music", "Null")
        AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, gcf_Night_R, gcf_Night_G, gcf_Night_B, gcf_Night_A, gcf_Night_R, gcf_Night_G, gcf_Night_B, gcf_Night_A, true)
    end
    
    -- |[Skillbooks]|
    --Place skillbook overlay code here.

end
