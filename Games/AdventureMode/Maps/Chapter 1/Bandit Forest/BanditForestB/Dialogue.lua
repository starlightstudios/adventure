-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then

    --Resolve the name of the calling entity.
    local sActorName = TA_GetProperty("Name")
    
    --Common.
	TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[Polaris]|
    if(sActorName == "Polaris") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Polaris:[VOICE|Polaris] Go bash that wannabe-witch's hat in for me.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[VOICE|Polaris] Oh no, lack of sleep is making me catty.[P] I'm sorry.") ]])
        fnCutsceneBlocker()
    
    -- |[Aquillia]|
    elseif(sActorName == "Aquillia") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Aquillia:[VOICE|Aquillia] Don't worry about me, I've got Dot here to protect me.[P] Go bash some heads.") ]])
        fnCutsceneBlocker()
    
    -- |[Dot]|
    elseif(sActorName == "Dot") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Despite it being the middle of the night, Dot is wagging her tail happily.)") ]])
        fnCutsceneBlocker()
    end
end
