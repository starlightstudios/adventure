-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ============================ Polaris Ambushes the Mannequins! ============================ ]|
if(sObjectName == "Ambush") then

    -- |[Activation/Repeat Check]|
    local iHeardDog     = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iHeardDog", "N")
    local iPolarisRunIn = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iPolarisRunIn", "N")
    if(iHeardDog == 0.0 or iPolarisRunIn == 1.0) then return end

    -- |[Flag]|
    VM_SetVar("Root/Variables/Chapter1/Scenes/Mann|iPolarisRunIn", "N", 1.0)
    
    --Loading order.
    fnLoadDelayedBitmapsFromList("Chapter 1 Mei Runestone", gciDelayedLoadLoadAtEndOfTick)
    AdvCombat_SetProperty("Push Party Member", "Mei")
        AdvCombatEntity_SetProperty("Display Name", "DEFAULT")
    DL_PopActiveObject()
    AdvCombat_SetProperty("Push Party Member", "Florentina")
        AdvCombatEntity_SetProperty("Display Name", "DEFAULT")
    DL_PopActiveObject()
    
    -- |[Setup]|
	TA_Create("PolarisPoof")
		TA_SetProperty("Position", -100, -100)
		TA_SetProperty("Rendering Depth", 0.000000)
		TA_SetProperty("Walk Ticks Per Frame", 5)
        TA_SetProperty("Add Special Frame", "Poof0", "Root/Images/Sprites/EnemyPoof/0")
        TA_SetProperty("Add Special Frame", "Poof1", "Root/Images/Sprites/EnemyPoof/1")
        TA_SetProperty("Add Special Frame", "Poof2", "Root/Images/Sprites/EnemyPoof/2")
        TA_SetProperty("Add Special Frame", "Poof3", "Root/Images/Sprites/EnemyPoof/3")
	DL_PopActiveObject()

    -- |[Movement]|
    fnCutsceneMove("Mei", 19.25, 40.50)
    fnCutsceneFace("Mei", -1, 0)
    fnCutsceneMove("Florentina", 19.25, 41.50)
    fnCutsceneFace("Florentina", -1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    -- |[Dialogue]|
    fnCutscenePlaySound("World|DogBark")
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Mannequin:[VOICE|Mei] (Silence the dog.)[B][C]") ]])
    fnCutscene([[ Append("Mannequin:[VOICE|Florentina] (Silence the dog.)[B][C]") ]])
    fnCutscene([[ Append("Aquillia:[VOICE|Aquillia] Now!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutscenePlaySound("World|MagicB")
    fnCutsceneWait(5)
    fnCutsceneBlocker()
    fnCutsceneSetFrame("PolarisPoof", "Poof0")
    fnCutsceneTeleport("PolarisPoof", 20.25, 40.50)
    fnCutsceneWait(5)
    fnCutsceneBlocker()
    fnCutsceneSetFrame("PolarisPoof", "Poof1")
    fnCutsceneTeleport("Polaris", 20.25, 40.50)
    fnCutsceneFace("Polaris", -1, 0)
    fnCutsceneWait(5)
    fnCutsceneBlocker()
    fnCutsceneSetFrame("PolarisPoof", "Poof2")
    fnCutsceneWait(5)
    fnCutsceneBlocker()
    fnCutsceneSetFrame("PolarisPoof", "Poof3")
    fnCutsceneWait(5)
    fnCutsceneBlocker()
    fnCutsceneTeleport("PolarisPoof", -1.25, -1.50)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Polaris:[VOICE|Polaris] *cough*[P] Damn it, forgot to remove the smoke from that spell![P] *cough*[B][C]") ]])
    fnCutscene([[ Append("Polaris:[VOICE|Polaris] Ka-runestone!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Runestone Animation]|
    local sAllocationString = "WD_SetProperty(\"Allocate Animation\", gciMeiRuneFramesTotal, 1, -90)"
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Activate Scene Fast") ]])
    fnCutscene(sAllocationString)
    fnCutscene([[ WD_SetProperty("Activate Flash", 0) ]])
    for i = 0, gciMeiRuneFramesTotal-1, 1 do
        local iNumber = string.format("%02i", i)
        local sString = "WD_SetProperty(\"Set Animation Frame\", " .. i .. ", \"Root/Images/Scenes/Mei/RuneAnim" .. iNumber .. "\")"
        fnCutscene(sString)
    end
    fnCutsceneBlocker()
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutsceneFace("Mei", 1, 0)
    fnCutsceneFace("Florentina", 1, -1)
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Mei:[VOICE|Mei] W-[P]what?[P] Was I dreaming?[P] What -[P] Polaris?[B][C]") ]])
    fnCutscene([[ Append("Polaris:[VOICE|Polaris] Quickly![P] Restrain Florentina![P] I need to cast a spell!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneJumpTo("Polaris", 20.25, 41.50, 5)
    fnCutsceneFace("Mei", 0, 1)
    fnCutsceneFace("Florentina", 0, -1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscenePlaySound("World|Thump")
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutscenePlaySound("World|Thump")
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Mei:[VOICE|Mei] Ouch![P] Knock it off![B][C]") ]])
    fnCutscene([[ Append("Polaris:[VOICE|Polaris] I got it![P] One second!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscenePlaySound("World|MagicA")
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Florentina", -1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Florentina", 0, 1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneSetFrame("Florentina", "Wounded")
    fnCutscenePlaySound("World|Thump")
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Florentina:[VOICE|Florentina] Oohh...[P] what happened...[P] Mei![P] The curse...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutsceneSetFrame("Florentina", "Crouch")
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutsceneSetFrame("Florentina", "Null")
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutsceneFace("Florentina", 0, -1)
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Polaris", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Aquillia", "Cast") ]])
    fnCutscene([[ Append("Polaris:[E|Upset] Splendid.[P] You two have no idea how lucky you are that I decided to build a cabin in a swamp backwater.[B][C]") ]])
    fnCutscene([[ Append("Aquillia:[E|Cast] We swapped notes.[P] Runestone, mannequins, that Victoria lady.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] I've got some questions of my own but I'll hold off for now.[P] I assume time is of the essence.[B][C]") ]])
    fnCutscene([[ Append("Polaris:[E|Upset] You don't know the half of it.[P] Sorry what I did to your head, Mei's runestone worked exactly as I predicted.[P] You, I had to give you a brain blast.[B][C]") ]])
    fnCutscene([[ Append("Polaris:[E|Upset] Luckily you're plastic, or resin, or...[P] whatever you are.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] Can you turn Florentina back to normal?[B][C]") ]])
    fnCutscene([[ Append("Polaris:[E|Neutral] Yeah, I disarmed the curse.[P] Just use any save point to change jobs like usual.[B][C]") ]])
    fnCutscene([[ Append("Polaris:[E|Upset] Okay, listen up.[P] I can explain more later but we're not done yet.[P] Victoria has no idea what she's gotten into.[B][C]") ]])
    fnCutscene([[ Append("Polaris:[E|Upset] I've pieced a few things together.[P] Victoria used to run with some bandits, and she was their mage.[P] She found a spell that turns people into mannequins and thought this was her chance to move up.[B][C]") ]])
    fnCutscene([[ Append("Polaris:[E|Upset] The idiot doesn't realize what that spell is.[P] It's a trap, a virus.[P] You use it, you think you're in charge, you turn a bunch of people into your little army.[P] Then you, and all your victims, vanish.[B][C]") ]])
    fnCutscene([[ Append("Polaris:[E|Upset] The mage guilds have been dealing with outbreaks for centuries, it's like a plague.[P] Pops up, then by the time anyone arrives to stop it, it's already over and everyone disappears.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] Where do they go?[P] What happens?[B][C]") ]])
    fnCutscene([[ Append("Polaris:[E|Thinking] I wish I knew, but nobody has ever heard from them again.[P] I can get more into it later, because, well, we don't know how long we have.[B][C]") ]])
    fnCutscene([[ Append("Polaris:[E|Upset] I need to prep more curative spells, which means you two need to go stop the boss.[P] I think the other mannequins are going to notice you've gone rogue, so don't waste time on subtlety.[B][C]") ]])
    fnCutscene([[ Append("Polaris:[E|Upset] Go beat some sense into her, that should make the mannequins much easier to deal with.[P] No time to talk, hop to it![B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] We're on it![B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] What about you three?[B][C]") ]])
    fnCutscene([[ Append("Aquillia:[E|Cast] Dot and I will help Polaris get the cures set up.[P] Don't worry about us, she's a legendary mage hero.[P] I think she can handle a stray mannequin.[B][C]") ]])
    fnCutscene([[ Append("Polaris:[E|Upset] Maybe not on two hours sleep...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
     -- |[Fold Up]|
    fnAutoFoldParty()
    fnCutsceneBlocker()
    fnCutscene([[ fnUnloadBitmapsFromList("Chapter 1 Mei Runestone") ]])
	
-- |[ =================================== Mannequin Bad End ==================================== ]|
--Most of this is in the Chapter1/Scenes/ folder. This trigger concerns the parts that are on this map.
elseif(sObjectName == "MannBadEnd") then

	-- |[Cut to Black]|
	AudioManager_PlayMusic("Null")
	fnCutsceneFadeOut()
	fnCutsceneBlocker()

	-- |[Call Cutscene]|
	--This is done immediately, the scene should append to the cutscene queue.
	LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/Mannequin_BadEnd/Scene_Begin.lua")
	
	-- |[Position]|
	fnCutsceneTeleport("Mei", 15.25, 28.50)
	fnCutsceneTeleport("Florentina", 16.25, 29.50)
	fnCutsceneFace("Mei", 0, 1)
	fnCutsceneFace("Florentina", -1, -1)
	fnCutsceneBlocker()
    fnCutsceneWait(245)
    fnCutsceneBlocker()
	
	-- |[Fade in]|
	fnCutsceneFadeIn(45)
	fnCutsceneBlocker()
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
	fnCutscene([[ Append("Florentina:[E|Neutral] Hey, Mei?[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] Huh?[P] What happened?[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] We got turned into mannequins and a swamp witch conked us on the head.[P] Now we're going to go give it to the lunatic who did this to us.[B][C]") ]])
	fnCutscene([[ Append("Florentina:[E|Neutral] And I thought my head was the one that got blasted.[B][C]") ]])
	fnCutscene([[ Append("Mei:[E|Neutral] S-[P]sorry.[P] I think I was just, yeah, must be that.[P] Okay, let's go stop Victoria!") ]])
	fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
	
	-- |[Finish]|
	fnAutoFoldParty()
	fnCutsceneBlocker()

end
