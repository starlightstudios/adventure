-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ =================================== "Go Away" -Bandits =================================== ]|
if(sObjectName == "Fort") then

    -- |[Repeat Check]|
    local iSawFort = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iSawFort", "N")
    if(iSawFort == 1.0) then return end
    
    -- |[Flag]|
    VM_SetVar("Root/Variables/Chapter1/Scenes/Mann|iSawFort", "N", 1.0)
    WD_SetProperty("Unlock Topic", "Bandits", 1)
    
    -- |[Spawning]|
    TA_Create("Boss")
        TA_SetProperty("Position", -10, -10)
        TA_SetProperty("Facing", gci_Face_West)
        TA_SetProperty("Clipping Flag", false)
        fnSetCharacterGraphics("Root/Images/Sprites/BanditCatB/", false)
    DL_PopActiveObject()
    
    -- |[Movement]|
    fnCutsceneMove("Mei", 15.25, 12.50)
    fnCutsceneFace("Mei", 1, 0)
    fnCutsceneMove("Florentina", 15.25, 13.50)
    fnCutsceneFace("Florentina", 1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ Append("Florentina:[E|Surprise] The little ones said this is the bandit's place, but they didn't mention the workmanship![P] Very well done.[P] Maybe I should hire them to fix up the trading post.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] Looks like one of the old dragon forts.[P] Solid foundation, usually good positioning.[P] These guys might be ex-military.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] Okay, so what now?[P] Bust their heads?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Happy] Right, I'm sure they went to all the effort to fix up this fort so they could just open the gate and we'd walk in.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneTeleport("Boss", 33.25, 12.50)
    fnCutsceneBlocker()
    fnCutsceneCamera("Position", gcfCamera_Cutscene_Speed, 21.25, 12.50)
    fnCutsceneMove("Boss", 21.25, 12.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "BanditCatB", "Neutral") ]])
    fnCutscene([[ Append("Boss:[E|Neutral] Hey, morons.[P] Get lost.[P] Gate's closed for a reason, we don't want visitors.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] What?[P] But we are mere tourists, here to see the sights.[B][C]") ]])
    fnCutscene([[ Append("Boss:[E|Neutral] Would you like to see our big attraction, 'Hail of Arrows'?[P] There's a showing coming up soon.[P] Just stand right there.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Happy] How rude![P] Fine, we're leaving.[B][C]") ]])
    fnCutscene([[ Append("Boss:[E|Neutral] Why are there so many freaks around here...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneMove("Boss", 33.25, 12.50)
    fnCutsceneCamera("Actor Name", gcfCamera_Cutscene_Speed, "Mei")
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ Append("Florentina:[E|Neutral] Little ones aren't giving me any good intel, the fort is solid.[P] No way in.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] Crud.[P] What do we do?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Happy] Sleep on it.[P] They're dug in, so unless you can put together an army, this is out of our hands.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Happy] I can ask the merchant companies if they'll throw in for some mercs.[P] You don't need to do it all yourself, Mei.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Sad] Oh well.[P] I just -[P] don't like letting people do bad things and get away with it.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] Not enough hours in the day to fix every problem, kid.[P] Come on.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Finish]|
    fnAutoFoldParty()
    fnCutsceneBlocker()

-- |[ =========================== "I swear I'm not crazy!" -Bandits ============================ ]|
elseif(sObjectName == "Freaky") then

    -- |[Repeat/Activation Check]|
    local iSawNightFort  = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iSawNightFort", "N")
    local iBeganSequence = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iBeganSequence", "N")
    if(iSawNightFort == 1.0 or iBeganSequence == 0.0) then return end
    
    -- |[Flag]|
    VM_SetVar("Root/Variables/Chapter1/Scenes/Mann|iSawNightFort", "N", 1.0)
    
    -- |[Spawn]|
    TA_Create("BanditGuy")
        TA_SetProperty("Position", -10, -10)
        TA_SetProperty("Facing", gci_Face_South)
        TA_SetProperty("Clipping Flag", false)
        fnSetCharacterGraphics("Root/Images/Sprites/Bandit_Male/", false)
    DL_PopActiveObject()
    
    -- |[Position]|
    fnCutsceneTeleport("BanditGuy", 31.25, 10.50)
    fnCutsceneMove("Mei", 15.25, 11.50)
    fnCutsceneFace("Mei", 1, 0)
    fnCutsceneMove("Florentina", 15.25, 12.50)
    fnCutsceneFace("Florentina", 1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Bandit:[VOICE|CultistM] Son of a - ") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneMove("BanditGuy", 21.25, 10.50)
    fnCutsceneCamera("Position", gcfCamera_Cutscene_Speed, 21.25, 11.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Bandit:[VOICE|CultistM] I heard that![P] Who's out there?![P] Just a shadow...[B][C]") ]])
    fnCutscene([[ Append("Bandit:[VOICE|CultistM] ...[B][C]") ]])
    fnCutscene([[ Append("Bandit:[VOICE|CultistM] Nothing.[P] It's too quiet.[P] They're right on the other side of the wall, watching...[B][C]") ]])
    fnCutscene([[ Append("Bandit:[VOICE|CultistM] I am burning this goddamn forest down tomorrow![P] You hear me, you freaks![P] Go away!") ]])
    fnCutsceneBlocker()
    fnCutsceneCamera("Actor Name", gcfCamera_Cutscene_Speed, "Mei")
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Finish Up]|
    fnAutoFoldParty()
    fnCutsceneBlocker()

-- |[ =================================== Capturing the Fort =================================== ]|
elseif(sObjectName == "Capture") then

    -- |[Repeat/Activation Check]|
    local iDefeatedLeader = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iDefeatedLeader", "N")
    if(iDefeatedLeader == 1.0) then return end
    
    -- |[Flag]|
    VM_SetVar("Root/Variables/Chapter1/Scenes/Mann|iSawNightFort", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter1/Scenes/Mann|iDefeatedLeader", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter1/Scenes/Mann|iOpenedFortGate", "N", 1.0)
    
    --Collisions.
    AL_SetProperty("Set Collision", 18, 12, 0, 0)
    AL_SetProperty("Set Collision", 18, 13, 0, 0)
    
    -- |[Spawn]|
    fnStandardNPCByPosition("Victoria")
    
    -- |[Movement]|
    fnCutsceneMergeParty()
    fnCutsceneBlocker()
    fnCutsceneCamera("Position", gcfCamera_Cutscene_Speed, 34.25, 12.50)
    fnCutsceneMove("Mei", 40.25, 12.50)
    fnCutsceneMove("Mei", 40.25, 14.50)
    fnCutsceneFace("Mei", -1, 0)
    fnCutsceneMove("Florentina", 40.25, 12.50)
    fnCutsceneMove("Florentina", 40.25, 13.50)
    fnCutsceneFace("Florentina", -1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("BanditM:[VOICE|CultistM] Hurry up with the soup.[B][C]") ]])
    fnCutscene([[ Append("BanditF:[VOICE|CultistF] At least stoke the fire you lazy sod.[P] I can hardly see.[B][C]") ]])
    fnCutscene([[ Append("BanditM:[VOICE|CultistM] Boss said to keep the fires down tonight.[P] Doesn't want us getting any attention.[B][C]") ]])
    fnCutscene([[ Append("BanditF:[VOICE|CultistF] You hear what Zert was saying about things in the forest at night?[B][C]") ]])
    fnCutscene([[ Append("BanditM:[VOICE|CultistM] Maybe the boss believed him.[P] I haven't seen a thing.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneMove("Mei", 40.25, 15.50, 2.50)
    fnCutsceneMove("Mei", 37.25, 15.50, 2.50)
    fnCutsceneMove("Mei", 37.25, 12.50, 2.50)
    fnCutsceneFace("Mei", -1, 0)
    fnCutsceneMove("Florentina", 40.25, 15.50, 2.50)
    fnCutsceneMove("Florentina", 34.25, 15.50, 2.50)
    fnCutsceneMove("Florentina", 34.25, 14.50, 2.50)
    fnCutsceneFace("Florentina", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(55)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("BanditM:[VOICE|CultistM] Did you hear something just now?[B][C]") ]])
    fnCutscene([[ Append("BanditF:[VOICE|CultistF] The rustling of leaves?[P] Yeah, the wind does that.[B][C]") ]])
    fnCutscene([[ Append("BanditM:[VOICE|CultistM] I just -[P] ah!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Switch]|
    fnCutscenePlaySound("Rubber|ChangeC")
    fnCutsceneMoveFace("Mei", 37.25, 14.50, -1, -1)
    fnCutsceneMoveFace("Florentina", 34.25, 15.50, 0, -1)
    for i = 1, 3, 1 do
        fnCutsceneTeleport("BanditM", -1.00, -1.00)
        fnCutsceneTeleport("BanditF", -1.00, -1.00)
        fnCutsceneTeleport("BanditMannM", 34.25, 13.50)
        fnCutsceneTeleport("BanditMannF", 36.25, 12.50)
        fnCutsceneBlocker()
        fnCutsceneWait(15)
        fnCutsceneBlocker()
        fnCutsceneTeleport("BanditM", 34.25, 13.50)
        fnCutsceneTeleport("BanditF", 36.25, 12.50)
        fnCutsceneTeleport("BanditMannM", -1.00, -1.00)
        fnCutsceneTeleport("BanditMannF", -1.00, -1.00)
        fnCutsceneBlocker()
        fnCutsceneWait(15)
        fnCutsceneBlocker()
    end
    fnCutsceneTeleport("BanditM", -1.00, -1.00)
    fnCutsceneTeleport("BanditF", -1.00, -1.00)
    fnCutsceneTeleport("BanditMannM", 34.25, 13.50)
    fnCutsceneTeleport("BanditMannF", 36.25, 12.50)
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Mannequin:[VOICE|CultistM] ...[B][C]") ]])
    fnCutscene([[ Append("Mannequin:[VOICE|CultistF] ...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneCamera("Actor Name", gcfCamera_Cutscene_Speed, "Mei")
    fnCutsceneMove("Mei", 37.25, 11.50)
    fnCutsceneMove("Mei", 20.25, 11.50)
    fnCutsceneMove("Florentina", 31.25, 14.50)
    fnCutsceneMove("Florentina", 31.25, 11.50)
    fnCutsceneMove("Florentina", 21.25, 11.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscenePlaySound("World|FlipSwitch")
    fnCutsceneBlocker()
    fnCutsceneWait(75)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Boss:[VOICE|BanditCatB] What the -[P] who gave permission to open that gate!?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneFace("Mei", 1, 0)
    fnCutsceneFace("Florentina", 1, 0)
    fnCutsceneMove("Boss", 27.25, 15.50)
    fnCutsceneMove("Boss", 27.25, 11.50)
    fnCutsceneFace("Boss", -1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Boss:[VOICE|BanditCatB] Who the hell are you?[P] How did you get in here?[B][C]") ]])
    fnCutscene([[ Append("Mannequin:[VOICE|Mei] ...[B][C]") ]])
    fnCutscene([[ Append("Boss:[VOICE|BanditCatB] Not going to talk?[P] Prepare to die, freaks![B][C]") ]])
    fnCutscene([[ Append("Voice:[VOICE|Victoria] Calm down, Boxy.[P] You'll give yourself a conniption.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneMove("Victoria", 22.25, 12.50)
    fnCutsceneMove("Victoria", 22.25, 11.50)
    fnCutsceneFace("Victoria", 1, 0)
    fnCutsceneBlocker()
    fnCutsceneMove("Mei", 21.25, 11.00)
    fnCutsceneFace("Mei", 1, 0)
    fnCutsceneMove("Florentina", 21.25, 12.00)
    fnCutsceneFace("Florentina", 1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Victoria", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "BanditCatB", "Neutral") ]])
    fnCutscene([[ Append("Boss:[E|Neutral] Oh, that's cute.[P] Been a while, Victoria.[P] I remember when you ran out of here with your tail between your legs.[B][C]") ]])
    fnCutscene([[ Append("Boss:[E|Neutral] Something about being upset that we found out you kept more of your share than allowed?[B][C]") ]])
    fnCutscene([[ Append("Victoria:[E|Evil] You don't have the brains or skill to lead.[P] I was keeping the percent I was owed.[B][C]") ]])
    fnCutscene([[ Append("Boss:[E|Neutral] So you made some toy-store slaves?[P] Wake up, everyone![P] We've got a traitor's face to crunch![B][C]") ]])
    fnCutscene([[ Append("Victoria:[E|Evil] Slaves?[P] No, no, these two volunteered.[P] They put on my special accessories.[B][C]") ]])
    fnCutscene([[ Append("Boss:[E|Neutral] I told you to leave that magic alone, you dip![B][C]") ]])
    fnCutscene([[ Append("Victoria:[E|Concern] And what do you know of magic?[B][C]") ]])
    fnCutscene([[ Append("Boss:[E|Neutral] Enough not to fool with it![B][C]") ]])
    fnCutscene([[ Append("Boss:[E|Neutral] Everyone![P] I said wake up![B][C]") ]])
    fnCutscene([[ Append("Victoria:[E|Evil] Oh, you think its twenty-to-three?[P] No, my dear, the odds go the other way.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutscenePlaySound("World|Heartbeat")
    fnCutsceneLayerDisabled("MannLo", false)
    fnCutsceneLayerDisabled("MannHi", false)
    fnCutsceneTeleport("BanditMannM", 27.25, 14.50)
    fnCutsceneFace("BanditMannM", 0, -1)
    fnCutsceneTeleport("BanditMannF", 25.25, 9.50)
    fnCutsceneFace("BanditMannF", 0, 1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Boss", 0, 1)
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutsceneFace("Boss", 0, -1)
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutsceneFace("Boss", 1, 0)
    fnCutsceneWait(185)
    fnCutsceneBlocker()
    fnCutsceneFace("Boss", -1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Set Collision", 27,  9, 0, 1) ]])
    fnCutscene([[ AL_SetProperty("Set Collision", 29,  8, 0, 1) ]])
    fnCutscene([[ AL_SetProperty("Set Collision", 29, 10, 0, 1) ]])
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Victoria", "Evil") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "BanditCatB", "Neutral") ]])
    fnCutscene([[ Append("Boss:[E|Neutral] ...[P] What are you going to do to me?[B][C]") ]])
    fnCutscene([[ Append("Victoria:[E|Evil] I just want to talk.[P] That's all.[P] We won't be interrupted.[P] After all, none of these things are talkative.[B][C]") ]])
    fnCutscene([[ Append("Mannequin:[FOCUS|Mei][E|Neutral] ...[B][C]") ]])
    fnCutscene([[ Append("Boss:[E|Neutral] You sick freak...[B][C]") ]])
    fnCutscene([[ Append("Victoria:[E|Disgust] If I were you, I'd watch what I say.[P] After all, you're one snap of my fingers away from joining this menagerie.[B][C]") ]])
    fnCutscene([[ Append("Mannequin:[FOCUS|Florentina][E|Neutral] ...[B][C]") ]])
    fnCutscene([[ Append("Boss:[E|Neutral] I have friends.[P] Friends who will stop you![B][C]") ]])
    fnCutscene([[ Append("Victoria:[E|Disgust] No you don't, you've burned every bridge you ever crossed.[P] Nobody is coming to save you.[P] So, let's start a fire, make some tea, and have a nice chat, shall we?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutscenePlaySound("World|DogBark")
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Victoria", -1, 0)
    fnCutsceneFace("Mei", -1, 0)
    fnCutsceneFace("Florentina", -1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Victoria:[VOICE|Victoria] A dog?[P] How annoying.[P] I despise mangy mutts.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscenePlaySound("World|DogBark")
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscenePlaySound("World|DogBark")
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscenePlaySound("World|DogBark")
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Victoria:[VOICE|Victoria] Shut up you stupid dog![P] You, go shut that dog up!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneMove("Mei", 21.25, 12.50)
    fnCutsceneMove("Mei", 20.25, 12.50)
    fnCutsceneMove("Florentina", 21.25, 12.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Victoria:[VOICE|Victoria] No, you.[P] Uhh, Florentina.[P] You stay.[P] I want to...[B][C]") ]])
    fnCutscene([[ Append("Victoria:[VOICE|Victoria] Nevermind.[P] You two clearly work well together.[P] Shut that dog up and get back here at once.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Finish Up]|
    fnAutoFoldParty()
    fnCutsceneBlocker()
    
-- |[ ================================ Boss Battle With Victoria =============================== ]|
elseif(sObjectName == "BossBattle") then
    
    -- |[Activation Check]|
    local iPolarisRunIn      = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iPolarisRunIn", "N")
    local iCompletedSequence = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iCompletedSequence", "N")
    if(iPolarisRunIn == 0.0 or iCompletedSequence == 1.0) then return end
    
    -- |[Variables]|
    local sFlorentinaJob = VM_GetVar("Root/Variables/Global/Florentina/sCurrentJob", "S")
    
    -- |[Movement]|
    fnCutsceneMergeParty()
    fnCutsceneBlocker()
    fnCutsceneMove("Mei", 24.25, 11.50)
    fnCutsceneMove("Florentina", 24.25, 11.50)
    fnCutsceneBlocker()
    fnCutsceneMove("Mei", 25.25, 11.50)
    fnCutsceneBlocker()
    fnCutsceneMove("Mei", 31.25, 11.50)
    fnCutsceneMove("Florentina", 30.25, 11.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Boss", 0, 1)
    fnCutsceneFace("Mei", 1, -1)
    fnCutsceneFace("Florentina", 1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "BanditCatB", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Victoria", "Neutral") ]])
    fnCutscene([[ Append("Victoria:[E|Apology] You have returned, but there is a slight flaw.[P] A distinct lack of loyalty.[B][C]") ]])
    if(sFlorentinaJob == "Lurker") then
        fnCutscene([[ Append("Victoria:[E|Disbelief] Why did you betray me, mannequin?[P] Was it something I said?[P] We would have been happy together.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] I'm an emotionless plastic monster and you're the creepy one right now, lady.[B][C]") ]])
        fnCutscene([[ Append("Victoria:[E|Disbelief] Look down at yourself.[P] Pert, taut, strong.[P] You're the ideal woman.[B][C]") ]])
    else
        fnCutscene([[ Append("Victoria:[E|Disbelief] Was the flawless body I gifted you not to your liking, Florentina?[P] Don't you think you were much more attractive that way?[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Confused] Get bent.[B][C]") ]])
        fnCutscene([[ Append("Victoria:[E|Disbelief] The sway of your solid chest, no jiggle.[P] The smooth, firm skin.[P] The rugged wooden arms.[B][C]") ]])
    end
    fnCutscene([[ Append("Mei:[E|Sad] Is this a sex thing or a power thing?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] Seems like both.[B][C]") ]])
    fnCutscene([[ Append("Boss:[E|Neutral] She has been talking about you since you left.[P] Like, the whole time, just this.[B][C]") ]])
    fnCutscene([[ Append("Victoria:[E|Disgust] I am simply admiring a beautiful woman's form.[P] Isn't that the purpose of fashion?[B][C]") ]])
    fnCutscene([[ Append("Boss:[E|Neutral] She's still doing it.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Offended] That's enough.[P] Time to give up.[P] You don't know what kind of power you're wielding, and if you don't give it up, it will consume you.[B][C]") ]])
    fnCutscene([[ Append("Victoria:[E|Evil] And if I say no?[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] We're supposed to beat you up.[B][C]") ]])
    fnCutscene([[ Append("Victoria:[E|Evil] Very well, I will show you that my mastery extends to the arcane and not just looking good.[P] En garde!") ]])
    fnCutsceneBlocker()
    
    -- |[Battle]|
    fnCutscene([[ AdvCombat_SetProperty("World Pulse", true) ]])
    fnCutscene([[ AdvCombat_SetProperty("Reinitialize") ]])
    fnCutscene([[ AdvCombat_SetProperty("Activate") ]])
    fnCutscene([[ AdvCombat_SetProperty("Unretreatable", true) ]])
    fnCutscene([[ AdvCombat_SetProperty("Victory Script", gsRoot .. "Maps/Chapter 1/Bandit Forest/BanditForestD/Combat_Victory.lua") ]])
    fnCutscene([[ AdvCombat_SetProperty("Defeat Script",  gsRoot .. "Maps/Chapter 1/Bandit Forest/BanditForestD/Combat_Defeat.lua") ]])
    fnCutscene([[ LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 1/000 Scripted Enemies/Boss Victoria.lua", 0) ]])
end
