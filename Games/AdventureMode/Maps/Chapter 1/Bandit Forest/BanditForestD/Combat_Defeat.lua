-- |[ ===================================== Combat Defeat ====================================== ]|
--Party loses in combat.

-- |[Setup]|
--Disable music.
AudioManager_PlayMusic("Null")

--Fade to black.
fnCutsceneFadeOut(45)

-- |[Dialogue]|
--Setup.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "BanditCatB", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Victoria", "Neutral") ]])
fnCutscene([[ Append("Mei:[E|Neutral] D-[P]damn it...[B][C]") ]])
fnCutscene([[ Append("Victoria:[E|Evil] Now let's fix that pesky loyalty problem you seem to have...") ]])
fnCutsceneBlocker()
fnCutsceneWait(45)
fnCutsceneBlocker()

--SFX, change to Mannequin forms.
fnCutsceneCall(gsRoot .. "FormHandlers/Mei/Form_Mannequin.lua")
fnCutsceneCall(gsRoot .. "FormHandlers/Florentina/Job_Lurker.lua")
fnCutscenePlaySound("World|Heartbeat")
fnCutsceneWait(25)
fnCutsceneBlocker()

-- |[Dialogue]|
--Setup.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "BanditCatB", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Victoria", "Disbelief") ]])
fnCutscene([[ Append("Boss:[E|Neutral] So much for that rescue attempt.[P] Good effort though.[B][C]") ]])
fnCutscene([[ Append("Victoria:[E|Disgust] I bet it was that swamp witch.[P] We'd better leg it before the mage's guild shows up on our doorstep.[B][C]") ]])
fnCutscene([[ Append("Boss:[E|Neutral] Are you including me in that we?[B][C]") ]])
fnCutscene([[ Append("Victoria:[E|Disgust] I was indeed.[P] So now...[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] ...[B][C]") ]])
fnCutscene([[ Append("Victoria:[E|Neutral] This spell is far too finicky, I will need to work on it.[P] Mannequins should be loyal.[P] Now...") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--SFX.
fnCutscenePlaySound("World|MagicA")
fnCutsceneWait(45)
fnCutsceneBlocker()

-- |[Dialogue]|
--Setup.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "BanditCatB", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Victoria", "Disbelief") ]])
fnCutscene([[ Append("Victoria:[E|Disgust] Good.[P] Grab anything of value and prepare to move out, we're going south down the coast.[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] ...[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Neutral] ...[B][C]") ]])
fnCutscene([[ Append("Victoria:[E|Concern] ...[P] I said, grab anything of value.[P] Mannequins![P] Follow my orders![P] Don't make me -[B][C]") ]])
fnCutscene([[ Append("Victoria:[E|Disbelief] -[P] Unhand me![P] You -[P] Stop that![P] I am your master![B][C]") ]])
fnCutscene([[ Append("Boss:[E|Neutral] Twin tails of fire, Victoria![P] I told you you'd lose control![B][C]") ]])
fnCutscene([[ Append("Victoria:[E|Disbelief] Get off me![P] Get off me damn you![B][C]") ]])
fnCutscene([[ Append("Victoria:[E|Mannequin] Ah...[P] I...[B][C]") ]])
fnCutscene([[ Append("Boss:[E|Neutral] Criminy![B][C]") ]])
fnCutscene([[ Append("Victoria:[E|Mannequin] (Stupid mannequins, I am your master![P] I -[P] why can't I speak?[P] They've -[P] turned me into a mannequin!)[B][C]") ]])
fnCutscene([[ Append("Victoria:[E|Mannequin] (Fortunately I know the reversal spell, I will just...)[B][C]") ]])
fnCutscene([[ Append("Victoria:[E|Mannequin] ...[B][C]") ]])
fnCutscene([[ Append("Mei:[E|Neutral] ...[B][C]") ]])
fnCutscene([[ Append("Florentina:[E|Neutral] ...") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

--SFX.
fnCutscenePlaySound("World|MagicB")
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutscenePlaySound("World|MagicB")
fnCutsceneWait(15)
fnCutsceneBlocker()
fnCutscenePlaySound("World|MagicB")
fnCutsceneWait(15)
fnCutsceneBlocker()
fnCutscenePlaySound("World|MagicB")
fnCutsceneWait(15)
fnCutsceneBlocker()

-- |[Dialogue]|
--Setup.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "BanditCatB", "Neutral") ]])
fnCutscene([[ Append("Boss:[E|Neutral] ...?[P] Where did they go?[B][C]") ]])
fnCutscene([[ Append("Boss:[E|Neutral] Does this mean I won?[P] Hello?[P] Victoria?[P] Creepy resin people?[P] Anyone?[B][C]") ]])
fnCutscene([[ Append("Boss:[E|Neutral] ...[P] Taff it I am so out of here.[P] Hope you get what's coming to you, Vicky![P] Save a nice spot in hell for me!") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

-- |[Transition]|
--Scene resumes on the top floor of the beehive.
fnCutscene([[ AL_BeginTransitionTo("BanditForestB", "FORCEPOS:1.0x1.0x0") ]])
fnCutsceneBlocker()
