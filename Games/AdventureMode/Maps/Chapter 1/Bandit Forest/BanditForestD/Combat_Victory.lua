-- |[ ===================================== Combat Victory ===================================== ]|
--Party wins in combat.

-- |[Setup]|
--Flag.
VM_SetVar("Root/Variables/Chapter1/Scenes/Mann|iCompletedSequence", "N", 1.0)

--Disable music.
AudioManager_PlayMusic("Null")

--Spawn Polaris.
fnStandardNPCByPosition("Polaris")

-- |[Fireworks]|
local sCatalystPath = "Root/Images/Sprites/Catalyst/Heart"
local iFireworks = 12
local ciFireworkTPF = 6
for q = 1, (iFireworks*2)+1, 1 do
	TA_Create("CatalystFirework" .. q)
		TA_SetProperty("Position", -100, -100)
		TA_SetProperty("Rendering Depth", 0.000000)
		TA_SetProperty("Walk Ticks Per Frame", ciFireworkTPF)
		TA_SetProperty("Auto Animates Fast", true)
		for i = 1, 8, 1 do
			for p = 1, 4, 1 do
				TA_SetProperty("Move Frame", i-1, p-1, sCatalystPath .. "Firework" .. (p-1))
			end
		end
	DL_PopActiveObject()
end

-- |[Dialogue]|
--Setup.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "BanditCatB", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Victoria", "Neutral") ]])
fnCutscene([[ Append("Mei:[E|Neutral] Not so tough now, are you?[B][C]") ]])
fnCutscene([[ Append("Victoria:[E|Evil] Have you forgotten?[P] Sure, you managed to beat me but my army surrounds you.[B][C]") ]])
fnCutscene([[ Append("Victoria:[E|Evil] I merely wanted to test your resolve.[P] I can see you are excellent additions to my forces.[P] They will subvert you again and you will again be mine.[B][C]") ]])
fnCutscene([[ Append("Victoria:[E|Evil] Mannequins![P] Attack!") ]])
fnCutsceneBlocker()
fnCutsceneWait(125)
fnCutsceneBlocker()

-- |[Dialogue]|
--Setup.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "BanditCatB", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Victoria", "Disbelief") ]])
fnCutscene([[ Append("Boss:[E|Neutral] It feels pretty bad, doesn't it?[P] I would empathize with your situation, except the whole 'you were probably going to enslave me' thing.[B][C]") ]])
fnCutscene([[ Append("Victoria:[E|Disbelief] What?[P] Why are they not following my orders?") ]])
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

-- |[Movement]|
fnCutscenePlaySound("World|MagicB")
fnCutsceneWait(5)
fnCutsceneBlocker()
fnCutsceneTeleport("Polaris", 33.25, 8.50)
local fChestX = 33.25 * gciSizePerTile
local fChestY =  8.25 * gciSizePerTile

--Spawn fireworks. Despawn them after a few frames.
for i = 1, iFireworks, 1 do
	
	--Play a sound effect.
	local sString = "AudioManager_PlaySound(\"World|Firework\" .. LM_GetRandomNumber(0, 2))"
	fnCutscene(sString)

	--Generate a position.
	local fRange = math.floor(LM_GetRandomNumber(5, 12)) / 10.0
	local fDegrees = ((900 / iFireworks) * i)  +  LM_GetRandomNumber(-25, 25)
	local fXPos = (fChestX / gciSizePerTile) + 0.25 + (math.cos(fDegrees * 3.1415926 / 180.0) * fRange)
	local fYPos = (fChestY / gciSizePerTile) - 1.00 + (math.sin(fDegrees * 3.1415926 / 180.0) * fRange)

	--Create an event to spawn the firework.
	fnCutsceneTeleport("CatalystFirework" .. (i*2)+0, fXPos, fYPos)
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "CatalystFirework" .. (i*2)+0)
		ActorEvent_SetProperty("Reset Move Timer")
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "CatalystFirework" .. (i*2)+0)
		ActorEvent_SetProperty("Auto Despawn")
	DL_PopActiveObject()
	
	--Wait for the next firework.
	fnCutsceneWait(LM_GetRandomNumber(3, 4))
	fnCutsceneBlocker()
	
	--Create another firework on a shorter radius.
	fRange = LM_GetRandomNumber(2, 10) / 10.0
	fDegrees = ((900 / iFireworks) * i)  +  LM_GetRandomNumber(-25, 25)
	fXPos = (fChestX / gciSizePerTile) + 0.25 + (math.cos(fDegrees * 3.1415926 / 180.0) * -fRange)
	fYPos = (fChestY / gciSizePerTile) - 1.00 + (math.sin(fDegrees * 3.1415926 / 180.0) * -fRange)

	--Create an event to spawn the firework.
	fnCutsceneTeleport("CatalystFirework" .. (i*2)+1, fXPos, fYPos)
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "CatalystFirework" .. (i*2)+1)
		ActorEvent_SetProperty("Reset Move Timer")
	DL_PopActiveObject()
	Cutscene_CreateEvent("ActorEvent", "Actor")
		ActorEvent_SetProperty("Subject Name", "CatalystFirework" .. (i*2)+1)
		ActorEvent_SetProperty("Auto Despawn")
	DL_PopActiveObject()
	
	--Wait for the next firework.
	fnCutsceneWait(LM_GetRandomNumber(3, 4))
	fnCutsceneBlocker()
end
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()

-- |[Dialogue]|
--Setup.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Polaris", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Victoria", "Disbelief") ]])
fnCutscene([[ Append("Polaris:[E|Upset] *cough*[P] Fireworks teleport.[P] Flashy, not practical.[P] Maybe I should make one where I just fade in from the shadows.[B][C]") ]])
fnCutscene([[ Append("Polaris:[E|Upset] Hi, my name is Polaris.[P] I'm the Sealing Stone Witch.[P] Nice to meet you.[B][C]") ]])
fnCutscene([[ Append("Victoria:[E|Disbelief] I - [P][C]") ]])
fnCutscene([[ Append("Polaris:[E|Upset] Be quiet.[P] I got two hours of sleep and had to come sort out this nonsense so I passed the edge of my patience long ago.[B][C]") ]])
fnCutscene([[ Append("Polaris:[E|Upset] I'm going to now teleport you to a prison cell and figure out how to undo all the damage you've caused.[B][C]") ]])
fnCutscene([[ Append("Victoria:[E|Disbelief] You - [P][C]") ]])
fnCutscene([[ Append("Polaris:[E|Upset] You want to test me, amateur?[P] I've killed gods.[P] You didn't even notice me jamming your control spell.[P] Hack.[P] So let's get going.[B][C]") ]])
fnCutscene([[ Append("Polaris:[E|Upset] As for what happens now, that's up to the mage's guild.[P] I hope you can afford a good magic lawyer.") ]])
fnCutsceneBlocker()
fnCutsceneWait(45)
fnCutsceneFadeOut(45)
fnCutsceneBlocker()
fnCutsceneWait(125)
fnCutsceneBlocker()
for i = 1, iFireworks, 1 do
	local sString = "AudioManager_PlaySound(\"World|Firework\" .. LM_GetRandomNumber(0, 2))"
	fnCutscene(sString)
	fnCutsceneBlocker()
	fnCutsceneWait(LM_GetRandomNumber(3, 4))
	fnCutsceneBlocker()
end
fnCutsceneWait(45)
fnCutsceneBlocker()
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ Append("Polaris:[VOICE|Polaris] *cough*...") ]])
fnCutsceneBlocker()

-- |[Transition]|
--Scene resumes on the top floor of the beehive.
fnCutscene([[ AL_BeginTransitionTo("RiverWildsA", "FORCEPOS:138.0x71.0x0") ]])
fnCutsceneBlocker()
