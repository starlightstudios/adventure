-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then

    --Resolve the name of the calling entity.
    local sActorName = TA_GetProperty("Name")
    
    --Common.
	TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[Victoria]|
    if(sActorName == "Victoria") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Victoria", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Mei", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Florentina", "Neutral") ]])
        fnCutscene([[ Append("Victoria:[E|Neutral] Deal with that dog, mannequins.[P] I hate being interrupted.[B][C]") ]])
        fnCutscene([[ Append("Mannequin:[FOCUS|Mei][E|Neutral] ...") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    
    -- |[Boss]|
    elseif(sActorName == "Boss") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "BanditCatB", "Neutral") ]])
        fnCutscene([[ Append("Boss:[E|Neutral] Keep away from me, freaks.[B][C]") ]])
        fnCutscene([[ Append("Mannequin:[FOCUS|Florentina][E|Neutral] ...[B][C]") ]])
        fnCutscene([[ Append("Boss:[E|Neutral] Say something![P] Say something damn you![B][C]") ]])
        fnCutscene([[ Append("Mannequin:[FOCUS|Mei][E|Neutral] ...") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    end
end
