-- |[ ======================================= Constructor ====================================== ]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = fnResolveDirectory() --Map's name is the name of the folder it is in.
local sLevelMusic = "ForestTheme"
local sMapResolveName = "Map Resolve Name"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[ ======================================== Standard ======================================== ]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

    -- |[Construction]|
	--Music.
	AL_SetProperty("Music", sLevelMusic)
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
    
    -- |[Map Handling]|
    --Resolve the map's variable.
    fnStandardCh1Map(sLevelName)

-- |[ ======================================== Post-Exec ======================================= ]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	-- |[NPC Spawning]|
	--If an entity needs to spawn after a post-exec cutscene, put them here.

    -- |[Overlays]|
    --Night time.
    local iBeganSequence     = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iBeganSequence", "N")
    local iCompletedSequence = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iCompletedSequence", "N")
    if(iBeganSequence == 1.0 and iCompletedSequence == 0.0) then
        
        --Music.
        AL_SetProperty("Music", "Null")
        
        --Overlay.
        AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, gcf_Night_R, gcf_Night_G, gcf_Night_B, gcf_Night_A, gcf_Night_R, gcf_Night_G, gcf_Night_B, gcf_Night_A, true)
        
        --Also spawn these NPCs.
        fnStandardNPCByPosition("Boss")
        fnStandardNPCByPosition("BanditM")
        fnStandardNPCByPosition("BanditF")
        fnStandardNPCByPosition("BanditMannM")
        fnStandardNPCByPosition("BanditMannF")
    end
    
    --Disable these unless in a specific part of the scene.
    local iDefeatedLeader = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iDefeatedLeader", "N")
    if(iDefeatedLeader == 0.0 or iCompletedSequence == 1.0) then
        AL_SetProperty("Set Layer Disabled", "MannLo", true)
        AL_SetProperty("Set Layer Disabled", "MannHi", true)
    
    --Leader defeated, spawn NPCs.
    else
        fnStandardNPCByPosition("Victoria")
        fnCutsceneTeleport("Victoria", 33.25, 9.50)
        fnCutsceneFace("Victoria", -1, 0)
        fnCutsceneTeleport("Boss", 31.25, 9.50)
        fnCutsceneFace("Boss", 1, 0)
        fnCutsceneTeleport("BanditM", -1.25, -1.50)
        fnCutsceneTeleport("BanditF", -1.25, -1.50)
        fnCutsceneTeleport("BanditMannM", 37.25, 9.50)
        fnCutsceneFace("BanditMannM", -1, 0)
        fnCutsceneTeleport("BanditMannF", 34.25, 6.50)
        fnCutsceneFace("BanditMannF", 0, 1)
        AL_SetProperty("Set Collision", 27,  9, 0, 1)
        AL_SetProperty("Set Collision", 29,  8, 0, 1)
        AL_SetProperty("Set Collision", 29, 10, 0, 1)
    end
    
    -- |[Despawn Enemies]|
    --During the mannequin sequence, despawn these enemies. They come back after the quest is done.
    if(iBeganSequence == 1.0 and iCompletedSequence == 0.0) then
        local saDespawnList = {"EnemyAA", "EnemyAB", "EnemyAC"}
        for i = 1, #saDespawnList, 1 do
            EM_PushEntity(saDespawnList[i])
                RE_SetDestruct(true)
            DL_PopActiveObject()
        end
    end
    
    -- |[Skillbooks]|
    --Place skillbook overlay code here.
    
    -- |[Collisions]|
    local iOpenedFortGate = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iOpenedFortGate", "N")
    if(iOpenedFortGate == 0.0) then
        AL_SetProperty("Set Collision", 18, 12, 0, 1)
        AL_SetProperty("Set Collision", 18, 13, 0, 1)
    end
end
