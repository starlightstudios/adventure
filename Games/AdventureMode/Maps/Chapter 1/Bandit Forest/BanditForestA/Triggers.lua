-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ================================= Mannequin Start Scene ================================== ]|
if(sObjectName == "Scene") then
    
    -- |[ ========= Setup ========== ]|
    --Note: "Root/Variables/Chapter1/Scenes/Mann|iBeganSequence" should be 1.0 before this trigger fires so the
    -- constructor spawns the needed NPCs.
    
    --Music off.
    AL_SetProperty("Music", "Null")
    
    --Set the last-save to the bench in BanditForestB.
    AL_SetProperty("Last Save Point", "BanditForestB")
    
    --Transform the characters if they weren't already in this form.
    LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Mannequin.lua")
    LM_ExecuteScript(gsRoot .. "FormHandlers/Florentina/Job_Lurker.lua")
    
    --Cut to black.
    fnCutsceneFadeOut()
    fnCutsceneBlocker()
    
    --Position.
    fnCutsceneTeleport("Mei", 39.25, 8.50)
    fnCutsceneTeleport("Florentina", 40.25, 8.50)
    fnCutsceneCamera("Position", 1000, 23.25, 6.50)
    
    --Names.
    AdvCombat_SetProperty("Push Party Member", "Mei")
        AdvCombatEntity_SetProperty("Display Name", "Mannequin")
    DL_PopActiveObject()
    AdvCombat_SetProperty("Push Party Member", "Florentina")
        AdvCombatEntity_SetProperty("Display Name", "Mannequin")
    DL_PopActiveObject()
    
    -- |[ ========= Scene ========== ]|
    -- |[Fade In]|
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, gcf_Night_R, gcf_Night_G, gcf_Night_B, gcf_Night_A, gcf_Night_R, gcf_Night_G, gcf_Night_B, gcf_Night_A, true) ]])
    fnCutsceneWait(85)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneMove("Mei", 24.25, 8.50)
    fnCutsceneMove("Mei", 24.25, 6.50)
    fnCutsceneMove("Mei", 23.25, 6.50)
    fnCutsceneMove("Florentina", 24.25, 8.50)
    fnCutsceneMove("Florentina", 24.25, 7.50)
    fnCutsceneMove("Florentina", 23.25, 7.50)
    fnCutsceneBlocker()
    fnCutsceneFace("Victoria", 1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Victoria", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Mei", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Florentina", "Neutral") ]])
    fnCutscene([[ Append("Victoria:[E|Neutral] Ah, there we are.[P] Are you warriors?[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] ...[B][C]") ]])
    fnCutscene([[ Append("Victoria:[E|Evil] Splendid.[P] Don't worry, ladies, I am a caring master.[P] You're in good hands.[B][C]") ]])
    fnCutscene([[ Append("Victoria:[E|Evil] With this, I think tonight is the night.[P] I have a feeling about you, mannequin.[P] Your name was Mei, right?[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] ...[B][C]") ]])
    fnCutscene([[ Append("Victoria:[E|Concern] Not anymore, I suppose.[B][C]") ]])
    fnCutscene([[ Append("Mannequin:[FOCUS|Mei][E|Neutral] ...[B][C]") ]])
    fnCutscene([[ Append("Victoria:[E|Neutral] There is a fortress just south of here, filled with my former comrades.[P] Get in, subdue them, and open the gate so the rest of my minions might flood in.[B][C]") ]])
    fnCutscene([[ Append("Victoria:[E|Neutral] Do what you like to the thugs, but the leader is a werecat.[P] Do not turn her.[P] Leave her for me.[P] Understood?[B][C]") ]])
    fnCutscene([[ Append("Mannequin:[FOCUS|Florentina][E|Neutral] ...[B][C]") ]])
    fnCutscene([[ Append("Victoria:[E|Evil] Ooh, I do remember you, the merchant.[P] When this is over, perhaps we can get to know each other better.[P] Or rather, you can worship me as you ought to.[B][C]") ]])
    fnCutscene([[ Append("Victoria:[E|Evil] Try not to get scuffed, I like you just the way you are.[P] Now, get to work, former-ladies!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[ ======= Finish Up ======== ]|
    fnAutoFoldParty()
    fnCutsceneBlocker()
    
-- |[ ============================== Cannot Exit During Sequence =============================== ]|
elseif(sObjectName == "Leaving") then
    
    --Variables.
    local iBeganSequence     = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iBeganSequence", "N")
    local iCompletedSequence = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iCompletedSequence", "N")
    local iPolarisRunIn      = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iPolarisRunIn", "N")
    if(iBeganSequence == 0.0 or iCompletedSequence == 1.0) then return end

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    if(iPolarisRunIn == 0.0) then
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Remain.)") ]])
    else
        fnCutscene([[ Append("Thought:[VOICE|Leader] (We can't just leave without stopping Victoria!)") ]])
    end
    fnCutsceneBlocker()
    
    --Move.
    fnCutsceneMoveAmount("Mei", 0.00, 2.00)
    fnCutsceneBlocker()
    fnAutoFoldParty()
    fnCutsceneBlocker()

-- |[ ====================================== A Dog Barks ======================================= ]|
elseif(sObjectName == "Dog") then

    -- |[Repeat Check]|
    local iHeardDog       = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iHeardDog", "N")
    local iBeganSequence  = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iBeganSequence", "N")
    local iDefeatedLeader = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iDefeatedLeader", "N")
    if(iHeardDog == 1.0 or iBeganSequence == 0.0 or iDefeatedLeader == 0.0) then return end
    
    -- |[Flag]|
    VM_SetVar("Root/Variables/Chapter1/Scenes/Mann|iHeardDog", "N", 1.0)

    -- |[Bark!]|
    fnCutscenePlaySound("World|DogBark")
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (West.)") ]])
    fnCutsceneBlocker()
    fnAutoFoldParty()
    fnCutsceneBlocker()
end
