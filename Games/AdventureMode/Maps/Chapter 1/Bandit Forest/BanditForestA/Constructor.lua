-- |[ ======================================= Constructor ====================================== ]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = fnResolveDirectory() --Map's name is the name of the folder it is in.
local sLevelMusic = "ForestTheme"
local sMapResolveName = "Map Resolve Name"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[ ======================================== Standard ======================================== ]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

    -- |[Construction]|
	--Music.
	AL_SetProperty("Music", sLevelMusic)
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
    
    -- |[Map Handling]|
    --Resolve the map's variable.
    fnStandardCh1Map(sLevelName)

-- |[ ======================================== Post-Exec ======================================= ]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	-- |[Victoria's Army]|
	--Victoria and some mannequins spawn during a quest segment.
    local iBeganSequence     = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iBeganSequence", "N")
    local iDefeatedLeader    = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iDefeatedLeader", "N")
    local iCompletedSequence = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iCompletedSequence", "N")
    if(iBeganSequence == 1.0 and iDefeatedLeader == 0.0) then
        fnStandardNPCByPosition("Victoria")
    
    --When Victoria is not here, remove the mannequins.
    else
        AL_SetProperty("Set Layer Disabled", "MannLo", true)
        AL_SetProperty("Set Layer Disabled", "MannHi", true)
        AL_SetProperty("Set Collision", 20, 7, 0, 0)
        AL_SetProperty("Set Collision", 23, 5, 0, 0)
        AL_SetProperty("Set Collision", 23, 8, 0, 0)
    end
    
    --Despawn bandit enemies if the mannequin sequence has begun. They stay despawned.
    if(iBeganSequence == 1.0) then
        local saDespawnList = {"EnemyAA", "EnemyBA", "EnemyBB", "EnemyCA", "EnemyDA"}
        for i = 1, #saDespawnList, 1 do
            EM_PushEntity(saDespawnList[i])
                RE_SetDestruct(true)
            DL_PopActiveObject()
        end
    end
    
    --If the player has not run into Polaris, despawn all Mannequin Bandit enemies.
    local iPolarisRunIn = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iPolarisRunIn", "N")
    if(iPolarisRunIn < 1.0) then
        local saDespawnList = {"MannEnemyAA", "MannEnemyAB", "MannEnemyBA", "MannEnemyCA", "MannEnemyCB", "MannEnemyCC"}
        for i = 1, #saDespawnList, 1 do
            EM_PushEntity(saDespawnList[i])
                RE_SetDestruct(true)
            DL_PopActiveObject()
        end
    end
    
    -- |[Overlays]|
    --Night overlay during this segment.
    if(iBeganSequence == 1.0 and iCompletedSequence == 0.0) then
        AL_SetProperty("Music", "Null")
        AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, gcf_Night_R, gcf_Night_G, gcf_Night_B, gcf_Night_A, gcf_Night_R, gcf_Night_G, gcf_Night_B, gcf_Night_A, true)
    end
    
    -- |[Skillbooks]|
    --Place skillbook overlay code here.

end
