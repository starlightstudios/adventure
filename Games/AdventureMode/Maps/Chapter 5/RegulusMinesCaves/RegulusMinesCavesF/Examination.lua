-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[ ======================================== Standard ======================================== ]|
--All pregenrandom levels use a common examinable script. If the script handles the object call,
-- the variable gbCaughtScript gets set to true.
gbCaughtScript = nil
LM_ExecuteScript(MapHelper.sExamineIntercept, sObjectName)
if(gbCaughtScript == true) then
    gbCaughtScript = nil
	return
end
