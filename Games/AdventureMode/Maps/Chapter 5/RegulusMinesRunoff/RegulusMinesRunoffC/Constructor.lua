-- |[ ======================================= Constructor ====================================== ]|
--Special map. While technically a normal map, the Regulus Mines floors auto-generate enemy compositions
-- and patrol routes. This construct therefore calls special functions to handle generation and
-- selection of enemy properties.
local bSpawnParty     = false					--Player warps to PlayerStart if map is warped to from debug menu.
local sBasePath       = fnResolvePath()
local sLevelName      = fnResolveDirectory() 	--Map's name is the name of the folder it is in.
local sChestName      = sLevelName				--Name used for chest storage vars. Must be unique, usually same as level name.
local sLevelMusic     = "RegulusTense"			--Set to "NoChange" to use previous. Set to "Null" to stop music.
local sMapResolveName = "TelluriumMinesB"		--Map lookup to use for where this map is on the in-game map UI.

-- |[Argument Check]|
--Determine constructor type. If not provided, then this is a warp from the debug menu.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then
	iConstructorType = LM_GetScriptArgument(0, "N")
else
	bSpawnParty = true
end

-- |[ ======================================== Standard ======================================== ]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then
	
	--Special: Because this is a randomly generated map, all chests are always full and will have
	-- their contents randomized. This is easily done by just deleting the DataLibrary's path that
	-- stores if chests are opened.
	DL_Purge("Root/Variables/Chests/" .. sChestName .. "/", false)
	
  --MapHelper:fnStandardLevel(psCallingPath, psLevelName, psChestLevelName, psMapResolveName, pbMustCreateChars, psMusicName, $piTileSize)
	MapHelper:fnStandardLevel(sBasePath, sLevelName, sChestName, sMapResolveName, bSpawnParty, sLevelMusic)

-- |[ ======================================== Post-Exec ======================================= ]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	-- |[NPC Spawning]|
	--If an entity needs to spawn after a post-exec cutscene, put them here.

    -- |[Overlays]|
    --Place overlay code here.
    
    -- |[Skillbooks]|
    --Place skillbook overlay code here.

end
