-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= Exits ========================================== ]|
if(sObjectName == "SampleExit") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("LevelName", "FORCEPOS:28.0x16.0x0")
    
-- |[ ====================================== Examinables ======================================= ]|
--All other objects query if it's Christine or Sophie in the lead.
else

    --Variables.
    local b254SophieLeading = fnIsSophieLeader()

    --Christine Leading:
    if(b254SophieLeading == false) then
        if(sObjectName == "TerminalA") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Leader] ('Shipping and receiving, public queries should be directed to the lord golem of this sector.')") ]])
            fnCutsceneBlocker()
        elseif(sObjectName == "TerminalB") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Leader] ('Pneumatic lift controls' I don't think I need to mess with these.)") ]])
            fnCutsceneBlocker()
        end
    else
        if(sObjectName == "TerminalA") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Sophie] ('Shipping and receiving, public queries should be directed to the lord golem of this sector.')") ]])
            fnCutsceneBlocker()
        elseif(sObjectName == "TerminalB") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Sophie] ('Pneumatic lift controls' It's probably find to ignore this terminal.)") ]])
            fnCutsceneBlocker()
        end
    end

-- |[ ========================================== Other ========================================= ]|
-- |[ ========================================== Debug ========================================= ]|
--else
	--fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
