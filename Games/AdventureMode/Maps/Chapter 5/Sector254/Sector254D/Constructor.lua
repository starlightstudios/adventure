-- |[ ======================================= Constructor ====================================== ]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = fnResolveDirectory() --Map's name is the name of the folder it is in.
local sLevelMusic = "RegulusTense"
local sMapResolveName = "Map Resolve Name"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[ ======================================== Standard ======================================== ]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

    -- |[Construction]|
	--Music.
	AL_SetProperty("Music", sLevelMusic)
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation(sMapResolveName)
    
    --Lights
	AL_SetProperty("Activate Lights")

-- |[ ======================================== Post-Exec ======================================= ]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	-- |[NPC Spawning]|
	--If an entity needs to spawn after a post-exec cutscene, put them here.
    local i254NightEscapeA = VM_GetVar("Root/Variables/Chapter5/Scenes/i254NightEscapeA", "N")
    local i254NightEscapeB = VM_GetVar("Root/Variables/Chapter5/Scenes/i254NightEscapeB", "N")
    if(i254NightEscapeA == 1.0 and i254NightEscapeB == 0.0) then
        fnStandardNPCByPosition("Night")
    end
    
    --PDU.
    local i254SophieLeading = VM_GetVar("Root/Variables/Chapter5/Scenes/i254SophieLeading", "N")
    local i254FoundPDU      = VM_GetVar("Root/Variables/Chapter5/Scenes/i254FoundPDU", "N")
    if(i254SophieLeading == 1.0 and i254FoundPDU == 0.0) then
        TA_Create("PDUNPC")
            TA_SetProperty("Position", 10, 11)
            TA_SetProperty("Clipping Flag", false)
            TA_SetProperty("Rendering Depth", -0.999990)
            for p = 1, 4, 1 do
                TA_SetProperty("Move Frame", 0, p-1, "Root/Images/Sprites/Objects/PDU")
                TA_SetProperty("Move Frame", 1, p-1, "Root/Images/Sprites/Objects/PDU")
                TA_SetProperty("Move Frame", 2, p-1, "Root/Images/Sprites/Objects/PDU")
                TA_SetProperty("Move Frame", 3, p-1, "Root/Images/Sprites/Objects/PDU")
            end
            TA_SetProperty("Facing", 0)
            TA_SetProperty("Activation Script", fnResolvePath() .. "Dialogue.lua")
        DL_PopActiveObject()
    end

    -- |[Overlays]|
    --Place overlay code here.
    
    -- |[Skillbooks]|
    --Place skillbook overlay code here.

end
