-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[Name Resolve]|
    --Resolve the name of the calling entity.
    local sActorName = TA_GetProperty("Name")

    --Set facing.
    TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[PDUNPC]|
    if(sActorName == "PDUNPC") then
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Scenes/i254FoundPDU", "N", 1.0)
        
        --Setup.
        fnCutsceneTeleport("PDUNPC", -1.25, -1.50)
        fnCutscenePlaySound("World|TakeItem")
        fnCutsceneMergeParty()
        fnCutsceneBlocker()
        
        --Spawn NPCs.
        TA_Create("8714")
            TA_SetProperty("Position", 35, 17)
            TA_SetProperty("Facing", gci_Face_South)
            TA_SetProperty("Clipping Flag", true)
            fnSetCharacterGraphics("Root/Images/Sprites/Doll/", false)
        DL_PopActiveObject()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sophie", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Christine", "Serious") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] (Hey, this is Christine's PDU![P] And her runestone is right under it!)[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Blush] (I really did think she was a secrebot for a microsecond there but...[P] does this mean all the secrebots were humans?)[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] (Okay, I better get her to the repair -[P] wait, is someone coming?)[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] *Christine -[P] CR-1-16![P] Hide!*") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneMove("Sophie", 7.25, 12.50, 2.50)
        fnCutsceneFace("Sophie", 1, 0)
        fnCutsceneMove("Christine", 7.25, 11.50, 2.50)
        fnCutsceneFace("Christine", 1, 0)
        fnCutsceneMove("8714", 12.25, 17.50)
        fnCutsceneCamera("Position", gcfCamera_Cutscene_Speed, 12.25, 17.50)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Doll", "Neutral") ]])
        fnCutscene([[ Append("8714:[E|Neutral] Blast it, it has to be around here somewhere.[P] Hm?[B][C]") ]])
        fnCutscene([[ Append("8714:[E|Neutral] *click*[P] Yes?[P] Prime Command Unit, good.[P] I am in the sector trying to locate the source.[B][C]") ]])
        fnCutscene([[ Append("8714:[E|Neutral] Yes, your prediction was correct.[P] Radio waves in the frequency you specified.[P] Same as the other incident, right at the time of the asset's transformation.[B][C]") ]])
        fnCutscene([[ Append("8714:[E|Neutral] Actually there were two bursts, but that may be have been due to whatever device they are using.[B][C]") ]])
        fnCutscene([[ Append("8714:[E|Neutral] ...[P] Are you sure?[P] I see.[B][C]") ]])
        fnCutscene([[ Append("8714:[E|Neutral] ...[B][C]") ]])
        fnCutscene([[ Append("8714:[E|Neutral] Somewhere in the sector, but I was not able to triangulate the signal.[P] Within a few hundred meters of my position.[B][C]") ]])
        fnCutscene([[ Append("8714:[E|Neutral] ...[P] Affirmative.[P] How long?[B][C]") ]])
        fnCutscene([[ Append("8714:[E|Neutral] No, the lord unit does not suspect anything but can we afford to wait that long?[B][C]") ]])
        fnCutscene([[ Append("8714:[E|Neutral] What?[P] Again?[P] The whole platoon -[P] gone!?[P] Damn it...[B][C]") ]])
        fnCutscene([[ Append("8714:[E|Neutral] Permit me to link up with the assault group and lead the attack.[P] I am most familiar with the area.[B][C]") ]])
        fnCutscene([[ Append("8714:[E|Neutral] Affirmative.[P] Out.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Movement.
        fnCutsceneMove("8714", 35.25, 17.50, 2.00)
        fnCutsceneBlocker()
        fnCutsceneCamera("Actor Name", gcfCamera_Cutscene_Speed, "Sophie")
        fnCutsceneTeleport("8714", -1.25, -1.50)
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sophie", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Christine", "Serious") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] What is she talking about?[P] Assault group?[P] Other incident?[B][C]") ]])
        fnCutscene([[ Append("CR-1-16:[E|Serious] The radio signals she is referring to are the ones detected during the Cryogenics Incident.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Offended] Oh, so you remember![B][C]") ]])
        fnCutscene([[ Append("CR-1-16:[E|Serious] I -[P] I am only trying to serve you, superior unit.[P] I cannot elaborate.[P] I do not know why.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Offended] Let's get out of here and get your head screwed back on before that assault group gets here!") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Fold
        fnAutoFoldParty()
        fnCutsceneBlocker()
    end
end
