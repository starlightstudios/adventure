-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ==================================== Hey, Free Stuff! ==================================== ]|
if(sObjectName == "FreeStuff") then
    
    -- |[Repeat Check]|
    local i254FreeStuff = VM_GetVar("Root/Variables/Chapter5/Scenes/i254FreeStuff", "N")
    if(i254FreeStuff == 1.0) then return end
    
    -- |[Variables]|
    local i254SophieLeading  = VM_GetVar("Root/Variables/Chapter5/Scenes/i254SophieLeading", "N")
    local i254FixedChristine = VM_GetVar("Root/Variables/Chapter5/Scenes/i254FixedChristine", "N")
    
    -- |[Flag]|
    VM_SetVar("Root/Variables/Chapter5/Scenes/i254FreeStuff", "N", 1.0)
    
    -- |[Dialogue]|
    if(i254SophieLeading == 0.0 or i254FixedChristine == 1.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] (Hello, what do we have here?[P] A bunch of free stuff and nobody to guard it?)[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] (Looks like scrap but it could well be useful.[P] Nobody will mind if I take it right?)") ]])
        fnCutsceneBlocker()
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sophie", "Neutral") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] (Hey, the chests here have a bunch of recycleable scrap!)[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Happy] (Maybe Christine's revolution could make use of it?[P] I'll steal it![P] No one will ever know!)") ]])
        fnCutsceneBlocker()
        
    end

-- |[ ==================================== Night Encounter ===================================== ]|
elseif(sObjectName == "OhNoW") then
    
    -- |[Repeat Check]|
    local i254NightEscapeB = VM_GetVar("Root/Variables/Chapter5/Scenes/i254NightEscapeB", "N")
    if(i254NightEscapeB == 1.0) then return end
    
    -- |[Activation Check]|
    local i254NightEscapeA = VM_GetVar("Root/Variables/Chapter5/Scenes/i254NightEscapeA", "N")
    if(i254NightEscapeA == 0.0) then return end
    
    -- |[Flag]|
    VM_SetVar("Root/Variables/Chapter5/Scenes/i254NightEscapeB", "N", 1.0)
    
    -- |[Movement]|
    fnCutsceneMove("Christine", 7.25, 5.50)
    fnCutsceneBlocker()
    fnCutsceneFace("Christine", -1, 0)
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Night", "Human") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Human, don't be afraid![P] I'm trying [P][C]") ]])
    fnCutscene([[ Append("Human:[E|Panic] Leave me alone!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneMove("Night", 4.25, 5.50, 2.50)
    fnCutsceneMove("Night", 6.25, 5.50, 2.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscenePlaySound("World|Thump")
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutscenePlaySound("World|Thump")
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Christine:[VOICE|Christine] Reinforced metal chassis.[P] Look, just -") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutscenePlaySound("World|Flee")
    fnCutsceneMove("Night",  6.25, 6.50, 2.50)
    fnCutsceneMove("Night", 10.25, 6.50, 2.50)
    fnCutsceneBlocker()
    fnCutsceneFace("Christine", 1, 0)
    fnCutsceneMove("Night", 10.25, 8.50, 2.50)
    fnCutsceneMove("Night", 24.25, 8.50, 2.50)
    fnCutsceneMove("Night", 24.25, 4.50, 2.50)
    fnCutsceneBlocker()
    fnCutsceneTeleport("Night", -1.25, -1.50)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    local iResolvedCassandraQuest = VM_GetVar("Root/Variables/Chapter5/Scenes/iResolvedCassandraQuest", "N")
    if(iResolvedCassandraQuest == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Angry") ]])
        fnCutscene([[ Append("Christine:[E|Angry] Stupid human![P] Maybe if I yelled 'Debate me!' that'd make her stop and listen to reason.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] If I don't catch her soon, the drones will![P] Next time, I'll get rough if I have to!") ]])
        fnCutsceneBlocker()
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Angry") ]])
        fnCutscene([[ Append("Christine:[E|Angry] This is giving me deja vu for what happened in Sector 15.[P] I don't have time to transform and coax her out, though.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] If I don't catch her soon, the drones will![P] Next time, I'll get rough if I have to!") ]])
        fnCutsceneBlocker()
    
    end
end
