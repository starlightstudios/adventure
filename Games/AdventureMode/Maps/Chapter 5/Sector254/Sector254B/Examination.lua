-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[ ========================================= Exits ========================================== ]|
if(sObjectName == "ToSector254C") then
    
    --Variables.
    local i254SawDrones     = VM_GetVar("Root/Variables/Chapter5/Scenes/i254SawDrones", "N")
    local i254SophieLeading = VM_GetVar("Root/Variables/Chapter5/Scenes/i254SophieLeading", "N")
    local i254TalkedTo55    = VM_GetVar("Root/Variables/Chapter5/Scenes/i254TalkedTo55", "N")

    --Has not seen the drones run by yet.
    if(i254SawDrones == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (This leads into the warehouse.[P] Better not go snooping around without a good reason.)") ]])
        fnCutsceneBlocker()

    --Sophie is in the lead but hasn't talked to 55 yet:
    elseif(i254SophieLeading == 1.0 and i254TalkedTo55 == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Sophie] (If Unit 487201 catches me in the warehouse without permission she'll have me reprimanded.)") ]])
        fnCutsceneBlocker()

    else
        AudioManager_PlaySound("World|AutoDoorOpen")
        AL_BeginTransitionTo("Sector254C", "FORCEPOS:34.5x26.0x0")
    end
    
elseif(sObjectName == "ToSector254D") then
    
    --Variables.
    local i254SawDrones     = VM_GetVar("Root/Variables/Chapter5/Scenes/i254SawDrones", "N")
    local i254SophieLeading = VM_GetVar("Root/Variables/Chapter5/Scenes/i254SophieLeading", "N")
    local i254TalkedTo55    = VM_GetVar("Root/Variables/Chapter5/Scenes/i254TalkedTo55", "N")
    
    --Has not seen the drones run by yet.
    if(i254SawDrones == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (This leads into the warehouse.[P] Better not go snooping around without a good reason.)") ]])
        fnCutsceneBlocker()

    --Sophie is in the lead but hasn't talked to 55 yet:
    elseif(i254SophieLeading == 1.0 and i254TalkedTo55 == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Sophie] (If Unit 487201 catches me in the warehouse without permission she'll have me reprimanded.)") ]])
        fnCutsceneBlocker()
    
    else
        AudioManager_PlaySound("World|AutoDoorOpen")
        AL_BeginTransitionTo("Sector254D", "FORCEPOS:24.5x4.0x0")
    end
    
-- |[ ====================================== Examinables ======================================= ]|
--All other objects query if it's Christine or Sophie in the lead.
else

    --Variables.
    local b254SophieLeading = fnIsSophieLeader()
    
    --Christine Leading:
    if(b254SophieLeading == false) then
        if(sObjectName == "TerminalA") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Leader] ('Raw materials inventory for shipping to sector 25.[P] Do NOT ship to sector 250 or they'll jam melted rubber in the door locks on me again!')") ]])
            fnCutsceneBlocker()
        elseif(sObjectName == "TerminalB") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Leader] (Short-term storage inventory listings.[P] These shelves are meant to be sorted.[P] This shelf stores material damaged and destined to be scrapped.)") ]])
            fnCutsceneBlocker()
        elseif(sObjectName == "TerminalC") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Leader] (Short-term storage inventory listings.[P] These shelves are meant to be sorted.[P] This shelf stores material that is to be sent to a repair bay for evaluations.)") ]])
            fnCutsceneBlocker()
        elseif(sObjectName == "TerminalD") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Leader] (Short-term storage inventory listings.[P] These shelves are meant to be sorted.[P] This shelf stores material that is obsoleted.)") ]])
            fnCutsceneBlocker()
        elseif(sObjectName == "TerminalE") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Leader] ('Authorized Sector 254 workers in the warehouse only.[P] Visitors, please see the lord golem in charge for authorization.')") ]])
            fnCutsceneBlocker()
        elseif(sObjectName == "TerminalF") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Leader] ('Remember to take breaks![P] Three 60 second breaks per workday!')") ]])
            fnCutsceneBlocker()
        elseif(sObjectName == "TerminalG") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Leader] (Oh great, it's yet another crude 'All My Processors' fanfiction page.[P] I am not reading all that.)") ]])
            fnCutsceneBlocker()
        elseif(sObjectName == "TerminalH") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Leader] (An inventory of terminals marked for outbound trams, probably to a recycling plant.)") ]])
            fnCutsceneBlocker()
        elseif(sObjectName == "FizzyPop") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Leader] (Fizzy Pop![P] This machine doesn't actually work, it's probably on its way to be warehoused.)") ]])
            fnCutsceneBlocker()
        elseif(sObjectName == "JunkTerminals") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Leader] (Terminals with most of the circuit boards stripped.[P] The housings will probably get recycled later.)") ]])
            fnCutsceneBlocker()
        elseif(sObjectName == "Storable") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Leader] (Sensor machines with the batteries taken out.[P] Probably destined to be stored.)") ]])
            fnCutsceneBlocker()
        elseif(sObjectName == "LaptopA") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Leader] (The secrebot was ordered to write a story![P] Hey, this is pretty good, but the first act sucks.[P] Why are two engaged humans out camping with one random friend?)") ]])
            fnCutsceneBlocker()
        elseif(sObjectName == "LaptopB") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Leader] ('Drone unit, do NOT forget to mop the residence floors with floor cleaner! If you use just water again I'll have your drives wiped!')") ]])
            fnCutsceneBlocker()
        elseif(sObjectName == "RVDA") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Leader] ('Safety is everyone's function assignment!'[P] As if I needed to be told not to handle plutonium waste without proper CPU protection.)") ]])
            fnCutsceneBlocker()
        elseif(sObjectName == "RVDB") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Leader] (The indie breakout hit show 'Humans In Love' is showing.[P] Oh![P] There's action-star Sammy Davis![P] She's great in everything she's in.[P] Didn't know she had a cameo!)") ]])
            fnCutsceneBlocker()
        elseif(sObjectName == "Residences") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Leader] (I don't have a reason to go into the residential wing of the sector.)") ]])
            fnCutsceneBlocker()
        end

    --Sophie Leading:
    else
        if(sObjectName == "TerminalA") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Sophie] (Jamming rubber in the door locks?[P] What a petty, mean thing to do over a simple mistake![P] Meanies!)") ]])
            fnCutsceneBlocker()
        elseif(sObjectName == "TerminalB") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Sophie] (Short-term storage inventory listings.[P] These shelves are meant to be sorted.[P] This shelf stores material damaged and destined to be scrapped.)") ]])
            fnCutsceneBlocker()
        elseif(sObjectName == "TerminalC") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Sophie] (Short-term storage inventory listings.[P] These shelves are meant to be sorted.[P] I can tell you right away, this stuff needs to be scrapped.)") ]])
            fnCutsceneBlocker()
        elseif(sObjectName == "TerminalD") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Sophie] (Short-term storage inventory listings.[P] These shelves are meant to be sorted.[P] This shelf stores material that is obsoleted.)") ]])
            fnCutsceneBlocker()
        elseif(sObjectName == "TerminalE") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Sophie] ('Authorized Sector 254 workers in the warehouse only. Visitors, please see the lord golem in charge for authorization.')") ]])
            fnCutsceneBlocker()
        elseif(sObjectName == "TerminalF") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Sophie] ('Remember to take breaks! Three 60 second breaks per workday!'[P] Christine is such a sweetie to me...)") ]])
            fnCutsceneBlocker()
        elseif(sObjectName == "TerminalG") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Sophie] (Oh my gosh, 'All My Processors' fanfiction![P] I love this![P] Uh oh, better not get caught up reading it.)") ]])
            fnCutsceneBlocker()
        elseif(sObjectName == "TerminalH") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Sophie] (An inventory of terminals marked for outbound trams, probably to a recycling plant.)") ]])
            fnCutsceneBlocker()
        elseif(sObjectName == "FizzyPop") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Sophie] (Fizzy Pop![P] The stock is gone, must be on its way to be stored.)") ]])
            fnCutsceneBlocker()
        elseif(sObjectName == "JunkTerminals") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Sophie] (Terminals with most of the circuit boards stripped.[P] The housings will probably get recycled later.)") ]])
            fnCutsceneBlocker()
        elseif(sObjectName == "Storable") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Sophie] (Sensor machines with the batteries taken out.[P] Probably destined to be stored.)") ]])
            fnCutsceneBlocker()
        elseif(sObjectName == "LaptopA") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Sophie] (Oh, the secrebot was writing a story?[P] ...[P] When was the last time I saw rain?[P] Wow, really takes me back.)") ]])
            fnCutsceneBlocker()
        elseif(sObjectName == "LaptopB") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Sophie] ('Drone unit, do NOT forget to mop the residence floors with floor cleaner! If you use just water again I'll have your drives wiped!')") ]])
            fnCutsceneBlocker()
        elseif(sObjectName == "RVDA") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Sophie] ('Safety is everyone's function assignment!'[P] Reminds of the time Christine had to spend a day getting decontaminated because she thought she could handle plutonium for 'just a couple minutes'.)") ]])
            fnCutsceneBlocker()
        elseif(sObjectName == "RVDB") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Sophie] (Wow, 'Humans In Love' is more popular than I thought.[P] Woah, is that Sammy Davis?[P] What a hell of a cameo in your second episode!)") ]])
            fnCutsceneBlocker()
        elseif(sObjectName == "Residences") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Sophie] (This leads to the residences.[P] Probably just small blocks for the workers, nothing to see there.)") ]])
            fnCutsceneBlocker()
        end
    end

-- |[ ========================================== Other ========================================= ]|
-- |[ ========================================== Debug ========================================= ]|
--else
	--fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
