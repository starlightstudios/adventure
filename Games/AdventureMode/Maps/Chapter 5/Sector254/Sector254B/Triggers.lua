-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ================================== Command Unit Meeting ================================== ]|
if(sObjectName == "Overhear") then

    -- |[Repeat Check]|
    local i254SawCommandUnit = VM_GetVar("Root/Variables/Chapter5/Scenes/i254SawCommandUnit", "N")
    if(i254SawCommandUnit == 1.0) then return end
    
    -- |[Flags]|
    VM_SetVar("Root/Variables/Chapter5/Scenes/i254SawCommandUnit", "N", 1.0)
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SecrebotP", "Neutral") ]])
    fnCutscene([[ Append("Secrebot:[E|Neutral] Authenticating...[P] Hello, Lord Unit 771852![P] How may I help you today?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Laugh] Oh, a secrebot being a secretary![P] Hello![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] I would like a meeting with the lord unit here.[P] Is she available?[B][C]") ]])
    fnCutscene([[ Append("Secrebot:[E|Neutral] Lord Unit 487201 is presently in a meeting with Command Unit 8714.[P] Please wait until the meeting is concluded.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] All right.[P] Is it okay if I wait here?[B][C]") ]])
    fnCutscene([[ Append("Secrebot:[E|Neutral] Certainly.[P] Would you like any refreshments?[P] Oil, Fizzy Pop!, perhaps some tires?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Old tires are my favourite![P] But no thank you.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] (Let's see...[P] I can put my hands on the wall and try to hear through the vibrations...[P] Let's see what they're talking about...)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneMove("Christine", 28.25, 11.50)
    fnCutsceneFace("Christine", 0, 1)
    fnCutsceneCamera("Position", gcfCamera_Cutscene_Speed, 27.25, 7.50)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "GolemLord", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
    fnCutscene([[ Append("8714:[E|Neutral] ...[P] Yes, it will be arriving soon.[P] I'm expecting several more by the end of the week.[P] I'd like to triple your current production rate.[B][C]") ]])
    fnCutscene([[ Append("487201:[E|Neutral] Triple it?[P] Splendid.[P] We've been refining the process, I think we can match the expected output.[B][C]") ]])
    fnCutscene([[ Append("8714:[E|Neutral] Yet you insist on not allowing me to inspect the process?[B][C]") ]])
    fnCutscene([[ Append("487201:[E|Neutral] I recognize your request but I have been passed over for promotions before.[P] No offense, of course.[P] I will give the administration full access to the schematics in due time once I am completely certain that the process is perfected, and that my recognition will not be denied.[B][C]") ]])
    fnCutscene([[ Append("8714:[E|Neutral] You're very defensive, I see.[B][C]") ]])
    fnCutscene([[ Append("487201:[E|Neutral] I've had work stolen from my work terminal before.[P] I am presently working in a warehouse sector and not the university.[P] I -[P] well, there is no need to say more.[B][C]") ]])
    fnCutscene([[ Append("487201:[E|Neutral] Thank you for your resources and appreciation.[P] When is the asset set to arrive?[B][C]") ]])
    fnCutscene([[ Append("8714:[E|Neutral] In a few minutes, actually.[P] The alert will be put out, you do the rest.[P] I had to call in a few favours with Abductions to keep this under wraps.[B][C]") ]])
    fnCutscene([[ Append("8714:[E|Neutral] I, too, am looking for a promotion.[B][C]") ]])
    fnCutscene([[ Append("487201:[E|Neutral] Oh?[P] And what will Unit 2855 say about that?[B][C]") ]])
    fnCutscene([[ Append("8714:[E|Neutral] (She doesn't know?[P] Good.)[B][C]") ]])
    fnCutscene([[ Append("8714:[E|Neutral] So far she has had no objections.[P] See to it she has no reason to doubt our work, and I will see to it you are given a senior position at the academy.[B][C]") ]])
    fnCutscene([[ Append("8714:[E|Neutral] Now I have pressing things to see to.[P] Goodbye.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneCamera("Actor Name", gcfCamera_Cutscene_Speed, "Christine")
    fnCutsceneMove("8714", 27.25, 9.50)
    fnCutsceneBlocker()
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Open Door", "OfficeDoor") ]])
    fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneMove("8714", 27.25, 15.50)
    fnCutsceneBlocker()
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    fnCutsceneFace("8714", 0, -1)
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    fnCutsceneMove("8714", 27.25, 11.50)
    fnCutsceneFace("8714", 1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Christine", -1, 0)
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
    fnCutscene([[ Append("8714:[E|Neutral] You.[P] Lord Unit 771852.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Hello, Command Unit.[P] How may I serve you?[B][C]") ]])
    fnCutscene([[ Append("8714:[E|Neutral] What are you doing here?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] I was told that you need to make an order in person to have a secrebot made.[P] I was hoping to get one for my department.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Sad] I've heard the wait list is a little long, though...[B][C]") ]])
    fnCutscene([[ Append("8714:[E|Neutral] It is.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] I assume it'll shorten when we ramp up production.[P] I was told the secrebots are in a beta phase.[P] Is that correct?[B][C]") ]])
    fnCutscene([[ Append("8714:[E|Neutral] Yes.[P] I was just having a brief discussion about that very fact.[B][C]") ]])
    fnCutscene([[ Append("8714:[E|Neutral] Very well.[P] Carry on, Lord Unit.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneFace("Christine", 0, 1)
    fnCutsceneMove("8714", 27.25, 15.50)
    fnCutsceneMove("8714", 23.25, 15.50)
    fnCutsceneMove("8714", 23.25, 20.50)
    fnCutsceneMove("8714", 29.25, 20.50)
    fnCutsceneMove("8714", 29.25, 22.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneTeleport("8714", -1.25, -1.50)
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "SecrebotP", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Offended] (Great, can I not go two minutes without running into some doings, intrigue, or goings-on?)[B][C]") ]])
    fnCutscene([[ Append("Secrebot:[E|Neutral] Lord Unit 487201 will see you know.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Thank you, secrebot.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneMove("Christine", 27.25, 11.50)
    fnCutsceneMove("Christine", 27.25,  7.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
    fnCutscene([[ Append("487201:[E|Neutral] Christine![P] My, I didn't expect you to come by today![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Are we...[P] acquainted, Unit 487201?[B][C]") ]])
    fnCutscene([[ Append("Odess:[E|Neutral] Please, call me Odess.[P] Yes, well, no, not quite.[B][C]") ]])
    fnCutscene([[ Append("Odess:[E|Neutral] Do you recall that safety interlocks issue from a week ago?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Laugh] With the fried interface demodulator?[P] That was you, now I recall![B][C]") ]])
    fnCutscene([[ Append("Odess:[E|Neutral] We're a warehousing sector so we don't have our own repair bay.[P] I was in a real short-circuit, you really saved my motherboard.[B][C]") ]])
    fnCutscene([[ Append("Odess:[E|Neutral] Sector 96 really has a reputation for quick and efficient repairs, so thank you so much for that.[P] I still owe you one.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Does that constitute getting me a secrebot, perchance?[B][C]") ]])
    fnCutscene([[ Append("Odess:[E|Neutral] Oh, dear.[P] I'm sorry but...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Yes, I've heard it's an extremely long waiting list.[P] I understand.[B][C]") ]])
    fnCutscene([[ Append("Odess:[E|Neutral] I could maybe get you one a little early but I have command units on that list and they do not appreciate being made to wait.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Is that what Command Unit 8714 was talking to you about?[P] She seemed rather terse.[B][C]") ]])
    fnCutscene([[ Append("Odess:[E|Neutral] Yes, my new line of secrebots is the talk of the city.[P] You know I used to be a programmer...[P] well, I don't need to dredge up the history.[B][C]") ]])
    fnCutscene([[ Append("Odess:[E|Neutral] My new AI enrichment process does take time, but it delivers a semi-learning, perfectly loyal, partially creative AI that can assist with all sorts of tasks.[P] Repairs, construction, scouting...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Combat?[B][C]") ]])
    fnCutscene([[ Append("Odess:[E|Neutral] Yes.[P] Well, I guess it's obvious why Security would be interested in them.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] I was just hoping to get one to help out around the repair bay.[P] It's all right, I hope it all goes well for you.[B][C]") ]])
    fnCutscene([[ Append("Odess:[E|Neutral] Of course.[P] I'll make sure you're on the waiting list, as high as I can get you without getting my own chassis flayed.[B][C]") ]])
    fnCutscene([[ Append("Odess:[E|Neutral] Success brings with it a host of new problems, doesn't it![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Of course![P] Thank you for your consideration.[P] Drop by Sector 96 if you're ever in the south end of the city, we'll do tea!") ]])
    fnCutsceneBlocker()
    
-- |[ ======================================== Drones! ========================================= ]|
elseif(sObjectName == "Drones") then

    -- |[Repeat Check]|
    local i254SawDrones = VM_GetVar("Root/Variables/Chapter5/Scenes/i254SawDrones", "N")
    if(i254SawDrones == 1.0) then return end
    
    -- |[Activation Check]|
    local i254SawCommandUnit = VM_GetVar("Root/Variables/Chapter5/Scenes/i254SawCommandUnit", "N")
    if(i254SawCommandUnit == 0.0) then return end
    
    -- |[Flags]|
    VM_SetVar("Root/Variables/Chapter5/Scenes/i254SawDrones", "N", 1.0)
    
    -- |[NPCs]|
    TA_Create("RunDroneA")
        TA_SetProperty("Position", 41, 20)
        TA_SetProperty("Facing", gci_Face_West)
        fnSetCharacterGraphics("Root/Images/Sprites/LatexDrone/", false)
    DL_PopActiveObject()
    TA_Create("RunDroneB")
        TA_SetProperty("Position", 41, 21)
        TA_SetProperty("Facing", gci_Face_West)
        fnSetCharacterGraphics("Root/Images/Sprites/LatexDrone/", false)
    DL_PopActiveObject()
    TA_Create("RunDroneC")
        TA_SetProperty("Position", 41, 22)
        TA_SetProperty("Facing", gci_Face_West)
        fnSetCharacterGraphics("Root/Images/Sprites/LatexDrone/", false)
    DL_PopActiveObject()
    
    -- |[Movement]|
    fnCutsceneMove("Christine", 23.25, 20.50)
    fnCutsceneFace("Christine", 1, 0)
    fnCutsceneBlocker()
    fnCutsceneMove("RunDroneA", 24.25, 20.50, 3.00)
    fnCutsceneMove("RunDroneA", 24.25, 19.50, 3.00)
    fnCutsceneMove("RunDroneA", 23.25, 19.50, 3.00)
    fnCutsceneMove("RunDroneA", 23.25, 16.50, 3.00)
    fnCutsceneMove("RunDroneA",  5.25, 16.50, 3.00)
    fnCutsceneMove("RunDroneB", 41.25, 20.50, 3.00)
    fnCutsceneMove("RunDroneB", 24.25, 20.50, 3.00)
    fnCutsceneMove("RunDroneB", 24.25, 21.50, 3.00)
    fnCutsceneMove("RunDroneB", 19.25, 21.50, 3.00)
    fnCutsceneMove("RunDroneB", 19.25, 16.50, 3.00)
    fnCutsceneMove("RunDroneB", 11.25, 16.50, 3.00)
    fnCutsceneMove("RunDroneB", 11.25, 11.50, 3.00)
    fnCutsceneTeleport("RunDroneA", -1.25, -1.50)
    fnCutsceneTeleport("RunDroneB", -1.25, -1.50)
    fnCutsceneBlocker()
    fnCutsceneMove("RunDroneC", 41.25, 20.50, 3.00)
    fnCutsceneMove("RunDroneC", 24.25, 20.50, 3.00)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "LatexDrone", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Offended] Drone unit![P] What is going on, why is everyone running?[B][C]") ]])
    fnCutscene([[ Append("Drone:[E|Neutral] HELLO, LORD UNIT.[P] A ROGUE HUMAN HAS BEEN SIGHTED IN THE WAREHOUSE.[P] WE ARE PERFORMING A SEARCH AND DETAIN.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] A rogue human?[P] Right, off you go.[P] Return to your assignment.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneMove("RunDroneC", 24.25, 19.50, 3.00)
    fnCutsceneMove("RunDroneC", 23.25, 19.50, 3.00)
    fnCutsceneMove("RunDroneC", 23.25, 16.50, 3.00)
    fnCutsceneMove("RunDroneC",  5.25, 16.50, 3.00)
    fnCutsceneBlocker()
    fnCutsceneTeleport("RunDroneC", -1.25, -1.50)
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Offended] (A rogue human, huh?[P] I don't buy that for a minute.[P] How would a human get all the way out to a warehouse on the west edge of the city?[P] I bet that's the asset that 8714 was talking about.)[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] (Even if it isn't, I can't let the administration turn another innocent human into a robot against their will.[P] I better find them before the drones do.)") ]])
    fnCutsceneBlocker()
    
-- |[ ===================================== Sophie Arrives ===================================== ]|
elseif(sObjectName == "SophieArrives") then

    -- |[Party Setup]|
    --Flag for autosaves to hopefully have a chance of recovery.
    VM_SetVar("Root/Variables/Chapter5/Scenes/i254SophieLeading", "N", 1.0)
    
    --Mark Christine as a folloewer.
    EM_PushEntity("Christine")
        local iChristineID = RE_GetID()
    DL_PopActiveObject()
    giFollowersTotal = 1
    gsaFollowerNames[1] = "Christine"
    giaFollowerIDs = {iChristineID}
    AL_SetProperty("Follow Actor ID", iChristineID)
    
    --Spawn and mark Sophie as the leader.
    fnSpecialCharacter("Sophie", 21, 21, gci_Face_North, false, "Null")
	EM_PushEntity("Sophie")
		giPartyLeaderID = RO_GetID()
		TA_SetProperty("Clipping Flag", false)
		TA_SetProperty("Activation Script", "Null")
	DL_PopActiveObject()
	AL_SetProperty("Player Actor ID", giPartyLeaderID)
    
    --Other characters.
    TA_Create("Night")
        TA_SetProperty("Position", 29, 6)
        TA_SetProperty("Facing", gci_Face_South)
        TA_SetProperty("Clipping Flag", true)
        TA_SetProperty("Activation Script", fnResolvePath() .. "Dialogue.lua")
        fnSetCharacterGraphics("Root/Images/Sprites/NightSecrebot/", false)
    DL_PopActiveObject()

    -- |[Blackout]|
    fnCutsceneFadeOut()
    fnCutsceneBlocker()
    
    -- |[Position]|
    fnCutsceneTeleport("Christine", 24.25, 6.50)
    fnCutsceneFace("Christine", 0, 1)
    fnCutsceneCamera("Position", gcfCamera_Cutscene_Speed, 27.25, 7.50)
    fnCutsceneBlocker()
    
    -- |[Wait]|
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Odess:[VOICE|GolemLord] Unit 499323 has just disembarked.[P] CR-1-16, what are your instructions?[B][C]") ]])
    fnCutscene([[ Append("CR-1-16:[VOICE|Christine] Perform normal secrebot functions in Maintenance and Repair, Sector 96.[P] Locate Unit 771852 and terminate her if and when she returns to the sector.[B][C]") ]])
    fnCutscene([[ Append("Odess:[VOICE|GolemLord] Good.[P] Do not mention these instructions to anyone unless I authorize it.[B][C]") ]])
    fnCutscene([[ Append("CR-1-16:[VOICE|Christine] Affirmative.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Fade In]|
    fnCutsceneFadeIn(45)
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneMove("Sophie", 21.25, 15.50)
    fnCutsceneMove("Sophie", 27.25, 15.50)
    fnCutsceneMove("Sophie", 27.25, 11.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Sophie", -1, 0)
    fnCutsceneFace("SecrebotA", 1, 0)
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutsceneFace("Sophie", 0, -1)
    fnCutsceneFace("SecrebotA", 0, 1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Open Door", "OfficeDoor") ]])
    fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneMove("Sophie", 27.25, 7.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sophie", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Christine", "Serious") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "GolemLord", "Neutral") ]])
    fnCutscene([[ Append("487201:[E|Neutral] Ah, there you are.[P] Unit 499323, Maintenance and Repair, Sector 96?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Neutral] Yes, Lord Golem 487201.[P] I am not sure why I -[P] I -[P][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] (Is Christine a -[P] that's gotta be her![P] Why is -[P] Spy stuff![P] Spy stuff![P] She's doing spy stuff I gotta play along!)[P][C]") ]])
    fnCutscene([[ Append("487201:[E|Neutral] Yes, good.[P] Christine requested a secrebot for your department but was called away before it was ready.[B][C]") ]])
    fnCutscene([[ Append("487201:[E|Neutral] She's not answering her PDU, but I checked her record.[P] Apparently she is often out of contact?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] (Don't go mad don't go mad don't go mad don't go mad!)[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] Yes, lord unit.[P] I'm a very poor combatant, so when she is asked to repair something in the undercity...[B][C]") ]])
    fnCutscene([[ Append("487201:[E|Neutral] Getting her hands dirty just to avoid inconveniencing the security units?[P] What a model unit![B][C]") ]])
    fnCutscene([[ Append("487201:[E|Neutral] (Shame she has to be retired.)[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] (Shame she's away from me so often![P] I wanna run over there and smooch her!)[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Smirk] Yes, lord unit.[P] Our whole sector loves her.[P] I'm sure she'll be pleased that her secrebot is ready.[B][C]") ]])
    fnCutscene([[ Append("487201:[E|Neutral] Very good.[P] CR-1-16?[B][C]") ]])
    fnCutscene([[ Append("CR-1-16:[E|Serious] How may I serve, lord unit?[B][C]") ]])
    fnCutscene([[ Append("487201:[E|Neutral] You're being transferred to another department.[P] This slave unit will take you there and instruct you.[P] Follow her orders.[B][C]") ]])
    fnCutscene([[ Append("CR-1-16:[E|Serious] Affirmative, lord unit.[P] I have transferred instruction privileges to Unit 499323.[P] Hello, superior unit.[P] How may I serve?[B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] Uh, oh I'm not used to -[P] ordering another unit around![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] Um, CR-1-16, on authority of Lord Unit 771852, follow me.[P] Hee hee![B][C]") ]])
    fnCutscene([[ Append("487201:[E|Neutral] Excellent.[P] And please tell every lord unit about our new secrebots![P] I want one in every office in the city![P] They clean, they work, and they serve![B][C]") ]])
    fnCutscene([[ Append("Sophie:[E|Blush] (I better get her someplace quiet before I make her tell me what's going on.[P] Oh dear me!)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneMove("Christine", 24.25, 7.50)
    fnCutsceneMove("Christine", 27.25, 7.50)
    fnCutsceneBlocker()
    
    -- |[Fold]|
    fnAutoFoldParty()
    fnCutsceneBlocker()
    
-- |[ ==================================== Trash the Place! ==================================== ]|
elseif(sObjectName == "FightTime") then

    -- |[Spawn, Party]|
    if(EM_Exists("Tiffany") == false) then
        fnSpecialCharacter("Tiffany", -100, -100, gci_Face_North, false, nil)

        --Get character's uniqueID. 
        EM_PushEntity("Tiffany")
            local iTiffanyID = RE_GetID()
        DL_PopActiveObject()

        --Store it and tell her to follow.
        giFollowersTotal = 1
        gsaFollowerNames[1] = "Tiffany"
        giaFollowerIDs = {iTiffanyID}
        AL_SetProperty("Follow Actor ID", iTiffanyID)
    end

    -- |[Setup]|
    fnCutsceneFadeOut()
    fnCutsceneCamera("Position", gcfCamera_Cutscene_Speed, 29.25, 22.50)
    fnCutsceneTeleport("Christine", -1.25, -1.50)
    fnCutsceneTeleport("Tiffany", -1.25, -1.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Fade In]|
    fnCutsceneFadeIn(45)
    fnCutsceneWait(65)
    fnCutsceneBlocker()

    -- |[Movement]|
    fnCutscenePlaySound("World|AutoDoorOpen")
    fnCutsceneTeleport("Christine", 29.25, 22.50)
    fnCutsceneFace("Christine", 0, -1)
    fnCutsceneTeleport("Tiffany", 30.25, 22.50)
    fnCutsceneFace("Tiffany", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
     -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Offended] Oh Odess?[P] I found Unit 771852![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] ...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Quiet.[P] Did the assault team already sweep through?[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Unlikely, no pulse impacts or plasma damage.[P] We need to move quickly.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] Right, the device is in the northwest corner of the north warehouse.[P] Let's go blow it up.") ]])
    fnCutsceneBlocker()
    fnAutoFoldParty()
    fnCutsceneBlocker()

end
