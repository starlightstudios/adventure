-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[Name Resolve]|
    --Resolve the name of the calling entity.
    local sActorName = TA_GetProperty("Name")

    --Set facing.
    TA_SetProperty("Face Character", "PlayerEntity")
    
    --Common variables.
    local i254SophieLeading = VM_GetVar("Root/Variables/Chapter5/Scenes/i254SophieLeading", "N") 
    
    -- |[Odess]|
    if(sActorName == "Odess") then
        
        --Christine meeting:
        if(i254SophieLeading == 0.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
            fnCutscene([[ Append("Odess:[E|Neutral] Never a dull moment around here.[P] I have four different command units breathing down my neck for secrebots.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Breathing?[B][C]") ]])
            fnCutscene([[ Append("Odess:[E|Neutral] Ha ha![P] Maybe I should say 'blowing exhaust' but I think that might give you an entirely different impression.[P] Then again...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] Just don't say it when one is around if you want to have a long, happy synthetic life.") ]])
            fnCutsceneBlocker()
        
        --Sophie:
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Sophie", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Christine", "Serious") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
            fnCutscene([[ Append("487201:[E|Neutral] Yes?[P] Was there something else, 499323?[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Neutral] Please forgive me if I am overstepping my bounds, but I had heard that the waiting list for a secrebot was quite long.[B][C]") ]])
            fnCutscene([[ Append("487201:[E|Neutral] It is![P] Ha ha ha![B][C]") ]])
            fnCutscene([[ Append("487201:[E|Neutral] I owe your sector a favour, so I bumped you up the list.[B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Happy] Oh![P] The safety interlocks problem![P] That was you, wasn't it![P] Yes, Sector 96 is very happy to help![B][C]") ]])
            fnCutscene([[ Append("Sophie:[E|Happy] I would never have guessed a demodulator was fried but Unit 771852 is very smart and helped me look in places I had never thought to![B][C]") ]])
            fnCutscene([[ Append("487201:[E|Neutral] Indeed.[P] Please give her my...[P] regards...[P] when you see her.[P] That goes for you as well, CR-1-16.[B][C]") ]])
            fnCutscene([[ Append("CR-1-16:[E|Serious] Affirmative, lord unit.") ]])
            fnCutsceneBlocker()
        end
    
    -- |[Night]|
    elseif(sActorName == "Night") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Sophie", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Christine", "Serious") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Night", "Secrebot") ]])
        fnCutscene([[ Append("CR-1-15:[E|Secrebot] Hello.[P] May I serve you, superior unit?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Smirk] Oh, hello![P] Secrebot CR-1-15?[P] Does that mean Chr -[P] I mean, CR-1-16 was created right after you?[B][C]") ]])
        fnCutscene([[ Append("CR-1-15:[E|Secrebot] That is correct.[P] We're like sisters![B][C]") ]])
        fnCutscene([[ Append("CR-1-16:[E|Serious] I'm going to miss you, CR-1-15.[P] We haven't been together long, but still.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] (This must be spy stuff, better keep my mouth shut.)[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] I promise I'll bring her by sometimes so you two can visit.[B][C]") ]])
        fnCutscene([[ Append("CR-1-15:[E|Secrebot] Assuming I am still assigned to this sector, that is.[P] Thank you, superior unit.") ]])
        fnCutsceneBlocker()
        
    -- |[Minor NPCs]|
    elseif(sActorName == "SecrebotA") then
        
        --Christine meeting:
        if(i254SophieLeading == 0.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Secrebot:[VOICE|SecrebotP] Please enjoy your stay in our humble sector!") ]])
            fnCutsceneBlocker()
        
        --Sophie:
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Secrebot:[VOICE|SecrebotP] It was nice to meet you, CR-1-16![P] Serve your new sector well!") ]])
            fnCutsceneBlocker()
        end
    
    elseif(sActorName == "SecrebotB") then
        
        --Christine meeting:
        if(i254SophieLeading == 0.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Secrebot:[VOICE|SecrebotY] Coffee, tea, soda, oil?[P] We have refreshments for organic and synthetic alike!") ]])
            fnCutsceneBlocker()
        
        --Sophie:
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Secrebot:[VOICE|SecrebotY] I only just met you CR-1-16, I don't want to say goodbye![P] Come visit!") ]])
            fnCutsceneBlocker()
        end
    
    elseif(sActorName == "SecrebotC") then
        
        --Christine meeting:
        if(i254SophieLeading == 0.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Secrebot:[VOICE|SecrebotY] Sorting.[P] Categorizing.[P] Oh, hello there![P] I haven't been shipped out yet, but I'm making myself useful!") ]])
            fnCutsceneBlocker()
        
        --Sophie:
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Secrebot:[VOICE|SecrebotY] CR-1-16, don't forget to write!") ]])
            fnCutsceneBlocker()
        end
    elseif(sActorName == "GolemA") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Even with all the hubbub about secrebots, my function hasn't changed.[P] Sort, haul, store.") ]])
        fnCutsceneBlocker()
    
    elseif(sActorName == "GolemB") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Where am I going to find a phase transistor to fix this...[P] maybe I should just send it to a repair bay.") ]])
        fnCutsceneBlocker()
    
    elseif(sActorName == "GolemC") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] I'm sorry, too busy to talk.[P] Got to get these updated records for my lord unit!") ]])
        fnCutsceneBlocker()
    
    elseif(sActorName == "DroneA") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|LatexDrone] UNIT AUTOREPAIR ENGAGED.[P] WOULD YOU LIKE SOME OIL?[P] THIS UNIT HAS BEEN ORDERED TO REMAIN HERE UNTIL THIS UNIT'S MOTIVATORS ARE REPAIRED.") ]])
        fnCutsceneBlocker()
    
    elseif(sActorName == "DroneB") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|LatexDrone] SORT, STORE.[P] SORT, STORE.[P] THIS UNIT IS VERY PRODUCTIVE.") ]])
        fnCutsceneBlocker()
    end
end
