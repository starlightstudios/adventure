-- |[ ======================================= Constructor ====================================== ]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = fnResolveDirectory() --Map's name is the name of the folder it is in.
local sLevelMusic = "RegulusTense"
local sMapResolveName = "Map Resolve Name"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[ ======================================== Standard ======================================== ]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

    -- |[Construction]|
	--Music.
	AL_SetProperty("Music", sLevelMusic)
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation(sMapResolveName)

-- |[ ======================================== Post-Exec ======================================= ]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	-- |[NPC Spawning]|
	--If an entity needs to spawn after a post-exec cutscene, put them here.
    local i254FixedChristine = VM_GetVar("Root/Variables/Chapter5/Scenes/i254FixedChristine", "N")
    if(i254FixedChristine == 0.0) then
        fnStandardNPCByPosition("Odess")
        fnStandardNPCByPosition("SecrebotA")
        fnStandardNPCByPosition("SecrebotB")
        local i254SawCommandUnit = VM_GetVar("Root/Variables/Chapter5/Scenes/i254SawCommandUnit", "N")
        if(i254SawCommandUnit == 0.0) then
            fnStandardNPCByPosition("8714")
        end
        
        local i254SophieLeading = VM_GetVar("Root/Variables/Chapter5/Scenes/i254SophieLeading", "N")
        if(i254SophieLeading == 1.0) then
            TA_Create("Night")
                TA_SetProperty("Position", 29, 6)
                TA_SetProperty("Facing", gci_Face_South)
                TA_SetProperty("Clipping Flag", true)
                TA_SetProperty("Activation Script", fnResolvePath() .. "Dialogue.lua")
                fnSetCharacterGraphics("Root/Images/Sprites/NightSecrebot/", false)
            DL_PopActiveObject()
        end
        
        fnStandardNPCByPosition("SecrebotC")
        fnStandardNPCByPosition("GolemA")
        fnStandardNPCByPosition("GolemB")
        fnStandardNPCByPosition("GolemC")
        fnStandardNPCByPosition("DroneA")
        fnStandardNPCByPosition("DroneB")
    end

    -- |[Overlays]|
    --Place overlay code here.
    
    -- |[Skillbooks]|
    --Place skillbook overlay code here.
    
    -- |[Misc]|
    VM_SetVar("Root/Variables/Chapter5/Scenes/iIsTramHere", "N", 0.0)

end
