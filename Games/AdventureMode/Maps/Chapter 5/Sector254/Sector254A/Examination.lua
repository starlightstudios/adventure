-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= Exits ========================================== ]|
if(sObjectName == "ToSector254B") then
    local i254Completed = VM_GetVar("Root/Variables/Chapter5/Scenes/i254Completed", "N")
    if(i254Completed == 1.0) then return end
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("Sector254B", "FORCEPOS:29.5x22.0x0")
    
-- |[ ====================================== Tram Handler ====================================== ]|
elseif(sObjectName == "TramCall") then

	--Variables.
	local iIsTramHere        = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsTramHere", "N")
    local i254SawDrones      = VM_GetVar("Root/Variables/Chapter5/Scenes/i254SawDrones", "N")
    local i254SophieLeading  = VM_GetVar("Root/Variables/Chapter5/Scenes/i254SophieLeading", "N")
    local i254FoundPDU       = VM_GetVar("Root/Variables/Chapter5/Scenes/i254FoundPDU", "N")
    local i254FixedChristine = VM_GetVar("Root/Variables/Chapter5/Scenes/i254FixedChristine", "N")
    local i254Completed      = VM_GetVar("Root/Variables/Chapter5/Scenes/i254Completed", "N")
	
	--Christine can't use the tram.
	if(i254SawDrones == 1.0 and i254SophieLeading == 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[VOICE|Christine](I had better find that human before the drones do!)") ]])
		fnCutsceneBlocker()
    
    --Sophie can't use the tram:
    elseif(i254SophieLeading == 1.0 and i254FoundPDU == 0.0 and i254FixedChristine == 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[VOICE|Sophie](55 said to find Christine's PDU, that it was in the southwest of this sector.[P] I better do that before we leave.)") ]])
		fnCutsceneBlocker()
    
    --Gotta blow up that device!
    elseif(i254FixedChristine == 1.0 and i254Completed == 0.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[VOICE|Christine](We need to blow up that conversion device before the security forces get it!)") ]])
		fnCutsceneBlocker()
	
	--Tram is already here.
    elseif(iIsTramHere == 1.0) then
        if(i254SophieLeading == 0.0 or i254Completed == 1.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("[VOICE|Christine](The tram is here already, no need to access this console.)") ]])
            fnCutsceneBlocker()
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("[VOICE|Sophie](The tram is here already, no need to access this console.)") ]])
            fnCutsceneBlocker()
        end

	--Tram is going to arrive soon.
	else
	
		--Set flags.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iIsTramHere", "N", 1.0)
		VM_SetVar("Root/Variables/Chapter5/Scenes/sCurrentTramArea", "S", "Sector254A")
	
		--Spawn the tram parts.
		TA_Create("TramA")
			TA_SetProperty("Position", 29, 13)
			TA_SetProperty("Move Frame", gci_Face_West, 0, "Root/Images/Sprites/Tram/West|0")
			TA_SetProperty("Move Frame", gci_Face_West, 1, "Root/Images/Sprites/Tram/West|0")
			TA_SetProperty("Move Frame", gci_Face_West, 2, "Root/Images/Sprites/Tram/West|0")
			TA_SetProperty("Move Frame", gci_Face_West, 3, "Root/Images/Sprites/Tram/West|0")
			TA_SetProperty("Facing", gci_Face_West)
			TA_SetProperty("Set Ignore Clips", true)
		DL_PopActiveObject()
		TA_Create("TramB")
			TA_SetProperty("Position", 30, 13)
			TA_SetProperty("Move Frame", gci_Face_West, 0, "Root/Images/Sprites/Tram/North|0")
			TA_SetProperty("Move Frame", gci_Face_West, 1, "Root/Images/Sprites/Tram/North|0")
			TA_SetProperty("Move Frame", gci_Face_West, 2, "Root/Images/Sprites/Tram/North|0")
			TA_SetProperty("Move Frame", gci_Face_West, 3, "Root/Images/Sprites/Tram/North|0")
			TA_SetProperty("Facing", gci_Face_West)
			TA_SetProperty("Set Ignore Clips", true)
		DL_PopActiveObject()
		TA_Create("TramC")
			TA_SetProperty("Position", 31, 13)
			TA_SetProperty("Move Frame", gci_Face_West, 0, "Root/Images/Sprites/Tram/South|0")
			TA_SetProperty("Move Frame", gci_Face_West, 1, "Root/Images/Sprites/Tram/South|0")
			TA_SetProperty("Move Frame", gci_Face_West, 2, "Root/Images/Sprites/Tram/South|0")
			TA_SetProperty("Move Frame", gci_Face_West, 3, "Root/Images/Sprites/Tram/South|0")
			TA_SetProperty("Move Frame", gci_Face_East, 0, "Root/Images/Sprites/Tram/South|1")
			TA_SetProperty("Move Frame", gci_Face_East, 1, "Root/Images/Sprites/Tram/South|1")
			TA_SetProperty("Move Frame", gci_Face_East, 2, "Root/Images/Sprites/Tram/South|1")
			TA_SetProperty("Move Frame", gci_Face_East, 3, "Root/Images/Sprites/Tram/South|1")
			TA_SetProperty("Facing", gci_Face_West)
			TA_SetProperty("Set Ignore Clips", true)
            TA_SetProperty("Extended Activation Direction", gci_Face_North)
			TA_SetProperty("Activation Script", gsRoot .. "Chapter 5/Dialogue/Tram/Root.lua")
		DL_PopActiveObject()
		TA_Create("TramD")
			TA_SetProperty("Position", 32, 13)
			TA_SetProperty("Move Frame", gci_Face_West, 0, "Root/Images/Sprites/Tram/North|1")
			TA_SetProperty("Move Frame", gci_Face_West, 1, "Root/Images/Sprites/Tram/North|1")
			TA_SetProperty("Move Frame", gci_Face_West, 2, "Root/Images/Sprites/Tram/North|1")
			TA_SetProperty("Move Frame", gci_Face_West, 3, "Root/Images/Sprites/Tram/North|1")
			TA_SetProperty("Facing", gci_Face_West)
			TA_SetProperty("Set Ignore Clips", true)
		DL_PopActiveObject()
		TA_Create("TramE")
			TA_SetProperty("Position", 33, 13)
			TA_SetProperty("Move Frame", gci_Face_West, 0, "Root/Images/Sprites/Tram/North|2")
			TA_SetProperty("Move Frame", gci_Face_West, 1, "Root/Images/Sprites/Tram/North|2")
			TA_SetProperty("Move Frame", gci_Face_West, 2, "Root/Images/Sprites/Tram/North|2")
			TA_SetProperty("Move Frame", gci_Face_West, 3, "Root/Images/Sprites/Tram/North|2")
			TA_SetProperty("Facing", gci_Face_West)
			TA_SetProperty("Set Ignore Clips", true)
		DL_PopActiveObject()
		TA_Create("TramF")
			TA_SetProperty("Position", 34, 13)
			TA_SetProperty("Move Frame", gci_Face_West, 0, "Root/Images/Sprites/Tram/North|3")
			TA_SetProperty("Move Frame", gci_Face_West, 1, "Root/Images/Sprites/Tram/North|3")
			TA_SetProperty("Move Frame", gci_Face_West, 2, "Root/Images/Sprites/Tram/North|3")
			TA_SetProperty("Move Frame", gci_Face_West, 3, "Root/Images/Sprites/Tram/North|3")
			TA_SetProperty("Facing", gci_Face_West)
			TA_SetProperty("Set Ignore Clips", true)
		DL_PopActiveObject()
	
		--Dialogue.
        if(i254SophieLeading == 0.0 or i254Completed == 1.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("[VOICE|Christine](Looks like the tram is set to arrive soon...)") ]])
            fnCutsceneBlocker()
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("[VOICE|Sophie](Looks like the tram is set to arrive soon...)") ]])
            fnCutsceneBlocker()
        end
		
		--Move the trams over. They all forcible face west while moving.
		fnCutsceneMoveFace("TramA", 13.75 + 6.0, 13.00, -1, 0, 2.50)
		fnCutsceneMoveFace("TramB", 14.75 + 6.0, 13.00, -1, 0, 2.50)
		fnCutsceneMoveFace("TramC", 15.75 + 6.0, 13.00, -1, 0, 2.50)
		fnCutsceneMoveFace("TramD", 16.75 + 6.0, 13.00, -1, 0, 2.50)
		fnCutsceneMoveFace("TramE", 17.75 + 6.0, 13.00, -1, 0, 2.50)
		fnCutsceneMoveFace("TramF", 18.75 + 6.0, 13.00, -1, 0, 2.50)
		fnCutsceneBlocker()
		fnCutscene([[ AudioManager_PlaySound("World|TramStop") ]])
		fnCutsceneMoveFace("TramA", 13.75 + 3.0, 13.00, -1, 0, 1.00)
		fnCutsceneMoveFace("TramB", 14.75 + 3.0, 13.00, -1, 0, 1.00)
		fnCutsceneMoveFace("TramC", 15.75 + 3.0, 13.00, -1, 0, 1.00)
		fnCutsceneMoveFace("TramD", 16.75 + 3.0, 13.00, -1, 0, 1.00)
		fnCutsceneMoveFace("TramE", 17.75 + 3.0, 13.00, -1, 0, 1.00)
		fnCutsceneMoveFace("TramF", 18.75 + 3.0, 13.00, -1, 0, 1.00)
		fnCutsceneBlocker()
		
		--Trams slow down.
		fnCutsceneMoveFace("TramA", 13.75 + 0.0, 13.00, -1, 0, 0.50)
		fnCutsceneMoveFace("TramB", 14.75 + 0.0, 13.00, -1, 0, 0.50)
		fnCutsceneMoveFace("TramC", 15.75 + 0.0, 13.00, -1, 0, 0.50)
		fnCutsceneMoveFace("TramD", 16.75 + 0.0, 13.00, -1, 0, 0.50)
		fnCutsceneMoveFace("TramE", 17.75 + 0.0, 13.00, -1, 0, 0.50)
		fnCutsceneMoveFace("TramF", 18.75 + 0.0, 13.00, -1, 0, 0.50)
		fnCutsceneBlocker()
		
		--Wait a bit.
		fnCutsceneWait(45)
		fnCutsceneBlocker()

		--Change the facing of part C. This causes the door to open.
		fnCutscene([[ AudioManager_PlaySound("World|TramArrive") ]])
		--fnCutsceneFace("TramC", 1, 0)
		fnCutsceneBlocker()

	end

-- |[ ====================================== Examinables ======================================= ]|
elseif(sObjectName == "Junk") then
    local b254SophieLeading = fnIsSophieLeader()
    if(b254SophieLeading == false) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Some useless junk that isn't even worth being stored in a warehouse.)") ]])
        fnCutsceneBlocker()
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Sophie] (Just leaving junk by the tracks?[P] I bet I could smelt this down into something useful!)") ]])
        fnCutsceneBlocker()
    end

-- |[ ========================================== Other ========================================= ]|
-- |[ ========================================== Debug ========================================= ]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
