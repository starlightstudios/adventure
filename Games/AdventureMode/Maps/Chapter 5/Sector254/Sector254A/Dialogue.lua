-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[Name Resolve]|
    --Resolve the name of the calling entity.
    local sActorName = TA_GetProperty("Name")

    --Set facing.
    TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[Golem]|
    if(sActorName == "Golem") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] Hello, lord unit.[P] Please do not interrupt us, a security sweep is underway.") ]])
    
    -- |[Drone]|
    elseif(sActorName == "Drone") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Drone:[VOICE|LatexDrone] I APOLOGIZE, I AM FORBIDDEN TO SPEAK TO NON-SECURITY UNITS FOR FEAR OF DIVULGING IMPORTANT INFORMATION.") ]])

    -- |[8714]|
    elseif(sActorName == "8714") then
    
        --Variables.
        local i254Extra = VM_GetVar("Root/Variables/Chapter5/Scenes/i254Extra", "N")
        
        --First time.
        if(i254Extra == 0.0) then
            VM_SetVar("Root/Variables/Chapter5/Scenes/i254Extra", "N", 1.0)
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
            fnCutscene([[ Append("8714:[E|Neutral] Unit 771852, I recognize you.[P] What are you doing here?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] Uh, I apologize if I am causing any inconvenience, command unit![P] Is there a problem?[B][C]") ]])
            fnCutscene([[ Append("8714:[E|Neutral] What I tell you depends heavily on how you answer.[P] What are you doing here?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] I was supervising one of my slave units fixing a power node in the lower city.[P] The network down there is a bit spotty, so...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] When I returned to the elevators I was notified a secrebot was ready for me.[P] I, uh...[B][C]") ]])
            fnCutscene([[ Append("8714:[E|Neutral] You pulled a favour to get one ahead of the others.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] I apologize, command unit![B][C]") ]])
            fnCutscene([[ Append("8714:[E|Neutral] Fine.[P] You will not be receiving a secrebot, security is currently in control of this sector, and are performing a sweep.[P] Do not interrupt them.[B][C]") ]])
            fnCutscene([[ Append("8714:[E|Neutral] We found that Unit 487201 was conducting illicit activities and have suspended manufacturing for the time being.[P] If you are needed as a witness, you will be contacted.[B][C]") ]])
            fnCutscene([[ Append("8714:[E|Neutral] Now leave before I find your actions suspicious.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] Right away, command unit!") ]])
    
        --Repeats.
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("8714:[VOICE|Doll] Don't waste your time coming to this sector again.") ]])
        end
    end
end
