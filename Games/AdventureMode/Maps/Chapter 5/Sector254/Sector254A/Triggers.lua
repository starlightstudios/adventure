-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================= Execution ======================================== ]|
if(sObjectName == "Alert") then

    -- |[ ========== Arrival Scene =========== ]|
    --Variables.
    local i254GotAlert = VM_GetVar("Root/Variables/Chapter5/Scenes/i254GotAlert", "N")

    -- |[Activation Check]|
    if(i254GotAlert == 0.0) then
        
        -- |[Flags]|
        VM_SetVar("Root/Variables/Chapter5/Scenes/i254GotAlert", "N", 1.0)
        
        -- |[Dialogue]|
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "PDU") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "PDU", "Neutral") ]])
        fnCutscene([[ Append("PDU:[E|Alarmed] Christine, priority call for you.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|PDU] Put it through.[B][C]") ]])
        fnCutscene([[ Append("PDU:[E|Quiet] [VOICE|Tiffany]Christine.[P] Transit records indicate Command Unit 8714 took the tram to Sector 254 and has not taken a tram leaving it.[P] She may still be there.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|PDU] Command Unit 8714?[P] What's special about her?[B][C]") ]])
        fnCutscene([[ Append("PDU:[E|Quiet] [VOICE|Tiffany]High-ranking security forces unit.[P] Exactly the robot you don't want to let anything slip in front of.[P] Use extreme caution.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|PDU] Got it.[P] Hopefully I can lay low.[P] Thanks for the warning.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] (What's a high-ranking security unit doing in a warehouse sector?[P] And why's a warehouse sector where they make the new secrebots?)") ]])
        fnCutsceneBlocker()
        return
    end

    -- |[ ========= Conspiracy Scene ========= ]|
    --Variables.
    local i254SophieLeading = VM_GetVar("Root/Variables/Chapter5/Scenes/i254SophieLeading", "N")
    local i254TalkedTo55    = VM_GetVar("Root/Variables/Chapter5/Scenes/i254TalkedTo55", "N")
    if(i254SophieLeading == 1.0 and i254TalkedTo55 == 0.0) then

        -- |[Flag]|
        VM_SetVar("Root/Variables/Chapter5/Scenes/i254TalkedTo55", "N", 1.0)

        -- |[Movement]|
        fnCutsceneMove("Sophie", 17.25, 5.50)
        fnCutsceneFace("Sophie", 1, 0)
        fnCutsceneMove("Christine", 18.25, 5.50)
        fnCutsceneFace("Christine", -1, 0)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        -- |[Dialogue]|
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Sophie", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Christine", "Serious") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] (Nobody's around and there's no microphone taps...[P] better do another scan...)[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Happy] (Nope, all clear!)[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Happy] Christine![P] Are you going to tell me what's going on?[B][C]") ]])
        fnCutscene([[ Append("CR-1-16:[E|Serious] Invalid desgination.[P] Would you like to designate me as 'Christine'?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] Huh?[P] Christine, I checked.[P] Nobody is around.[B][C]") ]])
        fnCutscene([[ Append("CR-1-16:[E|Serious] You appear to have me confused for someone else.[P] I am CR-1-16.[P] I began my existence approximately 135 minutes ago.[B][C]") ]])
        fnCutscene([[ Append("CR-1-16:[E|Serious] My records indicate Christine is the secondary designation of your lord unit.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] S-[P]so you're...[P] really a secrebot?[B][C]") ]])
        fnCutscene([[ Append("CR-1-16:[E|Serious] Yes.[P] I am not a golem.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Offended] And you just coincidentally look exactly like Christine would if she were a secrebot?[P] Including having that little accent and -[P] come on.[B][C]") ]])
        fnCutscene([[ Append("CR-1-16:[E|Serious] I apologize if I have made a mistake.[P] I am Secrebot CR-1-16.[P] Please allow me to serve you.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Offended] All right, smarty wheels.[P] Where's your runestone?[P] Use it to transform![B][C]") ]])
        fnCutscene([[ Append("CR-1-16:[E|Serious] Runestone?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Offended] You lost it![P] You need it, Christine![B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Surprised] Oh rusty lug nuts, what if you got reprogrammed![P] Uhhhh, what did we do last time?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] Oh, I know![B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] ...[B][C]") ]])
        fnCutscene([[ Append("55:[VOICE|Tiffany] Yes, you called.[P] I anticipated this.[P] Are you in Sector 254?[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Surprised] (S-[P]she knew!)[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Sad] (Wait of course she knew, she's super smart.[P] I am so out of my depth here!)[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Neutral] 55 I came to pick up a secrebot and it's Christine but she doesn't know she's Christine and I don't know where that runestone is [P][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Sad] And she says she's a secrebot and I can't get her back to normal and it's really scary help![P][C]") ]])
        fnCutscene([[ Append("55:[VOICE|Tiffany] Unit 499323, downcycle your core.[P] You have her there with you.[P] She doesn't have her PDU on her, either.[P] I've been trying to call it.[B][C]") ]])
        fnCutscene([[ Append("55:[VOICE|Tiffany] According to the network taps, her PDU is in the southwest part of that sector.[P] Find it and meet me in the repair bay.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Sad] What if I screw it up and we lose Christine and then I [P][C]") ]])
        fnCutscene([[ Append("55:[VOICE|Tiffany] Sophie.[P] Please.[P] I need your help, and so does she.[P] I am too far away right now and time may be of the essence.[P] Try to find that PDU and the runestone.[B][C]") ]])
        fnCutscene([[ Append("55:[VOICE|Tiffany] Be brave.[P] For Christine.[B][C]") ]])
        fnCutscene([[ Append("Sophie:[E|Sad] Y-[P]yeah...[P] I'll be brave...[P] come on, 'CR-1-16'![P] We're going to save you![B][C]") ]])
        fnCutscene([[ Append("CR-1-16:[E|Serious] I will help to the best of my ability!") ]])
        fnCutsceneBlocker()
    
        -- |[Fold]|
        fnAutoFoldParty()
        fnCutsceneBlocker()
        return
    end

-- |[ ====================================== Finale Scene ====================================== ]|
elseif(sObjectName == "Finale") then

    -- |[ ======================== Setup ========================= ]|
    -- |[Spawning]|
    --Night, as a secrebot.
    TA_Create("NightSecrebot")
        TA_SetProperty("Position", 2, 10)
        TA_SetProperty("Facing", gci_Face_South)
        TA_SetProperty("Clipping Flag", false)
        fnSetCharacterGraphics("Root/Images/Sprites/NightSecrebot/", false)
    DL_PopActiveObject()
    
    --Odess.
    TA_Create("Odess")
        TA_SetProperty("Position", 19, 2)
        TA_SetProperty("Facing", gci_Face_South)
        TA_SetProperty("Clipping Flag", false)
        fnSetCharacterGraphics("Root/Images/Sprites/GolemLordA/", false)
    DL_PopActiveObject()
    
    --Unit 8714.
    TA_Create("8714")
        TA_SetProperty("Position", 20, 2)
        TA_SetProperty("Facing", gci_Face_South)
        TA_SetProperty("Clipping Flag", false)
        fnSetCharacterGraphics("Root/Images/Sprites/Doll/", false)
    DL_PopActiveObject()
    
    --Security units.
    TA_Create("SecurityLord")
        TA_SetProperty("Position", 16, 4)
        TA_SetProperty("Facing", gci_Face_West)
        TA_SetProperty("Clipping Flag", false)
        fnSetCharacterGraphics("Root/Images/Sprites/GolemLordB/", false)
    DL_PopActiveObject()
    TA_Create("GolemA")
        TA_SetProperty("Position", 15, 4)
        TA_SetProperty("Facing", gci_Face_East)
        TA_SetProperty("Clipping Flag", false)
        fnSetCharacterGraphics("Root/Images/Sprites/GolemSlave/", false)
    DL_PopActiveObject()
    TA_Create("GolemB")
        TA_SetProperty("Position", 21, 7)
        TA_SetProperty("Facing", gci_Face_South)
        TA_SetProperty("Clipping Flag", false)
        fnSetCharacterGraphics("Root/Images/Sprites/GolemSlave/", false)
    DL_PopActiveObject()
    TA_Create("DroneA")
        TA_SetProperty("Position", 18, 7)
        TA_SetProperty("Facing", gci_Face_South)
        TA_SetProperty("Clipping Flag", false)
        fnSetCharacterGraphics("Root/Images/Sprites/LatexDrone/", false)
    DL_PopActiveObject()
    
    -- |[Collisions]|
    AL_SetProperty("Set Collision", 19, 3, 0, 0)
    AL_SetProperty("Set Collision", 20, 3, 0, 0)

    -- |[Position Party]|
    fnCutsceneCamera("Position", gcfCamera_Cutscene_Speed, 9.25, 10.50)
    fnCutsceneTeleport("Tiffany", 4.25, 10.50)
    fnCutsceneTeleport("Christine", 3.25, 10.50)
    fnCutsceneFadeOut()
    fnCutsceneBlocker()

    -- |[ ====================== Execution ======================= ]|
    -- |[Explosion]|
    fnCutscenePlaySound("World|Explosion")
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    
    -- |[Fade In]|
    fnCutsceneFadeIn(45)
    fnCutsceneWait(45)
    fnCutsceneBlocker()

    -- |[Movement]|
    fnCutsceneMove("Tiffany", 6.25, 10.50)
    fnCutsceneMove("Christine", 5.25, 10.50)
    fnCutsceneMove("NightSecrebot", 4.25, 10.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Christine:[VOICE|Christine] *55, the station is swarming with security units.[P] Let's find another way out.*[B][C]") ]])
    fnCutscene([[ Append("55:[VOICE|Tiffany] *Hold.[P] Standard procedure is a casualty report when a superior officer arrives.[P] I want to hear it.*") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneCamera("Position", gcfCamera_Cutscene_Speed, 19.25, 5.50)
    fnCutsceneBlocker()
    fnCutscenePlaySound("World|AutoDoorOpen")
    fnCutsceneLayerDisabled("DoorOpenLo", false)
    fnCutsceneLayerDisabled("DoorOpenHi", false)
    fnCutsceneLayerDisabled("DoorClosedLo", true)
    fnCutsceneLayerDisabled("DoorClosedHi", true)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneMove("8714", 20.25, 4.50)
    fnCutsceneFace("8714", -1, 0)
    fnCutsceneMove("Odess", 19.25, 4.50)
    fnCutsceneBlocker()
    fnCutsceneFace("SecurityLord", 1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("8714:[VOICE|Doll] Captain.[P] Report.[B][C]") ]])
    fnCutscene([[ Append("Lord:[VOICE|GolemLordB] No losses.[P] Enemy secrebots rounded up and arrested.[P] A handful of golems and drones were found cowering in their residences.[B][C]") ]])
    fnCutscene([[ Append("Lord:[VOICE|GolemLordB] Mission complete, we'll be sweeping the area.[P] Unfortunately, the enemy destroyed the conversion chamber with explosives.[P] There's not much left of it.[B][C]") ]])
    fnCutscene([[ Append("8714:[VOICE|Doll] The prime command units expected this, so it will not be a strike against us.[P] We need to redeploy, your team has two hours to search the area.[B][C]") ]])
    fnCutscene([[ Append("8714:[VOICE|Doll] I've personally apprehended the leader.[P] You're going in a very dark hole for a very long time, Odess.[B][C]") ]])
    fnCutscene([[ Append("Odess:[VOICE|GolemLord] You do not intimidate me, I serve a far higher power than you.[B][C]") ]])
    fnCutscene([[ Append("8714:[VOICE|Doll] We'll see about that.[B][C]") ]])
    fnCutscene([[ Append("Odess:[VOICE|GolemLord] I see you now, bearer.[P] I see what you were.[P] We will be as one flesh soon.[B][C]") ]])
    fnCutscene([[ Append("Lord:[VOICE|GolemLordB] She's gone mad, like the others.[B][C]") ]])
    fnCutscene([[ Append("8714:[VOICE|Doll] Doesn't matter.[P] Call the security tram and take her to Black Site P.[P] If there's anything useful on her drives, they'll tease it out of her.[B][C]") ]])
    fnCutscene([[ Append("8714:[VOICE|Doll] Reprogram the golems and drones, draft them into the security forces.[P] No witnesses.[P] Move out, lord.[B][C]") ]])
    fnCutscene([[ Append("Lord:[VOICE|GolemLordB] Yes, command unit.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Next Scene]|
    fnCutsceneFadeOut(125)
    fnCutsceneWait(145)
    fnCutsceneBlocker()
    fnCutscene([[ AL_BeginTransitionTo("RegulusCityC", "FORCEPOS:1.0x1.0x0") ]])
    fnCutsceneBlocker()
    

end
