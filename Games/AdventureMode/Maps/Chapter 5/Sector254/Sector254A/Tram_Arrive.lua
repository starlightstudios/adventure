-- |[Tram Arrival]|
--Called when the player is arriving via tram.
fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneBlocker()
	
--Set flags.
VM_SetVar("Root/Variables/Chapter5/Scenes/iIsTramHere", "N", 1.0)
VM_SetVar("Root/Variables/Chapter5/Scenes/sCurrentTramArea", "S", "Sector254A")

--Spawn the tram parts.
TA_Create("TramA")
	TA_SetProperty("Position", 29, 13)
	TA_SetProperty("Move Frame", gci_Face_West, 0, "Root/Images/Sprites/Tram/West|0")
	TA_SetProperty("Move Frame", gci_Face_West, 1, "Root/Images/Sprites/Tram/West|0")
	TA_SetProperty("Move Frame", gci_Face_West, 2, "Root/Images/Sprites/Tram/West|0")
	TA_SetProperty("Move Frame", gci_Face_West, 3, "Root/Images/Sprites/Tram/West|0")
	TA_SetProperty("Facing", gci_Face_West)
	TA_SetProperty("Set Ignore Clips", true)
DL_PopActiveObject()
TA_Create("TramB")
	TA_SetProperty("Position", 30, 13)
	TA_SetProperty("Move Frame", gci_Face_West, 0, "Root/Images/Sprites/Tram/North|0")
	TA_SetProperty("Move Frame", gci_Face_West, 1, "Root/Images/Sprites/Tram/North|0")
	TA_SetProperty("Move Frame", gci_Face_West, 2, "Root/Images/Sprites/Tram/North|0")
	TA_SetProperty("Move Frame", gci_Face_West, 3, "Root/Images/Sprites/Tram/North|0")
	TA_SetProperty("Facing", gci_Face_West)
	TA_SetProperty("Set Ignore Clips", true)
DL_PopActiveObject()
TA_Create("TramC")
	TA_SetProperty("Position", 31, 13)
	TA_SetProperty("Move Frame", gci_Face_West, 0, "Root/Images/Sprites/Tram/South|0")
	TA_SetProperty("Move Frame", gci_Face_West, 1, "Root/Images/Sprites/Tram/South|0")
	TA_SetProperty("Move Frame", gci_Face_West, 2, "Root/Images/Sprites/Tram/South|0")
	TA_SetProperty("Move Frame", gci_Face_West, 3, "Root/Images/Sprites/Tram/South|0")
	TA_SetProperty("Move Frame", gci_Face_East, 0, "Root/Images/Sprites/Tram/South|1")
	TA_SetProperty("Move Frame", gci_Face_East, 1, "Root/Images/Sprites/Tram/South|1")
	TA_SetProperty("Move Frame", gci_Face_East, 2, "Root/Images/Sprites/Tram/South|1")
	TA_SetProperty("Move Frame", gci_Face_East, 3, "Root/Images/Sprites/Tram/South|1")
	TA_SetProperty("Facing", gci_Face_West)
	TA_SetProperty("Set Ignore Clips", true)
    TA_SetProperty("Extended Activation Direction", gci_Face_North)
    TA_SetProperty("Activation Script", gsRoot .. "Chapter 5/Dialogue/Tram/Root.lua")
DL_PopActiveObject()
TA_Create("TramD")
	TA_SetProperty("Position", 32, 13)
	TA_SetProperty("Move Frame", gci_Face_West, 0, "Root/Images/Sprites/Tram/North|1")
	TA_SetProperty("Move Frame", gci_Face_West, 1, "Root/Images/Sprites/Tram/North|1")
	TA_SetProperty("Move Frame", gci_Face_West, 2, "Root/Images/Sprites/Tram/North|1")
	TA_SetProperty("Move Frame", gci_Face_West, 3, "Root/Images/Sprites/Tram/North|1")
	TA_SetProperty("Facing", gci_Face_West)
	TA_SetProperty("Set Ignore Clips", true)
DL_PopActiveObject()
TA_Create("TramE")
	TA_SetProperty("Position", 33, 13)
	TA_SetProperty("Move Frame", gci_Face_West, 0, "Root/Images/Sprites/Tram/North|2")
	TA_SetProperty("Move Frame", gci_Face_West, 1, "Root/Images/Sprites/Tram/North|2")
	TA_SetProperty("Move Frame", gci_Face_West, 2, "Root/Images/Sprites/Tram/North|2")
	TA_SetProperty("Move Frame", gci_Face_West, 3, "Root/Images/Sprites/Tram/North|2")
	TA_SetProperty("Facing", gci_Face_West)
	TA_SetProperty("Set Ignore Clips", true)
DL_PopActiveObject()
TA_Create("TramF")
	TA_SetProperty("Position", 34, 13)
	TA_SetProperty("Move Frame", gci_Face_West, 0, "Root/Images/Sprites/Tram/North|3")
	TA_SetProperty("Move Frame", gci_Face_West, 1, "Root/Images/Sprites/Tram/North|3")
	TA_SetProperty("Move Frame", gci_Face_West, 2, "Root/Images/Sprites/Tram/North|3")
	TA_SetProperty("Move Frame", gci_Face_West, 3, "Root/Images/Sprites/Tram/North|3")
	TA_SetProperty("Facing", gci_Face_West)
	TA_SetProperty("Set Ignore Clips", true)
DL_PopActiveObject()

--Wait a bit.
fnCutsceneWait(125)
fnCutsceneBlocker()

--Teleport Christine offscreen.
Cutscene_CreateEvent("CameraEvent", "Camera")
	CameraEvent_SetProperty("Max Move Speed", 5.0) --Default is 5.0
	CameraEvent_SetProperty("Focus Position", (15.75 * gciSizePerTile), (11.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneTeleport("Christine", -100.25, -100.50)
if(EM_Exists("Tiffany") == true) then
    fnCutsceneTeleport("Tiffany", -100.25, -100.50)
end
fnCutsceneBlocker()

--Fade in.
fnCutsceneWait(45)
fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
fnCutsceneBlocker()
		
--Move the trams over. They all forcible face west while moving.
fnCutsceneMoveFace("TramA", 13.75 + 6.0, 13.00, -1, 0, 2.50)
fnCutsceneMoveFace("TramB", 14.75 + 6.0, 13.00, -1, 0, 2.50)
fnCutsceneMoveFace("TramC", 15.75 + 6.0, 13.00, -1, 0, 2.50)
fnCutsceneMoveFace("TramD", 16.75 + 6.0, 13.00, -1, 0, 2.50)
fnCutsceneMoveFace("TramE", 17.75 + 6.0, 13.00, -1, 0, 2.50)
fnCutsceneMoveFace("TramF", 18.75 + 6.0, 13.00, -1, 0, 2.50)
fnCutsceneBlocker()
fnCutscene([[ AudioManager_PlaySound("World|TramStop") ]])
fnCutsceneMoveFace("TramA", 13.75 + 3.0, 13.00, -1, 0, 1.00)
fnCutsceneMoveFace("TramB", 14.75 + 3.0, 13.00, -1, 0, 1.00)
fnCutsceneMoveFace("TramC", 15.75 + 3.0, 13.00, -1, 0, 1.00)
fnCutsceneMoveFace("TramD", 16.75 + 3.0, 13.00, -1, 0, 1.00)
fnCutsceneMoveFace("TramE", 17.75 + 3.0, 13.00, -1, 0, 1.00)
fnCutsceneMoveFace("TramF", 18.75 + 3.0, 13.00, -1, 0, 1.00)
fnCutsceneBlocker()

--Trams slow down.
fnCutsceneMoveFace("TramA", 13.75 + 0.0, 13.00, -1, 0, 0.50)
fnCutsceneMoveFace("TramB", 14.75 + 0.0, 13.00, -1, 0, 0.50)
fnCutsceneMoveFace("TramC", 15.75 + 0.0, 13.00, -1, 0, 0.50)
fnCutsceneMoveFace("TramD", 16.75 + 0.0, 13.00, -1, 0, 0.50)
fnCutsceneMoveFace("TramE", 17.75 + 0.0, 13.00, -1, 0, 0.50)
fnCutsceneMoveFace("TramF", 18.75 + 0.0, 13.00, -1, 0, 0.50)
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(45)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ Append("Voice:[VOICE|Golem] Now arriving at Regulus City, Sector 254.[P] Please use caution when disembarking on the uneven grating.[P] Have an appallingly safe day.") ]])
fnCutsceneBlocker()

--Door opens.
fnCutscene([[ AudioManager_PlaySound("World|TramArrive") ]])
--fnCutsceneFace("TramC", 1, 0)
fnCutsceneWait(25)
fnCutsceneBlocker()

--Christine appears.
fnCutsceneFace("Christine", 0, -1)
fnCutsceneTeleport("Christine", 15.75, 11.50)
fnCutsceneBlocker()
if(EM_Exists("Tiffany") == true) then
    fnCutsceneFace("Tiffany", 0, 1)
    fnCutsceneTeleport("Tiffany", 15.75, 11.50)
end
fnCutsceneWait(25)
fnCutsceneBlocker()

--Move her north a bit.
fnCutsceneMove("Christine", 15.75, 10.50)
if(EM_Exists("Tiffany") == true) then
    fnCutsceneMove("Tiffany", 15.75, 10.50)
end
fnCutsceneBlocker()

--Fold the party.
fnCutscene([[ AL_SetProperty("Fold Party") ]])
fnCutsceneBlocker()
