-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ===================================== Chasing Night ====================================== ]|
if(sObjectName == "OhNoW") then
    
    -- |[Repeat Check]|
    local i254NightEscapeA = VM_GetVar("Root/Variables/Chapter5/Scenes/i254NightEscapeA", "N")
    if(i254NightEscapeA == 1.0) then return end
    
    -- |[Flag]|
    VM_SetVar("Root/Variables/Chapter5/Scenes/i254NightEscapeA", "N", 1.0)
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Night", "Human") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Hey, human![B][C]") ]])
    fnCutscene([[ Append("Human:[E|Panic] Eeeek!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneMove("Night", 44.25, 15.50, 2.50)
    fnCutsceneBlocker()
    fnCutsceneFace("Christine", 0, 1)
    fnCutsceneMove("Night", 42.25, 15.50, 2.50)
    fnCutsceneMove("Night", 42.25, 18.50, 2.50)
    fnCutsceneMove("Night", 38.25, 18.50, 2.50)
    fnCutsceneMove("Night", 38.25, 23.50, 2.50)
    fnCutsceneBlocker()
    fnCutsceneTeleport("Night", -1.25, -1.50)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Angry] Stupid human![P] I'm trying to help you![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] I guess it's my bad, she can't tell the difference between us robots.[P] Maybe if I can corner her long enough to get her to listen...") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "OhNoS") then
    
    -- |[Repeat Check]|
    local i254NightEscapeA = VM_GetVar("Root/Variables/Chapter5/Scenes/i254NightEscapeA", "N")
    if(i254NightEscapeA == 1.0) then return end
    
    -- |[Flag]|
    VM_SetVar("Root/Variables/Chapter5/Scenes/i254NightEscapeA", "N", 1.0)
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Night", "Human") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] Hey, human![B][C]") ]])
    fnCutscene([[ Append("Human:[E|Panic] Eeeek!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneMove("Night", 44.25, 8.50, 2.50)
    fnCutsceneBlocker()
    fnCutsceneFace("Christine", -1, 0)
    fnCutsceneMove("Night", 38.75,  8.50, 2.50)
    fnCutsceneMove("Night", 38.75, 23.50, 2.50)
    fnCutsceneMove("Night", 34.75, 23.50, 2.50)
    fnCutsceneBlocker()
    fnCutsceneTeleport("Night", -1.25, -1.50)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Angry] Stupid human![P] I'm trying to help you![B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] I guess it's my bad, she can't tell the difference between us robots.[P] Maybe if I can corner her long enough to get her to listen...") ]])
    fnCutsceneBlocker()

-- |[ ==================================== Night Captured! ===================================== ]|
elseif(sObjectName == "Eek") then
    
    -- |[Repeat Check]|
    local i254NightCaptured = VM_GetVar("Root/Variables/Chapter5/Scenes/i254NightCaptured", "N")
    if(i254NightCaptured == 1.0) then return end
    
    -- |[Activation Check]|
    local i254NightEscapeB = VM_GetVar("Root/Variables/Chapter5/Scenes/i254NightEscapeB", "N")
    if(i254NightEscapeB == 0.0) then return end
    
    -- |[Flag]|
    VM_SetVar("Root/Variables/Chapter5/Scenes/i254NightCaptured", "N", 1.0)
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Human:[VOICE|Night] Eeek![P] Let go of me![P] Help![P] Someone!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneMove("Christine", 32.25, 23.50, 2.50)
    fnCutsceneFace("Christine", -1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Christine:[VOICE|Christine] Damn it, I'm too late![P] Maybe I can...[P] pull rank on the drones or something.[P] She must be to the west of here, I gotta hurry!") ]])
    fnCutsceneBlocker()

-- |[ ====================================== TF Sequence ======================================= ]|
elseif(sObjectName == "TFSequence") then

    -- |[Variables]|
    local i254Transformed    = VM_GetVar("Root/Variables/Chapter5/Scenes/i254Transformed", "N")
    local i254NightCaptured  = VM_GetVar("Root/Variables/Chapter5/Scenes/i254NightCaptured", "N")
    local i254FixedChristine = VM_GetVar("Root/Variables/Chapter5/Scenes/i254FixedChristine", "N")

    -- |[ ================= Initial TF Sequence ================== ]|
    -- |[Repeat Check]|
    if(i254Transformed == 0.0 and i254NightCaptured == 1.0) then
    
        -- |[Flag]|
        VM_SetVar("Root/Variables/Chapter5/Scenes/i254Transformed", "N", 1.0)

        -- |[Movement]|
        fnCutsceneMove("Christine", 5.25, 13.50, 2.50)
        fnCutsceneFace("Christine", 1, 0)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        -- |[Dialogue]|
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] (Odess is already here, I knew it.[P] But why?[P] What does she need a human for?)") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        -- |[TF Sequence]|
        fnCutsceneFadeOut(45)
        fnCutsceneBlocker()
        fnCutsceneWait(65)
        fnCutsceneBlocker()
        
        --Call subscript. The scene should transition to the next room.
        LM_ExecuteScript(gsRoot .. "Chapter 5/Scenes/400 Volunteer/Secrebot Volunteer/Scene_Begin.lua")

    -- |[ ===================== Area Finale ====================== ]|
    elseif(i254FixedChristine == 1.0) then
    
        -- |[Movement]|
        fnCutsceneMergeParty()
        fnCutsceneBlocker()
        fnCutsceneMove("Christine", 6.25, 13.50)
        fnCutsceneMove("Tiffany", 6.25, 14.50)
        fnCutsceneBlocker()
        fnCutsceneMove("Christine", 6.25, 7.50)
        fnCutsceneMove("Tiffany", 6.25, 8.50)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        -- |[Dialogue]|
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Night", "Secrebot") ]])
        fnCutscene([[ Append("CR-1-15:[E|Secrebot] That's far enough, CR-1-16.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Blush] Uh, can you just call me Christine?[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] Does the secrebot designation excite you?[P] Nevermind.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] This unit CR-1-15 does not even have combat procedures downloaded, not that they'd help.[B][C]") ]])
        fnCutscene([[ Append("CR-1-15:[E|Secrebot] I am under orders to stop you.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] Uh huh, and your boss isn't even here?[P] She sent the cleaning staff to protect her assets?[P] Pathetic.[P] I bet she's ran off and hid someplace.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] 55, set the charges.[P] I think a highly-accurate gamma ray burst will take care of CR-1-15.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] You intend to rescue her?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] I want to rescue every robot I can.[P] If we can save the other secrebots, we will.[P] Now, let me borrow your diffractor.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        -- |[Movement]|
        fnCutsceneFace("Christine", 1, 1)
        fnCutsceneFace("Tiffany", 1, 1)
        fnCutscenePlaySound("World|LaserDistant")
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutscenePlaySound("World|LaserDistant")
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutscenePlaySound("World|LaserDistant")
        fnCutsceneWait(5)
        fnCutsceneBlocker()
        fnCutscenePlaySound("World|LaserDistant")
        fnCutsceneWait(5)
        fnCutsceneBlocker()
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("55:[VOICE|Tiffany] We're out of time![P] We'll need to exfiltrate via the transit tunnels![P] Do it quickly, Christine!") ]])
        fnCutsceneBlocker()
        
        -- |[Fade Out]|
        fnCutsceneFadeOut(25)
        fnCutsceneWait(35)
        fnCutsceneBlocker()
        
        -- |[Dialogue]|
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("55:[VOICE|Tiffany] Charges set.[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] PDU, recalibrate for a targeted gamma ray burst.[P] Fire![B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] [SOUND|Combat|Impact_Laser][P][P]PDU, scan.[B][C]") ]])
        fnCutscene([[ Append("PDU:[VOICE|Narrator] Organic compound within CR-1-15's cranial chassis is neutralized.[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] Great, we can pull it out later.[B][C]") ]])
        fnCutscene([[ Append("55:[VOICE|Tiffany] We need to leave.[B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] CR-1-15?[P] Wake up![B][C]") ]])
        fnCutscene([[ Append("Christine:[VOICE|Christine] Fine, I'll drag you along.[P] Let's go!") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    
        -- |[Next Scene]|
        fnCutscene([[ AL_BeginTransitionTo("Sector254A", "FORCEPOS:1.0x1.0x0") ]])
        fnCutsceneBlocker()
    
    end
end
