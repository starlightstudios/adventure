-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= Exits ========================================== ]|
if(sObjectName == "SampleExit") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("LevelName", "FORCEPOS:28.0x16.0x0")
    
-- |[ ====================================== Examinables ======================================= ]|
--All other objects query if it's Christine or Sophie in the lead.
else

    --Variables.
    local b254SophieLeading = fnIsSophieLeader()
    
    --Christine Leading:
    if(b254SophieLeading == false) then
        if(sObjectName == "TerminalA") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Leader] ('Remember to get out of the way if drones are transporting something!')") ]])
            fnCutsceneBlocker()
        elseif(sObjectName == "TerminalB") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Leader] ('Long Term Storage Area', looks like this terminal manages the inventory here.)") ]])
            fnCutsceneBlocker()
        elseif(sObjectName == "TerminalC") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Leader] ('Process complete, fluid depleted'.[P] Must be an old message.)") ]])
            fnCutsceneBlocker()
        elseif(sObjectName == "TerminalD") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Leader] ('Standby, system ready'.[P] Odd.[P] Is this terminal active, or in storage?)") ]])
            fnCutsceneBlocker()
        elseif(sObjectName == "FakeChests") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Leader] (So this is where they store all the unused chests![P] There must be mountains of them way in the back!)") ]])
            fnCutsceneBlocker()
        end
    else
        if(sObjectName == "TerminalA") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Sophie] (This terminal has some safety procedures on it.[P] Yeah, get out of the way of drones.[P] They're so dumb they will absolutely bump into you because they weren't ordered not to.)") ]])
            fnCutsceneBlocker()
        elseif(sObjectName == "TerminalB") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Sophie] (This terminal manages inventory for long-term storage.)") ]])
            fnCutsceneBlocker()
        elseif(sObjectName == "TerminalC") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Sophie] ('Process complete, fluid depleted'.[P] Weird, isn't this in storage?[P] Wait -[P] was this used on Christine!?)") ]])
            fnCutsceneBlocker()
        elseif(sObjectName == "TerminalD") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Sophie] ('Standby, system ready'.[P] I think someone left this terminal on.)") ]])
            fnCutsceneBlocker()
        elseif(sObjectName == "FakeChests") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Thought:[VOICE|Sophie] (Why do we store things in chests like this, anyway?[P] Oh well, these empty chests are in storage, waiting to be filled with some random crap.)") ]])
            fnCutsceneBlocker()
        end
    end

-- |[ ========================================== Other ========================================= ]|
-- |[ ========================================== Debug ========================================= ]|
--else
	--fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
