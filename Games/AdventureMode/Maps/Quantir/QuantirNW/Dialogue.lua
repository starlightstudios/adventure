-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    --Facing.
    TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[Werecat Vendor]|
    if(sActorName == "Werecat Vendor") then
        
        --In all cases, mark the combat intro dialogues as complete.
        VM_SetVar("Root/Variables/Chapter1/Counts/sMeiSeenPartirhuman", "S", "Complete")
        
        --Variables.
        local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
        local bIsFlorentinaPresent = AL_GetProperty("Is Character Following", "Florentina")
        local iTalkedWerecatVendor = VM_GetVar("Root/Variables/Chapter1/Scenes/iTalkedWerecatVendor", "N")
        
        --This is always a major dialogue.
        fnStandardMajorDialogue()
        
        --NPC takes slot 5.
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Werecat", "Neutral") ]])
        
        --Mei has not spoken to the vendor before:
        if(iTalkedWerecatVendor == 0.0) then
            
            --Florentina is not present:
            if(bIsFlorentinaPresent == false) then
            
                --Mei is a slime:
                if(sMeiForm == "Slime") then
                    fnCutscene([[ Append("Werecat: Hss![P] Away, filthy slime![P] Away![B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Offended] (Ack![P] What a jerk!)") ]])
                
                --Mei is a werecat:
                elseif(sMeiForm == "Werecat") then
            
                    --Flags.
                    VM_SetVar("Root/Variables/Chapter1/Scenes/iTalkedWerecatVendor", "N", 1.0)
                    
                    --Dialogue.
                    fnCutscene([[ Append("Werecat: Purrr, kinfang![P] Found some good stuff, very good.[P] Want to take a look?[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] What are you selling?[B][C]") ]])
                    fnCutscene([[ Append("Werecat: Mmmm, look![P] Small brick, but it makes light when you tap it.[P] See?[P] No magic![B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Surprise] ..![P] This is a cell phone![B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Happy] I forgot mine at work![P] Oh dear, I hope nobody tried to steal it.[B][C]") ]])
                    fnCutscene([[ Append("Werecat: Kinfang not amazed?[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] You don't even know what this is, do you?[P] Here, let me show you...[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] ...[P] See?[P] It takes photos![B][C]") ]])
                    fnCutscene([[ Append("Werecat: But not magic![P] How did it record my face?[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] It's got a camera right - [P][CLEAR]") ]])
                    fnCutscene([[ Append("Werecat: Hss![P] Kinfang![P] Brick is cursed![P] Smash it![B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] Hey, wait![P] I'll -[P] I'll dispose of it for you.[B][C]") ]])
                    fnCutscene([[ Append("Werecat: You would take a risk for me?[P] Brick is dangerous![B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] Only in the wrong hands.[P] I'll handle it.[B][C]") ]])
                    fnCutscene([[ Append("Werecat: Kinfang honors this one.[P] Thank you.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] (All right, let's see...)[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] (This phone belongs to someone named Sanya.[P] I guess she didn't put a lock screen on it.)[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] (She also doesn't take a lot of photos.[P] Seems she's from someplace in Europe.)[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] (There's not much else to find on here, unless I really feel like playing Enraged Avians.)[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Happy] (But it does mean that there have been other people from Earth here, and recently too!)[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] Kinfang, where did you find this brick?[B][C]") ]])
                    fnCutscene([[ Append("Werecat: Oh, you want to scavenge with me?[P] Mine![B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] Er, no.[P] The opposite.[P] I want to scavenge anywhere I will have no chance of finding one of these cursed bricks.[B][C]") ]])
                    fnCutscene([[ Append("Werecat: Ah![P] Clever, clever![P] Found it in the rockslide near the mountain south of here![B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] (Crud.[P] I guess that's out.[P] If I can't find a way back to Earth, maybe I should go find this Sanya person...)[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] Well, Kinfang, I'll go destroy this brick.[P] Thank you.[B][C]") ]])
                    fnCutscene([[ Append("Werecat: Eck, now I don't have anything else special to sell...") ]])
                    
                    --Topic.
                    WD_SetProperty("Unlock Topic", "Cell Phone", 1)
                
                --All other cases:
                else
            
                    --Flags.
                    VM_SetVar("Root/Variables/Chapter1/Scenes/iTalkedWerecatVendor", "N", 1.0)
                    
                    --Dialogue.
                    fnCutscene([[ Append("Werecat: Purrr, look, look![P] Found some good stuff, very good.[P] Want to take a look?[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] What are you selling?[B][C]") ]])
                    fnCutscene([[ Append("Werecat: Mmmm, look![P] Small brick, but it makes light when you tap it.[P] See?[P] No magic![B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Surprise] ..![P] This is a cell phone![B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Happy] I forgot mine at work![P] Oh dear, I hope nobody tried to steal it.[B][C]") ]])
                    fnCutscene([[ Append("Werecat: You're not amazed?[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] You don't even know what this is, do you?[P] Here, let me show you...[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] ...[P] See?[P] It takes photos![B][C]") ]])
                    fnCutscene([[ Append("Werecat: But not magic![P] How did it record my face?[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] It's got a camera right - [P][CLEAR]") ]])
                    fnCutscene([[ Append("Werecat: Hss![P] Filthy trap![P] Brick is cursed![P] Smash it![B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] Hey, wait![P] I'll -[P] I'll dispose of it for you.[B][C]") ]])
                    fnCutscene([[ Append("Werecat: You would take a risk for me?[P] Brick is dangerous![B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] Only in the wrong hands.[P] I'll handle it.[B][C]") ]])
                    fnCutscene([[ Append("Werecat: You honor this one.[P] Thank you.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] (All right, let's see...)[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] (This phone belongs to someone named Sanya.[P] I guess she didn't put a lock screen on it.)[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] (She also doesn't take a lot of photos.[P] Seems she's from someplace in Europe.)[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] (There's not much else to find on here, unless I really feel like playing Enraged Avians.)[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Happy] (But it does mean that there have been other people from Earth here, and recently too!)[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] Ms. Werecat, where did you find this brick?[B][C]") ]])
                    fnCutscene([[ Append("Werecat: Oh, you want to scavenge in my territory?[P] Mine![B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] Er, no.[P] The opposite.[P] I want to scavenge anywhere I will have no chance of finding one of these cursed bricks.[B][C]") ]])
                    fnCutscene([[ Append("Werecat: Ah![P] Clever, clever![P] Found it in the rockslide near the mountain south of here![B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] (Crud.[P] I guess that's out.[P] If I can't find a way back to Earth, maybe I should go find this Sanya person...)[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] Well, Werecat, I'll go destroy this brick.[P] Thank you.[B][C]") ]])
                    fnCutscene([[ Append("Werecat: Eck, now I don't have anything else to sell...") ]])
                    
                    --Topic.
                    WD_SetProperty("Unlock Topic", "Cell Phone", 1)
            
                end
            
            --Florentina is present:
            else
            
                --Flags.
                VM_SetVar("Root/Variables/Chapter1/Scenes/iTalkedWerecatVendor", "N", 1.0)
                
                --Dialogue.
                fnCutscene([[ Append("Werecat: Plant trader![P] Look![P] Good stuff, good stuff![B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] You know this cat, Florentina?[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Happy] Yeah, she's a scavenger.[P] Likes to pawn her crap off at my shop.[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Neutral] You better have something good this time, cat.[P] I don't want any more broken junk.[B][C]") ]])
                fnCutscene([[ Append("Werecat: Good stuff![P] Look, brick makes light when you tap it![P] See?[P] No magic![B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Happy] Well well well, that [P]*is*[P] pretty nifty.[P] That could fetch a bit for someone looking to rob a sorcerer.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Surprise] Wait a minute![P] This is a cell phone![B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Surprise] You know what this is?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Yeah, we have these on Earth, I -[P][EMOTION|Mei|Offended] hey![P] Rob a sorcerer?[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Blush] Err...[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Anyway, look.[P] You just tap here and here...[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Laugh] Say cheese![B][C]") ]])
                fnCutscene([[ Append("Werecat: ..![P] It recorded my face![P] Hss![P] Cursed brick, cursed![B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] It's just a camera...[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Happy] Well if it's cursed, you won't be charging us much for it now will you?[B][C]") ]])
                fnCutscene([[ Append("Werecat: Keep it![P] Take it far from me![B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Hold on a second.[P] Where did you get this thing?[B][C]") ]])
                fnCutscene([[ Append("Werecat: Rockslide far south of here, far south.[P] Found it there![B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Neutral] Hm, might have washed down from the glacier.[P] Unfortunately the path is blocked.[P] It'll be a while before anyone clears it.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] I'm just surprised it's still in working order.[P] Let's see who this belongs to...[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] ...[P] 'Sanya Pavletic'.[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Neutral] Friend of yours?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Well, no.[P] She's from Europe someplace.[P][EMOTION|Mei|Happy] But she was from Earth![P] There are others like me![B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Confused] If you want to go mountain climbing...[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Not right now.[P] But, if we can't find a way back to Earth, then finding out what happened to this Sanya person might be a good lead to follow.[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Happy] Well hey now, if there's any more of this Earth stuff you think I could sell...[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Ugh...") ]])
                    
                --Topic.
                WD_SetProperty("Unlock Topic", "Cell Phone", 1)
        
            end
        
        --Already talked to the werecat:
        else
            fnCutscene([[ Append("Werecat: Want to buy any of my other stuff, purr?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] May as well, let's see what you have.") ]])
            fnCutsceneBlocker()

            --Setup.
            local sBasePath = fnResolvePath()
            local sString = "AM_SetShopProperty(\"Show\", \"Werecat Junkseller\", \"" .. sBasePath .. "Shop Setup.lua\", \"Null\")"

            --Run the shop.
            fnCutscene(sString)
            fnCutsceneBlocker()
        end
    end
end
