-- |[ ============================= Trannadar Equipment Shop Setup ============================= ]|
--Sells some basic equipment.

-- |[Item Stock Listing]|
--Adamantite
AM_SetShopProperty("Add Item", "Adamantite Powder",           -1, -1)
AM_SetShopProperty("Add Item", "Wildflower's Knife",          -1, -1)
AM_SetShopProperty("Add Item", "Light Leather Vest",          -1, -1)
AM_SetShopProperty("Add Item", "Plated Leather Vest",         -1, -1)
AM_SetShopProperty("Add Item", "Healing Tincture",            -1, -1)
AM_SetShopProperty("Add Item", "Controversial Sugar Cookies", -1, -1)
AM_SetShopProperty("Add Item", "Yemite Gem",                  -1, -1)
AM_SetShopProperty("Add Item", "Rubose Gem",                  -1, -1)
AM_SetShopProperty("Add Item", "Qederphage Gem",              -1, -1)
