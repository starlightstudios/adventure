-- |[Combat Victory]|
--The party won!

-- |[Movement]|
--Wait a bit.
fnCutsceneWait(45)
fnCutsceneBlocker()

--Golem hits the floor.
fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "GolemA")
	ActorEvent_SetProperty("Special Frame", "Wounded")
DL_PopActiveObject()
fnCutsceneWait(45)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
fnCutscene([[ Append("Christine:[E|Sad] I'm sorry, there was nothing we could do...") ]])
fnCutsceneBlocker()

--Flags.
VM_SetVar("Root/Variables/Chapter5/Scenes/iFightGolemB", "N", 1.0)

--Fold the party.
fnCutscene([[ AL_SetProperty("Fold Party") ]])
fnCutsceneBlocker()
