-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Execution]|
--Exit
if(sObjectName == "ToEquinoxA") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusEquinoxA", "FORCEPOS:4.5x6.0x0")
	
--Exit
elseif(sObjectName == "ToEquinoxC") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusEquinoxC", "FORCEPOS:26.5x7.0x0")

-- |[Objects]|
elseif(sObjectName == "DollA") then
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("PDU: Scanning::[P] Unit #4992.[P] Status:: Retired.[B][C]") ]])
	fnCutscene([[ Append("PDU: Likely cause of retirement::[P] Unit was beaten from multiple sources and thrown through reinforced glass at extreme speed.[P] Massive system damage resulted in system shutdown.") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "GolemA") then
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("PDU: Scanning::[P] Unit unidentified.[P] Status:: Retired.[B][C]") ]])
	fnCutscene([[ Append("PDU: Likely cause of retirement::[P] Unit's head was hooked to two cables and electrocuted, wiping out all system functionality.[P] Authenticator chip unrecoverable, unit identity unknown.") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "GolemB") then
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("PDU: Scanning::[P] Unit unidentified.[P] Status:: Retired.[B][C]") ]])
	fnCutscene([[ Append("PDU: Likely cause of retirement::[P] Unit was beaten into system standby.[P] Some time later, the unit was hurled into exposed wall wiring and electrocuted.[P] Authenticator chip unrecoverable, unit identity unknown.") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "GolemC") then
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("PDU: Scanning::[P] Unit #205777.[P] Status:: Retired.[B][C]") ]])
	fnCutscene([[ Append("PDU: Likely cause of retirement::[P] High number of conventional tungsten slugs were launched into this unit's body at close range.[P] Massive system damage led to system offline.") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "GolemD") then
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("PDU: Scanning::[P] Unit #312728.[P] Status:: Retired.[B][C]") ]])
	fnCutscene([[ Append("PDU: Likely cause of retirement::[P] Unit launched a tungsten slug directly into its CPU from close range.[P] It is still clutching the makeshift firearm it used.") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "GolemBoss") then
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("PDU: Unit autopsy unnecessary.[P] Unit retired by Command Unit 2855 and Lord Unit 771852.[B][C]") ]])
	fnCutscene([[ Append("PDU: Cause of retirement was the usage of an Electrospear and Pulse Diffractor.[P] Chassis damage is as expected.") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "TerminalS") then
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Christine](This terminal contains a series of descriptions of plant subjects used for an experiment.[P] The last entry trails off mid-sentence.[P] It seems that First Drummer was enacted with no warning.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "TerminalSShot") then
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Christine](This terminal has been shot to pieces with conventional tungsten slugs.[P] It is unusable.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "TerminalN") then
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Christine](The terminal contains a list of...[P] retirements.[P] The unit was recording each victim and details of offlining them...)") ]])
	fnCutsceneBlocker()
	
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
