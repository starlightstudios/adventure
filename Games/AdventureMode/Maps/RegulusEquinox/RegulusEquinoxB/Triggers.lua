-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
--Sample.
if(sObjectName == "BattleUnitB") then

	--Variables.
	local iFightGolemB = VM_GetVar("Root/Variables/Chapter5/Scenes/iFightGolemB", "N")
	if(iFightGolemB == 0.0) then
		
		--Move Christine and 55 up.
		fnCutsceneMove("Christine", 7.25, 5.50)
		fnCutsceneMove("Tiffany", 7.25, 6.50)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
		fnCutscene([[ Append("Christine:[E|Sad] ...[P] Hello?[B][C]") ]])
		fnCutscene([[ Append("Golem: Non-compliant units.[P] Enact First Drummer Protocol.[P] Terminate.[P] Terminate.") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
        
        --Battle.
		fnCutscene([[ AdvCombat_SetProperty("Next Combat Music", "BossBattleTheme", 0.0000) ]])
        fnCutscene([[ AdvCombat_SetProperty("World Pulse", true) ]])
        fnCutscene([[ AdvCombat_SetProperty("Reinitialize") ]])
        fnCutscene([[ AdvCombat_SetProperty("Activate") ]])
        fnCutscene([[ AdvCombat_SetProperty("Unretreatable", true) ]])
        fnCutscene([[ AdvCombat_SetProperty("Victory Script", gsRoot .. "Maps/RegulusEquinox/RegulusEquinoxB/Combat_Victory.lua") ]])
        fnCutscene([[ AdvCombat_SetProperty("Defeat Script", gsStandardGameOver) ]])
        fnCutscene([[ LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 5/000 Scripted Enemies/Boss Equinox Golem Physical.lua") ]])
        fnCutsceneBlocker()
	end
end
