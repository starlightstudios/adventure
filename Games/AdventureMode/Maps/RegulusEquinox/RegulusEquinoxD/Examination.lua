-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Execution]|
--Exit
if(sObjectName == "ToEquinoxA") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusEquinoxA", "FORCEPOS:4.5x14.0x0")
	
--Exit
elseif(sObjectName == "ToEquinoxC") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusEquinoxC", "FORCEPOS:26.5x26.0x0")

-- |[Objects]|
elseif(sObjectName == "GolemA") then
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("PDU: Scanning::[P] Unit unidentified.[P] Status:: Retired.[B][C]") ]])
	fnCutscene([[ Append("PDU: Likely cause of retirement::[P] Unit was placed in a conversion chamber and converted to a golem.[P] It was then beaten to offline status for unknown reasons.[P] It seems likely that the unit was in conversion before First Drummer was enacted.") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "GolemB") then
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("PDU: Scanning::[P] Unit #301192.[P] Status:: Retired.[B][C]") ]])
	fnCutscene([[ Append("PDU: Likely cause of retirement::[P] Unit heard its killphrase twice and went into permanent shutdown.[P] Unit is permanently offline.") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "GolemC") then
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("PDU: Scanning::[P] Unit #89112.[P] Status:: Retired.[B][C]") ]])
	fnCutscene([[ Append("PDU: Cause of retirement is unknown.[P] The unit's body, save its cranial chassis, is unaccounted for.[P] The cranial chassis has had some of its recent ocular recordings uploaded to a nearby terminal.[B][C]") ]])
	fnCutscene([[ Append("PDU: The cranial chassis did not appear to suffer enough damage to cause the system offline, and was likely removed after retirement.") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "GolemD") then
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("PDU: Scanning::[P] Unit authenticator chip destroyed, unit cannot be identified.[P] Status:: Retired.[B][C]") ]])
	fnCutscene([[ Append("PDU: Likely cause of retirement::[P] Severe corrosive damage to many locations on the chassis.[P] Power core ruptured from one of the dozens of serious chemical burns.") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "GolemE") then
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("PDU: Scanning::[P] Unit authenticator chip destroyed, unit cannot be identified.[P] Status:: Retired.[B][C]") ]])
	fnCutscene([[ Append("PDU: Likely cause of retirement::[P] Corrosive destruction of entire chassis.[P] Exact cause of system offline is impossible to determine given the level of sustained damage.[P] The unit was essentially melted.") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "GolemBoss") then
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("PDU: Unit autopsy unnecessary.[P] Unit retired by Command Unit 2855 and Lord Unit 771852.[B][C]") ]])
	fnCutscene([[ Append("PDU: Cause of retirement was the usage of an Electrospear and Pulse Diffractor.[P] Chassis damage is as expected.[B][C]") ]])
	fnCutscene([[ Append("PDU: Traces of corrosive fluid are evident on the unit's chassis but were not the cause of retirement.") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "ConversionChamber") then
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Christine](A conversion chamber much like the one that perfected me.[P] Nanofluid is at 85 percent, suggesting some human was recently converted here.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "VideographTerminal") then
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Christine](It looks like a videograph of the events at the facility has been uploaded.[P] It shows golems destroying each other, then goes dark when the recording unit is struck with a pipe...)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "Academics") then

    --Variables:
    local iRuneUpgradeBriefing = VM_GetVar("Root/Variables/Chapter5/Scenes/iRuneUpgradeBriefing", "N")
    local iRuneUpgradeEquinox  = VM_GetVar("Root/Variables/Chapter5/Scenes/iRuneUpgradeEquinox", "N")
    
    --Not on the quest:
    if(iRuneUpgradeBriefing == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("[VOICE|Christine](An academic's terminal with readouts of experiments on elemental dissolution and density data.[P] Nothing important.)") ]])
        fnCutsceneBlocker()
    
    --On the quest, hasn't gotten the piece yet.
    elseif(iRuneUpgradeBriefing == 1.0 and iRuneUpgradeEquinox == 0.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iRuneUpgradeEquinox", "N", 1.0)
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("[VOICE|Christine](An academic's terminal with readouts of experiments on elemental dissolution and density data.[P] Wait, this has part of that academic decryption engine 55 needed!)[B][C]") ]])
        fnCutscene([[ Append("[VOICE|Christine][SOUND|World|TakeItem] (Got part of the academic decryption engine!)") ]])
        fnCutsceneBlocker()
    
    --Already got the piece.
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("[VOICE|Christine](An academic's terminal with readouts of experiments on elemental dissolution and density data.[P] It had some of the codes 55 needs to decrypt academic data about my runestone.)") ]])
        fnCutsceneBlocker()
    end

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
