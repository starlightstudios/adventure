-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Execution]|
--Exit
if(sObjectName == "ToEquinoxH") then
	
	--Variables.
	VM_SetVar("Root/Variables/Chapter5/Scenes/iSawAuthenticatorCount", "N", 1.0)
	local iFightGolemA = VM_GetVar("Root/Variables/Chapter5/Scenes/iFightGolemA", "N")
	local iFightGolemB = VM_GetVar("Root/Variables/Chapter5/Scenes/iFightGolemB", "N")
	local iFightGolemC = VM_GetVar("Root/Variables/Chapter5/Scenes/iFightGolemC", "N")
	local iFightGolemD = VM_GetVar("Root/Variables/Chapter5/Scenes/iFightGolemD", "N")
	local iSpokeWithDistressGolem = VM_GetVar("Root/Variables/Chapter5/Scenes/iSpokeWithDistressGolem", "N")
	
	--All checks passed.
	if(iFightGolemA == 1.0 and iFightGolemB == 1.0 and iFightGolemC == 1.0 and iFightGolemD == 1.0 and iSpokeWithDistressGolem == 2.0) then
		AudioManager_PlaySound("World|AutoDoorOpen")
		AL_BeginTransitionTo("RegulusEquinoxH", "FORCEPOS:7.0x9.0x0")
	
	--Access restricted.
	else
	
		local iCount = 0
		if(iFightGolemA == 0.0) then iCount = iCount + 1 end
		if(iFightGolemB == 0.0) then iCount = iCount + 1 end
		if(iFightGolemC == 0.0) then iCount = iCount + 1 end
		if(iFightGolemD == 0.0) then iCount = iCount + 1 end
		if(iSpokeWithDistressGolem < 2.0) then iCount = iCount + 1 end
		local sString = "WD_SetProperty(\"Append\", \"[SOUND|World|AutoDoorFail]Access denied. " .. iCount .. " authenticator chips remain.[P] Please terminate your fellow units.[P] Have a nice day.\")"
	
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene(sString)
		fnCutsceneBlocker()
	
	end
	
--Exit
elseif(sObjectName == "ToEquinoxF") then
	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("RegulusEquinoxF", "FORCEPOS:5.0x4.0x0")

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
