-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    --Facing.
    TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[GolemA]|
    if(sActorName == "GolemA") then
	
        --Variables.
        local iSawAuthenticatorCount = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawAuthenticatorCount", "N")
        local iSpokeWithDistressGolem = VM_GetVar("Root/Variables/Chapter5/Scenes/iSpokeWithDistressGolem", "N")
        local iCompletedEquinox = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedEquinox", "N")
        local iSawEquinoxReward = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawEquinoxReward", "N")
        
        --First time speaking with this golem, hasn't seen authenticator dialogue.
        if(iSawAuthenticatorCount == 0.0 and iSpokeWithDistressGolem == 0.0) then
            
            --Flags.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iSpokeWithDistressGolem", "N", 1.0)
            
            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
            fnCutscene([[ Append("Golem: Eek![P] No![P] Please don't hurt me![B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Calm down, we're not going to hurt you.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] That remains to be seen.[P] It might be a trick.[P] Don't trust her.[B][C]") ]])
            fnCutscene([[ Append("Golem: No, it's not a trick![P] I won't try anything, you've got to believe me![B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] It's all right, really.[P] We're friends.[P] We got a distress signal and came to investigate.[B][C]") ]])
            fnCutscene([[ Append("Golem: Oh, thank goodness![P] I sent that distress signal, but I got no response.[P] I thought maybe it had been blocked somehow.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Your signal was suppressed.[P] We're the only ones who received it.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] It seems that all the systems in Regulus City were programmed to ignore distress calls from Equinox.[B][C]") ]])
            fnCutscene([[ Append("Golem: So...[P] you're all the help that's coming?[P] No security teams?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] I'm afraid so.[B][C]") ]])
            fnCutscene([[ Append("Golem: Oh...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Can you tell us what happened here?[B][C]") ]])
            fnCutscene([[ Append("Golem: I -[P] I don't know for sure.[B][C]") ]])
            fnCutscene([[ Append("Golem: I was here delivering some spare parts for an experiment.[P] Then, a voice came over the speakers and said to initiate First Drummer protocols.[B][C]") ]])
            fnCutscene([[ Append("Golem: And then everyone went berserk![P] The units here just started attacking each other...[B][C]") ]])
            fnCutscene([[ Append("Golem: I ran and hid in here...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Do you know what First Drummer is?[B][C]") ]])
            fnCutscene([[ Append("Golem: No.[P] I'm sorry that I can't help you.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Unfortunate.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] We're -[P] security units.[P] Don't worry, we'll take care of everything.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Just stay here and remain hidden.[P] We'll come for you when the all clear is sounded.[B][C]") ]])
            fnCutscene([[ Append("Golem: Please be careful.[P] Don't trust any of the units out there.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] We didn't before, we certainly won't now.") ]])
            fnCutsceneBlocker()

        --Successive dialogues.
        elseif(iSawAuthenticatorCount == 0.0 and iSpokeWithDistressGolem == 1.0) then
            
            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
            fnCutscene([[ Append("Golem: I'll just stay here and try not to let anyone see me.[P] Good luck out there.") ]])
            fnCutsceneBlocker()
        
        --Saw authenticator, hasn't spoken with this golem previously.
        elseif(iSawAuthenticatorCount == 1.0 and iSpokeWithDistressGolem == 0.0) then
            
            --Flags.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iSpokeWithDistressGolem", "N", 2.0)
            
            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
            fnCutscene([[ Append("Golem: Eek![P] No![P] Please don't hurt me![B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Calm down, we're not going to hurt you.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] That remains to be seen.[P] It might be a trick.[P] Don't trust her.[B][C]") ]])
            fnCutscene([[ Append("Golem: No, it's not a trick![P] I won't try anything, you've got to believe me![B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] It's all right, really.[P] We're friends.[P] We got a distress signal and came to investigate.[B][C]") ]])
            fnCutscene([[ Append("Golem: Oh, thank goodness![P] I sent that distress signal, but I got no response.[P] I thought maybe it had been blocked somehow.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Your signal was suppressed.[P] We're the only ones who received it.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] It seems that all the systems in Regulus City were programmed to ignore distress calls from Equinox.[B][C]") ]])
            fnCutscene([[ Append("Golem: So...[P] you're all the help that's coming?[P] No security teams?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] I'm afraid so.[B][C]") ]])
            fnCutscene([[ Append("Golem: Oh...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Can you tell us what happened here?[B][C]") ]])
            fnCutscene([[ Append("Golem: I -[P] I don't know for sure.[B][C]") ]])
            fnCutscene([[ Append("Golem: I was here delivering some spare parts for an experiment.[P] Then, a voice came over the speakers and said to initiate First Drummer protocols.[B][C]") ]])
            fnCutscene([[ Append("Golem: And then everyone went berserk![P] The units here just started attacking each other...[B][C]") ]])
            fnCutscene([[ Append("Golem: I ran and hid in here...[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Do you know what First Drummer is?[B][C]") ]])
            fnCutscene([[ Append("Golem: No.[P] I'm sorry that I can't help you.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Fine then.[P] Disable your authenticator chip and we'll be on our way.[B][C]") ]])
            fnCutscene([[ Append("Golem: Come again?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] There's a lock on the door to the administrator's office here, and it replies to queries with how many authenticator chips are still active.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Ours are suppressed.[P] Turn yours off, and we might be able to get in.[B][C]") ]])
            fnCutscene([[ Append("Golem: Uh, can I even do that?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] No.[P] Fortunately, I disabled it with this PDU while you were talking.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Stay here.[P] We're going to go speak with the administrator about this.[B][C]") ]])
            fnCutscene([[ Append("Golem: Be careful.[P] She's a trained combat unit.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Good.[P] At least she'll present a challenge.") ]])
            fnCutsceneBlocker()
        
        --Saw authenticator, hasn't spoken with this golem previously.
        elseif(iSawAuthenticatorCount == 1.0 and iSpokeWithDistressGolem == 1.0) then
            
            --Flags.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iSpokeWithDistressGolem", "N", 2.0)
            
            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
            fnCutscene([[ Append("Golem: Have you had any luck finding out what's going on?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Yes, we have.[P] Disable your authenticator chip and we'll be on our way.[B][C]") ]])
            fnCutscene([[ Append("Golem: Come again?[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] There's a lock on the door to the administrator's office here, and it replies to queries with how many authenticator chips are still active.[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] Ours are suppressed.[P] Turn yours off, and we might be able to get in.[B][C]") ]])
            fnCutscene([[ Append("Golem: Uh, can I even do that?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] No.[P] Fortunately, I disabled it with this PDU while you were talking.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Stay here.[P] We're going to go speak with the administrator about this.[B][C]") ]])
            fnCutscene([[ Append("Golem: Be careful.[P] She's a trained combat unit.[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Good.[P] At least she'll present a challenge.") ]])
            fnCutsceneBlocker()
        
        --Saw authenticator, has spoken with this golem previously.
        elseif(iSawAuthenticatorCount == 1.0 and iSpokeWithDistressGolem == 2.0) then
            
            --Finale.
            if(iSawEquinoxReward == 0.0 and iCompletedEquinox == 1.0) then
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
                fnCutscene([[ Append("Golem: You're back![B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] We've dealt with the administrator.[P] The area is clear.[B][C]") ]])
                fnCutscene([[ Append("Golem: By clear, you mean...[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] One-hundred percent of offending units are retired.[P] The facility is quiet.[B][C]") ]])
                fnCutscene([[ Append("Golem: ...[P] Oh my...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Go back to Regulus City.[P] Don't tell anyone you were here.[P] The administrator didn't have any records of you that I could see.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] If they find out what you know, they'll reprogram you.[P] So don't let that happen.[B][C]") ]])
                fnCutscene([[ Append("Golem: What -[P] why?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] The administrators will pay for their crimes in time.[P] But you need to worry about yourself right now.[P] Stay safe.[B][C]") ]])
                fnCutscene([[ Append("Golem: I'll...[P] go get reassigned.[P] I'll stay quiet...[B][C]") ]])
                fnCutscene([[ Append("Golem: But if you need anything...[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] Your support will do.[P] We are planning an uprising at an unspecified time.[B][C]") ]])
                fnCutscene([[ Append("Golem: Oh geez, I'm not a soldier.[B][C]") ]])
                fnCutscene([[ Append("Golem: Here, take some of my work credits.[P] You can use them to buy equipment.[B][C]") ]])
                fnCutscene([[ Append("Golem: (Received 200 Work Credits)[B][C]") ]])
                fnCutscene([[ Append("Golem: Consider it war funding.[P] And I'll...[P] maybe try to round up some support.[P] We can't fight, but I know some golems who could maybe act as manufacturers...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] As I said, worry about your own safety for the moment.[P] Don't take any risks.[B][C]") ]])
                fnCutscene([[ Append("Golem: I won't...[P] And, thank you.[P] Thank you for saving my life...") ]])
                fnCutsceneBlocker()
                VM_SetVar("Root/Variables/Chapter5/Scenes/iSawEquinoxReward", "N", 1.0)
                local iWorkCreditsTotal = VM_GetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N")
                VM_SetVar("Root/Variables/Global/Christine/iWorkCreditsTotal", "N", iWorkCreditsTotal + 200)
            
            --Repeats.
            elseif(iSawEquinoxReward == 1.0) then
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
                fnCutscene([[ Append("Golem: I'll be heading out momentarily.[P] Thank you for saving me...") ]])
                fnCutsceneBlocker()
            
            --Hasn't completed Equinox yet.
            else
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
                fnCutscene([[ Append("Golem: Good luck.[P] I'll try not to attract any attention...") ]])
                fnCutsceneBlocker()
            end
        end
    end
end
