-- |[Constructor]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = "RegulusEquinoxC"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[Standard]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

	--Music.
	AL_SetProperty("Music", "EquinoxTheme")
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
    fnEquinoxMap(342, 0, 912.0 - (27*16) + 342 + 98, 525.0 - (7*16) + 32)
	
	-- |[Spawn NPCS Here]|
	--NPCs set to spawn here will be available for post-exec cutscenes.
	
	-- |[Lighting]|
	AL_SetProperty("Activate Lights")
	AL_SetProperty("Set Player Light No Drain", true)

-- |[Post-Exec]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	-- |[Spawn NPCs Here]|
	--If an entity needs to spawn after a post-exec cutscene, put them here.
	TA_CreateUsingPosition("GolemA", "GolemA")
		TA_SetProperty("Wipe Special Frames")
		TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/GolemLordA|Wounded")
		TA_SetProperty("Activation Script", "Nul")
	DL_PopActiveObject()
	
	--Unit has already been defeated.
	local iFightGolemC = VM_GetVar("Root/Variables/Chapter5/Scenes/iFightGolemC", "N")
	if(iFightGolemC == 1.0) then
		EM_PushEntity("GolemA")
			TA_SetProperty("Set Special Frame", "Wounded")
		DL_PopActiveObject()
	end
    
    -- |[Skillbook]|
    local iSkillbook2 = VM_GetVar("Root/Variables/Global/Christine/iSkillbook2", "N")
    if(iSkillbook2 == 1.0) then
		AL_SetProperty("Set Layer Disabled", "SkillbookIco", true)
    end

end
