-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
--Northern trigger.
if(sObjectName == "BattleUnitC") then

	--Variables.
	local iFightGolemC = VM_GetVar("Root/Variables/Chapter5/Scenes/iFightGolemC", "N")
	if(iFightGolemC == 0.0) then
		
		--Move Christine and 55 down.
		fnCutsceneMove("Christine", 8.25, 6.50)
		fnCutsceneMove("Tiffany", 8.25, 7.50)
		fnCutsceneBlocker()
		fnCutsceneFace("GolemA", 0, 1)
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] A lord unit![P] Are you in charge here?[B][C]") ]])
		fnCutscene([[ Append("Golem: Negative, a command unit has assumed control of the execution of the protocol.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] Then, what can you tell us about this First Drummer protocol?[B][C]") ]])
		fnCutscene([[ Append("Golem: Nothing.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Sad] Just -[P] nothing?[B][C]") ]])
		fnCutscene([[ Append("Golem: I'm afraid we do not budget for the dead.[P] I must retire you.[P] Nothing personal.") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
        
        --Battle.
		fnCutscene([[ AdvCombat_SetProperty("Next Combat Music", "BossBattleTheme", 0.0000) ]])
        fnCutscene([[ AdvCombat_SetProperty("World Pulse", true) ]])
        fnCutscene([[ AdvCombat_SetProperty("Reinitialize") ]])
        fnCutscene([[ AdvCombat_SetProperty("Activate") ]])
        fnCutscene([[ AdvCombat_SetProperty("Unretreatable", true) ]])
        fnCutscene([[ AdvCombat_SetProperty("Victory Script", gsRoot .. "Maps/RegulusEquinox/RegulusEquinoxC/Combat_Victory.lua") ]])
        fnCutscene([[ AdvCombat_SetProperty("Defeat Script", gsStandardGameOver) ]])
        fnCutscene([[ LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 5/000 Scripted Enemies/Boss Equinox Golem Lord.lua") ]])
        fnCutsceneBlocker()
	end

end
