-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Execution]|
--Exit
if(sObjectName == "ToEquinoxB") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusEquinoxB", "FORCEPOS:4.5x10.0x0")
	
--Exit
elseif(sObjectName == "ToEquinoxD") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusEquinoxD", "FORCEPOS:8.5x4.0x0")
	
--Exit
elseif(sObjectName == "ToEquinoxE") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusEquinoxE", "FORCEPOS:5.0x6.0x0")
	
--Exit
elseif(sObjectName == "ToEquinoxF") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusEquinoxF", "FORCEPOS:5.0x6.0x0")

-- |[Objects]|
elseif(sObjectName == "GolemA") then
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("PDU: Scanning::[P] Unit unidentified.[P] Status:: Retired.[B][C]") ]])
	fnCutscene([[ Append("PDU: Likely cause of retirement::[P] Unit received a massive electrical shock and went offline due to power core rupture.[P] Authenticator chip is unrecoverable, identity unknown.") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "GolemB") then
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("PDU: Scanning::[P] Unit unidentified.[P] Status:: Retired.[B][C]") ]])
	fnCutscene([[ Append("PDU: Likely cause of retirement::[P] Unit received a massive electrical shock and went offline due to power core rupture.[P] Authenticator chip is unrecoverable, identity unknown.[B][C]") ]])
	fnCutscene([[ Append("PDU: It seems likely, given the state of this room, that the unit was defragmenting when it was assaulted.") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "GolemBoss") then
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("PDU: Unit autopsy unnecessary.[P] Unit retired by Command Unit 2855 and Lord Unit 771852.[B][C]") ]])
	fnCutscene([[ Append("PDU: Cause of retirement was the usage of an Electrospear and Pulse Diffractor.[P] Chassis damage is as expected.") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "TerminalW") then
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Christine](Lift controls.[P] They're in lockdown at the moment.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "TerminalE") then
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("[VOICE|Christine](Lift controls.[P] They're in lockdown at the moment.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "ConsoleNoTarget") then
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Console: Error.[P] Unable to locate authenticator chip signal for unit assigned to this domicile.[P] Please notify the Chief of Equinox Security.") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "DefragTube") then
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("(A defragmentation tube.[P] No way would I ever deactivate cognitive routines in a place like this!)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "DefragTubeOpen") then
	
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("(A defragmentation tube.[P] Looks like its occupant is on the floor there...)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "Oilmaker") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("(At least the oilmaker is intact.[P] Even amidst a senseless slaughter, one can always go for a cuppa.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "Elevator") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("(The elevator's repair grid is blinking...[P] seems a cable was cut and the lift fell into the shaft.[P] With -[P] a unit inside...)") ]])
	fnCutsceneBlocker()

-- |[Skillbook]|
elseif(sObjectName == "Skillbook") then
    AL_SetProperty("Set Layer Disabled", "SkillbookIco", true)
	LM_ExecuteScript(gsChristineSkillbook, 2)

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
