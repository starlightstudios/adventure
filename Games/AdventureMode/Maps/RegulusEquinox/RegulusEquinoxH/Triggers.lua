-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
--Boss fight!
if(sObjectName == "BattleBoss") then

	--Variables.
	local iCompletedEquinox = VM_GetVar("Root/Variables/Chapter5/Scenes/iCompletedEquinox", "N")
    local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
	
	--Play the scene.
	if(iCompletedEquinox == 0.0) then
		
		--Party moves up.
		fnCutsceneMove("Christine", 6.75, 7.50)
		fnCutsceneMove("Tiffany", 7.75, 7.50)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
		fnCutscene([[ Append("Doll: Yes, yes, I'll be right with you.[P] I'm sorting through all the data you've provided.[P] There's so much of it![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Offended] Turn around.") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Turn to face.
		fnCutsceneFace("DollOverlord", 0, 1)
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
        if(sChristineForm == "Doll" or sChristineForm == "LatexDrone" or sChristineForm == "Golem") then
            fnCutscene([[ Append("Doll: So that explains the discrepancy![P] How clever of you to suppress your own authenticator chips.[B][C]") ]])
        else
            fnCutscene([[ Append("Doll: So that explains the discrepancy![P] You suppressed your own authenticator chip, and used a non-chipped mercenary.[B][C]") ]])
        end
        
		fnCutscene([[ Append("Doll: Still, thermal scopes indicate you have eliminated the remaining opposition.[P] Good work.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Angry] Good work?[P] Dozens of units are dead![B][C]") ]])
		fnCutscene([[ Append("Doll: Expendable, all of them.[P] In fact, we are expendable.[P] You, me, this whole facility, all in the name of science.[B][C]") ]])
		fnCutscene([[ Append("Doll: Is that Head of Security 2855?[P] Of all units I would expect you to agree with me.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Angry] Drop dead.[B][C]") ]])
		fnCutscene([[ Append("Doll: Such a pity.[P] Well, no matter.[P] I've already sent off my report to the administrators.[P] I expect they'll be repeating this experiment at a later date to gather more data.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Offended] Data?[P] All of this to gather data?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Offended] Just what is First Drummer?[B][C]") ]])
		fnCutscene([[ Append("Doll: An experimental protocol.[P] All units wish to be the First Drummer in the marching band, and they do whatever is necessary to achieve that goal.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Offended] Including murder...[B][C]") ]])
		fnCutscene([[ Append("Doll: It brings out the most powerful motivating force.[P] We remove all inhibitions, and the units do what comes naturally.[P] We will use this combat data to train the next wave of security units, and we can use versions of First Drummer to improve efficiency in sectors that lack it.[B][C]") ]])
		fnCutscene([[ Append("Doll: I'm so glad I was chosen to oversee this project![P] It truly has been an honor.[B][C]") ]])
		fnCutscene([[ Append("Doll: Now, if there's nothing else you require of me, I should return to Regulus City for reassignment.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Smug] What makes you think you're walking away from this?[B][C]") ]])
		fnCutscene([[ Append("Doll: ...[P] Come again, Unit 2855?[B][C]") ]])
		fnCutscene([[ Append("55:[E|Smug] I asked why you think you're above all of this.[B][C]") ]])
		fnCutscene([[ Append("Doll: Tch, if this is about quaint ethical boundaries, those have never been a consideration.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Angry] They are now![B][C]") ]])
        if(sChristineForm == "Doll" or sChristineForm == "LatexDrone" or sChristineForm == "Golem") then
            fnCutscene([[ Append("Doll: We are high-ranking units, and we use our logical processors.[P] Unit -[P] 771852?[P] Ah, you must be new.[B][C]") ]])
        else
            fnCutscene([[ Append("Doll: We are high-ranking units, and we use our logical processors.[P] I don't expect you to understand.[B][C]") ]])
        end
		fnCutscene([[ Append("Doll: If you retire me it will not undo all of the destruction wrought here.[B][C]") ]])
		fnCutscene([[ Append("Doll: I have violated no statutes.[P] I did exactly as the central administrators requested.[P] I fulfilled my purpose.[P] If you attempt to detain me, I will have to resist.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] ...[B][C]") ]])
		fnCutscene([[ Append("55:[E|Upset] What are you doing?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] She's right.[P] Fighting won't help anything, and it won't bring back all these fallen units...[B][C]") ]])
		fnCutscene([[ Append("55:[E|Upset] You can stand aside if you want, but I'm not going to.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Upset] Command Unit 8711, I am retiring you.[P] Please resist.[P] I wouldn't want my pulse diffractor to wear from disuse.[B][C]") ]])
		fnCutscene([[ Append("Doll: Unit 2855, this is highly irregular![B][C]") ]])
		fnCutscene([[ Append("55:[E|Upset] I'm not who I used to be.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Sad] *sigh* and so the cycle continues...") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
        
        --Battle.
		fnCutscene([[ AdvCombat_SetProperty("Next Combat Music", "BossBattleTheme", 0.0000) ]])
        fnCutscene([[ AdvCombat_SetProperty("World Pulse", true) ]])
        fnCutscene([[ AdvCombat_SetProperty("Reinitialize") ]])
        fnCutscene([[ AdvCombat_SetProperty("Activate") ]])
        fnCutscene([[ AdvCombat_SetProperty("Unretreatable", true) ]])
        fnCutscene([[ AdvCombat_SetProperty("Victory Script", gsRoot .. "Maps/RegulusEquinox/RegulusEquinoxH/Combat_Victory.lua") ]])
        fnCutscene([[ AdvCombat_SetProperty("Defeat Script", gsStandardGameOver) ]])
        fnCutscene([[ LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 5/000 Scripted Enemies/Boss Equinox Doll.lua") ]])
        fnCutsceneBlocker()
	end
end
