-- |[Combat Victory]|
--The party won!
        
--Achievement.
AM_SetPropertyJournal("Unlock Achievement", "FinishEquinox")

-- |[Movement]|
--Wait a bit.
fnCutsceneWait(45)
fnCutsceneBlocker()

--Golem hits the floor.
fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "DollOverlord")
	ActorEvent_SetProperty("Special Frame", "Wounded")
DL_PopActiveObject()
fnCutsceneWait(125)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Doll", "Neutral") ]])
fnCutscene([[ Append("Doll: System...[P] shutting...[P] down...[B][C]") ]])
fnCutscene([[ Append("55:[E|Upset] See you in hell...") ]])
fnCutsceneBlocker()

fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
fnCutscene([[ Append("Christine:[E|Sad] All this violence and for what?[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Sad] Now Equinox is just going to be a ghost facility, just like the Cryogenics Facility...[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Sad] Did retiring all the units within really solve anything?[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] I think it did.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] If there was any question before, there isn't now.[P] We need to deal with the administration.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] Any facility could be the next Equinox.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Sad] Is that really what you think?[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] No.[P] These units mean nothing to me.[B][C]") ]])
fnCutscene([[ Append("55:[E|Smirk] But I did enjoy retiring the malign units.[P] We should continue.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Sad] So none of this upsets you?[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] Emotions cloud judgement.[P] Our enemies have gained an advantage.[P] This combat data will serve to bolster their security forces.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] We need to strike quickly, not waste time pondering the morality of what happened here.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Sad] ...") ]])
fnCutsceneBlocker()

--Flags.
VM_SetVar("Root/Variables/Chapter5/Scenes/iCompletedEquinox", "N", 1.0)

--Fold the party.
fnCutsceneMove("Tiffany", 6.75, 7.50)
fnCutsceneBlocker()
fnCutscene([[ AL_SetProperty("Fold Party") ]])
fnCutsceneBlocker()
