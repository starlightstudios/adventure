-- |[Combat Victory]|
--The party won!

-- |[Movement]|
--Wait a bit.
fnCutsceneWait(45)
fnCutsceneBlocker()

--Golem hits the floor.
fnCutscene([[ AudioManager_PlaySound("World|Thump") ]])
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "GolemA")
	ActorEvent_SetProperty("Special Frame", "Wounded")
DL_PopActiveObject()
fnCutsceneWait(125)
fnCutsceneBlocker()

--Dialogue.
fnCutscene([[ WD_SetProperty("Show") ]])
fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "PDU", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
fnCutscene([[ Append("Christine:[E|Sad] I'm...[P] so sorry...[B][C]") ]])
fnCutscene([[ Append("55:[E|Upset] We did what we had to do.[P] There was no other way.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] This First Drummer Protocol must be responsible for the damage here.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Sad] Do we have to...[P] retire them?[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] They will attempt to do the same to us.[P] We must defend ourselves.[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] We should look around and try to find out what First Drummer is.[P] It's not in my memory banks.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] Mine neither.[P][E|PDU] PDU?[B][C]") ]])
fnCutscene([[ Append("PDU:[E|Question] No matches in local network.[P] Protocol appears to be active but is not registering on searches.[B][C]") ]])
fnCutscene([[ Append("Christine:[E|Neutral] How is that possible?[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] It's covering its tracks, possibly by hopping between nodes to evade scrutiny...[B][C]") ]])
fnCutscene([[ Append("55:[E|Neutral] I've downloaded her passcodes, so we can use the doors now.[P] Let's get going.[P] And be ready.") ]])
fnCutsceneBlocker()

--Flags.
VM_SetVar("Root/Variables/Chapter5/Scenes/iFightGolemA", "N", 1.0)

--Fold the party.
fnCutsceneMove("Tiffany", 11.25, 5.50)
fnCutsceneBlocker()
fnCutscene([[ AL_SetProperty("Fold Party") ]])
fnCutsceneBlocker()
