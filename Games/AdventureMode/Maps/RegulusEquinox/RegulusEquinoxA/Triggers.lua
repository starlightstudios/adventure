-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
if(sObjectName == "WarpActivate") then
    fnWarpUnlockHandler("Root/Variables/Chapter5/Campfires/iRegulusEquinoxA", 20.25, 10.50)
    
--Opening Cutscene.
elseif(sObjectName == "WelcomeToEquinox") then

	--Variables.
	local iSawEquinoxOpening = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawEquinoxOpening", "N")
	
	--Play the scene.
	if(iSawEquinoxOpening == 0.0) then
		
		--Flags.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSawEquinoxOpening", "N", 1.0)
		
		--Move Christine and 55 up.
		fnCutsceneMove("Christine", 12.25, 10.50)
		fnCutsceneMove("Tiffany", 13.25, 10.50)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
		fnCutscene([[ Append("Christine:[E|Scared] N-[P]no. P-[P]please no.[P] No![B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Scared] Not again![P] They're all dead, it's happening again![B][C]") ]])
		fnCutscene([[ Append("55:[E|Upset] Get hold of yourself, Christine.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Sad] It's just like it was at the Cryogenics Facility![P] They retired each other, look![B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] We don't know that for certain.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] There's a unit standing over there.[P] We should ask her what happened before we jump to conclusions.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Sad] A survivor?[P] Perhaps she's the one who sent the distress signal?[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] We won't find out unless we ask her.[P] Let's go.") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Fold party.
		fnCutsceneMove("Tiffany", 12.25, 10.50)
		fnCutsceneBlocker()
		fnCutscene([[ AL_SetProperty("Fold Party") ]])
		fnCutsceneBlocker()
	end

--Battle unit A.
elseif(sObjectName == "BattleUnitA") then

	--Variables.
	local iFightGolemA = VM_GetVar("Root/Variables/Chapter5/Scenes/iFightGolemA", "N")
	if(iFightGolemA == 0.0) then
		
		--Move Christine and 55 up.
		fnCutsceneMove("Christine", 11.25, 5.50)
		fnCutsceneMove("Tiffany", 12.25, 5.50)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
		fnCutscene([[ Append("Christine:[E|Sad] Um, excuse me?[P] Unit?[B][C]") ]])
		fnCutscene([[ Append("Golem: Error -[P] unit identity unknown.[P] Authenticator chip error.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Sad] Can you tell us what happened here?[P] Did you send the distress signal?[B][C]") ]])
		fnCutscene([[ Append("Golem: Engage First Drummer Protocol.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Sad] ...[P] What?[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] Never heard of it before.[B][C]") ]])
		fnCutscene([[ Append("Golem: Engage First Drummer Protocol.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Sad] Listen, we got a distress signal and we're here to help.[B][C]") ]])
		fnCutscene([[ Append("Golem: Terminate non-compliant units.") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
        
        --Battle.
		fnCutscene([[ AdvCombat_SetProperty("Next Combat Music", "BossBattleTheme", 0.0000) ]])
        fnCutscene([[ AdvCombat_SetProperty("World Pulse", true) ]])
        fnCutscene([[ AdvCombat_SetProperty("Reinitialize") ]])
        fnCutscene([[ AdvCombat_SetProperty("Activate") ]])
        fnCutscene([[ AdvCombat_SetProperty("Unretreatable", true) ]])
        fnCutscene([[ AdvCombat_SetProperty("Victory Script", gsRoot .. "Maps/RegulusEquinox/RegulusEquinoxA/Combat_Victory.lua") ]])
        fnCutscene([[ AdvCombat_SetProperty("Defeat Script", gsStandardGameOver) ]])
        fnCutscene([[ LM_ExecuteScript(gsRoot .. "Combat/Enemies/Chapter 5/000 Scripted Enemies/Boss Equinox Golem Timid.lua") ]])
        fnCutsceneBlocker()
	end

-- |[Load Special]|
--Only fires when loading the game.
elseif(sObjectName == "LoadSetter") then
    
    --Var set.
    local iIsLoaded = VM_GetVar("Root/Variables/Global/LoadSetting/iIsLoaded", "N")
    if(iIsLoaded == 1.0) then
        VM_SetVar("Root/Variables/Global/LoadSetting/iIsLoaded", "N", 0.0)
	
        --Unit has already been defeated.
        local iFightGolemA = VM_GetVar("Root/Variables/Chapter5/Scenes/iFightGolemA", "N")
        if(iFightGolemA == 1.0) then
            EM_PushEntity("GolemA")
                TA_SetProperty("Set Special Frame", "Wounded")
            DL_PopActiveObject()
        end
    end
end
