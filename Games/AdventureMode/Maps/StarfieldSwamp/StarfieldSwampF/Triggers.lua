-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
if(sObjectName == "WarpActivate") then
    fnWarpUnlockHandler("Root/Variables/Chapter1/Campfires/iStarfieldSwampF", 11.25, 19.50)
    
elseif(sObjectName == "RubberScene") then
    
    --Fade to black.
    fnCutscene([[ AL_SetProperty("Activate Fade", 0, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()

    --Flag.
    VM_SetVar("Root/Variables/Chapter1/Scenes/Rubber|iHasThralls", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter1/Scenes/Rubber|iIsRubberMode", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter1/Scenes/Rubber|iRubberedRochea", "N", 0.0)
    VM_SetVar("Root/Variables/Chapter1/Scenes/Rubber|iRubberedAdina", "N", 0.0)
    VM_SetVar("Root/Variables/Chapter1/Scenes/Rubber|iRubberedPolaris", "N", 0.0)

    --Position.
    fnCutsceneTeleport("Mei", 5.25, 27.50)
    
    --NPCs who join the party.
    TA_Create("Rubberraune")
        local iAlrauneID = RO_GetID()
        TA_SetProperty("Position", 10, 26)
        TA_SetProperty("Facing", gci_Face_West)
        fnSetCharacterGraphics("Root/Images/Sprites/AlrauneR/", false)
    DL_PopActiveObject()
    TA_Create("Rubberbee")
        local iBeeID = RO_GetID()
        TA_SetProperty("Position", 10, 27)
        TA_SetProperty("Facing", gci_Face_West)
        fnSetCharacterGraphics("Root/Images/Sprites/BeeGirlR/", false)
        TA_SetProperty("Auto Animates", true)
        TA_SetProperty("Y Oscillates", true)
    DL_PopActiveObject()
    giFollowersTotal = 2
    gsaFollowerNames = {"Rubberraune", "Rubberbee"}
    giaFollowerIDs = {iAlrauneID, iBeeID}
    AL_SetProperty("Follow Actor ID", iAlrauneID)
    AL_SetProperty("Follow Actor ID", iBeeID)
    
    --Reset Mei to Squeaky Thrall. This will give her the summon ability.
    LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Rubber.lua")
    
    --Switch to night.
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 1, gcf_Night_R, gcf_Night_G, gcf_Night_B, gcf_Night_A, true) ]])
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Mei", 9.25, 27.50)
    fnCutsceneBlocker()
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Thralls...[P] I can hear them think...[P] I can tell them what to think...)[B][C]") ]])
    fnCutscene([[ Append("Alraune:[VOICE|Alraune] (We follow...[P] spread...[P] we follow...)[B][C]") ]])
    fnCutscene([[ Append("Bee:[VOICE|Bee] (Spread...[P] protect host...[P] spread...)[B][C]") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (More thralls...[P] I can think more clearly as I create more thralls...[P] I need to spread and make more...)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Follow Mei.
    fnAutoFoldParty()
    fnCutsceneBlocker()
    
--Trigger to despawn certain enemy groups after a rest.
elseif(sObjectName == "EmergencyDespawn") then
    local iIsRubberMode = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iIsRubberMode", "N")
    if(iIsRubberMode == 1.0) then
        fnRubberRemover()
    end

--Can't go back.
elseif(sObjectName == "No Going Back") then
    
    --Case check:
    local iIsRubberMode = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iIsRubberMode", "N")
    if(iIsRubberMode == 0.0) then return end
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The trading post...[P] I need to spread there...)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    Cutscene_CreateEvent("ActorEvent", "Actor")
        ActorEvent_SetProperty("Subject Name", "Mei")
        ActorEvent_SetProperty("Move Amount", (1.00 * gciSizePerTile), (0.00 * gciSizePerTile))
    DL_PopActiveObject()
    fnCutsceneBlocker()
    fnAutoFoldParty()
    fnCutsceneBlocker()
    
end
