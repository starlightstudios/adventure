-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= Exits ========================================== ]|
if(sObjectName == "SampleExit") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("LevelName", "FORCEPOS:28.0x16.0x0")
    
-- |[ ====================================== Examinables ======================================= ]|
elseif(sObjectName == "Whirlpool") then
    fnExecWaterJumpToMap(5+gcfWhirl2x2OffX, 17+gcfWhirl2x2OffY, "StarfieldSwampB", 1, 1)
    
elseif(sObjectName == "Wood") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Despite being underground and the air being extremely humid, there's firewood here and it's quite dry.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Coffins") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Wooden coffins sealed with glue.[P] There are wax notes sticking out of the side but I don't want to risk getting close enough to read them.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Chest") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A box with some waxy papers and charcoal.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Pot") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The cooking pot has a thick rind of something slimy, like glue or wax.)") ]])
    fnCutsceneBlocker()

-- |[ ========================================== Other ========================================= ]|
-- |[ ========================================== Debug ========================================= ]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
