-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
if(sObjectName == "Arrival") then
    fnExecWaterEmerge(5+gcfWhirl2x2OffX, 17+gcfWhirl2x2OffY, 7.25, 18.00)
    
elseif(sObjectName == "Place") then
    
    -- |[Repeat Check]|
    local iStarfieldCaveScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iStarfieldCaveScene", "N")
    if(iStarfieldCaveScene == 1.0) then return end
    
    -- |[Flag]|
    VM_SetVar("Root/Variables/Chapter1/Scenes/iStarfieldCaveScene", "N", 1.0)
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("Mei:[E|Surprise] Wow![P] A huge air pocket under the swamp![P] Did you know about this?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] I had no idea.[P] Given how long it took to swim here, I'm shocked there's a house in here.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] Maybe there's another way in.[P] Should we see if anyone's home?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnAutoFoldParty()
    fnCutsceneBlocker()
    
elseif(sObjectName == "Coffins") then
    
    -- |[Repeat Check]|
    local iStarfieldCoffinScene = VM_GetVar("Root/Variables/Chapter1/Scenes/iStarfieldCoffinScene", "N")
    if(iStarfieldCoffinScene == 1.0) then return end
    
    -- |[Flag]|
    VM_SetVar("Root/Variables/Chapter1/Scenes/iStarfieldCoffinScene", "N", 1.0)
    
    -- |[Movement]|
    fnCutsceneMergeParty()
    fnCutsceneBlocker()
    fnCutsceneMove("Mei", 18.25, 13.50)
    fnCutsceneFace("Mei", 0, -1)
    fnCutsceneMove("Florentina", 18.25, 13.50)
    fnCutsceneMove("Florentina", 19.25, 13.50)
    fnCutsceneFace("Florentina", -1, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("Mei:[E|Sad] F-[P]Florentina... I...[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] Coffins.[P] I don't smell anything but I know they're full.[P] They're sealed with epoxy. [B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] There's a note sticking out of this one...[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] Don't touch a damn thing![P] We're leaving.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Angry] I don't know if we have a graverobber or something but we can't just let them get away with it![B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] Shut it Mei, sound carries in a cave like this.[P] You don't know what you're talking about.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] Coffins aren't a tradition here in Trannadar, we burn the dead.[P] The tradition is over 200 years old.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Sad] I-[P]I'm sorry.[P] Florentina, how do you know that?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] I went to school a lot when I was younger.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] Point is, this is the work of a foreigner.[P] Not from around here, and most likely, someone who isn't keeping these bodies for fun.[P] This is a shipment.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] I know everyone in Trannadar and everyone who travels through it.[P] I know every merchant, mercenary, and bounty hunter.[P] I don't know of anyone who has a hideout like this.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] I've heard of these coffins, everyone on the west coast has.[P] They're sealed, they wash up near Jeffespeir and whoever is supposed to find them, finds them.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] Someone makes them, seals them, notorizes them.[P] Someone is paid to do it, and what little authority there is makes no effort to find out who.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Sad] Do you...[P] know?[P] Know who it is[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] No.[P] I'd never hire a vicious bastard who'd sign a blood favour.[P] I'd sooner pluck out my other eye.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] What's a blood favour?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] A contract you sign with a piece of your soul.[P] Both parties give a piece of their soul, and if the contract is broken, you lose it.[P] Real sick stuff, Mei.[P] Never sign one, ever.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] These bodies must be part of such a thing.[P] Mei, we need to leave.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Angry] Or we wait here and shiv the son of a bitch when he comes to pick up his cargo![B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] You do [P]*not*[P] want to cross paths with someone like this, Mei.[P] Even if you do, I don't.[P] You don't hire this kind of person for a grudge, you hire him to kill someone nobody else can.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] The kinds of people who have armies protecting them.[P] Wizards with wards, or people who aren't used to dying.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] If you put your soul in the contract, you shrug off everything between you and the goal.[P] These psychos don't bleed to death, they don't sleep, or even eat.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Sad] Why would you do that?[P] Why risk your own soul for a job to kill someone?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] Hell if I know.[P] The rush of power you get from murdering someone?[P] The riches for the payoff?[P] Or maybe they're the kind of person who would do away with their soul if they could.[P] I don't know.[P] All I know is, these freaks are hard to stop.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] And the absolute last thing I want this guy to do is come back here and find out someone knows about his hideout because then his hitlist will have two new names on it.[P] Do you get me?[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Angry] ...[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Offended] I hate that you're right.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] I hope I'm not.[P] But I've heard of these coffins, and heard of the sorts of people who they're for.[P] I don't want anything to do with this guy.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Offended] Fine.[P] Let's get out of here...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnAutoFoldParty()
    fnCutsceneBlocker()
end
