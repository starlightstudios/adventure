-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= Exits ========================================== ]|
if(sObjectName == "SampleExit") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("LevelName", "FORCEPOS:28.0x16.0x0")
    
-- |[ ====================================== Examinables ======================================= ]|
elseif(sObjectName == "Gate") then

    --Variables.
    local iMetPolaris            = VM_GetVar("Root/Variables/Chapter1/Polaris/iMetPolaris", "N")
    local bIsFlorentinaPresent   = AL_GetProperty("Is Character Following", "Florentina")
    local iHasJob_TreasureHunter = VM_GetVar("Root/Variables/Global/Florentina/iHasJob_TreasureHunter", "N")

    --Florentina is present:
    if(bIsFlorentinaPresent == true) then
        
        --Has Treasure Hunter:
        if(iHasJob_TreasureHunter == 1.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
            fnCutscene([[ Append("Mei:[E|Neutral] All right, work your magic.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] I'll have it done before you know it.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] (To open the lock, equip the Field Ability 'Pick Lock' and press the matching button while facing the door.)[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] (You can see your field abilities on the left side of the screen.[P] The default key to open the menu is 'C'.)") ]])
            fnCutsceneBlocker()
        
        else
            VM_SetVar("Root/Variables/Chapter1/Polaris/iLockpickQuestState", "N", 1.0)
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
            fnCutscene([[ Append("Mei:[E|Neutral] Hm, the gate is locked.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] Looks like a simple latch, maybe it fell into place.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Confused] Or some rotten bastard locked it on purpose to screw with travellers.[P] I don't know.[B][C]") ]])
            
            local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
            if(sMeiForm == "Ghost") then
                fnCutscene([[ Append("Mei:[E|Laugh] Maybe I can squeeze through the cracks with my ghostly body?[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Neutral] Great idea, Mei![P] And once you're on the other side, you can just leave me to find my way through.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Oh, right, yeah.[P] Probably need to find a way around for you.[B][C]") ]])
            end
            
            fnCutscene([[ Append("Mei:[E|Neutral] Can you get the lock open?[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] If I had the tools, sure, I could pick the lock.[P] But I don't have anything that'd work.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] What kind of tools do you need?[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] A flatknife to hold the cylinder in place, and a pointed pick to rake the tumblers with, or press them if I can't rake them.[B][C]") ]])

            if(iMetPolaris == 1.0) then
                fnCutscene([[ Append("Mei:[E|Smirk] I bet Polaris would have that kind of thing![P] She had a belt full of tools on her![B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Neutral] Good thinking.[P] Let's go pay her a visit.") ]])
            else
                fnCutscene([[ Append("Mei:[E|Neutral] Any idea where we could get those?[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Neutral] Not at the trading post, unfortunately.[P] Blythe would kick my bottom square out if he found out I had those.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] We can ask around.[P] I'm sure someone will loan us some tools.") ]])
            end
            fnCutsceneBlocker()
            WD_SetProperty("Unlock Topic", "Polaris_LockedRuins", 1)
        
        end

    --Florentina not present:
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (Locked.[P] It might be lockpicked open...[P] Maybe I should ask someone for help?)") ]])
        fnCutsceneBlocker()
        WD_SetProperty("Unlock Topic", "Polaris_LockedRuins", 1)
    end

-- |[ ========================================== Other ========================================= ]|
-- |[Skillbook]|
elseif(sObjectName == "SkillbookCase") then
    AL_SetProperty("Set Layer Disabled", "SkillbookIco", true)
	LM_ExecuteScript(gsFlorentinaSkillbook, 3)

-- |[ ========================================== Debug ========================================= ]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
