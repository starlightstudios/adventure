-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
if(sObjectName == "ThePlace") then
    
    --Case check:
    local iIsRubberMode = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iIsRubberMode", "N")
    if(iIsRubberMode == 1.0) then return end

    --Repeat check.
    local iSawStForas = VM_GetVar("Root/Variables/Chapter1/Sharelock/iSawStForas", "N")
    if(iSawStForas == 1.0) then return end
    VM_SetVar("Root/Variables/Chapter1/Sharelock/iSawStForas", "N", 1.0)
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("Mei:[E|Neutral] What is this place, Florentina?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] Saint Fora's Convent.[P] I think.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] This is all second-hand information.[P] People come through the trading post and talk about it.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] Old ruin from centuries ago, built very solid.[P] It's been stripped of goodies, though sometimes a person sets up in the place.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] Sets up?[P] Like, builds a shop?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] Or just uses it as a house.[P] The walls are solid and the doors have probably been replaced a few times.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] Thing is, it feels off.[P] Like something is wrong, and there's more to it.[P] But the treasure hunters I've met either say they didn't find anything...[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] Or?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Happy] They don't come back.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Offended] And you are smiling about this?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Happy] If they don't come back, it means they got eaten or whatever, and the treasure is still there, for us to take.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Offended] But what about all the people who died in there, Florentina?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] Occupational hazard.[P] If you want to live a life free of danger, maybe don't pick treasure hunter as a career.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Offended] Add 'Grave Robber' to that list, because we're doing that now, apparently.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] Hey, you're right![P] Their stuff will be left behind, too![P] Maybe they'll have some magic kit![B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Offended] Let's just get going...") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "NoSpread") then
    
    --Case check:
    local iIsRubberMode = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iIsRubberMode", "N")
    if(iIsRubberMode == 0.0) then return end
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (There is no one here to spread to...)") ]])
    fnCutsceneBlocker()
    Cutscene_CreateEvent("ActorEvent", "Actor")
        ActorEvent_SetProperty("Subject Name", "Mei")
        ActorEvent_SetProperty("Move Amount", (-1.00 * gciSizePerTile), (0.00 * gciSizePerTile))
    DL_PopActiveObject()
    fnAutoFoldParty()
    fnCutsceneBlocker()
end
