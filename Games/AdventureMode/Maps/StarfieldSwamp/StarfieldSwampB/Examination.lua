-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= Exits ========================================== ]|
if(sObjectName == "SampleExit") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("LevelName", "FORCEPOS:28.0x16.0x0")
    
-- |[ ======================================= Whirlpools ======================================= ]|
--Connected whirlpools.
elseif(sObjectName == "WhirlpoolA") then

    --Can't use in rubber mode or if wisphag TF is not found.
    local iIsRubberMode = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iIsRubberMode", "N")
    local iHasWisphagForm = VM_GetVar("Root/Variables/Global/Mei/iHasWisphagForm", "N")
    if(iIsRubberMode == 1.0 or iHasWisphagForm == 0.0) then return end

    --Jump in and move.
    fnExecWaterJumpToPos(23.0 + gcfWhirl2x2OffX, 25.0 + gcfWhirl2x2OffY, 4.0 + gcfWhirl2x2OffX, 11.0 + gcfWhirl2x2OffY, 6.25, 12.0)
    
elseif(sObjectName == "WhirlpoolB") then
    fnExecWaterJumpToPos(4.0 + gcfWhirl2x2OffX, 11.0 + gcfWhirl2x2OffY, 23.0 + gcfWhirl2x2OffX, 25.0 + gcfWhirl2x2OffY, 25.25, 26.0)

--Leads to another room.
elseif(sObjectName == "WhirlpoolC") then

    --Can't use in rubber mode or if wisphag TF is not found.
    local iIsRubberMode = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iIsRubberMode", "N")
    local iHasWisphagForm = VM_GetVar("Root/Variables/Global/Mei/iHasWisphagForm", "N")
    if(iIsRubberMode == 1.0 or iHasWisphagForm == 0.0) then return end
    
    --Jump in, new map.
    fnExecWaterJumpToMap(87+gcfWhirl2x2OffX, 49+gcfWhirl2x2OffY, "WisphagSwampC", 1, 1)

-- |[ ========================================== Other ========================================= ]|
-- |[ ========================================== Debug ========================================= ]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
