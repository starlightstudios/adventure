-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
if(sObjectName == "Arrival") then
    fnExecWaterEmerge(87+gcfWhirl2x2OffX, 49+gcfWhirl2x2OffY, 87.75, 48.50)

-- |[ ================================= Whirlpool Explanation ================================== ]|
--Only happens on NC+ if Mei has Wisphag form.
elseif(sObjectName == "WhirlpoolKnowledge") then
    
    -- |[Activation Check]|
    local iHasWisphagForm   = VM_GetVar("Root/Variables/Global/Mei/iHasWisphagForm", "N")
    local iToldOfWhirlpools = VM_GetVar("Root/Variables/Chapter1/ScenesWisphag/iToldOfWhirlpools", "N")
    if(iHasWisphagForm == 0.0 or iToldOfWhirlpools == 1.0) then return end
    
    -- |[Flag]|
    VM_SetVar("Root/Variables/Chapter1/ScenesWisphag/iToldOfWhirlpools", "N", 1.0)
    
    -- |[Scene]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("Mei:[E|Happy] Oh![P] Florentina![P] If we see a whirlpool, I can guide us safely through it thanks to my wisphag form![B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Surprise] Hey![P] That's really stupid and I don't want to do that![B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Offended] Oh, I see.[P] Adventuresome, fearless, tough-as-nails Florentina doesn't wanna get her feets wet.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Blush] That's cheating, you can't use subtle flattery to manipulate me.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Laugh] I can, I am, I will.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Smirk] I can sense the currents, the souls of the swamp will guide me.[P] We'll be fine.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] There is no way my insurance policies will cover this, you know.[P] You're on the hook if I die.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Happy] I want a lavish funeral.[P] I want a cake taller than two werecats standing on each other's shoulders, cherry flavoured, and an open bar[P] Get *everyone* tanked.[P] And I want you to make sure everyone comes.[P] My friends, my enemies, everyone.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Smirk] If that's what it takes, deal.") ]])
    fnCutsceneBlocker()
end
