-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
--Big scene: Rubber vs Alraunes!
if(sObjectName == "AlrauneScene") then
    
    --Repeat check.
    local iIsRubberMode   = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iIsRubberMode", "N")
    local iRubberedRochea = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iRubberedRochea", "N")
    if(iIsRubberMode == 0.0 or iRubberedRochea == 1.0) then return end
    
    --Spawn NPCs.
    local sDialoguePath = fnResolvePath() .. "Dialogue.lua"
    TA_Create("Rochea")
        TA_SetProperty("Position", 22, 14)
        fnSetCharacterGraphics("Root/Images/Sprites/Rochea/", false)
        TA_SetProperty("Activation Script", sDialoguePath)
        TA_SetProperty("Facing", gci_Face_West)
    DL_PopActiveObject()
    TA_Create("AlrauneA")
        TA_SetProperty("Position", 23, 14)
        fnSetCharacterGraphics("Root/Images/Sprites/Alraune/", false)
        TA_SetProperty("Activation Script", sDialoguePath)
        TA_SetProperty("Facing", gci_Face_West)
    DL_PopActiveObject()
    TA_Create("AlrauneB")
        TA_SetProperty("Position", 24, 14)
        fnSetCharacterGraphics("Root/Images/Sprites/Alraune/", false)
        TA_SetProperty("Activation Script", sDialoguePath)
        TA_SetProperty("Facing", gci_Face_West)
    DL_PopActiveObject()
    TA_Create("AlrauneC")
        TA_SetProperty("Position", 25, 14)
        fnSetCharacterGraphics("Root/Images/Sprites/Alraune/", false)
        TA_SetProperty("Activation Script", sDialoguePath)
        TA_SetProperty("Facing", gci_Face_West)
    DL_PopActiveObject()
    TA_Create("AlrauneD")
        TA_SetProperty("Position", 22, 15)
        fnSetCharacterGraphics("Root/Images/Sprites/Alraune/", false)
        TA_SetProperty("Activation Script", sDialoguePath)
        TA_SetProperty("Facing", gci_Face_West)
    DL_PopActiveObject()
    TA_Create("AlrauneE")
        TA_SetProperty("Position", 23, 15)
        fnSetCharacterGraphics("Root/Images/Sprites/Alraune/", false)
        TA_SetProperty("Activation Script", sDialoguePath)
        TA_SetProperty("Facing", gci_Face_West)
    DL_PopActiveObject()
    TA_Create("AlrauneF")
        TA_SetProperty("Position", 24, 15)
        fnSetCharacterGraphics("Root/Images/Sprites/Alraune/", false)
        TA_SetProperty("Activation Script", sDialoguePath)
        TA_SetProperty("Facing", gci_Face_West)
    DL_PopActiveObject()
    TA_Create("AlrauneG")
        TA_SetProperty("Position", 25, 15)
        fnSetCharacterGraphics("Root/Images/Sprites/Alraune/", false)
        TA_SetProperty("Activation Script", sDialoguePath)
        TA_SetProperty("Facing", gci_Face_West)
    DL_PopActiveObject()
    
    --Offscreen allies.
    for i = 0, 7, 1 do
        local iRoll = LM_GetRandomNumber(1, 4)
        local sSprite = "Root/Images/Sprites/AlrauneR/"
        if(iRoll == 2) then
            sSprite = "Root/Images/Sprites/WerecatR/"
        elseif(iRoll == 3) then
            sSprite = "Root/Images/Sprites/WerecatThiefR/"
        elseif(iRoll == 4) then
            sSprite = "Root/Images/Sprites/BeeGirlR/"
        end
        
        TA_Create("RubberPal"..i)
            TA_SetProperty("Position", -1, -1)
            fnSetCharacterGraphics(sSprite, false)
            TA_SetProperty("Activation Script", sDialoguePath)
            TA_SetProperty("Facing", gci_Face_East)
            if(iRoll == 4) then
                TA_SetProperty("Y Oscillates", true)
                TA_SetProperty("Auto Animates", true)
            end
        DL_PopActiveObject()
    end
    
    --Flags.
    VM_SetVar("Root/Variables/Chapter1/Scenes/Rubber|iRubberedRochea", "N", 1.0)
    
    --Dialogue.
    fnCutscene([[ AL_SetProperty("Music", "Null") ]])
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Voice:[VOICE|Rochea] Hold there, plaguebearers!") ]])
    fnCutsceneBlocker()
    
    --Party moment.
    fnCutsceneMergeParty()
    fnCutsceneBlocker()
    fnCutsceneMove("Mei", 8.25, 18.50)
    fnCutsceneMove("Rubberraune", 7.25, 18.50)
    fnCutsceneMove("Rubberbee", 6.25, 18.50)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Mei", 18.25, 18.50)
    fnCutsceneFace("Mei", 0, -1)
    fnCutsceneMove("Rubberraune", 17.25, 18.50)
    fnCutsceneFace("Rubberraune", 0, -1)
    fnCutsceneMove("Rubberbee", 16.25, 18.50)
    fnCutsceneFace("Rubberbee", 0, -1)
    fnCutsceneMove("Rochea", 18.25, 14.50)
    fnCutsceneMove("Rochea", 18.25, 15.50)
    fnCutsceneMove("AlrauneA", 16.75, 14.50)
    fnCutsceneFace("AlrauneA", 0, 1)
    fnCutsceneMove("AlrauneB", 17.75, 14.50)
    fnCutsceneFace("AlrauneB", 0, 1)
    fnCutsceneMove("AlrauneC", 18.75, 14.50)
    fnCutsceneFace("AlrauneC", 0, 1)
    
    fnCutsceneMove("AlrauneD", 21.25, 15.50)
    fnCutsceneMove("AlrauneD", 21.25, 19.50)
    fnCutsceneFace("AlrauneD", -1, 0)
    fnCutsceneMove("AlrauneE", 21.25, 15.50)
    fnCutsceneMove("AlrauneE", 21.25, 18.50)
    fnCutsceneFace("AlrauneE", -1, 0)
    fnCutsceneMove("AlrauneF", 21.25, 15.50)
    fnCutsceneMove("AlrauneF", 21.25, 17.50)
    fnCutsceneFace("AlrauneF", -1, 0)
    fnCutsceneMove("AlrauneG", 21.25, 15.50)
    fnCutsceneMove("AlrauneG", 21.25, 16.50)
    fnCutsceneFace("AlrauneG", -1, 0)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Rochea", "Neutral") ]])
    fnCutscene([[ Append("Rochea:[E|Neutral] The daughters of the wild shall stop your plague here![B][C]") ]])
    fnCutscene([[ Append("Rochea:[E|Neutral] ...[P] Are there only the three of you?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Allies appear and walk in.
    fnCutsceneTeleport("RubberPal0", 3.25, 18.50)
    fnCutsceneTeleport("RubberPal1", 4.25, 18.50)
    fnCutsceneTeleport("RubberPal2", 3.25, 19.50)
    fnCutsceneTeleport("RubberPal3", 4.25, 19.50)
    
    fnCutsceneTeleport("RubberPal4", 26.25, 8.50)
    fnCutsceneTeleport("RubberPal5", 27.25, 8.50)
    fnCutsceneTeleport("RubberPal6", 26.25, 9.50)
    fnCutsceneTeleport("RubberPal7", 27.25, 9.50)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("RubberPal0", 14.25, 18.50)
    fnCutsceneMove("RubberPal1", 15.25, 18.50)
    fnCutsceneMove("RubberPal2", 14.25, 19.50)
    fnCutsceneMove("RubberPal3", 15.25, 19.50)
    
    fnCutsceneMove("RubberPal6", 26.25, 14.50)
    fnCutsceneMove("RubberPal6", 22.25, 14.50)
    fnCutsceneMove("RubberPal7", 27.25, 15.50)
    fnCutsceneMove("RubberPal7", 22.25, 15.50)
    
    fnCutsceneMove("RubberPal4", 26.25, 14.50)
    fnCutsceneMove("RubberPal4", 23.25, 14.50)
    fnCutsceneMove("RubberPal5", 27.25, 15.50)
    fnCutsceneMove("RubberPal5", 23.25, 15.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Rochea", "Neutral") ]])
    fnCutscene([[ Append("Rochea:[E|Neutral] ...[P] Well that's just not fair at all.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    fnCutscene([[ AL_SetProperty("Activate Fade", 0, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Sounds.
    for i = 1, 10, 1 do
        local iRoll = LM_GetRandomNumber(1, 4)
        if(iRoll == 1) then
            fnCutscene([[ AudioManager_PlaySound("Combat|Impact_Slash") ]])
        elseif(iRoll == 2) then
            fnCutscene([[ AudioManager_PlaySound("Combat|Impact_Strike") ]])
        elseif(iRoll == 3) then
            fnCutscene([[ AudioManager_PlaySound("Combat|Impact_Pierce") ]])
        elseif(iRoll == 4) then
            fnCutscene([[ AudioManager_PlaySound("Combat|Impact_CrossSlash") ]])
        end
        fnCutsceneBlocker()
        fnCutsceneWait(35)
        fnCutsceneBlocker()
    end
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    
    --Sounds
    for i = 1, 10, 1 do
        local iRoll = LM_GetRandomNumber(1, 4)
        if(iRoll == 1) then
            fnCutscene([[ AudioManager_PlaySound("Rubber|SpreadA") ]])
        elseif(iRoll == 2) then
            fnCutscene([[ AudioManager_PlaySound("Rubber|ChangeB") ]])
        elseif(iRoll == 3) then
            fnCutscene([[ AudioManager_PlaySound("Rubber|SpreadBig") ]])
        elseif(iRoll == 4) then
            fnCutscene([[ AudioManager_PlaySound("Rubber|SpreadC") ]])
        end
        fnCutsceneBlocker()
        fnCutsceneWait(35)
        fnCutsceneBlocker()
    end
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    
    --Transform Rochea to rubber.
    local sString =      "EM_PushEntity(\"Rochea\")\n"
    sString = sString .. "    fnSetCharacterGraphics(\"Root/Images/Sprites/RocheaR/\", false)\n"
    sString = sString .. "DL_PopActiveObject()"
    fnCutscene(sString)
    
    --Transform the alraunes to rubber.
    local function fnSetToRubber(sBaseName)
        local sString =      "EM_PushEntity(\"" .. sBaseName .. "\")\n"
        sString = sString .. "    fnSetCharacterGraphics(\"Root/Images/Sprites/AlrauneR/\", false)\n"
        sString = sString .. "DL_PopActiveObject()"
        fnCutscene(sString)
    end
    fnSetToRubber("AlrauneA")
    fnSetToRubber("AlrauneB")
    fnSetToRubber("AlrauneC")
    fnSetToRubber("AlrauneD")
    fnSetToRubber("AlrauneE")
    fnSetToRubber("AlrauneF")
    fnSetToRubber("AlrauneG")
    
    --Reposition.
    fnCutsceneTeleport("Mei", 18.75, 14.50)
    fnCutsceneFace("Mei", 0, 1)
    
    fnCutsceneTeleport("RubberPal0", 16.25, 17.50)
    fnCutsceneFace("RubberPal0", 0, -1)
    fnCutsceneTeleport("RubberPal1", 17.25, 17.50)
    fnCutsceneFace("RubberPal1", 0, -1)
    fnCutsceneTeleport("RubberPal2", 18.25, 17.50)
    fnCutsceneFace("RubberPal2", 0, -1)
    fnCutsceneTeleport("RubberPal3", 19.25, 17.50)
    fnCutsceneFace("RubberPal3", 0, -1)
    fnCutsceneTeleport("RubberPal4", 20.25, 17.50)
    fnCutsceneFace("RubberPal4", 0, -1)
    fnCutsceneTeleport("RubberPal5", 21.25, 17.50)
    fnCutsceneFace("RubberPal5", 0, -1)
    
    fnCutsceneTeleport("RubberPal6",  16.25, 18.50)
    fnCutsceneFace("RubberPal6", 0, -1)
    fnCutsceneTeleport("Rubberraune", 17.25, 18.50)
    fnCutsceneFace("Rubberraune", 0, -1)
    fnCutsceneTeleport("AlrauneA",    18.25, 18.50)
    fnCutsceneFace("AlrauneA", 0, -1)
    fnCutsceneTeleport("Rochea",      19.25, 18.50)
    fnCutsceneFace("Rochea", 0, -1)
    fnCutsceneTeleport("Rubberbee",   20.25, 18.50)
    fnCutsceneFace("Rubberbee", 0, -1)
    fnCutsceneTeleport("RubberPal7",  21.25, 18.50)
    fnCutsceneFace("RubberPal7", 0, -1)
    
    fnCutsceneTeleport("AlrauneB", 16.25, 19.50)
    fnCutsceneFace("AlrauneB", 0, -1)
    fnCutsceneTeleport("AlrauneC", 17.25, 19.50)
    fnCutsceneFace("AlrauneC", 0, -1)
    fnCutsceneTeleport("AlrauneD", 18.25, 19.50)
    fnCutsceneFace("AlrauneD", 0, -1)
    fnCutsceneTeleport("AlrauneE", 19.25, 19.50)
    fnCutsceneFace("AlrauneE", 0, -1)
    fnCutsceneTeleport("AlrauneF", 20.25, 19.50)
    fnCutsceneFace("AlrauneF", 0, -1)
    fnCutsceneTeleport("AlrauneG", 21.25, 19.50)
    fnCutsceneFace("AlrauneG", 0, -1)
    
    --Fade in.
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 1, gcf_Night_R, gcf_Night_G, gcf_Night_B, gcf_Night_A, true) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Rochea", "Rubber") ]])
    fnCutscene([[ Append("Rochea:[E|Rubber] (We are with you.[P] We will help you spread.)[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] (Only a few more.[P] I am nearly there.[P] I only need a few more thralls.)[B][C]") ]])
    fnCutscene([[ Append("Rochea:[E|Rubber] (Spread...)[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] (Spread...)") ]])
    fnCutsceneBlocker()
    
    --Allies stop following.
    AL_SetProperty("Unfollow Actor Name", "Rubberraune")
    AL_SetProperty("Unfollow Actor Name", "Rubberbee")
    giFollowersTotal = 0
    gsaFollowerNames = {}
    giaFollowerIDs = {}
    
    --Edit collisions.
    fnCutscene([[ AL_SetProperty("Set Collision", 16, 17, 0, 1) ]])
    fnCutscene([[ AL_SetProperty("Set Collision", 17, 17, 0, 1) ]])
    fnCutscene([[ AL_SetProperty("Set Collision", 18, 17, 0, 1) ]])
    fnCutscene([[ AL_SetProperty("Set Collision", 19, 17, 0, 1) ]])
    fnCutscene([[ AL_SetProperty("Set Collision", 20, 17, 0, 1) ]])
    fnCutscene([[ AL_SetProperty("Set Collision", 21, 17, 0, 1) ]])
    
--Adina scene. Optional.
elseif(sObjectName == "AdinaScene") then
    
    --Case check:
    local iIsRubberMode  = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iIsRubberMode", "N")
    local iMeiLovesAdina = VM_GetVar("Root/Variables/Chapter1/Scenes/iMeiLovesAdina", "N")
    local iRubberedAdina = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iRubberedAdina", "N")
    if(iIsRubberMode == 0.0 or iMeiLovesAdina == 0.0 or iRubberedAdina == 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter1/Scenes/Rubber|iRubberedAdina", "N", 1.0)
    
    --Spawn.
    TA_Create("Adina")
        TA_SetProperty("Position", 41, 10)
        TA_SetProperty("Clipping Flag", true)
        fnSetCharacterGraphics("Root/Images/Sprites/Adina/", false)
        TA_SetProperty("Activation Script", fnResolvePath() .. "Dialogue.lua")
        TA_SetProperty("Facing", gci_Face_West)
    DL_PopActiveObject()
    
    --Movement.
    fnCutsceneMove("Mei", 31.25, 10.50)
    fnCutsceneMove("Adina", 34.25, 10.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Neutral") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] (The host recognizes this one.)[B][C]") ]])
    fnCutscene([[ Append("Adina:[E|Neutral] So it is true.[P] Mei, are you still within that shell?[B][C]") ]])
    fnCutscene([[ Append("Adina:[E|Neutral] I have come because I do not wish to see my loving thrall become someone else's thing.[B][C]") ]])
    fnCutscene([[ Append("Adina:[E|Neutral] If there is anything of you within there, stop now.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] (...)[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] (...)[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] (Spread...)[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] [SOUND|Rubber|SpreadBig](...)[B][C]") ]])
    fnCutscene([[ Append("Adina:[E|Neutral] I see...[P] So this shall be my fate as well...[B][C]") ]])
    fnCutscene([[ Append("Adina:[E|Neutral] [SOUND|Rubber|SpreadBig]At least we shall share it.[B][C]") ]])
    fnCutscene([[ Append("Adina:[E|Rubber] [SOUND|Rubber|ChangeA](Ah, it feels good.[P] Tight, protected, covered...)") ]])
    fnCutsceneBlocker()
    
    --Transform Adina to rubber.
    local sString =      "EM_PushEntity(\"Adina\")\n"
    sString = sString .. "    fnSetCharacterGraphics(\"Root/Images/Sprites/AdinaR/\", false)\n"
    sString = sString .. "DL_PopActiveObject()"
    fnCutscene(sString)
    
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Adina", "Rubber") ]])
    fnCutscene([[ Append("Adina:[E|Rubber] [SOUND|Rubber|ChangeB](Everyone must feel this.[P] I must spread...)[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] (The host felt emotion.[P] This new thrall will be a good thrall.)[B][C]") ]])
    fnCutscene([[ Append("Adina:[E|Rubber] (Yes.[P] Spread.[P] I want to spread...)[B][C]") ]])
    fnCutscene([[ Append("Adina:[E|Rubber] (An army gathers to the east to oppose the spread.[P] We must cover them.)[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] (We cannot be stopped.[P] We will spread.)") ]])
    fnCutsceneBlocker()
    
--Walkback.
elseif(sObjectName == "NotSouth") then
    
    --Case check:
    local iIsRubberMode = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iIsRubberMode", "N")
    if(iIsRubberMode == 0.0) then return end
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The trading post...[P] I need to spread there...)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    Cutscene_CreateEvent("ActorEvent", "Actor")
        ActorEvent_SetProperty("Subject Name", "Mei")
        ActorEvent_SetProperty("Move Amount", (0.00 * gciSizePerTile), (-1.00 * gciSizePerTile))
    DL_PopActiveObject()
    fnCutsceneBlocker()

end
