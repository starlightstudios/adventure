-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
if(sObjectName == "WisphagPools") then
    
    -- |[Activation Check]|
    local iHasWisphagForm   = VM_GetVar("Root/Variables/Global/Mei/iHasWisphagForm", "N")
    local iToldOfWhirlpools = VM_GetVar("Root/Variables/Chapter1/ScenesWisphag/iToldOfWhirlpools", "N")
    if(iHasWisphagForm == 0.0 or iToldOfWhirlpools == 1.0) then return end
    
    -- |[Flag]|
    VM_SetVar("Root/Variables/Chapter1/ScenesWisphag/iToldOfWhirlpools", "N", 1.0)
    
    -- |[Variables]|
    local iWasVoluntary  = VM_GetVar("Root/Variables/Chapter1/ScenesWisphag/iWasVoluntary", "N")
    local sFlorentinaJob = VM_GetVar("Root/Variables/Global/Florentina/sCurrentJob", "S")
    
    -- |[Scene]|
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("Mei:[E|Happy] Oh![P] Florentina![P] Thanks to my new form, I believe we can make use of whirlpools now![B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Surprise] Wow![P] What a useful new ability you have![P] Just one problem.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] What's that?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] 'We'.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Offended] Oh for crying out loud.[P] Don't be a big baby.[B][C]") ]])
    if(sFlorentinaJob == "Lurker") then
        fnCutscene([[ Append("Florentina:[E|Neutral] I can't have you ruining my perfect resin skin.[P] What if I dissolve?[P] Swamp water is full of all kinds of awful stuff.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] That, and while not breathing is real fun, if this amulet slips off I don't like my odds.[B][C]") ]])
    
    elseif(iWasVoluntary == 0.0) then
        fnCutscene([[ Append("Florentina:[E|Neutral] I'm not exactly keen to dive into a watery grave, Mei.[P] I don't get a new transformation by being reckless like you do.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Confused] Unless you consider shapeshifting into a dead alraune to be a new form.[P] What about this makes you think it's a good idea?[B][C]") ]])
    else
        fnCutscene([[ Append("Florentina:[E|Neutral] You're not the one who fought to keep her head above water when she dove in to rescue her friend who she thought was drowning.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] ...[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Smirk] ...[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Laugh] FRIEND!?[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Facepalm] *Taff...*[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Confused] Me thinking you were dying aside, what about these whirlpools makes you think this is a good idea?[B][C]") ]])
    end
    fnCutscene([[ Append("Mei:[E|Neutral] The souls of the dead and the flow of the water guide me.[P] Even when not in this form, I can sense where the pools go intuitively.[P] We will be safe.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] Oh yeah?[P] The souls of the dead guide you?[P] Is that supposed to *help* here?[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Offended] Come on![P] You saw me burst out of the water after I transformed![P] It's perfectly safe![B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] Okay, fine.[P] Just keep in mind I don't have a magic runestone or the luck of a rabbit here.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Happy] I will keep you safe.[P] I swear it.") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "Arrival") then
    fnExecWaterEmerge(30+gcfWhirl2x2OffX, 53+gcfWhirl2x2OffY, 29.25, 54.00)
end
