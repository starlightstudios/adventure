-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= Exits ========================================== ]|
if(sObjectName == "WhirlpoolA") then
    
    --Variables.
    local iHasWisphagForm = VM_GetVar("Root/Variables/Global/Mei/iHasWisphagForm", "N")
    local iIsRubberMode   = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iIsRubberMode", "N")
    
    --Do nothing in rubber mode.
    if(iIsRubberMode == 1.0) then
    
    --If Mei does not have wisphag form, this will trigger the wisphag volunteer sequence.
    elseif(iHasWisphagForm == 0.0) then

        --Setup.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Mei:[VOICE|Mei] (There's a whirlpool here.[P] I think I can see something on the bottom.[P] Jump in?)[B]") ]])

        --Decision script is this script. It must be surrounded by quotes.
        local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
        fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"You crazy?\",  " .. sDecisionScript .. ", \"No\") ")
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\",         " .. sDecisionScript .. ", \"Yes\") ")
        fnCutsceneBlocker()
        
    --Otherwise, jump in normally.
    else
        fnExecWaterJumpToMap(30+gcfWhirl2x2OffX, 53+gcfWhirl2x2OffX, "StarfieldCavesA", 1, 1)
    end

--Other whirlpools require wisphag form.
elseif(sObjectName == "WhirlpoolB") then

    --Can't use in rubber mode or if wisphag TF is not found.
    local iIsRubberMode = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iIsRubberMode", "N")
    local iHasWisphagForm = VM_GetVar("Root/Variables/Global/Mei/iHasWisphagForm", "N")
    if(iIsRubberMode == 1.0 or iHasWisphagForm == 0.0) then return end

    --Jump in and move.
    fnExecWaterJumpToPos(57 + gcfWhirl3x2OffX, 16 + gcfWhirl3x2OffY, 49 + gcfWhirl2x2OffX, 22 + gcfWhirl2x2OffY, 49.75, 24.50)

--Whirlpool that returns to land, doesn't check forms.
elseif(sObjectName == "WhirlpoolC") then
    fnExecWaterJumpToPos(49 + gcfWhirl2x2OffX, 22 + gcfWhirl2x2OffY, 57 + gcfWhirl3x2OffX, 16 + gcfWhirl3x2OffY, 58.25, 15.50)

-- |[ ====================================== Examinables ======================================= ]|
elseif(sObjectName == "SampleExaminable") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Text here.)") ]])
    fnCutsceneBlocker()

-- |[ ===================================== Don't Jump In ====================================== ]|
elseif(sObjectName == "No") then
	WD_SetProperty("Hide")

-- |[ ======================================= Do Jump In ======================================= ]|
elseif(sObjectName == "Yes") then
	WD_SetProperty("Hide")
    
    --Flags.
    VM_SetVar("Root/Variables/Chapter1/ScenesWisphag/iWasVoluntary", "N", 1.0)
    
    --Spawn NPCs.
    fnStandardNPCByPosition("WisphagBuddy")
    fnSpawnSplash("MeiSplash")
    fnSpawnSplash("WisphagSplash")
    
    --Disable collisions.
    AL_SetProperty("Set Collision", 30, 53, 0, 0)
    
    --Position.
    fnCutsceneMove("Mei", 29.25, 53.50)
    fnCutsceneFace("Mei", 1, 0)
    fnCutsceneMove("Florentina", 29.25, 54.50)
    fnCutsceneFace("Florentina", 1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("Florentina:[E|Neutral] You sure about this, Mei?[P] Diving into a swamp is a bad idea at the best of times.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] There's something at the bottom, I can see it glowing.[P] It looks important.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Happy] What if it's treasure?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Happy] That's called a 'stranded asset'.[P] If there was a giant pile of gold on the moon, it'd cost more than it's worth to retrieve it.[P] If you could retrieve it at all.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] Besides, I don't see anything, and I'm looking right at it right now.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Sad] You don't?[P] Are you sure?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] Very sure.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Offended] That just deepens the mystery![B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] You're the one off the deep end right now.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Offended] Okay, okay, fine.[P] You made your point.[P] Maybe we can come back later with some ropes and do a safe dive.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Surprise] Are you speaking sense all of the sudden?[P] Do my ears deceive me?[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Offended] You know, if you make fun of me for doing the right thing, I'm less likely to do it.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("WisphagBuddy", 27.25, 53.50, 2.00)
    fnCutsceneMove("WisphagBuddy", 28.25, 53.50, 2.00)
    fnCutsceneBlocker()
    fnCutsceneFace("Mei", -1, 0)
    fnCutsceneFace("Florentina", -1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Wisphag:[VOICE|Wisphag] Hsss![P] Hsss![B][C]") ]])
    fnCutscene([[ Append("Mei:[VOICE|Mei] What the -[P] get away from me![B][C]") ]])
    fnCutscene([[ Append("Wisphag:[VOICE|Wisphag] Hsss![P] Hsss![B][C]") ]])
    fnCutscene([[ Append("Mei:[VOICE|Mei] Back off -[P] yikes!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMoveFace("Mei", 29.70, 53.50, -1, 0, 0.30)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Mei:[VOICE|Mei] Aieee![B][C]") ]])
    fnCutscene([[ Append("Wisphag:[VOICE|Wisphag] Hsss![P] Hsss!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Sploosh.
    fnCutsceneCamera("Position", gcfCamera_Cutscene_Speed, 30.00, 53.50)
    fnCutsceneMoveFace("Mei", 30.00, 53.50, -1, 0, 1.00)
    fnCutsceneBlocker()
    fnCutsceneFace("Florentina", 0, -1)
    fnCutsceneTeleport("Mei", -1.25, -1.50)
    fnCutscenePlaySound("World|BigSplash")
    fnCutsceneMoveExpirableSprite("MeiSplash", 30.25, 53.50)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("WisphagBuddy", 29.25, 53.50, 2.00)
    fnCutsceneBlocker()

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Wisphag:[VOICE|Wisphag] Hsss![P] Hsss![B][C]") ]])
    fnCutscene([[ Append("Florentina:[VOICE|Florentina] Mei![B][C]") ]])
    fnCutscene([[ Append("Wisphag:[VOICE|Wisphag] Hsss![P] Hsss![B][C]") ]])
    fnCutscene([[ Append("Florentina:[VOICE|Florentina] That's it, you're dead, freak!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneJumpTo("WisphagBuddy", 30.25, 53.50, 25)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneTeleport("WisphagBuddy", -1.25, -1.50)
    fnCutscenePlaySound("World|BigSplash")
    fnCutsceneMoveExpirableSprite("WisphagSplash", 30.25, 53.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Florentina:[VOICE|Florentina] Damn it![P] Mei![P] Mei hold on, I'll tie a vine and come after you!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Blackout.
    fnCutscene([[ AL_SetProperty("Activate Fade", 65, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    
    --Transition level.
    fnCutscene([[ AL_BeginTransitionTo("WisphagSwampA", "FORCEPOS:4.0x1.0x0") ]])

-- |[ ========================================== Debug ========================================= ]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
