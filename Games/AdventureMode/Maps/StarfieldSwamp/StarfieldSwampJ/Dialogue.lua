-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[Name Resolve]|
    --Resolve the name of the calling entity.
    local sActorName = TA_GetProperty("Name")

    --Set facing.
    TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[NPCName]|
    if(sActorName == "Wisphag") then
        
        --Variables.
        local sMeiForm        = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
        local iHasWisphagForm = VM_GetVar("Root/Variables/Global/Mei/iHasWisphagForm", "N")
        
        --Not a wisphag, doesn't have the form.
        if(sMeiForm ~= "Wisphag" and iHasWisphagForm == 0.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Mei:[VOICE|Mei] (It's not attacking...)[B][C]") ]])
            fnCutscene([[ Append("Wisphag:[VOICE|Wisphag] Hsss...?[B][C]") ]])
            fnCutscene([[ Append("Mei:[VOICE|Mei] (Best to keep my distance...)") ]])
            fnCutsceneBlocker()
        
        --Not a wisphag, but has the form:
        elseif(sMeiForm ~= "Wisphag" and iHasWisphagForm == 1.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Mei:[VOICE|Mei] Hey there...[P] sister?[P] Comrade?[P] Friend?[B][C]") ]])
            fnCutscene([[ Append("Wisphag:[VOICE|Wisphag] Hsss...?[B][C]") ]])
            fnCutscene([[ Append("Mei:[VOICE|Mei] How's it going?[B][C]") ]])
            fnCutscene([[ Append("Wisphag:[VOICE|Wisphag] Hsss...[B][C]") ]])
            fnCutscene([[ Append("Mei:[VOICE|Mei] Good talk!") ]])
            fnCutsceneBlocker()
        
        --Is a wisphag.
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Mei:[VOICE|Mei] Hey there...[P] sister?[P] Comrade?[P] Friend?[B][C]") ]])
            fnCutscene([[ Append("Wisphag:[VOICE|Wisphag] Hsss...?[B][C]") ]])
            fnCutscene([[ Append("Mei:[VOICE|Mei] I can't understand you but I can at least read your body language now.[P] I like you too![B][C]") ]])
            fnCutscene([[ Append("Mei:[VOICE|Mei] Sorry if I was rude earlier.[B][C]") ]])
            fnCutscene([[ Append("Wisphag:[VOICE|Wisphag] Hsss![B][C]") ]])
            fnCutscene([[ Append("Mei:[VOICE|Mei] Yeah Florentina's a real handful.[P] But she means well.[B][C]") ]])
            fnCutscene([[ Append("Wisphag:[VOICE|Wisphag] Hsss...[B][C]") ]])
            fnCutscene([[ Append("Florentina:[VOICE|Florentina] Still keeping my distance over here.") ]])
            fnCutsceneBlocker()
        
        end
    end
end
