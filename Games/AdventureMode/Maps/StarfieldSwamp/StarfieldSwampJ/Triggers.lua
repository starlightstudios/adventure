-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
if(sObjectName == "Arrival") then
    
    -- |[Basic]|
    --Variables.
    local iRecoveredFlorentina      = VM_GetVar("Root/Variables/Chapter1/ScenesWisphag/iRecoveredFlorentina", "N")
    local iFlorentinaKnowsAboutRune = VM_GetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsAboutRune", "N")
    
    -- |[No Recovery]|
    --Just play the emerge case.
    if(iRecoveredFlorentina == 1.0) then
        fnExecWaterEmerge(11.25, 9.50, 11.25, 10.50)
        return
    end
    
    -- |[Recovery Scene]|
    --Variables.
    local iWasVoluntary = VM_GetVar("Root/Variables/Chapter1/ScenesWisphag/iWasVoluntary", "N")
    
    --Florentina rejoins the party.
    VM_SetVar("Root/Variables/Chapter1/ScenesWisphag/iRecoveredFlorentina", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsAboutRune", "N", 1.0)

    --Camera.
    fnCutsceneCamera("Position", gcfCamera_Cutscene_Speed, 11.25, 13.50)
    
    --Blackout.
    fnCutscene([[ AL_SetProperty("Activate Fade", 0, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()

    --Create Florentina, add her to the party.
    AdvCombat_SetProperty("Party Slot", 1, "Florentina")
    fnSpecialCharacter("Florentina", -100, -100, gci_Face_South, false, nil)
    EM_PushEntity("Florentina")
        local iFlorentinaID = RE_GetID()
    DL_PopActiveObject()

    --Store it and tell her to follow.
    giFollowersTotal = 1
    giaFollowerIDs = {iFlorentinaID}
    gsaFollowerNames = {"Florentina"}
    AL_SetProperty("Follow Actor ID", iFlorentinaID)
    
    --Position Florentina and wisphag.
    fnCutsceneTeleport("Florentina", 10.25, 13.50)
    fnCutsceneFace("Florentina", -1, 0)  
    fnCutsceneFace("Wisphag", 1, 0) 
    
    --Fade in.
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
        
    --Create a splash sprite.
    fnSpawnSplash("MeiSplash")
    
    --Emerge from the water.
    fnCutscenePlaySound("World|MedSplash")
    fnCutsceneTeleport("Mei", 11.25, 9.50)
    fnCutsceneJumpTo("Mei", 11.25, 10.50, 15)
    fnCutsceneMoveExpirableSprite("MeiSplash", 11.25, 9.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Mei", 10.25, 11.50)
    fnCutsceneFace("Mei", 0, 1)  
    fnCutsceneBlocker()
    fnCutsceneFace("Florentina", 0, -1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Voluntary TF dialogue:
    if(iWasVoluntary == 1.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        fnCutscene([[ Append("Florentina:[E|Surprise] Mei![P] You're...[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Happy] A wisphag![P] Yeah![P] Don't worry about my friend over there, she saved my life![P] Sort of![B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Angry] Even though it was kind of her fault I fell in to begin with...[B][C]") ]])
        if(iFlorentinaKnowsAboutRune == 0.0) then
            fnCutscene([[ Append("Florentina:[E|Surprise] And you're just okay with being turned into one of those things?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Happy] Of course! I know so much more now, and I can always transform back if I frighten people.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Surprise] Transform back?[P] How!?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] I can't believe I haven't mentioned my transformation power by now.[P] With the runestone?[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Offended] Seems like something you should mention when we meet up![B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Sad] Uhhhh...[P] it never came up?[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Facepalm] Unbelievable...[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Confused] I didn't even know wisphags turned people into...[P] wisphags.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] The wisphags didn't do it, the wisps did.[P] I think...[P] I think it's because I understand them.[B][C]") ]])
        else
            fnCutscene([[ Append("Florentina:[E|Surprise] And you got turned into a wisphag?[P] I didn't even know that was possible.[P] I've never seen that happen.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] The wisphags didn't do it, the wisps did.[P] I think...[P] I think it's because I understand them.[B][C]") ]])
        end
        fnCutscene([[ Append("Mei:[E|Smirk] The thing I saw underwater was a shining soul.[P] A soul, Florentina![P] And I could see it![B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Blush] Maybe it's my runestone, I don't really know, but I can sense them.[P] Starfield Swamp is full of them.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] Why's that?[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Offended] I was...[P] going to ask you, actually.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] The wisphags converge in places where there are many lost souls that need to be sent on to the next life.[P] They build monuments that attract and guide them.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] Do you have any idea why this swamp would have so many lost souls?[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] No.[P] Best guess is that there was some battle here long ago.[P] You could say that about almost anywhere, of course.[P] Anywhere you go, some army clashed there.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] So, this whole soul-guiding thing.[P] Are you still bent on going home?[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] I...[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Blush] I'd love to stay and help the wisphags but...[P] I guess I'm a lost soul, too.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Sad] I'm sorry, my friends.[P] I must find my way back to Earth.[P] But, if I see any lost souls there, I will make sure to send them on their way![B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] The job is the same as ever.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Happy] Of course![P] Sorry about making you worry.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Confused] Don't make me dive into a swamp trying to rescue your stupid butt in the future.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Cry] Sorry...[P] sorry...[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Smirk] The wisphags are pretty nice but they think we're intruding onto their sanctuary.[P] So let's not aggravate them, okay?[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] I'm still keeping my distance if that's okay with you.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] Oh, and one other thing.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Offended] If you want to go for round 2 with that whirlpool, I'm game.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] You think there's something down there?[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Offended] I know there is, we should take a look.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    
    --Non-voluntary version.
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        fnCutscene([[ Append("Florentina:[E|Surprise] Mei![P] You're...[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Happy] A wisphag![P] Yeah![P] Are you getting along with my friend over there?[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Happy] Sort of?[P] When we got split up she just kept on beckoning me.[P] The little ones said she helped rescue you.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Sad] Yeah, I fell in some kind of sinkhole, almost drowned.[P] I blacked out but I think the wisphags saved me.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Angry] They thought we were intruding on their shrine.[P] Humans do it all the time, or just plain attack them.[P] They were defending themselves.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] I can respect that, getting mad when someone's on your turf.[P] How come they don't just...[P] tell us to leave?[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] I don't think they were humans once, they're like really smart animals.[P] Like, monkey or ape smart.[P] But they can't speak.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] Monkeys?[P] I think I saw one of those, once.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] Humans are descended from apes.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Happy] Are they, now?[P] Glad I'm a plant.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Offended] The point is, please tell people to let the wisphags do their thing in peace.[P] They don't mean to hurt anyone.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] And what, exactly, is their 'thing'?[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] We are servants of the earth.[P] We find souls who can't find their way to the next life, and guide them.[P] That's what the glowy shrine across the water is for.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] Seems like an alraune thing.[P] You should tell the wisphags to team up with Rochea.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Smirk] I could mention it, but I think they don't need help.[P] They just need privacy.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] Can you sense souls now, too?[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Blush] Maybe it's my runestone, I don't really know, but I could sense them before.[P] Now, I can see them and talk to them.[P] Starfield Swamp is full of them.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] Why's that?[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Offended] I was...[P] going to ask you, actually.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] The wisphags converge in places where there are many lost souls that need to be sent on to the next life.[P] They build monuments that attract and guide them.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] Do you have any idea why this swamp would have so many lost souls?[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] No.[P] Best guess is that there was some battle here long ago.[P] You could say that about almost anywhere, of course.[P] Anywhere you go, some army clashed there.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] So, this whole soul-guiding thing.[P] Are you still bent on going home?[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] I...[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Blush] I'd love to stay and help the wisphags but...[P] I guess I'm a lost soul, too.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Sad] I'm sorry, my friends.[P] I must find my way back to Earth.[P] But, if I see any lost souls there, I will make sure to send them on their way![B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] The job is the same as ever.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Smirk] Yeah, but please, I mean it.[P] Tell everyone at the trading post to just let the wisphags be.[B][C]") ]])
        if(iFlorentinaKnowsAboutRune == 0.0) then
            fnCutscene([[ Append("Florentina:[E|Neutral] I'm still keeping my distance from them if that's okay with you.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] I can transform back if it's making you uncomfortable.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] You can what back now?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] Transform?[P] With the runestone?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Sad] How have I not mentioned this yet.[P] This seems like the first thing I should mention.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Happy] I'll say.[P] Think of all the places we can break into with this![B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Offended] Breaking into places with the help of lost souls is a quick way to become one yourself.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Happy] Hey I don't come to your parties and dump out the punchbowl.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Offended] I will only help you break into a place if it's to help me find a way home or if I think it's sufficiently funny or justified.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] You drive a hard bargain but I must accept.") ]])
        else
            fnCutscene([[ Append("Florentina:[E|Neutral] I'm still keeping my distance from them if that's okay with you.") ]])
        end
        fnCutsceneBlocker()
    end
    
    -- |[Finish]|
    fnCutsceneCamera("Actor Name", gcfCamera_Cutscene_Speed, "Mei")
    fnAutoFoldParty()
    fnCutsceneBlocker()
        
end
