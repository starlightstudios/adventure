-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= Exits ========================================== ]|
if(sObjectName == "Whirlpool") then
    
    --Variables.
    local iHasWisphagForm   = VM_GetVar("Root/Variables/Global/Mei/iHasWisphagForm", "N")
    local iToldOfWhirlpools = VM_GetVar("Root/Variables/Chapter1/ScenesWisphag/iToldOfWhirlpools", "N")
    local iIsRubberMode     = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iIsRubberMode", "N")
    
    --If Mei does not have wisphag form, this does nothing.
    if(iHasWisphagForm == 0.0) then return end
    
    --Rubber mode, does nothing.
    if(iIsRubberMode == 1.0) then return end
    
    -- |[Has Form, Not Told Florentina]|
    --If you have Wisphag form but have not told Florentina about whirlpools, this scene plays.
    if(iToldOfWhirlpools == 0.0) then
    
        --Flag.
        VM_SetVar("Root/Variables/Chapter1/ScenesWisphag/iToldOfWhirlpools", "N", 1.0)
        
        --Variables.
        local iWasVoluntary = VM_GetVar("Root/Variables/Chapter1/ScenesWisphag/iWasVoluntary", "N")
        
        --Scene.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        fnCutscene([[ Append("Mei:[E|Happy] Oh![P] Florentina![P] Thanks to my new form, I believe we can make use of whirlpools now![B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Surprise] Wow![P] What a useful new ability you have![P] Just one problem.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] What's that?[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] 'We'.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Offended] Oh for crying out loud.[P] Don't be a big baby.[B][C]") ]])
        if(iWasVoluntary == 0.0) then
            fnCutscene([[ Append("Florentina:[E|Neutral] I'm not exactly keen to dive into a watery grave, Mei.[P] I don't get a new transformation by being reckless like you do.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Confused] Unless you consider shapeshifting into a dead alraune to be a new form.[P] What about this makes you think it's a good idea?[B][C]") ]])
        else
            fnCutscene([[ Append("Florentina:[E|Neutral] You're not the one who fought to keep her head above water when she dove in to rescue her friend who she thought was drowning.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] ...[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] ...[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Laugh] FRIEND!?[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Facepalm] *Taff...*[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Confused] Me thinking you were dying aside, what about these whirlpools makes you think this is a good idea?[B][C]") ]])
        end
        fnCutscene([[ Append("Mei:[E|Neutral] The souls of the dead and the flow of the water guide me.[P] Even when not in this form, I can sense where the pools go intuitively.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Confused] Oh yeah?[P] The souls of the dead guide you?[P] Is that supposed to *help* here?[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Offended] Come on![P] You saw me burst out of the water after I transformed![P] It's perfectly safe![B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] Okay, fine.[P] Just keep in mind I don't have a magic runestone or the luck of a rabbit here.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Happy] I will keep you safe.[P] I swear it.") ]])
        fnCutsceneBlocker()
    
    --Jump in.
    else
        fnExecWaterJumpToMap(10+gcfWhirl3x2OffX, 8+gcfWhirl3x2OffY, "WisphagSwampA", 1, 1)
    end
    
-- |[ ====================================== Examinables ======================================= ]|
elseif(sObjectName == "SampleExaminable") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Text here.)") ]])
    fnCutsceneBlocker()

-- |[ ========================================== Other ========================================= ]|
-- |[ ========================================== Debug ========================================= ]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
