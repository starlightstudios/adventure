-- |[ ======================================= Constructor ====================================== ]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = fnResolveDirectory() --Map's name is the name of the folder it is in.
local sLevelMusic = "StarfieldSwamp"
local sMapResolveName = "StarfieldSwampG"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[ ======================================== Standard ======================================== ]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

    -- |[Construction]|
	--Music.
	AL_SetProperty("Music", sLevelMusic)
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
    
    -- |[Map Handling]|
    --Resolve the map's variable.
    fnStandardCh1Map(sLevelName)
	
	-- |[Spawn NPCS Here]|
	--NPCs set to spawn here will be available for post-exec cutscenes.
    
    -- |[Aura]|
    local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
    if(sMeiForm == "Wisphag") then
        AL_SetProperty("Aura Spawn Properties", 3, "Root/Images/Sprites/Aura/Wisp")
    end

-- |[ ======================================== Post-Exec ======================================= ]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	-- |[Spawn NPCs Here]|
	--If an entity needs to spawn after a post-exec cutscene, put them here.
    local iMetPolaris = VM_GetVar("Root/Variables/Chapter1/Polaris/iMetPolaris", "N")
    if(iMetPolaris == 0.0) then
        fnStandardNPCByPosition("Polaris")
        fnStandardNPCByPosition("Dot")
        fnCutsceneTeleport("Dot", 27.25, 26.50)
        AL_SetProperty("Set Layer Disabled", "StoneCorgi", true)
        fnCutsceneFace("Polaris", -1, 0)
        fnCutsceneFace("Dot", 1, 0)
    else
        fnStandardNPCByPosition("Polaris")
        fnStandardNPCByPosition("Dot")
        fnCutsceneFace("Polaris", 0, 1)
        fnCutsceneFace("Dot", 0, 1)
    end
    
    -- |[Skillbook]|
    local iSkillbook5 = VM_GetVar("Root/Variables/Global/Mei/iSkillbook5", "N")
    if(iSkillbook5 == 1.0) then
		AL_SetProperty("Set Layer Disabled", "SkillbookIco", true)
    end

    -- |[Layers]|
    local iHasGravemarkerForm = VM_GetVar("Root/Variables/Global/Mei/iHasGravemarkerForm", "N")
    if(iHasGravemarkerForm == 0.0) then
		AL_SetProperty("Set Layer Disabled", "FloorDetectorBroken", true)
		AL_SetProperty("Set Layer Disabled", "FloorDetectorUnbroken", true)
    else
		AL_SetProperty("Set Layer Disabled", "FloorDetectorUnbroken", true)
    end

    -- |[Rubber Case]|
    --Change the defeat scene for enemies on the field.
    local iIsRubberMode = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iIsRubberMode", "N")
    if(iIsRubberMode == 1.0) then
        AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 1, gcf_Night_R, gcf_Night_G, gcf_Night_B, gcf_Night_A, true)
        
        --Movement.
        local iRubberedPolaris = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iRubberedPolaris", "N")
        if(iRubberedPolaris == 0.0) then
            fnCutsceneTeleport("Polaris", 28.25, 18.50)
            fnCutsceneFace("Polaris", 0, -1)
            fnCutsceneTeleport("Dot", 27.25, 17.50)
            fnCutsceneFace("Dot", 1, 0)
        else
            fnCutsceneTeleport("Polaris", 26.25, 21.50)
            fnCutsceneFace("Polaris", 0, 1)
            fnCutsceneTeleport("Dot", 25.25, 21.50)
            fnCutsceneFace("Dot", 1, 0)
            
            --Change Polaris to rubber.
            EM_PushEntity("Polaris")
                fnSetCharacterGraphics("Root/Images/Sprites/PolarisR/", false)
            DL_PopActiveObject()
        end
    end
end
