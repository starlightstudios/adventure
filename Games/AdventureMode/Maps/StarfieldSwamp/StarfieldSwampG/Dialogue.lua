-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[ ========== Dot ========== ]|
    --Dot, a cute corgo!
    if(sActorName == "Dot") then
        
        --Rubber.
        local iIsRubberMode      = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iIsRubberMode", "N")
        local iRubberedPolaris   = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iRubberedPolaris", "N")
        if(iIsRubberMode == 1.0 and iRubberedPolaris == 1.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] (A superior being...[P] You shall come to no harm...)") ]])
            return
        end
        
        --Pie Job:
        local iPaperCount = AdInv_GetProperty("Item Count", "Booped Paper")
        local iTakenPieJob = VM_GetVar("Root/Variables/Chapter1/Breanne/iTakenPieJob", "N")
        if(iTakenPieJob == 1.0 and iPaperCount < 1) then
            
            --Short scene.
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] Okay, give me a piece of paper...[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Happy] Here Dot![P] Good girl![P] [SOUND|World|DogBark]Who's a good girl![B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Happy] Boop![B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] You're a special kind of moron, you know that?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] We need paper booped against a dog's nose.[P] This is a dog.[P] Do you have some kind of problem with that?[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Happy] I suppose as long as Dot doesn't.[P] I think she enjoys it.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Happy] Boop![B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Laugh] Boop![B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Confused] That's enough, people are going to think you're insane.[B][C]") ]])
            fnCutscene([[ Append("[SOUND|World|TakeItem]*Got Booped Paper*") ]])
            
            --Add the item.
            LM_ExecuteScript(gsItemListing, "Booped Paper")
            
            --Reset flag.
            WD_SetProperty("Clear Topic Read", "Pepper Pie")
            
        --Normal case:
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            AudioManager_PlaySound("World|DogBark")
        end
    
    -- |[ ========== Polaris ========== ]|
    --Polaris, the amazing wizard!
    elseif(sActorName == "Polaris") then
    
        -- |[Variables]|
        --Mei's form variables.
        local sMeiForm           = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
        local sPolarisFirstForm  = VM_GetVar("Root/Variables/Chapter1/Polaris/sPolarisFirstForm", "S")
        local iNoticedFormChange = VM_GetVar("Root/Variables/Chapter1/Polaris/iNoticedFormChange", "N")
        local iMeiKnowsRilmani   = VM_GetVar("Root/Variables/Chapter1/Scenes/iMeiKnowsRilmani", "N")
        local iIsRubberMode      = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iIsRubberMode", "N")
        local iRubberedPolaris   = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iRubberedPolaris", "N")
    
        --If Polaris has not met Florentina, and she is present:
        local bIsFlorentinaPresent  = AL_GetProperty("Is Character Following", "Florentina")
        local iFlorentinaMetPolaris = VM_GetVar("Root/Variables/Chapter1/Polaris/iFlorentinaMetPolaris", "N")
        
        --Common.
        TA_SetProperty("Face Character", "PlayerEntity")
    
        -- |[Rubber]|
        --Rubberized polaris.
        if(iIsRubberMode == 1.0 and iRubberedPolaris == 1.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Polaris", "Rubber") ]])
            fnCutscene([[ Append("Polaris:[E|Rubber] (Thrall will... spread...)[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] (Spread...)") ]])
    
        -- |[Form Commentary]|
        --First, if Mei is a different form than when she first met Polaris.
        elseif(sMeiForm ~= sPolarisFirstForm and iNoticedFormChange == 0.0) then
            VM_SetVar("Root/Variables/Chapter1/Polaris/iNoticedFormChange", "N", 1.0)
            VM_SetVar("Root/Variables/Chapter1/Polaris/iPolarisSuggestStForas", "N", 1.0)
            VM_SetVar("Root/Variables/Chapter1/Polaris/iPolarisRuneQuestState", "N", 1.0)
            WD_SetProperty("Unlock Topic", "Polaris_Runestone", 1)
            
            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Polaris", "Neutral") ]])
            fnCutscene([[ Append("Polaris:[E|Neutral] Oh, hello there, Mei![B][C]") ]])
            
            --Notices Florentina if not already:
            if(bIsFlorentinaPresent == true and iFlorentinaMetPolaris == 0.0) then
                VM_SetVar("Root/Variables/Chapter1/Polaris/iFlorentinaMetPolaris", "N", 1.0)
                fnCutscene([[ Append("Mei:[E|Neutral] (Does she not notice I've changed forms?)[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Smirk] Polaris, I'd like you to meet Florentina.[P] She's helping me find a way home.[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Neutral] Polaris, the crazy weirdo who was hawking dog statues a while back?[B][C]") ]])
                fnCutscene([[ Append("Polaris:[E|Upset] Oh thank you for introducing me to this lovely person, Mei.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Happy] She's a little rough around the edges, but she's just joking.[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Neutral] Mei's lying because she's dumb.[B][C]") ]])
                fnCutscene([[ Append("Polaris:[E|Neutral] Joking again.[P] I get you.[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Neutral] I'm a merchant by trade.[P] You look like you've got some unique skills.[B][C]") ]])
                fnCutscene([[ Append("Polaris:[E|Upset] The hat gives it away?[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Happy] The hat gives it away, yes.[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Neutral] If you want to talk business...[B][C]") ]])
                fnCutscene([[ Append("Polaris:[E|Neutral] Later, but I am interested.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Yeah, we need to talk about how I've transformed.[B][C]") ]])
                
                --Special:
                local iFlorentinaKnowsAboutRune = VM_GetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsAboutRune", "N")
                if(iFlorentinaKnowsAboutRune == 0.0) then
                    VM_SetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsAboutRune", "N", 1.0)
                    fnCutscene([[ Append("Polaris:[E|Neutral] Uh, oh, oh my I didn't notice no how did that slip by?[B][C]") ]])
                    fnCutscene([[ Append("Florentina:[E|Offended] Yeah, what?[P] Mei, what are you babbling about?[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] I can transform myself using my runestone.[P] Did I not tell you that?[B][C]") ]])
                    fnCutscene([[ Append("Florentina:[E|Surprise] That seems kind of important, like one of those things you tell someone![B][C]") ]])
                    fnCutscene([[ Append("Florentina:[E|Happy] Show me it later, Mei.[P] I want to see it.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Happy] Okay![B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Upset] So much for a vacation...[B][C]") ]])
                
                --Normal:
                else
                    fnCutscene([[ Append("Polaris:[E|Neutral] Uh, oh, oh my I didn't notice no how did that slip by?[B][C]") ]])
                    fnCutscene([[ Append("Florentina:[E|Offended] Polaris, lying is a fine art.[P] A tapestry that must be woven carefully.[P] You are not an artist.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Offended] I -[P] I almost believed her.[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Neutral] Damn.[P] Guess we're going to have to talk about this now.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Offended] Talk about what?[B][C]") ]])
                end
                
                fnCutscene([[ Append("Polaris:[E|Upset] I don't want to get involved, this was supposed to be a nice place to get away and just catch up on things.[B][C]") ]])
                fnCutscene([[ Append("Polaris:[E|Neutral] Mei, do you know the big building east of here?[P] Looks like a mansion?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Yes, that's where I woke up after getting jumped by some thugs in robes.[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Neutral] And we've been trying to find her a way home.[B][C]") ]])
                fnCutscene([[ Append("Polaris:[E|Thinking] And, out of curiosity, have you seen this symbol before?[B][C]") ]])
                
                if(iMeiKnowsRilmani == 0.0) then
                    fnCutscene([[ Append("Mei:[E|Happy] Yes![B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Sad] Wait -[P] that's not the rune on my runestone.[P] What's that rune?[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Thinking] Runestone -[P] oh wow.[P] You have one of them.[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Thinking] This symbol here is the symbol of 'Power'.[P] It's a draconic word, I think.[P] It is pronounced 'San - Ya'.[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Thinking] There's a legend from the Arulente empire, seven centuries ago, about a hero who will appear bearing a runestone who can change her form.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] But it's not the same as the one on mine?[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Thinking] No, it isn't, but it looks to be of the same language.[P] If you're looking for a way home, you might have some luck looking for draconic script.[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Thinking] Arulente's capital was in Trafal, but it's long gone.[B][C]") ]])
                    fnCutscene([[ Append("Florentina:[E|Happy] You know an awful lot, don't you Polaris?[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Neutral] Thanks![P] I keep up with the literature when I can.[P] In fact, coming out here I was going to spend a few years getting caught up and writing some papers of my own.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] So, you're saying we need to look in Trafal?[P] Where's that?[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Thinking] No no, I'm saying you'd need to find something to help translate draconic script.[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Thinking] Draconic ruins litter this continent -[P] it's named Arulenta for a reason.[P] This whole place used to be part of the same empire.[B][C]") ]])
                    fnCutscene([[ Append("Florentina:[E|Neutral] You think St. Fora's would be the right age to have draconic writing?[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] What's that?[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Neutral] You'd know better than I would.[B][C]") ]])
                    fnCutscene([[ Append("Florentina:[E|Neutral] There's an old religious convent south of here, across the swamp, called St. Fora's.[P] Been abandoned for as long as I've been here.[B][C]") ]])
                    fnCutscene([[ Append("Florentina:[E|Blush] Treasure hunters come by looking to get in, but nobody's managed to find anything useful.[P] But the place might have something...[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Smirk] You think we can find some clues there?[B][C]") ]])
                    fnCutscene([[ Append("Florentina:[E|Blush] Yes, clues.[P] Of course.[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Thinking] Not the same rune, but similar.[P] I wonder if you're the hero of the legend, or maybe related to her in some way?[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Thinking] Then that spatial shock was...[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Neutral] I'll see what I can find out, but it'll take a while.") ]])
                else
                    fnCutscene([[ Append("Mei:[E|Happy] Yes![B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] Wait -[P] hold on, I think that one was 'power'.[P] I saw it in the Rilmani language guide.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Smirk] It's pretty similar to the one on my runestone![B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Thinking] Runestone -[P] oh wow.[P] You have one of them.[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Neutral] Yes, this is the draconic symbol for 'power'.[P] But you said it was Rilmani?[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Thinking] I wonder if the Arulentans borrowed some of the symbols...[P] But how would they even have met a Rilmani in the first place?[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Neutral] Still, something for me to look into.[P] So, it's a Rilmani symbol, then.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Smirk] I found a language guide in the Quantir manor way to the east of here.[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Neutral] I suppose that's as good as it gets.[P] You might want to look for more Rilmani artifacts, then.[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Neutral] I'd start with the manor you woke up in.[P] If the rumours are true, that whole building is a Rilmani artifact.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Happy] I had the same idea.[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Neutral] Great minds think alike.[P] Good luck, Mei.") ]])
                end
            
            --Florentina is present, but already introduced.
            elseif(bIsFlorentinaPresent == true) then
                VM_SetVar("Root/Variables/Chapter1/Polaris/iPolarisSuggestStForas", "N", 1.0)
                fnCutscene([[ Append("Mei:[E|Neutral] (Does she not notice I've changed forms?)[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Do you notice anything different about me?[B][C]") ]])
                fnCutscene([[ Append("Polaris:[E|Neutral] ...[P] No?[B][C]") ]])
                if(sMeiForm == "Human") then
                    fnCutscene([[ Append("Mei:[E|Neutral] Was I exactly, physically the same as before?[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Neutral] Er, I'm not -[P] good at recognizing faces?[P] Did you dye your hair?[B][C]") ]])
                elseif(sMeiForm == "Alraune") then
                    fnCutscene([[ Append("Mei:[E|Neutral] Was I blue and wearing a leafy chemise?[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Neutral] Is that what's that's called?[P] A chemise?[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] Uh -[P] yes?[P] Oh no, now I'm wondering if I've been calling it the wrong thing this whole time.[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Neutral] I think a chemise is longer and doesn't have the exposed stomach.[B][C]") ]])
                elseif(sMeiForm == "Bee") then
                    fnCutscene([[ Append("Mei:[E|Neutral] Did I have wings and antennae last we spoke?[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Neutral] I don't know, did you?[P] I was -[P] just so focused on Dot![P] She's just so cute![B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] (Damn, that's a really good excuse...)[B][C]") ]])
                elseif(sMeiForm == "Ghost") then
                    fnCutscene([[ Append("Mei:[E|Neutral] If you didn't notice, I'm now both dead and dressed in a maid outfit.[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Neutral] Oh.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Offended] And you don't find this remarkable?[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Neutral] I, uh, thought it'd be rude to point it out?[B][C]") ]])
                elseif(sMeiForm == "Werecat") then
                    fnCutscene([[ Append("Mei:[E|Offended] Don't notice the ears and claws?[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Neutral] Oh.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Offended] And you don't find this remarkable?[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Neutral] I, uh, thought it'd be rude to point it out?[B][C]") ]])
                elseif(sMeiForm == "Gravemarker") then
                    fnCutscene([[ Append("Mei:[E|Offended] I am currently, actually, a living angel statue.[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Neutral] Trust me, honey, I've seen a lot of statues.[B][C]") ]])
                elseif(sMeiForm == "Slime") then
                    fnCutscene([[ Append("Mei:[E|Offended] Do the big sloshy slime titties not stand out, at all?[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Neutral] Not more than usual.[P] The slimes around here all seem pretty happy about theirs.[B][C]") ]])
                elseif(sMeiForm == "Wisphag") then
                    fnCutscene([[ Append("Mei:[E|Offended] I can see your soul and I know you're lying.[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Neutral] See my soul, sure, but know I'm lying?[P] I know for a fact you can't tell that.[B][C]") ]])
                elseif(sMeiForm == "Mannequin") then
                    fnCutscene([[ Append("Mei:[E|Offended] I got turned into a mannequin.[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Neutral] I've been turned into a figurine so many times I really didn't notice.[B][C]") ]])
                end
                fnCutscene([[ Append("Mei:[E|Offended] Why are you playing dumb about this?[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Neutral] And more importantly, why are you so bad at it?[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Happy] You're not one of those [P]*habitually honest*[P] people, are you?[B][C]") ]])
                fnCutscene([[ Append("Polaris:[E|Upset] I really don't like you putting emphasis on words that shouldn't be emphasised.[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Happy] [P]*Oh don't you*[P].[B][C]") ]])
                fnCutscene([[ Append("Polaris:[E|Upset] Stop that.[B][C]") ]])
                fnCutscene([[ Append("Polaris:[E|Neutral] Fine.[P] Yes, Mei, you've changed forms.[P] And you can probably do it whenever you feel like.[B][C]") ]])
                fnCutscene([[ Append("Polaris:[E|Upset] Damn it.[P] Guess we're going to have to talk about this now.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Offended] Talk about what?[P] What are you hiding?[B][C]") ]])
                fnCutscene([[ Append("Polaris:[E|Upset] I don't want to get involved, this was supposed to be a nice place to get away and just catch up on things.[B][C]") ]])
                fnCutscene([[ Append("Polaris:[E|Neutral] Mei, do you know the big building east of here?[P] Looks like a mansion?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Yes, that's where I woke up after getting jumped by some thugs in robes.[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Neutral] And we've been trying to find her a way home.[B][C]") ]])
                fnCutscene([[ Append("Polaris:[E|Thinking] And, out of curiosity, have you seen this symbol before?[B][C]") ]])
                
                if(iMeiKnowsRilmani == 0.0) then
                    fnCutscene([[ Append("Mei:[E|Happy] Yes![B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Sad] Wait -[P] that's not the rune on my runestone.[P] What's that rune?[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Thinking] Runestone -[P] oh wow.[P] You have one of them.[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Thinking] This symbol here is the symbol of 'Power'.[P] It's a draconic word, I think.[P] It is pronounced 'San - Ya'.[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Thinking] There's a legend from the Arulente empire, seven centuries ago, about a hero who will appear bearing a runestone who can change her form.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] But it's not the same as the one on mine?[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Thinking] No, it isn't, but it looks to be of the same language.[P] If you're looking for a way home, you might have some luck looking for draconic script.[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Thinking] Arulente's capital was in Trafal, but it's long gone.[B][C]") ]])
                    fnCutscene([[ Append("Florentina:[E|Happy] You know an awful lot, don't you Polaris?[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Neutral] Thanks![P] I keep up with the literature when I can.[P] In fact, coming out here I was going to spend a few years getting caught up and writing some papers of my own.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] So, you're saying we need to look in Trafal?[P] Where's that?[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Thinking] No no, I'm saying you'd need to find something to help translate draconic script.[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Thinking] Draconic ruins litter this continent -[P] it's named Arulenta for a reason.[P] This whole place used to be part of the same empire.[B][C]") ]])
                    fnCutscene([[ Append("Florentina:[E|Neutral] You think St. Fora's would be the right age to have draconic writing?[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] What's that?[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Neutral] You'd know better than I would.[B][C]") ]])
                    fnCutscene([[ Append("Florentina:[E|Neutral] There's an old religious convent south of here, across the swamp, called St. Fora's.[P] Been abandoned for as long as I've been here.[B][C]") ]])
                    fnCutscene([[ Append("Florentina:[E|Blush] Treasure hunters come by looking to get in, but nobody's managed to find anything useful.[P] But the place might have something...[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Smirk] You think we can find some clues there?[B][C]") ]])
                    fnCutscene([[ Append("Florentina:[E|Blush] Yes, clues.[P] Of course.[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Thinking] Not the same rune, but similar.[P] I wonder if you're the hero of the legend, or maybe related to her in some way?[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Thinking] Then that spatial shock was...[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Neutral] I'll see what I can find out, but it'll take a while.") ]])
                else
                    fnCutscene([[ Append("Mei:[E|Happy] Yes![B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] Wait -[P] hold on, I think that one was 'power'.[P] I saw it in the Rilmani language guide.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Smirk] It's pretty similar to the one on my runestone![B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Thinking] Runestone -[P] oh wow.[P] You have one of them.[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Neutral] Yes, this is the draconic symbol for 'power'.[P] But you said it was Rilmani?[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Thinking] I wonder if the Arulentans borrowed some of the symbols...[P] But how would they even have met a Rilmani in the first place?[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Neutral] Still, something for me to look into.[P] So, it's a Rilmani symbol, then.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Smirk] I found a language guide in the Quantir manor way to the east of here.[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Neutral] I suppose that's as good as it gets.[P] You might want to look for more Rilmani artifacts, then.[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Neutral] I'd start with the manor you woke up in.[P] If the rumours are true, that whole building is a Rilmani artifact.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Happy] I had the same idea.[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Neutral] Great minds think alike.[P] Good luck, Mei.") ]])
                end
            
            --Florentina is not present.
            else
                fnCutscene([[ Append("Mei:[E|Neutral] (Does she not notice I've changed forms?)[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Do you notice anything different about me?[B][C]") ]])
                fnCutscene([[ Append("Polaris:[E|Neutral] ...[P] No?[B][C]") ]])
                if(sMeiForm == "Human") then
                    fnCutscene([[ Append("Mei:[E|Neutral] Was I exactly, physically the same as before?[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Neutral] Er, I'm not -[P] good at recognizing faces?[P] Did you dye your hair?[B][C]") ]])
                elseif(sMeiForm == "Alraune") then
                    fnCutscene([[ Append("Mei:[E|Neutral] Was I blue and wearing a leafy chemise?[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Neutral] Is that what's that's called?[P] A chemise?[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] Uh -[P] yes?[P] Oh no, now I'm wondering if I've been calling it the wrong thing this whole time.[B][C]") ]])
                elseif(sMeiForm == "Bee") then
                    fnCutscene([[ Append("Mei:[E|Neutral] Did I have wings and antennae last we spoke?[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Neutral] I don't know, did you?[P] I was -[P] just so focused on Dot![P] She's just so cute![B][C]") ]])
                elseif(sMeiForm == "Ghost") then
                    fnCutscene([[ Append("Mei:[E|Neutral] If you didn't notice, I'm now both dead and dressed in a maid outfit.[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Neutral] Oh.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Offended] And you don't find this remarkable?[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Neutral] I, uh, thought it'd be rude to point it out?[B][C]") ]])
                elseif(sMeiForm == "Werecat") then
                    fnCutscene([[ Append("Mei:[E|Offended] Don't notice the ears and claws?[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Neutral] Oh.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Offended] And you don't find this remarkable?[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Neutral] I, uh, thought it'd be rude to point it out?[B][C]") ]])
                elseif(sMeiForm == "Gravemarker") then
                    fnCutscene([[ Append("Mei:[E|Offended] I am currently, actually, a living angel statue.[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Neutral] Trust me, honey, I've seen a lot of statues.[B][C]") ]])
                elseif(sMeiForm == "Slime") then
                    fnCutscene([[ Append("Mei:[E|Offended] Do the big sloshy slime titties not stand out, at all?[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Neutral] Not more than usual.[P] The slimes around here all seem pretty happy about theirs.[B][C]") ]])
                elseif(sMeiForm == "Wisphag") then
                    fnCutscene([[ Append("Mei:[E|Offended] I can see your soul and I know you're lying.[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Neutral] See my soul, sure, but know I'm lying?[P] I know for a fact you can't tell that.[B][C]") ]])
                elseif(sMeiForm == "Mannequin") then
                    fnCutscene([[ Append("Mei:[E|Offended] I got turned into a mannequin.[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Neutral] I've been turned into a figurine so many times I really didn't notice.[B][C]") ]])
                end
                fnCutscene([[ Append("Mei:[E|Offended] Why are you playing dumb about this?[B][C]") ]])
                fnCutscene([[ Append("Polaris:[E|Thinking] Playing dumb about what?[P] I have no idea what you're talking about.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Angry] You're so easy to see through![P] You clearly know more than you're letting on, stop acting like you don't![B][C]") ]])
                if(sMeiForm == "Ghost") then
                    fnCutscene([[ Append("Polaris:[E|Upset] Hey, speaking of easy to see through.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Laugh] Ha ha![P] Poor choice of words![B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Offended] But you're not off the hook![B][C]") ]])
                end
                fnCutscene([[ Append("Mei:[E|Offended] Honestly, if you want to play dumb, you can't be a famous sorceress and dress the part.[B][C]") ]])
                fnCutscene([[ Append("Polaris:[E|Upset] You got me there.[P] Next time, I'll dress like a hobo.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] It's the hat.[P] The hat says 'I know magic stuff'.[B][C]") ]])
                fnCutscene([[ Append("Polaris:[E|Neutral] So, Mei.[P] You can change forms whenever you want, can't you?[B][C]") ]])
                fnCutscene([[ Append("Polaris:[E|Upset] I didn't want to get involved, this was supposed to be a nice place to get away and just catch up on things.[B][C]") ]])
                fnCutscene([[ Append("Polaris:[E|Upset] But I guess I can't stay neutral.[P] Oh well.[B][C]") ]])
                fnCutscene([[ Append("Polaris:[E|Neutral] Mei, do you know the big building east of here?[P] Looks like a mansion?[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Neutral] Yes, that's where I woke up after getting jumped by some thugs in robes.[B][C]") ]])
                fnCutscene([[ Append("Polaris:[E|Thinking] And, out of curiosity, have you seen this symbol before?[B][C]") ]])
                if(iMeiKnowsRilmani == 0.0) then
                    fnCutscene([[ Append("Mei:[E|Happy] Yes![B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Sad] Wait -[P] that's not the rune on my runestone.[P] What's that rune?[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Thinking] Runestone -[P] oh wow.[P] You have one of them.[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Thinking] This symbol here is the symbol of 'Power'.[P] It's a draconic word, I think.[P] It is pronounced 'San - Ya'.[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Thinking] There's a legend from the Arulente empire, seven centuries ago, about a hero who will appear bearing a runestone who can change her form.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] But it's not the same as the one on mine?[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Thinking] No, it isn't, but it looks to be of the same language.[P] If you're looking for a way home, you might have some luck looking for draconic script.[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Thinking] Arulente's capital was in Trafal, but it's long gone.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] So, you're saying we need to look in Trafal?[P] Where's that?[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Thinking] No no, I'm saying you'd need to find something to help translate draconic script.[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Thinking] Draconic ruins litter this continent -[P] it's named Arulenta for a reason.[P] This whole place used to be part of the same empire.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] Oh, that'd explain those old brick buildings I've been seeing.[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Neutral] Unfortunately, I'm not from Trannadar myself.[P] I don't know where you ought to look.[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Neutral] There's a fishing village west of here, or that trading post east of here.[P] I'm sure the locals can help.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] And I should ask about my runestone?[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Neutral] Yes, ask if they've seen anything that looks like it.[P] I'll see about getting some books sent out here, but it'll be a while.[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Thinking] Earliest I could promise any sort of research help is a few weeks from now.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Sad] A few weeks?[P] My parents will be so worried.[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Upset] I -[P] I'm sorry, Mei.[P] I didn't mean to be callous.[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Neutral] I just have a big backlog of other things I've been meaning to read.[P] But, helping others takes priority.[P] I'll do what I can.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Smirk] Thank you, Miss Polaris.[P] Just knowing there are people like you around means this place isn't so bad.") ]])
                else
                    fnCutscene([[ Append("Mei:[E|Happy] Yes![B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] Wait -[P] hold on, I think that one was 'power'.[P] I saw it in the Rilmani language guide.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Smirk] It's pretty similar to the one on my runestone![B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Thinking] Runestone -[P] oh wow.[P] You have one of them.[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Neutral] Yes, this is the draconic symbol for 'power'.[P] But you said it was Rilmani?[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Thinking] I wonder if the Arulentans borrowed some of the symbols...[P] But how would they even have met a Rilmani in the first place?[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Neutral] Still, something for me to look into.[P] So, it's a Rilmani symbol, then.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Smirk] I found a language guide in the Quantir manor way to the east of here.[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Neutral] I suppose that's as good as it gets.[P] You might want to look for more Rilmani artifacts, then.[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Neutral] I'd start with the manor you woke up in.[P] If the rumours are true, that whole building is a Rilmani artifact.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Happy] I had the same idea.[B][C]") ]])
                    fnCutscene([[ Append("Polaris:[E|Neutral] Great minds think alike.[P] Good luck, Mei.") ]])
                end
            end
            fnCutsceneBlocker()
            return
        
        --Meeting Florentina for the first time.
        elseif(bIsFlorentinaPresent == true and iFlorentinaMetPolaris == 0.0) then
            VM_SetVar("Root/Variables/Chapter1/Polaris/iFlorentinaMetPolaris", "N", 1.0)
            
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Polaris", "Neutral") ]])
            fnCutscene([[ Append("Polaris:[E|Neutral] Oh, hello there, Mei![P] Who's your friend?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] Polaris, I'd like you to meet Florentina.[P] She's helping me find a way home.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] Florentina, Polaris.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] Polaris, the crazy weirdo who was hawking dog statues a while back?[B][C]") ]])
            fnCutscene([[ Append("Polaris:[E|Upset] Oh thank you for introducing me to this lovely person, Mei.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Happy] She's a little rough around the edges, but she's just joking.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] Mei's lying because she's dumb.[B][C]") ]])
            fnCutscene([[ Append("Polaris:[E|Neutral] Joking again.[P] I get you.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] I'm a merchant by trade.[P] You look like you've got some unique skills.[B][C]") ]])
            fnCutscene([[ Append("Polaris:[E|Upset] The hat gives it away?[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Happy] The hat gives it away, yes.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] If you want to talk business...[B][C]") ]])
            fnCutscene([[ Append("Polaris:[E|Neutral] Later, but I am interested.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] All right, I'll ask you about it.[P] I have some ideas.[B][C]") ]])
            fnCutscene([[ Append("Polaris:[E|Upset] Legal ideas?[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Blush] Ethical.[B][C]") ]])
            fnCutscene([[ Append("Polaris:[E|Neutral] Oddly, that's actually a better answer.[B][C]") ]])
            fnCutscene([[ Append("Florentina:[E|Neutral] I think we could help each other out, Polaris.") ]])
            fnCutsceneBlocker()
            return
        
        --All other cases, go to topics.
        else
            
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Polaris", "Neutral") ]])
            fnCutscene([[ Append("Polaris:[E|Neutral] Mei, what would you like to talk about?[B][C]") ]])
            fnCutscene([[ WD_SetProperty("Activate Topics After Dialogue", "Polaris") ]])
        end
    end
end
