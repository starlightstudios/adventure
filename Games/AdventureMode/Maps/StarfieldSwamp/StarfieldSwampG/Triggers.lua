-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
if(sObjectName == "WarpActivate") then
    fnWarpUnlockHandler("Root/Variables/Chapter1/Campfires/iStarfieldSwampG", 49.25, 21.50)

elseif(sObjectName == "MeetPolaris") then

    --Repeat check.
    local iMetPolaris = VM_GetVar("Root/Variables/Chapter1/Polaris/iMetPolaris", "N")
    if(iMetPolaris == 1.0) then return end
    
    --Other variables.
    local sMeiForm              = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
    local iFlorentinaMetPolaris = VM_GetVar("Root/Variables/Chapter1/Polaris/iFlorentinaMetPolaris", "N")
    local iSeenDoorInSwampC     = VM_GetVar("Root/Variables/Chapter1/Polaris/iSeenDoorInSwampC", "N")
    local bIsFlorentinaPresent  = AL_GetProperty("Is Character Following", "Florentina")

    --Set flags.
    VM_SetVar("Root/Variables/Chapter1/Polaris/iMetPolaris", "N", 1.0)
    VM_SetVar("Root/Variables/Chapter1/Polaris/sPolarisFirstForm", "S", sMeiForm)
        
    -- |[Achievement]|
    AM_SetPropertyJournal("Unlock Achievement", "MeetPolaris")

    -- |[Mei Only]|
    if(bIsFlorentinaPresent == false) then
        
        --Create a character to represent the animation.
        TA_Create("Animation")
            TA_SetProperty("Position", -100, -100)
            TA_SetProperty("Facing", gci_Face_South)
            TA_SetProperty("Clipping Flag", false)
            fnSetCharacterGraphics("Root/Images/Sprites/GenericF0/", false)
            TA_SetProperty("Wipe Special Frames")
            for i = 31, 49, 1 do
                TA_SetProperty("Add Special Frame", "Anim"..i-31, "Root/Images/CombatAnimations/ShadowB/" .. i)
            end
            TA_SetProperty("Rendering Depth", 0.0)
        DL_PopActiveObject()
        fnCutsceneLayerDisabled("StoneCorgi", true)
        fnCutsceneTeleport("Animation", -100, -100)
        
        --Movement.
        fnCutsceneMergeParty()
        fnCutsceneBlocker()
        
        --Move Mei into position.
        fnCutsceneMove("Mei", 28.25, 29.50)
        fnCutsceneFace("Mei", 0, -1)
        
        --Scene.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Polaris", "Neutral") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] All right, Dot.[P] Hold still, just like that...[P] good girl...") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Effect.
        fnCutscene([[ AudioManager_PlaySound("World|MagicA") ]])
        fnCutsceneTeleport("Animation", 27.25-11, 26.50-11)
        fnCutsceneSetFrame("Animation", "Anim0")
        fnCutsceneBlocker()
        for i = 49, 31, -1 do
            fnCutsceneWait(3)
            fnCutsceneSetFrame("Animation", "Anim"..i-31)
            fnCutsceneBlocker()
        end
        
        --Move Dot off stage.
        fnCutsceneLayerDisabled("StoneCorgi", false)
        fnCutsceneWait(25)
        fnCutsceneTeleport("Dot", -100, -100)
        fnCutsceneBlocker()
        
        --Finish the effect.
        fnCutscene([[ AudioManager_PlaySound("World|MagicB") ]])
        for i = 31, 49, 1 do
            fnCutsceneWait(3)
            fnCutsceneSetFrame("Animation", "Anim"..i-31)
            fnCutsceneBlocker()
        end
        fnCutsceneTeleport("Animation", -100, -100)
        fnCutsceneBlocker()
        
        --Pause.
        fnCutsceneWait(65)
        fnCutsceneBlocker()
        
        --Move.
        fnCutsceneMove("Mei", 28.25, 28.50)
        fnCutsceneWait(65)
        fnCutsceneBlocker()
        fnCutsceneMove("Mei", 28.25, 27.50)
        fnCutsceneWait(65)
        fnCutsceneBlocker()
        fnCutsceneFace("Polaris", 0, 1)
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Angry") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Polaris", "Neutral") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] Oh, hello there![P] Lovely day, isn't it?[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Angry] Yes, lovely day in the swamp.[P] WHAT DID YOU DO TO THAT DOG?[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Thinking] What do you mean?[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Angry] YOU JUST TURNED THAT DOG TO STONE!!![B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] Huh?[P] No I didn't.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Angry] YES YOU DID![P] I SAW IT, THE STATUE IS RIGHT THERE![B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] ...[P] Hey, wait a minute.[P] Where did Dot go?[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] Uh oh...[P] uhh...[P] hey, did you see where Dot went?[P] She's my dog.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] Corgi, yellow and white.[P] You'd know her if you'd seen her.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Offended] ...[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] ...[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Offended] ...[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Upset] Is that a no?") ]])
        fnCutsceneBlocker()
        
        --Movement.
        fnCutscene([[ AudioManager_PlaySound("World|DogBark") ]])
        fnCutsceneTeleport("Dot", 17.25, 15.50)
        fnCutsceneBlocker()
        fnCutsceneMove("Dot", 17.25, 26.50, 2.50)
        fnCutsceneMove("Dot", 26.25, 26.50, 2.50)
        fnCutsceneBlocker()
        fnCutscene([[ AudioManager_PlaySound("World|DogBark") ]])
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Offended") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Polaris", "Neutral") ]])
        fnCutscene([[ Append("Witch:[E|Neutral] Oh, there you are![P] You had me worried![B][C]") ]])
        fnCutscene([[ Append("Witch:[E|Neutral] Who's a good girl![P] Who's a good girl![B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Sad] (What the heck is going on?)[B][C]") ]])
        fnCutscene([[ Append("Witch:[E|Neutral] Sorry about that.[P] The spell usually puts her right next to me.[B][C]") ]])
        fnCutscene([[ Append("Witch:[E|Neutral] There was a big spatial anomaly a while ago, might have put some things out of whack.[P] Better recalibrate everything.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Sad] So...[P] You didn't turn the dog to stone?[B][C]") ]])
        if(sMeiForm == "Human") then
            fnCutscene([[ Append("Witch:[E|Upset] You're not a cop, are you?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] Very far from it.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Offended] But mistreating a dog will put you on the wrong side of this sword.[B][C]") ]])
            fnCutscene([[ Append("Witch:[E|Neutral] ...[P] I respect you, madam.[P] Lethal force is justified for people who hurt dogs.[B][C]") ]])
            fnCutscene([[ Append("Witch:[E|Neutral] Luckily, that's not what I'm doing.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] So just what [P]*were*[P] you doing to this adorable corgi?[B][C]") ]])
        elseif(sMeiForm == "Alraune") then
            fnCutscene([[ Append("Witch:[E|Upset] What are you, some kind of plant cop?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] Very far from it.[P] You're thinking of Rochea...[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Offended] But mistreating a dog will put you on the wrong side of this sword.[B][C]") ]])
            fnCutscene([[ Append("Witch:[E|Neutral] ...[P] I respect you, madam. Lethal force is justified for people who hurt dogs.[B][C]") ]])
            fnCutscene([[ Append("Witch:[E|Neutral] Luckily, that's not what I'm doing.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] So just what [P]*were*[P] you doing to this adorable corgi?[B][C]") ]])
        elseif(sMeiForm == "Bee") then
            fnCutscene([[ Append("Witch:[E|Upset] What are you, a bee cop?[B][C]") ]])
            fnCutscene([[ Append("Witch:[E|Neutral] Hey wait, how are you talking?[P] Beegirls don't talk.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Blush] Uh, it's a long story?[B][C]") ]])
            fnCutscene([[ Append("Witch:[E|Thinking] If I were an entomologist, I'd want to hear it.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Offended] Don't change the subject.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Offended] So just what [P]*were*[P] you doing to this adorable corgi?[B][C]") ]])
        elseif(sMeiForm == "Ghost") then
            fnCutscene([[ Append("Witch:[E|Upset] Being dead doesn't stop you from being a cop, it seems.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Offended] If you made a new dog ghost, I'd make a new witch ghost, you get me?[B][C]") ]])
            fnCutscene([[ Append("Witch:[E|Neutral] I'd do it to myself first if I ever hurt Dot![P] She's my good girl![B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Offended] So just what [P]*were*[P] you doing to this adorable corgi?[B][C]") ]])
        elseif(sMeiForm == "Werecat") then
            fnCutscene([[ Append("Witch:[E|Upset] Cats don't make good narcs, kiddo.[P] Take it from me.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Offended] Hey, despite what you may have heard, cats and dogs are on the same team.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Offended] We're fluffy, we're cute, and we don't take any crap from abusive humans.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Offended] So what were you doing to this adorable corgi, miss?[B][C]") ]])
        elseif(sMeiForm == "Gravemarker") then
            fnCutscene([[ Append("Witch:[E|Upset] I'm not going to patronize you, you ought to know a lot about petrification magic.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Offended] I haven't got any idea what you're talking about.[B][C]") ]])
            fnCutscene([[ Append("Witch:[E|Upset] You're a statue.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Offended] No kidding?[P] That doesn't explain what you were doing to that dog![B][C]") ]])
        elseif(sMeiForm == "Wisphag") then
            fnCutscene([[ Append("Witch:[E|Upset] I'm more surprised that you wisphags can talk.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Offended] Don't change the subject, human.[P] What did you do to that dog?[B][C]") ]])
        elseif(sMeiForm == "Mannequin") then
            fnCutscene([[ Append("Witch:[E|Upset] I'm not going to patronize you, you ought to know a lot about petrification magic.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Offended] I haven't got any idea what you're talking about.[B][C]") ]])
            fnCutscene([[ Append("Witch:[E|Upset] You're a resinous mannequin.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Offended] And my arms are fully articulated, so explain what you did to that dog before I strangle you.[B][C]") ]])
        end
        fnCutscene([[ Append("Witch:[E|Neutral] I suppose I have not introduced myself.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] My name is Polaris![B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] ...[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Upset] Nothing?[P] No?[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] Should I know that name?[P] I'm not really from around here.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Upset] Well, I'm pretty famous...[P] if you go all the way to Yeto.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Sad] Ummm...[P] Is that far away?[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] Doesn't matter.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] Anyway, I've been working on a new technique to create realistic stone carvings.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] I create a plaster cast around someone, teleport them away and fill it from within.[P] Bam, two seconds and you've got a realistic statue![B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Happy] Oh![P] I see![P] So she's just a model![B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] Yes, that is correct![P] And let me tell you, I only model the world's cutest dogs![B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Upset] Oh, I see.[P] You thought I was turning Dot to stone.[P] I guess that makes sense.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Smirk] Yes, but you weren't, so you get to live.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Upset] How much do you know about petrification magic, miss...[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Smirk] Mei.[P] And -[P] zilch.[P] Nada.[P] Zero.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] Ah.[P] Well, if you turn something to stone, and that something does not [P]*want*[P] to be stone, they break out of it after a few minutes.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] You cannot force the soul into a vessel it refuses to be in, not forever.[P] With proper preparation you can turn someone to stone for a day or two, otherwise, you'll be lucky if it lasts thirty seconds.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Smirk] Huh.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Thinking] Then again, don't take my word for it.[P] There's a lot of academic discussion about the true limits of state change magic.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Thinking] For example, what if one takes advantage of the weakened state of one's soul after inflicting a state change to confuse the soul into believing their new body is the correct one?[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Thinking] I really must see if there's been anything published on that.[P] Herschel was working on it, I think, but he...[P] Oh, I wonder if he's still alive.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Smirk] I am not following this at all.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] It's all right.[P] Just thinking out loud.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] Well miss Mei, what brings you here?[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] I'm trying to find my way back home.[P] I was kidnapped, I think, and woke up not far from here.[B][C]") ]])
        if(sMeiForm == "Human") then
            fnCutscene([[ Append("Polaris:[E|Thinking] Hm.[P] And where are you from?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] Hong Kong.[B][C]") ]])
            fnCutscene([[ Append("Polaris:[E|Thinking] Never heard of -[P] never heard of...[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] ..?[B][C]") ]])
        elseif(sMeiForm == "Alraune") then
            fnCutscene([[ Append("Polaris:[E|Thinking] I was under the impression alraunes were mostly nomadic.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Happy] I've changed a bit since I got here, but no.[P] I'm trying to get back to Hong Kong.[B][C]") ]])
            fnCutscene([[ Append("Polaris:[E|Thinking] Never heard of -[P] never heard of...[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] ..?[B][C]") ]])
        elseif(sMeiForm == "Bee") then
            fnCutscene([[ Append("Polaris:[E|Thinking] Did you get cut off from your hive?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Happy] Ha ha![P] No, we're still in constant contact.[P] I'm just looking for a way back to my human home, in Hong Kong.[B][C]") ]])
            fnCutscene([[ Append("Polaris:[E|Thinking] Never heard of -[P] never heard of...[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] ..?[B][C]") ]])
        elseif(sMeiForm == "Ghost") then
            fnCutscene([[ Append("Polaris:[E|Thinking] Hm.[P] And where are you from?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] Hong Kong.[B][C]") ]])
            fnCutscene([[ Append("Polaris:[E|Thinking] Never heard of -[P] never heard of...[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] ..?[B][C]") ]])
        elseif(sMeiForm == "Werecat") then
            fnCutscene([[ Append("Polaris:[E|Thinking] I would assume the forest is the home of the werecat.[P] Is that not the case?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Happy] I've changed a bit since I got here, but no.[P] I'm trying to get back to Hong Kong.[B][C]") ]])
            fnCutscene([[ Append("Polaris:[E|Thinking] Never heard of -[P] never heard of...[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] ..?[B][C]") ]])
        elseif(sMeiForm == "Slime") then
            fnCutscene([[ Append("Polaris:[E|Thinking] Hm.[P] And where are you from?[P] Is there some sort of secret slime village?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] Hong Kong.[B][C]") ]])
            fnCutscene([[ Append("Polaris:[E|Upset] Darn.[P] There goes that theory.[B][C]") ]])
            fnCutscene([[ Append("Polaris:[E|Upset] Everyone at the academy thinks I'm crazy for believing slimes have permanent settlements.[P] One day, I'll show them.[B][C]") ]])
            fnCutscene([[ Append("Polaris:[E|Thinking] Still, Hong Kong...[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] ..?[B][C]") ]])
        elseif(sMeiForm == "Gravemarker") then
            fnCutscene([[ Append("Polaris:[E|Thinking] And just where are you from?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] Hong Kong.[B][C]") ]])
            fnCutscene([[ Append("Polaris:[E|Upset] Doesn't sound like any convent I've ever heard of.[B][C]") ]])
            fnCutscene([[ Append("Polaris:[E|Thinking] Still, Hong Kong...[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] ..?[B][C]") ]])
        elseif(sMeiForm == "Wisphag") then
            fnCutscene([[ Append("Polaris:[E|Neutral] Where are you from, then?[P] Not this swamp?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] Hong Kong.[P] Which, if memory serves, was built on a swamp.[P] Or a river estuary, whatever.[B][C]") ]])
            fnCutscene([[ Append("Polaris:[E|Thinking] Hong Kong...[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] Heard of it?[B][C]") ]])
        elseif(sMeiForm == "Mannequin") then
            fnCutscene([[ Append("Polaris:[E|Neutral] I've seen mannequins like you in shops in Jeffespeir.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] I'm not from...[P] there.[P] I'm from Hong Kong.[B][C]") ]])
            fnCutscene([[ Append("Polaris:[E|Thinking] Hong Kong...[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] You have heard the name?[B][C]") ]])
        end
        fnCutscene([[ Append("Polaris:[E|Neutral] Nothing.[P] Nevermind.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] (She knows something, holy cow she's bad at hiding it.)[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] Unfortunately, I'm not from around here myself.[P] But, I have some spare beds if you need to rest.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] Let me know if I can help you with anything else, Miss Mei.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Smirk] Thank you![P] I will.") ]])
        fnCutsceneBlocker()
        
        --Move Dot.
        fnCutsceneMove("Dot", 25.25, 26.50)
        fnCutsceneFace("Dot", 0, 1)
        fnCutsceneBlocker()
        fnAutoFoldParty()
        fnCutsceneBlocker()
        
    -- |[Florentina Is Here]|
    else
        
        --Flags.
        VM_SetVar("Root/Variables/Chapter1/Polaris/iFlorentinaMetPolaris", "N", 1.0)
        
        --Create a character to represent the animation.
        TA_Create("Animation")
            TA_SetProperty("Position", -100, -100)
            TA_SetProperty("Facing", gci_Face_South)
            TA_SetProperty("Clipping Flag", false)
            fnSetCharacterGraphics("Root/Images/Sprites/GenericF0/", false)
            TA_SetProperty("Wipe Special Frames")
            for i = 31, 49, 1 do
                TA_SetProperty("Add Special Frame", "Anim"..i-31, "Root/Images/CombatAnimations/ShadowB/" .. i)
            end
            TA_SetProperty("Rendering Depth", 0.0)
        DL_PopActiveObject()
        fnCutsceneLayerDisabled("StoneCorgi", true)
        fnCutsceneTeleport("Animation", -100, -100)
        
        --Movement.
        fnCutsceneMergeParty()
        fnCutsceneBlocker()
        
        --Move Mei into position.
        fnCutsceneMove("Mei", 28.25, 29.50)
        fnCutsceneMove("Florentina", 27.25, 29.50)
        fnCutsceneFace("Mei", 0, -1)
        fnCutsceneFace("Florentina", 0, -1)
        
        --Scene.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Polaris", "Neutral") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] All right, Dot.[P] Hold still, just like that...[P] good girl...") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Effect.
        fnCutscene([[ AudioManager_PlaySound("World|MagicA") ]])
        fnCutsceneTeleport("Animation", 27.25-11, 26.50-11)
        fnCutsceneSetFrame("Animation", "Anim0")
        fnCutsceneBlocker()
        for i = 49, 31, -1 do
            fnCutsceneWait(3)
            fnCutsceneSetFrame("Animation", "Anim"..i-31)
            fnCutsceneBlocker()
        end
        
        --Move Dot off stage.
        fnCutsceneLayerDisabled("StoneCorgi", false)
        fnCutsceneWait(25)
        fnCutsceneTeleport("Dot", -100, -100)
        fnCutsceneBlocker()
        
        --Finish the effect.
        fnCutscene([[ AudioManager_PlaySound("World|MagicB") ]])
        for i = 31, 49, 1 do
            fnCutsceneWait(3)
            fnCutsceneSetFrame("Animation", "Anim"..i-31)
            fnCutsceneBlocker()
        end
        fnCutsceneTeleport("Animation", -100, -100)
        fnCutsceneBlocker()
        
        --Pause.
        fnCutsceneWait(65)
        fnCutsceneBlocker()
        
        --Move.
        fnCutsceneMove("Mei", 28.25, 28.50)
        fnCutsceneMove("Florentina", 27.25, 28.50)
        fnCutsceneWait(65)
        fnCutsceneBlocker()
        fnCutsceneMove("Mei", 28.25, 27.50)
        fnCutsceneMove("Florentina", 27.25, 27.50)
        fnCutsceneWait(65)
        fnCutsceneBlocker()
        fnCutsceneFace("Polaris", 0, 1)
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Angry") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Confused") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Polaris", "Neutral") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] Oh, hello there![P] Lovely day, isn't it?[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Angry] Yes, lovely day in the swamp.[P] WHAT DID YOU DO TO THAT DOG?[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Confused] A capital crime, that's what.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Thinking] What do you mean?[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Confused] Oh nothing, just the DOG YOU TURNED INTO A ROCK.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] Huh?[P] No I didn't.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Angry] YES YOU DID![P] I SAW IT, THE STATUE IS RIGHT THERE![B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] ...[P] Hey, wait a minute.[P] Where did Dot go?[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] Uh oh...[P] uhh...[P] hey, did you see where Dot went?[P] She's my dog.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] Corgi, yellow and white.[P] You'd know her if you'd seen her.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Offended] ...[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] ...[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Confused] Are you some kind of idiot?[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Upset] If you haven't seen her, you can just say that.[P] Sheesh.") ]])
        fnCutsceneBlocker()
        
        --Movement.
        fnCutscene([[ AudioManager_PlaySound("World|DogBark") ]])
        fnCutsceneTeleport("Dot", 17.25, 15.50)
        fnCutsceneBlocker()
        fnCutsceneMove("Dot", 17.25, 26.50, 2.50)
        fnCutsceneMove("Dot", 26.25, 26.50, 2.50)
        fnCutsceneBlocker()
        fnCutscene([[ AudioManager_PlaySound("World|DogBark") ]])
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Offended") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Confused") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Polaris", "Neutral") ]])
        fnCutscene([[ Append("Witch:[E|Neutral] Oh, there you are![P] You had me worried![B][C]") ]])
        fnCutscene([[ Append("Witch:[E|Neutral] Who's a good girl![P] Who's a good girl![B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Sad] Uh, what's going on Florentina?[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Surprise] Don't look at me like I know what just happened.[B][C]") ]])
        fnCutscene([[ Append("Witch:[E|Neutral] Sorry about that.[P] The spell usually puts her right next to me.[B][C]") ]])
        fnCutscene([[ Append("Witch:[E|Neutral] There was a big spatial anomaly a while ago, might have put some things out of whack.[P] Better recalibrate everything.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Sad] So...[P] You didn't turn the dog to stone?[B][C]") ]])
        if(sMeiForm == "Human") then
            fnCutscene([[ Append("Witch:[E|Upset] You're not a cop, are you?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] Very far from it.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Offended] But mistreating a dog will put you on the wrong side of this sword.[B][C]") ]])
            fnCutscene([[ Append("Witch:[E|Neutral] ...[P] I respect you, madam.[P] Lethal force is justified for people who hurt dogs.[B][C]") ]])
            fnCutscene([[ Append("Witch:[E|Neutral] Luckily, that's not what I'm doing.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] So just what [P]*were*[P] you doing to this adorable corgi?[B][C]") ]])
        elseif(sMeiForm == "Alraune") then
            fnCutscene([[ Append("Witch:[E|Upset] What are you, some kind of plant cop?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] Very far from it.[P] You're thinking of Rochea...[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Offended] But mistreating a dog will put you on the wrong side of this sword.[B][C]") ]])
            fnCutscene([[ Append("Witch:[E|Neutral] ...[P] I respect you, madam. Lethal force is justified for people who hurt dogs.[B][C]") ]])
            fnCutscene([[ Append("Witch:[E|Neutral] Luckily, that's not what I'm doing.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] So just what [P]*were*[P] you doing to this adorable corgi?[B][C]") ]])
        elseif(sMeiForm == "Bee") then
            fnCutscene([[ Append("Witch:[E|Upset] What are you, a bee cop?[B][C]") ]])
            fnCutscene([[ Append("Witch:[E|Neutral] Hey wait, how are you talking?[P] Beegirls don't talk.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Blush] Uh, it's a long story?[B][C]") ]])
            fnCutscene([[ Append("Witch:[E|Thinking] If I were an entomologist, I'd want to hear it.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Offended] Don't change the subject.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Offended] So just what [P]*were*[P] you doing to this adorable corgi?[B][C]") ]])
        elseif(sMeiForm == "Ghost") then
            fnCutscene([[ Append("Witch:[E|Upset] Being dead doesn't stop you from being a cop, it seems.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Offended] If you made a new dog ghost, I'd make a new witch ghost, you get me?[B][C]") ]])
            fnCutscene([[ Append("Witch:[E|Neutral] I'd do it to myself first if I ever hurt Dot![P] She's my good girl![B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Offended] So just what [P]*were*[P] you doing to this adorable corgi?[B][C]") ]])
        elseif(sMeiForm == "Werecat") then
            fnCutscene([[ Append("Witch:[E|Upset] Cats don't make good narcs, kiddo.[P] Take it from me.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Offended] Hey, despite what you may have heard, cats and dogs are on the same team.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Offended] We're fluffy, we're cute, and we don't take any crap from abusive humans.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Offended] So what were you doing to this adorable corgi, miss?[B][C]") ]])
        end
        fnCutscene([[ Append("Witch:[E|Neutral] I suppose I have not introduced myself.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] My name is Polaris![B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] ...[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] Polaris?[P] Hey, I know you![B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] My fame precedes me![B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] You're the crazy lady who built a house in the swamp and was hawking dog statues a week ago![B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Upset] My fame precedes me...[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] Oh don't be like that.[P] Your work is top notch.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Upset] I ought not be famous for my carvings, but for my exploits.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] Sorry, but I'm not from around here, so I wouldn't know.[P] Are you famous here?[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Upset] If you asked someone in Yeto who Polaris was...[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Surprise] Oh, Mei?[P] Yeto is across the ocean to the west.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] When Mei here says she's not from around here, she really means it.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Upset] I can relate.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] Anyway, I've been working on a new technique to create realistic stone carvings.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] I create a plaster cast around someone, teleport them away and fill it from within.[P] Bam, two seconds and you've got a realistic statue![B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Happy] Oh![P] I see![P] So she's just a model![B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] Yes, that is correct![P] And let me tell you, I only model the world's cutest dogs![B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Upset] Oh, I see.[P] You thought I was turning Dot to stone.[P] I guess that makes sense.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Smirk] Yes, but you weren't, so you get to live.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Upset] How much do you know about petrification magic, miss Mei?[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] Nothing. In fact, if you had told me magic was real a few days ago, I'd have laughed at you.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] Ah.[P] Well, if you turn something to stone, and that something does not [P]*want*[P] to be stone, they break out of it after a few minutes.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] You cannot force the soul into a vessel it refuses to be in, not forever.[P] With proper preparation you can turn someone to stone for a day or two, otherwise, you'll be lucky if it lasts thirty seconds.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Smirk] Huh.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Thinking] Then again, don't take my word for it.[P] There's a lot of academic discussion about the true limits of state change magic.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Thinking] For example, what if one takes advantage of the weakened state of one's soul after inflicting a state change to confuse the soul into believing their new body is the correct one?[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Thinking] I really must see if there's been anything published on that.[P] Herschel was working on it, I think, but he...[P] Oh, I wonder if he's still alive.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Smirk] I am not following this at all.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] It's all right.[P] Just thinking out loud.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] Well miss Mei, what brings you here?[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] I'm trying to find my way back home.[P] I was kidnapped, I think, and woke up not far from here.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] I'm just lending a hand.[P] Totally altruistic, that.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Happy] Florentina gets a cut of the treasure we find![B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Blush] ...[P] Guilty as charged.[B][C]") ]])
        if(sMeiForm == "Human") then
            fnCutscene([[ Append("Polaris:[E|Thinking] Hm.[P] And where are you from?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] Hong Kong.[B][C]") ]])
            fnCutscene([[ Append("Polaris:[E|Thinking] Never heard of -[P] never heard of...[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] ..?[B][C]") ]])
        elseif(sMeiForm == "Alraune") then
            fnCutscene([[ Append("Polaris:[E|Thinking] I was under the impression alraunes were mostly nomadic.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Happy] I've changed a bit since I got here, but no.[P] I'm trying to get back to Hong Kong.[B][C]") ]])
            fnCutscene([[ Append("Polaris:[E|Thinking] Never heard of -[P] never heard of...[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] ..?[B][C]") ]])
        elseif(sMeiForm == "Bee") then
            fnCutscene([[ Append("Polaris:[E|Thinking] Did you get cut off from your hive?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Happy] Ha ha![P] No, we're still in constant contact.[P] I'm just looking for a way back to my human home, in Hong Kong.[B][C]") ]])
            fnCutscene([[ Append("Polaris:[E|Thinking] Never heard of -[P] never heard of...[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] ..?[B][C]") ]])
        elseif(sMeiForm == "Ghost") then
            fnCutscene([[ Append("Polaris:[E|Thinking] Hm.[P] And where are you from?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] Hong Kong.[B][C]") ]])
            fnCutscene([[ Append("Polaris:[E|Thinking] Never heard of -[P] never heard of...[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] ..?[B][C]") ]])
        elseif(sMeiForm == "Werecat") then
            fnCutscene([[ Append("Polaris:[E|Thinking] I would assume the forest is the home of the werecat.[P] Is that not the case?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Happy] I've changed a bit since I got here, but no.[P] I'm trying to get back to Hong Kong.[B][C]") ]])
            fnCutscene([[ Append("Polaris:[E|Thinking] Never heard of -[P] never heard of...[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] ..?[B][C]") ]])
        elseif(sMeiForm == "Slime") then
            fnCutscene([[ Append("Polaris:[E|Thinking] Hm.[P] And where are you from?[P] Is there some sort of secret slime village?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] Hong Kong.[B][C]") ]])
            fnCutscene([[ Append("Polaris:[E|Upset] Darn.[P] There goes that theory.[B][C]") ]])
            fnCutscene([[ Append("Polaris:[E|Upset] Everyone at the academy thinks I'm crazy for believing slimes have permanent settlements.[P] One day, I'll show them.[B][C]") ]])
            fnCutscene([[ Append("Polaris:[E|Thinking] Still, Hong Kong...[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] ..?[B][C]") ]])
        elseif(sMeiForm == "Gravemarker") then
            fnCutscene([[ Append("Polaris:[E|Thinking] And just where are you from?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] Hong Kong.[B][C]") ]])
            fnCutscene([[ Append("Polaris:[E|Upset] Doesn't sound like any convent I've ever heard of.[B][C]") ]])
            fnCutscene([[ Append("Polaris:[E|Thinking] Still, Hong Kong...[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] ..?[B][C]") ]])
        elseif(sMeiForm == "Wisphag") then
            fnCutscene([[ Append("Polaris:[E|Neutral] Where are you from, then?[P] Not this swamp?[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] Hong Kong.[P] Which, if memory serves, was built on a swamp.[P] Or a river estuary, whatever.[B][C]") ]])
            fnCutscene([[ Append("Polaris:[E|Thinking] Hong Kong...[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Smirk] Heard of it?[B][C]") ]])
        elseif(sMeiForm == "Mannequin") then
            fnCutscene([[ Append("Polaris:[E|Neutral] I've seen mannequins like you in shops in Jeffespeir.[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] I'm not from...[P] there.[P] I'm from Hong Kong.[B][C]") ]])
            fnCutscene([[ Append("Polaris:[E|Thinking] Hong Kong...[B][C]") ]])
            fnCutscene([[ Append("Mei:[E|Neutral] You have heard the name?[B][C]") ]])
        end
        fnCutscene([[ Append("Polaris:[E|Neutral] Nothing.[P] Nevermind.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] (She knows something, holy cow she's bad at hiding it.)[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] Unfortunately, I'm not from around here myself.[P] But, I have some spare beds if you need to rest.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] Let me know if I can help you with anything else, Miss Mei.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Smirk] Thank you![P] I will.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] I happen to be a merchant, miss Polaris.[P] I was wondering...[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] Oh, you want to talk business?[P] I'd be delighted![P] But first, rest, if you are so inclined.") ]])
        fnCutsceneBlocker()
        
        --Move Dot.
        fnCutsceneMove("Dot", 25.25, 26.50)
        fnCutsceneFace("Dot", 0, 1)
        fnCutsceneBlocker()
        fnAutoFoldParty()
        fnCutsceneBlocker()
    end

-- |[ ==================================== Gravemarker Scene =================================== ]|
elseif(sObjectName == "GravemarkerScene") then
    
    --Variables.
    local iNoticedFormChange = VM_GetVar("Root/Variables/Chapter1/Polaris/iNoticedFormChange", "N")
	fnCutscene([[ AL_SetProperty("Music", "Null") ]])
    
    --Flags.
    VM_SetVar("Root/Variables/Chapter1/Polaris/iNoticedFormChange", "N", 1.0)
    local iPolarisRuneQuestState = VM_GetVar("Root/Variables/Chapter1/Polaris/iPolarisRuneQuestState", "N")
    if(iPolarisRuneQuestState < 1) then
        VM_SetVar("Root/Variables/Chapter1/Polaris/iPolarisRuneQuestState", "N", 1.0)
    end
    
    --Topic unlocks.
    WD_SetProperty("Unlock Topic", "Polaris_Gravemarkers", 1)
    WD_SetProperty("Unlock Topic", "Polaris_Runestone", 1)
    
    --Snap to black.
    fnCutscene([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()

    --Position all the actors.
    fnCutsceneTeleport("Mei", -1.25, -1.50)
    fnCutsceneTeleport("Florentina", -1.25, -1.50)
    fnCutsceneTeleport("Polaris", 36.25, 21.50)
    fnCutsceneFace("Polaris", 1, 0)
    fnCutsceneTeleport("Dot", 38.25, 21.50)
    fnCutsceneFace("Dot", -1, 0)
    
    --Position the camera.
    Cutscene_CreateEvent("CameraEvent", "Camera")
        CameraEvent_SetProperty("Max Move Speed", 5.0)
        CameraEvent_SetProperty("Focus Position", (37.25 * gciSizePerTile), (21.50 * gciSizePerTile))
    DL_PopActiveObject()
    fnCutsceneBlocker()
    
    --Layers.
    fnCutsceneLayerDisabled("FloorDetectorBroken", true)
    fnCutsceneLayerDisabled("FloorDetectorUnbroken", false)
    
    --Wait a bit.
    fnCutsceneWait(45)
    fnCutsceneBlocker()

    --Fade in.
    fnCutsceneWait(45)
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Polaris", "Neutral") ]])
    fnCutscene([[ Append("Polaris:[E|Neutral] All right Dot, it only took six hours, but we finally got the spatial thread monitor up![B][C]") ]])
    fnCutscene([[ Append("Polaris:[E|Upset] Next time don't run off with the focusing crystal and it won't take so darn long.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneMove("Dot", 38.25, 19.50, 2.0)
    fnCutsceneBlocker()
    fnCutsceneFaceTarget("Polaris", "Dot")
    fnCutsceneMove("Dot", 35.25, 19.50, 2.0)
    fnCutsceneBlocker()
    fnCutsceneFaceTarget("Polaris", "Dot")
    fnCutsceneMove("Dot", 35.25, 21.50, 2.0)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|DogBark") ]])
    fnCutsceneFaceTarget("Polaris", "Dot")
    fnCutsceneMove("Dot", 35.25, 23.50, 2.0)
    fnCutsceneBlocker()
    fnCutsceneFaceTarget("Polaris", "Dot")
    fnCutsceneMove("Dot", 38.25, 23.50, 2.0)
    fnCutsceneBlocker()
    fnCutsceneFaceTarget("Polaris", "Dot")
    fnCutsceneMove("Dot", 38.25, 21.50, 2.0)
    fnCutsceneBlocker()
    fnCutsceneFace("Dot", 1, 0)
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneFace("Dot", -1, 0)
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneFace("Dot", 1, 0)
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneFace("Dot", -1, 0)
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|DogBark") ]])
    fnCutsceneFace("Dot", 1, 0)
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneFace("Dot", -1, 0)
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneFace("Dot", 1, 0)
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneFace("Dot", -1, 0)
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Polaris", "Neutral") ]])
    fnCutscene([[ Append("Polaris:[E|Neutral] Okay okay, let's get this sucker fired up![B][C]") ]])
    fnCutscene([[ Append("Polaris:[E|Thinking] I wonder why a place like this would have such odd spatial disturbances, though.[P] And only in the past little while...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    --Flash to white.
    fnCutscene([[ AudioManager_PlaySound("World|Flash") ]])
    fnCutscene([[ AL_SetProperty("Activate Fade", 15, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 1, 1, 1, 1) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(125)
    fnCutsceneBlocker()
    fnCutsceneTeleport("Mei", 36.25, 19.50)
    fnCutsceneFace("Mei", 0, 1)
    fnCutsceneSetFrame("Mei", "Crouch")
    fnCutsceneTeleport("Florentina", 38.25, 19.50)
    fnCutsceneFace("Florentina", 1, -1)
    fnCutsceneBlocker()
    fnCutsceneLayerDisabled("FloorDetectorBroken", false)
    fnCutsceneLayerDisabled("FloorDetectorUnbroken", true)
    fnCutscene([[ AudioManager_PlaySound("World|BigGlassShatter") ]])

    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Polaris:[VOICE|Polaris] Argh![P] Dot, Dot, did you bite the zappy crystal again![P] Dot!?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|DogBark") ]])
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Polaris:[VOICE|Polaris] Oh good, I'd hate to have to recombobulate you yet again...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Fade in.
    fnCutsceneWait(45)
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 1, 1, 1, 1, 1, 1, 1, 0) ]])
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneFace("Polaris", 0, -1)
    fnCutsceneFace("Dot", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneSetFrame("Florentina", "Crouch")
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneSetFrame("Florentina", "Null")
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Florentina", -1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
	fnCutscene([[ AL_SetProperty("Music", "StarfieldSwamp") ]])
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("Florentina:[E|Surprise] Hey, I cheated death again![P] You okay, Mei?[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Sad] I will be fine.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Movement.
    fnCutsceneSetFrame("Mei", "Null")
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Mei", -1, 0)
    fnCutsceneWait(10)
    fnCutsceneBlocker()
    fnCutsceneFace("Mei", 1, 0)
    fnCutsceneWait(10)
    fnCutsceneBlocker()
    fnCutsceneFace("Mei", -1, 0)
    fnCutsceneWait(10)
    fnCutsceneBlocker()
    fnCutsceneFace("Mei", 1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Mei", 0, 1)
    fnCutsceneFace("Florentina", 0, 1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Setup.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Polaris", "Neutral") ]])
    
    --Polaris already knew about Mei's runestone:
    if(iNoticedFormChange == 1.0) then
        fnCutscene([[ Append("Polaris:[E|Upset] You two again.[P] I guess I should have known.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Upset] At least I figured out what the source of those spatial shocks was.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] Miss Polaris?[P] Are we at your cabin?[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] Yes, and judging by your wings, you decided to poke around in that ruin.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Happy] How could we not?[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Thinking] So let's see here.[P] The legendary runestones are supposed to have the power of transformation and translocation.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Upset] I think that they ought to be a bit more graceful than the equivalent of jumping off a cliff, though.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Sad] I'm very sorry.[P] I didn't mean to.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Sad] When I am in great danger, my runestone can warp me to safety.[P] I don't know how I know that, it's just something I know.[P] It's there to save me.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Sad] But this time, I held it close and prepared, and I felt like I had run into a wall.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] If you look down you will find the cause of your little conundrum.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] This is a spatial thread monitor.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Upset] Or it was, anyway.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Sad] Sorry...[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] No no, it's not a problem.[P] On the bright side, this is a very important piece of information you gave me.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Thinking] Normally, a rupture in a spatial thread is created by something moving through the gaps between space.[P] This device detects them, snaps around them, and then the crystal moves and the thread unwinds.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Thinking] You, however, pulled the thread so hard as to shatter the crystal.[P] Incredible, actually.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Thinking] It means that, if anyone tries to 'catch' you during a teleport, you'll probably blow them into bits.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Surprise] Oh no![P] I don't want to blow anyone up![B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] Not even those statues that tried to kill us?[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Offended] No![P] They are simply doing what they think is right.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] Could I ask you about them, actually?[P] I'd love to get some first-hand testimony on them.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Smirk] Sure, but first, can I replace your device for you?[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Upset] Seriously, don't worry about it.[P] These things happen.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Happy] Don't offer to pay for it, Mei.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Smirk] Please, I can pay to replace it.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Facepalm] ...[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Upset] I can get a dozen more if I want to, Mei.[P] Stop worrying.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Sad] All right, okay.[P] I just feel bad about it.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] Worst-case, I'll just take it out of your share of our earnings.[P] I can probably get the parts shipped here.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] At least I can check off one mystery.[P] So, since you're here, can I ask you about your new form?[B][C]") ]])
        fnCutscene([[ WD_SetProperty("Activate Topics After Dialogue", "Polaris") ]])
        
    --Polaris didn't know about Mei's runestone:
    else
        VM_SetVar("Root/Variables/Chapter1/Scenes/iFlorentinaKnowsAboutRune", "N", 1.0)
        fnCutscene([[ Append("Polaris:[E|Upset] Now that answers one question and asks a lot more.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Upset] At least I figured out what the source of those spatial shocks was.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] Miss Polaris?[P] Are we at your cabin?[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] Yes, and judging by your wings, you decided to poke around in that ruin.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Happy] How could we not?[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Thinking] Guess there's no point playing dumb now.[P] Mei, that runestone of yours allows you to transform and warp, doesn't it?[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Sad] You already knew?[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Thinking] There's a legend from a dead empire about a hero who will come bearing a runestone like that.[P] But, the symbol on yours is different.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Thinking] I thought it was draconic at first, and maybe it is, but I don't recognize the rune.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] The great hero will defeat the evil that was sealed at the Roof of the World, the highest mountain in Trafal.[P] The hero can transform herself and can warp through non-magical means.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Thinking] But the great hero will have the name 'Son-ja', and that clearly isn't you.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Thinking] I wonder if there are others like you?[P] Hmmm...[P] I'll see if I can find anything out about other runestones...[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Sad] But why didn't you mention this earlier?[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] As I said, your rune is different.[P] I was worried I'd give you false hope.[P] But now I've seen you can transform and warp.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] You're not stuck in that statue body, are you?[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] No.[P] I can change if I want to, by holding the runestone and concentrating.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] Good, because your wings give me the creeps.[P] They're rock but they're flexible, and I hate that.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] See, I set up this detector because I kept noticing little spatial fluctuations.[P] Nothing big, but definitely there.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Upset] And then you blew the darn thing right up...[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Sad] I'm very sorry.[P] I didn't mean to.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Sad] I was going to warp by holding it, but suddenly it was like running into a brick wall.[P] It had never felt like that before.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] Yeah, you slammed right into it.[P] Look down.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] This is a spatial thread monitor.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Upset] Or it was, anyway.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Sad] Sorry...[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] No no, it's not a problem.[P] On the bright side, this is a very important piece of information you gave me.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Thinking] Normally, a rupture in a spatial thread is created by something moving through the gaps between space.[P] This device detects them, snaps around them, and then the crystal moves and the thread unwinds.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Thinking] You, however, pulled the thread so hard as to shatter the crystal.[P] Incredible, actually.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Thinking] It means that, if anyone tries to 'catch' you during a teleport, you'll probably blow them into bits.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Surprise] Oh no![P] I don't want to blow anyone up![B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] Not even those statues that tried to kill us?[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Offended] No![P] They are simply doing what they think is right.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] Could I ask you about them, actually?[P] I'd love to get some first-hand testimony on them.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Smirk] Sure, but first, can I replace your device for you?[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Upset] Seriously, don't worry about it.[P] These things happen.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Happy] Don't offer to pay for it, Mei.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Smirk] Please, I can pay to replace it.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Facepalm] ...[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Upset] I can get a dozen more if I want to, Mei.[P] Stop worrying.[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Sad] All right, okay.[P] I just feel bad about it.[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] Worst-case, I'll just take it out of your share of our earnings.[P] I can probably get the parts shipped here.[B][C]") ]])
        fnCutscene([[ Append("Polaris:[E|Neutral] At least I can check off one mystery.[P] So, since you're here, can I ask you about your new form?[B][C]") ]])
        fnCutscene([[ WD_SetProperty("Activate Topics After Dialogue", "Polaris") ]])
    end
    fnAutoFoldParty()
    fnCutsceneBlocker()
    
-- |[ ================================= Turn Polaris To Rubber ================================= ]|
elseif(sObjectName == "RubberPolaris") then

    --Repeat check.
    local iIsRubberMode = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iIsRubberMode", "N")
    local iRubberedPolaris = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iRubberedPolaris", "N")
    if(iIsRubberMode == 0.0 or iRubberedPolaris == 1.0) then return end
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter1/Scenes/Rubber|iRubberedPolaris", "N", 1.0)
    
    --Movement.
    fnCutsceneMergeParty()
    fnCutsceneBlocker()
    fnCutsceneMove("Mei", 26.25, 20.50)
    fnCutsceneMove("Mei", 28.25, 20.50)
    fnCutsceneMove("Mei", 28.25, 19.50)
    fnCutsceneFace("Mei", 0, -1)
    fnCutsceneMove("Rubberraune", 25.25, 23.50)
    fnCutsceneFace("Rubberraune", 0, -1)
    fnCutsceneMove("Rubberbee", 27.25, 23.50)
    fnCutsceneFace("Rubberbee", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Dot", 0, 1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dot notices!
    fnCutscene([[ AudioManager_PlaySound("World|DogBark") ]])
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Polaris", "Neutral") ]])
    fnCutscene([[ Append("Polaris:[E|Upset] Dot, calm down![P] Dinner is almost ready![B][C]") ]])
    fnCutscene([[ Append("Polaris:[E|Neutral] ...[B][C]") ]])
    fnCutscene([[ Append("Polaris:[E|Neutral] What the - [P][CLEAR]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] [SOUND|Rubber|SpreadB](Spread...)[B][C]") ]])
    fnCutscene([[ Append("Polaris:[E|Neutral] Aahh![B][C]") ]])
    fnCutscene([[ Append("Polaris:[E|Rubber] (Rubber![P] I'm covered in rubber, again![P] Gotta - )[B][C]") ]])
    fnCutscene([[ Append("Polaris:[E|Rubber] [SOUND|Rubber|ChangeA](Fight it...[P] no...[P] spread...)") ]])
    fnCutsceneBlocker()
    
    --Transform Polaris to rubber.
    local sString =      "EM_PushEntity(\"Polaris\")\n"
    sString = sString .. "    fnSetCharacterGraphics(\"Root/Images/Sprites/PolarisR/\", false)\n"
    sString = sString .. "DL_PopActiveObject()"
    fnCutscene(sString)
    fnCutsceneFace("Polaris", 0, 1)
    
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Polaris", "Rubber") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] (This thrall has magical power...[P] good...)[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] (You will be a useful thrall...)[B][C]") ]])
    fnCutscene([[ Append("Polaris:[E|Rubber] (Yes...[P] I will be useful...[P] spread...)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnAutoFoldParty()
    fnCutsceneBlocker()
    
-- |[ ================================== Failed Rubber Finale ================================== ]|
elseif(sObjectName == "FailRubber") then

    --Snap to black.
    fnCutscene([[ AL_SetProperty("Activate Fade", 0, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Execute a rest action.
    fnCutscene([[ AM_SetProperty("Execute Rest") ]])
    
    --Switch Mei back to human.
    VM_SetVar("Root/Variables/Chapter1/Scenes/Rubber|iIsRubberMode", "N", 0.0)
    LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Human.lua")
    
    --Position actors.
    fnCutsceneTeleport("Mei", 22.25, 19.50)
    fnCutsceneFace("Mei", -1, 0)
    fnCutsceneTeleport("Polaris", 19.25, 19.50)
    fnCutsceneFace("Polaris", 1, 0)
    fnCutsceneTeleport("Dot", 28.25, 18.50)
    fnCutsceneFace("Dot", 0, -1)
    
    --Spawn Florentina, Blythe, and Nadia.
    fnAddPartyMember("Florentina")
    fnCutsceneTeleport("Florentina", 20.25, 18.50)
    fnCutsceneFace("Florentina", 0, 1)
    TA_Create("Nadia")
        TA_SetProperty("Position", 21, 18)
        TA_SetProperty("Facing", gci_Face_South)
        fnSetCharacterGraphics("Root/Images/Sprites/Nadia/", false)
    DL_PopActiveObject()
    TA_Create("Blythe")
        TA_SetProperty("Position", 21, 20)
        TA_SetProperty("Facing", gci_Face_North)
        fnSetCharacterGraphics("Root/Images/Sprites/Blythe/", false)
    DL_PopActiveObject()
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    
    --Fade in.
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Polaris", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Blythe", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Nadia", "Neutral") ]])
    fnCutscene([[ Append("Polaris:[E|Neutral] Luckily, they hadn't noticed my little shack here, or we'd have been in some real trouble.[B][C]") ]])
    fnCutscene([[ Append("Blythe:[E|Neutral] I had only mustered a dozen troops.[P] Fortunately, your magical rainstorm was plenty.[P] Thank you.[B][C]") ]])
    fnCutscene([[ Append("Nadia:[E|Neutral] Meanwhile, Florry and I were chasing a trader halfway across the province, ha ha![B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] How many traders wear a grey outfit, though?[P] That's not my fault.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] You still don't remember anything, Mei?[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Sad] No, nothing.[P] We crossed the logs on the river, and I can vaguely remember a tower, but that's the end of it.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] Darn.[P] And it's probably not a good idea to go snooping back in there to find whatever did that to you.[B][C]") ]])
    fnCutscene([[ Append("Nadia:[E|Neutral] Should we tell miss Vendril about it, Cap'n?[B][C]") ]])
    fnCutscene([[ Append("Blythe:[E|Neutral] That will be our next objective, Nadia.[P] Polaris, how far did your storm extend?[B][C]") ]])
    fnCutscene([[ Append("Polaris:[E|Upset] I may have overdone it, actually.[P] Apparently a hurricane hit Jeffespeir an hour ago, and the mage's guild is pretty ticked.[B][C]") ]])
    fnCutscene([[ Append("Polaris:[E|Neutral] None of you had better rat me out, I don't want to make enemies with the mage's guild there.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] I'm pretty good at laying low when there's a price on my head, this is the perfect place to do it.[B][C]") ]])
    fnCutscene([[ Append("Blythe:[E|Neutral] Very good.[P] Nadia, we have to get going.[P] Thank you again for your help, Polaris.[P] The people of Trannadar are in your debt.[B][C]") ]])
    fnCutscene([[ Append("Polaris:[E|Neutral] Oh it was nothing.[P] Add it to the list of times I saved the world.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Nadia and Blythe leave.
    fnCutsceneMove("Nadia", 27.25, 18.50)
    fnCutsceneMove("Blythe", 26.25, 20.50)
    fnCutsceneMove("Blythe", 26.25, 30.50)
    fnCutsceneBlocker()
    fnCutsceneTeleport("Blythe", -1.25, -1.50)
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Dot!
    fnCutsceneFace("Dot", -1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|DogBark") ]])
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Blythe:[VOICE|Blythe] Nadia![B][C]") ]])
    fnCutscene([[ Append("Nadia:[VOICE|Nadia] Coming, cap'n!") ]])
    fnCutsceneBlocker()
    fnCutsceneMove("Nadia", 26.25, 30.50)
    fnCutsceneBlocker()
    fnCutsceneTeleport("Nadia", -1.25, -1.50)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Polaris", "Neutral") ]])
    fnCutscene([[ Append("Polaris:[E|Neutral] Let me tell you, I don't think I've ever met someone who gets into trouble as much as you![B][C]") ]])
    fnCutscene([[ Append("Polaris:[E|Upset] (Reminds of me Prow, actually...)[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Sad] I'm really sorry.[P] I just don't remember anything at all.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] It's a good thing you came along when you did.[B][C]") ]])
    fnCutscene([[ Append("Polaris:[E|Neutral] I guess I'll add the rubber tower as yet another place worthy of investigation around Trannadar.[B][C]") ]])
    fnCutscene([[ Append("Polaris:[E|Neutral] This place is full of hazards, isn't it?[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Happy] That's why I love it here.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Happy] Me too![P] Never a dull moment![B][C]") ]])
    fnCutscene([[ Append("Polaris:[E|Neutral] Just don't go messing with that rubber stuff, okay?[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Blush] Aha, well, of course not.[P] It won't find me a way home, that's for sure![B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] (But I wonder...)[B][C]") ]])
    fnCutscene([[ Append("Narrator: (The rubber event can be replayed if you desire.[P] There may yet be a way to succeed!)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Fold party.
    fnCutsceneMove("Mei", 22.25, 18.50)
    fnCutsceneBlocker()
    fnAutoFoldParty()
    fnCutsceneBlocker()
    
-- |[ ================================ Successful Rubber Finale ================================ ]|
elseif(sObjectName == "PostRubber") then

    --Snap to black.
    fnCutscene([[ AL_SetProperty("Activate Fade", 0, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Switch Mei back to human.
    VM_SetVar("Root/Variables/Chapter1/Scenes/Rubber|iIsRubberMode", "N", 0.0)
    LM_ExecuteScript(gsRoot .. "FormHandlers/Mei/Form_Human.lua")
    
    --Execute a rest action.
    fnCutscene([[ AM_SetProperty("Execute Rest") ]])
    
    --Flag.
    VM_SetVar("Root/Variables/Chapter1/Scenes/Rubber|iRubberVictory", "N", 1.0)
    
    --Position actors.
    fnCutsceneTeleport("Mei", 22.25, 19.50)
    fnCutsceneFace("Mei", -1, 0)
    fnCutsceneTeleport("Polaris", 19.25, 19.50)
    fnCutsceneFace("Polaris", 1, 0)
    fnCutsceneTeleport("Dot", 28.25, 18.50)
    fnCutsceneFace("Dot", 0, -1)
    
    --Spawn Florentina, Blythe, and Nadia.
    fnAddPartyMember("Florentina")
    fnCutsceneTeleport("Florentina", 20.25, 18.50)
    fnCutsceneFace("Florentina", 0, 1)
    TA_Create("Nadia")
        TA_SetProperty("Position", 21, 18)
        TA_SetProperty("Facing", gci_Face_South)
        fnSetCharacterGraphics("Root/Images/Sprites/Nadia/", false)
    DL_PopActiveObject()
    TA_Create("Blythe")
        TA_SetProperty("Position", 21, 20)
        TA_SetProperty("Facing", gci_Face_North)
        fnSetCharacterGraphics("Root/Images/Sprites/Blythe/", false)
    DL_PopActiveObject()
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    
    --Fade in.
    fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 5, "Polaris", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Blythe", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 3, "Nadia", "Neutral") ]])
    fnCutscene([[ Append("Blythe:[E|Neutral] The troops and I just found you all lying in the field.[P] You don't remember anything?[B][C]") ]])
    fnCutscene([[ Append("Polaris:[E|Upset] Not at all.[P] I went to bed here and woke up out there.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Sad] I don't remember anything...[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] Nadia and I were chasing after a merchant wearing grey who we thought was Mei...[B][C]") ]])
    fnCutscene([[ Append("Nadia:[E|Neutral] After that, we woke up in the field.[B][C]") ]])
    fnCutscene([[ Append("Blythe:[E|Neutral] I got similar stories from all the others in the field, too.[P] What in the wastes happened?[B][C]") ]])
    fnCutscene([[ Append("Nadia:[E|Neutral] The little ones didn't know, either...[B][C]") ]])
    fnCutscene([[ Append("Blythe:[E|Neutral] Mei, you said the last thing you remember was crossing the logs north of the fishing village.[B][C]") ]])
    fnCutscene([[ Append("Blythe:[E|Neutral] I'll go give the guards at the village a warning, and we'll see if anyone else saw anything, but this mystery only deepens.[B][C]") ]])
    fnCutscene([[ Append("Polaris:[E|Upset] I don't like it, at all.[P] It might be a good idea to leave the west part of the province alone.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] Maybe...[B][C]") ]])
    fnCutscene([[ Append("Blythe:[E|Neutral] Come along then, Nadia.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Nadia and Blythe leave.
    fnCutsceneMove("Nadia", 27.25, 18.50)
    fnCutsceneMove("Blythe", 26.25, 20.50)
    fnCutsceneMove("Blythe", 26.25, 30.50)
    fnCutsceneBlocker()
    fnCutsceneTeleport("Blythe", -1.25, -1.50)
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    --Dot!
    fnCutsceneFace("Dot", -1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscene([[ AudioManager_PlaySound("World|DogBark") ]])
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Blythe:[VOICE|Blythe] Nadia![B][C]") ]])
    fnCutscene([[ Append("Nadia:[VOICE|Nadia] Coming, cap'n!") ]])
    fnCutsceneBlocker()
    fnCutsceneMove("Nadia", 26.25, 30.50)
    fnCutsceneBlocker()
    fnCutsceneTeleport("Nadia", -1.25, -1.50)
    fnCutsceneBlocker()
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Polaris", "Neutral") ]])
    fnCutscene([[ Append("Polaris:[E|Neutral] Stay away from the tower.[P] The master needs time to think.[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] The master needs time to think.[P] It's not time to spread.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] Huh?[P] What did you two say?[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] Forget that.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Neutral] ...[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Confused] Woah.[P] Did I just zone out?[P] Were you talking to me?[B][C]") ]])
    fnCutscene([[ Append("Mei:[E|Neutral] We were just about to get back to the adventure, Florentina.[B][C]") ]])
    fnCutscene([[ Append("Florentina:[E|Happy] Oh, right.[P] Let's go.[B][C]") ]])
    fnCutscene([[ Append("Narrator: (The rubber quest is completed![P] You can now use the Rubber Queen costume for Mei, a costume for her human form.)[B][C]") ]])
    fnCutscene([[ Append("Narrator: (For future playthroughs, you can unlock this form with the password 'Jiggly Squeaky' from the campfire menu.)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Fold party.
    fnCutsceneMove("Mei", 22.25, 18.50)
    fnCutsceneBlocker()
    fnAutoFoldParty()
    fnCutsceneBlocker()
    
    --Costume unlocked.
    VM_SetVar("Root/Variables/Costumes/Mei/iRubberQueen", "N", 1.0)

end
