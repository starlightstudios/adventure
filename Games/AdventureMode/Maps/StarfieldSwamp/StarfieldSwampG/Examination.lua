-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ==================================== Special Instance ==================================== ]|
local iIsRubberMode = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iIsRubberMode", "N")
if(iIsRubberMode == 1.0) then return end

-- |[ ========================================= Exits ========================================== ]|
if(sObjectName == "SampleExit") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("LevelName", "FORCEPOS:28.0x16.0x0")
    
-- |[ ====================================== Examinables ======================================= ]|
elseif(sObjectName == "BookshelfA") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Magical Theory and Electromagnetism'.[P] This book is way over my head.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookshelfB") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Hardening, State Changes, Polymorphism, and Partirhumans. The Collected Papers.'[P] Hundreds of mini-essays about various magical scientific topics.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookshelfC") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Autonomous Non-Living Constructs:: Beyond the Impossible'.[P] The book seems to be trying to argue that robots are impossible.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "BookshelfD") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] ('Politics in Trannadar:: What You Need to Know So You Don't Get Tried For Treason'.[P] Full of useful advice like not stealing or murdering!)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "CookingPot") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (I think Polaris is mixing some chocolate, but I know better than to sample something a witch is cooking.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Barrel") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The barrel is full of wine.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "OtherBarrel") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Empty barrels.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "SpareBeds") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (Spare beds to be put out for guests.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Crayfish") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (The barrels have dried crayfish in them for later cooking.)") ]])
    fnCutsceneBlocker()
    
elseif(sObjectName == "Statue") then
    
    --Boop Paper.
    local iPaperCount = AdInv_GetProperty("Item Count", "Booped Paper")
    local iTakenPieJob = VM_GetVar("Root/Variables/Chapter1/Breanne/iTakenPieJob", "N")
    if(iTakenPieJob == 1.0 and iPaperCount < 1) then
            
        --Short scene.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] Okay, give me a piece of paper...[B][C]") ]])
        fnCutscene([[ Append("Florentina:[E|Neutral] Uh, Mei?[P] Why would you boop a statue's nose when there's a real-life corgi right there?[B][C]") ]])
        fnCutscene([[ Append("Mei:[E|Neutral] ...[P] Right.") ]])
        fnCutsceneBlocker()

    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Thought:[VOICE|Leader] (A statue of Dot, the adorable corgi![P] I'm having a heart attack just looking at how cute she is![P] Call for help I'm dying!)") ]])
        fnCutsceneBlocker()
    end

-- |[ ========================================== Other ========================================= ]|
-- |[Skillbook]|
elseif(sObjectName == "Skillbook") then
    AL_SetProperty("Set Layer Disabled", "SkillbookIco", true)
	LM_ExecuteScript(gsMeiSkillbook, 5)
    
-- |[ ========================================== Debug ========================================= ]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
