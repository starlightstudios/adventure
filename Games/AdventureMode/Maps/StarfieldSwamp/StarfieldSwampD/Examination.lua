-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= Exits ========================================== ]|
if(sObjectName == "SampleExit") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("LevelName", "FORCEPOS:28.0x16.0x0")
    
-- |[ ======================================= Whirlpools ======================================= ]|
--Other whirlpools require wisphag form.
elseif(sObjectName == "WhirlpoolA") then

    --Can't use in rubber mode or if wisphag TF is not found.
    local iIsRubberMode = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iIsRubberMode", "N")
    local iHasWisphagForm = VM_GetVar("Root/Variables/Global/Mei/iHasWisphagForm", "N")
    if(iIsRubberMode == 1.0 or iHasWisphagForm == 0.0) then return end

    --Jump in and move.
    fnExecWaterJumpToPos(75+gcfWhirl2x2OffY, 47+gcfWhirl2x2OffY, 11+gcfWhirl2x2OffX, 53+gcfWhirl2x2OffY, 13.25, 54.00)

elseif(sObjectName == "WhirlpoolB") then
    fnExecWaterJumpToPos(11+gcfWhirl2x2OffX, 53+gcfWhirl2x2OffY, 75+gcfWhirl2x2OffY, 47+gcfWhirl2x2OffY, 75.75, 46.50)

elseif(sObjectName == "WhirlpoolC") then

    --Can't use in rubber mode or if wisphag TF is not found.
    local iIsRubberMode = VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iIsRubberMode", "N")
    local iHasWisphagForm = VM_GetVar("Root/Variables/Global/Mei/iHasWisphagForm", "N")
    if(iIsRubberMode == 1.0 or iHasWisphagForm == 0.0) then return end
    
    fnExecWaterJumpToMap(51+gcfWhirl2x2OffX, 15+gcfWhirl2x2OffX, "WisphagSwampB", 1, 1)

-- |[ ========================================== Other ========================================= ]|
-- |[ ========================================== Debug ========================================= ]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
