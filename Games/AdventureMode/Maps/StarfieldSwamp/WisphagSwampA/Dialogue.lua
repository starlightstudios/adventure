-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[Name Resolve]|
    --Resolve the name of the calling entity.
    local sActorName = TA_GetProperty("Name")

    --Set facing.
    TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[NPCName]|
    if(sActorName == "Wisphag") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Wisphag:[VOICE|Wisphag] Hsss...[B][C]") ]])
        fnCutscene([[ Append("Mei:[VOICE|Mei] Uh, friends?[B][C]") ]])
        fnCutscene([[ Append("Wisphag:[VOICE|Wisphag] Hsss![B][C]") ]])
        fnCutscene([[ Append("Mei:[VOICE|Mei] Pretty sure that was a yes!") ]])
        fnCutsceneBlocker()
    elseif(sActorName == "WispA") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Mei:[VOICE|Mei] Hello, buddy![P] It seems you were an artist once?[P] You liked to draw big and elaborate eyes?[P] Good for you!") ]])
        fnCutsceneBlocker()
    elseif(sActorName == "WispB") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Mei:[VOICE|Mei] Hi there?[P] Oh, a baker?[P] I would have loved to have tried your sweets!") ]])
        fnCutsceneBlocker()
    elseif(sActorName == "WispC") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Mei:[VOICE|Mei] You don't really know who you were?[P] I'm sure you can make a new...[P] whatever it is the next life has.[P] Whenever you're ready to move on!") ]])
        fnCutsceneBlocker()
    end
end
