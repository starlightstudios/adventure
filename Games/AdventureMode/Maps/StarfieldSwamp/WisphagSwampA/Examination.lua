-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= Exits ========================================== ]|
if(sObjectName == "Whirlpool") then
    
    -- |[ ========== Relive Handler ========== ]|
    --During a relive, perform the jump but warp back to the last savepoint.
    local iIsRelivingScene         = VM_GetVar("Root/Variables/Chapter1/Scenes/iIsRelivingScene", "N")
    if(iIsRelivingScene == 1.0) then
        
        -- |[Camera]|
        fnCutsceneCamera("Position", gcfCamera_Cutscene_Speed, 12+gcfWhirl3x2OffX, 22+gcfWhirl3x2OffY)
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    
        -- |[Jump Party In]|
        --Create lits of members.
        local saList = {gsPartyLeaderName}
        for i = 1, #gsaFollowerNames, 1 do
            table.insert(saList, gsaFollowerNames[i])
        end
        
        -- |[Jump Followers In]|
        --List is of variable size, may be zero.
        for i = 1, #saList, 1 do
            
            --Spawn the splash.
            fnSpawnSplash(saList[i] .. "Splash")
            
            --Jump into the water.
            fnCutsceneFacePos(saList[i], 12+gcfWhirl3x2OffX, 22+gcfWhirl3x2OffY)
            fnCutsceneJumpTo(saList[i], 12+gcfWhirl3x2OffX, 22+gcfWhirl3x2OffY, 25)
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            fnCutsceneTeleport(saList[i], -1.25, -1.50)
            fnCutscenePlaySound("World|MedSplash")
        
            --Reposition the splash to this position, order it to reset animations, and to expire when done one animation.
            fnCutsceneMoveExpirableSprite(saList[i] .. "Splash", 12+gcfWhirl3x2OffX, 22+gcfWhirl3x2OffY)
        end
        
        -- |[Transition to Next Room]|
        --Wait a bit.
        fnCutsceneWait(45)
        fnCutsceneBlocker()
        
        --Transition.
        fnCutscene([[ AL_BeginTransitionTo("LASTSAVE", gsStandardReliveEnd) ]])
        fnCutsceneBlocker()
    
    -- |[ ========== Normal Handler ========== ]|
    --Normal case.
    else
        fnExecWaterJumpToMap(12+gcfWhirl3x2OffX, 22+gcfWhirl3x2OffY, "StarfieldSwampJ", 1, 1)
    end
    
-- |[ ====================================== Examinables ======================================= ]|
elseif(sObjectName == "Shrine") then
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ Append("Thought:[VOICE|Leader] (A waystone shrine sits in the middle of this isolated pond.[P] Drifting souls can see its light and use it to find their way.)") ]])
    fnCutsceneBlocker()

-- |[ ========================================== Other ========================================= ]|
-- |[ ========================================== Debug ========================================= ]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
