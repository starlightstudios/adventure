-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Examinations]|
if(sObjectName == "Junk") then
	fnStandardDialogue([[ [VOICE|Mei](Writing supplies.[P] Paper, empty journals, pencils...) ]])
	
elseif(sObjectName == "Statue") then
	fnStandardDialogue([[ [VOICE|Mei](A statue.[P] The armor is rusted and tarnished.) ]])
	
elseif(sObjectName == "BookshelfA") then
	LM_ExecuteScript(gsJeanneSkillbook, 0)
	
elseif(sObjectName == "BookshelfB") then
	fnStandardDialogue([[ [VOICE|Mei]('Geography of the World'...[P] I don't recognize any of these places or names![P] Argh!) ]])
	
elseif(sObjectName == "BookshelfC") then
	fnStandardDialogue([[ [VOICE|Mei](The books here are so worn out as to be illegible...) ]])
	
elseif(sObjectName == "BookshelfD") then
	fnStandardDialogue([[ [VOICE|Mei]('Apologism and Practices of Apologists'.[P] Looks like a very scientific guide to the art of religious proofs.[P] There's a whole section dedicated to angel size vs. pin head size calculations!) ]])
	
elseif(sObjectName == "BookshelfE") then
	LM_ExecuteScript(gsJeanneSkillbook, 1)
	
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
