-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Examinations]|
if(sObjectName == "Fields") then
	local sMeiForm = VM_GetVar("Root/Variables/Global/Mei/sForm", "S")
	if(sMeiForm ~= "Alraune") then
		fnStandardDialogue([[ [VOICE|Mei](Amazingly, someone transported soil up here and started a large garden.) ]])
	else
		fnStandardDialogue([[ [VOICE|Mei](The little ones don't seem to know who planted them up here...) ]])
	end
	
elseif(sObjectName == "Bath") then
    fnStandardDialogue([[ [VOICE|Mei](A large bath.[P] Despite the surroundings, the water is clean, and even peach-scented!) ]])
    
--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
