-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Examinations]|
if(sObjectName == "Junk") then
	fnStandardDialogue([[ [VOICE|Mei](Moldy old bedsheets...) ]])
	
elseif(sObjectName == "Bed") then
	fnStandardDialogue([[ [VOICE|Mei](A spare bed.[P] The sheets on it are covered in dust.) ]])
	
elseif(sObjectName == "Statue") then
	fnStandardDialogue([[ [VOICE|Mei](Presumably this statue is in storage.[P] It hasn't been cleaned in a very long time.) ]])
	
elseif(sObjectName == "Mirror") then
	
	--Variables.
	local iMeiKnowsRilmani = VM_GetVar("Root/Variables/Chapter1/Scenes/iMeiKnowsRilmani", "N")
	
	--Normal case:
	if(iMeiKnowsRilmani == 0.0) then
		fnStandardDialogue([[ [VOICE|Mei](This mirror is clean and well-maintained, unlike the rest of the room.[P] Its surface almost shimmers...) ]])
	
	--Chapter-ending case:
	else
		--Variables.
		local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
		local iExaminedMirror = VM_GetVar("Root/Variables/Chapter1/Scenes/iExaminedMirror", "N")
		
		--Set flags.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iExaminedMirror", "N", 1.0)
		
		--Mei is alone:
		if(bIsFlorentinaPresent == false) then
			
			--Setup.
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	
			--If the mirror has not been examined:
			if(iExaminedMirror == 0.0) then
				fnCutscene([[ Append("Mei:[E|Neutral] (This mirror is clean, unlike everything else here...)[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Offended] (Wait a second...)[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Surprise] (There's Rilmani writing in the corner here!)[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Neutral] (Hmmm...)[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Neutral] (According to the notes, it means something like 'Reflection Space Worlds Unknown Between'.)[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Neutral] (I think the grammar is wrong, but...)[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Surprise] (...!)[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Surprise] (I swear I just saw the mirror shimmer when I thought that!)[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Neutral] ('Reflection Space Worlds Unknown Between'.)[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Surprise] (It did it again![P] The mirror is some kind of portal!)[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Happy] (Maybe it will lead me back home?[P] Maybe this is my ticket out of here?)[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Neutral] (But...[P] I am getting the feeling that I won't be able to come back.[P] Better make sure I don't have any loose ends...)[B][C]") ]])
	
			--Mirror was examined before:
			else
				fnCutscene([[ Append("Mei:[E|Neutral] (I think I can use this mirror to go back home.[P] Should I?)[BLOCK]") ]])
			end
	
			--Decision script is this script. It must be surrounded by quotes.
			local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
			fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
			fnCutscene(" WD_SetProperty(\"Add Decision\", \"Enter\", " .. sDecisionScript .. ", \"EnterMirror\") ")
			fnCutscene(" WD_SetProperty(\"Add Decision\", \"Not Yet\",  " .. sDecisionScript .. ", \"Leave\") ")
			fnCutsceneBlocker()
		
		--Florentina is here:
		else
			
			--Setup.
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
	
			--If the mirror has not been examined:
			if(iExaminedMirror == 0.0) then
				fnCutscene([[ Append("Mei:[E|Neutral] This mirror is clean, unlike everything else here...[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Offended] Wait a second...[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Surprise] There's Rilmani writing in the corner here![B][C]") ]])
				fnCutscene([[ Append("Florentina:[E|Happy] You think this thing is actually a Rilmani artifact?[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Happy] Please don't try to loot it until I've had a chance to translate it![B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Neutral] Hmmm...[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Neutral] According to the notes, it means something like 'Reflection Space Worlds Unknown Between'.[B][C]") ]])
				fnCutscene([[ Append("Florentina:[E|Surprise] Hey![P] Say that again![B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Neutral] 'Reflection Space Worlds Unknown Between'.[B][C]") ]])
				fnCutscene([[ Append("Florentina:[E|Surprise] Look![P] It's like it's reacting when you say that![B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Neutral] Hmmm, Reflection Space Worlds Between?[P] The reflection leads to the unknown space between worlds, maybe?[B][C]") ]])
				fnCutscene([[ Append("Florentina:[E|Happy] This thing is some kind of portal.[P] I bet it'll lead you straight to the Rilmani![B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Neutral] Let's go![B][C]") ]])
				fnCutscene([[ Append("Florentina:[E|Confused] Uh, yeah.[P] About that.[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Neutral] Huh -[P] you're not coming?[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Sad] C-[P]come on, Florentina![P] I'm sure there's all kinds of great loot to be had![B][C]") ]])
				fnCutscene([[ Append("Florentina:[E|Neutral] Mei, I think you know as well as I do that this is going to be where you and I part ways.[B][C]") ]])
				fnCutscene([[ Append("Florentina:[E|Neutral] This will probably take you right back to Earth, or maybe you'll have to do something else first.[P] I don't know.[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Sad] B-[P]but...[B][C]") ]])
				fnCutscene([[ Append("Florentina:[E|Happy] Hey, we had a lot of fun, right?[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Neutral] Well, I mean, I don't have to go right this second.[P] There's still stuff we can do here, right?[B][C]") ]])
				fnCutscene([[ Append("Florentina:[E|Neutral] And prolong the inevitable?[B][C]") ]])
				fnCutscene([[ Append("Florentina:[E|Happy] I'm game if you are.[B][C]") ]])
				fnCutscene([[ Append("Mei:[E|Neutral] (Well, I won't be coming back if I do go.[P] Should I?)[B][C]") ]])
			
			--Mirror has been examined.
			else
				fnCutscene([[ Append("Mei:[E|Neutral] (I think I can use this mirror to go back home.[P] Should I?)[BLOCK]") ]])
			end
			
			--Decision script is this script. It must be surrounded by quotes.
			local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
			fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
			fnCutscene(" WD_SetProperty(\"Add Decision\", \"Enter\", " .. sDecisionScript .. ", \"EnterMirror\") ")
			fnCutscene(" WD_SetProperty(\"Add Decision\", \"Not Yet\",  " .. sDecisionScript .. ", \"Leave\") ")
			fnCutsceneBlocker()
		
		end
	end

-- |[Decision Responses]|
--Enter the mirror.
elseif(sObjectName == "EnterMirror") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
    
    --Flags.
    VM_SetVar("Root/Variables/Chapter1/Scenes/iEnteredNixNedar", "N", 1.0)
	
	--Variables.
	local bIsFlorentinaPresent = fnIsCharacterPresent("Florentina")
	
	--Mei is alone:
	if(bIsFlorentinaPresent == false) then
		
		--Setup.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])

		--Dialogue.
		fnCutscene([[ Append("Mei:[E|Neutral] (Okay, here we go...)") ]])
		fnCutsceneBlocker()
		
		--Wait a bit.
		fnCutsceneWait(30)
		fnCutsceneBlocker()
		
		--Grey the screen up.
		fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0.7, 0.7, 0.7, 0, 0.7, 0.7, 0.7, 1) ]])
		fnCutsceneWait(120)
		fnCutsceneBlocker()
		
		--Transition to Nix Nedar.
		fnCutscene([[ AL_BeginTransitionTo("NixNedarSouthPath", gsRoot .. "Chapter 1/Scenes/NixNedar/Scene_FirstEnter.lua") ]])
		fnCutsceneBlocker()
	
	--Florentina is present:
	else
		
		--Remove Florentina from the party.
        fnRemovePartyMember("Florentina", false)
		
		--Setup.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Mei", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Florentina", "Neutral") ]])

		--Dialogue. Call the interstitial script.
		LM_ExecuteScript(gsRoot .. "Chapter 1/Scenes/FlorentinaGoodbye/Execution.lua")
		fnCutsceneBlocker()
		
		--Wait a bit.
		fnCutsceneWait(30)
		fnCutsceneBlocker()
		
		--Grey the screen up.
		fnCutscene([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0.7, 0.7, 0.7, 0, 0.7, 0.7, 0.7, 1) ]])
		fnCutsceneWait(120)
		fnCutsceneBlocker()
		
		--Transition to Nix Nedar.
		fnCutscene([[ AL_BeginTransitionTo("NixNedarSouthPath", gsRoot .. "Chapter 1/Scenes/NixNedar/Scene_FirstEnter.lua") ]])
		fnCutsceneBlocker()
	end

--Don't.
elseif(sObjectName == "Leave") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
