-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Examinations]|
--This door is locked unless Florentina is present. She can unlock it.
if(sObjectName == "LockedDoor") then
	
	--Variables
	local bIsFlorentinaPresent        = fnIsCharacterPresent("Florentina")
	local iFlorentinaUnlockedTrapWest = VM_GetVar("Root/Variables/Chapter1/Scenes/iFlorentinaUnlockedTrapWest", "N")
    local iSawTrapWest                = VM_GetVar("Root/Variables/Chapter1/Scenes/iSawTrapWest", "N")
    local iMetPolaris                 = VM_GetVar("Root/Variables/Chapter1/Polaris/iMetPolaris", "N")
    VM_SetVar("Root/Variables/Chapter1/Scenes/iDepthReset", "N", 1.0)
    
    --Florentina's variable.
    local iHasJob_TreasureHunter      = VM_GetVar("Root/Variables/Global/Florentina/iHasJob_TreasureHunter", "N")
	
	--Door is not unlocked:
	if(iFlorentinaUnlockedTrapWest == 0.0) then
	
		--If Florentina is not present:
		if(bIsFlorentinaPresent == false) then
	
			--Mei talks to herself.
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
			fnCutscene([[ Append("Mei:[E|Offended] (Crud, this door is locked...)") ]])
	
		--Florentina is here, so unlock the door:
		else
	
			--Setup.
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])
			
			--Dialogue.
			fnCutscene([[ Append("Mei:[E|Offended] Tch, the door is locked.[B][C]") ]])
            
            --Has treasure hunter:
            if(iHasJob_TreasureHunter == 1.0) then
                fnCutscene([[ Append("Florentina:[E|Happy] No it isn't.[B][C]") ]])
                fnCutscene([[ Append("Mei:[E|Offended] Yes it is![B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Happy] Nah, just let me pick it.[P] I can handle this.[B][C]") ]])
                fnCutscene([[ Append("Florentina:[E|Happy] (Use Florentina's 'Pick Lock' field ability on this door!)") ]])
                
            --No class:
            else

                --Has not seen the scene before:
                if(iSawTrapWest == 0.0) then

                    --Flag.
                    VM_SetVar("Root/Variables/Chapter1/Scenes/iSawTrapWest", "N", 1.0)
                    fnCutscene([[ Append("Florentina:[E|Happy] Let me see it...[B][C]") ]])
                    fnCutscene([[ Append("Florentina:[E|Neutral] Nuts, I can't get it.[P] I need a set of lockpicks.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Laugh] You know how to pick locks?[P] Cool![B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] Where'd you learn to do that?[B][C]") ]])
                    fnCutscene([[ Append("Florentina:[E|Neutral] A caravan came through with a whole bunch of spare locks, once, but it turned out they got the paperwork wrong.[P] So they gave me some platina to dispose of them.[B][C]") ]])
                    fnCutscene([[ Append("Florentina:[E|Neutral] Rather than just dump a hundred door locks, I started fiddling with them.[P] Turns out, picking them isn't hard if you practice.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] Can you teach me how to do that?[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Happy] I've always wanted to be a burglar![B][C]") ]])
                    fnCutscene([[ Append("Florentina:[E|Blush] R-[P]really?[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] Ah, no, I was kidding.[B][C]") ]])
                    fnCutscene([[ Append("Florentina:[E|Blush] Don't lead a girl on like that...[B][C]") ]])
                    fnCutscene([[ Append("Florentina:[E|Neutral] Anyway, I need some picks to get it open.[P] Unfortunately, Blythe doesn't let me keep any at the trading post.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] For...[P] reasons?[B][C]") ]])
                    fnCutscene([[ Append("Florentina:[E|Neutral] He's a keen eye, that one.[B][C]") ]])
                    fnCutscene([[ Append("Florentina:[E|Neutral] We'll need to find some before we can go in here.") ]])
                
                else
                    fnCutscene([[ Append("Florentina:[E|Neutral] I can't get the lock open without something to pick it.[P] We should search elsewhere.[B][C]") ]])
                    fnCutscene([[ Append("Mei:[E|Neutral] Where could we get some lockpicks?[B][C]") ]])
                    
                    --Met Polaris.
                    if(iMetPolaris == 1.0) then
                        fnCutscene([[ Append("Florentina:[E|Neutral] Polaris might have some.[P] She had a bunch of tools on her.[B][C]") ]])
                        fnCutscene([[ Append("Mei:[E|Neutral] Good idea![P] Let's go ask her!") ]])
                    else
                        fnCutscene([[ Append("Florentina:[E|Neutral] No idea.[P] They're not allowed in the trading post.[P] We should check other populated areas.") ]])
                    end
                end
            end
		end
	
	--Door is unlocked, go in!
    else
		AudioManager_PlaySound("World|FlipSwitch")
		AL_BeginTransitionTo("TrapDungeonEntry", "FORCEPOS:17.0x13.0x0")
	end

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
