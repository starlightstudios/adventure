-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Execution]|
--Exit.
if(sObjectName == "ToExteriorEC") then
    
    --Vacuum check.
    local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
	if(sChristineForm == "Human" or sChristineForm == "Raiju") then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("[SOUND|World|AutoDoorFail]Access denied.[P] Air pressure at unsafe levels.[P] Please secure organics for transport.") ]])
		fnCutsceneBlocker()
    else
        AudioManager_PlaySound("World|AutoDoorOpen")
        AL_BeginTransitionTo("RegulusExteriorEC", "FORCEPOS:36.0x28.0x0")
    end
	
--Exit.
elseif(sObjectName == "ToLRTA") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusLRTA", "FORCEPOS:12.0x22.0x0")

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
