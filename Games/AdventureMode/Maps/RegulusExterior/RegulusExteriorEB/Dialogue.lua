-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    --Set facing.
    TA_SetProperty("Face Character", "PlayerEntity")
    

    -- |[DarkmatterA]|
    if(sActorName == "DarkmatterA") then
        
        --Variables.
        local iHasDarkmatterForm = VM_GetVar("Root/Variables/Global/Christine/iHasDarkmatterForm", "N")
        if(iHasDarkmatterForm == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("[VOICE|Leader](The creature is regarding me calmly.[P] It's not moving to attack, and doesn't run away.)") ]])
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Darkmatter:[VOICE|DarkmatterGirl] I am glad to see you well, friend.[P] We will be vigilant for you.") ]])
        end
        
    -- |[DarkmatterB]|
    elseif(sActorName == "DarkmatterB") then
    
        --Variables.
        local iHasDarkmatterForm = VM_GetVar("Root/Variables/Global/Christine/iHasDarkmatterForm", "N")
        if(iHasDarkmatterForm == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("[VOICE|Leader](The creature seems to be ignoring me.[P] It doesn't seem hostile.)") ]])
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Darkmatter:[VOICE|DarkmatterGirl] I'm so bored.[P] Maybe there's a supernova I can go watch...") ]])
        end
        
    -- |[GolemA]|
    elseif(sActorName == "GolemA") then
    
        --Variables.
        local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
    
        --Mechanical forms that are not objectionable.
        if(sChristineForm == "Golem" or sChristineForm == "LatexDrone" or sChristineForm == "Secrebot") then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] ^The batteries are superb, but the cable tends to break.[P] The Darkmatters also tend to play with them and tangle them up.[P] How aggravating!^") ]])
        
        --Command unit!
        elseif(sChristineForm == "Doll") then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] ^Hello, command unit.[P] If you have any control over the production mandates, could we get some more durable cables?[P] They are the weakest part of our electrical systems.^") ]])
        
        --Eldritch.
        elseif(sChristineForm == "Eldritch") then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] ^Command Unit, please keep your...[P] creature...[P] on a short leash...^") ]])
        
        --Electrosprite.
        elseif(sChristineForm == "Electrosprite") then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] ^An -[P] oh my![P] You shouldn't show yourself to the Lord Golem, she doesn't know!^") ]])
        
        --Steam Droid.
        elseif(sChristineForm == "SteamDroid") then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] ^You're with a Command Unit?[P] Are you some sort of mercenary?[P] We don't want any trouble, please.^") ]])
        
        --Darkmatter.
        elseif(sChristineForm == "Darkmatter") then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] ^No![P] No![P] Bad Darkmatter![P] Do not tangle up those cables![P] Arrggh!^[B][C]") ]])
            fnCutscene([[ Append("Thought:[VOICE|Leader] (Being told not to just makes me want to do it more...)") ]])
        
        --Else-case:
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] ^This dialogue case is not handled.^") ]])
        end
        
    -- |[GolemB]|
    elseif(sActorName == "GolemB") then
    
        --Variables.
        local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
        
        --Mechanical forms that are not objectionable.
        if(sChristineForm == "Golem" or sChristineForm == "LatexDrone" or sChristineForm == "Secrebot") then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] ^I am currently in charge of security for this survey team.[P] Do not worry, we all have up-to-date combat routines, and the Lord Unit is a superb close-quarters fighter.^") ]])
        
        --Command unit!
        elseif(sChristineForm == "Doll") then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] ^Hello, command units.[P] I am charged with security of this team.[P] No problems.[P] Let me know if I may aid you in any way.^") ]])
        
        --Eldritch.
        elseif(sChristineForm == "Eldritch") then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] ^I am unfamiliar with this species, Command Unit.[P] Is it a threat to my team?^[B][C]") ]])
            fnCutscene([[ Append("55:[VOICE|Tiffany] ^Negatory.[P] As you were, Unit.[P] This is under control.^") ]])
        
        --Electrosprite.
        elseif(sChristineForm == "Electrosprite") then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] ^Hm.[P] I've heard of you from the others.[P] So long as you do not cause problems, I will tolerate you, Electrosprite.^") ]])
        
        --Steam Droid.
        elseif(sChristineForm == "SteamDroid") then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] ^Don't make any sudden moves, mercenary, and we'll get along just fine.^[B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] ^Affirmative.[P] I'm not here to make trouble.^") ]])
        
        --Darkmatter.
        elseif(sChristineForm == "Darkmatter") then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] ^Shoo, shoo! Go on, get out of here, Darkmatter!^") ]])
        
        --Else-case:
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] ^This dialogue case is not handled.^") ]])
        end
        
    -- |[GolemC]|
    elseif(sActorName == "GolemC") then
    
        --Variables.
        local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
        
        --Mechanical forms that are not objectionable.
        if(sChristineForm == "Golem" or sChristineForm == "LatexDrone" or sChristineForm == "Secrebot") then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] ^Survey work is rather pleasant, if dangerous, but the recent increase in tectonic activity means we have a long work order list...^") ]])
        
        --Command unit!
        elseif(sChristineForm == "Doll") then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] ^We're working very hard to catch up with our backlog, command units.^") ]])
            
        --Eldritch.
        elseif(sChristineForm == "Eldritch") then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] ^Oh my, you look like those things we saw on the crater shelf!^[B][C]") ]])
            fnCutscene([[ Append("55:[VOICE|Tiffany] ^My prisoner here is not of your concern.[P] Ignore her and the things you've seen.[P] For your own safety.^[B][C]") ]])
            fnCutscene([[ Append("55:[VOICE|Tiffany] ^I cannot guarantee any other Command Unit will be as lenient as I am.^") ]])
        
        --Electrosprite.
        elseif(sChristineForm == "Electrosprite") then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] ^Are you a friend of LIFO's?[P] She's the tandem unit of a friend of mine.[P] Not suggesting you all know each other or anything, that'd be weird.^") ]])
        
        --Steam Droid.
        elseif(sChristineForm == "SteamDroid") then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] ^Are we hiring Steam Droids to help with survey work now?[P] I knew we were short-staffed but this is going too far.^") ]])
        
        --Darkmatter.
        elseif(sChristineForm == "Darkmatter") then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] ^Oh no.[P] Darkmatters always make the long-range tachyon sensors short out...[P] Now I'll have to recalibrate it...^") ]])
        
        --Else-case:
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] ^This dialogue case is not handled.^") ]])
        end
        
    -- |[GolemD]|
    elseif(sActorName == "GolemD") then

        --Variables.
        local sChristineForm              = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
        local iMetSurveyLordGolem         = VM_GetVar("Root/Variables/Chapter5/Scenes/iMetSurveyLordGolem", "N")
        local iMetSurveyLordLatex         = VM_GetVar("Root/Variables/Chapter5/Scenes/iMetSurveyLordLatex", "N")
        local iMetSurveyLordEldritch      = VM_GetVar("Root/Variables/Chapter5/Scenes/iMetSurveyLordEldritch", "N")
        local iMetSurveyLordElectrosprite = VM_GetVar("Root/Variables/Chapter5/Scenes/iMetSurveyLordElectrosprite", "N")
        local iMetSurveyLordSteamDroid    = VM_GetVar("Root/Variables/Chapter5/Scenes/iMetSurveyLordSteamDroid", "N")
        local iMetSurveyLordDarkmatter    = VM_GetVar("Root/Variables/Chapter5/Scenes/iMetSurveyLordDarkmatter", "N")
        local iMetSurveyLordDoll          = VM_GetVar("Root/Variables/Chapter5/Scenes/iMetSurveyLordDoll", "N")
        local iMetSurveyLordSecrebot      = VM_GetVar("Root/Variables/Chapter5/Scenes/iMetSurveyLordSecrebot", "N")
        local iKnowsAboutDarkmatters      = VM_GetVar("Root/Variables/Global/Christine/iKnowsAboutDarkmatters", "N")
        
        --Face the party.
        TA_SetProperty("Face Character", "PlayerEntity")
        
        --If Christine is a Golem:
        if(sChristineForm == "Golem") then
            
            --If this is the first time meeting the survey lord as a golem:
            if(iMetSurveyLordGolem == 0.0) then
            
                --Flag.
                VM_SetVar("Root/Variables/Chapter5/Scenes/iMetSurveyLordGolem", "N", 1.0)
                
                --Dialogue.
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
                fnCutscene([[ Append("Golem: ^Ah, greetings, fellow Lord Unit.[P] Are you perhaps on your way to Serenity Crater?^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] ^That is just south of here, correct?^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^Affirmative.[P] They have been most helpful to my efforts.^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] ^What are you doing out here?^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^I am administering Geological Survey Team 82.[P] Due to the recent tectonic activity in this area, we are re-mapping the terrain.^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^The transit network has taken a lot of damage.[P] I expect it will be some time before we return to full efficiency.^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] ^Ah, very good.[P] I was just going to ask about the effects on productivity.^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^As it stands, cargo is mostly hauled manually.[P] The Slave Units, of course, won't stop complaining.[P] Why, even I had to haul the battery pack here!^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] (The battery pack is the lightest part, you slacker...)[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] ^Peh, I despise manual labour.^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^As do we all.[P] Our bodies are simply not designed for it.^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] (Our model variant has less shielding but the same density of motivators...)[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] ^Of course.[P] The Slave Units should do it.[P] But, we must all pitch in for the Cause of Science.^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^I agree fully.[P] Speaking of...^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^If you are on your way to Serenity Crater, do be careful.[P] The Darkmatters seem to be upset about something.^[B][C]") ]])
                
                --If Christine doesn't know what a Darkmatter is:
                if(iKnowsAboutDarkmatters == 0.0) then
                    VM_SetVar("Root/Variables/Global/Christine/iKnowsAboutDarkmatters", "N", 1.0)
                    fnCutscene([[ Append("Christine:[E|Neutral] ^Those shadowy creatures are called Darkmatters?^[B][C]") ]])
                    fnCutscene([[ Append("Golem: ^Affirmative, though many units often call them 'Darkmatter Girls'.[P] They are, however, certainly not girls.^[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Smirk] ^So what are they?^[B][C]") ]])
                    fnCutscene([[ Append("Golem: ^I would receive a research commendation if I knew![P] They are virtually impossible to study, as they absorb almost all forms of radiation and seem to be able to disappear on a whim.^[B][C]") ]])
                    fnCutscene([[ Append("Golem: ^They are not usually aggressive.[P] In fact, often they are quite charming.[P] But do not take any risks around them.^[B][C]") ]])
                    fnCutscene([[ Append("55:[E|Smirk] ^We don't as a matter of course.^[B][C]") ]])
                    fnCutscene([[ Append("Golem: ^Oh, a command unit![P] We are being very productive here.^[B][C]") ]])
                    fnCutscene([[ Append("55:[E|Neutral] ^Not my concern.[P] Forget you saw me.^[B][C]") ]])
                    fnCutscene([[ Append("Golem: ^Covert operations.[P] I understand.[P] I will set a timer to purge my memories of you when I defragment.[P] Thank you for your service.^[B][C]") ]])
                    fnCutscene([[ Append("55:[E|Neutral] ^Good.[P] 771852, let's go.^[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Smirk] ^Affirmative.^") ]])
                
                --Christine knows about Darkmatters.
                else
                    fnCutscene([[ Append("Christine:[E|Neutral] ^What has the Darkmatters so riled up?^[B][C]") ]])
                    fnCutscene([[ Append("Golem: ^I am not sure.[P] I believe Serenity Observatory put in a work order to investigate exactly that.^[B][C]") ]])
                    fnCutscene([[ Append("Golem: ^Isn't that what you were on your way there to see about?^[B][C]") ]])
                    fnCutscene([[ Append("55:[E|Neutral] ^What we intend to do is not your business.^[B][C]") ]])
                    fnCutscene([[ Append("Golem: ^Oh, a command unit![P] We are being very productive here.^[B][C]") ]])
                    fnCutscene([[ Append("55:[E|Neutral] ^Not my concern.[P] Forget you saw me.^[B][C]") ]])
                    fnCutscene([[ Append("Golem: ^Covert operations.[P] I understand.[P] I will set a timer to purge my memories of you when I defragment.[P] Thank you for your service.^[B][C]") ]])
                    fnCutscene([[ Append("55:[E|Smirk] ^Good.[P] 771852, let's go.^[B][C]") ]])
                    fnCutscene([[ Append("Christine:[E|Smirk] ^Affirmative.^") ]])
                end
                fnCutsceneBlocker()
            
            else
                --Dialogue.
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
                fnCutscene([[ Append("Golem: ^I wish you luck on whatever your assignment is.[P] Serve the Cause of Science.^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] ^To the best of our ability, we will.^") ]])
            end
            
        --If Christine is a Latex Drone:
        elseif(sChristineForm == "LatexDrone") then
        
            --If Christine has met the Lord Golem before, as a Golem:
            if(iMetSurveyLordGolem == 1.0 and iMetSurveyLordLatex == 0.0) then
        
                --Flag.
                VM_SetVar("Root/Variables/Chapter5/Scenes/iMetSurveyLordLatex", "N", 1.0)
                
                --Dialogue.
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
                fnCutscene([[ Append("Golem: ^Hmm, you seem familiar.[P] Unit, what is your designation?^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] ^Uhh...^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^Most curious!^[B][C]") ]])
                fnCutscene([[ Append("55:[E|Upset] ^*Retain your cover, Unit 771852.*^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] ^*R-[P]right!*^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] ^MY DESIGNATION IS UNIT 771852.[P] GREETINGS, LORD UNIT.[P] HOW MAY I SERVE YOU?^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^Ah, perhaps you linguistic generators were delayed.[P] Not unsurprising -[P] wait.[P] Unit 771852?^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^Have we met before?^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] ^AFFIRMATIVE, LORD UNIT.[P] THIS UNIT HAS BEEN REPURPOSED.^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^Most unfortunate...^[B][C]") ]])
                fnCutscene([[ Append("55:[E|Smirk] ^I needed her this way for my purposes.[P] Do not question them.^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^Of course not, command unit![P] If you will it, then this is the ideal form for Unit 771852.^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] ^THANK YOU FOR THE COMPLIMENT.[P] THIS UNIT IS GLAD IT COULD SERVE YOU ADEQUATELY.^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^No, you didn't actually serve -[P] ugh.[P] Stupid latex drones!^[B][C]") ]])
                fnCutscene([[ Append("55:[E|Smirk] ^I wouldn't insult my personal servitor units.[P][EMOTION|55|Smug] Unless you wish to take her place...^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^N-n-n-n-n-no![P] Unit 771852, you are an excellent unit![P] Please continue to serve your Command Unit to the best of your ability.^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] ^COMMAND RECEIVED.[P] THIS UNIT WILL SERVE ITS COMMAND UNIT.^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] ^COMMAND UNIT, HOW MAY I SERVE YOU?^[B][C]") ]])
                fnCutscene([[ Append("55:[E|Smirk] ^We have places to go, 771852.[P] Let's move out.^[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] ^And you -[P] forget you saw me.[P] I wouldn't want to have to come back and do cleanup.^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^I -[P] I will set a program to purge my memories of you during defragment!^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Laugh] ^*Ha ha![P] That was fun!*^[B][C]") ]])
                fnCutscene([[ Append("55:[E|Smirk] ^*Maybe we should come back after she's defragmented and do it again.*^") ]])
                
            --First time meeting this Lord Golem as a Latex Drone:
            elseif(iMetSurveyLordGolem == 0.0 and iMetSurveyLordLatex == 0.0) then
        
                --Flag.
                VM_SetVar("Root/Variables/Chapter5/Scenes/iMetSurveyLordLatex", "N", 1.0)
                
                --Dialogue.
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
                fnCutscene([[ Append("Golem: ^A Drone Unit?[P] What are you doing here?[P] Did you wander off from the LRT Facility?^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] ^Uhh...^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^Oh dear,[P] I hope I didn't ask too complex a question!^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Angry] (Grrrrrr!)[B][C]") ]])
                fnCutscene([[ Append("55:[E|Upset] ^*Retain your cover, Unit 771852.*^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] ^*R-[P]right!*^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] ^MY DESIGNATION IS UNIT 771852.[P] GREETINGS, LORD UNIT.[P] HOW MAY I SERVE YOU?^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^Ah, much better.[P] At least you seem to be able to speak coherently.^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Angry] (GRRRR!!)[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] ^THANK YOU FOR THE COMPLIMENT, LORD UNIT.[P] AT LEAST YOU SEEM TO BE ABLE TO SPEAK COHERENTLY, AS WELL.^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^What was that, you insolent -[P] disrepectful -[P] little -[P] -[P]!!^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] ^THANK YOU FOR THE COMPLIMENT, LORD UNIT.[P] WHAT WAS THAT, YOU INSOLENT DISRESPECTFUL LITTLE, AS WELL.^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^Stupid, [P]stupid drone![P] Why are you bothering me?^[B][C]") ]])
                fnCutscene([[ Append("55:[E|Upset] ^I see you are verbally abusing my Drone Unit.^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^C-c-c-command unit![P] Hello![P] I was just admiring this unit![P] She is -[P] shiny?^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] ^THANK YOU FOR THE COMPLIMENT.[P] YOU ARE -[P] SHINY?[P] AS WELL.^[B][C]") ]])
                fnCutscene([[ Append("55:[E|Smirk] ^That will be enough, Drone.^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] ^AFFIRMATIVE, COMMAND UNIT.^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Laugh] (Ha ha ha ha ha ha!)[B][C]") ]])
                fnCutscene([[ Append("55:[E|Smirk] ^I would recommend against talking down to my servitors, Lord Unit.[P] She can be replaced at any time.[P] Specifically, by a unit who has displeased me.^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^A thousand pardons, Command Unit![P] It will not occur again.^[B][C]") ]])
                fnCutscene([[ Append("55:[E|Smirk] ^See that it doesn't.^[B][C]") ]])
                fnCutscene([[ Append("55:[E|Smirk] ^We have places to go, 771852.[P] Let's move out.^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] ^AFFIRMATIVE, COMMAND UNIT.[P] MOVING OUT.^") ]])
        
            --Repeat encounters.
            else
                --Dialogue.
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
                fnCutscene([[ Append("Golem: ^I wish you luck on whatever your assignment is.[P] Serve the Cause of Science.^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] ^AFFIRMATIVE.[P] THE CAUSE OF SCIENCE WILL BE SERVED.^") ]])
            end
        
        --Eldritch.
        elseif(sChristineForm == "Eldritch") then
        
            --First time.
            if(iMetSurveyLordEldritch == 0.0) then
        
                --Flag.
                VM_SetVar("Root/Variables/Chapter5/Scenes/iMetSurveyLordEldritch", "N", 1.0)
                
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
                fnCutscene([[ Append("Golem: ^My-[P]my goodness!^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^Security!^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] ^Well hello, metal one.[P] You're looking delectable this cycle.^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^S-[P]someone!^[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] ^That's quite enough.[P] Down.^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] ^As you wish, Command Unit.^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^Command Unit, what is this thing?[P] What made it?^[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] ^This thing is part of a new program.[P] It is top secret.[P] I expect you and your team will refrain from reporting on it, lest the reports be classified.^[B][C]") ]])
                fnCutscene([[ Append("55:[E|Smirk] ^These creatures, when tamed, make excellent attack dogs.^[B][C]") ]])
                fnCutscene([[ Append("55:[E|Smug] ^Though I sometimes lose control of it, you understand.^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] ^Can I just nibble at her skin?[P] Surely she doesn't need all of it.^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^Please keep this creature away from me!^[B][C]") ]])
                fnCutscene([[ Append("55:[E|Smirk] ^You heard the Lord Golem.[P] Leave all of her components intact.^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^Phew...^[B][C]") ]])
                fnCutscene([[ Append("55:[E|Smug] ^Unless she displeases me.^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] ^As you wish, Command Unit.^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^How can an organic emit radio waves...?^[B][C]") ]])
                fnCutscene([[ Append("55:[E|Smug] ^You have a specific task as a geological survey team.[P] I suggest you limit queries to that task.^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^Yes Command Unit![P] Right away!^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Laugh] (Ha ha ha ha ha!)[B][C]") ]])
                fnCutscene([[ Append("55:[E|Smug] ^We'll be off now.^[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] ^Oh, and as a reminder...^[B][C]") ]])
                fnCutscene([[ Append("55:[E|Smirk] ^These creatures never rest, grow tired, and are single-minded when given an order.^[B][C]") ]])
                fnCutscene([[ Append("55:[E|Smirk] ^It'd be a shame if that order were to [P]*find*[P] someone.[P] Wouldn't it?^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^Affirmative, Command Unit![P] Suspending all further queries![P] Have a wonderful day!^") ]])
        
            --Repeats.
            else
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
                fnCutscene([[ Append("Golem: ^Please have a wonderfully productive day, Command Unit!^[B][C]") ]])
                fnCutscene([[ Append("Golem: (Ideally, as far away from me as possible!)") ]])
            end
        
        --Electrosprite.
        elseif(sChristineForm == "Electrosprite") then
        
            --First time.
            if(iMetSurveyLordElectrosprite == 0.0) then
                
                --Flag.
                VM_SetVar("Root/Variables/Chapter5/Scenes/iMetSurveyLordElectrosprite", "N", 1.0)
                
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
                fnCutscene([[ Append("Golem: ^Now this...[P] What is this creature before me?^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^PDU, begin taking scans.[P] Log under 'Unidentified Voidborne Partirhuman'.^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] (She doesn't know what an electrosprite is?[P] Time to be mean!)[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] ^Hello bodied-creature.^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^Partirhuman species is capable of vocalization and use of short-range radio transmissions.^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^Hello, unbodied creature.[P] Does your kind have a name?^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] ^I am a Trebolyte.[P] I am an ambassador for my kind.^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] ^I have been observing your 'Golem' kind for a long time.[P] My people are in need of a great hero to save them.^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] ^I think you are the one we have been looking for.^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^Me?[P] I am a simple researcher.^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] ^Yes, you are.[P] But my people have little time.[P] You see, we have been slowly dying out for centuries.^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] ^We are being gradually killed off by the Enedytes, or 'grounds' as you call them.^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] ^They devour electricity and disperse it over a wide area.[P] We have been at war since the dawn of time.^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] ^But you metal people, you combine both electricity and grounds, to make 'conductors'.[P] You could turn the tide of the war!^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] ^Please, come with me.[P] A grand adventure lies before you, chosen one.^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^I -[P] I cannot.[P] I'm sorry, you must have the wrong golem.^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^I am merely a researcher.[P] I survey geological formations.[P] I am no hero.^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Sad] ^Are you sure?^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^Please continue looking, noble Trebolyte.[P] I am flattered, but certain I am not the one you need.^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Sad] ^I see, perhaps I have erred...^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] ^I will continue my search.[P] I have heard there is a great warrior among you.[P] She fears nothing.[P] She will save us.^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] ^I must find her to save my people...^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^*I got some good readings on these 'Trebolytes'...[P] But it's too risky to try capturing one.*^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^Good luck, Trebolyte.[P] Find the one you seek.^[B][C]") ]])
                fnCutscene([[ Append("Golem: (Meanwhile, I'll get a promotion for discovering a new partirhuman species!)[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Laugh] (This sucker's report should throw off the Administration by muddying the waters a bit![P] Ha ha ha!)") ]])
                
            --Repeats.
            else
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
                fnCutscene([[ Append("Golem: ^*PDU, take readings.*^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^Good luck, noble Trebolyte.^") ]])
        
            end
        
        --Steam Droid.
        elseif(sChristineForm == "SteamDroid") then
        
            if(iMetSurveyLordSteamDroid == 0.0) then
                
                --Flag.
                VM_SetVar("Root/Variables/Chapter5/Scenes/iMetSurveyLordSteamDroid", "N", 1.0)
                
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
                fnCutscene([[ Append("Golem: ^Tch, what have we here.[P] Begone, scavenger.^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^Take anything from my equipment and you'd best believe I will send my security units after you.^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] ^Well, that's no way to treat an ambassador!^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] ^*Play along, 55.*^[B][C]") ]])
                fnCutscene([[ Append("55:[E|Smirk] ^*Affirmative.*^[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] ^Insulting an ambassador?[P] I think I'll have to log this for disciplinary action.^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^C-[P]c-[P]command Unit![P] I didn't see you there![P] My apologies!^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] ^I'll let it slide this time.[P] I suppose I should expect as much from these uncouth Lord Units.^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^Uncouth![P] Why -[P] Command Unit, may I speak to you for a moment?^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^*Why are we not recycling this low-life rapscallion?[P] Surely she is better melted down for scrap?*^[B][C]") ]])
                fnCutscene([[ Append("55:[E|Smirk] ^*We are attempting to foment a civil war amongst the Steam Droids.[P] By arming certain groups covertly, we can allow them to eliminate one another.*^[B][C]") ]])
                fnCutscene([[ Append("55:[E|Smirk] ^*So put up with this one and grant her requests.[P] Confirm order.*^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^*...*^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^*Affirmative, Command Unit.[P] Carrying out orders.*^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^I apologize, Ambassador.[P] May I get your name?^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] ^CD-97.^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] ^I was told I could expect to receive supplies.[P] Are we at the pickup site, Command Unit?^[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] ^Negatory.[P] However, any equipment this Lord Golem could furnish that would not be missed would serve our purposes.^[B][C]") ]])
                fnCutscene([[ Append("55:[E|Smirk] ^It may even prevent me from filing her for disciplinary action.^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^...[P] Oh![P] Yes!^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^I see you're using an Electrospear, CD-97.[P] Perhaps this foregrip that fell down a chasm and was unrecoverable would suit your needs?^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] [SOUND|World|TakeItem]^This will do nicely.[P] Thank you.^[B][C]") ]])
                fnCutscene([[ Append("Golem: (If it furthers your elimination, so much the better...)[B][C]") ]])
                fnCutscene([[ Append("Golem: ^Very good.[P] Command Unit?^[B][C]") ]])
                fnCutscene([[ Append("55:[E|Smirk] ^There will be no logs of our encounter.[P] As you were, Lord Unit.^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] (I do so love free stuff![P] Thanks for playing the part!)") ]])
                LM_ExecuteScript(gsItemListing, "Spear Foregrip")
        
            else
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
                fnCutscene([[ Append("Golem: ^Good luck, Ambassador.[P] I hope you succeed at your endeavours.^[B][C]") ]])
                fnCutscene([[ Append("Golem: (Specifically, wiping one another out.)[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] ^Thank you for your aid, Lord Unit.^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Laugh] (Specifically, being so easy to trick![P] Ha ha ha!)") ]])
        
            end
        
        --Darkmatter.
        elseif(sChristineForm == "Darkmatter") then
        
            if(iMetSurveyLordDarkmatter== 0.0) then
                
                --Flag.
                VM_SetVar("Root/Variables/Chapter5/Scenes/iMetSurveyLordDarkmatter", "N", 1.0)
                
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
                fnCutscene([[ Append("Golem: ^Darkmatter, please don't knock anything over.^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] (Cannot -[P] rgh -[P] fight it...)[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] (Must -[P] mess -[P] with -[P] stuck up -[P] ponce![P] Arrgh!)[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] ^Hardmatter.^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^Did you -[P] did you just talk?^[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] ^No, I did not.[P] Were you speaking to me?^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^That Darkmatter -[P] it just sent a radio transmission!^[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] ^My receptors did not pick anything up.^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] ^Hardmatter.^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^Again![P] It spoke!^[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] ^Do your receptors need a recalibration?^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] ^Hardmatter.^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] ^We.[P] Know.^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^OH MY GOODNESS!!!^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] ^Confess.^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^Command Unit![P] I -[P] I have been stealing resources for my personal pursuits!^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^I took three kilograms of organic butter rations and slathered myself with them as my tandem unit watched!^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^...^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] ^...^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] ^Confess.^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^Ah![P] No![P] I -[P] I also forced a Slave Unit to record her masturbating with the butter while I admired myself in the mirror!^[B][C]") ]])
                fnCutscene([[ Append("55:[E|Offended] ^Uhh...^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] ^Confess.^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^Please, no more![P] And then I threatened the Slave Unit with repurposement if she told anyone![P] Waahhh![P] Let me go!^[B][C]") ]])
                fnCutscene([[ Append("55:[E|Offended] ^That's more than enough, Lord Unit!^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] ^Atone.^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^Okay, okay.[P] I'll make it up to the Slave Unit -[P] I'll give her extra time off and ask if she'd like to be reassigned away from me!^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^I'm sorry, I swear I'll be good!^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] ^...^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] ^Acceptable.^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] ^Kinks.[P] Fine.[P] Harming.[P] Subordinates.[P] Not.^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^I understand!^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] ^We.[P] Always.[P] Watching.^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] ^Always.^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^*sob*^[B][C]") ]])
                fnCutscene([[ Append("55:[E|Offended] ^I will be going now, Lord Unit.[P] As you were.^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] (Well, hopefully she won't mistreat any Slave Units now...)") ]])
        
            else
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
                fnCutscene([[ Append("Golem: ^Please, torment me no more, Darkmatter.^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] (I'll just smile idly and say nothing. )[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] (And point straight at her suddenly!)[B][C]") ]])
                fnCutscene([[ Append("Golem: ^Waahh![P] No more, no more!^") ]])
        
            end
        
        --Command unit.
        elseif(sChristineForm == "Doll") then
        
            --First time.
            if(iMetSurveyLordDoll == 0.0) then
                
                --Flag.
                VM_SetVar("Root/Variables/Chapter5/Scenes/iMetSurveyLordDoll", "N", 1.0)
                
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
                fnCutscene([[ Append("Golem: ^Greetings, command units.[P] To what do I owe the pleasure?^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] ^We were just on our way to Serenity Crater, but I see tectonic activity has changed the path.^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^Correct.[P] It is just south of here.[P] It is still accessible by foot.^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^May I inquire as to the purpose of your visit?^[B][C]") ]])
                fnCutscene([[ Append("55: ^You may not.^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] ^No need to be rude.^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] ^Please forgive her, she is very discretion-minded.^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^I just wanted to know if it was anything to do with my crew, such as a reassignment.[P] I meant no offense.^[B][C]") ]])
                fnCutscene([[ Append("55: ^We should be going.^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] ^Well, the Cause calls, as it were.[P] Back to work, lord unit.^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^Right away!^") ]])
                
            --Repeat:
            else
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
                fnCutscene([[ Append("Golem: ^Please give Unit 300910 my regards, and thank her for allowing us to use her charging facilities.^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^She always goes out of her way to make sure survey crews are comfortable.[P] It is appreciated among we of the lower castes.^") ]])
            
            end
        
        --Secrebot
        elseif(sChristineForm == "Secrebot") then
        
            --First time.
            if(iMetSurveyLordSecrebot == 0.0) then
                
                --Flag.
                VM_SetVar("Root/Variables/Chapter5/Scenes/iMetSurveyLordSecrebot", "N", 1.0)
                
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
                fnCutscene([[ Append("Golem: ^Hello, command unit. Is that a refurbished secrebot? I didn't know any were still in use.^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] ^Hello, lord golem. I am CR-1-16. Is this the way to Serenity Observatory?^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^Correct.[P] It is just south of here.[P] It is still accessible by foot.^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^Or... wheel.^[B][C]") ]])
                fnCutscene([[ Append("55: ^This refurbished unit has updated vocal routines. I've found they are capable of combat, as well.^[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] ^No harm will come to you, command unit!^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^I had heard something about secrebots becoming popular again, but obviously tectonic surveys don't support a wheeled transport method.^[B][C]") ]])
                fnCutscene([[ Append("Golem: ^It was very nice meeting you, though. Good luck, command unit.^") ]])
                
            --Repeat:
            else
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
                fnCutscene([[ Append("Golem: ^Perhaps secrebots are a fad that come and go, like fashion. No offense, of course. I'm just so surprised they're making a comeback.^") ]])
            
            end
        end
    end
end
