-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Execution]|
--Exit.
if(sObjectName == "ToExteriorEA") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusExteriorEA", "FORCEPOS:14.0x4.0x0")

--Terminal.
elseif(sObjectName == "Terminal") then

	--Variables.
	local iIsTramHere = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsTramHere", "N")
	
	--Tram is already here.
	if(iIsTramHere == 1.0) then
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine](The tram is here already, no need to access this console.)") ]])
		fnCutsceneBlocker()

	--Tram is going to arrive soon.
	else
	
		--Set flags.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iIsTramHere", "N", 1.0)
		VM_SetVar("Root/Variables/Chapter5/Scenes/sCurrentTramArea", "S", "RegulusExteriorStationE")
	
		--Spawn the tram parts.
		TA_Create("TramA")
			TA_SetProperty("Position", 33, 6)
			TA_SetProperty("Move Frame", gci_Face_West, 0, "Root/Images/Sprites/Tram/West|0")
			TA_SetProperty("Move Frame", gci_Face_West, 1, "Root/Images/Sprites/Tram/West|0")
			TA_SetProperty("Move Frame", gci_Face_West, 2, "Root/Images/Sprites/Tram/West|0")
			TA_SetProperty("Move Frame", gci_Face_West, 3, "Root/Images/Sprites/Tram/West|0")
			TA_SetProperty("Facing", gci_Face_West)
			TA_SetProperty("Set Ignore Clips", true)
		DL_PopActiveObject()
		TA_Create("TramB")
			TA_SetProperty("Position", 34, 6)
			TA_SetProperty("Move Frame", gci_Face_West, 0, "Root/Images/Sprites/Tram/North|0")
			TA_SetProperty("Move Frame", gci_Face_West, 1, "Root/Images/Sprites/Tram/North|0")
			TA_SetProperty("Move Frame", gci_Face_West, 2, "Root/Images/Sprites/Tram/North|0")
			TA_SetProperty("Move Frame", gci_Face_West, 3, "Root/Images/Sprites/Tram/North|0")
			TA_SetProperty("Facing", gci_Face_West)
			TA_SetProperty("Set Ignore Clips", true)
		DL_PopActiveObject()
		TA_Create("TramC")
			TA_SetProperty("Position", 35, 6)
			TA_SetProperty("Move Frame", gci_Face_West, 0, "Root/Images/Sprites/Tram/South|0")
			TA_SetProperty("Move Frame", gci_Face_West, 1, "Root/Images/Sprites/Tram/South|0")
			TA_SetProperty("Move Frame", gci_Face_West, 2, "Root/Images/Sprites/Tram/South|0")
			TA_SetProperty("Move Frame", gci_Face_West, 3, "Root/Images/Sprites/Tram/South|0")
			TA_SetProperty("Move Frame", gci_Face_East, 0, "Root/Images/Sprites/Tram/South|1")
			TA_SetProperty("Move Frame", gci_Face_East, 1, "Root/Images/Sprites/Tram/South|1")
			TA_SetProperty("Move Frame", gci_Face_East, 2, "Root/Images/Sprites/Tram/South|1")
			TA_SetProperty("Move Frame", gci_Face_East, 3, "Root/Images/Sprites/Tram/South|1")
			TA_SetProperty("Facing", gci_Face_West)
			TA_SetProperty("Set Ignore Clips", true)
			TA_SetProperty("Activation Script", gsRoot .. "Chapter 5/Dialogue/Tram/Root.lua")
		DL_PopActiveObject()
		TA_Create("TramD")
			TA_SetProperty("Position", 36, 6)
			TA_SetProperty("Move Frame", gci_Face_West, 0, "Root/Images/Sprites/Tram/North|1")
			TA_SetProperty("Move Frame", gci_Face_West, 1, "Root/Images/Sprites/Tram/North|1")
			TA_SetProperty("Move Frame", gci_Face_West, 2, "Root/Images/Sprites/Tram/North|1")
			TA_SetProperty("Move Frame", gci_Face_West, 3, "Root/Images/Sprites/Tram/North|1")
			TA_SetProperty("Facing", gci_Face_West)
			TA_SetProperty("Set Ignore Clips", true)
		DL_PopActiveObject()
		TA_Create("TramE")
			TA_SetProperty("Position", 37, 6)
			TA_SetProperty("Move Frame", gci_Face_West, 0, "Root/Images/Sprites/Tram/North|2")
			TA_SetProperty("Move Frame", gci_Face_West, 1, "Root/Images/Sprites/Tram/North|2")
			TA_SetProperty("Move Frame", gci_Face_West, 2, "Root/Images/Sprites/Tram/North|2")
			TA_SetProperty("Move Frame", gci_Face_West, 3, "Root/Images/Sprites/Tram/North|2")
			TA_SetProperty("Facing", gci_Face_West)
			TA_SetProperty("Set Ignore Clips", true)
		DL_PopActiveObject()
		TA_Create("TramF")
			TA_SetProperty("Position", 38, 6)
			TA_SetProperty("Move Frame", gci_Face_West, 0, "Root/Images/Sprites/Tram/North|3")
			TA_SetProperty("Move Frame", gci_Face_West, 1, "Root/Images/Sprites/Tram/North|3")
			TA_SetProperty("Move Frame", gci_Face_West, 2, "Root/Images/Sprites/Tram/North|3")
			TA_SetProperty("Move Frame", gci_Face_West, 3, "Root/Images/Sprites/Tram/North|3")
			TA_SetProperty("Facing", gci_Face_West)
			TA_SetProperty("Set Ignore Clips", true)
		DL_PopActiveObject()
	
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine](Looks like the tram is set to arrive soon...)") ]])
		fnCutsceneBlocker()
		
		--Move the trams over. They all forcible face west while moving.
		fnCutsceneMoveFace("TramA", 17.25 + 6.0, 6.50, -1, 0, 2.50)
		fnCutsceneMoveFace("TramB", 18.25 + 6.0, 6.50, -1, 0, 2.50)
		fnCutsceneMoveFace("TramC", 19.25 + 6.0, 6.50, -1, 0, 2.50)
		fnCutsceneMoveFace("TramD", 20.25 + 6.0, 6.50, -1, 0, 2.50)
		fnCutsceneMoveFace("TramE", 21.25 + 6.0, 6.50, -1, 0, 2.50)
		fnCutsceneMoveFace("TramF", 22.25 + 6.0, 6.50, -1, 0, 2.50)
		fnCutsceneBlocker()
		fnCutscene([[ AudioManager_PlaySound("World|TramStop") ]])
		fnCutsceneMoveFace("TramA", 17.25 + 3.0, 6.50, -1, 0, 1.00)
		fnCutsceneMoveFace("TramB", 18.25 + 3.0, 6.50, -1, 0, 1.00)
		fnCutsceneMoveFace("TramC", 19.25 + 3.0, 6.50, -1, 0, 1.00)
		fnCutsceneMoveFace("TramD", 20.25 + 3.0, 6.50, -1, 0, 1.00)
		fnCutsceneMoveFace("TramE", 21.25 + 3.0, 6.50, -1, 0, 1.00)
		fnCutsceneMoveFace("TramF", 22.25 + 3.0, 6.50, -1, 0, 1.00)
		fnCutsceneBlocker()
		
		--Trams slow down.
		fnCutsceneMoveFace("TramA", 17.25 + 0.0, 6.50, -1, 0, 0.50)
		fnCutsceneMoveFace("TramB", 18.25 + 0.0, 6.50, -1, 0, 0.50)
		fnCutsceneMoveFace("TramC", 19.25 + 0.0, 6.50, -1, 0, 0.50)
		fnCutsceneMoveFace("TramD", 20.25 + 0.0, 6.50, -1, 0, 0.50)
		fnCutsceneMoveFace("TramE", 21.25 + 0.0, 6.50, -1, 0, 0.50)
		fnCutsceneMoveFace("TramF", 22.25 + 0.0, 6.50, -1, 0, 0.50)
		fnCutsceneBlocker()
		
		--Wait a bit.
		fnCutsceneWait(45)
		fnCutsceneBlocker()

		--Change the facing of part C. This causes the door to open.
		fnCutscene([[ AudioManager_PlaySound("World|TramArrive") ]])
		fnCutsceneFace("TramC", 1, 0)
		fnCutsceneBlocker()

	end

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
