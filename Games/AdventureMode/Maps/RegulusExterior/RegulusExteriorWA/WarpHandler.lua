-- |[Warp Handler]|
--If the player warps here using the campfire, this script is called to reposition them as necessary.

--Constants. This is the important part that changes between each room.
local fTargetX = (36.75 * gciSizePerTile)
local fTargetY = (32.50 * gciSizePerTile)

--If 55 was not in the party when the warp started, add her.
local bWas55PresentAtStart = fnIsCharacterPresent("Tiffany")
if(bWas55PresentAtStart == false) then
    fnAddPartyMember("Tiffany")
end

--If Christine was an organic, she shifts back to Golem form. This is a convenience feature.
local bShiftFromOrganic = false
local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
if(sChristineForm == "Human" or sChristineForm == "Raiju") then
    bShiftFromOrganic = true
    fnCutscene([[ LM_ExecuteScript(gsRoot .. "FormHandlers/Christine/Form_Golem.lua") ]])
end

--Execute.
LM_ExecuteScript(gsStandardWarpHandler, fTargetX, fTargetY)

--If 55 was not present before the warp started, and we have not seen this dialogue yet, show it.
local iSaidWarpDialogue = VM_GetVar("Root/Variables/Global/2855/iSaidWarpDialogue", "N")
local iSaidAutoTransformDialogue = VM_GetVar("Root/Variables/Global/Christine/iSaidAutoTransformDialogue", "N")
if(iSaidWarpDialogue == 0.0 and bWas55PresentAtStart == false) then
    
    --Flag.
    VM_SetVar("Root/Variables/Global/2855/iSaidWarpDialogue", "N", 1.0)
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
    fnCutscene([[ Append("55:[E|Neutral] ^!!!^[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] ^55?^[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] ^Interesting.[P] The reports *did* indicate your runestone allowed limited-scope teleportation of an unknown mechanism.^[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Neutral] ^Then why are you surprised?^[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] ^I am not unacquainted with arcane teleportation spells.[P] However, I am, or was, unacquainted with being teleported while not near the caster.^[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Sad] ^Oh...[P] Yes, I suppose you weren't.[P] Maybe my rune's magic pulled you along because you're such a close friend?^[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] ^An unproveable supposition.[P] You are not the ideal candidate to advance the Cause of Science.^[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Offended] ^Gee, thanks, 55.^[B][C]") ]])
    
    --Extra dialogue:
    if(bShiftFromOrganic == true and iSaidAutoTransformDialogue == 0.0) then
        fnCutscene([[ Append("55:[E|Neutral] ^You have also transformed yourself mid-teleport.^[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Laugh] ^Oh, I have, haven't I?[P] That's terribly convenient!^[B][C]") ]])
        fnCutscene([[ Append("55:[E|Upset] ^Are you saying you did not know this would happen, yet decided to teleport to an unpressurized location anyway?^[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] ^I am...[P] saying that, yes.^[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] ^Please have Unit 499323 examine your cranial chassis at her earliest convenience.[P] It is clearly damaged.[P] Now, let us proceed.^") ]])
    
    --Normal:
    else
        fnCutscene([[ Append("55:[E|Neutral] ^The magic on the runestone has proved convenient for our purposes.[P] Let us continue.^") ]])
    end
    fnCutsceneBlocker()

--Auto-shifted from organic form.
elseif(bShiftFromOrganic == true and iSaidAutoTransformDialogue == 0.0) then

    --Flag.
    VM_SetVar("Root/Variables/Global/Christine/iSaidAutoTransformDialogue", "N", 1.0)
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] ^...?[P] I'm metal again!^[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] ^I suppose the runestone transformed me while warping to keep me from being exposed to a vacuum.^[B][C]") ]])
    fnCutscene([[ Append("55:[E|Upset] ^Are you saying you did not know this would happen, yet decided to teleport to an unpressurized location anyway?^[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Smirk] ^I am...[P] saying that, yes.^[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] ^Please have Unit 499323 examine your cranial chassis at her earliest convenience.[P] It is clearly damaged.[P] Now, let us proceed.^") ]])
    fnCutsceneBlocker()

end
