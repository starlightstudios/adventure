-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    --Set facing.
    TA_SetProperty("Face Character", "PlayerEntity")
    
    --Variables.
    local iHasDarkmatterForm = VM_GetVar("Root/Variables/Global/Christine/iHasDarkmatterForm", "N")
    
    -- |[DarkmatterA]|
    if(sActorName == "DarkmatterA") then
        if(iHasDarkmatterForm == 0.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("[VOICE|Leader](This Darkmatter clearly knocked over the light here, but is acting innocent.)") ]])
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Darkmatter:[VOICE|DarkmatterGirl] I cannot tell a lie.[P] I knocked over this light.[B][C]") ]])
            fnCutscene([[ Append("Darkmatter:[VOICE|DarkmatterGirl] I'm not going to put it back up.[P] It's supposed to be knocked over.[P] That is the right way of the universe.") ]])
        end
        
    -- |[DarkmatterB]|
    elseif(sActorName == "DarkmatterB") then
        
        if(iHasDarkmatterForm == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("[VOICE|Leader](I'm pretty sure this Darkmatter was rearranging the crates, but since she noticed me, she stopped.)") ]])
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Darkmatter:[VOICE|DarkmatterGirl] These crates are organized incorrectly.[P] I am reorganizing them to help our hardmatter friends.[B][C]") ]])
            fnCutscene([[ Append("Darkmatter:[VOICE|DarkmatterGirl] Unfortunately I don't know how to organize them in three dimensions, so I expect to be here for a few thousand years.[P] I'll get it right eventually.") ]])
        end
    end
end
