-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    --Set facing.
    TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[GolemPipeAttendant]|
    if(sActorName == "GolemPipeAttendant") then
	
        --Variables.
        local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
        local iTalkedToSophie = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N")
        local iPipeGolemState = VM_GetVar("Root/Variables/Chapter5/Scenes/iPipeGolemState", "N")
        
        --Common.
        TA_SetProperty("Face Character", "PlayerEntity")
        
        --If on your way for a function assignment, the golem responds much differently.
        if(iTalkedToSophie == 0.0) then
            
            --Flag.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iPipeGolemState", "N", 1.0)
            
            --First time talking to this golem.
            if(iPipeGolemState == 0.0) then
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Serious") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
                fnCutscene([[ Append("Golem: Oh no![B][C]") ]])
                fnCutscene([[ Append("Golem: Lord Golem, I apologize![P] I've been very productive![P] Please - [P][CLEAR]") ]])
                fnCutscene([[ Append("771852:[E|Serious] My current function is to report to Regulus City for further assignment.[B][C]") ]])
                fnCutscene([[ Append("Golem: H-[P]huh?[P] Oh![P] You're a new unit![B][C]") ]])
                fnCutscene([[ Append("Golem: In that case, Regulus City is just a short walk north of here.[P] Head up the ladder, out the airlock, and around the building.[B][C]") ]])
                fnCutscene([[ Append("771852:[E|Serious] Proceeding to Regulus City for further assignment.[B][C]") ]])
                fnCutscene([[ Append("Golem: (Really dodged a pulse round there...)") ]])
                fnCutsceneBlocker()
            
            --Repeat.
            else
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Serious") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
                fnCutscene([[ Append("Golem: In that case, Regulus City is just a short walk north of here.[P] Head up the ladder, out the airlock, and around the building.[B][C]") ]])
                fnCutscene([[ Append("771852:[E|Serious] Proceeding to Regulus City for further assignment.") ]])
                fnCutsceneBlocker()
            end
            
        --Non-Golem Christine. 55 will always be present but the player can't do the quest.
        elseif(sChristineForm ~= "Golem") then
        
            --Human variant.
            if(sChristineForm == "Human") then
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
                fnCutscene([[ Append("Golem: A human?[P] In here?[P] Will this day never end?[P] Now I have to convert you![B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] (Mmmmm...[P] that'd be nice.)[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] Stand down, unit.[P] This organic is in my custody.[B][C]") ]])
                fnCutscene([[ Append("Golem: Oh, thank goodness.[P] My plate is already full as it is.[B][C]") ]])
                fnCutscene([[ Append("Golem: ...[P] That expression doesn't make any sense for me, now that I think about it.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] Don't be such a buzzkill, Command Unit![B][C]") ]])
                fnCutscene([[ Append("Golem: ...?[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] ...[P] Are you serious?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] *I want to be converted...[P] to feel the nanofluid cover me, fill me...*[B][C]") ]])
                fnCutscene([[ Append("55:[E|Upset] *Shut up, idiot.*[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] That's quite enough, organic.[P] I will electrocute you if you misbehave.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Scared] Eep![B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] You have to be firm with these organics.[P] They're dumber than even the dumbest latex drone.[B][C]") ]])
                fnCutscene([[ Append("Golem: Heh.[P] Is she dumber than a scraprat?[B][C]") ]])
                fnCutscene([[ Append("55:[E|Smug] Sometimes I wonder.[B][C]") ]])
                if(iPipeGolemState == 0.0 or iPipeGolemState == 1.0) then
                    fnCutscene([[ Append("55:[E|Neutral] (We should return when Christine is in Lord Golem form...)[B][C]") ]])
                end
                fnCutscene([[ Append("55:[E|Neutral] We'll take our leave now.[P] Back to work, unit.[B][C]") ]])
                fnCutscene([[ Append("Golem: Right away![B][C]") ]])
                fnCutscene([[ Append("Golem: (I don't know how she's going to get that organic to Regulus City without a vac suit, but I guess it's not my problem.)") ]])
                fnCutsceneBlocker()
                
            --LatexDrone! Beep boop!
            elseif(sChristineForm == "LatexDrone") then
            
                --Sequence.
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
                fnCutscene([[ Append("Golem: (Okay, act synthetic...)[B][C]") ]])
                fnCutscene([[ Append("Golem: Greetings, Command Unit.[P] Your Drone Unit is looking well-polished.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] COMPLIMENT LOGGED.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] Be silent, Drone Unit.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Laugh] AFFIRMATIVE.[P] BEGINNING SILENCE ROUTINES.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] UNIT HAS BEEN SILENT FOR 1 CYCLE.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] UNIT HAS BEEN SILENT FOR 2 CYCLES.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] UNIT HAS BEEN SILENT FOR 3 CYCLES...[B][C]") ]])
                fnCutscene([[ Append("Golem: I'm very sorry to interrupt, Command Unit, but does your Drone need maintenance?[B][C]") ]])
                fnCutscene([[ Append("Golem: The cognitive inhibitor might be overclocked.[P] I don't think I've seen one that dumb before.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Smug] She is not here for her intelligence.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] UNIT IS HERE FOR ITS LOOKS.[P] UNIT IS PRETTY.[B][C]") ]])
                fnCutscene([[ Append("Golem: So...[P] was there something I could help you with?[B][C]") ]])
                if(iPipeGolemState == 0.0 or iPipeGolemState == 1.0) then
                    fnCutscene([[ Append("55:[E|Neutral] (We should return when Christine is in Lord Golem form...)[B][C]") ]])
                end
                fnCutscene([[ Append("55:[E|Neutral] Not at the moment.[P] Back to work, unit.") ]])
                fnCutsceneBlocker()
                
            --Darkmatter
            elseif(sChristineForm == "Darkmatter") then
            
                --Sequence.
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
                fnCutscene([[ Append("Golem: Uhhhhh....[P] Command Unit?[P] There's a Darkmatter here...[P] Is she with you?[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] You could say that.[P] She has decided to follow me around for reasons unknown to me.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] As per usual, my attempts to shoo her away have failed.[P] They are difficult to influence with physical weaponry.[B][C]") ]])
                fnCutscene([[ Append("Golem: You shot at it?[B][C]") ]])
                fnCutscene([[ Append("55:[E|Smirk] Yes.[P] Yes I did.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] (Better not say anything or I'll blow my cover...)[B][C]") ]])
                fnCutscene([[ Append("55:[E|Smug] It cowered and hid and I suspect it was crying starlight tears.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Angry] (You're a laugh riot, 55!)[B][C]") ]])
                fnCutscene([[ Append("Golem: Uh, sure.[P] Okay.[B][C]") ]])
                fnCutscene([[ Append("Golem: Was there something I could do for you, Command Unit?[B][C]") ]])
                if(iPipeGolemState == 0.0 or iPipeGolemState == 1.0) then
                    fnCutscene([[ Append("55:[E|Neutral] (We should return when Christine is in Lord Golem form...)[B][C]") ]])
                end
                fnCutscene([[ Append("55:[E|Neutral] Not at the moment.[P] Back to work, unit.") ]])
                fnCutsceneBlocker()
                
            --Eldritch Dreamer
            elseif(sChristineForm == "Eldritch") then
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
                fnCutscene([[ Append("Golem: !!![P] Oh my goodness![P] There -[P] there's[P] -[P] !![P] - [P][CLEAR]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Do not be afraid, my friend.[P] I am not here to harm you.[B][C]") ]])
                fnCutscene([[ Append("Golem: Huh?[P] Oh.[P] Hi.[B][C]") ]])
                fnCutscene([[ Append("Golem: *Psst, organic![P] There's a Command Unit right behind you!*[B][C]") ]])
                fnCutscene([[ Append("Golem: *Act productive!*[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] Oh for crying out loud...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] This Command Unit is not here to punish you.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Smug] Yet.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] Don't scare the poor thing![B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] I do what I want, when I want.[B][C]") ]])
                fnCutscene([[ Append("Golem: (Oh goodness, what have I done to deserve this?)[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] Does it have to involve victimizing some poor worker?[P] You scared her half to retirement![B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] Emotions are not considerations of machines.[B][C]") ]])
                fnCutscene([[ Append("Golem: Err, Units?[B][C]") ]])
                fnCutscene([[ Append("Golem: Is there something I can do for you?[P] I'm attempting to -[P] maximize productivity -[P] at the moment.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] My form doesn't intimidate you?[B][C]") ]])
                fnCutscene([[ Append("Golem: Of course not, Unit.[P] Organics come in many shapes and forms.[P] I am not judgemental.[B][C]") ]])
                fnCutscene([[ Append("Golem: *Just try not to set off that Command Unit or she'll have us both retired!*[B][C]") ]])
                if(iPipeGolemState == 0.0 or iPipeGolemState == 1.0) then
                    fnCutscene([[ Append("55:[E|Neutral] (We should return when Christine is in Lord Golem form...)[B][C]") ]])
                end
                fnCutscene([[ Append("55:[E|Neutral] We will settle our differences elsewhere.[P] Back to work, unit.") ]])
                fnCutsceneBlocker()
                
            --Electrosprite!
            elseif(sChristineForm == "Electrosprite") then
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
                fnCutscene([[ Append("Golem: FIFO?[P] Is that -[P] oh no you're not her.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Do I remind you of someone -[P] someone special to you?[B][C]") ]])
                fnCutscene([[ Append("Golem: Heh, a little.[B][C]") ]])
                fnCutscene([[ Append("Golem: Uh, should I assume that this Command Unit is...[P] cool?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] She's a sympathizer.[B][C]") ]])
                fnCutscene([[ Append("Golem: And is she your tandem unit?[B][C]") ]])
                fnCutscene([[ Append("55:[E|Angry] No.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Scared] Woah, calm down![B][C]") ]])
                fnCutscene([[ Append("Golem: Sorry, I just assumed since, well, that's how Electrosprites come to be.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] We're just good friends.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] ...[B][C]") ]])
                fnCutscene([[ Append("Golem: FIFO is sort of a -[P] synchro-buddy, I guess.[P] We're not an item, but we do synchronize.[P] Often.[B][C]") ]])
                fnCutscene([[ Append("Golem: She's kind of odd.[P] I send her a message on the network, but she has other synchro-buddies and always visits us in the exact order we messaged her.[B][C]") ]])
                fnCutscene([[ Append("Golem: Is it weird that I share her with five other golems?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Not at all.[P] Love takes many forms.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] You're all consenting adults, right?[P] If you can all get along, there's no harm in it.[B][C]") ]])
                fnCutscene([[ Append("Golem: I still feel odd about it, but the synchronizations are so good...[P] I'd probably go crazy without her in my life.[B][C]") ]])
                fnCutscene([[ Append("Golem: The Electrosprites becoming real is probably the best thing that's ever happened to Regulus City, in my opinion.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] We're so happy to help![P] Just be sure to keep us a secret from the Administration![B][C]") ]])
                fnCutscene([[ Append("Golem: Of course![P] I'd never betray FIFO![P] I'd sooner be retired![B][C]") ]])
                if(iPipeGolemState == 0.0 or iPipeGolemState == 1.0) then
                    fnCutscene([[ Append("55:[E|Neutral] (We should return when Christine is in Lord Golem form...)[B][C]") ]])
                end
                fnCutscene([[ Append("55:[E|Neutral] Good.[P] That confirms my initial assessment of the security situation.[P] We'll take our leave, then.") ]])
                fnCutsceneBlocker()
                
            --Steam Droid
            elseif(sChristineForm == "SteamDroid") then
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
                fnCutscene([[ Append("Golem: Well there's something you don't see everyday.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Oh, don't worry.[P] I'm a friend.[B][C]") ]])
                fnCutscene([[ Append("Golem: *I'm more worried about the Command Unit right behind you...*[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Oh, her?[P] She's cool.[P] Right, Command Unit?[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] ...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] Cool as ice.[B][C]") ]])
                fnCutscene([[ Append("Golem: Are you a -[P] security contractor, or something?[B][C]") ]])
                fnCutscene([[ Append("55:[E|Smirk] To that effect, yes.[P] And she is currently on a secret mission.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] Obviously you are to omit that detail in any reports you make.[B][C]") ]])
                fnCutscene([[ Append("Golem: Yes, Command Unit![B][C]") ]])
                fnCutscene([[ Append("Golem: So was there anything I could do for you?[B][C]") ]])
                if(iPipeGolemState == 0.0 or iPipeGolemState == 1.0) then
                    fnCutscene([[ Append("55:[E|Neutral] (We should return when Christine is in Lord Golem form...)[B][C]") ]])
                end
                fnCutscene([[ Append("55:[E|Neutral] Not at the moment.[P] Back to work, unit.") ]])
                fnCutsceneBlocker()
                
            --Doll
            elseif(sChristineForm == "Doll") then
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
                fnCutscene([[ Append("Golem: Oh, joy.[P] Are you here to shoot me?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Scared] Perish the thought![P] Why would you think that?[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] She is doing something illicit, likely, and believes we have caught her.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] Why else would two armed command units appear at this outpost?[B][C]") ]])
                fnCutscene([[ Append("Golem: I'm not doing any crimes, command unit.[P] I also know that that rarely matters to the security services.[B][C]") ]])
                fnCutscene([[ Append("Golem: Just, please don't do it in front of the scraprats.[P] Don't scar their fragile psyches.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Offended] We're not going to shoot you.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] We were just making a surprise inspection.[B][C]") ]])
                fnCutscene([[ Append("Golem: Given how often this place breaks down, an inspection would probably also be grounds for termination.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Upset] You seem determined to be retired.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Look, if you need some help, I'll send along a friend of mine.[P] She's a lord golem in Sector 96.[B][C]") ]])
                if(iPipeGolemState >= 3.0) then
                    fnCutscene([[ Append("Golem: Oh, she was already here.[P] You two are friends?[B][C]") ]])
                    fnCutscene([[ Append("Golem: I take back everything I said, then.[P] Very nice to meet you.") ]])
                else
                    fnCutscene([[ Append("Golem: Heh, you mean Sophie's new squeeze.[P] I've heard good things.[B][C]") ]])
                    fnCutscene([[ Append("Golem: If you want to go out of your way to help out a lowly slave unit, it's much appreciated.[P] Thank you.") ]])
                end
            
            --Raiju
            elseif(sChristineForm == "Raiju") then
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
                fnCutscene([[ Append("Golem: My goodness, how did you get here without a vac-suit?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] I held my breath.[B][C]") ]])
                fnCutscene([[ Append("Golem: Wouldn't the water on your eyes boil off?[P] And your ears pop?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Maybe if you're a total wuss, which I'm not.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] Christine, that is enough teasing.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] This raiju is very playful, as you can see.[P] We left her suit on the main floor.[B][C]") ]])
                fnCutscene([[ Append("Golem: Oh, well, I suppose that's all right.[P] You should be careful, though.[P] Outposts like this are far more prone to depressurization than the city.[B][C]") ]])
                fnCutscene([[ Append("Golem: ...[P] What are you two doing out here, anyway?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Just visiting.[P] Need a jolt?[B][C]") ]])
                fnCutscene([[ Append("Golem: Oh no, that's the last thing I need.[P] I have enough trouble defragmenting with a half-charged core.[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] Very good.[P] Carry on, unit.") ]])
        
            elseif(sChristineForm == "Secrebot") then
                fnCutscene([[ WD_SetProperty("Show") ]])
                fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
                fnCutscene([[ Append("Golem: Hello, command unit.[P] May I compliment your secrebot's appearance?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Blush] Go right ahead.[B][C]") ]])
                fnCutscene([[ Append("Golem: That -[P] uhhh, are you blushing?[B][C]") ]])
                fnCutscene([[ Append("Golem: Can secrebots do that?[P] Um, what is the purpose of your visit, command unit?[B][C]") ]])
                fnCutscene([[ Append("55:[E|Neutral] Just visiting.[P] Carry on.") ]])
            end
        
        --Normal.
        else
        
            --First time talking to the Golem after conversion, didn't talk to her the first pass:
            if(iPipeGolemState == 0.0) then
                VM_SetVar("Root/Variables/Chapter5/Scenes/iPipeGolemState", "N", 2.0)
                fnStandardMajorDialogue()
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
                fnCutscene([[ Append("Golem: Lord Golem![P] H-[P]Hello![B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Hello, unit![P] May I get your designation?[B][C]") ]])
                fnCutscene([[ Append("565102: Unit 565102...[B][C]") ]])
                fnCutscene([[ Append("565102: Please don't be too hard on me...[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Sad] Why would I be hard on you?[P] Is something wrong?[B][C]") ]])
                fnCutscene([[ Append("565102: You didn't -[P] oh.[P] Err, well.[P] I can explain.[B][C]") ]])
                fnCutscene([[ Append("565102: Everything was going well down here, but then this stupid scraprat bit through one of the pipes and wrecked the delicate balance of pressure I had come up with.[B][C]") ]])
                fnCutscene([[ Append("565102: ...[P] Oh, and then he kind of exploded.[P] They do that sometimes.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Scared] Oh dear![B][C]") ]])
                fnCutscene([[ Append("565102: I just got done putting him back together, but now -[P] ugh![B][C]") ]])
                fnCutscene([[ Append("565102: Energy transfer is down by 70 percent and I have to get the pipe pressures realigned with the missing section removed.[B][C]") ]])
                fnCutscene([[ Append("565102: You didn't know all this?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] I'm sorry.[P] Did you put in a work order?[B][C]") ]])
                fnCutscene([[ Append("565102: Oh I don't want to bother 499323.[P] She's always so backlogged, Maintenance and Repair in Sector 96 will probably take months to come help.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] I am Unit 771852, Lord Golem of Maintenance and Repair, Sector 96.[B][C]") ]])
                fnCutscene([[ Append("565102: ...[B][C]") ]])
                fnCutscene([[ Append("565102: So you're here to fix the pipes?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] I am often told I have great timing.[B][C]") ]])
                fnCutscene([[ Append("565102: I'll say![P] You're a lifesaver![B][C]") ]])
                fnCutscene([[ Append("565102: Oh and, please tell 499323 I said hi.[P] She's always very sweet when I go in for maintenance.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] So.[P] How do I go about fixing the pipes?[B][C]") ]])
                fnCutscene([[ Append("565102: Well, it's a little complex. I can explain how the pipes work, and then maybe you can give it a shot.[B][C]") ]])
                fnCutscene([[ WD_SetProperty("Activate Topics After Dialogue", "PipeNightmare") ]])
            
            --First time talking to the golem after conversion, talked to her the first pass:
            elseif(iPipeGolemState == 1.0) then
                VM_SetVar("Root/Variables/Chapter5/Scenes/iPipeGolemState", "N", 2.0)
                fnStandardMajorDialogue()
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
                fnCutscene([[ Append("Golem: You're back![P] It's been a while...[P] Did you get your function assignment?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] I did![P] Thank you for the directions![B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] I was assigned as Lord Golem of Maintenance and Repair, Sector 96.[B][C]") ]])
                fnCutscene([[ Append("Golem: Wait.[P] They got a Lord Golem?[P] Really?[B][C]") ]])
                fnCutscene([[ Append("Golem: Wow, how long has it been?[P] Poor 499323 is always in there by herself.[B][C]") ]])
                fnCutscene([[ Append("Golem: Uh, sorry.[P] I got a little off course.[P] Are you here to fix the pipes?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] No.[P] Is there something wrong with them?[B][C]") ]])
                fnCutscene([[ Append("Golem: Well, yes![P] This stupid scraprat bit through a pipe and ruined my delicate balance of pressure alignment![B][C]") ]])
                fnCutscene([[ Append("Golem: ...[P] And then he blew up.[P] I think they do that when they get flustered.[P] Or bored.[P] Maybe.[B][C]") ]])
                fnCutscene([[ Append("Golem: I put him back together, but that was the easy part...[B][C]") ]])
                fnCutscene([[ Append("Golem: I don't think I can fix the pipes by myself, and power transfer is down by 70 percent.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] Sounds like you're in need.[P] Lucky I came by.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Smirk] I should put in a work order before I get started.[P] May I have your designation?[B][C]") ]])
                fnCutscene([[ Append("565102: Unit 565102.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|PDU] All right, my PDU will file the work order.[P][EMOTION|Christine|Smirk] So.[P] How do I go about fixing the pipes?[B][C]") ]])
                fnCutscene([[ Append("565102: Here, I'll try to explain...[B][C]") ]])
                fnCutscene([[ WD_SetProperty("Activate Topics After Dialogue", "PipeNightmare") ]])
            
            --We already talked to the golem, but haven't fixed the pipes.
            elseif(iPipeGolemState == 2.0) then
                fnStandardMajorDialogue()
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
                fnCutscene([[ Append("565102: Any questions for me?[B][C]") ]])
                fnCutscene([[ WD_SetProperty("Activate Topics After Dialogue", "PipeNightmare") ]])
        
            --Talking to the golem, the pipes have just been fixed.
            elseif(iPipeGolemState == 3.0) then
                VM_SetVar("Root/Variables/Chapter5/Scenes/iPipeGolemState", "N", 4.0)
                fnStandardMajorDialogue()
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
                fnCutscene([[ Append("565102: Thanks again for all your help.[B][C]") ]])
                fnCutscene([[ Append("565102: Hey, do you think I should maybe remove the teeth on my scraprat here?[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Neutral] No, I don't think so.[P] Maybe give him some pipe chunks to chew on so he doesn't bite the real ones?[B][C]") ]])
                fnCutscene([[ Append("565102: That's a good idea, maybe I'll do that.[B][C]") ]])
                fnCutscene([[ Append("565102: And then wonder why a scraprat was coded to want to chew on things...[P] oh well.[P] I guess it's fundamental to being a rat, isn't it?") ]])
                fnCutsceneBlocker()
            
            --Successive cases.
            else
                fnStandardMajorDialogue()
                fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
                fnCutscene([[ Append("565102: Thanks for your help, Lord Unit.[P] Give Unit 499323 my best when you see her.[B][C]") ]])
                fnCutscene([[ Append("Christine:[E|Happy] Will do!") ]])
                fnCutsceneBlocker()
            end
        end
    
    -- |[ScrapratPipeAttendant]|
    elseif(sActorName == "ScrapratPipeAttendant") then
        
        --Variables.
        local iTalkedToSophie = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N")
        
        --If on your way for a function assignment, the scraprat responds much differently.
        if(iTalkedToSophie == 0.0) then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("[VOICE|Christine](The scraprat smiles stupidly.[P] It seems to be giggling periodically.)[B][C]") ]])
            fnCutscene([[ Append("[VOICE|Christine](This has nothing to do with my programming.[P] I should proceed to Regulus City for further assignment.)") ]])
            fnCutsceneBlocker()

        --Normal.
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Scraprat:[VOICE|Drone] Scraprat primed and ready![B][C]") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] No![P] Scraprat un-prime![P] Un-prime![B][C]") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Phew...[P] They say that right before they explode...") ]])
            fnCutsceneBlocker()
        end
    end
end
