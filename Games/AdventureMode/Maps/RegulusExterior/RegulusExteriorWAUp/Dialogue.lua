-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    --Set facing.
    TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[GolemA]|
    if(sActorName == "GolemA") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] ^Greetings.[P] If you are here for an inspection, please be aware that the lights on the transit tracks turn red when a tram is passing.^[B][C]") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] ^Also we leave the area unpressurized due to the frequency of trams.[P] The facilities are adjusted for short-range radio transmissions.^") ]])
    
    -- |[GolemB]|
    elseif(sActorName == "GolemB") then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] ^This small facility and others like it service trams from all across Regulus City, but all my fabrication orders are for combat equipment.^[B][C]") ]])
        fnCutscene([[ Append("Golem:[VOICE|Golem] ^I am not sure what we will do when we run out of spare parts, but my Lord Unit seems unconcerned...^") ]])
    
    -- |[GolemC]|
    elseif(sActorName == "GolemC") then
	
        --Variables.
        local iMetWAFacilityLord = VM_GetVar("Root/Variables/Chapter5/Scenes/iMetWAFacilityLord", "N")
        
        --Meeting the lord for the first time.
        if(iMetWAFacilityLord == 0.0) then
            
            --Flag.
            VM_SetVar("Root/Variables/Chapter5/Scenes/iMetWAFacilityLord", "N", 1.0)
            
            --Dialogue.
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
            fnCutscene([[ Append("Golem: ^Oh thank goodness, some intelligent conversation.[P] Fellow Lord Unit, your visit is appreciated.^[B][C]") ]])
            fnCutscene([[ Append("Golem: ^Unbelievably, I must share my quarters with the Slave Units on duty here.[P] I suffer this indignity with quiet resolve, as you can see.^[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Offended] (Quiet resolve involves complaining to the first unit to come by?[P] Sure.)[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] ^We must all make sacrifices for the Cause.[P] This includes Lord Units.^[B][C]") ]])
            fnCutscene([[ Append("Golem: ^Without question, of course![P] Obviously, I would never allow them to actually use my quarters except for defragmentation.^[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Neutral] ^As much as I am loathe to admit it, allowing my Slave Units additional amenities has increased productivity in Sector 96.^[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] ^Perhaps you should allow them to use the RVD and the furnishings here?[P] The improvement [P]*did*[P] look excellent on my reports.^[B][C]") ]])
            fnCutscene([[ Append("Golem: ^...[P] Did it?^[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Happy] ^Oh, spectacularly so.[P] Just look at the productivity charts for Unit 499323.^[B][C]") ]])
            fnCutscene([[ Append("Golem: ^I'll get my PDU to bring them up...[P] Oh![P] Truly?^[B][C]") ]])
            fnCutscene([[ Append("Golem: ^...[P] If I must do this for the Cause of Science...^[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] ^To suffer for a cause is martyrdom.[P] Remember that.^[B][C]") ]])
            fnCutscene([[ Append("Golem: ^Thank you, Unit 771852.[P] If you need a favour at any time, I will do my best to aid you.^[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Laugh] (Sucker![P] Have fun being nice to your Slave Units and increasing efficiency!)[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Sad] (...[P] Why do I have to trick Lord Units into doing their jobs properly...?)") ]])
            fnCutsceneBlocker()
        
        --Repeat.
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "GolemLord", "Neutral") ]])
            fnCutscene([[ Append("Golem: ^Hello, Unit 771852.[P] I wish you well.[P] Serve the Cause of Science.^[B][C]") ]])
            fnCutscene([[ Append("Christine:[E|Smirk] ^To the best of my ability.[P] Thank you.^") ]])
            fnCutsceneBlocker()
        end
    end
end
