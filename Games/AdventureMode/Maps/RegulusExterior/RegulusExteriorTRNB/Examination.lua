-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
if(sObjectName == "LadderN") then
	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("RegulusExteriorTRNA", "FORCEPOS:12.0x7.0x0")
	
elseif(sObjectName == "LadderS") then
	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("RegulusExteriorTRNA", "FORCEPOS:12.0x13.0x0")
	
elseif(sObjectName == "LadderS2") then
	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("RegulusExteriorSB", "FORCEPOS:104.0x46.0x0")

-- |[Examinables]|
elseif(sObjectName == "PartsShelfA") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (The parts here are all corroded and useless.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "PartsShelfB") then
	local iServicePartsB = VM_GetVar("Root/Variables/Chapter5/Scenes/iServicePartsB", "N")
	if(iServicePartsB == 0.0) then
		LM_ExecuteScript(gsItemListing, "Recycleable Junk")
		VM_SetVar("Root/Variables/Chapter5/Scenes/iServicePartsB", "N", 1.0)
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Thought: (There's some junk here which could be recycled...)[B][C]") ]])
		fnCutscene([[ Append("Thought: [SOUND|World|TakeItem](Received Recycleable Junk x1)") ]])
		fnCutsceneBlocker()
	else
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Thought: (There's nothing useful left on the shelf.)") ]])
		fnCutsceneBlocker()
	end
	
elseif(sObjectName == "PartsShelfC") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (These parts aren't even worth scrapping.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "PartsShelfD") then
	local iServicePartsC = VM_GetVar("Root/Variables/Chapter5/Scenes/iServicePartsC", "N")
	if(iServicePartsC == 0.0) then
		LM_ExecuteScript(gsItemListing, "Bent Tools")
		VM_SetVar("Root/Variables/Chapter5/Scenes/iServicePartsC", "N", 1.0)
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Thought: (Though damaged, I think some of these tools could be fixed.)[B][C]") ]])
		fnCutscene([[ Append("Thought: [SOUND|World|TakeItem](Received Bent Tools x1)") ]])
		fnCutsceneBlocker()
	else
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Thought: (There's nothing useful left on the shelf.)") ]])
		fnCutsceneBlocker()
	end
	
elseif(sObjectName == "PartsShelfE") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (Junk.)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "TerminalA") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (The parts manifest indicates that not all of the useful parts were taken when this station was abandoned.[P] I should check the shelves for anything useful.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "TerminalB") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (This unit left a journal entry talking about someone named 'Professor Killsalot'.[P] There's lots of gushing praise, but few details on who this professor is.)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "TerminalC") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (This terminal is non-functional.[P] In fact, the interior has a compartment where it seems cans of lubricant were stored.[P] I guess that's one way of reusing broken equipment!)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "TerminalD") then

    --Variables:
    local iRuneUpgradeBriefing = VM_GetVar("Root/Variables/Chapter5/Scenes/iRuneUpgradeBriefing", "N")
    local iRuneUpgradeTransit  = VM_GetVar("Root/Variables/Chapter5/Scenes/iRuneUpgradeTransit", "N")
    
    --Not on the quest:
    if(iRuneUpgradeBriefing == 0.0) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("[VOICE|Christine](A network-bounce terminal, connecting nearby facilities and transmitting data between them.)") ]])
        fnCutsceneBlocker()
    
    --On the quest, hasn't gotten the piece yet.
    elseif(iRuneUpgradeBriefing == 1.0 and iRuneUpgradeTransit == 0.0) then
        VM_SetVar("Root/Variables/Chapter5/Scenes/iRuneUpgradeTransit", "N", 1.0)
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("[VOICE|Christine](A network-bounce terminal, connecting nearby facilities and transmitting data between them.[P] It seems to have some lingering encryption data 55 might able to use.)[B][C]") ]])
        fnCutscene([[ Append("[VOICE|Christine][SOUND|World|TakeItem] (Got part of the academic decryption engine!)") ]])
        fnCutsceneBlocker()
    
    --Already got the piece.
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ Append("[VOICE|Christine](A network-bounce terminal, connecting nearby facilities and transmitting data between them.[P] It had some of the codes 55 needs to decrypt academic data about my runestone.)") ]])
        fnCutsceneBlocker()
    end

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
