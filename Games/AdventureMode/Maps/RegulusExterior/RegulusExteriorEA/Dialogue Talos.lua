-- |[ ================================ Dialogue Script - Talos ================================= ]|
--Entry point for dialogue with Talos. Note: There are no power/initiative catalysts in this chapter.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[ =================================== fnSetTalosTopics() =================================== ]|
local function fnSetTalosTopics()
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
    
    --Variables.
    local iTalosShowHealth     = VM_GetVar("Root/Variables/Chapter5/Talos/iTalosShow_Health", "N")
    --local iTalosShowPower      = VM_GetVar("Root/Variables/Chapter5/Talos/iTalosShow_Power", "N")
    --local iTalosShowInitiative = VM_GetVar("Root/Variables/Chapter5/Talos/iTalosShow_Initiative", "N")
    local iTalosShowAccuracy   = VM_GetVar("Root/Variables/Chapter5/Talos/iTalosShow_Accuracy", "N")
    local iTalosShowEvade      = VM_GetVar("Root/Variables/Chapter5/Talos/iTalosShow_Evasion", "N")
    local iTalosShowSkills     = VM_GetVar("Root/Variables/Chapter5/Talos/iTalosShow_Skills", "N")
    
    --Always available.
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Fashion\",  " .. sDecisionScript .. ", \"Fashion\") ")
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Hackers\",  " .. sDecisionScript .. ", \"Hackers\") ")
    
    --Health Catalysts.
    if(iTalosShowHealth == 0.0) then
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Health Catalysts\",  " .. sDecisionScript .. ", \"QueryHealth\") ")
    end
    --if(iTalosShowPower == 0.0) then
    --    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Power Catalysts\",  " .. sDecisionScript .. ", \"QueryPower\") ")
    --end
    --if(iTalosShowInitiative == 0.0) then
    --    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Initiative Catalysts\",  " .. sDecisionScript .. ", \"QueryInitiative\") ")
    --end
    if(iTalosShowAccuracy == 0.0) then
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Accuracy Catalysts\",  " .. sDecisionScript .. ", \"QueryAccuracy\") ")
    end
    if(iTalosShowEvade == 0.0) then
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Evasion Catalysts\",  " .. sDecisionScript .. ", \"QueryEvasion\") ")
    end
    if(iTalosShowSkills == 0.0) then
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Skills Catalysts\",  " .. sDecisionScript .. ", \"QuerySkills\") ")
    end
    
    --Goodbye. Always available.
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Goodbye\",  " .. sDecisionScript .. ", \"Goodbye\") ")
    fnCutsceneBlocker()
end

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[Common Setup]|
    TA_SetProperty("Face Character", "PlayerEntity")
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Talos", "Neutral") ]])
    
    -- |[Variables]|
    local iMetTalos = VM_GetVar("Root/Variables/Chapter5/Talos/iMetTalos", "N")
    
    -- |[First Meeting]|
    if(iMetTalos == 0.0) then
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter5/Talos/iMetTalos", "N", 1.0)
        
        --Variables.
        local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
    
        --Dialogue.
        fnCutscene([[ Append("Lord:[E|Neutral] Ho, ho, ho![P] Welcome, to my lair, Christine and 55![B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] 55, ready weapons.[P] I'm not getting an authenticator signal and I know there's no lord golem assigned to this station.[B][C]") ]])
        fnCutscene([[ Append("Lord:[E|Unsure] Eheh, that won't be necessary.[B][C]") ]])
        fnCutscene([[ Append("Lord:[E|Neutral] Call me 'Talos'.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Upset] This is Unit 706662, secondary designation 'Claretta', commonly called 'Talos' on the networks as the leader of a close-knit hacking group that has remained mostly anonymous despite security's efforts.[B][C]") ]])
        fnCutscene([[ Append("55:[E|Upset] The membership consists of 6 units, though I am only certain of two of their identities.[B][C]") ]])
        fnCutscene([[ Append("Lord:[E|Neutral] I had a feeling you wouldn't be any fun.[P] I had a whole speech I was going to give.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Offended] Can she be trusted?[B][C]") ]])
        fnCutscene([[ Append("Talos:[E|Neutral] Of course not![B][C]") ]])
        fnCutscene([[ Append("55:[E|Smirk] Absolutely.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Well, all right then.[P] Hello, Talos![B][C]") ]])
        fnCutscene([[ Append("Talos:[E|Unsure] Excuse me?[P] I know all about you two, I've seen what you've done.[P] I'm liable to sell you out![B][C]") ]])
        fnCutscene([[ Append("55:[E|Smug] She is a highly altruistic hacker who has protected vulnerable slave units and deleted sensitive security footage to hide evidence of wrongdoing.[B][C]") ]])
        fnCutscene([[ Append("Talos:[E|Neutral] Great.[P] Hi, Christine, 55's right.[P] I've been a maverick for a few years now, and I've been eager to meet up with you two.[B][C]") ]])
        fnCutscene([[ Append("Talos:[E|Neutral] You know how half the cameras in the city are malfunctioning?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Smirk] Of course.[P] I'm a repair unit, you know.[P] I've been told all parts are on hold in favour of munitions.[B][C]") ]])
        fnCutscene([[ Append("Talos:[E|Wise] Ehehehe, certainly.[P] But they aren't broken, merely redirecting feeds to my 'command centers'.[P] I have a few like this.[P] I know a lot about a lot.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Sad] But you're a lord golem.[P] Why are you turning against the city that has given you so much luxury and power?[B][C]") ]])
        fnCutscene([[ Append("Talos:[E|Neutral] *ahem*[P] You're a lord golem, why are [P]*you*[P] turning against the city that has given you so much luxury and power?[B][C]") ]])
        if(sChristineForm ~= "Golem") then
            fnCutscene([[ Append("Talos:[E|Neutral] Don't bother explaining the transformation thing, I've seen it with my own cameras.[P] The point stands.[B][C]") ]])
        end
        fnCutscene([[ Append("Talos:[E|Unsure] There are other lords like me who hate the Administration, that's all.[P] Don't paint us all with the same brush.[B][C]") ]])
        fnCutscene([[ Append("Talos:[E|Wise] I can help, I want to help.[P] I wasn't sure about contacting you directly but when I saw you were boarding a tram all the way out here?[B][C]") ]])
        fnCutscene([[ Append("Talos:[E|Wise] I can help you locate any catalysts you may have missed, making helpful marks on your map.[P] I know where they are![P] Mostly![B][C]") ]])
        fnCutscene([[ Append("Talos:[E|Neutral] I just need some platina and my team will help you out.[P] Sorry, but processor time doesn't come cheap.[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] Platina?[P] Not work credits?[B][C]") ]])
        fnCutscene([[ Append("Talos:[E|Wise] Smugglers like things a bit more universal.[P] Ehehehe...[B][C]") ]])
        fnCutscene([[ Append("55:[E|Neutral] I leave the decision on contracting her services to you, Christine.[P] I have no doubt this hacking circle will be useful for our other purposes.[B][C]") ]])
        fnCutscene([[ Append("Talos:[E|Neutral] Absolutely![P] Let me show you what I can do with just a bit of video footage![B]") ]])
        fnSetTalosTopics()
    
    -- |[Repeats]|
    else
        fnCutscene([[ Append("Talos:[E|Neutral] What can I help you with, comrades?[B]") ]])
        fnSetTalosTopics()
    end
    
-- |[ ==================================== Always-Available ==================================== ]|
elseif(sTopicName == "No") then
	WD_SetProperty("Hide")
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Talos", "Neutral") ]])
    fnCutscene([[ Append("Talos:[E|Unsure] Changed your mind?[P] That's chill.[B][C]") ]])
    fnCutscene([[ Append("Talos:[E|Neutral] Anything else I can do for you?[B]") ]])
    fnSetTalosTopics()

elseif(sTopicName == "Goodbye") then
	WD_SetProperty("Hide")
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Talos", "Neutral") ]])
    fnCutscene([[ Append("Talos:[E|Neutral] From the hills to Serenity, Regulus City will be free.") ]])

elseif(sTopicName == "Fashion") then
	WD_SetProperty("Hide")
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Talos", "Neutral") ]])
    fnCutscene([[ Append("Christine:[E|Blush] How do I put this gently...[P] the way that you dress...[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Blush] You look like a raiju ate a prism and vomited it back up.[B][C]") ]])
    fnCutscene([[ Append("Talos:[E|Unsure] You want it so bad, don't you?[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Blush] Completely and totally, give me your fashion wisdom.[P] How can I be like you?[B][C]") ]])
    fnCutscene([[ Append("Talos:[E|Neutral] Beauty is in the eye of the beholder, my dear.[P] Be what you want and you will never dress wrong.[B][C]") ]])
    fnCutscene([[ Append("Talos:[E|Neutral] If you are comfortable and happy then everyone who complains is a naysayer.[B][C]") ]])
    fnCutscene([[ Append("Talos:[E|Neutral] Anything else you need from me?[B]") ]])
    fnSetTalosTopics()

elseif(sTopicName == "Hackers") then
	WD_SetProperty("Hide")
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Talos", "Neutral") ]])
    fnCutscene([[ Append("55:[E|Neutral] I have a few questions about your hacker collective.[B][C]") ]])
    fnCutscene([[ Append("Talos:[E|Unsure] Anonymity is important, comrade.[P] I'm kind of surprised you figured out who two of us are.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Neutral] Indeed.[P] I was not going to ask about the membership.[P] I was going to ask how you maintain your cover.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Smug] Not that lord golems need much of a reason to be aloof, in fact, I understand it is a sign of social strength to miss work.[B][C]") ]])
    fnCutscene([[ Append("Talos:[E|Neutral] You got that right.[P] I found a real useful business.[B][C]") ]])
    fnCutscene([[ Append("Talos:[E|Wise] Ever been looking over someone's shoulder, only for them to switch what's on their PDU to something 'work related'?[P] You know they're looking at porn.[B][C]") ]])
    fnCutscene([[ Append("Christine:[E|Blush] Uh, um, uh...[B][C]") ]])
    fnCutscene([[ Append("Talos:[E|Wise] Eheheh, darkmatters kissing golems, so lewd![B][C]") ]])
    fnCutscene([[ Append("Talos:[E|Neutral] I basically made myself a 'cam service' that other lord golems pay me for.[P] They get to watch anonymized units making out, to get their thrills.[B][C]") ]])
    fnCutscene([[ Append("Talos:[E|Neutral] It's not actually illegal if it's not lord or command units, you know.[P] So my spy ring is totally legit.[B][C]") ]])
    fnCutscene([[ Append("55:[E|Offended] Disgusting.[B][C]") ]])
    fnCutscene([[ Append("Talos:[E|Neutral] I completely agree, why do you think I went maverick?[P] Anyway, the data I collect is 'known' by security, and they sometimes ask me for 'help'.[B][C]") ]])
    fnCutscene([[ Append("Talos:[E|Neutral] I have been walking an extremely fine line here.[P] Security would love to know who we are, but they also depend on us, so they can't press too hard.[B][C]") ]])
    fnCutscene([[ Append("Talos:[E|Neutral] I have to travel through the city to help with bugs and other tech support, kind of like you, Christine.[P] My helpers are in similar positions.[B][C]") ]])
    fnCutscene([[ Append("Talos:[E|Unsure] It's...[P] unsavory, but it has kept us out of the crosshairs.[P] Other mavericks aren't 'useful', so they get...[P] shot.[B][C]") ]])
    fnCutscene([[ Append("Talos:[E|Neutral] Anyway, can we talk about something else?[B]") ]])
    fnSetTalosTopics()
    
-- |[ =================================== Querying Catalysts =================================== ]|
elseif(string.sub(sTopicName, 1, 5) == "Query") then
	WD_SetProperty("Hide")

    --Resolve the catalyst name.
    local sCatalystType = string.sub(sTopicName, 6)
    
    --Variables.
    local iPlatina = AdInv_GetProperty("Platina")
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    
    --Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Talos", "Neutral") ]])
    fnCutscene([[ Append("Talos:[E|Neutral] I will mark all the catalysts of that type on your map for a one-time fee of 500 platina.[P] Any you have already found won't appear, and they will be removed as you find them.[B][C]") ]])
    if(iPlatina < 500) then
        fnCutscene([[ Append("Talos:[E|Neutral] We got a deal?[B][C]") ]])
        fnCutscene([[ Append("Christine:[E|Neutral] I'm a little strapped, actually.[B][C]") ]])
        fnCutscene([[ Append("Talos:[E|Neutral] No problem, get back to me when you have the readies.[P] My robots are ready to move when you are.") ]])
    else
        fnCutscene([[ Append("Talos:[E|Neutral] We got a deal?[B]") ]])
        fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Sure\",      " .. sDecisionScript .. ", \"Yes" .. sCatalystType .. "\") ")
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"No Thanks\", " .. sDecisionScript .. ", \"No\") ")
    end
    
-- |[ ============================== Purchasing Catalyst Markers =============================== ]|
elseif(string.sub(sTopicName, 1, 3) == "Yes") then
	WD_SetProperty("Hide")

    --Resolve the catalyst name.
    local sCatalystType = string.sub(sTopicName, 4)
    
    --Assemble the variable path.
    local sPath = "Root/Variables/Chapter5/Talos/iTalosShow_" .. sCatalystType
    
    --Set it to 1
    VM_SetVar(sPath, "N", 1.0)
    
    --Subtract platina.
	AdInv_SetProperty("Remove Platina", 500)
    
    --Show pins.
    --local sLevelName = AL_GetProperty("Name")
    --GUIMap:fnRunPinHandlerOnAllMaps(sLevelName)
    
    --Dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Talos", "Neutral") ]])
    fnCutscene([[ Append("Talos:[E|Neutral] [SOUND|Menu|BuyOrSell]There you go, I've updated your PDU.[P] My girls work fast.[B][C]") ]])
    fnCutscene([[ Append("Talos:[E|Neutral] Anything else I can do for you?[B]") ]])
    fnSetTalosTopics()
    
    --Update map, must be done instead of the pin handler due to a different format for chapter 5 maps.
    local sLevelName = fnResolveFolderName(fnResolvePath())
	fnResolveMapLocation(sLevelName)
    
end