-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
if(sObjectName == "WarpActivate") then
    fnWarpUnlockHandler("Root/Variables/Chapter5/Campfires/iRegulusExteriorEA", 8.25, 5.50)
    
--Speaking to the station attendant.
elseif(sObjectName == "AttendantScene") then

	--Variables.
	local iSawAttendantStationE = VM_GetVar("Root/Variables/Chapter5/Scenes/iSawAttendantStationE", "N")
	if(iSawAttendantStationE == 0.0) then
		
		--Flags.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iSawAttendantStationE", "N", 1.0)
		
		--Move the party south through the door.
		fnCutsceneMove("Christine", 14.25, 5.50)
		fnCutsceneBlocker()
		fnCutsceneMove("Christine", 14.25, 6.50)
		fnCutsceneMove("Tiffany", 14.25, 5.50)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		fnCutscene([[ AL_SetProperty("Open Door", "DoorA") ]])
		fnCutscene([[ AudioManager_PlaySound("World|AutoDoorOpen") ]])
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		fnCutsceneMove("Christine", 14.25, 9.50)
		fnCutsceneMove("Tiffany", 14.25, 8.50)
		fnCutsceneBlocker()
		fnCutsceneMove("Christine", 15.25, 9.50)
		fnCutsceneMove("Tiffany", 14.25, 9.50)
		fnCutsceneBlocker()
		fnCutsceneMove("Christine", 19.25, 9.50)
		fnCutsceneMove("Tiffany", 18.25, 9.50)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Attendant notices the party.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
		fnCutscene([[ Append("Christine:[E|Neutral] Excuse me, are you the attendant here?[B][C]") ]])
		fnCutscene([[ Append("Golem:[E|Neutral] Oh my, visitors?") ]])
		fnCutsceneBlocker()
		
		--Turns to speak to the party.
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		fnCutsceneFace("GolemA", -1, 0)
		fnCutsceneWait(45)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
		fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
		fnCutscene([[ Append("Golem:[E|Neutral] A lord unit and a command unit?[B][C]") ]])
		fnCutscene([[ Append("Golem:[E|Neutral] ...[P] I'm not in trouble am I?[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Laugh] Of course not![P] We're just checking in on our way to the Telemetry Facility.[B][C]") ]])
		fnCutscene([[ Append("Golem:[E|Neutral] Oh.[P] All right.[B][C]") ]])
		fnCutscene([[ Append("Golem:[E|Neutral] Wait.[B][C]") ]])
		fnCutscene([[ Append("Golem:[E|Neutral] No you're not.[P] I haven't received any notice of any units being transferred.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] Of course you haven't, as we are not being transferred.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] It's a -[P] surprise inspection.[P] Of the Telemetry Facility.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] Obviously if we gave notice, it'd not be a surprise inspection.[B][C]") ]])
		fnCutscene([[ Append("Golem:[E|Neutral] Ah, very well.[P] You may proceed.[B][C]") ]])
		fnCutscene([[ Append("Golem:[E|Neutral] Security clearance is very high for the Telemetry facility, but I doubt a command unit would lack clearance.[B][C]") ]])
		fnCutscene([[ Append("Golem:[E|Neutral] *sigh*[P] And for a moment I thought I might have someone else to talk to.[B][C]") ]])
		fnCutscene([[ Append("Golem:[E|Neutral] Oh, before you go.[P] There's been a great deal of tectonic activity as of late.[B][C]") ]])
		fnCutscene([[ Append("Golem:[E|Neutral] A survey team went by recently and installed walkways, so you should be able to make your way to the Telemetry Facility.[B][C]") ]])
		fnCutscene([[ Append("Golem:[E|Neutral] Just be careful.[P] This area is not patrolled by security units.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] We will use extra caution.[B][C]") ]])
		fnCutscene([[ Append("Christine:[E|Smirk] And what if we run out of caution?[B][C]") ]])
		fnCutscene([[ Append("55:[E|Smirk] Then we'll use our weapons.[B][C]") ]])
		fnCutscene([[ Append("Golem:[E|Neutral] Ha ha![P] Good one![B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] It was not a joke.[B][C]") ]])
		fnCutscene([[ Append("55:[E|Neutral] Lord Unit, let us proceed.") ]])
		fnCutsceneBlocker()
		
		--Golem goes back to watching TV.
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		fnCutsceneFace("GolemA", 0, -1)
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Fold the party.
		fnCutsceneMove("Tiffany", 19.25, 9.50)
		fnCutsceneBlocker()
		fnCutscene([[ AL_SetProperty("Fold Party") ]])
		fnCutsceneBlocker()
	end

--These triggers share the same name, and cause the Darkmatter girl to watch the player's party.
elseif(sObjectName == "DarkmatterAWatch") then
	EM_PushEntity("DarkmatterA")
		TA_SetProperty("Face Character", "PlayerEntity")
	DL_PopActiveObject()

end
