-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[DarkmatterA]|
    if(sActorName == "DarkmatterA") then
        
        --Variables.
        local iHasDarkmatterForm = VM_GetVar("Root/Variables/Global/Christine/iHasDarkmatterForm", "N")
        if(iHasDarkmatterForm == 0.0) then
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("[VOICE|Leader](The creature is trying to look as if it is ignoring me, but is quietly following my every move...)") ]])
        else
            TA_SetProperty("Face Character", "PlayerEntity")
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Darkmatter:[VOICE|DarkmatterGirl] Greetings, friend.[P] I will watch over the machine at this place.[P] I will keep her safe.") ]])
        end
        
    -- |[GolemA]|
    elseif(sActorName == "GolemA") then
	
        --Variables.
        local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
        
        --Normal case:
        if(sChristineForm == "Golem") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Please take care, Lord Unit.[P] If you wish to linger, All My Processors is on...[B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] Is this what you do all day?[B][C]") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Yes.[B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] Do you ever get bored?[B][C]") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] No, Lord Unit.[P] There are 62,000 seasons of All My Processors to watch.[P] I'm not even caught up yet![B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] (Woah...)") ]])
            fnCutsceneBlocker()

        --Steam droid:
        elseif(sChristineForm == "SteamDroid" or sChristineForm == "LatexDrone" or sChristineForm == "Doll" or sChristineForm == "Secrebot") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Please take care, Lord Unit.[P] If you wish to linger, All My Processors is on...[B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] (She's so caught up in her show that she hasn't even noticed I'm not a golem...)") ]])
            fnCutsceneBlocker()

        --Exotic cases:
        else
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Please take care, Lord Unit.[P] If you wish to linger, All My Processors is on...[B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] (She's so caught up in her show that she hasn't even noticed I'm not a robot...)") ]])
            fnCutsceneBlocker()
        end
        
    -- |[Talos]|
    elseif(sActorName == "Talos") then
        LM_ExecuteScript(fnResolvePath() .. "Dialogue Talos.lua", "Hello")
    end
end
