-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Name Resolve]|
--Resolve the name of the calling entity.
local sActorName = TA_GetProperty("Name")

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    --Set facing.
    TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[GolemAA]|
    if(sActorName == "GolemAA") then
    
        --Variables.
        local sChristineForm = VM_GetVar("Root/Variables/Global/Christine/sForm", "S")
        
        --Golem:
        if(sChristineForm == "Golem") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Hello, Lord Unit.[P] I am currently assigned to watch duty out here.[P] If you are heading to the Equinox Laboratories, please go south from here.[B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] Have you gotten any...[P] unusual messages from there, recently?[B][C]") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] No.[P] In fact, I haven't gotten any messages from there at all.[B][C]") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] They don't relay much through here except experiment results, so that is no surprise.[B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] All right then...") ]])
            fnCutsceneBlocker()
        
        --Latex Drone:
        elseif(sChristineForm == "LatexDrone") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Oh, hello, Drone Unit![P] You're looking well.[B][C]") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Are you on the way to the Equinox Labs?[B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] AFFIRMATIVE.[P] PLEASE PROVIDE DIRECTIONS.[B][C]") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Head south.[P] It's hard to miss.[B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] AFFIRMATIVE.[P] NAVIGATING.[B][C]") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Command Unit, please keep an eye on her.[P] Don't let her get lost.[B][C]") ]])
            fnCutscene([[ Append("55:[VOICE|Tiffany] She won't.[P] Come along, Drone.[B][C]") ]])
            fnCutscene([[ Append("55:[VOICE|Tiffany] Unit, have you heard any unusual radio traffic from the labs?[B][C]") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] No, nothing.[P] I only get results relayed every few days from their experiments.[P] It's not unusual for radio silence between then.[B][C]") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Have a nice visit.[B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] AFFIRMATIVE.[P] HAVING A NICE VISIT IN PROGRESS.") ]])
            fnCutsceneBlocker()
            
        --Doll:
        elseif(sChristineForm == "Doll") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Nothing of interest to report besides the regular darkmatter activity, command unit.[P] Though I do appreciate your personal attention.[B][C]") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] (What an outfit![P] There's no way that's regulation![P] Oh, but if I compliment her...[P] she might retire me!)[B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] Out of curiosity, have you heard anything unusual coming from Equinox?[B][C]") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] No, command unit.[P] They only bounce transmissions on to Regulus City when they have data to report, their network is otherwise isolated.[B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] All right, just checking.[P] What about you?[B][C]") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] ...[P] Me?[B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] Must be boring out here, all alone.[B][C]") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] I am not complaining![B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] Unit, I am casually flirting with you.[B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] Not seriously, I have a tandem unit, but you are clearly bored and horny.[B][C]") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] !!![P] I -[P] how can you tell?[B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] You've been staring at my midriff this whole time.[B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] I'd suggest putting in for some time in the city.[P] Otherwise, you'll lose focus on your important duties.[B][C]") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Of course, command unit![P] Thank you!") ]])
            fnCutsceneBlocker()
        
        --Electrosprite:
        elseif(sChristineForm == "Electrosprite") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Huh, another test subject?[P] Some kind of electricity girl?[B][C]") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Just what are they experimenting on down there?[B][C]") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Equinox Labs is just south of here, Command Unit.[P] Mind the ledges.[B][C]") ]])
            fnCutscene([[ Append("55:[VOICE|Tiffany] Have you received any unusual radio traffic from the labs recently?[B][C]") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Recently?[P] No.[B][C]") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Be on your best behavior, electricity girl.[P] They're a little gruff down there, but they'll let you go once the experiments are over.[B][C]") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] ...[P] Probably...[B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] (I don't like the sound of that at all...)") ]])
            fnCutsceneBlocker()
        
        --Eldritch:
        elseif(sChristineForm == "Eldritch") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Huh, another test subject?[P] Never seen an organic that doesn't need a vac-suit out here.[B][C]") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Then again, that's probably why you're on the way to Equinox, right?[B][C]") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Equinox Labs is just south of here, Command Unit.[P] Mind the ledges.[B][C]") ]])
            fnCutscene([[ Append("55:[VOICE|Tiffany] Have you received any unusual radio traffic from the labs recently?[B][C]") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Recently?[P] No.[B][C]") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] I hope the experiments go well.[P] Can you imagine genetically modifying humans to resist a vacuum?[B][C]") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] We could make an army out of them![P] Call them Space Marines![B][C]") ]])
            fnCutscene([[ Append("Christine:[VOICE|Christine] (Sounds oddly familiar to me...)") ]])
            fnCutsceneBlocker()
        
        --Steam Droid:
        elseif(sChristineForm == "SteamDroid") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Oh, a Steam Droid.[B][C]") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] It's not my business to pass judgement but, I -[P] I hope you're not an experiment subject.[B][C]") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] I've seen a couple of your kind sent to Equinox.[P] They don't get sent back to the city.[B][C]") ]])
            fnCutscene([[ Append("55:[VOICE|Tiffany] The status of this Steam Droid is not your concern.[B][C]") ]])
            fnCutscene([[ Append("55:[VOICE|Tiffany] Have you received any unusual radio traffic from the labs recently?[B][C]") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Recently?[P] No.[B][C]") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] They route experimental data through my comms equipment here, but that's all I get from them.[P] It's often days between transmissions.[B][C]") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Good luck, Steam Droid.[P] I hope it isn't too painful...") ]])
            fnCutsceneBlocker()
        
        --Darkmatter:
        elseif(sChristineForm == "Darkmatter") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Oh, a -[P] a captured Darkmatter?[P] Are you taking her to Equinox, Command Unit?[B][C]") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Then again, she's probably just letting you lead her around.[P] They come in here every now and then, there's not much I can do to keep them out.[B][C]") ]])
            fnCutscene([[ Append("55:[VOICE|Tiffany] This Darkmatter is well-behaved enough.[P] We may yet get some data out of her.[B][C]") ]])
            fnCutscene([[ Append("55:[VOICE|Tiffany] Have you received any unusual radio traffic from the labs recently?[B][C]") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] Recently?[P] No.[B][C]") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] They route experimental data through my comms equipment here, but that's all I get from them.[P] It's often days between transmissions.[B][C]") ]])
            fnCutscene([[ Append("Golem:[VOICE|Golem] I hope you can get some good readings![P] Solving the Darkmatter mystery will put Equinox on the map, for sure!") ]])
            fnCutsceneBlocker()
        
        elseif(sChristineForm == "Secrebot") then
            fnCutscene([[ WD_SetProperty("Show") ]])
            fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 0, "Tiffany", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 2, "Christine", "Neutral") ]])
            fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Golem", "Neutral") ]])
            fnCutscene([[ Append("Golem: Hello, command unit.[P] May I help you?[B][C]") ]])
            fnCutscene([[ Append("55:[E|Neutral] Not in particular.[P] Have you detected any unusual radio traffic recently?[B][C]") ]])
            fnCutscene([[ Append("Golem: The station picks up traffic echoes all the time, but we're just a network bounce.[P] I'm sorry I can't really help you further.") ]])
        end
    end
end
