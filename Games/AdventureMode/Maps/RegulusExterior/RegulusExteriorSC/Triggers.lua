-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[ ======================================== Triggers ======================================== ]|
--Object causes 55 to rejoin the party if she wasn't in it.
if(sObjectName == "55Rejoins") then

	--Variables.
	local iIs55Following = VM_GetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N")
	local iMet55InLowerRegulus = VM_GetVar("Root/Variables/Chapter5/Scenes/iMet55InLowerRegulus", "N")
	
	--If 55 is not following but should, cutscene time.
	if(iIs55Following == 0.0 and iMet55InLowerRegulus == 1.0) then
		
		--Flag 55 as following Christine.
		VM_SetVar("Root/Variables/Chapter5/Scenes/iIs55Following", "N", 1.0)
		
		--Create if she doesn't exist. She probably won't, but you never know.
		if(EM_Exists("Tiffany") == false) then
			fnSpecialCharacter("Tiffany", -100, -100, gci_Face_South, false, nil)
		end
		
		--Christine moves south a bit.
		fnCutsceneMove("Christine", 16.75, 9.50)
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		fnCutsceneFace("Christine", -1, -1)
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		fnCutsceneFace("Christine", -1, 1)
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		fnCutsceneFace("Christine", 1, 1)
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		fnCutsceneFace("Christine", 1, -1)
		fnCutsceneWait(65)
		fnCutsceneBlocker()
		
		--Dialogue.
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Christine:[VOICE|Christine] ^Area Clear.[P] Proceed.^[B][C]") ]])
		fnCutscene([[ Append("55:[VOICE|Tiffany] ^On the way.^") ]])
		fnCutsceneBlocker()
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Teleport 55 in.
		fnCutsceneTeleport("Tiffany", 28.25, 4.50)
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		fnCutsceneMove("Tiffany", 22.25, 4.50)
		fnCutsceneMove("Tiffany", 19.25, 7.50)
		fnCutsceneMove("Tiffany", 16.75, 7.50)
		fnCutsceneMove("Tiffany", 16.75, 8.50)
		fnCutsceneBlocker()
		fnCutsceneFace("Christine", 0, -1)
		fnCutsceneWait(25)
		fnCutsceneBlocker()
		
		--Dialogue changes if 55 hasn't mentioned the radio yet.
		local i55ToldAboutRadio = VM_GetVar("Root/Variables/Chapter5/Scenes/i55ToldAboutRadio", "N")
		if(i55ToldAboutRadio == 0.0) then
			
			--Flag.
			VM_SetVar("Root/Variables/Chapter5/Scenes/i55ToldAboutRadio", "N", 1.0)
		
			--Dialogue.
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
			fnCutscene([[ Append("55:[E|Neutral] ^Unit 771852, I am broadcasting an encryption algorithm. Use it to encrypt your short-range radio transmissions.^[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] ^Best if nobody accidentally intercepts one.^[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] ^Affirmative.[P] Updating.^[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Smirk] ^What's the effective range on these receivers?^[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] ^It becomes garbled after about 20 meters.[P] The administrators are looking for suspicious radio traffic, so don't use the longer range waves.^[B][C]") ]])
			fnCutscene([[ Append("Christine:[E|Neutral] ^Affirmative.[P] We'll use the short-range radio.^[B][C]") ]])
			fnCutscene([[ Append("55:[E|Neutral] ^Let's get going.^") ]])
			fnCutsceneBlocker()
		
		--Much shorter dialogue.
		else
			fnCutscene([[ WD_SetProperty("Show") ]])
			fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
			fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Tiffany", "Neutral") ]])
			fnCutscene([[ Append("55: ^Let's go.^") ]])
			fnCutsceneBlocker()
		end
		
		--Walk 55 onto Christine, fold the party.
		fnCutsceneMove("Tiffany", 16.75, 9.50)
		fnCutsceneBlocker()
		fnCutscene([[ AL_SetProperty("Fold Party") ]])
		fnCutsceneBlocker()
		
		--Lua globals.
		giFollowersTotal = 1
		gsaFollowerNames = {"Tiffany"}
		giaFollowerIDs = {0}

		--Get 55's uniqueID. 
		EM_PushEntity("Tiffany")
			local iCharacterID = RE_GetID()
		DL_PopActiveObject()

		--Store it and tell her to follow.
		giaFollowerIDs = {iCharacterID}
		AL_SetProperty("Follow Actor ID", iCharacterID)
	end
end
