-- |[Examination]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Exits]|
if(sObjectName == "ToExteriorSB") then
	AudioManager_PlaySound("World|AutoDoorOpen")
	AL_BeginTransitionTo("RegulusExteriorSB", "FORCEPOS:95.5x42.0x0")
	
elseif(sObjectName == "LadderN") then
	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("RegulusExteriorTRNB", "FORCEPOS:7.0x7.0x0")
	
elseif(sObjectName == "LadderS") then
	AudioManager_PlaySound("World|ClimbLadder")
	AL_BeginTransitionTo("RegulusExteriorTRNB", "FORCEPOS:7.0x13.0x0")

-- |[Examinables]|
elseif(sObjectName == "FizzyPopBlue") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (Fizzy Pop![P] The blue variety.[P] I don't need a recharge right now.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "FizzyPopPink") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (Fizzy Pop![P] The pink variety.[P] I don't need a recharge right now.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "WorkTerminal") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (A work terminal.[P] It's been disconnected from the Regulus City network.[P] The security authorization driver has been pulled out...)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "TerminalA") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (The defragmentation logs for this unit show nothing of interest.)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "TerminalB") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (This unit had music play while she defragmented.[P] It seems she was a fan of industrial metal -[P] as all good robots should be!)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "TerminalC") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (The last defragmentation logs on this terminal indicate this facility was vacated shortly before the Cryogenics facility was 'restricted'.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "TerminalD") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: ('Attention all units:: This facility is to be vacated immediately.[P] Please acquire high priority equipment as per protocol SSCR-7 and return to Regulus City for reassignment.)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "TerminalE") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (The work logs show a steady increase in work assignments until the facility was ordered abandoned.[P] No reason for the abandonment can be found in the logs...)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "TerminalF") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (The only thing on the hard drive is a long list of transit network orders and repair requests.)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "TerminalG") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (The hard drive has two folders.[P] One labeled 'Work' and the other 'Porn'.[P] They both contain images of scantily-clad tram diagnostics and repair manuals.)") ]])
	fnCutsceneBlocker()

elseif(sObjectName == "Oilmaker") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (The oilmaker here is flavoured with sulfur and shaved iron crystals.[P] Yum!)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "Couch") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (A synthleather chesterfield, facing the RVD on the south wall.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "FabricationBench") then
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ Append("Thought: (A fabrication bench.[P] The tools have been completely stripped, so it's useless at the moment.)") ]])
	fnCutsceneBlocker()
	
elseif(sObjectName == "SpareParts") then
	local iServicePartsA = VM_GetVar("Root/Variables/Chapter5/Scenes/iServicePartsA", "N")
	if(iServicePartsA == 0.0) then
		LM_ExecuteScript(gsItemListing, "Assorted Parts")
		VM_SetVar("Root/Variables/Chapter5/Scenes/iServicePartsA", "N", 1.0)
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Thought: (Spare parts.[P] I don't think anyone will mind if I borrow some...)[B][C]") ]])
		fnCutscene([[ Append("Thought: [SOUND|World|TakeItem](Received Assorted Parts x1)") ]])
		fnCutsceneBlocker()
	else
		fnCutscene([[ WD_SetProperty("Show") ]])
		fnCutscene([[ Append("Thought: (There's nothing useful left on the shelf.)") ]])
		fnCutsceneBlocker()
	end

--Unhandled case.
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
