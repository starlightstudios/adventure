-- |[ ======================== MapHelper Pregenerated Random Functions ========================= ]|
--Functions needed for the "pre-generated random" levels. These levels use random handlers to specify
-- enemy properties, patrol routes, and chest contents. The levels themselves are made in Tiled and
-- have fixed layouts.
--Variables used for determining enemy patrols and chest contents are stored in the MapHelper class.

-- |[ ==================================== System Functions ==================================== ]|
-- |[MapHelper:fnClearPreranVariables()]|
--Clears PregenRandom variables back to their default values so nothing is lingering in memory.
function MapHelper:fnClearPreranVariables()
	
	-- |[Variables]|
	self.bIsRandomGenerationReady = false
	self.sEntryPointPath          = "Null"
	self.sExamineIntercept        = "Null"
	self.sFloorRoller             = "Null"
	self.zaFloorData              = {}
	self.zaAppearanceData         = {}
	
	-- |[Functions]|
	self.fnGenerateEnemyGroup = nil
	self.fnGenerateTreasure   = nil
	self.fnSelectNextLevel    = nil
end

-- |[MapHelper:fnVerifyPreranVariables()]|
--Checks all PregenRandom variables to make sure they are both valid and, if needed, that they point
-- to a script that actually exists. If any of them fail, returns false and prints warnings for 
-- each unset variable. Returns true if all variables are ready.
--Note that this should be called after you set up the variables, but not in each function call.
-- Checking if the scripts exist could be non-trivial on a network drive.
function MapHelper:fnVerifyPreranVariables()
	
	-- |[Setup]|
	local saErrorVars = {}
	
	-- |[Scan Variables]|
	if(self.sEntryPointPath == "Null") then
		table.insert(saErrorVars, "sEntryPointPath")
	end
	if(self.sExamineIntercept == "Null") then
		table.insert(saErrorVars, "sExamineIntercept")
	end
	if(self.sFloorRoller == "Null") then
		table.insert(saErrorVars, "sFloorRoller")
	end
	
	-- |[Scan Functions]|
	if(self.fnGenerateEnemyGroup == nil) then
		table.insert(saErrorVars, "fnGenerateEnemyGroup")
	end
	if(self.fnGenerateTreasure == nil) then
		table.insert(saErrorVars, "fnGenerateTreasure")
	end
	if(self.fnSelectNextLevel == nil) then
		table.insert(saErrorVars, "fnSelectNextLevel")
	end
	
	-- |[Finish]|
	--If no errors were found, return true.
	if(#saErrorVars < 1) then return true end
	
	--Otherwise, print warnings for each erroneous variable.
	io.write("MapHelper:fnVerifyPreranVariables() - Warning. At least one variable was not set correctly. A list follows.\n")
	for i = 1, #saErrorVars, 1 do
		io.write(" " .. saErrorVars[i] .. "\n")
	end
	
	--Fail here.
	return false
end

-- |[ ================================== Floor Data Functions ================================== ]|
-- |[MapHelper:fnSetFloorData()]|
--Specifies basic data for each floor that is possible in the generator.
function MapHelper:fnSetFloorData(piFloor, piMaxEnemies, piMaxTreasures, pzaEnemyLookups, pzaTreasureLookups)
	
	-- |[Argument Check]|
	if(piFloor        == nil) then return end
	if(piMaxEnemies   == nil) then return end
	if(piMaxTreasures == nil) then return end
	
	-- |[Execution]|
	--Create.
	local zFloorData = {}
	zFloorData.iMaxEnemies       = piMaxEnemies
	zFloorData.iMaxTreasures     = piMaxTreasures
	zFloorData.zaEnemyLookups    = pzaEnemyLookups
	zFloorData.zaTreasureLookups = pzaTreasureLookups
	
	--Register.
	self.zaFloorData[piFloor] = zFloorData
end

-- |[MapHelper:fnGetMaxEnemiesForFloor()]|
--Returns the maximum number of enemies allowed on the floor. If the floor is out of range, returns 0.
function MapHelper:fnGetMaxEnemiesForFloor(piFloor)
	
	-- |[Argument Check]|
	if(piFloor == nil) then return 0 end
	
	-- |[Execution]|
	--If not filled or out of range, defaults to 0.
	if(self.zaFloorData[piFloor] == nil) then return 0 end
	
	--Return the value.
	return self.zaFloorData[piFloor].iMaxEnemies
end

-- |[MapHelper:fnGetMaxTreasuresForFloor()]|
--Returns the maximum number of tresures allowed on the floor. If the floor is out of range, returns 0.
function MapHelper:fnGetMaxTreasuresForFloor(piFloor)
	
	-- |[Argument Check]|
	if(piFloor == nil) then return 0 end
	
	-- |[Execution]|
	--If not filled or out of range, defaults to 0.
	if(self.zaFloorData[piFloor] == nil) then return 0 end
	
	--Return the value.
	return self.zaFloorData[piFloor].iMaxTreasures
end

-- |[MapHelper:fnGetEnemyDataForFloor()]|
--Returns a table containing a series of zEnemyRollData entries. Can legally be an empty table.
function MapHelper:fnGetEnemyDataForFloor(piFloor)
	
	-- |[Argument Check]|
	if(piFloor == nil) then return {} end
	
	-- |[Execution]|
	--If not filled or out of range, defaults to 0.
	if(self.zaFloorData[piFloor] == nil) then return {} end
	
	--Return the value.
	return self.zaFloorData[piFloor].zaEnemyLookups
end

-- |[MapHelper:fnGetTreasureDataForFloor()]|
function MapHelper:fnGetTreasureDataForFloor(piFloor)
	
	-- |[Argument Check]|
	if(piFloor == nil) then return {} end
	
	-- |[Execution]|
	--If not filled or out of range, defaults to 0.
	if(self.zaFloorData[piFloor] == nil) then return 0 end
	
	--Return the value.
	return self.zaFloorData[piFloor].zaTreasureLookups
end

-- |[ =============================== Appearance Data Functions ================================ ]|
-- |[MapHelper:fnAddAppearanceData()]|
--Creates a new entry in the appearance list. This allows combat enemies to be mapped to sprites and toughness
-- values, so the correct overworld representation appears.
function MapHelper:fnAddAppearanceData(psEnemyName, psSpriteName, piToughness)
	
	-- |[Argument Check]|
	if(psEnemyName  == nil) then return end
	if(psSpriteName == nil) then return end
	if(piToughness  == nil) then return end
	
	-- |[Duplicate Check]|
	--If the enemy name already exists, overwrite it.
	for i = 1, #self.zaAppearanceData, 1 do
		if(self.zaAppearanceData[i].sEnemyName == psEnemyName) then
			self.zaAppearanceData[i].sEnemyName  = psEnemyName
			self.zaAppearanceData[i].sSpriteName = psSpriteName
			self.zaAppearanceData[i].iToughness  = piToughness
			return
		end
	end
	
	-- |[Append]|
	local zAppearance = {}
	zAppearance.sEnemyName  = psEnemyName
	zAppearance.sSpriteName = psSpriteName
	zAppearance.iToughness  = piToughness
	table.insert(self.zaAppearanceData, zAppearance)
end

-- |[MapHelper:fnGetAppearanceData()]|
--Returns the sSpriteName and iToughness for the named enemy. If the enemy is not found, prints a warning
-- and returns "Null", 0.
function MapHelper:fnGetAppearanceData(psEnemyName)
	
	-- |[Argument Check]|
	if(psEnemyName == nil) then return "Null", 0 end
	
	-- |[Scan]|
	for i = 1, #self.zaAppearanceData, 1 do
		if(self.zaAppearanceData[i].sEnemyName == psEnemyName) then
			return self.zaAppearanceData[i].sSpriteName, self.zaAppearanceData[i].iToughness
		end
	end
	
	-- |[Not Found]|
	io.write("MapHelper:fnGetAppearanceData() - Warning. Unable to find enemy " .. psEnemyName .. " in lookups.\n")
	io.write(" Lookups contains " .. #self.zaAppearanceData .. " entries.\n")
	return "Null", 0
end

-- |[ =============================== Enemy Roll Data Functions ================================ ]|
-- |[MapHelper:fnCreateEnemyRollData()]|
--Creates and returns a zEnemyRollData structure. Note that unlike the Appearance functions above, this does
-- not get added to a static database, it is returned immediately.
function MapHelper:fnCreateEnemyRollData(piRollChance, psaEnemyNames)
	
	-- |[Argument Check]|
	if(piRollChance  == nil) then return nil end
	if(psaEnemyNames == nil) then return nil end
	
	-- |[Zero Chance Check]|
	if(piRollChance < 1) then
		io.write("MapHelper:fnCreateEnemyRollData() - Warning. Roll chance for package was zero. Failing.\n")
		return nil
	end
	
	-- |[Create, Return]|
	local zEnemyRollData = {}
	zEnemyRollData.iRollChance  = piRollChance
	zEnemyRollData.saEnemyNames = psaEnemyNames
	return zEnemyRollData
end

-- |[MapHelper:fnSelectEnemyGroup()]|
--Given a zEnemyRollData structure, selects an enemy group using a random roll and returns it. The enemy
-- group is a list of strings. The list can legally be empty.
function MapHelper:fnSelectEnemyGroup(pzaEnemyRollData)
	
	-- |[Argument Check]|
	if(pzaEnemyRollData == nil) then return {} end
	
	-- |[Compute Maximum Roll]|
	--Scan across the table to figure out what the highest possible roll is.
	local iMaxRoll = 0
	for i = 1, #pzaEnemyRollData, 1 do
		iMaxRoll = iMaxRoll + pzaEnemyRollData[i].iRollChance
	end
	
	--If the max roll is somehow zero, return an empty list.
	if(iMaxRoll < 1) then return {} end
	
	-- |[Roll, Select]|
	--Perform the roll.
	local iRoll = LM_GetRandomNumber(1, iMaxRoll)
	
	--Scan the enemy data, this time selecting the entry that matches the weighted roll.
	for i = 1, #pzaEnemyRollData, 1 do
		iRoll = iRoll - pzaEnemyRollData[i].iRollChance
		if(iRoll <= 0) then
			return pzaEnemyRollData[i].saEnemyNames
		end
	end
	
	--If we somehow got this far, something went really wrong as it shouldn't be logically possible.
	return {}
end

-- |[ ============================== Treasure Roll Data Functions ============================== ]|
-- |[MapHelper:fnCreateTreasureRollData()]|
--Creates and returns a treasure roll package. Does not add to an internal database.
function MapHelper:fnCreateTreasureRollData(piRollChance, psaTreasureNames)
	
	-- |[Argument Check]|
	if(piRollChance     == nil) then return nil end
	if(psaTreasureNames == nil) then return nil end
	
	-- |[Zero Chance Check]|
	if(piRollChance < 1) then
		io.write("MapHelper:fnCreateTreasureRollData() - Warning. Roll chance for package was zero. Failing.\n")
		return nil
	end
	
	-- |[Create, Return]|
	local zTreasureRollData = {}
	zTreasureRollData.iRollChance     = piRollChance
	zTreasureRollData.saTreasureNames = psaTreasureNames
	return zTreasureRollData
end

-- |[MapHelper:fnCloneTreasureCluster()]|
--Given a base cluster, clones it and then returns it. In addition, if pzaTreasureRollList is provided, 
-- appends those lists to the cluster.
function MapHelper:fnCloneTreasureCluster(pzaBaseCluster, pzaTreasureRollList)
	
	-- |[Argument Check]|
	if(pzaBaseCluster == nil) then return {} end
	
	-- |[Base Clone]|
	--Clone the base cluster.
	local zaNewCluster = {}
	for i = 1, #pzaBaseCluster, 1 do
		table.insert(zaNewCluster, pzaBaseCluster[i])
	end
	
	-- |[Optional Extra Lists]|
	--If extra lists are provided, append those.
	if(pzaTreasureRollList ~= nil) then
		for i = 1, #pzaTreasureRollList, 1 do
			table.insert(zaNewCluster, pzaTreasureRollList[i])
		end
	end
	
	-- |[Finish Up]|
	return zaNewCluster
end

-- |[MapHelper:fnSelectTreasure()]|
--Given a treasure cluster, which is a list of zTreasureRollData's, selects one with a random roll and returns
-- a single string that is the selected treasure. Can return nil legally, which indicates the chest should
-- be despawned.
function MapHelper:fnSelectTreasure(pzaTreasureRollData)
	
	-- |[Argument Check]|
	if(pzaTreasureRollData == nil) then return nil end
	
	-- |[Compute Maximum Roll]|
	--Scan across the table to figure out what the highest possible roll is.
	local iMaxRoll = 0
	for i = 1, #pzaTreasureRollData, 1 do
		iMaxRoll = iMaxRoll + pzaTreasureRollData[i].iRollChance
	end
	
	--If the max roll is somehow zero, return no string.
	if(iMaxRoll < 1) then return nil end
	
	-- |[Roll, Select]|
	--Perform the roll.
	local iRoll = LM_GetRandomNumber(1, iMaxRoll)
	
	--Scan the treasure data, this time selecting the entry that matches the weighted roll.
	for i = 1, #pzaTreasureRollData, 1 do
		iRoll = iRoll - pzaTreasureRollData[i].iRollChance
		
		--Once we have located the active entry, select one of the elements from its list with a flat roll.
		if(iRoll <= 0) then
			
			--If the list is empty, return nil to indicate no treasure goes here.
			if(#pzaTreasureRollData[i].saTreasureNames < 1) then
				return nil
			end
			
			--Rll against the list size.
			local iSelect = LM_GetRandomNumber(1, #pzaTreasureRollData[i].saTreasureNames)
			return pzaTreasureRollData[i].saTreasureNames[iSelect]
		end
	end
	
	--If we somehow got this far, something went really wrong as it shouldn't be logically possible.
	return nil
end

-- |[ ================================== Execution Functions =================================== ]|
-- |[MapHelper:fnPreranSetPatrols()]|
--Scans the enemy groupings in the level and checks for any that have their appearance and party
-- set to "AUTO". This will then modify that NPC's appearance and party composition using random
-- generation. It will bark warnings if the generators are not set up.
function MapHelper:fnPreranSetPatrols(piFloorNumber)
	
	-- |[Argument Check]|
	if(piFloorNumber == nil) then return end
	
	-- |[Validity Check]|
	--Make sure all functions are set correctly or do nothing.
	if(self.bIsRandomGenerationReady == false) then return end
	
	-- |[Enemy Scan]|
	--Scan across the enemy names. Enemies are assumed to be named sequentially, "EnemyAA", "EnemyBA",
	-- and so on. As soon as an enemy doesn't exist, stop.
	local sCurLetter = "A"
	local sEndLetter = "Z"
	while(true) do
		
		--Assemble an enemy name.
		local sCheckEnemyName = "Enemy" .. sCurLetter .. "A"
		
		--If the enemy name does not exist, stop.
		if(EM_Exists(sCheckEnemyName) == false) then
			break
		end
		
		--Enemy exists. Run a script to generate a group for it.
		MapHelper:fnGenerateEnemyGroup(piFloorNumber, sCheckEnemyName)
		
		--If this was the last possible letter, stop.
		if(sCurLetter == sEndLetter) then break end
		
		--Increment the name of the enemy.
		local iCharacter = string.byte(sCurLetter)
		sCurLetter = string.char(iCharacter + 1)
	end
	
end

-- |[ ============================ MapHelper:fnPreranSetTreasure() ============================= ]|
--Scans the treasure chest spawn locations on the level and populates them with goodies, or despawns
-- them if there's too many chests.
function MapHelper:fnPreranSetTreasure(piFloorNumber)
	
	-- |[Argument Check]|
	if(piFloorNumber == nil) then return end
	
	-- |[Treasure Scan]|
	--Scan across the treausre names. Chests are assumed to be named sequentially, "ChestA", "ChestB",
	-- and so on. As soon as a chest doesn't exist, stop.
	local sCurLetter = "A"
	local sEndLetter = "Z"
	while(true) do
		
		--Assemble an enemy name.
		local sCheckTreasureName = "Chest" .. sCurLetter
		
		--If the enemy name does not exist, stop.
		if(AL_GetProperty("Does Chest Exist", sCheckTreasureName) == false) then
			break
		end
		
		--Enemy exists. Run a script to generate a group for it.
		MapHelper:fnGenerateTreasure(piFloorNumber, sCheckTreasureName)
		
		--If this was the last possible letter, stop.
		if(sCurLetter == sEndLetter) then break end
		
		--Increment the name of the enemy.
		local iCharacter = string.byte(sCurLetter)
		sCurLetter = string.char(iCharacter + 1)
	end

end
