-- |[ ============================= MapHelper Location Resolve Ch5 ============================= ]|
--Handles creating the map and populating it with location information in chapter 5. This is also
-- where map pin information is spawned.

--Temporary. Routes the call to whichever chapter handler is needed. This will need to be rewritten
-- once all the map resolvers are unified.
function MapHelper:fnResolveMapLocation(psMapName)
	self:fnResolveMapLocationCh5(psMapName)
end

-- |[ ========================== MapHelper:fnResolveMapLocationCh5() =========================== ]|
--Handles map resolve for chapter 5.
function MapHelper:fnResolveMapLocationCh5(psMapName)

	-- |[Error Check]|
	--Argument check.
	if(psMapName == nil) then return end
	
	--Make sure the array exists.
	if(gsaMapLookups == nil) then return end
    
    -- |[Clear]|
    gfnLastMapFunction = nil
    giLastPlayerX = nil
    giLastPlayerY = nil
    giLastRemapX = nil
    giLastRemapY = nil
    
    --No map edge padding. Any map using this function does not have padding.
    AM_SetProperty("Map Edge Pad", 0)
    
    --Other.
    local sLastMapSet = "None"
	
	-- |[Scan]|
    for i = 1, #gsaMapLookups, 1 do

		--Name match?
		if(gsaMapLookups[i][1] == psMapName) then
            
            --Figure out the overlay. Not all maps have these. "Null" is a valid case.
            local sOverlayName = "Null"
            if(gsaMapLookups[i][2] == "RegulusMap") then
                sLastMapSet = "Regulus"
                sOverlayName = "Root/Images/AdvMaps/General/RegulusMapOverlay"
            elseif(gsaMapLookups[i][2] == "BiolabsMap") then
                sLastMapSet = "Biolabs"
            end
            
			AM_SetMapInfo("Root/Images/AdvMaps/General/" .. gsaMapLookups[i][2], sOverlayName, gsaMapLookups[i][3], gsaMapLookups[i][4])
			break
		end
    end
    
    -- |[Map Pin Resolver]|
    --Place map pins if the player has unlocked them.
    if(sLastMapSet == "Regulus") then
        
        --Variables.
        local iTalosShowHealth = VM_GetVar("Root/Variables/Chapter5/Talos/iTalosShow_Health", "N")
        local iTalosShowSkills = VM_GetVar("Root/Variables/Chapter5/Talos/iTalosShow_Skills", "N")
        
        --Health Catalysts.
        if(iTalosShowHealth == 1.0) then
            if(DL_Exists("Root/Variables/Chests/RegulusCity198C/Chest A") == false) then
                AM_SetProperty("Register Map Pin", 1305, 638, gcsMapPinSet_IcoHlt)
            end
            if(DL_Exists("Root/Variables/Chests/RegulusCryoC/Chest A") == false) then
                AM_SetProperty("Register Map Pin", 1215, 1198, gcsMapPinSet_IcoHlt)
            end
            if(DL_Exists("Root/Variables/Chests/RegulusExteriorWB/Chest H") == false) then
                AM_SetProperty("Register Map Pin", 637,  823, gcsMapPinSet_IcoHlt)
            end
            if(DL_Exists("Root/Variables/Chests/RegulusLRTEA/Chest A") == false) then
                AM_SetProperty("Register Map Pin", 2065, 645, gcsMapPinSet_IcoHlt)
            end
            if(DL_Exists("Root/Variables/Chests/SprocketCityA/Chest A") == false) then
                AM_SetProperty("Register Map Pin", 1435, 480, gcsMapPinSet_IcoHlt)
            end
        end
        
        --Skill Catalysts
        if(iTalosShowSkills == 1.0) then
            if(DL_Exists("Root/Variables/Chests/RegulusCryoLowerC/ChestB") == false) then
                AM_SetProperty("Register Map Pin", 1278, 1198, gcsMapPinSet_IcoSki)
            end
            if(DL_Exists("Root/Variables/Chests/RegulusCryoPowerCoreD/Chest A") == false) then
                AM_SetProperty("Register Map Pin", 1111, 1235, gcsMapPinSet_IcoSki)
            end
            if(DL_Exists("Root/Variables/Chests/RegulusExteriorTRNB/ChestA") == false) then
                AM_SetProperty("Register Map Pin", 1369, 1025, gcsMapPinSet_IcoSki)
            end
            if(DL_Exists("Root/Variables/Chests/RegulusLRTHB/ChestA") == false) then
                AM_SetProperty("Register Map Pin", 2205, 665, gcsMapPinSet_IcoSki)
            end
            if(DL_Exists("Root/Variables/Chests/RegulusManufactoryG/ChestC") == false) then
                AM_SetProperty("Register Map Pin", 1136, 321, gcsMapPinSet_IcoSki)
            end
            if(DL_Exists("Root/Variables/Chests/SerenityObservatoryE/Chest A") == false) then
                AM_SetProperty("Register Map Pin", 1782, 1150, gcsMapPinSet_IcoSki)
            end
        end

    --Biolabs set.
    elseif(sLastMapSet == "Biolabs") then
    
        --Variables.
        local iTalosShowAccuracy = VM_GetVar("Root/Variables/Chapter5/Talos/iTalosShow_Accuracy", "N")
        local iTalosShowEvade    = VM_GetVar("Root/Variables/Chapter5/Talos/iTalosShow_Evasion", "N")
        
        --Accuracy Catalysts
        if(iTalosShowAccuracy == 1.0) then
            if(DL_Exists("Root/Variables/Chests/RegulusBiolabsAmphibianE/ChestA") == false) then
                AM_SetProperty("Register Map Pin", 853,   99, gcsMapPinSet_IcoAcc)
            end
            if(DL_Exists("Root/Variables/Chests/RegulusBiolabsAmphibianE/ChestB") == false) then
                AM_SetProperty("Register Map Pin", 833,   99, gcsMapPinSet_IcoAcc)
            end
            if(DL_Exists("Root/Variables/Chests/RegulusBiolabsAmphibianE/ChestC") == false) then
                AM_SetProperty("Register Map Pin", 873,   99, gcsMapPinSet_IcoAcc)
            end
        end
    
        --Evade Catalysts.
        if(iTalosShowEvade == 1.0) then
            if(DL_Exists("Root/Variables/Chests/RegulusBiolabsAlphaC/Chest D") == false) then
                AM_SetProperty("Register Map Pin", 1101, 1331, gcsMapPinSet_IcoEvd)
            end
            if(DL_Exists("Root/Variables/Chests/RegulusBiolabsDatacoreD/Chest C") == false) then
                AM_SetProperty("Register Map Pin",  853,  924, gcsMapPinSet_IcoEvd)
            end
            if(DL_Exists("Root/Variables/Chests/RegulusBiolabsGeneticsC/Chest B") == false) then
                AM_SetProperty("Register Map Pin", 2070, 1406, gcsMapPinSet_IcoEvd)
            end
        end
    end
end