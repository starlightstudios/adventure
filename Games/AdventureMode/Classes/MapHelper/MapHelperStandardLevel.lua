-- |[ ================================ MapHelper Standard Level ================================ ]|
--Contains fnStandardLevel which most levels use to perform basic setup.

-- |[ ============================== MapHelper:fnStandardLevel() =============================== ]|
--Most levels call this function in their constructor. It creates the level, parses SLF data, sets
-- paths for scripts, and all that.
function MapHelper:fnStandardLevel(psCallingPath, psLevelName, psChestLevelName, psMapResolveName, pbMustCreateChars, psMusicName, piTileSize)
    
    -- |[ ============== Setup =============== ]|
    -- |[Argument Check]|
    if(psCallingPath     == nil) then return end
    if(psLevelName       == nil) then return end
    if(psChestLevelName  == nil) then return end
    if(psMapResolveName  == nil) then return end
    if(pbMustCreateChars == nil) then return end
    if(psMusicName       == nil) then return end
    
    --If piTileSize is not provided, it defaults to 16.
    if(piTileSize == nil) then piTileSize = 16 end
    
    -- |[Pre-Creation Setup]|
    --Clear all parallel scripts. There is no guarantee that the previous map used a trigger to clean
    -- parallel scripts and they are not implicitly deleted.
    Cutscene_HandleParallel("ClearAll")
    
	--Chest path. This is the DataLibrary path used to mark chests as previously opened. A given chest,
    -- for example, "ChestA", will create a variable in "Root/Variables/Chests/LevelName/ChestA" that
    -- stores its opening state. This therefore needs to be a unique level name or chests will conflict
    -- in terms of open status.
    --Using a non-unique level name allows multiple levels to share chest data. For example, this is used
    -- in the "thawed" biolabs areas. If the player already opened a chest in the frozen area, it is
    -- still open in the thawed area.
	DL_AddPath("Root/Variables/Chests/" .. psChestLevelName .. "/")

    -- |[ =========== Map Creation =========== ]|
    -- |[Creation Routines]|
    --Music. If "Null" is passed that will clear existing music. If "NoChange" is passed, this 
    -- routine is not called and whatever the previous map's music is will remain.
    if(psMusicName ~= "NoChange") then
        AL_SetProperty("Music", "RegulusTense")
        
    end
    
	--Create the map and pass it to the MapManager.
	AL_Create()

	--Basic terrain data, automatically handles layers and depth-sorting. Note: This IS a loading
    -- action and will take a variable amount of time to complete.
	AL_ParseSLF(psCallingPath .. "MapData.slf", piTileSize)
    
    --Set the internal name of the level.
	AL_SetProperty("Name", psLevelName)
    
    --Call the region marker.
    LM_ExecuteScript(gsRegionMarkerPath, psCallingPath)
    
    --Pulse map. This spawns enemies and flags them to ignore the player.
	fnStandardEnemyPulse()

    -- |[Script Path Setup]|
	--Set the examination script to the standard path. Most examinable points are set via the .slf file object parsing.
    AL_SetProperty("Base Path", psCallingPath)
	AL_SetProperty("Trigger Script", psCallingPath .. "Triggers.lua")
	AL_SetProperty("Examine Script", psCallingPath .. "Examination.lua")
    gsFieldAbilityCheckPath = psCallingPath .. "FieldAbilities.lua"

    --Set the menu close script. Only certain chapters use this.
    local iCurrentChapter = VM_GetVar("Root/Variables/Global/ChapterComplete/iCurrentChapter", "N")
    if(iCurrentChapter == 1.0) then
        AL_SetProperty("Menu Close Script", gsRoot .. "Chapter 1/Menu Close Handler.lua")
    elseif(iCurrentChapter == 2.0) then
        AL_SetProperty("Menu Close Script", gsRoot .. "Chapter 2/Menu Close Handler.lua")
    elseif(iCurrentChapter == 5.0) then
        AL_SetProperty("Menu Close Script", gsRoot .. "Chapter 5/Menu Close Handler.lua")
    end
    
    -- |[Map Position Resolver]|
    MapHelper:fnResolveMapLocation(psMapResolveName)

    -- |[ ======== Character Creation ======== ]|
    --Typically this is called if the player character either doesn't exist, or if this map is being transition
    -- to as a result of using the debug menu. This function will reconstitute the player's field party, and also
    -- reposition everyone onto the PlayerStart object so they aren't stuck outside the map.
    if(pbMustCreateChars == true) then
        fnStandardCharacter()
    end
    
    -- |[ ======== Random Components ========= ]|
    --Maps can technically have random components even when not in a random dungeon like the Tellurium Mines. This
    -- set of routines handles that. If the random data isn't set, these are skipped.
    if(self.bIsRandomGenerationReady == true) then
        LM_ExecuteScript(self.sEntryPointPath)
    end

    -- |[Finish Up]|
	--Drop all events. This prevents the game from running while the loading screen is firing.
	Debug_DropEvents()
end
