-- |[ ================================== MapHelper Class File ================================== ]|
--Class file for MapHelper. Call this once at initialization.

--Singleton function class. Holds functions that help with things like constructing maps, setting
-- music, querying properties, etc.

-- |[ =========== Globals/Statics ============ ]|
-- |[ ============ Class Members ============= ]|
-- |[System]|
MapHelper = {}
MapHelper.__index = MapHelper

-- |[Pregenerated Random Variables]|
MapHelper.bIsRandomGenerationReady = false
MapHelper.sEntryPointPath          = "Null"
MapHelper.sExamineIntercept        = "Null"
MapHelper.sFloorRoller             = "Null"
MapHelper.zaFloorData              = {} --List of zFloorDatas for each possible floor in the generator.
MapHelper.zaAppearanceData         = {} --List of zAppearanceDatas for each possible enemy in the generator.

-- |[Pregenerated Random Functions]|
--These functions need to be implemented before this functionality can be used. Their argument lists are variable.
MapHelper.fnGenerateEnemyGroup = nil
MapHelper.fnGenerateTreasure   = nil
MapHelper.fnSelectNextLevel    = nil

-- |[zFloorData]|
--zFloorData = {}
--zFloorData.iMaxEnemies   = 0				--Highest number of enemy patrols that can spawn on this floor.
--zFloorData.iMaxTreasures = 0				--Highest number of treasure chests that can spawn on this floor.
--zFloorData.zaEnemyLookups = {}			--Table of zEnemyRollData to specify what enemies can spawn on this floor.
--zFloorData.zaTreasureLookups = {}			--Table of zTreasureRollData to specify what treasures can spawn on this floor.

-- |[zAppearanceData]|
--zAppearanceData = {}
--zAppearanceData.sEnemyName  = "Null"		--Name of the enemy on the combat lookups.
--zAppearanceData.sSpriteName = "Null"		--Sprite name in the datalibrary
--zAppearanceData.iToughness  = 0			--Toughness, at 1 or 2 causes outlines.

-- |[zEnemyRollData]|
--zEnemyRollData = {}
--zEnemyRollData.iRollChance = 0			--Chance of this table being picked. Should not be zero.
--zEnemyRollData.saEnemyNames = {}			--List of enemy names that will appear if this table is picked. Can be empty.

-- |[zTreasureRollData]|
--zTreasureRollData = {}
--zTreasureRollData.iRollChance = 0			--Chance of this table being picked. Should not be zero.
--zTreasureRollData.saTreasureNames = {}	--List of items on this table. One is picked at flat-random. If empty, the chest despawns.

-- |[ ============ Class Methods ============= ]|
--System
--Location Resolve
function MapHelper:fnResolveMapLocation(psMapName)    end
function MapHelper:fnResolveMapLocationCh5(psMapName) end

--Pregenerated Random Levels
function MapHelper:fnClearPreranVariables()                                    end
function MapHelper:fnVerifyPreranVariables()                                   end
function MapHelper:fnSetFloorData(piFloor, piMaxEnemies, piMaxTreasures)       end
function MapHelper:fnGetMaxEnemiesForFloor(piFloor)                            end
function MapHelper:fnGetMaxTreasuresForFloor(piFloor)                          end
function MapHelper:fnGetEnemyDataForFloor(piFloor)                             end
function MapHelper:fnGetTreasureDataForFloor(piFloor)                          end
function MapHelper:fnAddAppearanceData(psEnemyName, psSpriteName, piToughness) end
function MapHelper:fnGetAppearanceData(psEnemyName)                            end
function MapHelper:fnCreateEnemyRollData(piRollChance, psaEnemyNames)          end
function MapHelper:fnSelectEnemyGroup(pzaEnemyRollData)                        end
function MapHelper:fnCreateTreasureRollData(piRollChance, psaTreasureNames)    end
function MapHelper:fnCloneTreasureCluster(pzaBaseCluster, pzaTreasureRollList) end
function MapHelper:fnSelectTreasure(pzaTreasureRollData)                       end
function MapHelper:fnPreranSetPatrols(piFloorNumber)                           end
function MapHelper:fnPreranSetTreasure(piFloorNumber)                          end

--Standard Level
function MapHelper:fnStandardLevel(psCallingPath, psLevelName, psChestLevelName, psMapResolveName, pbMustCreateChars, psMusicName, piTileSize) end

-- |[ ========== Class Constructor =========== ]|
--Singleton, class should not be instantiated.

-- |[ ========= Call Implementations ========= ]|
LM_ExecuteScript(fnResolvePath() .. "MapHelperLocationResolveCh5.lua")
LM_ExecuteScript(fnResolvePath() .. "MapHelperPregenRandom.lua")
LM_ExecuteScript(fnResolvePath() .. "MapHelperStandardLevel.lua")
