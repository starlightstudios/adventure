-- |[ ================================== TagTable Class File =================================== ]|
--Class file for TagTable. Call this once at initialization.

--A TagTable is actually just a normal table. This is a namespacing class used to run operations
-- very common tag tables.
--The format of a tag table is: { {sTagA, iTagCountA}, {sTagB, iTagCountB}, ... }

-- |[ =========== Globals/Statics ============ ]|
-- |[ ============ Class Members ============= ]|
-- |[System]|
TagTable = {}

-- |[ ============ Class Methods ============= ]|
--System
--Property Queries
function TagTable:fnGetTagCount(pzaTagArray, psTagName) end

--Manipulators
function TagTable:fnAddTag(pzaTagArray, psTagName, piTagCount)        end
function TagTable:fnSetTag(pzaTagArray, psTagName, piTagCount)        end
function TagTable:fnSetTagHighest(pzaTagArray, psTagName, piTagCount) end
function TagTable:fnSubtractTag(pzaTagArray, psTagName, piTagCount)   end

-- |[ ========== Class Constructor =========== ]|
--This class is a static singleton and does not need to be instantiated.

-- |[ ========= Call Implementations ========= ]|
LM_ExecuteScript(fnResolvePath() .. "TagTablePropertyQueries.lua")
LM_ExecuteScript(fnResolvePath() .. "TagTableManipulators.lua")
