-- |[ ================================= TagTable Manipulators ================================== ]|
--Sets data in a tag table.

-- |[ ================================== TagTable:fnAddTag() =================================== ]|
--Adds a tag to the provided table. If the count is negative, calls the remove function.
function TagTable:fnAddTag(pzaTagArray, psTagName, piTagCount)
    
    -- |[Argument Check]|
    if(pzaTagArray == nil) then return end
    if(psTagName   == nil) then return end
    if(piTagCount  == nil) then return end
    
    -- |[Redirect]|
    --If the tag count is zero, stop.
    if(piTagCount == 0) then return end
    
    --If the tag count is negative, call the subtract function, as it can handle removing tag
    -- entries entirely.
    if(piTagCount < 0) then
        self:fnSubtractTag(pzaTagArray, psTagName, piTagCount * -1)
        return
    end
    
    -- |[Search Table]|
    --If an entry in the table already exists, add it.
    for i = 1, #pzaTagArray, 1 do
        if(pzaTagArray[i][1] == psTagName) then
            pzaTagArray[i][2] = pzaTagArray[i][2] + piTagCount
            return
        end
    end
    
    -- |[No Match]|
    --Create and add a new tag.
    local zTag = {psTagName, piTagCount}
    table.insert(pzaTagArray, zTag)
end

-- |[ ================================== TagTable:fnSetTag() =================================== ]|
--Sets the tag count in the table, overriding whatever it currently is.
function TagTable:fnSetTag(pzaTagArray, psTagName, piTagCount)
    
    -- |[Argument Check]|
    if(pzaTagArray == nil) then return end
    if(psTagName   == nil) then return end
    if(piTagCount  == nil) then return end
    
    --It is not a legal action to set a count to a negative number.
    if(piTagCount < 0) then return end
    
    -- |[Search Table]|
    --If an entry in the table already exists, add it.
    for i = 1, #pzaTagArray, 1 do
        if(pzaTagArray[i][1] == psTagName) then
            
            --If the requested count is zero, delete the entry.
            if(piTagCount == 0) then
                table.remove(pzaTagArray, i)
                return
            end
            
            --Set it.
            pzaTagArray[i][2] = piTagCount
            return
        end
    end
    
    -- |[No Match]|
    --Create and add a new tag. If the count is zero, do nothing.
    if(piTagCount < 1) then return end
    
    --Add.
    local zTag = {psTagName, piTagCount}
    table.insert(pzaTagArray, zTag)
end

-- |[ =============================== TagTable:fnSetTagHighest() =============================== ]|
--Sets the tag count in the table, overriding whatever it currently is, but only if the new number
-- is higher than the existing count.
function TagTable:fnSetTagHighest(pzaTagArray, psTagName, piTagCount)
    
    -- |[Argument Check]|
    if(pzaTagArray == nil) then return end
    if(psTagName   == nil) then return end
    if(piTagCount  == nil) then return end
    
    -- |[Get Current]|
    local iCurrentTagCount = self:fnGetTagCount(pzaTagArray, psTagName)
    if(iCurrentTagCount < piTagCount) then
        self:fnSetTag(pzaTagArray, psTagName, piTagCount)
    end
end

-- |[ ================================ TagTable:fnSubtractTag() ================================ ]|
--Subtracts the tag count from the table, including removing the tag if an entry hits zero. If the
-- value is negative, calls the adder function.
function TagTable:fnSubtractTag(pzaTagArray, psTagName, piTagCount)
    
    -- |[Argument Check]|
    if(pzaTagArray == nil) then return end
    if(psTagName   == nil) then return end
    if(piTagCount  == nil) then return end
    
    -- |[Redirect]|
    --If the tag count is zero, stop.
    if(piTagCount == 0) then return end
    
    --If the tag count is negative, call the subtract function, as it can handle removing tag
    -- entries entirely.
    if(piTagCount < 0) then
        self:fnAddTag(pzaTagArray, psTagName, piTagCount * -1)
        return
    end
    
    -- |[Search Table]|
    --Search for the entry.
    for i = 1, #pzaTagArray, 1 do
        if(pzaTagArray[i][1] == psTagName) then
            
            --If the entry does not have enough tags, remove it and stop.
            if(pzaTagArray[i][2] <= piTagCount) then
                table.remove(pzaTagArray, i)
                return
            end
            
            --Has enough. Decrement and stop.
            pzaTagArray[i][2] = pzaTagArray[i][2] - piTagCount
            return
        end
    end
end