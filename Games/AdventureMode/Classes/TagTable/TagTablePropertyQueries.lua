-- |[ =============================== TagTable Property Queries ================================ ]|
--Returns information about a tag table.

-- |[ ================================ TagTable:fnGetTagCount() ================================ ]|
--Returns how many times a tag appears on a table, which can be zero.
function TagTable:fnGetTagCount(pzaTagArray, psTagName)
    
    -- |[Argument Check]|
    if(pzaTagArray == nil) then return 0 end
    if(psTagName   == nil) then return 0 end
    
    -- |[Search Table]|
    --Setup.
    local iCount = 0
    
    --If an entry in the table already exists, add it.
    for i = 1, #pzaTagArray, 1 do
        if(pzaTagArray[i][1] == psTagName) then
            iCount = iCount + pzaTagArray[i][2]
        end
    end
    
    --Finish up.
    return iCount
end