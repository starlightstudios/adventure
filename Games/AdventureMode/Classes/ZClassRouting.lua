-- |[ ===================================== Class Routing ====================================== ]|
--Calls all class files.

-- |[Normal]|
LM_ExecuteScript(gsRoot .. "Classes/DiaHelper/DiaHelper.lua")
LM_ExecuteScript(gsRoot .. "Classes/GUIMap/GUIMap.lua")
LM_ExecuteScript(gsRoot .. "Classes/Hunting/Hunting.lua")
LM_ExecuteScript(gsRoot .. "Classes/JournalEntry/JournalEntry.lua")
LM_ExecuteScript(gsRoot .. "Classes/MapHelper/MapHelper.lua")
LM_ExecuteScript(gsRoot .. "Classes/MapPinSet/MapPinSet.lua")
LM_ExecuteScript(gsRoot .. "Classes/SceneReversion/SceneReversion.lua")
LM_ExecuteScript(gsRoot .. "Classes/SpriteHelper/SpriteHelper.lua")
LM_ExecuteScript(gsRoot .. "Classes/TagTable/TagTable.lua")