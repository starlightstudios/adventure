-- |[ ================================= MapPinSet Data Handler ================================= ]|
--Controls data sets.

-- |[ =============================== MapPinSet:fnRegisterPin() ================================ ]|
--Registers a new pin.
function MapPinSet:fnRegisterPin(pfX, pfY, psDisplayVar, psAlreadyFoundVar, psImagePath)
    
    -- |[Argument Check]|
    if(pfX               == nil) then return end
    if(pfY               == nil) then return end
    if(psDisplayVar      == nil) then return end
    if(psAlreadyFoundVar == nil) then return end
    if(psImagePath       == nil) then return end
    
    -- |[Create]|
    local zPackage = {}
    zPackage.fXPos             = pfX
    zPackage.fYPos             = pfY
    zPackage.sShouldDisplayVar = psDisplayVar
    zPackage.sAlreadyFoundVar  = psAlreadyFoundVar
    zPackage.sImagePath        = psImagePath
    
    -- |[Register]|
    table.insert(self.zaPinData, zPackage)
end

-- |[ ============================== MapPinSet:fnPopulatePins() ================================ ]|
--Sends all pin data to the C++ state.
function MapPinSet:fnPopulatePins()
    for i = 1, #self.zaPinData, 1 do
        self:fnUploadPinData(i)
    end
end

-- |[ ============================== MapPinSet:fnUploadPinData() =============================== ]|
--Given a slot, checks if the pin should upload to the C++ state and does so if checks pass.
function MapPinSet:fnUploadPinData(piSlot)
    
    -- |[Argument Check]|
    if(piSlot == nil) then return end
    
    -- |[Checks]|
    --Make sure the pin slot is within range.
    local zPinData = self.zaPinData[piSlot]
    if(zPinData == nil) then return end
    
    --Check if the display var is 1 or higher. If so, the pin can display. If the value is "TRUE" then always pass.
    if(zPinData.sShouldDisplayVar ~= "TRUE") then
        local sVariable = VM_GetVar(zPinData.sShouldDisplayVar, "N")
        if(sVariable < 1.0) then return end
    end
    
    --Check if the pin is in the "already found" category. If this is "FALSE" do nothing.
    if(zPinData.sAlreadyFoundVar ~= "FALSE") then
        if(DL_Exists(zPinData.sAlreadyFoundVar) == true) then return end
    end
    
    --All checks passed. Upload the data.
    AM_SetProperty("Register Map Pin", zPinData.fXPos, zPinData.fYPos, zPinData.sImagePath)
end
