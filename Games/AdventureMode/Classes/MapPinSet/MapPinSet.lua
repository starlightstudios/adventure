-- |[ ================================== MapPinSet Class File ================================== ]|
--Class file for MapPinSet. Call this once at initialization.

--This stores a list of map pins, as well as a name identifier so the set can be searched based
-- on the associated map set.

-- |[ =========== Globals/Statics ============ ]|
-- |[Constants]|
gcsMapPinSet_IcoHlt = "Root/Images/AdventureUI/CampfireMenu/Pin_Health"
gcsMapPinSet_IcoAtk = "Root/Images/AdventureUI/CampfireMenu/Pin_Power"
gcsMapPinSet_IcoIni = "Root/Images/AdventureUI/CampfireMenu/Pin_Initiative"
gcsMapPinSet_IcoAcc = "Root/Images/AdventureUI/CampfireMenu/Pin_Accuracy"
gcsMapPinSet_IcoEvd = "Root/Images/AdventureUI/CampfireMenu/Pin_Evade"
gcsMapPinSet_IcoSki = "Root/Images/AdventureUI/CampfireMenu/Pin_Skill"

-- |[ ============ Class Members ============= ]|

-- |[System]|
MapPinSet = {}
MapPinSet.__index = MapPinSet
MapPinSet.sLocalName = "Default"

-- |[Lists]|
MapPinSet.zaPinData = {} --Format is MapPinData, below

-- |[MapPinData]|
--zMapPinData.fXPos             --X position on the map
--zMapPinData.fYPos             --Y position on the map
--zMapPinData.sShouldDisplayVar --DLPath, if queried variable is 1, this pin can display
--zMapPinData.sAlreadyFoundVar  --DLPath, if queried variable exists, the pin does not display
--zMapPinData.sImagePath        --DLPath for the image to display.

-- |[ ============ Class Methods ============= ]|
--System
function MapPinSet:new(psName) end

--Data Handling
function MapPinSet:fnRegisterPin(pfX, pfY, psDisplayVar, psAlreadyFoundVar, psImagePath) end
function MapPinSet:fnPopulatePins()                                                      end
function MapPinSet:fnUploadPinData(piSlot)                                               end

-- |[ ========== Class Constructor =========== ]|
--Must have a unique name for the character in question.
function MapPinSet:new(psName)
    
    -- |[ ========= Setup ========== ]|
    -- |[Argument Check]|
    if(psName == nil) then psName = "Default" end
    
    -- |[Create]|
    local zObject = {}
    setmetatable(zObject, MapPinSet)
    
    -- |[ ======== Members ========= ]|
    --Local member.
    zObject.sLocalName = psName
    
    --Reset local lists so they don't use the global class listings.
    zObject.zaPinData = {}
    
    -- |[ ======= Finish Up ======== ]|
    --Return object.
    return zObject
end

-- |[ ========= Call Implementations ========= ]|
LM_ExecuteScript(fnResolvePath() .. "MapPinSetDataHandler.lua")
