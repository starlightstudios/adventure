-- |[ ============================= SpriteHelper System Functions ============================== ]|
--System operations related to clearing or setting core variables.

-- |[ =============================== SpriteHelper Manipulators ================================ ]|
-- |[fnSetActiveSprite]|
--Sets name of active sprite in static class.
function SpriteHelper:fnSetActiveSprite(psName)
    
    -- |[Nil: Clears]|
    if(psName == nil) then
        self.sActiveSpriteName = "Null"
        return
    end
    
    -- |[Normal]|
    self.sActiveSpriteName = psName
    
end

-- |[fnClear]|
--Clears all active variables in the static class.
function SpriteHelper:fnClear()
    self.sActiveSpriteName = "Null"
end
