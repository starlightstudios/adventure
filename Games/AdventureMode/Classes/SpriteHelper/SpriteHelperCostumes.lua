-- |[ ============================= SpriteHelper Costume Functions ============================= ]|
--Functions typically used by costume scripts.

-- |[ ========================= SpriteHelper:fnDisableIdleAnimation() ========================== ]|
--Removes idle animation from the active sprite name, if it exists.
function SpriteHelper:fnDisableIdleAnimation()

    -- |[Error Checks]|
    --Active actor check. If the actor was not specified, bark a warning.
    if(self.sActiveSpriteName == "Null") then
        io.write("SpriteHelper:fnDisableIdleAnimation() - Warning. Active sprite was Null. It must be set with fnSetActiveSprite() first. Stopping.\n")
        return
    end
    
    --Do nothing if the actor does not exist. This is not an error, costumes often call this
    -- function even if the character hasn't been created yet.
    if(EM_Exists(self.sActiveSpriteName) == false) then return end

    -- |[Disable]|
    EM_PushEntity(self.sActiveSpriteName)
        TA_SetProperty("Idle Animation Flag", false)
    DL_PopActiveObject()
end

-- |[ =========================== SpriteHelper:fnSetIdleAnimation() ============================ ]|
--Sets the idle animation for the sActiveSpriteName in the given form. Pass "Null" to disable. 
-- This one uses a simple endless loop case, or a play-once-then-done case.
function SpriteHelper:fnSetIdleAnimation(psDLPathPattern, piFramesTotal, piTPF, piLoopFrame)
    
    -- |[Error Checks]|
    --Arg check.
    if(psDLPathPattern == nil) then return end
    if(piFramesTotal   == nil) then return end
    if(piTPF           == nil) then return end
    if(piLoopFrame     == nil) then return end
    
    --Active actor check. If the actor was not specified, bark a warning.
    if(self.sActiveSpriteName == "Null") then
        io.write("SpriteHelper:fnSetIdleAnimation() - Warning. Active sprite was Null. It must be set with fnSetActiveSprite() first. Stopping.\n")
        return
    end
    
    --Do nothing if the actor does not exist. This is not an error, costumes often call this
    -- function even if the character hasn't been created yet.
    if(EM_Exists(self.sActiveSpriteName) == false) then return end
    
    -- |[No Animation]|
    --If the path pattern is "Null", order the sprite to disable idle animations and stop.
    if(psDLPathPattern == "Null") then
        SpriteHelper:fnDisableIdleAnimation()
        return
    end
    
    -- |[Set Sprites]|
    --Legality check.
    if(piFramesTotal < 1) then
        SpriteHelper:fnDisableIdleAnimation()
    end
    
    --Otherwise, set the idle animations.
    EM_PushEntity(self.sActiveSpriteName)
        TA_SetProperty("Idle Animation Flag", true)
        TA_SetProperty("Allocate Idle Frames", piFramesTotal)
        for i = 0, piFramesTotal-1, 1 do
            TA_SetProperty("Set Idle Frame", i, psDLPathPattern .. i)
        end
        TA_SetProperty("Set Idle TPF", piTPF)
        TA_SetProperty("Set Idle Loop Frame", piLoopFrame)
    DL_PopActiveObject()
end

-- |[ ======================= SpriteHelper:fnSetIdleAnimationAdvanced() ======================== ]|
--Same as above, except uses a list of frames ex: {0, 1, 2, 2, 1, 0} to specify which frame is being
-- used, allowing more complex animations.
function SpriteHelper:fnSetIdleAnimationAdvanced(psDLPathPattern, piTPF, piaFrameList)
    
    -- |[Error Checks]|
    --Arg check.
    if(psDLPathPattern == nil) then return end
    if(piTPF           == nil) then return end
    if(piaFrameList    == nil) then return end
    
    --Active actor check. If the actor was not specified, bark a warning.
    if(self.sActiveSpriteName == "Null") then
        io.write("SpriteHelper:fnSetIdleAnimationAdvanced() - Warning. Active sprite was Null. It must be set with fnSetActiveSprite() first. Stopping.\n")
        return
    end
    
    --Do nothing if the actor does not exist. This is not an error, costumes often call this
    -- function even if the character hasn't been created yet.
    if(EM_Exists(self.sActiveSpriteName) == false) then return end
    
    -- |[No Animation]|
    --Disable if "Null".
    if(psDLPathPattern == "Null") then
        SpriteHelper:fnDisableIdleAnimation()
        return
    end
    
    -- |[Set Sprites]|
    --Legality check.
    if(#piaFrameList < 1) then
        SpriteHelper:fnDisableIdleAnimation()
        return
    end
    
    --Otherwise, set the idle animations.
    EM_PushEntity(self.sActiveSpriteName)
        TA_SetProperty("Idle Animation Flag", true)
        TA_SetProperty("Allocate Idle Frames", #piaFrameList)
        for i = 1, #piaFrameList, 1 do
            TA_SetProperty("Set Idle Frame", i-1, psDLPathPattern .. piaFrameList[i])
        end
        TA_SetProperty("Set Idle TPF", piTPF)
        TA_SetProperty("Set Idle Loop Frame", -1)
    DL_PopActiveObject()
end
