-- |[ ================================ SpriteHelper Class File ================================= ]|
--Class file for SpriteHelper. Call this once at initialization.

--Place a description here. This is a singleton version of the normal file, so it has no constructor.

-- |[ =========== Globals/Statics ============ ]|
-- |[ ============ Class Members ============= ]|
-- |[System]|
SpriteHelper = {}
SpriteHelper.__index = SpriteHelper

-- |[Active Sprite]|
SpriteHelper.sActiveSpriteName = "Null"

-- |[ ============ Class Methods ============= ]|
--System
function SpriteHelper:fnSetActiveSprite(psName) end
function SpriteHelper:fnClear()                 end

--Costumes
function SpriteHelper:fnSetIdleAnimation(psDLPathPattern, piFramesTotal, piTPF, piLoopFrame) end
function SpriteHelper:fnSetIdleAnimationAdvanced(psDLPathPattern, piTPF, piaFrameList)       end

-- |[ ========== Class Constructor =========== ]|
--Singleton, class should not be instantiated.

-- |[ ========= Call Implementations ========= ]|
LM_ExecuteScript(fnResolvePath() .. "SpriteHelperSystem.lua")
LM_ExecuteScript(fnResolvePath() .. "SpriteHelperCostumes.lua")
