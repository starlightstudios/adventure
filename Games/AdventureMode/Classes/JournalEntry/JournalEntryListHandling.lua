-- |[ =============================== JournalEntry List Handling =============================== ]|
--Handles list functions. Lists are typically passed in. Journal entries do not use global listings
-- for most cases because they interfere with mods.

-- |[ ============================== JournalEntry:fnEntryExists() ============================== ]|
--Returns true if the named entry exists, false if not. Never prints diagnostics.
function JournalEntry:fnEntryExists(psName, pzaListing)

	-- |[Arg Check]|
	if(psName     == nil) then return false end
	if(pzaListing == nil) then return false end
    
    -- |[Scan]|
    for i = 1, #pzaListing, 1 do
        if(pzaListing[i].sLocalName == psName) then return true end
    end
    
    -- |[Not Found]|
    return false
end

-- |[ ============================== JournalEntry:fnLocateEntry() ============================== ]|
--Locates the named entry in the provided list. Returns nil if not found. Reports an error if not
-- found if the diagnostic flags are on.
function JournalEntry:fnLocateEntry(psName, pzaListing)
    
    -- |[Diagnostics]|
    local bDiagnostics = Debug_GetFlag("Journal: All") or Debug_GetFlag("Journal: Lists")

	-- |[Arg Check]|
	if(psName == nil) then
        Debug_ForcePrint("JournalEntry:fnLocateEntry() - Warning, name was nil. Failing.\n")
        return nil
    end
	if(pzaListing == nil) then
        Debug_ForcePrint("JournalEntry:fnLocateEntry() - Warning, list was nil. Failing.\n")
        return nil
    end
    
    -- |[Scan]|
    for i = 1, #pzaListing, 1 do
        if(pzaListing[i].sLocalName == psName) then return pzaListing[i] end
    end
    
    -- |[Not Found]|
    if(bDiagnostics) then
        Debug_ForcePrint("JournalEntry:fnLocateEntry() - Warning, entry " .. psName .. " was not found. List had " .. #pzaListing .. " entries.\n")
    end
    return nil
end

-- |[ ====================== JournalEntry:fnLocateEnemyDataOnStatChart() ======================= ]|
--Locates the named entry in the provided list, which should be enemy stat chart entries. Returns 
-- nil if not found. Reports an error if not found if the diagnostic flags are on.
function JournalEntry:fnLocateEnemyDataOnStatChart(psName, pzaListing)
    
    -- |[Diagnostics]|
    local bDiagnostics = Debug_GetFlag("Journal: All") or Debug_GetFlag("Journal: Lists")

	-- |[Arg Check]|
	if(psName == nil) then
        Debug_ForcePrint("JournalEntry:fnLocateEnemyDataOnStatChart() - Warning, name was nil. Failing.\n")
        return nil
    end
	if(pzaListing == nil) then
        Debug_ForcePrint("JournalEntry:fnLocateEnemyDataOnStatChart() - Warning, list was nil. Failing.\n")
        return nil
    end
    
    -- |[Scan]|
    for i = 1, #pzaListing, 1 do
        if(pzaListing[i].sActualName == psName) then return pzaListing[i] end
    end
    
    -- |[Not Found]|
    if(bDiagnostics) then
        Debug_ForcePrint("JournalEntry:fnLocateEnemyDataOnStatChart() - Warning, entry " .. psName .. " was not found. List had " .. #pzaListing .. " entries.\n")
    end
    return nil
    
end