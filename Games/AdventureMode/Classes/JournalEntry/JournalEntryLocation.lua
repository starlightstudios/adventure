-- |[ ================================= JournalEntry Locations ================================= ]|
--Functions for creating and uploading location entries.

-- |[ ============================ JournalEntry:fnSetToLocation() ============================ ]|
--Converts this object into a location entry, and sets the local variables.
function JournalEntry:fnSetToLocation(psImagePath, pfOffsetX, pfOffsetY)

    -- |[Argument Check]|
    if(psImagePath == nil) then psImagePath = "Null" end
    if(pfOffsetX   == nil) then pfOffsetX = 0 end
    if(pfOffsetY   == nil) then pfOffsetY = 0 end
    
    -- |[Set Local Variables]|
    self.sType = "Location"
    self.sPortrait = psImagePath
    self.fPortraitX = pfOffsetX
    self.fPortraitY = pfOffsetY
    
    -- |[Register]|
    --Register this object to the provided storage list.
    table.insert(JournalEntry.zaAppendList, self)
end

-- |[ ============================== JournalEntry:fnSetVariable() ============================== ]|
--Sets the variable the entry will use to display or not. Pass "TRUE" to always show.
function JournalEntry:fnSetVariable(psDLPath)

    -- |[Argument Check]|
    if(psDLPath == nil) then return end
    
    -- |[Set Path]|
    self.sUnlockedVar = psDLPath
end

-- |[ ====================== JournalEntry:fnCreateProgramLocationEntry() ======================= ]|
--Creates a Location entry in the C++ state.
function JournalEntry:fnCreateProgramLocationEntry(psInternalName, psDisplayName, psVariablePath, psImagePath, psOffsetX, pfOffsetY, psDescription)

    -- |[Argument Check]|
    if(psInternalName == nil) then return end
    if(psDisplayName  == nil) then return end
    if(psVariablePath == nil) then return end
    if(psImagePath    == nil) then return end
    if(psOffsetX      == nil) then return end
    if(pfOffsetY      == nil) then return end
    if(psDescription  == nil) then return end
    
    -- |[Blank Entry]|
    --If the variable is "TRUE" then just bypass this. Alternately, if the debug option to show all entries is true, bypass this check.
    local bShowAllEntries = AM_GetPropertyJournal("Show All Entries")
    if(bShowAllEntries ~= true and psVariablePath ~= "TRUE") then
        
        --Retrieve the variable. If it's not 1.0, create a blank entry and stop.
        local iHasSeenThisRegion = VM_GetVar(psVariablePath, "N")
        if(iHasSeenThisRegion ~= 1.0) then
            AM_SetPropertyJournal("Add Location Entry", psInternalName, "-----")
            return
        end
    end
    
    -- |[Set]|
    AM_SetPropertyJournal("Add Location Entry", psInternalName, psDisplayName)
    AM_SetPropertyJournal("Set Location Image", psInternalName, psImagePath)
    AM_SetPropertyJournal("Set Location Image Offsets", psInternalName, psOffsetX, pfOffsetY)
    
    -- |[Description]|
    AM_SetPropertyJournal("Set Location Description Auto", psInternalName, psDescription)
end

