-- |[ ================================= JournalEntry Paragons ================================== ]|
--Functions for creating and uploading bestiary entries.

-- |[ ============================= JournalEntry:newParagon() ============================== ]|
--Creates a new JournalEntry and immediately sets it to Paragon mode. Registers to the global list.
function JournalEntry:newParagon(psInternalName, psDisplayName, psGroupName)

    -- |[Argument Check]|
    if(psInternalName == nil) then return end
    if(psDisplayName  == nil) then return end
    if(psGroupName    == nil) then return end
    
    -- |[Create]|
    local zNewObject = JournalEntry:new(psInternalName, psDisplayName)
    
    -- |[Set Local Variables]|
    zNewObject.sType = "Paragon"
    zNewObject.sParagonGroup = psGroupName
    zNewObject.bParagonEnabled = false
    zNewObject.iKOCurrent = 0
    zNewObject.iKORequired = gciParagon_KO_Needed_To_Spawn
    zNewObject.bDefeatedActivator = false
    zNewObject.sEnabledPath = "Null"
    
    -- |[Check Stored Values]|
    --Get variables.
    local iIsEnabled          = VM_GetVar("Root/Variables/Paragons/"..psGroupName.."/iIsEnabled", "N")
    local iKillCount          = VM_GetVar("Root/Variables/Paragons/"..psGroupName.."/iKillCount", "N")
    local iKillsNeeded        = VM_GetVar("Root/Variables/Paragons/"..psGroupName.."/iKillsNeeded", "N")
    local iHasDefeatedParagon = VM_GetVar("Root/Variables/Paragons/"..psGroupName.."/iHasDefeatedParagon", "N")
    
    --Set.
    if(iIsEnabled == 1.0) then zNewObject.bParagonEnabled = true end
    zNewObject.iKOCurrent = iKillCount
    zNewObject.iKORequired = iKillsNeeded
    if(iHasDefeatedParagon == 1.0) then zNewObject.bDefeatedActivator = true end
    zNewObject.sEnabledPath = "Root/Variables/Paragons/"..psGroupName.."/iIsEnabled"
    
    -- |[Register]|
    --Register this object to the provided storage list.
    table.insert(JournalEntry.zaAppendList, zNewObject)
end

-- |[ ======================= JournalEntry:fnCreateProgramParagonEntry() ======================= ]|
--Static function, uses the provided variables to create a new paragon entry in the C++ state.
function JournalEntry:fnCreateProgramParagonEntry(psInternalName, psDisplayName, pbIsEnabled, piKOCurrent, piKONeeded, pbActivatorDefeated, psEnabledPath)
    
    -- |[Argument Check]|
    if(psInternalName      == nil) then return end
    if(psDisplayName       == nil) then return end
    if(pbIsEnabled         == nil) then return end
    if(piKOCurrent         == nil) then return end
    if(piKONeeded          == nil) then return end
    if(pbActivatorDefeated == nil) then return end
    if(psEnabledPath       == nil) then return end
    
    -- |[Zero KOs]|
    --If the KO count was zero, add a blank entry.
    if(piKOCurrent < 1) then
        AM_SetPropertyJournal("Add Paragon Entry", psInternalName, "-----")
    else
        AM_SetPropertyJournal("Add Paragon Entry", psInternalName, psDisplayName)
    end
    
    -- |[Data]|
    AM_SetPropertyJournal("Set Paragon Enabled", psInternalName, pbIsEnabled)
    AM_SetPropertyJournal("Set Paragon KO Counts", psInternalName, piKOCurrent, piKONeeded)
    AM_SetPropertyJournal("Set Paragon Has Defeated Activator", psInternalName, pbActivatorDefeated)
    AM_SetPropertyJournal("Set Paragon Enabled Path", psInternalName, psEnabledPath)
end