-- |[ ================================= JournalEntry Bestiary ================================== ]|
--Functions for creating and uploading bestiary entries.

-- |[ ========================== JournalEntry:fnCreateBestiaryEntry() ========================== ]|
--The raw creation function for a new bestiary entry. This can operate without the stat chart/name
-- and will still create a limited version of the entry. This is a static function.
--If an entry is created without the stat chart, it always displays, since nominally at least 1
-- kill is required to display.
--This function is called by JournalEntry:fnUploadData() if the type is "Bestiary".
function JournalEntry:fnCreateBestiaryEntry(pzaEnemyStatChart, psInternalName, psChartName, pbAlwaysShow, psDisplayName, psPortrait, psParagon, pfPortraitX, pfPortraitY, psTurnIco, pbCanTFPlayer, psDescription)
    
    -- |[ =============== Argument Check =============== ]|
    if(pbAlwaysShow  == nil) then return end
    if(psDisplayName == nil) then return end
    if(psPortrait    == nil) then return end
    if(psParagon     == nil) then return end
    if(pfPortraitX   == nil) then return end
    if(pfPortraitY   == nil) then return end
    if(pbCanTFPlayer == nil) then return end
    if(psDescription == nil) then return end
    
    -- |[Setup]|
    --Flags.
    local iAlwaysShowStats = VM_GetVar("Root/Variables/Global/Debug/iAlwaysShowStats", "N")
    
    --Debug flag.
    local bShowAllEntries = AM_GetPropertyJournal("Show All Entries")
    if(bShowAllEntries) then 
        pbAlwaysShow = true
        iAlwaysShowStats = 1
    end
    
    -- |[ =============== Common Display =============== ]|
    --This section always displays, even if the enemy stat chart is not provided.
    local iKOCount = 0
    local iParagonKOCount = 0
    
    -- |[No Chart or Names]|
    --If no enemy chart or requisite names are provided, this entry always appears:
    if(pzaEnemyStatChart == nil or psInternalName == nil or psChartName == nil) then

    -- |[Visiblity]|
    else
    
        --Huntable track. Doesn't always exist, but if it does it can reveal an entry even with zero KO's.
        local sHuntTrackerPath = VM_GetVar("Root/Variables/Global/Combat/sHuntTrackerPath", "S")
        local iHuntCount = VM_GetVar(sHuntTrackerPath .. psInternalName, "N")
        
        --Get the KO count. If the count is zero, and not flagged to always show, then don't create an entry (unless a hunt has occurred).
        local sKOTrackerPath = VM_GetVar("Root/Variables/Global/Combat/sKOTrackerPath", "S")
        iKOCount = VM_GetVar(sKOTrackerPath .. psInternalName, "N")
    
        --KO for the paragon variant, if it exists.
        iParagonKOCount = VM_GetVar(sKOTrackerPath .. psInternalName .. " Paragon", "N")
        
        --No KO's, no hunts.
        if(iKOCount < 1 and iHuntCount < 1 and pbAlwaysShow == false) then
            AM_SetPropertyJournal("Add Bestiary Entry", psInternalName, "-----")
            return
        end
    end
    
    -- |[Basic Display]|
    --Display.
    AM_SetPropertyJournal("Add Bestiary Entry",             psInternalName, psDisplayName)
    AM_SetPropertyJournal("Set Bestiary Image",             psInternalName, psPortrait, pfPortraitX, pfPortraitY)
    AM_SetPropertyJournal("Set Bestiary Paragon",           psInternalName, psParagon,  pfPortraitX, pfPortraitY)
    AM_SetPropertyJournal("Set Bestiary Turn Ico",          psInternalName, psTurnIco)
    AM_SetPropertyJournal("Set Bestiary Defeat Count",      psInternalName, iKOCount)
    AM_SetPropertyJournal("Set Bestiary Defeat Paragon",    psInternalName, iParagonKOCount)
    AM_SetPropertyJournal("Set Bestiary Defeat Max",        psInternalName, 0)
    AM_SetPropertyJournal("Set Bestiary Can Transform You", psInternalName, pbCanTFPlayer)
    
    --Description. The C++ parser handles line breaks and special sequences.
    AM_SetPropertyJournal("Set Bestiary Description Auto", psInternalName, psDescription)

    -- |[ ============== Chart Statistics ============== ]|
    -- |[Display Check]|
    --If the charts were not provided, stop here.
    if(pzaEnemyStatChart == nil or psInternalName == nil or psChartName == nil) then return end
    
    --Get the chart entry. If not found, stop here. This will print a diagnostic.
    local zEntry = JournalEntry:fnLocateEnemyDataOnStatChart(psChartName, pzaEnemyStatChart)
    if(zEntry == nil) then
        Debug_ForcePrint("JournalEntry:fnCreateBestiaryEntry() - Warning, entry " .. psChartName .. " was not found. Chart has " .. #pzaEnemyStatChart .. " entries.\n")
        return
    end
    
    -- |[Kill Counts]|
    --Combat results.
    AM_SetPropertyJournal("Set Bestiary Results", psInternalName, zEntry.iExp, zEntry.iPlatina)
    
    --Determine highest KOs needed to show all stats.
    local iHighestKOs = zEntry.iKOForStats
    if(zEntry.iKOForResists > iHighestKOs) then iHighestKOs = zEntry.iKOForResists end
    AM_SetPropertyJournal("Set Bestiary Defeat Max", psInternalName, iHighestKOs)

    -- |[Stats and Resistances]|
    --Determine if we should print statistics/resistances.
    local bPrintStats   = (iAlwaysShowStats == 1) or (zEntry.iKOForStats   ~= -1 and iKOCount >= zEntry.iKOForStats)
    local bPrintResists = (iAlwaysShowStats == 1) or (zEntry.iKOForResists ~= -1 and iKOCount >= zEntry.iKOForResists)
    
    --Level. Always shows.
    AM_SetPropertyJournal("Set Bestiary Level", psInternalName, zEntry.iLevel)
    
    --Statistics. Shows if the player has enough KOs.
    if(bPrintStats) then
        AM_SetPropertyJournal("Set Bestiary Statistics", psInternalName, zEntry.iHealth, zEntry.iAtk, zEntry.iIni, zEntry.iAcc, zEntry.iEvd, zEntry.iPrt)
    end
    
    --Resistances. Shows if the player has enough KOs.
    if(bPrintResists) then
        AM_SetPropertyJournal("Set Bestiary Resistances", psInternalName, zEntry.iSls, zEntry.iStk, zEntry.iPrc, zEntry.iFlm, zEntry.iFrz, zEntry.iShk, zEntry.iCru, zEntry.iObs, zEntry.iBld, 
                                                                          zEntry.iPsn, zEntry.iCrd, zEntry.iTrf)
    end
end

-- |[ ============================ JournalEntry:fnSetToChartEntry() ============================ ]|
--Converts this object into a bestiary entry and stores required information. Requires the global 
-- lists JournalEntry.zaEnemyStats and JournalEntry.zaAppendList to be set first.
function JournalEntry:fnSetToChartEntry(psChartName, pbAlwaysShow, pfPortraitX, pfPortraitY, pbCanTFPlayer)

    -- |[Diagnostics]|
    local bDiagnostics = Debug_GetFlag("Journal: All") or Debug_GetFlag("Journal: Bestiary")
    Debug_PushPrint(bDiagnostics, "JournalEntry:fnSetToChartEntry() - Begin.\n")
    Debug_Print("Entry name is " .. self.sLocalName .. "\n")
    
    -- |[List Check]|
    if(JournalEntry.zaEnemyStats == nil or JournalEntry.zaAppendList == nil) then
        Debug_PopPrint("Failed due to unset global list..\n")
        return
    end

    -- |[Argument Check]|
    local bFail = false
    if(psChartName    == nil) then bFail = true end
    if(pbAlwaysShow   == nil) then bFail = true end
    if(pfPortraitX    == nil) then bFail = true end
    if(pfPortraitY    == nil) then bFail = true end
    if(pbCanTFPlayer  == nil) then bFail = true end
    if(bFail) then
        Debug_PopPrint("Failed due to nil argument.\n")
        return
    end
    
    -- |[Enemy Stat Chart Entry]|
    --Note: Diagnostics print in the calling function, don't need to be done here.
    local zEnemyChartEntry = JournalEntry:fnLocateEnemyDataOnStatChart(psChartName, JournalEntry.zaEnemyStats)
    if(zEnemyChartEntry == nil) then
        Debug_PopPrint("Failed, unable to find entry on provided chart. Chart name: " .. psChartName .. ", chart has " .. #JournalEntry.zaEnemyStats .. " entries.\n")
        return
    end

    -- |[Paragon Display Path]|
    --Get the paragon portrait path. The basic path is always "Root/Images/Portraits/Paragon/" with the 
    -- final part of the name the same as the base path.
    local sToSlash = ""
    local iLen = string.len(zEnemyChartEntry.sPortraitPath)
    for x = iLen, 1, -1 do
        if(string.sub(zEnemyChartEntry.sPortraitPath, x, x) == "/") then
            sToSlash = string.sub(zEnemyChartEntry.sPortraitPath, x+1)
            break
        end
    end
    
    -- |[Set Local Variables]|
    --Common.
    self.sType = "Bestiary"
    
    --Bestiary type.
    self.zaChart          = JournalEntry.zaEnemyStats
    self.sChartName       = psChartName
    self.bAlwaysShow      = pbAlwaysShow
    self.sPortrait        = zEnemyChartEntry.sPortraitPath
    self.sParagon         = "Root/Images/Portraits/Paragon/" .. sToSlash
    self.fPortraitX       = pfPortraitX
    self.fPortraitY       = pfPortraitY
    self.sTurnIco         = zEnemyChartEntry.sTurnPortraitPath
    self.bCanTFPlayer     = pbCanTFPlayer
    self.saDescription    = {""}
    Debug_Print("Report chart name: " .. self.sChartName .. "\n")
    Debug_Print("Portrait: " .. self.sPortrait .. "\n")
    Debug_Print("Paragon: " .. self.sParagon .. "\n")
    
    -- |[Register]|
    --Register this object to the provided storage list.
    table.insert(JournalEntry.zaAppendList, self)
    Debug_PopPrint("Finished normally.\n")
end