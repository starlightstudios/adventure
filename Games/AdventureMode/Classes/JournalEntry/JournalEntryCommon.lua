-- |[ ============================= JournalEntry Common Functions ============================== ]|
--Functions common to many types of journal entries.

-- |[ ============================ JournalEntry:fnSetDisplayName() ============================= ]|
--Sets the display name. Passing nil or "Default" will use the internal name instead.
function JournalEntry:fnSetDisplayName(psDisplayName)
    if(psDisplayName == nil)       then psDisplayName = self.sLocalName end
    if(psDisplayName == "Default") then psDisplayName = self.sLocalName end
    self.sDisplayName = psDisplayName
end

-- |[ ============================== JournalEntry:fnSetPortrait() ============================== ]|
--Changes the portrait. Passing nil for any value uses a default of "Null" and 0.
function JournalEntry:fnSetPortrait(psPortraitPath, pfOffsetX, pfOffsetY)
    
    -- |[Argument Check]|
    if(psPortraitPath == nil) then psPortraitPath = "Null" end
    if(pfOffsetX == nil) then pfOffsetX = 0 end
    if(pfOffsetY == nil) then pfOffsetY = 0 end
    
    -- |[Set]|
    self.sPortrait  = psPortraitPath
    self.fPortraitX = pfOffsetX
    self.fPortraitY = pfOffsetY
end

-- |[ ============================ JournalEntry:fnSetDescription() ============================= ]|
--Returns true if the named entry exists, false if not. Never prints diagnostics.
function JournalEntry:fnSetDescription(psDescription)

	-- |[Arg Check]|
	if(psDescription == nil) then return end
    
    -- |[Set]|
    self.sDescription = psDescription
end

-- |[ ============================== JournalEntry:fnUploadData() =============================== ]|
--Uploads data to the C++ state appropriate to the local type of the entry.
function JournalEntry:fnUploadData()

    -- |[Bestiary Entry]|
    if(self.sType == "Bestiary") then
        self:fnCreateBestiaryEntry(self.zaChart, self.sLocalName, self.sChartName, self.bAlwaysShow, self.sDisplayName, self.sPortrait, self.sParagon, self.fPortraitX, self.fPortraitY, 
                                   self.sTurnIco, self.bCanTFPlayer, self.sDescription)

    -- |[Location Entry]|
    elseif(self.sType == "Location") then
        self:fnCreateProgramLocationEntry(self.sLocalName, self.sDisplayName, self.sUnlockedVar ,self.sPortrait, self.fPortraitX, self.fPortraitY, self.sDescription)
    
    -- |[Paragon Entry]|
    elseif(self.sType == "Paragon") then
        self:fnCreateProgramParagonEntry(self.sLocalName, self.sDisplayName, self.bParagonEnabled, self.iKOCurrent, self.iKORequired, self.bDefeatedActivator, self.sEnabledPath)

    -- |[Profile Entry]|
    elseif(self.sType == "Profile") then
        self:fnCreateJournalProfile(self.sLocalName, self.sDisplayName, self.sPortrait, self.fPortraitX, self.fPortraitY, self.sDescription)

    -- |[Quest Entry]|
    elseif(self.sType == "Quest") then
        self:fnCreateQuestEntry(self.sLocalName, self.sDisplayName, self.sQuestObjective, self.sDescription)
    
    -- |[No Type or Unhandled]|
    --Bark a warning, the object did not set its type correctly.
    else
        Debug_ForcePrint("JournalEntry:fnUploadData() - Warning, type " .. self.sType .. " was not handled in entry " .. self.sLocalName .. "\n")
    end

end

-- |[ ============================= JournalEntry:fnCleanGlobals() ============================== ]|
--Clears all globals back to defaults.
function JournalEntry:fnCleanGlobals()
    JournalEntry.zaEnemyStats = nil
    JournalEntry.zaAppendList = nil
end
