-- |[ ================================== JournalEntry Quests =================================== ]|
--Functions for creating and uploading bestiary entries.

-- |[ ============================ JournalEntry:fnSetToChartEntry() ============================ ]|
--Converts this object into a bestiary entry and stores required information. Requires the global 
-- lists JournalEntry.zaEnemyStats and JournalEntry.zaAppendList to be set first.
function JournalEntry:fnSetToQuest(psObjective)

    -- |[Argument Check]|
    if(psObjective == nil) then return end
    
    -- |[Set Local Variables]|
    self.sType = "Quest"
    self.sQuestObjective = psObjective
    
    -- |[Register]|
    --Register this object to the provided storage list.
    table.insert(JournalEntry.zaAppendList, self)
end

-- |[ =========================== JournalEntry:fnCreateQuestEntry() ============================ ]|
function JournalEntry:fnCreateQuestEntry(psInternalName, psDisplayName, psObjective, psDescription)

    -- |[Argument Check]|
    if(psInternalName == nil) then return end
    if(psDisplayName  == nil) then return end
    if(psObjective    == nil) then return end
    if(psDescription  == nil) then return end
    
    -- |[Create, Upload]|
    AM_SetPropertyJournal("Add Quest Entry", psInternalName, psDisplayName)
    AM_SetPropertyJournal("Set Quest Objective", psInternalName, psObjective)
    AM_SetPropertyJournal("Set Quest Description Auto", psInternalName, psDescription)
end

-- |[ =========================== JournalEntry:fnBlankQuest() ============================ ]|
function JournalEntry:fnBlankQuest()
    self:fnSetDisplayName("-----")
    self:fnSetToQuest("")
    self:fnSetDescription(" ")
end
