-- |[ ================================= JournalEntry Profiles ================================== ]|
--Functions for creating and uploading profile entries.

-- |[ ============================= JournalEntry:fnSetToProfile() ============================== ]|
--Converts this object to a Profile type and sets common variables. Also registers to the global list.
function JournalEntry:fnSetToProfile(psImagePath, pfOffX, pfOffY)
    
    -- |[Argument Check]|
    if(psImagePath == nil) then return end
    if(pfOffX      == nil) then return end
    if(pfOffY      == nil) then return end
    
    -- |[Set]|
    self.sType        = "Profile"
    self.sPortrait    = psImagePath
    self.fPortraitX   = pfOffX
    self.fPortraitY   = pfOffY
    
    -- |[Register]|
    table.insert(JournalEntry.zaAppendList, self)
end

-- |[ ============================= JournalEntry:fnBlankProfile() ============================== ]|
--"Blanks" a profile, making it have the display name of "-----" and no profile or image.
function JournalEntry:fnBlankProfile()
    self:fnSetDisplayName("-----")
    self:fnSetToProfile("Null", 0, 0)
    self:fnSetDescription(" ")
end

-- |[ ========================= JournalEntry:fnCreateJournalProfile() ========================== ]|
--Static function, creates and uploads a journal entry in the C++ state.
function JournalEntry:fnCreateJournalProfile(psInternalName, psDisplayName, psImagePath, pfOffsetX, pfOffsetY, psDescription)
    
    -- |[Argument Check]|
    if(psInternalName == nil) then return end
    if(psDisplayName  == nil) then return end
    if(psImagePath    == nil) then return end
    if(pfOffsetX      == nil) then return end
    if(pfOffsetY      == nil) then return end
    if(psDescription  == nil) then return end
    
    -- |[Create]|
    --System.
    AM_SetPropertyJournal("Add Profile Entry", psInternalName, psDisplayName)
    AM_SetPropertyJournal("Set Profile Image", psInternalName, psImagePath, pfOffsetX, pfOffsetY)
    
    -- |[Description]|
    --If the provided description is "", this is an empty entry.
    if(psDescription == " ") then
        AM_SetPropertyJournal("Allocate Profile Description Lines", psInternalName, 1)
        AM_SetPropertyJournal("Set Profile Description Line", psInternalName, 0, " ")
    
    --Otherwise, use the auto-setter.
    else
        AM_SetPropertyJournal("Set Profile Description Auto", psInternalName, psDescription)
    end
end