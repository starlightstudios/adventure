-- |[ ================================ JournalEntry Class File ================================= ]|
--Class file for JournalEntry. Call this once at initialization.

--Stores a journal entry, for bestiary, quest, profiles, etc. This allows storage for local variables
-- as well as functions to process and upload. Most journal entries have similar layouts but may
-- require different C++ calls to get to the right part of the journal.

-- |[ =========== Globals/Statics ============ ]|
-- |[Constants]|
gbCanTFPlayer = true
gbCannotTFPlayer = false
gbAlwaysShow = true
gbDoNotAlwaysShow = false

-- |[ ============ Class Members ============= ]|
-- |[System]|
JournalEntry = {}
JournalEntry.__index = JournalEntry
JournalEntry.sLocalName   = "Default"
JournalEntry.sDisplayName = "Default"

-- |[Common]|
JournalEntry.sType        = "No Type"                   --Accepted types are "Bestiary", "Profile", "Quest", "Location"
JournalEntry.sPortrait    = "Null"                      --DLPath to the display portrait.
JournalEntry.fPortraitX   = 0                           --X offset when rendering the portraits.
JournalEntry.fPortraitY   = 0                           --Y offset when rendering the portraits.
JournalEntry.sDescription = ""

-- |[Bestiary]|
JournalEntry.zaChart          = nil                     --Chart storing enemy data, typically built in the Combat/Enemies/ folder for the given chapter.
JournalEntry.sChartName       = "Default"               --The name on the chart that stores stats for this bestiary entry.
JournalEntry.bAlwaysShow      = gbDoNotAlwaysShow       --If true, shows even if the player has never encountered the enemy.
JournalEntry.sDisplayName     = "Default"               --Name that appears on the journal pane.
JournalEntry.sParagon         = "Null"                  --DLPath to the paragon portrait.
JournalEntry.sTurnIco         = "Null"                  --Turn icon, used in the leftmost selection pane to preview the enemy.
JournalEntry.bCanTFPlayer     = gbCannotTFPlayer        --If true, the journal will render that the player can be TFd by this enemy.
JournalEntry.zaEnemyStats     = nil                     --Global reference pointer. Stores the "active" stat chart used in fnSetToChartEntry()
JournalEntry.zaAppendList     = nil                     --Global reference pointer. Stores the "active" entry chart used in fnSetToChartEntry()

-- |[Location]|
JournalEntry.sUnlockedVar = "TRUE"                      --DLPath to a variable, should be a number. If the value is 1, the entry can appear in the journal. if the path is "TRUE" always displays.

-- |[Paragon]|
JournalEntry.sParagonGroup      = "Null"                --Paragon grouping name which is used in DLPaths related to the given group.
JournalEntry.bParagonEnabled    = false                 --If true, the paragons will spawn. Player can turn them off if desired.
JournalEntry.iKOCurrent         = 0                     --Number of enemies defeated in this area.
JournalEntry.iKORequired        = 1                     --Number of enemies the player must defeat to spawn the activator.
JournalEntry.bDefeatedActivator = false                 --The activator is a unique enemy, once defeated, other enemies in the area will spawn as paragons.
JournalEntry.sEnabledPath       = "Null"                --DLPath to the variable that tracks if a group is enabled.

-- |[Profile]|
--Has no unique variables.

-- |[Quests]|
JournalEntry.sQuestObjective = ""                       --The "Objective" that appears under the quest name.

-- |[ ============ Class Methods ============= ]|
--System
function JournalEntry:new(psName, psDisplayName) end

--Common
function JournalEntry:fnSetDisplayName(psDisplayName)                     end
function JournalEntry:fnSetPortrait(psPortraitPath, pfOffsetX, pfOffsetY) end
function JournalEntry:fnSetDescription(psDescription)                     end
function JournalEntry:fnUploadData()                                      end
function JournalEntry:fnCleanGlobals() end

--Bestiary
function JournalEntry:fnCreateBestiaryEntry(pzaEnemyStatChart, psInternalName, psChartName, pbAlwaysShow, psDisplayName, psPortrait, psParagon, pfPortraitX, pfPortraitY, psTurnIco, pbCanTFPlayer, psaDescriptionLines) end
function JournalEntry:fnSetToChartEntry(psChartName, pbAlwaysShow, pfPortraitX, pfPortraitY, pbCanTFPlayer) end

--List Handling
function JournalEntry:fnEntryExists(psName, pzaListing)                end --Static
function JournalEntry:fnLocateEntry(psName, pzaListing)                end --Static
function JournalEntry:fnLocateEnemyDataOnStatChart(psName, pzaListing) end --Static

--Location
function JournalEntry:fnSetToLocation(psImagePath, pfOffsetX, pfOffsetY)                                                                            end
function JournalEntry:fnSetVariable(psDLPath)                                                                                                       end
function JournalEntry:fnCreateProgramLocationEntry(psInternalName, psDisplayName, psVariablePath, psImagePath, psOffsetX, pfOffsetY, psDescription) end --Static

--Paragon
function JournalEntry:newParagon(psInternalName, psDisplayName, psGroupName)                                                                               end --Static
function JournalEntry:fnCreateProgramParagonEntry(psInternalName, psDisplayName, pbIsEnabled, piKOCurrent, piKONeeded, pbActivatorDefeated, psEnabledPath) end --Static

--Profiles
function JournalEntry:fnSetToProfile(psImagePath, pfOffX, pfOffY)                                                             end
function JournalEntry:fnBlankProfile()                                                                                        end
function JournalEntry:fnCreateJournalProfile(psInternalName, psDisplayName, psImagePath, pfOffsetX, pfOffsetY, psDescription) end --Static

--Quest
function JournalEntry:fnSetToQuest(psObjective)                                                     end
function JournalEntry:fnCreateQuestEntry(psInternalName, psDisplayName, psObjective, psDescription) end --Static
function JournalEntry:fnBlankQuest()                                                                end

-- |[ ========== Class Constructor =========== ]|
--Must have a unique name for the map in question.
function JournalEntry:new(psName, psDisplayName)
    
    -- |[ ========= Setup ========== ]|
    -- |[Argument Check]|
    if(psName        == nil) then psName        = "Default" end
    if(psDisplayName == nil) then psDisplayName = psName end
    
    -- |[Create]|
    local zObject = {}
    setmetatable(zObject, JournalEntry)
    
    -- |[ ======== Members ========= ]|
    --Local member.
    zObject.sLocalName = psName
    zObject.sDisplayName = psDisplayName
    
    --Reset local lists so they don't use the global class listings.
    --zObject.zaWarpDestinations = {}
    
    -- |[ ======= Finish Up ======== ]|
    --Return object.
    return zObject
end

-- |[ ========= Call Implementations ========= ]|
LM_ExecuteScript(fnResolvePath() .. "JournalEntryBestiary.lua")
LM_ExecuteScript(fnResolvePath() .. "JournalEntryCommon.lua")
LM_ExecuteScript(fnResolvePath() .. "JournalEntryListHandling.lua")
LM_ExecuteScript(fnResolvePath() .. "JournalEntryLocation.lua")
LM_ExecuteScript(fnResolvePath() .. "JournalEntryParagon.lua")
LM_ExecuteScript(fnResolvePath() .. "JournalEntryProfile.lua")
LM_ExecuteScript(fnResolvePath() .. "JournalEntryQuest.lua")
