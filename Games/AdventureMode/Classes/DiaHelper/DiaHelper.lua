-- |[ ================================== DiaHelper Class File ================================== ]|
--Class file for DiaHelper. Call this once at initialization.

--Dialogue helper functions. Has subroutines for things like automatically positioning characters
-- on the dialogue stage, loading actors, setting emotions, and more!

-- |[ =========== Globals/Statics ============ ]|
-- |[ ============ Class Members ============= ]|
-- |[System]|
DiaHelper = {}
DiaHelper.__index = DiaHelper

-- |[Actor Worker]|
DiaHelper.sActorName              = ""
DiaHelper.sActorVoice             = ""
DiaHelper.saActorAliasList        = {}
DiaHelper.saActorAliasDisplayList = {}
DiaHelper.saActorEmotionList      = {}

-- |[Topic Worker]|
DiaHelper.sActiveTopicName = ""

-- |[ ============ Class Methods ============= ]|
--System
--Actors
function DiaHelper:fnActorReset()                                                                                                                 end
function DiaHelper:fnActorSetName(psName)                                                                                                         end
function DiaHelper:fnActorSetVoice(psVoice)                                                                                                       end
function DiaHelper:fnActorAddAlias(psAlias, psDisplayName)                                                                                        end
function DiaHelper:fnActorSetAliases(psaAliasList, psaDisplayList)                                                                                end
function DiaHelper:fnActorAddEmotion(psEmotion, psImagePath, pbIsNormalWidth)                                                                     end
function DiaHelper:fnActorSetEmotions(pzaEmotionList)                                                                                             end
function DiaHelper:fnActorUploadAndClear()                                                                                                        end
function DiaHelper:fnCreateSimpleActor(psActorName, psVoiceName, psPathPattern, pbIsNormalWidth, psaAliasList, psaEmotionList, psaAliasRemapList) end

--Topics
function DiaHelper:fnGetTopicState(psTopicName, psCharacterName)                                                       end
function DiaHelper:fnSetActiveTopicNPCName(psActiveName)                                                               end
function DiaHelper:fnConstructTopic(sTopicInternalName, sTopicDisplayName, iTopicStartLevel, sNPCName, iNPCTopicLevel) end
function DiaHelper:fnAutoConstructTopic(bIsUnlocked, sTopicInternalName, sTopicDisplayName)                            end

-- |[ ========== Class Constructor =========== ]|
--Singleton, class should not be instantiated.

-- |[ ========= Call Implementations ========= ]|
LM_ExecuteScript(fnResolvePath() .. "DiaHelperActors.lua")
LM_ExecuteScript(fnResolvePath() .. "DiaHelperTopics.lua")
