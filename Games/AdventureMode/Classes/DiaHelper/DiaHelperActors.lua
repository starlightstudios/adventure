-- |[ ==================================== DiaHelper Actors ==================================== ]|
--Actor functions, based largely around constructing and registering actors. Actors are the images
-- that appear in dialogue, having names, aliases, and voice ticks that play when they speak.
--Actors are constructed as objects held inside the DiaHelper class. Once all setup is done, call
-- fnActorUploadAndClear(), which will send all the information to the C++ state and clear the data
-- so it doesn't accidentally overwrite anything.

-- |[ ================================ DiaHelper:fnActorReset() ================================ ]|
--Clears are resets all internal class actor variables.
function DiaHelper:fnActorReset()
    
    -- |[Clear]|
    self.sActorName              = ""
    self.sActorVoice             = ""
    self.saActorAliasList        = {}
    self.saActorAliasDisplayList = {}
    self.saActorEmotionList      = {}
end

-- |[ =============================== DiaHelper:fnActorSetName() =============================== ]|
--Sets the actor's internal name. This also implicitly counts as an alias.
function DiaHelper:fnActorSetName(psName)
    
    -- |[Error Check]|
    if(psName == nil) then return end
    
    -- |[Execution]|
    self.sActorName = psName
end

-- |[ ============================== DiaHelper:fnActorSetVoice() =============================== ]|
--Sets the actor's voice.
function DiaHelper:fnActorSetVoice(psVoice)
    
    -- |[Error Check]|
    --Can legally set the voice to nil!
    
    -- |[Execution]|
    self.sActorVoice = psVoice
end

-- |[ ============================== DiaHelper:fnActorAddAlias() =============================== ]|
--Adds an alias to the alias list. If the alias is already on the list, does nothing. psDisplayName 
-- is optional, it allows this alias to be displayed differently. This is used exclusively for 
-- translations. If not provided then the display name is the alias.
--The display name is not checked for duplicates and can legally be a duplicate of another alias.
function DiaHelper:fnActorAddAlias(psAlias, psDisplayName)
    
    -- |[Error Check]|
    if(psAlias       == nil) then return end
    if(psDisplayName == nil) then psDisplayName = psAlias end
    
    -- |[Duplicate Check]|
    --Scan the list. If the alias is already on the list, do nothing.
    for i = 1, #self.saActorAliasList, 1 do
        if(self.saActorAliasList[i] == psAlias) then return end
    end
    
    -- |[Add]|
    table.insert(self.saActorAliasList,        psAlias)
    table.insert(self.saActorAliasDisplayList, psDisplayName)
end

-- |[ ============================= DiaHelper:fnActorSetAliases() ============================== ]|
--Given a list of aliases, appends them all. This implicitly does duplicate checks for each name,
-- if the same alias appears twice in the list it will only be added once.
--As above, if display names are provided then those are added, but the psaDisplayList is optional.
-- If not provided, the alias list is used. If the lists are a different length, any missing display
-- names default to being aliases.
function DiaHelper:fnActorSetAliases(psaAliasList, psaDisplayList)
    
    -- |[Error Check]|
    if(psaAliasList   == nil) then return end
    if(psaDisplayList == nil) then psaDisplayList = {} end
    
    --Display list nominally needs to be the same length as the alias list. If it isn't, replace
    -- missing elements on the display list.
    if(#psaDisplayList < #psaAliasList) then
        for i = 1, #psaAliasList, 1 do
            if(#psaDisplayList < i) then
                table.insert(psaDisplayList, psaAliasList[i])
            end
        end
    end
    
    -- |[Execution]|
    for i = 1, #psaAliasList, 1 do
        self:fnActorAddAlias(psaAliasList[i], psaDisplayList[i])
    end
end

-- |[ ============================= DiaHelper:fnActorAddEmotion() ============================== ]|
--Adds an emotion to the actor, creating a table for it. Emotions require a name and an associated
-- image, and a flag indicating if the emotion is wider than normal. Duplicates are ignored.
function DiaHelper:fnActorAddEmotion(psEmotion, psImagePath, pbIsNormalWidth)
    
    -- |[Error Check]|
    if(psEmotion       == nil) then return end
    if(psImagePath     == nil) then return end
    if(pbIsNormalWidth == nil) then return end
    
    -- |[Duplicate Check]|
    --Scan the list, if the names match do nothing.
    for i = 1, #self.saActorEmotionList, 1 do
        if(self.saActorEmotionList[i].sName == psEmotion) then return end
    end
    
    -- |[Create, Register]|
    --Create package.
    local zEmotion = {}
    zEmotion.sName = psEmotion
    zEmotion.sPath = psImagePath
    zEmotion.bIsNormalWid = pbIsNormalWidth
    
    --Register.
    table.insert(self.saActorEmotionList, zEmotion)
end

-- |[ ============================= DiaHelper:fnActorSetEmotions() ============================= ]|
--Adds emotions from a table to the actor. Implicitly does duplicate checks.
function DiaHelper:fnActorSetEmotions(pzaEmotionList)
    
    -- |[Error Check]|
    if(pzaEmotionList == nil) then return end
    
    -- |[Execution]|
    for i = 1, #pzaEmotionList, 1 do
        self:fnActorAddEmotion(pzaEmotionList[i].sName, pzaEmotionList[i].sPath, pzaEmotionList[i].bIsNormalWid)
    end
end

-- |[ =========================== DiaHelper:fnActorUploadAndClear() ============================ ]|
--Once all other setup is done, uploads data to the C++ state and clears it off to prevent accidental
-- re-use in a future actor.
function DiaHelper:fnActorUploadAndClear()
    
    -- |[Diagnostics]|
    local bDiagnostics = fnAutoCheckDebugFlag("DiaHelper: Actors")
    Debug_PushPrint(bDiagnostics, "DiaHelper:fnActorUploadAndClear() - Begin.\n")
    Debug_Print("Performing error checks.\n")

    -- |[Error Checks]|
    --Actor must have a name.
    if(self.sActorName == nil or self.sActorName == "") then
        io.write("DiaHelper:fnActorUploadAndClear() - Error. No Actor Name set. Failing.\n")
        self:fnActorReset()
        return
    end
    
    --If an actor does not have a voice set, then nothing occurs. This is not an error, actors can
    -- legally not have an associated voice. This is the case if an actor appears but never
    -- speaks, or if the actor uses some other method of speaking and doesn't need a tick.
    
    --An actor can legally have no aliases listed. This means the actor only lights up when its name
    -- is used.
    
    --Actor must have at least one emotion registered. It is an error if none are registered.
    if(self.saActorEmotionList == nil or #self.saActorEmotionList < 1) then
        io.write("DiaHelper:fnActorUploadAndClear() - Error. No emotions set for  " .. self.sActorName .. "\n. Failing.n")
        self.sActorVoice = "Voice|Narrator"
    end
    
    -- |[Diagnostic Report]|
    Debug_Print("Error checks passed. Report:\n")
    Debug_Print(" Actor name is: " .. self.sActorName .. "\n")
    Debug_Print(" Actor voice is: " .. self.sActorVoice .. "\n")
    Debug_Print(" Alias count: " .. #self.saActorAliasList .. "\n")
    Debug_Print(" Emotion count: " .. #self.saActorEmotionList .. "\n")
    if(#self.saActorAliasList > 0) then
        Debug_Print(" Listing aliases.\n")
        for i = 1, #self.saActorAliasList, 1 do
            Debug_Print(self.saActorAliasList[i] .. " ")
        end
        Debug_Print("\n")
    end
    if(#self.saActorEmotionList > 0) then
        Debug_Print(" Listing emotions.\n")
        for i = 1, #self.saActorEmotionList, 1 do
            Debug_Print(self.saActorEmotionList[i].sName .. " x " .. self.saActorEmotionList[i].sPath .. "\n")
        end
    end
    
    -- |[Uploading]|
    --All checks passed. Begin uploading data to the C++ state. First, create the actor if it does not already exist.
    -- It is legal to call this even if the actor exists, it will be pushed to the activity stack.
    Debug_Print("Uploading data to C++ state.\n")
    DialogueActor_Create(self.sActorName)
    
    --Set the voice. This can technically be done independently of the actual DialogueActor object. If the voice
    -- already exists, it is overwritten.
    if(self.sActorVoice ~= nil and self.sActorVoice ~= "") then
        WD_SetProperty("Register Voice", self.sActorName, self.sActorVoice)
    end
    
    --Add aliases with display names.
    for i = 1, #self.saActorAliasList, 1 do
        DialogueActor_SetProperty("Add Alias", self.saActorAliasList[i], self.saActorAliasDisplayList[i])
    end
    
    --Add emotions. If an emotion already exists, its path can be overwritten. Costumes use this.
    for i = 1, #self.saActorEmotionList, 1 do
    
        --Fast-access variables.
        local sEmotionName = self.saActorEmotionList[i].sName
        local sEmotionPath = self.saActorEmotionList[i].sPath
        local sEmotionFlag = self.saActorEmotionList[i].bIsNormalWid
    
        --Execute.
        DialogueActor_SetProperty("Add Emotion", sEmotionName, sEmotionPath, sEmotionFlag)
    end
    
    -- |[Clear]|
    --Pop the activity stack.
    DL_PopActiveObject()
    
    --Clear all lingering data.
    Debug_Print("Clearing.\n")
    self:fnActorReset()
    
    --Diagnostics.
    Debug_PopPrint("Finished.\n")
end

-- |[ ============================ DiaHelper:fnCreateSimpleActor() ============================= ]|
--Does all the work of creating an actor in a single line. Used for actors that don't have a lot
-- of complex emotions requiring multiple sizing flags. Non-party actors and one-off enemy actors
-- are good candidates for this.
--psVoiceName is not optional, but can legally be "". It cannot be nil!
--psaAliasRemapList is optional, if not provided no alias display names are used.
function DiaHelper:fnCreateSimpleActor(psActorName, psVoiceName, psPathPattern, pbIsNormalWidth, psaAliasList, psaEmotionList, psaAliasRemapList)
    
    -- |[Error Check]|
    if(psActorName     == nil) then return end
    if(psVoiceName     == nil) then return end
    if(psPathPattern   == nil) then return end
    if(pbIsNormalWidth == nil) then return end
    if(psaAliasList    == nil) then return end
    if(psaEmotionList  == nil) then return end
    
    --The emotion list must contain at least one emotion or the object is invalid and barks a warning.
    if(#psaEmotionList < 1) then
        io.write("DiaHelper:fnCreateSimpleActor() - Error. Emotion list contains no elements. If an actor only has 'Neutral' then that must still be provided. Failing.\n")
        return
    end
    
    -- |[Execution]|
    --Basic variables.
    self:fnActorSetName(psActorName)
    self:fnActorSetVoice(psVoiceName)
    
    --Aliases. Can be set directly from provided lists. If no psaAliasRemapList is found, then aliases are not remapped.
    self:fnActorSetAliases(psaAliasList, psaAliasRemapList)
    
    -- |[Emotion Handling]|
    --Emotion processing. First, a special case: If there is exactly one emotion, and it's "NEUTRALISPATH" then the
    -- psPathPattern is the path to the neutral emotion.
    if(#psaEmotionList == 1 and psaEmotionList[1] == "NEUTRALISPATH") then
        self:fnActorAddEmotion("Neutral", psPathPattern, pbIsNormalWidth)
        
    --Otherwise, each emotion is considered to be located at "psPathPattern .. sEmotionName". For example, if the path is
    -- "Root/Images/Portraits/Kona/" and the emotion is "Happy", then the final path is "Root/Images/Portraits/Kona/Happy".
    -- All emotions are assumed to use the same pbIsNormalWidth flag.
    else
        for i = 1, #psaEmotionList, 1 do
            self:fnActorAddEmotion(psaEmotionList[i], psPathPattern .. psaEmotionList[i], pbIsNormalWidth)
        end
    end
    
    -- |[Upload]|
    --Upload data to the C++ state.
    self:fnActorUploadAndClear()
end
