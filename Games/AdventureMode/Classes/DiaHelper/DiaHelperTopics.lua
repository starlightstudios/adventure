-- |[ ==================================== DiaHelper Topics ==================================== ]|
--Topics functions for registering topics.

-- |[ ============================== DiaHelper:fnGetTopicState() =============================== ]|
--Returns an integer indicating the current topic level for the given topic.
function DiaHelper:fnGetTopicState(psTopicName, psCharacterName)
    
	-- |[Argument Check]|
    if(psTopicName     == nil) then return 0 end
    if(psCharacterName == nil) then return 0 end
    
	-- |[Execution]|
    --Iterate across all topics.
    local iTopics = WD_GetProperty("Total Topics")
    for i = 0, iTopics-1, 1 do
        
        --Check if the topic matches:
        if(WD_GetProperty("Topic Name", i) == psTopicName) then
            
            --Iterate across all NPCs in this topic:
            local iNPCs = WD_GetProperty("Topic NPCs Total", i)
            for p = 0, iNPCs-1, 1 do
                
                --Check if this NPC name matches:
                if(WD_GetProperty("Topic NPC Name", i, p) == psCharacterName) then
                    
                    --Return the topic level.
                    return WD_GetProperty("Topic NPC Level", i, p)
                end
            end
        end
    end

    --Not found.
    return 0
end

-- |[ ========================== DiaHelper:fnSetActiveTopicNPCName() =========================== ]|
--Sets the name of the active character for automated topic construction.
function DiaHelper:fnSetActiveTopicNPCName(psActiveName)
	
	-- |[Nil Check]|
	if(psActiveName == nil) then psActiveName = "" end
	
	-- |[Set]|
	self.sActiveTopicName = psActiveName
	
end

-- |[ ============================== DiaHelper:fnConstructTopic() ============================== ]|
--Constructs a topic and immediately registers the given NPC for that topic. Essentially combines two 
-- lines into one.
function DiaHelper:fnConstructTopic(sTopicInternalName, sTopicDisplayName, iTopicStartLevel, sNPCName, iNPCTopicLevel)

	-- |[Argument Check]|
	if(sTopicInternalName == nil) then return end
	if(sTopicDisplayName  == nil) then return end
	if(iTopicStartLevel   == nil) then return end
	if(sNPCName           == nil) then return end
	if(iNPCTopicLevel     == nil) then return end

	-- |[Diagnostics]|
	--Debug: Builds a list of all unique topics currently in the game.
	if(gsaUniqueTopics ~= nil) then
		
		--Loop across the existing topics. If any match this new one, stop.
		local bFoundCopy = false
		local i = 1
		while(gsaUniqueTopics[i] ~= nil) do
			if(gsaUniqueTopics[i] == sTopicInternalName) then
				bFoundCopy = true
				break
			end
			
			i = i + 1
		end
		
		--If no copy was found, add it.
		if(bFoundCopy == false) then
			gsaUniqueTopics[i] = sTopicInternalName
		end
	end
    
	-- |[Execution]|
    --Handle translation.
    local sDisplayName = Translate(gsTranslationUI, sTopicDisplayName)

    --Upload to C++ state.
	WD_SetProperty("Register Topic", sTopicInternalName, sDisplayName, iTopicStartLevel)
	WD_SetProperty("Register Topic For", sTopicInternalName, sNPCName, iNPCTopicLevel)

end

-- |[ ============================ DiaHelper:fnAutoConstructTopic() ============================ ]|
--Shorthand Function. If the display name is not provided, it uses the internal name.
function DiaHelper:fnAutoConstructTopic(bIsUnlocked, sTopicInternalName, sTopicDisplayName)

	-- |[Error Checks]|
    --Arg check. Internal must be provided.
    if(bIsUnlocked        == nil) then return end
    if(sTopicInternalName == nil) then return end
    
    --If the topic display name isn't provided, it's identical to the internal name.
    if(sTopicDisplayName == nil) then sTopicDisplayName = sTopicInternalName end
    
    --If the active NPC name is not set, fail here.
    if(DiaHelper.sActiveTopicName == "") then return end

	-- |[Diagnostics]|
	--Debug: Builds a list of all unique topics currently in the game.
	if(gsaUniqueTopics ~= nil) then
		
		--Loop across the existing topics. If any match this new one, stop.
		local bFoundCopy = false
		local i = 1
		while(gsaUniqueTopics[i] ~= nil) do
			if(gsaUniqueTopics[i] == sTopicInternalName) then
				bFoundCopy = true
				break
			end
			
			i = i + 1
		end
		
		--If no copy was found, add it.
		if(bFoundCopy == false) then
			gsaUniqueTopics[i] = sTopicInternalName
		end
	end

	-- |[Execution]|
    local iUnlockValue = -1
    if(bIsUnlocked) then iUnlockValue = 1 end
    
    --Handle translation.
    local sDisplayName = Translate(gsTranslationUI, sTopicDisplayName)

    --Upload to C++ state.
	WD_SetProperty("Register Topic", sTopicInternalName, sDisplayName, iUnlockValue)
	WD_SetProperty("Register Topic For", sTopicInternalName, DiaHelper.sActiveTopicName, 0)

end