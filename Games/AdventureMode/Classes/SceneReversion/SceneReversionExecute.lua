-- |[ ================================= SceneReversion Execute ================================= ]|
--Execution function called during normal operations.

-- |[ =============================== SceneReversion:fnExecute() =============================== ]|
--Static function, executes the reversion cutscene.
function SceneReversion:fnExecute()
    
    -- |[ ============== Setup =============== ]|
    -- |[Debug]|
    local bDiagnostics = Debug_GetFlag("Reversion: All")
    Debug_PushPrint(bDiagnostics, "SceneReversion:fnExecute() - Begin.\n")
    
    -- |[Variables]|
    local bAtLeastOneRevert = false
    local saPartyList       = {gsPartyLeaderName}
    local baMustTransform   = {false}
    
    -- |[Clear]|
    SceneReversion.saRevertFormStore = {}
    
    -- |[List Assembly]|
    --Append the followers to the list.
    for i = 1, giFollowersTotal, 1 do
        table.insert(saPartyList, gsaFollowerNames[i])
        table.insert(baMustTransform, false)
    end

    --Check all party members to see if they revert.
    for i = 1, #saPartyList, 1 do
        
        --Storage.
        local bMustRevert                   = SceneReversion:fnCheckMustTransform(saPartyList[i])
        SceneReversion.saRevertFormStore[i] = SceneReversion:fnGetCurrentForm(saPartyList[i])
        
        --Must revert, trip flags.
        if(bMustRevert == true) then
            bAtLeastOneRevert = true
            baMustTransform[i] = true
        end
    end
    
    --Report.
    if(bDiagnostics) then
        Debug_Print("Party size: " .. #saPartyList .. "\n")
        for i = 1, #saPartyList, 1 do
            Debug_Print(" Name: " .. saPartyList[i] .. "\n")
            Debug_Print(" Form: " .. SceneReversion.saRevertFormStore[i] .. "\n")
            if(baMustTransform[i] == true) then
                Debug_Print(" Must transform.\n")
            else
                Debug_Print(" Doesn't transform.\n")
            end
        end
    end

    -- |[ ============ Execution ============= ]|
    -- |[Debug]|
    Debug_Print("Ordering crouch/wound sequence.\n")
    
    -- |[Crouch/Wound]|
    --Knock down all the party members.
    for i = 1, #saPartyList, 1 do
        fnCutsceneSetFrame(saPartyList[i], "Crouch")
    end

    --Wait a few ticks.
    fnCutsceneWait(5)
    fnCutsceneBlocker()

    --Wound all party members.
    for i = 1, #saPartyList, 1 do
        fnCutsceneSetFrame(saPartyList[i], "Wounded")
    end
    
    -- |[Reversion]|
    --If flagged, call a reversion sequence for all party members that need it.
    if(bAtLeastOneRevert == true) then
    
        --Debug.
        Debug_Print("Executing reversion sequence.\n")
    
        --Iterate, order a flashwhite event for all reverting members.
        for i = 1, #saPartyList, 1 do
            if(baMustTransform[i] == true) then
                Cutscene_CreateEvent("Flash Character White", "Actor")
                    ActorEvent_SetProperty("Subject Name", saPartyList[i])
                    ActorEvent_SetProperty("Flashwhite Quickly", "Wounded")
                DL_PopActiveObject()
            end
        end
    
        --Wait a bit for the flash to occur.
        fnCutsceneWait(15)
        fnCutsceneBlocker()
        
        --Order all flashing members to execute the form change script here.
        for i = 1, #saPartyList, 1 do
            if(baMustTransform[i] == true) then
                SceneReversion:fnCallFormRevertHandler(saPartyList[i])
            end
        end
        
        --Wait for the flash to finish.
        fnCutsceneWait(gci_Flashwhite_Ticks_Total)
        fnCutsceneBlocker()
    
    --No reversion, just wait a few ticks.
    else
        --Debug.
        Debug_Print("Skipping reversion sequence.\n")
    
        --Wait.
        fnCutsceneWait(15)
        fnCutsceneBlocker()
    end

    --Wait a bit.
    fnCutsceneWait(30)
    fnCutsceneBlocker()

    -- |[Fade Out]|
    --Debug.
    Debug_Print("Fading out.\n")
    
    --Fade to black.
    fnCutscene([[ AL_SetProperty("Activate Fade", 75, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneWait(75)
    fnCutsceneBlocker()

    --Wait a bit.
    fnCutsceneWait(30)
    fnCutsceneBlocker()
    
    --At this point, the calling script should perform whatever other events needed, usually changing
    -- the active map or repositioning the party.
    Debug_PopPrint("Finished.\n")
end
