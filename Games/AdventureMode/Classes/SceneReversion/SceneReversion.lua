-- |[ =============================== SceneReversion Class File ================================ ]|
--Class file for SceneReversion. Call this once at initialization.

--A class meant to store information related to the act of "reversion" on the field. After the player
-- loses a battle, a short cutscene plays showing the party falling down, and any runebearers
-- transforming back to human. This is actually a fairly complicated sequence as it involves a
-- number of variables such as who should transform and when (Christine does, but not during Cryogenics!)
-- so the variables and handling are encapsulated in this class.

--This class is a static singleton and does not need to be instantiated to be used.

-- |[ =========== Globals/Statics ============ ]|
-- |[Type RevertEntry]|
--Used to store a reversion case. Mei, for example, does not revert when human. Christine does not when human *and male*.
--zRevertEntry = {}
--zRevertEntry.sName                            --Name of the character. Must be the field entity name.
--zRevertEntry.sFormVar                         --DLPath to the variable used to track the form.
--zRevertEntry.sRevertCall                      --Script to call to revert back to the base form.
--zRevertEntry.saExceptions                     --List of strings. {"Human", "Male"}. If the character is a form in the list, does not revert.
--zRevertEntry.saFormMap                        --List of pairs. {{"FormName", CallPath.lua}, {}}, used for transforming after the revert scene ends.

-- |[ ============ Class Members ============= ]|
-- |[System]|
SceneReversion = {}

-- |[Variables]|
--Stores a list of all characters (runebearers) who, when defeated, revert to another form (usually human). This list is
-- populated in SceneReversionInitialize.lua
SceneReversion.csaRevertList = {}

--During a standard defeat sequence, the party wakes up at the last save point they used.
-- These variables track which forms the characters were in. If they disagree with the 
-- current form (runebearers revert to human when defeated) then the runebearer transforms.
SceneReversion.saRevertFormStore = {}

-- |[ ============ Class Methods ============= ]|
--System
--Execution
function SceneReversion:fnExecute() end

--Initialize
--Post-Exec
function SceneReversion:fnSavePointRearrange() end
function SceneReversion:fnPartyStandUp()       end
function SceneReversion:fnRetransform()        end
function SceneReversion:fnSavePointFold()      end

--Utility
function SceneReversion:fnGetCurrentForm(psName)                        end
function SceneReversion:fnGetTransformPath(psCharacterName, psFormName) end
function SceneReversion:fnCheckMustTransform(psName)                    end
function SceneReversion:fnCallFormRevertHandler(psName)                 end

-- |[ ========== Class Constructor =========== ]|
--This class is a static singleton and does not need to be instantiated.

-- |[ ========= Call Implementations ========= ]|
LM_ExecuteScript(fnResolvePath() .. "SceneReversionExecute.lua")
LM_ExecuteScript(fnResolvePath() .. "SceneReversionInitialize.lua")
LM_ExecuteScript(fnResolvePath() .. "SceneReversionPostExec.lua")
LM_ExecuteScript(fnResolvePath() .. "SceneReversionUtility.lua")
