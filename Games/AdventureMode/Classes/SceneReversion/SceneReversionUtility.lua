-- |[ ================================= SceneReversion Utility ================================= ]|
--Utility functions to run queries and sanity-check the data used during scene-reversion.

-- |[ =========================== SceneReversion:fnGetCurrentForm() ============================ ]|
--Returns the current form for the given character. Used for storage. If a character does not revert,
-- returns "Null".
function SceneReversion:fnGetCurrentForm(psName)
    
    -- |[Argument Check]|
    if(psName == nil) then return "Null" end
    
    -- |[List Check]|
    --If the entity is not on the revert list at all, return "Null".
    local zRevertEntry = nil
    for i = 1, #SceneReversion.csaRevertList, 1 do
        if(psName == SceneReversion.csaRevertList[i].sName) then
            zRevertEntry = SceneReversion.csaRevertList[i]
            break
        end
    end
    
    --Not on the list.
    if(zRevertEntry == nil) then return "Null" end
    
    -- |[Return Form]|
    local sCurrentForm = VM_GetVar(zRevertEntry.sFormVar, "S")
    return sCurrentForm
    
end

-- |[ ========================== SceneReversion:fnGetTransformPath() =========================== ]|
--Given a character and the form that is desired, resolves and returns the path that should be called
-- during the flashwhite section to transform them back to their original form pre-reversion.
--Returns "Null" if not found.
function SceneReversion:fnGetTransformPath(psCharacterName, psFormName)
    
    -- |[Argument Check]|
    if(psCharacterName == nil) then return "Null" end
    if(psFormName      == nil) then return "Null" end
    
    -- |[Diagnostics]|
    local bDiagnostics = Debug_GetFlag("Reversion: All")
    Debug_PushPrint(bDiagnostics, "SceneReversion:fnGetTransformPath() - Begin.\n")
    Debug_Print("Character Name: " .. psCharacterName .. "\n")
    Debug_Print("Form Name: " .. psFormName .. "\n")

    -- |[Locate Character]|
    --If the entity is not on the revert list at all, return "Null".
    local zRevertEntry = nil
    for i = 1, #SceneReversion.csaRevertList, 1 do
        if(psCharacterName == SceneReversion.csaRevertList[i].sName) then
            zRevertEntry = SceneReversion.csaRevertList[i]
            break
        end
    end
    
    --Not found.
    if(zRevertEntry == nil) then
        Debug_PopPrint("Finished. Character is not on reversion list. Returning Null.\n")
        return "Null"
    end

    -- |[Resolve Path from Form]|
    --Debug.
    Debug_Print("Size of form remapping list: " .. #zRevertEntry.saFormMap .. "\n")
    
    --Scan the character's form listing to find the path.
    for i = 1, #zRevertEntry.saFormMap, 1 do
        
        --Debug.
        Debug_Print("Check form " .. zRevertEntry.saFormMap[i][1] .. "\n")
        
        --Check.
        if(zRevertEntry.saFormMap[i][1] == psFormName) then
            Debug_PopPrint("Finished. Character has match for " .. psFormName .. " in slot " .. i .. "\n")
            return zRevertEntry.saFormMap[i][2]
        end
    end

    --Not found. Return "Null".
    Debug_PopPrint("Finished. Character found but form not found, returning Null.\n")
    return "Null"
end
    
-- |[ ========================= SceneReversion:fnCheckMustTransform() ========================== ]|
--Scans the given character's name against the reversion list. If found, returns true. If not, false.
-- Also checks if the character is in a form that must revert, usually non-human.
function SceneReversion:fnCheckMustTransform(psName)
    
    -- |[Argument Check]|
    if(psName == nil) then return false end
    
    -- |[List Check]|
    --If the entity is not on the revert list at all, false.
    local zRevertEntry = nil
    for i = 1, #SceneReversion.csaRevertList, 1 do
        if(psName == SceneReversion.csaRevertList[i].sName) then
            zRevertEntry = SceneReversion.csaRevertList[i]
            break
        end
    end
    
    --Not on the list.
    if(zRevertEntry == nil) then return false end
    
    -- |[Form-Appropriate Check]|
    --If a character is on the revert list, but is in a form that doesn't revert (usually human) then
    -- don't revert. If the value comes back 0 then it was not found, fail.
    local sCurrentForm = VM_GetVar(zRevertEntry.sFormVar, "S")
    if(sCurrentForm == 0.0) then return false end
    
    --Scan.
    for i = 1, #zRevertEntry.saExceptions, 1 do
        if(sCurrentForm == zRevertEntry.saExceptions[i]) then
            return false
        end
    end
    
    -- |[All Checks Passed]|
    return true
end

-- |[ ======================== SceneReversion:fnCallFormRevertHandler() ======================== ]|
--Given a character name, calls their form revert script. Mostly here to prevent the above code
-- from getting really long due to the requirements of a cutscene handler call.
function SceneReversion:fnCallFormRevertHandler(psName)
    
    -- |[Argument Check]|
    if(psName == nil) then return end
    
    -- |[List Check]|
    --Debug.
    Debug_Print("SceneReversion:fnCallFormRevertHandler() - Checking character: " .. psName .. "\n")
    
    --If the entity is not on the revert list at all, false.
    local zRevertEntry = nil
    for i = 1, #SceneReversion.csaRevertList, 1 do
        if(psName == SceneReversion.csaRevertList[i].sName) then
            zRevertEntry = SceneReversion.csaRevertList[i]
            break
        end
    end
    
    --Not on the list.
    if(zRevertEntry == nil) then return end
    
    --Call entry is "Null". This is used for placeholders, do nothing.
    if(zRevertEntry.sRevertCall == "Null") then return end
    
    -- |[Call]|
    Debug_Print("Calling: " .. zRevertEntry.sRevertCall .. "\n")
	fnCutsceneCall(zRevertEntry.sRevertCall)
    
end
