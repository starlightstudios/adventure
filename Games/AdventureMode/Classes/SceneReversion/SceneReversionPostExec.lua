-- |[ ================================ SceneReversion PostExec ================================= ]|
--Execution functions for the post-transition scene. These only get called if the player was defeated
-- and did not trigger a special scene. This scene constitutes the characters waking up at a save
-- point and usually having some dialogue.

-- |[ ========================= SceneReversion:fnSavePointRearrange() ========================== ]|
--Called after the save point level is loaded. Arranges the party around the save point. The leader
-- is always 1 tile north, second character is 1 east, third is 1 west, fourth is one south.
--By default, all characters spawn exactly on top of the party lead, so these are relative repositions.
function SceneReversion:fnSavePointRearrange()
    
    -- |[Common]|
    --Set leader to wounded. Happens even if there are no party members to rearrange.
    fnCutsceneSetFrame(gsPartyLeaderName, "Wounded")
    
    -- |[Activation Check]|
    --If there are zero followers, do nothing.
    if(giFollowersTotal < 1) then return end
    
    -- |[Setup]|
    --Store the position of the party leader. These are in world coordinates.
    EM_PushEntity(gsPartyLeaderName)
        local iLeaderX, iLeaderY = TA_GetProperty("Position")
    DL_PopActiveObject()
    
    --Convert to tile coordinates, as the teleport function uses those.
    local fLeaderTileX = iLeaderX / gciSizePerTile
    local fLeaderTileY = iLeaderY / gciSizePerTile
    
    -- |[Reposition Followers]|
    if(giFollowersTotal >= 1) then
        fnCutsceneTeleport(gsaFollowerNames[1], fLeaderTileX + 1.00, fLeaderTileY + 1.00)
        fnCutsceneFace(gsaFollowerNames[1], 0, 1)
        fnCutsceneSetFrame(gsaFollowerNames[1], "Wounded")
    end
    if(giFollowersTotal >= 2) then
        fnCutsceneTeleport(gsaFollowerNames[2], fLeaderTileX - 1.00, fLeaderTileY + 1.00)
        fnCutsceneFace(gsaFollowerNames[2], 0, 1)
        fnCutsceneSetFrame(gsaFollowerNames[2], "Wounded")
    end
    if(giFollowersTotal >= 3) then
        fnCutsceneTeleport(gsaFollowerNames[3], fLeaderTileX + 0.00, fLeaderTileY + 2.00)
        fnCutsceneFace(gsaFollowerNames[3], 0, 1)
        fnCutsceneSetFrame(gsaFollowerNames[3], "Wounded")
    end
end

-- |[ ============================ SceneReversion:fnPartyStandUp() ============================= ]|
--After fading in, the party stands up. They then turn to face the leader.
function SceneReversion:fnPartyStandUp()

    -- |[Assemble List]|
    --For many operations, all party members are handled the same.
    local saPartyList = {gsPartyLeaderName}
    for i = 1, giFollowersTotal, 1 do
        table.insert(saPartyList, gsaFollowerNames[i])
    end

    -- |[Crouch]|
    --Set party to crouch.
    for i = 1, #saPartyList, 1 do
        fnCutsceneSetFrame(saPartyList[i], "Crouch")
    end
    
    --Wait a bit.
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    -- |[Stand Up]|
    --Set to no special frame.
    for i = 1, #saPartyList, 1 do
        fnCutsceneSetFrame(saPartyList[i], "Null")
    end
    fnCutsceneBlocker()
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    
    -- |[Turn to Face]|
    --Party members quickly turn to face the leader. They start facing down. If there are no party members
    -- then this section does not execute and thus doesn't slow down execution.
    if(giFollowersTotal >= 1) then
        if(giFollowersTotal >= 1) then
            fnCutsceneFace(saPartyList[2], -1, 1)
        end
        if(giFollowersTotal >= 2) then
            fnCutsceneFace(saPartyList[3], 1, 1)
        end
        if(giFollowersTotal >= 3) then
            fnCutsceneFace(saPartyList[4], 1, 1)
        end
        fnCutsceneWait(5)
        fnCutsceneBlocker()
    
        --Segment two:
        if(giFollowersTotal >= 1) then
            fnCutsceneFace(saPartyList[2], -1, 0)
        end
        if(giFollowersTotal >= 2) then
            fnCutsceneFace(saPartyList[3], 1, 0)
        end
        if(giFollowersTotal >= 3) then
            fnCutsceneFace(saPartyList[4], 1, 0)
        end
        fnCutsceneWait(5)
        fnCutsceneBlocker()
        
        --Segment three:
        if(giFollowersTotal >= 1) then
            fnCutsceneFace(saPartyList[2], -1, -1)
        end
        if(giFollowersTotal >= 2) then
            fnCutsceneFace(saPartyList[3], 1, -1)
        end
        if(giFollowersTotal >= 3) then
            fnCutsceneFace(saPartyList[4], 1, -1)
        end
        fnCutsceneWait(5)
        fnCutsceneBlocker()
        
        --Segment four. Only happens if there are 3 followers.
        if(giFollowersTotal >= 3) then
            fnCutsceneFace(saPartyList[4], 0, -1)
            fnCutsceneWait(5)
            fnCutsceneBlocker()
        end
    end

end

-- |[ ============================= SceneReversion:fnRetransform() ============================= ]|
--Called after the party has woken up at a save point during a standard back-to-save action, causes
-- the party to stand up and any reverted characters to return to their forms.
--This may not always be called, depending on the scenes that may play out, or if a special scene
-- or TF is playing, in which case every stays human.
function SceneReversion:fnRetransform()

    -- |[Debug]|
    local bDiagnostics = Debug_GetFlag("Reversion: All")
    Debug_PushPrint(bDiagnostics, "SceneReversion:fnRetransform() - Begin.\n")
    
    -- |[Variables]|
    local bAtLeastOneTransform = false
    local saPartyList          = {gsPartyLeaderName}
    local saCalls              = {"Null"}
    
    -- |[List Assembly]|
    --Append the followers to the list.
    for i = 1, giFollowersTotal, 1 do
        table.insert(saPartyList, gsaFollowerNames[i])
        table.insert(saCalls, "Null")
    end
    
    -- |[Activation Check]|
    --Debug.
    Debug_Print("Scanning party of size: " .. #saPartyList .. "\n")
    
    --Scan the party. Check if a character a) reverts and b) is not the same form as they started.
    for i = 1, #saPartyList, 1 do
        
        --Get form.
        local sCurrentForm = SceneReversion:fnGetCurrentForm(saPartyList[i])
    
        --Debug.
        Debug_Print(" Character: " .. saPartyList[i] .. "\n")
        Debug_Print(" Form: " .. sCurrentForm .. "\n")
    
        --If a character does not revert at all, they will not be on the internal list and their form will resolve
        -- as "Null". Ignore them.
        if(sCurrentForm ~= "Null") then
            
            --Debug.
            Debug_Print("  Character is on revert list. Checking form against stored: " .. SceneReversion.saRevertFormStore[i] .. "\n")
    
            --Check the storage list. If they aren't the same, this character needs to transform.
            -- The party order needs to be the same between the two.
            if(SceneReversion.saRevertFormStore[i] ~= sCurrentForm) then
                
                --Debug.
                Debug_Print("   Forms are different.\n")
                
                --Get the expected path.
                saCalls[i] = SceneReversion:fnGetTransformPath(saPartyList[i], SceneReversion.saRevertFormStore[i])
                Debug_Print("   Transform call path is: " .. saCalls[i] .. "\n")
        
                --If the path came back "Null", then this may be a form that doesn't revert. If it's not null, flag
                -- at least one character must transform.
                if(saCalls[i] ~= "Null") then
                    bAtLeastOneTransform = true
                    Debug_Print("   Character will re-transform.\n")
                end
            end
        
        --Diagnostics.
        else
            Debug_Print("  Ignore this character.\n")
        end
    end
    
    --If this flag is false, then nobody needs to re-transform so do nothing.
    if(bAtLeastOneTransform == false) then
        Debug_PopPrint("Finished, nobody is re-transforming.\n")
        return
    end
    
    -- |[Order Flashwhites]|
    --Debug.
    Debug_Print("Ordering flashwhites.\n")
    
    --All transforming members flashwhite here.
    for i = 1, #saPartyList, 1 do
        if(saCalls[i] ~= "Null") then
            Cutscene_CreateEvent("Flash Character White", "Actor")
                ActorEvent_SetProperty("Subject Name", saPartyList[i])
                ActorEvent_SetProperty("Flashwhite Quickly", "Null")
            DL_PopActiveObject()
        end
    end

    --Wait a bit for the flash to occur.
    fnCutsceneWait(15)
    fnCutsceneBlocker()
    
    --Debug.
    Debug_Print("Ordering transformations.\n")
    
    --Order all flashing members to execute the form change script here.
    for i = 1, #saPartyList, 1 do
        if(saCalls[i] ~= "Null") then
            fnCutsceneCall(saCalls[i])
            Debug_Print("Character " .. saPartyList[i] .. " transforms. Call: " .. saCalls[i] .. "\n")
        end
    end
    
    --Wait for the flash to finish.
    fnCutsceneWait(gci_Flashwhite_Ticks_Total)
    fnCutsceneBlocker()
    
    --Debug.
    Debug_PopPrint("Finished.\n")
end

-- |[ ============================ SceneReversion:fnSavePointFold() ============================ ]|
--Called once dialogue and other activity is handled, party walks onto the party leader and folds.
function SceneReversion:fnSavePointFold()
    
    -- |[Activation Check]|
    --If there are zero followers, do nothing.
    if(giFollowersTotal < 1) then return end
    
    -- |[Setup]|
    --Store the position of the party leader. These are in world coordinates.
    EM_PushEntity(gsPartyLeaderName)
        local iLeaderX, iLeaderY = TA_GetProperty("Position")
    DL_PopActiveObject()
    
    --Convert to tile coordinates, as the teleport function uses those.
    local fLeaderTileX = iLeaderX / gciSizePerTile
    local fLeaderTileY = iLeaderY / gciSizePerTile
    
    -- |[Reposition Followers]|
    if(giFollowersTotal >= 1) then
        fnCutsceneMove(gsaFollowerNames[1], fLeaderTileX + 1.00, fLeaderTileY + 0.00)
        fnCutsceneMove(gsaFollowerNames[1], fLeaderTileX + 0.00, fLeaderTileY + 0.00)
    end
    if(giFollowersTotal >= 2) then
        fnCutsceneMove(gsaFollowerNames[2], fLeaderTileX - 1.00, fLeaderTileY + 0.00)
        fnCutsceneMove(gsaFollowerNames[2], fLeaderTileX + 0.00, fLeaderTileY + 0.00)
    end
    if(giFollowersTotal >= 3) then
        fnCutsceneMove(gsaFollowerNames[3], fLeaderTileX + 1.00, fLeaderTileY + 2.00)
        fnCutsceneMove(gsaFollowerNames[3], fLeaderTileX + 1.00, fLeaderTileY + 0.00)
        fnCutsceneMove(gsaFollowerNames[3], fLeaderTileX + 0.00, fLeaderTileY + 0.00)
    end
	fnCutsceneBlocker()
    
    -- |[Fold]|
	fnCutscene([[ AL_SetProperty("Fold Party") ]])
	fnCutsceneBlocker()
end
