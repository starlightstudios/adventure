-- |[ =============================== SceneReversion Initialize ================================ ]|
--Called after the SceneReversion class file executes. This builds constants.

-- |[ ================================= Runebearer Information ================================= ]|
--Each of the six characters that can revert during a defeat scene needs to have a lookup of which
-- form names map to which scripts so the game can correctly re-transform them.

-- |[ =============== Function =============== ]|
--Local manufacturing function. Creates a reversion entry.
local function fnCreateRevertEntry(psName, psVar, psCall, psaList)
    local zRevertEntry = {}
    zRevertEntry.sName        = psName
    zRevertEntry.sFormVar     = psVar
    zRevertEntry.sRevertCall  = psCall
    zRevertEntry.saExceptions = psaList
    zRevertEntry.saFormMap    = {}
    return zRevertEntry
end

-- |[ ================= Mei ================== ]|
--Create.
local zEntry = fnCreateRevertEntry("Mei", "Root/Variables/Global/Mei/sForm", gsRoot .. "FormHandlers/Mei/Form_Human.lua", {"Human"})

--Form Mapping.
table.insert(zEntry.saFormMap, {"Human",       gsRoot .. "FormHandlers/Mei/Form_Human.lua"})
table.insert(zEntry.saFormMap, {"Alraune",     gsRoot .. "FormHandlers/Mei/Form_Alraune.lua"})
table.insert(zEntry.saFormMap, {"Bee",         gsRoot .. "FormHandlers/Mei/Form_Bee.lua"})
table.insert(zEntry.saFormMap, {"Ghost",       gsRoot .. "FormHandlers/Mei/Form_Ghost.lua"})
table.insert(zEntry.saFormMap, {"Gravemarker", gsRoot .. "FormHandlers/Mei/Form_Gravemarker.lua"})
table.insert(zEntry.saFormMap, {"Mannequin",   gsRoot .. "FormHandlers/Mei/Form_Mannequin.lua"})
table.insert(zEntry.saFormMap, {"Slime",       gsRoot .. "FormHandlers/Mei/Form_Slime.lua"})
table.insert(zEntry.saFormMap, {"Werecat",     gsRoot .. "FormHandlers/Mei/Form_Werecat.lua"})
table.insert(zEntry.saFormMap, {"Wisphag",     gsRoot .. "FormHandlers/Mei/Form_Wisphag.lua"})

--Register.
table.insert(SceneReversion.csaRevertList, zEntry)

-- |[ ================ Sanya ================= ]|
--Create.
zEntry = fnCreateRevertEntry("Sanya", "Root/Variables/Global/Sanya/sForm", gsRoot .. "FormHandlers/Sanya/Form_Human.lua", {"Human"})

--Form Mapping.
table.insert(zEntry.saFormMap, {"Human",   gsRoot .. "FormHandlers/Sanya/Form_Human.lua"})
table.insert(zEntry.saFormMap, {"Bunny",   gsRoot .. "FormHandlers/Sanya/Form_Bunny.lua"})
table.insert(zEntry.saFormMap, {"Harpy",   gsRoot .. "FormHandlers/Sanya/Form_Harpy.lua"})
table.insert(zEntry.saFormMap, {"Sevavi",  gsRoot .. "FormHandlers/Sanya/Form_Sevavi.lua"})
table.insert(zEntry.saFormMap, {"Werebat", gsRoot .. "FormHandlers/Sanya/Form_Werebat.lua"})

--Register.
table.insert(SceneReversion.csaRevertList, zEntry)

-- |[ ================ Jeanne ================ ]|
--Create.
table.insert(SceneReversion.csaRevertList, fnCreateRevertEntry("Jeanne", "Root/Variables/Global/Jeanne/sForm", "Null", {"Human"}))

--Form Mapping.
--Register.
table.insert(SceneReversion.csaRevertList, zEntry)

-- |[ ================ Lotta ================= ]|
--Create.
table.insert(SceneReversion.csaRevertList, fnCreateRevertEntry("Lotta", "Root/Variables/Global/Lotta/sForm", "Null", {"Human"}))

--Form Mapping.
--Register.
table.insert(SceneReversion.csaRevertList, zEntry)

-- |[ ============== Christine =============== ]|
--Create.
zEntry = fnCreateRevertEntry("Christine", "Root/Variables/Global/Christine/sForm", gsRoot .. "FormHandlers/Christine/Form_Human.lua", {"Human", "Male"})

--Form Mapping.
table.insert(zEntry.saFormMap, {"Human",         gsRoot .. "FormHandlers/Christine/Form_Human.lua"})
table.insert(zEntry.saFormMap, {"Male",          gsRoot .. "FormHandlers/Christine/Form_Male.lua"})
table.insert(zEntry.saFormMap, {"Darkmatter",    gsRoot .. "FormHandlers/Christine/Form_Darkmatter.lua"})
table.insert(zEntry.saFormMap, {"Doll",          gsRoot .. "FormHandlers/Christine/Form_Doll.lua"})
table.insert(zEntry.saFormMap, {"Eldritch",      gsRoot .. "FormHandlers/Christine/Form_Eldritch.lua"})
table.insert(zEntry.saFormMap, {"Electrosprite", gsRoot .. "FormHandlers/Christine/Form_Electro.lua"})
table.insert(zEntry.saFormMap, {"Golem",         gsRoot .. "FormHandlers/Christine/Form_Golem.lua"})
table.insert(zEntry.saFormMap, {"LatexDrone",    gsRoot .. "FormHandlers/Christine/Form_Latex.lua"})
table.insert(zEntry.saFormMap, {"Raiju",         gsRoot .. "FormHandlers/Christine/Form_Raiju.lua"})
table.insert(zEntry.saFormMap, {"Secrebot",      gsRoot .. "FormHandlers/Christine/Form_Secrebot.lua"})
table.insert(zEntry.saFormMap, {"SteamDroid",    gsRoot .. "FormHandlers/Christine/Form_SteamDroid.lua"})

--Register.
table.insert(SceneReversion.csaRevertList, zEntry)

-- |[ ================ Talia ================= ]|
--Create.
table.insert(SceneReversion.csaRevertList, fnCreateRevertEntry("Talia", "Root/Variables/Global/Talia/sForm", "Null", {"Human"}))

--Form Mapping.
--Register.
table.insert(SceneReversion.csaRevertList, zEntry)