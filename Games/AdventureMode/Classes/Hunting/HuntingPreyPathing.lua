-- |[ ================================== Hunting Prey Pathing ================================== ]|
--Causes prey to "path" around the map, changing where they are from their spawn and leaving clues.

-- |[ ================================ Hunting:fnPathPreyNest() ================================ ]|
--All rooms adjacent to the prey's starting room will be marked as paths towards its room. This means
-- nests have tracks pointing the player to the nest.
function Hunting:fnPathPreyNest(psPreyName)
	
    -- |[Argument Check]|
    if(psPreyName == nil) then return end
    
	-- |[Existence Check]|
    --Make sure the given prey exists.
    local iPreySlot, zPreyObject = self:fnGetActivePrey(psPreyName)
    if(iPreySlot == nil) then
        io.write("Hunting:fnPathPreyNest(): Error, no prey creature named " .. psPreyName .. " found.\n")
        return
    end
    
    -- |[Setup]|
    --Get where the prey currently is.
    local sCurLocation = zPreyObject.sCurLocation
	
	--Get the room in question.
	local iMapIndex, zMapObject = self:fnGetMapByName(sCurLocation)
    if(iMapIndex == nil) then
        io.write("Hunting:fnPathPreyNest(): Error, prey named " .. psPreyName .. "exists, but was in map " .. sCurLocation .. " that does not exist.\n")
        return
    end
    
    -- |[Assemble List of Locations]|
    --Create a list of all locations we can go to from here, and their weights.
    for p = 1, #zMapObject.zaConnections, 1 do

        --Fast-access pointer.
        local zTargetRoom = zMapObject.zaConnections[p]
        local iTargetSlot = zTargetRoom.iSlot
        
        --Add the prey's clue indicator to the adjacent room. Note: Direction is inverted, as this is not
        -- the prey moving from the nest, it's rooms adjacent to the nest moving to the nest.
        local zMovement = {}
        zMovement.sName = zTargetRoom.sName
        zMovement.iSlot = zTargetRoom.iSlot
        zMovement.iDirection = SysConstants:fnGetReverseDirFromCode(zTargetRoom.iDirection)
        table.insert(zPreyObject.zaPathList, zMovement)
        
    end
end

-- |[ ============================== Hunting:fnPathPreyManually() ============================== ]|
--Sets a specific path for a prey. Used for story-specific moments.
function Hunting:fnPathPreyManually(psName, psaPathNames)
    
    -- |[Argument Check]|
    if(psName       == nil) then return end
    if(psaPathNames == nil) then return end
	
	--Empty array does nothing.
	if(#psaPathNames < 1) then return end
    
	-- |[Existence Check]|
    --Make sure the given prey exists.
    local iPreySlot, zPreyObject = self:fnGetActivePrey(psName)
    if(iPreySlot == nil) then
        io.write("Hunting:fnPathPreyManually() - Error, no prey creature named " .. psName .. " found.\n")
        return
    end
    
    -- |[Room Check]|
    --Check every room in the provided array. They all have to exist.
    for i = 1, #psaPathNames, 1 do
		local iMapIndex, zMapObject = self:fnGetMapByName(psaPathNames[i])
        if(iMapIndex == nil) then
            io.write("Hunting:fnPathPreyManually() - Error, room " .. psaPathNames[i] .. " does not exist.\n")
            return
        end
    end
    
    -- |[Setup]|
    --Get where the prey currently is.
    local sCurLocation = zPreyObject.sCurLocation
    local iCurLocation, zCurRoomObject = self:fnGetMapByName(sCurLocation)
	
    -- |[Begin Pathing]|
    for i = 1, #psaPathNames, 1 do
    
        --Get the room in question.
		local iTargetMapIndex, zTargetMapObject = self:fnGetMapByName(psaPathNames[i])
    
        --Store the current location in the path array.
        local zMovement = {}
        zMovement.sName = sCurLocation
        zMovement.iSlot = iCurLocation
        zMovement.iDirection = 0
        
        --Locate the direction between the two rooms.
        for p = 1, #zCurRoomObject.zaConnections, 1 do
            if(zCurRoomObject.zaConnections[p].sName == zTargetMapObject.sName) then
                zMovement.iDirection = zCurRoomObject.zaConnections[p].iDirection
            end
        end
        
        --Insert it.
        table.insert(zPreyObject.zaPathList, zMovement)
        
        --Move the prey's current location to the target.
        zPreyObject.sCurLocation = zTargetMapObject.sName
        zPreyObject.iCurLocation = iTargetMapIndex
		
		--Update temporary variables.
        sCurLocation   = zPreyObject.sCurLocation
        iCurLocation   = zPreyObject.iCurLocation
		zCurRoomObject = zTargetMapObject
    end

end

--Archived version of a function from the previous implementation, not used since nests are the current standard.
-- Might be revived and updated later.
--[=[
-- |[fnPathPrey]|
--Causes the prey to path around the map randomly. The prey weights rolls against where it has already
-- been, but may sometimes backtrack anyway by sheer coincidence or necessity.
function gzaHuntData.fnPathPrey(psName, piMinDistance, piMaxDistance)
    
    -- |[Error Check]|
    --Verify arguments.
    local this = gzaHuntData
    if(psName        == nil) then return end
    if(piMinDistance == nil) then return end
    if(piMaxDistance == nil) then return end
    
    --Make sure the given prey exists.
    local iPreySlot = this.fnGetSlotOfPrey(psName)
    if(iPreySlot == -1) then
        io.write("fnPathPrey(): Error, no prey creature named " .. psName .. " found.\n")
        return
    end
    
    --Make sure the distance is nonzero, and that the min/max distances make sense.
    if(piMinDistance > piMaxDistance) then piMinDistance = piMaxDistance end
    if(piMaxDistance < 1) then
        io.write("fnPathPrey(): Error, invalid distance " .. piMaxDistance .. ".\n")
        return
    end
    
    -- |[Constants]|
    local ciBaseRollValue = 100
    local cfRepeatRoomFactor = 0.10
    
    -- |[Distance Roll]|
    --Figure out how far we want to path. If the min and max distances are identical, then exactly that many moves will be made.
    local iDistance = LM_GetRandomNumber(piMinDistance, piMaxDistance)
    
    -- |[Setup]|
    --Get where the prey currently is.
    local sCurLocation = this.zaPreyList[iPreySlot].sCurLocation
    local iCurLocation = this.zaPreyList[iPreySlot].iCurLocation
    
    --Debug.
    --io.write("Begin prey pathing. Start point: " .. sCurLocation .. "\n")
    --io.write(" Path distance: " .. iDistance .. "\n")
    
    -- |[Begin Pathing]|
    for i = 1, iDistance, 1 do
    
        -- |[Setup]|
        local iTotalWeight = 0
        --io.write("  =>Path Begins at  " .. sCurLocation .. "\n")
    
        -- |[Assemble List of Locations]|
        --Create a list of all locations we can go to from here, and their weights.
        local zaPathList = {}
        for p = 1, #this.zaRoomList[iCurLocation].zaConnections, 1 do

            --Create entry. Set default weight to 100.
            local zEntry = {}
            zEntry.sName = this.zaRoomList[iCurLocation].zaConnections[p].sName
            zEntry.iSlot = this.zaRoomList[iCurLocation].zaConnections[p].iSlot
            zEntry.iDir  = this.zaRoomList[iCurLocation].zaConnections[p].iDirection
            zEntry.iWeight = ciBaseRollValue

            --Default weight is 100.
            table.insert(zaPathList, zEntry)
            
            --Search across the existing path list. If this room has already been visited, then halve its weight
            -- for each time it was visited.
            for c = 1, #this.zaPreyList[iPreySlot].zaPathList, 1 do
                if(this.zaPreyList[iPreySlot].zaPathList[c].iSlot == zEntry.iSlot) then
                    zEntry.iWeight = math.floor(zEntry.iWeight * cfRepeatRoomFactor)
                end
            end
            
            --Add the final weight to the sum.
            iTotalWeight = iTotalWeight + zEntry.iWeight
        end
        
        -- |[Error]|
        --If the total weight was zero, then something went wrong.
        if(iTotalWeight < 1 or #zaPathList < 1) then return end
        
        --Debug.
        --io.write("     Locations available: " .. #zaPathList .. "\n")
        --for p = 1, #zaPathList, 1 do
            --io.write("     " .. zaPathList[p].sName .. " x " .. zaPathList[p].iWeight .. "\n")
        --end
    
        -- |[Roll By Weight]|
        --Roll by the total weight and select which room to path to.
        local iSlot = 1
        local iTotalRoll = LM_GetRandomNumber(1, iTotalWeight)
        
        --Resolve which room to enter and path there.
        while(true) do
            
            --Subtract.
            iTotalRoll = iTotalRoll - zaPathList[iSlot].iWeight
            
            --Check for ending case:
            if(iTotalRoll < 1 or zaPathList[iSlot] == nil) then
                break
            end
            
            --Otherwise, next entry.
            iSlot = iSlot + 1
        end
        
        --We should now have a valid room to path to. First, store the current room in the pathing list.
        local zMapObject = this.zaRoomList[iCurLocation]
        local zMovement = {}
        zMovement.sName = zMapObject.sName
        zMovement.iSlot = zMapObject.iSlot
        zMovement.iDirection = zaPathList[iSlot].iDir
        table.insert(this.zaPreyList[iPreySlot].zaPathList, zMovement)
        
        --Move the prey's current location to the target.
        this.zaPreyList[iPreySlot].sCurLocation = zaPathList[iSlot].sName
        this.zaPreyList[iPreySlot].iCurLocation = zaPathList[iSlot].iSlot
        sCurLocation = this.zaPreyList[iPreySlot].sCurLocation
        iCurLocation = this.zaPreyList[iPreySlot].iCurLocation
        --io.write("     Prey paths to " .. this.zaPreyList[iPreySlot].sCurLocation .. "\n")
    end
    
    --Debug.
    --io.write("End prey pathing.\n")
end
]=]