-- |[ ================================= Hunting Exec Tutorial ================================== ]|
--Function for creating a tutorial for the hunting mechanics. This gets called from the normal 
-- execution code automatically.

-- |[ ============================ Hunting:fnGenerateTutorialHunt() ============================ ]|
--Spawns a goose and paths it from the spot right outside the cabin to a spot up north.
function Hunting:fnGenerateTutorialHunt()
	
	-- |[Variable Reset]|
	--Clear any prey.
	self:fnWipePreyData()
	
	--Reset the prey spawn count.
	self.iPreySpawnedTotal = 0
	
	-- |[Spawn]|
    --Spawns one enemy and paths them along a specific path.
	self:fnSpawnActivePrey( "The Goose", gciHunt_PreyType_Omnigoose, "NorthwoodsNCA")
    self:fnPathPreyManually("The Goose", {"NorthwoodsNCB", "NorthwoodsNWA", "NorthwoodsNWB", "NorthwoodsNWC"})

    --Set the spawn position flag to make the clues easier for the player to find.
    self.zaPreyList[1].iClueRoll = 1
	
end