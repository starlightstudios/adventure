-- |[ ====================================== Hunting Prey ====================================== ]|
--Functions related to prey, such as making prototypes, spawning active prey, marking them as dead.

-- |[ ==================================== Property Queries ==================================== ]|
-- |[Hunting:fnGetActivePrey()]|
--Returns the array index and the zActivePrey object of the named prey. If not found, returns nil, nil.
function Hunting:fnGetActivePrey(psName)
    
    -- |[Argument Check]|
    if(psName == nil) then return nil, nil end
    
    -- |[Scan]|
    for i = 1, #self.zaPreyList, 1 do
        if(self.zaPreyList[i].sName == psName) then
            return i, self.zaPreyList[i]
        end
    end
    
    --Not found.
    return nil, nil
end

-- |[Hunting:fnGetPreyPrototypeByName()]|
--Returns the array index and the zPreyData object of the named prey prototype. If not found, returns nil, nil.
function Hunting:fnGetPreyPrototypeByName(psName)
    
    -- |[Argument Check]|
    if(psName == nil) then return nil, nil end
    
    -- |[Scan]|
    for i = 1, #self.zaPreyData, 1 do
        if(self.zaPreyData[i].sName == psName) then
            return i, self.zaPreyData[i]
        end
    end
    
    --Not found.
    return nil, nil
end

-- |[Hunting:fnGetPreyPrototypeByType()]|
--Returns the array index and the zPreyData object of the typed prey prototype. If not found, returns nil, nil.
function Hunting:fnGetPreyPrototypeByType(piType)
    
    -- |[Argument Check]|
    if(piType == nil) then return nil, nil end
    
    -- |[Scan]|
    for i = 1, #self.zaPreyData, 1 do
        if(self.zaPreyData[i].iType == piType) then
            return i, self.zaPreyData[i]
        end
    end
    
    --Not found.
    return nil, nil
end

-- |[ ====================================== Manipulators ====================================== ]|
-- |[Hunting:fnWipePreyData()]|
--Clears all active prey data.
function Hunting:fnWipePreyData()
	self.zaPreyList = {}
end

-- |[Hunting:fnRegisterPreyPrototype()]|
--Creates and registers a prototype for a prey animal.
function Hunting:fnRegisterPreyPrototype(psName, piCode, piToughness, psDrop, psSprite, psCombatEnemy)

	-- |[Argument Check]|
	if(psName        == nil) then return end
	if(piCode        == nil) then return end
	if(piToughness   == nil) then return end
	if(psDrop        == nil) then return end
	if(psSprite      == nil) then return end
	if(psCombatEnemy == nil) then return end
	
	-- |[Create, Set]|
	local zNewPreyPrototype = {}
	zNewPreyPrototype.sName = psName
	zNewPreyPrototype.iType = piCode
	zNewPreyPrototype.iToughness = piToughness
	zNewPreyPrototype.sHuntDrop = psDrop
	zNewPreyPrototype.sSpriteName = psSprite
	zNewPreyPrototype.sCombatEnemy = psCombatEnemy
	
	-- |[Register]|
	table.insert(self.zaPreyData, zNewPreyPrototype)
end

-- |[Hunting:fnSpawnActivePrey()]|
--Creates and registers a new active prey animal of the given type in the given room. Prints warnings if any
-- of the types or rooms are not found. The prey animal will be spawned with the given name which can be used
-- to locate it.
function Hunting:fnSpawnActivePrey(psName, piType, psSpawnRoom)
	
	-- |[ ============== Setup =============== ]|
    -- |[Argument Check]|
    if(psName      == nil) then return end
    if(piType      == nil) then return end
    if(psSpawnRoom == nil) then return end
    
	-- |[Validity Checks]|
	local iPrototypeSlot, zPrototype = self:fnGetPreyPrototypeByType(piType)
	local iMapSlot, zMapObject       = self:fnGetMapByName(psSpawnRoom)
	if(iPrototypeSlot == nil or iMapSlot == nil) then return end
    
    -- |[Create and Register]|
    --Create.
    local zPrey = {}
    zPrey.bDisabled = false
    zPrey.sName = psName
    zPrey.iType = piType
    zPrey.sCurLocation = psSpawnRoom
    zPrey.iCurLocation = iMapSlot
    zPrey.iClueRoll = LM_GetRandomNumber(0, 1000)
    zPrey.zaPathList = {}
    zPrey.sLootDrop = zPrototype.sHuntDrop
    
    --Register.
    table.insert(self.zaPreyList, zPrey)
	
end