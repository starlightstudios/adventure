-- |[ ==================================== Hunting Set Data ==================================== ]|
--Called after class initialization, builds the lookup tables used in the chapter. Contains no functions.

-- |[ ==================================== Prey Prototypes ===================================== ]|
-- |[Northwoods Huntables]|
--Hunting.fnRegisterPreyPrototype(psName, piCode, piToughness, psDrop, psSprite, psCombatEnemy)
Hunting:fnRegisterPreyPrototype("Omnigoose", gciHunt_PreyType_Omnigoose, 0, "Bird Meat",          "Omnigoose", "Puissant Omnigoose")
Hunting:fnRegisterPreyPrototype("Turkerus",  gciHunt_PreyType_Turkerus,  0, "Bird Meat",          "Turkerus",  "Clever Turkerus")
Hunting:fnRegisterPreyPrototype("Brimhog",   gciHunt_PreyType_Brimhog,   0, "Slippery Boar Meat", "Brimhog",   "Rowdy Brimhog")
Hunting:fnRegisterPreyPrototype("Unielk",    gciHunt_PreyType_Unielk,    0, "Mighty Venison",     "Unielk",    "Unielk Grazer")

-- |[Westwoods Huntables]|
Hunting:fnRegisterPreyPrototype("Macho Brimhog",     gciHunt_PreyType_MBrimhog,  0, "Bonesaw Hog Meat", "Brimhog",       "Macho Brimhog")
Hunting:fnRegisterPreyPrototype("Slathering Unielk", gciHunt_PreyType_SUnielk,   1, "Caustic Vension",  "Unielk",        "Unielk Slatherer")
Hunting:fnRegisterPreyPrototype("Psuedogriffon Hen", gciHunt_PreyType_PesudoHen, 0, "Griffon Egg",      "Pseudogriffon", "Pseudogriffon Hen")
Hunting:fnRegisterPreyPrototype("Buneye",            gciHunt_PreyType_Buneye,    0, "Buneye Fluff",     "BunEye",        "Evasive Buneye")

-- |[Diagnostics]|
local bDiagnostics = fnAutoCheckDebugFlag("Hunting: Data Report")
if(bDiagnostics) then
    io.write("Reporting prey prototype data.\n")
    for i = 1, #Hunting.zaPreyData, 1 do
        io.write(" " .. Hunting.zaPreyData[i].sName .. "\n")
    end
end

-- |[ ======================================= Map Setup ======================================== ]|
-- |[ ============== Northwoods ============== ]|
--A nice boreal forest, nothing sinister.
Hunting:fnRegisterMapSeries("NorthwoodsNE", "A", "E")
Hunting:fnRegisterMapSeries("NorthwoodsNC", "A", "D")
Hunting:fnRegisterMapSeries("NorthwoodsNW", "A", "F")
Hunting:fnRegisterMapSeries("NorthwoodsSC", "A", "E")
Hunting:fnRegisterMapSeries("NorthwoodsSE", "A", "D")
Hunting:fnRegisterMapSeries("NorthwoodsSW", "A", "D")

-- |[NE Series]|
--Hunting:fnCreateConnection(psRoomNameA, psRoomNameB, ...)
Hunting:fnCreateConnection("NorthwoodsNEA", "NorthwoodsNEB|S", "NorthwoodsNEE|W")
Hunting:fnCreateConnection("NorthwoodsNEB", "NorthwoodsNEA|N", "NorthwoodsNEC|S")
Hunting:fnCreateConnection("NorthwoodsNEC", "NorthwoodsNEB|N", "NorthwoodsNED|W")
Hunting:fnCreateConnection("NorthwoodsNED", "NorthwoodsNEC|E", "NorthwoodsNCA|W")
Hunting:fnCreateConnection("NorthwoodsNEE", "NorthwoodsNEA|E", "NorthwoodsNWC|W")

-- |[NC Series]|
Hunting:fnCreateConnection("NorthwoodsNCA", "NorthwoodsNED|E",  "NorthwoodsNCB|W",  "NorthwoodsSCA|S")
Hunting:fnCreateConnection("NorthwoodsNCB", "NorthwoodsNWA|N",  "NorthwoodsNCC|W",  "NorthwoodsNCD|S")
Hunting:fnCreateConnection("NorthwoodsNCC", "NorthwoodsNCB|NE", "NorthwoodsNCD|SE", "NorthwoodsSWA|S")
Hunting:fnCreateConnection("NorthwoodsNCD", "NorthwoodsNCC|NW", "NorthwoodsNCB|N")

-- |[NW Series]|
Hunting:fnCreateConnection("NorthwoodsNWA", "NorthwoodsNWB|N", "NorthwoodsNWD|W", "NorthwoodsNCB|S")
Hunting:fnCreateConnection("NorthwoodsNWB", "NorthwoodsNWC|E", "NorthwoodsNWA|S")
Hunting:fnCreateConnection("NorthwoodsNWC", "NorthwoodsNWB|W", "NorthwoodsNEE|E")
Hunting:fnCreateConnection("NorthwoodsNWD", "NorthwoodsNWA|E", "NorthwoodsNWE|N", "NorthwoodsNWF|S")
Hunting:fnCreateConnection("NorthwoodsNWE", "NorthwoodsNWD|S")
Hunting:fnCreateConnection("NorthwoodsNWF", "NorthwoodsNWD|N")

-- |[SC Series]|
Hunting:fnCreateConnection("NorthwoodsSCA", "NorthwoodsNCA|N", "NorthwoodsSCB|S", "NorthwoodsSCE|E")
Hunting:fnCreateConnection("NorthwoodsSCB", "NorthwoodsSCA|N", "NorthwoodsSEA|S", "NorthwoodsSCC|W")
Hunting:fnCreateConnection("NorthwoodsSCC", "NorthwoodsSCB|E", "NorthwoodsSCD|W")
Hunting:fnCreateConnection("NorthwoodsSCD", "NorthwoodsSCC|E")
Hunting:fnCreateConnection("NorthwoodsSCE", "NorthwoodsSCA|W")

-- |[SE Series]|
Hunting:fnCreateConnection("NorthwoodsSEA", "NorthwoodsSCB|N", "NorthwoodsSWD|W", "NorthwoodsSEB|E")
Hunting:fnCreateConnection("NorthwoodsSEB", "NorthwoodsSEA|W", "NorthwoodsSEC|N", "NorthwoodsSED|E")
Hunting:fnCreateConnection("NorthwoodsSEC", "NorthwoodsSEB|S")
Hunting:fnCreateConnection("NorthwoodsSED", "NorthwoodsSEB|W")

-- |[SW Series]|
Hunting:fnCreateConnection("NorthwoodsSWA", "NorthwoodsNCC|N", "NorthwoodsSWB|S")
Hunting:fnCreateConnection("NorthwoodsSWB", "NorthwoodsSWA|N", "NorthwoodsSWC|E")
Hunting:fnCreateConnection("NorthwoodsSWC", "NorthwoodsSWB|W", "NorthwoodsSWD|E")
Hunting:fnCreateConnection("NorthwoodsSWD", "NorthwoodsSWC|W", "NorthwoodsSEA|E")

-- |[ ============== Westwoods =============== ]|
--The southwestern part of the glacier. Not a nice place.
Hunting:fnRegisterMapSeries("WestwoodsN",  "A", "F")
Hunting:fnRegisterMapSeries("WestwoodsW",  "A", "C")
Hunting:fnRegisterMapSeries("WestwoodsC",  "D", "G")
Hunting:fnRegisterMapSeries("WestwoodsE",  "A", "C")
Hunting:fnRegisterMapSeries("WestwoodsE",  "F", "G")
Hunting:fnRegisterMapSeries("WestwoodsSE", "A", "A")

-- |[N Series]|
Hunting:fnCreateConnection("WestwoodsNA", "WestwoodsNB|E", "WestwoodsWA|S")
Hunting:fnCreateConnection("WestwoodsNB", "WestwoodsNA|W", "WestwoodsNC|E")
Hunting:fnCreateConnection("WestwoodsNC", "WestwoodsNB|W", "WestwoodsND|E")
Hunting:fnCreateConnection("WestwoodsND", "WestwoodsNC|W", "WestwoodsNE|E")
Hunting:fnCreateConnection("WestwoodsNE", "WestwoodsND|W", "WestwoodsNF|E")
Hunting:fnCreateConnection("WestwoodsNF", "WestwoodsNE|W", "WestwoodsEA|S")

-- |[W Series]|
Hunting:fnCreateConnection("WestwoodsWA", "WestwoodsNA|N", "WestwoodsWB|S")
Hunting:fnCreateConnection("WestwoodsWB", "WestwoodsWA|N", "WestwoodsWC|S")
Hunting:fnCreateConnection("WestwoodsWC", "WestwoodsWB|N")

-- |[C Series]|
Hunting:fnCreateConnection("WestwoodsCD", "WestwoodsCE|E")
Hunting:fnCreateConnection("WestwoodsCE", "WestwoodsCD|W", "WestwoodsCF|S", "WestwoodsCG|E")
Hunting:fnCreateConnection("WestwoodsCF", "WestwoodsCE|N")
Hunting:fnCreateConnection("WestwoodsCG", "WestwoodsCE|W", "WestwoodsEB|E")

-- |[E Series]|
Hunting:fnCreateConnection("WestwoodsEA", "WestwoodsNF|N", "WestwoodsEB|S")
Hunting:fnCreateConnection("WestwoodsEB", "WestwoodsEA|N", "WestwoodsEG|S", "WestwoodsCG|W")
Hunting:fnCreateConnection("WestwoodsEC", "WestwoodsEG|N", "WestwoodsEF|E", "WestwoodsSEA|S")
Hunting:fnCreateConnection("WestwoodsEF", "WestwoodsEC|W")
Hunting:fnCreateConnection("WestwoodsEG", "WestwoodsEB|N", "WestwoodsEC|S")

-- |[SE Series]|
Hunting:fnCreateConnection("WestwoodsSEA", "WestwoodsEC|N")

-- |[ ============= Diagnostics ============== ]|
if(bDiagnostics) then
    io.write("Reporting map data.\n")
    for i = 1, #Hunting.zaRoomList, 1 do
        io.write(" " .. Hunting.zaRoomList[i].sName .. "\n")
    end
end
