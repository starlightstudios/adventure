-- |[ ====================================== Hunting Map ======================================= ]|
--Functions for querying the map, setting it up, and building connections.

-- |[ ==================================== Property Queries ==================================== ]|
-- |[Hunting:fnGetMapByName()]|
--Locates the named map in the array of maps and returns an index indicating where it is in the list,
-- and a direct pointer to the object. If not found, returns nil,nil.
function Hunting:fnGetMapByName(psName)

	-- |[Argument Check]|
	if(psName == nil) then return nil, nil end

	-- |[Scan]|
	for i = 1, #self.zaRoomList, 1 do
		if(self.zaRoomList[i].sName == psName) then
			return i, self.zaRoomList[i]
		end
	end
	
	-- |[Not Found]|
	return nil, nil
end

-- |[Hunting:fnIsRoomConnectedTo()]|
--Returns true if the room in SlotA is connected to SlotB. Does NOT check if B is connected to A,
-- this is only a one-directional check.
function Hunting:fnIsRoomConnectedTo(piSlotA, piSlotB)
	
	-- |[Argument Check]|
	if(piSlotA == nil) then return false end
	if(piSlotB == nil) then return false end
	
	-- |[Range Check]|
	if(piSlotA < 1 or piSlotA > #self.zaRoomList) then return false end
	if(piSlotB < 1 or piSlotB > #self.zaRoomList) then return false end
	
	-- |[Scan]|
	local zRoomA = self.zaRoomList[piSlotA]
	for i = 1, #zRoomA.zaConnections, 1 do
		if(zRoomA.zaConnections[i].iSlot == piSlotB) then return true end
	end
	
	--Not found.
	return false
end

-- |[ ====================================== Manipulators ====================================== ]|
-- |[Hunting:fnRegisterMapObject()]|
--By default, a map only contains its own name, and its own slot index. Does not check for duplicates.
function Hunting:fnRegisterMapObject(psName)

	-- |[Argument Check]|
	if(psName == nil) then return end
	
	-- |[Create, Set, Register]|
	local zMapObject = {}
	zMapObject.iSlot = #self.zaRoomList
	zMapObject.sName = psName
	zMapObject.zaConnections = {}
	table.insert(self.zaRoomList, zMapObject)
end

-- |[Hunting:fnRegisterMapSeries()]|
--Because most maps exist in a series from "A" to "Z" or some lesser number, creates maps in batches.
function Hunting:fnRegisterMapSeries(psNameBase, psStart, psEnd)

	-- |[Argument Check]|
	if(psNameBase == nil) then return end
    if(psStart    == nil) then return end
    if(psEnd      == nil) then return end
    
	-- |[Iteration]|
	--Setup.
	local sCurrentLetter = psStart
	
	--Range check. If the letter is out of the normal range, fail.
	local iByteValue = string.byte(sCurrentLetter)
	if(iByteValue < string.byte("A") or iByteValue > string.byte("Z")) then return end
	
	--Iterate across the letters. Always stops on Z. Also always stops on non-capitals.
	while(true) do
		
		--Add the remap.
		local sLevelName = psNameBase .. sCurrentLetter
        self:fnRegisterMapObject(sLevelName)
		
		--If this was the ending letter, or the letter 'Z', stop.
		if(sCurrentLetter == psEnd or sCurrentLetter == "Z") then break end
		
		--Move to the next letter.
		local iByteValue = string.byte(sCurrentLetter)
		sCurrentLetter = string.char(iByteValue + 1)
	end
end

-- |[Hunting:fnCreateConnection()]|
--Creates a connection between the two or more provided rooms. Only one is mandatory.
function Hunting:fnCreateConnection(psRoomNameA, psRoomNameB, ...)
    
	-- |[ ============== Setup =============== ]|
    -- |[Error Check]|
    if(psRoomNameA == nil) then return end
    if(psRoomNameB == nil) then return end

    -- |[Divide]|
    --The B room is expected to be "Roomname|Direction" where direction is N, NE, E, SE, etc.
    -- Split the strings up here.
    local iBarSlot   = string.find(psRoomNameB, "|")
    local sRoomNameB = string.sub(psRoomNameB, 1, iBarSlot-1)
    local sRoomDirB  = string.sub(psRoomNameB, iBarSlot+1)

	-- |[Existence Check]|
	--Make sure both maps actually exist.
	local iMapASlot, zMapAObject = self:fnGetMapByName(psRoomNameA)
	local iMapBSlot, zMapBObject = self:fnGetMapByName(sRoomNameB)
	if(iMapASlot == nil or iMapBSlot == nil) then return end
	
	--If both are somehow the same slot, also fail.
	if(iMapASlot == iMapBSlot) then return end
    
	-- |[ ========= Apply Connection ========= ]|
    -- |[A to B]|
    --Check Room A to see if it is already connected. If not, add a connection.
    if(self:fnIsRoomConnectedTo(iMapASlot, iMapBSlot) == false) then
        local zConnection = {}
        zConnection.sName = sRoomNameB
        zConnection.iSlot = iMapBSlot
        zConnection.iDirection = SysConstants:fnGetDirCodeFromString(sRoomDirB)
        table.insert(zMapAObject.zaConnections, zConnection)
    end
    
	-- |[B to A]|
    --Check Room B to see if it is already connected. If not, add a connection.
    if(self:fnIsRoomConnectedTo(iMapBSlot, iMapASlot) == false) then
        local zConnection = {}
        zConnection.sName = psRoomNameA
        zConnection.iSlot = iMapASlot
        zConnection.iDirection = SysConstants:fnGetRevDirCodeFromString(sRoomDirB)
        table.insert(zMapBObject.zaConnections, zConnection)
    end

	-- |[ ====== Additional Connections ====== ]|
    --All additional arguments follow the same set of logic. Just call the function again with
	-- the additional arg in the second slot.
    for i = 1, select("#", ...), 1 do
        local sRoomBaseN = select(i, ...)
		self:fnCreateConnection(psRoomNameA, sRoomBaseN)
    end
end
