-- |[ ================================= Hunting Save and Load ================================== ]|
--A saver function and a loader function for communicating hunt data across savefiles.

-- |[ ================================ Hunting:fnSaveHuntData() ================================ ]|
--Writes hunting data into a special set of parts of the DataLibrary which will be saved into savefiles.
function Hunting:fnSaveHuntData()
    
    -- |[System]|
    --Basic Flag.
    VM_SetVar("Root/Saving/Hunting/System/iHasHuntData", "N", 1.0)
    
    -- |[Prey Data]|
    local iTotalPrey = #self.zaPreyList
    VM_SetVar("Root/Saving/Hunting/Prey/iTotalPrey", "N", iTotalPrey)
    
    --Begin storing.
    for i = 1, iTotalPrey, 1 do
    
		--Object.
		local zPreyData = self.zaPreyList[i]
	
        --Prey's savefile name.
        local sPreyIntName = string.format("Entry%02i", i-1)
        
        --Write.
        VM_SetVar("Root/Saving/Hunting/Prey/"..sPreyIntName.."_sName",      "S", zPreyData.sName)
        VM_SetVar("Root/Saving/Hunting/Prey/"..sPreyIntName.."_iType",      "N", zPreyData.iType)
        VM_SetVar("Root/Saving/Hunting/Prey/"..sPreyIntName.."_sLocationS", "S", zPreyData.sCurLocation)
        VM_SetVar("Root/Saving/Hunting/Prey/"..sPreyIntName.."_iLocationN", "N", zPreyData.iCurLocation)
        VM_SetVar("Root/Saving/Hunting/Prey/"..sPreyIntName.."_iClueRoll",  "N", zPreyData.iClueRoll)
        VM_SetVar("Root/Saving/Hunting/Prey/"..sPreyIntName.."_sLootDrop",  "S", zPreyData.sLootDrop)
        
        --Path Information.
        local iPathEntries = #zPreyData.zaPathList
        VM_SetVar("Root/Saving/Hunting/Prey/"..sPreyIntName.."_iPathLen", "N", iPathEntries)
        for p = 1, iPathEntries, 1 do
            local sPathIntName = string.format("Path%02i", p-1)
            VM_SetVar("Root/Saving/Hunting/Prey/"..sPreyIntName.."_"..sPathIntName.."_sName", "S", zPreyData.zaPathList[p].sName)
            VM_SetVar("Root/Saving/Hunting/Prey/"..sPreyIntName.."_"..sPathIntName.."_iSlot", "N", zPreyData.zaPathList[p].iSlot)
            VM_SetVar("Root/Saving/Hunting/Prey/"..sPreyIntName.."_"..sPathIntName.."_iDir",  "N", zPreyData.zaPathList[p].iDirection)
        end
    
    end
end

-- |[ ================================ Hunting:fnLoadHuntData() ================================ ]|
--Loads hunting data out of the DataLibrary and places it into the lua storage arrays.
function Hunting:fnLoadHuntData()

    -- |[Clear]|
    --Make sure there's no lingering data.
    self:fnWipePreyData()
    
    -- |[Prey Data]|
    --Load total.
    local iTotalPrey = VM_GetVar("Root/Saving/Hunting/Prey/iTotalPrey", "N")
    
    --Begin reading.
    for i = 1, iTotalPrey, 1 do
    
        --Prey's savefile name.
        local sPreyIntName = string.format("Entry%02i", i-1)
        
        --New prey entry.
        local zPrey = {}
        zPrey.bDisabled = false
        zPrey.zaPathList = {}
        
        --Read.
        zPrey.sName        = VM_GetVar("Root/Saving/Hunting/Prey/"..sPreyIntName.."_sName",      "S")
        zPrey.iType        = VM_GetVar("Root/Saving/Hunting/Prey/"..sPreyIntName.."_iType",      "N")
        zPrey.sCurLocation = VM_GetVar("Root/Saving/Hunting/Prey/"..sPreyIntName.."_sLocationS", "S")
        zPrey.iCurLocation = VM_GetVar("Root/Saving/Hunting/Prey/"..sPreyIntName.."_iLocationN", "N")
        zPrey.iClueRoll    = VM_GetVar("Root/Saving/Hunting/Prey/"..sPreyIntName.."_iClueRoll",  "N")
        zPrey.sLootDrop    = VM_GetVar("Root/Saving/Hunting/Prey/"..sPreyIntName.."_sLootDrop",  "S")
        
        --Path Information.
        local iPathEntries = VM_GetVar("Root/Saving/Hunting/Prey/"..sPreyIntName.."_iPathLen", "N")
        for p = 1, iPathEntries, 1 do
            
            --Create.
            local zMovement = {}
            local sPathIntName = string.format("Path%02i", p-1)
            zMovement.sName      = VM_GetVar("Root/Saving/Hunting/Prey/"..sPreyIntName.."_"..sPathIntName.."_sName", "S")
            zMovement.iSlot      = VM_GetVar("Root/Saving/Hunting/Prey/"..sPreyIntName.."_"..sPathIntName.."_iSlot", "N")
            zMovement.iDirection = VM_GetVar("Root/Saving/Hunting/Prey/"..sPreyIntName.."_"..sPathIntName.."_iDir",  "N")
            
            --Add to the path entries.
            table.insert(zPrey.zaPathList, zMovement)
        end
    
        --Add to the prey entries.
        table.insert(self.zaPreyList, zPrey)
    end
end