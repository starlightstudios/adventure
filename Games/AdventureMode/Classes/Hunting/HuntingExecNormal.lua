-- |[ ================================== Hunting Exec Normal =================================== ]|
--Function that acts as an entry point to generate hunts from nothing, assuming the class is set up.

-- |[ ================================ Hunting:fnGenerateHunt() ================================ ]|
function Hunting:fnGenerateHunt()
    
    -- |[ ========= Setup ========== ]|
	-- |[Tutorial Case]|
	--If the tutorial flag is active, run the tutorial function and stop.
	if(self.bIsTutorial == true) then
		self:fnGenerateTutorialHunt()
		return
	end
	
    -- |[Diagnostics]|
	local bDiagnostics = fnAutoCheckDebugFlag("Hunting: Create Hunt")
    Debug_PushPrint(bDiagnostics, "== Running Hunting Spawning Routines ==\n")
    Debug_Print("Total rooms detected: " .. #self.zaRoomList .. "\n")
    Debug_Print("Total prey prototypes: " .. #self.zaPreyData .. "\n")
    
	-- |[Variable Reset]|
	--Clear any prey.
	self:fnWipePreyData()
	
	--Reset the prey spawn count.
	self.iPreySpawnedTotal = 0
	
    -- |[ ======= Northwoods ======= ]|
	Debug_Print("Generating Northwoods groups.\n")

    -- |[Prey Types]|
    --Create a list of all prey types that can spawn.
    local iaPreyTypes = {}
    table.insert(iaPreyTypes, gciHunt_PreyType_Omnigoose)
    table.insert(iaPreyTypes, gciHunt_PreyType_Turkerus)
    table.insert(iaPreyTypes, gciHunt_PreyType_Brimhog)
    table.insert(iaPreyTypes, gciHunt_PreyType_Unielk)
    
    -- |[Map Indices]|
    --Create a list of all maps that can spawn a nest.
    local saHuntMapNames = {}
    table.insert(saHuntMapNames, "NorthwoodsSWD")
    table.insert(saHuntMapNames, "NorthwoodsNCD")
    table.insert(saHuntMapNames, "NorthwoodsNWF")
    table.insert(saHuntMapNames, "NorthwoodsNWC")
    table.insert(saHuntMapNames, "NorthwoodsNED")
    table.insert(saHuntMapNames, "NorthwoodsSCD")
    
    -- |[How Many To Spawn]|
    --There are 3 hunts spawned per set.
    local iHuntsToSpawn = 3
    
	-- |[Exec]|
	Hunting:fnGenerateNestSet(iHuntsToSpawn, iaPreyTypes, saHuntMapNames)
	
    -- |[ ======= Westwoods ======== ]|
	Debug_Print("Generating Westwoods groups.\n")

    -- |[Prey Types]|
    --Create a list of all prey types that can spawn.
    iaPreyTypes = {}
    table.insert(iaPreyTypes, gciHunt_PreyType_MBrimhog)
    table.insert(iaPreyTypes, gciHunt_PreyType_SUnielk)
    table.insert(iaPreyTypes, gciHunt_PreyType_PesudoHen)
    table.insert(iaPreyTypes, gciHunt_PreyType_Buneye)

    -- |[Map Indices]|
    --Create a list of all maps that can spawn a nest.
    saHuntMapNames = {}
    table.insert(saHuntMapNames, "WestwoodsND")
    table.insert(saHuntMapNames, "WestwoodsWA")
    table.insert(saHuntMapNames, "WestwoodsEG")
    table.insert(saHuntMapNames, "WestwoodsCE")
    
    -- |[How Many To Spawn]|
    --There are 3 hunts spawned per set.
    iHuntsToSpawn = 3
    
	-- |[Exec]|
	Hunting:fnGenerateNestSet(iHuntsToSpawn, iaPreyTypes, saHuntMapNames)
	
    -- |[ == Old Capitol Grounds === ]|
    -- |[ ======= Ice Spires ======= ]|
    -- |[ ======= Finish Up ======== ]|
	Debug_PopPrint("Finished hunt generation.\n")
end