-- |[ =================================== Hunting Class File =================================== ]|
--Class file for Hunting. Call this once at initialization.

--Singleton handler for the Chapter 2 Hunting mechanics. Has initializers for data storage, functions
-- to set hunts, and stores prey and map data.

-- |[ =========== Globals/Statics ============ ]|
-- |[Constants]|
gciHunt_PreyType_Error = -1
gciHunt_PreyType_Omnigoose = 0
gciHunt_PreyType_Turkerus = 1
gciHunt_PreyType_Brimhog = 2
gciHunt_PreyType_Unielk = 3
gciHunt_PreyType_MBrimhog = 4
gciHunt_PreyType_SUnielk = 5
gciHunt_PreyType_PesudoHen = 6
gciHunt_PreyType_Buneye = 7
gciHunt_PreyType_Total = 8

-- |[Map Object]|
--Represents a room in the world. Rooms know which slot they are in.
--zMapObject.iSlot				--Index in Hunting.zaRoomList where this room is.
--zMapObject.sName				--Unique name of the room.
--zMapObject.zaConnections		--Made of zConnections. Used to allow prey to path between rooms.

-- |[Connection Object]|
--Represents a connection between two MapObjects.
--zConnection.sName				--Name of the room that is connected to.
--zConnection.iSlot				--Index in Hunting.zaRoomList of the destination room.
--zConnection.iDirection		--One of gci_Face_North, gci_Face_South, etc indicating which direction the target is relative to the owner.

-- |[Prey Data Object]|
--Prototype used to create a prey entity with a set of properties.
--zPreyData.sName				--Unique name
--zPreyData.iType				--Integer, one of gciHunt_PreyType_SUBTYPE above.
--zPreyData.iToughness			--Used to outline sprites.
--zPreyData.sHuntDrop			--Name of the item dropped if this entity is shot on the overworld
--zPreyData.sSpriteName			--Name of the world sprite this entity uses.
--zPreyData.sCombatEnemy		--If the player decides to fight this enemy, name of the combat enemy spawned.

-- |[Active Prey Object]|
--Represents a creature the player can hunt.
--zActivePrey.bDisabled			--If true, the entity has been shot and removed.
--zActivePrey.sName				--Unique name, usually "Autogen00", "Autogen01" etc.
--zActivePrey.iType				--One of gciHunt_PreyType_SUBTYPE above.
--zActivePrey.sCurLocation		--Name of the map this entity is in.
--zActivePrey.iCurLocation		--Index in Hunting.zaRoomList this entity is in.
--zActivePrey.iClueRoll			--Integer from 1 to 1000 which indicates which clue entity is used to display tracks or spawn.
--zActivePrey.zaPathList		--List of zMovement objects indicating where the tracks left by this entity are.
--zActivePrey.sLootDrop			--Name of the item dropped if this entity is shot on the overworld.

-- |[Prey Movement Object]|
--Represents a movement made by a prey. The name is the room it moved to.
--zMovement.sName				--Name of the room moved to.
--zMovement.iSlot				--Index in Hunting.zaRoomList of the destination room.
--zMovement.iDirection			--One of gci_Face_North, gci_Face_South, etc indicating which direction the target is relative to the owner.

-- |[ ============ Class Members ============= ]|
-- |[System]|
Hunting = {}
Hunting.__index = Hunting

-- |[Tutorial]|
--When this flag is true, a pre-made tutorial hunt is the only one that spawns. This is part
-- of the story and is un-toggled once the player completes it.
Hunting.bIsTutorial = false

-- |[Object Storage]|
Hunting.zaRoomList = {} --List of zMapObjects.
Hunting.zaPreyData = {} --List of zPreyData objects.
Hunting.zaPreyList = {} --Made of zActivePrey objects.

-- |[Globals]|
--This variable tracks how many prey have been spawned, so newly spawned prey can have unique names.
Hunting.iPreySpawnedTotal = 0

--Path for dialogue script.
Hunting.sDialoguePath = gsRoot .. "Chapter 2/Hunting/Hunting Dialogue.lua"

-- |[ ============ Class Methods ============= ]|
--System
--Exec: Normal
function Hunting:fnGenerateHunt() end

--Exec: Tutorial
function Hunting:fnGenerateTutorialHunt() end

--Generate
function Hunting:fnGenerateNestSet(piHuntsToSpawn, piaPreyTypes, psaMapNames) end

--Map Functions
function Hunting:fnGetMapByName(psName)                            end
function Hunting:fnIsRoomConnectedTo(piSlotA, piSlotB)             end
function Hunting:fnRegisterMapObject(psName)                       end
function Hunting:fnRegisterMapSeries(psNameBase, psStart, psEnd)   end
function Hunting:fnCreateConnection(psRoomNameA, psRoomNameB, ...) end

--Prey Functions
function Hunting:fnGetActivePrey(psName)                                                               end
function Hunting:fnGetPreyPrototypeByName(psName)                                                      end
function Hunting:fnGetPreyPrototypeByType(piType)                                                      end
function Hunting:fnWipePreyData()                                                                      end
function Hunting:fnRegisterPreyPrototype(psName, piCode, piToughness, psDrop, psSprite, psCombatEnemy) end
function Hunting:fnSpawnActivePrey(psName, piType, psSpawnRoom)                                        end

--Prey Pathing
function Hunting:fnPathPreyNest(psPreyName)               end
function Hunting:fnPathPreyManually(psName, psaPathNames) end

--Saving and Loading
function Hunting:fnSaveHuntData() end
function Hunting:fnLoadHuntData() end

--World Functions
function Hunting:fnGetAsciiSum(psWord)                                     end
function Hunting:fnHuntConstructor()                                       end
function Hunting:fnSpawnClue(psName, piDirection, pzaClueList, piClueSlot) end

-- |[ ========== Class Constructor =========== ]|
--Singleton, class should not be instantiated.

-- |[ ========= Call Implementations ========= ]|
LM_ExecuteScript(fnResolvePath() .. "HuntingExecNormal.lua")
LM_ExecuteScript(fnResolvePath() .. "HuntingExecTutorial.lua")
LM_ExecuteScript(fnResolvePath() .. "HuntingGenerate.lua")
LM_ExecuteScript(fnResolvePath() .. "HuntingMap.lua")
LM_ExecuteScript(fnResolvePath() .. "HuntingPrey.lua")
LM_ExecuteScript(fnResolvePath() .. "HuntingPreyPathing.lua")
LM_ExecuteScript(fnResolvePath() .. "HuntingSaveLoad.lua")
LM_ExecuteScript(fnResolvePath() .. "HuntingWorldFunctions.lua")

--Build prototypes. Note this must be called last, so it can use the functions set in other files.
LM_ExecuteScript(fnResolvePath() .. "HuntingSetData.lua")
