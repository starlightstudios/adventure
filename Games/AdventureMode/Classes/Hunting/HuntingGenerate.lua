-- |[ ============================== Hunting Generation Functions ============================== ]|
--Functions that use given sets of rooms and prey to generate hunts.

-- |[ ============================== Hunting:fnGenerateNestSet() =============================== ]|
--Given a set of prey types and maps, generates nests at those maps that are populated by that
-- prey type. Each prey entry can be in at most one nest, though if there are duplicates on the
-- prey list they could appear in two nests.
--Advances Hunting.iPreySpawnedTotal to track unique names. The lists will be modified to remove
-- used elements.
function Hunting:fnGenerateNestSet(piHuntsToSpawn, piaPreyTypes, psaMapNames)
	
	-- |[Argument Check]|
	if(piHuntsToSpawn == nil) then return end
	if(piaPreyTypes   == nil) then return end
	if(psaMapNames    == nil) then return end
	
	-- |[Setup]|
	--If a variable tracker hasn't been made yet, do that here.
	DL_AddPath("Root/Variables/Chapter2/HuntTracker/")

    -- |[Iteration]|
    --For each hunt that must spawn:
    for i = 1, piHuntsToSpawn, 1 do
    
		-- |[List Population Check]|
		--If either the map list, or the prey type list, runs out of entries, stop. This is not an error.
		if(#piaPreyTypes < 1) then return end
		if(#psaMapNames  < 1) then return end
		
		--Diagnostics.
        Debug_Print("Spawning hunt " .. i .. "\n")
        Debug_Print(" At start of spawn, prey count is: " .. self.iPreySpawnedTotal .. "\n")
        Debug_Print(" Size of prey list: " .. #piaPreyTypes .. ", size of map list: " .. #psaMapNames .. "\n")
	
		-- |[Select Prey and Map]|
        --Select a prey type and a map.
        local iPreyIndex = LM_GetRandomNumber(1, #piaPreyTypes)
        local iMapIndex  = LM_GetRandomNumber(1, #psaMapNames)
		
		--Diagnostics.
        Debug_Print(" Selected prey: " .. iPreyIndex .. " and map: " .. iMapIndex .. "\n")
	
        --Fast-access variables, remove from original table.
		local iUsePreyIndex, zPreyPrototype = self:fnGetPreyPrototypeByType(piaPreyTypes[iPreyIndex])
		local iUseMapIndex,  zMapDataObject = self:fnGetMapByName(psaMapNames[iMapIndex])
        table.remove(piaPreyTypes, iPreyIndex)
        table.remove(psaMapNames,  iMapIndex)
		
		--Make sure these actually exist in the lookup tables.
		if(iUsePreyIndex == nil) then
			io.write("Hunting:fnGenerateHunt() - Warning. Unable to locate prey prototype in index " .. iPreyIndex .. " of table with size " .. #piaPreyTypes .. "\n")
			return
		end
		if(iUseMapIndex == nil) then
			io.write("Hunting:fnGenerateHunt() - Warning. Unable to locate map " .. psaMapNames[iMapIndex] .. ".\n")
			return
		end
	
        --Diagnostics.
        Debug_Print(" Using prey type: " .. zPreyPrototype.iType .. ", named: " .. zPreyPrototype.sName .. "\n")
        Debug_Print(" Name of room: " .. zMapDataObject.sName .. "\n")
        Debug_Print(" Connections: " .. #zMapDataObject.zaConnections .. "\n")
        
        -- |[Run Creation]|
        --Spawn between 2-4 of this prey on the given map.
        local iNumToSpawn = LM_GetRandomNumber(2, 4)
        for p = 1, iNumToSpawn, 1 do
            
			--Create a tracker variable. This gets set to 1 if the prey is shot.
			local sPreyTrackerPath = string.format("Root/Variables/Chapter2/HuntTracker/%02i", self.iPreySpawnedTotal)
			VM_SetVar(sPreyTrackerPath, "N", 0)
	
            --Spawn.
            local sPreyName = string.format("Autogen%02i", self.iPreySpawnedTotal)
			self:fnSpawnActivePrey(sPreyName, zPreyPrototype.iType, zMapDataObject.sName)
			
			--Diagnostics.
			Debug_Print("  Spawned prey: " .. sPreyName .. "\n")
			Debug_Print("   Variable: " .. sPreyTrackerPath .. "\n")
        
            --If this is the first element, place adjacent paths.
            if(p == 1) then
                self:fnPathPreyNest(sPreyName)
            end
			
			--Advance the prey name tracker.
			self.iPreySpawnedTotal = self.iPreySpawnedTotal + 1
			
        end
    end
	
end