-- |[ ================================== GUIMap List Handling ================================== ]|
--Handles global list functions.

-- |[ =============================== GUIMap:fnClearGlobalList() =============================== ]|
--Clears the global MapPinSet list.
function GUIMap:fnClearGlobalList()
    gzaGUIMaps = {}
end

-- |[ =============================== GUIMap:fnRegisterNewSet() ================================ ]|
--Creates, registers, and returns a new MapPinSet to the global list.
function GUIMap:fnRegisterNewMap(psName)
    
    --Create.
    local zNewMap = GUIMap:new(psName)
    
    --Register.
    table.insert(gzaGUIMaps, zNewMap)
    
    --Return.
    return zNewMap
end

-- |[ ================================== GUIMap:fnLocateMap() ================================== ]|
--Locates the named GUIMap from the global list and returns it, or nil if not found.
function GUIMap:fnLocateMap(psName)

	-- |[Arg Check]|
	if(psName == nil) then return nil end
    
    -- |[Scan]|
    for i = 1, #gzaGUIMaps, 1 do
        if(gzaGUIMaps[i].sLocalName == psName) then return gzaGUIMaps[i] end
    end
    
    -- |[Not Found]|
    return nil
end

-- |[ ========================== GUIMap:fnLocateMapContainingLookup() ========================== ]|
--Given the name of a level, which could be contained in a map's lookups, finds and returns the map
-- that contains that lookup. Returns nil if no map was found.
function GUIMap:fnLocateMapContainingLookup(psName)

	-- |[Arg Check]|
	if(psName == nil) then return nil end

    -- |[Scan]|
    for i = 1, #gzaGUIMaps, 1 do
        if(gzaGUIMaps[i]:fnMapContainsLookup(psName) == true) then
            return gzaGUIMaps[i]
        end
    end
    
    -- |[Not Found]|
    return nil
end

-- |[ =========================== GUIMap:fnRunWarpHandlerOnAllMaps() =========================== ]|
--Orders all maps currently on the global list to attempt to create a warp region.
function GUIMap:fnRunWarpHandlerOnAllMaps(psRoomName)
    for i = 1, #gzaGUIMaps, 1 do
        gzaGUIMaps[i]:fnHandleWarp()
        gzaGUIMaps[i]:fnSendWarpDestinationsToWarpList(psRoomName)
    end
end

-- |[ =========================== GUIMap:fnRunPinHandlerOnAllMaps() ============================ ]|
--Orders all maps currently on the global list to place map pins if the player is in a room on
-- their lookup list.
function GUIMap:fnRunPinHandlerOnAllMaps(psLevelName)
    for i = 1, #gzaGUIMaps, 1 do
        gzaGUIMaps[i]:fnHandleMapPinsForRoom(psLevelName)
    end
end
