-- |[ ===================================== GUIMap Lookups ===================================== ]|
--Lookups are instances on the map where something can be, typically a room the player can enter
-- and then check the map to see where they are.

-- |[ =============================== GUIMap:fnRegisterLookup() ================================ ]|
--Registers a new lookup.
function GUIMap:fnRegisterLookup(psName, psUnlockVar, pfX, pfY)
    
    -- |[Argument Check]|
    if(psName      == nil) then return end
    if(psUnlockVar == nil) then return end
    if(pfX         == nil) then return end
    if(pfY         == nil) then return end
    
    -- |[Create]|
    local zEntry = {}
    zEntry.sName    = psName
    zEntry.sVarPath = psUnlockVar
    zEntry.fXOffset = pfX
    zEntry.fYOffset = pfY
    
    -- |[Register]|
    table.insert(self.zaLookups, zEntry)
end

-- |[ ============================== GUIMap:fnMapContainsLookup() ============================== ]|
--Returns true if this object has the named lookup, false if not.
function GUIMap:fnMapContainsLookup(psName)
    
    -- |[Argument Check]|
    if(psName == nil) then return false end
    
    -- |[Scan]|
    local iSlot = self:fnGetSlotOfLookup(psName)
    if(iSlot ~= -1) then return true end

    -- |[Not Found]|
    return false
end

-- |[ =============================== GUIMap:fnGetSlotOfLookup() =============================== ]|
--Returns the slot that the given named lookup is in, or -1 if not found.
function GUIMap:fnGetSlotOfLookup(psName)
    
    -- |[Argument Check]|
    if(psName == nil) then return -1 end
    
    -- |[Scan]|
    for i = 1, #self.zaLookups, 1 do
        if(self.zaLookups[i].sName == psName) then return i end
    end
    
    -- |[Not Found]|
    return -1
end

-- |[ ================================ GUIMap:fnHandleUnlock() ================================= ]|
--When the player enters the given level, then this function is called to handle setting the 
-- associated variable and revealing a layer of the map.
function GUIMap:fnHandleUnlock(psLevelName)
    
    -- |[Argument Check]|
    if(psLevelName == nil) then return end
    
    -- |[Get Slot]|
    local iLookupSlot = self:fnGetSlotOfLookup(psLevelName)
    if(iLookupSlot == -1) then return end
    
    -- |[Set Variable]|
    --Get the variable.
    local sVariablePath = self.zaLookups[iLookupSlot].sVarPath
    
    --If the variable is "No Set" we don't need to do anything, this level doesn't unlock a layer.
    if(sVariablePath == "No Set") then return end
    
    --Set the flag to 1.0.
    VM_SetVar(sVariablePath, "N", 1.0)
end