-- |[ ================================== GUIMap Render Upload ================================== ]|
--When the C++ needs to render, it runs through the GUIMap interface which renders image layers. These
-- functions upload that information to the C++ state. While many maps have unique parts, most can
-- be handled by these functions.

-- |[ ================================== GUIMap:fnHandleMap() ================================== ]|
--Uploads information to the C++ state so it can render the map layer, which exists for any room
-- the player can press the menu button in and check the map.
function GUIMap:fnHandleMap(psLevelName)
    
    -- |[ ============== Setup =============== ]|
    -- |[Arg Check]|
    if(psLevelName == nil) then return end
    
    -- |[Clear]|
    --Remove any data already in the C++ state.
    AM_SetProperty("Clear Advanced Map")
    
    -- |[ ============== Pieces ============== ]|
    -- |[Base]|
    --Always visible, the base underlies the map.
    if(self.sBaseDLPath ~= "Null") then
        AM_SetProperty("Create Advanced Map Layer", "Base", self.sBaseDLPath)
    end
    
    -- |[Pieces]|
    --Overlay all discovered pieces atop the map. None of the patterns are allowed to be "Null".
    if(self.sVariablePattern ~= "Null" and self.sPiecePattern ~= "Null") then
        for i = 0, self.iPieces, 1 do
            
            --Get the variable path and the map piece path.
            local sLayerName    = string.format(self.sLayerPattern, i)
            local sVariablePath = string.format(self.sVariablePattern, i)
            local sPiecePath    = string.format(self.sPiecePattern, i)

            --If the variable is 1, place the piece. If the variable path is "TRUE" then always render it.
            local iVariable = VM_GetVar(sVariablePath, "N")
            if(iVariable == 1.0 or sVariablePath == "TRUE") then
                AM_SetProperty("Create Advanced Map Layer", sLayerName, sPiecePath)
                AM_SetProperty("Layer Writes Stencil", sLayerName, 10)
            end
        end
    end
    
    -- |[ ======== Connections Layer ========= ]|
    --An optional layer that uses stencils.
    if(self.sConnectionDLPath ~= "Null") then
        AM_SetProperty("Create Advanced Map Layer", "Connections", self.sConnectionDLPath)
        AM_SetProperty("Layer Reads Stencil", "Connections", 10)
        AM_SetProperty("Layer Is Toggleable", "Connections", true)
    end
    
    -- |[ ========= Player Indicator ========= ]|
    --Where the player is. On a per-map basis. The lookup table should have already set coordinates.
    for i = 1, #self.zaLookups, 1 do
        if(self.zaLookups[i].sName == psLevelName) then
            AM_SetProperty("Create Advanced Map Layer", "LayerP", "Null")
            AM_SetProperty("Layer Renders Player",      "LayerP", "Root/Images/AdvMaps/MapIndicators/Mei_Human", true, self.zaLookups[i].fXOffset, self.zaLookups[i].fYOffset)
        end
    end
    
    -- |[ ============= Overlay ============== ]|
    --Overlays can be added over the player's indicator. They are optional.
    if(self.sOverlayDLPath ~= "Null") then
        AM_SetProperty("Create Advanced Map Layer", "Overlay", self.sOverlayDLPath)
    end
    
    self:fnHandleMapPinsForRoom(psLevelName)
end

-- |[ ================================= GUIMap:fnHandleWarp() ================================== ]|
--Uploads information to the C++ state so it can render the warping information.
function GUIMap:fnHandleWarp()
    
    -- |[Region Setup]|
    --Constants
    local cfSizeFactor = 1.0 / 1.5
    local cfScreenWid  = 1366.0
    local cfScreenHei  =  768.0
    local cfMapWid     = 2732.0
    local cfMapHei     = 1536.0
    
    --Creation
    AM_SetProperty("Register Warp Region",            self.sLocalName, self.sCampfireWarpIconPath, "Advanced", "Null")
    AM_SetProperty("Set Warp Region Alignments",      self.sLocalName,  0.0, 0.0, 0.0, 0.0, 90.0, 90.0)
    AM_SetProperty("Set Warp Region Advanced Clamps", self.sLocalName, 0, 0, -(cfMapWid * cfSizeFactor) + cfScreenWid, -(cfMapHei * cfSizeFactor) + cfScreenHei)

    -- |[Base Layer]|
    if(self.sBaseDLPath ~= "Null") then
        AM_SetProperty("Register Warp Region Advanced Pack", self.sLocalName, "Base", self.sBaseDLPath)
    end

    -- |[Pieces]|
    --Overlay all discovered pieces atop the map.
    if(self.sVariablePattern ~= "Null" and self.sPiecePattern ~= "Null") then
        for i = 0, self.iPieces, 1 do
            
            --Get the variable path and the map piece path.
            local sLayerName    = string.format(self.sLayerPattern, i)
            local sVariablePath = string.format(self.sVariablePattern, i)
            local sPiecePath    = string.format(self.sPiecePattern, i)

            --If the variable is 1, place the piece.
            local iVariable = VM_GetVar(sVariablePath, "N")
            if(iVariable == 1.0 or sVariablePath == "TRUE") then
                AM_SetProperty("Register Warp Region Advanced Pack",         self.sLocalName, sLayerName, sPiecePath)
                AM_SetProperty("Warp Region Advanced Render Writes Stencil", self.sLocalName, sLayerName, 10)
            end
        end
    end

    -- |[Connections Layer]|
    --An optional layer that uses stencils.
    if(self.sConnectionDLPath ~= "Null") then
        AM_SetProperty("Register Warp Region Advanced Pack",        self.sLocalName, "Connections", self.sConnectionDLPath)
        AM_SetProperty("Warp Region Advanced Render Reads Stencil", self.sLocalName, "Connections", 10)
        AM_SetProperty("Warp Region Advanced Render Can Toggle",    self.sLocalName, "Connections", true)
    end
end