-- |[ ====================================== GUIMap Warp ======================================= ]|
--Functions related to the player entering warp mode.

-- |[ =========================== GUIMap:fnRegisterWarpDestination() =========================== ]|
--Registers warp destinations associated with this map object.
function GUIMap:fnRegisterWarpDestination(psDisplayName, psRoomName, psUnlockVarPath, pfMapX, pfMapY)

    -- |[Argument Check]|
    if(psDisplayName   == nil) then return end
    if(psRoomName      == nil) then return end
    if(psUnlockVarPath == nil) then return end
    --pfMapX is optional
    --pfMapY is optional

    -- |[Create]|
    local zEntry = {}
	zEntry.sRegionName   = self.sLocalName
	zEntry.sDisplayName  = psDisplayName
	zEntry.sRoomName     = psRoomName
	zEntry.sVariablePath = psUnlockVarPath
	zEntry.sMapName      = "No Map"
	zEntry.fMapX         = 0.0
	zEntry.fMapY         = 0.0

    -- |[Find Coordinates]|
    --If the mapx/y positions were not provided, check the lookups for the room name. If it's found, use those
    -- coordinates. The user can input manual coordinates to improve accuracy.
    if(pfMapX == nil or pfMapY == nil) then
        
        --Check the slot.
        local iCheckSlot = self:fnGetSlotOfLookup(psRoomName)
        if(iCheckSlot ~= -1) then
            zEntry.fMapX = self.zaLookups[iCheckSlot].fXOffset
            zEntry.fMapY = self.zaLookups[iCheckSlot].fYOffset
        end

    --Otherwise, place them directly.
    else
        zEntry.fMapX = pfMapX
        zEntry.fMapY = pfMapY
    end

    -- |[Register]|
    table.insert(self.zaWarpDestinations, zEntry)
end

-- |[ ======================= GUIMap:fnSendWarpDestinationsToWarpList() ======================== ]|
--Sends all warp destinations to the global warp list. Used during warp assembly.
function GUIMap:fnSendWarpDestinationsToWarpList(psCurrentRoomName)

    -- |[Argument Check]|
    if(psCurrentRoomName == nil) then return end
    
    -- |[Iterate]|
    for i = 1, #self.zaWarpDestinations, 1 do
        
        --Fast-access.
        local zEntry = self.zaWarpDestinations[i]
        
        --Upload.
        fnAddWarpEntry(self.sLocalName, zEntry.sDisplayName, psCurrentRoomName, zEntry.sRoomName, zEntry.sVariablePath, zEntry.fMapX, zEntry.fMapY)
    end
end