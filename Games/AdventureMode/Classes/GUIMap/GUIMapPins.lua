-- |[ ====================================== GUIMap Pins ======================================= ]|
--Handles registering and resolving map pins for this map.

-- |[ ============================= GUIMap:fnCreateMapPinObject() ============================== ]|
--Creates and returns a MapPinSet object, which is registered into this object. If one already existed
-- then it gets removed.
function GUIMap:fnCreateMapPinObject()
    self.zMapPins = MapPinSet:new("Internal Pin Set")
    return self.zMapPins
end

-- |[ ============================ GUIMap:fnHandleMapPinsForLevel() ============================ ]|
--Given the name of the level the player is currently in, resolves which map pins should be present
-- if the room is handled by this map object. If any pins are needed, uploads them to the C++ state.
function GUIMap:fnHandleMapPinsForRoom(psLevelName)
    
    -- |[Argument Check]|
    if(psLevelName == nil) then return end
    
    -- |[No Map Pins]|
    --Do nothing if we don't have a map pin object.
    if(self.zMapPins == nil) then return end
    
    -- |[Map Is Handled]|
    --Check if the level in question is in this map object. If not, stop.
    if(self:fnMapContainsLookup(psLevelName) == false) then return end
    
    -- |[Handle]|
    self.zMapPins:fnPopulatePins()
end