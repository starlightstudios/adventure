-- |[ =================================== GUIMap Class File ==================================== ]|
--Class file for GUIMap. Call this once at initialization.

--This stores information needed to create the in-game map, which can be seen when the player pauses
-- and checks the map, or when the player uses warping at a campfire.

-- |[ =========== Globals/Statics ============ ]|
-- |[Global List]|
--Global list that stores all GUIMap objects. A single chapter can have multiple maps.
gzaGUIMaps = {}

--Stores the name of the last object that handled setting map information. Can be nil.
gsLastGUIMap = nil

-- |[ ============ Class Members ============= ]|

-- |[System]|
GUIMap = {}
GUIMap.__index = GUIMap
GUIMap.sLocalName = "Default"

-- |[Map Variables]|
GUIMap.iPieces           = 0                      --How many layers the map has, other than base/overlay/connection. Starts at 0.
GUIMap.sBaseDLPath       = "Null"                 --DLPath to the image used for the base layer.
GUIMap.sLayerPattern     = "Layer%02i"            --Used to resolve the layer name, with the current piece number passed in.
GUIMap.sVariablePattern  = "Null"                 --Used to resolve the layer variable, with the current piece number passed in.
GUIMap.sPiecePattern     = "Null"                 --Used to resolve the image associated with the layer, with the current piece number passed in.
GUIMap.sConnectionDLPath = "Null"                 --DLPath to the image used for the connection layer.
GUIMap.sOverlayDLPath    = "Null"                 --DLPath to the image used for the overlay layer.

-- |[Warp Variables]|
GUIMap.sCampfireWarpIconPath = "Null"
GUIMap.zaWarpDestinations = {}
        
-- |[Lookups]|
GUIMap.zaLookups = {}

-- |[Map Pins]|
GUIMap.zMapPins = nil

-- |[ ============ Class Methods ============= ]|
--System
function GUIMap:new(psName) end

--Global List
function GUIMap:fnClearGlobalList()                   end --Static
function GUIMap:fnRegisterNewMap(psName)              end --Static
function GUIMap:fnLocateMap(psName)                   end --Static
function GUIMap:fnLocateMapContainingLookup(psName)   end --Static
function GUIMap:fnRunWarpHandlerOnAllMaps(psRoomName) end --Static
function GUIMap:fnRunPinHandlerOnAllMaps(psLevelName) end --Static

--Lookups
function GUIMap:fnRegisterLookup(psName, psUnlockVar, pfX, pfY) end
function GUIMap:fnMapContainsLookup(psName)                     end
function GUIMap:fnGetSlotOfLookup(psName)                       end
function GUIMap:fnHandleUnlock(psLevelName)                     end

--Pins
function GUIMap:fnCreateMapPinObject() end

--Render Upload
function GUIMap:fnHandleMap(psLevelName) end
function GUIMap:fnHandleWarp()           end

--Warp Handling
function GUIMap:fnRegisterWarpDestination(psDisplayName, psRoomName, psUnlockVarPath, pfMapX, pfMapY) end
function GUIMap:fnSendWarpDestinationsToWarpList(psCurrentRoomName)                                   end

-- |[ ========== Class Constructor =========== ]|
--Must have a unique name for the map in question.
function GUIMap:new(psName)
    
    -- |[ ========= Setup ========== ]|
    -- |[Argument Check]|
    if(psName == nil) then psName = "Default" end
    
    -- |[Create]|
    local zObject = {}
    setmetatable(zObject, GUIMap)
    
    -- |[ ======== Members ========= ]|
    --Local member.
    zObject.sLocalName = psName
    
    --Reset local lists so they don't use the global class listings.
    zObject.zaWarpDestinations = {}
    zObject.zaLookups = {}
    
    -- |[ ======= Finish Up ======== ]|
    --Return object.
    return zObject
end

-- |[ ========= Call Implementations ========= ]|
LM_ExecuteScript(fnResolvePath() .. "GUIMapListHandling.lua")
LM_ExecuteScript(fnResolvePath() .. "GUIMapLookups.lua")
LM_ExecuteScript(fnResolvePath() .. "GUIMapPins.lua")
LM_ExecuteScript(fnResolvePath() .. "GUIMapRenderUpload.lua")
LM_ExecuteScript(fnResolvePath() .. "GUIMapWarp.lua")
