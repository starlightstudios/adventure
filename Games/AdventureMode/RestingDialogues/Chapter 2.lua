-- |[ ================================ Chapter 2: Rest Dialogues =============================== ]|
--Resting dialogues for chapter 2.

-- |[Variables]|

-- |[ ==================================== Werebat Handling ==================================== ]|
--Werebat advanced.
local iWerebatStage = VM_GetVar("Root/Variables/Chapter2/WerebatTF/iWerebatStage", "N")
if(iWerebatStage == gciWerebatTF_Infected) then
    
    -- |[Block]|
    --If this happens at a point when Izuna or Zeke are not in the party, do nothing.
    local bFoundIzuna = false
    local bFoundZeke = false
    for i = 1, giFollowersTotal, 1 do
        if(gsaFollowerNames[i] == "Izuna") then bFoundIzuna = true end
        if(gsaFollowerNames[i] == "Zeke")  then bFoundZeke  = true end
    end
    if(bFoundIzuna == false or bFoundZeke == false) then return end
    
    -- |[Setup]|
    --Increase state.
    VM_SetVar("Root/Variables/Chapter2/WerebatTF/iWerebatStage", "N", gciWerebatTF_SkinChange)
    
    --Variables.
    local iHasSeenWerebatSkinChange = VM_GetVar("Root/Variables/Chapter2/WerebatTF/iHasSeenWerebatSkinChange", "N")
    local iHasSeenWerebatAlmostTF   = VM_GetVar("Root/Variables/Chapter2/WerebatTF/iHasSeenWerebatAlmostTF", "N")
    local iEmpressJoined            = VM_GetVar("Root/Variables/Chapter2/MtSarulente/iEmpressJoined", "N")

    --Run costume change.
    VM_SetVar("Root/Variables/Costumes/Sanya/sCostumeHuman", "S", "WerebatA")
    LM_ExecuteScript(gsCharacterAutoresolve, "Sanya")
    
    -- |[First Time]|
    if(iHasSeenWerebatSkinChange == 0.0) then
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter2/WerebatTF/iHasSeenWerebatSkinChange", "N", 1.0)
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        fnCutscene([[ Append("Izuna:[E|Neutral] Sanya?[P] Are you feeling all right?[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Smirk] Never felt better.[P] Why do you ask?[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Neutral] You're looking a little pale.[P] Let me check your forehead...[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Neutral] No fever.[P] Hmm...[B][C]") ]])
        if(iEmpressJoined == 1.0) then
            fnCutscene([[ Append("Empress:[E|Neutral] Anemia.[P] You were bleeding earlier.[P] You may have lost too much blood.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Neutral] No, I don't think so.[P] No feelings of weakness.[P] Are you lightheaded?[P] Trouble breathing?[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] I feel great.[B][C]") ]])
        end
        fnCutscene([[ Append("Sanya:[E|Blush] You smell wonderful, by the way.[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Explain] Oh, thank you![P] It's a trick I learned way back when![B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Explain] You can put a bit of perfume in your tail and it releases slowly over time.[P] I always smell great, and it comes out in the bath if you don't like it anymore![B][C]") ]])
        fnCutscene([[ Append("Zeke:[E|Neutral] Bah![B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Blush] Yeah, she smells...[P] delicious...[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Blush] *kiss*[P] You're delicious![B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Smirk] Maybe your blood is just a little thin from the change in diet.[P] I'm sure it's no big deal.[P] Back to the adventure!") ]])
        fnCutsceneBlocker()
    
    -- |[Saw Next Stage]|
    elseif(iHasSeenWerebatAlmostTF == 1.0) then
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        fnCutscene([[ Append("Izuna:[E|Neutral] Seems the werebat curse is setting in again.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Smirk] Cool![P] I can hear things a little better and definitely smell better.[P] No complaints!") ]])
        fnCutsceneBlocker()

    -- |[Repeats But Not Previous Progression]|
    else
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        fnCutscene([[ Append("Izuna:[E|Neutral] Oh my, Sanya?[P] You're looking pale again.[P] Did you eat something rotten?[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Smirk] Nope.[P] I feel great![B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Neutral] I guess it's probably nothing, but I'll keep an eye on you just in case.") ]])
        fnCutsceneBlocker()
    
    end

--Another advancement.
elseif(iWerebatStage == gciWerebatTF_SkinChange) then
    
    -- |[Block]|
    --If this happens at a point when Izuna or Zeke are not in the party, do nothing.
    local bFoundIzuna = false
    local bFoundZeke = false
    for i = 1, giFollowersTotal, 1 do
        if(gsaFollowerNames[i] == "Izuna") then bFoundIzuna = true end
        if(gsaFollowerNames[i] == "Zeke")  then bFoundZeke  = true end
    end
    if(bFoundIzuna == false or bFoundZeke == false) then return end

    -- |[Setup]|
    --Increase state.
    VM_SetVar("Root/Variables/Chapter2/WerebatTF/iWerebatStage", "N", gciWerebatTF_AlmostTFd)
    
    --Variables.
    local iHasSeenWerebatAlmostTF = VM_GetVar("Root/Variables/Chapter2/WerebatTF/iHasSeenWerebatAlmostTF", "N")
    
    --Run costume change.
    VM_SetVar("Root/Variables/Costumes/Sanya/sCostumeHuman", "S", "WerebatB")
    LM_ExecuteScript(gsCharacterAutoresolve, "Sanya")
    
    -- |[First Time]|
    if(iHasSeenWerebatAlmostTF == 0.0) then
        
        --Flag.
        VM_SetVar("Root/Variables/Chapter2/WerebatTF/iHasSeenWerebatAlmostTF", "N", 1.0)
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        fnCutscene([[ Append("Izuna:[E|Cry] Sanya?[P] Oh my goodness![P] What happened?[P] Are you okay?[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Blush] Feeling fine.[P] Hm?[P] Oh my, what happened to my hair?[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Blush] Am I turning into you?[P] Cool![B][C]") ]])
        fnCutscene([[ Append("Zeke:[E|Neutral] Nyeh![P] Nyeh![B][C]") ]])
        if(iEmpressJoined == 1.0) then
            fnCutscene([[ Append("Empress:[E|Neutral] It's a werebat curse. Have you seen any batlike monstergirls? When were you bitten?[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] I think I'd know if I saw one of those.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Neutral] ...[P] Unless the bats in Trafal have the curse too?[B][C]") ]])
        else
            fnCutscene([[ Append("Izuna:[E|Cry] No you silly human, you're -[P] it must be a werebat curse![P] You must have gotten bitten![B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] What's a werebat?[P] Actually, scratch that, I think I can guess.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Neutral] But we haven't seen any werebats...[P] unless the bats in Trafal have the curse too?[B][C]") ]])
        end
        fnCutscene([[ Append("Sanya:[E|Happy] C'mere![P] Let me suck your blood![P] You smell delicious![B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Ugh] Eek![P] Sanya, get a hold of yourself![B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Happy] But you smell so tasty...[P] especially...[P] your tail...[B][C]") ]])
        fnCutscene([[ Append("Zeke:[E|Neutral] [SOUND|World|Thump]BAH![B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Offended] Ouch![B][C]") ]])
        if(iEmpressJoined == 1.0) then
            fnCutscene([[ Append("Empress:[E|Neutral] Zeke, stop.[P] She is not dangerous.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Sad] I am extremely dangerous, but Zeke was right to give me a headbutt.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] His understanding of morality is very advanced.[P] A headbutt means he's very certain.[B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] You're currently transforming into a creature of the night.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Ugh] Hmm, I have an idea.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Explain] My 'purify' spell should clear the problem up![B][C]") ]])
            fnCutscene([[ Append("Empress:[E|Neutral] She can use her runestone.[P] There is no issue at hand.[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Smirk] Plus I look really cool like this.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Smirk] I suppose you do.[P] It's there if you change your mind.[P] And stay away from my tail, okay?[B][C]") ]])
        else
            fnCutscene([[ Append("Sanya:[E|Sad] S-[P]sorry.[P] I just wanted to have a nibble...[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Cry] I'm cursed to become a bloodsucking creature of the night![P] No![P] Can't someone save me?[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Explain] I can - [P][CLEAR]") ]])
            fnCutscene([[ Append("Sanya:[E|Neutral] It's a joke, Izuna.[P] I can use my runestone to transform.[P] Might be useful to have a new form, right?[B][C]") ]])
            fnCutscene([[ Append("Sanya:[E|Blush] I promise not to suck your blood unless you really want me to.[B][C]") ]])
            fnCutscene([[ Append("Izuna:[E|Neutral] All right, I suppose it's not a problem.[P] But if you change your mind, my spell 'Purify' should be able to remove the curse.[B][C]") ]])
        end
        fnCutscene([[ Append("Sanya:[E|Blush] Are you sure I can't just a little nibble?[B][C]") ]])
        fnCutscene([[ Append("Zeke:[E|Neutral] NYEH![B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Sad] Fine![P] Jeez!") ]])
        fnCutsceneBlocker()
    
    -- |[Repeats]|
    else
        
        --Dialogue.
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
        fnCutscene([[ Append("Izuna:[E|Jot] Oh, maybe I should ask some questions![P] How does your hearing and smell feel relative to normal?[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Blush] Let's see...[P] I guess you could say they're sharper.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Smirk] When Zeke adjusts his stance, I can hear a little creak from the joints moving.[B][C]") ]])
        if(iEmpressJoined == 1.0) then
            fnCutscene([[ Append("Empress:[E|Neutral] The enhanced senses, and possibly improved speed, could be of use.[P] Can you hear better at range?[P] Even footsteps?[B][C]") ]])
        else
            fnCutscene([[ Append("Zeke:[E|Neutral] Bah?[B][C]") ]])
        end
        fnCutscene([[ Append("Sanya:[E|Neutral] Yep, even tiny little movements.[P] I could probably hear you sneak into a room just from those.[B][C]") ]])
        fnCutscene([[ Append("Sanya:[E|Neutral] I can smell things whenever the wind blows, mostly sweet scents.[P] It's hard to pick out a direction from a distance but up close I could tell exactly how far you are from me, Izuna.[B][C]") ]])
        fnCutscene([[ Append("Izuna:[E|Jot] Fascinating![P] You don't usually get to interview someone who can transform more than once!") ]])
        fnCutsceneBlocker()

    end

--Finalized.
elseif(iWerebatStage == gciWerebatTF_AlmostTFd) then
    
    -- |[Block]|
    --If this happens at a point when Izuna or Zeke are not in the party, do nothing.
    local bFoundIzuna = false
    local bFoundZeke = false
    for i = 1, giFollowersTotal, 1 do
        if(gsaFollowerNames[i] == "Izuna") then bFoundIzuna = true end
        if(gsaFollowerNames[i] == "Zeke")  then bFoundZeke  = true end
    end
    if(bFoundIzuna == false or bFoundZeke == false) then return end

    --Transform Sanya.
    LM_ExecuteScript(gsRoot .. "FormHandlers/Sanya/Form_Werebat.lua")

    --Reset the TF variables.
    VM_SetVar("Root/Variables/Chapter2/WerebatTF/iWerebatStage", "N", gciWerebatTF_FullyTFd) --Prevents being reinfected later
    VM_SetVar("Root/Variables/Costumes/Sanya/sCostumeHuman", "S", "Rifle")
    
    --Dialogue.
    fnCutscene([[ WD_SetProperty("Show") ]])
    fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
    fnCutscene([[ Append("Sanya:[E|Neutral] At last![P] I have become the night![B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Ugh] I suppose this is a good thing...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] You don't like it?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Ugh] Night creatures are...[P] weird.[P] I don't like them.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] But maybe I just need to get used to one, right?[P] You're not going to suck my blood, right?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] No...[P] I want to eat you whole![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Wait -[P] just your tail.[P] I'll eat it whole![B][C]") ]])
    fnCutscene([[ Append("Zeke:[E|Neutral] [SOUND|World|Thump]BAH NYEH!![B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Smirk] Zeke stop![P] Let her smell it![B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] It smells so good, I'm so hungry...[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] Does it smell like cherries?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Yeah.[P] Does blood smell like cherries when you're a werebat?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Explain] No but the cherry extract in my perfume does![P] Werebats don't even suck blood, do they?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] We don't?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Explain] Only a few bats actually suck blood, most just eat fruit, or insects.[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Ew, no on the insects.[P] But a big juicy honeydew melon...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Or an entire plate of grapes...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] ...[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] Well that's lame, I wanted to suck blood.[P] I got fangs and everything![B][C]") ]])
    if(iEmpressJoined == 1.0) then
        fnCutscene([[ Append("Empress:[E|Neutral] You could have sucked blood as a human, you know.[B][C]") ]])
    else
        fnCutscene([[ Append("Izuna:[E|Smirk] You still...[P] can suck blood if you want.[P] You could do that as a human, too.[B][C]") ]])
    end
    fnCutscene([[ Append("Sanya:[E|Neutral] Good, I'm going to suck blood even if my base nature doesn't want me to.[P] This is on principle, damn it.[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Neutral] And what principle is that?[B][C]") ]])
    fnCutscene([[ Append("Sanya:[E|Neutral] The principle of sucking blood for it's own sake![P] I'll drain you dry![P] Nobody tells me who I can suck![B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Blush] That's my girl![P] *kiss*[B][C]") ]])
    if(iEmpressJoined == 1.0) then
        fnCutscene([[ Append("Empress:[E|Neutral] (Ah, young love.)[B][C]") ]])
    end
    fnCutscene([[ Append("Sanya:[E|Neutral] Not so scared of night creatures anymore, are you?[B][C]") ]])
    fnCutscene([[ Append("Izuna:[E|Blush] Maybe a little, but that just makes it more fun![P] Now, back to the adventure!") ]])
    fnCutsceneBlocker()

end
