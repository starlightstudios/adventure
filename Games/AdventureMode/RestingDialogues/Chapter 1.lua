-- |[ ================================ Chapter 1: Rest Dialogues =============================== ]|
--Resting dialogues for chapter 1.

-- |[Variables]|
--Whether or not Florentina is in the party.
local bIsFlorentinaPresent = false
if(giFollowersTotal > 0) then bIsFlorentinaPresent = true end

--Script variables.
local iCombatVictories               = VM_GetVar("Root/Variables/Chapter1/Counts/iCombatVictories", "N")
local iVictoriesWhenFlorentinaJoined = VM_GetVar("Root/Variables/Chapter1/Counts/iVictoriesWhenFlorentinaJoined", "N")

--Cutscene flags.
local iUseSpecialBeeOpening       = VM_GetVar("Root/Variables/Chapter1/Scenes/iUseSpecialBeeOpening", "N")
local iHasSeenFlorentinaOneWin    = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenFlorentinaOneWin", "N")
local iHasSeenFlorentinaFiveWin   = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenFlorentinaFiveWin", "N")
local iHasSeenFlorentinaTwentyWin = VM_GetVar("Root/Variables/Chapter1/Scenes/iHasSeenFlorentinaTwentyWin", "N")
local iStartedCassandraEvent      = VM_GetVar("Root/Variables/Chapter1/Scenes/iStartedCassandraEvent", "N")

--Disable all cutscenes during the rubber sequence.
local iIsRubberMode= VM_GetVar("Root/Variables/Chapter1/Scenes/Rubber|iIsRubberMode", "N")
if(iIsRubberMode == 1.0) then return end

-- |[ ====================================== Mannequin TF ====================================== ]|
--Check if we can begin the mannequin TF sequence. If so, lock everything else out.
local iBeganSequence = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iBeganSequence", "N")
if(iBeganSequence == 0.0 and iStartedCassandraEvent ~= 1.0) then
    
    --Mei or Florentina needs to be wearing one of the cursed items.
    AdvCombat_SetProperty("Push Party Member", "Mei")
        local sMeiAccessoryA = AdvCombatEntity_GetProperty("Equipment In Slot S", "Accessory A")
        local sMeiAccessoryB = AdvCombatEntity_GetProperty("Equipment In Slot S", "Accessory B")
    DL_PopActiveObject()
    AdvCombat_SetProperty("Push Party Member", "Florentina")
        local sFloAccessoryA = AdvCombatEntity_GetProperty("Equipment In Slot S", "Accessory A")
        local sFloAccessoryB = AdvCombatEntity_GetProperty("Equipment In Slot S", "Accessory B")
    DL_PopActiveObject()
    
    --Player must also have spoken to Victoria. This prevents NC+ problems.
    local iMetVictoriaInVillage = VM_GetVar("Root/Variables/Chapter1/Scenes/Mann|iMetVictoriaInVillage", "N")
    
    --If any of the slots matches, activate the scene.
    if(iMetVictoriaInVillage == 1.0 and (sMeiAccessoryA == "Diamond Bracelet" or sMeiAccessoryB == "Diamond Bracelet" or sFloAccessoryA == "Ruby Necklace" or sFloAccessoryB == "Ruby Necklace")) then
        fnCutsceneFadeOut()
        AM_SetProperty("Close Menu Now")
        AL_SetProperty("End Resting Now")
        AL_BeginTransitionTo("BanditForestScene", "FORCEPOS:1.0x1.0x0")
        return
    end
end

-- |[ ===================================== Scene Handlers ===================================== ]|
--We need to determine whether or not a resting dialogue needs to play out at all. If so, a special
-- bypass flag gets set. Alternately, a random roll of 5% causes a scene to play out.
local bBypassRoll = false

--5% chance of playing a scene.
local iRoll = LM_GetRandomNumber(1, 100)

-- |[Special Handlers]|
--Florentina asks Mei about the bee thing. She must have been in the party and Mei did a voluntary bee TF.
if(iUseSpecialBeeOpening == 1.0) then
    bBypassRoll = true

--If the player needs to see the Florentina one-win cutscene.
elseif(iHasSeenFlorentinaOneWin == 0.0 and iCombatVictories > iVictoriesWhenFlorentinaJoined) then
    bBypassRoll = true

--If the player needs to see the Florentina five-win cutscene.
elseif(iHasSeenFlorentinaFiveWin == 0.0 and iHasSeenFlorentinaOneWin ~= 0.0 and iCombatVictories >= iHasSeenFlorentinaOneWin + 5) then
    bBypassRoll = true

--If the player needs to see the Florentina twenty-win cutscene.
elseif(iHasSeenFlorentinaTwentyWin == 0.0 and iHasSeenFlorentinaFiveWin ~= 0.0 and iHasSeenFlorentinaOneWin ~= 0.0 and iCombatVictories >= iHasSeenFlorentinaFiveWin + 15) then
    bBypassRoll = true
end

--If the bypass is present, or a scene is mandated, proceed.
if(iRoll > 5 and bBypassRoll == false) then
	return
end

-- |[ ==================================== Mei Alone Scenes ==================================== ]|
--Scenes when Mei is alone.
if(bIsFlorentinaPresent == false) then

-- |[ ==================================== Florentina Scenes =================================== ]|
--Scenes for when Florentina is in the party.
else

	--Setup.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Florentina", "Neutral") ]])

    -- |[Voluntary Bee TF]|
	--Florentina has a few questions about the voluntary bee TF.
	if(iUseSpecialBeeOpening == 1.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iUseSpecialBeeOpening", "N", 2.0)
		
		--Dialogue.
		fnCutscene([[ Append("Mei:[E|Blush] ...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] ...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] Hey, Mei![B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Huh?[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] Back there in the bee hive.[P] Why'd you eat the honey?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] I just -[P] I don't know.[P] Something came over me.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] I wanted it more than anything else, ever.[P] If you'd tried to keep me from it -[P] I'd have struck you...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Confused] Feh. [P]Probably the bee buzzing got to you.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] No, that wasn't it.[P] Once I remembered myself, that was the first thing I asked the other drones.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] They said they hadn't done anything to me.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] And that's exactly what they would say, isn't it?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Sad] They...[P] we...[P] are incapable of lying to each other.[P] It's like lying to yourself.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] People lie to themselves all the time.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] I still know they did not influence me.[P] I did it of my own accord.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Blush] If I could do it again I would...[P] I was fantasizing about it until you spoke up.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Surprise] Wow...[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] Well, in the future, please listen to me when I tell you not to drink strange fluids.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] No promises.[P] Besides, I'm fine now, right?") ]])
        fnCutsceneBlocker()
        return
    end

    -- |[Florentina: One Combat Victory]|
	--The two bond over violence.
	if(iHasSeenFlorentinaOneWin == 0.0 and iCombatVictories > iVictoriesWhenFlorentinaJoined) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenFlorentinaOneWin", "N", iCombatVictories)
		
		--Dialogue.
		fnCutscene([[ Append("Florentina:[E|Neutral] So you're saying you're not a fighter, huh?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] I swear.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] I guess some people are just naturally good at this.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] You're not bad either.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] I can handle myself.[P] Just don't get cocky.") ]])
        fnCutsceneBlocker()
        return
    end
	
    -- |[Florentina: Five Combat Victories]|
	--She's starting to dig your prowiss, your capacity for violence.
	if(iHasSeenFlorentinaFiveWin == 0.0 and iHasSeenFlorentinaOneWin ~= 0.0 and iCombatVictories >= iHasSeenFlorentinaOneWin + 5) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenFlorentinaFiveWin", "N", iCombatVictories)
		
		--Dialogue.
		fnCutscene([[ Append("Florentina:[E|Happy] Don't take this the wrong way, but I think I'm starting to like you.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Happy] Is my chipper attitude getting through?[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] More like the sharp end of your sword.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] I love a girl who loves a good beatdown...[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Er, thanks I guess.") ]])
        fnCutsceneBlocker()
        return
    end
	
    -- |[Florentina: Twenty Combat Victories]|
    --Kicking ass is how friendships are made.
	if(iHasSeenFlorentinaTwentyWin == 0.0 and iHasSeenFlorentinaFiveWin ~= 0.0 and iHasSeenFlorentinaOneWin ~= 0.0 and iCombatVictories >= iHasSeenFlorentinaFiveWin + 15) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter1/Scenes/iHasSeenFlorentinaTwentyWin", "N", iCombatVictories)
		
		--Dialogue.
		fnCutscene([[ Append("Florentina:[E|Neutral] Mei?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Yes?[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] You're all right.[P] You've got my back, and that's where it really counts.[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] It's hard to find people you can trust in Pandemonium.[P] At least in a scrap, I can count on you.[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Neutral] Thanks, Florentina.[P] I feel the same way about you.") ]])
        fnCutsceneBlocker()
        return
    end
	
    -- |[Random Dialogue]|
    --Roll.
	local iRoll = LM_GetRandomNumber(0, 5)
    iRoll = 0
    
	--Generic dialogue:
	if(iRoll == 0) then
		fnCutscene([[ Append("Florentina:[E|Neutral] Hey, want to swap scary stories?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Laugh] I'm not scared of anything![B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] Oh really?[B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Happy] Not even [SPOOKY]SKELETONS[NOSPOOKY]?[B][C]") ]])
		fnCutscene([[ Append("Mei:[E|Offended] O-[P]Of course not![P] [SPOOKY]SKELETONS[NOSPOOKY] are n-n-nothing![B][C]") ]])
		fnCutscene([[ Append("Florentina:[E|Neutral] I'm getting chills just thinking about them.[P] Let's just get going, okay?") ]])
        fnCutsceneBlocker()
	end
end
