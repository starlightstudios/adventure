-- |[ ================================= Chapter 2 Hunt Handler ================================= ]|
--Called whenever the player rests or otherwise executes a rest action (such as through a cutscene).
-- This resets and regenerates the hunts in the chapter.

-- |[Activation Check]|
--If the player has not completed the tutorial hunt, do nothing.
local iCompletedHunt = VM_GetVar("Root/Variables/Chapter2/Northwoods/iCompletedHunt", "N")
if(iCompletedHunt == 0.0) then return end

-- |[Call Hunt Construction]|
Hunting:fnGenerateHunt()
