-- |[ ====================================== Rest Resolve ====================================== ]|
--Resolve what to do during a rest sequence. This can involve firing cutscenes, dialogues, or modifying variables.
-- Note that this script fires *after* the variables in the RestReset category are reset!
--This also resets the paragon code.
VM_SetVar("Root/Variables/Global/Paragons/iParagonRoll", "N", LM_GetRandomNumber(-10000, 10000))

--Level name. In "Nowhere", nothing happens.
local sCurrentLevel = AL_GetProperty("Name")
if(sCurrentLevel == "Nowhere") then return end

-- |[Variables]|
local iCurrentChapter = VM_GetVar("Root/Variables/Global/ChapterComplete/iCurrentChapter", "N")

-- |[Chapter 1]|
if(iCurrentChapter == 1.0) then
    LM_ExecuteScript(fnResolvePath() .. "Chapter 1.lua")
    return

-- |[Chapter 2]|
elseif(iCurrentChapter == 2.0) then

    --Basic script.
    LM_ExecuteScript(fnResolvePath() .. "Chapter 2.lua")
    
    --Reset hunts. This never spawns dialogue.
    LM_ExecuteScript(fnResolvePath() .. "Chapter 2 Hunt Handler.lua")
    return

-- |[Chapter 5]|
elseif(iCurrentChapter == 5.0) then
    return
end

-- |[Mods]|
--If none of the above cases handled the call, check if any of the modkit scripts did.
fnExecModScript("Scenes/200 Rest Handler.lua")
