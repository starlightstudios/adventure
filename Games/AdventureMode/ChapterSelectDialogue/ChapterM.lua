-- |[Root]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Initialize]|
--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    -- |[One Mod]|
    --Only one mod exists, ask if they want to play it.
    if(#gsModDirectories == 1) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Maram", "Neutral") ]])
        fnCutscene([[ Append("Modified Chapter.[P] Proceed at your own risk.[B][C]") ]])
        fnCutscene([[ Append("Do you want to play this chapter?[BLOCK]") ]])
        
        --Decision script is this script. It must be surrounded by quotes.
        local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
        
        --Decision mode.
        fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"YesOne\") ")
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"No\") ")
        fnCutsceneBlocker()
    
    -- |[Multiple Mods]|
    --Multiple mods exist. Create a list.
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Maram", "Neutral") ]])
        fnCutscene([[ Append("Several mods have been detected.[P] Remember that modified files are not supported by the Bottled Starlight developers.[B][C]") ]])
        fnCutscene([[ Append("Select a mod to activate.[BLOCK]") ]])
        
        --Decision script is this script. It must be surrounded by quotes.
        local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
        fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
    
        --Iterate.
        for i = 1, #gsModNames, 1 do
            --io.write("Mod Name: " .. gsModNames[i] .. "\n")
            fnCutscene(" WD_SetProperty(\"Add Decision\", \"" .. gsModNames[i] .. "\", " .. sDecisionScript .. ", \"INDEX " .. i .. "\") ")
        end
    
        --Always add a "Cancel" option.
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Cancel\",  " .. sDecisionScript .. ", \"No\") ")
        fnCutsceneBlocker()
    
    end

-- |[One Mod]|
--"YesOne" launches the only mod on the list.
elseif(sTopicName == "YesOne") then
	
	--Common. This is done to clear up any pending blocks from the previous part of the scene.
	WD_SetProperty("Hide")
    
    --Assemble the boot script name.
    local sModBoothPath = gsModDirectories[1] .. "System/000 Mod Setup.lua"
    
    --Call the mod boot script.
    gbIsStartingChapter = true
    LM_ExecuteScript(sModBoothPath)
    gbIsStartingChapter = false

-- |[Activate By Index]|
--If the topic is "INDEX [X]" then the [X] is the index (starting from 1) of the mod to tuse.
elseif(string.sub(sTopicName, 1, 6) == "INDEX ") then
	
	--Common. This is done to clear up any pending blocks from the previous part of the scene.
	WD_SetProperty("Hide")

    --Get the number.
    local sIndexString = string.sub(sTopicName, 7)
    --io.write("Getting index from string: " .. sIndexString .. "\n")
    
    --Turn it into an integer.
    local iIndex = math.floor(tonumber(sIndexString))
    --io.write(" Got index: " .. iIndex .. "\n")
    
    --Range check.
    if(iIndex < 1 or iIndex > #gsModNames) then
        io.write("Error, out of range.\n")
        return
    end
    
    --Assemble the boot script name.
    local sModBoothPath = gsModDirectories[iIndex] .. "System/000 Mod Setup.lua"
    
    --Call the mod boot script.
    gbIsStartingChapter = true
    LM_ExecuteScript(sModBoothPath)
    gbIsStartingChapter = false

-- |[Cancel]|
--"No" stops here.
elseif(sTopicName == "No") then
	
	--Common. This is done to clear up any pending blocks from the previous part of the scene.
	WD_SetProperty("Hide")

end
