-- |[Root]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
	--Reboot the dialogue.
	TA_SetProperty("Face Character", "PlayerEntity")
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Maram", "Neutral") ]])
	fnCutscene([[ Append("Rilmani: Would you like to change a game setting?[BLOCK]") ]])
	
	--Decision script is this script. It must be surrounded by quotes.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	
	--Decision mode.
	fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
	if(gbBypassIntro == false) then
		fnCutscene(" WD_SetProperty(\"Add Decision\", \"Disable Chapter Intro\", " .. sDecisionScript .. ", \"Disable Chapter Intro\") ")
	else
		fnCutscene(" WD_SetProperty(\"Add Decision\", \"Enable Chapter Intro\", " .. sDecisionScript .. ", \"Enable Chapter Intro\") ")
	end
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"No Thanks\",  " .. sDecisionScript .. ", \"No\") ")
	fnCutsceneBlocker()

--Disable the introduction.
elseif(sTopicName == "Disable Chapter Intro") then
	gbBypassIntro = true
	WD_SetProperty("Hide")
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Maram", "Neutral") ]])
	fnCutscene([[ Append("Rilmani: The introduction has been disabled.") ]])

--Enable the introduction.
elseif(sTopicName == "Enable Chapter Intro") then
	gbBypassIntro = false
	WD_SetProperty("Hide")
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Maram", "Neutral") ]])
	fnCutscene([[ Append("Rilmani: The introduction has been re-enabled.") ]])

--"No" stops here.
elseif(sTopicName == "No") then
	WD_SetProperty("Hide")
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Maram", "Neutral") ]])
	fnCutscene([[ Append("Rilmani: Very well.") ]])

end
