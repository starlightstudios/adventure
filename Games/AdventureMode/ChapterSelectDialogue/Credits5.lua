-- |[Root]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    --If we haven't loaded the audio for the credits theme, do that now.
    if(gbLoadedCreditsTheme5 == false) then
        gbLoadedCreditsTheme5 = true
		AudioManager_Register("Credits5", "AsMusic", "AsStream", gsRoot .. "Audio/Music/Credits/Patricia Taxxon - Kissing Ancaps.ogg")
    end
    
	--Reboot the dialogue.
	TA_SetProperty("Face Character", "PlayerEntity")
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 4, "Maram", "Neutral") ]])
	fnCutscene([[ Append("Rilmani: Would you like to view the credits sequence for chapter 5?[BLOCK]") ]])
	
	--Decision script is this script. It must be surrounded by quotes.
	local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
	
	--Decision mode.
	fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
    fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"Show Credits\") ")
	fnCutscene(" WD_SetProperty(\"Add Decision\", \"No Thanks\",  " .. sDecisionScript .. ", \"No\") ")
	fnCutsceneBlocker()

--Show dem creds.
elseif(sTopicName == "Show Credits") then

    --Clean.
	WD_SetProperty("Hide")
    
    --Setup.
    WD_SetProperty("Show")
    WD_SetProperty("Activate Credits", "Root/Images/Scenes/System/CreditsBacking5")
    
    --Timing.
    local iRunningTimer = 0
    local ciTweenPause = 45
    local ciReadingTime = 150
    
    --Positions
    local cfScreenWid = 1366
    local cfScreenHei = 768
    local cfScreenXCenter = cfScreenWid * 0.50
    local cfScreenXLJust = cfScreenWid * 0.35
    local cfScreenXLJustInd = cfScreenXLJust + 25
    local cfScreenXRJust = cfScreenWid * 0.65
    local cfScreenXRJustInd = cfScreenXLJust + 25
    local cfScreenYHeading = cfScreenHei * 0.20
    local cfScreenYBody = cfScreenHei * 0.30
    
    --Fonts and Sizes
    local sOxy27 = "World Dialogue Credits Oxy"
    local sSanchez30 = "World Dialogue Credits Sanchez"
    local fYPad = 37
    local fTitleSize = 2.00
    local fHeadSize = 1.00
    local fLineSize = 0.70
    
    --Builder Functions
    local fnTextFadePack = function(iTimeToStart, iHoldTicks, fX, fY, sText, uiFlags, fScale, sFont)
        WD_SetProperty("Create Credits Pack")
            WD_SetCreditsProperty("Set Time to Start", iTimeToStart)
            WD_SetCreditsProperty("Set Mode As Fades", 30, iHoldTicks, 30)
            WD_SetCreditsProperty("Set Position", fX, fY)
            WD_SetCreditsProperty("Set Text", sText, uiFlags, fScale, sFont)
        DL_PopActiveObject()
    end
    
    -- |[Music]|
    AudioManager_PlayMusic("Null")
    AudioManager_PlayMusic("Credits5")
    
    --Wait a bit to fade the background in.
    iRunningTimer = iRunningTimer + 30
    
    -- |[Title]|
    fnTextFadePack(iRunningTimer,      ciReadingTime,      cfScreenXCenter, cfScreenYHeading,       "Pandemonium: Chapter 5",   gci_Font_AutocenterX, fTitleSize, sOxy27)
    fnTextFadePack(iRunningTimer + 30, ciReadingTime - 30, cfScreenXCenter, cfScreenYHeading+fYPad, "A Bottled Starlight Game", gci_Font_AutocenterX, fHeadSize,  sOxy27)
    fnTextFadePack(iRunningTimer + 30, ciReadingTime - 30, cfScreenXCenter, cfScreenYHeading+fYPad*3, "(Press escape to exit)", gci_Font_AutocenterX, fLineSize,  sOxy27)
    iRunningTimer = iRunningTimer + ciTweenPause + ciReadingTime
    
    -- |[Main Credits]|
    --Heading.
    local ciBlockTime = 600
    fnTextFadePack(iRunningTimer +   0, ciBlockTime,       cfScreenXCenter,   cfScreenYHeading, "Starlight Staff",  gci_Font_AutocenterX, 1.50, sOxy27)
    
    --Block 1.
    fnTextFadePack(iRunningTimer +  30, ciBlockTime -  30, cfScreenXLJust, cfScreenYBody + fYPad * 0, "Project Lead",              gci_Font_AutocenterX, fHeadSize, sOxy27)
    fnTextFadePack(iRunningTimer +  60, ciBlockTime -  60, cfScreenXLJust, cfScreenYBody + fYPad * 1, "Salty Justice",             gci_Font_AutocenterX, fLineSize, sSanchez30)
    fnTextFadePack(iRunningTimer +  30, ciBlockTime -  30, cfScreenXRJust, cfScreenYBody + fYPad * 0, "Inking, Shading, Overlays", gci_Font_AutocenterX, fHeadSize, sOxy27)
    fnTextFadePack(iRunningTimer +  60, ciBlockTime -  60, cfScreenXRJust, cfScreenYBody + fYPad * 1, "Stinkehund",                gci_Font_AutocenterX, fLineSize, sSanchez30)
    
    --Block 2.
    fnTextFadePack(iRunningTimer +  90, ciBlockTime -  90, cfScreenXLJust, cfScreenYBody + fYPad * 3, "Art Director",   gci_Font_AutocenterX, fHeadSize, sOxy27)
    fnTextFadePack(iRunningTimer + 120, ciBlockTime - 120, cfScreenXLJust, cfScreenYBody + fYPad * 4, "Chickenwhite",   gci_Font_AutocenterX, fLineSize, sSanchez30)
    fnTextFadePack(iRunningTimer +  90, ciBlockTime -  90, cfScreenXRJust, cfScreenYBody + fYPad * 3, "Concept Artist", gci_Font_AutocenterX, fHeadSize, sOxy27)
    fnTextFadePack(iRunningTimer + 120, ciBlockTime - 120, cfScreenXRJust, cfScreenYBody + fYPad * 4, "Koops",          gci_Font_AutocenterX, fLineSize, sSanchez30)
    
    --Block 3.
    fnTextFadePack(iRunningTimer + 150, ciBlockTime - 150, cfScreenXLJust, cfScreenYBody + fYPad * 6, "Sprites, Tiles",   gci_Font_AutocenterX, fHeadSize, sOxy27)
    fnTextFadePack(iRunningTimer + 180, ciBlockTime - 180, cfScreenXLJust, cfScreenYBody + fYPad * 7, "Urimas Ebonheart", gci_Font_AutocenterX, fLineSize, sSanchez30)
    fnTextFadePack(iRunningTimer + 150, ciBlockTime - 150, cfScreenXRJust, cfScreenYBody + fYPad * 6, "Writing, Editing", gci_Font_AutocenterX, fHeadSize, sOxy27)
    fnTextFadePack(iRunningTimer + 180, ciBlockTime - 180, cfScreenXRJust, cfScreenYBody + fYPad * 7, "MarioneTTe",       gci_Font_AutocenterX, fLineSize, sSanchez30)
    iRunningTimer = iRunningTimer + ciTweenPause + ciBlockTime

    -- |[Second Block]|
    --All the stuff Salty does. Holy cow.
    fnTextFadePack(iRunningTimer +  30, ciBlockTime -  30, cfScreenXLJust, cfScreenYBody + fYPad * 0, "Starlight Engine", gci_Font_AutocenterX, fHeadSize, sOxy27)
    fnTextFadePack(iRunningTimer +  60, ciBlockTime -  60, cfScreenXLJust, cfScreenYBody + fYPad * 1, "Salty Justice",    gci_Font_AutocenterX, fLineSize, sSanchez30)
    fnTextFadePack(iRunningTimer +  90, ciBlockTime -  90, cfScreenXRJust, cfScreenYBody + fYPad * 0, "Story by",         gci_Font_AutocenterX, fHeadSize, sOxy27)
    fnTextFadePack(iRunningTimer + 120, ciBlockTime - 120, cfScreenXRJust, cfScreenYBody + fYPad * 1, "Salty Justice",    gci_Font_AutocenterX, fLineSize, sSanchez30)
    
    fnTextFadePack(iRunningTimer + 150, ciBlockTime - 150, cfScreenXLJust, cfScreenYBody + fYPad * 3, "Sound Mixing",  gci_Font_AutocenterX, fHeadSize, sOxy27)
    fnTextFadePack(iRunningTimer + 180, ciBlockTime - 180, cfScreenXLJust, cfScreenYBody + fYPad * 4, "Salty Justice", gci_Font_AutocenterX, fLineSize, sSanchez30)
    fnTextFadePack(iRunningTimer + 210, ciBlockTime - 210, cfScreenXRJust, cfScreenYBody + fYPad * 3, "Maps by",       gci_Font_AutocenterX, fHeadSize, sOxy27)
    fnTextFadePack(iRunningTimer + 240, ciBlockTime - 240, cfScreenXRJust, cfScreenYBody + fYPad * 4, "Salty Justice", gci_Font_AutocenterX, fLineSize, sSanchez30)
    
    fnTextFadePack(iRunningTimer + 270, ciBlockTime - 270, cfScreenXCenter, cfScreenYBody + fYPad * 6, "Starlight Tools", gci_Font_AutocenterX, fHeadSize, sOxy27)
    fnTextFadePack(iRunningTimer + 300, ciBlockTime - 300, cfScreenXCenter, cfScreenYBody + fYPad * 7, "Salty Justice",   gci_Font_AutocenterX, fLineSize, sSanchez30)
    iRunningTimer = iRunningTimer + ciTweenPause + ciBlockTime
    
    -- |[Third Block]|
    --Music
    fnTextFadePack(iRunningTimer +  30, ciBlockTime -  30, cfScreenXCenter, cfScreenYBody + fYPad * 0, "Soundtrack by", gci_Font_AutocenterX, fHeadSize, sOxy27)
    fnTextFadePack(iRunningTimer +  60, ciBlockTime -  60, cfScreenXCenter, cfScreenYBody + fYPad * 1, "DrDissonance",  gci_Font_AutocenterX, fLineSize, sSanchez30)
    
    fnTextFadePack(iRunningTimer +  90, ciBlockTime -  90, cfScreenXCenter, cfScreenYBody + fYPad * 3, "Additional Music",             gci_Font_AutocenterX, fHeadSize, sOxy27)
    fnTextFadePack(iRunningTimer + 120, ciBlockTime - 120, cfScreenXCenter, cfScreenYBody + fYPad * 4, "Ove Melaa      Matthew Pablo", gci_Font_AutocenterX, fLineSize, sSanchez30)
    fnTextFadePack(iRunningTimer + 150, ciBlockTime - 150, cfScreenXCenter, cfScreenYBody + fYPad * 5, "Alexandr Zhelanov",            gci_Font_AutocenterX, fLineSize, sSanchez30)
    
    fnTextFadePack(iRunningTimer + 180, ciBlockTime - 180, cfScreenXCenter, cfScreenYBody + fYPad * 7, "Sound Effects",                      gci_Font_AutocenterX, fHeadSize, sOxy27)
    fnTextFadePack(iRunningTimer + 210, ciBlockTime - 210, cfScreenXCenter, cfScreenYBody + fYPad * 8, "Dr Dissonance        Salty Justice", gci_Font_AutocenterX, fLineSize, sSanchez30)
    fnTextFadePack(iRunningTimer + 240, ciBlockTime - 240, cfScreenXCenter, cfScreenYBody + fYPad * 9, "opengameart.org",                    gci_Font_AutocenterX, fLineSize, sSanchez30)
    iRunningTimer = iRunningTimer + ciTweenPause + ciBlockTime

    -- |[Fourth Block]|
    --Ancillary and Testers
    fnTextFadePack(iRunningTimer +  30, ciBlockTime -  30, cfScreenXLJust, cfScreenYBody + fYPad * 0, "Art Intern",            gci_Font_AutocenterX, fHeadSize, sOxy27)
    fnTextFadePack(iRunningTimer +  60, ciBlockTime -  60, cfScreenXLJust, cfScreenYBody + fYPad * 1, "Yuuta Wisp",            gci_Font_AutocenterX, fLineSize, sSanchez30)
    fnTextFadePack(iRunningTimer +  30, ciBlockTime -  30, cfScreenXRJust, cfScreenYBody + fYPad * 0, "Unpaid Writing Intern", gci_Font_AutocenterX, fHeadSize, sOxy27)
    fnTextFadePack(iRunningTimer +  60, ciBlockTime -  60, cfScreenXRJust, cfScreenYBody + fYPad * 1, "Pyxxis",                gci_Font_AutocenterX, fLineSize, sSanchez30)
    
    fnTextFadePack(iRunningTimer +  90, ciBlockTime -  90, cfScreenXCenter, cfScreenYBody + fYPad * 3, "Unpaid Testers",     gci_Font_AutocenterX, fHeadSize, sOxy27)
    fnTextFadePack(iRunningTimer + 120, ciBlockTime - 120, cfScreenXCenter, cfScreenYBody + fYPad * 4, "Klaysee",            gci_Font_AutocenterX, fLineSize, sSanchez30)
    fnTextFadePack(iRunningTimer + 150, ciBlockTime - 150, cfScreenXCenter, cfScreenYBody + fYPad * 5, "Trinity      Aaaac", gci_Font_AutocenterX, fLineSize, sSanchez30)
    
    fnTextFadePack(iRunningTimer + 500, ciBlockTime - 500, cfScreenXCenter, cfScreenYBody + fYPad * 7, "Paid Testers",        gci_Font_AutocenterX, fHeadSize, sOxy27)
    fnTextFadePack(iRunningTimer + 500, ciBlockTime - 500, cfScreenXCenter, cfScreenYBody + fYPad * 8, "(hahaha fat chance)", gci_Font_AutocenterX, fLineSize, sSanchez30)
    iRunningTimer = iRunningTimer + ciTweenPause + ciBlockTime

    -- |[Fifth Block]|
    --Paishaimeru!
    local ciBriefTime = 180
    fnTextFadePack(iRunningTimer +  30, ciBriefTime -  30, cfScreenXCenter, cfScreenYBody + fYPad * 0, "Original Pandemonium World by", gci_Font_AutocenterX, fHeadSize, sOxy27)
    fnTextFadePack(iRunningTimer +  60, ciBriefTime -  60, cfScreenXCenter, cfScreenYBody + fYPad * 1, "Pashaimeru",                    gci_Font_AutocenterX, fLineSize, sSanchez30)
    iRunningTimer = iRunningTimer + ciTweenPause + ciBriefTime
    
    -- |[Fifth Block]|
    --Guest artists, Fans
    fnTextFadePack(iRunningTimer +  30, ciBlockTime -  30, cfScreenXCenter, cfScreenYBody + fYPad * 0, "Story Consultant", gci_Font_AutocenterX, fHeadSize, sOxy27)
    fnTextFadePack(iRunningTimer +  60, ciBlockTime -  60, cfScreenXCenter, cfScreenYBody + fYPad * 1, "Klaysee",          gci_Font_AutocenterX, fLineSize, sSanchez30)
    
    fnTextFadePack(iRunningTimer +  90, ciBlockTime -  90, cfScreenXCenter, cfScreenYBody + fYPad * 3, "Guest Artists",  gci_Font_AutocenterX, fHeadSize, sOxy27)
    fnTextFadePack(iRunningTimer + 120, ciBlockTime - 120, cfScreenXCenter, cfScreenYBody + fYPad * 4, "Comdost",        gci_Font_AutocenterX, fLineSize, sSanchez30)
    fnTextFadePack(iRunningTimer + 150, ciBlockTime - 150, cfScreenXCenter, cfScreenYBody + fYPad * 5, "Littleinksheep", gci_Font_AutocenterX, fLineSize, sSanchez30)
    
    fnTextFadePack(iRunningTimer + 270, ciBlockTime - 270, cfScreenXCenter, cfScreenYBody + fYPad * 7, "Credits Music",                    gci_Font_AutocenterX, fHeadSize, sOxy27)
    fnTextFadePack(iRunningTimer + 300, ciBlockTime - 300, cfScreenXCenter, cfScreenYBody + fYPad * 8, "Patricia Taxxon - Kissing Ancaps", gci_Font_AutocenterX, fLineSize, sSanchez30)
    iRunningTimer = iRunningTimer + ciTweenPause + ciBlockTime
    
    -- |[Sixth Block]|
    --Weird Stuff
    local iDumbTime = 300
    fnTextFadePack(iRunningTimer +  30, iDumbTime -  30, cfScreenXCenter, cfScreenYBody + fYPad * -2, "Person Named Joel",     gci_Font_AutocenterX, fHeadSize, sOxy27)
    fnTextFadePack(iRunningTimer +  60, iDumbTime -  60, cfScreenXCenter, cfScreenYBody + fYPad * -1, "Joel",                  gci_Font_AutocenterX, fLineSize, sSanchez30)
    fnTextFadePack(iRunningTimer +  90, iDumbTime -  90, cfScreenXCenter, cfScreenYBody + fYPad * 2, "Person Not Named Joel", gci_Font_AutocenterX, fHeadSize, sOxy27)
    fnTextFadePack(iRunningTimer + 120, iDumbTime - 120, cfScreenXCenter, cfScreenYBody + fYPad * 3, "Tina Klein",            gci_Font_AutocenterX, fLineSize, sSanchez30)
    fnTextFadePack(iRunningTimer + 150, iDumbTime - 150, cfScreenXCenter, cfScreenYBody + fYPad * 5, "Jokes in Credits", gci_Font_AutocenterX, fHeadSize, sOxy27)
    fnTextFadePack(iRunningTimer + 180, iDumbTime - 180, cfScreenXCenter, cfScreenYBody + fYPad * 6, "Salty",            gci_Font_AutocenterX, fLineSize, sSanchez30)
    fnTextFadePack(iRunningTimer + 180, iDumbTime - 180, cfScreenXCenter, cfScreenYBody + fYPad * 8, "Funny Jokes in Credits", gci_Font_AutocenterX, fHeadSize, sOxy27)
    fnTextFadePack(iRunningTimer + 210, iDumbTime - 210, cfScreenXCenter, cfScreenYBody + fYPad * 9, "Nobody",            gci_Font_AutocenterX, fLineSize, sSanchez30)
    iRunningTimer = iRunningTimer + ciTweenPause + iDumbTime
    
    -- |[Seventh Block]|
    --Patrons
    fnTextFadePack(iRunningTimer +   0, ciBlockTime,       cfScreenXCenter,   cfScreenYHeading, "Special Thanks to All Our Patrons",                         gci_Font_AutocenterX, 1.50, sOxy27)
    fnTextFadePack(iRunningTimer +   0, ciBlockTime -   0, cfScreenXCenter, cfScreenYBody + fYPad * 0, "Patrons over 100$ Lifetime",                         gci_Font_AutocenterX, fHeadSize, sOxy27)
    fnTextFadePack(iRunningTimer +  30, ciBlockTime -  30, cfScreenXCenter, cfScreenYBody + fYPad * 1, "Kumquat     Repeated Meme",                          gci_Font_AutocenterX, fLineSize, sSanchez30)
    fnTextFadePack(iRunningTimer +  60, ciBlockTime -  60, cfScreenXCenter, cfScreenYBody + fYPad * 2, "Austin Durbin     MarioneTTe     James Upton",       gci_Font_AutocenterX, fLineSize, sSanchez30)
    fnTextFadePack(iRunningTimer +  90, ciBlockTime -  90, cfScreenXCenter, cfScreenYBody + fYPad * 3, "Christian Gross     Aaaac     Kaerea Shine",         gci_Font_AutocenterX, fLineSize, sSanchez30)
    fnTextFadePack(iRunningTimer + 120, ciBlockTime - 120, cfScreenXCenter, cfScreenYBody + fYPad * 4, "Abrissgurke     Dark Miros     Cryostorm     Najal", gci_Font_AutocenterX, fLineSize, sSanchez30)
    fnTextFadePack(iRunningTimer + 150, ciBlockTime - 150, cfScreenXCenter, cfScreenYBody + fYPad * 5, "Timothy Cotner     Abliss85     Rylliquinn",         gci_Font_AutocenterX, fLineSize, sSanchez30)
    fnTextFadePack(iRunningTimer + 180, ciBlockTime - 180, cfScreenXCenter, cfScreenYBody + fYPad * 6, "T.Bot/Imp16k4     Aaron Wall     Dyamonde",          gci_Font_AutocenterX, fLineSize, sSanchez30)
    fnTextFadePack(iRunningTimer + 210, ciBlockTime - 210, cfScreenXCenter, cfScreenYBody + fYPad * 7, "Kamen Drago     Goop Sinpai       Reiella",          gci_Font_AutocenterX, fLineSize, sSanchez30)
    fnTextFadePack(iRunningTimer + 240, ciBlockTime - 240, cfScreenXCenter, cfScreenYBody + fYPad * 8, "Pandavenger       Log082       Hunter4242",          gci_Font_AutocenterX, fLineSize, sSanchez30)
    fnTextFadePack(iRunningTimer + 270, ciBlockTime - 270, cfScreenXCenter, cfScreenYBody + fYPad * 9, "Adrian Barnes    Christian Adalbert   N Abram",      gci_Font_AutocenterX, fLineSize, sSanchez30)
    fnTextFadePack(iRunningTimer + 300, ciBlockTime - 300, cfScreenXCenter, cfScreenYBody + fYPad *10, "Trinity         Damaso Castro      Tenmachi",        gci_Font_AutocenterX, fLineSize, sSanchez30)
    iRunningTimer = iRunningTimer + ciTweenPause + iDumbTime
    iRunningTimer = iRunningTimer + ciTweenPause + 330
    
    -- |[Eight Block]|
    --Even More!
    fnTextFadePack(iRunningTimer +   0, ciBlockTime,       cfScreenXCenter,   cfScreenYHeading, "Special Thanks to All Our Patrons",                         gci_Font_AutocenterX, 1.50, sOxy27)
    fnTextFadePack(iRunningTimer +   0, ciBlockTime -   0, cfScreenXCenter, cfScreenYBody + fYPad * 0, "Patrons over 100$ Lifetime",                         gci_Font_AutocenterX, fHeadSize, sOxy27)
    fnTextFadePack(iRunningTimer +  30, ciBlockTime -  30, cfScreenXCenter, cfScreenYBody + fYPad * 1, "EldritchWeaver     Liadri       Carl Endres",        gci_Font_AutocenterX, fLineSize, sSanchez30)
    fnTextFadePack(iRunningTimer +  60, ciBlockTime -  60, cfScreenXCenter, cfScreenYBody + fYPad * 2, "Jon X64            David           WanNyan",         gci_Font_AutocenterX, fLineSize, sSanchez30)
    fnTextFadePack(iRunningTimer +  90, ciBlockTime -  90, cfScreenXCenter, cfScreenYBody + fYPad * 3, "Lancy               TAC       CorruptiveSpirit",     gci_Font_AutocenterX, fLineSize, sSanchez30)
    fnTextFadePack(iRunningTimer + 120, ciBlockTime - 120, cfScreenXCenter, cfScreenYBody + fYPad * 4, "Ethyl Benzene     FlowEXE          Robert",          gci_Font_AutocenterX, fLineSize, sSanchez30)
    fnTextFadePack(iRunningTimer + 150, ciBlockTime - 150, cfScreenXCenter, cfScreenYBody + fYPad * 5, "Lunaria           AGBell8           Amari",          gci_Font_AutocenterX, fLineSize, sSanchez30)
    fnTextFadePack(iRunningTimer + 180, ciBlockTime - 180, cfScreenXCenter, cfScreenYBody + fYPad * 6, "David Eberhard      Erractic    ProtoHeis",          gci_Font_AutocenterX, fLineSize, sSanchez30)
    fnTextFadePack(iRunningTimer + 210, ciBlockTime - 210, cfScreenXCenter, cfScreenYBody + fYPad * 7, "Gil Heinrich      Michael Simpson    Jack Tone",     gci_Font_AutocenterX, fLineSize, sSanchez30)
    fnTextFadePack(iRunningTimer + 240, ciBlockTime - 240, cfScreenXCenter, cfScreenYBody + fYPad * 8, "Adyen             Tom Davies      Emily Nuenke",     gci_Font_AutocenterX, fLineSize, sSanchez30)
    fnTextFadePack(iRunningTimer + 270, ciBlockTime - 270, cfScreenXCenter, cfScreenYBody + fYPad * 9, "Matthew Jultak    John Goodenough    BishopMist",    gci_Font_AutocenterX, fLineSize, sSanchez30)
    fnTextFadePack(iRunningTimer + 300, ciBlockTime - 300, cfScreenXCenter, cfScreenYBody + fYPad *10, "Fakeine            ShadowPlay      ZenithSeeker",    gci_Font_AutocenterX, fLineSize, sSanchez30)
    fnTextFadePack(iRunningTimer + 330, ciBlockTime - 330, cfScreenXCenter, cfScreenYBody + fYPad *11, "FigelNarage    Abraxos Wyvern   Kallie Hallberg",    gci_Font_AutocenterX, fLineSize, sSanchez30)
    fnTextFadePack(iRunningTimer + 360, ciBlockTime - 360, cfScreenXCenter, cfScreenYBody + fYPad *12, "Vikignir           Kyuven",                          gci_Font_AutocenterX, fLineSize, sSanchez30)
    iRunningTimer = iRunningTimer + ciTweenPause + iDumbTime
    iRunningTimer = iRunningTimer + ciTweenPause + 390

    --Total ticks.
    WD_SetProperty("Set Credits Total Ticks", iRunningTimer)


--"No" stops here.
elseif(sTopicName == "No") then
	WD_SetProperty("Hide")

end
