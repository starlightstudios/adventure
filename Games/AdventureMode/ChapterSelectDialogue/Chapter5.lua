-- |[Root]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    --Variables.
    local iChapter1 = VM_GetVar("Root/Variables/Global/ChapterComplete/iChapter1", "N")
    local iChapter5 = VM_GetVar("Root/Variables/Global/ChapterComplete/iChapter5", "N")
    
    --Options.
    local iShowChristine = VM_GetVar("Root/Variables/Global/ChapterComplete/iChapter5", "N")
        
	--Reboot the dialogue.
	fnCutscene([[ WD_SetProperty("Show") ]])
	fnCutscene([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
    if(iShowChristine == 1.0) then
        fnCutscene([[ Append("Chapter 5.[P] Christine.[B][C]") ]])
    else
        fnCutscene([[ Append("Chapter 5.[P] Chris.[B][C]") ]])
    end
    
	--Normal:
    if(iChapter5 < 1) then
        if(iChapter1 < 1.0) then
            fnCutscene([[ Append("It is recommended to complete Chapter 1 before playing Chapter 5.[B][C]") ]])
        end
        fnCutscene([[ Append("Do you want to play this chapter?[BLOCK]") ]])
        
        --Decision script is this script. It must be surrounded by quotes.
        local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
        
        --Decision mode.
        fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"Yes\") ")
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"No\") ")
        fnCutsceneBlocker()
    
    --NC+:
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
        fnCutscene([[ Append("Chapter 5.[P] Christine.[B][C]") ]])
        fnCutscene([[ Append("You have already completed this chapter, unlocking New Chapter Plus.[B][C]") ]])
        fnCutscene([[ Append("NC+ Allows you to replay the chapter, keeping unlocked forms, inventory, and character levels.[B][C]") ]])
        fnCutscene([[ Append("Do you want to play NC+?[BLOCK]") ]])
        
        --Decision script is this script. It must be surrounded by quotes.
        local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
        
        --Decision mode.
        fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"YesNCPlus\") ")
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"No\") ")
        fnCutsceneBlocker()
    
    end
    
--"Yes" Launches Chapter 5.
elseif(sTopicName == "Yes") then
	
	--Common. This is done to clear up any pending blocks from the previous part of the scene.
	WD_SetProperty("Hide")
	
	--Take control of the chapter leader.
	EM_PushEntity("Christine")
		giPartyLeaderID = RO_GetID()
		TA_SetProperty("Clipping Flag", false)
		TA_SetProperty("Activation Script", "Null")
	DL_PopActiveObject()
	AL_SetProperty("Player Actor ID", giPartyLeaderID)
    
	--Settings.
    gbShouldLoadChapter5Assets = true
	LM_ExecuteScript(gsRoot .. "Chapter 5/000 Initialize.lua")
    
	--Load the map.
	AL_BeginTransitionTo("RegulusCryoA", "Null")
    
--"YesNCPlus" Launches Chapter 1, new chapter plus.
elseif(sTopicName == "YesNCPlus") then
	
	--Common. This is done to clear up any pending blocks from the previous part of the scene.
	WD_SetProperty("Hide")
	
	--Push Christine on the activity stack and take control of her. This causes her to transition to the new level correctly.
	EM_PushEntity("Christine")
		giPartyLeaderID = RO_GetID()
		TA_SetProperty("Clipping Flag", false)
		TA_SetProperty("Activation Script", "Null")
	DL_PopActiveObject()
	AL_SetProperty("Player Actor ID", giPartyLeaderID)

	--Settings.
    gbShouldLoadChapter5Assets = true
	LM_ExecuteScript(gsRoot .. "Chapter 5/300 New Chapter Plus Init.lua")
	LM_ExecuteScript(gsRoot .. "Chapter 5/000 Initialize.lua")
	LM_ExecuteScript(gsRoot .. "Chapter 5/301 New Chapter Plus Post.lua")
    
	--Load the map.
	AL_BeginTransitionTo("RegulusCryoA", "Null")

--"No" stops here.
elseif(sTopicName == "No") then
	
	--Common. This is done to clear up any pending blocks from the previous part of the scene.
	WD_SetProperty("Hide")

end
