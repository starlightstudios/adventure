-- |[Root]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--"Hello", standard entry topic.
if(sTopicName == "Hello") then
	
    --Variables:
    local iChapter1 = VM_GetVar("Root/Variables/Global/ChapterComplete/iChapter1", "N")
    
	--Normal:
    if(iChapter1 < 1) then
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
        fnCutscene([[ Append("Chapter 1.[P] Mei.[B][C]") ]])
        fnCutscene([[ Append("Do you want to play this chapter?[BLOCK]") ]])
        
        --Decision script is this script. It must be surrounded by quotes.
        local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
        
        --Decision mode.
        fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"Yes\") ")
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"No\") ")
        fnCutsceneBlocker()
    
    --New Chapter Plus:
    else
        fnCutscene([[ WD_SetProperty("Show") ]])
        fnCutscene([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutscene([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
        fnCutscene([[ Append("Chapter 1.[P] Mei.[B][C]") ]])
        fnCutscene([[ Append("You have already completed this chapter, unlocking New Chapter Plus.[B][C]") ]])
        fnCutscene([[ Append("NC+ Allows you to replay the chapter, keeping unlocked forms, inventory, and character levels.[B][C]") ]])
        fnCutscene([[ Append("Do you want to play NC+?[BLOCK]") ]])
        
        --Decision script is this script. It must be surrounded by quotes.
        local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
        
        --Decision mode.
        fnCutscene([[ WD_SetProperty("Activate Decisions") ]])
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"YesNCPlus\") ")
        fnCutscene(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"No\") ")
        fnCutsceneBlocker()
    end

--"Yes" Launches Chapter 1.
elseif(sTopicName == "Yes") then
	
	--Common. This is done to clear up any pending blocks from the previous part of the scene.
	WD_SetProperty("Hide")
	
	--Push Mei on the activity stack and take control of her. This causes her to transition to the new level correctly.
	EM_PushEntity("Mei")
		giPartyLeaderID = RO_GetID()
		TA_SetProperty("Clipping Flag", false)
		TA_SetProperty("Activation Script", "Null")
	DL_PopActiveObject()
	AL_SetProperty("Player Actor ID", giPartyLeaderID)

	--Settings.
    gbShouldLoadChapter1Assets = true
	LM_ExecuteScript(gsRoot .. "Chapter 1/000 Initialize.lua")
    
	--Load the map.
	AL_BeginTransitionTo("TrapBasementA", "Null")
    
--"YesNCPlus" Launches Chapter 1, new chapter plus.
elseif(sTopicName == "YesNCPlus") then
	
	--Common. This is done to clear up any pending blocks from the previous part of the scene.
	WD_SetProperty("Hide")
	
	--Push Mei on the activity stack and take control of her. This causes her to transition to the new level correctly.
	EM_PushEntity("Mei")
		giPartyLeaderID = RO_GetID()
		TA_SetProperty("Clipping Flag", false)
		TA_SetProperty("Activation Script", "Null")
	DL_PopActiveObject()
	AL_SetProperty("Player Actor ID", giPartyLeaderID)

	--Settings.
    gbShouldLoadChapter1Assets = true
	LM_ExecuteScript(gsRoot .. "Chapter 1/300 New Chapter Plus Init.lua")
	LM_ExecuteScript(gsRoot .. "Chapter 1/000 Initialize.lua")
	LM_ExecuteScript(gsRoot .. "Chapter 1/301 New Chapter Plus Post.lua")
    
	--Load the map.
	AL_BeginTransitionTo("TrapBasementA", "Null")

--"No" stops here.
elseif(sTopicName == "No") then
	
	--Common. This is done to clear up any pending blocks from the previous part of the scene.
	WD_SetProperty("Hide")

end
