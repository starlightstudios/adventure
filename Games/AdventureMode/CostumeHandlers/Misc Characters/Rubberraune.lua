-- |[ ====================================== Rubberraune ======================================= ]|
--Used for the sprites of the Rubber Alraune who joins the party briefly in chapter 1.

-- |[Sprite]|
if(EM_Exists("Rubberraune") == true) then
	EM_PushEntity("Rubberraune")
	
		--Base.
		fnSetCharacterGraphics("Root/Images/Sprites/AlrauneR/", false)
		
		--Animation properties.
		TA_SetProperty("Auto Animates", false)
		TA_SetProperty("Y Oscillates", false)
		TA_SetProperty("No Footstep Sound", false)
        TA_SetProperty("No Automatic Shadow", false)
		
		--Special frames.
        TA_SetProperty("Wipe Special Frames")
	DL_PopActiveObject()
end
