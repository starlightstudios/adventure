-- |[ ======================================= Rubber Bee ======================================= ]|
--Used for the sprites of the Rubber Bee who joins the party briefly in chapter 1.

-- |[Sprite]|
if(EM_Exists("Rubberbee") == true) then
	EM_PushEntity("Rubberbee")
	
		--Base.
		fnSetCharacterGraphics("Root/Images/Sprites/BeeGirlR/", false)
		
		--Animation properties.
		TA_SetProperty("Auto Animates", true)
		TA_SetProperty("Y Oscillates", true)
		TA_SetProperty("No Footstep Sound", false)
        TA_SetProperty("No Automatic Shadow", false)
		
		--Special frames.
        TA_SetProperty("Wipe Special Frames")
	DL_PopActiveObject()
end
