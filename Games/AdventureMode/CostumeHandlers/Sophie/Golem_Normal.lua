-- |[Golem Normal]|
--Sophie in her adorable skirt.

-- |[Variables]|
--Flag.
VM_SetVar("Root/Variables/Costumes/Sophie/sCostumeGolem", "S", "Normal")

-- |[Sprite]|
if(EM_Exists("Sophie") == true) then
	EM_PushEntity("Sophie")
        --Base.
        fnSetCharacterGraphics("Root/Images/Sprites/Sophie/", true)
		
		--Animation properties.
		TA_SetProperty("Auto Animates", false)
		TA_SetProperty("Y Oscillates", false)
		TA_SetProperty("No Footstep Sound", false)
        TA_SetProperty("No Automatic Shadow", false)
		
		--Special frames.
        TA_SetProperty("Wipe Special Frames")
	DL_PopActiveObject()
end

-- |[Combat Portraits, UI Images]|
--Sophie is not a combat character.

-- |[Sprites]|
--Character Sprite Lookup
for i = 1, giCharSpritesTotal, 1 do
    if(gsCharSprites[i][1] == "Sophie") then
        gsCharSprites[i][2] = "Root/Images/Sprites/Sophie/SW|0"
        break
    end
end
	
-- |[Dialogue Portraits]|
if(DialogueActor_Exists("Sophie") == true) then
    DialogueActor_Push("Sophie")
        DialogueActor_SetProperty("Add Emotion", "Neutral",   "Root/Images/Portraits/SophieDialogue/Neutral",   true)
        DialogueActor_SetProperty("Add Emotion", "Happy",     "Root/Images/Portraits/SophieDialogue/Happy",     true)
        DialogueActor_SetProperty("Add Emotion", "Blush",     "Root/Images/Portraits/SophieDialogue/Blush",     true)
        DialogueActor_SetProperty("Add Emotion", "Smirk",     "Root/Images/Portraits/SophieDialogue/Smirk",     true)
        DialogueActor_SetProperty("Add Emotion", "Sad",       "Root/Images/Portraits/SophieDialogue/Sad",       true)
        DialogueActor_SetProperty("Add Emotion", "Surprised", "Root/Images/Portraits/SophieDialogue/Surprised", true)
        DialogueActor_SetProperty("Add Emotion", "Offended",  "Root/Images/Portraits/SophieDialogue/Offended",  true)
    DL_PopActiveObject()
end
