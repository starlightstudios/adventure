-- |[ ============================ Sammy Davis, Action Moth, Normal ============================ ]|
-- |[Setup]|
--Set all variables for this script. Special frames and alignments need to be entered below.
-- First, constants across this character.
--Sammy has no combat properties.
local sActorName = "Sammy"
local sFieldName = "Sammy"
local sFormVar = "Null"

--Costume Variables
local sCostumeName = "Normal"
local sCostumeVar = "Root/Variables/Costumes/Sammy/sCostumeActionMoth"

--Actor Portrait
local sDialoguePath = "Root/Images/Portraits/Sammy/"

--Sprite
local sSpritePath = "Root/Images/Sprites/Sammy/"

-- |[Dialogue]|
DialogueActor_Push(sActorName)
    DialogueActor_SetProperty("Add Emotion", "Neutral",  sDialoguePath .. "Neutral", true)
DL_PopActiveObject()

-- |[Sprite]|
if(EM_Exists(sFieldName) == true) then
	EM_PushEntity(sFieldName)
	
		--Base.
		fnSetCharacterGraphics(sSpritePath, true)
		
		--Animation properties.
		TA_SetProperty("Auto Animates", false)
		TA_SetProperty("Y Oscillates", false)
		TA_SetProperty("No Footstep Sound", false)
        TA_SetProperty("No Automatic Shadow", false)
		
		--Special frames.
        TA_SetProperty("Wipe Special Frames")
        TA_SetProperty("Add Special Frame", "SwimS", "Root/Images/Sprites/Special/SammySwimS")
        TA_SetProperty("Add Special Frame", "SwimW", "Root/Images/Sprites/Special/SammySwimW")

        --Sammy's dancing frames.
        for i = 0, 7, 1 do
            TA_SetProperty("Add Special Frame", "DanceA"..i, "Root/Images/Sprites/Special/SammyDanceA|" .. i)
            TA_SetProperty("Add Special Frame", "DanceB"..i, "Root/Images/Sprites/Special/SammyDanceB|" .. i)
            TA_SetProperty("Add Special Frame", "DanceC"..i, "Root/Images/Sprites/Special/SammyDanceC|" .. i)
            TA_SetProperty("Add Special Frame", "DanceD"..i, "Root/Images/Sprites/Special/SammyDanceD|" .. i)
            TA_SetProperty("Add Special Frame", "DanceE"..i, "Root/Images/Sprites/Special/SammyDanceE|" .. i)
            TA_SetProperty("Add Special Frame", "DanceF"..i, "Root/Images/Sprites/Special/SammyDanceF|" .. i)
            TA_SetProperty("Add Special Frame", "DanceG"..i, "Root/Images/Sprites/Special/SammyDanceG|" .. i)
            TA_SetProperty("Add Special Frame", "DanceH"..i, "Root/Images/Sprites/Special/SammyDanceH|" .. i)
        end
	DL_PopActiveObject()
end
        
-- |[Lua Sprite Lookup]|
for i = 1, giCharSpritesTotal, 1 do
    if(gsCharSprites[i][1] == sFieldName) then
        gsCharSprites[i][2] = sSpritePath .. "SW|0"
        break
    end
end
