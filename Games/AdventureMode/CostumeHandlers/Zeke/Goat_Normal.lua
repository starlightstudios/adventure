-- |[ =================================== Zeke, Goat, Normal =================================== ]|
-- |[Setup]|
--Set all variables for this script. Special frames and alignments need to be entered below.
-- First, constants across this character.
local sActorName = "Zeke"
local sCombatName = "Zeke"
local sFieldName = "Zeke"
local sFormVar = "Root/Variables/Global/Zeke/sCurrentJob"

--Costume Variables
local sCostumeName = "Normal"
local sCostumeVar = "Root/Variables/Costumes/Zeke/sCostumeGoat"

--Actor Portrait
local sDialoguePath = "Root/Images/Portraits/ZekeGoatDialogue/"

--Sprite
local sSpritePath = "Root/Images/Sprites/Zeke_Goat/"
local sSpecialPath = "Root/Images/Sprites/Special/Zeke_Goat|"
local fIndexX = 5
local fIndexY = 6

--Combat
local sCombatPortrait = "Root/Images/Portraits/Combat/Zeke_Goat"
local sCountermask    = "Null"
local sTurnPortrait   = "Root/Images/AdventureUI/TurnPortraits/Zeke_Goat"

--Delayed-Load Groupings
local sDialogueCostumeGrouping = "Zeke Costume Goat"
local sCombatCostumeGrouping   = sDialogueCostumeGrouping .. " Combat"
local saDialogueGroupingList   = gsaZekeDialogueList
local saCombatGroupingList     = gsaZekeCombatList

-- |[Stop Check]|
--If not in this form, just flag the variable.
local sCharacterForm = VM_GetVar(sFormVar, "S")
VM_SetVar(sCostumeVar, "S", sCostumeName)
if(sCharacterForm ~= "Goat") then
    return
end

-- |[ ======================================== Dialogue ======================================== ]|
--Standard.
fnSetZekeDialogue(sDialoguePath)

-- |[ ====================================== Field Sprite ====================================== ]|
fnSetZekeFieldSprite(sSpritePath, sSpecialPath, false)
fnSetZekeLuaSpriteLookup(sSpritePath)
fnSetZekeIdleAnimation("Null", 0, 0, -1)

-- |[ ===================================== Combat and UI ====================================== ]|
--Basic.
fnSetZekeCombatData(sCombatName, sCombatPortrait, sCountermask, sTurnPortrait, sSpritePath, fIndexX, fIndexY)

--Alignments.
if(AdvCombat_GetProperty("Does Party Member Exist", sCombatName) == true) then
    AdvCombat_SetProperty("Push Party Member", sCombatName)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Basemenu,         -78, 401)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Base,      120,  77)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Ally,     -228, -93)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Inspector, 210,  53)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equipment,         20, 131)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Skills,            28,  68)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status,            18,  27)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Vendor,          -127, 105)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Victory,   600,  75)
    DL_PopActiveObject()
end

-- |[ ================================= Loading and Unloading ================================== ]|
--Order all not-in-use dialogue and combat images to unload their data. Then order all in-use 
-- images to load their data. Images that are in multiple sets will correctly load their data
-- if the load is called after the unload.
if(gbUnloadDialoguePortraitsWhenDialogueCloses == false) then
    fnUnloadGrouping(saDialogueGroupingList, sDialogueCostumeGrouping)
    fnLoadGrouping  (saDialogueGroupingList, sDialogueCostumeGrouping)
end

--Combat always loads.
fnUnloadGrouping(saCombatGroupingList, sCombatCostumeGrouping)
fnLoadGrouping  (saCombatGroupingList, sCombatCostumeGrouping)
