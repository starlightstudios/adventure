-- |[ ======================== Common Functions and Variables for Zeke ========================= ]|
--Some parts of Zeke's costume code is identical across all costumes. This handles those.

-- |[ ==================================== fnSetZekeDialogue =================================== ]|
--Sets Zeke's emotions using the given dialogue path.
function fnSetZekeDialogue(psDialoguePath)

    --Argument check.
    if(psDialoguePath == nil) then return end

    --Verify dialogue actor exists.
    if(DialogueActor_Exists("Zeke") == false) then return end

    --Push, set.
    DialogueActor_Push("Zeke")
        DialogueActor_SetProperty("Add Emotion", "Neutral",  psDialoguePath .. "Neutral",  true)
    DL_PopActiveObject()

end

-- |[ ================================== fnSetZekeFieldSprite ================================== ]|
--If the field sprite for Zeke exists, updates its properties and special frames.
function fnSetZekeFieldSprite(psSpritePath, psSpecialPath, pbIsAirborne)
    
    --Argument check.
    if(psSpritePath == nil) then return end
    if(psSpecialPath == nil) then return end
    if(pbIsAirborne == nil) then pbIsAirborne = false end
    
    --No field sprite, exit.
    if(EM_Exists("Zeke") == false) then return end
    
    --Push.
    EM_PushEntity("Zeke")
    
        --Base.
        fnSetCharacterGraphics(psSpritePath, true)
        
        --Animation properties for ground forms:
        if(pbIsAirborne == false) then
            TA_SetProperty("Auto Animates",       false)
            TA_SetProperty("Y Oscillates",        false)
            TA_SetProperty("No Footstep Sound",   false)
            TA_SetProperty("No Automatic Shadow", false)
        
        --Airborne forms:
        else
            TA_SetProperty("Auto Animates",       true)
            TA_SetProperty("Y Oscillates",        true)
            TA_SetProperty("No Footstep Sound",   true)
            TA_SetProperty("No Automatic Shadow", false)
        end
        
        --Special frames.
        TA_SetProperty("Wipe Special Frames")
        TA_SetProperty("Add Special Frame", "Wounded", psSpecialPath .. "Wounded")
        TA_SetProperty("Add Special Frame", "Crouch",  psSpecialPath .. "Crouch")
        
        --Water handler.
        TA_SetProperty("Add Special Frame", "WaterOver", "Root/Images/Sprites/Shadows/Depth")
        
        --Debug.
        --TA_GetProperty("Print Special Frames")
    
    --Clean.
    DL_PopActiveObject()

end

-- |[ ================================= fnSetZekeIdleAnimation ================================= ]|
--Sets the idle animation for Zeke in the given form. Pass "Null" to disable. This one uses a simple
-- endless loop case, or a play-once-then-done case.
function fnSetZekeIdleAnimation(psDLPathPattern, piFramesTotal, piTPF, piLoopFrame)
    
    --Arg check.
    if(psDLPathPattern == nil) then return end
    if(piFramesTotal   == nil) then return end
    if(piTPF           == nil) then return end
    if(piLoopFrame     == nil) then return end
    
    --Do nothing if the actor does not exist.
    if(EM_Exists("Zeke") == false) then return end
    
    --Disable if "Null".
    if(psDLPathPattern == "Null") then
        EM_PushEntity("Zeke")
            TA_SetProperty("Idle Animation Flag", false)
        DL_PopActiveObject()
        return
    end
    
    --Legality check.
    if(piFramesTotal < 1) then return end
    
    --Otherwise, set the idle animations.
    EM_PushEntity("Zeke")
        TA_SetProperty("Idle Animation Flag", true)
        TA_SetProperty("Allocate Idle Frames", piFramesTotal)
        for i = 0, piFramesTotal-1, 1 do
            TA_SetProperty("Set Idle Frame", i, psDLPathPattern .. i)
        end
        TA_SetProperty("Set Idle TPF", piTPF)
        TA_SetProperty("Set Idle Loop Frame", piLoopFrame)
    DL_PopActiveObject()
end

--Sets the idle animation for Mei in a given form. This one uses a list of the frames, which allows
-- for complicated, though finite, looping. The animation is one-then-done but can re-use frames.
function fnSetZekeIdleAnimationAdvanced(psDLPathPattern, piTPF, piaFrameList)
    
    --Arg check.
    if(psDLPathPattern == nil) then return end
    if(piTPF           == nil) then return end
    if(piaFrameList    == nil) then return end
    
    --Do nothing if the actor does not exist.
    if(EM_Exists("Zeke") == false) then return end
    
    --Disable if "Null".
    if(psDLPathPattern == "Null") then
        EM_PushEntity("Zeke")
            TA_SetProperty("Idle Animation Flag", false)
        DL_PopActiveObject()
        return
    end
    
    --Legality check.
    if(#piaFrameList < 1) then return end
    
    --Otherwise, set the idle animations.
    EM_PushEntity("Zeke")
        TA_SetProperty("Idle Animation Flag", true)
        TA_SetProperty("Allocate Idle Frames", #piaFrameList)
        for i = 1, #piaFrameList, 1 do
            TA_SetProperty("Set Idle Frame", i-1, psDLPathPattern .. piaFrameList[i])
        end
        TA_SetProperty("Set Idle TPF", piTPF)
        TA_SetProperty("Set Idle Loop Frame", -1)
    DL_PopActiveObject()
    
end

-- |[ ================================ fnSetZekeLuaSpriteLookup ================================ ]|
--Updates which sprites the lua code uses to refer to this character.
function fnSetZekeLuaSpriteLookup(psSpritePath)
    
    --Argument check.
    if(psSpritePath == nil) then return end
    
    --Iterate.
    for i = 1, giCharSpritesTotal, 1 do
        
        --Match.
        if(gsCharSprites[i][1] == "Zeke") then
            gsCharSprites[i][2] = psSpritePath .. "SW|0"
            break
        end
    end
    
end

-- |[ =================================== fnSetZekeCombatData ================================== ]|
--Sets combat data for the given party member. Does not set UI rendering positions.
function fnSetZekeCombatData(psCombatName, psPortrait, psCountermask, psTurnPortrait, psSpritePath, pfIndexX, pfIndexY)
    
    --Argument check.
    if(psCombatName   == nil) then return end
    if(psPortrait     == nil) then return end
    if(psCountermask  == nil) then return end
    if(psTurnPortrait == nil) then return end
    if(psSpritePath   == nil) then return end
    if(pfIndexX       == nil) then return end
    if(pfIndexY       == nil) then return end
    
    --Existence check.
    if(AdvCombat_GetProperty("Does Party Member Exist", psCombatName) == false) then return end
    
    --Push.
    AdvCombat_SetProperty("Push Party Member", psCombatName)
    
        --Portraits
        AdvCombatEntity_SetProperty("Combat Portrait",    psPortrait)
        AdvCombatEntity_SetProperty("Combat Countermask", psCountermask)
        AdvCombatEntity_SetProperty("Turn Icon",          psTurnPortrait)
    
        --Vendor Sprites
        AdvCombatEntity_SetProperty("Vendor Image", 0, psSpritePath .. "SW|0")
        AdvCombatEntity_SetProperty("Vendor Image", 1, psSpritePath .. "SW|1")
        AdvCombatEntity_SetProperty("Vendor Image", 2, psSpritePath .. "SW|2")
        AdvCombatEntity_SetProperty("Vendor Image", 3, psSpritePath .. "SW|3")
    
        --Face Table, used on the Equipment UI to show who has the item equipped.
        local fLft = (pfIndexX+0) * gci_FaceTable_Size
        local fTop = (pfIndexY+0) * gci_FaceTable_Size
        local fRgt = (pfIndexX+1) * gci_FaceTable_Size
        local fBot = (pfIndexY+1) * gci_FaceTable_Size
        AdvCombatEntity_SetProperty("Face Table Data", fLft, fTop, fRgt, fBot, "Root/Images/AdventureUI/ItemIcons/CharacterFaces")

    --Clean.
    DL_PopActiveObject()
    
end
