-- |[ ============================= Christine, Steam Droid, SX-399 ============================= ]|
-- |[Setup]|
--Set all variables for this script. Special frames and alignments need to be entered below.
-- First, constants across this character.
local sActorName = "Christine"
local sCombatName = "Christine"
local sFieldName = "Christine"
local sFormVar = "Root/Variables/Global/Christine/sForm"

--Voice Variables
local sVoiceVar = "Root/Variables/Global/Christine/sVoiceJob"
local sVoiceThis = "Lancer"

--Costume Variables
local sCostumeName = "SX-399"
local sCostumeVar = "Root/Variables/Costumes/Christine/sCostumeSteamDroid"

--Actor Portrait
local sDialoguePath = "Root/Images/Portraits/ChristineDialogue/SteamDroidSX399|"

--Sprite
local sSpritePath = "Root/Images/Sprites/SX-399/"
local sSpcFrame = "Christine_Human"
local fIndexX = 2
local fIndexY = 6

--Combat
local sCombatPortrait = "Root/Images/Portraits/Combat/Christine_SteamDroid"
local sCountermask    = "Null"
local sTurnPortrait = "Root/Images/AdventureUI/TurnPortraits/Christine_SteamDroid"

--Delayed-Load Groupings
local sDialogueCostumeGrouping = "Christine Costume SteamDroid SX399"
local sCombatCostumeGrouping   = sDialogueCostumeGrouping .. " Combat"
local saDialogueGroupingList   = gsaChristineDialogueList
local saCombatGroupingList     = gsaChristineCombatList

-- |[Stop Check]|
--If not in this form, just flag the variable.
local sCharacterForm = VM_GetVar(sFormVar, "S")
VM_SetVar(sCostumeVar, "S", sCostumeName)
if(sCharacterForm ~= "SteamDroid") then
    return
end

-- |[Dialogue]|
--All portraits are the SX-399 portrait.
DialogueActor_Push("Christine")
    DialogueActor_SetProperty("Add Emotion", "Neutral",  "Root/Images/Portraits/SX-399YoungDroid/Neutral", true)
    DialogueActor_SetProperty("Add Emotion", "Happy",    "Root/Images/Portraits/SX-399YoungDroid/Neutral", true)
    DialogueActor_SetProperty("Add Emotion", "Blush",    "Root/Images/Portraits/SX-399YoungDroid/Neutral", true)
    DialogueActor_SetProperty("Add Emotion", "Smirk",    "Root/Images/Portraits/SX-399YoungDroid/Neutral", true)
    DialogueActor_SetProperty("Add Emotion", "Sad",      "Root/Images/Portraits/SX-399YoungDroid/Neutral", true)
    DialogueActor_SetProperty("Add Emotion", "Scared",   "Root/Images/Portraits/SX-399YoungDroid/Neutral", true)
    DialogueActor_SetProperty("Add Emotion", "Offended", "Root/Images/Portraits/SX-399YoungDroid/Neutral", true)
    DialogueActor_SetProperty("Add Emotion", "Serious",  "Root/Images/Portraits/SX-399YoungDroid/Neutral", true)
    DialogueActor_SetProperty("Add Emotion", "Cry",      "Root/Images/Portraits/SX-399YoungDroid/Neutral", true)
    DialogueActor_SetProperty("Add Emotion", "Laugh",    "Root/Images/Portraits/SX-399YoungDroid/Neutral", true)
    DialogueActor_SetProperty("Add Emotion", "Angry",    "Root/Images/Portraits/SX-399YoungDroid/Neutral", true)
    DialogueActor_SetProperty("Add Emotion", "PDU",      "Root/Images/Portraits/SX-399YoungDroid/Neutral", true)
DL_PopActiveObject()

-- |[Voice Handling]|
VM_SetVar(sVoiceVar, "S", sVoiceThis)

-- |[Sprite]|
if(EM_Exists("Christine") == true) then
	EM_PushEntity("Christine")
	
		--Base.
		fnSetCharacterGraphics("Root/Images/Sprites/SX-399/", true)
		
		--Animation properties.
		TA_SetProperty("Auto Animates", false)
		TA_SetProperty("Y Oscillates", false)
		TA_SetProperty("No Footstep Sound", false)
        TA_SetProperty("No Automatic Shadow", false)
		
		--Special frames.
        TA_SetProperty("Wipe Special Frames")
	DL_PopActiveObject()
end
        
-- |[Lua Sprite Lookup]|
for i = 1, giCharSpritesTotal, 1 do
    if(gsCharSprites[i][1] == "Christine") then
        gsCharSprites[i][2] = "Root/Images/Sprites/Christine_SteamDroid/SW|0"
        break
    end
end

-- |[Combat Portraits, UI Images]|
if(AdvCombat_GetProperty("Does Party Member Exist", "Christine") == true) then
    AdvCombat_SetProperty("Push Party Member", "Christine")

        --Portraits
        AdvCombatEntity_SetProperty("Combat Portrait",    "Root/Images/Portraits/Combat/Christine_SteamDroid")
        AdvCombatEntity_SetProperty("Combat Countermask", "Null")
        AdvCombatEntity_SetProperty("Turn Icon",          "Root/Images/AdventureUI/TurnPortraits/Christine_SteamDroid")
    
        --Vendor Sprites
        AdvCombatEntity_SetProperty("Vendor Image", 0, "Root/Images/Sprites/SX-399/South|0")
        AdvCombatEntity_SetProperty("Vendor Image", 1, "Root/Images/Sprites/SX-399/South|1")
        AdvCombatEntity_SetProperty("Vendor Image", 2, "Root/Images/Sprites/SX-399/South|2")
        AdvCombatEntity_SetProperty("Vendor Image", 3, "Root/Images/Sprites/SX-399/South|3")
	
        --UI Rendering Positions
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Basemenu,         -18, 427)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Base,      196, 110)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Ally,     -136, -46)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Inspector, 248,  87)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equipment,         29,  74)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Skills,            72, 108)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status,            69,  69)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Vendor,           -46, 110)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Trainer,          -20, 139)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Victory,   642,  91)
    
        --Face Table, used on the Equipment UI to show who has the item equipped.
        local fIndexX = 2
        local fIndexY = 6
        local fLft = (fIndexX+0) * gci_FaceTable_Size
        local fTop = (fIndexY+0) * gci_FaceTable_Size
        local fRgt = (fIndexX+1) * gci_FaceTable_Size
        local fBot = (fIndexY+1) * gci_FaceTable_Size
        AdvCombatEntity_SetProperty("Face Table Data", fLft, fTop, fRgt, fBot, "Root/Images/AdventureUI/ItemIcons/CharacterFaces")

    DL_PopActiveObject()
end

-- |[ ================================= Loading and Unloading ================================== ]|
--Order all of Christines not-in-use dialogue and combat images to unload their data. Then order
-- all in-use images to load their data. Images that are in multiple sets will correctly load their
-- data if the load is called after the unload.
if(gbUnloadDialoguePortraitsWhenDialogueCloses == false) then
    fnUnloadGrouping(saDialogueGroupingList, sDialogueCostumeGrouping)
    fnLoadGrouping  (saDialogueGroupingList, sDialogueCostumeGrouping)
end

--Combat always loads.
fnUnloadGrouping(saCombatGroupingList, sCombatCostumeGrouping)
fnLoadGrouping  (saCombatGroupingList, sCombatCostumeGrouping)
