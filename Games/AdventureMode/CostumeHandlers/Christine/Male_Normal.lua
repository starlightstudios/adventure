-- |[ ================================ Christine, Male, Normal ================================= ]|
-- |[Setup]|
--Set all variables for this script. Special frames and alignments need to be entered below.
-- First, constants across this character.
local sActorName = "Christine"
local sCombatName = "Christine"
local sFieldName = "Christine"
local sFormVar = "Root/Variables/Global/Christine/sForm"

--Voice Variables
local sVoiceVar = "Root/Variables/Global/Christine/sVoiceJob"
local sVoiceThis = "Male"

--Costume Variables
local sCostumeName = "Normal"
local sCostumeVar = "Root/Variables/Costumes/Christine/sCostumeMale"

--Actor Portrait
local sDialoguePath = "Root/Images/Portraits/ChristineDialogue/Male|"

--Sprite
local sSpritePath = "Root/Images/Sprites/Christine_Male/"
local sSpcFrame = "Christine_Human"
local fIndexX = 30
local fIndexY = 4

--Combat
local sCombatPortrait = "Root/Images/Portraits/Combat/Christine_Male"
local sCountermask    = "Null"
local sTurnPortrait   = "Root/Images/AdventureUI/TurnPortraits/Christine_Male"

--Delayed-Load Groupings
local sDialogueCostumeGrouping = "Christine Costume Male"
local sCombatCostumeGrouping   = sDialogueCostumeGrouping .. " Combat"
local saDialogueGroupingList   = gsaChristineDialogueList
local saCombatGroupingList     = gsaChristineCombatList

-- |[Stop Check]|
--If not in this form, just flag the variable.
local sCharacterForm = VM_GetVar(sFormVar, "S")
VM_SetVar(sCostumeVar, "S", sCostumeName)
if(sCharacterForm ~= "Male") then
    return
end

-- |[Dialogue]|
--Male Christine has only the neutral portrait.
if(DialogueActor_Exists(sActorName) == true) then
    DialogueActor_Push(sActorName)
        DialogueActor_SetProperty("Add Emotion", "Neutral",  sDialoguePath .. "Neutral", true)
        DialogueActor_SetProperty("Add Emotion", "Happy",    sDialoguePath .. "Neutral", true)
        DialogueActor_SetProperty("Add Emotion", "Blush",    sDialoguePath .. "Neutral", true)
        DialogueActor_SetProperty("Add Emotion", "Smirk",    sDialoguePath .. "Neutral", true)
        DialogueActor_SetProperty("Add Emotion", "Sad",      sDialoguePath .. "Neutral", true)
        DialogueActor_SetProperty("Add Emotion", "Scared",   sDialoguePath .. "Neutral", true)
        DialogueActor_SetProperty("Add Emotion", "Offended", sDialoguePath .. "Neutral", true)
        DialogueActor_SetProperty("Add Emotion", "Serious",  sDialoguePath .. "Neutral", true)
        DialogueActor_SetProperty("Add Emotion", "Cry",      sDialoguePath .. "Neutral", true)
        DialogueActor_SetProperty("Add Emotion", "Laugh",    sDialoguePath .. "Neutral", true)
        DialogueActor_SetProperty("Add Emotion", "Angry",    sDialoguePath .. "Neutral", true)
        DialogueActor_SetProperty("Add Emotion", "PDU",      sDialoguePath .. "Neutral", true)
    DL_PopActiveObject()
end

-- |[Voice Handling]|
VM_SetVar(sVoiceVar, "S", sVoiceThis)

-- |[Sprite]|
if(EM_Exists(sFieldName) == true) then
	EM_PushEntity(sFieldName)
	
		--Base.
		fnSetCharacterGraphics(sSpritePath, true)
		
		--Animation properties.
		TA_SetProperty("Auto Animates", false)
		TA_SetProperty("Y Oscillates", false)
		TA_SetProperty("No Footstep Sound", false)
        TA_SetProperty("No Automatic Shadow", false)
		
		--Special frames.
        TA_SetProperty("Wipe Special Frames")
        TA_SetProperty("Add Special Frame", "Wounded", "Root/Images/Sprites/Special/Christine_Male|Wounded")
        TA_SetProperty("Add Special Frame", "Crouch",  "Root/Images/Sprites/Special/Christine_Male|Crouch")
        TA_SetProperty("Add Special Frame", "Laugh0",  "Root/Images/Sprites/Special/Christine|Laugh0")
        TA_SetProperty("Add Special Frame", "Laugh1",  "Root/Images/Sprites/Special/Christine|Laugh1")
        TA_SetProperty("Add Special Frame", "Sad",     "Root/Images/Sprites/Special/Christine|Sad")
	DL_PopActiveObject()
end
        
-- |[Lua Sprite Lookup]|
for i = 1, giCharSpritesTotal, 1 do
    if(gsCharSprites[i][1] == sFieldName) then
        gsCharSprites[i][2] = sSpritePath .. "SW|0"
        break
    end
end

-- |[Combat Portraits, UI Images]|
if(AdvCombat_GetProperty("Does Party Member Exist", sCombatName) == true) then
    AdvCombat_SetProperty("Push Party Member", sCombatName)

        --Portraits
        AdvCombatEntity_SetProperty("Combat Portrait",    sCombatPortrait)
        AdvCombatEntity_SetProperty("Combat Countermask", "Null")
        AdvCombatEntity_SetProperty("Turn Icon",          sTurnPortrait)
    
        --Vendor Sprites
        AdvCombatEntity_SetProperty("Vendor Image", 0, sSpritePath .. "SW|0")
        AdvCombatEntity_SetProperty("Vendor Image", 1, sSpritePath .. "SW|1")
        AdvCombatEntity_SetProperty("Vendor Image", 2, sSpritePath .. "SW|2")
        AdvCombatEntity_SetProperty("Vendor Image", 3, sSpritePath .. "SW|3")
	
        --UI Rendering Positions
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Basemenu,          20, 440)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Base,      237, 121)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Ally,     -102, -35)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Inspector, 284, 102)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equipment,         71,  87)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Skills,           102, 115)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status,            99,  76)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Vendor,           -11, 115)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Trainer,           48, 155)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Victory,   665,  96)
    
        --Face Table, used on the Equipment UI to show who has the item equipped.
        local fLft = (fIndexX+0) * gci_FaceTable_Size
        local fTop = (fIndexY+0) * gci_FaceTable_Size
        local fRgt = (fIndexX+1) * gci_FaceTable_Size
        local fBot = (fIndexY+1) * gci_FaceTable_Size
        AdvCombatEntity_SetProperty("Face Table Data", fLft, fTop, fRgt, fBot, "Root/Images/AdventureUI/ItemIcons/CharacterFaces")

    DL_PopActiveObject()
end

-- |[ ================================= Loading and Unloading ================================== ]|
--Order all of Christines not-in-use dialogue and combat images to unload their data. Then order
-- all in-use images to load their data. Images that are in multiple sets will correctly load their
-- data if the load is called after the unload.
if(gbUnloadDialoguePortraitsWhenDialogueCloses == false) then
    fnUnloadGrouping(saDialogueGroupingList, sDialogueCostumeGrouping)
    fnLoadGrouping  (saDialogueGroupingList, sDialogueCostumeGrouping)
end

--Combat always loads.
fnUnloadGrouping(saCombatGroupingList, sCombatCostumeGrouping)
fnLoadGrouping  (saCombatGroupingList, sCombatCostumeGrouping)
