-- |[ ================================ Christine, Human, Normal ================================ ]|
-- |[Setup]|
--Set all variables for this script. Special frames and alignments need to be entered below.
-- First, constants across this character.
local sActorName = "Christine"
local sCombatName = "Christine"
local sFieldName = "Christine"
local sFormVar = "Root/Variables/Global/Christine/sForm"

--Voice Variables
local sVoiceVar = "Root/Variables/Global/Christine/sVoiceJob"
local sVoiceThis = "Lancer"

--Costume Variables
local sCostumeName = "Normal"
local sCostumeVar = "Root/Variables/Costumes/Christine/sCostumeHuman"

--Actor Portrait
local sDialoguePath = "Root/Images/Portraits/ChristineDialogue/"

--Sprite
local sSpritePath = "Root/Images/Sprites/Christine_Human/"
local sSpcFrame = "Christine_Human"
local fIndexX = 0
local fIndexY = 4

--Combat
local sCombatPortrait = "Root/Images/Portraits/Combat/Christine_Human"
local sCountermask    = "Null"
local sTurnPortrait   = "Root/Images/AdventureUI/TurnPortraits/Christine_Human"

--Delayed-Load Groupings
local sDialogueCostumeGrouping = "Christine Costume Human"
local sCombatCostumeGrouping   = sDialogueCostumeGrouping .. " Combat"
local saDialogueGroupingList   = gsaChristineDialogueList
local saCombatGroupingList     = gsaChristineCombatList

-- |[Stop Check]|
--If not in this form, just flag the variable.
local sCharacterForm = VM_GetVar(sFormVar, "S")
VM_SetVar(sCostumeVar, "S", sCostumeName)
if(sCharacterForm ~= "Human") then
    return
end

-- |[ ======================================== Dialogue ======================================== ]|
-- |[Talk Sprites]|
DialogueActor_Push(sActorName)
    DialogueActor_SetProperty("Add Emotion", "Neutral",  sDialoguePath .. "Neutral", true)
    DialogueActor_SetProperty("Add Emotion", "Happy",    sDialoguePath .. "Happy", true)
    DialogueActor_SetProperty("Add Emotion", "Blush",    sDialoguePath .. "Blush", true)
    DialogueActor_SetProperty("Add Emotion", "Smirk",    sDialoguePath .. "Smirk", true)
    DialogueActor_SetProperty("Add Emotion", "Sad",      sDialoguePath .. "Sad", true)
    DialogueActor_SetProperty("Add Emotion", "Scared",   sDialoguePath .. "Scared", true)
    DialogueActor_SetProperty("Add Emotion", "Offended", sDialoguePath .. "Offended", true)
    DialogueActor_SetProperty("Add Emotion", "Serious",  sDialoguePath .. "Neutral", true)
    DialogueActor_SetProperty("Add Emotion", "Cry",      sDialoguePath .. "Cry", true)
    DialogueActor_SetProperty("Add Emotion", "Laugh",    sDialoguePath .. "Laugh", true)
    DialogueActor_SetProperty("Add Emotion", "Angry",    sDialoguePath .. "Angry", true)
    DialogueActor_SetProperty("Add Emotion", "PDU",      sDialoguePath .. "PDU", true)
DL_PopActiveObject()

-- |[Voice Handling]|
VM_SetVar(sVoiceVar, "S", sVoiceThis)

-- |[ ====================================== Field Sprite ====================================== ]|
-- |[Sprite]|
if(EM_Exists(sFieldName) == true) then
	EM_PushEntity(sFieldName)
	
		--Base.
		fnSetCharacterGraphics(sSpritePath, true)
		
		--Animation properties.
		TA_SetProperty("Auto Animates", false)
		TA_SetProperty("Y Oscillates", false)
		TA_SetProperty("No Footstep Sound", false)
        TA_SetProperty("No Automatic Shadow", false)
		
		--Special frames.
        TA_SetProperty("Wipe Special Frames")
        TA_SetProperty("Add Special Frame", "Wounded",  "Root/Images/Sprites/Special/" .. sSpcFrame .. "|Wounded")
        TA_SetProperty("Add Special Frame", "Crouch",   "Root/Images/Sprites/Special/" .. sSpcFrame .. "|Crouch")
        TA_SetProperty("Add Special Frame", "Laugh0",   "Root/Images/Sprites/Special/Christine|Laugh0")
        TA_SetProperty("Add Special Frame", "Laugh1",   "Root/Images/Sprites/Special/Christine|Laugh1")
        TA_SetProperty("Add Special Frame", "Sad",      "Root/Images/Sprites/Special/Christine|Sad")
		TA_SetProperty("Add Special Frame", "BedSleep", "Root/Images/Sprites/Special/Christine_Human|BedSleep")
		TA_SetProperty("Add Special Frame", "BedWake",  "Root/Images/Sprites/Special/Christine_Human|BedWake")
		TA_SetProperty("Add Special Frame", "BedFull",  "Root/Images/Sprites/Special/Christine_Human|BedFull")
		TA_SetProperty("Add Special Frame", "BedWakeR", "Root/Images/Sprites/Special/Christine_Human|BedWakeR")
		TA_SetProperty("Add Special Frame", "BedFullR", "Root/Images/Sprites/Special/Christine_Human|BedFullR")
	DL_PopActiveObject()
end
        
-- |[Lua Sprite Lookup]|
for i = 1, giCharSpritesTotal, 1 do
    if(gsCharSprites[i][1] == sFieldName) then
        gsCharSprites[i][2] = sSpritePath .. "SW|0"
        break
    end
end

-- |[Idle Animation]|
SpriteHelper:fnSetActiveSprite(sFieldName)
SpriteHelper:fnSetIdleAnimationAdvanced("Root/Images/Sprites/IdleAnim/Christine_Human|", 6, {0, 0, 0, 0, 0, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 11, 11, 11, 11})
SpriteHelper:fnClear()

-- |[ ===================================== Combat and UI ====================================== ]|
-- |[Combat Portraits, UI Images]|
if(AdvCombat_GetProperty("Does Party Member Exist", sCombatName) == true) then
    AdvCombat_SetProperty("Push Party Member", sCombatName)

        --Portraits
        AdvCombatEntity_SetProperty("Combat Portrait",    sCombatPortrait)
        AdvCombatEntity_SetProperty("Combat Countermask", "Null")
        AdvCombatEntity_SetProperty("Turn Icon",          sTurnPortrait)
    
        --Vendor Sprites
        AdvCombatEntity_SetProperty("Vendor Image", 0, sSpritePath .. "SW|0")
        AdvCombatEntity_SetProperty("Vendor Image", 1, sSpritePath .. "SW|1")
        AdvCombatEntity_SetProperty("Vendor Image", 2, sSpritePath .. "SW|2")
        AdvCombatEntity_SetProperty("Vendor Image", 3, sSpritePath .. "SW|3")
	
        --UI Rendering Positions
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Basemenu,          22, 435)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Base,      237, 115)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Ally,     -100, -42)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Inspector, 280,  91)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equipment,         72,  80)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Skills,           106, 110)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status,           106,  69)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Vendor,            -7, 108)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Trainer,           38, 174)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Victory,   668,  89)
    
        --Face Table, used on the Equipment UI to show who has the item equipped.
        local fLft = (fIndexX+0) * gci_FaceTable_Size
        local fTop = (fIndexY+0) * gci_FaceTable_Size
        local fRgt = (fIndexX+1) * gci_FaceTable_Size
        local fBot = (fIndexY+1) * gci_FaceTable_Size
        AdvCombatEntity_SetProperty("Face Table Data", fLft, fTop, fRgt, fBot, "Root/Images/AdventureUI/ItemIcons/CharacterFaces")

    DL_PopActiveObject()
end

-- |[ ================================= Loading and Unloading ================================== ]|
--Order all of Christines not-in-use dialogue and combat images to unload their data. Then order
-- all in-use images to load their data. Images that are in multiple sets will correctly load their
-- data if the load is called after the unload.
if(gbUnloadDialoguePortraitsWhenDialogueCloses == false) then
    fnUnloadGrouping(saDialogueGroupingList, sDialogueCostumeGrouping)
    fnLoadGrouping  (saDialogueGroupingList, sDialogueCostumeGrouping)
end

--Combat always loads.
fnUnloadGrouping(saCombatGroupingList, sCombatCostumeGrouping)
fnLoadGrouping  (saCombatGroupingList, sCombatCostumeGrouping)
