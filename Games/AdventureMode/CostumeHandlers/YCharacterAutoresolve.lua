-- |[ =============================== Character Costume Resolver =============================== ]|
--Used to help standardize form and costume handlers, simply pass in the character's name and the
-- correct job will be resolved along with the correct costume.
--Why isn't a given job simply mapped directly to a costume? Some jobs (such as 55's algorithms)
-- all share the same form, so this script maps jobs to form to costume.
local sBasePath = fnResolvePath()
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sCharacterName = LM_GetScriptArgument(0)

--Diagnostics.
local bDiagnostics = Debug_GetFlag("Costume: All") or Debug_GetFlag("Costume: Character Resolve")
Debug_PushPrint(bDiagnostics, "[ === Character Autoresolve ===]\n")
Debug_Print("Provided name: " .. sCharacterName .. "\n")
Debug_Print("Originator: " .. LM_GetCallStack(1) .. "\n")

--Special: "No Leader" means no character.
if(sCharacterName == "No Leader") then
    Debug_PopPrint("[ === Done Character Autoresolve, No Leader ===]\n")
    return
end

--Setup.
gsCharacterFinalName = "Mei"
gsCharacterFinalJob = "Human"

-- |[ ================================== Costume Resolve List ================================== ]|
--Created at program start, this list contains characters and remappings. The job listing and
-- form listing are in paralell, and map a character's job to their form for costume purposes.
-- If a character is not found, the manual lists below are used.

--Table format:
--gzCostumeResolveList[i].sCharacterName        Name of the character. Ex: "Mei", "Florentina"
--gzCostumeResolveList[i].saJobListing          String list of jobs. Ex: {"Fencer", "Nightshade"}
--gzCostumeResolveList[i].saFormListing         String list of forms. Ex: {"Human", "Alraune"}

-- |[Check Table Entries]|
--Make sure the list has been initialized.
local bResolveListHandledCase = false

--Get the slot of this entry, if it exists. If not it will come back -1. -1 is also returned if the
-- list has not been initialized yet.
Debug_Print("Checking costume list.\n")
local i = fnGetSlotOnCostumeList(sCharacterName)
if(i ~= -1) then
    
    --Diagnostics.
    Debug_Print(" Was on costume list, slot:" .. i .. "\n")
    
    --Set it as the final name.
    gsCharacterFinalName = sCharacterName
    
    --Run subroutine to get the final job entry. If it's "Fail" then the entry wasn't found.
    gsCharacterFinalJob = fnGetFormFromCostume(i)
    Debug_Print(" Character final job: " .. gsCharacterFinalJob .. "\n")
    if(gsCharacterFinalJob ~= "Fail") then
        bResolveListHandledCase = true
    else
        Debug_Print(" Failed, unable to resolve job.\n")
    end
end

-- |[ =================================== Chapter 1 Resolver =================================== ]|
if(bResolveListHandledCase == true) then
    --Don't run the manual resolvers.
    
-- |[ ======= Miscellaneous ======== ]|
elseif(sCharacterName == "Rubberbee") then
    gsCharacterFinalName = "Rubberbee"
    gsCharacterFinalJob = "Rubber"
    
elseif(sCharacterName == "Rubberraune") then
    gsCharacterFinalName = "Rubberraune"
    gsCharacterFinalJob = "Rubber"
    
-- |[ =================================== Chapter 2 Resolver =================================== ]|
-- |[ ============ Sanya =========== ]|
elseif(sCharacterName == "Sanya") then
    
    --Set internal name, get job.
    gsCharacterFinalName = "Sanya"
    AdvCombat_SetProperty("Push Party Member", "Sanya")
        local sCurrentJob = AdvCombatEntity_GetProperty("Current Job")
    DL_PopActiveObject()
    
    --Change job to a costume form.
    if(sCurrentJob == "Hunter") then
        gsCharacterFinalJob = "Human"
    elseif(sCurrentJob == "Agontea") then
        gsCharacterFinalJob = "Sevavi"
    elseif(sCurrentJob == "Skystriker") then
        gsCharacterFinalJob = "Werebat"
    elseif(sCurrentJob == "Ranger") then
        gsCharacterFinalJob = "Harpy"
    elseif(sCurrentJob == "Scofflaw") then
        gsCharacterFinalJob = "Bunny"
    end
    
-- |[ ========== Empress =========== ]|
elseif(sCharacterName == "Empress") then
    
    --Set internal name, get job.
    gsCharacterFinalName = "Empress"
    AdvCombat_SetProperty("Push Party Member", "Empress")
        local sCurrentJob = AdvCombatEntity_GetProperty("Current Job")
    DL_PopActiveObject()
    
    --Change job to a costume form.
    if(sCurrentJob == "Conquerer") then
        gsCharacterFinalJob = "Conquerer"
    end
    
-- |[ ============ Izuna =========== ]|
elseif(sCharacterName == "Izuna") then
    
    --Set internal name, get job.
    gsCharacterFinalName = "Izuna"
    AdvCombat_SetProperty("Push Party Member", "Izuna")
        local sCurrentJob = AdvCombatEntity_GetProperty("Current Job")
    DL_PopActiveObject()
    
    --Change job to a costume form.
    if(sCurrentJob == "Miko") then
        gsCharacterFinalJob = "Miko"
    end
    
-- |[ ============ Zeke ============ ]|
elseif(sCharacterName == "Zeke") then
    
    --Set internal name, get job.
    gsCharacterFinalName = "Zeke"
    AdvCombat_SetProperty("Push Party Member", "Zeke")
        local sCurrentJob = AdvCombatEntity_GetProperty("Current Job")
    DL_PopActiveObject()
    
    --Change job to a costume form.
    if(sCurrentJob == "Goat") then
        gsCharacterFinalJob = "Goat"
    end
    
-- |[ ============ Odar ============ ]|
elseif(sCharacterName == "Odar") then
    
    --Set internal name, get job.
    gsCharacterFinalName = "Odar"
    AdvCombat_SetProperty("Push Party Member", "Odar")
        local sCurrentJob = AdvCombatEntity_GetProperty("Current Job")
    DL_PopActiveObject()
    
    --Change job to a costume form.
    if(sCurrentJob == "Prince") then
        gsCharacterFinalJob = "Prince"
    end

-- |[ =================================== Chapter 5 Resolver =================================== ]|
-- |[ ========= Christine ========== ]|
elseif(sCharacterName == "Christine") then
    
    --Set internal name, get job.
    gsCharacterFinalName = "Christine"
    AdvCombat_SetProperty("Push Party Member", "Christine")
        local sCurrentJob = AdvCombatEntity_GetProperty("Current Job")
    DL_PopActiveObject()
    
    --Change job to a costume form.
    if(sCurrentJob == "Teacher") then
        gsCharacterFinalJob = "Male"
    elseif(sCurrentJob == "Lancer") then
        gsCharacterFinalJob = "Human"
    elseif(sCurrentJob == "Repair Unit") then
        gsCharacterFinalJob = "Golem"
    elseif(sCurrentJob == "Brass Brawler") then
        gsCharacterFinalJob = "SteamDroid"
    elseif(sCurrentJob == "Drone") then
        gsCharacterFinalJob = "LatexDrone"
    elseif(sCurrentJob == "Starseer") then
        gsCharacterFinalJob = "Darkmatter"
    elseif(sCurrentJob == "Handmaiden") then
        gsCharacterFinalJob = "Dreamer"
    elseif(sCurrentJob == "Live Wire") then
        gsCharacterFinalJob = "Electrosprite"
    elseif(sCurrentJob == "Thunder Rat") then
        gsCharacterFinalJob = "Raiju"
    elseif(sCurrentJob == "Commissar") then
        gsCharacterFinalJob = "Doll"
    elseif(sCurrentJob == "Menialist") then
        gsCharacterFinalJob = "Secrebot"
    end

-- |[ =========== JX-101 =========== ]|
elseif(sCharacterName == "JX-101") then
    
    --Set internal name, get job.
    gsCharacterFinalName = "JX-101"
    AdvCombat_SetProperty("Push Party Member", "JX-101")
        local sCurrentJob = AdvCombatEntity_GetProperty("Current Job")
    DL_PopActiveObject()
    
    --Change job to a costume form.
    if(sCurrentJob == "Tactician") then
        gsCharacterFinalJob = "Tactician"
    end

-- |[ =========== Sophie =========== ]|
elseif(sCharacterName == "Sophie") then
    gsCharacterFinalName = "Sophie"
    gsCharacterFinalJob = "Golem"
    
-- |[ =========== SX-399 =========== ]|
elseif(sCharacterName == "SX-399") then

    --Set internal name, get job.
    gsCharacterFinalName = "SX-399"
    AdvCombat_SetProperty("Push Party Member", "SX-399")
        local sCurrentJob = AdvCombatEntity_GetProperty("Current Job")
    DL_PopActiveObject()
    
    --Change job to a costume form.
    if(sCurrentJob == "Young Droid") then
        gsCharacterFinalJob = "Young Droid"
    elseif(sCurrentJob == "Shocktrooper") then
        gsCharacterFinalJob = "Shocktrooper"
    end
    
-- |[ =========== Tiffany ========== ]|
elseif(sCharacterName == "Tiffany") then
    
    --Set internal name, get job.
    gsCharacterFinalName = "Tiffany"
    AdvCombat_SetProperty("Push Party Member", "Tiffany")
        local sCurrentJob = AdvCombatEntity_GetProperty("Current Job")
    DL_PopActiveObject()
    
    --Change job to a costume form.
    if(sCurrentJob == "Assault") then
        gsCharacterFinalJob = "Assault"
    elseif(sCurrentJob == "Support") then
        gsCharacterFinalJob = "Support"
    elseif(sCurrentJob == "Subvert") then
        gsCharacterFinalJob = "Subvert"
    elseif(sCurrentJob == "Spearhead") then
        gsCharacterFinalJob = "Spearhead"
    end
    
-- |[ ============ Sammy =========== ]|
--Not a conventional character. Used during the moth movie.
elseif(sCharacterName == "Sammy") then
    gsCharacterFinalName = "Sammy"
    gsCharacterFinalJob = "ActionMoth"
    
-- |[ =================================== Chapter 6 Resolver =================================== ]|
-- |[ ======================================== No Match ======================================== ]|
else

    --Debug.
    Debug_Print("Checking mod characters.\n")
    
    --Run across the mods.
    local bCaughtCharacter = fnExecModScript("CostumeHandlers/YCharacterAutoresolve.lua", sCharacterName)
    if(bCaughtCharacter == false) then
        Debug_ForcePrint("Error: No match for " .. sCharacterName .. " was found.\n")
    end
end

-- |[ ======================================== Finish Up ======================================= ]|
--Fire costume autoresolve.
Debug_Print("Executing " .. gsCharacterFinalName .. "_" .. gsCharacterFinalJob .. "\n")
LM_ExecuteScript(gsCostumeAutoresolve, gsCharacterFinalName .. "_" .. gsCharacterFinalJob)
Debug_PopPrint("[ === Done Character Autoresolve ===]\n")

--Clean.
gsCharacterFinalName = nil
gsCharacterFinalJob = nil
