-- |[ ============================== JX-101, Steam Droid, Normal =============================== ]|
-- |[Flag]|
VM_SetVar("Root/Variables/Costumes/JX-101/sCostumeTactician", "S", "Normal")

-- |[Form Check]|
--Sprite/Portrait/UI is only modified if the character is currently in this class.
local sCurrentJob = VM_GetVar("Root/Variables/Global/JX-101/sCurrentJob", "S")
if(sCurrentJob ~= "Tactician") then return end

-- |[Dialogue]|
if(DialogueActor_Exists("JX-101") == true) then
    DialogueActor_Push("JX-101")
        DialogueActor_SetProperty("Add Emotion", "Neutral", "Root/Images/Portraits/JX-101/Neutral",  true)
        DialogueActor_SetProperty("Add Emotion", "HatA",    "Root/Images/Portraits/JX-101/HatA",  true)
        DialogueActor_SetProperty("Add Emotion", "HatB",    "Root/Images/Portraits/JX-101/HatB",  true)
        DialogueActor_SetProperty("Add Emotion", "HatC",    "Root/Images/Portraits/JX-101/HatC",  true)
    DL_PopActiveObject()
end

-- |[Sprite]|
if(EM_Exists("JX-101") == true) then
	EM_PushEntity("JX-101")
	
		--Base.
		fnSetCharacterGraphics("Root/Images/Sprites/JX-101/", true)
		
		--Animation properties.
		TA_SetProperty("Auto Animates", false)
		TA_SetProperty("Y Oscillates", false)
		TA_SetProperty("No Footstep Sound", false)
        TA_SetProperty("No Automatic Shadow", false)
		
		--Special frames.
        TA_SetProperty("Wipe Special Frames")
	DL_PopActiveObject()
end
        
-- |[Lua Sprite Lookup]|
for i = 1, giCharSpritesTotal, 1 do
    if(gsCharSprites[i][1] == "JX-101") then
        gsCharSprites[i][2] = "Root/Images/Sprites/JX-101/SW|0"
        break
    end
end

-- |[Combat Portraits, UI Images]|
if(AdvCombat_GetProperty("Does Party Member Exist", "JX-101") == true) then
    AdvCombat_SetProperty("Push Party Member", "JX-101")

        --Portraits
        AdvCombatEntity_SetProperty("Combat Portrait",    "Root/Images/Portraits/Combat/JX-101")
        AdvCombatEntity_SetProperty("Combat Countermask", "Null")
        AdvCombatEntity_SetProperty("Turn Icon",          "Root/Images/AdventureUI/TurnPortraits/JX-101")
    
        --Vendor Sprites
        AdvCombatEntity_SetProperty("Vendor Image", 0, "Root/Images/Sprites/JX-101/SW|0")
        AdvCombatEntity_SetProperty("Vendor Image", 1, "Root/Images/Sprites/JX-101/SW|1")
        AdvCombatEntity_SetProperty("Vendor Image", 2, "Root/Images/Sprites/JX-101/SW|2")
        AdvCombatEntity_SetProperty("Vendor Image", 3, "Root/Images/Sprites/JX-101/SW|3")
	
        --UI Rendering Positions
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Basemenu,           7, 416)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Base,      226,  95)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Ally,     -115, -59)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Inspector, 267,  72)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equipment,         56,  50)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Skills,            95,  87)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status,            91,  46)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Vendor,           -26,  91)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Trainer,           23, 154)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Victory,   654,  84)
    
        --Face Table, used on the Equipment UI to show who has the item equipped.
        local fIndexX = 3
        local fIndexY = 6
        local fLft = (fIndexX+0) * gci_FaceTable_Size
        local fTop = (fIndexY+0) * gci_FaceTable_Size
        local fRgt = (fIndexX+1) * gci_FaceTable_Size
        local fBot = (fIndexY+1) * gci_FaceTable_Size
        AdvCombatEntity_SetProperty("Face Table Data", fLft, fTop, fRgt, fBot, "Root/Images/AdventureUI/ItemIcons/CharacterFaces")

    DL_PopActiveObject()
end
