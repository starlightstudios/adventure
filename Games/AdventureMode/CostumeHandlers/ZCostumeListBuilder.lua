-- |[ ================================== Costume Routing File ================================== ]|
--Whenever the costumes option on the campfire menu is selected, this file runs and builds a list
-- of costumes for available characters.
--Note that the characters don't have to be in the party, necessarily. For example, and for no
-- obvious reason, Unit 2856 has a nude costume. The script checks for chapter and usually quest
-- variables to determine who should be on the list.
local iCurrentChapter = VM_GetVar("Root/Variables/Global/ChapterComplete/iCurrentChapter", "N")

-- |[Password Case]|
--If this script is called with an argument, it is a password attempting to unlock a costume.
local sPassword = "Null"
local iArgs = LM_GetNumOfArgs()
if(iArgs == 1) then
    sPassword = LM_GetScriptArgument(0)
end

-- |[ ====================================== All Chapters ====================================== ]|
--Only occurs if this is a password case. Handles passwords common to all chapters.
if(iArgs >= 1) then
    
    -- |[O_O]|
    if(sPassword == "I love Salty") then
        io.write("O_o\n")
        return
    
    -- |[Disable XP]|
    --Party members will be unable to gain more XP, but keep their current levels.
    elseif(sPassword == "Broken Treadmill") then
        VM_SetVar( "Root/Variables/CrossChapter/EXPTracker/iBarEXPGain", "N", 1)
        AM_SetProperty("Flag Handled Password")
        return
    
    --Re-enables XP gain if it was disabled.
    elseif(sPassword == "Niko Repair Service") then
        VM_SetVar( "Root/Variables/CrossChapter/EXPTracker/iBarEXPGain", "N", 0)
        AM_SetProperty("Flag Handled Password")
        return
    
    -- |[Lock Level to 0]|
    --Sets all party members to level 0 when they next gain XP, and blocks XP gain.
    elseif(sPassword == "I love kumquat") then
        VM_SetVar( "Root/Variables/CrossChapter/EXPTracker/iAlwaysLevelZero", "N", 1)
        AM_SetProperty("Flag Handled Password")
        return
    
    --Undoes the above, but doesn't return removed levels.
    elseif(sPassword == "Review WHI on Steam and Itch") then
        VM_SetVar( "Root/Variables/CrossChapter/EXPTracker/iAlwaysLevelZero", "N", 0)
        AM_SetProperty("Flag Handled Password")
        return
    
    -- |[Return to Chapter Select]|
    --Punts the player back to the chapter select screen. Loses all progress.
    elseif(sPassword == "Escape Pipe") then
    
        --Variables.
        local iCurrentChapter = VM_GetVar("Root/Variables/Global/ChapterComplete/iCurrentChapter", "N")
    
        --Chapter 1
        if(iCurrentChapter == 1.0) then
            LM_ExecuteScript(gsRoot .. "Chapter 1/Cleanup/000 Entry Point.lua")
        elseif(iCurrentChapter == 2.0) then
            LM_ExecuteScript(gsRoot .. "Chapter 2/Cleanup/000 Entry Point.lua")
        elseif(iCurrentChapter == 5.0) then
            LM_ExecuteScript(gsRoot .. "Chapter 5/Cleanup/000 Entry Point.lua")
        end
        AL_BeginTransitionTo("Nowhere", "FORCEPOS:10.0x2.0x0")
        AM_SetProperty("Flag Handled Password")
        return
    end
end

-- |[ ======================================== Chapter 1 ======================================= ]|
if(iCurrentChapter == 1.0) then
    
    -- |[ ======================= Password ======================= ]|
    --Passwords for chapter 1.
    if(sPassword == "Carry Fire") then
        VM_SetVar("Root/Variables/Costumes/Mei/iQueenBee", "N", 1.0)
        AM_SetProperty("Flag Handled Password")
        return
    elseif(sPassword == "Jiggly Squeaky") then
        VM_SetVar("Root/Variables/Costumes/Mei/iRubberQueen", "N", 1.0)
        AM_SetProperty("Flag Handled Password")
        return
    elseif(sPassword ~= "Null") then
        return
    end

    -- |[Setup]|
    local bAtLeastOneCostume = false
    
    -- |[ ========================= Mei ========================== ]|
    --Forms.
    local iHasBeeForm    = VM_GetVar("Root/Variables/Global/Mei/iHasBeeForm",    "N")
    local iHasSlimeForm  = VM_GetVar("Root/Variables/Global/Mei/iHasSlimeForm",  "N")
    local iHasZombeeForm = VM_GetVar("Root/Variables/Global/Mei/iHasZombeeForm", "N")
    
    --Unlocked Costumes.
    local iMei_QueenBee = VM_GetVar("Root/Variables/Costumes/Mei/iQueenBee", "N")
    local iMei_RubberQueen = VM_GetVar("Root/Variables/Costumes/Mei/iRubberQueen", "N")
    
    -- |[ ========== Slime Costumes ========== ]|
    if(iHasSlimeForm == 1.0) then
        
        -- |[Setup]|
        --Flag.
        bAtLeastOneCostume = true
        
        --Get the current costume for Slime.
        local sSlimeCostume = VM_GetVar("Root/Variables/Costumes/Mei/sCostumeSlime", "S")
        
        -- |[Category]|
        if(sSlimeCostume == "Normal") then
            AM_SetProperty("Register Costume Heading", "MeiSlime", "Mei", "Mei Slime", "Slime", "Root/Images/Portraits/MeiDialogue/SlimeNeutral")
            AM_SetProperty("Set Heading Alignments",   "MeiSlime", 0.0, -1.0, 170.0, 90.0, 408.0, 408.0)
        elseif(sSlimeCostume == "Blue") then
            AM_SetProperty("Register Costume Heading", "MeiSlime", "Mei", "Mei Slime", "Slime", "Root/Images/Portraits/MeiDialogue/SlimeBlueNeutral")
            AM_SetProperty("Set Heading Alignments",   "MeiSlime", 0.0, -1.0, 170.0, 90.0, 408.0, 408.0)
        elseif(sSlimeCostume == "Ink") then
            AM_SetProperty("Register Costume Heading", "MeiSlime", "Mei", "Mei Slime", "Slime", "Root/Images/Portraits/MeiDialogue/SlimeInkNeutral")
            AM_SetProperty("Set Heading Alignments",   "MeiSlime", 0.0, -1.0, 170.0, 90.0, 408.0, 408.0)
        elseif(sSlimeCostume == "Pink") then
            AM_SetProperty("Register Costume Heading", "MeiSlime", "Mei", "Mei Slime", "Slime", "Root/Images/Portraits/MeiDialogue/SlimePinkNeutral")
            AM_SetProperty("Set Heading Alignments",   "MeiSlime", 0.0, -1.0, 170.0, 90.0, 408.0, 408.0)
        elseif(sSlimeCostume == "Yellow") then
            AM_SetProperty("Register Costume Heading", "MeiSlime", "Mei", "Mei Slime", "Slime", "Root/Images/Portraits/MeiDialogue/SlimeYellowNeutral")
            AM_SetProperty("Set Heading Alignments",   "MeiSlime", 0.0, -1.0, 170.0, 90.0, 408.0, 408.0)
        end
        
        -- |[Costume Handling]|
        --Base entry.
        AM_SetProperty("Register Costume Entry", "MeiSlime", "Normal", "Root/Images/Portraits/MeiDialogue/SlimeNeutral", "CostumeHandlers/Mei/Slime_Normal.lua")
        AM_SetProperty("Set Entry Alignments",   "MeiSlime", "Normal", 0.0, -1.0, 170.0, 90.0, 408.0, 408.0)
        
        --Blue.
        AM_SetProperty("Register Costume Entry", "MeiSlime", "Blue", "Root/Images/Portraits/MeiDialogue/SlimeBlueNeutral", "CostumeHandlers/Mei/Slime_Blue.lua")
        AM_SetProperty("Set Entry Alignments",   "MeiSlime", "Blue", 0.0, -1.0, 170.0, 90.0, 408.0, 408.0)
        
        --Pink.
        AM_SetProperty("Register Costume Entry", "MeiSlime", "Pink", "Root/Images/Portraits/MeiDialogue/SlimePinkNeutral", "CostumeHandlers/Mei/Slime_Pink.lua")
        AM_SetProperty("Set Entry Alignments",   "MeiSlime", "Pink", 0.0, -1.0, 170.0, 90.0, 408.0, 408.0)
        
        --Ink.
        AM_SetProperty("Register Costume Entry", "MeiSlime", "Ink", "Root/Images/Portraits/MeiDialogue/SlimeInkNeutral", "CostumeHandlers/Mei/Slime_Ink.lua")
        AM_SetProperty("Set Entry Alignments",   "MeiSlime", "Ink", 0.0, -1.0, 170.0, 90.0, 408.0, 408.0)
        
        --Yellow.
        AM_SetProperty("Register Costume Entry", "MeiSlime", "Yellow", "Root/Images/Portraits/MeiDialogue/SlimeYellowNeutral", "CostumeHandlers/Mei/Slime_Yellow.lua")
        AM_SetProperty("Set Entry Alignments",   "MeiSlime", "Yellow", 0.0, -1.0, 170.0, 90.0, 408.0, 408.0)
        
    end
    
    -- |[ ======= Rubberqueen Costume ======== ]|
    if(iMei_RubberQueen == 1.0) then
        
        -- |[Setup]|
        --Flag.
        bAtLeastOneCostume = true
        
        --Get the current costume for Human and Rubber.
        local sHumanCostume = VM_GetVar("Root/Variables/Costumes/Mei/sCostumeHuman", "S")
        local sRubberCostume = VM_GetVar("Root/Variables/Costumes/Mei/sCostumeRubber", "S")
        
        -- |[Human Handling]|
        if(sHumanCostume == "Normal") then
            AM_SetProperty("Register Costume Heading", "MeiHuman", "Mei", "Mei Human", "Human", "Root/Images/Portraits/MeiDialogue/HumanNeutral")
            AM_SetProperty("Set Heading Alignments",   "MeiHuman", 0.0, 0.0, 135.0, 69.0, 408.0, 408.0)
        else
            AM_SetProperty("Register Costume Heading", "MeiHuman", "Mei", "Mei Human", "Human", "Root/Images/Portraits/MeiDialogue/RubberQueenNeutral")
            AM_SetProperty("Set Heading Alignments",   "MeiHuman", 00.0, 0.0, 135.0, 21.0, 408.0, 408.0)
        end
        
        --Base entry.
        AM_SetProperty("Register Costume Entry", "MeiHuman", "Normal", "Root/Images/Portraits/MeiDialogue/HumanNeutral", "CostumeHandlers/Mei/Human_Normal.lua")
        AM_SetProperty("Set Entry Alignments",   "MeiHuman", "Normal", 0.0, 0.0, 135.0, 69.0, 408.0, 408.0)
        
        --Rubber Queen.
        AM_SetProperty("Register Costume Entry", "MeiHuman", "Queen", "Root/Images/Portraits/MeiDialogue/RubberQueenNeutral", "CostumeHandlers/Mei/Human_RubberQueen.lua")
        AM_SetProperty("Set Entry Alignments",   "MeiHuman", "Queen", 0.0, 0.0, 116.0, 21.0, 408.0, 408.0)

        -- |[Rubber Handling]|
        if(sRubberCostume == "Normal") then
            AM_SetProperty("Register Costume Heading", "MeiRubber", "Mei", "Mei Rubber", "Rubber", "Root/Images/Portraits/MeiDialogue/Rubber")
            AM_SetProperty("Set Heading Alignments",   "MeiRubber", 0.0, 0.0, 69.0, 69.0, 408.0, 408.0)
        else
            AM_SetProperty("Register Costume Heading", "MeiRubber", "Mei", "Mei Rubber", "Rubber", "Root/Images/Portraits/MeiDialogue/RubberQueenNeutral")
            AM_SetProperty("Set Heading Alignments",   "MeiRubber", 00.0, 0.0, 135.0, 21.0, 408.0, 408.0)
        end
        
        --Base entry.
        AM_SetProperty("Register Costume Entry", "MeiRubber", "Normal", "Root/Images/Portraits/MeiDialogue/Rubber", "CostumeHandlers/Mei/Rubber_Normal.lua")
        AM_SetProperty("Set Entry Alignments",   "MeiRubber", "Normal", 0.0, 0.0, 69.0, 69.0, 408.0, 408.0)
        
        --Rubber Queen.
        AM_SetProperty("Register Costume Entry", "MeiRubber", "Queen", "Root/Images/Portraits/MeiDialogue/RubberQueenNeutral", "CostumeHandlers/Mei/Rubber_RubberQueen.lua")
        AM_SetProperty("Set Entry Alignments",   "MeiRubber", "Queen", 0.0, 0.0, 116.0, 21.0, 408.0, 408.0)
    end
    
    -- |[ ======== Bee Queen Costume ========= ]|
    if(iHasBeeForm == 1.0 and iMei_QueenBee == 1.0) then
        
        --Flag.
        bAtLeastOneCostume = true
        
        --Get the current costume for Bee.
        local sBeeCostume = VM_GetVar("Root/Variables/Costumes/Mei/sCostumeBee", "S")
        
        --Heading.
        if(sBeeCostume == "Normal") then
            AM_SetProperty("Register Costume Heading", "MeiBee", "Mei", "Mei Bee", "Bee", "Root/Images/Portraits/MeiDialogue/BeeNeutral")
            AM_SetProperty("Set Heading Alignments",   "MeiBee", 0.0, 0.0, 135.0, 69.0, 408.0, 408.0)
        else
            AM_SetProperty("Register Costume Heading", "MeiBee", "Mei", "Mei Bee", "Bee", "Root/Images/Portraits/MeiDialogue/BeeQueenNeutral")
            AM_SetProperty("Set Heading Alignments",   "MeiBee", -23.0, 0.0, 135.0, 69.0, 408.0, 408.0)
        end
        
        --Base entry.
        AM_SetProperty("Register Costume Entry", "MeiBee", "Normal", "Root/Images/Portraits/MeiDialogue/BeeNeutral", "CostumeHandlers/Mei/Bee_Normal.lua")
        AM_SetProperty("Set Entry Alignments",   "MeiBee", "Normal", 0.0, 0.0, 135.0, 69.0, 408.0, 408.0)
        
        --Queen Bee.
        AM_SetProperty("Register Costume Entry", "MeiBee", "Queen", "Root/Images/Portraits/MeiDialogue/BeeQueenNeutral", "CostumeHandlers/Mei/Bee_Queen.lua")
        AM_SetProperty("Set Entry Alignments",   "MeiBee", "Queen", -23.0, 0.0, 135.0, 69.0, 408.0, 408.0)
    end
    
    -- |[ ======= Zombee Queen Costume ======= ]|
    if(iHasZombeeForm == 1.0 and iMei_QueenBee == 1.0) then
        
        --Flag.
        bAtLeastOneCostume = true
        
        --Get the current costume for Bee.
        local sZombeeCostume = VM_GetVar("Root/Variables/Costumes/Mei/sCostumeZombee", "S")
        
        --Heading.
        if(sZombeeCostume == "Normal") then
            AM_SetProperty("Register Costume Heading", "MeiZombee", "Mei", "Mei Zombee", "Zombee", "Root/Images/Portraits/MeiDialogue/BeeMC")
            AM_SetProperty("Set Heading Alignments",   "MeiZombee", 0.0, 0.0, 135.0, 69.0, 408.0, 408.0)
        else
            AM_SetProperty("Register Costume Heading", "MeiZombee", "Mei", "Mei Zombee", "Zombee", "Root/Images/Portraits/MeiDialogue/BeeQueenMC")
            AM_SetProperty("Set Heading Alignments",   "MeiZombee", -25.0, -17.0, 135.0, 69.0, 408.0, 408.0)
        end
        
        --Base entry.
        AM_SetProperty("Register Costume Entry", "MeiZombee", "Normal", "Root/Images/Portraits/MeiDialogue/BeeMC", "CostumeHandlers/Mei/Zombee_Normal.lua")
        AM_SetProperty("Set Entry Alignments",   "MeiZombee", "Normal", 0.0, 0.0, 135.0, 69.0, 408.0, 408.0)
        
        --Queen Bee.
        AM_SetProperty("Register Costume Entry", "MeiZombee", "Queen", "Root/Images/Portraits/MeiDialogue/BeeQueenMC", "CostumeHandlers/Mei/Zombee_Queen.lua")
        AM_SetProperty("Set Entry Alignments",   "MeiZombee", "Queen", -25.0, -17.0, 135.0, 69.0, 408.0, 408.0)
    end
    
    -- |[No Costumes]|
    --If no costume headings were registered, register the "No Costumes" entry.
    if(bAtLeastOneCostume == false) then
        AM_SetProperty("Register Costume Heading", "NoCostume", "", "", "", "Root/Images/AdventureUI/CampfireMenuIcon/NoCostumes")
        AM_SetProperty("Set Heading Alignments",   "NoCostume", 0.0, 0.0, 0.0, 0.0, 96.0, 96.0)
    end
    
-- |[ ======================================== Chapter 2 ======================================= ]|
elseif(iCurrentChapter == 2.0) then

    --None yet.

-- |[ ======================================== Chapter 5 ======================================= ]|
elseif(iCurrentChapter == 5.0) then

    -- |[Password]|
    if(sPassword ~= "Null") then
        
        --Flag to unlock the terminal in Sector 99.
        if(sPassword == "My Best Friend") then
            AM_SetProperty("Flag Handled Password")
            VM_SetVar("Root/Variables/Chapter5/Scenes/iTerminalHasBackers", "N", 1.0)
        end
        
        --Allows the Gala outfits.
        if(sPassword == "Dressed To Retire") then
            AM_SetProperty("Flag Handled Password")
            VM_SetVar("Root/Variables/Costumes/Christine/iGolemGala", "N", 1.0)
        end
        
        --Allows Sundress 55 to wreak havoc.
        if(sPassword == "Pulse Picnic Panic") then
            AM_SetProperty("Flag Handled Password")
            VM_SetVar("Root/Variables/Costumes/Tiffany/iHasSundress", "N", 1.0)
        end
        
        return
    end
    
    --If the password was anything else, ignore it.
    if(sPassword ~= "Null") then return end

    -- |[Setup]|
    local bAtLeastOneCostume = false

    -- |[Special Bypass]|
    --The player cannot change costumes at the gala for any reason.
    local iIsGalaTime     = VM_GetVar("Root/Variables/Chapter5/Scenes/iIsGalaTime", "N")
    local iReachedBiolabs = VM_GetVar("Root/Variables/Chapter5/Scenes/iReachedBiolabs", "N")
    if(iIsGalaTime == 1.0 and iReachedBiolabs == 0.0) then
        AM_SetProperty("Register Costume Heading", "NoCostume", "", "", "", "Root/Images/AdventureUI/CampfireMenuIcon/Unavailable")
        AM_SetProperty("Set Heading Alignments",   "NoCostume", 0.0, 0.0, 0.0, 0.0, 96.0, 96.0)
    end
    
    --Player cannot change costumes during the dream sequence. There is only one savepoint here.
    local sRoomName = AL_GetProperty("Name")
    if(sRoomName == "RegulusFlashbackA") then 
        AM_SetProperty("Register Costume Heading", "NoCostume", "", "", "", "Root/Images/AdventureUI/CampfireMenuIcon/Unavailable")
        AM_SetProperty("Set Heading Alignments",   "NoCostume", 0.0, 0.0, 0.0, 0.0, 96.0, 96.0)
    end

    -- |[Christine]|
    --Forms.
    local iHasDollForm  = VM_GetVar("Root/Variables/Global/Christine/iHasDollForm", "N")
    local iHasGolemForm = VM_GetVar("Root/Variables/Global/Christine/iHasGolemForm", "N")
    local iHasRaijuForm = VM_GetVar("Root/Variables/Global/Christine/iHasRaijuForm", "N")
    
    --Unlocked Costumes.
    local iChristine_GolemGala   = VM_GetVar("Root/Variables/Costumes/Christine/iGolemGala", "N")
    local iChristine_DollSweater = VM_GetVar("Root/Variables/Costumes/Christine/iDollSweater", "N")
    local iChristine_DollNude    = VM_GetVar("Root/Variables/Costumes/Christine/iDollNude", "N")
    local iChristine_HumanNude   = VM_GetVar("Root/Variables/Costumes/Christine/iHumanNude", "N")
    local iChristine_RaijuNude   = VM_GetVar("Root/Variables/Costumes/Christine/iRaijuNude", "N")
    local iTiffany_HasSundress   = VM_GetVar("Root/Variables/Costumes/Tiffany/iHasSundress", "N")
    
    --Other.
    local iMet55InLowerRegulus = VM_GetVar("Root/Variables/Chapter5/Scenes/iMet55InLowerRegulus", "N")
    
    --If Christine has not gone to lower Regulus City in the story yet, she can't change costumes.
    if(iMet55InLowerRegulus == 1.0) then
        
        --Christine's display name.
        local sDisplayName = "Christine"
        AdvCombat_SetProperty("Push Party Member", "Christine")
            sDisplayName = AdvCombatEntity_GetProperty("Display Name")
        DL_PopActiveEntity()
        
        --Golem. Also restricts access to human, since if Christine does not have golem form, she does not have her female form yet.
        if(iHasGolemForm == 1.0 and iChristine_GolemGala == 1.0) then
            
            --Heading.
            bAtLeastOneCostume = true
            AM_SetProperty("Register Costume Heading", "ChristineGolem", "Christine", sDisplayName, "Golem", "Root/Images/Portraits/ChristineDialogue/Golem|Smirk")
            AM_SetProperty("Set Heading Alignments",   "ChristineGolem", -10.0, 0.0, 102.0, 79.0, 454.0, 454.0)
            
            --Base entry.
            AM_SetProperty("Register Costume Entry", "ChristineGolem", "Normal", "Root/Images/Portraits/ChristineDialogue/Golem|Smirk", "CostumeHandlers/Christine/Golem_Normal.lua")
            AM_SetProperty("Set Entry Alignments",   "ChristineGolem", "Normal", -10.0, 0.0, 102.0, 79.0, 454.0, 454.0)
            
            --Gala dress.
            AM_SetProperty("Register Costume Entry", "ChristineGolem", "Gala Dress", "Root/Images/Portraits/ChristineDialogue/GolemGala|Smirk", "CostumeHandlers/Christine/Golem_GalaDress.lua")
            AM_SetProperty("Set Entry Alignments",   "ChristineGolem", "Gala Dress", 0.0, 0.0, 110.0, 73.0, 405.0, 405.0)
        end
        
        --Raiju.
        if(iHasRaijuForm == 1.0) then
            
            --Heading.
            bAtLeastOneCostume = true
            AM_SetProperty("Register Costume Heading", "ChristineRaiju", "Christine", sDisplayName, "Raiju", "Root/Images/Portraits/ChristineDialogue/RaijuClothes|Smirk")
            AM_SetProperty("Set Heading Alignments",   "ChristineRaiju", 0.0, -20.0, 86.0, 102.0, 446.0, 438.0)
            
            --Normal.
            AM_SetProperty("Register Costume Entry", "ChristineRaiju", "Normal", "Root/Images/Portraits/ChristineDialogue/RaijuClothes|Smirk", "CostumeHandlers/Christine/Raiju_Normal.lua")
            AM_SetProperty("Set Entry Alignments",   "ChristineRaiju", "Normal", 0.0, -20.0, 86.0, 102.0, 446.0, 438.0)
            
            --Nude.
            AM_SetProperty("Register Costume Entry", "ChristineRaiju", "Nude", "Root/Images/Portraits/ChristineDialogue/Raiju|Smirk", "CostumeHandlers/Christine/Raiju_Nude.lua")
            AM_SetProperty("Set Entry Alignments",   "ChristineRaiju", "Nude", 0.0, -20.0, 86.0, 102.0, 446.0, 438.0)
        end
        
        --Doll.
        if(iHasDollForm == 1.0) then
            
            --Heading.
            bAtLeastOneCostume = true
            AM_SetProperty("Register Costume Heading", "ChristineDoll", "Christine", sDisplayName, "Doll", "Root/Images/Portraits/ChristineDialogue/Doll|Smirk")
            AM_SetProperty("Set Heading Alignments",   "ChristineDoll", -11.0, -10.0, 50.0, 101.0, 440.0, 400.0)
            
            --Normal.
            AM_SetProperty("Register Costume Entry", "ChristineDoll", "Normal", "Root/Images/Portraits/ChristineDialogue/Doll|Smirk", "CostumeHandlers/Christine/Doll_Normal.lua")
            AM_SetProperty("Set Entry Alignments",   "ChristineDoll", "Normal", -11.0, -10.0, 50.0, 101.0, 440.0, 400.0)
            
            --Sweater.
            AM_SetProperty("Register Costume Entry", "ChristineDoll", "Sweater", "Root/Images/Portraits/ChristineDialogue/DollSweater|Smirk", "CostumeHandlers/Christine/Doll_Sweater.lua")
            AM_SetProperty("Set Entry Alignments",   "ChristineDoll", "Sweater", 10.0, -10.0, 50.0, 101.0, 440.0, 400.0)
            
            --Nude.
            AM_SetProperty("Register Costume Entry", "ChristineDoll", "Nude", "Root/Images/Portraits/ChristineDialogue/DollNude|Smirk", "CostumeHandlers/Christine/Doll_Nude.lua")
            AM_SetProperty("Set Entry Alignments",   "ChristineDoll", "Nude", 10.0, -10.0, 50.0, 101.0, 440.0, 400.0)
        end

    end

    -- |[55]|
    if(iMet55InLowerRegulus == 1.0) then
        if(iTiffany_HasSundress == 1.0) then
            
            --Heading.
            bAtLeastOneCostume = true
            AM_SetProperty("Register Costume Heading", "TiffanyOutfit", "Tiffany", "55", "All Jobs", "Root/Images/Portraits/TiffanyDialogue/Neutral")
            AM_SetProperty("Set Heading Alignments",   "TiffanyOutfit", 0.0, 5.0, 128.0, 45.0, 454.0, 454.0)
            
            --Base entry.
            AM_SetProperty("Register Costume Entry", "TiffanyOutfit", "Normal", "Root/Images/Portraits/TiffanyDialogue/Neutral", "CostumeHandlers/Tiffany/Doll_Normal.lua")
            AM_SetProperty("Set Entry Alignments",   "TiffanyOutfit", "Normal", 0.0, 5.0, 128.0, 45.0, 454.0, 454.0)
            
            --Gala dress.
            AM_SetProperty("Register Costume Entry", "TiffanyOutfit", "Sundress", "Root/Images/Portraits/TiffanySundressDialogue/Neutral", "CostumeHandlers/Tiffany/Doll_Sundress.lua")
            AM_SetProperty("Set Entry Alignments",   "TiffanyOutfit", "Sundress", -9.0, -10.0, 128.0, 45.0, 454.0, 454.00)
    
        end
    end

    -- |[SX-399]|
    -- |[Sophie]|
    local iTalkedToSophie   = VM_GetVar("Root/Variables/Chapter5/Scenes/iTalkedToSophie", "N")
    local iSophie_GolemGala = VM_GetVar("Root/Variables/Costumes/Sophie/iGolemGala", "N")
    if(iTalkedToSophie == 1.0 and iSophie_GolemGala == 1.0) then
        
        --Heading.
        bAtLeastOneCostume = true
        AM_SetProperty("Register Costume Heading", "SophieGolem", "Sophie", "Sophie", "Golem", "Root/Images/Portraits/SophieDialogue/Smirk")
        AM_SetProperty("Set Heading Alignments",   "SophieGolem", 0.0, 0.0, 142.0, 115.0, 344.0, 344.0)
        
        --Normal.
        AM_SetProperty("Register Costume Entry", "SophieGolem", "Normal", "Root/Images/Portraits/SophieDialogue/Smirk", "CostumeHandlers/Sophie/Golem_Normal.lua")
        AM_SetProperty("Set Entry Alignments",   "SophieGolem", "Normal", 0.0, 0.0, 142.0, 115.0, 344.0, 344.0)
        
        --Gala Dress.
        AM_SetProperty("Register Costume Entry", "SophieGolem", "Gala Dress", "Root/Images/Portraits/SophieDialogueGala/Smirk", "CostumeHandlers/Sophie/Golem_GalaDress.lua")
        AM_SetProperty("Set Entry Alignments",   "SophieGolem", "Gala Dress", -5.0, 7.0, 142.0, 115.0, 344.0, 344.0)
    end
    
    -- |[56]|
    --local i56_DollNude = VM_GetVar("Root/Variables/Costumes/56/iDollNude", "N")
    
    -- |[No Costumes]|
    --If no costume headings were registered, register the "No Costumes" entry.
    if(bAtLeastOneCostume == false) then
        AM_SetProperty("Register Costume Heading", "NoCostume", "", "", "", "Root/Images/AdventureUI/CampfireMenuIcon/NoCostumes")
        AM_SetProperty("Set Heading Alignments",   "NoCostume", 0.0, 0.0, 0.0, 0.0, 96.0, 96.0)
    end

-- |[ ====================================== Error Handler ===================================== ]|
else
end

-- |[ ========================================== Mods ========================================== ]|
--Modded characters/chapters fire here to see if they are applicable.
fnExecModScript("CostumeHandlers/ZCostumeListBuilder.lua", sPassword)
