-- |[ ================================ Tiffany, Doll, Sundress ================================= ]|
-- |[Setup]|
--Set all variables for this script. Special frames and alignments need to be entered below.
-- First, constants across this character.
local sActorName = "Tiffany"
local sCombatName = "Tiffany"
local sFieldName = "Tiffany"
local sFormVar = "Root/Variables/Global/Tiffany/sCurrentJob"

--Costume Variables
local sCostumeName = "Sundress"
local sCostumeVar = "Root/Variables/Costumes/Tiffany/sCostumeDoll"

--Actor Portrait
local sDialoguePath = "Root/Images/Portraits/TiffanySundressDialogue/"
local sReversePath  = "Root/Images/Portraits/TiffanySundressDialogueRev/"

--Sprite
local sSpritePath  = "Root/Images/Sprites/TiffanySundress/"
local sSpecialPath = "Root/Images/Sprites/Special/Tiffany|"
local fIndexX = 1
local fIndexY = 6

--Combat
local sCombatPortrait = "Root/Images/Portraits/Combat/Tiffany_Assault" --Modified below
local sCountermask    = "Null"
local sTurnPortrait   = "Root/Images/AdventureUI/TurnPortraits/TiffanySundress"

--Delayed-Load Groupings
local sDialogueCostumeGrouping = "Tiffany Costume Sundress"
local sCombatCostumeGrouping   = "Tiffany Costume Assault Combat" --Modified below
local saDialogueGroupingList   = gsaTiffanyDialogueList
local saCombatGroupingList     = gsaTiffanyCombatList

-- |[Stop Check]|
--If not in this form, just flag the variable.
local sCharacterForm = VM_GetVar(sFormVar, "S")
VM_SetVar(sCostumeVar, "S", sCostumeName)
if(sCharacterForm ~= "Assault" and sCharacterForm ~= "Support" and sCharacterForm ~= "Subvert" and sCharacterForm ~= "Spearhead") then
    return
end

-- |[ ======================================== Dialogue ======================================== ]|
--Standard.
fnSetTiffanyDialogue(sDialoguePath, sReversePath)

-- |[ ====================================== Field Sprite ====================================== ]|
fnSetTiffanyFieldSprite(sSpritePath, sSpecialPath)
fnSetTiffanyLuaSpriteLookup(sSpritePath)

-- |[ ===================================== Combat and UI ====================================== ]|
--Regardless of job, always uses the sundress.
sCombatCostumeGrouping = "Tiffany Costume Sundress Assault Combat"
sCombatPortrait = "Root/Images/Portraits/Combat/Tiffany_Sundress"

--Basic.
fnSetTiffanyCombatData(sCombatName, sCombatPortrait, sCountermask, sTurnPortrait, sSpritePath, fIndexX, fIndexY)

--Alignments.
fnSetTiffanyAlignment(sCombatName, sCharacterForm)

-- |[ ================================= Loading and Unloading ================================== ]|
--Order all not-in-use dialogue and combat images to unload their data. Then order all in-use 
-- images to load their data. Images that are in multiple sets will correctly load their data
-- if the load is called after the unload.
--Dialogue is ordered to load if this flag is false. Otherwise, it loads on demand.
if(gbUnloadDialoguePortraitsWhenDialogueCloses == false) then
    fnUnloadGrouping(saDialogueGroupingList, sDialogueCostumeGrouping)
    fnLoadGrouping  (saDialogueGroupingList, sDialogueCostumeGrouping)
end

--Combat always loads.
fnUnloadGrouping(saCombatGroupingList, sCombatCostumeGrouping)
fnLoadGrouping  (saCombatGroupingList, sCombatCostumeGrouping)
