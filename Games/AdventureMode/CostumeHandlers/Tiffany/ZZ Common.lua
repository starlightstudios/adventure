-- |[ ======================= Common Functions and Variables for Tiffany ======================= ]|
--Some parts of Tiffany's costume code is identical across all costumes. This handles those.

-- |[ ================================ fnSetTiffanyAlignments() ================================ ]|
--Sets alignments for the UI based on which of the subclasses Tiffany is in.
function fnSetTiffanyAlignment(psCombatName, psClassName)
    
    --Arg check.
    if(psCombatName == nil) then return end
    if(psClassName  == nil) then return end
    
    --Verify the character exists.
    if(AdvCombat_GetProperty("Does Party Member Exist", psCombatName) == false) then return end
    
    --Get costume. Sundress applies to all jobs.
    local sCostumeName = VM_GetVar("Root/Variables/Costumes/Tiffany/sCostumeDoll", "S")
    
    --Common: Push the character.
    AdvCombat_SetProperty("Push Party Member", psCombatName)
    
        -- |[Sundress]|
        --Ignores the class and always sets the same values.
        if(sCostumeName == "Sundress") then
            AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Basemenu,         -75, 408)
            AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Base,      127,  88)
            AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Ally,     -217, -76)
            AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Inspector, 201,  81)
            AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equipment,        -13, 127)
            AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Skills,             6,  94)
            AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status,            25,  68)
            AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Vendor,          -118,  47)
            AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Trainer,          -41, 199)
            AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Victory,   619,  84)
            AdvCombatEntity_SetProperty("Override Combat Y", -1)
    
        -- |[Assault]|
        elseif(psClassName == "Assault") then
            AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Basemenu,        -117, 308)
            AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Base,      108,   0)
            AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Ally,     -251,-160)
            AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Inspector, 153, -28)
            AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equipment,        -59, -46)
            AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Skills,           -35,  -9)
            AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status,           -33, -52)
            AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Vendor,          -142, -13)
            AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Trainer,         -116,  64)
            AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Victory,   594,  29)
            AdvCombatEntity_SetProperty("Override Combat Y", 0)
        
        -- |[Spearhead]|
        elseif(psClassName == "Spearhead") then
            AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Basemenu,        -121, 317)
            AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Base,       99,   7)
            AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Ally,     -246,-149)
            AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Inspector, 140, -26)
            AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equipment,        -71, -44)
            AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Skills,           -39,  -3)
            AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status,           -31, -42)
            AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Vendor,          -156,   0)
            AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Trainer,          -94,  96)
            AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Victory,   596,  33)
            AdvCombatEntity_SetProperty("Override Combat Y", 0)
        
        -- |[Subvert]|
        elseif(psClassName == "Subvert") then
            AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Basemenu,        -123, 244)
            AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Base,       99, -74)
            AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Ally,     -241,-231)
            AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Inspector, 145, -97)
            AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equipment,        -65, -31)
            AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Skills,           -35, -61)
            AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status,           -42,-113)
            AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Vendor,          -156,   0)
            AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Trainer,         -130, 114)
            AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Victory,   588,  -3)
            AdvCombatEntity_SetProperty("Override Combat Y", 0)
        
        -- |[Support]|
        else
            AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Basemenu,        -112, 300)
            AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Base,       95,  -9)
            AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Ally,     -250,-179)
            AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Inspector, 132, -39)
            AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equipment,        -33,  -4)
            AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Skills,           -28,  -1)
            AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status,           -41, -48)
            AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Vendor,          -149,   0)
            AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Trainer,          -91,  89)
            AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Victory,   594,  22)
            AdvCombatEntity_SetProperty("Override Combat Y", 0)
        end
    
    --Common: Pop the character.
    DL_PopActiveObject()
end

-- |[ ================================= fnSetTiffanyDialogue() ================================= ]|
--Sets emotions using the dialogue path. Tiffany has a "reverse" emotion to handle her eye color swap.
function fnSetTiffanyDialogue(psDialoguePath, psReversePath)

    --Argument check.
    if(psDialoguePath == nil) then return end
    if(psReversePath  == nil) then return end

    --Verify dialogue actor exists.
    if(DialogueActor_Exists("Tiffany") == false) then return end

    --Push.
    DialogueActor_Push("Tiffany")

        --Normal.
        DialogueActor_SetProperty("Add Emotion", "Neutral",  psDialoguePath .. "Neutral",  true)
        DialogueActor_SetProperty("Add Emotion", "Down",     psDialoguePath .. "Down",     true)
        DialogueActor_SetProperty("Add Emotion", "Blush",    psDialoguePath .. "Blush",    true)
        DialogueActor_SetProperty("Add Emotion", "Smirk",    psDialoguePath .. "Smirk",    true)
        DialogueActor_SetProperty("Add Emotion", "Upset",    psDialoguePath .. "Upset",    true)
        DialogueActor_SetProperty("Add Emotion", "Smug",     psDialoguePath .. "Smug",     true)
        DialogueActor_SetProperty("Add Emotion", "Offended", psDialoguePath .. "Offended", true)
        DialogueActor_SetProperty("Add Emotion", "Angry",    psDialoguePath .. "Angry",    true)
        DialogueActor_SetProperty("Add Emotion", "Broken",   psDialoguePath .. "Broken",   true)
        DialogueActor_SetProperty("Add Emotion", "Cry",      psDialoguePath .. "Cry",      true)
        
        --Reverse.
        DialogueActor_SetProperty("Add Emotion Rev", "Neutral",  psDialoguePath .. "Neutral",  true)
        DialogueActor_SetProperty("Add Emotion Rev", "Down",     psDialoguePath .. "Down",     true)
        DialogueActor_SetProperty("Add Emotion Rev", "Blush",    psDialoguePath .. "Blush",    true)
        DialogueActor_SetProperty("Add Emotion Rev", "Smirk",    psDialoguePath .. "Smirk",    true)
        DialogueActor_SetProperty("Add Emotion Rev", "Upset",    psDialoguePath .. "Upset",    true)
        DialogueActor_SetProperty("Add Emotion Rev", "Smug",     psDialoguePath .. "Smug",     true)
        DialogueActor_SetProperty("Add Emotion Rev", "Offended", psDialoguePath .. "Offended", true)
        DialogueActor_SetProperty("Add Emotion Rev", "Angry",    psDialoguePath .. "Angry",    true)
        DialogueActor_SetProperty("Add Emotion Rev", "Broken",   psDialoguePath .. "Broken",   true)
        DialogueActor_SetProperty("Add Emotion Rev", "Cry",      psDialoguePath .. "Cry",      true)
    
    --Pop.
    DL_PopActiveObject()

end

-- |[ =============================== fnSetTiffanyFieldSprite() ================================ ]|
--If the field sprite for Mei exists, updates its properties and special frames.
function fnSetTiffanyFieldSprite(psSpritePath, psSpecialPath)
    
    -- |[Argument Check]|
    if(psSpritePath == nil) then return end
    if(psSpecialPath == nil) then return end
    
    -- |[Error Check]|
    --No field sprite, exit.
    if(EM_Exists("Tiffany") == false) then return end
    
    --Push.
	EM_PushEntity("Tiffany")
    
        -- |[Base]|
        fnSetCharacterGraphics(psSpritePath, true)
        
        -- |[Animation properties]|
        TA_SetProperty("Auto Animates",       false)
        TA_SetProperty("Y Oscillates",        false)
        TA_SetProperty("No Footstep Sound",   false)
        TA_SetProperty("No Automatic Shadow", false)
        
        -- |[Special frames]|
        TA_SetProperty("Wipe Special Frames")
        TA_SetProperty("Add Special Frame", "Chair0",  psSpecialPath .. "Chair0")
        TA_SetProperty("Add Special Frame", "Chair1",  psSpecialPath .. "Chair1")
        TA_SetProperty("Add Special Frame", "Chair2",  psSpecialPath .. "Chair2")
        TA_SetProperty("Add Special Frame", "Chair3",  psSpecialPath .. "Chair3")
        TA_SetProperty("Add Special Frame", "Chair4",  psSpecialPath .. "Chair4")
        TA_SetProperty("Add Special Frame", "Crouch",  psSpecialPath .. "Crouch")
        TA_SetProperty("Add Special Frame", "Wounded", psSpecialPath .. "Wounded")
        TA_SetProperty("Add Special Frame", "Downed",  psSpecialPath .. "Downed")
        TA_SetProperty("Add Special Frame", "Stealth", psSpecialPath .. "Stealth")
        TA_SetProperty("Add Special Frame", "Draw0",   psSpecialPath .. "Draw0")
        TA_SetProperty("Add Special Frame", "Draw1",   psSpecialPath .. "Draw1")
        TA_SetProperty("Add Special Frame", "Draw2",   psSpecialPath .. "Draw2")
        TA_SetProperty("Add Special Frame", "Exec0",   psSpecialPath .. "Exec0")
        TA_SetProperty("Add Special Frame", "Exec1",   psSpecialPath .. "Exec1")
        TA_SetProperty("Add Special Frame", "Exec2",   psSpecialPath .. "Exec2")
        TA_SetProperty("Add Special Frame", "Exec3",   psSpecialPath .. "Exec3")
        TA_SetProperty("Add Special Frame", "Exec4",   psSpecialPath .. "Exec4")
        TA_SetProperty("Add Special Frame", "Exec5",   psSpecialPath .. "Exec5")
    
        -- |[Idle Animation]|
        --Tiffany has idle animations for both costumes that are basically the same.
        local sPathName = "Tiffany"
        local iaPattern = {0, 1, 2, 3, 4, 5, 6, 3, 4, 5, 6, 3, 4, 5, 6, 3, 4, 5, 6, 2, 1, 0}
        
        --Sundress, change path name.
        if(psSpritePath == "Root/Images/Sprites/TiffanySundress/") then
            sPathName = "TiffanySundress"
        end
        
        --Set.
        TA_SetProperty("Idle Animation Flag", true)
        TA_SetProperty("Allocate Idle Frames", #iaPattern)
        for i = 0, #iaPattern-1, 1 do
            TA_SetProperty("Set Idle Frame", i, "Root/Images/Sprites/IdleAnim/" .. sPathName .. "|" .. iaPattern[i+1])
        end
        TA_SetProperty("Set Idle TPF", 12)
        TA_SetProperty("Set Idle Loop Frame", -1)
    
    --Clean.
    DL_PopActiveObject()

end

-- |[ ============================= fnSetTiffanyLuaSpriteLookup() ============================== ]|
--Updates which sprites the lua code uses to refer to this character.
function fnSetTiffanyLuaSpriteLookup(psSpritePath)
    
    --Argument check.
    if(psSpritePath == nil) then return end
    
    --Iterate.
    for i = 1, giCharSpritesTotal, 1 do
        
        --Match.
        if(gsCharSprites[i][1] == "Tiffany") then
            gsCharSprites[i][2] = psSpritePath .. "SW|0"
            break
        end
    end
    
end

-- |[ ================================ fnSetTiffanyCombatData() ================================ ]|
--Sets combat data for the given party member. Does not set UI rendering positions.
function fnSetTiffanyCombatData(psCombatName, psPortrait, psCountermask, psTurnPortrait, psSpritePath, pfIndexX, pfIndexY)
    
    --Argument check.
    if(psCombatName   == nil) then return end
    if(psPortrait     == nil) then return end
    if(psCountermask  == nil) then return end
    if(psTurnPortrait == nil) then return end
    if(psSpritePath   == nil) then return end
    if(pfIndexX       == nil) then return end
    if(pfIndexY       == nil) then return end
    
    --Existence check.
    if(AdvCombat_GetProperty("Does Party Member Exist", psCombatName) == false) then return end
    
    --Push.
    AdvCombat_SetProperty("Push Party Member", psCombatName)

        --Portraits
        AdvCombatEntity_SetProperty("Combat Portrait",    psPortrait)
        AdvCombatEntity_SetProperty("Combat Countermask", psCountermask)
        AdvCombatEntity_SetProperty("Turn Icon",          psTurnPortrait)
    
        --Vendor Sprites
        AdvCombatEntity_SetProperty("Vendor Image", 0, psSpritePath .. "SW|0")
        AdvCombatEntity_SetProperty("Vendor Image", 1, psSpritePath .. "SW|1")
        AdvCombatEntity_SetProperty("Vendor Image", 2, psSpritePath .. "SW|2")
        AdvCombatEntity_SetProperty("Vendor Image", 3, psSpritePath .. "SW|3")
    
        --Face Table, used on the Equipment UI to show who has the item equipped.
        local fLft = (pfIndexX+0) * gci_FaceTable_Size
        local fTop = (pfIndexY+0) * gci_FaceTable_Size
        local fRgt = (pfIndexX+1) * gci_FaceTable_Size
        local fBot = (pfIndexY+1) * gci_FaceTable_Size
        AdvCombatEntity_SetProperty("Face Table Data", fLft, fTop, fRgt, fBot, "Root/Images/AdventureUI/ItemIcons/CharacterFaces")

    --Clean.
    DL_PopActiveObject()
    
end
