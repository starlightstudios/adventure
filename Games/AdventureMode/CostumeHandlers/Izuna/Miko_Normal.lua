-- |[ =================================== Izuna, Miko, Normal ================================== ]|
-- |[Setup]|
--Set all variables for this script. Special frames and alignments need to be entered below.
-- First, constants across this character.
local sActorName = "Izuna"
local sCombatName = "Izuna"
local sFieldName = "Izuna"
local sFormVar = "Root/Variables/Global/Izuna/sCurrentJob"

--Costume Variables
local sCostumeName = "Normal"
local sCostumeVar = "Root/Variables/Costumes/Izuna/sCostumeMiko"

--Actor Portrait
local sDialoguePath = "Root/Images/Portraits/IzunaMikoDialogue/"

--Sprite
local sSpritePath = "Root/Images/Sprites/Izuna_Miko/"
local sSpecialPath = "Root/Images/Sprites/Special/Izuna_Miko|"
local fIndexX = 4
local fIndexY = 6

--Combat
local sCombatPortrait = "Root/Images/Portraits/Combat/Izuna_Miko"
local sCountermask    = "Null"
local sTurnPortrait   = "Root/Images/AdventureUI/TurnPortraits/Izuna_Miko"

--Delayed-Load Groupings
local sDialogueCostumeGrouping = "Izuna Costume Miko"
local sCombatCostumeGrouping   = sDialogueCostumeGrouping .. " Combat"
local saDialogueGroupingList   = gsaIzunaDialogueList
local saCombatGroupingList     = gsaIzunaCombatList

--Costume Function Grouping
local zaCostumeFunctions = gzaIzunaCostumeFunctions

-- |[Stop Check]|
--If not in this form, just flag the variable.
local sCharacterForm = VM_GetVar(sFormVar, "S")
VM_SetVar(sCostumeVar, "S", sCostumeName)
if(sCharacterForm ~= "Miko") then
    return
end

-- |[ ======================================== Dialogue ======================================== ]|
--Standard.
zaCostumeFunctions.fnSetDialogue(sDialoguePath)

-- |[ ====================================== Field Sprite ====================================== ]|
zaCostumeFunctions.fnSetFieldSprite(sSpritePath, sSpecialPath, false)
zaCostumeFunctions.fnSetLuaSpriteLookup(sSpritePath)
zaCostumeFunctions.fnSetIdleAnimation("Null", 0, 0, -1)

-- |[ ===================================== Combat and UI ====================================== ]|
--Basic.
zaCostumeFunctions.fnSetCombatData(sCombatName, sCombatPortrait, sCountermask, sTurnPortrait, sSpritePath, fIndexX, fIndexY)

--Alignments.
if(AdvCombat_GetProperty("Does Party Member Exist", sCombatName) == true) then
    AdvCombat_SetProperty("Push Party Member", sCombatName)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Basemenu,         -16, 389)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Base,      198,  67)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Ally,     -147, -91)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Inspector, 245,  46)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equipment,         24,  54)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Skills,            68,  71)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status,            57,  39)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Vendor,           -72, 101)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Victory,   643,  74)
    DL_PopActiveObject()
end

-- |[ ================================= Loading and Unloading ================================== ]|
--Order all not-in-use dialogue and combat images to unload their data. Then order all in-use 
-- images to load their data. Images that are in multiple sets will correctly load their data
-- if the load is called after the unload.
if(gbUnloadDialoguePortraitsWhenDialogueCloses == false) then
    fnUnloadGrouping(saDialogueGroupingList, sDialogueCostumeGrouping)
    fnLoadGrouping  (saDialogueGroupingList, sDialogueCostumeGrouping)
end

--Combat always loads.
fnUnloadGrouping(saCombatGroupingList, sCombatCostumeGrouping)
fnLoadGrouping  (saCombatGroupingList, sCombatCostumeGrouping)
