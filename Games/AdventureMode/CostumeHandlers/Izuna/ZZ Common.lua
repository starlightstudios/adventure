-- |[ ======================== Common Functions and Variables for Izuna ======================== ]|
--Some parts of Izuna's costume code is identical across all costumes. This handles those.
local zaCostumeFunctions = {}

-- |[ =================================== fnSetIzunaDialogue =================================== ]|
--Sets Mei's emotions using the given dialogue path.
function zaCostumeFunctions.fnSetDialogue(psDialoguePath)

    --Argument check.
    if(psDialoguePath == nil) then return end

    --Verify dialogue actor exists.
    if(DialogueActor_Exists("Izuna") == false) then return end

    --Push, set.
    DialogueActor_Push("Izuna")
        DialogueActor_SetProperty("Add Emotion", "Neutral", psDialoguePath .. "Neutral", true)
        DialogueActor_SetProperty("Add Emotion", "Happy",   psDialoguePath .. "Happy",   true)
        DialogueActor_SetProperty("Add Emotion", "Cry",     psDialoguePath .. "Cry",     true)
        DialogueActor_SetProperty("Add Emotion", "Smirk",   psDialoguePath .. "Smirk",   true)
        DialogueActor_SetProperty("Add Emotion", "Explain", psDialoguePath .. "Explain", true)
        DialogueActor_SetProperty("Add Emotion", "Jot",     psDialoguePath .. "Jot",     true)
        DialogueActor_SetProperty("Add Emotion", "Ugh",     psDialoguePath .. "Ugh",     true)
        DialogueActor_SetProperty("Add Emotion", "Trip",    psDialoguePath .. "Trip",    true)
        DialogueActor_SetProperty("Add Emotion", "Blush",   psDialoguePath .. "Blush",   true)
    DL_PopActiveObject()
end

-- |[ ================================= fnSetIzunaFieldSprite ================================== ]|
--If the field sprite for Izuna exists, updates its properties and special frames.
function zaCostumeFunctions.fnSetFieldSprite(psSpritePath, psSpecialPath, pbIsAirborne)
    
    --Argument check.
    if(psSpritePath == nil) then return end
    if(psSpecialPath == nil) then return end
    if(pbIsAirborne == nil) then pbIsAirborne = false end
    
    --No field sprite, exit.
    if(EM_Exists("Izuna") == false) then return end
    
    --Push.
    EM_PushEntity("Izuna")
    
        --Base.
        fnSetCharacterGraphics(psSpritePath, true)
        
        --Animation properties for ground forms:
        if(pbIsAirborne == false) then
            TA_SetProperty("Auto Animates",       false)
            TA_SetProperty("Y Oscillates",        false)
            TA_SetProperty("No Footstep Sound",   false)
            TA_SetProperty("No Automatic Shadow", false)
        
        --Airborne forms:
        else
            TA_SetProperty("Auto Animates",       true)
            TA_SetProperty("Y Oscillates",        true)
            TA_SetProperty("No Footstep Sound",   true)
            TA_SetProperty("No Automatic Shadow", false)
        end
        
        --Special frames.
        TA_SetProperty("Wipe Special Frames")
        TA_SetProperty("Add Special Frame", "Wounded", psSpecialPath .. "Wounded")
        TA_SetProperty("Add Special Frame", "Crouch",  psSpecialPath .. "Crouch")
        
        --Water handler.
        TA_SetProperty("Add Special Frame", "WaterOver", "Root/Images/Sprites/Shadows/Depth")
        
        --Sprite Path is "Izuna_Miko". Add falling frames.
        if(psSpritePath == "Root/Images/Sprites/Izuna_Miko/") then
            TA_SetProperty("Add Special Frame", "Fall0",       psSpecialPath .. "Fall0")
            TA_SetProperty("Add Special Frame", "Fall1",       psSpecialPath .. "Fall1")
            TA_SetProperty("Add Special Frame", "Fall2",       psSpecialPath .. "Fall2")
            TA_SetProperty("Add Special Frame", "LyingClosed", psSpecialPath .. "LyingClosed")
            TA_SetProperty("Add Special Frame", "LyingOpen",   psSpecialPath .. "LyingOpen")
            TA_SetProperty("Add Special Frame", "LyingHeal0",  psSpecialPath .. "LyingHeal0")
            TA_SetProperty("Add Special Frame", "LyingHeal1",  psSpecialPath .. "LyingHeal1")
            TA_SetProperty("Add Special Frame", "LyingHeal2",  psSpecialPath .. "LyingHeal2")
        end
        
        --Debug.
        --TA_GetProperty("Print Special Frames")
    
    --Clean.
    DL_PopActiveObject()

end

-- |[ ================================ fnSetIzunaIdleAnimation ================================= ]|
--Sets the idle animation for Izuna in the given form. Pass "Null" to disable. This one uses a simple
-- endless loop case, or a play-once-then-done case.
function zaCostumeFunctions.fnSetIdleAnimation(psDLPathPattern, piFramesTotal, piTPF, piLoopFrame)
    
    --Arg check.
    if(psDLPathPattern == nil) then return end
    if(piFramesTotal   == nil) then return end
    if(piTPF           == nil) then return end
    if(piLoopFrame     == nil) then return end
    
    --Do nothing if the actor does not exist.
    if(EM_Exists("Izuna") == false) then return end
    
    --Disable if "Null".
    if(psDLPathPattern == "Null") then
        EM_PushEntity("Izuna")
            TA_SetProperty("Idle Animation Flag", false)
        DL_PopActiveObject()
        return
    end
    
    --Legality check.
    if(piFramesTotal < 1) then return end
    
    --Otherwise, set the idle animations.
    EM_PushEntity("Izuna")
        TA_SetProperty("Idle Animation Flag", true)
        TA_SetProperty("Allocate Idle Frames", piFramesTotal)
        for i = 0, piFramesTotal-1, 1 do
            TA_SetProperty("Set Idle Frame", i, psDLPathPattern .. i)
        end
        TA_SetProperty("Set Idle TPF", piTPF)
        TA_SetProperty("Set Idle Loop Frame", piLoopFrame)
    DL_PopActiveObject()
end

--Sets the idle animation for Mei in a given form. This one uses a list of the frames, which allows
-- for complicated, though finite, looping. The animation is one-then-done but can re-use frames.
function zaCostumeFunctions.fnSetIdleAnimationAdvanced(psDLPathPattern, piTPF, piaFrameList)
    
    --Arg check.
    if(psDLPathPattern == nil) then return end
    if(piTPF           == nil) then return end
    if(piaFrameList    == nil) then return end
    
    --Do nothing if the actor does not exist.
    if(EM_Exists("Izuna") == false) then return end
    
    --Disable if "Null".
    if(psDLPathPattern == "Null") then
        EM_PushEntity("Izuna")
            TA_SetProperty("Idle Animation Flag", false)
        DL_PopActiveObject()
        return
    end
    
    --Legality check.
    if(#piaFrameList < 1) then return end
    
    --Otherwise, set the idle animations.
    EM_PushEntity("Izuna")
        TA_SetProperty("Idle Animation Flag", true)
        TA_SetProperty("Allocate Idle Frames", #piaFrameList)
        for i = 1, #piaFrameList, 1 do
            TA_SetProperty("Set Idle Frame", i-1, psDLPathPattern .. piaFrameList[i])
        end
        TA_SetProperty("Set Idle TPF", piTPF)
        TA_SetProperty("Set Idle Loop Frame", -1)
    DL_PopActiveObject()
    
end

-- |[ =============================== fnSetIzunaLuaSpriteLookup ================================ ]|
--Updates which sprites the lua code uses to refer to this character.
function zaCostumeFunctions.fnSetLuaSpriteLookup(psSpritePath)
    
    --Argument check.
    if(psSpritePath == nil) then return end
    
    --Iterate.
    for i = 1, giCharSpritesTotal, 1 do
        
        --Match.
        if(gsCharSprites[i][1] == "Izuna") then
            gsCharSprites[i][2] = psSpritePath .. "SW|0"
            break
        end
    end
    
end

-- |[ ================================== fnSetIzunaCombatData ================================== ]|
--Sets combat data for the given party member. Does not set UI rendering positions.
function zaCostumeFunctions.fnSetCombatData(psCombatName, psPortrait, psCountermask, psTurnPortrait, psSpritePath, pfIndexX, pfIndexY)
    
    --Argument check.
    if(psCombatName   == nil) then return end
    if(psPortrait     == nil) then return end
    if(psCountermask  == nil) then return end
    if(psTurnPortrait == nil) then return end
    if(psSpritePath   == nil) then return end
    if(pfIndexX       == nil) then return end
    if(pfIndexY       == nil) then return end
    
    --Existence check.
    if(AdvCombat_GetProperty("Does Party Member Exist", psCombatName) == false) then return end
    
    --Push.
    AdvCombat_SetProperty("Push Party Member", psCombatName)
    
        --Portraits
        AdvCombatEntity_SetProperty("Combat Portrait",    psPortrait)
        AdvCombatEntity_SetProperty("Combat Countermask", psCountermask)
        AdvCombatEntity_SetProperty("Turn Icon",          psTurnPortrait)
    
        --Vendor Sprites
        AdvCombatEntity_SetProperty("Vendor Image", 0, psSpritePath .. "SW|0")
        AdvCombatEntity_SetProperty("Vendor Image", 1, psSpritePath .. "SW|1")
        AdvCombatEntity_SetProperty("Vendor Image", 2, psSpritePath .. "SW|2")
        AdvCombatEntity_SetProperty("Vendor Image", 3, psSpritePath .. "SW|3")
    
        --Face Table, used on the Equipment UI to show who has the item equipped.
        local fLft = (pfIndexX+0) * gci_FaceTable_Size
        local fTop = (pfIndexY+0) * gci_FaceTable_Size
        local fRgt = (pfIndexX+1) * gci_FaceTable_Size
        local fBot = (pfIndexY+1) * gci_FaceTable_Size
        AdvCombatEntity_SetProperty("Face Table Data", fLft, fTop, fRgt, fBot, "Root/Images/AdventureUI/ItemIcons/CharacterFaces")

    --Clean.
    DL_PopActiveObject()
    
end

-- |[Finalize]|
--Set the costume functions as a global.
gzaIzunaCostumeFunctions = zaCostumeFunctions
