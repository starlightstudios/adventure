-- |[ ================================== Mei, Wisphag, Normal ================================== ]|
-- |[Setup]|
--Set all variables for this script. Special frames and alignments need to be entered below.
-- First, constants across this character.
local sActorName = "Mei"
local sCombatName = "Mei"
local sFieldName = "Mei"
local sFormVar = "Root/Variables/Global/Mei/sForm"

--Costume Variables
local sCostumeName = "Normal"
local sCostumeVar = "Root/Variables/Costumes/Mei/sCostumeWisphag"

--Actor Portrait
local sDialoguePath = "Root/Images/Portraits/MeiDialogue/Wisphag"

--Sprite
local sSpritePath = "Root/Images/Sprites/Mei_Wisphag/"
local sSpecialPath = "Root/Images/Sprites/Special/MeiWisphag|"
local fIndexX = 8
local fIndexY = 0

--Combat
local sCombatPortrait = "Root/Images/Portraits/Combat/Mei_Wisphag"
local sCountermask    = "Null"
local sTurnPortrait   = "Root/Images/AdventureUI/TurnPortraits/Mei_Wisphag"

--Delayed-Load Groupings
local sDialogueCostumeGrouping = "Mei Costume Wisphag"
local sCombatCostumeGrouping   = sDialogueCostumeGrouping .. " Combat"
local saDialogueGroupingList   = gsaMeiDialogueList
local saCombatGroupingList     = gsaMeiCombatList

-- |[Stop Check]|
--If not in this form, just flag the variable.
local sCharacterForm = VM_GetVar(sFormVar, "S")
VM_SetVar(sCostumeVar, "S", sCostumeName)
if(sCharacterForm ~= "Wisphag") then
    return
end

-- |[ ======================================== Dialogue ======================================== ]|
--Standard.
fnSetMeiDialogue(sDialoguePath, true)

-- |[ ====================================== Field Sprite ====================================== ]|
fnSetMeiFieldSprite(sSpritePath, sSpecialPath, false)
fnSetMeiLuaSpriteLookup(sSpritePath)
fnSetMeiIdleAnimationAdvanced("Root/Images/Sprites/IdleAnim/Mei_Wisphag|", 12, {0, 1, 2, 3, 0, 1, 2, 3, 0, 1, 2, 3})

-- |[ ===================================== Combat and UI ====================================== ]|
--Basic.
fnSetMeiCombatData(sCombatName, sCombatPortrait, sCountermask, sTurnPortrait, sSpritePath, fIndexX, fIndexY)

--Alignments.
if(AdvCombat_GetProperty("Does Party Member Exist", sCombatName) == true) then
    AdvCombat_SetProperty("Push Party Member", sCombatName)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Basemenu,           -13, 342)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Base,        215,  26)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Ally,       -121,-117)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Inspector,   262,  12)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equipment,           32,  33)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Skills,              82,  37)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status,              79,   1)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Trainer,             -6,  95)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Vendor,             -65,  43)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Victory,     660,  54)
    DL_PopActiveObject()
end

-- |[ ================================= Loading and Unloading ================================== ]|
--Order all not-in-use dialogue and combat images to unload their data. Then order all in-use 
-- images to load their data. Images that are in multiple sets will correctly load their data
-- if the load is called after the unload.
if(gbUnloadDialoguePortraitsWhenDialogueCloses == false) then
    fnUnloadGrouping(saDialogueGroupingList, sDialogueCostumeGrouping)
    fnLoadGrouping  (saDialogueGroupingList, sDialogueCostumeGrouping)
end

--Combat always loads.
fnUnloadGrouping(saCombatGroupingList, sCombatCostumeGrouping)
fnLoadGrouping  (saCombatGroupingList, sCombatCostumeGrouping)
