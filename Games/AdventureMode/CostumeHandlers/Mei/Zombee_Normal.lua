-- |[ ================================== Mei, Zombee, Normal =================================== ]|
-- |[Setup]|
--Set all variables for this script. Special frames and alignments need to be entered below.
-- First, constants across this character.
local sActorName = "Mei"
local sCombatName = "Mei"
local sFieldName = "Mei"
local sFormVar = "Root/Variables/Global/Mei/sForm"

--Costume Variables
local sCostumeName = "Normal"
local sCostumeVar = "Root/Variables/Costumes/Mei/sCostumeZombee"

--Actor Portrait
local sDialoguePath = "Root/Images/Portraits/MeiDialogue/"

--Sprite
local sSpritePath = "Root/Images/Sprites/Mei_Bee_MC/"
local sSpecialPath = "Root/Images/Sprites/Special/MeiBeeMC|"
local fIndexX = 29
local fIndexY = 0

--Combat
local sCombatPortrait = "Root/Images/Portraits/Combat/Mei_Zombee"
local sCountermask    = "Null"
local sTurnPortrait   = "Root/Images/AdventureUI/TurnPortraits/Mei_Zombee"

--Delayed-Load Groupings
local sDialogueCostumeGrouping = "Mei Costume BeeMC"
local sCombatCostumeGrouping   = sDialogueCostumeGrouping .. " Combat"
local saDialogueGroupingList   = gsaMeiDialogueList
local saCombatGroupingList     = gsaMeiCombatList

-- |[Stop Check]|
--If not in this form, just flag the variable.
local sCharacterForm = VM_GetVar(sFormVar, "S")
VM_SetVar(sCostumeVar, "S", sCostumeName)
if(sCharacterForm ~= "Zombee") then
    return
end

-- |[ ======================================== Dialogue ======================================== ]|
--Zombee Mei uses only the bee MC frame.
DialogueActor_Push(sActorName)
    DialogueActor_SetProperty("Add Emotion", "Neutral",  "Root/Images/Portraits/MeiDialogue/BeeMC", true)
    DialogueActor_SetProperty("Add Emotion", "Smirk",    "Root/Images/Portraits/MeiDialogue/BeeMC", true)
    DialogueActor_SetProperty("Add Emotion", "Happy",    "Root/Images/Portraits/MeiDialogue/BeeMC", true)
    DialogueActor_SetProperty("Add Emotion", "Blush",    "Root/Images/Portraits/MeiDialogue/BeeMC", true)
    DialogueActor_SetProperty("Add Emotion", "Sad",      "Root/Images/Portraits/MeiDialogue/BeeMC", true)
    DialogueActor_SetProperty("Add Emotion", "Surprise", "Root/Images/Portraits/MeiDialogue/BeeMC", true)
    DialogueActor_SetProperty("Add Emotion", "Offended", "Root/Images/Portraits/MeiDialogue/BeeMC", true)
    DialogueActor_SetProperty("Add Emotion", "Cry",      "Root/Images/Portraits/MeiDialogue/BeeMC", true)
    DialogueActor_SetProperty("Add Emotion", "Laugh",    "Root/Images/Portraits/MeiDialogue/BeeMC", true)
    DialogueActor_SetProperty("Add Emotion", "Angry",    "Root/Images/Portraits/MeiDialogue/BeeMC", true)
    DialogueActor_SetProperty("Add Emotion", "MC",       "Root/Images/Portraits/MeiDialogue/BeeMC", true)
DL_PopActiveObject()

-- |[ ====================================== Field Sprite ====================================== ]|
fnSetMeiFieldSprite(sSpritePath, sSpecialPath, true)
fnSetMeiLuaSpriteLookup(sSpritePath)
fnSetMeiIdleAnimationAdvanced("Root/Images/Sprites/IdleAnim/Mei_Bee_MC|", 12, {0, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 2, 1, 1, 1, 0, 1, 1})

-- |[ ===================================== Combat and UI ====================================== ]|
--Basic.
fnSetMeiCombatData(sCombatName, sCombatPortrait, sCountermask, sTurnPortrait, sSpritePath, fIndexX, fIndexY)

--Alignments.
if(AdvCombat_GetProperty("Does Party Member Exist", sCombatName) == true) then
    AdvCombat_SetProperty("Push Party Member", sCombatName)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Basemenu,         -85, 424)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Base,      127,  91)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Ally,     -205, -63)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Inspector, 171,  72)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equipment,        -33,  56)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Skills,            -7,  91)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status,            -9,  56)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Trainer,         -106, 154)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Vendor,          -112,  93)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Victory,   611,  76)
    DL_PopActiveObject()
end

-- |[ ================================= Loading and Unloading ================================== ]|
--Order all not-in-use dialogue and combat images to unload their data. Then order all in-use 
-- images to load their data. Images that are in multiple sets will correctly load their data
-- if the load is called after the unload.
if(gbUnloadDialoguePortraitsWhenDialogueCloses == false) then
    fnUnloadGrouping(saDialogueGroupingList, sDialogueCostumeGrouping)
    fnLoadGrouping  (saDialogueGroupingList, sDialogueCostumeGrouping)
end

--Combat always loads.
fnUnloadGrouping(saCombatGroupingList, sCombatCostumeGrouping)
fnLoadGrouping  (saCombatGroupingList, sCombatCostumeGrouping)
