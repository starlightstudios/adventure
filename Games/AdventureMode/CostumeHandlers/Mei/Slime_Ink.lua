-- |[ ==================================== Mei, Slime, Ink ===================================== ]|
-- |[Setup]|
--Set all variables for this script. Special frames and alignments need to be entered below.
-- First, constants across this character.
local sActorName  = "Mei"
local sCombatName = "Mei"
local sFieldName  = "Mei"
local sFormVar    = "Root/Variables/Global/Mei/sForm"

--Costume Variables
local sCostumeName = "Ink"
local sCostumeVar = "Root/Variables/Costumes/Mei/sCostumeSlime"

--Actor Portrait
local sDialoguePath = "Root/Images/Portraits/MeiDialogue/SlimeInk"

--Sprite
local sSpritePath = "Root/Images/Sprites/Mei_SlimeInk/"
local sSpecialPath = "Root/Images/Sprites/Special/MeiSlimeInk|"
local fIndexX = 4
local fIndexY = 0

--Combat
local sCombatPortrait = "Root/Images/Portraits/Combat/Mei_SlimeInk"
local sCountermask    = "Null"
local sTurnPortrait   = "Root/Images/AdventureUI/TurnPortraits/Mei_SlimeInk"

--Delayed-Load Groupings
local sDialogueCostumeGrouping = "Mei Costume Slime"
local sCombatCostumeGrouping   = sDialogueCostumeGrouping .. " Combat"
local saDialogueGroupingList   = gsaMeiDialogueList
local saCombatGroupingList     = gsaMeiCombatList

-- |[Stop Check]|
--If not in this form, just flag the variable.
local sCharacterForm = VM_GetVar(sFormVar, "S")
VM_SetVar(sCostumeVar, "S", sCostumeName)
if(sCharacterForm ~= "Slime") then
    return
end

-- |[ ======================================== Dialogue ======================================== ]|
--Standard.
fnSetMeiDialogue(sDialoguePath, false)

-- |[ ====================================== Field Sprite ====================================== ]|
fnSetMeiFieldSprite(sSpritePath, sSpecialPath, false)
fnSetMeiLuaSpriteLookup(sSpritePath)
fnSetMeiIdleAnimationAdvanced("Root/Images/Sprites/IdleAnim/Mei_SlimeInk|", 12, {0, 1, 2, 3, 4, 5, 6, 7, 8, 8, 8, 5, 6, 7, 8, 8, 8, 5, 6, 7, 8, 8, 8, 5, 6, 7, 8, 8, 8, 4, 3, 2, 1, 0})

-- |[ ===================================== Combat and UI ====================================== ]|
--Basic.
fnSetMeiCombatData(sCombatName, sCombatPortrait, sCountermask, sTurnPortrait, sSpritePath, fIndexX, fIndexY)

--Alignments.
if(AdvCombat_GetProperty("Does Party Member Exist", sCombatName) == true) then
    AdvCombat_SetProperty("Push Party Member", sCombatName)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Basemenu,         -41, 440)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Base,      175, 115)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Ally,     -158, -39)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Inspector, 220,  93)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equipment,          9,  76)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Skills,            41, 117)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status,            35,  63)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Trainer,          -52, 168)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Vendor,           -74, 113)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Victory,   631,  91)
    DL_PopActiveObject()
end

-- |[ ================================= Loading and Unloading ================================== ]|
--Order all not-in-use dialogue and combat images to unload their data. Then order all in-use 
-- images to load their data. Images that are in multiple sets will correctly load their data
-- if the load is called after the unload.
if(gbUnloadDialoguePortraitsWhenDialogueCloses == false) then
    fnUnloadGrouping(saDialogueGroupingList, sDialogueCostumeGrouping)
    fnLoadGrouping  (saDialogueGroupingList, sDialogueCostumeGrouping)
end

--Combat always loads.
fnUnloadGrouping(saCombatGroupingList, sCombatCostumeGrouping)
fnLoadGrouping  (saCombatGroupingList, sCombatCostumeGrouping)
