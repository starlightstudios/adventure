-- |[ ===================================== Mei, Bee, Queen ==================================== ]|
-- |[Setup]|
--Set all variables for this script. Special frames and alignments need to be entered below.
-- First, constants across this character.
local sActorName = "Mei"
local sCombatName = "Mei"
local sFieldName = "Mei"
local sFormVar = "Root/Variables/Global/Mei/sForm"

--Costume Variables
local sCostumeName = "Queen"
local sCostumeVar = "Root/Variables/Costumes/Mei/sCostumeBee"

--Actor Portrait
local sDialoguePath = "Root/Images/Portraits/MeiDialogue/BeeQueen"

--Sprite
local sSpritePath = "Root/Images/Sprites/Mei_BeeQueen/"
local sSpecialPath = "Root/Images/Sprites/Special/MeiBeeQueen|"
local fIndexX = 2
local fIndexY = 0

--Combat
local sCombatPortrait = "Root/Images/Portraits/Combat/Mei_BeeQueen"
local sCountermask    = "Null"
local sTurnPortrait   = "Root/Images/AdventureUI/TurnPortraits/Mei_BeeQueen"

--Delayed-Load Groupings
local sDialogueCostumeGrouping = "Mei Costume BeeQueen"
local sCombatCostumeGrouping   = sDialogueCostumeGrouping .. " Combat"
local saDialogueGroupingList   = gsaMeiDialogueList
local saCombatGroupingList     = gsaMeiCombatList

-- |[Stop Check]|
--If not in this form, just flag the variable.
local sCharacterForm = VM_GetVar(sFormVar, "S")
VM_SetVar(sCostumeVar, "S", sCostumeName)
if(sCharacterForm ~= "Bee") then
    return
end

-- |[ ======================================== Dialogue ======================================== ]|
--Standard.
fnSetMeiDialogue(sDialoguePath, true)

-- |[ ====================================== Field Sprite ====================================== ]|
fnSetMeiFieldSprite(sSpritePath, sSpecialPath, false)
fnSetMeiLuaSpriteLookup(sSpritePath)
fnSetMeiIdleAnimationAdvanced("Root/Images/Sprites/IdleAnim/Mei_BeeQueen|", 12, {0, 1, 4, 5, 4, 5, 4, 5, 2, 3, 4, 5, 4, 5, 6, 7, 4, 5, 4, 5, 1, 1, 0})

-- |[ ===================================== Combat and UI ====================================== ]|
--Basic.
fnSetMeiCombatData(sCombatName, sCombatPortrait, sCountermask, sTurnPortrait, sSpritePath, fIndexX, fIndexY)

--Alignments.
if(AdvCombat_GetProperty("Does Party Member Exist", sCombatName) == true) then
    AdvCombat_SetProperty("Push Party Member", sCombatName)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Basemenu,          -105, 406)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Base,        116,  87)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Ally,       -220, -70)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Inspector,   158,  64)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equipment,          -52,  50)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Skills,             -20,  89)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status,             -26,  45)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Vendor,            -134,  81)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Trainer,           -119, 138)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Victory,     603,  77)
    DL_PopActiveObject()
end

-- |[ ================================= Loading and Unloading ================================== ]|
--Order all not-in-use dialogue and combat images to unload their data. Then order all in-use 
-- images to load their data. Images that are in multiple sets will correctly load their data
-- if the load is called after the unload.
if(gbUnloadDialoguePortraitsWhenDialogueCloses == false) then
    fnUnloadGrouping(saDialogueGroupingList, sDialogueCostumeGrouping)
    fnLoadGrouping  (saDialogueGroupingList, sDialogueCostumeGrouping)
end

--Combat always loads.
fnUnloadGrouping(saCombatGroupingList, sCombatCostumeGrouping)
fnLoadGrouping  (saCombatGroupingList, sCombatCostumeGrouping)
