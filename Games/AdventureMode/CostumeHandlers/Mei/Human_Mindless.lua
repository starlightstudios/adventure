-- |[ ================================== Mei, Human, Mindless ================================== ]|
-- |[Setup]|
--Set all variables for this script. Special frames and alignments need to be entered below.
-- First, constants across this character.
local sActorName = "Mei"
local sCombatName = "Mei"
local sFieldName = "Mei"
local sFormVar = "Root/Variables/Global/Mei/sForm"

--Costume Variables
local sCostumeName = "Mindless"
local sCostumeVar = "Root/Variables/Costumes/Mei/sCostumeHuman"

--Actor Portrait
local sDialoguePath = "Root/Images/Portraits/MeiDialogue/Human"

--Sprite
local sSpritePath = "Root/Images/Sprites/Mei_Human_MC/"
local sSpecialPath = "Root/Images/Sprites/Special/MeiMC|"
local fIndexX = 0
local fIndexY = 0

--Combat
local sCombatPortrait = "Root/Images/Portraits/Combat/Mei_HumanMC"
local sCountermask    = "Root/Images/Portraits/Combat/Mei_Human_Countermask"
local sTurnPortrait   = "Root/Images/AdventureUI/TurnPortraits/Mei_Human"

--Delayed-Load Groupings
local sDialogueCostumeGrouping = "Mei Costume HumanMC"
local sCombatCostumeGrouping   = sDialogueCostumeGrouping .. " Combat"
local saDialogueGroupingList   = gsaMeiDialogueList
local saCombatGroupingList     = gsaMeiCombatList

function fnSetMeiDialogue(psDialoguePath, pbHasMCEmote)

    --Argument check.
    if(psDialoguePath == nil) then return end
    if(pbHasMCEmote   == nil) then pbHasMCEmote = false end

    --Verify dialogue actor exists.
    if(DialogueActor_Exists("Mei") == false) then return end

    --Push, set.
    DialogueActor_Push("Mei")
        DialogueActor_SetProperty("Add Emotion", "Neutral",  psDialoguePath .. "Neutral",  true)
        DialogueActor_SetProperty("Add Emotion", "Smirk",    psDialoguePath .. "Smirk",    true)
        DialogueActor_SetProperty("Add Emotion", "Happy",    psDialoguePath .. "Happy",    true)
        DialogueActor_SetProperty("Add Emotion", "Blush",    psDialoguePath .. "Blush",    true)
        DialogueActor_SetProperty("Add Emotion", "Sad",      psDialoguePath .. "Sad",      true)
        DialogueActor_SetProperty("Add Emotion", "Surprise", psDialoguePath .. "Surprise", true)
        DialogueActor_SetProperty("Add Emotion", "Offended", psDialoguePath .. "Offended", true)
        DialogueActor_SetProperty("Add Emotion", "Cry",      psDialoguePath .. "Cry",      true)
        DialogueActor_SetProperty("Add Emotion", "Laugh",    psDialoguePath .. "Laugh",    true)
        DialogueActor_SetProperty("Add Emotion", "Angry",    psDialoguePath .. "Angry",    true)
        
        --Not all forms have the MC emote.
        if(pbHasMCEmote == true) then
            DialogueActor_SetProperty("Add Emotion", "MC", psDialoguePath .. "MC", true)
            io.write("SET MC!\n")
            DL_ReportBitmap(psDialoguePath .. "MC")
        else
            DialogueActor_SetProperty("Add Emotion", "MC", psDialoguePath .. "Neutral", true)
        end
        
    DL_PopActiveObject()

end

-- |[Stop Check]|
--If not in this form, just flag the variable.
local sCharacterForm = VM_GetVar(sFormVar, "S")
VM_SetVar(sCostumeVar, "S", sCostumeName)
if(sCharacterForm ~= "Human") then
    return
end

-- |[ ======================================== Dialogue ======================================== ]|
--Standard.
fnSetMeiDialogue(sDialoguePath, true)

-- |[ ====================================== Field Sprite ====================================== ]|
fnSetMeiFieldSprite(sSpritePath, sSpecialPath, false)
fnSetMeiLuaSpriteLookup(sSpritePath)
fnSetMeiIdleAnimation("Null", 0, 0, -1)

-- |[ ===================================== Combat and UI ====================================== ]|
--Basic.
fnSetMeiCombatData(sCombatName, sCombatPortrait, sCountermask, sTurnPortrait, sSpritePath, fIndexX, fIndexY)

--Alignments.
if(AdvCombat_GetProperty("Does Party Member Exist", sCombatName) == true) then
    AdvCombat_SetProperty("Push Party Member", sCombatName)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Basemenu,          33, 435)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Base,      256, 112)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Ally,      -84,  42)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Inspector, 291,  91)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equipment,         82,  76)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Skills,           115,  21)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status,           116,  69)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Vendor,             0, 112)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Trainer,           38, 164)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Victory,   670,  91)
    DL_PopActiveObject()
end

-- |[ ================================= Loading and Unloading ================================== ]|
--Order all not-in-use dialogue and combat images to unload their data. Then order all in-use 
-- images to load their data. Images that are in multiple sets will correctly load their data
-- if the load is called after the unload.
if(gbUnloadDialoguePortraitsWhenDialogueCloses == false) then
    fnUnloadGrouping(saDialogueGroupingList, sDialogueCostumeGrouping)
    fnLoadGrouping  (saDialogueGroupingList, sDialogueCostumeGrouping)
end

--Combat always loads.
fnUnloadGrouping(saCombatGroupingList, sCombatCostumeGrouping)
fnLoadGrouping  (saCombatGroupingList, sCombatCostumeGrouping)
