-- |[ =================================== Sanya, Harpy, Rifle ================================== ]|
-- |[Setup]|
--Set all variables for this script. Special frames and alignments need to be entered below.
-- First, constants across this character.
local sActorName = "Sanya"
local sCombatName = "Sanya"
local sFieldName = "Sanya"
local sFormVar = "Root/Variables/Global/Sanya/sForm"

--Costume Variables
local sCostumeName = "Rifle"
local sCostumeVar = "Root/Variables/Costumes/Sanya/sCostumeHarpy"

--Actor Portrait
local sDialoguePath = "Root/Images/Portraits/SanyaDialogue/Harpy"

--Sprite
local sSpritePath = "Root/Images/Sprites/Sanya_Harpy_Rifle/"
local sSpecialPath = "Root/Images/Sprites/Special/Sanya_Harpy_Rifle|"
local fIndexX = 4
local fIndexY = 1

--Combat
local sCombatPortrait = "Root/Images/Portraits/Combat/Sanya_Harpy"
local sCountermask    = "Null"
local sTurnPortrait   = "Root/Images/AdventureUI/TurnPortraits/Sanya_Harpy"

--Delayed-Load Groupings
local sDialogueCostumeGrouping = "Sanya Costume Harpy"
local sCombatCostumeGrouping   = sDialogueCostumeGrouping .. " Combat"
local saDialogueGroupingList   = gsaSanyaDialogueList
local saCombatGroupingList     = gsaSanyaCombatList

-- |[Stop Check]|
--If not in this form, just flag the variable.
local sCharacterForm = VM_GetVar(sFormVar, "S")
VM_SetVar(sCostumeVar, "S", sCostumeName)
if(sCharacterForm ~= "Harpy") then
    return
end

-- |[ ======================================== Dialogue ======================================== ]|
--Standard.
fnSetSanyaDialogue(sDialoguePath, true)

-- |[ ====================================== Field Sprite ====================================== ]|
fnSetSanyaFieldSprite(sSpritePath, sSpecialPath, false)
fnSetSanyaLuaSpriteLookup(sSpritePath)
fnSetSanyaIdleAnimation("Null", 0, 0, -1)

-- |[ ===================================== Combat and UI ====================================== ]|
--Basic.
fnSetSanyaCombatData(sCombatName, sCombatPortrait, sCountermask, sTurnPortrait, sSpritePath, fIndexX, fIndexY)

--Alignments.
if(AdvCombat_GetProperty("Does Party Member Exist", sCombatName) == true) then
    AdvCombat_SetProperty("Push Party Member", sCombatName)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Basemenu,          23, 442)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Base,      240,  95)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Ally,     -102, -30)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Inspector, 284,  89)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equipment,         71,  93)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Skills,           110, 109)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status,           102,  86)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Vendor,            -7, 124)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Victory,   667,  92)
    DL_PopActiveObject()
end

-- |[ ================================= Loading and Unloading ================================== ]|
--Order all not-in-use dialogue and combat images to unload their data. Then order all in-use 
-- images to load their data. Images that are in multiple sets will correctly load their data
-- if the load is called after the unload.
if(gbUnloadDialoguePortraitsWhenDialogueCloses == false) then
    fnUnloadGrouping(saDialogueGroupingList, sDialogueCostumeGrouping)
    fnLoadGrouping  (saDialogueGroupingList, sDialogueCostumeGrouping)
end

--Combat always loads.
fnUnloadGrouping(saCombatGroupingList, sCombatCostumeGrouping)
fnLoadGrouping  (saCombatGroupingList, sCombatCostumeGrouping)
