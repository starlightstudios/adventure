-- |[ ================================== Sanya, Human, Normal ================================== ]|
-- |[Setup]|
--Set all variables for this script. Special frames and alignments need to be entered below.
-- First, constants across this character.
local sActorName = "Sanya"
local sCombatName = "Sanya"
local sFieldName = "Sanya"
local sFormVar = "Root/Variables/Global/Sanya/sForm"

--Costume Variables
local sCostumeName = "Normal"
local sCostumeVar = "Root/Variables/Costumes/Sanya/sCostumeSevavi"

--Actor Portrait
local sDialoguePath = "Root/Images/Portraits/SanyaDialogue/Sevavi"

--Sprite
local sSpritePath = "Root/Images/Sprites/Sanya_Sevavi/"
local sSpecialPath = "Root/Images/Sprites/Special/Sanya_Sevavi|"
local fIndexX = 1
local fIndexY = 1

--Combat
local sCombatPortrait = "Root/Images/Portraits/Combat/Sanya_SevaviNoRifle"
local sCountermask    = "Null"
local sTurnPortrait   = "Root/Images/AdventureUI/TurnPortraits/Sanya_Sevavi"

--Delayed-Load Groupings
local sDialogueCostumeGrouping = "Sanya Costume SevaviNoRifle"
local sCombatCostumeGrouping   = sDialogueCostumeGrouping .. " Combat"
local saDialogueGroupingList   = gsaSanyaDialogueList
local saCombatGroupingList     = gsaSanyaCombatList

-- |[Stop Check]|
--If not in this form, just flag the variable.
local sCharacterForm = VM_GetVar(sFormVar, "S")
VM_SetVar(sCostumeVar, "S", sCostumeName)
if(sCharacterForm ~= "Sevavi") then
    return
end

-- |[ ======================================== Dialogue ======================================== ]|
--Standard.
fnSetSanyaDialogue(sDialoguePath, true)

-- |[ ====================================== Field Sprite ====================================== ]|
fnSetSanyaFieldSprite(sSpritePath, sSpecialPath, false)
fnSetSanyaLuaSpriteLookup(sSpritePath)
fnSetSanyaIdleAnimation("Null", 0, 0, -1)

-- |[ ===================================== Combat and UI ====================================== ]|
--Basic.
fnSetSanyaCombatData(sCombatName, sCombatPortrait, sCountermask, sTurnPortrait, sSpritePath, fIndexX, fIndexY)

--Alignments.
if(AdvCombat_GetProperty("Does Party Member Exist", sCombatName) == true) then
    AdvCombat_SetProperty("Push Party Member", sCombatName)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Basemenu,          -7, 441)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Base,      218, 111)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Ally,     -121, -36)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Inspector, 270,  93)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equipment,         50,  91)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Skills,            86, 123)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status,            82,  87)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Trainer,          -16, 155)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Vendor,           -32, 122)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Victory,   667,  95)
    DL_PopActiveObject()
end

-- |[ ================================= Loading and Unloading ================================== ]|
--Order all not-in-use dialogue and combat images to unload their data. Then order all in-use 
-- images to load their data. Images that are in multiple sets will correctly load their data
-- if the load is called after the unload.
if(gbUnloadDialoguePortraitsWhenDialogueCloses == false) then
    fnUnloadGrouping(saDialogueGroupingList, sDialogueCostumeGrouping)
    fnLoadGrouping  (saDialogueGroupingList, sDialogueCostumeGrouping)
end

--Combat always loads.
fnUnloadGrouping(saCombatGroupingList, sCombatCostumeGrouping)
fnLoadGrouping  (saCombatGroupingList, sCombatCostumeGrouping)
