-- |[ ======================== Common Functions and Variables for Sanya ======================== ]|
--Some parts of Sanya's costume code is identical across all costumes. This handles those.

-- |[ =================================== fnSetSanyaDialogue =================================== ]|
--Sets Sanya's emotions using the given dialogue path.
function fnSetSanyaDialogue(psDialoguePath, pbUseFullEmotes)

    --Argument check.
    if(psDialoguePath == nil) then return end
    if(pbUseFullEmotes == nil) then pbUseFullEmotes = false end

    --Verify dialogue actor exists.
    if(DialogueActor_Exists("Sanya") == false) then return end

    --Push, set.
    DialogueActor_Push("Sanya")
    
        --Always have the neutral.
        DialogueActor_SetProperty("Add Emotion", "Neutral", psDialoguePath .. "Neutral",   true)
        
        --Full emote set:
        if(pbUseFullEmotes) then
            DialogueActor_SetProperty("Add Emotion", "Happy",     psDialoguePath .. "Happy",     true)
            DialogueActor_SetProperty("Add Emotion", "Blush",     psDialoguePath .. "Blush",     true)
            DialogueActor_SetProperty("Add Emotion", "Smirk",     psDialoguePath .. "Smirk",     true)
            DialogueActor_SetProperty("Add Emotion", "Sad",       psDialoguePath .. "Sad",       true)
            DialogueActor_SetProperty("Add Emotion", "Surprised", psDialoguePath .. "Surprised", true)
            DialogueActor_SetProperty("Add Emotion", "Offended",  psDialoguePath .. "Offended",  true)
            DialogueActor_SetProperty("Add Emotion", "Cry",       psDialoguePath .. "Cry",       true)
            DialogueActor_SetProperty("Add Emotion", "Laugh",     psDialoguePath .. "Laugh",     true)
            DialogueActor_SetProperty("Add Emotion", "Angry",     psDialoguePath .. "Angry",     true)
        
        --Other emotes become the neutral if the portraits aren't ready.
        else
            DialogueActor_SetProperty("Add Emotion", "Happy",     psDialoguePath .. "Neutral",     true)
            DialogueActor_SetProperty("Add Emotion", "Blush",     psDialoguePath .. "Neutral",     true)
            DialogueActor_SetProperty("Add Emotion", "Smirk",     psDialoguePath .. "Smirk",       true)
            DialogueActor_SetProperty("Add Emotion", "Sad",       psDialoguePath .. "Neutral",     true)
            DialogueActor_SetProperty("Add Emotion", "Surprised", psDialoguePath .. "Neutral",     true)
            DialogueActor_SetProperty("Add Emotion", "Offended",  psDialoguePath .. "Neutral",     true)
            DialogueActor_SetProperty("Add Emotion", "Cry",       psDialoguePath .. "Neutral",     true)
            DialogueActor_SetProperty("Add Emotion", "Laugh",     psDialoguePath .. "Neutral",     true)
            DialogueActor_SetProperty("Add Emotion", "Angry",     psDialoguePath .. "Neutral",     true)
        end
    DL_PopActiveObject()

end

-- |[ ================================= fnSetSanyaFieldSprite ================================== ]|
--If the field sprite for anya exists, updates its properties and special frames.
function fnSetSanyaFieldSprite(psSpritePath, psSpecialPath, pbIsAirborne)
    
    -- |[Arguments and Error Checks]|
    --Argument check.
    if(psSpritePath == nil) then return end
    if(psSpecialPath == nil) then return end
    if(pbIsAirborne == nil) then pbIsAirborne = false end
    
    --No field sprite, exit.
    if(EM_Exists("Sanya") == false) then return end
    
    -- |[Checks Passed. Push]|
    --Push.
    EM_PushEntity("Sanya")
    
        -- |[Basics]|
        --Base.
        fnSetCharacterGraphics(psSpritePath, true)
        
        -- |[Special Properties]|
        --Animation properties for ground forms:
        if(pbIsAirborne == false) then
            TA_SetProperty("Auto Animates",       false)
            TA_SetProperty("Y Oscillates",        false)
            TA_SetProperty("No Footstep Sound",   false)
            TA_SetProperty("No Automatic Shadow", false)
            TA_SetProperty("Jumping Sound", "World|Jump")
            TA_SetProperty("Landing Sound", "World|Land")
        
        --Airborne forms:
        else
            TA_SetProperty("Auto Animates",       true)
            TA_SetProperty("Y Oscillates",        true)
            TA_SetProperty("No Footstep Sound",   true)
            TA_SetProperty("No Automatic Shadow", false)
            TA_SetProperty("Jumping Sound", "Null")
            TA_SetProperty("Landing Sound", "Null")
        end
        
        -- |[Common Frames]|
        --Special frames.
        TA_SetProperty("Wipe Special Frames")
        TA_SetProperty("Add Special Frame", "Wounded", psSpecialPath .. "Wounded")
        TA_SetProperty("Add Special Frame", "Crouch",  psSpecialPath .. "Crouch")
        
        --Water handler.
        TA_SetProperty("Add Special Frame", "WaterOver", "Root/Images/Sprites/Shadows/Depth")
        
        --Special Cases:
        if(psSpritePath == "Root/Images/Sprites/Sanya_Human/") then
            TA_SetProperty("Add Special Frame", "Falling", psSpecialPath .. "Fall")
            TA_SetProperty("Add Special Frame", "ArmUp",   psSpecialPath .. "ArmUp")
            TA_SetProperty("Add Special Frame", "Catch0",  psSpecialPath .. "Catch0")
            TA_SetProperty("Add Special Frame", "Catch1",  psSpecialPath .. "Catch1")
            TA_SetProperty("Add Special Frame", "Catch2",  psSpecialPath .. "Catch2")
            TA_SetProperty("Add Special Frame", "Catch3",  psSpecialPath .. "Catch3")
        elseif(psSpritePath == "Root/Images/Sprites/Sanya_Human_Rifle/") then
            TA_SetProperty("Add Special Frame", "Falling", psSpecialPath .. "Fall")
            TA_SetProperty("Add Special Frame", "ArmUp",   psSpecialPath .. "ArmUp")
            TA_SetProperty("Add Special Frame", "Catch0",  psSpecialPath .. "Catch0")
            TA_SetProperty("Add Special Frame", "Catch1",  psSpecialPath .. "Catch1")
            TA_SetProperty("Add Special Frame", "Catch2",  psSpecialPath .. "Catch2")
            TA_SetProperty("Add Special Frame", "Catch3",  psSpecialPath .. "Catch3")
        end

        -- |[Kicking Frames]|
        --Used for puzzles, present on every sprite sheet.
        local saKickList  = {"KickS0", "KickS1", "KickN0", "KickN1", "KickE0", "KickE1", "KickW0", "KickW1"}
        for i = 1, #saKickList, 1 do
            TA_SetProperty("Add Special Frame", saKickList[i], psSpecialPath..saKickList[i])
        end
        
        -- |[Shooting Frames]|
        --Used for field abilities. Only present on certain sheets.
        local bHasShootingFrames = false
        local saHasShootingFramesList = {"Root/Images/Sprites/Sanya_Bunny_Rifle/", "Root/Images/Sprites/Sanya_Harpy_Rifle/", "Root/Images/Sprites/Sanya_Human_Rifle/", "Root/Images/Sprites/Sanya_Sevavi_Rifle/", 
                                         "Root/Images/Sprites/Sanya_Werebat_Rifle/"}
        for i = 1, #saHasShootingFramesList, 1 do
            if(saHasShootingFramesList[i] == psSpritePath) then
                bHasShootingFrames = true
                break
            end
        end
        
        --List of shooting frames.
        local sPrefix = "Shoot"
        local saDirections = {"N", "S", "E", "W", "NW", "NE", "SE", "SW"}
        for i = 1, #saDirections, 1 do
            for p = 0, 3, 1 do
                TA_SetProperty("Add Special Frame", sPrefix .. saDirections[i] .. p, psSpecialPath .. sPrefix .. saDirections[i] .. p)
            end
        end
        
        -- |[Debug]|
        --TA_GetProperty("Print Special Frames")
    
    -- |[Clean Up]|
    DL_PopActiveObject()

end

-- |[ ================================ fnSetSanyaIdleAnimation ================================= ]|
--Sets the idle animation for anya in the given form. Pass "Null" to disable. This one uses a simple
-- endless loop case, or a play-once-then-done case.
function fnSetSanyaIdleAnimation(psDLPathPattern, piFramesTotal, piTPF, piLoopFrame)
    
    --Arg check.
    if(psDLPathPattern == nil) then return end
    if(piFramesTotal   == nil) then return end
    if(piTPF           == nil) then return end
    if(piLoopFrame     == nil) then return end
    
    --Do nothing if the actor does not exist.
    if(EM_Exists("Sanya") == false) then return end
    
    --Disable if "Null".
    if(psDLPathPattern == "Null") then
        EM_PushEntity("Sanya")
            TA_SetProperty("Idle Animation Flag", false)
        DL_PopActiveObject()
        return
    end
    
    --Legality check.
    if(piFramesTotal < 1) then return end
    
    --Otherwise, set the idle animations.
    EM_PushEntity("Sanya")
        TA_SetProperty("Idle Animation Flag", true)
        TA_SetProperty("Allocate Idle Frames", piFramesTotal)
        for i = 0, piFramesTotal-1, 1 do
            TA_SetProperty("Set Idle Frame", i, psDLPathPattern .. i)
        end
        TA_SetProperty("Set Idle TPF", piTPF)
        TA_SetProperty("Set Idle Loop Frame", piLoopFrame)
    DL_PopActiveObject()
end

--Sets the idle animation for Sanya in a given form. This one uses a list of the frames, which allows
-- for complicated, though finite, looping. The animation is one-then-done but can re-use frames.
function fnSetSanyaIdleAnimationAdvanced(psDLPathPattern, piTPF, piaFrameList)
    
    --Arg check.
    if(psDLPathPattern == nil) then return end
    if(piTPF           == nil) then return end
    if(piaFrameList    == nil) then return end
    
    --Do nothing if the actor does not exist.
    if(EM_Exists("Sanya") == false) then return end
    
    --Disable if "Null".
    if(psDLPathPattern == "Null") then
        EM_PushEntity("Sanya")
            TA_SetProperty("Idle Animation Flag", false)
        DL_PopActiveObject()
        return
    end
    
    --Legality check.
    if(#piaFrameList < 1) then return end
    
    --Otherwise, set the idle animations.
    EM_PushEntity("Sanya")
        TA_SetProperty("Idle Animation Flag", true)
        TA_SetProperty("Allocate Idle Frames", #piaFrameList)
        for i = 1, #piaFrameList, 1 do
            TA_SetProperty("Set Idle Frame", i-1, psDLPathPattern .. piaFrameList[i])
        end
        TA_SetProperty("Set Idle TPF", piTPF)
        TA_SetProperty("Set Idle Loop Frame", -1)
    DL_PopActiveObject()
    
end

-- |[ =============================== fnSetSanyaLuaSpriteLookup ================================ ]|
--Updates which sprites the lua code uses to refer to this character.
function fnSetSanyaLuaSpriteLookup(psSpritePath)
    
    --Argument check.
    if(psSpritePath == nil) then return end
    
    --Iterate.
    for i = 1, giCharSpritesTotal, 1 do
        
        --Match.
        if(gsCharSprites[i][1] == "Sanya") then
            gsCharSprites[i][2] = psSpritePath .. "SW|0"
            break
        end
    end
    
end

-- |[ ================================== fnSetSanyaCombatData ================================== ]|
--Sets combat data for the given party member. Does not set UI rendering positions.
function fnSetSanyaCombatData(psCombatName, psPortrait, psCountermask, psTurnPortrait, psSpritePath, pfIndexX, pfIndexY)
    
    --Argument check.
    if(psCombatName   == nil) then return end
    if(psPortrait     == nil) then return end
    if(psCountermask  == nil) then return end
    if(psTurnPortrait == nil) then return end
    if(psSpritePath   == nil) then return end
    if(pfIndexX       == nil) then return end
    if(pfIndexY       == nil) then return end
    
    --Existence check.
    if(AdvCombat_GetProperty("Does Party Member Exist", psCombatName) == false) then return end
    
    --Push.
    AdvCombat_SetProperty("Push Party Member", psCombatName)
    
        --Portraits
        AdvCombatEntity_SetProperty("Combat Portrait",    psPortrait)
        AdvCombatEntity_SetProperty("Combat Countermask", psCountermask)
        AdvCombatEntity_SetProperty("Turn Icon",          psTurnPortrait)
    
        --Vendor Sprites
        AdvCombatEntity_SetProperty("Vendor Image", 0, psSpritePath .. "SW|0")
        AdvCombatEntity_SetProperty("Vendor Image", 1, psSpritePath .. "SW|1")
        AdvCombatEntity_SetProperty("Vendor Image", 2, psSpritePath .. "SW|2")
        AdvCombatEntity_SetProperty("Vendor Image", 3, psSpritePath .. "SW|3")
    
        --Face Table, used on the Equipment UI to show who has the item equipped.
        local fLft = (pfIndexX+0) * gci_FaceTable_Size
        local fTop = (pfIndexY+0) * gci_FaceTable_Size
        local fRgt = (pfIndexX+1) * gci_FaceTable_Size
        local fBot = (pfIndexY+1) * gci_FaceTable_Size
        AdvCombatEntity_SetProperty("Face Table Data", fLft, fTop, fRgt, fBot, "Root/Images/AdventureUI/ItemIcons/CharacterFaces")

    --Clean.
    DL_PopActiveObject()
    
end
