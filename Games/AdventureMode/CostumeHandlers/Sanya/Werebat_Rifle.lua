-- |[ ================================= Sanya, Werebat, Rifle ================================== ]|
-- |[Setup]|
--Set all variables for this script. Special frames and alignments need to be entered below.
-- First, constants across this character.
local sActorName = "Sanya"
local sCombatName = "Sanya"
local sFieldName = "Sanya"
local sFormVar = "Root/Variables/Global/Sanya/sForm"

--Costume Variables
local sCostumeName = "Rifle"
local sCostumeVar = "Root/Variables/Costumes/Sanya/sCostumeWerebat"

--Actor Portrait
local sDialoguePath = "Root/Images/Portraits/SanyaDialogue/Werebat"

--Sprite
local sSpritePath = "Root/Images/Sprites/Sanya_Werebat_Rifle/"
local sSpecialPath = "Root/Images/Sprites/Special/Sanya_Werebat_Rifle|"
local fIndexX = 2
local fIndexY = 1

--Combat
local sCombatPortrait = "Root/Images/Portraits/Combat/Sanya_Werebat"
local sCountermask    = "Null"
local sTurnPortrait   = "Root/Images/AdventureUI/TurnPortraits/Sanya_Werebat"

--Delayed-Load Groupings
local sDialogueCostumeGrouping = "Sanya Costume Werebat"
local sCombatCostumeGrouping   = sDialogueCostumeGrouping .. " Combat"
local saDialogueGroupingList   = gsaSanyaDialogueList
local saCombatGroupingList     = gsaSanyaCombatList

-- |[Stop Check]|
--If not in this form, just flag the variable.
local sCharacterForm = VM_GetVar(sFormVar, "S")
VM_SetVar(sCostumeVar, "S", sCostumeName)
if(sCharacterForm ~= "Werebat") then
    return
end

-- |[ ======================================== Dialogue ======================================== ]|
--Standard.
fnSetSanyaDialogue(sDialoguePath, true)

-- |[ ====================================== Field Sprite ====================================== ]|
fnSetSanyaFieldSprite(sSpritePath, sSpecialPath, false)
fnSetSanyaLuaSpriteLookup(sSpritePath)
fnSetSanyaIdleAnimation("Null", 0, 0, -1)

-- |[ ===================================== Combat and UI ====================================== ]|
--Basic.
fnSetSanyaCombatData(sCombatName, sCombatPortrait, sCountermask, sTurnPortrait, sSpritePath, fIndexX, fIndexY)

--Alignments.
if(AdvCombat_GetProperty("Does Party Member Exist", sCombatName) == true) then
    AdvCombat_SetProperty("Push Party Member", sCombatName)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Basemenu,           8, 375)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Base,      221,  47)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Ally,     -112,-102)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Inspector, 254,  35)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equipment,         56,  15)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Skills,            86,  48)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status,            84,  10)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Vendor,           -21,  52)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Victory,   656,  61)
    DL_PopActiveObject()
end

-- |[ ================================= Loading and Unloading ================================== ]|
--Order all not-in-use dialogue and combat images to unload their data. Then order all in-use 
-- images to load their data. Images that are in multiple sets will correctly load their data
-- if the load is called after the unload.
if(gbUnloadDialoguePortraitsWhenDialogueCloses == false) then
    fnUnloadGrouping(saDialogueGroupingList, sDialogueCostumeGrouping)
    fnLoadGrouping  (saDialogueGroupingList, sDialogueCostumeGrouping)
end

--Combat always loads.
fnUnloadGrouping(saCombatGroupingList, sCombatCostumeGrouping)
fnLoadGrouping  (saCombatGroupingList, sCombatCostumeGrouping)
