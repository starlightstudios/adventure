-- |[ ================================= Sanya, Human, WerebatA ================================= ]|
-- |[Setup]|
--Set all variables for this script. Special frames and alignments need to be entered below.
-- First, constants across this character.
local sActorName = "Sanya"
local sCombatName = "Sanya"
local sFieldName = "Sanya"
local sFormVar = "Root/Variables/Global/Sanya/sForm"

--Costume Variables
local sCostumeName = "WerebatB"
local sCostumeVar = "Root/Variables/Costumes/Sanya/sCostumeHuman"

--Actor Portrait
local sDialoguePath = "Root/Images/Portraits/SanyaDialogue/HumanWerebatB"

--Sprite
local sSpritePath = "Root/Images/Sprites/Sanya_Human_Rifle/"
local sSpecialPath = "Root/Images/Sprites/Special/Sanya_Human_Rifle|"
local fIndexX = 0
local fIndexY = 1

--Combat
local sCombatPortrait = "Root/Images/Portraits/Combat/Sanya_Human"
local sCountermask    = "Null"
local sTurnPortrait   = "Root/Images/AdventureUI/TurnPortraits/Sanya_Human"

--Delayed-Load Groupings
local sDialogueCostumeGrouping = "Sanya Costume Human"
local sCombatCostumeGrouping   = sDialogueCostumeGrouping .. " Combat"
local saDialogueGroupingList   = gsaSanyaDialogueList
local saCombatGroupingList     = gsaSanyaCombatList

-- |[Stop Check]|
--If not in this form, just flag the variable.
local sCharacterForm = VM_GetVar(sFormVar, "S")
VM_SetVar(sCostumeVar, "S", sCostumeName)
if(sCharacterForm ~= "Human") then
    return
end

-- |[ ======================================== Dialogue ======================================== ]|
--Standard.
fnSetSanyaDialogue(sDialoguePath, true)

-- |[ ====================================== Field Sprite ====================================== ]|
fnSetSanyaFieldSprite(sSpritePath, sSpecialPath, false)
fnSetSanyaLuaSpriteLookup(sSpritePath)
fnSetSanyaIdleAnimation("Null", 0, 0, -1)

-- |[ ===================================== Combat and UI ====================================== ]|
--Basic.
fnSetSanyaCombatData(sCombatName, sCombatPortrait, sCountermask, sTurnPortrait, sSpritePath, fIndexX, fIndexY)

--Alignments.
if(AdvCombat_GetProperty("Does Party Member Exist", sCombatName) == true) then
    AdvCombat_SetProperty("Push Party Member", sCombatName)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Basemenu,          -7, 444)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Base,      213, 130)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Ally,     -163, -33)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Inspector, 259, 104)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equipment,         41,  87)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Skills,            87, 127)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status,            78,  78)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Vendor,           -33, 123)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Victory,   652,  97)
    DL_PopActiveObject()
end

-- |[ ================================= Loading and Unloading ================================== ]|
--Order all not-in-use dialogue and combat images to unload their data. Then order all in-use 
-- images to load their data. Images that are in multiple sets will correctly load their data
-- if the load is called after the unload.
if(gbUnloadDialoguePortraitsWhenDialogueCloses == false) then
    fnUnloadGrouping(saDialogueGroupingList, sDialogueCostumeGrouping)
    fnLoadGrouping  (saDialogueGroupingList, sDialogueCostumeGrouping)
end

--Combat always loads.
fnUnloadGrouping(saCombatGroupingList, sCombatCostumeGrouping)
fnLoadGrouping  (saCombatGroupingList, sCombatCostumeGrouping)
