-- |[ =============================== SX-399, Young Droid, Normal ============================== ]|
-- |[Setup]|
--Set all variables for this script. Special frames and alignments need to be entered below.
-- First, constants across this character.
local sActorName = "SX-399"
local sCombatName = "SX-399"
local sFieldName = "SX-399"
local sFormVar = "Root/Variables/Global/SX-399/sCurrentJob"

--Costume Variables
local sCostumeName = "Young Droid"
local sCostumeVar = "Root/Variables/Costumes/SX-399/sCostumeShocktrooper"

--Actor Portrait
local sDialoguePath = "Root/Images/Portraits/SX-399Shocktrooper/"

--Sprite
local sSpritePath  = "Root/Images/Sprites/SX-399/"
local sSpecialPath = "Root/Images/Sprites/Special/SX-399|"
local fIndexX = 2
local fIndexY = 6

--Combat
local sCombatPortrait = "Root/Images/Portraits/Combat/SX-399"
local sCountermask    = "Null"
local sTurnPortrait   = "Root/Images/AdventureUI/TurnPortraits/SX-399"

--Delayed-Load Groupings
local sDialogueCostumeGrouping = "SX-399 Costume Young"
local sCombatCostumeGrouping   = sDialogueCostumeGrouping .. " Combat"
local saDialogueGroupingList   = gsaSX399DialogueList
local saCombatGroupingList     = gsaSX399CombatList

-- |[Stop Check]|
--If not in this form, just flag the variable.
local sCharacterForm = VM_GetVar(sFormVar, "S")
VM_SetVar(sCostumeVar, "S", sCostumeName)
if(sCharacterForm ~= "Shocktrooper") then
    return
end

-- |[ ======================================== Dialogue ======================================== ]|
--This form uses the same emote for all cases.
if(DialogueActor_Exists("SX-399") == true) then
    DialogueActor_Push("SX-399")
        DialogueActor_SetProperty("Add Emotion", "Neutral", "Root/Images/Portraits/SX-399YoungDroid/Neutral", true)
        DialogueActor_SetProperty("Add Emotion", "Happy",   "Root/Images/Portraits/SX-399YoungDroid/Neutral", true)
        DialogueActor_SetProperty("Add Emotion", "Blush",   "Root/Images/Portraits/SX-399YoungDroid/Neutral", true)
        DialogueActor_SetProperty("Add Emotion", "Smirk",   "Root/Images/Portraits/SX-399YoungDroid/Neutral", true)
        DialogueActor_SetProperty("Add Emotion", "Sad",     "Root/Images/Portraits/SX-399YoungDroid/Neutral", true)
        DialogueActor_SetProperty("Add Emotion", "Flirt",   "Root/Images/Portraits/SX-399YoungDroid/Neutral", true)
        DialogueActor_SetProperty("Add Emotion", "Angry",   "Root/Images/Portraits/SX-399YoungDroid/Neutral", true)
        DialogueActor_SetProperty("Add Emotion", "Flirt2",  "Root/Images/Portraits/SX-399YoungDroid/Neutral", true)
    DL_PopActiveObject()
end

-- |[ ====================================== Field Sprite ====================================== ]|
fnSetSX399FieldSprite(sSpritePath, sSpecialPath)
fnSetSX399LuaSpriteLookup(sSpritePath)

-- |[ ===================================== Combat and UI ====================================== ]|
--Basic.
fnSetSX399CombatData(sCombatName, sCombatPortrait, sCountermask, sTurnPortrait, sSpritePath, fIndexX, fIndexY)

--Alignments.
if(AdvCombat_GetProperty("Does Party Member Exist", sCombatName) == true) then
    AdvCombat_SetProperty("Push Party Member", sCombatName)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Basemenu,         -29, 377)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Base,      181,  54)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Ally,     -149,-104)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Inspector, 226,  31)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equipment,         16,  14)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Skills,            54,  52)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status,            52,   5)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Vendor,           -52,  50)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Victory,   637,  59)
    DL_PopActiveObject()
end

-- |[ ================================= Loading and Unloading ================================== ]|
--Order all not-in-use dialogue and combat images to unload their data. Then order all in-use 
-- images to load their data. Images that are in multiple sets will correctly load their data
-- if the load is called after the unload.
--Dialogue is ordered to load if this flag is false. Otherwise, it loads on demand.
if(gbUnloadDialoguePortraitsWhenDialogueCloses == false) then
    fnUnloadGrouping(saDialogueGroupingList, sDialogueCostumeGrouping)
    fnLoadGrouping  (saDialogueGroupingList, sDialogueCostumeGrouping)
end

--Combat always loads.
fnUnloadGrouping(saCombatGroupingList, sCombatCostumeGrouping)
fnLoadGrouping  (saCombatGroupingList, sCombatCostumeGrouping)
