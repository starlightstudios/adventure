-- |[ ======================== Common Functions and Variables for SX-399 ======================= ]|
--Some parts of SX-399's costume code is identical across all costumes. This handles those.

-- |[ ================================== fnSetTiffanyDialogue ================================== ]|
--Sets SX-399's emotions using the given dialogue path.
function fnSetSX399Dialogue(psDialoguePath)

    --Argument check.
    if(psDialoguePath == nil) then return end

    --Verify dialogue actor exists.
    if(DialogueActor_Exists("SX-399") == false) then return end

    --Set.
    DialogueActor_Push("SX-399")
        DialogueActor_SetProperty("Add Emotion", "Neutral", psDialoguePath .. "Neutral", true)
        DialogueActor_SetProperty("Add Emotion", "Happy",   psDialoguePath .. "Happy",   true)
        DialogueActor_SetProperty("Add Emotion", "Blush",   psDialoguePath .. "Blush",   true)
        DialogueActor_SetProperty("Add Emotion", "Smirk",   psDialoguePath .. "Smirk",   true)
        DialogueActor_SetProperty("Add Emotion", "Sad",     psDialoguePath .. "Sad",     true)
        DialogueActor_SetProperty("Add Emotion", "Flirt",   psDialoguePath .. "Flirt",   true)
        DialogueActor_SetProperty("Add Emotion", "Angry",   psDialoguePath .. "Angry",   true)
        DialogueActor_SetProperty("Add Emotion", "Flirt2",  psDialoguePath .. "Flirt2",  true)
    DL_PopActiveObject()

end

-- |[ ================================ fnSetTiffanyFieldSprite ================================= ]|
--If the field sprite for SX-399 exists, updates its properties and special frames.
function fnSetSX399FieldSprite(psSpritePath, psSpecialPath)
    
    --Argument check.
    if(psSpritePath == nil) then return end
    if(psSpecialPath == nil) then return end
    
    --No field sprite, exit.
    if(EM_Exists("SX-399") == false) then return end
    
    --Push.
	EM_PushEntity("SX-399")
    
        --Base.
        fnSetCharacterGraphics(psSpritePath, true)
        
        --Animation properties.
        TA_SetProperty("Auto Animates",       false)
        TA_SetProperty("Y Oscillates",        false)
        TA_SetProperty("No Footstep Sound",   false)
        TA_SetProperty("No Automatic Shadow", false)
        
        --Special frames.
        TA_SetProperty("Wipe Special Frames")
        TA_SetProperty("Add Special Frame", "Laugh0",  psSpecialPath .. "Laugh0")
        TA_SetProperty("Add Special Frame", "Laugh1",  psSpecialPath .. "Laugh1")
        TA_SetProperty("Add Special Frame", "Crouch",  psSpecialPath .. "Crouch")
        TA_SetProperty("Add Special Frame", "Wounded", psSpecialPath .. "Wounded")
    
    --Clean.
    DL_PopActiveObject()

end

-- |[ ============================== fnSetTiffanyLuaSpriteLookup =============================== ]|
--Updates which sprites the lua code uses to refer to this character.
function fnSetSX399LuaSpriteLookup(psSpritePath)
    
    --Argument check.
    if(psSpritePath == nil) then return end
    
    --Iterate.
    for i = 1, giCharSpritesTotal, 1 do
        
        --Match.
        if(gsCharSprites[i][1] == "SX-399") then
            gsCharSprites[i][2] = psSpritePath .. "SW|0"
            break
        end
    end
    
end

-- |[ ================================= fnSetTiffanyCombatData ================================= ]|
--Sets combat data for the given party member. Does not set UI rendering positions.
function fnSetSX399CombatData(psCombatName, psPortrait, psCountermask, psTurnPortrait, psSpritePath, pfIndexX, pfIndexY)
    
    --Argument check.
    if(psCombatName   == nil) then return end
    if(psPortrait     == nil) then return end
    if(psCountermask  == nil) then return end
    if(psTurnPortrait == nil) then return end
    if(psSpritePath   == nil) then return end
    if(pfIndexX       == nil) then return end
    if(pfIndexY       == nil) then return end
    
    --Existence check.
    if(AdvCombat_GetProperty("Does Party Member Exist", psCombatName) == false) then return end
    
    --Push.
    AdvCombat_SetProperty("Push Party Member", psCombatName)

        --Portraits
        AdvCombatEntity_SetProperty("Combat Portrait",    psPortrait)
        AdvCombatEntity_SetProperty("Combat Countermask", psCountermask)
        AdvCombatEntity_SetProperty("Turn Icon",          psTurnPortrait)
    
        --Vendor Sprites
        AdvCombatEntity_SetProperty("Vendor Image", 0, psSpritePath .. "SW|0")
        AdvCombatEntity_SetProperty("Vendor Image", 1, psSpritePath .. "SW|1")
        AdvCombatEntity_SetProperty("Vendor Image", 2, psSpritePath .. "SW|2")
        AdvCombatEntity_SetProperty("Vendor Image", 3, psSpritePath .. "SW|3")
    
        --Face Table, used on the Equipment UI to show who has the item equipped.
        local fLft = (pfIndexX+0) * gci_FaceTable_Size
        local fTop = (pfIndexY+0) * gci_FaceTable_Size
        local fRgt = (pfIndexX+1) * gci_FaceTable_Size
        local fBot = (pfIndexY+1) * gci_FaceTable_Size
        AdvCombatEntity_SetProperty("Face Table Data", fLft, fTop, fRgt, fBot, "Root/Images/AdventureUI/ItemIcons/CharacterFaces")

    --Clean.
    DL_PopActiveObject()
    
end
