-- |[ ============================== SX-399, Shocktrooper, Normal ============================== ]|
-- |[Setup]|
--Set all variables for this script. Special frames and alignments need to be entered below.
-- First, constants across this character.
local sActorName = "SX-399"
local sCombatName = "SX-399"
local sFieldName = "SX-399"
local sFormVar = "Root/Variables/Global/SX-399/sCurrentJob"

--Costume Variables
local sCostumeName = "Normal"
local sCostumeVar = "Root/Variables/Costumes/SX-399/sCostumeShocktrooper"

--Actor Portrait
local sDialoguePath = "Root/Images/Portraits/SX-399Shocktrooper/"

--Sprite
local sSpritePath  = "Root/Images/Sprites/SX-399Lord/"
local sSpecialPath = "Root/Images/Sprites/Special/SX-399Lord|"
local fIndexX = 2
local fIndexY = 6

--Combat
local sCombatPortrait = "Root/Images/Portraits/Combat/SX-399"
local sCountermask    = "Null"
local sTurnPortrait   = "Root/Images/AdventureUI/TurnPortraits/SX-399"

--Delayed-Load Groupings
local sDialogueCostumeGrouping = "SX-399 Costume Normal"
local sCombatCostumeGrouping   = sDialogueCostumeGrouping .. " Combat"
local saDialogueGroupingList   = gsaSX399DialogueList
local saCombatGroupingList     = gsaSX399CombatList

-- |[Stop Check]|
--If not in this form, just flag the variable.
local sCharacterForm = VM_GetVar(sFormVar, "S")
VM_SetVar(sCostumeVar, "S", sCostumeName)
if(sCharacterForm ~= "Shocktrooper") then
    return
end

-- |[ ======================================== Dialogue ======================================== ]|
--Standard.
fnSetSX399Dialogue(sDialoguePath)

-- |[ ====================================== Field Sprite ====================================== ]|
fnSetSX399FieldSprite(sSpritePath, sSpecialPath)
fnSetSX399LuaSpriteLookup(sSpritePath)

-- |[Idle Animation]|
SpriteHelper:fnSetActiveSprite(sFieldName)
SpriteHelper:fnSetIdleAnimationAdvanced("Root/Images/Sprites/IdleAnim/SX-399Lord|", 6, {0, 1, 2, 3, 4, 5, 6, 3, 4, 5, 6, 3, 4, 5, 6, 3, 4, 5, 6, 3, 4, 5, 6, 3, 4, 5, 6, 7, 8, 9, 10})
SpriteHelper:fnClear()

-- |[ ===================================== Combat and UI ====================================== ]|
--Basic.
fnSetSX399CombatData(sCombatName, sCombatPortrait, sCountermask, sTurnPortrait, sSpritePath, fIndexX, fIndexY)

--Alignments.
if(AdvCombat_GetProperty("Does Party Member Exist", sCombatName) == true) then
    AdvCombat_SetProperty("Push Party Member", sCombatName)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Basemenu,         -29, 377)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Base,      181,  54)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Ally,     -149,-104)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Inspector, 226,  31)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equipment,         16,  14)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Skills,            54,  52)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status,            52,   5)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Vendor,           -52,  50)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Trainer,          -35, 132)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Victory,   637,  59)
    DL_PopActiveObject()
end

-- |[ ================================= Loading and Unloading ================================== ]|
--Order all not-in-use dialogue and combat images to unload their data. Then order all in-use 
-- images to load their data. Images that are in multiple sets will correctly load their data
-- if the load is called after the unload.
--Dialogue is ordered to load if this flag is false. Otherwise, it loads on demand.
if(gbUnloadDialoguePortraitsWhenDialogueCloses == false) then
    fnUnloadGrouping(saDialogueGroupingList, sDialogueCostumeGrouping)
    fnLoadGrouping  (saDialogueGroupingList, sDialogueCostumeGrouping)
end

--Combat always loads.
fnUnloadGrouping(saCombatGroupingList, sCombatCostumeGrouping)
fnLoadGrouping  (saCombatGroupingList, sCombatCostumeGrouping)
