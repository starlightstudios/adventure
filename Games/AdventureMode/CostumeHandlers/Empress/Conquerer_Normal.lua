-- |[ =============================== Empress, Conquerer, Normal =============================== ]|
-- |[Setup]|
--Set all variables for this script. Special frames and alignments need to be entered below.
-- First, constants across this character.
local sActorName = "Empress"
local sCombatName = "Empress"
local sFieldName = "Empress"
local sFormVar = "Root/Variables/Global/Empress/sCurrentJob"

--Costume Variables
local sCostumeName = "Normal"
local sCostumeVar = "Root/Variables/Costumes/Empress/sCostumeConquerer"

--Actor Portrait
local sDialoguePath = "Root/Images/Portraits/EmpressConquererDialogue/"

--Sprite
local sSpritePath = "Root/Images/Sprites/Empress_Conquerer/"
local sSpecialPath = "Root/Images/Sprites/Special/Empress_Conquerer|"
local fIndexX = 6
local fIndexY = 6

--Combat
local sCombatPortrait = "Root/Images/Portraits/Combat/Empress_Conquerer"
local sCountermask    = "Null"
local sTurnPortrait   = "Root/Images/AdventureUI/TurnPortraits/Empress_Conquerer"

--Delayed-Load Groupings
local sDialogueCostumeGrouping = "Empress Costume Conquerer"
local sCombatCostumeGrouping   = sDialogueCostumeGrouping .. " Combat"
local saDialogueGroupingList   = gsaEmpressDialogueList
local saCombatGroupingList     = gsaEmpressCombatList

--Costume Function Grouping
local zaCostumeFunctions = gzaEmpressCostumeFunctions

-- |[Stop Check]|
--If not in this form, just flag the variable.
local sCharacterForm = VM_GetVar(sFormVar, "S")
VM_SetVar(sCostumeVar, "S", sCostumeName)
if(sCharacterForm ~= "Conquerer") then
    return
end

-- |[ ======================================== Dialogue ======================================== ]|
--Standard.
zaCostumeFunctions.fnSetDialogue(sDialoguePath)

-- |[ ====================================== Field Sprite ====================================== ]|
zaCostumeFunctions.fnSetFieldSprite(sSpritePath, sSpecialPath, false)
zaCostumeFunctions.fnSetLuaSpriteLookup(sSpritePath)
zaCostumeFunctions.fnSetIdleAnimation("Null", 0, 0, -1)

-- |[ ===================================== Combat and UI ====================================== ]|
--Basic.
zaCostumeFunctions.fnSetCombatData(sCombatName, sCombatPortrait, sCountermask, sTurnPortrait, sSpritePath, fIndexX, fIndexY)

--Alignments.
if(AdvCombat_GetProperty("Does Party Member Exist", sCombatName) == true) then
    AdvCombat_SetProperty("Push Party Member", sCombatName)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Basemenu,         -49, 427)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Base,      160,  60)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Ally,     -153, -32)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Inspector, 177,  61)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equipment,         -3,  51)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Skills,            29,  89)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status,            23,  43)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Vendor,          -105,  79)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Victory,   627,  94)
    DL_PopActiveObject()
end

-- |[ ================================= Loading and Unloading ================================== ]|
--Order all not-in-use dialogue and combat images to unload their data. Then order all in-use 
-- images to load their data. Images that are in multiple sets will correctly load their data
-- if the load is called after the unload.
if(gbUnloadDialoguePortraitsWhenDialogueCloses == false) then
    fnUnloadGrouping(saDialogueGroupingList, sDialogueCostumeGrouping)
    fnLoadGrouping  (saDialogueGroupingList, sDialogueCostumeGrouping)
end

--Combat always loads.
fnUnloadGrouping(saCombatGroupingList, sCombatCostumeGrouping)
fnLoadGrouping  (saCombatGroupingList, sCombatCostumeGrouping)
