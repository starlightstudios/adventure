-- |[ ==================================== Form Autoresolve ==================================== ]|
--Used to help standardize form and costume handlers, such that the form handler just calls the costume handler
-- and we don't need to maintain multiple areas of code. This script, given a single string, calls the appropriate handler.
--With this script, switching forms automatically dresses in the appropriate costume.
--The string is always "Character_Form". Ex: "Christine_Human", "Mei_Alraune"
local sBasePath = fnResolvePath()
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sHandlerName = LM_GetScriptArgument(0)

--Diagnostics.
local bDiagnostics = Debug_GetFlag("Costume: All") or Debug_GetFlag("Costume: Form Resolve")
Debug_PushPrint(bDiagnostics, "[ === Costume Autoresolve ===]\n")
Debug_Print("Provided handler: " .. sHandlerName .. "\n")

-- |[ ===================================== Path Groupings ===================================== ]|
--Only build this the first time.
if(gzaCostumeGroups == nil) then

    -- |[Setup]|
    local iLastCreatedGroup = 0
    local iLastCreatedPath = 0
    gzaCostumeGroups = {}

    -- |[Grouping Creator Function]|
    function fnAddCostumeGroup(sGroupName, sFormName, sCharFormPath, sCharCostumePath)

        --Add space.
        iLastCreatedPath = 0
        iLastCreatedGroup = iLastCreatedGroup + 1
        gzaCostumeGroups[iLastCreatedGroup] = {}

        --Fill.
        gzaCostumeGroups[iLastCreatedGroup].sHandlerName = sGroupName
        gzaCostumeGroups[iLastCreatedGroup].sFormName = sFormName
        gzaCostumeGroups[iLastCreatedGroup].sCharacterFormPath = sCharFormPath
        gzaCostumeGroups[iLastCreatedGroup].sCharacterCostumePath = sCharCostumePath
        gzaCostumeGroups[iLastCreatedGroup].zaKeyValues = {}
    end

    -- |[Path Creator Function]|
    function fnAddCostumePath(sName, sPath)
        if(gzaCostumeGroups[iLastCreatedGroup] == nil) then return end
        
        --Add space.
        iLastCreatedPath = iLastCreatedPath + 1
        gzaCostumeGroups[iLastCreatedGroup].zaKeyValues[iLastCreatedPath] = {}
        
        --Fill.
        gzaCostumeGroups[iLastCreatedGroup].zaKeyValues[iLastCreatedPath].sName = sName
        gzaCostumeGroups[iLastCreatedGroup].zaKeyValues[iLastCreatedPath].sPath = sPath
    end

    -- |[ ======================= Christine ====================== ]|
    --Christine, Darkmatter
    fnAddCostumeGroup("Christine_Darkmatter", "Darkmatter", "Root/Variables/Global/Christine/sForm", "Root/Variables/Costumes/Christine/sCostumeDarkmatter")
    fnAddCostumePath("Normal", sBasePath .. "Christine/Darkmatter_Normal.lua")
    
    --Christine, Doll
    fnAddCostumeGroup("Christine_Doll", "Doll", "Root/Variables/Global/Christine/sForm", "Root/Variables/Costumes/Christine/sCostumeDoll")
    fnAddCostumePath("Normal",  sBasePath .. "Christine/Doll_Normal.lua")
    fnAddCostumePath("Sweater", sBasePath .. "Christine/Doll_Sweater.lua")
    fnAddCostumePath("Nude",    sBasePath .. "Christine/Doll_Nude.lua")
    
    --Christine, Eldritch Dream Girl
    fnAddCostumeGroup("Christine_Dreamer", "Eldritch", "Root/Variables/Global/Christine/sForm", "Root/Variables/Costumes/Christine/sCostumeDreamer")
    fnAddCostumePath("Normal", sBasePath .. "Christine/Dreamer_Normal.lua")
    
    --Christine, Electrosprite
    fnAddCostumeGroup("Christine_Electrosprite", "Electrosprite", "Root/Variables/Global/Christine/sForm", "Root/Variables/Costumes/Christine/sCostumeElectrosprite")
    fnAddCostumePath("Normal", sBasePath .. "Christine/Electrosprite_Normal.lua")

    --Christine, Golem
    fnAddCostumeGroup("Christine_Golem", "Golem", "Root/Variables/Global/Christine/sForm", "Root/Variables/Costumes/Christine/sCostumeGolem")
    fnAddCostumePath("Normal",  sBasePath .. "Christine/Golem_Normal.lua")
    fnAddCostumePath("Serious", sBasePath .. "Christine/Golem_Serious.lua")
    fnAddCostumePath("Gala",    sBasePath .. "Christine/Golem_GalaDress.lua")
    
    --Christine, Human
    fnAddCostumeGroup("Christine_Human", "Human", "Root/Variables/Global/Christine/sForm", "Root/Variables/Costumes/Christine/sCostumeHuman")
    fnAddCostumePath("Normal", sBasePath .. "Christine/Human_Normal.lua")
    fnAddCostumePath("Nude",   sBasePath .. "Christine/Human_Nude.lua")
    
    --Christine, Latex Drone
    fnAddCostumeGroup("Christine_LatexDrone", "LatexDrone", "Root/Variables/Global/Christine/sForm", "Root/Variables/Costumes/Christine/sCostumeLatexDrone")
    fnAddCostumePath("Normal", sBasePath .. "Christine/LatexDrone_Normal.lua")
    
    --Christine, Male (Human)
    fnAddCostumeGroup("Christine_Male", "Male", "Root/Variables/Global/Christine/sForm", "Root/Variables/Costumes/Christine/sCostumeMale")
    fnAddCostumePath("Normal", sBasePath .. "Christine/Male_Normal.lua")
    
    --Christine, Raibie
    fnAddCostumeGroup("Christine_Raibie", "Raibie", "Root/Variables/Global/Christine/sForm", "Root/Variables/Costumes/Christine/sCostumeRaibie")
    fnAddCostumePath("Normal", sBasePath .. "Christine/Raibie_Normal.lua")
    
    --Christine, Raiju
    fnAddCostumeGroup("Christine_Raiju", "Raiju", "Root/Variables/Global/Christine/sForm", "Root/Variables/Costumes/Christine/sCostumeRaiju")
    fnAddCostumePath("Normal", sBasePath .. "Christine/Raiju_Normal.lua")
    fnAddCostumePath("Nude",   sBasePath .. "Christine/Raiju_Nude.lua")

    --Christine, Steam Droid
    fnAddCostumeGroup("Christine_SteamDroid", "SteamDroid", "Root/Variables/Global/Christine/sForm", "Root/Variables/Costumes/Christine/sCostumeSteamDroid")
    fnAddCostumePath("Normal", sBasePath .. "Christine/SteamDroid_Normal.lua")
    fnAddCostumePath("SX-399", sBasePath .. "Christine/SteamDroid_SX-399.lua")
    
    --Christine, Secrebot
    fnAddCostumeGroup("Christine_Secrebot", "Secrebot", "Root/Variables/Global/Christine/sForm", "Root/Variables/Costumes/Christine/sCostumeSecrebot")
    fnAddCostumePath("Normal", sBasePath .. "Christine/Secrebot_Normal.lua")

    --Christine, Wiregirl
    fnAddCostumeGroup("Christine_Wiregirl", "Wiregirl", "Root/Variables/Global/Christine/sForm", "Root/Variables/Costumes/Christine/sCostumeWiregirl")
    fnAddCostumePath("Normal", sBasePath .. "Christine/Wiregirl_Normal.lua")
    
    -- |[ ======================== Empress ======================= ]|
    fnAddCostumeGroup("Empress_Conquerer", "Conquerer", "Root/Variables/Global/Empress/sCurrentJob", "Root/Variables/Costumes/Empress/sCostumeConquerer")
    fnAddCostumePath("Normal", sBasePath .. "Empress/Conquerer_Normal.lua")
    
    -- |[ ====================== Florentina ====================== ]|
    fnAddCostumeGroup("Florentina_Merchant", "Merchant", "Root/Variables/Global/Florentina/sCurrentJob", "Root/Variables/Costumes/Florentina/sCostumeMerchant")
    fnAddCostumePath("Normal", sBasePath .. "Florentina/Merchant_Normal.lua")
    
    fnAddCostumeGroup("Florentina_TreasureHunter", "TreasureHunter", "Root/Variables/Global/Florentina/sCurrentJob", "Root/Variables/Costumes/Florentina/sCostumeTreasureHunter")
    fnAddCostumePath("Normal", sBasePath .. "Florentina/TreasureHunter_Normal.lua")
    
    fnAddCostumeGroup("Florentina_Mediator", "Mediator", "Root/Variables/Global/Florentina/sCurrentJob", "Root/Variables/Costumes/Florentina/sCostumeMediator")
    fnAddCostumePath("Normal", sBasePath .. "Florentina/Mediator_Normal.lua")
    
    fnAddCostumeGroup("Florentina_Agarist", "Agarist", "Root/Variables/Global/Florentina/sCurrentJob", "Root/Variables/Costumes/Florentina/sCostumeAgarist")
    fnAddCostumePath("Normal", sBasePath .. "Florentina/Agarist_Normal.lua")
    
    fnAddCostumeGroup("Florentina_Lurker", "Lurker", "Root/Variables/Global/Florentina/sCurrentJob", "Root/Variables/Costumes/Florentina/sCostumeLurker")
    fnAddCostumePath("Normal", sBasePath .. "Florentina/Lurker_Normal.lua")
    
    -- |[ ========================= Izuna ======================== ]|
    fnAddCostumeGroup("Izuna_Miko", "Miko", "Root/Variables/Global/Izuna/sCurrentJob", "Root/Variables/Costumes/Izuna/sCostumeMiko")
    fnAddCostumePath("Normal", sBasePath .. "Izuna/Miko_Normal.lua")
    fnAddCostumePath("Nude",   sBasePath .. "Izuna/Miko_Nude.lua")
    
    -- |[ ======================== JX-101 ======================== ]|
    fnAddCostumeGroup("JX-101_Tactician", "Tactician", "Root/Variables/Global/JX-101/sCurrentJob", "Root/Variables/Costumes/JX-101/sCostumeTactician")
    fnAddCostumePath("Normal", sBasePath .. "JX-101/Tactician_Normal.lua")

    -- |[ ========================== Mei ========================= ]|
    fnAddCostumeGroup("Mei_Human", "Human", "Root/Variables/Global/Mei/sForm", "Root/Variables/Costumes/Mei/sCostumeHuman")
    fnAddCostumePath("Normal",      sBasePath .. "Mei/Human_Normal.lua")
    fnAddCostumePath("Mindless",    sBasePath .. "Mei/Human_Mindless.lua")
    fnAddCostumePath("RubberQueen", sBasePath .. "Mei/Human_RubberQueen.lua")
    
    fnAddCostumeGroup("Mei_Alraune", "Alraune", "Root/Variables/Global/Mei/sForm", "Root/Variables/Costumes/Mei/sCostumeAlraune")
    fnAddCostumePath("Normal",   sBasePath .. "Mei/Alraune_Normal.lua")
    fnAddCostumePath("Mindless", sBasePath .. "Mei/Alraune_Mindless.lua")
    
    fnAddCostumeGroup("Mei_Bee", "Bee", "Root/Variables/Global/Mei/sForm", "Root/Variables/Costumes/Mei/sCostumeBee")
    fnAddCostumePath("Normal", sBasePath .. "Mei/Bee_Normal.lua")
    fnAddCostumePath("Queen",  sBasePath .. "Mei/Bee_Queen.lua")
    
    fnAddCostumeGroup("Mei_Ghost", "Ghost", "Root/Variables/Global/Mei/sForm", "Root/Variables/Costumes/Mei/sCostumeGhost")
    fnAddCostumePath("Normal", sBasePath .. "Mei/Ghost_Normal.lua")
    
    fnAddCostumeGroup("Mei_Gravemarker", "Gravemarker", "Root/Variables/Global/Mei/sForm", "Root/Variables/Costumes/Mei/sCostumeGravemarker")
    fnAddCostumePath("Normal", sBasePath .. "Mei/Gravemarker_Normal.lua")
    
    fnAddCostumeGroup("Mei_Mannequin", "Mannequin", "Root/Variables/Global/Mei/sForm", "Root/Variables/Costumes/Mei/sCostumeMannequin")
    fnAddCostumePath("Normal", sBasePath .. "Mei/Mannequin_Normal.lua")
    
    fnAddCostumeGroup("Mei_Rubber", "Rubber", "Root/Variables/Global/Mei/sForm", "Root/Variables/Costumes/Mei/sCostumeRubber")
    fnAddCostumePath("Normal",      sBasePath .. "Mei/Rubber_Normal.lua")
    fnAddCostumePath("RubberQueen", sBasePath .. "Mei/Rubber_RubberQueen.lua")
    
    fnAddCostumeGroup("Mei_Slime", "Slime", "Root/Variables/Global/Mei/sForm", "Root/Variables/Costumes/Mei/sCostumeSlime")
    fnAddCostumePath("Normal", sBasePath .. "Mei/Slime_Normal.lua")
    
    fnAddCostumeGroup("Mei_Werecat", "Werecat", "Root/Variables/Global/Mei/sForm", "Root/Variables/Costumes/Mei/sCostumeWerecat")
    fnAddCostumePath("Normal", sBasePath .. "Mei/Werecat_Normal.lua")
    
    fnAddCostumeGroup("Mei_Wisphag", "Wisphag", "Root/Variables/Global/Mei/sForm", "Root/Variables/Costumes/Mei/sCostumeWisphag")
    fnAddCostumePath("Normal", sBasePath .. "Mei/Wisphag_Normal.lua")
    
    fnAddCostumeGroup("Mei_Zombee", "Zombee", "Root/Variables/Global/Mei/sForm", "Root/Variables/Costumes/Mei/sCostumeZombee")
    fnAddCostumePath("Normal", sBasePath .. "Mei/Zombee_Normal.lua")
    fnAddCostumePath("Queen",  sBasePath .. "Mei/Zombee_Queen.lua")
    
    -- |[ ========================= Odar ========================= ]|
    fnAddCostumeGroup("Odar_Prince", "Prince", "Root/Variables/Global/Odar/sCurrentJob", "Root/Variables/Costumes/Odar/sCostumePrince")
    fnAddCostumePath("Normal", sBasePath .. "Odar/Prince_Normal.lua")
    
    -- |[ ========================= Sammy ======================== ]|
    --Not a conventional character, used in some moth movie sequences.
    fnAddCostumeGroup("Sammy_ActionMoth", "ActionMoth", "TRUE", "Root/Variables/Costumes/Sammy/sCostumeActionMoth")
    fnAddCostumePath("Normal", sBasePath .. "Sammy/ActionMoth_Normal.lua")

    -- |[ ======================== Sanya ========================= ]|
    fnAddCostumeGroup("Sanya_Human", "Human", "Root/Variables/Global/Sanya/sForm", "Root/Variables/Costumes/Sanya/sCostumeHuman")
    fnAddCostumePath("Normal",   sBasePath .. "Sanya/Human_Normal.lua")
    fnAddCostumePath("Carrying", sBasePath .. "Sanya/Human_Carrying.lua")
    fnAddCostumePath("Rifle",    sBasePath .. "Sanya/Human_Rifle.lua")
    fnAddCostumePath("WerebatA", sBasePath .. "Sanya/Human_WerebatA.lua")
    fnAddCostumePath("WerebatB", sBasePath .. "Sanya/Human_WerebatB.lua")
    
    fnAddCostumeGroup("Sanya_Sevavi", "Sevavi", "Root/Variables/Global/Sanya/sForm", "Root/Variables/Costumes/Sanya/sCostumeSevavi")
    fnAddCostumePath("Normal",   sBasePath .. "Sanya/Sevavi_Normal.lua")
    fnAddCostumePath("Carrying", sBasePath .. "Sanya/Sevavi_Carrying.lua")
    fnAddCostumePath("Rifle",    sBasePath .. "Sanya/Sevavi_Rifle.lua")
    
    fnAddCostumeGroup("Sanya_Werebat", "Werebat", "Root/Variables/Global/Sanya/sForm", "Root/Variables/Costumes/Sanya/sCostumeWerebat")
    fnAddCostumePath("Rifle",   sBasePath .. "Sanya/Werebat_Rifle.lua")
    
    fnAddCostumeGroup("Sanya_Bunny", "Bunny", "Root/Variables/Global/Sanya/sForm", "Root/Variables/Costumes/Sanya/sCostumeBunny")
    fnAddCostumePath("Rifle",   sBasePath .. "Sanya/Bunny_Rifle.lua")
    
    fnAddCostumeGroup("Sanya_Harpy", "Harpy", "Root/Variables/Global/Sanya/sForm", "Root/Variables/Costumes/Sanya/sCostumeHarpy")
    fnAddCostumePath("Rifle",   sBasePath .. "Sanya/Harpy_Rifle.lua")
    
    -- |[ ======================== Sophie ======================== ]|
    fnAddCostumeGroup("Sophie_Golem", "Golem", "TRUE", "Root/Variables/Costumes/Sophie/sCostumeGolem")
    fnAddCostumePath("Normal", sBasePath .. "Sophie/Golem_Normal.lua")
    fnAddCostumePath("Gala",   sBasePath .. "Sophie/Golem_GalaDress.lua")
    
    -- |[ ======================== SX-399 ======================== ]|
    fnAddCostumeGroup("SX-399_Shocktrooper", "Shocktrooper", "Root/Variables/Global/SX-399/sCurrentJob", "Root/Variables/Costumes/SX-399/sCostumeShocktrooper")
    fnAddCostumePath("Normal",      sBasePath .. "SX-399/Shocktrooper_Normal.lua")
    fnAddCostumePath("Young Droid", sBasePath .. "SX-399/Shocktrooper_Young Droid.lua")
    
    -- |[ ======================== Tiffany ======================= ]|
    --These three jobs use the same costume.
    fnAddCostumeGroup("Tiffany_Assault", "Assault", "Root/Variables/Global/Tiffany/sCurrentJob", "Root/Variables/Costumes/Tiffany/sCostumeDoll")
    fnAddCostumePath("Normal",   sBasePath .. "Tiffany/Doll_Normal.lua")
    fnAddCostumePath("Sundress", sBasePath .. "Tiffany/Doll_Sundress.lua")
    fnAddCostumeGroup("Tiffany_Support", "Support", "Root/Variables/Global/Tiffany/sCurrentJob", "Root/Variables/Costumes/Tiffany/sCostumeDoll")
    fnAddCostumePath("Normal",   sBasePath .. "Tiffany/Doll_Normal.lua")
    fnAddCostumePath("Sundress", sBasePath .. "Tiffany/Doll_Sundress.lua")
    fnAddCostumeGroup("Tiffany_Subvert", "Subvert", "Root/Variables/Global/Tiffany/sCurrentJob", "Root/Variables/Costumes/Tiffany/sCostumeDoll")
    fnAddCostumePath("Normal",   sBasePath .. "Tiffany/Doll_Normal.lua")
    fnAddCostumePath("Sundress", sBasePath .. "Tiffany/Doll_Sundress.lua")
    fnAddCostumeGroup("Tiffany_Spearhead", "Spearhead", "Root/Variables/Global/Tiffany/sCurrentJob", "Root/Variables/Costumes/Tiffany/sCostumeDoll")
    fnAddCostumePath("Normal",   sBasePath .. "Tiffany/Doll_Normal.lua")
    fnAddCostumePath("Sundress", sBasePath .. "Tiffany/Doll_Sundress.lua")
    
    -- |[ ========================= Zeke ========================= ]|
    fnAddCostumeGroup("Zeke_Goat", "Goat", "Root/Variables/Global/Zeke/sCurrentJob", "Root/Variables/Costumes/Zeke/sCostumeGoat")
    fnAddCostumePath("Normal", sBasePath .. "Zeke/Goat_Normal.lua")
    
    -- |[ ==================== Miscellaneous ===================== ]|
    fnAddCostumeGroup("Rubberbee_Rubber", "Rubber", "TRUE", "Root/Variables/Global/Constants/sNormal")
    fnAddCostumePath("Normal", sBasePath .. "Misc Characters/Rubberbee.lua")
    
    fnAddCostumeGroup("Rubberraune_Rubber", "Rubber", "TRUE", "Root/Variables/Global/Constants/sNormal")
    fnAddCostumePath("Normal", sBasePath .. "Misc Characters/Rubberraune.lua")
    
    -- |[ ===================== Mod Groupings ==================== ]|
    --If there are any mods present, check to see if the characters in them are handled.
    fnExecModScript("CostumeHandlers/XFormAutoresolve.lua")
    
    -- |[ ======================= Clean Up ======================= ]|
    --Deallocate the adder functions.
    fnAddCostumeGroup = nil
    fnAddCostumePath  = nil
end

-- |[ ======================================== Execution ======================================= ]|
local bFoundMatch = false
local sCharacterMatch = ""
local sFormMismatch = ""
local sCostumeMismatch = ""
local iTotalGroups = #gzaCostumeGroups
Debug_Print("Scanning groups.\n")
for i = 1, iTotalGroups, 1 do
    
    --Match:
    if(gzaCostumeGroups[i].sHandlerName == sHandlerName) then
        Debug_Print(" Compare: " .. gzaCostumeGroups[i].sHandlerName .. " to " .. sHandlerName .. "\n")
    
        --Mark partial matches for debug.
        sCharacterMatch = gzaCostumeGroups[i].sHandlerName
    
        --Get the form. It needs to match.
        local sCurrentForm = VM_GetVar(gzaCostumeGroups[i].sCharacterFormPath, "S")
        Debug_Print(" Form Path: " .. gzaCostumeGroups[i].sCharacterFormPath .. "\n")
        Debug_Print(" Form: " .. sCurrentForm .. " to " .. gzaCostumeGroups[i].sFormName .. "\n")
        
        --March in form:
        if(gzaCostumeGroups[i].sCharacterFormPath == "TRUE" or sCurrentForm == gzaCostumeGroups[i].sFormName) then
            
            --Flag.
            Debug_Print("  Form matches.\n")
            sFormMismatch = sCurrentForm
    
            --Get the current costume.
            local sCurrentCostume = VM_GetVar(gzaCostumeGroups[i].sCharacterCostumePath, "S")
            Debug_Print("  Costume path: " .. gzaCostumeGroups[i].sCharacterCostumePath .. "\n")
            Debug_Print("  Costume value: " .. sCurrentCostume .. "\n")
            sCostumeMismatch = sCurrentCostume
    
            --If the path didn't exist, don't bother. The script can be called before the variables get set up.
            if(VM_Exists(gzaCostumeGroups[i].sCharacterCostumePath) == false) then
                Debug_Print("  Costume path for costume " ..  sCurrentCostume .. " did not exist yet.\n")
                bFoundMatch = true
                break
            end
    
            --Scan for matches.
            local iScanCostumes = #gzaCostumeGroups[i].zaKeyValues
            Debug_Print("  There are " .. iScanCostumes .. " costumes available.\n")
            for p = 1, iScanCostumes, 1 do
                Debug_Print("  Costume: " .. sCurrentCostume .. " to " .. gzaCostumeGroups[i].zaKeyValues[p].sName .. "\n")
                
                --Match found!
                if(sCurrentCostume == gzaCostumeGroups[i].zaKeyValues[p].sName) then
                    bFoundMatch = true
                    Debug_Print("   Match. Executing costume script.\n")
                    LM_ExecuteScript(gzaCostumeGroups[i].zaKeyValues[p].sPath)
                    Debug_Print("   Finished costume script execution. Breaking.\n")
                    break
                end
            end
            Debug_Print("  Finished scanning costumes.\n")
        else
            sFormMismatch = sCurrentForm
        end
        
        --Stop iterating on a handler match.
        break
    end
end

--No match, print an error:
if(bFoundMatch == false) then
    Debug_ForcePrint("Error, no costume match found: " .. sHandlerName .. "\n")
    if(sCharacterMatch ~= "") then
        Debug_ForcePrint("   Form mismatch: " .. sCharacterMatch .. " to current form: " .. sFormMismatch .. "\n")
    end
    if(sCostumeMismatch ~= "") then
        Debug_ForcePrint("   Costume mismatch: " .. sCostumeMismatch .. ".\n")
    end
end

--Finish up.
Debug_PopPrint("[ === Finished Costume Autoresolve === ]\n")
