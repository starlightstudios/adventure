-- |[ ============================== Florentina, Merchant, Normal ============================== ]|
-- |[Setup]|
--Set all variables for this script. Special frames and alignments need to be entered below.
-- First, constants across this character.
local sActorName = "Florentina"
local sCombatName = "Florentina"
local sFieldName = "Florentina"
local sFormVar = "Root/Variables/Global/Florentina/sCurrentJob"

--Costume Variables
local sCostumeName = "Normal"
local sCostumeVar = "Root/Variables/Costumes/Florentina/sCostumeMerchant"

--Actor Portrait
local sDialoguePath = "Root/Images/Portraits/FlorentinaDialogue/Merchant"

--Sprite
local sSpritePath = "Root/Images/Sprites/Florentina/"
local sSpecialPath = "Root/Images/Sprites/Special/Florentina|"
local fIndexX = 0
local fIndexY = 6

--Combat
local sCombatPortrait     = "Root/Images/Portraits/Combat/Florentina_Merchant"
local sCombatCountermask  = "Root/Images/Portraits/Combat/FlorentinaCountermask"
local sVictoryCountermask = "Root/Images/Portraits/Combat/FlorentinaCountermask"
local sTurnPortrait       = "Root/Images/AdventureUI/TurnPortraits/Florentina"

--Delayed-Load Groupings
local sDialogueCostumeGrouping = "Florentina Costume Merchant"
local sCombatCostumeGrouping   = sDialogueCostumeGrouping .. " Combat"
local saDialogueGroupingList   = gsaFlorentinaDialogueList
local saCombatGroupingList     = gsaFlorentinaCombatList

-- |[Stop Check]|
--If not in this form, just flag the variable.
local sCharacterForm = VM_GetVar(sFormVar, "S")
VM_SetVar(sCostumeVar, "S", sCostumeName)
if(sCharacterForm ~= "Merchant") then
    return
end

-- |[ ======================================== Dialogue ======================================== ]|
--Standard.
fnSetFlorentinaDialogue(sDialoguePath)

-- |[ ====================================== Field Sprite ====================================== ]|
fnSetFlorentinaFieldSprite(sSpritePath, sSpecialPath, false)
fnSetFlorentinaLuaSpriteLookup(sSpritePath)
fnSetFlorentinaIdleAnimationAdvanced("Root/Images/Sprites/IdleAnim/Florentina|", 8, {0, 1, 2, 3, 4, 5, 6, 7, 8, 8, 8, 2, 3, 4, 5, 6, 7, 8, 8, 8, 9, 10, 11, 12, 13, 14, 11, 12, 13, 14, 11, 12, 13, 14, 10, 9, 0})

-- |[ ===================================== Combat and UI ====================================== ]|
--Basic.
fnSetFlorentinaCombatData(sCombatName, sCombatPortrait, sCombatCountermask, sVictoryCountermask, sTurnPortrait, sSpritePath, fIndexX, fIndexY)

--Alignments.
if(AdvCombat_GetProperty("Does Party Member Exist", sCombatName) == true) then
    AdvCombat_SetProperty("Push Party Member", sCombatName)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Basemenu,        -104, 396)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Base,      117,  74)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Ally,     -216, -78)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Inspector, 156,  52)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equipment,        -57, 107)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Skills,           -24,  78)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status,           -24,  41)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Trainer,          -92, 133)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Vendor,          -128,  67)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Victory,   603,  69)
    DL_PopActiveObject()
end

-- |[ ================================= Loading and Unloading ================================== ]|
--Order all not-in-use dialogue and combat images to unload their data. Then order all in-use 
-- images to load their data. Images that are in multiple sets will correctly load their data
-- if the load is called after the unload.
if(gbUnloadDialoguePortraitsWhenDialogueCloses == false) then
    fnUnloadGrouping(saDialogueGroupingList, sDialogueCostumeGrouping)
    fnLoadGrouping  (saDialogueGroupingList, sDialogueCostumeGrouping)
end

--Combat always loads.
fnUnloadGrouping(saCombatGroupingList, sCombatCostumeGrouping)
fnLoadGrouping  (saCombatGroupingList, sCombatCostumeGrouping)
