-- |[ ===================== Common Functions and Variables for Florentina ====================== ]|
--Some parts of Florentina's costume code is identical across all costumes. This handles those.

-- |[ ================================ fnSetFlorentinaDialogue ================================= ]|
--Sets Mei's emotions using the given dialogue path.
function fnSetFlorentinaDialogue(psDialoguePath)

    --Argument check.
    if(psDialoguePath == nil) then return end

    --Verify dialogue actor exists.
    if(DialogueActor_Exists("Florentina") == false) then return end
    
    --Lurker: Always uses neutral.
    if(psDialoguePath == "Root/Images/Portraits/FlorentinaDialogue/Lurker") then
        DialogueActor_Push("Florentina")
            DialogueActor_SetProperty("Add Emotion", "Neutral",  psDialoguePath, true)
            DialogueActor_SetProperty("Add Emotion", "Happy",    psDialoguePath, true)
            DialogueActor_SetProperty("Add Emotion", "Blush",    psDialoguePath, true)
            DialogueActor_SetProperty("Add Emotion", "Facepalm", psDialoguePath, true)
            DialogueActor_SetProperty("Add Emotion", "Surprise", psDialoguePath, true)
            DialogueActor_SetProperty("Add Emotion", "Offended", psDialoguePath, true)
            DialogueActor_SetProperty("Add Emotion", "Confused", psDialoguePath, true)
            DialogueActor_SetProperty("Add Emotion", "Rubber", "Root/Images/Portraits/FlorentinaDialogue/MerchantRubber", true)
        DL_PopActiveObject()
        
    --Normal case:
    else
        DialogueActor_Push("Florentina")
            DialogueActor_SetProperty("Add Emotion", "Neutral",  psDialoguePath .. "Neutral",  true)
            DialogueActor_SetProperty("Add Emotion", "Happy",    psDialoguePath .. "Happy",    true)
            DialogueActor_SetProperty("Add Emotion", "Blush",    psDialoguePath .. "Blush",    true)
            DialogueActor_SetProperty("Add Emotion", "Facepalm", psDialoguePath .. "Facepalm", true)
            DialogueActor_SetProperty("Add Emotion", "Surprise", psDialoguePath .. "Surprise", true)
            DialogueActor_SetProperty("Add Emotion", "Offended", psDialoguePath .. "Offended", true)
            DialogueActor_SetProperty("Add Emotion", "Confused", psDialoguePath .. "Confused", true)
            
            --Rubber always uses the merchant variant.
            DialogueActor_SetProperty("Add Emotion", "Rubber", "Root/Images/Portraits/FlorentinaDialogue/MerchantRubber", true)
            
        DL_PopActiveObject()
    end
end

-- |[ =============================== fnSetFlorentinaFieldSprite =============================== ]|
--If the field sprite for Florentina exists, updates its properties and special frames.
function fnSetFlorentinaFieldSprite(psSpritePath, psSpecialPath)
    
    --Argument check.
    if(psSpritePath == nil) then return end
    if(psSpecialPath == nil) then return end
    
    --No field sprite, exit.
    if(EM_Exists("Florentina") == false) then return end
    
    --Push.
    EM_PushEntity("Florentina")
    
        --Base.
        fnSetCharacterGraphics(psSpritePath, true)
        
        --Animation properties.
        TA_SetProperty("Auto Animates",       false)
        TA_SetProperty("Y Oscillates",        false)
        TA_SetProperty("No Footstep Sound",   false)
        TA_SetProperty("No Automatic Shadow", false)
        
        --Special frames.
        TA_SetProperty("Wipe Special Frames")
        TA_SetProperty("Add Special Frame", "Wounded", psSpecialPath .. "Wounded")
        TA_SetProperty("Add Special Frame", "Crouch",  psSpecialPath .. "Crouch")
        
        --Sleep doesn't have a class-specific special frame.
		TA_SetProperty("Add Special Frame", "Sleep",   "Root/Images/Sprites/Special/Florentina|Sleep")
        
        --Drinking contest.
		TA_SetProperty("Add Special Frame", "Drink0", psSpecialPath .. "Drink0")
		TA_SetProperty("Add Special Frame", "Drink1", psSpecialPath .. "Drink1")
		TA_SetProperty("Add Special Frame", "Drink2", psSpecialPath .. "Drink2")
    
    --Clean.
    DL_PopActiveObject()

end

-- |[ ============================== fnSetFlorentinaIdleAnimation ============================== ]|
--Sets the idle animation for Florentina in the given form. Pass "Null" to disable. This one uses a simple
-- endless loop case, or a play-once-then-done case.
function fnSetFlorentinaIdleAnimation(psDLPathPattern, piFramesTotal, piTPF, piLoopFrame)
    
    --Arg check.
    if(psDLPathPattern == nil) then return end
    if(piFramesTotal   == nil) then return end
    if(piTPF           == nil) then return end
    if(piLoopFrame     == nil) then return end
    
    --Do nothing if the actor does not exist.
    if(EM_Exists("Florentina") == false) then return end
    
    --Disable if "Null".
    if(psDLPathPattern == "Null") then
        EM_PushEntity("Florentina")
            TA_SetProperty("Idle Animation Flag", false)
        DL_PopActiveObject()
        return
    end
    
    --Legality check.
    if(piFramesTotal < 1) then return end
    
    --Otherwise, set the idle animations.
    EM_PushEntity("Florentina")
        TA_SetProperty("Idle Animation Flag", true)
        TA_SetProperty("Allocate Idle Frames", piFramesTotal)
        for i = 0, piFramesTotal-1, 1 do
            TA_SetProperty("Set Idle Frame", i, psDLPathPattern .. i)
        end
        TA_SetProperty("Set Idle TPF", piTPF)
        TA_SetProperty("Set Idle Loop Frame", piLoopFrame)
    DL_PopActiveObject()
end

--Sets the idle animation for Florentina in a given form. This one uses a list of the frames, which allows
-- for complicated, though finite, looping. The animation is one-then-done but can re-use frames.
function fnSetFlorentinaIdleAnimationAdvanced(psDLPathPattern, piTPF, piaFrameList)
    
    --Arg check.
    if(psDLPathPattern == nil) then return end
    if(piTPF           == nil) then return end
    if(piaFrameList    == nil) then return end
    
    --Do nothing if the actor does not exist.
    if(EM_Exists("Florentina") == false) then return end
    
    --Disable if "Null".
    if(psDLPathPattern == "Null") then
        EM_PushEntity("Florentina")
            TA_SetProperty("Idle Animation Flag", false)
        DL_PopActiveObject()
        return
    end
    
    --Legality check.
    if(#piaFrameList < 1) then return end
    
    --Otherwise, set the idle animations.
    EM_PushEntity("Florentina")
        TA_SetProperty("Idle Animation Flag", true)
        TA_SetProperty("Allocate Idle Frames", #piaFrameList)
        for i = 1, #piaFrameList, 1 do
            TA_SetProperty("Set Idle Frame", i-1, psDLPathPattern .. piaFrameList[i])
        end
        TA_SetProperty("Set Idle TPF", piTPF)
        TA_SetProperty("Set Idle Loop Frame", -1)
    DL_PopActiveObject()
    
end

-- |[ ============================= fnSetFlorentinaLuaSpriteLookup ============================= ]|
--Updates which sprites the lua code uses to refer to this character.
function fnSetFlorentinaLuaSpriteLookup(psSpritePath)
    
    --Argument check.
    if(psSpritePath == nil) then return end
    
    --Iterate.
    for i = 1, giCharSpritesTotal, 1 do
        
        --Match.
        if(gsCharSprites[i][1] == "Florentina") then
            gsCharSprites[i][2] = psSpritePath .. "SW|0"
            break
        end
    end
    
end

-- |[ =============================== fnSetFlorentinaCombatData ================================ ]|
--Sets combat data for the given party member. Does not set UI rendering positions.
function fnSetFlorentinaCombatData(psCombatName, psPortrait, psCombatCountermask, psVictoryCountermask, psTurnPortrait, psSpritePath, pfIndexX, pfIndexY)
    
    --Argument check.
    if(psCombatName         == nil) then return end
    if(psPortrait           == nil) then return end
    if(psCombatCountermask  == nil) then return end
    if(psVictoryCountermask == nil) then return end
    if(psTurnPortrait       == nil) then return end
    if(psSpritePath         == nil) then return end
    if(pfIndexX             == nil) then return end
    if(pfIndexY             == nil) then return end
    
    --Existence check.
    if(AdvCombat_GetProperty("Does Party Member Exist", psCombatName) == false) then return end
    
    --Push.
    AdvCombat_SetProperty("Push Party Member", psCombatName)
    
        --Portraits
        AdvCombatEntity_SetProperty("Combat Portrait",     psPortrait)
        AdvCombatEntity_SetProperty("Combat Countermask",  psCombatCountermask)
        AdvCombatEntity_SetProperty("Victory Countermask", psVictoryCountermask)
        AdvCombatEntity_SetProperty("Turn Icon",           psTurnPortrait)
    
        --Vendor Sprites
        AdvCombatEntity_SetProperty("Vendor Image", 0, psSpritePath .. "SW|0")
        AdvCombatEntity_SetProperty("Vendor Image", 1, psSpritePath .. "SW|1")
        AdvCombatEntity_SetProperty("Vendor Image", 2, psSpritePath .. "SW|2")
        AdvCombatEntity_SetProperty("Vendor Image", 3, psSpritePath .. "SW|3")
    
        --Face Table, used on the Equipment UI to show who has the item equipped.
        local fLft = (pfIndexX+0) * gci_FaceTable_Size
        local fTop = (pfIndexY+0) * gci_FaceTable_Size
        local fRgt = (pfIndexX+1) * gci_FaceTable_Size
        local fBot = (pfIndexY+1) * gci_FaceTable_Size
        AdvCombatEntity_SetProperty("Face Table Data", fLft, fTop, fRgt, fBot, "Root/Images/AdventureUI/ItemIcons/CharacterFaces")

    --Clean.
    DL_PopActiveObject()
    
end
