-- |[ =============================== Florentina, Lurker, Normal =============================== ]|
-- |[Setup]|
--Set all variables for this script. Special frames and alignments need to be entered below.
-- First, constants across this character.
local sActorName = "Florentina"
local sCombatName = "Florentina"
local sFieldName = "Florentina"
local sFormVar = "Root/Variables/Global/Florentina/sCurrentJob"

--Costume Variables
local sCostumeName = "Normal"
local sCostumeVar = "Root/Variables/Costumes/Florentina/sCostumeLurker"

--Actor Portrait
local sDialoguePath = "Root/Images/Portraits/FlorentinaDialogue/Lurker"

--Sprite
local sSpritePath = "Root/Images/Sprites/FlorentinaLurker/"
local sSpecialPath = "Root/Images/Sprites/Special/FlorentinaLurker|"
local fIndexX = 0
local fIndexY = 10

--Combat
local sCombatPortrait     = "Root/Images/Portraits/Combat/Florentina_Lurker"
local sCombatCountermask  = "Null"
local sVictoryCountermask = "Null"
local sTurnPortrait       = "Root/Images/AdventureUI/TurnPortraits/FlorentinaLurk"

--Delayed-Load Groupings
local sDialogueCostumeGrouping = "Florentina Costume Lurker"
local sCombatCostumeGrouping   = sDialogueCostumeGrouping .. " Combat"
local saDialogueGroupingList   = gsaFlorentinaDialogueList
local saCombatGroupingList     = gsaFlorentinaCombatList

-- |[Stop Check]|
--If not in this form, just flag the variable.
local sCharacterForm = VM_GetVar(sFormVar, "S")
VM_SetVar(sCostumeVar, "S", sCostumeName)
if(sCharacterForm ~= "Lurker") then
    return
end

-- |[ ======================================== Dialogue ======================================== ]|
--Standard.
fnSetFlorentinaDialogue(sDialoguePath)

-- |[ ====================================== Field Sprite ====================================== ]|
fnSetFlorentinaFieldSprite(sSpritePath, sSpecialPath, false)
fnSetFlorentinaLuaSpriteLookup(sSpritePath)

--Randomly select one of the three idle frames each time this form is entered.
local iRoll = LM_GetRandomNumber(0, 2)
fnSetFlorentinaIdleAnimationAdvanced("Root/Images/Sprites/IdleAnim/FlorentinaLurker|", 6000, {iRoll})

-- |[ ===================================== Combat and UI ====================================== ]|
--Basic.
fnSetFlorentinaCombatData(sCombatName, sCombatPortrait, sCombatCountermask, sVictoryCountermask, sTurnPortrait, sSpritePath, fIndexX, fIndexY)

--Alignments.
if(AdvCombat_GetProperty("Does Party Member Exist", sCombatName) == true) then
    AdvCombat_SetProperty("Push Party Member", sCombatName)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Basemenu,          -1, 429)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Base,      209,  95)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Ally,     -140, -53)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Inspector, 269,  89)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equipment,         57,  76)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Skills,            84, 110)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status,            75,  56)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Trainer,           23, 169)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Vendor,           -35, 105)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Victory,   655,  88)
    DL_PopActiveObject()
end

-- |[ ================================= Loading and Unloading ================================== ]|
--Order all not-in-use dialogue and combat images to unload their data. Then order all in-use 
-- images to load their data. Images that are in multiple sets will correctly load their data
-- if the load is called after the unload.
if(gbUnloadDialoguePortraitsWhenDialogueCloses == false) then
    fnUnloadGrouping(saDialogueGroupingList, sDialogueCostumeGrouping)
    fnLoadGrouping  (saDialogueGroupingList, sDialogueCostumeGrouping)
end

--Combat always loads.
fnUnloadGrouping(saCombatGroupingList, sCombatCostumeGrouping)
fnLoadGrouping  (saCombatGroupingList, sCombatCostumeGrouping)
