-- |[ =============================== Florentina, Agarist, Normal ============================== ]|
-- |[Setup]|
--Set all variables for this script. Special frames and alignments need to be entered below.
-- First, constants across this character.
local sActorName = "Florentina"
local sCombatName = "Florentina"
local sFieldName = "Florentina"
local sFormVar = "Root/Variables/Global/Florentina/sCurrentJob"

--Costume Variables
local sCostumeName = "Normal"
local sCostumeVar = "Root/Variables/Costumes/Florentina/sCostumeAgarist"

--Actor Portrait
local sDialoguePath = "Root/Images/Portraits/FlorentinaDialogue/Agarist"

--Sprite
local sSpritePath = "Root/Images/Sprites/FlorentinaAgarist/"
local sSpecialPath = "Root/Images/Sprites/Special/FlorentinaAgarist|"
local fIndexX = 0
local fIndexY = 8

--Combat
local sCombatPortrait     = "Root/Images/Portraits/Combat/Florentina_Agarist"
local sCombatCountermask  = "Null"
local sVictoryCountermask = "Null"
local sTurnPortrait       = "Root/Images/AdventureUI/TurnPortraits/FlorentinaAg"

--Delayed-Load Groupings
local sDialogueCostumeGrouping = "Florentina Costume Agarist"
local sCombatCostumeGrouping   = sDialogueCostumeGrouping .. " Combat"
local saDialogueGroupingList   = gsaFlorentinaDialogueList
local saCombatGroupingList     = gsaFlorentinaCombatList

-- |[Stop Check]|
--If not in this form, just flag the variable.
local sCharacterForm = VM_GetVar(sFormVar, "S")
VM_SetVar(sCostumeVar, "S", sCostumeName)
if(sCharacterForm ~= "Agarist") then
    return
end

-- |[ ======================================== Dialogue ======================================== ]|
--Standard.
fnSetFlorentinaDialogue(sDialoguePath)

-- |[ ====================================== Field Sprite ====================================== ]|
fnSetFlorentinaFieldSprite(sSpritePath, sSpecialPath, false)
fnSetFlorentinaLuaSpriteLookup(sSpritePath)

--Randomly select one of the three idle frames each time this form is entered.
fnSetFlorentinaIdleAnimationAdvanced("Root/Images/Sprites/IdleAnim/FlorentinaAgarist|", 15, {0, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 1, 0})

-- |[ ===================================== Combat and UI ====================================== ]|
--Basic.
fnSetFlorentinaCombatData(sCombatName, sCombatPortrait, sCombatCountermask, sVictoryCountermask, sTurnPortrait, sSpritePath, fIndexX, fIndexY)

--Alignments.
if(AdvCombat_GetProperty("Does Party Member Exist", sCombatName) == true) then
    AdvCombat_SetProperty("Push Party Member", sCombatName)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Basemenu,          41, 423)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Base,      259, 102)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Ally,      -85, -46)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Inspector, 309,  87)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equipment,         91,  74)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Skills,           130, 103)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status,           126,  65)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Trainer,           47, 156)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Vendor,             5, 101)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Victory,   676,  90)
    DL_PopActiveObject()
end

-- |[ ================================= Loading and Unloading ================================== ]|
--Order all not-in-use dialogue and combat images to unload their data. Then order all in-use 
-- images to load their data. Images that are in multiple sets will correctly load their data
-- if the load is called after the unload.
if(gbUnloadDialoguePortraitsWhenDialogueCloses == false) then
    fnUnloadGrouping(saDialogueGroupingList, sDialogueCostumeGrouping)
    fnLoadGrouping  (saDialogueGroupingList, sDialogueCostumeGrouping)
end

--Combat always loads.
fnUnloadGrouping(saCombatGroupingList, sCombatCostumeGrouping)
fnLoadGrouping  (saCombatGroupingList, sCombatCostumeGrouping)
