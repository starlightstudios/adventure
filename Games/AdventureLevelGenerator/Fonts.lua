-- |[ ======================================= Boot Fonts ======================================= ]|
--Boots fonts used by Adventure Mode's Map Generator
if(gbBootedAdventureGeneratorFonts == true) then return end
gbBootedAdventureGeneratorFonts = true

-- |[Setup]|
--Paths.
local sEngineFontPath = "Data/Scripts/Fonts/"
local sLocalFontPath  = fnResolvePath()

--Special Flags
local ciFontNoFlags = 0
local ciFontNearest  = Font_GetProperty("Constant Precache With Nearest")
local ciFontEdge     = Font_GetProperty("Constant Precache With Edge")
local ciFontDownfade = Font_GetProperty("Constant Precache With Downfade")
local ciFontSpecialS = Font_GetProperty("Constant Precache With Special S")

-- |[ ===================================== Font Registry ====================================== ]|
--Mister Pixel
Font_SetProperty("Downfade", 0.90, 0.60)
Font_SetProperty("Outline Width", 1)
Font_Register("Mister Pixel 16 DFO", sLocalFontPath .. "MisterPixel.ttf", sLocalFontPath .. "MisterPixel16_Kerning.lua", 16, ciFontNearest + ciFontEdge + ciFontDownfade)

--Sanchez
Font_SetProperty("Downfade", 0.90, 0.60)
Font_SetProperty("Outline Width", 1)
Font_Register("Sanchez 22 DFO", sEngineFontPath .. "SanchezRegular.otf", sEngineFontPath .. "Sanchez20_Kerning.lua", 22,  ciFontEdge + ciFontDownfade)

-- |[ ========================================= Aliases ======================================== ]|
--Mister Pixel 16 DFO
Font_SetProperty("Add Alias", "Mister Pixel 16 DFO", "Adlev Generator Small")

--Sanchez 22
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "Adlev Generator Main")
