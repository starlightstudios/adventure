-- |[ ======================================== Game Info ======================================= ]|
--This file is executed at program startup to specify that this is a game the engine can load.
local sBasePath       = fnResolvePath()
local sGameName       = "Level Generator"
local sGamePath       = "Root/Paths/System/Startup/sLevelGeneratorPath" --Must exist in the C++ listing.
local sButtonText     = "Show Level Generator"
local iPriority       = gciTools_Set_Start + 0
local sLauncherScript = "YMenuLaunch.lua" --File in the same folder as this file that will launch the program.
local iNegativeLen    = -10 --Indicates length of launcher name. ZLaunch.lua is -12.

--Local variables.
local zLaunchPath = sBasePath .. "Grids.lua"

-- |[Compatibility Check]|
if(LM_IsGameRegistered("Pandemonium") == false) then
    if(SysPaths.bShowFailedRegistrations) then
        io.write("Not registering Adventure Level Generator. Engine is incompatible.\n")
    end
    return
end

--Add the game entry. Does nothing if it already exists.
SysPaths:fnAddGameEntry(sGameName, sGamePath, sButtonText, iPriority, sLauncherScript, iNegativeLen)

--Add search path.
SysPaths:fnAddSearchPathToEntry(sGameName, zLaunchPath)
